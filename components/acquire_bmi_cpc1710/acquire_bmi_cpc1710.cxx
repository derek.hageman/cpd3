/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_bmi_cpc1710.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

static const char *instrumentFlagTranslation[16] = {"EEPROMError",              /* 0x0001 */
                                                    "ConfigurationError",       /* 0x0002 */
                                                    "RTCReset",                 /* 0x0004 */
                                                    "RTCError",                 /* 0x0008 */
                                                    "SDCardError",              /* 0x0010 */
                                                    "SDCardFormatError",        /* 0x0020 */
                                                    "SDCardFull",               /* 0x0040 */
                                                    "SaturatorPumpWarning",     /* 0x0080 */
                                                    "LiquidLow",                /* 0x0100 */
                                                    "TemperatureControlError",  /* 0x0200 */
                                                    "Overheating",              /* 0x0400 */
                                                    "OpticsThermistorError",    /* 0x0800 */
                                                    "CondenserThermistorError", /* 0x1000 */
                                                    "SaturatorTopThermistorError",/* 0x2000 */
                                                    "SaturatorBottomThermistorError",/* 0x4000 */
                                                    "InletThermistorError",     /* 0x8000 */
};

AcquireBMICPC1710::Configuration::Configuration() : start(FP::undefined()),
                                                    end(FP::undefined()),
                                                    calculateConcentration(false),
                                                    pollInterval(0.5)
{ }

AcquireBMICPC1710::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          calculateConcentration(other.calculateConcentration),
          pollInterval(other.pollInterval)
{ }

void AcquireBMICPC1710::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("BMI");
    instrumentMeta["Model"].setString("1710");

    memset(streamAge, 0, sizeof(streamAge));
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
    }

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    forceRealtimeStateEmit = true;

    lastSampleFlow = FP::undefined();
}

AcquireBMICPC1710::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s), end(e), calculateConcentration(false), pollInterval(0.5)
{
    setFromSegment(other);
}

AcquireBMICPC1710::Configuration::Configuration(const Configuration &under,
                                                const ValueSegment &over,
                                                double s,
                                                double e) : start(s),
                                                            end(e),
                                                            calculateConcentration(
                                                                    under.calculateConcentration),
                                                            pollInterval(under.pollInterval)
{
    setFromSegment(over);
}

void AcquireBMICPC1710::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["CalculateConcentration"].exists())
        calculateConcentration = config["CalculateConcentration"].toBool();
    if (FP::defined(config["PollInterval"].toDouble()))
        pollInterval = config["PollInterval"].toDouble();
}

AcquireBMICPC1710::AcquireBMICPC1710(const ValueSegment::Transfer &configData,
                                     const std::string &loggingContext) : FramedInstrument(
        "bmi1710", loggingContext),
                                                                          autoprobeStatus(
                                                                                  AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                          responseState(
                                                                                  RESP_PASSIVE_WAIT),
                                                                          loggingMux(
                                                                                  LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireBMICPC1710::setToAutoprobe()
{ responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE; }

void AcquireBMICPC1710::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireBMICPC1710Component::getBaseOptions()
{
    ComponentOptions options;

    options.add("recalculate", new ComponentOptionBoolean(tr("recalculate", "name"),
                                                          tr("Recalculate concentration"),
                                                          tr("When enabled the concentration is calculated from the count rate "
                                                             "and reported flow."), QString()));

    return options;
}

AcquireBMICPC1710::AcquireBMICPC1710(const ComponentOptions &options,
                                     const std::string &loggingContext) : FramedInstrument(
        "bmi1710", loggingContext),
                                                                          autoprobeStatus(
                                                                                  AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                          responseState(
                                                                                  RESP_PASSIVE_WAIT),
                                                                          loggingMux(
                                                                                  LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());

    if (options.isSet("recalculate")) {
        config.first().calculateConcentration =
                qobject_cast<ComponentOptionBoolean *>(options.get("recalculate"))->get();
    }
}

AcquireBMICPC1710::~AcquireBMICPC1710()
{
}


void AcquireBMICPC1710::logValue(double frameTime,
                                 SequenceName::Component name,
                                 Variant::Root &&value,
                                 int streamID)
{
    double startTime = streamTime[streamID];
    double endTime = frameTime;
    streamAge[streamID]++;
    streamTime[streamID] = endTime;
    Q_ASSERT(FP::defined(endTime));

    if (!FP::defined(startTime) || startTime == endTime || !loggingEgress) {
        if (loggingEgress)
            streamTime[streamID] = endTime;
        if (!realtimeEgress)
            return;
        realtimeEgress->emplaceData(SequenceName({}, "raw", std::move(name)), std::move(value),
                                    frameTime, frameTime + 0.5);
        return;
    }

    SequenceValue
            dv(SequenceName({}, "raw", std::move(name)), std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        loggingMux.incoming(streamID, std::move(dv), loggingEgress);
        return;
    }

    loggingMux.incoming(streamID, dv, loggingEgress);
    dv.setStart(endTime);
    dv.setEnd(endTime + 1.0);
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireBMICPC1710::realtimeValue(double time,
                                      SequenceName::Component name,
                                      Variant::Root &&value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

void AcquireBMICPC1710::invalidateLogValues(double frameTime)
{
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;

        if (loggingEgress != NULL)
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    loggingMux.clear();
    reportedFlags.clear();
    loggingLost(frameTime);
}

SequenceValue::Transfer AcquireBMICPC1710::buildLogMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_bmi_cpc1710");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);

    result.emplace_back(SequenceName({}, "raw_meta", "N"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Condensation nuclei concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));

    result.emplace_back(SequenceName({}, "raw_meta", "Tu"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Inlet temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Inlet"));

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Saturator bottom temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Saturator bottom"));

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Saturator top temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Saturator top"));

    result.emplace_back(SequenceName({}, "raw_meta", "T3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Condenser temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Condenser"));

    result.emplace_back(SequenceName({}, "raw_meta", "T4"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Optics temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Optics"));

    result.emplace_back(SequenceName({}, "raw_meta", "Q1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));

    result.emplace_back(SequenceName({}, "raw_meta", "Q2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Saturator flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Saturator"));

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("CoincidenceCorrected")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1710");
    result.back()
          .write()
          .metadataSingleFlag("CoincidenceCorrected")
          .hash("Description")
          .setString("Particle concentration has a coincidence correction applied");

    result.back()
          .write()
          .metadataSingleFlag("EEPROMError")
          .hash("Description")
          .setString("Instrument unable to read or write to EEPROM");
    result.back().write().metadataSingleFlag("EEPROMError").hash("Bits").setInt64(0x00010000);
    result.back()
          .write()
          .metadataSingleFlag("EEPROMError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1710");

    result.back()
          .write()
          .metadataSingleFlag("ConfigurationError")
          .hash("Description")
          .setString("Instrument saved configuration value out of range");
    result.back()
          .write()
          .metadataSingleFlag("ConfigurationError")
          .hash("Bits")
          .setInt64(0x00020000);
    result.back()
          .write()
          .metadataSingleFlag("ConfigurationError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1710");

    result.back()
          .write()
          .metadataSingleFlag("RTCReset")
          .hash("Description")
          .setString("Instrument internal RTC time has been reset (check RTC battery)");
    result.back().write().metadataSingleFlag("RTCReset").hash("Bits").setInt64(0x00040000);
    result.back()
          .write()
          .metadataSingleFlag("RTCReset")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1710");

    result.back()
          .write()
          .metadataSingleFlag("RTCError")
          .hash("Description")
          .setString("Instrument unable to communicate with RTC chip");
    result.back().write().metadataSingleFlag("RTCError").hash("Bits").setInt64(0x00080000);
    result.back()
          .write()
          .metadataSingleFlag("RTCError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1710");

    result.back()
          .write()
          .metadataSingleFlag("SDCardError")
          .hash("Description")
          .setString("Instrument SD card error");
    result.back().write().metadataSingleFlag("SDCardError").hash("Bits").setInt64(0x00100000);
    result.back()
          .write()
          .metadataSingleFlag("SDCardError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1710");

    result.back()
          .write()
          .metadataSingleFlag("SDCardFormatError")
          .hash("Description")
          .setString("Instrument SD card filesystem is not FAT32");
    result.back().write().metadataSingleFlag("SDCardFormatError").hash("Bits").setInt64(0x00200000);
    result.back()
          .write()
          .metadataSingleFlag("SDCardFormatError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1710");

    result.back()
          .write()
          .metadataSingleFlag("SDCardFull")
          .hash("Description")
          .setString("Instrument SD card is full");
    result.back().write().metadataSingleFlag("SDCardFull").hash("Bits").setInt64(0x00400000);
    result.back()
          .write()
          .metadataSingleFlag("SDCardFull")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1710");

    result.back()
          .write()
          .metadataSingleFlag("SaturatorPumpWarning")
          .hash("Description")
          .setString("Saturator pump at maximum power");
    result.back()
          .write()
          .metadataSingleFlag("SaturatorPumpWarning")
          .hash("Bits")
          .setInt64(0x00800000);
    result.back()
          .write()
          .metadataSingleFlag("SaturatorPumpWarning")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1710");

    result.back()
          .write()
          .metadataSingleFlag("LiquidLow")
          .hash("Description")
          .setString("Instrument reporting low butanol level");
    result.back().write().metadataSingleFlag("LiquidLow").hash("Bits").setInt64(0x01000000);
    result.back()
          .write()
          .metadataSingleFlag("LiquidLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1710");

    result.back()
          .write()
          .metadataSingleFlag("TemperatureControlError")
          .hash("Description")
          .setString("Condenser temperature greater than inlet temperature (check condenser fan)");
    result.back()
          .write()
          .metadataSingleFlag("TemperatureControlError")
          .hash("Bits")
          .setInt64(0x02000000);
    result.back()
          .write()
          .metadataSingleFlag("TemperatureControlError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1710");

    result.back()
          .write()
          .metadataSingleFlag("Overheating")
          .hash("Description")
          .setString("Condenser temperature greater than 45\xC2\xB0\x43");
    result.back().write().metadataSingleFlag("Overheating").hash("Bits").setInt64(0x04000000);
    result.back()
          .write()
          .metadataSingleFlag("Overheating")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1710");

    result.back()
          .write()
          .metadataSingleFlag("OpticsThermistorError")
          .hash("Description")
          .setString("Optics thermistor malfunctioning or out of range");
    result.back()
          .write()
          .metadataSingleFlag("OpticsThermistorError")
          .hash("Bits")
          .setInt64(0x08000000);
    result.back()
          .write()
          .metadataSingleFlag("OpticsThermistorError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1710");

    result.back()
          .write()
          .metadataSingleFlag("CondenserThermistorError")
          .hash("Description")
          .setString("Condenser thermistor malfunctioning or out of range");
    result.back()
          .write()
          .metadataSingleFlag("CondenserThermistorError")
          .hash("Bits")
          .setInt64(0x10000000);
    result.back()
          .write()
          .metadataSingleFlag("CondenserThermistorError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1710");

    result.back()
          .write()
          .metadataSingleFlag("SaturatorTopThermistorError")
          .hash("Description")
          .setString("Saturator top thermistor malfunctioning or out of range");
    result.back()
          .write()
          .metadataSingleFlag("SaturatorTopThermistorError")
          .hash("Bits")
          .setInt64(0x20000000);
    result.back()
          .write()
          .metadataSingleFlag("SaturatorTopThermistorError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1710");

    result.back()
          .write()
          .metadataSingleFlag("SaturatorBottomThermistorError")
          .hash("Description")
          .setString("Saturator bottom thermistor malfunctioning or out of range");
    result.back()
          .write()
          .metadataSingleFlag("SaturatorBottomThermistorError")
          .hash("Bits")
          .setInt64(0x40000000);
    result.back()
          .write()
          .metadataSingleFlag("SaturatorBottomThermistorError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1710");

    result.back()
          .write()
          .metadataSingleFlag("InletThermistorError")
          .hash("Description")
          .setString("Inlet thermistor malfunctioning or out of range");
    result.back()
          .write()
          .metadataSingleFlag("InletThermistorError")
          .hash("Bits")
          .setInt64(0x80000000);
    result.back()
          .write()
          .metadataSingleFlag("InletThermistorError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1710");

    return result;
}

SequenceValue::Transfer AcquireBMICPC1710::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_bmi_cpc1710");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);

    result.emplace_back(SequenceName({}, "raw_meta", "C"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Count rate"));

    result.emplace_back(SequenceName({}, "raw_meta", "PCT1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Saturator bottom heater power");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Saturator bottom"));

    result.emplace_back(SequenceName({}, "raw_meta", "PCT2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Saturator top heater power");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Saturator top"));

    result.emplace_back(SequenceName({}, "raw_meta", "PCT3"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Condenser heater power");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Condenser"));

    result.emplace_back(SequenceName({}, "raw_meta", "PCT4"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Optics heater power");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Optics"));

    result.emplace_back(SequenceName({}, "raw_meta", "PCT5"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Saturator pump power");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Saturator pump"));


    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    /*result.back().write().metadataString("Realtime").hash("Hide").
        setBool(true);*/
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(8);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidRecord")
          .setString(QObject::tr("NO COMMS: Invalid record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveEnableReportLabel")
          .setString(QObject::tr("STARTING COMMS: Enabling report labels"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadFirmwareVersion")
          .setString(QObject::tr("STARTING COMMS: Reading firmware version"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveDiscardFirst")
          .setString(QObject::tr("STARTING COMMS: Flushing status report"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveStartReadFirstRecord")
          .setString(QObject::tr("STARTING COMMS: Waiting for first record"));

    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("EEPROMError")
          .setString(QObject::tr("EEPROM read/write error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("ConfigurationError")
          .setString(QObject::tr("Invalid stored configuration value"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("RTCReset")
          .setString(QObject::tr("Internal RTC reset (check battery)"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("RTCError")
          .setString(QObject::tr("Internal RTC read/write error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SDCardError")
          .setString(QObject::tr("Internal SD card error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SDCardFormatError")
          .setString(QObject::tr("Internal SD is not FAT32"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SDCardFull")
          .setString(QObject::tr("Internal SD is full"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SaturatorPumpWarning")
          .setString(QObject::tr("Saturator pump at maximum"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("LiquidLow")
          .setString(QObject::tr("LOW BUTANOL"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("TemperatureControlError")
          .setString(QObject::tr("Condenser temperature control error (check fan)"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Overheating")
          .setString(QObject::tr("Condenser overheating"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("OpticsThermistorError")
          .setString(QObject::tr("Optics thermistor error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("CondenserThermistorError")
          .setString(QObject::tr("Condenser thermistor error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SaturatorTopThermistorError")
          .setString(QObject::tr("Saturator top thermistor error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SaturatorBottomThermistorError")
          .setString(QObject::tr("Saturator bottom thermistor error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InletThermistorError")
          .setString(QObject::tr("Inlet thermistor error"));

    return result;
}

SequenceMatch::Composite AcquireBMICPC1710::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    return sel;
}

static double calculateConcentration(double C, double Q, double tau = 0.5E-6)
{
    if (!FP::defined(C) || !FP::defined(Q) || !FP::defined(tau))
        return FP::undefined();
    if (Q <= 0.0)
        return 0.0;
    C *= exp(C * tau);
    return C / (Q * (1000.0 / 60.0));
}

static int extractNextKey(Util::ByteView &data, Util::ByteView &key, Util::ByteView &value)
{
    auto idxSpace = data.indexOf(' ');
    while (idxSpace == 0) {
        data = data.mid(1);
        idxSpace = data.indexOf(' ');
    }
    if (idxSpace == data.npos) {
        if (data.empty())
            return -1;
        key = data;
        data = Util::ByteView();
    } else {
        Q_ASSERT(idxSpace > 0);
        key = data.mid(0, idxSpace);
        Q_ASSERT(!key.empty());
        data = data.mid(idxSpace + 1);
    }

    auto idxEqual = key.indexOf('=');
    if (idxEqual == 0)
        return 1;
    if (idxEqual == key.npos)
        return 2;
    value = key.mid(idxEqual + 1);
    if (value.empty())
        return 3;
    key = key.mid(0, idxEqual);
    Q_ASSERT(!key.empty());

    return 0;
}

int AcquireBMICPC1710::processRecord(Util::ByteView line, double frameTime)
{
    Q_ASSERT(!config.isEmpty());

    int pairCount = 0;
    std::unordered_map<std::string, Variant::Root> updatedValues;
    bool ok = false;
    for (; pairCount < 1000; ++pairCount) {
        Util::ByteView key;
        Util::ByteView value;
        int result = extractNextKey(line, key, value);
        if (result < 0)
            break;
        if (result != 0)
            return 1000 * (pairCount + 1) + result;

        if (key.string_equal_insensitive("concn")) {
            if (updatedValues.count("N"))
                return 1000 * (pairCount + 1) + 100;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 101;
            remap("N", val);
            Util::insert_or_assign(updatedValues, "N", std::move(val));
        } else if (key.string_equal_insensitive("count")) {
            if (updatedValues.count("C"))
                return 1000 * (pairCount + 1) + 102;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 103;
            remap("C", val);
            Util::insert_or_assign(updatedValues, "C", std::move(val));
        } else if (key.string_equal_insensitive("optct")) {
            if (updatedValues.count("T4"))
                return 1000 * (pairCount + 1) + 104;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 105;
            remap("T4", val);
            Util::insert_or_assign(updatedValues, "T4", std::move(val));
        } else if (key.string_equal_insensitive("optcp")) {
            if (updatedValues.count("PCT4"))
                return 1000 * (pairCount + 1) + 104;
            double d = value.parse_real(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 105;
            if (!FP::defined(d) || d < 0.0 || d > 200.0)
                return 1000 * (pairCount + 1) + 106;
            Variant::Root val((d / 200.0) * 100.0);
            remap("PCT4", val);
            Util::insert_or_assign(updatedValues, "PCT4", std::move(val));
        } else if (key.string_equal_insensitive("condt")) {
            if (updatedValues.count("T3"))
                return 1000 * (pairCount + 1) + 107;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 108;
            remap("T3", val);
            Util::insert_or_assign(updatedValues, "T3", std::move(val));
        } else if (key.string_equal_insensitive("condp")) {
            if (updatedValues.count("PCT3"))
                return 1000 * (pairCount + 1) + 109;
            double d = value.parse_real(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 110;
            if (!FP::defined(d) || d < 0.0 || d > 250.0)
                return 1000 * (pairCount + 1) + 111;
            Variant::Root val((d / 250.0) * 100.0);
            remap("PCT3", val);
            Util::insert_or_assign(updatedValues, "PCT3", std::move(val));
        } else if (key.string_equal_insensitive("sattt")) {
            if (updatedValues.count("T2"))
                return 1000 * (pairCount + 1) + 112;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 113;
            remap("T2", val);
            Util::insert_or_assign(updatedValues, "T2", std::move(val));
        } else if (key.string_equal_insensitive("sattp")) {
            if (updatedValues.count("PCT2"))
                return 1000 * (pairCount + 1) + 114;
            double d = value.parse_real(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 115;
            if (!FP::defined(d) || d < 0.0 || d > 200.0)
                return 1000 * (pairCount + 1) + 116;
            Variant::Root val((d / 200.0) * 100.0);
            remap("PCT2", val);
            Util::insert_or_assign(updatedValues, "PCT2", std::move(val));
        } else if (key.string_equal_insensitive("satbt")) {
            if (updatedValues.count("T1"))
                return 1000 * (pairCount + 1) + 116;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 117;
            remap("T1", val);
            Util::insert_or_assign(updatedValues, "T1", std::move(val));
        } else if (key.string_equal_insensitive("satbp")) {
            if (updatedValues.count("PCT1"))
                return 1000 * (pairCount + 1) + 118;
            double d = value.parse_real(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 119;
            if (!FP::defined(d) || d < 0.0 || d > 200.0)
                return 1000 * (pairCount + 1) + 120;
            Variant::Root val((d / 200.0) * 100.0);
            remap("PCT1", val);
            Util::insert_or_assign(updatedValues, "PCT1", std::move(val));
        } else if (key.string_equal_insensitive("satfl")) {
            if (updatedValues.count("Q2"))
                return 1000 * (pairCount + 1) + 121;
            double d = value.parse_real(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 122;
            if (FP::defined(d))
                d /= 1000.0;
            Variant::Root val(d);
            remap("Q2", val);
            Util::insert_or_assign(updatedValues, "Q2", std::move(val));
        } else if (key.string_equal_insensitive("satfp")) {
            if (updatedValues.count("PCT5"))
                return 1000 * (pairCount + 1) + 123;
            double d = value.parse_real(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 124;
            if (!FP::defined(d) || d < 0.0 || d > 200.0)
                return 1000 * (pairCount + 1) + 125;
            Variant::Root val((d / 200.0) * 100.0);
            remap("PCT5", val);
            Util::insert_or_assign(updatedValues, "PCT5", std::move(val));
        } else if (key.string_equal_insensitive("smpfl")) {
            if (updatedValues.count("Q1"))
                return 1000 * (pairCount + 1) + 126;
            double d = value.parse_real(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 127;
            if (FP::defined(d))
                d /= 1000.0;
            Variant::Root val(d);
            remap("Q1", val);
            Util::insert_or_assign(updatedValues, "Q1", std::move(val));
        } else if (key.string_equal_insensitive("inltt")) {
            if (updatedValues.count("Tu"))
                return 1000 * (pairCount + 1) + 128;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 129;
            remap("Tu", val);
            Util::insert_or_assign(updatedValues, "Tu", std::move(val));
        } else if (key.string_equal_insensitive("errnm")) {
            if (updatedValues.count("F1"))
                return 1000 * (pairCount + 1) + 130;
            Variant::Root val((qint64) value.parse_u16(&ok, 16));
            if (!ok) return 1000 * (pairCount + 1) + 131;
            remap("FRAW", val);

            if (INTEGER::defined(val.read().toInt64())) {
                Variant::Flags oldFlags = reportedFlags;
                reportedFlags.clear();
                qint64 flagsBits(val.read().toInt64());
                for (int i = 0;
                        i <
                                (int) (sizeof(instrumentFlagTranslation) /
                                        sizeof(instrumentFlagTranslation[0]));
                        i++) {
                    if (flagsBits & (Q_INT64_C(1) << i)) {
                        if (instrumentFlagTranslation[i] != NULL) {
                            reportedFlags.insert(instrumentFlagTranslation[i]);
                        } else {
                            qCDebug(log) << "Unrecognized bit" << hex << ((1 << i))
                                         << "set in instrument flags";
                        }
                    }
                }

                if (reportedFlags != oldFlags)
                    forceRealtimeStateEmit = true;
            }

            Util::insert_or_assign(updatedValues, "F1", std::move(val));
        } else if (key.string_equal_insensitive("fillc")) {
        } else if (key.string_equal_insensitive("rconc")) {
        } else {
            return 1000 * (pairCount + 1) + 199;
        }
        if (line.empty())
            break;
    }
    if (pairCount >= 1000)
        return 200;
    if (pairCount < 3)
        return 201;

    if (loggingEgress == NULL) {
        haveEmittedLogMeta = false;
        loggingMux.clear();
        memset(streamAge, 0, sizeof(streamAge));
        for (int i = 0; i < LogStream_TOTAL; i++) {
            streamTime[i] = FP::undefined();
        }
    } else {
        if (!haveEmittedLogMeta) {
            haveEmittedLogMeta = true;
            loggingMux.incoming(LogStream_Metadata, buildLogMeta(frameTime), loggingEgress);
        }
        loggingMux.advance(LogStream_Metadata, frameTime, loggingEgress);
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }


    if (updatedValues.count("N") && !config.first().calculateConcentration) {
        logValue(frameTime, "N", std::move(updatedValues["N"]), LogStream_Concentration);
    }
    if (updatedValues.count("Q1")) {
        lastSampleFlow = updatedValues["Q1"].read().toDouble();
        logValue(frameTime, "Q1", std::move(updatedValues["Q1"]), LogStream_SampleFlow);
    }
    if (updatedValues.count("Q2")) {
        logValue(frameTime, "Q2", std::move(updatedValues["Q2"]), LogStream_SaturatorFlow);
    }
    if (updatedValues.count("C")) {
        if (config.first().calculateConcentration) {
            Variant::Root
                    N(calculateConcentration(updatedValues["C"].read().toReal(), lastSampleFlow));
            remap("N", N);
            logValue(frameTime, "N", std::move(N), LogStream_Concentration);
        }
        realtimeValue(frameTime, "C", std::move(updatedValues["C"]));
    }
    if (updatedValues.count("T4")) {
        logValue(frameTime, "T4", std::move(updatedValues["T4"]), LogStream_OpticsTemperature);
    }
    if (updatedValues.count("T3")) {
        logValue(frameTime, "T3", std::move(updatedValues["T3"]), LogStream_CondenserTemperature);
    }
    if (updatedValues.count("T2")) {
        logValue(frameTime, "T2", std::move(updatedValues["T2"]),
                 LogStream_SaturatorTopTemperature);
    }
    if (updatedValues.count("T1")) {
        logValue(frameTime, "T1", std::move(updatedValues["T1"]),
                 LogStream_SaturatorBottomTemperature);
    }
    if (updatedValues.count("Tu")) {
        logValue(frameTime, "Tu", std::move(updatedValues["Tu"]), LogStream_InletTemperature);
    }

    if (updatedValues.count("PCT1")) {
        realtimeValue(frameTime, "PCT1", std::move(updatedValues["PCT1"]));
    }
    if (updatedValues.count("PCT2")) {
        realtimeValue(frameTime, "PCT2", std::move(updatedValues["PCT2"]));
    }
    if (updatedValues.count("PCT3")) {
        realtimeValue(frameTime, "PCT3", std::move(updatedValues["PCT3"]));
    }
    if (updatedValues.count("PCT4")) {
        realtimeValue(frameTime, "PCT4", std::move(updatedValues["PCT4"]));
    }
    if (updatedValues.count("PCT5")) {
        realtimeValue(frameTime, "PCT5", std::move(updatedValues["PCT5"]));
    }

    Variant::Flags flags = reportedFlags;
    flags.insert("CoincidenceCorrected");
    realtimeValue(frameTime, "F1", Variant::Root(flags));

    for (int i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
        if (streamAge[i] < 2)
            continue;
        if (loggingEgress == NULL)
            continue;

        if (FP::defined(streamTime[LogStream_State])) {
            double startTime = streamTime[LogStream_State];
            double endTime = frameTime;

            loggingMux.incoming(LogStream_State,
                                SequenceValue({{}, "raw", "F1"}, Variant::Root(flags), startTime,
                                              endTime), loggingEgress);
        } else {
            loggingMux.advance(LogStream_State, frameTime, loggingEgress);
        }
        streamTime[LogStream_State] = frameTime;

        for (i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
            if (streamAge[i] == 0) {
                loggingMux.advance(i, frameTime, loggingEgress);
                streamTime[i] = FP::undefined();
            }
            streamAge[i] = 0;
        }
        break;
    }

    if (realtimeEgress != NULL && forceRealtimeStateEmit) {
        forceRealtimeStateEmit = false;

        if (reportedFlags.empty()) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                  FP::undefined()));
        } else {
            auto sorted = Util::to_qstringlist(reportedFlags);
            std::sort(sorted.begin(), sorted.end());
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(sorted.join(",")), frameTime,
                                  FP::undefined()));
        }
    }

    return 0;
}

void AcquireBMICPC1710::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    {
        Q_ASSERT(!config.isEmpty());
        bool oldCalculate = config.first().calculateConcentration;
        if (!Range::intersectShift(config, frameTime)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.isEmpty());
        if (oldCalculate != config.first().calculateConcentration) {
            streamAge[LogStream_Concentration] = 0;
            streamTime[LogStream_Concentration] = FP::undefined();
        }
    }

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

            responseState = RESP_PASSIVE_RUN;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            timeoutAt(frameTime + config.first().pollInterval + 2.0);

            forceRealtimeStateEmit = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveAutoprobeWait");
            event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }
    case RESP_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);
            responseState = RESP_PASSIVE_RUN;
            generalStatusUpdated();

            timeoutAt(frameTime + config.first().pollInterval + 2.0);

            forceRealtimeStateEmit = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_FIRSTVALID: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            timeoutAt(frameTime + config.first().pollInterval);
            responseState = RESP_INTERACTIVE_RUN_WAIT;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            forceRealtimeStateEmit = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_RUN_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (responseState != RESP_PASSIVE_RUN) {
                responseState = RESP_INTERACTIVE_RUN_WAIT;
                timeoutAt(frameTime + config.first().pollInterval);
            } else {
                timeoutAt(frameTime + config.first().pollInterval + 2.0);
            }
        } else if (code > 0) {
            qCDebug(log) << "Line at " << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();

            if (responseState == RESP_PASSIVE_RUN) {
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
                info.hash("ResponseState").setString("PassiveRun");
            } else {
                if (controlStream != NULL) {
                    controlStream->writeControl("autorpt=0\r");
                }
                responseState = RESP_INTERACTIVE_START_STOPREPORTS;
                discardData(frameTime + 0.5);
                info.hash("ResponseState").setString("Run");
            }
            generalStatusUpdated();

            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case RESP_INTERACTIVE_START_READVER: {
        QRegExp re("\\s*BMI\\s*MCPC\\s*v([\\d\\.]+)\\s*");

        Variant::Root oldValue(instrumentMeta["FirmwareVersion"]);
        QString str(frame.toQString(false));
        if (re.exactMatch(str) && re.cap(1).length() > 0) {
            instrumentMeta["FirmwareVersion"].setString(re.cap(1));
        } else {
            instrumentMeta["FirmwareVersion"].setString(str.trimmed());
        }

        timeoutAt(frameTime + 2.0);
        discardData(frameTime + 0.75);
        responseState = RESP_INTERACTIVE_START_DISCARDFIRST;

        if (controlStream != NULL) {
            controlStream->writeControl("status\r");
        }

        if (oldValue.read() != instrumentMeta["FirmwareVersion"]) {
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveDiscardFirst"), frameTime, FP::undefined()));
        }

        break;
    }

    default:
        break;
    }
}

void AcquireBMICPC1710::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));

    switch (responseState) {
    case RESP_INTERACTIVE_RUN:
        qCDebug(log) << "Timeout in interactive mode at" << Logging::time(frameTime);
        timeoutAt(frameTime + 30.0);

        if (controlStream != NULL) {
            controlStream->writeControl("autorpt=0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(frameTime + 0.5);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("InteractiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_RUN_WAIT:
        timeoutAt(frameTime + 2.0);
        responseState = RESP_INTERACTIVE_RUN;

        if (controlStream != NULL) {
            controlStream->writeControl("status\r");
        }
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_ENABLEREPORTLABEL:
    case RESP_INTERACTIVE_START_READVER:
    case RESP_INTERACTIVE_START_DISCARDFIRST:
    case RESP_INTERACTIVE_START_FIRSTVALID:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        timeoutAt(frameTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("autorpt=0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(frameTime + 0.5);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        invalidateLogValues(frameTime);
        break;

    default:
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireBMICPC1710::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    switch (responseState) {
    case RESP_INTERACTIVE_START_STOPREPORTS:
        if (controlStream != NULL) {
            controlStream->writeControl("rptlabel=1\r");
        }
        responseState = RESP_INTERACTIVE_START_ENABLEREPORTLABEL;
        discardData(frameTime + 0.5);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveEnableReportLabel"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_ENABLEREPORTLABEL:
        if (controlStream != NULL) {
            controlStream->writeControl("ver\r");
        }
        responseState = RESP_INTERACTIVE_START_READVER;
        timeoutAt(frameTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadFirmwareVersion"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_DISCARDFIRST:
        if (controlStream != NULL) {
            controlStream->writeControl("status\r");
        }
        responseState = RESP_INTERACTIVE_START_FIRSTVALID;
        timeoutAt(frameTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveStartReadFirstRecord"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        timeoutAt(frameTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("autorpt=0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(frameTime + 0.5);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    default:
        break;
    }
}


void AcquireBMICPC1710::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frameTime);
    Q_UNUSED(frame);
    /* Maybe implement something to discard unknown command responses? */
}

Variant::Root AcquireBMICPC1710::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireBMICPC1710::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_RUN_WAIT:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

AcquisitionInterface::AutoprobeStatus AcquireBMICPC1710::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireBMICPC1710::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_RUN_WAIT:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_ENABLEREPORTLABEL:
    case RESP_INTERACTIVE_START_READVER:
    case RESP_INTERACTIVE_START_DISCARDFIRST:
    case RESP_INTERACTIVE_START_FIRSTVALID:
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("autorpt=0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(time + 0.5);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireBMICPC1710::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    discardData(time + 0.5, 1);
    timeoutAt(time + config.first().pollInterval * 3.0 + 2.0);
    generalStatusUpdated();
}

void AcquireBMICPC1710::autoprobePromote(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Promoted from interactive wait to interactive acquisition at"
                     << Logging::time(time);

        timeoutAt(time);
        break;
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_ENABLEREPORTLABEL:
    case RESP_INTERACTIVE_START_READVER:
    case RESP_INTERACTIVE_START_DISCARDFIRST:
    case RESP_INTERACTIVE_START_FIRSTVALID:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + config.first().pollInterval + 2.0);
        break;

    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("autorpt=0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(time + 0.5);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireBMICPC1710::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    Q_ASSERT(!config.isEmpty());

    timeoutAt(time + config.first().pollInterval + 2.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_RUN:
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from interactive to passive acquisition at"
                     << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireBMICPC1710::getDefaults()
{
    AutomaticDefaults result;
    result.name = "N$1$2";
    result.setSerialN81(38400);
    return result;
}


ComponentOptions AcquireBMICPC1710Component::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options);
    return options;
}

ComponentOptions AcquireBMICPC1710Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireBMICPC1710Component::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireBMICPC1710Component::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireBMICPC1710Component::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options);
}

QList<ComponentExample> AcquireBMICPC1710Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options,
                                     tr("Convert data using the instrument reported concentration")));

    options = getBaseOptions();
    (qobject_cast<ComponentOptionBoolean *>(options.get("recalculate")))->set(true);
    examples.append(
            ComponentExample(options, tr("Recalculate the concentration from the count rate")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireBMICPC1710Component::createAcquisitionPassive(const ComponentOptions &options,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireBMICPC1710(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireBMICPC1710Component::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireBMICPC1710(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireBMICPC1710Component::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{
    std::unique_ptr<AcquireBMICPC1710> i(new AcquireBMICPC1710(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireBMICPC1710Component::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{
    std::unique_ptr<AcquireBMICPC1710> i(new AcquireBMICPC1710(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
