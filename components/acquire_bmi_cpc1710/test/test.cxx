/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;

    double tau;
    double counts;
    double sampleFlow;
    double saturatorFlow;

    double Toptics;
    double Poptics;
    double Tcondenser;
    double Pcondenser;
    double Tsattop;
    double Psattop;
    double Tsatbot;
    double Psatbot;
    double Psatflow;
    double Tinlet;
    int fillCount;
    quint16 errorBits;

    double rawConcentration() const
    {
        return counts / (sampleFlow / 60.0);
    }

    double correctedConcentration() const
    {
        return (counts * exp(counts * tau)) / (sampleFlow / 60.0);
    }

    double sampleFlowLPM() const
    {
        return sampleFlow / 1000.0;
    }

    double saturatorFlowLPM() const
    {
        return saturatorFlow / 1000.0;
    }

    double opticsPower() const
    {
        return (Poptics / 200.0) * 100.0;
    }

    double condenserPower() const
    {
        return (Pcondenser / 250.0) * 100.0;
    }

    double saturatorTopPower() const
    {
        return (Psattop / 200.0) * 100.0;
    }

    double saturatorBottomPower() const
    {
        return (Psatbot / 200.0) * 100.0;
    }

    double saturatorFlowPower() const
    {
        return (Psatflow / 200.0) * 100.0;
    }

    void writeStatus()
    {
        outgoing.append("CONCN=");
        outgoing.append(QByteArray::number(correctedConcentration(), 'f', 1));
        outgoing.append(" RCONC=");
        outgoing.append(QByteArray::number(rawConcentration(), 'f', 0));
        outgoing.append(" COUNT=");
        outgoing.append(QByteArray::number(counts, 'f', 0));
        outgoing.append(" OPTCT=");
        outgoing.append(QByteArray::number(Toptics, 'f', 1));
        outgoing.append(" OPTCP=");
        outgoing.append(QByteArray::number(Poptics, 'f', 0));
        outgoing.append("\r\nCONDT=");
        outgoing.append(QByteArray::number(Tcondenser, 'f', 0));
        outgoing.append(" CONDP=");
        outgoing.append(QByteArray::number(Pcondenser, 'f', 0));
        outgoing.append(" SATTT=");
        outgoing.append(QByteArray::number(Tsattop, 'f', 1));
        outgoing.append(" SATTP=");
        outgoing.append(QByteArray::number(Psattop, 'f', 0));
        outgoing.append(" SATBT=");
        outgoing.append(QByteArray::number(Tsatbot, 'f', 1));
        outgoing.append(" SATBP=");
        outgoing.append(QByteArray::number(Psatbot, 'f', 0));
        outgoing.append("\r\nSATFL=");
        outgoing.append(QByteArray::number(saturatorFlow, 'f', 0));
        outgoing.append(" SATFP=");
        outgoing.append(QByteArray::number(Psatflow, 'f', 0));
        outgoing.append(" SMPFL=");
        outgoing.append(QByteArray::number(sampleFlow, 'f', 0));
        outgoing.append(" INLTT=");
        outgoing.append(QByteArray::number(Tinlet, 'f', 1));
        outgoing.append(" FILLC=");
        outgoing.append(QByteArray::number(fillCount));
        outgoing.append(" ERRNM=");
        outgoing.append(QByteArray::number(errorBits));
        outgoing.append("\r\n");
    }

    ModelInstrument() : incoming(), outgoing(), unpolledRemaining(FP::undefined())
    {
        counts = 6000;
        sampleFlow = 300;
        saturatorFlow = 310;
        tau = 0.5E-6;

        Toptics = 23.1;
        Poptics = 20;
        Tcondenser = 15.0;
        Pcondenser = 10;
        Tsattop = 21.0;
        Psattop = 30;
        Tsatbot = 20.5;
        Psatbot = 29;
        Psatflow = 150;
        Tinlet = 10.0;
        fillCount = 0;
        errorBits = 0;
    }

    void advance(double seconds)
    {

        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR));

            if (line == "ver") {
                outgoing.append("BMI MCPC v3.4\r\n");
            } else if (line == "status") {
                writeStatus();
            }

            if (idxCR >= incoming.size() - 1) {
                incoming.clear();
                break;
            }
            incoming = incoming.mid(idxCR + 1);
        }

        if (FP::defined(unpolledRemaining)) {
            unpolledRemaining -= seconds;
            while (unpolledRemaining <= 0.0) {
                unpolledRemaining += 0.5;

                writeStatus();
            }
        }
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("N", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Tu", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T4", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q2", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("C", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCT1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCT2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCT3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCT4", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCT5", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("N"))
            return false;
        if (!stream.checkContiguous("Tu"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("T3"))
            return false;
        if (!stream.checkContiguous("T4"))
            return false;
        if (!stream.checkContiguous("Q1"))
            return false;
        if (!stream.checkContiguous("Q2"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream, const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("N", Variant::Root(model.correctedConcentration()), time,
                                        QString(),
                                        1E-4))
            return false;
        if (!stream.hasAnyMatchingValue("Tu", Variant::Root(model.Tinlet), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.Tsatbot), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.Tsattop), time))
            return false;
        if (!stream.hasAnyMatchingValue("T3", Variant::Root(model.Tcondenser), time))
            return false;
        if (!stream.hasAnyMatchingValue("T4", Variant::Root(model.Toptics), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q1", Variant::Root(model.sampleFlowLPM()), time, QString(),
                                        1E-4))
            return false;
        if (!stream.hasAnyMatchingValue("Q2", Variant::Root(model.saturatorFlowLPM()), time,
                                        QString(),
                                        1E-4))
            return false;
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("C", Variant::Root(model.counts), time))
            return false;
        if (!stream.hasAnyMatchingValue("PCT1", Variant::Root(model.saturatorBottomPower()), time))
            return false;
        if (!stream.hasAnyMatchingValue("PCT2", Variant::Root(model.saturatorTopPower()), time))
            return false;
        if (!stream.hasAnyMatchingValue("PCT3", Variant::Root(model.condenserPower()), time))
            return false;
        if (!stream.hasAnyMatchingValue("PCT4", Variant::Root(model.opticsPower()), time))
            return false;
        if (!stream.hasAnyMatchingValue("PCT5", Variant::Root(model.saturatorFlowPower()), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_bmi_cpc1710"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_bmi_cpc1710"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.1;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(0.2);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 20; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["CalculateConcentration"].setBool(true);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.1;

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        control.advance(0.1);

        for (int i = 0; i < 20; i++) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(logging.hasAnyMatchingValue("F1"));

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        double checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        control.advance(0.125);
        checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
