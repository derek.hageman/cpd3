/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREBMICPC1710_H
#define ACQUIREBMICPC1710_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class AcquireBMICPC1710 : public CPD3::Acquisition::FramedInstrument {
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum {
        /* Passive autoprobe initialization, ignoring the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,
        /* Acquiring data in interactive mode, reading the response to a
         * query. */
                RESP_INTERACTIVE_RUN,
        /* Acquisting data in interactive mode, sleeping until the next
         * time to query. */
                RESP_INTERACTIVE_RUN_WAIT,

        /* Waiting for the completion of the "autorpt=0" command during start
         * communications. */
                RESP_INTERACTIVE_START_STOPREPORTS,
        /* Waiting for the completion of the "rptlabel=1" command during start
         * communications. */
                RESP_INTERACTIVE_START_ENABLEREPORTLABEL,
        /* Waiting for the result of the "ver" command */
                RESP_INTERACTIVE_START_READVER,

        /* Discarding the first report, after starting communications */
                RESP_INTERACTIVE_START_DISCARDFIRST,
        /* Waiting for the first valid record */
                RESP_INTERACTIVE_START_FIRSTVALID,

        /* Waiting before attempting a communications restart */
                RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
                RESP_INTERACTIVE_INITIALIZE,
    } responseState;

    enum LogStreamID {
        LogStream_Metadata = 0,
        LogStream_State,

        LogStream_Concentration,
        LogStream_OpticsTemperature,
        LogStream_CondenserTemperature,
        LogStream_SaturatorTopTemperature,
        LogStream_SaturatorBottomTemperature,
        LogStream_SaturatorFlow,
        LogStream_SampleFlow,
        LogStream_InletTemperature,

        LogStream_TOTAL,
        LogStream_RecordBaseStart = LogStream_Concentration,
    };
    CPD3::Data::StaticMultiplexer loggingMux;
    quint32 streamAge[LogStream_TOTAL];
    double streamTime[LogStream_TOTAL];

    class Configuration {
        double start;
        double end;

    public:
        bool calculateConcentration;
        double pollInterval;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    CPD3::Data::Variant::Root instrumentMeta;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool forceRealtimeStateEmit;

    double lastSampleFlow;
    CPD3::Data::Variant::Flags reportedFlags;

    void setDefaultInvalid();

    void logValue(double frameTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value,
                  int streamID);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    void invalidateLogValues(double frameTime);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    int processRecord(CPD3::Util::ByteView line, double frameTime);

public:
    AcquireBMICPC1710(const CPD3::Data::ValueSegment::Transfer &config,
                      const std::string &loggingContext);

    AcquireBMICPC1710(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireBMICPC1710();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    virtual CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    virtual void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingInstrumentTimeout(double time);

    virtual void discardDataCompleted(double time);
};

class AcquireBMICPC1710Component
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_bmi_cpc1710"
                              FILE
                              "acquire_bmi_cpc1710.json")

    CPD3::ComponentOptions getBaseOptions();

public:

    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
