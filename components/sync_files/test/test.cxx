/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>
#include <QSignalSpy>
#include <QTcpServer>
#include <QSslCertificate>
#include <QDir>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/actioncomponent.hxx"
#include "core/waitutils.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "database/runlock.hxx"
#include "algorithms/cryptography.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Algorithms;

static const char *cert1Data = "-----BEGIN CERTIFICATE-----\n"
        "MIICbTCCAdYCCQDZ6mj3BI+kCTANBgkqhkiG9w0BAQUFADB7MQswCQYDVQQGEwJV\n"
        "UzERMA8GA1UECAwIQ29sb3JhZG8xEDAOBgNVBAcMB0JvdWxkZXIxFjAUBgNVBAoM\n"
        "DU5PQUEvR01EL0VTUkwxFzAVBgNVBAsMDkFlcm9zb2xzIEdyb3VwMRYwFAYDVQQD\n"
        "DA1EZXJlayBIYWdlbWFuMB4XDTEyMDcyNzE1MzkwNloXDTM5MTIxMjE1MzkwNlow\n"
        "ezELMAkGA1UEBhMCVVMxETAPBgNVBAgMCENvbG9yYWRvMRAwDgYDVQQHDAdCb3Vs\n"
        "ZGVyMRYwFAYDVQQKDA1OT0FBL0dNRC9FU1JMMRcwFQYDVQQLDA5BZXJvc29scyBH\n"
        "cm91cDEWMBQGA1UEAwwNRGVyZWsgSGFnZW1hbjCBnzANBgkqhkiG9w0BAQEFAAOB\n"
        "jQAwgYkCgYEAn3nIKzjLLEY8KG6+hUvrkjDkl9VU8HFKnp8wA2RsMta/UvKG4ZIY\n"
        "bNVsKSiAGm1G0i6Mtftw8quyihoxuy/hElH/XJChU5lersdrhalocHXoipNx8BeW\n"
        "T8sXuJOrh+IoDKqVI4I6Sz42qAlqBU7bNm40TE8sBPAOefyTGavC3e0CAwEAATAN\n"
        "BgkqhkiG9w0BAQUFAAOBgQBU2hljZZpJJ+zK6eePUpdELQGPGHJEbzfReeSBhRau\n"
        "iDj4jEa7IE3yrMv8NqC3Q+6lEVuJg0NInvDYsNYLPjMSPS+jKldrmrJvg+TFrp1w\n"
        "Qr3JoznNxwcwuDyrKjOVux6jo2aype3Zayo5VUizJX9Zo+xLe2MBVR1D0MXmoq8L\n"
        "zA==\n"
        "-----END CERTIFICATE-----";
static const char *key1Data = "-----BEGIN RSA PRIVATE KEY-----\n"
        "MIICXAIBAAKBgQCfecgrOMssRjwobr6FS+uSMOSX1VTwcUqenzADZGwy1r9S8obh\n"
        "khhs1WwpKIAabUbSLoy1+3Dyq7KKGjG7L+ESUf9ckKFTmV6ux2uFqWhwdeiKk3Hw\n"
        "F5ZPyxe4k6uH4igMqpUjgjpLPjaoCWoFTts2bjRMTywE8A55/JMZq8Ld7QIDAQAB\n"
        "AoGBAIQOd0f7PpsKCfS9R7zfklG7dP+Z4z07wzu4vCyC8uniVAoe1LxjmyA8VtV6\n"
        "OSIpDTUs4M4tSWlZ7n1XlYjY6/lNt3xsy5BBQMlWVI8BV/yXnWRCxkQGBvXYTUKe\n"
        "7LRsUcqyGpAPeuStO/V8GEM5H272yv5S4413D09C9cAhROMtAkEA07j2lnnSl/WR\n"
        "0NFEOT9+nbh6Z+yRTzybC86TBhLCN3bY4VvqSWi1PdcU3oiz81dcaAPTksdx18/Y\n"
        "cSfz295ewwJBAMDTq22Dt2HMPQymxNUn9/IdIPU34/fEqSHS3e9hAgrp2gDVeNHR\n"
        "ZEzMkW00rLgdukkW51PV9hVIhYXdxYOjZY8CQFsf/cnwLurGf/b/SrzVDjr1/oEi\n"
        "Obx/2j+vrmnrwvm6Rkhglir4TSGLo+jPr5vpmtUN6I8BFoeLZp31UyjrwZ8CQC3W\n"
        "Y2ruI7qozV5jimjNToCMchg4yAVPB5GVydIsskqb2onWNRlTeE9VVcCrA9/kmTLk\n"
        "serY8t2OVsdCt8AaKHsCQFMEhJCpXaeLnrzToSv0uqRYjcT+r3ZjSghhSepN0ZA+\n"
        "PPj1CMmq9JrQaQAv3rT3hCbDF38TPhKX64OYzbuvdxA=\n"
        "-----END RSA PRIVATE KEY-----\n";
static const char *cert2Data = "-----BEGIN CERTIFICATE-----\n"
        "MIICbTCCAdYCCQCr9EVbER/M4TANBgkqhkiG9w0BAQUFADB7MQswCQYDVQQGEwJV\n"
        "UzERMA8GA1UECAwIQ29sb3JhZG8xEDAOBgNVBAcMB0JvdWxkZXIxFjAUBgNVBAoM\n"
        "DU5PQUEvRVNSTC9HTUQxFzAVBgNVBAsMDkFlcm9zb2xzIEdyb3VwMRYwFAYDVQQD\n"
        "DA1EZXJlayBIYWdlbWFuMB4XDTEyMDcyNzE1NDIxNloXDTM5MTIxMjE1NDIxNlow\n"
        "ezELMAkGA1UEBhMCVVMxETAPBgNVBAgMCENvbG9yYWRvMRAwDgYDVQQHDAdCb3Vs\n"
        "ZGVyMRYwFAYDVQQKDA1OT0FBL0VTUkwvR01EMRcwFQYDVQQLDA5BZXJvc29scyBH\n"
        "cm91cDEWMBQGA1UEAwwNRGVyZWsgSGFnZW1hbjCBnzANBgkqhkiG9w0BAQEFAAOB\n"
        "jQAwgYkCgYEAqa4sVcN1M4mWVqTqZuG2VikkOTpYrHYgnD4jFKDfi5FTFE9OQ5oW\n"
        "xhrBdoEtiWLEPmPSQCtJWCbD0d/YQ1ITxOF0OSaAa4c6PkdeiDmfull8EGmuaTFo\n"
        "ikcFS0r2SVtvIy3eFEAg9GBF4iCMp2AvUo2eqNCLt4CdRi6LQs2xkmUCAwEAATAN\n"
        "BgkqhkiG9w0BAQUFAAOBgQCEvlrqC1zChgkFMq8Oi7YRQOdx7ikzDodey/uu0r/W\n"
        "DxCnpA+F2f3okHiR5+LfYqQ5fvCYnmZd7w69tvdpui8swa15aXWaxLVD9/woyM2y\n"
        "Ofce/9pw6UgoYLkh9KsW5+2EP2Sn44lWcU5Jf8U/p8cDdjNIDzDG4vw7UFWiRakm\n"
        "Tw==\n"
        "-----END CERTIFICATE-----\n";
static const char *key2Data = "-----BEGIN RSA PRIVATE KEY-----\n"
        "MIICWwIBAAKBgQCprixVw3UziZZWpOpm4bZWKSQ5OlisdiCcPiMUoN+LkVMUT05D\n"
        "mhbGGsF2gS2JYsQ+Y9JAK0lYJsPR39hDUhPE4XQ5JoBrhzo+R16IOZ+6WXwQaa5p\n"
        "MWiKRwVLSvZJW28jLd4UQCD0YEXiIIynYC9SjZ6o0Iu3gJ1GLotCzbGSZQIDAQAB\n"
        "AoGAUzE2Q4ZlfDNFJo4M7wxTXcMmI3jb6RKxwmkkwgRuFfvWg+quMK7n45FSsUt8\n"
        "jBOErCI8/4E5oKLA97GMUtV3IxAXT/hxQRe6qZS7PEUW4k6Op7evX1hSCfI/IAB/\n"
        "AS6OqUR1U4+AFArhPS3CA+vuY4I01mpWpvI3dfvq29oUW4ECQQDQ4ItViA/CjWiq\n"
        "peW2+cptQ8QqkNGscjsUTZyp3aNtunC1mP/HRKlKLnuhY6G+LQWbzDv7BSJ9UNO4\n"
        "Ro1y90jJAkEAz/Xdjqx/xZntCleuxvMACWdhpMBqfdgs7+maLDXbtc9G43ygbqIz\n"
        "Wj99k9lu0L3iHdRlYicwdfFFBbvd4jsmvQJATOF5J4AvHNLjpXvuc0y5n0IEIA6x\n"
        "viFFcZGnijZUAv1OouivrG6vSOiXBK4hSFhV6iRgJ2KacTmg1ADT627tUQJAK4hl\n"
        "W9OCX8QMGekm/iCqNk284/cfk75oEcTN8ElJ9/Iu/bn9/4rWwyKdUBDpIKtPJT1s\n"
        "B7L6cwYRk9Sy6wPE5QJAFmjRUr3GypvuC4haazaOMyIt/FEbC+kUs9kq6JZsiBuM\n"
        "LbZ02ko9IkhfOiUIewV6+Xb2j+1u/Itc737M6RZ+3Q==\n"
        "-----END RSA PRIVATE KEY-----";

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;
    QString tmpPath;
    QDir tmpDir;

    QString serverCert;
    QString serverKey;
    QString serverAuth;

    QString clientCert;
    QString clientKey;
    QString clientAuth;

    static void recursiveRemove(const QDir &base)
    {
        if (base.path().isEmpty() || base == QDir::current())
            return;
        QFileInfoList list(base.entryInfoList(QDir::Files | QDir::NoDotAndDotDot));
        for (QFileInfoList::const_iterator rm = list.constBegin(), endRm = list.constEnd();
                rm != endRm;
                ++rm) {
            if (!QFile::remove(rm->absoluteFilePath())) {
                qDebug() << "Failed to remove file" << rm->absoluteFilePath();
                continue;
            }
        }
        list = base.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);
        for (QFileInfoList::const_iterator rm = list.constBegin(), endRm = list.constEnd();
                rm != endRm;
                ++rm) {
            QDir subDir(base);
            if (!subDir.cd(rm->fileName())) {
                qDebug() << "Failed to change directory" << rm->absoluteFilePath();
                continue;
            }
            recursiveRemove(subDir);
            base.rmdir(rm->fileName());
        }
    }

    ActionComponent *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ActionComponent *>(ComponentLoader::create("sync_files"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());


        serverCert = cert1Data;
        serverKey = key1Data;
        serverAuth = QString::fromLatin1(
                Cryptography::sha512(QSslCertificate(QByteArray(cert1Data))).leftJustified(64, 0,
                                                                                           true)
                                                                            .toHex()
                                                                            .toLower());

        clientCert = cert2Data;
        clientKey = key2Data;
        clientAuth = QString::fromLatin1(
                Cryptography::sha512(QSslCertificate(QByteArray(cert2Data))).leftJustified(64, 0,
                                                                                           true)
                                                                            .toHex()
                                                                            .toLower());

        tmpDir = QDir(QDir::tempPath());
        tmpPath = "CPD3SyncFiles-";
        tmpPath.append(Random::string());
        tmpDir.mkdir(tmpPath);
        QVERIFY(tmpDir.cd(tmpPath));
    }

    void cleanupTestCase()
    {
        recursiveRemove(tmpDir);
        tmpDir.cdUp();
        tmpDir.rmdir(tmpPath);
    }

    void init()
    {
        recursiveRemove(tmpDir);
        databaseFile.resize(0);
    }


    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionStringSet *>(options.get("profile")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("update")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("upload")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("download")));
        QVERIFY(qobject_cast<ComponentOptionSingleTime *>(options.get("after")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("timeout")));
    }

    void basic()
    {
        tmpDir.mkdir("in");
        tmpDir.mkdir("out");

        Variant::Root config;

        QString fileWriteName
                ("${STATION|LOWER}/${REMOTE|SHORT|LOWER}/${STATION|UPPER}_${REMOTE|SHORT|UPPER}_${TIME}_${UID|UPPER}.c3y");
        QString fileReadName("${STATION|UPPER}_${AUTHORIZATION|SHORT}_${TIME}_${UID}.c3y");

        auto auth = config["Authorization"].hash(clientAuth);
        auth["Local/Synchronize/#0/Variable"] = "T_S11";
        auth["Local/Synchronize/#0/Archive"] = "raw";
        auth["Local/Synchronize/#0/Accept"] = true;
        auth["Remote/Synchronize/#0/Variable"] = "P_S11";
        auth["Remote/Synchronize/#0/Archive"] = "raw";
        auth["Remote/Synchronize/#0/Accept"] = true;
        auth["Encryption"] = clientCert;
        auth["Decryption/" + serverAuth + "/Certificate"] = serverCert;
        auth["Decryption/" + serverAuth + "/Key"] = serverKey;
        auth["Sign/Certificate"] = serverCert;
        auth["Sign/Key"] = serverKey;
        auth["Transfer/Type"] = "File";
        auth["Transfer/Local/CreateDirectory"] = true;
        auth["Transfer/Local/Target"] = tmpDir.absoluteFilePath("out") + "/" + fileWriteName;
        auth["Sources/Writable"] = true;
        auth["Sources/MinimumAge"] = 0;
        auth["Sources/Path"] = tmpDir.absoluteFilePath("in") + "/nil";
        auth["Sources/MatchPath/#0"] = "${AUTHORIZATION|SHORT}";
        auth["Sources/Match"] = fileReadName;

        auth = config["Authorization"].hash(serverAuth);
        auth["Remote/Synchronize/#0/Variable"] = "T_S11";
        auth["Remote/Synchronize/#0/Archive"] = "raw";
        auth["Remote/Synchronize/#0/Accept"] = true;
        auth["Local/Synchronize/#0/Variable"] = "P_S11";
        auth["Local/Synchronize/#0/Archive"] = "raw";
        auth["Local/Synchronize/#0/Accept"] = true;
        auth["Encryption"] = serverCert;
        auth["Decryption/" + clientAuth + "/Certificate"] = clientCert;
        auth["Decryption/" + clientAuth + "/Key"] = clientKey;
        auth["Sign/Certificate"] = clientCert;
        auth["Sign/Key"] = clientKey;
        auth["Transfer/Type"] = "File";
        auth["Transfer/Local/CreateDirectory"] = true;
        auth["Transfer/Local/Target"] = tmpDir.absoluteFilePath("in") + "/" + fileWriteName;
        auth["Sources/Writable"] = true;
        auth["Sources/MinimumAge"] = 0;
        auth["Sources/Path"] = tmpDir.absoluteFilePath("out") + "/nil";
        auth["Sources/MatchPath/#0"] = "${AUTHORIZATION|SHORT}";
        auth["Sources/Match"] = fileReadName;

        SequenceValue::Transfer networkedValues
                {SequenceValue({"nil", "raw", "T_S11"}, Variant::Root(1.0), 100, 200),
                 SequenceValue({"nil", "raw", "P_S11"}, Variant::Root(2.0), 100, 200),
                 SequenceValue({"nil", "raw", "T_S11"}, Variant::Root(3.0), 300, 400),
                 SequenceValue({"nil", "raw", "P_S11"}, Variant::Root(4.0), 300, 400)};

        Archive::Access(databaseFile).writeSynchronous(networkedValues);
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"_", "configuration", "synchronize"}, config)});

        {
            ComponentOptions options;
            options = component->getOptions();
            (qobject_cast<ComponentOptionStringSet *>(options.get("profile")))->add(clientAuth);
            CPD3Action *action = component->createAction(options, {"nil"});

            action->start();
            action->wait(30000);
            delete action;
        }

        {
            ComponentOptions options;
            options = component->getOptions();
            (qobject_cast<ComponentOptionStringSet *>(options.get("profile")))->add(serverAuth);
            CPD3Action *action = component->createAction(options, {"nil"});

            action->start();
            action->wait(30000);
            delete action;
        }

        {
            ComponentOptions options;
            options = component->getOptions();
            (qobject_cast<ComponentOptionStringSet *>(options.get("profile")))->add(clientAuth);
            CPD3Action *action = component->createAction(options, {"nil"});

            action->start();
            action->wait(30000);
            delete action;
        }


        {
            Database::RunLock rc;
            bool ok = false;
            double m = rc.acquire(QString("synchronize %1 %2").arg("nil", clientAuth), 30.0, &ok);
            QVERIFY(ok);
            QVERIFY(FP::defined(m));
            m = rc.acquire(QString("synchronizedownload %1 %2").arg("nil", clientAuth), 30.0, &ok);
            QVERIFY(ok);
            QVERIFY(FP::defined(m));
            rc.fail();
        }
        {
            Database::RunLock rc;
            bool ok = false;
            double m = rc.acquire(QString("synchronize %1 %2").arg("nil", serverAuth), 30.0, &ok);
            QVERIFY(ok);
            QVERIFY(FP::defined(m));
            m = rc.acquire(QString("synchronizedownload %1 %2").arg("nil", serverAuth), 30.0, &ok);
            QVERIFY(ok);
            QVERIFY(FP::defined(m));
            rc.fail();
        }

        {
            Archive::Access access(databaseFile);
            ErasureSink::Iterator data;
            access.readErasure(
                          Archive::Selection(FP::undefined(), FP::undefined(), {"nil"}, {"raw"}),
                          &data)->detach();
            while (data.hasNext()) {
                QVERIFY(data.isNextArchive());
                auto v = data.archiveNext();
                QVERIFY(v.isRemoteReferenced());
            }
        }

        {
            auto events = Archive::Access(databaseFile).readSynchronous(
                    Archive::Selection(FP::undefined(), FP::undefined(), {"nil"}, {"events"},
                                       {"synchronize"}));
            QCOMPARE((int) events.size(), 4);
        }

        QCoreApplication::processEvents();
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
