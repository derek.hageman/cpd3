/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QSslCertificate>
#include <QLoggingCategory>

#include "database/runlock.hxx"
#include "core/environment.hxx"
#include "core/qtcompat.hxx"
#include "core/memory.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/segment.hxx"
#include "algorithms/cryptography.hxx"

#include "sync_files.hxx"


Q_LOGGING_CATEGORY(log_component_sync_files, "cpd3.component.sync.files", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Sync;
using namespace CPD3::Transfer;

SyncFiles::SyncFiles()
        : profiles(),
          stations(),
          lockTimeout(30.0),
          runUpdate(true),
          haveAfter(false),
          afterTime(FP::undefined()),
          doUpload(true),
          doDownload(true),
          mutex(),
          terminated(false)
{ }

SyncFiles::SyncFiles(const ComponentOptions &options,
                     const std::vector<SequenceName::Component> &setStations)
        : profiles(),
          stations(setStations),
          lockTimeout(30.0),
          runUpdate(true),
          haveAfter(false),
          afterTime(FP::undefined()),
          doUpload(true),
          doDownload(true),
          mutex(),
          terminated(false)
{
    if (options.isSet("profile")) {
        QSet<QString> unique;
        QSet<QString> set(qobject_cast<ComponentOptionStringSet *>(options.get("profile"))->get());
        for (QSet<QString>::const_iterator add = set.constBegin(), endAdd = set.constEnd();
                add != endAdd;
                ++add) {
            if (add->isEmpty())
                continue;
            unique.insert(add->toLower());
        }
        profiles = unique;
    }
    if (options.isSet("update")) {
        runUpdate = qobject_cast<ComponentOptionBoolean *>(options.get("update"))->get();
    }
    if (options.isSet("upload")) {
        doUpload = qobject_cast<ComponentOptionBoolean *>(options.get("upload"))->get();
    }
    if (options.isSet("download")) {
        doDownload = qobject_cast<ComponentOptionBoolean *>(options.get("download"))->get();
    }
    if (options.isSet("timeout")) {
        lockTimeout = qobject_cast<ComponentOptionSingleDouble *>(options.get("timeout"))->get();
    }
    if (options.isSet("after")) {
        haveAfter = true;
        afterTime = qobject_cast<ComponentOptionSingleTime *>(options.get("after"))->get();
    }
}

SyncFiles::~SyncFiles()
{ }

namespace {
class DigestReplacer : public TextSubstitutionStack::Replacer {
    QByteArray digest;

    QString toLong() const
    {
        static const int length = 64;
        return digest.leftJustified(length, (char) 0, true).toHex().toLower();
    }

    QString toShort() const
    {
        static const char digits[33] = "0123456789abcdefghijklmnopqrstuv";
        static const int length = 16;

        QString result;
        for (const char *add = digest.constData(), *end = add + digest.size();
                add != end && result.length() < length;
                ++add) {
            result.append(digits[((quint8) (*add)) >> 3]);
        }
        return result.leftJustified(length, '0', true);
    }

public:
    DigestReplacer(const QByteArray &d) : digest(d)
    { }

    virtual ~DigestReplacer()
    { }

    virtual QString get(const QStringList &elements) const
    {
        QString type;
        int index = 0;
        if (index < elements.size())
            type = elements.at(index++).toLower();

        if (type == "short") {
            return TextSubstitutionStack::formatString(toShort(), elements.mid(index));
        }
        return TextSubstitutionStack::formatString(toLong(), elements.mid(index));
    }
};
}

SyncFiles::SubstitutionStack::SubstitutionStack()
{ }

SyncFiles::SubstitutionStack::SubstitutionStack(const SubstitutionStack &other)
        : TextSubstitutionStack(other)
{ }

SyncFiles::SubstitutionStack &SyncFiles::SubstitutionStack::operator=(const SubstitutionStack &other)
{
    TextSubstitutionStack::operator=(other);
    return *this;
}

void SyncFiles::SubstitutionStack::setDigest(const QString &key, const QByteArray &digest)
{ setReplacement(key, new DigestReplacer(digest)); }

bool SyncFiles::SubstitutionStack::setDigest(const QString &key, const Variant::Read &value)
{
    QSslCertificate certificate(Algorithms::Cryptography::getCertificate(value));
    if (certificate.isNull())
        return false;
    QByteArray digest(Algorithms::Cryptography::sha512(certificate));
    if (digest.isEmpty())
        return false;
    setDigest(key, digest);
    return true;
}

SyncFiles::Context::Context(SyncFiles *parent,
                            const SequenceName::Component &station,
                            double lastDownloadTime,
                            double currentExecution,
                            QByteArray &downloaderState,
                            const CPD3::Data::ValueSegment::Transfer &stationConfiguration)
        : parent(parent),
          station(station),
          lastDownloadTime(lastDownloadTime),
          currentExecution(currentExecution),
          downloaderState(&downloaderState),
          stationConfiguration(stationConfiguration)
{ }

SyncFiles::Context::Context(const Context &other) : parent(other.parent),
                                                    station(other.station),
                                                    lastDownloadTime(other.lastDownloadTime),
                                                    currentExecution(other.currentExecution),
                                                    downloaderState(other.downloaderState),
                                                    stationConfiguration(other.stationConfiguration)
{ }

SyncFiles::Context &SyncFiles::Context::operator=(const Context &other)
{
    if (&other == this)
        return *this;
    parent = other.parent;
    station = other.station;
    lastDownloadTime = other.lastDownloadTime;
    currentExecution = other.currentExecution;
    downloaderState = other.downloaderState;
    stationConfiguration = other.stationConfiguration;
    return *this;
}

void SyncFiles::Context::configureDownloader(FileDownloader *downloader) const
{
    downloader->setupExecutionBounds(lastDownloadTime, currentExecution);
    downloader->restoreState(*downloaderState);
}

void SyncFiles::Context::saveDownloader(FileDownloader *downloader) const
{
    *downloaderState = downloader->saveState();
}

bool SyncFiles::Context::signatureAuthorized(const QString &authorization)
{
    for (const auto &check : stationConfiguration) {
        auto auth = check.read().hash("Authorization").hash(authorization);
        if (!auth.exists())
            continue;
        if (!auth.hash("Remote").exists())
            continue;
        return true;
    }
    return false;
}

Variant::Read SyncFiles::Context::decryptionInformation(const QString &authorization,
                                                        const QString &encryptingCertificate,
                                                        DownloadFile *file)
{
    double time = file->getFileTime();
    if (!FP::defined(time))
        time = file->getModifiedTime();
    if (!FP::defined(time))
        time = Time::time();
    auto check = Range::findIntersecting(stationConfiguration.cbegin(), stationConfiguration.cend(),
                                         time);
    if (check == stationConfiguration.cend())
        return Variant::Read::empty();

    auto dec = check->read().hash("Files").hash("Decryption").hash(encryptingCertificate);
    if (dec.exists())
        return dec;
    return check->read()
                .hash("Authorization")
                .hash(authorization)
                .hash("Decryption")
                .hash(encryptingCertificate);
}

CPD3::Sync::Filter *SyncFiles::Context::createFilter(const QString &authorization)
{
    return new BasicFilter(ValueSegment::withPath(stationConfiguration,
                                                  QString("Authorization/%1/Remote/Synchronize").arg(
                                                          authorization)), station);
}


SyncFiles::UploadHandler::Uploader::Uploader(const CPD3::Data::Variant::Read &config,
                                             UploadHandler *p) : FileUploader(config), parent(p)
{ }

SyncFiles::UploadHandler::Uploader::~Uploader() = default;

QString SyncFiles::UploadHandler::Uploader::applySubstitutions(const QString &input) const
{ return parent->substitutions.apply(input); }

void SyncFiles::UploadHandler::Uploader::pushTemporaryOutput(const QFileInfo &info)
{
    parent->substitutions.push();
    parent->substitutions.setFile("upload", info);
    parent->substitutions.setFile("source", info);
    parent->substitutions.setFile("input", info);
}

void SyncFiles::UploadHandler::Uploader::popTemporaryOutput()
{
    parent->substitutions.pop();
}

void SyncFiles::UploadHandler::Uploader::pushInputFile(const QFileInfo &info)
{
    parent->substitutions.push();
    parent->substitutions.setFile("input", info);
    parent->substitutions.setFile("upload", info);
    parent->substitutions.setFile("source", info);
    parent->substitutions.setString("file", info.fileName());
}

void SyncFiles::UploadHandler::Uploader::popInputFile()
{
    parent->substitutions.pop();
}

SyncFiles::UploadHandler::UploadHandler(const Context &context,
                                        Filter *local,
                                        const Variant::Read &encryption,
                                        const Variant::Read &signature,
                                        const Variant::Read &upload,
                                        const std::shared_ptr<Archive::Access> &writeA,
                                        double modifiedTime) : SyncUpload(local, encryption,
                                                                          signature, upload,
                                                                          modifiedTime, writeA),
                                                               substitutions(
                                                                       context.substitutions())
{ }

SyncFiles::UploadHandler::~UploadHandler() = default;

std::unique_ptr<
        FileUploader> SyncFiles::UploadHandler::createUploader(const CPD3::Data::Variant::Read &configuration,
                                                               IO::Access::Handle inputFile)
{
    std::unique_ptr<FileUploader> result(new Uploader(configuration, this));
    result->addUpload(std::move(inputFile));
    return std::move(result);
}

SyncFiles::DownloadHandler::Downloader::Downloader(DownloadHandler *p) : parent(p)
{ }

SyncFiles::DownloadHandler::Downloader::~Downloader()
{ }

QString SyncFiles::DownloadHandler::Downloader::applySubstitutions(const QString &input) const
{ return parent->substitutions.apply(input); }

SyncFiles::DownloadHandler::Downloader::MatchingStack::MatchingStack(const SubstitutionStack &base,
                                                                     QList<TimeCapture> &caps)
        : SubstitutionStack(base), captures(caps)
{ }

SyncFiles::DownloadHandler::Downloader::MatchingStack::~MatchingStack()
{ }

QString SyncFiles::DownloadHandler::Downloader::MatchingStack::substitution(const QStringList &elements) const
{
    QString check(getTimeCapture(elements, captures));
    if (!check.isEmpty())
        return check;
    int index = 0;
    QString name;
    if (index < elements.size())
        name = elements.at(index++).toLower();
    if (name == "uid") {
        return "[a-zA-Z0-9]{8}";
    } else if (name == "authorization" || name == "auth" || name == "digest") {
        QString type;
        if (index < elements.size())
            type = elements.at(index++).toLower();
        if (type == "short")
            return "[a-zA-Z0-9]{16}";
        return "[a-fA-F0-9]{128}";
    }
    return QRegExp::escape(TextSubstitutionStack::substitution(elements));
}

QString SyncFiles::DownloadHandler::Downloader::applyMatching(const QString &input,
                                                              QList<TimeCapture> &captures) const
{
    MatchingStack subs(parent->substitutions, captures);
    return subs.apply(input);
}

void SyncFiles::DownloadHandler::Downloader::pushTemporaryOutput(const QFileInfo &fileName)
{
    parent->substitutions.push();
    parent->substitutions.setFile("file", fileName);
}

void SyncFiles::DownloadHandler::Downloader::popTemporaryOutput()
{ parent->substitutions.pop(); }

void SyncFiles::DownloadHandler::Downloader::pushFilterDirectory(const QDir &dir,
                                                                 const QDir &rootDir)
{
    parent->substitutions.push();
    parent->substitutions.setFile("directory", dir.absolutePath());
    parent->substitutions.setFile("rootdirectory", rootDir.absolutePath());
}

void SyncFiles::DownloadHandler::Downloader::pushFilterRemote(const std::string &path,
                                                              const std::string &rootPath)
{
    parent->substitutions.push();
    parent->substitutions.setString("directory", QString::fromStdString(path));
    parent->substitutions.setString("rootdirectory", QString::fromStdString(rootPath));
}

void SyncFiles::DownloadHandler::Downloader::popFilter()
{ parent->substitutions.pop(); }

SyncFiles::DownloadHandler::DownloadHandler(const Context &ctx,
                                            const Variant::Read &download,
                                            const std::shared_ptr<Archive::Access> &writeA)
        : SyncDownload(download, writeA), context(ctx), substitutions(context.substitutions())
{ }

SyncFiles::DownloadHandler::~DownloadHandler()
{ }

QString SyncFiles::DownloadHandler::applySubstitutions(const QString &input) const
{ return substitutions.apply(input); }

void SyncFiles::DownloadHandler::pushInvokeStage(const QString &input, const QString &output)
{
    substitutions.push();
    substitutions.setFile("input", input);
    substitutions.setFile("output", output);
}

void SyncFiles::DownloadHandler::popInvokeStage()
{
    substitutions.pop();
}

FileDownloader *SyncFiles::DownloadHandler::createDownloader(const Variant::Read &configuration)
{
    Downloader *downloader = new Downloader(this);
    context.configureDownloader(downloader);
    return downloader;
}

void SyncFiles::DownloadHandler::completeDownloader(FileDownloader *downloader)
{
    context.saveDownloader(downloader);
}

bool SyncFiles::DownloadHandler::signatureAuthorized(const QString &authorization)
{ return context.signatureAuthorized(authorization); }

CPD3::Data::Variant::Read SyncFiles::DownloadHandler::decryptionInformation(const QString &authorization,
                                                                            const QString &encryptingCertificate,
                                                                            CPD3::Transfer::DownloadFile *file)
{ return context.decryptionInformation(authorization, encryptingCertificate, file); }

CPD3::Sync::Filter *SyncFiles::DownloadHandler::createFilter(const QString &authorization)
{ return context.createFilter(authorization); }


void SyncFiles::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    terminateRequested();
}

bool SyncFiles::testTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}

void SyncFiles::run()
{
    if (testTerminated())
        return;
    double startProcessing = Time::time();

    SequenceSegment::Transfer globalConfiguration;
    {
        Archive::Access access;
        Archive::Access::ReadLock lock(access);

        auto allStations = access.availableStations(stations);
        if (allStations.empty()) {
            qCDebug(log_component_sync_files) << "No stations available";
            return;
        }
        stations.clear();
        std::copy(allStations.begin(), allStations.end(), Util::back_emplacer(stations));
        std::sort(stations.begin(), stations.end());
        globalConfiguration = SequenceSegment::Stream::read(
                Archive::Selection(FP::undefined(), FP::undefined(), stations, {"configuration"},
                                   {"synchronize"}), &access);
    }

    if (testTerminated())
        return;

    qCDebug(log_component_sync_files) << "Starting synchronize files for" << stations.size()
                                      << "stations(s)";

    for (const auto &station : stations) {
        if (testTerminated())
            break;

        SequenceName stationUnit(station, "configuration", "synchronize");
        ValueSegment::Transfer stationConfiguration;
        QSet<QString> uniqueProfiles;
        for (auto c = globalConfiguration.begin(), endC = globalConfiguration.end();
                c != endC;
                ++c) {
            auto add = c->takeValue(stationUnit);
            if (!add.read().exists())
                continue;

            stationConfiguration.emplace_back(c->getStart(), c->getEnd(), add);

            if (profiles.isEmpty()) {
                for (auto p : add.read().hash("Authorization").toHash()) {
                    if (p.first.empty())
                        continue;
                    uniqueProfiles.insert(QString::fromStdString(p.first));
                }
            } else {
                for (const auto &p : profiles) {
                    if (!add.read().hash("Authorization").hash(p).exists())
                        continue;
                    uniqueProfiles.insert(p);
                }
            }
        }
        if (uniqueProfiles.isEmpty())
            continue;
        if (stationConfiguration.empty())
            continue;


        QStringList sortedProfiles(uniqueProfiles.values());
        std::sort(sortedProfiles.begin(), sortedProfiles.end());

        qCDebug(log_component_sync_files) << "Starting synchronize for" << station << "with"
                                          << sortedProfiles.size() << "profile(s)";

        double beginStation = Time::time();

        for (const auto &profile : sortedProfiles) {
            if (testTerminated())
                break;

            double beginProfile = Time::time();
            Memory::release_unused();

            substitutions.clear();
            substitutions.setString("station", QString::fromStdString(station).toLower());
            substitutions.setString("profile", profile);

            Variant::Read profileConfiguration = Variant::Read::empty();
            Variant::Read filesConfiguration = Variant::Read::empty();
            {
                auto check = Range::findIntersecting(stationConfiguration.cbegin(),
                                                     stationConfiguration.cend(), Time::time());
                if (check != stationConfiguration.cend()) {
                    filesConfiguration = check->getValue().hash("Files");
                    profileConfiguration = check->getValue().hash("Authorization").hash(profile);
                }
            }

            auto uploadConfiguration = profileConfiguration.hash("Transfer");
            if (!uploadConfiguration.exists())
                uploadConfiguration = filesConfiguration.hash("Transfer");

            auto downloadConfiguration = profileConfiguration.hash("Sources");
            if (!downloadConfiguration.exists())
                downloadConfiguration = filesConfiguration.hash("Sources");

            bool doProfileUpload = doUpload;
            if (!uploadConfiguration.exists())
                doProfileUpload = false;

            bool doProfileDownload = doDownload;
            if (!downloadConfiguration.exists())
                doProfileDownload = false;

            if (!doProfileUpload && !doProfileDownload) {
                qCDebug(log_component_sync_files) << "No upload or download actions for" << station
                                                  << profile;
                continue;
            }

            Database::RunLock rc;

            static const quint8 rcSynchronizeStateVersion = 1;

            double modifiedAfter = FP::undefined();
            QString rcKeySynchronize;
            QByteArray uploaderState;
            if (doProfileUpload) {
                feedback.emitStage(tr("Lock %1").arg(QString::fromStdString(station).toUpper()),
                                   tr("The system is acquiring an exclusive lock for %1 on profile %2.")
                                           .arg(QString::fromStdString(station).toUpper(),
                                                profile.toLower()), false);
                qCDebug(log_component_sync_files) << "Locking " << station << profile;

                bool ok = false;
                rcKeySynchronize =
                        QString("synchronize %1 %2").arg(QString::fromStdString(station), profile);
                modifiedAfter = rc.acquire(rcKeySynchronize, lockTimeout, &ok);
                if (!ok) {
                    feedback.emitFailure();
                    qCDebug(log_component_sync_files) << "Update skipped for" << station
                                                      << "profile" << profile;
                    continue;
                }

                substitutions.setTime("modified", modifiedAfter, false);
                substitutions.setTime("start", modifiedAfter, false);

                {
                    QByteArray data(rc.get(rcKeySynchronize));
                    if (!data.isEmpty() && (!haveAfter || !FP::defined(afterTime))) {
                        QDataStream stream(&data, QIODevice::ReadOnly);
                        quint8 version = 0;
                        stream >> version;

                        if (stream.status() != QDataStream::Ok ||
                                version != rcSynchronizeStateVersion) {
                            qCDebug(log_component_sync_files) << "Invalid state data";
                        } else {
                            stream >> uploaderState;
                        }
                    }
                }
            }

            static const quint8 rcDownloadStateVersion = 1;

            double lastDownloadTime = FP::undefined();
            QByteArray downloaderState;
            QString rcKeyDownload;
            if (doProfileDownload) {
                if (!doProfileUpload) {
                    feedback.emitStage(tr("Lock %1").arg(QString::fromStdString(station).toUpper()),
                                       tr("The system is acquiring an exclusive lock for %1 on profile %2.")
                                               .arg(QString::fromStdString(station).toUpper(),
                                                    profile.toLower()), false);
                    qCDebug(log_component_sync_files) << "Locking " << station << profile;
                }

                bool ok = false;
                rcKeyDownload =
                        QString("synchronizedownload %1 %2").arg(QString::fromStdString(station),
                                                                 profile);
                lastDownloadTime = rc.acquire(rcKeyDownload, lockTimeout, &ok);
                if (!ok) {
                    feedback.emitFailure();
                    qCDebug(log_component_sync_files) << "Update skipped for" << station
                                                      << "profile" << profile;
                    continue;
                }

                {
                    QByteArray data(rc.get(rcKeyDownload));
                    if (!data.isEmpty() && (!haveAfter || !FP::defined(afterTime))) {
                        QDataStream stream(&data, QIODevice::ReadOnly);
                        quint8 version = 0;
                        stream >> version;

                        if (stream.status() != QDataStream::Ok ||
                                version != rcDownloadStateVersion) {
                            qCDebug(log_component_sync_files) << "Invalid state data";
                        } else {
                            stream >> downloaderState;
                        }
                    }
                }
            }

            double currentExecution = rc.startTime();

            substitutions.setTime("end", currentExecution, false);
            substitutions.setTime("time", Time::time(), false);
            substitutions.setString("uid", Random::string(8));

            if (haveAfter) {
                lastDownloadTime = afterTime;
                modifiedAfter = afterTime;
            }

            auto encryption = profileConfiguration.hash("Encryption");
            if (!encryption.exists())
                encryption = filesConfiguration.hash("Encryption");

            auto sign = profileConfiguration.hash("Sign");
            if (!sign.exists())
                sign = filesConfiguration.hash("Sign");
            substitutions.setDigest("local", sign);

            if (!substitutions.setDigest("remote", encryption)) {
                QByteArray profileDigest(QByteArray::fromHex(profile.toLatin1()));
                if (profileDigest.length() == profile.length() * 2) {
                    substitutions.setDigest("remote", profileDigest);
                }
            }

            auto writer = std::make_shared<Archive::Access>();

            Context context(this, station, lastDownloadTime, currentExecution, downloaderState,
                            stationConfiguration);

            std::unique_ptr<DownloadHandler> downloader;
            bool downloadStageOutput = false;
            if (doProfileDownload) {
                downloader.reset(new DownloadHandler(context, downloadConfiguration, writer));
                terminateRequested.connect(downloader.get(), "signalTerminate",
                                           Qt::DirectConnection);
                downloader->feedback.forward(feedback);
                downloadStageOutput = true;

                downloader->start();
            }

            std::unique_ptr<UploadHandler> uploader;
            if (doProfileUpload) {
                /* Do the tracking state with the local filter, since it's what
                 * generates the requests.  This is inconsistent with the other
                 * interactive synchronizes, but we don't have a choice.  This
                 * works since the state is just a modified time tracking, so
                 * here we'll generate output for any bidirectional ones
                 * that have been updated.  This breaks down if the  incoming
                 * also has a modified one, before we see an update from
                 * another source. */
                Filter *filter = new TrackingFilter(ValueSegment::withPath(stationConfiguration,
                                                                           QString("Authorization/%1/Local/Synchronize")
                                                                                   .arg(profile)),
                                                    station, uploaderState);
                uploader.reset(
                        new UploadHandler(context, filter, encryption, sign, uploadConfiguration,
                                          writer, modifiedAfter));
                terminateRequested.connect(uploader.get(), "signalTerminate", Qt::DirectConnection);

                /* See if the downloader finishes really fast, so we can emit progress
                 * from the upload instead */
                if (!downloader || downloader->wait(500)) {
                    downloadStageOutput = false;
                    uploader->feedback.forward(feedback);
                }

                uploader->start();
            }

            if (testTerminated()) {
                if (uploader) {
                    uploader->signalTerminate();
                    uploader->wait();
                    uploader.reset();
                }
                if (downloader) {
                    downloader->signalTerminate();
                    downloader->wait();
                    downloader.reset();
                }
                writer->signalTerminate();
                writer->waitForLocks();
                rc.fail();
                continue;
            }

            writer.reset();

            bool ok = true;

            if (downloader) {
                downloader->wait();
                if (!downloader->succeeded())
                    ok = false;

                auto fileResults = downloader->getFileResults();
                downloader.reset();

                if (!fileResults.isEmpty()) {
                    Variant::Root event;

                    event["Text"].setString(tr("Completed synchronization download of %n file(s)",
                                               "synchronize event text", fileResults.size()));
                    event["Information"].hash("Profile").setString(profile);
                    event["Information"].hash("By").setString("sync_files");
                    event["Information"].hash("At").setDouble(Time::time());
                    event["Information"].hash("Environment").setString(Environment::describe());
                    event["Information"].hash("Revision").setString(Environment::revision());
                    event["Information"].hash("TotalFiles").setInt64(fileResults.size());
                    event["Information"].hash("LastRunTime").setDouble(lastDownloadTime);
                    event["Information"].hash("EndTime").setDouble(currentExecution);
                    event["Information"].hash("ElapsedTime").setDouble(Time::time() - beginProfile);

                    for (const auto &file : fileResults) {
                        event["Files"].toArray().after_back().set(file);
                    }

                    double now = Time::time();
                    Archive::Access().writeSynchronous(SequenceValue::Transfer{
                            SequenceValue({station, "events", "synchronize"}, std::move(event), now,
                                          now, 1)});
                }
            }

            if (uploader) {
                if (downloadStageOutput) {
                    feedback.emitStage(
                            tr("Outgoing %1").arg(QString::fromStdString(station).toUpper()),
                            tr("The system is completing the upload process for %1.").arg(
                                    QString::fromStdString(station).toUpper()), false);
                }
                uploader->wait();

                Variant::Read uploadResult = Variant::Read::empty();
                if (!uploader->succeeded()) {
                    ok = false;
                    if (doProfileDownload) {
                        feedback.emitFailure();
                    }
                } else {
                    uploadResult = uploader->getResultInformation();
                }

                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    stream << rcSynchronizeStateVersion;
                    stream << uploader->getFilterState();
                }
                rc.set(rcKeySynchronize, data);

                uploader.reset();

                if (uploadResult.exists()) {
                    Variant::Root event;

                    event["Text"].setString(
                            tr("Completed synchronization file upload", "synchronize event text"));
                    event["Information"].set(uploadResult);
                    event["Information"].hash("Profile").setString(profile);
                    event["Information"].hash("By").setString("sync_files");
                    event["Information"].hash("At").setDouble(Time::time());
                    event["Information"].hash("Environment").setString(Environment::describe());
                    event["Information"].hash("Revision").setString(Environment::revision());
                    event["Information"].hash("ModifiedTime").setDouble(modifiedAfter);
                    event["Information"].hash("ElapsedTime").setDouble(Time::time() - beginProfile);

                    double now = Time::time();
                    Archive::Access().writeSynchronous(SequenceValue::Transfer{
                            SequenceValue({station, "events", "synchronize"}, std::move(event), now,
                                          now, 2)});
                }
            }

            if (!ok) {
                feedback.emitFailure(tr("Synchronization failed"));
                qCDebug(log_component_sync_files) << "Error in file synchronization process";
                rc.fail();
                continue;
            }

            feedback.emitStage(tr("Finalize %1").arg(QString::fromStdString(station).toUpper()),
                               tr("Final cleanup is taking place for %1.").arg(
                                       QString::fromStdString(station).toUpper()), false);

            if (doProfileDownload) {
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    stream << rcDownloadStateVersion;
                    stream << downloaderState;
                }
                rc.set(rcKeyDownload, data);
            }

            rc.release();
            double endProfile = Time::time();

            qCInfo(log_component_sync_files) << "Synchronize for profile" << profile << "on"
                                             << station << "finished after"
                                             << Logging::elapsed(endProfile - beginProfile);
        }

        double finishStation = Time::time();

        qCDebug(log_component_sync_files) << "Synchronize for" << station << "finished after"
                                          << Logging::elapsed(finishStation - beginStation);
    }

    if (runUpdate)
        invokeUpdated();

    double endProcessing = Time::time();
    qCDebug(log_component_sync_files) << "Synchronize finished after "
                                      << Logging::elapsed(endProcessing - startProcessing);

    QCoreApplication::processEvents();
}


void SyncFiles::executeAction(CPD3Action *action)
{
    if (action == NULL) {
        qCWarning(log_component_sync_files) << "Unable to create action";
        return;
    }

    action->feedback.forward(feedback);
    terminateRequested.connect(action, "signalTerminate", Qt::DirectConnection);

    action->start();
    action->wait();
    delete action;
}

void SyncFiles::invokeUpdated()
{
    feedback.emitStage(tr("Checking for updated data"),
                       tr("The system is starting the passed data update."), false);

    QObject *component = ComponentLoader::create("update_passed");
    if (component == NULL) {
        qCWarning(log_component_sync_files) << "Can't load updated passed component";
        return;
    }

    ActionComponentTime *actionComponentTime;
    if (!(actionComponentTime = qobject_cast<ActionComponentTime *>(component))) {
        qCWarning(log_component_sync_files) << "Updated passed component is not a time action";
        return;
    }

    if (actionComponentTime->actionAllowStations() < 1 ||
            actionComponentTime->actionRequireStations() > 1) {
        qCWarning(log_component_sync_files)
            << "Updated passed component does not accept the number of passed stations";
        return;
    }

    ComponentOptions o = actionComponentTime->getOptions();

    if (auto detachedOption = qobject_cast<ComponentOptionBoolean *>(o.get("detached"))) {
        detachedOption->set(true);
    }

    executeAction(actionComponentTime->createTimeAction(o, stations));
}


ComponentOptions SyncFilesComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionStringSet(tr("profile", "name"), tr("Synchronize profile"),
                                             tr("This is the authorization profile to synchronize.  The profile "
                                                "controls where data are read from and written to.  "
                                                "Consult the station configuration and authorization for available profiles."),
                                             tr("All available")));

    options.add("update", new ComponentOptionBoolean(tr("update", "name"), tr("Enable data update"),
                                                     tr("When disabled, this causes the immediate data update to be "
                                                        "skipped.  The result is that the clean and averaged data are "
                                                        "not updated until the next time the station tasks are run at "
                                                        "the appropriate level.  This normally means that the data will "
                                                        "not be updated until the following night when the execution tasks "
                                                        "run automatically."), tr("Enabled")));

    options.add("upload", new ComponentOptionBoolean(tr("upload", "name"), tr("Upload data files"),
                                                     tr("This controls if data files containing local changes are generated.  "
                                                        "When disabled local changes are not uploaded."),
                                                     tr("Enabled")));

    options.add("download",
                new ComponentOptionBoolean(tr("download", "name"), tr("Download data files"),
                                           tr("This controls if data files containing remote changes are processed.  "
                                              "When disabled any remote files are ignored and not processed.  "
                                              "They may still be processed later if the synchronize is run without "
                                              "disabling them."), tr("Enabled")));

    ComponentOptionSingleDouble *timeout =
            new ComponentOptionSingleDouble(tr("wait", "name"), tr("Maximum wait time"),
                                            tr("This is the maximum time to wait for another process to "
                                               "complete, in seconds.  If there are two concurrent processes for "
                                               "the same station and profile, the new one will wait at most this "
                                               "many seconds before giving up without performing an update."),
                                            tr("Thirty seconds"));
    timeout->setMinimum(0.0);
    timeout->setAllowUndefined(true);
    options.add("timeout", timeout);

    ComponentOptionSingleTime *after =
            new ComponentOptionSingleTime(tr("after", "name"), tr("Override time"),
                                          tr("This sets the time to synchronize after.  This can be used "
                                             "to re-synchronize data after a given time or to completely resynchronize all data."),
                                          tr("Last run time"));
    after->setAllowUndefined(true);
    options.add("after", after);

    return options;
}

QList<ComponentExample> SyncFilesComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Run synchronization", "default example name"),
                                     tr("This will run synchronization using all available profiles.")));

    return examples;
}

int SyncFilesComponent::actionAllowStations()
{ return INT_MAX; }

CPD3Action *SyncFilesComponent::createAction(const ComponentOptions &options,
                                             const std::vector<std::string> &stations)
{
    return new SyncFiles(options, stations);
}


