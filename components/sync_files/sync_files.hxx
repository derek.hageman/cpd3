/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef SYNCFILES_H
#define SYNCFILES_H

#include "core/first.hxx"

#include <memory>
#include <mutex>
#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "core/threading.hxx"
#include "core/actioncomponent.hxx"
#include "core/textsubstitution.hxx"
#include "datacore/segment.hxx"
#include "datacore/archive/access.hxx"
#include "sync/files.hxx"

class SyncFiles : public CPD3::CPD3Action {
Q_OBJECT

    friend class PlotOutputTarget;

    QSet<QString> profiles;
    std::vector<CPD3::Data::SequenceName::Component> stations;
    double lockTimeout;
    bool runUpdate;

    bool haveAfter;
    double afterTime;

    bool doUpload;
    bool doDownload;

    std::mutex mutex;
    bool terminated;

    class SubstitutionStack : public CPD3::TextSubstitutionStack {
    public:
        SubstitutionStack();

        SubstitutionStack(const SubstitutionStack &other);

        SubstitutionStack &operator=(const SubstitutionStack &other);

        void setDigest(const QString &key, const QByteArray &digest);

        bool setDigest(const QString &key, const CPD3::Data::Variant::Read &value);
    };

    SubstitutionStack substitutions;

    class Context {
        SyncFiles *parent;
        CPD3::Data::SequenceName::Component station;
        double lastDownloadTime;
        double currentExecution;
        QByteArray *downloaderState;
        CPD3::Data::ValueSegment::Transfer stationConfiguration;

        friend class SyncFiles;

        Context(SyncFiles *parent,
                const CPD3::Data::SequenceName::Component &station,
                double lastDownloadTime,
                double currentExecution,
                QByteArray &downloaderState,
                const CPD3::Data::ValueSegment::Transfer &stationConfiguration);

        Context()
        { }

    public:
        Context(const Context &other);

        Context &operator=(const Context &other);

        inline const SubstitutionStack &substitutions() const
        { return parent->substitutions; }

        void configureDownloader(CPD3::Transfer::FileDownloader *downloader) const;

        void saveDownloader(CPD3::Transfer::FileDownloader *downloader) const;

        bool signatureAuthorized(const QString &authorization);

        CPD3::Data::Variant::Read decryptionInformation(const QString &authorization,
                                                        const QString &encryptingCertificate,
                                                        CPD3::Transfer::DownloadFile *file);

        CPD3::Sync::Filter *createFilter(const QString &authorization);
    };

    friend class Context;

    class UploadHandler : public CPD3::Sync::SyncUpload {
        SubstitutionStack substitutions;

        class Uploader : public CPD3::Transfer::FileUploader {
            UploadHandler *parent;
        public:
            Uploader(const CPD3::Data::Variant::Read &config, UploadHandler *parent);

            virtual ~Uploader();

        protected:
            virtual QString applySubstitutions(const QString &input) const;

            virtual void pushTemporaryOutput(const QFileInfo &fileName);

            virtual void popTemporaryOutput();

            virtual void pushInputFile(const QFileInfo &file);

            virtual void popInputFile();
        };

        friend class Uploader;

    public:
        UploadHandler(const Context &context,
                      CPD3::Sync::Filter *local,
                      const CPD3::Data::Variant::Read &encryption,
                      const CPD3::Data::Variant::Read &signature,
                      const CPD3::Data::Variant::Read &upload,
                      const std::shared_ptr<CPD3::Data::Archive::Access> &writeA,
                      double modifiedTime);

        virtual ~UploadHandler();

    protected:
        std::unique_ptr<
                CPD3::Transfer::FileUploader> createUploader(const CPD3::Data::Variant::Read &configuration,
                                                             CPD3::IO::Access::Handle inputFileinputFile);
    };

    class DownloadHandler : public CPD3::Sync::SyncDownload {
        Context context;
        SubstitutionStack substitutions;

        class Downloader : public CPD3::Transfer::FileDownloader {
            DownloadHandler *parent;

            friend class DownloadHandler;

        public:
            Downloader(DownloadHandler *parent);

            virtual ~Downloader();

        protected:
            QString applySubstitutions(const QString &input) const override;

            class MatchingStack : public SubstitutionStack {
                QList<TimeCapture> &captures;
            public:
                MatchingStack(const SubstitutionStack &base, QList<TimeCapture> &captures);

                virtual ~MatchingStack();

            protected:
                QString substitution(const QStringList &elements) const override;
            };

            QString applyMatching(const QString &input,
                                  QList<TimeCapture> &captures) const override;

            void pushTemporaryOutput(const QFileInfo &fileName) override;

            void popTemporaryOutput() override;

            void pushFilterDirectory(const QDir &dir, const QDir &rootDir) override;

            void pushFilterRemote(const std::string &path, const std::string &rootPath) override;

            void popFilter() override;
        };

    public:
        DownloadHandler(const Context &context,
                        const CPD3::Data::Variant::Read &download,
                        const std::shared_ptr<CPD3::Data::Archive::Access> &writeA);

        virtual ~DownloadHandler();

    protected:
        QString applySubstitutions(const QString &input) const override;

        void pushInvokeStage(const QString &input, const QString &output) override;

        void popInvokeStage() override;

        virtual CPD3::Transfer::FileDownloader *createDownloader(const CPD3::Data::Variant::Read &configuration);

        virtual void completeDownloader(CPD3::Transfer::FileDownloader *downloader);

        virtual bool signatureAuthorized(const QString &authorization);

        CPD3::Data::Variant::Read decryptionInformation(const QString &authorization,
                                                        const QString &encryptingCertificate,
                                                        CPD3::Transfer::DownloadFile *file) override;

        virtual CPD3::Sync::Filter *createFilter(const QString &authorization);
    };

    friend class DownloadHandler;

    void executeAction(CPD3::CPD3Action *action);

    void invokeUpdated();

    bool testTerminated();

    CPD3::Threading::Signal<> terminateRequested;

public:
    SyncFiles();

    SyncFiles(const CPD3::ComponentOptions &options,
              const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~SyncFiles();

    virtual void signalTerminate();

protected:
    virtual void run();
};


class SyncFilesComponent : public QObject, virtual public CPD3::ActionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::ActionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.sync_files"
                              FILE
                              "sync_files.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int actionAllowStations();

    CPD3::CPD3Action *createAction(const CPD3::ComponentOptions &options = {},
                                   const std::vector<std::string> &stations = {}) override;
};

#endif
