/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREECOTECHNEPHAURORA_HXX
#define ACQUIREECOTECHNEPHAURORA_HXX

#include "core/first.hxx"

#include <memory>
#include <deque>
#include <map>
#include <array>
#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "acquisition/spancheck.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"
#include "smoothing/baseline.hxx"

class AcquireEcotechNephAurora : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;
    int autoprobeValidRecords;

    enum class ResponseState : int {
        /* Waiting a response to a command in passive mode */
                Passive_Run,
        /* Waiting for enough data to confirm communications */
                Passive_Wait,
        /* Passive initialization (same as passive wait, but discard the
         * first timeout). */
                Passive_Initialize,

        /* Same as passive wait but currently autoprobing */
                Autoprobe_Passive_Wait,
        /* Same as passive initialize but autoprobing */
                Autoprobe_Passive_Initialize,

        /* ID */
                Interactive_Start_ReadID,
        /* VI000 */
                Interactive_Start_ReadStatus,
        /* **0S... */
                Interactive_Start_SetTime,
        /* **0PCF,... */
                Interactive_Start_SetFilter,
        /* **0PCSTP,... */
                Interactive_Start_SetNormalization,
        /* EE */
                Interactive_Start_ReadEE,
        /* VI088 */
                Interactive_Start_CheckFlags,
        /* DO0000 */
                Interactive_Start_DisableSpan,
        /* DO0010 */
                Interactive_Start_DisableZero,

        /* Waiting before attempting a communications restart */
                Interactive_Restart_Wait,
        /* Initial state for interactive start */
                Interactive_Initialize,

        /* In interactive mode interrogating the instrument. */
                Interactive_Run_Active,
        /* A command has failed and the retry is in progress. */
                Interactive_Run_Retry,
        /* Discard before the resume. */
                Interactive_Run_Resume,
        /* During interrogation, waiting before sending the next command. */
                Interactive_Run_Delay,

        /* After a zero, reading the EE results */
                Interactive_Zero_ReadEE,
        /* Same, except in retry mode */
                Interactive_Zero_ReadEE_Retry,
        /* Now discarding before the resume. */
                Interactive_Zero_ReadEE_Resume,
    } responseState;
    std::size_t commandRetryFailureCounter;
    double commandRetryAbsoluteTimeout;

    bool isInteractiveStart() const;

    bool isInteractive() const;

    enum class CommandState : int {
        /* VI000: system state */
                SystemState,

        /* VI099: single line output */
                DataLine,

        /* VI004: dark counts (use moving average, since the sample counts are) */
                Dark,


        /* VI006: shutter/reference counts */
                Red_Reference,

        /* VI007: sample counts */
                Red_Sample,

        /* VI008: sample/reference ratio */
                Red_Ratio,

        /* VI033: backscatter sample counts */
                Red_BackSample,

        /* VI034: backscatter sample/reference ratio */
                Red_BackRatio,


        /* VI009: shutter/reference counts */
                Green_Reference,

        /* VI010: sample counts */
                Green_Sample,

        /* VI011: sample/reference ratio */
                Green_Ratio,

        /* VI035: backscatter sample counts */
                Green_BackSample,

        /* VI036: backscatter sample/reference ratio */
                Green_BackRatio,


        /* VI012: shutter/reference counts */
                Blue_Reference,

        /* VI013: sample counts */
                Blue_Sample,

        /* VI014: sample/reference ratio */
                Blue_Ratio,

        /* VI037: backscatter sample counts */
                Blue_BackSample,

        /* VI038: backscatter sample/reference ratio */
                Blue_BackRatio,


        /* VI088: system status flags */
                StatusFlags,
    } commandState;

    std::deque<CPD3::Util::ByteArray> okCommandQueue;

    enum class CommandOperation {
        Resend, Advance, Response
    };

    void commandAdvance(double time, CommandOperation operation);

    bool isCommandCycleComplete() const;

    enum class SampleState : int {
        Measure,

        ZeroAccumulateEnterBlank, ZeroAccumulate,

        ZeroInstrument,

        BlankExitZero,

        Spancheck,

        InstrumentCalibration,
    } sampleState;
    double stateEndTime;
    bool zeroReadPending;

    enum class Feature : std::size_t {
        Polar, StatusFlags, FilterControl,

        COUNT
    };
    std::array<bool, static_cast<std::size_t>(Feature::COUNT)> features;

    inline bool hasFeature(Feature feature) const
    { return features[static_cast<std::size_t>(feature)]; }

    inline void setFeature(Feature feature, bool status)
    { features[static_cast<std::size_t>(feature)] = status; }

public:
    class ActiveCommand;

    friend class ActiveCommand;

private:
    std::deque<std::unique_ptr<ActiveCommand>> outstandingCommands;

    class SpancheckInterface
            : public CPD3::Acquisition::NephelometerSpancheckController::Interface {
        AcquireEcotechNephAurora &parent;
    public:
        explicit SpancheckInterface(AcquireEcotechNephAurora &neph);

        virtual ~SpancheckInterface();

        void setBypass(bool enable) override;

        void switchToFilteredAir() override;

        void switchToGas(CPD3::Algorithms::Rayleigh::Gas gas) override;

        void issueZero() override;

        void issueCommand(const QString &target, const CPD3::Data::Variant::Read &command) override;

        bool start() override;

        void completed() override;

        void aborted() override;
    };

    friend class SpancheckInterface;

    SpancheckInterface spancheckInterface;

    enum class LogStream : std::size_t {
        Instant = 0, Metadata, Flags, Optical, Reference,

        TOTAL,
    };
    CPD3::Data::StaticMultiplexer loggingMux;
    std::array<double, static_cast<std::size_t>(LogStream::TOTAL)> streamBeginTime;

    struct Sample {
        std::array<std::map<double, CPD3::Data::Variant::Root>, 3> Bs;
        std::array<CPD3::Data::Variant::Root, 3> Cf;
        std::array<CPD3::Data::Variant::Root, 3> Cs;
        std::array<CPD3::Data::Variant::Root, 3> Cbs;
        std::array<CPD3::Data::Variant::Root, 3> Cr;
        std::array<CPD3::Data::Variant::Root, 3> Cbr;
        CPD3::Data::Variant::Root Cd;

        CPD3::Data::Variant::Root T;
        CPD3::Data::Variant::Root Tx;
        CPD3::Data::Variant::Root U;
        CPD3::Data::Variant::Root P;

        CPD3::Data::Variant::Flags reportedFlags;
        CPD3::Data::Variant::Root F2;

        int majorState;
        int minorState;
    } values;
    std::vector<double> angles;

    enum class OpticalUpdate {
        Unchanged, ChangeDetected, ReadPending,
    } opticalUpdate;
    double opticalUpdateTimeout;

    void opticalChangeDetected();

    enum class ReferenceUpdate {
        ShutterOff, ShutterOn, ReadPending,
    } referenceUpdate;
    std::array<bool, 3> referenceColorsRead;
    double referenceUpdateTimeout;

    double blankEndTime;

    void beginExplicitBlank(double time = CPD3::FP::undefined());

    double beginZeroFill(double time = CPD3::FP::undefined());

    inline bool haveAllReferenceColors() const
    { return referenceColorsRead[0] && referenceColorsRead[1] && referenceColorsRead[2]; }

    void processShutterUpdate();

    void processReferenceUpdate(std::size_t color, bool changed);

    void autoprobeValidResponse(double frameTime);

    void dataAdvance(double frameTime);

    void outputOptical(double frameTime);

    void outputReference(double frameTime);

    void outputInstant(double frameTime);

    void outputFlags(double frameTime);

    void finishOutput(double frameTime, LogStream stream);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root value);

    void loggingValue(LogStream stream,
                      double time,
                      CPD3::Data::SequenceName::Component name,
                      CPD3::Data::Variant::Root value);


    enum class EEComponent : std::size_t {
        Angles, TemperatureUnit, PressureUnit, ReportingTemperature, FilteringMethod,

        WavelengthRed, WavelengthGreen, WavelengthBlue,

        ZeroTemperature, ZeroPressure,

        CalMRed, CalMGreen, CalMBlue,

        CalCRed, CalCGreen, CalCBlue,

        ZeroXRed, ZeroXGreen, ZeroXBlue,

        WallRed, WallGreen, WallBlue,

        COUNT
    };
    std::array<bool, static_cast<std::size_t>(EEComponent::COUNT)> seenEEComponents;

    inline void markEESeen(EEComponent component)
    { seenEEComponents[static_cast<std::size_t>(component)] = true; }

    double readEEAbsoluteTimeout;

    void processEELine(const CPD3::Util::ByteArray &frame, double frameTime);

    bool haveSeenAllEE() const;

    bool haveSeenAnyEE() const;

    void finalizeEE(double frameTime);

    class Configuration {
        double start;
        double end;

    public:
        int address;
        double inactiveDelay;
        double commandDelay;
        double retryDelay;
        double retryBusyTimeout;
        std::size_t retryFailureMaximum;

        double opticalUpdateTimeout;
        double referenceUpdateTimeout;

        std::array<double, 3> wavelengths;

        CPD3::Time::LogicalTimeUnit blankUnit;
        int blankCount;
        bool blankAlign;

        enum class Zero {
            Native,

            EnableFilter,

            Offset,
        } zeroMode;
        CPD3::Time::LogicalTimeUnit zeroMeasureUnit;
        int zeroMeasureCount;
        bool zeroMeasureAlign;

        CPD3::Time::LogicalTimeUnit zeroFillUnit;
        int zeroFillCount;
        bool zeroFillAlign;

        enum class Filter {
            None, Kalman, Average
        } filterMode;
        double normalizationTemperature;

        std::unordered_map<std::string, CPD3::Data::Variant::Root> zeroEnableCommands;
        std::unordered_map<std::string, CPD3::Data::Variant::Root> zeroDisableCommands;


        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under,
                      const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    std::deque<Configuration> config;
    std::array<double, 3> reportedWavelengths;
    std::array<double, 3> effectiveWavelengths;
    double reportingTemperature;
    bool inconsistentZero;

    std::unique_ptr<CPD3::Acquisition::NephelometerSpancheckController> spancheckController;

    CPD3::Data::SequenceValue spancheckDetails;

    CPD3::Data::Variant::Root instrumentMeta;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool realtimeStateUpdated;

    CPD3::Data::SequenceValue digitalState;

    CPD3::Data::Variant::Root lastEEResults;
    bool persistentEEUpdated;
    bool realtimeEEUpdated;

    CPD3::Data::Variant::Root Tw;
    CPD3::Data::Variant::Root Pw;
    std::array<std::map<double, CPD3::Data::Variant::Root>, 3> Bsw;
    std::array<std::map<double, CPD3::Data::Variant::Root>, 3> Bsz;
    std::array<std::map<double, CPD3::Data::Variant::Root>, 3> Bswd;
    std::array<std::map<double, CPD3::Data::Variant::Root>, 3> effectiveBsw;
    double zeroEffectiveTime;
    bool realtimeZeroUpdated;
    bool persistentZeroUpdated;

    class ZeroAccumulator {
        double sum;
        std::size_t count;
    public:
        inline ZeroAccumulator() : sum(0), count(0)
        { }

        inline void operator+=(double add)
        {
            if (!CPD3::FP::defined(add))
                return;
            sum += add;
            count++;
        }

        inline double value() const
        {
            if (!count)
                return CPD3::FP::undefined();
            return sum / static_cast<double>(count);
        }
    };

    ZeroAccumulator zeroT;
    ZeroAccumulator zeroP;
    std::array<std::map<double, ZeroAccumulator>, 3> zeroBs;
    std::array<std::vector<double>, 3> calM;
    std::array<std::vector<double>, 3> calC;
    std::array<std::vector<double>, 3> zeroAdjX;
    std::array<std::vector<double>, 3> calWall;

    bool requestZero();

    void applyZeroOffset(CPD3::Data::Variant::Write &value, std::size_t color, double angle);

    void finishZeroAccumulation(double time);

    void updateEffectiveZero();

    void completeZero(double time);

    bool zeroConditionAccumulationActive() const;

    bool zeroOpticalAccumulationActive() const;

    void setDefaultInvalid();

    void invalidateLogValues(double frameTime);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    bool logScatteringsValid(double time) const;

    void configAdvance(double frameTime);

    CPD3::Data::Variant::Root describeState() const;

    void retryBusyExhausted(double frameTime);

    int processUnpolledResponse(const CPD3::Util::ByteView &frame, double &frameTime);

    CPD3::Data::SequenceValue::SequenceValue::Transfer constructZeroVariables(double time) const;

    CPD3::Data::SequenceValue::Transfer constructSpancheckVariables(bool includeRealtime = false);

    void updateSpancheckData();

public:
    AcquireEcotechNephAurora(const CPD3::Data::ValueSegment::Transfer &config,
                             const std::string &loggingContext);

    AcquireEcotechNephAurora(const CPD3::ComponentOptions &options,
                             const std::string &loggingContext);

    virtual ~AcquireEcotechNephAurora();

    CPD3::Data::SequenceValue::Transfer getPersistentValues() override;

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    CPD3::Data::Variant::Root getCommands() override;

    CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables() override;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus() override;

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    void serializeState(QDataStream &stream) override;

    void deserializeState(QDataStream &stream) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    void command(const CPD3::Data::Variant::Read &value) override;

    std::size_t dataFrameEnd(std::size_t start, const CPD3::Util::ByteArray &input) const override;

    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;
};

class AcquireEcotechNephAuroraComponent
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_ecotech_nephaurora"
                              FILE
                              "acquire_ecotech_nephaurora.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
