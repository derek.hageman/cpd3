/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <array>
#include <cstdint>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;

    enum class ReferenceShutter {
        Up, MovingDown, Down, MovingUp
    } referenceShutter;
    double referenceShutterTime;

    enum class ZeroState {
        Disabled, FlushBefore, Measure,
    } zeroState;
    double zeroStateTime;

    bool dospan;
    bool dozero;

    std::vector<double> angles;

    std::array<std::vector<double>, 3> Bs;
    double scatteringUpdateTime;
    bool scatteringDeltaPolarity;

    std::array<double, 3> Cf;
    bool referenceDeltaPolarity;

    std::array<double, 3> Cs;
    std::array<double, 3> Cbs;
    double Cd;

    double Tx;
    double T;
    double U;
    double P;

    std::uint_fast16_t F1;
    std::uint_fast16_t F2;

    QByteArray id;
    std::array<std::vector<double>, 3> zeroAdjX;
    std::array<std::vector<double>, 3> zeroAdjY;
    std::array<std::vector<double>, 3> calM;
    std::array<std::vector<double>, 3> calC;
    std::array<std::vector<double>, 3> calWall;

    bool enableFilterControl;
    enum class FilterMode {
        None, Kalman, Average
    } filterMode;
    double normalizationTemperature;

    std::size_t garbleCounter;
    std::size_t busyCounter;
    std::size_t ignoreCounter;

    ModelInstrument()
            : incoming(),
              outgoing(),
              unpolledRemaining(FP::undefined()),
              referenceShutter(ReferenceShutter::Up),
              referenceShutterTime(5.0),
              zeroState(ZeroState::Disabled),
              zeroStateTime(0),
              dospan(false),
              dozero(false),
              angles({0, 90}),
              scatteringUpdateTime(1.0),
              scatteringDeltaPolarity(false),
              referenceDeltaPolarity(false)
    {
        Bs[0] = std::vector<double>{10.0, 3.0};
        Bs[1] = std::vector<double>{20.0, 6.0};
        Bs[2] = std::vector<double>{30.0, 15.0};

        Cf[0] = 100000;
        Cf[1] = 200000;
        Cf[2] = 300000;

        Cs[0] = 1000;
        Cs[1] = 2000;
        Cs[2] = 3000;

        Cbs[0] = 1100;
        Cbs[1] = 2200;
        Cbs[2] = 3300;

        zeroAdjX[0] = std::vector<double>{60.0, 30.0};
        zeroAdjX[1] = std::vector<double>{90.0, 45.0};
        zeroAdjX[2] = std::vector<double>{100.0, 50.0};
        zeroAdjY[0] = std::vector<double>{0.009, 0.004};
        zeroAdjY[1] = std::vector<double>{0.008, 0.003};
        zeroAdjY[2] = std::vector<double>{0.007, 0.002};
        calM[0] = std::vector<double>{0.000100, 0.000110};
        calM[1] = std::vector<double>{0.000080, 0.000090};
        calM[2] = std::vector<double>{0.000060, 0.000070};
        calC[0] = std::vector<double>{0.00100, 0.00110};
        calC[1] = std::vector<double>{0.00080, 0.00090};
        calC[2] = std::vector<double>{0.00060, 0.00070};
        calWall[0] = std::vector<double>{90, 91};
        calWall[1] = std::vector<double>{80, 81};
        calWall[2] = std::vector<double>{70, 71};

        Cd = 5;

        Tx = 25;
        T = 25;
        U = 10;
        P = 1001;

        F1 = 0;
        F2 = 0;

        id = "Ecotech Aurora  Nephelometer v1.20.000, ID #111444";

        enableFilterControl = true;
        filterMode = FilterMode::Kalman;
        normalizationTemperature = 0;

        garbleCounter = 0;
        busyCounter = 0;
        ignoreCounter = 0;
    }

    std::size_t angleIndex(double angle, std::size_t missing = static_cast<std::size_t>(-1)) const
    {
        auto check = std::find(angles.begin(), angles.end(), angle);
        if (check == angles.end())
            return missing;
        return check - angles.begin();
    }

    int majorState() const
    {
        switch (zeroState) {
        case ZeroState::FlushBefore:
        case ZeroState::Measure:
            return 5;
        default:
            return 0;
        }
        Q_ASSERT(false);
        return 0;
    }

    int minorState() const
    {
        switch (referenceShutter) {
        case ReferenceShutter::Up:
            return 3;
        case ReferenceShutter::MovingDown:
            return 0;
        case ReferenceShutter::Down:
            return 1;
        case ReferenceShutter::MovingUp:
            return 2;
        }
        Q_ASSERT(false);
        return 0;
    }

    static double VI(double input)
    { return std::floor(input * 1E5 + 0.5) / 1E5; }

    void respond(double input)
    {
        outgoing += QByteArray::number(VI(input), 'f', 5).rightJustified(9, ' ');
        outgoing += "\r\n";
    }

    void unpolledResponse()
    {
        outgoing += "09/06/2011 15:57:00,Instant set";
        for (const auto &color : Bs) {
            outgoing += ",";
            if (color.empty())
                continue;
            outgoing += QByteArray::number(VI(color.front()), 'f', 5);
        }
        auto bbsIndex = angleIndex(90);
        for (const auto &color : Bs) {
            if (bbsIndex == static_cast<std::size_t>(-1) || bbsIndex >= color.size())
                continue;
            outgoing += ",";
            outgoing += QByteArray::number(VI(color[bbsIndex]), 'f', 5);
        }
        outgoing += ",";
        outgoing += QByteArray::number(T, 'f', 2);
        outgoing += ",";
        outgoing += QByteArray::number(Tx, 'f', 2);
        outgoing += ",";
        outgoing += QByteArray::number(U, 'f', 2);
        outgoing += ",";
        outgoing += QByteArray::number(P, 'f', 2);
        outgoing += "\r\n";
    }

    void outputEEArray(const char *prefix,
                       const std::array<std::vector<double>, 3> &contents,
                       int decimals = 3)
    {
        for (std::size_t color = 0; color < contents.size(); ++color) {
            outgoing += prefix + QByteArray::number(static_cast<int>(color + 1)) + " =";
            for (auto add : contents[color]) {
                outgoing += ", ";
                outgoing += QByteArray::number(add, 'f', decimals);
            }
            outgoing += "\r\n";
        }
    }

    void advance(double seconds)
    {
        switch (referenceShutter) {
        case ReferenceShutter::Up:
            referenceShutterTime -= seconds;
            if (referenceShutterTime > 0.0)
                break;
            referenceShutter = ReferenceShutter::MovingDown;
            referenceShutterTime = 1.0;
            break;
        case ReferenceShutter::MovingDown:
            referenceShutterTime -= seconds;
            if (referenceShutterTime > 0.0)
                break;
            referenceShutter = ReferenceShutter::Down;
            referenceShutterTime = 3.0;
            break;
        case ReferenceShutter::Down:
            referenceShutterTime -= seconds;
            if (referenceShutterTime > 0.0)
                break;
            referenceShutter = ReferenceShutter::MovingUp;
            referenceShutterTime = 1.0;

            {
                double delta;
                if (referenceDeltaPolarity)
                    delta = 1.0;
                else
                    delta = -1.0;
                referenceDeltaPolarity = !referenceDeltaPolarity;
                for (auto &v : Cf) {
                    v += delta;
                }
            }

            break;
        case ReferenceShutter::MovingUp:
            referenceShutterTime -= seconds;
            if (referenceShutterTime > 0.0)
                break;
            referenceShutter = ReferenceShutter::Up;
            referenceShutterTime = 30.0;
            break;
        }

        switch (zeroState) {
        case ZeroState::Disabled:
            break;
        case ZeroState::FlushBefore:
            zeroStateTime -= seconds;
            if (zeroStateTime > 0.0)
                break;
            zeroState = ZeroState::Measure;
            zeroStateTime = 120;
            break;
        case ZeroState::Measure:
            zeroStateTime -= seconds;
            if (zeroStateTime > 0.0)
                break;
            zeroState = ZeroState::Disabled;
            break;
        }

        scatteringUpdateTime -= seconds;
        if (scatteringUpdateTime == 0.0) {
            scatteringUpdateTime = angles.size() * 1.5;
            double delta;
            if (scatteringDeltaPolarity)
                delta = 1.0;
            else
                delta = -1.0;
            scatteringDeltaPolarity = !scatteringDeltaPolarity;
            for (auto &color : Bs) {
                for (auto &v : color) {
                    v += delta;
                }
            }
        }

        if (FP::defined(unpolledRemaining)) {
            unpolledRemaining -= seconds;
            while (unpolledRemaining < 0.0) {
                unpolledRemaining += 5;
                unpolledResponse();
            }
        }


        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR).trimmed());
            incoming = incoming.mid(idxCR + 1);

            if (ignoreCounter) {
                --ignoreCounter;
                continue;
            }

            if (garbleCounter) {
                --garbleCounter;
                outgoing += "-434.4AASD343ad\r\n";
                continue;
            }

            if (busyCounter) {
                --busyCounter;
                outgoing += "\x15";
                continue;
            }

            if (line == "ID0") {
                outgoing += id;
                outgoing += "\r";
            } else if (line == "EE") {
                outgoing += "Temperature Unit    =, \xC2\xB0\x43\r\n";
                outgoing += "AtmPressureUnit     =, mb\r\n";
                if (FP::defined(normalizationTemperature)) {
                    outgoing += "Normalise to        =, " +
                            QByteArray::number(normalizationTemperature, 'f', 0) +
                            "\xC2\xB0\x43\r\n";
                } else {
                    outgoing += "Normalise to        =, \r\n";
                }
                switch (filterMode) {
                case FilterMode::None:
                    outgoing += "Filtering Method    =, None  \r\n";
                    break;
                case FilterMode::Kalman:
                    outgoing += "Filtering Method    =, Kalman\r\n";
                    break;
                case FilterMode::Average:
                    outgoing += "Filtering Method    =, Moving Average\r\n";
                    break;
                }
                outgoing += "Wavelength 1        =, 635nm \r\n";
                outgoing += "Wavelength 2        =, 525nm \r\n";
                outgoing += "Wavelength 3        =, 450nm \r\n";
                outgoing.append("Angle Count         =, " +
                                        QByteArray::number(static_cast<int>(angles.size())) +
                                        "\r");
                outgoing += "Angle List          =, ";
                for (std::size_t i = 0; i < angles.size(); i++) {
                    if (i != 0) outgoing += ",";
                    outgoing += QByteArray::number(angles[i], 'f', 0);
                }
                outgoing += "\r\n";
                outputEEArray("Calibration Ms    ", calM, 6);
                outputEEArray("Calibration Cs    ", calC, 6);
                outputEEArray("Calibration Walls ", calWall);
                outputEEArray("Cal ZeroAdj Xs    ", zeroAdjX);
                outputEEArray("Cal ZeroAdj Ys    ", zeroAdjY);
                outgoing += "Cal ZeroAdj Temp    =,  300.205\r\n";
                outgoing += "Cal ZeroAdj Pressure=,  827.394\r\n";
            } else if (line == "**0J5") {
                zeroState = ZeroState::FlushBefore;
                zeroStateTime = 30.0;
                outgoing += "OK\r\n";
            } else if (line.startsWith("DO000") && line.size() > 5) {
                outgoing += "OK\r\n";
                dospan = (line[5] == '1');
            } else if (line.startsWith("DO001") && line.size() > 5) {
                outgoing += "OK\r\n";
                dozero = (line[5] == '1');
            } else if (line.startsWith("**0PCF,") && line.size() > 7) {
                if (enableFilterControl) {
                    switch (line[7]) {
                    case 'K':
                        filterMode = FilterMode::Kalman;
                        outgoing += "OK\r\n";
                        break;
                    case 'N':
                        filterMode = FilterMode::None;
                        outgoing += "OK\r\n";
                        break;
                    case 'M':
                        filterMode = FilterMode::Average;
                        outgoing += "OK\r\n";
                        break;
                    default:
                        outgoing += "ERROR\r\n";
                        break;
                    }
                } else {
                    outgoing += "ERROR\r\n";
                }
            } else if (line.startsWith("**0PCSTP,") && line.size() > 9) {
                if (line[9] == 'N') {
                    normalizationTemperature = FP::undefined();
                    outgoing += "OK\r\n";
                } else {
                    bool ok = false;
                    normalizationTemperature = line.mid(9).toDouble(&ok);
                    if (!ok || !FP::defined(normalizationTemperature)) {
                        outgoing += "ERROR\r\n";
                    } else {
                        outgoing += "OK\r\n";
                    }
                }
            } else if (line.startsWith("**0S") || line.startsWith("**0PC") || line.startsWith("DO0")) {
                outgoing += "OK\r\n";
            } else if (line.startsWith("VI0") && line.length() == 5) {
                bool ok = false;
                int code = line.mid(3, 2).toInt(&ok);
                if (!ok) {
                    outgoing += "ERROR\r\n";
                    continue;
                }
                switch (code) {
                default:
                    outgoing += "ERROR\r\n";
                    break;

                case 0: {
                    outgoing += "  ";
                    outgoing += QByteArray::number(majorState()).rightJustified(2, '0');
                    outgoing += ".";
                    outgoing += QByteArray::number(minorState()).rightJustified(2, '0');
                    outgoing += "\r\n";
                    break;
                }

                case 99: {
                    outgoing += "21/11/2003 09:56:10";
                    for (std::size_t idx = 0; idx < angles.size(); idx++) {
                        for (const auto &color: Bs) {
                            outgoing += ", ";
                            if (idx >= color.size())
                                continue;
                            outgoing += QByteArray::number(color[idx], 'f', 3);
                        }
                    }

                    outgoing += ", ";
                    outgoing += QByteArray::number(T, 'f', 3);
                    outgoing += ", ";
                    outgoing += QByteArray::number(Tx, 'f', 3);
                    outgoing += ", ";
                    outgoing += QByteArray::number(U, 'f', 3);
                    outgoing += ", ";
                    outgoing += QByteArray::number(P, 'f', 3);
                    outgoing += ",";
                    outgoing += QByteArray::number(majorState()).rightJustified(2, '0');
                    outgoing += ",";
                    outgoing +=
                            QByteArray::number(static_cast<quint16>(F2), 16).rightJustified(2, '0');
                    outgoing += "\r\n";
                    break;
                }

                case 4:
                    respond(Cd);
                    break;

                case 6:
                    respond(Cf[0]);
                    break;
                case 7:
                    respond(Cs[0]);
                    break;
                case 8:
                    respond(Cs[0] / Cf[0]);
                    break;
                case 33:
                    respond(Cbs[0]);
                    break;
                case 34:
                    respond(Cbs[0] / Cf[0]);
                    break;

                case 9:
                    respond(Cf[1]);
                    break;
                case 10:
                    respond(Cs[1]);
                    break;
                case 11:
                    respond(Cs[1] / Cf[1]);
                    break;
                case 35:
                    respond(Cbs[1]);
                    break;
                case 36:
                    respond(Cbs[1] / Cf[1]);
                    break;

                case 12:
                    respond(Cf[2]);
                    break;
                case 13:
                    respond(Cs[2]);
                    break;
                case 14:
                    respond(Cs[2] / Cf[2]);
                    break;
                case 37:
                    respond(Cbs[2]);
                    break;
                case 38:
                    respond(Cbs[2] / Cf[2]);
                    break;

                case 88:
                    outgoing += " 0\r\n";
                    break;

                }
            } else if (line.startsWith("VI") && line.length() == 5) {
                bool ok = false;
                int color = line.mid(2, 1).toInt(&ok);
                if (!ok) {
                    outgoing.append("ERROR\r\n");
                    continue;
                }
                color -= 1;
                if (color < 0 || color > 3) {
                    outgoing.append("ERROR\r\n");
                    continue;
                }

                int angle = line.mid(3).toInt(&ok);
                if (!ok || angle < 0 || angle > 90) {
                    outgoing.append("ERROR\r\n");
                    continue;
                }
                const auto &source = Bs[color];

                auto index = angleIndex(angle);
                if (index == static_cast<std::size_t>(-1) || index >= source.size()) {
                    outgoing.append("-9999\r\n");
                    continue;
                }

                respond(source[index]);
            } else {
                outgoing.append("ERROR\r\n");
            }
        }
    }

    double calcBsw(int color, int angle) const
    {
        return calC[color][angle] / calM[color][angle];
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("F2", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("Tx", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("U", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("P", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("BsB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BsG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BsR", Variant::Root(635.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("Cd", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("CfB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CfG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CfR", Variant::Root(635.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CsB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CsG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CsR", Variant::Root(635.0), "^Wavelength", time))
            return false;

        if (!stream.hasMeta("Tw", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("Pw", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("BswB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BswG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BswR", Variant::Root(635.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("ZEE", Variant::Root(), {}, time))
            return false;

        if (!stream.hasMeta("ZSPANCHECK", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("PCTcB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("PCTcG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("PCTcR", Variant::Root(635.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CcB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CcG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CcR", Variant::Root(635.0), "^Wavelength", time))
            return false;

        return true;
    }

    bool checkMetaBack(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("BbsB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BbsG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BbsR", Variant::Root(635.0), "^Wavelength", time))
            return false;

        if (!stream.hasMeta("CbsB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CbsG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CbsR", Variant::Root(635.0), "^Wavelength", time))
            return false;

        if (!stream.hasMeta("BbswB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BbswG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BbswR", Variant::Root(635.0), "^Wavelength", time))
            return false;

        if (!stream.hasMeta("PCTbcB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("PCTbcG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("PCTbcR", Variant::Root(635.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CbcB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CbcG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CbcR", Variant::Root(635.0), "^Wavelength", time))
            return false;

        return true;
    }

    bool checkMetaPolar(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("Bn", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("BsnB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BsnG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BsnR", Variant::Root(635.0), "^Wavelength", time))
            return false;

        if (!stream.hasMeta("Bnw", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("BsnwB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BsnwG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BsnwR", Variant::Root(635.0), "^Wavelength", time))
            return false;

        if (!stream.hasMeta("Bnc", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("PCTncB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("PCTncG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("PCTncR", Variant::Root(635.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CncB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CncG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CncR", Variant::Root(635.0), "^Wavelength", time))
            return false;

        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZSTATE", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("ZSPANCHECKSTATE", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("ZBsB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("ZBsG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("ZBsR", Variant::Root(635.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BswdB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BswdG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BswdR", Variant::Root(635.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CrB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CrG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CrR", Variant::Root(635.0), "^Wavelength", time))
            return false;
        return true;
    }

    bool checkRealtimeMetaBack(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZBbsB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("ZBbsG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("ZBbsR", Variant::Root(635.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BbswdB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BbswdG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BbswdR", Variant::Root(635.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CbrB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CbrG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CbrR", Variant::Root(635.0), "^Wavelength", time))
            return false;
        return true;
    }

    bool checkRealtimeMetaPolar(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("BsnwdB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BsnwdG", Variant::Root(525.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BsnwdR", Variant::Root(635.0), "^Wavelength", time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream, bool polled = true)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("T"))
            return false;
        if (!stream.checkContiguous("Tx"))
            return false;
        if (!stream.checkContiguous("U"))
            return false;
        if (!stream.checkContiguous("BsB"))
            return false;
        if (!stream.checkContiguous("BsG"))
            return false;
        if (!stream.checkContiguous("BsR"))
            return false;
        if (!polled)
            return true;
        if (!stream.checkContiguous("Cd"))
            return false;
        if (!stream.checkContiguous("CfB"))
            return false;
        if (!stream.checkContiguous("CfG"))
            return false;
        if (!stream.checkContiguous("CfR"))
            return false;
        if (!stream.checkContiguous("CsB"))
            return false;
        if (!stream.checkContiguous("CsG"))
            return false;
        if (!stream.checkContiguous("CsR"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("T", Variant::Root(model.T), time))
            return false;
        if (!stream.hasAnyMatchingValue("Tx", Variant::Root(model.Tx), time))
            return false;
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.P), time))
            return false;
        if (!stream.hasAnyMatchingValue("U", Variant::Root(model.U), time))
            return false;

        auto index = model.angleIndex(0);
        if (index != static_cast<std::size_t>(-1)) {
            if (!stream.hasAnyMatchingValue("BsB", Variant::Root(model.VI(model.Bs[2][index])),
                                            time))
                return false;
            if (!stream.hasAnyMatchingValue("BsG", Variant::Root(model.VI(model.Bs[1][index])),
                                            time))
                return false;
            if (!stream.hasAnyMatchingValue("BsR", Variant::Root(model.VI(model.Bs[0][index])),
                                            time))
                return false;
        }

        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             double time = FP::undefined())
    {
        auto index = model.angleIndex(0);
        if (index != static_cast<std::size_t>(-1)) {
            if (!stream.hasAnyMatchingValue("ZBsB", Variant::Root(model.VI(model.Bs[2][index])),
                                            time))
                return false;
            if (!stream.hasAnyMatchingValue("ZBsG", Variant::Root(model.VI(model.Bs[1][index])),
                                            time))
                return false;
            if (!stream.hasAnyMatchingValue("ZBsR", Variant::Root(model.VI(model.Bs[0][index])),
                                            time))
                return false;
        }

        return true;
    }

    bool checkValuesBack(StreamCapture &stream,
                         const ModelInstrument &model,
                         double time = FP::undefined())
    {
        auto index = model.angleIndex(90);
        if (index == static_cast<std::size_t>(-1))
            return false;

        if (!stream.hasAnyMatchingValue("BbsB", Variant::Root(model.VI(model.Bs[2][index])), time))
            return false;
        if (!stream.hasAnyMatchingValue("BbsG", Variant::Root(model.VI(model.Bs[1][index])), time))
            return false;
        if (!stream.hasAnyMatchingValue("BbsR", Variant::Root(model.VI(model.Bs[0][index])), time))
            return false;
        return true;
    }

    bool checkRealtimeValuesBack(StreamCapture &stream,
                                 const ModelInstrument &model,
                                 double time = FP::undefined())
    {
        auto index = model.angleIndex(90);
        if (index == static_cast<std::size_t>(-1))
            return false;

        if (!stream.hasAnyMatchingValue("ZBbsB", Variant::Root(model.VI(model.Bs[2][index])), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBbsG", Variant::Root(model.VI(model.Bs[1][index])), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBbsR", Variant::Root(model.VI(model.Bs[0][index])), time))
            return false;
        return true;
    }

    bool checkPolar(StreamCapture &stream,
                    const ModelInstrument &model,
                    double time = FP::undefined())
    {
        {
            Variant::Write check = Variant::Write::empty();
            for (auto angle : model.angles) {
                check.toArray().after_back().setReal(angle);
            }
            if (!stream.hasAnyMatchingValue("Bn", check, time))
                return false;
        }
        {
            Variant::Write check = Variant::Write::empty();
            for (auto angle : model.Bs[2]) {
                check.toArray().after_back().setReal(angle);
            }
            if (!stream.hasAnyMatchingValue("BsnB", check, time))
                return false;
        }
        {
            Variant::Write check = Variant::Write::empty();
            for (auto angle : model.Bs[1]) {
                check.toArray().after_back().setReal(angle);
            }
            if (!stream.hasAnyMatchingValue("BsnG", check, time))
                return false;
        }
        {
            Variant::Write check = Variant::Write::empty();
            for (auto angle : model.Bs[0]) {
                check.toArray().after_back().setReal(angle);
            }
            if (!stream.hasAnyMatchingValue("BsnR", check, time))
                return false;
        }

        return true;
    }

    bool checkRealtimePolar(StreamCapture &stream,
                            const ModelInstrument &model,
                            double time = FP::undefined())
    {
        {
            Variant::Write check = Variant::Write::empty();
            for (auto angle : model.Bs[2]) {
                check.toArray().after_back().setReal(angle);
            }
            if (!stream.hasAnyMatchingValue("ZBsnB", check, time))
                return false;
        }
        {
            Variant::Write check = Variant::Write::empty();
            for (auto angle : model.Bs[1]) {
                check.toArray().after_back().setReal(angle);
            }
            if (!stream.hasAnyMatchingValue("ZBsnG", check, time))
                return false;
        }
        {
            Variant::Write check = Variant::Write::empty();
            for (auto angle : model.Bs[0]) {
                check.toArray().after_back().setReal(angle);
            }
            if (!stream.hasAnyMatchingValue("ZBsnR", check, time))
                return false;
        }

        return true;
    }

    bool checkCounts(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("Cd", Variant::Root(model.VI(model.Cd)), time))
            return false;
        if (!stream.hasAnyMatchingValue("CfB", Variant::Root(model.VI(model.Cf[2])), time))
            return false;
        if (!stream.hasAnyMatchingValue("CfG", Variant::Root(model.VI(model.Cf[1])), time))
            return false;
        if (!stream.hasAnyMatchingValue("CfR", Variant::Root(model.VI(model.Cf[0])), time))
            return false;

        if (!stream.hasAnyMatchingValue("CsB", Variant::Root(model.VI(model.Cs[2])), time))
            return false;
        if (!stream.hasAnyMatchingValue("CsG", Variant::Root(model.VI(model.Cs[1])), time))
            return false;
        if (!stream.hasAnyMatchingValue("CsR", Variant::Root(model.VI(model.Cs[0])), time))
            return false;

        return true;
    }

    bool checkCountsBack(StreamCapture &stream,
                         const ModelInstrument &model,
                         double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("CbsB", Variant::Root(model.VI(model.Cbs[2])), time))
            return false;
        if (!stream.hasAnyMatchingValue("CbsG", Variant::Root(model.VI(model.Cbs[1])), time))
            return false;
        if (!stream.hasAnyMatchingValue("CbsR", Variant::Root(model.VI(model.Cbs[0])), time))
            return false;

        return true;
    }

    bool checkRealtimeCounts(StreamCapture &stream,
                             const ModelInstrument &model,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("CrB", Variant::Root(model.VI(model.Cs[2] / model.Cf[2])),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("CrG", Variant::Root(model.VI(model.Cs[1] / model.Cf[1])),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("CrR", Variant::Root(model.VI(model.Cs[0] / model.Cf[0])),
                                        time))
            return false;

        return true;
    }

    bool checkRealtimeCountsBack(StreamCapture &stream,
                                 const ModelInstrument &model,
                                 double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("CbrB", Variant::Root(model.VI(model.Cbs[2] / model.Cf[2])),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("CbrG", Variant::Root(model.VI(model.Cbs[1] / model.Cf[1])),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("CbrR", Variant::Root(model.VI(model.Cbs[0] / model.Cf[0])),
                                        time))
            return false;

        return true;
    }

    bool checkSpancheck(const SequenceValue::Transfer &values)
    {
        if (!StreamCapture::findValue(values, "raw", "ZSPANCHECK", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "PCTcB", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "PCTcG", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "PCTcR", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "PCTbcB", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "PCTbcG", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "PCTbcR", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "CcB", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "CcG", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "CcR", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "CbcB", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "CbcG", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "CbcR", Variant::Root()))
            return false;
        return true;
    }

    bool checkZeroData(StreamCapture &stream,
                       const ModelInstrument &model,
                       const std::array<std::vector<double>, 3> &offsets = {
                               std::vector<double>{0, 0}, std::vector<double>{0, 0},
                               std::vector<double>{0, 0}},
                       double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("BswB", Variant::Root(model.calcBsw(2, 0) + offsets[2][0]),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("BswG", Variant::Root(model.calcBsw(1, 0) + offsets[1][0]),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("BswR", Variant::Root(model.calcBsw(0, 0) + offsets[0][0]),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("BbswB", Variant::Root(model.calcBsw(2, 1) + offsets[2][1]),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("BbswG", Variant::Root(model.calcBsw(1, 1) + offsets[1][1]),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("BbswR", Variant::Root(model.calcBsw(0, 1) + offsets[0][1]),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("Tw", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("Pw", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkZeroShift(StreamCapture &stream, double time = FP::undefined(), int multiple = 1)
    {
        if (!stream.hasAnyMatchingValue("BswdB", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("BswdG", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("BswdR", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("BbswdB", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("BbswdG", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("BbswdR", Variant::Root(), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_ecotech_nephaurora"));
        QVERIFY(component);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.25);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(0.5);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging, false));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMetaBack(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkMetaBack(realtime));
        QVERIFY(checkRealtimeMeta(realtime));
        QVERIFY(checkRealtimeMetaBack(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValuesBack(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkValuesBack(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
        QVERIFY(checkRealtimeValuesBack(realtime, instrument));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.normalizationTemperature = 25;
        instrument.filterMode = ModelInstrument::FilterMode::Kalman;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (realtime.hasAnyMatchingValue("ZEE"))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZEE"));

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (logging.hasAnyMatchingValue("BsG") && logging.hasAnyMatchingValue("CfG"))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("BsG"));
        QVERIFY(logging.hasAnyMatchingValue("CfG"));

        control.advance(0.1);
        QTest::qSleep(50);
        double time = control.time();

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (logging.hasAnyMatchingValue("BsG", Variant::Root(), time) &&
                    logging.hasAnyMatchingValue("CfG", Variant::Root(), time))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("BsG", Variant::Root(), time));
        QVERIFY(logging.hasAnyMatchingValue("CfG", Variant::Root(), time));

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QCOMPARE(instrument.normalizationTemperature, 0.0);
        QCOMPARE(instrument.filterMode, ModelInstrument::FilterMode::None);

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMetaBack(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkMetaBack(realtime));
        QVERIFY(checkRealtimeMeta(realtime));
        QVERIFY(checkRealtimeMetaBack(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValuesBack(logging, instrument));
        QVERIFY(checkCounts(logging, instrument));
        QVERIFY(checkCountsBack(logging, instrument));

        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkValuesBack(realtime, instrument));
        QVERIFY(checkCounts(realtime, instrument));
        QVERIFY(checkCountsBack(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
        QVERIFY(checkRealtimeValuesBack(realtime, instrument));
        QVERIFY(checkRealtimeCounts(realtime, instrument));
        QVERIFY(checkRealtimeCountsBack(realtime, instrument));

        QVERIFY(logging.hasAnyMatchingValue("F1", Variant::Root(Data::Variant::Flags{"STP"})));
    }

    void interactiveAutoprobePolar()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.id = "Ecotech Aurora 4000 Nephelometer v1.20.000, ID #111444";
        instrument.angles = std::vector<double>{0, 45, 90};
        instrument.Bs[0] = std::vector<double>{50.0, 10.0, 3.0};
        instrument.Bs[1] = std::vector<double>{60.0, 20.0, 6.0};
        instrument.Bs[2] = std::vector<double>{70.0, 30.0, 15.0};
        instrument.zeroAdjX[0] = std::vector<double>{200.0, 60.0, 30.0};
        instrument.zeroAdjX[1] = std::vector<double>{210.0, 90.0, 45.0};
        instrument.zeroAdjX[2] = std::vector<double>{220.0, 100.0, 50.0};
        instrument.zeroAdjY[0] = std::vector<double>{0.009, 0.004, 0.005};
        instrument.zeroAdjY[1] = std::vector<double>{0.008, 0.003, 0.006};
        instrument.zeroAdjY[2] = std::vector<double>{0.007, 0.002, 0.007};
        instrument.calM[0] = std::vector<double>{0.000100, 0.000110, 0.000111};
        instrument.calM[1] = std::vector<double>{0.000080, 0.000090, 0.000112};
        instrument.calM[2] = std::vector<double>{0.000060, 0.000070, 0.000113};
        instrument.calC[0] = std::vector<double>{0.00100, 0.00110, 0.00111};
        instrument.calC[1] = std::vector<double>{0.00080, 0.00090, 0.00112};
        instrument.calC[2] = std::vector<double>{0.00060, 0.00070, 0.00113};
        instrument.calWall[0] = std::vector<double>{87, 88, 89};
        instrument.calWall[1] = std::vector<double>{77, 78, 79};
        instrument.calWall[2] = std::vector<double>{67, 68, 69};
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (realtime.hasAnyMatchingValue("ZEE"))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZEE"));

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (logging.hasAnyMatchingValue("BsnG") && logging.hasAnyMatchingValue("CfG"))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("BsnG"));
        QVERIFY(logging.hasAnyMatchingValue("CfG"));

        control.advance(0.1);
        QTest::qSleep(50);
        double time = control.time();

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (logging.hasAnyMatchingValue("BsnG", Variant::Root(), time) &&
                    logging.hasAnyMatchingValue("CfG", Variant::Root(), time))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("BsnG", Variant::Root(), time));
        QVERIFY(logging.hasAnyMatchingValue("CfG", Variant::Root(), time));

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMetaBack(logging));
        QVERIFY(checkMetaPolar(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkMetaBack(realtime));
        QVERIFY(checkMetaPolar(realtime));
        QVERIFY(checkRealtimeMeta(realtime));
        QVERIFY(checkRealtimeMetaBack(realtime));
        QVERIFY(checkRealtimeMetaPolar(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValuesBack(logging, instrument));
        QVERIFY(checkPolar(logging, instrument));
        QVERIFY(checkCounts(logging, instrument));
        QVERIFY(checkCountsBack(logging, instrument));

        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkValuesBack(realtime, instrument));
        QVERIFY(checkPolar(realtime, instrument));
        QVERIFY(checkCounts(realtime, instrument));
        QVERIFY(checkCountsBack(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
        QVERIFY(checkRealtimeValuesBack(realtime, instrument));
        QVERIFY(checkRealtimePolar(realtime, instrument));
        QVERIFY(checkRealtimeCounts(realtime, instrument));
        QVERIFY(checkRealtimeCountsBack(realtime, instrument));
    }

    void interactiveAutoprobeTotal()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.id = "Ecotech Aurora 3000 Nephelometer v1.20.000, ID #111444";
        instrument.angles = std::vector<double>{0};
        instrument.Bs[0] = std::vector<double>{50.0};
        instrument.Bs[1] = std::vector<double>{60.0,};
        instrument.Bs[2] = std::vector<double>{70.0};
        instrument.zeroAdjX[0] = std::vector<double>{200.0};
        instrument.zeroAdjX[1] = std::vector<double>{210.0};
        instrument.zeroAdjX[2] = std::vector<double>{220.0};
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (realtime.hasAnyMatchingValue("ZEE"))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZEE"));

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (logging.hasAnyMatchingValue("BsG") && logging.hasAnyMatchingValue("CfG"))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("BsG"));
        QVERIFY(logging.hasAnyMatchingValue("CfG"));

        control.advance(0.1);
        QTest::qSleep(50);
        double time = control.time();

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (logging.hasAnyMatchingValue("BsG", Variant::Root(), time) &&
                    logging.hasAnyMatchingValue("CfG", Variant::Root(), time))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("BsG", Variant::Root(), time));
        QVERIFY(logging.hasAnyMatchingValue("CfG", Variant::Root(), time));

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(!checkMetaBack(logging));
        QVERIFY(!checkMetaPolar(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(!checkMetaBack(realtime));
        QVERIFY(!checkMetaPolar(realtime));
        QVERIFY(checkRealtimeMeta(realtime));
        QVERIFY(!checkRealtimeMetaBack(realtime));
        QVERIFY(!checkRealtimeMetaPolar(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(!checkValuesBack(logging, instrument));
        QVERIFY(!checkPolar(logging, instrument));
        QVERIFY(checkCounts(logging, instrument));
        QVERIFY(!checkCountsBack(logging, instrument));

        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(!checkValuesBack(realtime, instrument));
        QVERIFY(!checkPolar(realtime, instrument));
        QVERIFY(checkCounts(realtime, instrument));
        QVERIFY(!checkCountsBack(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
        QVERIFY(!checkRealtimeValuesBack(realtime, instrument));
        QVERIFY(!checkRealtimePolar(realtime, instrument));
        QVERIFY(checkRealtimeCounts(realtime, instrument));
        QVERIFY(!checkRealtimeCountsBack(realtime, instrument));
    }

    void interactiveAutoprobeNoFilterControl()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Zero/Mode"] = "Offset";
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.enableFilterControl = false;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (realtime.hasAnyMatchingValue("ZEE"))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZEE"));

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (logging.hasAnyMatchingValue("BsG") && logging.hasAnyMatchingValue("CfG"))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("BsG"));
        QVERIFY(logging.hasAnyMatchingValue("CfG"));

        control.advance(0.1);
        QTest::qSleep(50);
        double time = control.time();

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (logging.hasAnyMatchingValue("BsG", Variant::Root(), time) &&
                    logging.hasAnyMatchingValue("CfG", Variant::Root(), time))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("BsG", Variant::Root(), time));
        QVERIFY(logging.hasAnyMatchingValue("CfG", Variant::Root(), time));

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMetaBack(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkMetaBack(realtime));
        QVERIFY(checkRealtimeMeta(realtime));
        QVERIFY(checkRealtimeMetaBack(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValuesBack(logging, instrument));
        QVERIFY(checkCounts(logging, instrument));
        QVERIFY(checkCountsBack(logging, instrument));

        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkValuesBack(realtime, instrument));
        QVERIFY(checkCounts(realtime, instrument));
        QVERIFY(checkCountsBack(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
        QVERIFY(checkRealtimeValuesBack(realtime, instrument));
        QVERIFY(checkRealtimeCounts(realtime, instrument));
        QVERIFY(checkRealtimeCountsBack(realtime, instrument));
    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));
        control.advance(0.1);

        for (int i = 0; i < 50; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging, false));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMetaBack(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkMetaBack(realtime));
        QVERIFY(checkRealtimeMeta(realtime));
        QVERIFY(checkRealtimeMetaBack(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValuesBack(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkValuesBack(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
        QVERIFY(checkRealtimeValuesBack(realtime, instrument));
    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Zero/Mode"] = "Native";
        cv["NormalizationTemperature"] = FP::undefined();
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        instrument.filterMode = ModelInstrument::FilterMode::None;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (realtime.hasAnyMatchingValue("ZEE"))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZEE"));

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (logging.hasAnyMatchingValue("BsG") && logging.hasAnyMatchingValue("CfG"))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("BsG"));
        QVERIFY(logging.hasAnyMatchingValue("CfG"));

        control.advance(0.1);
        QTest::qSleep(50);
        double time = control.time();

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (logging.hasAnyMatchingValue("BsG", Variant::Root(), time) &&
                    logging.hasAnyMatchingValue("CfG", Variant::Root(), time))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("BsG", Variant::Root(), time));
        QVERIFY(logging.hasAnyMatchingValue("CfG", Variant::Root(), time));

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QCOMPARE(instrument.filterMode, ModelInstrument::FilterMode::Kalman);

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging, false));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMetaBack(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkMetaBack(realtime));
        QVERIFY(checkRealtimeMeta(realtime));
        QVERIFY(checkRealtimeMetaBack(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValuesBack(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkValuesBack(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
        QVERIFY(checkRealtimeValuesBack(realtime, instrument));

        QVERIFY(checkZeroData(persistent, instrument));
        QVERIFY(checkZeroData(realtime, instrument));

        QVERIFY(logging.hasAnyMatchingValue("F1", Variant::Root(Data::Variant::Flags())));
    }

    void interactiveRetry()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (realtime.hasAnyMatchingValue("ZEE"))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZEE"));

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (logging.hasAnyMatchingValue("BsG") && logging.hasAnyMatchingValue("CfG"))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("BsG"));
        QVERIFY(logging.hasAnyMatchingValue("CfG"));

        control.advance(0.1);
        QTest::qSleep(50);

        double time = control.time();
        instrument.busyCounter = 5;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (logging.hasAnyMatchingValue("BsG", Variant::Root(), time) &&
                    logging.hasAnyMatchingValue("CfG", Variant::Root(), time))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("BsG", Variant::Root(), time));
        QVERIFY(logging.hasAnyMatchingValue("CfG", Variant::Root(), time));

        control.advance(0.1);
        QTest::qSleep(50);

        time = control.time();
        instrument.garbleCounter = 1;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (logging.hasAnyMatchingValue("BsG", Variant::Root(), time) &&
                    logging.hasAnyMatchingValue("CfG", Variant::Root(), time))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("BsG", Variant::Root(), time));
        QVERIFY(logging.hasAnyMatchingValue("CfG", Variant::Root(), time));

        control.advance(0.1);
        QTest::qSleep(50);

        time = control.time();
        instrument.ignoreCounter = 2;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (logging.hasAnyMatchingValue("BsG", Variant::Root(), time) &&
                    logging.hasAnyMatchingValue("CfG", Variant::Root(), time))
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("BsG", Variant::Root(), time));
        QVERIFY(logging.hasAnyMatchingValue("CfG", Variant::Root(), time));

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging, false));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMetaBack(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkMetaBack(realtime));
        QVERIFY(checkRealtimeMeta(realtime));
        QVERIFY(checkRealtimeMetaBack(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValuesBack(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkValuesBack(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
        QVERIFY(checkRealtimeValuesBack(realtime, instrument));
    }

    void interactiveSpancheck()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Spancheck/Gas/MinimumSample"].setDouble(60.0);
        cv["Spancheck/Air/MinimumSample"].setDouble(60.0);
        cv["Spancheck/Gas/Flush"].setDouble(10.0);
        cv["Spancheck/Air/Flush"].setDouble(10.0);
        cv["Parameters/STZ"].setInt64(30);
        cv["Parameters/STB"].setInt64(15);
        cv["Zero/Mode"] = "Native";
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));
        QVERIFY(!instrument.dospan);
        QVERIFY(!instrument.dozero);

        while (control.time() < 60.0) {
            if (logging.hasAnyMatchingValue("BsB", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        while (control.time() < 60.0) {
            if (logging.hasAnyMatchingValue("F1", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        QVERIFY(logging.hasAnyMatchingValue("BsB", Variant::Root(), FP::undefined()));

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkZeroData(persistent, instrument));
        QVERIFY(checkZeroData(realtime, instrument));

        double checkTime = control.time();

        Variant::Write cmd = Variant::Write::empty();
        cmd["StartSpancheck"].setBool(true);
        interface->incomingCommand(cmd);

        while (control.time() < 240.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Spancheck"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Spancheck"), checkTime));
        QVERIFY(realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("GasAirFlush"),
                                             checkTime, "Current"));
        while (control.time() < 240.0) {
            if (instrument.dozero && !instrument.dospan)
                break;
            if (realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("GasSample"),
                                             checkTime, "Current"))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        QVERIFY(instrument.dozero);
        QVERIFY(!instrument.dospan);

        checkTime = control.time();
        while (control.time() < 360.0) {
            if (!instrument.dozero && instrument.dospan)
                break;
            if (realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("AirSample"),
                                             checkTime, "Current"))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        QVERIFY(!instrument.dozero);
        QVERIFY(instrument.dospan);

        while (control.time() < 360.0) {
            if (realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("AirSample"),
                                             checkTime, "Current"))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        while (control.time() < 360.0) {
            if (realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("Inactive"),
                                             checkTime, "Current"))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 360.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("Inactive"),
                                             checkTime, "Current"));

        while (control.time() < 420.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Zero"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 420.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Zero"), checkTime));
        /* Builtin zero mode, so this should be unset now */
        QVERIFY(!instrument.dozero);
        QVERIFY(!instrument.dospan);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(!checkSpancheck(persistent.values()));
        QVERIFY(checkSpancheck(persistentValues));
        QVERIFY(checkSpancheck(realtime.values()));
    }

    void interactiveSpancheckPolar()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Spancheck/Gas/MinimumSample"].setDouble(60.0);
        cv["Spancheck/Air/MinimumSample"].setDouble(60.0);
        cv["Spancheck/Gas/Flush"].setDouble(10.0);
        cv["Spancheck/Air/Flush"].setDouble(10.0);
        cv["Parameters/STZ"].setInt64(30);
        cv["Parameters/STB"].setInt64(15);
        cv["Zero/Mode"] = "Native";
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.id = "Ecotech Aurora 4000 Nephelometer v1.20.000, ID #111444";
        instrument.angles = std::vector<double>{0, 45, 90};
        instrument.Bs[0] = std::vector<double>{50.0, 10.0, 3.0};
        instrument.Bs[1] = std::vector<double>{60.0, 20.0, 6.0};
        instrument.Bs[2] = std::vector<double>{70.0, 30.0, 15.0};
        instrument.zeroAdjX[0] = std::vector<double>{200.0, 60.0, 30.0};
        instrument.zeroAdjX[1] = std::vector<double>{210.0, 90.0, 45.0};
        instrument.zeroAdjX[2] = std::vector<double>{220.0, 100.0, 50.0};
        instrument.zeroAdjY[0] = std::vector<double>{0.009, 0.004, 0.005};
        instrument.zeroAdjY[1] = std::vector<double>{0.008, 0.003, 0.006};
        instrument.zeroAdjY[2] = std::vector<double>{0.007, 0.002, 0.007};
        instrument.calM[0] = std::vector<double>{0.000100, 0.000110, 0.000111};
        instrument.calM[1] = std::vector<double>{0.000080, 0.000090, 0.000112};
        instrument.calM[2] = std::vector<double>{0.000060, 0.000070, 0.000113};
        instrument.calC[0] = std::vector<double>{0.00100, 0.00110, 0.00111};
        instrument.calC[1] = std::vector<double>{0.00080, 0.00090, 0.00112};
        instrument.calC[2] = std::vector<double>{0.00060, 0.00070, 0.00113};
        instrument.calWall[0] = std::vector<double>{87, 88, 89};
        instrument.calWall[1] = std::vector<double>{77, 78, 79};
        instrument.calWall[2] = std::vector<double>{67, 68, 69};
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));
        QVERIFY(!instrument.dospan);
        QVERIFY(!instrument.dozero);

        while (control.time() < 60.0) {
            if (logging.hasAnyMatchingValue("BsnG", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        while (control.time() < 60.0) {
            if (logging.hasAnyMatchingValue("F1", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        QVERIFY(logging.hasAnyMatchingValue("BsnG"));

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkZeroData(persistent, instrument));
        QVERIFY(checkZeroData(realtime, instrument));

        double checkTime = control.time();

        Variant::Write cmd = Variant::Write::empty();
        cmd["StartSpancheck"].setBool(true);
        interface->incomingCommand(cmd);

        while (control.time() < 240.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Spancheck"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Spancheck"), checkTime));
        QVERIFY(realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("GasAirFlush"),
                                             checkTime, "Current"));
        while (control.time() < 240.0) {
            if (instrument.dozero && !instrument.dospan)
                break;
            if (realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("GasSample"),
                                             checkTime, "Current"))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        QVERIFY(instrument.dozero);
        QVERIFY(!instrument.dospan);

        checkTime = control.time();
        while (control.time() < 360.0) {
            if (!instrument.dozero && instrument.dospan)
                break;
            if (realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("AirSample"),
                                             checkTime, "Current"))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        QVERIFY(!instrument.dozero);
        QVERIFY(instrument.dospan);

        while (control.time() < 360.0) {
            if (realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("AirSample"),
                                             checkTime, "Current"))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        while (control.time() < 360.0) {
            if (realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("Inactive"),
                                             checkTime, "Current"))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 360.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("Inactive"),
                                             checkTime, "Current"));

        while (control.time() < 420.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Zero"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 420.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Zero"), checkTime));
        /* Builtin zero mode, so this should be unset now */
        QVERIFY(!instrument.dozero);
        QVERIFY(!instrument.dospan);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(!checkSpancheck(persistent.values()));
        QVERIFY(checkSpancheck(persistentValues));
        QVERIFY(checkSpancheck(realtime.values()));
    }

    void interactiveOffsetZero()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Zero/Mode"] = "Offset";
        cv["Zero/Measure"] = 60.0;
        cv["Zero/Fill"] = 10.0;
        cv["BlankTime"] = 10.0;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));
        QVERIFY(!instrument.dospan);
        QVERIFY(!instrument.dozero);

        while (control.time() < 60.0) {
            if (logging.hasAnyMatchingValue("BsB", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        while (control.time() < 60.0) {
            if (logging.hasAnyMatchingValue("F1", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        while (control.time() < 60.0) {
            if (realtime.hasAnyMatchingValue("BswB", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        QVERIFY(!instrument.dozero);
        QVERIFY(!instrument.dospan);

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkZeroData(persistent, instrument));
        QVERIFY(checkZeroData(realtime, instrument));


        double checkTime = control.time();

        Variant::Write cmd = Variant::Write::empty();
        cmd["StartZero"].setBool(true);
        interface->incomingCommand(cmd);

        while (control.time() < 120.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Zero"), checkTime))
                break;
            QTest::qSleep(50);
        }
        while (control.time() < 120.0) {
            if (instrument.dozero)
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Zero"), checkTime));
        QVERIFY(instrument.dozero);
        QVERIFY(!instrument.dospan);

        checkTime = control.time();
        double zeroAfter = checkTime;
        while (control.time() < 240.0) {
            if (!instrument.dozero)
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        while (control.time() < 240.0) {
            if (realtime.hasAnyMatchingValue("BswB", Variant::Root(), checkTime))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        QVERIFY(!instrument.dozero);
        QVERIFY(!instrument.dospan);

        checkTime = control.time();
        while (control.time() < 300.0) {
            if (realtime.hasAnyMatchingValue("BsG", Variant::Root(0.0), checkTime))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }
        while (control.time() < 300.0) {
            if (realtime.hasAnyMatchingValue("BbsR", Variant::Root(0.0), checkTime))
                break;
            control.advance(0.1);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkZeroData(persistent, instrument, instrument.Bs, zeroAfter));
        QVERIFY(checkZeroData(realtime, instrument, instrument.Bs, zeroAfter));
        QVERIFY(checkZeroShift(realtime, zeroAfter));

        /* Offset equal to current scattering, so the result is zero */
        QVERIFY(realtime.hasAnyMatchingValue("BsG", Variant::Root(0.0), zeroAfter));
        QVERIFY(realtime.hasAnyMatchingValue("BbsR", Variant::Root(0.0), zeroAfter));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
