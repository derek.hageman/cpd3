/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cctype>
#include <QRegularExpression>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "core/csv.hxx"
#include "core/timeutils.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"
#include "algorithms/rayleigh.hxx"

#include "acquire_ecotech_nephaurora.hxx"


using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Smoothing;

static const std::array<std::string, 16>
        instrumentFlagTranslation{"BackscatterFault",         /* 0x0001 */
                                  "BackscatterDigitalFault",  /* 0x0002 */
                                  "ShutterFault",             /* 0x0004 */
                                  "LightSourceFault",         /* 0x0008 */
                                  "PressureFault",            /* 0x0010 */
                                  "EnclosureTemperatureFault",/* 0x0020 */
                                  "SampleTemperatureFault",   /* 0x0040 */
                                  "RHFault",                  /* 0x0080 */
                                  "PMTFault",                 /* 0x0100 */
                                  "WarmupFault",              /* 0x0200 */
                                  "BackscattterHighWarning",  /* 0x0400 */
                                  {},                         /* 0x0800 */
                                  {},                         /* 0x1000 */
                                  {},                         /* 0x2000 */
                                  {},                         /* 0x4000 */
                                  "SystemFault",              /* 0x8000 */
};

static const std::array<std::string, 3> colorCodes = {"B", "G", "R"};


AcquireEcotechNephAurora::Configuration::Configuration() : start(FP::undefined()),
                                                           end(FP::undefined()),
                                                           address(0),
                                                           inactiveDelay(FP::undefined()),
                                                           commandDelay(0.125),
                                                           retryDelay(2.0),
                                                           retryBusyTimeout(1800.0),
                                                           retryFailureMaximum(5),
                                                           opticalUpdateTimeout(FP::undefined()),
                                                           referenceUpdateTimeout(FP::undefined()),
                                                           wavelengths(),
                                                           blankUnit(Time::Second),
                                                           blankCount(62),
                                                           blankAlign(false),
                                                           zeroMode(Zero::Offset),
                                                           zeroMeasureUnit(Time::Second),
                                                           zeroMeasureCount(300),
                                                           zeroMeasureAlign(false),
                                                           zeroFillUnit(Time::Second),
                                                           zeroFillCount(240),
                                                           zeroFillAlign(false),
                                                           filterMode(Filter::Kalman),
                                                           normalizationTemperature(0)
{
    wavelengths.fill(FP::undefined());
}

AcquireEcotechNephAurora::Configuration::Configuration(const Configuration &other,
                                                       double s,
                                                       double e) : start(s),
                                                                   end(e),
                                                                   address(other.address),
                                                                   inactiveDelay(
                                                                           other.inactiveDelay),
                                                                   commandDelay(other.commandDelay),
                                                                   retryDelay(other.retryDelay),
                                                                   retryBusyTimeout(
                                                                           other.retryBusyTimeout),
                                                                   retryFailureMaximum(
                                                                           other.retryFailureMaximum),
                                                                   opticalUpdateTimeout(
                                                                           other.opticalUpdateTimeout),
                                                                   referenceUpdateTimeout(
                                                                           other.referenceUpdateTimeout),
                                                                   wavelengths(),
                                                                   blankUnit(other.blankUnit),
                                                                   blankCount(other.blankCount),
                                                                   blankAlign(other.blankAlign),
                                                                   zeroMode(other.zeroMode),
                                                                   zeroMeasureUnit(
                                                                           other.zeroMeasureUnit),
                                                                   zeroMeasureCount(
                                                                           other.zeroMeasureCount),
                                                                   zeroMeasureAlign(
                                                                           other.zeroMeasureAlign),
                                                                   zeroFillUnit(other.zeroFillUnit),
                                                                   zeroFillCount(
                                                                           other.zeroFillCount),
                                                                   zeroFillAlign(
                                                                           other.zeroFillAlign),
                                                                   filterMode(other.filterMode),
                                                                   normalizationTemperature(
                                                                           other.normalizationTemperature)
{
    std::copy(other.wavelengths.begin(), other.wavelengths.end(), wavelengths.begin());
}

void AcquireEcotechNephAurora::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Ecotech");
    instrumentMeta["Model"].setString("Aurora");

    lastRecordTime = FP::undefined();
    autoprobeValidRecords = 0;
    okCommandQueue.clear();

    spancheckDetails.setUnit(SequenceName({}, "raw", "ZSPANCHECK"));
    digitalState.setUnit(SequenceName({}, "raw", "F2"));

    reportedWavelengths[0] = 450;
    reportedWavelengths[1] = 525;
    reportedWavelengths[2] = 635;
    effectiveWavelengths[0] = 450;
    effectiveWavelengths[1] = 525;
    effectiveWavelengths[2] = 635;
    spancheckController->setWavelength(0, 450, "B");
    spancheckController->setWavelength(1, 525, "G");
    spancheckController->setWavelength(2, 635, "R");
    reportingTemperature = 0;

    values.majorState = -1;
    values.minorState = -1;
    angles.clear();

    opticalUpdate = OpticalUpdate::ReadPending;
    opticalUpdateTimeout = FP::undefined();
    referenceUpdate = ReferenceUpdate::ReadPending;
    referenceUpdateTimeout = FP::undefined();
    referenceColorsRead.fill(false);

    seenEEComponents.fill(false);
    readEEAbsoluteTimeout = FP::undefined();

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    realtimeStateUpdated = false;

    inconsistentZero = false;

    lastEEResults = Variant::Root();
    persistentEEUpdated = false;
    realtimeEEUpdated = false;

    blankEndTime = FP::undefined();

    zeroT = ZeroAccumulator();
    zeroP = ZeroAccumulator();
    zeroBs.fill(std::map<double, ZeroAccumulator>());
    calM.fill(std::vector<double>());
    calC.fill(std::vector<double>());
    calWall.fill(std::vector<double>());
    zeroAdjX.fill(std::vector<double>());
    persistentZeroUpdated = false;
    realtimeZeroUpdated = false;
    zeroEffectiveTime = FP::undefined();
}

AcquireEcotechNephAurora::Configuration::Configuration(const ValueSegment &other,
                                                       double s,
                                                       double e) : start(s),
                                                                   end(e),
                                                                   address(0),
                                                                   inactiveDelay(FP::undefined()),
                                                                   commandDelay(0.125),
                                                                   retryDelay(2.0),
                                                                   retryBusyTimeout(1800.0),
                                                                   retryFailureMaximum(5),
                                                                   opticalUpdateTimeout(
                                                                           FP::undefined()),
                                                                   referenceUpdateTimeout(
                                                                           FP::undefined()),
                                                                   wavelengths(),
                                                                   blankUnit(Time::Second),
                                                                   blankCount(62),
                                                                   blankAlign(false),
                                                                   zeroMode(Zero::Offset),
                                                                   zeroMeasureUnit(Time::Second),
                                                                   zeroMeasureCount(300),
                                                                   zeroMeasureAlign(false),
                                                                   zeroFillUnit(Time::Second),
                                                                   zeroFillCount(240),
                                                                   zeroFillAlign(false),
                                                                   filterMode(Filter::Kalman),
                                                                   normalizationTemperature(0)
{
    wavelengths.fill(FP::undefined());
    setFromSegment(other);
}

AcquireEcotechNephAurora::Configuration::Configuration(const Configuration &under,
                                                       const ValueSegment &over,
                                                       double s,
                                                       double e) : start(s),
                                                                   end(e),
                                                                   address(under.address),
                                                                   inactiveDelay(
                                                                           under.inactiveDelay),
                                                                   commandDelay(under.commandDelay),
                                                                   retryDelay(under.retryDelay),
                                                                   retryBusyTimeout(
                                                                           under.retryBusyTimeout),
                                                                   retryFailureMaximum(
                                                                           under.retryFailureMaximum),
                                                                   opticalUpdateTimeout(
                                                                           under.opticalUpdateTimeout),
                                                                   referenceUpdateTimeout(
                                                                           under.referenceUpdateTimeout),
                                                                   wavelengths(),
                                                                   blankUnit(under.blankUnit),
                                                                   blankCount(under.blankCount),
                                                                   blankAlign(under.blankAlign),
                                                                   zeroMode(under.zeroMode),
                                                                   zeroMeasureUnit(
                                                                           under.zeroMeasureUnit),
                                                                   zeroMeasureCount(
                                                                           under.zeroMeasureCount),
                                                                   zeroMeasureAlign(
                                                                           under.zeroMeasureAlign),
                                                                   zeroFillUnit(under.zeroFillUnit),
                                                                   zeroFillCount(
                                                                           under.zeroFillCount),
                                                                   zeroFillAlign(
                                                                           under.zeroFillAlign),
                                                                   filterMode(under.filterMode),
                                                                   normalizationTemperature(
                                                                           under.normalizationTemperature)
{
    std::copy(under.wavelengths.begin(), under.wavelengths.end(), wavelengths.begin());
    setFromSegment(over);
}

void AcquireEcotechNephAurora::Configuration::setFromSegment(const ValueSegment &config)
{
    {
        auto check = config["Address"].toInteger();
        if (INTEGER::defined(check) && check >= 0 && check <= 6)
            address = static_cast<int>(check);
    }

    if (config["InterrogationDelay"].exists())
        inactiveDelay = config["InterrogationDelay"].toReal();
    if (config["BurstDelay"].exists())
        commandDelay = config["BurstDelay"].toReal();
    {
        auto check = config["Retry/Delay"].toReal();
        if (FP::defined(check) && check >= 0.0)
            retryDelay = check;
    }
    {
        auto check = config["Retry/BusyTimeout"].toReal();
        if (FP::defined(check) && check >= 0.0)
            retryBusyTimeout = check;
    }
    {
        auto check = config["Retry/MaximumFailures"].toInteger();
        if (INTEGER::defined(check) && check >= 0)
            retryFailureMaximum = static_cast<std::size_t>(check);
    }

    if (config["OpticalUpdateTimeout"].exists())
        opticalUpdateTimeout = config["OpticalUpdateTimeout"].toReal();
    if (config["ReferenceUpdateTimeout"].exists())
        referenceUpdateTimeout = config["ReferenceUpdateTimeout"].toReal();

    if (config["DataSmoothing"].exists()) {
        const auto &value = config["DataSmoothing"].toString();
        if (Util::equal_insensitive(value, "Kalman", "KalmanFilter")) {
            filterMode = Filter::Kalman;
        } else if (Util::equal_insensitive(value, "Average", "MovingAverage")) {
            filterMode = Filter::Average;
        } else {
            filterMode = Filter::None;
        }
    }
    if (config["NormalizationTemperature"].exists())
        normalizationTemperature = config["NormalizationTemperature"].toReal();

    if (FP::defined(config["Wavelength/B"].toDouble()))
        wavelengths[0] = config["Wavelength/B"].toDouble();
    if (FP::defined(config["Wavelength/G"].toDouble()))
        wavelengths[1] = config["Wavelength/G"].toDouble();
    if (FP::defined(config["Wavelength/R"].toDouble()))
        wavelengths[2] = config["Wavelength/R"].toDouble();

    if (config["BlankTime"].exists()) {
        blankUnit =
                Variant::Composite::toTimeInterval(config["BlankTime"], &blankCount, &blankAlign);
    }

    if (config["Zero/Mode"].exists()) {
        const auto &value = config["Zero/Mode"].toString();
        if (Util::equal_insensitive(value, "Native", "Instrument")) {
            zeroMode = Zero::Native;
        } else if (Util::equal_insensitive(value, "EnableFilter")) {
            zeroMode = Zero::EnableFilter;
        } else {
            zeroMode = Zero::Offset;
        }
    }
    if (config["Zero/Measure"].exists()) {
        zeroMeasureUnit =
                Variant::Composite::toTimeInterval(config["Zero/Measure"], &zeroMeasureCount,
                                                   &zeroMeasureAlign);
    }
    if (config["Zero/Fill"].exists()) {
        zeroFillUnit = Variant::Composite::toTimeInterval(config["Zero/Fill"], &zeroFillCount,
                                                          &zeroFillAlign);
    }
    if (config["Zero/EnableInstrumentCommands"].exists()) {
        for (auto add : config["Zero/EnableInstrumentCommands"].toHash()) {
            if (add.first.empty())
                continue;

            auto merge = zeroEnableCommands.find(add.first);
            if (merge == zeroEnableCommands.end()) {
                zeroEnableCommands.emplace(add.first, Variant::Root(add.second));
                continue;
            }

            merge->second = Variant::Root::overlay(merge->second, Variant::Root(add.second));
        }
    }
    if (config["Zero/EnableCommands"].exists()) {
        auto merge = zeroEnableCommands.find(std::string());
        if (merge == zeroEnableCommands.end()) {
            zeroEnableCommands.emplace(std::string(), Variant::Root(config["Zero/EnableCommands"]));
        } else {
            merge->second = Variant::Root::overlay(merge->second,
                                                   Variant::Root(config["Zero/EnableCommands"]));
        }
    }
    if (config["Zero/DisableInstrumentCommands"].exists()) {
        for (auto add : config["Zero/DisableInstrumentCommands"].toHash()) {
            if (add.first.empty())
                continue;

            auto merge = zeroDisableCommands.find(add.first);
            if (merge == zeroDisableCommands.end()) {
                zeroDisableCommands.emplace(add.first, Variant::Root(add.second));
                continue;
            }

            merge->second = Variant::Root::overlay(merge->second, Variant::Root(add.second));
        }
    }
    if (config["Zero/DisableCommands"].exists()) {
        auto merge = zeroDisableCommands.find(std::string());
        if (merge == zeroDisableCommands.end()) {
            zeroDisableCommands.emplace(std::string(),
                                        Variant::Root(config["Zero/DisableCommands"]));
        } else {
            merge->second = Variant::Root::overlay(merge->second,
                                                   Variant::Root(config["Zero/DisableCommands"]));
        }
    }
}

static Variant::Root defaultSpancheckConfiguration()
{
    Variant::Root result;
    result["Gas/Flush"].setDouble(480.0);
    result["Gas/MinimumSample"].setDouble(600.0);
    result["Air/Flush"].setDouble(480.0);
    result["Air/MinimumSample"].setDouble(600.0);
    return result;
}

AcquireEcotechNephAurora::AcquireEcotechNephAurora(const ValueSegment::Transfer &configData,
                                                   const std::string &loggingContext)
        : FramedInstrument("aurora", loggingContext),
          lastRecordTime(FP::undefined()),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          autoprobeValidRecords(0),
          responseState(ResponseState::Passive_Initialize),
          commandRetryFailureCounter(0),
          commandRetryAbsoluteTimeout(FP::undefined()),
          commandState(CommandState::SystemState),
          okCommandQueue(),
          sampleState(SampleState::Measure),
          stateEndTime(FP::undefined()),
          zeroReadPending(true),
          features(),
          spancheckInterface(*this),
          loggingMux(static_cast<std::size_t>(LogStream::TOTAL))
{
    features.fill(false);
    streamBeginTime.fill(FP::undefined());

    spancheckController.reset(
            new NephelometerSpancheckController(ValueSegment::withPath(configData, "Spancheck"),
                                                defaultSpancheckConfiguration(),
                                                NephelometerSpancheckController::EcotechAuroraCalibrations |
                                                        NephelometerSpancheckController::PartialCounts));
    spancheckController->setInterface(&spancheckInterface);

    setDefaultInvalid();
    config.emplace_back();

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireEcotechNephAurora::setToAutoprobe()
{
    responseState = ResponseState::Autoprobe_Passive_Initialize;
}

void AcquireEcotechNephAurora::setToInteractive()
{ responseState = ResponseState::Interactive_Initialize; }


ComponentOptions AcquireEcotechNephAuroraComponent::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireEcotechNephAurora::AcquireEcotechNephAurora(const ComponentOptions &,
                                                   const std::string &loggingContext)
        : FramedInstrument("aurora", loggingContext),
          lastRecordTime(FP::undefined()),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          autoprobeValidRecords(0),
          responseState(ResponseState::Passive_Initialize),
          commandRetryFailureCounter(0),
          commandRetryAbsoluteTimeout(FP::undefined()),
          commandState(CommandState::SystemState),
          okCommandQueue(),
          sampleState(SampleState::Measure),
          stateEndTime(FP::undefined()),
          zeroReadPending(true),
          features(),
          spancheckInterface(*this),
          loggingMux(static_cast<std::size_t>(LogStream::TOTAL))
{
    features.fill(false);
    streamBeginTime.fill(FP::undefined());

    spancheckController.reset(new NephelometerSpancheckController(ValueSegment::Transfer(),
                                                                  defaultSpancheckConfiguration(),
                                                                  NephelometerSpancheckController::EcotechAuroraCalibrations));
    spancheckController->setInterface(&spancheckInterface);

    setDefaultInvalid();
    config.emplace_back();
}

AcquireEcotechNephAurora::~AcquireEcotechNephAurora() = default;

void AcquireEcotechNephAurora::invalidateLogValues(double frameTime)
{
    lastRecordTime = FP::undefined();

    for (std::size_t i = 0; i < static_cast<std::size_t>(LogStream::TOTAL); i++) {
        if (loggingEgress)
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    loggingMux.clear();
    streamBeginTime.fill(FP::undefined());

    loggingLost(frameTime);
}

enum {
    RealtimePage_Main = 0,
    RealtimePage_Counts,
    RealtimePage_Spancheck,
    RealtimePage_PolarScattering,
    RealtimePage_PolarZero,
    RealtimePage_PolarSpancheck,
};

SequenceValue::Transfer AcquireEcotechNephAurora::buildLogMeta(double time) const
{
    Q_ASSERT(!config.empty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_ecotech_nephaurora");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    if (instrumentMeta["SerialNumber"].exists())
        processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    if (instrumentMeta["Model"].exists())
        processing["Model"].set(instrumentMeta["Model"]);

    bool haveBackscatter = false;
    if (!angles.empty())
        haveBackscatter = std::fabs(angles.back() - 90) < 5;

    static constexpr auto
            maskGeneralParameters = (1 << RealtimePage_Main) | (1 << RealtimePage_Counts);

    result.emplace_back(SequenceName({}, "raw_meta", "T"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("PageMask")
          .setInteger(maskGeneralParameters);

    result.emplace_back(SequenceName({}, "raw_meta", "Tx"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Cell temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Cell"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("PageMask")
          .setInteger(maskGeneralParameters);

    result.emplace_back(SequenceName({}, "raw_meta", "U"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Sample RH");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("PageMask")
          .setInteger(maskGeneralParameters);

    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("PageMask")
          .setInteger(maskGeneralParameters);


    result.emplace_back(SequenceName({}, "raw_meta", "BsB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[0]);
    if (FP::defined(reportingTemperature)) {
        result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "BsG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[1]);
    if (FP::defined(reportingTemperature)) {
        result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "BsR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[2]);
    if (FP::defined(reportingTemperature)) {
        result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);


    if (haveBackscatter) {
        result.emplace_back(SequenceName({}, "raw_meta", "BbsB"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[0]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataReal("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Aerosol light backwards-hemispheric scattering coefficient");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

        result.emplace_back(SequenceName({}, "raw_meta", "BbsG"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[1]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataReal("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Aerosol light backwards-hemispheric scattering coefficient");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

        result.emplace_back(SequenceName({}, "raw_meta", "BbsR"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[2]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataReal("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Aerosol light backwards-hemispheric scattering coefficient");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    }


    result.emplace_back(SequenceName({}, "raw_meta", "Cd"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Dark count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Dark"));
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(1);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("PageMask")
          .setInteger(maskGeneralParameters);

    result.emplace_back(SequenceName({}, "raw_meta", "CfB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[0]);
    result.back().write().metadataReal("Format").setString("00000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Reference shutter count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Counts);
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("CRef"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(1);

    result.emplace_back(SequenceName({}, "raw_meta", "CfG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[1]);
    result.back().write().metadataReal("Format").setString("00000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Reference shutter count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Counts);
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("CRef"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(1);

    result.emplace_back(SequenceName({}, "raw_meta", "CfR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[2]);
    result.back().write().metadataReal("Format").setString("00000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Reference shutter count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Counts);
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("CRef"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(1);


    result.emplace_back(SequenceName({}, "raw_meta", "CsB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[0]);
    result.back().write().metadataReal("Format").setString("00000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Measurement count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Counts);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CMeasTS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(2);

    result.emplace_back(SequenceName({}, "raw_meta", "CsG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[1]);
    result.back().write().metadataReal("Format").setString("00000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Measurement count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Counts);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CMeasTS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(2);

    result.emplace_back(SequenceName({}, "raw_meta", "CsR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[2]);
    result.back().write().metadataReal("Format").setString("00000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Measurement count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Counts);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CMeasTS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(2);


    if (haveBackscatter) {
        result.emplace_back(SequenceName({}, "raw_meta", "CbsB"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[0]);
        result.back().write().metadataReal("Format").setString("00000000");
        result.back().write().metadataReal("Units").setString("Hz");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Measurement backwards-hemispheric count rate");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Counts);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("CMeasBS"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Blue"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
        result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(2);

        result.emplace_back(SequenceName({}, "raw_meta", "CbsG"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[1]);
        result.back().write().metadataReal("Format").setString("00000000");
        result.back().write().metadataReal("Units").setString("Hz");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Measurement backwards-hemispheric count rate");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Counts);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("CMeasBS"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Green"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
        result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(2);

        result.emplace_back(SequenceName({}, "raw_meta", "CbsR"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[2]);
        result.back().write().metadataReal("Format").setString("00000000");
        result.back().write().metadataReal("Units").setString("Hz");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Measurement backwards-hemispheric count rate");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Counts);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("CMeasBS"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Red"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
        result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(2);
    }


    result.emplace_back(SequenceName({}, "raw_meta", "Tw"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature during zero");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "Pw"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure during zero");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "BswB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[0]);
    if (FP::defined(reportingTemperature)) {
        result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient from wall signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BspBK"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "BswG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[1]);
    if (FP::defined(reportingTemperature)) {
        result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient from wall signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BspBK"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "BswR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[2]);
    if (FP::defined(reportingTemperature)) {
        result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient from wall signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BspBK"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);


    if (haveBackscatter) {
        result.emplace_back(SequenceName({}, "raw_meta", "BbswB"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[0]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataReal("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataReal("Description")
              .setString(
                      "Aerosol light backwards-hemispheric scattering coefficient from wall signal");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("BbspBK"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Blue"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(3);
        result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
        result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "BbswG"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[1]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataReal("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataReal("Description")
              .setString(
                      "Aerosol light backwards-hemispheric scattering coefficient from wall signal");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("BbspBK"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Green"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(3);
        result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
        result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "BbswR"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[2]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataReal("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataReal("Description")
              .setString(
                      "Aerosol light backwards-hemispheric scattering coefficient from wall signal");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("BbspBK"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Red"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(3);
        result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
        result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    }

    if (hasFeature(Feature::Polar)) {
        result.emplace_back(SequenceName({}, "raw_meta", "Bn"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataArray("Count").setInteger(angles.size());
        result.back().write().metadataArray("Children").metadataReal("Format").setString("00");
        result.back().write().metadataArray("Units").setString("degrees");
        result.back().write().metadataArray("Description").setString("Measurement start angle");
        result.back().write().metadataArray("Source").set(instrumentMeta);
        result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
        result.back().write().metadataArray("Smoothing").hash("Mode").setString("None");
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarScattering);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Box")
              .setString(QObject::tr("Angular Data"));
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Name")
              .setString(QObject::tr("Angle"));
        result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInteger(1);

        result.emplace_back(SequenceName({}, "raw_meta", "BsnB"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataArray("Count").setInteger(angles.size());
        result.back().write().metadataArray("Wavelength").setDouble(effectiveWavelengths[0]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataArray("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataArray("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataArray("Children").metadataReal("Format").setString("0000.00");
        result.back().write().metadataArray("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataArray("Description")
              .setString("Aerosol angular light scattering coefficient");
        result.back().write().metadataArray("Source").set(instrumentMeta);
        result.back().write().metadataArray("Processing").toArray().after_back().set(processing);

        result.emplace_back(SequenceName({}, "raw_meta", "BsnG"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataArray("Count").setInteger(angles.size());
        result.back().write().metadataArray("Wavelength").setDouble(effectiveWavelengths[1]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataArray("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataArray("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataArray("Children").metadataReal("Format").setString("0000.00");
        result.back().write().metadataArray("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataArray("Description")
              .setString("Aerosol angular light scattering coefficient");
        result.back().write().metadataArray("Source").set(instrumentMeta);
        result.back().write().metadataArray("Processing").toArray().after_back().set(processing);

        result.emplace_back(SequenceName({}, "raw_meta", "BsnR"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataArray("Count").setInteger(angles.size());
        result.back().write().metadataArray("Wavelength").setDouble(effectiveWavelengths[2]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataArray("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataArray("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataArray("Children").metadataReal("Format").setString("0000.00");
        result.back().write().metadataArray("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataArray("Description")
              .setString("Aerosol angular light scattering coefficient");
        result.back().write().metadataArray("Source").set(instrumentMeta);
        result.back().write().metadataArray("Processing").toArray().after_back().set(processing);


        result.emplace_back(SequenceName({}, "raw_meta", "Bnw"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataArray("Count").setInteger(angles.size());
        result.back().write().metadataArray("Children").metadataReal("Format").setString("00");
        result.back().write().metadataArray("Units").setString("degrees");
        result.back()
              .write().metadataArray("Description").setString("Zero measurement start angle");
        result.back().write().metadataArray("Source").set(instrumentMeta);
        result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
        result.back().write().metadataArray("Smoothing").hash("Mode").setString("None");
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarZero);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Box")
              .setString(QObject::tr("Zero Results"));
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Name")
              .setString(QObject::tr("Angle"));
        result.back().write().metadataArray("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "BsnwB"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataArray("Count").setInteger(angles.size());
        result.back().write().metadataArray("Wavelength").setDouble(effectiveWavelengths[0]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataArray("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataArray("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataArray("Children").metadataReal("Format").setString("0000.00");
        result.back().write().metadataArray("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataArray("Description")
              .setString("Aerosol angular light scattering coefficient from wall signal");
        result.back().write().metadataArray("Source").set(instrumentMeta);
        result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
        result.back().write().metadataArray("Smoothing").hash("Mode").setString("None");
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarZero);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Box")
              .setString(QObject::tr("Zero Results"));
        result.back().write().metadataArray("Realtime").hash("Name").setString(QObject::tr("Blue"));
        result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadataArray("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "BsnwG"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataArray("Count").setInteger(angles.size());
        result.back().write().metadataArray("Wavelength").setDouble(effectiveWavelengths[1]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataArray("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataArray("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataArray("Children").metadataReal("Format").setString("0000.00");
        result.back().write().metadataArray("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataArray("Description")
              .setString("Aerosol angular light scattering coefficient from wall signal");
        result.back().write().metadataArray("Source").set(instrumentMeta);
        result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
        result.back().write().metadataArray("Smoothing").hash("Mode").setString("None");
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarZero);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Box")
              .setString(QObject::tr("Zero Results"));
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Name")
              .setString(QObject::tr("Green"));
        result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadataArray("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "BsnwR"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataArray("Count").setInteger(angles.size());
        result.back().write().metadataArray("Wavelength").setDouble(effectiveWavelengths[2]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataArray("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataArray("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataArray("Children").metadataReal("Format").setString("0000.00");
        result.back().write().metadataArray("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataArray("Description")
              .setString("Aerosol angular light scattering coefficient from wall signal");
        result.back().write().metadataArray("Source").set(instrumentMeta);
        result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
        result.back().write().metadataArray("Smoothing").hash("Mode").setString("None");
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarZero);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Box")
              .setString(QObject::tr("Zero Results"));
        result.back().write().metadataArray("Realtime").hash("Name").setString(QObject::tr("Red"));
        result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadataArray("Realtime").hash("Persistent").setBool(true);
    }


    result.emplace_back(SequenceName({}, "raw_meta", "ZEE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("EE command configuration data");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataHashChild("Lines")
          .metadataArray("Description")
          .setString("Raw response lines, in order");
    result.back()
          .write()
          .metadataHashChild("Lines")
          .metadataArray("Children")
          .metadataString("Description")
          .setString("EE response line");
    result.back()
          .write()
          .metadataHashChild("Values")
          .metadataHash("Description")
          .setString("EE command value pairs");
    result.back()
          .write()
          .metadataHashChild("Values")
          .metadataHashChild("")
          .metadataString("Description")
          .setString("EE command key value");
    result.back()
          .write()
          .metadataHashChild("Angles")
          .metadataArray("Description")
          .setString("Parsed polar sampling angles");
    result.back()
          .write()
          .metadataHashChild("Angles")
          .metadataArray("Children")
          .metadataReal("Description")
          .setString("Polar angle");
    result.back()
          .write()
          .metadataHashChild("Angles")
          .metadataArray("Children")
          .metadataReal("Format")
          .setString("00");
    result.back()
          .write()
          .metadataHashChild("Angles")
          .metadataArray("Children")
          .metadataReal("Units")
          .setString("degrees");
    result.back()
          .write()
          .metadataHashChild("TemperatureUnit")
          .metadataString("Description")
          .setString("Reported temperature unit");
    result.back()
          .write()
          .metadataHashChild("PressureUnit")
          .metadataString("Description")
          .setString("Reported pressure unit");
    result.back()
          .write()
          .metadataHashChild("NormalizationTemperature")
          .metadataReal("Description")
          .setString("Reporting temperature");
    result.back()
          .write()
          .metadataHashChild("NormalizationTemperature")
          .metadataReal("Format")
          .setString("00.0");
    result.back()
          .write()
          .metadataHashChild("NormalizationTemperature")
          .metadataReal("Units")
          .setString("\xC2\xB0\x43");
    result.back()
          .write()
          .metadataHashChild("Wavelengths")
          .metadataArray("Description")
          .setString("Parsed channel wavelengths");
    result.back()
          .write()
          .metadataHashChild("Wavelengths")
          .metadataArray("Children")
          .metadataReal("Description")
          .setString("Channel wavelength");
    result.back()
          .write()
          .metadataHashChild("Wavelengths")
          .metadataArray("Children")
          .metadataReal("Format")
          .setString("000");
    result.back()
          .write()
          .metadataHashChild("Wavelengths")
          .metadataArray("Children")
          .metadataReal("Units")
          .setString("nm");
    result.back()
          .write()
          .metadataHashChild("ZeroTemperature")
          .metadataReal("Description")
          .setString("Temperature during the zero measurement");
    result.back()
          .write()
          .metadataHashChild("ZeroTemperature")
          .metadataReal("Format")
          .setString("00.0");
    result.back()
          .write()
          .metadataHashChild("ZeroTemperature")
          .metadataReal("Units")
          .setString("\xC2\xB0\x43");
    result.back()
          .write()
          .metadataHashChild("ZeroPressure")
          .metadataReal("Description")
          .setString("Pressure during the zero measurement");
    result.back()
          .write()
          .metadataHashChild("ZeroPressure")
          .metadataReal("Format")
          .setString("0000.0");
    result.back().write().metadataHashChild("ZeroPressure").metadataReal("Units").setString("hPa");
    result.back()
          .write()
          .metadataHashChild("ZeroAdjX")
          .metadataArray("Description")
          .setString("Zero adjust X coordinates");
    result.back()
          .write()
          .metadataHashChild("ZeroAdjX")
          .metadataArray("Children")
          .metadataArray("Description")
          .setString("Channel coordinates");
    result.back()
          .write()
          .metadataHashChild("ZeroAdjX")
          .metadataArray("Children")
          .metadataArray("Children")
          .metadataReal("Description")
          .setString("Zero adjustment X coordinate");
    result.back()
          .write()
          .metadataHashChild("ZeroAdjX")
          .metadataArray("Children")
          .metadataArray("Children")
          .metadataReal("Format")
          .setString("00.0000");
    result.back()
          .write()
          .metadataHashChild("ZeroAdjX")
          .metadataArray("Children")
          .metadataArray("Children")
          .metadataReal("Units")
          .setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataHashChild("CalM")
          .metadataArray("Description")
          .setString("Calibration slopes");
    result.back()
          .write()
          .metadataHashChild("CalM")
          .metadataArray("Children")
          .metadataArray("Description")
          .setString("Channel slopes");
    result.back()
          .write()
          .metadataHashChild("CalM")
          .metadataArray("Children")
          .metadataArray("Children")
          .metadataReal("Description")
          .setString("Calibration slope");
    result.back()
          .write()
          .metadataHashChild("CalM")
          .metadataArray("Children")
          .metadataArray("Children")
          .metadataReal("Format")
          .setString("0.000E-0");
    result.back()
          .write()
          .metadataHashChild("CalM")
          .metadataArray("Children")
          .metadataArray("Children")
          .metadataReal("Units")
          .setString("Mm");
    result.back()
          .write()
          .metadataHashChild("CalC")
          .metadataArray("Description")
          .setString("Calibration intercepts");
    result.back()
          .write()
          .metadataHashChild("CalC")
          .metadataArray("Children")
          .metadataArray("Description")
          .setString("Channel intercepts");
    result.back()
          .write()
          .metadataHashChild("CalC")
          .metadataArray("Children")
          .metadataArray("Children")
          .metadataReal("Description")
          .setString("Calibration intercepts");
    result.back()
          .write()
          .metadataHashChild("CalC")
          .metadataArray("Children")
          .metadataArray("Children")
          .metadataReal("Format")
          .setString("0.000E-0");
    result.back()
          .write()
          .metadataHashChild("Wall")
          .metadataArray("Description")
          .setString("Calibration wall factor");
    result.back()
          .write()
          .metadataHashChild("Wall")
          .metadataArray("Children")
          .metadataArray("Description")
          .setString("Wall factors");
    result.back()
          .write()
          .metadataHashChild("Wall")
          .metadataArray("Children")
          .metadataArray("Children")
          .metadataReal("Description")
          .setString("Calibration wall factors");
    result.back()
          .write()
          .metadataHashChild("Wall")
          .metadataArray("Children")
          .metadataArray("Children")
          .metadataReal("Format")
          .setString("00.000");


    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("Blank")
          .hash("Description")
          .setString("Data removed in blank period");
    result.back().write().metadataSingleFlag("Blank").hash("Bits").setInteger(0x0000);
    result.back()
          .write()
          .metadataSingleFlag("Blank")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("Zero")
          .hash("Description")
          .setString("Zero in progress");
    result.back().write().metadataSingleFlag("Zero").hash("Bits").setInteger(0x20000000);
    result.back()
          .write()
          .metadataSingleFlag("Zero")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("Spancheck")
          .hash("Description")
          .setString("Spancheck in progress");
    result.back()
          .write()
          .metadataSingleFlag("Spancheck")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("Calibration")
          .hash("Description")
          .setString("Instrument calibration in progress");
    result.back()
          .write()
          .metadataSingleFlag("Calibration")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("STP")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");
    result.back().write().metadataSingleFlag("STP").hash("Bits").setInteger(0x0200);
    result.back()
          .write()
          .metadataSingleFlag("STP")
          .hash("Description")
          .setString("STP correction applied");

    result.back()
          .write()
          .metadataSingleFlag("InconsistentZero")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");
    result.back()
          .write()
          .metadataSingleFlag("InconsistentZero")
          .hash("Description")
          .setString("Inconsistent zeroing mode and data filter settings");

    result.back()
          .write()
          .metadataSingleFlag("InstrumentSpanCalibration")
          .hash("Description")
          .setString("Instrument controlled span calibration in progress");
    result.back()
          .write()
          .metadataSingleFlag("InstrumentSpanCalibartion")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("InstrumentZeroCalibration")
          .hash("Description")
          .setString("Instrument controlled zero calibration in progress");
    result.back()
          .write()
          .metadataSingleFlag("InstrumentZeroCalibration")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("InstrumentSpanCheck")
          .hash("Description")
          .setString("Instrument controlled span check in progress");
    result.back()
          .write()
          .metadataSingleFlag("InstrumentSpanCheck")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("InstrumentZeroCheck")
          .hash("Description")
          .setString("Instrument controlled zero check in progress");
    result.back()
          .write()
          .metadataSingleFlag("InstrumentZeroCheck")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("InstrumentZeroAdjust")
          .hash("Description")
          .setString("Instrument controlled zero adjust in progress");
    result.back()
          .write()
          .metadataSingleFlag("InstrumentZeroAdjust")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("InstrumentSystemCalibration")
          .hash("Description")
          .setString("Instrument controlled system (startup) calibration in progress");
    result.back()
          .write()
          .metadataSingleFlag("InstrumentSystemCalibration")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("InstrumentEnvironmentalCalibration")
          .hash("Description")
          .setString("Instrument controlled environmental calibration in progress");
    result.back()
          .write()
          .metadataSingleFlag("InstrumentEnvironmentalCalibration")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("BackscatterFault")
          .hash("Description")
          .setString("Instrument reporting a backscatter fault condition");
    result.back()
          .write()
          .metadataSingleFlag("BackscatterFault")
          .hash("Bits")
          .setInteger(0x00010000);
    result.back()
          .write()
          .metadataSingleFlag("BackscatterFault")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("BackscatterDigitalFault")
          .hash("Description")
          .setString("Instrument reporting a backscatter digital IO fault condition");
    result.back()
          .write()
          .metadataSingleFlag("BackscatterDigitalFault")
          .hash("Bits")
          .setInteger(0x00020000);
    result.back()
          .write()
          .metadataSingleFlag("BackscatterDigitalFault")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("ShutterFault")
          .hash("Description")
          .setString("Instrument reporting a shutter fault condition");
    result.back().write().metadataSingleFlag("ShutterFault").hash("Bits").setInteger(0x00040000);
    result.back()
          .write()
          .metadataSingleFlag("ShutterFault")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("LightSourceFault")
          .hash("Description")
          .setString("Instrument reporting a light source fault condition");
    result.back()
          .write()
          .metadataSingleFlag("LightSourceFault")
          .hash("Bits")
          .setInteger(0x00080000);
    result.back()
          .write()
          .metadataSingleFlag("LightSourceFault")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("PressureFault")
          .hash("Description")
          .setString("Instrument reporting a pressure sensor fault condition");
    result.back().write().metadataSingleFlag("PressureFault").hash("Bits").setInteger(0x00100000);
    result.back()
          .write()
          .metadataSingleFlag("PressureFault")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("EnclosureTemperatureFault")
          .hash("Description")
          .setString("Instrument reporting a enclosure temperature sensor fault condition");
    result.back()
          .write()
          .metadataSingleFlag("EnclosureTemperatureFault")
          .hash("Bits")
          .setInteger(0x00200000);
    result.back()
          .write()
          .metadataSingleFlag("EnclosureTemperatureFault")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureFault")
          .hash("Description")
          .setString("Instrument reporting a sample temperature sensor fault condition");
    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureFault")
          .hash("Bits")
          .setInteger(0x00400000);
    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureFault")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("RHFault")
          .hash("Description")
          .setString("Instrument reporting a RH sensor fault condition");
    result.back().write().metadataSingleFlag("RHFault").hash("Bits").setInteger(0x00800000);
    result.back()
          .write()
          .metadataSingleFlag("RHFault")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("PMTFault")
          .hash("Description")
          .setString("Instrument reporting a PMT fault condition");
    result.back().write().metadataSingleFlag("PMTFault").hash("Bits").setInteger(0x01000000);
    result.back()
          .write()
          .metadataSingleFlag("PMTFault")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("WarmupFault")
          .hash("Description")
          .setString("Instrument reporting a failure during warm up");
    result.back().write().metadataSingleFlag("WarmupFault").hash("Bits").setInteger(0x02000000);
    result.back()
          .write()
          .metadataSingleFlag("WarmupFault")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("BackscattterHighWarning")
          .hash("Description")
          .setString("Instrument reporting a high backscatter warning condition");
    result.back()
          .write()
          .metadataSingleFlag("BackscattterHighWarning")
          .hash("Bits")
          .setInteger(0x04000000);
    result.back()
          .write()
          .metadataSingleFlag("BackscattterHighWarning")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");

    result.back()
          .write()
          .metadataSingleFlag("SystemFault")
          .hash("Description")
          .setString("Instrument reporting a system fault condition");
    result.back().write().metadataSingleFlag("SystemFault").hash("Bits").setInteger(0x80000000);
    result.back()
          .write()
          .metadataSingleFlag("SystemFault")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ecotech_nephaurora");


    result.emplace_back(SequenceName({}, "raw_meta", "F2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataInteger("Format").setString("FF");
    result.back().write().metadataInteger("Description").setString("Instrument digital state");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataInteger("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataInteger("Realtime").hash("Hide").setBool(true);


    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECK"),
                        spancheckController->metadata(NephelometerSpancheckController::FullResults,
                                                      processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadata("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "PCTcB"), spancheckController->metadata(
            NephelometerSpancheckController::TotalPercentError, processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[0]);
    result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("Error"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Blue"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "PCTcG"), spancheckController->metadata(
            NephelometerSpancheckController::TotalPercentError, processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[1]);
    result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("Error"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Green"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "PCTcR"), spancheckController->metadata(
            NephelometerSpancheckController::TotalPercentError, processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[2]);
    result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("Error"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Red"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    if (haveBackscatter) {
        result.emplace_back(SequenceName({}, "raw_meta", "PCTbcB"), spancheckController->metadata(
                NephelometerSpancheckController::BackPercentError, processing), time,
                            FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[0]);
        result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Results"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("ErrorBK"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Blue"));
        result.back().write().metadata("Realtime").hash("RowOrder").setInteger(1);
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "PCTbcG"), spancheckController->metadata(
                NephelometerSpancheckController::BackPercentError, processing), time,
                            FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[1]);
        result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Results"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("ErrorBK"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Green"));
        result.back().write().metadata("Realtime").hash("RowOrder").setInteger(1);
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "PCTbcR"), spancheckController->metadata(
                NephelometerSpancheckController::BackPercentError, processing), time,
                            FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[2]);
        result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Results"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("ErrorBK"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Red"));
        result.back().write().metadata("Realtime").hash("RowOrder").setInteger(1);
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);
    }

    result.emplace_back(SequenceName({}, "raw_meta", "CcB"), spancheckController->metadata(
            NephelometerSpancheckController::TotalSensitivityFactor, processing), time,
                        FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[0]);
    result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("Sens"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Blue"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "CcG"), spancheckController->metadata(
            NephelometerSpancheckController::TotalSensitivityFactor, processing), time,
                        FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[1]);
    result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("Sens"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Green"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "CcR"), spancheckController->metadata(
            NephelometerSpancheckController::TotalSensitivityFactor, processing), time,
                        FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[2]);
    result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("Sens"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Red"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    if (haveBackscatter) {
        result.emplace_back(SequenceName({}, "raw_meta", "CbcB"), spancheckController->metadata(
                NephelometerSpancheckController::BackSensitivityFactor, processing), time,
                            FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[0]);
        result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Results"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("SensBK"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Blue"));
        result.back().write().metadata("Realtime").hash("RowOrder").setInteger(3);
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "CbcG"), spancheckController->metadata(
                NephelometerSpancheckController::BackSensitivityFactor, processing), time,
                            FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[1]);
        result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Results"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("SensBK"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Green"));
        result.back().write().metadata("Realtime").hash("RowOrder").setInteger(3);
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "CbcR"), spancheckController->metadata(
                NephelometerSpancheckController::BackSensitivityFactor, processing), time,
                            FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[2]);
        result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Results"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("SensBK"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Red"));
        result.back().write().metadata("Realtime").hash("RowOrder").setInteger(3);
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);
    }

    if (hasFeature(Feature::Polar)) {
        result.emplace_back(SequenceName({}, "raw_meta", "Bnc"),
                            spancheckController->metadata(NephelometerSpancheckController::Angles,
                                                          processing), time, FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarSpancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Angles"));
        result.back().write().metadata("Realtime").hash("Name").setString(QObject::tr("Angle"));
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "PCTncB"), spancheckController->metadata(
                NephelometerSpancheckController::PercentError, processing), time, FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[0]);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarSpancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Angles"));
        result.back().write().metadata("Realtime").hash("Name").setString(QObject::tr("ErrB"));
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "PCTncG"), spancheckController->metadata(
                NephelometerSpancheckController::PercentError, processing), time, FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[1]);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarSpancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Angles"));
        result.back().write().metadata("Realtime").hash("Name").setString(QObject::tr("ErrG"));
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "PCTncR"), spancheckController->metadata(
                NephelometerSpancheckController::PercentError, processing), time, FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[2]);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarSpancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Angles"));
        result.back().write().metadata("Realtime").hash("Name").setString(QObject::tr("ErrR"));
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "CncB"), spancheckController->metadata(
                NephelometerSpancheckController::SensitivityFactor, processing), time,
                            FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[0]);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarSpancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Angles"));
        result.back().write().metadata("Realtime").hash("Name").setString(QObject::tr("SensB"));
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "CncB"), spancheckController->metadata(
                NephelometerSpancheckController::SensitivityFactor, processing), time,
                            FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[0]);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarSpancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Angles"));
        result.back().write().metadata("Realtime").hash("Name").setString(QObject::tr("SensB"));
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "CncG"), spancheckController->metadata(
                NephelometerSpancheckController::SensitivityFactor, processing), time,
                            FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[1]);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarSpancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Angles"));
        result.back().write().metadata("Realtime").hash("Name").setString(QObject::tr("SensG"));
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "CncR"), spancheckController->metadata(
                NephelometerSpancheckController::SensitivityFactor, processing), time,
                            FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[2]);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarSpancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Angles"));
        result.back().write().metadata("Realtime").hash("Name").setString(QObject::tr("SensR"));
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);
    }

    if (hasFeature(Feature::Polar)) {
        Variant::Root Bn;
        auto target = Bn.write().toArray();
        for (auto angle : angles) {
            target.after_back().setReal(angle);
        }

        /* This isn't "really" metadata, but the metadata is always updated with it
         * so output it concurrently. */
        result.emplace_back(SequenceName({}, "raw", "Bn"), Bn, time, FP::undefined());
        result.emplace_back(SequenceName({}, "raw", "Bnw"), Bn, time, FP::undefined());
    }

    return result;
}

SequenceValue::Transfer AcquireEcotechNephAurora::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_ecotech_nephaurora");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    if (instrumentMeta["SerialNumber"].exists())
        processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    if (instrumentMeta["Model"].exists())
        processing["Model"].set(instrumentMeta["Model"]);

    bool haveBackscatter = false;
    if (!angles.empty())
        haveBackscatter = std::fabs(angles.back() - 90) < 5;

    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Name").setString(std::string());
    result.back().write().metadataString("Realtime").hash("Units").setString(std::string());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInteger(8);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Zero")
          .setString(QObject::tr("Zero in progress", "zero state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Spancheck")
          .setString(QObject::tr("Spancheck in progress", "spancheck state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Calibration")
          .setString(QObject::tr("Calibration in progress", "calibration state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Blank")
          .setString(QObject::tr("Blank in progress", "spancheck state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Warmup")
          .setString(QObject::tr("Warming up", "warmup state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidRecord")
          .setString(QObject::tr("NO COMMS: Invalid record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadStatus")
          .setString(QObject::tr("STARTING COMMS: Reading system status state"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetTime")
          .setString(QObject::tr("STARTING COMMS: Setting instrument time"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetFilter")
          .setString(QObject::tr("STARTING COMMS: Setting filter mode"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetNormalization")
          .setString(QObject::tr("STARTING COMMS: Setting normalization"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadEE")
          .setString(QObject::tr("STARTING COMMS: Reading EE parameters report"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveDisableSpan")
          .setString(QObject::tr("STARTING COMMS: Disabling span gas override"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveDisableZero")
          .setString(QObject::tr("STARTING COMMS: Disabling zero pump override"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveCheckFlags")
          .setString(QObject::tr("STARTING COMMS: Checking system flags response"));

    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BackscatterFault")
          .setString(QObject::tr("BACKSCATTER FAULT", "flag translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BackscatterDigitalFault")
          .setString(QObject::tr("BACKSCATTER DIGITAL FAULT", "flag translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("ShutterFault")
          .setString(QObject::tr("REFERENCE SHUTTER FAULT", "flag translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("LightSourceFault")
          .setString(QObject::tr("LED FAULT", "flag translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("PressureFault")
          .setString(QObject::tr("PRESSURE SENSOR FAULT", "flag translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("EnclosureTemperatureFault")
          .setString(QObject::tr("ENCLOSURE TEMP SENSOR FAULT", "flag translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SampleTemperatureFault")
          .setString(QObject::tr("SAMPLE TEMP SENSOR FAULT", "flag translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("RHFault")
          .setString(QObject::tr("RH SENSOR FAULT", "flag translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("PMTFault")
          .setString(QObject::tr("PMT FAULT", "flag translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("WarmupFault")
          .setString(QObject::tr("Warming up", "flag translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BackscattterHighWarning")
          .setString(QObject::tr("Backscatter too high", "flag translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SystemFault")
          .setString(QObject::tr("UNCLASSIFIED SYSTEM FAULT", "flag translation"));


    /* These exist so that the text mode clients show values during spanchecks
     * and zeros, but the plots and logged values do not. */
    result.emplace_back(SequenceName({}, "raw_meta", "ZBsB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[0]);
    if (FP::defined(reportingTemperature)) {
        result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("Bsp"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Main);

    result.emplace_back(SequenceName({}, "raw_meta", "ZBsG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[1]);
    if (FP::defined(reportingTemperature)) {
        result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("Bsp"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Main);

    result.emplace_back(SequenceName({}, "raw_meta", "ZBsR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[2]);
    if (FP::defined(reportingTemperature)) {
        result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("Bsp"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Main);


    if (haveBackscatter) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZBbsB"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[0]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataReal("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Aerosol light backwards-hemispheric scattering coefficient");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Bbsp"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Blue"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
        result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
        result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Main);

        result.emplace_back(SequenceName({}, "raw_meta", "ZBbsG"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[1]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataReal("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Aerosol light backwards-hemispheric scattering coefficient");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Bbsp"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Green"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
        result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
        result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Main);

        result.emplace_back(SequenceName({}, "raw_meta", "ZBbsR"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[2]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataReal("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Aerosol light backwards-hemispheric scattering coefficient");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Bbsp"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Red"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
        result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
        result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Main);
    }


    result.emplace_back(SequenceName({}, "raw_meta", "BswdB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[0]);
    if (FP::defined(reportingTemperature)) {
        result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient from wall signal shift");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BspBKShift"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(4);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Main);

    result.emplace_back(SequenceName({}, "raw_meta", "BswdG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[1]);
    if (FP::defined(reportingTemperature)) {
        result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient from wall signal shift");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BspBKShift"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(4);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Main);

    result.emplace_back(SequenceName({}, "raw_meta", "BswdR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[2]);
    if (FP::defined(reportingTemperature)) {
        result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient from wall signal shift");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BspBKShift"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(4);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Main);


    if (haveBackscatter) {
        result.emplace_back(SequenceName({}, "raw_meta", "BbswdB"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[0]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataReal("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataReal("Description")
              .setString(
                      "Aerosol light backwards-hemispheric scattering coefficient from wall signal shift");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("BbspBKShift"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Blue"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(5);
        result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Main);

        result.emplace_back(SequenceName({}, "raw_meta", "BbswdG"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[1]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataReal("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataReal("Description")
              .setString(
                      "Aerosol light backwards-hemispheric scattering coefficient from wall signal shift");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("BbspBKShift"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Green"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(5);
        result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Main);

        result.emplace_back(SequenceName({}, "raw_meta", "BbswdR"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[2]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataReal("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataReal("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataReal("Description")
              .setString(
                      "Aerosol light backwards-hemispheric scattering coefficient from wall signal shift");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("BbspBKShift"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Red"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(5);
        result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Main);
    }

    if (hasFeature(Feature::Polar)) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZBsnB"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataArray("Count").setInteger(angles.size());
        result.back().write().metadataArray("Wavelength").setDouble(effectiveWavelengths[0]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataArray("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataArray("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataArray("Children").metadataReal("Format").setString("0000.00");
        result.back().write().metadataArray("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataArray("Description")
              .setString("Aerosol angular light scattering coefficient");
        result.back().write().metadataArray("Source").set(instrumentMeta);
        result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarScattering);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Box")
              .setString(QObject::tr("Angular Data"));
        result.back().write().metadataArray("Realtime").hash("Name").setString(QObject::tr("Blue"));
        result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInteger(1);

        result.emplace_back(SequenceName({}, "raw_meta", "ZBsnG"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataArray("Count").setInteger(angles.size());
        result.back().write().metadataArray("Wavelength").setDouble(effectiveWavelengths[1]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataArray("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataArray("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataArray("Children").metadataReal("Format").setString("0000.00");
        result.back().write().metadataArray("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataArray("Description")
              .setString("Aerosol angular light scattering coefficient");
        result.back().write().metadataArray("Source").set(instrumentMeta);
        result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarScattering);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Box")
              .setString(QObject::tr("Angular Data"));
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Name")
              .setString(QObject::tr("Green"));
        result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInteger(1);

        result.emplace_back(SequenceName({}, "raw_meta", "ZBsnR"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataArray("Count").setInteger(angles.size());
        result.back().write().metadataArray("Wavelength").setDouble(effectiveWavelengths[2]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataArray("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataArray("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataArray("Children").metadataReal("Format").setString("0000.00");
        result.back().write().metadataArray("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataArray("Description")
              .setString("Aerosol angular light scattering coefficient");
        result.back().write().metadataArray("Source").set(instrumentMeta);
        result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarScattering);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Box")
              .setString(QObject::tr("Angular Data"));
        result.back().write().metadataArray("Realtime").hash("Name").setString(QObject::tr("Red"));
        result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInteger(1);
    }


    result.emplace_back(SequenceName({}, "raw_meta", "CrB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[0]);
    result.back().write().metadataReal("Format").setString("00.00000");
    result.back().write().metadataReal("Description").setString("Measurement ratio");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Counts);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CMeRaTS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(2);

    result.emplace_back(SequenceName({}, "raw_meta", "CrG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[1]);
    result.back().write().metadataReal("Format").setString("00.00000");
    result.back().write().metadataReal("Description").setString("Measurement ratio");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Counts);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CMeRaTS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(2);

    result.emplace_back(SequenceName({}, "raw_meta", "CrR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[2]);
    result.back().write().metadataReal("Format").setString("00.00000");
    result.back().write().metadataReal("Description").setString("Measurement ratio");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Counts);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CMeRaTS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(2);


    if (haveBackscatter) {
        result.emplace_back(SequenceName({}, "raw_meta", "CbrB"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[0]);
        result.back().write().metadataReal("Format").setString("00.00000");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Measurement backwards-hemispheric ratio");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Counts);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("CMeRaBS"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Blue"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(3);
        result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(2);

        result.emplace_back(SequenceName({}, "raw_meta", "CbrG"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[1]);
        result.back().write().metadataReal("Format").setString("00.00000");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Measurement backwards-hemispheric ratio");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Counts);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("CMeRaBS"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Green"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(3);
        result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(2);

        result.emplace_back(SequenceName({}, "raw_meta", "CbrR"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelengths[2]);
        result.back().write().metadataReal("Format").setString("00.00000");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Measurement backwards-hemispheric ratio");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("Page").setInteger(RealtimePage_Counts);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("CMeRaBS"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Red"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(3);
        result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(2);
    }


    if (hasFeature(Feature::Polar)) {
        result.emplace_back(SequenceName({}, "raw_meta", "BsnwdB"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataArray("Count").setInteger(angles.size());
        result.back().write().metadataArray("Wavelength").setDouble(effectiveWavelengths[0]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataArray("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataArray("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataArray("Children").metadataReal("Format").setString("0000.00");
        result.back().write().metadataArray("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataArray("Description")
              .setString("Aerosol angular light scattering coefficient from wall signal shift");
        result.back().write().metadataArray("Source").set(instrumentMeta);
        result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
        result.back().write().metadataArray("Smoothing").hash("Mode").setString("None");
        result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInteger(2);
        result.back().write().metadataArray("Realtime").hash("Persistent").setBool(true);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarZero);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Box")
              .setString(QObject::tr("Zero Results"));
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Name")
              .setString(QObject::tr("Blue Shift"));

        result.emplace_back(SequenceName({}, "raw_meta", "BsnwdG"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataArray("Count").setInteger(angles.size());
        result.back().write().metadataArray("Wavelength").setDouble(effectiveWavelengths[1]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataArray("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataArray("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataArray("Children").metadataReal("Format").setString("0000.00");
        result.back().write().metadataArray("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataArray("Description")
              .setString("Aerosol angular light scattering coefficient from wall signal shift");
        result.back().write().metadataArray("Source").set(instrumentMeta);
        result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
        result.back().write().metadataArray("Smoothing").hash("Mode").setString("None");
        result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInteger(2);
        result.back().write().metadataArray("Realtime").hash("Persistent").setBool(true);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarZero);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Box")
              .setString(QObject::tr("Zero Results"));
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Name")
              .setString(QObject::tr("Green Shift"));

        result.emplace_back(SequenceName({}, "raw_meta", "BsnwdR"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataArray("Count").setInteger(angles.size());
        result.back().write().metadataArray("Wavelength").setDouble(effectiveWavelengths[2]);
        if (FP::defined(reportingTemperature)) {
            result.back().write().metadataArray("ReportT").setDouble(reportingTemperature);
            result.back().write().metadataArray("ReportP").setDouble(1013.25);
        }
        result.back().write().metadataArray("Children").metadataReal("Format").setString("0000.00");
        result.back().write().metadataArray("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataArray("Description")
              .setString("Aerosol angular light scattering coefficient from wall signal shift");
        result.back().write().metadataArray("Source").set(instrumentMeta);
        result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
        result.back().write().metadataArray("Smoothing").hash("Mode").setString("None");
        result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInteger(2);
        result.back().write().metadataArray("Realtime").hash("Persistent").setBool(true);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarZero);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Box")
              .setString(QObject::tr("Zero Results"));
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Name")
              .setString(QObject::tr("Red Shift"));
    }


    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKMB"), spancheckController->metadata(
            NephelometerSpancheckController::TotalEcotechAuroraM, processing), time,
                        FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[0]);
    result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("CalM"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Blue"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInteger(4);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKMG"), spancheckController->metadata(
            NephelometerSpancheckController::TotalEcotechAuroraM, processing), time,
                        FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[1]);
    result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("CalM"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Green"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInteger(4);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKMR"), spancheckController->metadata(
            NephelometerSpancheckController::TotalEcotechAuroraM, processing), time,
                        FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[2]);
    result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("CalM"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Red"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInteger(4);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    if (haveBackscatter) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKMbB"),
                            spancheckController->metadata(
                                    NephelometerSpancheckController::BackEcotechAuroraM,
                                    processing), time, FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[0]);
        result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Results"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("CalMBK"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Blue"));
        result.back().write().metadata("Realtime").hash("RowOrder").setInteger(5);
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKMbG"),
                            spancheckController->metadata(
                                    NephelometerSpancheckController::BackEcotechAuroraM,
                                    processing), time, FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[1]);
        result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Results"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("CalMBK"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Green"));
        result.back().write().metadata("Realtime").hash("RowOrder").setInteger(5);
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKMbR"),
                            spancheckController->metadata(
                                    NephelometerSpancheckController::BackEcotechAuroraM,
                                    processing), time, FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[2]);
        result.back().write().metadata("Realtime").hash("Page").setInteger(RealtimePage_Spancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Results"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("CalMBK"));
        result.back()
              .write()
              .metadata("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Red"));
        result.back().write().metadata("Realtime").hash("RowOrder").setInteger(5);
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);
    }

    if (hasFeature(Feature::Polar)) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKMnB"),
                            spancheckController->metadata(
                                    NephelometerSpancheckController::EcotechAuroraMPolar,
                                    processing), time, FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[0]);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarSpancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Angles"));
        result.back().write().metadata("Realtime").hash("Name").setString(QObject::tr("MB"));
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKMnG"),
                            spancheckController->metadata(
                                    NephelometerSpancheckController::EcotechAuroraMPolar,
                                    processing), time, FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[1]);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarSpancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Angles"));
        result.back().write().metadata("Realtime").hash("Name").setString(QObject::tr("MG"));
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

        result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKMnR"),
                            spancheckController->metadata(
                                    NephelometerSpancheckController::EcotechAuroraMPolar,
                                    processing), time, FP::undefined());
        result.back().write().metadata("Source").set(instrumentMeta);
        result.back().write().metadata("Wavelength").setDouble(effectiveWavelengths[2]);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Page")
              .setInteger(RealtimePage_PolarSpancheck);
        result.back()
              .write()
              .metadata("Realtime")
              .hash("Box")
              .setString(QObject::tr("Spancheck Angles"));
        result.back().write().metadata("Realtime").hash("Name").setString(QObject::tr("MR"));
        result.back().write().metadata("Realtime").hash("NetworkPriority").setInteger(1);
        result.back().write().metadata("Realtime").hash("Persistent").setBool(true);
    }


    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKSTATE"),
                        spancheckController->metadata(
                                NephelometerSpancheckController::RealtimeState, processing), time,
                        FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);

    return result;
}

SequenceMatch::Composite AcquireEcotechNephAurora::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "F2");
    sel.append({}, {}, "Bnw?");
    sel.append({}, {}, "Tw");
    sel.append({}, {}, "Pw");
    sel.append({}, {}, "Bb?sn?wd?[BGR]");
    sel.append({}, {}, "C[bn]?c[BGR]");
    sel.append({}, {}, "PCTb?c[BGR]");
    sel.append({}, {}, "ZSPANCHECK.*");
    sel.append({}, {}, "ZSTATE");
    sel.append({}, {}, "ZEE");
    return sel;
}

Data::Variant::Root AcquireEcotechNephAurora::describeState() const
{
    Data::Variant::Root result;

    switch (responseState) {
    case ResponseState::Passive_Run:
        result["ResponseState"] = "Passive_Run";
        break;
    case ResponseState::Passive_Wait:
        result["ResponseState"] = "Passive_Wait";
        break;
    case ResponseState::Passive_Initialize:
        result["ResponseState"] = "Passive_Initialize";
        break;
    case ResponseState::Autoprobe_Passive_Wait:
        result["ResponseState"] = "Autoprobe_Passive_Wait";
        break;
    case ResponseState::Autoprobe_Passive_Initialize:
        result["ResponseState"] = "Autoprobe_Passive_Initialize";
        break;
    case ResponseState::Interactive_Start_ReadID:
        result["ResponseState"] = "Interactive_Start_ReadID";
        break;
    case ResponseState::Interactive_Start_ReadStatus:
        result["ResponseState"] = "Interactive_Start_ReadStatus";
        break;
    case ResponseState::Interactive_Start_SetTime:
        result["ResponseState"] = "Interactive_Start_SetTime";
        break;
    case ResponseState::Interactive_Start_SetFilter:
        result["ResponseState"] = "Interactive_Start_SetFilter";
        break;
    case ResponseState::Interactive_Start_SetNormalization:
        result["ResponseState"] = "Interactive_Start_SetNormalization";
        break;
    case ResponseState::Interactive_Start_ReadEE:
        result["ResponseState"] = "Interactive_Start_ReadEE";
        break;
    case ResponseState::Interactive_Start_CheckFlags:
        result["ResponseState"] = "Interactive_Start_CheckFlags";
        break;
    case ResponseState::Interactive_Start_DisableSpan:
        result["ResponseState"] = "Interactive_Start_DisableSpan";
        break;
    case ResponseState::Interactive_Start_DisableZero:
        result["ResponseState"] = "Interactive_Start_DisableZero";
        break;
    case ResponseState::Interactive_Restart_Wait:
        result["ResponseState"] = "Interactive_Restart_Wait";
        break;
    case ResponseState::Interactive_Initialize:
        result["ResponseState"] = "Interactive_Initialize";
        break;
    case ResponseState::Interactive_Run_Active:
        result["ResponseState"] = "Interactive_Run_Active";
        break;
    case ResponseState::Interactive_Run_Retry:
        result["ResponseState"] = "Interactive_Run_Retry";
        break;
    case ResponseState::Interactive_Run_Resume:
        result["ResponseState"] = "Interactive_Run_Resume";
        break;
    case ResponseState::Interactive_Run_Delay:
        result["ResponseState"] = "Interactive_Run_Delay";
        break;
    case ResponseState::Interactive_Zero_ReadEE:
        result["ResponseState"] = "Interactive_Zero_ReadEE";
        break;
    case ResponseState::Interactive_Zero_ReadEE_Retry:
        result["ResponseState"] = "Interactive_Zero_ReadEE_Retry";
        break;
    case ResponseState::Interactive_Zero_ReadEE_Resume:
        result["ResponseState"] = "Interactive_Zero_ReadEE_Resume";
        break;
    }

    switch (commandState) {
    case CommandState::SystemState:
        result["CommandState"] = "SystemState";
        break;
    case CommandState::DataLine:
        result["CommandState"] = "DataLine";
        break;
    case CommandState::Dark:
        result["CommandState"] = "Dark";
        break;
    case CommandState::Red_Reference:
        result["CommandState"] = "Red_Reference";
        break;
    case CommandState::Red_Sample:
        result["CommandState"] = "Red_Sample";
        break;
    case CommandState::Red_Ratio:
        result["CommandState"] = "Red_Ratio";
        break;
    case CommandState::Red_BackSample:
        result["CommandState"] = "Red_BackSample";
        break;
    case CommandState::Red_BackRatio:
        result["CommandState"] = "Red_BackRatio";
        break;
    case CommandState::Green_Reference:
        result["CommandState"] = "Green_Reference";
        break;
    case CommandState::Green_Sample:
        result["CommandState"] = "Green_Sample";
        break;
    case CommandState::Green_Ratio:
        result["CommandState"] = "Green_Ratio";
        break;
    case CommandState::Green_BackSample:
        result["CommandState"] = "Green_BackSample";
        break;
    case CommandState::Green_BackRatio:
        result["CommandState"] = "Green_BackRatio";
        break;
    case CommandState::Blue_Reference:
        result["CommandState"] = "Blue_Reference";
        break;
    case CommandState::Blue_Sample:
        result["CommandState"] = "Blue_Sample";
        break;
    case CommandState::Blue_Ratio:
        result["CommandState"] = "Blue_Ratio";
        break;
    case CommandState::Blue_BackSample:
        result["CommandState"] = "Blue_BackSample";
        break;
    case CommandState::Blue_BackRatio:
        result["CommandState"] = "Blue_BackRatio";
        break;
    case CommandState::StatusFlags:
        result["CommandState"] = "StatusFlags";
        break;
        break;
    }

    switch (sampleState) {
    case SampleState::Measure:
        result["SampleState"] = "Measure";
        break;
    case SampleState::ZeroAccumulateEnterBlank:
        result["SampleState"] = "ZeroAccumulateEnterBlank";
        result["SampleStateEndTime"].setReal(stateEndTime);
        break;
    case SampleState::ZeroAccumulate:
        result["SampleState"] = "ZeroAccumulate";
        result["SampleStateEndTime"].setReal(stateEndTime);
        break;
    case SampleState::ZeroInstrument:
        result["SampleState"] = "ZeroInstrument";
        break;
    case SampleState::BlankExitZero:
        result["SampleState"] = "BlankExitZero";
        result["SampleStateEndTime"].setReal(stateEndTime);
        break;
    case SampleState::Spancheck:
        result["SampleState"] = "Spancheck";
        break;
    case SampleState::InstrumentCalibration:
        result["SampleState"] = "Calibration";
        break;
    }

    if (FP::defined(blankEndTime)) {
        result["BlankEndTime"].setReal(blankEndTime);
    }

    result["InstrumentMajorState"].setInteger(values.majorState);
    result["InstrumentMinorState"].setInteger(values.minorState);

    switch (opticalUpdate) {
    case OpticalUpdate::Unchanged:
        result["OpticalUpdate"] = "Unchanged";
        break;
    case OpticalUpdate::ChangeDetected:
        result["OpticalUpdate"] = "ChangeDetected";
        break;
    case OpticalUpdate::ReadPending:
        result["OpticalUpdate"] = "ReadPending";
        break;
    }
    switch (referenceUpdate) {
    case ReferenceUpdate::ShutterOff:
        result["ReferenceUpdate"] = "ShutterOff";
        break;
    case ReferenceUpdate::ShutterOn:
        result["ReferenceUpdate"] = "ShutterOn";
        break;
    case ReferenceUpdate::ReadPending:
        result["ReferenceUpdate"] = "ReadPending";
        break;
    }

    if (hasFeature(Feature::Polar))
        result["Polar"].setBoolean(true);

    return result;
}

SequenceValue::Transfer AcquireEcotechNephAurora::constructZeroVariables(double time) const
{
    SequenceValue::Transfer result;

    if (Tw.read().exists()) {
        result.emplace_back(SequenceIdentity({{}, "raw", "Tw"}, time, FP::undefined()), Tw);
    }
    if (Pw.read().exists()) {
        result.emplace_back(SequenceIdentity({{}, "raw", "Pw"}, time, FP::undefined()), Pw);
    }

    for (std::size_t color = 0; color < effectiveBsw.size(); ++color) {
        if (effectiveBsw[color].empty())
            continue;

        if (std::fabs(effectiveBsw[color].begin()->first) < 5) {
            result.emplace_back(
                    SequenceIdentity({{}, "raw", "Bsw" + colorCodes[color]}, time, FP::undefined()),
                    effectiveBsw[color].begin()->second);
        }
        if (std::fabs(effectiveBsw[color].rbegin()->first - 90) < 5) {
            result.emplace_back(SequenceIdentity({{}, "raw", "Bbsw" + colorCodes[color]}, time,
                                                 FP::undefined()),
                                effectiveBsw[color].rbegin()->second);
        }

        if (hasFeature(Feature::Polar)) {
            Variant::Root Bsnw;
            for (const auto &add : effectiveBsw[color]) {
                Bsnw.write().toArray().after_back().set(add.second);
            }

            result.emplace_back(SequenceIdentity({{}, "raw", "Bsnw" + colorCodes[color]}, time,
                                                 FP::undefined()), std::move(Bsnw));
        }
    }

    return result;
}

static bool isBusyCharacter(char ch)
{ return ch == static_cast<char>(0x15) || ch == static_cast<char>(0xF); }

std::size_t AcquireEcotechNephAurora::dataFrameEnd(std::size_t start,
                                                   const Util::ByteArray &input) const
{
    if (start < input.size() && isBusyCharacter(input[start])) {
        ++start;
        for (std::size_t max = input.size(); start < max; start++) {
            char ch = input[start];
            if (!isBusyCharacter(ch))
                break;
        }
        return start;
    }
    for (std::size_t max = input.size(); start < max; start++) {
        char ch = input[start];
        if (ch == '\r' || ch == '\n')
            return start;
    }
    return input.npos;
}

static bool isPaddingSpace(char check)
{
    switch (check) {
    case ' ':
    case '\t':
        return true;
    default:
        return false;
    }
    return false;
}

static bool isDigit(char c)
{
    switch (c) {
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
        return true;
    default:
        return false;
    }
    return false;
}

static bool isBusyFrame(const Util::ByteView &frame)
{
    if (frame.empty())
        return true;
    for (auto check : frame) {
        if (!isBusyCharacter(check))
            return false;
    }
    return true;
}

static void adjustTemperature(Data::Variant::Write value)
{
    double check = value.toReal();
    if (!FP::defined(check))
        return;
    if (check < 150.0)
        return;
    check -= 273.15;
    value.setReal(check);
}

class AcquireEcotechNephAurora::ActiveCommand {
    AcquireEcotechNephAurora &parent;
protected:
    AcquireEcotechNephAurora::Sample &values;
    QLoggingCategory &log;
public:
    ActiveCommand() = delete;

    explicit ActiveCommand(AcquireEcotechNephAurora &parent) : parent(parent),
                                                               values(parent.values),
                                                               log(parent.log)
    { }

    virtual ~ActiveCommand() = default;

    virtual int response(const Util::ByteView &frame, double frameTime) = 0;

protected:

    inline void remap(const SequenceName::Component &name, Data::Variant::Root &value)
    { return parent.remap(name, value); }

    bool hasBackscatter() const
    {
        if (parent.angles.empty())
            return true;
        return std::fabs(parent.angles.back() - 90) < 5;
    }

    inline void opticalChangeDetected()
    { return parent.opticalChangeDetected(); }

    inline void processShutterUpdate()
    { return parent.processShutterUpdate(); }

    inline void processReferenceUpdate(std::size_t color, bool changed)
    { return parent.processReferenceUpdate(color, changed); }

    void digitalChangeDetected(double frameTime)
    {
        if (FP::defined(frameTime)) {
            parent.digitalState.setEnd(FP::undefined());
            if (parent.persistentEgress &&
                    parent.digitalState.read().exists() &&
                    FP::defined(parent.digitalState.getStart()) &&
                    parent.digitalState.getStart() < frameTime) {
                parent.persistentEgress->incomingData(parent.digitalState);
            }
        }
        parent.realtimeStateUpdated = true;

        parent.digitalState.write().set(values.F2);
        parent.digitalState.setStart(frameTime);
        parent.digitalState.setEnd(FP::undefined());
    }

    void stateChangeDetected()
    {
        parent.realtimeStateUpdated = true;
    }

    void autoprobeValidResponse(double frameTime)
    { return parent.autoprobeValidResponse(frameTime); }
};

namespace {

class Command_Ignore : public AcquireEcotechNephAurora::ActiveCommand {
public:
    explicit Command_Ignore(AcquireEcotechNephAurora &parent)
            : AcquireEcotechNephAurora::ActiveCommand(parent)
    { }

    virtual ~Command_Ignore() = default;

    int response(const Util::ByteView &, double) override
    { return 0; }
};

class Command_Ok : public AcquireEcotechNephAurora::ActiveCommand {
public:
    Util::ByteArray command;

    Command_Ok(AcquireEcotechNephAurora &parent, Util::ByteArray command)
            : AcquireEcotechNephAurora::ActiveCommand(parent), command(std::move(command))
    { }

    virtual ~Command_Ok() = default;

    int response(const Util::ByteView &frame, double) override
    { return frame.string_equal_insensitive("ok") ? 0 : 100; }
};

/* VI000 */
class Command_SystemStatus : public AcquireEcotechNephAurora::ActiveCommand {
public:
    /* Aurora 3000 manual (p 53) has a different order, but these seem correct */
    enum {
        Major_Normal = 0,
        Major_SpanCalibration = 1,
        Major_ZeroCalibration = 2,
        Major_SpanCheck = 3,
        Major_ZeroCheck = 4,
        Major_ZeroAdjust = 5,
        Major_SystemCalibration = 6,
        Major_EnvironmentalCalibration = 7,
    };

    enum {
        Minor_Normal_ShutterDown = 0,
        Minor_Normal_ShutterMeasure = 1,
        Minor_Normal_ShutterUp = 2,
        Minor_Normal_Measure = 3,
    };

    explicit Command_SystemStatus(AcquireEcotechNephAurora &parent)
            : AcquireEcotechNephAurora::ActiveCommand(parent)
    { }

    static int parse(const Util::ByteView &frame, int *major = nullptr, int *minor = nullptr)
    {
        /* Look for: <space>0.00<space> */
        if (frame.size() < 4 || frame.size() > 10)
            return 201;

        auto check = frame.begin();
        auto end = frame.end();

        for (; check != end && isPaddingSpace(*check); ++check) { }
        if (check == end)
            return 202;
        if (!isDigit(*check))
            return 203;
        for (; check + 1 != end && *check == '0' && isDigit(*(check + 1)); ++check) { }
        if (major)
            *major = static_cast<int>(*check - '0');

        ++check;
        if (check == end)
            return 204;
        if (*check != '.')
            return 205;

        ++check;
        if (check == end)
            return 206;
        if (!isDigit(*check))
            return 207;
        ++check;
        if (check == end)
            return 208;
        if (!isDigit(*check))
            return 209;
        for (; check + 1 != end && *check == '0' && isDigit(*(check + 1)); ++check) { }
        if (minor)
            *minor = static_cast<int>(*check - '0');

        ++check;
        for (; check != end && isPaddingSpace(*check); ++check) { }
        return check == end ? 0 : 210;
    }

    static inline bool isValidMajorState(int state)
    { return state >= 0 && state <= 7; }

    int response(const Util::ByteView &frame, double frameTime) override
    {
        int major = 0;
        int minor = 0;
        int rc = parse(frame, &major, &minor);
        if (rc != 0)
            return rc;
        if (!isValidMajorState(major))
            return 210;

        values.majorState = major;
        values.minorState = minor;

        processShutterUpdate();

        return 0;
    }
};

/* VI099 */
class Command_DataLine : public AcquireEcotechNephAurora::ActiveCommand {
    std::vector<double> angles;
public:
    explicit Command_DataLine(AcquireEcotechNephAurora &parent, std::vector<double> angles = {})
            : AcquireEcotechNephAurora::ActiveCommand(parent), angles(std::move(angles))
    {
        if (angles.empty())
            angles.emplace_back(90.0);
    }

    virtual ~Command_DataLine() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        if (frame.size() < 3)
            return 301;
        auto fields = Util::as_deque(frame.split(','));
        bool ok = false;

        /* Date and time */
        if (fields.empty()) return 302;
        fields.pop_front();

        std::array<std::map<double, CPD3::Data::Variant::Root>, 3> Bs;
        bool enablePolar = false;
        bool enableBackscatter = false;

        if (fields.empty()) return 303;
        auto field = fields.front().string_trimmed();
        fields.pop_front();
        auto value = field.parse_real(&ok);
        if (!ok) return 304;
        if (!FP::defined(value)) return 305;
        Bs[2][angles.front()] = Variant::Root(value);

        if (fields.empty()) return 306;
        field = fields.front().string_trimmed();
        fields.pop_front();
        value = field.parse_real(&ok);
        if (!ok) return 307;
        if (!FP::defined(value)) return 308;
        Bs[1][angles.front()] = Variant::Root(value);

        if (fields.empty()) return 309;
        field = fields.front().string_trimmed();
        fields.pop_front();
        value = field.parse_real(&ok);
        if (!ok) return 310;
        if (!FP::defined(value)) return 311;
        Bs[0][angles.front()] = Variant::Root(value);

        static constexpr std::size_t remainingDataFields = 6;
        if (fields.size() >= remainingDataFields + ((angles.size() - 1) * 3)) {
            for (std::size_t angleIndex = 1; angleIndex < angles.size(); angleIndex++) {
                auto angle = angles[angleIndex];

                field = fields.front().string_trimmed();
                fields.pop_front();
                if (!field.empty() && field != "-9999") {
                    enablePolar = true;
                    value = field.parse_real(&ok);
                    if (!ok) return 320 + angleIndex * 3;
                    if (!FP::defined(value)) return 321 + angleIndex * 3;
                    Bs[2][angle] = Variant::Root(value);
                }

                field = fields.front().string_trimmed();
                fields.pop_front();
                if (!field.empty() && field != "-9999") {
                    enablePolar = true;
                    value = field.parse_real(&ok);
                    if (!ok) return 322 + angleIndex * 3;
                    if (!FP::defined(value)) return 323 + angleIndex * 3;
                    Bs[1][angle] = Variant::Root(value);
                }

                field = fields.front().string_trimmed();
                fields.pop_front();
                if (!field.empty() && field != "-9999") {
                    enablePolar = true;
                    value = field.parse_real(&ok);
                    if (!ok) return 324 + angleIndex * 3;
                    if (!FP::defined(value)) return 325 + angleIndex * 3;
                    Bs[0][angle] = Variant::Root(value);
                }
            }
        } else if (fields.size() >= (remainingDataFields + 3)) {
            field = fields.front().string_trimmed();
            fields.pop_front();
            if (!field.empty() && field != "-9999") {
                enableBackscatter = true;
                value = field.parse_real(&ok);
                if (!ok) return 312;
                if (!FP::defined(value)) return 313;
                Bs[2][90] = Variant::Root(value);
            }

            field = fields.front().string_trimmed();
            fields.pop_front();
            if (!field.empty() && field != "-9999") {
                enableBackscatter = true;
                value = field.parse_real(&ok);
                if (!ok) return 314;
                if (!FP::defined(value)) return 315;
                Bs[1][90] = Variant::Root(value);
            }

            field = fields.front().string_trimmed();
            fields.pop_front();
            if (!field.empty() && field != "-9999") {
                enableBackscatter = true;
                value = field.parse_real(&ok);
                if (!ok) return 316;
                if (!FP::defined(value)) return 317;
                Bs[0][90] = Variant::Root(value);
            }
        }

        if (!enablePolar && (!enableBackscatter || !hasBackscatter())) {
            for (std::size_t color = 0; color < 3; color++) {
                Bs[color].erase(90);
            }
        }

        if (fields.empty()) return 380;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root T(field.parse_real(&ok));
        if (!ok) return 381;
        if (!FP::defined(T.read().toReal())) return 382;
        adjustTemperature(T.write());
        remap("T", T);

        if (fields.empty()) return 383;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root Tx(field.parse_real(&ok));
        if (!ok) return 384;
        if (!FP::defined(Tx.read().toReal())) return 385;
        adjustTemperature(Tx.write());
        remap("Tx", Tx);

        if (fields.empty()) return 386;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root U(field.parse_real(&ok));
        if (!ok) return 389;
        if (!FP::defined(U.read().toReal())) return 390;
        remap("U", U);

        if (fields.empty()) return 391;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root P(field.parse_real(&ok));
        if (!ok) return 392;
        if (!FP::defined(P.read().toReal())) return 393;
        remap("P", P);

        if (fields.empty()) return 394;
        field = fields.front().string_trimmed();
        fields.pop_front();
        int majorState = field.parse_i32(&ok);
        if (!ok) return 395;
        if (!Command_SystemStatus::isValidMajorState(majorState)) return 396;

        if (fields.empty()) return 397;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root F2(static_cast<std::int64_t>(field.parse_u16(&ok, 16)));
        if (!ok) return 398;
        remap("F2", F2);

        if (!fields.empty()) return 399;

        bool opticalChanged = false;
        for (std::size_t color = 0; color < 3; color++) {
            if (enablePolar) {
                for (std::size_t angleIndex = 0; angleIndex < angles.size(); angleIndex++) {
                    auto angle = angles[angleIndex];
                    auto target = Bs[color].find(angle);
                    if (target == Bs[color].end())
                        continue;
                    remap("ZBs" + std::to_string(angle) + colorCodes[color], target->second);
                }
            }

            {
                auto check = Bs[color].begin();
                if (std::fabs(check->first) < 5) {
                    remap("Bs" + colorCodes[color], check->second);
                }
            }
            {
                auto check = Bs[color].rbegin();
                if (std::fabs(check->first - 90) < 5) {
                    remap("Bbs" + colorCodes[color], check->second);
                }
            }

            for (const auto &update: Bs[color]) {
                auto check = values.Bs[color].find(update.first);
                if (check == values.Bs[color].end() ||
                        check->second.read() != update.second.read()) {
                    opticalChanged = true;
                }
            }

            values.Bs[color] = std::move(Bs[color]);
        }

        values.T = std::move(T);
        values.Tx = std::move(Tx);
        values.U = std::move(U);
        values.P = std::move(P);
        values.majorState = majorState;

        bool digitalChanged = false;
        if (!values.F2.read().exists()) {
            digitalChanged = F2.read().exists();
        } else {
            enum {
                Bit_CellHeaterOff = (1 << 0),
                Bit_InletHeaterOff = (1 << 1),
                Bit_SamplePumpOn = (1 << 2),
                Bit_ZeroPumpOn = (1 << 3),
                Bit_SpanGasValveOpen = (1 << 4),
                Bit_DigitalAuxOutputOn = (1 << 7),
            };
            static std::int_fast64_t
                    changeMask = Bit_SamplePumpOn | Bit_ZeroPumpOn | Bit_SpanGasValveOpen;

            auto current = F2.read().toInteger();
            auto prior = values.F2.read().toInteger();
            if (INTEGER::defined(current) && INTEGER::defined(prior)) {
                digitalChanged = (current & changeMask) != (prior & changeMask);
            }
        }
        values.F2 = std::move(F2);

        if (opticalChanged)
            opticalChangeDetected();
        if (digitalChanged)
            digitalChangeDetected(frameTime);

        autoprobeValidResponse(frameTime);

        return 0;
    }
};

/* VI088 */
class Command_StatusFlags : public AcquireEcotechNephAurora::ActiveCommand {
public:
    explicit Command_StatusFlags(AcquireEcotechNephAurora &parent)
            : AcquireEcotechNephAurora::ActiveCommand(parent)
    { }

    virtual ~Command_StatusFlags() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        if (frame.size() < 1 || frame.size() > 10)
            return 401;
        bool ok = false;
        auto converted = frame.toQByteArray().trimmed();
        if (converted.endsWith('.'))
            converted.chop(1);
        Variant::Root flags(static_cast<std::int_fast64_t>(converted.toUShort(&ok)));
        if (!ok) return 402;
        remap("FRAW", flags);

        if (INTEGER::defined(flags.read().toInt64())) {
            auto prior = std::move(values.reportedFlags);
            values.reportedFlags.clear();
            auto flagsBits = flags.read().toInteger();
            std::int_fast64_t bit = 1;
            for (const auto &flag : instrumentFlagTranslation) {
                if (flagsBits & bit) {
                    if (!flag.empty()) {
                        values.reportedFlags.insert(flag);
                    } else {
                        qCDebug(log) << "Unrecognized bit" << hex << bit
                                     << "set in instrument flags";
                    }
                }
                bit <<= 1;
            }
            if (prior != values.reportedFlags)
                stateChangeDetected();
        }

        return 0;
    }
};

class Command_Counts : public AcquireEcotechNephAurora::ActiveCommand {
public:
    explicit Command_Counts(AcquireEcotechNephAurora &parent)
            : AcquireEcotechNephAurora::ActiveCommand(parent)
    { }

    virtual ~Command_Counts() = default;

protected:
    int parse(const Util::ByteView &frame, double &raw, double maximum = 1E6)
    {
        if (frame.size() < 1 || frame.size() > 20)
            return 1;
        bool ok = false;
        raw = frame.toQByteArray().trimmed().toDouble(&ok);
        if (!ok) return 2;
        if (!FP::defined(raw)) return 3;
        if (raw < -100) return 4;
        if (raw > maximum) return 5;

        return 0;
    }
};

class Command_MeasureRatio : public AcquireEcotechNephAurora::ActiveCommand {
public:
    explicit Command_MeasureRatio(AcquireEcotechNephAurora &parent)
            : AcquireEcotechNephAurora::ActiveCommand(parent)
    { }

    virtual ~Command_MeasureRatio() = default;

protected:
    int parse(const Util::ByteView &frame, double &raw)
    {
        if (frame.size() < 1 || frame.size() > 20)
            return 1;
        bool ok = false;
        raw = frame.toQByteArray().trimmed().toDouble(&ok);
        if (!ok) return 2;
        if (!FP::defined(raw)) return 3;
        if (raw < -1) return 4;
        if (raw > 100) return 5;

        return 0;
    }
};

/* VI004 */
class Command_Dark : public Command_Counts {
public:
    explicit Command_Dark(AcquireEcotechNephAurora &parent) : Command_Counts(parent)
    { }

    virtual ~Command_Dark() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        double raw = FP::undefined();
        int rc = parse(frame, raw);
        if (rc != 0)
            return rc + 500;
        Variant::Root value(raw);
        remap("Cd", value);
        values.Cd = std::move(value);

        return 0;
    }
};


/* VI006 */
class Command_ReferenceRed : public Command_Counts {
public:
    explicit Command_ReferenceRed(AcquireEcotechNephAurora &parent) : Command_Counts(parent)
    { }

    virtual ~Command_ReferenceRed() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        double raw = FP::undefined();
        int rc = parse(frame, raw, 100E6);
        if (rc != 0)
            return rc + 1000;
        Variant::Root value(raw);
        remap("CfR", value);

        bool changed = values.Cf[2].read() != value.read();
        values.Cf[2] = std::move(value);
        processReferenceUpdate(2, changed);

        return 0;
    }
};

/* VI007 */
class Command_SampleRed : public Command_Counts {
public:
    explicit Command_SampleRed(AcquireEcotechNephAurora &parent) : Command_Counts(parent)
    { }

    virtual ~Command_SampleRed() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        double raw = FP::undefined();
        int rc = parse(frame, raw);
        if (rc != 0)
            return rc + 600;
        Variant::Root value(raw);
        remap("CsR", value);

        bool changed = values.Cs[2].read() != value.read();
        values.Cs[2] = std::move(value);
        if (changed)
            opticalChangeDetected();

        return 0;
    }
};

/* VI008 */
class Command_RatioRed : public Command_MeasureRatio {
public:
    explicit Command_RatioRed(AcquireEcotechNephAurora &parent) : Command_MeasureRatio(parent)
    { }

    virtual ~Command_RatioRed() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        double raw = FP::undefined();
        int rc = parse(frame, raw);
        if (rc != 0)
            return rc + 1200;
        Variant::Root value(raw);
        remap("CrR", value);
        values.Cr[2] = std::move(value);

        return 0;
    }
};

/* VI033 */
class Command_BackSampleRed : public Command_Counts {
public:
    explicit Command_BackSampleRed(AcquireEcotechNephAurora &parent) : Command_Counts(parent)
    { }

    virtual ~Command_BackSampleRed() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        double raw = FP::undefined();
        int rc = parse(frame, raw);
        if (rc != 0)
            return rc + 1300;
        Variant::Root value(raw);
        remap("CbsR", value);

        bool changed = values.Cbs[2].read() != value.read();
        values.Cbs[2] = std::move(value);
        if (changed)
            opticalChangeDetected();

        return 0;
    }
};

/* VI034 */
class Command_BackRatioRed : public Command_MeasureRatio {
public:
    explicit Command_BackRatioRed(AcquireEcotechNephAurora &parent) : Command_MeasureRatio(parent)
    { }

    virtual ~Command_BackRatioRed() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        double raw = FP::undefined();
        int rc = parse(frame, raw);
        if (rc != 0)
            return rc + 1400;
        Variant::Root value(raw);
        remap("CbrR", value);
        values.Cbr[2] = std::move(value);

        return 0;
    }
};


/* VI009 */
class Command_ReferenceGreen : public Command_Counts {
public:
    explicit Command_ReferenceGreen(AcquireEcotechNephAurora &parent) : Command_Counts(parent)
    { }

    virtual ~Command_ReferenceGreen() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        double raw = FP::undefined();
        int rc = parse(frame, raw, 100E6);
        if (rc != 0)
            return rc + 2000;
        Variant::Root value(raw);
        remap("CfG", value);

        bool changed = values.Cf[1].read() != value.read();
        values.Cf[1] = std::move(value);
        processReferenceUpdate(1, changed);

        return 0;
    }
};

/* VI010 */
class Command_SampleGreen : public Command_Counts {
public:
    explicit Command_SampleGreen(AcquireEcotechNephAurora &parent) : Command_Counts(parent)
    { }

    virtual ~Command_SampleGreen() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        double raw = FP::undefined();
        int rc = parse(frame, raw);
        if (rc != 0)
            return rc + 2100;
        Variant::Root value(raw);
        remap("CsG", value);

        bool changed = values.Cs[1].read() != value.read();
        values.Cs[1] = std::move(value);
        if (changed)
            opticalChangeDetected();

        return 0;
    }
};

/* VI011 */
class Command_RatioGreen : public Command_MeasureRatio {
public:
    explicit Command_RatioGreen(AcquireEcotechNephAurora &parent) : Command_MeasureRatio(parent)
    { }

    virtual ~Command_RatioGreen() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        double raw = FP::undefined();
        int rc = parse(frame, raw);
        if (rc != 0)
            return rc + 2200;
        Variant::Root value(raw);
        remap("CrG", value);
        values.Cr[1] = std::move(value);

        return 0;
    }
};

/* VI035 */
class Command_BackSampleGreen : public Command_Counts {
public:
    explicit Command_BackSampleGreen(AcquireEcotechNephAurora &parent) : Command_Counts(parent)
    { }

    virtual ~Command_BackSampleGreen() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        double raw = FP::undefined();
        int rc = parse(frame, raw);
        if (rc != 0)
            return rc + 2300;
        Variant::Root value(raw);
        remap("CbsG", value);

        bool changed = values.Cbs[1].read() != value.read();
        values.Cbs[1] = std::move(value);
        if (changed)
            opticalChangeDetected();

        return 0;
    }
};

/* VI036 */
class Command_BackRatioGreen : public Command_MeasureRatio {
public:
    explicit Command_BackRatioGreen(AcquireEcotechNephAurora &parent) : Command_MeasureRatio(parent)
    { }

    virtual ~Command_BackRatioGreen() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        double raw = FP::undefined();
        int rc = parse(frame, raw);
        if (rc != 0)
            return rc + 2400;
        Variant::Root value(raw);
        remap("CbrG", value);
        values.Cbr[1] = std::move(value);

        return 0;
    }
};


/* VI012 */
class Command_ReferenceBlue : public Command_Counts {
public:
    explicit Command_ReferenceBlue(AcquireEcotechNephAurora &parent) : Command_Counts(parent)
    { }

    virtual ~Command_ReferenceBlue() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        double raw = FP::undefined();
        int rc = parse(frame, raw, 100E6);
        if (rc != 0)
            return rc + 3000;
        Variant::Root value(raw);
        remap("CfB", value);

        bool changed = values.Cf[0].read() != value.read();
        values.Cf[0] = std::move(value);
        processReferenceUpdate(0, changed);

        return 0;
    }
};

/* VI013 */
class Command_SampleBlue : public Command_Counts {
public:
    explicit Command_SampleBlue(AcquireEcotechNephAurora &parent) : Command_Counts(parent)
    { }

    virtual ~Command_SampleBlue() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        double raw = FP::undefined();
        int rc = parse(frame, raw);
        if (rc != 0)
            return rc + 3100;
        Variant::Root value(raw);
        remap("CsB", value);

        bool changed = values.Cs[0].read() != value.read();
        values.Cs[0] = std::move(value);
        if (changed)
            opticalChangeDetected();

        return 0;
    }
};

/* VI014 */
class Command_RatioBlue : public Command_MeasureRatio {
public:
    explicit Command_RatioBlue(AcquireEcotechNephAurora &parent) : Command_MeasureRatio(parent)
    { }

    virtual ~Command_RatioBlue() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        double raw = FP::undefined();
        int rc = parse(frame, raw);
        if (rc != 0)
            return rc + 3200;
        Variant::Root value(raw);
        remap("CrB", value);
        values.Cr[0] = std::move(value);

        return 0;
    }
};

/* VI037 */
class Command_BackSampleBlue : public Command_Counts {
public:
    explicit Command_BackSampleBlue(AcquireEcotechNephAurora &parent) : Command_Counts(parent)
    { }

    virtual ~Command_BackSampleBlue() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        double raw = FP::undefined();
        int rc = parse(frame, raw);
        if (rc != 0)
            return rc + 3300;
        Variant::Root value(raw);
        remap("CbsB", value);

        bool changed = values.Cbs[0].read() != value.read();
        values.Cbs[0] = std::move(value);
        if (changed)
            opticalChangeDetected();

        return 0;
    }
};

/* VI038 */
class Command_BackRatioBlue : public Command_MeasureRatio {
public:
    explicit Command_BackRatioBlue(AcquireEcotechNephAurora &parent) : Command_MeasureRatio(parent)
    { }

    virtual ~Command_BackRatioBlue() = default;

    int response(const Util::ByteView &frame, double frameTime) override
    {
        double raw = FP::undefined();
        int rc = parse(frame, raw);
        if (rc != 0)
            return rc + 3400;
        Variant::Root value(raw);
        remap("CbrB", value);
        values.Cbr[0] = std::move(value);

        return 0;
    }
};


class Command_PolarScattering : public AcquireEcotechNephAurora::ActiveCommand {
protected:
    double angle;
public:
    Command_PolarScattering(AcquireEcotechNephAurora &parent, double angle)
            : AcquireEcotechNephAurora::ActiveCommand(parent), angle(angle)
    { }

    virtual ~Command_PolarScattering() = default;

protected:
    int parse(const Util::ByteView &frame, double &raw)
    {
        if (frame.size() < 1 || frame.size() > 12)
            return 1;
        bool ok = false;
        auto base = frame.toQByteArray().trimmed();
        if (base == "-9999")
            return -1;
        raw = base.toDouble(&ok);
        if (!ok) return 2;
        if (!FP::defined(raw)) return 3;
        if (raw < -100) return 4;
        if (raw > 1E4) return 5;

        return 0;
    }

    void processValue(std::map<double, Data::Variant::Root> &target, Data::Variant::Root &value)
    {
        auto ins = target.find(angle);
        if (ins == target.end()) {
            target.emplace(angle, std::move(value));
            opticalChangeDetected();
            return;
        }

        if (ins->second.read() == value.read())
            return;

        ins->second = std::move(value);
        opticalChangeDetected();
    }
};

}

bool AcquireEcotechNephAurora::isCommandCycleComplete() const
{
    switch (commandState) {
    case CommandState::Blue_Ratio:
        if (hasFeature(Feature::Polar) && !angles.empty())
            return false;
        /* Fall through */
    case CommandState::Blue_BackRatio:
        if (angles.empty() || std::fabs(angles.back() - 90) < 5)
            return false;
        if (hasFeature(Feature::StatusFlags))
            return false;
        /* Fall through */
    case CommandState::StatusFlags:
        return true;
    default:
        break;
    }

    return false;
}

bool AcquireEcotechNephAurora::isInteractiveStart() const
{
    switch (responseState) {
    case ResponseState::Interactive_Start_ReadID:
    case ResponseState::Interactive_Start_ReadStatus:
    case ResponseState::Interactive_Start_SetTime:
    case ResponseState::Interactive_Start_SetFilter:
    case ResponseState::Interactive_Start_SetNormalization:
    case ResponseState::Interactive_Start_ReadEE:
    case ResponseState::Interactive_Start_CheckFlags:
    case ResponseState::Interactive_Start_DisableSpan:
    case ResponseState::Interactive_Start_DisableZero:
        return true;

    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Run_Active:
    case ResponseState::Interactive_Run_Retry:
    case ResponseState::Interactive_Run_Resume:
    case ResponseState::Interactive_Run_Delay:
    case ResponseState::Interactive_Zero_ReadEE:
    case ResponseState::Interactive_Zero_ReadEE_Retry:
    case ResponseState::Interactive_Zero_ReadEE_Resume:
        return false;
    }

    Q_ASSERT(false);
    return false;
}

bool AcquireEcotechNephAurora::isInteractive() const
{
    switch (responseState) {
    case ResponseState::Interactive_Start_ReadID:
    case ResponseState::Interactive_Start_ReadStatus:
    case ResponseState::Interactive_Start_SetTime:
    case ResponseState::Interactive_Start_SetFilter:
    case ResponseState::Interactive_Start_SetNormalization:
    case ResponseState::Interactive_Start_ReadEE:
    case ResponseState::Interactive_Start_CheckFlags:
    case ResponseState::Interactive_Start_DisableSpan:
    case ResponseState::Interactive_Start_DisableZero:
    case ResponseState::Interactive_Restart_Wait:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Run_Active:
    case ResponseState::Interactive_Run_Retry:
    case ResponseState::Interactive_Run_Resume:
    case ResponseState::Interactive_Run_Delay:
    case ResponseState::Interactive_Zero_ReadEE:
    case ResponseState::Interactive_Zero_ReadEE_Retry:
    case ResponseState::Interactive_Zero_ReadEE_Resume:
        return true;

    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
        return false;
    }

    Q_ASSERT(false);
    return false;
}

bool AcquireEcotechNephAurora::zeroConditionAccumulationActive() const
{
    switch (sampleState) {
    case SampleState::Measure:
    case SampleState::BlankExitZero:
    case SampleState::ZeroAccumulateEnterBlank:
    case SampleState::Spancheck:
    case SampleState::InstrumentCalibration:
        return false;
    case SampleState::ZeroAccumulate:
    case SampleState::ZeroInstrument:
        return true;
    }

    Q_ASSERT(false);
    return false;
}

bool AcquireEcotechNephAurora::zeroOpticalAccumulationActive() const
{
    switch (sampleState) {
    case SampleState::Measure:
    case SampleState::BlankExitZero:
    case SampleState::ZeroAccumulateEnterBlank:
    case SampleState::Spancheck:
    case SampleState::ZeroInstrument:
    case SampleState::InstrumentCalibration:
        return false;
    case SampleState::ZeroAccumulate:
        return true;
    }

    Q_ASSERT(false);
    return false;
}

void AcquireEcotechNephAurora::commandAdvance(double time, CommandOperation operation)
{
    switch (operation) {
    case CommandOperation::Response:
        Q_ASSERT(!config.empty());
        if (isCommandCycleComplete()) {
            if (zeroReadPending &&
                    responseState == ResponseState::Interactive_Run_Active &&
                    outstandingCommands.empty() &&
                    okCommandQueue.empty()) {
                zeroReadPending = false;

                commandState = CommandState::SystemState;
                responseState = ResponseState::Interactive_Zero_ReadEE;
                timeoutAt(time + 2.0);
                seenEEComponents.fill(false);
                readEEAbsoluteTimeout = FP::undefined();
                if (controlStream) {
                    controlStream->writeControl("EE\r");
                }
                lastEEResults = CPD3::Data::Variant::Root();
                return;
            }

            double delay = config.front().inactiveDelay;
            if (!FP::defined(delay)) {

                double perCommand = config.front().commandDelay;
                if (!FP::defined(perCommand) || perCommand <= 0.0)
                    perCommand = 0.0;

                int expectedCommands = 18;
                if (hasFeature(Feature::StatusFlags))
                    expectedCommands += 1;

                if (!hasFeature(Feature::Polar) || angles.empty()) {
                    delay = 3.0 - perCommand * expectedCommands;
                } else {
                    delay = angles.size() * 3.5 - perCommand * expectedCommands;
                }
            }
            if (delay > 0.0) {
                responseState = ResponseState::Interactive_Run_Delay;
                timeoutAt(time + delay);
                return;
            }
        }
        if (FP::defined(config.front().commandDelay) && config.front().commandDelay > 0.0) {
            responseState = ResponseState::Interactive_Run_Delay;
            timeoutAt(time + config.front().commandDelay);
            return;
        }

        /* Fall through */
    case CommandOperation::Advance:
        /* If there's control command, then run it first */
        if (!okCommandQueue.empty())
            break;

        switch (commandState) {
        case CommandState::SystemState:
            commandState = CommandState::DataLine;
            break;
        case CommandState::DataLine:
            commandState = CommandState::Dark;
            break;
        case CommandState::Dark:
            commandState = CommandState::Red_Reference;
            break;

        case CommandState::Red_Reference:
            commandState = CommandState::Red_Sample;
            break;
        case CommandState::Red_Sample:
            commandState = CommandState::Red_Ratio;
            break;
        case CommandState::Red_Ratio:
            if (angles.empty() || std::fabs(angles.back() - 90) < 5) {
                commandState = CommandState::Red_BackSample;
                break;
            }
            /* Fall through */
        case CommandState::Red_BackRatio:
            commandState = CommandState::Green_Reference;
            break;
        case CommandState::Red_BackSample:
            commandState = CommandState::Red_BackRatio;
            break;

        case CommandState::Green_Reference:
            commandState = CommandState::Green_Sample;
            break;
        case CommandState::Green_Sample:
            commandState = CommandState::Green_Ratio;
            break;
        case CommandState::Green_Ratio:
            if (angles.empty() || std::fabs(angles.back() - 90) < 5) {
                commandState = CommandState::Green_BackSample;
                break;
            }
            /* Fall through */
        case CommandState::Green_BackRatio:
            commandState = CommandState::Blue_Reference;
            break;
        case CommandState::Green_BackSample:
            commandState = CommandState::Green_BackRatio;
            break;

        case CommandState::Blue_Reference:
            commandState = CommandState::Blue_Sample;
            break;
        case CommandState::Blue_Sample:
            commandState = CommandState::Blue_Ratio;
            break;
        case CommandState::Blue_Ratio:
            if (angles.empty() || std::fabs(angles.back() - 90) < 5) {
                commandState = CommandState::Blue_BackSample;
                break;
            }
            /* Fall through */
        case CommandState::Blue_BackRatio:
            if (hasFeature(Feature::StatusFlags)) {
                commandState = CommandState::StatusFlags;
                break;
            }
            /* Fall through */
        case CommandState::StatusFlags:
            commandState = CommandState::SystemState;
            break;
        case CommandState::Blue_BackSample:
            commandState = CommandState::Blue_BackRatio;
            break;
        }
        break;

    case CommandOperation::Resend:
        break;
    }

    timeoutAt(time + 2.0);

    responseState = ResponseState::Interactive_Run_Active;

    if (!okCommandQueue.empty()) {
        auto command = std::move(okCommandQueue.front());
        okCommandQueue.pop_front();

        if (controlStream) {
            controlStream->writeControl(command);
        }

        outstandingCommands.emplace_back(new Command_Ok(*this, std::move(command)));
        return;
    }

    switch (commandState) {
    case CommandState::SystemState:
        outstandingCommands.emplace_back(new Command_SystemStatus(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "00\r");
        }
        break;
    case CommandState::DataLine: {
        outstandingCommands.emplace_back(new Command_DataLine(*this, angles));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "99\r");
        }
        break;
    }
    case CommandState::Dark:
        outstandingCommands.emplace_back(new Command_Dark(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "04\r");
        }
        break;

    case CommandState::Red_Reference:
        outstandingCommands.emplace_back(new Command_ReferenceRed(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "06\r");
        }
        break;
    case CommandState::Red_Sample:
        outstandingCommands.emplace_back(new Command_SampleRed(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "07\r");
        }
        break;
    case CommandState::Red_Ratio:
        outstandingCommands.emplace_back(new Command_RatioRed(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "08\r");
        }
        break;
    case CommandState::Red_BackSample:
        outstandingCommands.emplace_back(new Command_BackSampleRed(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "33\r");
        }
        break;
    case CommandState::Red_BackRatio:
        outstandingCommands.emplace_back(new Command_BackRatioRed(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "34\r");
        }
        break;

    case CommandState::Green_Reference:
        outstandingCommands.emplace_back(new Command_ReferenceGreen(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "09\r");
        }
        break;
    case CommandState::Green_Sample:
        outstandingCommands.emplace_back(new Command_SampleGreen(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "10\r");
        }
        break;
    case CommandState::Green_Ratio:
        outstandingCommands.emplace_back(new Command_RatioGreen(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "11\r");
        }
        break;
    case CommandState::Green_BackSample:
        outstandingCommands.emplace_back(new Command_BackSampleGreen(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "35\r");
        }
        break;
    case CommandState::Green_BackRatio:
        outstandingCommands.emplace_back(new Command_BackRatioGreen(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "36\r");
        }
        break;

    case CommandState::Blue_Reference:
        outstandingCommands.emplace_back(new Command_ReferenceBlue(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "12\r");
        }
        break;
    case CommandState::Blue_Sample:
        outstandingCommands.emplace_back(new Command_SampleBlue(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "13\r");
        }
        break;
    case CommandState::Blue_Ratio:
        outstandingCommands.emplace_back(new Command_RatioBlue(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "14\r");
        }
        break;
    case CommandState::Blue_BackSample:
        outstandingCommands.emplace_back(new Command_BackSampleBlue(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "37\r");
        }
        break;
    case CommandState::Blue_BackRatio:
        outstandingCommands.emplace_back(new Command_BackRatioBlue(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "38\r");
        }
        break;

    case CommandState::StatusFlags:
        outstandingCommands.emplace_back(new Command_StatusFlags(*this));
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "88\r");
        }
        break;
    }
}

int AcquireEcotechNephAurora::processUnpolledResponse(const Util::ByteView &frame,
                                                      double &frameTime)
{
    if (frame.size() < 3)
        return 1;

    auto fields = Util::as_deque(frame.split(','));
    bool ok = false;

    if (fields.empty()) return 2;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    auto spacefields = Util::as_deque(field.split(' '));
    if (spacefields.size() != 2) return 3;
    auto subfields = Util::as_deque(spacefields.front().string_trimmed().split('/'));
    spacefields.pop_front();
    if (subfields.size() != 3) return 4;

    Q_ASSERT(!subfields.empty());
    qint64 iday = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 5;
    if (iday < 1) return 6;
    if (iday > 31) return 7;
    Variant::Root day(iday);
    remap("DAY", day);
    iday = day.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 imonth = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 8;
    if (imonth < 1) return 9;
    if (imonth > 12) return 10;
    Variant::Root month(imonth);
    remap("MONTH", month);
    imonth = month.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 iyear = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 11;
    if (iyear < 1900) {
        if (iyear > 99) return 12;
        if (iyear < 0) return 13;
        iyear += 2000;
    }
    if (iyear > 2999) return 14;
    Variant::Root year(iyear);
    remap("YEAR", year);
    iyear = year.read().toInt64();

    Q_ASSERT(!spacefields.empty());
    subfields = Util::as_deque(spacefields.front().string_trimmed().split(':'));
    spacefields.pop_front();
    if (subfields.size() != 3) return 15;

    Q_ASSERT(!subfields.empty());
    qint64 ihour = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 16;
    if (ihour < 0) return 17;
    if (ihour > 23) return 18;
    Variant::Root hour(ihour);
    remap("HOUR", hour);
    ihour = hour.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 iminute = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 19;
    if (iminute < 0) return 20;
    if (iminute > 59) return 21;
    Variant::Root minute(iminute);
    remap("MINUTE", minute);
    iminute = minute.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 isecond = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 22;
    if (isecond < 0) return 23;
    if (isecond > 60) return 24;
    Variant::Root second(isecond);
    remap("SECOND", second);
    isecond = second.read().toInt64();

    if (!FP::defined(frameTime) && INTEGER::defined(iyear) && iyear >= 1900 && iyear <= 2999 &&
            INTEGER::defined(imonth) &&
            imonth >= 1 &&
            imonth <= 12 &&
            INTEGER::defined(iday) &&
            iday >= 1 &&
            iday <= 31 &&
            INTEGER::defined(ihour) &&
            ihour >= 0 &&
            ihour <= 23 &&
            INTEGER::defined(iminute) &&
            iminute >= 0 &&
            iminute <= 59 &&
            INTEGER::defined(isecond) &&
            isecond >= 0 &&
            isecond <= 60) {
        frameTime = Time::fromDateTime(QDateTime(
                QDate(static_cast<int>(iyear), static_cast<int>(imonth), static_cast<int>(iday)),
                QTime(static_cast<int>(ihour), static_cast<int>(iminute),
                      static_cast<int>(isecond)), Qt::UTC));
        configAdvance(frameTime);
    }
    if (FP::defined(lastRecordTime) && FP::defined(frameTime) && frameTime < lastRecordTime)
        return -1;

    if (fields.empty()) return 25;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.size() < 4) return 26;
    if (field.string_start("Instant")) {
    } else if (isDigit(field[0])) {
        if (!Util::contains(Util::to_lower(field.toString()), "min")) return 27;
    }

    if (fields.empty()) return 28;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root BsR(field.parse_real(&ok));
    if (!ok) return 29;
    if (!FP::defined(BsR.read().toReal())) return 30;
    remap("BsR", BsR);

    if (fields.empty()) return 31;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root BsG(field.parse_real(&ok));
    if (!ok) return 32;
    if (!FP::defined(BsG.read().toReal())) return 33;
    remap("BsG", BsG);

    if (fields.empty()) return 34;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root BsB(field.parse_real(&ok));
    if (!ok) return 35;
    if (!FP::defined(BsB.read().toReal())) return 36;
    remap("BsB", BsB);

    bool haveBackscatter = false;
    Variant::Root BbsR;
    Variant::Root BbsG;
    Variant::Root BbsB;
    if (fields.size() >= 7) {
        haveBackscatter = true;

        if (fields.empty()) return 37;
        field = fields.front().string_trimmed();
        fields.pop_front();
        BbsR.write().setDouble(field.parse_real(&ok));
        if (!ok) return 38;
        if (!FP::defined(BbsR.read().toReal())) return 39;
        remap("BbsR", BbsR);

        if (fields.empty()) return 40;
        field = fields.front().string_trimmed();
        fields.pop_front();
        BbsG.write().setDouble(field.parse_real(&ok));
        if (!ok) return 41;
        if (!FP::defined(BbsG.read().toReal())) return 42;
        remap("BbsG", BbsG);

        if (fields.empty()) return 43;
        field = fields.front().string_trimmed();
        fields.pop_front();
        BbsB.write().setDouble(field.parse_real(&ok));
        if (!ok) return 44;
        if (!FP::defined(BbsB.read().toReal())) return 45;
        remap("BbsB", BbsB);
    }

    if (fields.empty()) return 46;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T(field.parse_real(&ok));
    if (!ok) return 47;
    if (!FP::defined(T.read().toReal())) return 48;
    adjustTemperature(T.write());
    remap("T", T);

    if (fields.empty()) return 49;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Tx(field.parse_real(&ok));
    if (!ok) return 50;
    if (!FP::defined(Tx.read().toReal())) return 51;
    adjustTemperature(Tx.write());
    remap("Tx", Tx);

    if (fields.empty()) return 52;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root U(field.parse_real(&ok));
    if (!ok) return 53;
    if (!FP::defined(U.read().toReal())) return 54;
    remap("U", U);

    if (fields.empty()) return 55;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root P(field.parse_real(&ok));
    if (!ok) return 56;
    if (!FP::defined(P.read().toReal())) return 57;
    remap("P", P);

    if (!fields.empty())
        return 99;

    values.Bs[0].clear();
    values.Bs[1].clear();
    values.Bs[2].clear();

    values.Bs[0].emplace(0, std::move(BsB));
    values.Bs[1].emplace(0, std::move(BsG));
    values.Bs[2].emplace(0, std::move(BsR));
    if (haveBackscatter) {
        values.Bs[0].emplace(90, std::move(BbsB));
        values.Bs[1].emplace(90, std::move(BbsG));
        values.Bs[2].emplace(90, std::move(BbsR));
    }

    values.T = std::move(T);
    values.Tx = std::move(Tx);
    values.U = std::move(U);
    values.P = std::move(P);

    if (isInteractive())
        return 0;

    opticalUpdate = OpticalUpdate::ReadPending;
    dataAdvance(frameTime);

    return 0;
}

void AcquireEcotechNephAurora::autoprobeValidResponse(double frameTime)
{
    switch (responseState) {
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
        if (++autoprobeValidRecords < 5)
            break;

        qCDebug(log) << "Comms established at" << Logging::time(frameTime);
        event(frameTime, QObject::tr("Communications established."), false, describeState());

        commandState = CommandState::SystemState;
        responseState = ResponseState::Passive_Run;
        generalStatusUpdated();

        opticalUpdate = OpticalUpdate::ReadPending;
        opticalUpdateTimeout = FP::undefined();
        referenceUpdate = ReferenceUpdate::ReadPending;
        referenceUpdateTimeout = FP::undefined();
        referenceColorsRead.fill(false);

        timeoutAt(frameTime + config.front().inactiveDelay * 2.0 + 1.0);

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                  FP::undefined()));
        }
        realtimeStateUpdated = true;
        break;

    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
        if (++autoprobeValidRecords < 5)
            break;

        qCDebug(log) << "Autoprobe succeeded at" << Logging::time(frameTime);
        event(frameTime, QObject::tr("Autoprobe succeeded."), false, describeState());

        commandState = CommandState::SystemState;
        responseState = ResponseState::Passive_Run;

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        generalStatusUpdated();

        timeoutAt(frameTime + config.front().inactiveDelay * 2.0 + 1.0);

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                  FP::undefined()));
        }
        realtimeStateUpdated = true;
        break;

    default:
        break;
    }
}

void AcquireEcotechNephAurora::opticalChangeDetected()
{
    switch (opticalUpdate) {
    case OpticalUpdate::Unchanged:
        opticalUpdate = OpticalUpdate::ChangeDetected;
        return;
    case OpticalUpdate::ChangeDetected:
    case OpticalUpdate::ReadPending:
        return;
    }
}

void AcquireEcotechNephAurora::processShutterUpdate()
{
    switch (values.majorState) {
    case Command_SystemStatus::Major_Normal:
        break;
    default:
        return;
    }

    switch (referenceUpdate) {
    case ReferenceUpdate::ShutterOff:
        switch (values.minorState) {
        default:
            break;
        case Command_SystemStatus::Minor_Normal_ShutterDown:
        case Command_SystemStatus::Minor_Normal_ShutterMeasure:
            referenceUpdate = ReferenceUpdate::ShutterOn;
            referenceColorsRead.fill(false);
            return;
        case Command_SystemStatus::Minor_Normal_ShutterUp:
            referenceUpdate = ReferenceUpdate::ReadPending;
            referenceColorsRead.fill(false);
            return;
        }
        break;
    case ReferenceUpdate::ShutterOn:
        switch (values.minorState) {
        default:
            break;
        case Command_SystemStatus::Minor_Normal_ShutterUp:
        case Command_SystemStatus::Minor_Normal_Measure:
            referenceUpdate = ReferenceUpdate::ReadPending;
            referenceColorsRead.fill(false);
            return;
        }
        break;

    case ReferenceUpdate::ReadPending:
        break;
    }
}

void AcquireEcotechNephAurora::processReferenceUpdate(std::size_t color, bool changed)
{
    Q_ASSERT(color >= 0 && color <= 2);
    if (changed) {
        switch (referenceUpdate) {
        case ReferenceUpdate::ShutterOn:
            break;
        case ReferenceUpdate::ShutterOff:
            referenceUpdate = ReferenceUpdate::ShutterOn;
            referenceColorsRead.fill(false);
            return;
        case ReferenceUpdate::ReadPending:
            break;
        }
    } else {
        switch (referenceUpdate) {
        case ReferenceUpdate::ShutterOn:
        case ReferenceUpdate::ShutterOff:
            return;
        case ReferenceUpdate::ReadPending:
            break;
        }
    }

    referenceColorsRead[color] = true;
}

void AcquireEcotechNephAurora::dataAdvance(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));

    if (angles.empty()) {
        std::unordered_set<double> uniqueAngles;
        for (const auto &Bs : values.Bs) {
            for (const auto &add : Bs) {
                uniqueAngles.insert(add.first);
            }
        }
        if (!uniqueAngles.empty()) {
            Util::append(uniqueAngles, angles);
            std::sort(angles.begin(), angles.end());

            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
    }

    if (loggingEgress) {
        if (!haveEmittedLogMeta) {
            haveEmittedLogMeta = true;
            loggingMux.incoming(static_cast<std::size_t>(LogStream::Metadata),
                                buildLogMeta(frameTime), loggingEgress);
        }
        loggingMux.advance(static_cast<std::size_t>(LogStream::Metadata), frameTime, loggingEgress);
        streamBeginTime[static_cast<std::size_t>(LogStream::Metadata)] = frameTime;
    } else {
        invalidateLogValues(frameTime);
    }

    if (!haveEmittedRealtimeMeta && realtimeEgress) {
        haveEmittedRealtimeMeta = true;

        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    if (realtimeStateUpdated && realtimeEgress) {
        realtimeStateUpdated = false;

        switch (sampleState) {
        case SampleState::ZeroInstrument:
        case SampleState::ZeroAccumulate:
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Zero"), frameTime,
                                  FP::undefined()));
            break;
        case SampleState::ZeroAccumulateEnterBlank:
        case SampleState::BlankExitZero:
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Blank"), frameTime,
                                  FP::undefined()));
            break;
        case SampleState::Spancheck:
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Spancheck"), frameTime,
                                  FP::undefined()));
            break;
        case SampleState::InstrumentCalibration:
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Calibration"), frameTime,
                                  FP::undefined()));
            break;
        case SampleState::Measure:
            if (hasFeature(Feature::StatusFlags) && !values.reportedFlags.empty()) {
                if (values.reportedFlags.count("WarmupFault")) {
                    realtimeEgress->incomingData(
                            SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Warmup"), frameTime,
                                          FP::undefined()));
                    break;
                }

                auto checkFlags = values.reportedFlags;
                if (checkFlags.size() > 1) {
                    checkFlags.erase("SystemFault");
                }

                if (checkFlags.size() == 1) {
                    realtimeEgress->incomingData(
                            SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(*checkFlags.begin()),
                                          frameTime, FP::undefined()));
                } else {
                    QStringList sorted;
                    for (const auto &add : checkFlags) {
                        sorted.append(QString::fromStdString(add));
                    }
                    std::sort(sorted.begin(), sorted.end());
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            QString("Status: %1").arg(sorted.join(','))), frameTime,
                                                               FP::undefined()));
                }
                break;
            }
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                  FP::undefined()));
            break;
        }

        realtimeEgress->incomingData(digitalState);
    }

    if (realtimeZeroUpdated && realtimeEgress) {
        realtimeZeroUpdated = false;
        auto result = constructZeroVariables(frameTime);
        for (std::size_t color = 0; color < effectiveBsw.size(); ++color) {
            if (Bswd[color].empty())
                continue;

            if (std::fabs(Bswd[color].begin()->first) < 5) {
                result.emplace_back(
                        SequenceIdentity({{}, "raw", "Bswd" + colorCodes[color]}, frameTime,
                                         FP::undefined()), Bswd[color].begin()->second);
            }
            if (std::fabs(Bswd[color].rbegin()->first - 90) < 5) {
                result.emplace_back(
                        SequenceIdentity({{}, "raw", "Bbswd" + colorCodes[color]}, frameTime,
                                         FP::undefined()), Bswd[color].rbegin()->second);
            }
        }
        realtimeEgress->incomingData(std::move(result));
    }
    if (persistentZeroUpdated && persistentEgress && FP::defined(zeroEffectiveTime)) {
        persistentZeroUpdated = false;
        persistentEgress->incomingData(constructZeroVariables(zeroEffectiveTime));
    }

    if (realtimeEEUpdated && realtimeEgress) {
        realtimeEEUpdated = false;
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZEE"}, lastEEResults, frameTime, FP::undefined()));
    }
    if (persistentEEUpdated && persistentEgress) {
        persistentEEUpdated = false;
        persistentEgress->incomingData(
                SequenceValue({{}, "raw", "ZEE"}, lastEEResults, frameTime, FP::undefined()));
    }

    spancheckController->advance(frameTime, realtimeEgress);

    if (FP::defined(blankEndTime) && blankEndTime < frameTime)
        blankEndTime = FP::undefined();

    if (isInteractive() && commandState != CommandState::SystemState)
        return;

    switch (opticalUpdate) {
    case OpticalUpdate::Unchanged:
        if (FP::defined(opticalUpdateTimeout) && frameTime > opticalUpdateTimeout) {
            outputOptical(frameTime);
        }
        break;
    case OpticalUpdate::ChangeDetected:
        opticalUpdate = OpticalUpdate::ReadPending;
        break;
    case OpticalUpdate::ReadPending:
        opticalUpdate = OpticalUpdate::Unchanged;
        outputOptical(frameTime);
        break;
    }

    if (values.majorState == Command_SystemStatus::Major_Normal) {
        switch (referenceUpdate) {
        case ReferenceUpdate::ReadPending:
            if (haveAllReferenceColors()) {
                referenceUpdate = ReferenceUpdate::ShutterOff;
                referenceColorsRead.fill(false);
                outputReference(frameTime);
            }
            break;
        case ReferenceUpdate::ShutterOff:
        case ReferenceUpdate::ShutterOn:
            if (FP::defined(referenceUpdateTimeout) && frameTime > referenceUpdateTimeout) {
                referenceColorsRead.fill(false);
                outputReference(frameTime);
            }
            break;
        }
    } else {
        referenceUpdate = ReferenceUpdate::ShutterOff;
        referenceColorsRead.fill(false);
        finishOutput(frameTime, LogStream::Reference);
    }

    outputInstant(frameTime);
    outputFlags(frameTime);

    double cutoff = frameTime - 120;
    for (std::size_t i = 0; i < static_cast<std::size_t>(LogStream::TOTAL); i++) {
        if (!FP::defined(streamBeginTime[i])) {
            loggingMux.advance(i, frameTime, loggingEgress);
            continue;
        }
        if (streamBeginTime[i] > cutoff)
            continue;

#ifndef NDEBUG
        qCDebug(log) << "Log stream" << i << "stalled";
#endif

        streamBeginTime[i] = cutoff;
        loggingMux.advance(i, cutoff, loggingEgress);
    }

    switch (sampleState) {
    case SampleState::Spancheck:
        break;

    case SampleState::BlankExitZero:
        if (!FP::defined(stateEndTime) || frameTime > stateEndTime) {
            sampleState = SampleState::Measure;
            stateEndTime = FP::undefined();
            realtimeStateUpdated = true;
            break;
        }
        break;

    case SampleState::ZeroAccumulateEnterBlank:
        if (!FP::defined(stateEndTime)) {
            beginExplicitBlank(frameTime);
            stateEndTime = beginZeroFill(frameTime);
            realtimeStateUpdated = true;
            if (FP::defined(stateEndTime))
                break;
        }
        if (FP::defined(stateEndTime) && frameTime <= stateEndTime)
            break;

        sampleState = SampleState::ZeroAccumulate;
        stateEndTime = Time::logical(frameTime, config.front().zeroMeasureUnit,
                                     config.front().zeroMeasureCount,
                                     config.front().zeroMeasureAlign, true);
        realtimeStateUpdated = true;

        zeroT = ZeroAccumulator();
        zeroP = ZeroAccumulator();
        for (auto &clear : zeroBs) {
            clear.clear();
        }
        break;

    case SampleState::Measure:
        switch (values.majorState) {
        case Command_SystemStatus::Major_Normal:
            break;
        case Command_SystemStatus::Major_ZeroAdjust:
        case Command_SystemStatus::Major_ZeroCalibration:
            qCDebug(log) << "Detected instrument zero state at" << Logging::time(frameTime);

            sampleState = SampleState::ZeroInstrument;
            realtimeStateUpdated = true;

            for (std::size_t color = 0; color < zeroBs.size(); ++color) {
                Bsz[color].clear();
            }
            break;
        case Command_SystemStatus::Major_SpanCalibration:
            qCDebug(log) << "Detected instrument calibration state at" << Logging::time(frameTime);

            sampleState = SampleState::InstrumentCalibration;
            realtimeStateUpdated = true;

            for (std::size_t color = 0; color < zeroBs.size(); ++color) {
                Bsz[color].clear();
            }
            break;

        default:
            break;
        }
        break;

    case SampleState::ZeroInstrument:
        if (values.majorState != Command_SystemStatus::Major_Normal)
            break;

        if (config.front().zeroMode == Configuration::Zero::EnableFilter &&
                isInteractive() &&
                hasFeature(Feature::FilterControl)) {
            okCommandQueue.emplace_back(
                    "**" + QByteArray::number(config.front().address) + "PCF,N\n\r");
        }

        for (const auto &send : config.front().zeroDisableCommands) {
            if (state) {
                state->sendCommand(send.first, send.second);
            }
        }

        beginExplicitBlank(frameTime);
        sampleState = SampleState::BlankExitZero;
        stateEndTime = blankEndTime;
        realtimeStateUpdated = true;
        zeroReadPending = true;
        break;

    case SampleState::ZeroAccumulate:
        if (FP::defined(stateEndTime) && frameTime <= stateEndTime)
            break;

        finishZeroAccumulation(frameTime);
        completeZero(frameTime);

        okCommandQueue.emplace_back("DO" + QByteArray::number(config.front().address) + "010\r");

        for (const auto &send : config.front().zeroDisableCommands) {
            if (state) {
                state->sendCommand(send.first, send.second);
            }
        }

        beginExplicitBlank(frameTime);
        sampleState = SampleState::BlankExitZero;
        stateEndTime = blankEndTime;
        realtimeStateUpdated = true;
        break;

    case SampleState::InstrumentCalibration:
        if (values.majorState != Command_SystemStatus::Major_Normal)
            break;

        beginExplicitBlank(frameTime);
        sampleState = SampleState::BlankExitZero;
        stateEndTime = blankEndTime;
        realtimeStateUpdated = true;
        zeroReadPending = true;
        break;
    }
}

bool AcquireEcotechNephAurora::logScatteringsValid(double time) const
{
    if (FP::defined(blankEndTime) && blankEndTime >= time)
        return false;

    switch (sampleState) {
    case SampleState::Measure:
        break;

    case SampleState::ZeroInstrument:
    case SampleState::ZeroAccumulate:
    case SampleState::BlankExitZero:
    case SampleState::ZeroAccumulateEnterBlank:
    case SampleState::Spancheck:
    case SampleState::InstrumentCalibration:
        return false;
    }

    switch (values.majorState) {
    case Command_SystemStatus::Major_ZeroAdjust:
    case Command_SystemStatus::Major_SpanCalibration:
    case Command_SystemStatus::Major_ZeroCalibration:
    case Command_SystemStatus::Major_SystemCalibration:
    case Command_SystemStatus::Major_SpanCheck:
    case Command_SystemStatus::Major_ZeroCheck:
    case Command_SystemStatus::Major_EnvironmentalCalibration:
        return false;

    case Command_SystemStatus::Major_Normal:
    default:
        break;
    }

    return true;
}

static double backoutScattingSTP(double Bs,
                                 double T,
                                 double P,
                                 double reportingT,
                                 double reportingP = 1013.25)
{
    if (!FP::defined(Bs) || !FP::defined(reportingT) || !FP::defined(reportingP))
        return Bs;
    if (!FP::defined(T) || !FP::defined(P))
        return FP::undefined();

    if (T < 150.0)
        T += 273.15;
    if (T < 150.0 || T > 350.0)
        return FP::undefined();
    if (reportingT < 150.0)
        reportingT += 273.15;
    if (reportingT < 150.0 || reportingT > 350.0)
        return FP::undefined();

    if (P < 10.0 || P > 2000.0)
        return FP::undefined();
    if (reportingP < 10.0 || reportingP > 2000.0)
        return FP::undefined();

    return Bs * (P / reportingP) * (reportingT / T);
}

void AcquireEcotechNephAurora::outputOptical(double frameTime)
{
    if (!FP::defined(config.front().opticalUpdateTimeout)) {
        if (!hasFeature(Feature::Polar) || angles.empty()) {
            opticalUpdateTimeout = frameTime + 7.0;
        } else {
            opticalUpdateTimeout = frameTime + angles.size() * 5;
        }
    } else {
        opticalUpdateTimeout = frameTime + config.front().opticalUpdateTimeout;
    }

    auto outputValues = values;

    for (std::size_t color = 0; color < colorCodes.size(); color++) {
        if (!outputValues.Cr[color].read().exists()) {
            auto a = outputValues.Cs[color].read().toReal();
            auto b = outputValues.Cf[color].read().toReal();
            if (FP::defined(a) && FP::defined(b) && b != 0.0) {
                outputValues.Cr[color].write().setReal(a / b);
            }
            remap("Cr" + colorCodes[color], outputValues.Cr[color]);
        }
        if (!outputValues.Cbr[color].read().exists()) {
            auto a = outputValues.Cbs[color].read().toReal();
            auto b = outputValues.Cf[color].read().toReal();
            if (FP::defined(a) && FP::defined(b) && b != 0.0) {
                outputValues.Cbr[color].write().setReal(a / b);
            }
            remap("Cbr" + colorCodes[color], outputValues.Cbr[color]);
        }

        if (outputValues.Cs[color].read().exists()) {
            loggingValue(LogStream::Optical, frameTime, "Cs" + colorCodes[color],
                         outputValues.Cs[color]);

            spancheckController->update(NephelometerSpancheckController::MeasurementCounts,
                                        outputValues.Cs[color].read().toReal(), color, 0);
        }

        if (outputValues.Cbs[color].read().exists()) {
            loggingValue(LogStream::Optical, frameTime, "Cbs" + colorCodes[color],
                         outputValues.Cbs[color]);

            spancheckController->update(NephelometerSpancheckController::MeasurementCounts,
                                        outputValues.Cbs[color].read().toReal(), color, 90);
        }

        if (outputValues.Cr[color].read().exists()) {
            realtimeValue(frameTime, "Cr" + colorCodes[color], outputValues.Cr[color]);
        }

        if (outputValues.Cbr[color].read().exists()) {
            realtimeValue(frameTime, "Cbr" + colorCodes[color], outputValues.Cbr[color]);
        }

        CPD3::Data::Variant::Root Bs;
        CPD3::Data::Variant::Root Bbs;
        CPD3::Data::Variant::Root Bsn;

        if (!outputValues.Bs[color].empty()) {
            auto target = Bsn.write().toArray();
            for (auto &add : outputValues.Bs[color]) {
                auto effective = add.second.write();
                applyZeroOffset(effective, color, add.first);

                target.after_back().set(effective);

                spancheckController->update(NephelometerSpancheckController::Scattering,
                                            backoutScattingSTP(effective.toReal(),
                                                               values.T.read().toReal(),
                                                               values.P.read().toReal(),
                                                               reportingTemperature), color,
                                            add.first);

                if (zeroOpticalAccumulationActive()) {
                    zeroBs[color][add.first] += effective.toReal();
                }
            }

            if (std::fabs(outputValues.Bs[color].begin()->first) < 5) {
                Bs = outputValues.Bs[color].begin()->second;
            }
            if (std::fabs(outputValues.Bs[color].rbegin()->first - 90) < 5) {
                Bbs = outputValues.Bs[color].rbegin()->second;
            }
        }

        if (Bs.read().exists()) {
            realtimeValue(frameTime, "ZBs" + colorCodes[color], Bs);

            if (!logScatteringsValid(frameTime)) {
                loggingValue(LogStream::Optical, frameTime, "Bs" + colorCodes[color], {});
            } else {
                loggingValue(LogStream::Optical, frameTime, "Bs" + colorCodes[color],
                             std::move(Bs));
            }
        }

        if (Bbs.read().exists()) {
            realtimeValue(frameTime, "ZBbs" + colorCodes[color], Bbs);

            if (!logScatteringsValid(frameTime)) {
                loggingValue(LogStream::Optical, frameTime, "Bbs" + colorCodes[color], {});
            } else {
                loggingValue(LogStream::Optical, frameTime, "Bbs" + colorCodes[color],
                             std::move(Bbs));
            }
        }

        if (hasFeature(Feature::Polar) && Bsn.read().exists()) {
            realtimeValue(frameTime, "ZBsn" + colorCodes[color], Bsn);

            if (!logScatteringsValid(frameTime)) {
                loggingValue(LogStream::Optical, frameTime, "Bsn" + colorCodes[color], {});
            } else {
                loggingValue(LogStream::Optical, frameTime, "Bsn" + colorCodes[color],
                             std::move(Bsn));
            }
        }
    }

    if (outputValues.Cd.read().exists()) {
        loggingValue(LogStream::Optical, frameTime, "Cd", outputValues.Cd);

        spancheckController->update(NephelometerSpancheckController::DarkCounts,
                                    outputValues.Cd.read().toReal());
    }

    finishOutput(frameTime, LogStream::Optical);
}

void AcquireEcotechNephAurora::outputReference(double frameTime)
{
    if (!FP::defined(config.front().referenceUpdateTimeout)) {
        if (!hasFeature(Feature::Polar) || angles.empty()) {
            referenceUpdateTimeout = frameTime + 45.0;
        } else {
            referenceUpdateTimeout = frameTime + std::max<double>(angles.size() * 5, 45.0);
        }
    } else {
        referenceUpdateTimeout = frameTime + config.front().referenceUpdateTimeout;
    }

    for (std::size_t color = 0; color < colorCodes.size(); color++) {
        if (values.Cf[color].read().exists()) {
            loggingValue(LogStream::Reference, frameTime, "Cf" + colorCodes[color],
                         values.Cf[color]);

            spancheckController->update(NephelometerSpancheckController::ReferenceCounts,
                                        values.Cf[color].read().toReal(), color);
        }
    }

    finishOutput(frameTime, LogStream::Reference);
}

void AcquireEcotechNephAurora::outputInstant(double frameTime)
{
    if (values.T.read().exists()) {
        loggingValue(LogStream::Instant, frameTime, "T", values.T);

        spancheckController->update(NephelometerSpancheckController::Temperature,
                                    values.T.read().toReal());
    }

    if (values.Tx.read().exists()) {
        loggingValue(LogStream::Instant, frameTime, "Tx", values.Tx);
    }

    if (values.U.read().exists()) {
        loggingValue(LogStream::Instant, frameTime, "U", values.U);
    }

    if (values.P.read().exists()) {
        loggingValue(LogStream::Instant, frameTime, "P", values.P);

        spancheckController->update(NephelometerSpancheckController::Pressure,
                                    values.P.read().toReal());
    }

    if (zeroConditionAccumulationActive()) {
        zeroT += values.T.read().toReal();
        zeroP += values.P.read().toReal();
    }

    finishOutput(frameTime, LogStream::Instant);
}

void AcquireEcotechNephAurora::outputFlags(double frameTime)
{
    auto effectiveFlags = values.reportedFlags;
    switch (sampleState) {
    case SampleState::Measure:
        break;
    case SampleState::ZeroInstrument:
    case SampleState::ZeroAccumulate:
        effectiveFlags.insert("Zero");
        break;
    case SampleState::ZeroAccumulateEnterBlank:
    case SampleState::BlankExitZero:
        effectiveFlags.insert("Blank");
        break;
    case SampleState::Spancheck:
        effectiveFlags.insert("Spancheck");
        break;
    case SampleState::InstrumentCalibration:
        effectiveFlags.insert("Calibration");
        break;
    }

    if (FP::defined(reportingTemperature))
        effectiveFlags.insert("STP");
    if (inconsistentZero)
        effectiveFlags.insert("InconsistentZero");

    loggingValue(LogStream::Flags, frameTime, "F1", Variant::Root(std::move(effectiveFlags)));

    finishOutput(frameTime, LogStream::Flags);
}

void AcquireEcotechNephAurora::finishOutput(double frameTime, LogStream stream)
{
    auto index = static_cast<std::size_t>(stream);
    if (FP::defined(streamBeginTime[index]) && streamBeginTime[index] >= frameTime)
        return;

    streamBeginTime[index] = frameTime;
    loggingMux.advance(index, frameTime, loggingEgress);
}

void AcquireEcotechNephAurora::realtimeValue(double time,
                                             SequenceName::Component name,
                                             Variant::Root value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

void AcquireEcotechNephAurora::loggingValue(LogStream stream,
                                            double time,
                                            SequenceName::Component name,
                                            Variant::Root value)
{
    if (!loggingEgress) {
        if (!realtimeEgress)
            return;
        realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                    time, time + 1.0);
        return;
    }

    if (realtimeEgress) {
        realtimeEgress->emplaceData(SequenceIdentity({}, "raw", name), value, time, time + 1.0);
    }

    auto index = static_cast<std::size_t>(stream);
    if (!FP::defined(streamBeginTime[index]))
        return;
    if (streamBeginTime[index] >= time)
        return;

    loggingMux.incoming(index, SequenceValue(SequenceIdentity({}, "raw", std::move(name)),
                                             std::move(value), streamBeginTime[index], time),
                        loggingEgress);
}

static std::pair<Util::ByteView, Util::ByteView> splitEE(const Util::ByteArray &frame)
{
    /*
     * Lines are like:
     * CONFIGURATION REPORT=, 22/02/2013 14:36:25
     * Serial Number       =, 1
     * So we ignore leading spaces, trailing spaces and the "," after the =
     */

    const char *beginInput = frame.data<const char *>();
    const char *ptr = beginInput;
    const char *endInput = frame.data<const char *>() + frame.size();
    for (; ptr != endInput; ++ptr) {
        if (isPaddingSpace(*ptr))
            continue;
        break;
    }
    if (ptr == endInput)
        return {};

    const char *beginKey = ptr;
    const char *endKey = beginKey;
    for (; ptr != endInput; ++ptr) {
        if (*ptr == '=')
            break;
        if (!isPaddingSpace(*ptr)) {
            endKey = ptr;
        }
    }
    if (ptr == endInput)
        return {};

    std::size_t lenKey = (endKey - beginKey) + 1;
    if (lenKey < 4)
        return {};

    ++ptr;
    if (ptr == endInput)
        return {};
    if (*ptr == ',') {
        ++ptr;
        if (ptr == endInput)
            return {};
    }

    Util::ByteView key(beginKey, lenKey);

    for (; ptr != endInput; ++ptr) {
        if (isPaddingSpace(*ptr))
            continue;
        break;
    }
    if (ptr == endInput)
        return {std::move(key), {}};

    const char *beginValue = ptr;
    const char *endValue = ptr;
    for (++ptr; ptr != endInput; ++ptr) {
        if (!isPaddingSpace(*ptr)) {
            endValue = ptr;
        }
    }

    std::size_t lenValue = (endValue - beginValue) + 1;
    Util::ByteView value(beginValue, lenValue);

    return {std::move(key), std::move(value)};
}

void AcquireEcotechNephAurora::processEELine(const Util::ByteArray &frame, double frameTime)
{
    auto split = splitEE(frame);
    if (split.first.empty()) {
        if (responseState == ResponseState::Interactive_Start_ReadEE) {
            qCDebug(log) << "Invalid EE line" << frame << "during startup at"
                         << Logging::time(frameTime);

            spancheckController->terminate();
            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            outstandingCommands.clear();
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            return;
        }

        qCDebug(log) << "Invalid EE line" << frame << "in state" << static_cast<int>(responseState)
                     << "at" << Logging::time(frameTime);

        {
            auto info = describeState();
            info["Line"].setString(frame.toString());
            event(frameTime, QObject::tr("Invalid EE read response.  Communications dropped."),
                  true, info);
        }

        spancheckController->terminate();
        invalidateLogValues(frameTime);

        responseState = ResponseState::Interactive_Restart_Wait;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        outstandingCommands.clear();
        autoprobeValidRecords = 0;
        generalStatusUpdated();

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                  frameTime, FP::undefined()));
        }
        return;
    }

    lastEEResults["Lines"].toArray().after_back().setString(frame.toString());
    lastEEResults["Values"].hash(split.first.toString()).setString(split.second.toString());

    if (!FP::defined(readEEAbsoluteTimeout)) {
        readEEAbsoluteTimeout = frameTime + 30;
    } else if (frameTime > readEEAbsoluteTimeout) {
        if (responseState == ResponseState::Interactive_Start_ReadEE) {
            qCDebug(log) << "Startup read EE took too long at" << Logging::time(frameTime);

            spancheckController->terminate();
            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            outstandingCommands.clear();
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            return;
        }

        qCDebug(log) << "EE read timeout in state" << static_cast<int>(responseState) << "at"
                     << Logging::time(frameTime);

        {
            auto info = describeState();
            info["Line"].setString(frame.toString());
            event(frameTime, QObject::tr("Excessive EE read response.  Communications dropped."),
                  true, info);
        }

        spancheckController->terminate();
        invalidateLogValues(frameTime);

        responseState = ResponseState::Interactive_Restart_Wait;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        outstandingCommands.clear();
        autoprobeValidRecords = 0;
        generalStatusUpdated();

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                  frameTime, FP::undefined()));
        }
        return;
    }

    /* Restart the end timeout */
    timeoutAt(frameTime + 0.5);

    if (split.first.string_equal_insensitive("Angle List")) {
        auto list = split.second.split(',');
        if (!list.empty()) {
            auto oldAngles = std::move(angles);
            angles.clear();
            for (const auto &raw : list) {
                bool ok = false;
                double angle = raw.parse_real(&ok);
                if (!ok || !FP::defined(angle) || angle < 0 || angle > 90) {
                    qCDebug(log) << "Ignoring invalid angle" << raw;
                    continue;
                }
                angles.push_back(angle);
            }

            if (angles != oldAngles) {
                haveEmittedRealtimeMeta = false;
                haveEmittedLogMeta = false;
            }

            switch (angles.size()) {
            case 0:
                if (hasFeature(Feature::Polar)) {
                    setFeature(Feature::Polar, false);
                    haveEmittedRealtimeMeta = false;
                    haveEmittedLogMeta = false;
                }
                break;
            case 1:
                if (angles.front() != 0) {
                    if (!hasFeature(Feature::Polar)) {
                        setFeature(Feature::Polar, true);
                        haveEmittedRealtimeMeta = false;
                        haveEmittedLogMeta = false;
                    }
                }
                break;
            case 2:
                if (angles.front() != 0 || angles.back() != 90) {
                    if (!hasFeature(Feature::Polar)) {
                        setFeature(Feature::Polar, true);
                        haveEmittedRealtimeMeta = false;
                        haveEmittedLogMeta = false;
                    }
                }
                break;
            default:
                if (!hasFeature(Feature::Polar)) {
                    setFeature(Feature::Polar, true);
                    haveEmittedRealtimeMeta = false;
                    haveEmittedLogMeta = false;
                }
                break;
            }

            lastEEResults["Angles"].setEmpty();
            auto target = lastEEResults["Angles"].toArray();
            for (const auto &add : angles) {
                target.after_back().setReal(add);
            }
        } else {
            qCDebug(log) << "Ignoring empty angles list" << frame;
        }

        markEESeen(EEComponent::Angles);
        return;
    }

    if (split.first.string_equal_insensitive("Temperature Unit")) {
        if (!split.second.empty()) {
            auto units = std::tolower(static_cast<char>(split.second.back()));
            if (units != 'c' && units != 'k') {
                qCDebug(log) << "Invalid temperature unit in" << frame << ":" << units;

                auto info = describeState();
                info["Line"].setString(frame.toString());
                event(frameTime,
                      QObject::tr("Reported temperature unit (\"%1\") is not recognized.").arg(
                              split.second.toQString()), true, info);
            }

            lastEEResults["TemperatureUnit"].setString(std::string(units, 1));
        }

        markEESeen(EEComponent::TemperatureUnit);
        return;
    }

    if (split.first.string_equal_insensitive("AtmPressureUnit")) {
        if (!split.second.empty()) {
            if (!split.second.string_equal_insensitive("mb") &&
                    split.second.string_equal_insensitive("mbar") &&
                    split.second.string_equal_insensitive("hPa")) {
                qCDebug(log) << "Invalid pressure unit in" << frame << ":" << split.second;

                auto info = describeState();
                info["Line"].setString(frame.toString());
                event(frameTime,
                      QObject::tr("Reported pressure unit (\"%1\") is not recognized.").arg(
                              split.second.toQString()), true, info);
            }

            lastEEResults["PressureUnit"].setString(split.second.toString());
        }

        markEESeen(EEComponent::PressureUnit);
        return;
    }

    if (split.first.string_equal_insensitive("Normalise To") ||
            split.first.string_equal_insensitive("Normalize To")) {
        double temperature = FP::undefined();
        if (!split.second.empty() && !split.second.string_equal_insensitive("None")) {
            auto contents = split.second.toQByteArray();
            for (int final = 0, max = contents.size(); final < max; ++final) {
                if (isDigit(contents[final]))
                    continue;
                if (contents[final] == '.')
                    continue;
                contents.resize(final);
                break;
            }

            bool ok = false;
            temperature = contents.toDouble(&ok);
            if (!ok) {
                qCDebug(log) << "Ignoring invalid normalization temperature in" << frame << ":"
                             << split.second;
                temperature = FP::undefined();
            }

            if (FP::defined(temperature) && temperature > 150)
                temperature -= 273.15;

            lastEEResults["NormalizationTemperature"].setReal(temperature);
        }

        if (!FP::equal(reportingTemperature, temperature)) {
            haveEmittedRealtimeMeta = false;
            haveEmittedLogMeta = false;
            reportingTemperature = temperature;
        }

        markEESeen(EEComponent::ReportingTemperature);
        return;
    }

    if (split.first.string_equal_insensitive("Filtering Method")) {
        if (!split.second.empty()) {
            bool instrumentFilterActive = false;

            switch (static_cast<char>(split.second.front())) {
            case 'K':
            case 'k':
            case 'M':
            case 'm':
            case 'A':
            case 'a':
                instrumentFilterActive = true;
            default:
                instrumentFilterActive = false;
                break;
            }

            if (hasFeature(Feature::FilterControl)) {
                inconsistentZero = false;
            } else {
                switch (config.front().zeroMode) {
                case Configuration::Zero::Native:
                    inconsistentZero = !instrumentFilterActive;
                    break;
                case Configuration::Zero::EnableFilter:
                    inconsistentZero = !instrumentFilterActive;
                    break;
                case Configuration::Zero::Offset:
                    inconsistentZero = instrumentFilterActive;
                    break;
                }
            }
        }

        markEESeen(EEComponent::FilteringMethod);
        return;
    }

    if (split.first.string_equal_insensitive("Wavelength 1")) {
        if (!split.second.empty()) {
            auto contents = split.second.toQByteArray();
            for (int final = 0, max = contents.size(); final < max; ++final) {
                if (isDigit(contents[final]))
                    continue;
                contents.resize(final);
                break;
            }

            bool ok = false;
            auto wl = contents.toDouble(&ok);
            if (ok && FP::defined(wl) && wl > 50 && wl < 9999) {
                reportedWavelengths[2] = wl;
                if (!FP::defined(config.front().wavelengths[2]) &&
                        !FP::equal(wl, effectiveWavelengths[2])) {
                    effectiveWavelengths[2] = wl;
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;

                    spancheckController->setWavelength(2, effectiveWavelengths[2], "R");
                }
            } else {
                qCDebug(log) << "Ignoring invalid wavelength in" << frame << ":" << split.second;
            }

            lastEEResults["Wavelengths"].array(2).setReal(wl);
        }

        markEESeen(EEComponent::WavelengthRed);
        return;
    }

    if (split.first.string_equal_insensitive("Wavelength 2")) {
        if (!split.second.empty()) {
            auto contents = split.second.toQByteArray();
            for (int final = 0, max = contents.size(); final < max; ++final) {
                if (isDigit(contents[final]))
                    continue;
                contents.resize(final);
                break;
            }

            bool ok = false;
            auto wl = contents.toDouble(&ok);
            if (ok && FP::defined(wl) && wl > 50 && wl < 9999) {
                reportedWavelengths[1] = wl;
                if (!FP::defined(config.front().wavelengths[1]) &&
                        !FP::equal(wl, effectiveWavelengths[1])) {
                    effectiveWavelengths[1] = wl;
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;

                    spancheckController->setWavelength(1, effectiveWavelengths[1], "G");
                }
            } else {
                qCDebug(log) << "Ignoring invalid wavelength in" << frame << ":" << split.second;
            }

            lastEEResults["Wavelengths"].array(1).setReal(wl);
        }

        markEESeen(EEComponent::WavelengthGreen);
        return;
    }

    if (split.first.string_equal_insensitive("Wavelength 3")) {
        if (!split.second.empty()) {
            auto contents = split.second.toQByteArray();
            for (int final = 0, max = contents.size(); final < max; ++final) {
                if (isDigit(contents[final]))
                    continue;
                contents.resize(final);
                break;
            }

            bool ok = false;
            auto wl = contents.toDouble(&ok);
            if (ok && FP::defined(wl) && wl > 50 && wl < 9999) {
                reportedWavelengths[0] = wl;
                if (!FP::defined(config.front().wavelengths[0]) &&
                        !FP::equal(wl, effectiveWavelengths[0])) {
                    effectiveWavelengths[0] = wl;
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;

                    spancheckController->setWavelength(0, effectiveWavelengths[0], "B");
                }
            } else {
                qCDebug(log) << "Ignoring invalid wavelength in" << frame << ":" << split.second;
            }

            lastEEResults["Wavelengths"].array(0).setReal(wl);
        }

        markEESeen(EEComponent::WavelengthBlue);
        return;
    }

    if (split.first.string_equal_insensitive("Cal ZeroAdj Temp")) {
        bool ok = false;
        if (!split.second.empty()) {
            Tw.write().setReal(split.second.parse_real(&ok));
            if (!ok) {
                qCDebug(log) << "Ignoring invalid zero temperature in" << frame << ":"
                             << split.second;
                Tw.write().setReal(FP::undefined());
            }

            lastEEResults["ZeroTemperature"].set(Tw);
        } else {
            Tw.write().setReal(FP::undefined());
        }
        remap("Tw", Tw);
        adjustTemperature(Tw.write());

        markEESeen(EEComponent::ZeroTemperature);
        return;
    }

    if (split.first.string_equal_insensitive("Cal ZeroAdj Pressure")) {
        bool ok = false;
        Pw.write().setReal(split.second.parse_real(&ok));
        if (!split.second.empty()) {
            if (!ok) {
                qCDebug(log) << "Ignoring invalid zero pressure in" << frame << ":" << split.second;
                Pw.write().setReal(FP::undefined());
            }

            lastEEResults["ZeroPressure"].set(Pw);
        } else {
            Pw.write().setReal(FP::undefined());
        }
        remap("Pw", Pw);

        markEESeen(EEComponent::ZeroPressure);
        return;
    }

    if (split.first.string_start_insensitive("Calibration Ms")) {
        int color = -1;
        if (split.first.size() > 14) {
            bool ok = false;
            color = split.first.mid(14).toQByteArray().trimmed().toInt(&ok);
            if (!ok)
                color = -1;
        }

        if (color < 1 || color > 3) {
            qCDebug(log) << "Ignoring malformed calibration slope in" << frame << ":"
                         << split.second;
            return;
        }

        auto list = split.second.split(',');
        auto &output = calM[3 - color];
        if (!list.empty()) {
            output.clear();
            bool haveMalformed = false;
            for (const auto &raw : list) {
                bool ok = false;
                double value = raw.toQByteArray().trimmed().toDouble(&ok);
                if (!ok || !FP::defined(value) || value < -1 || value >= 99) {
                    haveMalformed = true;
                    value = FP::undefined();
                }
                output.emplace_back(value);
            }

            if (haveMalformed) {
                qCDebug(log) << "Malformed calibration slope in" << frame << ":" << split.second;
                return;
            }

            auto target = lastEEResults["CalM"].array(3 - color).toArray();
            for (const auto &add : output) {
                target.after_back().setReal(add);
            }
        }

        switch (color) {
        case 1:
            markEESeen(EEComponent::CalMRed);
            break;
        case 2:
            markEESeen(EEComponent::CalMGreen);
            break;
        case 3:
            markEESeen(EEComponent::CalMBlue);
            break;
        default:
            Q_ASSERT(false);
            break;
        }


        return;
    }

    if (split.first.string_start_insensitive("Calibration Cs")) {
        int color = -1;
        if (split.first.size() > 14) {
            bool ok = false;
            color = split.first.mid(14).toQByteArray().trimmed().toInt(&ok);
            if (!ok)
                color = -1;
        }

        if (color < 1 || color > 3) {
            qCDebug(log) << "Ignoring malformed calibration intercept in" << frame << ":"
                         << split.second;
            return;
        }

        auto list = split.second.toQByteArrayRef().split(',');
        auto &output = calC[3 - color];
        if (!list.empty()) {
            output.clear();
            bool haveMalformed = false;
            for (const auto &raw : list) {
                bool ok = false;
                double value = raw.trimmed().toDouble(&ok);
                if (!ok || !FP::defined(value) || value < -1 || value >= 99) {
                    haveMalformed = true;
                    value = FP::undefined();
                }
                output.emplace_back(value);
            }

            if (haveMalformed) {
                qCDebug(log) << "Malformed calibration intercept in" << frame << ":"
                             << split.second;
                return;
            }

            auto target = lastEEResults["CalC"].array(3 - color).toArray();
            for (const auto &add : output) {
                target.after_back().setReal(add);
            }
        }

        switch (color) {
        case 1:
            markEESeen(EEComponent::CalCRed);
            break;
        case 2:
            markEESeen(EEComponent::CalCGreen);
            break;
        case 3:
            markEESeen(EEComponent::CalCBlue);
            break;
        default:
            Q_ASSERT(false);
            break;
        }


        return;
    }

    if (split.first.string_start_insensitive("Cal ZeroAdj Xs")) {
        int color = -1;
        if (split.first.size() > 14) {
            bool ok = false;
            color = split.first.mid(14).toQByteArray().trimmed().toInt(&ok);
            if (!ok)
                color = -1;
        }

        if (color < 1 || color > 3) {
            qCDebug(log) << "Ignoring malformed zero extinction in" << frame << ":" << split.second;
            return;
        }

        auto list = split.second.split(',');
        auto &output = zeroAdjX[3 - color];
        if (!list.empty()) {
            output.clear();
            bool haveMalformed = false;
            for (const auto &raw : list) {
                bool ok = false;
                double value = raw.toQByteArray().trimmed().toDouble(&ok);
                if (!ok || !FP::defined(value) || value < -100 || value >= 9999) {
                    haveMalformed = true;
                    value = FP::undefined();
                }
                output.emplace_back(value);
            }

            if (haveMalformed) {
                qCDebug(log) << "Malformed zero extinction in" << frame << ":" << split.second;
                return;
            }

            auto target = lastEEResults["ZeroAdjX"].array(3 - color).toArray();
            for (const auto &add : output) {
                target.after_back().setReal(add);
            }
        }

        switch (color) {
        case 1:
            markEESeen(EEComponent::ZeroXRed);
            break;
        case 2:
            markEESeen(EEComponent::ZeroXGreen);
            break;
        case 3:
            markEESeen(EEComponent::ZeroXBlue);
            break;
        default:
            Q_ASSERT(false);
            break;
        }


        return;
    }

    if (split.first.string_start_insensitive("Calibration Walls")) {
        int color = -1;
        if (split.first.size() > 17) {
            bool ok = false;
            color = split.first.mid(17).toQByteArray().trimmed().toInt(&ok);
            if (!ok)
                color = -1;
        }

        if (color < 1 || color > 3) {
            qCDebug(log) << "Ignoring malformed calibration wall in" << frame << ":"
                         << split.second;
            return;
        }

        auto list = split.second.split(',');
        auto &output = calWall[3 - color];
        if (!list.empty()) {
            output.clear();
            bool haveMalformed = false;
            for (const auto &raw : list) {
                bool ok = false;
                double value = raw.toQByteArray().trimmed().toDouble(&ok);
                if (!ok || !FP::defined(value) || value < 1 || value >= 110) {
                    haveMalformed = true;
                    value = FP::undefined();
                }
                output.emplace_back(value);
            }

            if (haveMalformed) {
                qCDebug(log) << "Malformed calibration wall in" << frame << ":" << split.second;
                return;
            }

            auto target = lastEEResults["Wall"].array(3 - color).toArray();
            for (const auto &add : output) {
                target.after_back().setReal(add);
            }
        }

        switch (color) {
        case 1:
            markEESeen(EEComponent::WallRed);
            break;
        case 2:
            markEESeen(EEComponent::WallGreen);
            break;
        case 3:
            markEESeen(EEComponent::WallBlue);
            break;
        default:
            Q_ASSERT(false);
            break;
        }


        return;
    }

    if (haveSeenAllEE()) {
        double checkTimeout = frameTime + 2.0;
        if (readEEAbsoluteTimeout > checkTimeout)
            readEEAbsoluteTimeout = checkTimeout;
    }
}

static double calculateBswMC(double M, double C)
{
    if (!FP::defined(M))
        return FP::undefined();
    if (!FP::defined(C))
        return FP::undefined();
    if (std::fabs(M) < 1E-9)
        return FP::undefined();
    return C / M;
}

static double calculateBswWall(double wall, double Bsr)
{
    if (!FP::defined(wall) || wall <= 1.0)
        return FP::undefined();
    if (!FP::defined(Bsr))
        return FP::undefined();
    wall /= 100.0;
    return (wall * Bsr) / (1.0 - wall);
}

void AcquireEcotechNephAurora::finalizeEE(double frameTime)
{
    realtimeEEUpdated = true;
    persistentEEUpdated = true;

    for (std::size_t color = 0; color < zeroAdjX.size(); color++) {
        const auto &adjX = zeroAdjX[color];
        const auto &M = calM[color];
        const auto &C = calC[color];
        const auto &wall = calWall[color];
        std::size_t angleCount = adjX.size();
        if (angleCount > M.size())
            angleCount = M.size();
        if (angleCount > C.size())
            angleCount = C.size();
        if (angleCount > wall.size())
            angleCount = wall.size();
        for (std::size_t angleIndex = 0; angleIndex < angleCount; ++angleIndex) {
            double angle = FP::undefined();
            if (angleIndex < angles.size()) {
                angle = angles[angleIndex];
            } else {
                if (angleIndex == 0 && (angleCount == 1 || angleCount == 2)) {
                    angle = 0;
                } else if (angleIndex == 1 && angleCount == 2) {
                    angle = 90;
                }
            }
            if (!FP::defined(angle))
                break;

            double t = Tw.read().toReal();
            double p = Pw.read().toReal();
            double Bsr = adjX[angleIndex];

            double Bsrc =
                    Algorithms::Rayleigh::angleScattering(effectiveWavelengths[color], angle, 180.0,
                                                          Algorithms::Rayleigh::Air, t, p);
            if (FP::defined(Bsrc)) {
                double div = std::max(std::fabs(Bsr), std::fabs(Bsrc));
                if (div < 0.01)
                    div = 0.01;
                div = (Bsr - Bsrc) / div;
                if (std::fabs(div) > 0.03) {
                    Variant::Write info = Variant::Write::empty();
                    info.hash("Bsr").setDouble(Bsr);
                    info.hash("Bsrc").setDouble(Bsrc);
                    info.hash("T").setDouble(t);
                    info.hash("P").setDouble(p);
                    info.hash("Difference").setDouble(div);
                    info.hash("Wavelength").setDouble(effectiveWavelengths[color]);
                    info.hash("Angle").setDouble(angle);
                    event(frameTime, QObject::tr(
                            "Discrepancy of %1% detected in Rayleigh scattering calculation.").arg(
                            QString::number(div * 100.0, 'f', 1)), false, info);

                    qCDebug(log) << "Rayleigh scattering calculation discrepancy (expected" << Bsrc
                                 << "got" << Bsr << "for" << effectiveWavelengths[color] << ","
                                 << angle << "at" << t << "," << p << ")";
                }

                Bsr = Bsrc;
            }

            double Bsw_MC = calculateBswMC(M[angleIndex], C[angleIndex]);
            double Bsw_Wall = calculateBswWall(wall[angleIndex], Bsr);

            if (FP::defined(Bsw_Wall)) {
                double div = std::max(std::fabs(Bsw_MC), std::fabs(Bsw_Wall));
                if (div < 0.01)
                    div = 0.01;
                div = (Bsr - Bsrc) / div;
                if (std::fabs(div) > 0.03) {
                    Variant::Write info = Variant::Write::empty();
                    info.hash("BswMC").setDouble(Bsw_MC);
                    info.hash("BswWall").setDouble(Bsw_Wall);
                    info.hash("M").setDouble(M[angleIndex]);
                    info.hash("C").setDouble(C[angleIndex]);
                    info.hash("wall").setDouble(wall[angleIndex]);
                    info.hash("Bsr").setDouble(Bsr);
                    info.hash("Difference").setDouble(div);
                    info.hash("Wavelength").setDouble(effectiveWavelengths[color]);
                    info.hash("Angle").setDouble(angle);
                    event(frameTime,
                          QObject::tr("Discrepancy of %1% detected in wall scattering calculation.")
                                  .arg(QString::number(div * 100.0, 'f', 1)), false, info);

                    qCDebug(log) << "Wall scattering calculation discrepancy (calibration:"
                                 << Bsw_MC << "wall:" << Bsw_Wall << "for"
                                 << effectiveWavelengths[color] << "," << angle << ")";
                }
            } else {
                Bsw_MC = Bsw_Wall;
            }

            Bsw[color][angle].write().setReal(Bsw_MC);

            remap("ZBsw" + std::to_string(static_cast<int>(angle)) + colorCodes[color],
                  Bsw[color][angle]);
        }

        if (!Bsw[color].empty()) {
            if (std::fabs(Bsw[color].begin()->first) < 5) {
                remap("Bsw" + colorCodes[color], Bsw[color].begin()->second);
            }
            if (std::fabs(Bsw[color].rbegin()->first - 90) < 5) {
                remap("Bbsw" + colorCodes[color], Bsw[color].rbegin()->second);
            }
        }

        qCDebug(log) << "Zero wall scatterings for" << color << "set to" << Bsw[color];
    }

    if (!FP::defined(zeroEffectiveTime)) {
        zeroEffectiveTime = frameTime;
    }

    zeroReadPending = false;
    realtimeZeroUpdated = true;
    persistentZeroUpdated = true;
}

bool AcquireEcotechNephAurora::haveSeenAllEE() const
{
    for (std::size_t i = 0; i < seenEEComponents.size(); i++) {
        if (seenEEComponents[i])
            continue;

        switch (static_cast<EEComponent>(i)) {
        case EEComponent::Angles:
            if (hasFeature(Feature::Polar))
                return false;
            break;

        default:
            return false;
        }
    }

    return true;
}

bool AcquireEcotechNephAurora::haveSeenAnyEE() const
{
    for (auto check : seenEEComponents) {
        if (check)
            return true;
    }
    return false;
}

void AcquireEcotechNephAurora::beginExplicitBlank(double time)
{
    Q_ASSERT(!config.empty());

    if (!FP::defined(time))
        time = lastRecordTime;

    double endTime = Time::logical(time, config.front().blankUnit, config.front().blankCount,
                                   config.front().blankAlign, true);
    if (!FP::defined(blankEndTime) || endTime > blankEndTime)
        blankEndTime = endTime;
}

double AcquireEcotechNephAurora::beginZeroFill(double time)
{
    Q_ASSERT(!config.empty());

    if (!FP::defined(time))
        time = lastRecordTime;

    double endTime = Time::logical(time, config.front().zeroFillUnit, config.front().zeroFillCount,
                                   config.front().zeroFillAlign, true);
    if (!FP::defined(blankEndTime) || endTime > blankEndTime)
        blankEndTime = endTime;

    return endTime;
}

bool AcquireEcotechNephAurora::requestZero()
{
    Q_ASSERT(!config.empty());

    for (const auto &send : config.front().zeroEnableCommands) {
        if (state) {
            state->sendCommand(send.first, send.second);
        }
    }

    switch (config.front().zeroMode) {
    case Configuration::Zero::Offset:
        okCommandQueue.emplace_back("DO" + QByteArray::number(config.front().address) + "011\r");

        stateEndTime = FP::undefined();
        sampleState = SampleState::ZeroAccumulateEnterBlank;
        realtimeStateUpdated = true;
        return true;

    case Configuration::Zero::EnableFilter:
        if (!hasFeature(Feature::FilterControl)) {
            qCDebug(log) << "Filter control not supported, so the data filter mode was not changed";
        } else {
            Util::ByteArray send("**");
            send += QByteArray::number(config.front().address);
            send += "PCF,";
            switch (config.front().filterMode) {
            case Configuration::Filter::None:
                send += "N";
                break;
            case Configuration::Filter::Kalman:
                send += "K";
                break;
            case Configuration::Filter::Average:
                send += "M";
                break;
            }
            send += "\n\r";
            okCommandQueue.emplace_back(std::move(send));
        }
        /* Fall through */
    case Configuration::Zero::Native:
        okCommandQueue.emplace_back("**" + QByteArray::number(config.front().address) + "J5\r");
        break;
    }

    return false;
}

void AcquireEcotechNephAurora::applyZeroOffset(Data::Variant::Write &value,
                                               std::size_t color,
                                               double angle)
{
    switch (sampleState) {
    case SampleState::Measure:
    case SampleState::Spancheck:
        break;

    case SampleState::ZeroAccumulateEnterBlank:
    case SampleState::ZeroAccumulate:
    case SampleState::ZeroInstrument:
    case SampleState::BlankExitZero:
    case SampleState::InstrumentCalibration:
        /* No subtraction in zero states */
        return;
    }

    Q_ASSERT(!config.empty());

    switch (config.front().zeroMode) {
    case Configuration::Zero::Native:
    case Configuration::Zero::EnableFilter:
        return;

    case Configuration::Zero::Offset:
        break;
    }

    Q_ASSERT(color >= 0 && color <= Bsz.size());

    auto check = Bsz[color].find(angle);
    if (check == Bsz[color].end())
        return;

    double base = value.toReal();
    if (!FP::defined(base))
        return;

    double sub = check->second.read().toReal();
    if (!FP::defined(sub))
        return;

    value.setReal(base - sub);
}

void AcquireEcotechNephAurora::finishZeroAccumulation(double)
{
    Tw.write().setReal(zeroT.value());
    remap("Tw", Tw);
    adjustTemperature(Tw.write());

    Pw.write().setReal(zeroP.value());
    remap("Pw", Pw);
    adjustTemperature(Tw.write());

    for (std::size_t color = 0; color < zeroBs.size(); ++color) {
        Bsz[color].clear();
        for (const auto &Bs : zeroBs[color]) {
            Bsz[color][Bs.first].write().setReal(Bs.second.value());

            remap("ZBsz" + std::to_string(static_cast<int>(Bs.first)) + colorCodes[color],
                  Bsz[color][Bs.first]);
        }

        if (!Bsz[color].empty()) {
            if (std::fabs(Bsz[color].begin()->first) < 5) {
                remap("Bsz" + colorCodes[color], Bsz[color].begin()->second);
            }
            if (std::fabs(Bsz[color].rbegin()->first - 90) < 5) {
                remap("Bbsz" + colorCodes[color], Bsz[color].rbegin()->second);
            }
        }

        qCDebug(log) << "Zero offsets for" << color << "set to" << Bsz[color];
    }

    realtimeZeroUpdated = true;
    persistentZeroUpdated = true;
}

void AcquireEcotechNephAurora::updateEffectiveZero()
{
    for (std::size_t color = 0; color < Bsw.size(); ++color) {
        auto &target = effectiveBsw[color];

        switch (config.front().zeroMode) {
        case Configuration::Zero::Native:
        case Configuration::Zero::EnableFilter:
            target = Bsw[color];
            break;

        case Configuration::Zero::Offset: {
            target.clear();
            for (const auto &base : Bsw[color]) {
                auto offset = Bsz[color].find(base.first);
                if (offset == Bsz[color].end()) {
                    target.emplace(base.first, base.second);
                    continue;
                }
                double original = base.second.read().toReal();
                double add = offset->second.read().toReal();
                if (!FP::defined(original) || !FP::defined(add)) {
                    target.emplace(base.first, base.second);
                    continue;
                }

                target.emplace(base.first, Variant::Root(original + add));
            }
            break;
        }
        }
    }
}

static double calculateBswd(double prior, double current)
{
    if (!FP::defined(prior) || !FP::defined(current))
        return FP::undefined();
    return current - prior;
}

void AcquireEcotechNephAurora::completeZero(double time)
{
    auto priorBsw = effectiveBsw;

    if (!config.empty() && config.front().zeroMode != Configuration::Zero::Offset) {
        for (auto &reset : Bsz) {
            reset.clear();
        }
    }

    updateEffectiveZero();

    zeroEffectiveTime = time;

    for (std::size_t color = 0; color < effectiveBsw.size(); ++color) {
        Bswd[color].clear();
        for (const auto &angle : effectiveBsw[color]) {
            double current = angle.second.read().toReal();
            double prior = priorBsw[color][angle.first].read().toReal();

            Bswd[color].emplace(angle.first, Variant::Root(calculateBswd(prior, current)));
        }
    }
}

void AcquireEcotechNephAurora::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    /* If this is a busy frame, enter or continue the retry process */
    if (isBusyFrame(frame)) {
        switch (responseState) {
        case ResponseState::Interactive_Run_Active:
        case ResponseState::Interactive_Run_Resume:
        case ResponseState::Interactive_Run_Delay:
            if (!FP::defined(commandRetryAbsoluteTimeout) && FP::defined(frameTime)) {
                configAdvance(frameTime);

                commandRetryAbsoluteTimeout = frameTime + config.front().retryBusyTimeout;
                qCDebug(log) << "Starting busy retry sequence for command"
                             << static_cast<int>(commandState) << "at" << Logging::time(frameTime);
            }
            /* Fall through */
        case ResponseState::Interactive_Run_Retry:
            responseState = ResponseState::Interactive_Run_Retry;
            break;

        case ResponseState::Interactive_Zero_ReadEE:
        case ResponseState::Interactive_Zero_ReadEE_Resume:
            if (!FP::defined(commandRetryAbsoluteTimeout) && FP::defined(frameTime)) {
                configAdvance(frameTime);

                commandRetryAbsoluteTimeout = frameTime + config.front().retryBusyTimeout;
                qCDebug(log) << "Starting busy EE retry sequence at" << Logging::time(frameTime);
            }
            /* Fall through */
        case ResponseState::Interactive_Zero_ReadEE_Retry:
            responseState = ResponseState::Interactive_Zero_ReadEE_Retry;
            break;

        case ResponseState::Passive_Run:
        case ResponseState::Passive_Wait:
        case ResponseState::Passive_Initialize:
        case ResponseState::Autoprobe_Passive_Wait:
        case ResponseState::Autoprobe_Passive_Initialize:
            return;

        case ResponseState::Interactive_Start_ReadID:
            Q_ASSERT(FP::defined(frameTime));

            configAdvance(frameTime);
            if (!FP::defined(commandRetryAbsoluteTimeout)) {
                commandRetryAbsoluteTimeout = frameTime + 30;

                discardData(frameTime + 2.0);
                timeoutAt(frameTime + 10.0);
                return;
            } else {
                if (commandRetryAbsoluteTimeout < frameTime) {
                    discardData(frameTime + 2.0);
                    timeoutAt(frameTime + 2.0);
                    return;
                }
            }

            /* Fall through */

            /* All interactive start states, so no log message */
        default: {
            Q_ASSERT(isInteractiveStart());

            qCDebug(log) << "Invalid busy response in state" << static_cast<int>(responseState)
                         << "at" << Logging::time(frameTime);

            spancheckController->terminate();
            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            outstandingCommands.clear();
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            return;
        }
        }

        Q_ASSERT(FP::defined(frameTime));
        Q_ASSERT(isInteractive());
        Q_ASSERT(!isInteractiveStart());

        configAdvance(frameTime);

        if (FP::defined(commandRetryAbsoluteTimeout) && frameTime > commandRetryAbsoluteTimeout) {
            return retryBusyExhausted(frameTime);
        }

        /* Discard anything pending and try again */
        discardData(frameTime + config.front().retryDelay);
        timeoutAt(frameTime + config.front().retryDelay + 10.0);
        outstandingCommands.clear();
        return;
    }

    /* Check to see if this is a retry completion, or a fixed form response */
    switch (responseState) {
    case ResponseState::Interactive_Run_Retry:
    case ResponseState::Interactive_Zero_ReadEE_Retry:
        if (Command_SystemStatus::parse(frame) != 0) {
            ++commandRetryFailureCounter;
            if (commandRetryFailureCounter <= config.front().retryFailureMaximum) {
                discardData(frameTime + config.front().retryDelay);
                timeoutAt(frameTime + config.front().retryDelay + 10.0);
                outstandingCommands.clear();
                return;
            }

            qCDebug(log) << "Invalid retry status response" << frame << "in state"
                         << static_cast<int>(responseState) << "at" << Logging::time(frameTime);

            {
                auto info = describeState();
                info["Line"].setString(frame.toString());
                event(frameTime,
                      QObject::tr("Invalid command status response.  Communications dropped."),
                      true, info);
            }

            spancheckController->terminate();
            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(frameTime + 10.0);
            discardData(frameTime + 1.0);
            outstandingCommands.clear();
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            return;
        }

        /* Got a valid status response, so now do a discard to throw out anything that
         * may be queued, before sending the actual final response */

        if (responseState == ResponseState::Interactive_Run_Retry)
            responseState = ResponseState::Interactive_Run_Resume;
        else
            responseState = ResponseState::Interactive_Zero_ReadEE_Resume;

        timeoutAt(frameTime + 20.0);
        discardData(frameTime + 2.0);
        return;

    case ResponseState::Interactive_Start_ReadID: {
        auto str = QString::fromUtf8(frame.toQByteArray().trimmed());
        if (str == "ERROR" || str == "OK") {
            qCDebug(log) << "Invalid ID response" << frame << "during startup at"
                         << Logging::time(frameTime);

            spancheckController->terminate();
            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            outstandingCommands.clear();
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            return;
        }
        static thread_local QRegularExpression pattern
                (R"EOF(Aurora\s*(\d+).*v([^\s#]+).*#(.+)$)EOF",
                 QRegularExpression::CaseInsensitiveOption);
        auto match = pattern.match(str);
        if (match.hasMatch()) {
            auto modelString = match.captured(1).trimmed();
            auto fwString = match.captured(2).trimmed();
            auto snString = match.captured(3).trimmed();

            if (modelString != instrumentMeta["Model"].toQString()) {
                haveEmittedRealtimeMeta = false;
                haveEmittedLogMeta = false;
                instrumentMeta["Model"].setString(modelString);
            }
            if (fwString != instrumentMeta["FirmwareVersion"].toQString()) {
                haveEmittedRealtimeMeta = false;
                haveEmittedLogMeta = false;
                instrumentMeta["FirmwareVersion"].setString(fwString);
            }

            bool ok = false;
            std::int_fast64_t sn = snString.toLongLong(&ok);
            if (ok && INTEGER::defined(sn) && sn > 0) {
                if (sn != instrumentMeta["SerialNumber"].toInteger()) {
                    haveEmittedRealtimeMeta = false;
                    haveEmittedLogMeta = false;
                    instrumentMeta["SerialNumber"].setInteger(sn);
                }
            } else {
                if (snString != instrumentMeta["SerialNumber"].toQString()) {
                    haveEmittedRealtimeMeta = false;
                    haveEmittedLogMeta = false;
                    instrumentMeta["SerialNumber"].setString(snString);
                }
            }

            if (modelString == "4000") {
                if (!hasFeature(Feature::Polar)) {
                    setFeature(Feature::Polar, true);
                    haveEmittedRealtimeMeta = false;
                    haveEmittedLogMeta = false;
                }
            } else {
                if (hasFeature(Feature::Polar)) {
                    setFeature(Feature::Polar, false);
                    haveEmittedRealtimeMeta = false;
                    haveEmittedLogMeta = false;
                }
            }
        } else {
            instrumentMeta["ID"].setString(str);

            if (str.contains(" 4000 ")) {
                if (instrumentMeta["Model"].toString() != "4000") {
                    haveEmittedRealtimeMeta = false;
                    haveEmittedLogMeta = false;
                    instrumentMeta["Model"].setString("4000");
                }
                if (!hasFeature(Feature::Polar)) {
                    setFeature(Feature::Polar, true);
                    haveEmittedRealtimeMeta = false;
                    haveEmittedLogMeta = false;
                }
            } else if (str.contains(" 3000 ")) {
                if (instrumentMeta["Model"].toString() != "3000") {
                    haveEmittedRealtimeMeta = false;
                    haveEmittedLogMeta = false;
                    instrumentMeta["Model"].setString("3000");
                }
                if (hasFeature(Feature::Polar)) {
                    setFeature(Feature::Polar, false);
                    haveEmittedRealtimeMeta = false;
                    haveEmittedLogMeta = false;
                }
            }
        }

        responseState = ResponseState::Interactive_Start_ReadStatus;
        timeoutAt(frameTime + 2.0);
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "00\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveReadStatus"),
                                                       frameTime, FP::undefined()));
        }
        return;
    }
    case ResponseState::Interactive_Start_ReadStatus: {
        if (int code = Command_SystemStatus::parse(frame)) {
            qCDebug(log) << "Invalid status response" << frame << "during startup at"
                         << Logging::time(frameTime) << "code" << code;

            spancheckController->terminate();
            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            outstandingCommands.clear();
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            return;
        }

        timeoutAt(frameTime + 2.0);
        outstandingCommands.clear();
        responseState = ResponseState::Interactive_Start_SetTime;
        if (controlStream) {
            auto dt = Time::toDateTime(frameTime + 0.5);
            Util::ByteArray send("**");
            send += QByteArray::number(config.front().address);
            send += "S";
            send += QByteArray::number(dt.time().hour()).rightJustified(2, '0');
            send += QByteArray::number(dt.time().minute()).rightJustified(2, '0');
            send += QByteArray::number(dt.time().second()).rightJustified(2, '0');
            send += QByteArray::number(dt.date().day()).rightJustified(2, '0');
            send += QByteArray::number(dt.date().month()).rightJustified(2, '0');
            send += QByteArray::number(dt.date().year() % 100).rightJustified(2, '0');
            send += "\r";
            controlStream->writeControl(std::move(send));
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetTime"),
                                  frameTime, FP::undefined()));
        }
        return;
    }
    case ResponseState::Interactive_Start_SetTime:
        if (frame.toQByteArray().trimmed() != "OK") {
            qCDebug(log) << "Invalid set time response" << frame << "during startup at"
                         << Logging::time(frameTime);

            spancheckController->terminate();
            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            outstandingCommands.clear();
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            return;
        }

        timeoutAt(frameTime + 2.0);
        outstandingCommands.clear();
        responseState = ResponseState::Interactive_Start_SetFilter;
        if (controlStream) {
            Util::ByteArray send("**");
            send += QByteArray::number(config.front().address);
            send += "PCF,";
            switch (config.front().zeroMode) {
            case Configuration::Zero::Native:
                switch (config.front().filterMode) {
                case Configuration::Filter::None:
                    send += "N";
                    break;
                case Configuration::Filter::Kalman:
                    send += "K";
                    break;
                case Configuration::Filter::Average:
                    send += "M";
                    break;
                }
                break;
            case Configuration::Zero::EnableFilter:
            case Configuration::Zero::Offset:
                send += "N";
                break;
            }
            send += "\n\r";
            controlStream->writeControl(std::move(send));
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetFilter"),
                                  frameTime, FP::undefined()));
        }
        return;
    case ResponseState::Interactive_Start_SetFilter:
        if (frame.toQByteArray().trimmed() != "OK") {
            setFeature(Feature::FilterControl, false);

            responseState = ResponseState::Interactive_Start_ReadEE;
            timeoutAt(frameTime + 2.0);
            seenEEComponents.fill(false);
            readEEAbsoluteTimeout = FP::undefined();
            if (controlStream) {
                controlStream->writeControl("EE\r");
            }
            lastEEResults = CPD3::Data::Variant::Root();
            if (realtimeEgress) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                           Variant::Root("StartInteractiveReadEE"),
                                                           frameTime, FP::undefined()));
            }
            return;
        }

        setFeature(Feature::FilterControl, true);

        timeoutAt(frameTime + 2.0);
        outstandingCommands.clear();
        responseState = ResponseState::Interactive_Start_SetNormalization;
        if (controlStream) {
            Util::ByteArray send("**");
            send += QByteArray::number(config.front().address);
            send += "PCSTP,";
            if (!FP::defined(config.front().normalizationTemperature)) {
                send += "N";
            } else {
                send += QByteArray::number(config.front().normalizationTemperature, 'f', 0);
            }
            send += "\n\r";
            controlStream->writeControl(std::move(send));
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetNormalization"), frameTime, FP::undefined()));
        }
        return;
    case ResponseState::Interactive_Start_SetNormalization:
        if (frame.toQByteArray().trimmed() != "OK") {
            /* Accept this, since we read it back from the EE record anyway */
            qCDebug(log) << "Invalid set normalization response" << frame << "during startup at"
                         << Logging::time(frameTime);
        }

        responseState = ResponseState::Interactive_Start_ReadEE;
        timeoutAt(frameTime + 2.0);
        seenEEComponents.fill(false);
        readEEAbsoluteTimeout = FP::undefined();
        if (controlStream) {
            controlStream->writeControl("EE\r");
        }
        lastEEResults = CPD3::Data::Variant::Root();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveReadEE"),
                                  frameTime, FP::undefined()));
        }
        return;
    case ResponseState::Interactive_Start_ReadEE:
        processEELine(frame, frameTime);
        return;
    case ResponseState::Interactive_Start_CheckFlags: {
        bool ok = false;
        auto check = frame.toQByteArray().trimmed();
        if (check.endsWith('.'))
            check.chop(1);
        check.toUShort(&ok);
        if (!ok) {
            setFeature(Feature::StatusFlags, false);
        } else {
            setFeature(Feature::StatusFlags, true);
        }

        responseState = ResponseState::Interactive_Start_DisableSpan;
        timeoutAt(frameTime + 2.0);
        if (controlStream) {
            controlStream->writeControl(
                    "DO" + QByteArray::number(config.front().address) + "000\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveDisableSpan"),
                                                       frameTime, FP::undefined()));
        }
        return;
    }
    case ResponseState::Interactive_Start_DisableSpan:
        if (frame.toQByteArray().trimmed() != "OK") {
            qCDebug(log) << "Invalid span disable response" << frame << "during startup at"
                         << Logging::time(frameTime);

            spancheckController->terminate();
            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            outstandingCommands.clear();
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            return;
        }

        timeoutAt(frameTime + 2.0);
        outstandingCommands.clear();
        responseState = ResponseState::Interactive_Start_DisableZero;
        if (controlStream) {
            controlStream->writeControl(
                    "DO" + QByteArray::number(config.front().address) + "010\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveReadZero"),
                                  frameTime, FP::undefined()));
        }
        return;
    case ResponseState::Interactive_Start_DisableZero:
        if (frame.toQByteArray().trimmed() != "OK") {
            qCDebug(log) << "Invalid zero disable response" << frame << "during startup at"
                         << Logging::time(frameTime);

            spancheckController->terminate();
            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            outstandingCommands.clear();
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            return;
        }

        qCDebug(log) << "Comms established at" << Logging::time(frameTime);

        responseState = ResponseState::Interactive_Run_Delay;
        commandState = CommandState::SystemState;
        discardData(frameTime + 1.0);
        timeoutAt(frameTime + 10.0);

        opticalUpdate = OpticalUpdate::ReadPending;
        opticalUpdateTimeout = FP::undefined();
        referenceUpdate = ReferenceUpdate::ReadPending;
        referenceUpdateTimeout = FP::undefined();
        referenceColorsRead.fill(false);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        generalStatusUpdated();

        event(frameTime, QObject::tr("Communications established."), false, describeState());

        realtimeStateUpdated = true;

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                  FP::undefined()));
        }
        return;

    case ResponseState::Interactive_Zero_ReadEE:
        processEELine(frame, frameTime);
        return;

    case ResponseState::Interactive_Zero_ReadEE_Resume:
        Q_ASSERT(false);
        return;
    default:
        break;
    }

    int passiveCode = processUnpolledResponse(frame, frameTime);
    if (passiveCode == 0) {
        switch (responseState) {
        case ResponseState::Passive_Wait:
        case ResponseState::Passive_Initialize:
            if (++autoprobeValidRecords < 5) {
                timeoutAt(frameTime + config.front().inactiveDelay * 2.0 + 1.0);
                break;
            }

            qCDebug(log) << "Comms established at" << Logging::time(frameTime);
            event(frameTime, QObject::tr("Communications established."), false, describeState());

            commandState = CommandState::SystemState;
            responseState = ResponseState::Passive_Run;
            generalStatusUpdated();

            opticalUpdate = OpticalUpdate::ReadPending;
            opticalUpdateTimeout = FP::undefined();
            referenceUpdate = ReferenceUpdate::ReadPending;
            referenceUpdateTimeout = FP::undefined();
            referenceColorsRead.fill(false);

            timeoutAt(frameTime + config.front().inactiveDelay * 2.0 + 1.0);

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                      FP::undefined()));
            }
            realtimeStateUpdated = true;
            break;

        case ResponseState::Autoprobe_Passive_Wait:
        case ResponseState::Autoprobe_Passive_Initialize:
            if (++autoprobeValidRecords < 5) {
                timeoutAt(frameTime + config.front().inactiveDelay * 2.0 + 1.0);
                break;
            }

            qCDebug(log) << "Autoprobe succeeded at" << Logging::time(frameTime);
            event(frameTime, QObject::tr("Autoprobe succeeded."), false, describeState());

            commandState = CommandState::SystemState;
            responseState = ResponseState::Passive_Run;

            opticalUpdate = OpticalUpdate::ReadPending;
            opticalUpdateTimeout = FP::undefined();
            referenceUpdate = ReferenceUpdate::ReadPending;
            referenceUpdateTimeout = FP::undefined();
            referenceColorsRead.fill(false);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            timeoutAt(frameTime + config.front().inactiveDelay * 2.0 + 1.0);

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                      FP::undefined()));
            }
            realtimeStateUpdated = true;
            break;

        default:
            break;
        }

        /* Possibly unsolicited passive response, so just ignore it */
        return;
    } else if (outstandingCommands.empty()) {
        switch (responseState) {
        case ResponseState::Interactive_Run_Active:
        case ResponseState::Interactive_Run_Retry:
        case ResponseState::Interactive_Run_Resume:
        case ResponseState::Interactive_Run_Delay:
            /* Unsolicited and unknown response, start the command retry sequence */
            responseState = ResponseState::Interactive_Run_Retry;
            discardData(frameTime + config.front().retryDelay);
            timeoutAt(frameTime + config.front().retryDelay + 10.0);
            outstandingCommands.clear();
            return;
        case ResponseState::Interactive_Zero_ReadEE:
        case ResponseState::Interactive_Zero_ReadEE_Retry:
        case ResponseState::Interactive_Zero_ReadEE_Resume:
            /* Unsolicited and unknown response, start the command retry sequence */
            responseState = ResponseState::Interactive_Zero_ReadEE_Retry;
            discardData(frameTime + config.front().retryDelay);
            timeoutAt(frameTime + config.front().retryDelay + 10.0);
            outstandingCommands.clear();
            return;

        case ResponseState::Autoprobe_Passive_Wait:
        case ResponseState::Autoprobe_Passive_Initialize:
            if (passiveCode < 0) {
                autoprobeValidRecords = 0;
                invalidateLogValues(frameTime);
                break;
            }

            qCDebug(log) << "Autoprobe failed at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << passiveCode;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            responseState = ResponseState::Passive_Wait;
            autoprobeValidRecords = 0;

            if (realtimeEgress && FP::defined(frameTime)) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            autoprobeValidRecords = 0;
            invalidateLogValues(frameTime);
            break;

        case ResponseState::Passive_Wait:
        case ResponseState::Passive_Initialize:
            autoprobeValidRecords = 0;
            invalidateLogValues(frameTime);
            break;

        case ResponseState::Passive_Run:
            if (passiveCode < 0) {
                autoprobeValidRecords = 0;
                invalidateLogValues(frameTime);
                break;
            }

            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << passiveCode;

            {
                auto info = describeState();
                info["Code"].setInteger(passiveCode);
                info["Line"].setString(frame.toString());
                event(frameTime,
                      QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                              passiveCode), true, info);
            }

            responseState = ResponseState::Passive_Wait;
            discardData(frameTime + 1.0);
            generalStatusUpdated();

            if (realtimeEgress && FP::defined(frameTime)) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            autoprobeValidRecords = 0;
            invalidateLogValues(frameTime);
            break;

        default:
            Q_ASSERT(isInteractiveStart());
            /* Should be handled above */
            Q_ASSERT(false);
            break;
        }

        return;
    }

    if (!FP::defined(frameTime))
        return;
    lastRecordTime = frameTime;
    configAdvance(frameTime);

    Q_ASSERT(!outstandingCommands.empty());
    Q_ASSERT(!isInteractiveStart());

    int commandCode = outstandingCommands.front()->response(frame, frameTime);
    if (commandCode == 0) {
        commandRetryFailureCounter = 0;
        commandRetryAbsoluteTimeout = FP::undefined();

        outstandingCommands.pop_front();
        if (isInteractive()) {
            Q_ASSERT(FP::defined(frameTime));

            if (outstandingCommands.empty()) {
                commandAdvance(frameTime, CommandOperation::Response);
            }
        }

        dataAdvance(frameTime);
        return;
    }

    switch (responseState) {
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
        return;
    case ResponseState::Passive_Run:
        qCDebug(log) << "Command response at" << Logging::time(frameTime) << ":" << frame
                     << "rejected with code" << commandCode;

        {
            auto info = describeState();
            info["Code"].setInteger(commandCode);
            info["Line"].setString(frame.toString());
            event(frameTime, QObject::tr(
                    "Invalid command response received (code %1).  Communications dropped.").arg(
                    passiveCode), true, info);
        }

        spancheckController->terminate();
        invalidateLogValues(frameTime);

        responseState = ResponseState::Passive_Wait;
        discardData(frameTime + 1.0);
        generalStatusUpdated();

        if (realtimeEgress && FP::defined(frameTime)) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                  frameTime, FP::undefined()));
        }

        autoprobeValidRecords = 0;
        invalidateLogValues(frameTime);
        return;

    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
        qCDebug(log) << "Autoprobe failed at" << Logging::time(frameTime) << ":" << frame
                     << "rejected with command code" << commandCode;

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        spancheckController->terminate();
        invalidateLogValues(frameTime);

        responseState = ResponseState::Passive_Wait;
        autoprobeValidRecords = 0;

        if (realtimeEgress && FP::defined(frameTime)) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                  frameTime, FP::undefined()));
        }

        autoprobeValidRecords = 0;
        invalidateLogValues(frameTime);
        return;

    default:
        break;
    }

    Q_ASSERT(isInteractive());
    Q_ASSERT(FP::defined(frameTime));

    /* A garbled response, so attempt a retry */

    ++commandRetryFailureCounter;
    if (commandRetryFailureCounter > config.front().retryFailureMaximum) {
        qCDebug(log) << "Response at" << Logging::time(frameTime) << ":" << frame
                     << "rejected with code" << commandCode << "in state"
                     << static_cast<int>(responseState) << "to command state"
                     << static_cast<int>(commandState);

        {
            auto info = describeState();
            info["Code"].setInteger(passiveCode);
            info["Line"].setString(frame.toString());
            event(frameTime, QObject::tr(
                    "Invalid command response received (code %1).  Communications dropped.").arg(
                    commandCode), true, info);
        }

        spancheckController->terminate();
        invalidateLogValues(frameTime);

        responseState = ResponseState::Interactive_Restart_Wait;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        outstandingCommands.clear();
        autoprobeValidRecords = 0;
        generalStatusUpdated();

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                  frameTime, FP::undefined()));
        }

        return;
    }

    qCDebug(log) << "Retrying due to invalid response at" << Logging::time(frameTime) << ":"
                 << frame << "rejected with code" << commandCode << "in state"
                 << static_cast<int>(responseState) << "to command state"
                 << static_cast<int>(commandState);

    responseState = ResponseState::Interactive_Run_Retry;
    discardData(frameTime + config.front().retryDelay);
    timeoutAt(frameTime + config.front().retryDelay + 10.0);
    outstandingCommands.clear();
}

void AcquireEcotechNephAurora::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Interactive_Start_CheckFlags:
        setFeature(Feature::StatusFlags, false);

        responseState = ResponseState::Interactive_Start_DisableSpan;
        timeoutAt(frameTime + 2.0);
        if (controlStream) {
            controlStream->writeControl(
                    "DO" + QByteArray::number(config.front().address) + "000\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveDisableSpan"),
                                                       frameTime, FP::undefined()));
        }
        break;

    case ResponseState::Interactive_Start_SetFilter:
        setFeature(Feature::FilterControl, false);
        /* Fall through */
    case ResponseState::Interactive_Start_SetNormalization:
        responseState = ResponseState::Interactive_Start_ReadEE;
        timeoutAt(frameTime + 2.0);
        seenEEComponents.fill(false);
        readEEAbsoluteTimeout = FP::undefined();
        if (controlStream) {
            controlStream->writeControl("EE\r");
        }
        lastEEResults = CPD3::Data::Variant::Root();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveReadEE"),
                                  frameTime, FP::undefined()));
        }
        break;

    case ResponseState::Interactive_Zero_ReadEE:
        if (haveSeenAnyEE()) {
            if (!haveSeenAllEE()) {
                qCDebug(log) << "EE read finished but with missing components at"
                             << Logging::time(frameTime);
            }
            finalizeEE(frameTime);

            switch (config.front().zeroMode) {
            case Configuration::Zero::Offset:
                break;
            case Configuration::Zero::Native:
            case Configuration::Zero::EnableFilter:
                completeZero(frameTime);
                break;
            }

            outstandingCommands.clear();
            commandAdvance(frameTime, CommandOperation::Resend);
            break;
        }
        /* Fall through */
    case ResponseState::Interactive_Zero_ReadEE_Retry:
        if (FP::defined(commandRetryAbsoluteTimeout) && frameTime > commandRetryAbsoluteTimeout) {
            return retryBusyExhausted(frameTime);
        }

        ++commandRetryFailureCounter;
        if (commandRetryFailureCounter <= config.front().retryFailureMaximum) {
            qCDebug(log) << "Retrying timed out EE read at" << Logging::time(frameTime);

            responseState = ResponseState::Interactive_Zero_ReadEE_Retry;
            discardData(frameTime + config.front().retryDelay);
            timeoutAt(frameTime + config.front().retryDelay + 10.0);
            outstandingCommands.clear();
            break;
        }

        qCDebug(log) << "Timeout in state" << static_cast<int>(responseState) << "at"
                     << Logging::time(frameTime);

        event(frameTime, QObject::tr("Timeout waiting for response.  Communications Dropped."),
              true, describeState());

        spancheckController->terminate();
        invalidateLogValues(frameTime);

        responseState = ResponseState::Interactive_Restart_Wait;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        outstandingCommands.clear();
        autoprobeValidRecords = 0;
        generalStatusUpdated();

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        break;

    case ResponseState::Interactive_Run_Active:
    case ResponseState::Interactive_Run_Retry:
        if (FP::defined(commandRetryAbsoluteTimeout) && frameTime > commandRetryAbsoluteTimeout) {
            return retryBusyExhausted(frameTime);
        }

        ++commandRetryFailureCounter;
        if (commandRetryFailureCounter <= config.front().retryFailureMaximum) {
            qCDebug(log) << "Retrying timed out command" << static_cast<int>(commandState) << "at"
                         << Logging::time(frameTime);

            responseState = ResponseState::Interactive_Run_Retry;
            discardData(frameTime + config.front().retryDelay);
            timeoutAt(frameTime + config.front().retryDelay + 10.0);
            outstandingCommands.clear();
            break;
        }

        /*Fall through */
    case ResponseState::Interactive_Run_Resume:
    case ResponseState::Interactive_Zero_ReadEE_Resume:

        event(frameTime, QObject::tr("Timeout waiting for response.  Communications Dropped."),
              true, describeState());

        qCDebug(log) << "Timeout in state" << static_cast<int>(responseState) << "at"
                     << Logging::time(frameTime);

        spancheckController->terminate();
        invalidateLogValues(frameTime);

        responseState = ResponseState::Interactive_Restart_Wait;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        outstandingCommands.clear();
        autoprobeValidRecords = 0;
        generalStatusUpdated();

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        break;

    case ResponseState::Interactive_Run_Delay:
        outstandingCommands.clear();
        commandAdvance(frameTime, CommandOperation::Advance);
        break;

    case ResponseState::Interactive_Start_ReadEE:
        if (haveSeenAnyEE()) {
            if (!haveSeenAllEE()) {
                qCDebug(log) << "EE read finished but with missing components at"
                             << Logging::time(frameTime);
            }
            finalizeEE(frameTime);
            updateEffectiveZero();

            timeoutAt(frameTime + 2.0);
            outstandingCommands.clear();
            responseState = ResponseState::Interactive_Start_CheckFlags;
            if (controlStream) {
                controlStream->writeControl(
                        "VI" + QByteArray::number(config.front().address) + "88\r");
            }
            if (realtimeEgress) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveCheckFlags"), frameTime, FP::undefined()));
            }
            break;
        }
        /* Fall through */
    case ResponseState::Interactive_Start_ReadID:
    case ResponseState::Interactive_Start_ReadStatus:
    case ResponseState::Interactive_Start_SetTime:
    case ResponseState::Interactive_Start_DisableSpan:
    case ResponseState::Interactive_Start_DisableZero:
        qCDebug(log) << "Timeout in interactive start state" << static_cast<int>(responseState)
                     << "at" << Logging::time(frameTime);

        spancheckController->terminate();
        invalidateLogValues(frameTime);

        responseState = ResponseState::Interactive_Restart_Wait;
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);
        outstandingCommands.clear();
        autoprobeValidRecords = 0;
        generalStatusUpdated();

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        break;

    case ResponseState::Passive_Run:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        event(frameTime, QObject::tr("Timeout waiting for response.  Communications Dropped."),
              true, describeState());

        spancheckController->terminate();
        invalidateLogValues(frameTime);

        responseState = ResponseState::Passive_Wait;
        timeoutAt(FP::undefined());
        outstandingCommands.clear();
        autoprobeValidRecords = 0;
        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        break;

    case ResponseState::Passive_Wait: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* fall through */
    case ResponseState::Passive_Initialize:
        responseState = ResponseState::Passive_Wait;
        autoprobeValidRecords = 0;
        invalidateLogValues(frameTime);
        break;

    case ResponseState::Autoprobe_Passive_Wait: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* fall through */
    case ResponseState::Autoprobe_Passive_Initialize:
        responseState = ResponseState::Autoprobe_Passive_Wait;
        autoprobeValidRecords = 0;
        invalidateLogValues(frameTime);
        break;

    case ResponseState::Interactive_Restart_Wait:
    case ResponseState::Interactive_Initialize:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        responseState = ResponseState::Interactive_Start_ReadID;
        timeoutAt(frameTime + 2.0);
        outstandingCommands.clear();
        if (controlStream) {
            controlStream->writeControl("ID" + QByteArray::number(config.front().address) + "\r");
        }

        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;
    }
}

void AcquireEcotechNephAurora::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Interactive_Start_ReadID:
        timeoutAt(frameTime + 2.0);
        outstandingCommands.clear();
        if (controlStream) {
            controlStream->writeControl("ID" + QByteArray::number(config.front().address) + "\r");
        }
        break;

    case ResponseState::Interactive_Run_Retry:
    case ResponseState::Interactive_Zero_ReadEE_Retry:
        /* Re-queue any command we expect an Ok response to */
        for (const auto &check : outstandingCommands) {
            if (auto resend = dynamic_cast<const Command_Ok *>(check.get())) {
                okCommandQueue.push_back(std::move(resend->command));
            }
        }

        /* Send read status command, and try to get a valid response from it */

        timeoutAt(frameTime + 2.0);
        outstandingCommands.clear();
        if (controlStream) {
            controlStream->writeControl("VI" + QByteArray::number(config.front().address) + "00\r");
        }
        break;

    case ResponseState::Interactive_Restart_Wait:
    case ResponseState::Interactive_Initialize:
        timeoutAt(frameTime + 2.0);

        outstandingCommands.clear();
        commandRetryFailureCounter = 0;

        commandRetryAbsoluteTimeout = FP::undefined();
        if (controlStream) {
            controlStream->writeControl("ID" + QByteArray::number(config.front().address) + "\r");
        }

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveReadID"),
                                  frameTime, FP::undefined()));
        }

        responseState = ResponseState::Interactive_Start_ReadID;
        break;

    case ResponseState::Interactive_Run_Resume:
        outstandingCommands.clear();
        commandAdvance(frameTime, CommandOperation::Resend);
        break;

    case ResponseState::Interactive_Zero_ReadEE_Resume:
        outstandingCommands.clear();
        responseState = ResponseState::Interactive_Zero_ReadEE;

        timeoutAt(frameTime + 2.0);
        readEEAbsoluteTimeout = FP::undefined();
        if (controlStream) {
            controlStream->writeControl("EE\r");
        }

        lastEEResults = CPD3::Data::Variant::Root();
        break;

    case ResponseState::Interactive_Run_Delay:
        outstandingCommands.clear();
        commandAdvance(frameTime, CommandOperation::Advance);
        break;

    default:
        break;
    }
}

void AcquireEcotechNephAurora::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configAdvance(frameTime);

    if (frame.string_start("VI" + std::to_string(config.front().address))) {
        bool ok = false;
        int n = frame.mid(3).parse_i32(&ok);
        if (!ok)
            return;
        switch (n) {
        case 0:
            outstandingCommands.emplace_back(new Command_SystemStatus(*this));
            return;
        case 99:
            outstandingCommands.emplace_back(new Command_DataLine(*this));
            return;
        case 4:
            outstandingCommands.emplace_back(new Command_Dark(*this));
            return;

        case 6:
            outstandingCommands.emplace_back(new Command_ReferenceRed(*this));
            return;
        case 7:
            outstandingCommands.emplace_back(new Command_SampleRed(*this));
            return;
        case 8:
            outstandingCommands.emplace_back(new Command_RatioRed(*this));
            return;
        case 33:
            outstandingCommands.emplace_back(new Command_BackSampleRed(*this));
            return;
        case 34:
            outstandingCommands.emplace_back(new Command_BackRatioRed(*this));
            return;

        case 9:
            outstandingCommands.emplace_back(new Command_ReferenceGreen(*this));
            return;
        case 10:
            outstandingCommands.emplace_back(new Command_SampleGreen(*this));
            return;
        case 11:
            outstandingCommands.emplace_back(new Command_RatioGreen(*this));
            return;
        case 35:
            outstandingCommands.emplace_back(new Command_BackSampleGreen(*this));
            return;
        case 36:
            outstandingCommands.emplace_back(new Command_BackRatioGreen(*this));
            return;

        case 12:
            outstandingCommands.emplace_back(new Command_ReferenceBlue(*this));
            return;
        case 13:
            outstandingCommands.emplace_back(new Command_SampleBlue(*this));
            return;
        case 14:
            outstandingCommands.emplace_back(new Command_RatioBlue(*this));
            return;
        case 37:
            outstandingCommands.emplace_back(new Command_BackSampleBlue(*this));
            return;
        case 38:
            outstandingCommands.emplace_back(new Command_BackRatioBlue(*this));
            return;

        case 88:
            outstandingCommands.emplace_back(new Command_StatusFlags(*this));
            return;

        default:
            break;
        }
    }

    if (frame == "ID" + QByteArray::number(config.front().address)) {
        outstandingCommands.emplace_back(new Command_Ignore(*this));
        return;
    }

    if (frame == "DO" + QByteArray::number(config.front().address)) {
        outstandingCommands.emplace_back(new Command_Ignore(*this));
        return;
    }

    if (frame.string_start("**" + std::to_string(config.front().address)) && frame.size() > 3) {
        if (frame[3] == 'S' || frame[3] == 'J' || frame[3] == 'M') {
            outstandingCommands.emplace_back(new Command_Ignore(*this));
            return;
        }
    }
}

void AcquireEcotechNephAurora::retryBusyExhausted(double frameTime)
{
    switch (responseState) {
    case ResponseState::Interactive_Zero_ReadEE:
    case ResponseState::Interactive_Zero_ReadEE_Retry:
    case ResponseState::Interactive_Run_Active:
    case ResponseState::Interactive_Run_Retry:
    case ResponseState::Interactive_Run_Resume:
    case ResponseState::Interactive_Zero_ReadEE_Resume:
    case ResponseState::Interactive_Run_Delay:
        event(frameTime, QObject::tr("Command retry timeout exhausted.  Communications Dropped."),
              true, describeState());

        qCDebug(log) << "Retry timeout in state" << static_cast<int>(responseState) << "at"
                     << Logging::time(frameTime);

        spancheckController->terminate();
        invalidateLogValues(frameTime);

        responseState = ResponseState::Interactive_Restart_Wait;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        outstandingCommands.clear();
        autoprobeValidRecords = 0;
        generalStatusUpdated();

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        break;

    case ResponseState::Interactive_Start_CheckFlags:
    case ResponseState::Interactive_Start_ReadEE:
    case ResponseState::Interactive_Start_ReadID:
    case ResponseState::Interactive_Start_ReadStatus:
    case ResponseState::Interactive_Start_SetTime:
    case ResponseState::Interactive_Start_SetFilter:
    case ResponseState::Interactive_Start_SetNormalization:
    case ResponseState::Interactive_Start_DisableSpan:
    case ResponseState::Interactive_Start_DisableZero:
    case ResponseState::Interactive_Restart_Wait:
    case ResponseState::Interactive_Initialize:
        qCDebug(log) << "Retry timeout in interactive start state"
                     << static_cast<int>(responseState) << "at" << Logging::time(frameTime);

        spancheckController->terminate();
        invalidateLogValues(frameTime);

        responseState = ResponseState::Interactive_Restart_Wait;
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);
        outstandingCommands.clear();
        autoprobeValidRecords = 0;
        generalStatusUpdated();

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        break;

    case ResponseState::Passive_Run:
    case ResponseState::Passive_Initialize:
    case ResponseState::Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait:
        qCDebug(log) << "Retry timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        event(frameTime, QObject::tr("Command retry timeout exhausted.  Communications Dropped."),
              true, describeState());

        spancheckController->terminate();
        invalidateLogValues(frameTime);

        responseState = ResponseState::Passive_Wait;
        timeoutAt(FP::undefined());
        outstandingCommands.clear();
        autoprobeValidRecords = 0;
        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireEcotechNephAurora::configAdvance(double frameTime)
{
    Q_ASSERT(!config.empty());
    auto size = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.empty());

    if (size == config.size())
        return;

    for (std::size_t color = 0; color < 3; color++) {
        if (!FP::defined(config.front().wavelengths[color]))
            continue;
        if (!FP::equal(config.front().wavelengths[color], effectiveWavelengths[color]))
            continue;
        effectiveWavelengths[color] = config.front().wavelengths[color];
        haveEmittedLogMeta = false;
        haveEmittedRealtimeMeta = false;

        spancheckController->setWavelength(color, effectiveWavelengths[color],
                                           QString::fromStdString(colorCodes[color]));
    }
}

SequenceValue::Transfer AcquireEcotechNephAurora::constructSpancheckVariables(bool includeRealtime)
{
    SequenceValue::Transfer result;
    if (!spancheckDetails.read().exists())
        return result;

    result.emplace_back(spancheckDetails);

    if (hasFeature(Feature::Polar)) {
        auto add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                            NephelometerSpancheckController::Angles);
        if (add.read().exists()) {
            result.emplace_back(SequenceName({}, "raw", "Bnc"), std::move(add),
                                spancheckDetails.getStart(), spancheckDetails.getEnd());
        }
    }

    for (std::size_t color = 0; color < 3; color++) {
        auto add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                            NephelometerSpancheckController::TotalPercentError,
                                                            QString::fromStdString(
                                                                    colorCodes[color]));
        if (add.read().exists()) {
            result.emplace_back(SequenceName({}, "raw", "PCTc" + colorCodes[color]), std::move(add),
                                spancheckDetails.getStart(), spancheckDetails.getEnd());
        }

        add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                       NephelometerSpancheckController::BackPercentError,
                                                       QString::fromStdString(colorCodes[color]));
        if (add.read().exists()) {
            result.emplace_back(SequenceName({}, "raw", "PCTbc" + colorCodes[color]),
                                std::move(add), spancheckDetails.getStart(),
                                spancheckDetails.getEnd());
        }

        add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                       NephelometerSpancheckController::TotalSensitivityFactor,
                                                       QString::fromStdString(colorCodes[color]));
        if (add.read().exists()) {
            result.emplace_back(SequenceName({}, "raw", "Cc" + colorCodes[color]), std::move(add),
                                spancheckDetails.getStart(), spancheckDetails.getEnd());
        }

        add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                       NephelometerSpancheckController::BackSensitivityFactor,
                                                       QString::fromStdString(colorCodes[color]));
        if (add.read().exists()) {
            result.emplace_back(SequenceName({}, "raw", "Cbc" + colorCodes[color]), std::move(add),
                                spancheckDetails.getStart(), spancheckDetails.getEnd());
        }


        if (includeRealtime) {
            if (angles.empty() || std::fabs(angles.front()) < 5) {
                add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                               NephelometerSpancheckController::TotalEcotechAuroraM,
                                                               QString::fromStdString(
                                                                       colorCodes[color]));
                if (add.read().exists()) {
                    result.emplace_back(SequenceName({}, "raw", "ZSPANCHECKM" + colorCodes[color]),
                                        std::move(add), spancheckDetails.getStart(),
                                        spancheckDetails.getEnd());
                }
            }

            if (!angles.empty() && std::fabs(angles.back() - 90) < 5) {
                add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                               NephelometerSpancheckController::BackEcotechAuroraM,
                                                               QString::fromStdString(
                                                                       colorCodes[color]));
                if (add.read().exists()) {
                    result.emplace_back(SequenceName({}, "raw", "ZSPANCHECKMb" + colorCodes[color]),
                                        std::move(add), spancheckDetails.getStart(),
                                        spancheckDetails.getEnd());
                }
            }
        }


        if (!hasFeature(Feature::Polar))
            continue;

        add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                       NephelometerSpancheckController::PercentError,
                                                       QString::fromStdString(colorCodes[color]));
        if (add.read().exists()) {
            result.emplace_back(SequenceName({}, "raw", "PCTnc" + colorCodes[color]),
                                std::move(add), spancheckDetails.getStart(),
                                spancheckDetails.getEnd());
        }

        add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                       NephelometerSpancheckController::SensitivityFactor,
                                                       QString::fromStdString(colorCodes[color]));
        if (add.read().exists()) {
            result.emplace_back(SequenceName({}, "raw", "Cnc" + colorCodes[color]), std::move(add),
                                spancheckDetails.getStart(), spancheckDetails.getEnd());
        }

        if (includeRealtime) {
            add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                           NephelometerSpancheckController::EcotechAuroraMPolar,
                                                           QString::fromStdString(
                                                                   colorCodes[color]));
            result.emplace_back(SequenceName({}, "raw", "ZSPANCHECKMn" + colorCodes[color]),
                                std::move(add), spancheckDetails.getStart(),
                                spancheckDetails.getEnd());
        }
    }

    return result;
}

void AcquireEcotechNephAurora::updateSpancheckData()
{
    if (persistentEgress) {
        spancheckDetails.setEnd(lastRecordTime);
        persistentEgress->incomingData(constructSpancheckVariables());
    }

    spancheckDetails.setRoot(
            spancheckController->results(NephelometerSpancheckController::FullResults));
    spancheckDetails.setStart(lastRecordTime);
    spancheckDetails.setEnd(FP::undefined());

    persistentValuesUpdated();

    if (realtimeEgress) {
        realtimeEgress->incomingData(constructSpancheckVariables(true));
    }
}

SequenceValue::Transfer AcquireEcotechNephAurora::getPersistentValues()
{
    PauseLock paused(*this);
    SequenceValue::Transfer result(constructSpancheckVariables());

    if (digitalState.read().exists() && FP::defined(digitalState.getStart())) {
        result.push_back(digitalState);
    }

    return result;
}

Variant::Root AcquireEcotechNephAurora::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireEcotechNephAurora::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case ResponseState::Passive_Run:
    case ResponseState::Interactive_Run_Active:
    case ResponseState::Interactive_Run_Retry:
    case ResponseState::Interactive_Run_Resume:
    case ResponseState::Interactive_Run_Delay:
    case ResponseState::Interactive_Zero_ReadEE:
    case ResponseState::Interactive_Zero_ReadEE_Retry:
    case ResponseState::Interactive_Zero_ReadEE_Resume:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

AcquireEcotechNephAurora::SpancheckInterface::SpancheckInterface(AcquireEcotechNephAurora &neph)
        : parent(neph)
{ }

AcquireEcotechNephAurora::SpancheckInterface::~SpancheckInterface() = default;

void AcquireEcotechNephAurora::SpancheckInterface::setBypass(bool enable)
{
    if (!parent.state)
        return;
    if (enable) {
        parent.state->setBypassFlag("Spancheck");
        qCDebug(parent.log) << "Spancheck bypass flag set";
    } else {
        parent.state->clearBypassFlag("Spancheck");
        qCDebug(parent.log) << "Spancheck bypass flag cleared";
    }
}

void AcquireEcotechNephAurora::SpancheckInterface::switchToFilteredAir()
{
    Q_ASSERT(!parent.config.empty());
    parent.okCommandQueue
          .emplace_back("DO" + QByteArray::number(parent.config.front().address) + "000\r");
    parent.okCommandQueue
          .emplace_back("DO" + QByteArray::number(parent.config.front().address) + "011\r");

    qCDebug(parent.log) << "Spancheck switching to filtered air";
}

void AcquireEcotechNephAurora::SpancheckInterface::switchToGas(CPD3::Algorithms::Rayleigh::Gas)
{
    Q_ASSERT(!parent.config.empty());

    parent.okCommandQueue
          .emplace_back("DO" + QByteArray::number(parent.config.front().address) + "010\r");
    parent.okCommandQueue
          .emplace_back("DO" + QByteArray::number(parent.config.front().address) + "001\r");

    qCDebug(parent.log) << "Spancheck switching to span gas";
}

void AcquireEcotechNephAurora::SpancheckInterface::issueZero()
{
    Q_ASSERT(!parent.config.empty());

    parent.okCommandQueue
          .emplace_back("DO" + QByteArray::number(parent.config.front().address) + "000\r");
    if (!parent.requestZero()) {
        parent.okCommandQueue
              .emplace_back("DO" + QByteArray::number(parent.config.front().address) + "010\r");
    }

    qCDebug(parent.log) << "Spancheck issuing zero";
}

void AcquireEcotechNephAurora::SpancheckInterface::issueCommand(const QString &target,
                                                                const CPD3::Data::Variant::Read &command)
{
    if (!parent.state)
        return;
    parent.state->sendCommand(target.toStdString(), command);
}

bool AcquireEcotechNephAurora::SpancheckInterface::start()
{
    switch (parent.responseState) {
    case ResponseState::Passive_Run:
    case ResponseState::Interactive_Run_Active:
    case ResponseState::Interactive_Run_Retry:
    case ResponseState::Interactive_Run_Resume:
    case ResponseState::Interactive_Run_Delay:
        break;
    default:
        return false;
    }
    switch (parent.sampleState) {
    case SampleState::Measure:
        break;
    default:
        return false;
    }

    qCDebug(parent.log) << "Beginning spancheck";

    parent.event(parent.lastRecordTime, QObject::tr("Spancheck initiated."), false,
                 parent.describeState());

    parent.sampleState = SampleState::Spancheck;
    parent.realtimeStateUpdated = true;

    return true;
}

void AcquireEcotechNephAurora::SpancheckInterface::completed()
{
    parent.event(parent.lastRecordTime, QObject::tr("Spancheck completed."), false,
                 parent.describeState());

    parent.beginExplicitBlank();

    parent.updateSpancheckData();

    if (parent.sampleState == SampleState::Spancheck)
        parent.sampleState = SampleState::Measure;
    parent.realtimeStateUpdated = true;

    qCDebug(parent.log) << "Spancheck completed";
}

void AcquireEcotechNephAurora::SpancheckInterface::aborted()
{
    switch (parent.responseState) {
    case ResponseState::Passive_Run:
    case ResponseState::Interactive_Run_Active:
    case ResponseState::Interactive_Run_Retry:
    case ResponseState::Interactive_Run_Resume:
    case ResponseState::Interactive_Run_Delay:
        break;
    default:
        return;
    }

    parent.event(parent.lastRecordTime, QObject::tr("Spancheck aborted."), false,
                 parent.describeState());

    Q_ASSERT(!parent.config.empty());
    parent.okCommandQueue
          .emplace_back("DO" + QByteArray::number(parent.config.front().address) + "000\r");
    parent.okCommandQueue
          .emplace_back("DO" + QByteArray::number(parent.config.front().address) + "010\r");

    parent.beginExplicitBlank();
    parent.sampleState = SampleState::Measure;
    parent.realtimeStateUpdated = true;

    qCDebug(parent.log) << "Spancheck aborted";
}

void AcquireEcotechNephAurora::command(const Variant::Read &command)
{
    Q_ASSERT(!config.empty());

    spancheckController->command(command);

    if (command.hash("StartZero").exists()) {
        if (sampleState != SampleState::Measure) {
            qCDebug(log) << "Discarding zero request, sample state:"
                         << static_cast<int>(sampleState) << ", response state:"
                         << static_cast<int>(responseState);
        } else {
            switch (responseState) {
            case ResponseState::Passive_Run:
            case ResponseState::Passive_Wait:
            case ResponseState::Passive_Initialize:
            case ResponseState::Autoprobe_Passive_Wait:
            case ResponseState::Autoprobe_Passive_Initialize:
                qCDebug(log) << "Queued zero initiate command from passive state"
                             << static_cast<int>(responseState);
                requestZero();
                break;

            case ResponseState::Interactive_Run_Active:
            case ResponseState::Interactive_Run_Retry:
            case ResponseState::Interactive_Run_Resume:
            case ResponseState::Interactive_Run_Delay:
            case ResponseState::Interactive_Zero_ReadEE:
            case ResponseState::Interactive_Zero_ReadEE_Retry:
            case ResponseState::Interactive_Zero_ReadEE_Resume:
                qCDebug(log) << "Queued zero initiate command from state"
                             << static_cast<int>(responseState);
                requestZero();
                break;

            default:
                qCDebug(log) << "Discarding zero request during in state"
                             << static_cast<int>(responseState);
                break;
            }
        }
    }

    if (command.hash("ApplySpancheckCalibration").exists()) {
        switch (responseState) {
        case ResponseState::Interactive_Run_Active:
        case ResponseState::Interactive_Run_Retry:
        case ResponseState::Interactive_Run_Resume:
        case ResponseState::Interactive_Run_Delay:
        case ResponseState::Interactive_Zero_ReadEE:
        case ResponseState::Interactive_Zero_ReadEE_Retry:
        case ResponseState::Interactive_Zero_ReadEE_Resume: {
            Variant::Write info = Variant::Write::empty();
            std::array<std::vector<double>, 3> applyM;
            std::array<std::vector<double>, 3> applyC;
            bool ok = true;

            for (std::size_t color = 0; color < 3; color++) {
                for (const auto &angle : NephelometerSpancheckController::results(
                        spancheckDetails.read(),
                        NephelometerSpancheckController::EcotechAuroraMPolar,
                        QString::fromStdString(colorCodes[color])).read().toArray()) {
                    double v = angle.toReal();
                    if (!FP::defined(v) || v <= 0.0 || v >= 0.1) {
                        ok = false;
                    }
                    applyM[color].emplace_back(v);

                    info.hash(colorCodes[color]).hash("M").toArray().after_back().setReal(v);
                }
                for (const auto &angle : NephelometerSpancheckController::results(
                        spancheckDetails.read(),
                        NephelometerSpancheckController::EcotechAuroraCPolar,
                        QString::fromStdString(colorCodes[color])).read().toArray()) {
                    double v = angle.toReal();
                    if (!FP::defined(v) || v <= 0.0 || v >= 0.1) {
                        ok = false;
                    }
                    applyC[color].emplace_back(v);

                    info.hash(colorCodes[color]).hash("C").toArray().after_back().setReal(v);
                }
            }
            info.hash("Results").set(spancheckDetails.read());

            if (!ok) {
                event(lastRecordTime,
                      QObject::tr("Invalid spancheck calibration, no parameters changed."), true,
                      info);

                qCDebug(log) << "Discarding attempt to apply invalid calibration from state"
                             << static_cast<int>(responseState);
            } else {
                for (std::size_t color = 0; color < 3; color++) {
                    for (std::size_t angle = 0; angle < applyM[color].size(); angle++) {
                        Util::ByteArray send("**");
                        send += QByteArray::number(config.front().address);
                        send += "PCM";
                        send += QByteArray::number(static_cast<int>(3 - color));
                        send += QByteArray::number(static_cast<int>(angle + 1)).rightJustified(2,
                                                                                               '0');
                        send += ",";
                        send += QByteArray::number(applyM[color][angle], 'f', 10);
                        send += "\n\r";
                        okCommandQueue.emplace_back(std::move(send));
                    }
                    for (std::size_t angle = 0; angle < applyC[color].size(); angle++) {
                        Util::ByteArray send("**");
                        send += QByteArray::number(config.front().address);
                        send += "PCC";
                        send += QByteArray::number(static_cast<int>(3 - color));
                        send += QByteArray::number(static_cast<int>(angle + 1)).rightJustified(2,
                                                                                               '0');
                        send += ",";
                        send += QByteArray::number(applyC[color][angle], 'f', 10);
                        send += "\n\r";
                        okCommandQueue.emplace_back(std::move(send));
                    }
                }

                zeroReadPending = true;

                /* Added in the same firmware version, so check it here */
                if (hasFeature(Feature::FilterControl)) {
                    event(lastRecordTime,
                          QObject::tr("Spancheck results applied to instrument calibration."),
                          false, info);

                    qCDebug(log) << "Applying spancheck calibration from state"
                                 << static_cast<int>(responseState);
                } else {
                    event(lastRecordTime, QObject::tr(
                            "Spancheck results applied to instrument calibration (unsupported firmware detected)."),
                          true, info);

                    qCDebug(log)
                        << "Applying spancheck calibration to unsupported firmware from state"
                        << static_cast<int>(responseState);
                }
            }
            break;
        }

        default:
            qCDebug(log) << "Discarding spancheck apply request during in state"
                         << static_cast<int>(responseState);
            break;
        }
    }

    if (command.hash("Reboot").exists()) {
        qCDebug(log) << "Issuing instrument reboot command";
        if (controlStream) {
            controlStream->writeControl("**" + QByteArray::number(config.front().address) + "B");
        }
    }
}

Variant::Root AcquireEcotechNephAurora::getCommands()
{
    Variant::Root result = spancheckController->getCommands();

    result["StartZero"].hash("DisplayName").setString("Start &Zero");
    result["StartZero"].hash("ToolTip").setString("Start a zero offset adjustment.");
    result["StartZero"].hash("Include").array(0).hash("Type").setString("Equal");
    result["StartZero"].hash("Include").array(0).hash("Value").setString("Run");
    result["StartZero"].hash("Include").array(0).hash("Variable").setString("ZSTATE");

    result["ApplySpancheckCalibration"].hash("DisplayName")
                                       .setString("Apply Spancheck &Calibration");
    result["ApplySpancheckCalibration"].hash("ToolTip")
                                       .setString(
                                               "Apply the result of the last spancheck to the instrument calibration.");
    result["ApplySpancheckCalibration"].hash("Confirm")
                                       .setString(
                                               "This will change the instrument calibration to match the last spancheck.  Continue?");
    result["ApplySpancheckCalibration"].hash("Exclude").array(0).hash("Type").setString("NotEqual");
    result["ApplySpancheckCalibration"].hash("Exclude").array(0).hash("Value").setString("Run");
    result["ApplySpancheckCalibration"].hash("Exclude")
                                       .array(0)
                                       .hash("Variable")
                                       .setString("ZSTATE");
    result["ApplySpancheckCalibration"].hash("Include").array(0).hash("Type").setString("Defined");
    result["ApplySpancheckCalibration"].hash("Include")
                                       .array(0)
                                       .hash("Variable")
                                       .setString("ZSPANCHECK");

    result["Reboot"].hash("DisplayName").setString("&Reboot Instrument");
    result["Reboot"].hash("ToolTip")
                    .setString("Execute a remote reboot command on the instrument.");
    result["Reboot"].hash("Confirm")
                    .setString("Reboot the nephelometer? (communications will be interrupted)");

    return result;
}

AcquisitionInterface::AutoprobeStatus AcquireEcotechNephAurora::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireEcotechNephAurora::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case ResponseState::Interactive_Run_Active:
    case ResponseState::Interactive_Run_Retry:
    case ResponseState::Interactive_Run_Resume:
    case ResponseState::Interactive_Run_Delay:
    case ResponseState::Interactive_Zero_ReadEE:
    case ResponseState::Interactive_Zero_ReadEE_Retry:
    case ResponseState::Interactive_Zero_ReadEE_Resume:
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << static_cast<int>(responseState) << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case ResponseState::Interactive_Start_ReadID:
    case ResponseState::Interactive_Start_ReadStatus:
    case ResponseState::Interactive_Start_SetTime:
    case ResponseState::Interactive_Start_SetFilter:
    case ResponseState::Interactive_Start_SetNormalization:
    case ResponseState::Interactive_Start_ReadEE:
    case ResponseState::Interactive_Start_CheckFlags:
    case ResponseState::Interactive_Start_DisableSpan:
    case ResponseState::Interactive_Start_DisableZero:
    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        qCDebug(log) << "Interactive autoprobe started from state"
                     << static_cast<int>(responseState) << "at" << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        responseState = ResponseState::Interactive_Start_ReadID;
        timeoutAt(time + 2.0);
        outstandingCommands.clear();
        if (controlStream) {
            controlStream->writeControl("ID" + QByteArray::number(config.front().address) + "\r");
        }

        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireEcotechNephAurora::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.empty());
    double unpolledResponseTime = config.front().inactiveDelay;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    discardData(time + 0.5, 1);
    timeoutAt(time + unpolledResponseTime * 7.0 + 3.0);

    qCDebug(log) << "Reset to passive autoprobe from state" << static_cast<int>(responseState)
                 << "at" << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    outstandingCommands.clear();
    responseState = ResponseState::Autoprobe_Passive_Wait;
    autoprobeValidRecords = 0;
    generalStatusUpdated();
}

void AcquireEcotechNephAurora::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 2.0);

    switch (responseState) {
    case ResponseState::Interactive_Start_ReadID:
    case ResponseState::Interactive_Start_ReadStatus:
    case ResponseState::Interactive_Start_SetTime:
    case ResponseState::Interactive_Start_SetFilter:
    case ResponseState::Interactive_Start_SetNormalization:
    case ResponseState::Interactive_Start_ReadEE:
    case ResponseState::Interactive_Start_CheckFlags:
    case ResponseState::Interactive_Start_DisableSpan:
    case ResponseState::Interactive_Start_DisableZero:
    case ResponseState::Interactive_Run_Active:
    case ResponseState::Interactive_Run_Retry:
    case ResponseState::Interactive_Run_Resume:
    case ResponseState::Interactive_Run_Delay:
    case ResponseState::Interactive_Zero_ReadEE:
    case ResponseState::Interactive_Zero_ReadEE_Retry:
    case ResponseState::Interactive_Zero_ReadEE_Resume:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << static_cast<int>(responseState)
                     << "to interactive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        qCDebug(log) << "Promoted from passive state" << static_cast<int>(responseState)
                     << "to interactive acquisition at" << Logging::time(time);

        responseState = ResponseState::Interactive_Start_ReadID;
        timeoutAt(time + 2.0);
        outstandingCommands.clear();
        if (controlStream) {
            controlStream->writeControl("ID" + QByteArray::number(config.front().address) + "\r");
        }

        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireEcotechNephAurora::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.empty());
    double unpolledResponseTime = config.front().inactiveDelay;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    timeoutAt(time + unpolledResponseTime * 2.0 + 1.0);

    switch (responseState) {
    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from autoprobe passive state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        responseState = ResponseState::Passive_Initialize;
        break;

    case ResponseState::Interactive_Run_Active:
    case ResponseState::Interactive_Run_Retry:
    case ResponseState::Interactive_Run_Resume:
    case ResponseState::Interactive_Run_Delay:
    case ResponseState::Interactive_Zero_ReadEE:
    case ResponseState::Interactive_Zero_ReadEE_Retry:
    case ResponseState::Interactive_Zero_ReadEE_Resume:
        /* Have communications, so don't discard anything we've done so far,
         * and just assume something else is transparently assuming control. */
        responseState = ResponseState::Passive_Run;

        qCDebug(log) << "Promoted from interactive to passive acquisition at"
                     << Logging::time(time);
        break;

    case ResponseState::Interactive_Start_ReadID:
    case ResponseState::Interactive_Start_ReadStatus:
    case ResponseState::Interactive_Start_SetTime:
    case ResponseState::Interactive_Start_SetFilter:
    case ResponseState::Interactive_Start_SetNormalization:
    case ResponseState::Interactive_Start_ReadEE:
    case ResponseState::Interactive_Start_CheckFlags:
    case ResponseState::Interactive_Start_DisableSpan:
    case ResponseState::Interactive_Start_DisableZero:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        /* We didn't have full communications, so just drop it and start
         * over. */
        qCDebug(log) << "Promoted from state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);

        okCommandQueue.clear();
        outstandingCommands.clear();
        discardData(FP::undefined());

        commandState = CommandState::SystemState;
        responseState = ResponseState::Passive_Initialize;
        generalStatusUpdated();
        break;
    }
}

static const quint16 serializedStateVersion = 1;

void AcquireEcotechNephAurora::serializeState(QDataStream &stream)
{
    PauseLock paused(*this);

    stream << serializedStateVersion;

    stream << spancheckDetails.getStart();
    stream << spancheckDetails.read();

    stream << Tw << Pw;
    for (const auto &add : Bsz) {
        stream << add;
    }
}

void AcquireEcotechNephAurora::deserializeState(QDataStream &stream)
{
    quint16 version = 0;
    stream >> version;
    if (version != serializedStateVersion)
        return;

    PauseLock paused(*this);

    double start = FP::undefined();
    stream >> start;
    spancheckDetails.setStart(start);
    spancheckDetails.setEnd(FP::undefined());
    stream >> spancheckDetails.root();

    stream >> Tw >> Pw;
    for (auto &add : Bsz) {
        stream >> add;
    }
    if (!config.empty() && config.front().zeroMode == Configuration::Zero::Offset) {
        updateEffectiveZero();

        realtimeZeroUpdated = true;
        persistentZeroUpdated = true;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireEcotechNephAurora::getDefaults()
{
    AutomaticDefaults result;
    result.name = "S$1$2";
    result.setSerialN81(38400);
    return result;
}


ComponentOptions AcquireEcotechNephAuroraComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);
    return options;
}

ComponentOptions AcquireEcotechNephAuroraComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireEcotechNephAuroraComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples(true));
    examples.append(getPassiveExamples());
    return examples;
}

QList<ComponentExample> AcquireEcotechNephAuroraComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data")));

    return examples;
}

bool AcquireEcotechNephAuroraComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireEcotechNephAuroraComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

std::unique_ptr<AcquisitionInterface> AcquireEcotechNephAuroraComponent::createAcquisitionPassive(
        const ComponentOptions &options,
        const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(
            new AcquireEcotechNephAurora(options, loggingContext));
}

std::unique_ptr<AcquisitionInterface> AcquireEcotechNephAuroraComponent::createAcquisitionPassive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(
            new AcquireEcotechNephAurora(config, loggingContext));
}

std::unique_ptr<AcquisitionInterface> AcquireEcotechNephAuroraComponent::createAcquisitionAutoprobe(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireEcotechNephAurora>
            i(new AcquireEcotechNephAurora(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireEcotechNephAuroraComponent::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                              const std::string &loggingContext)
{
    std::unique_ptr<AcquireEcotechNephAurora>
            i(new AcquireEcotechNephAurora(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
