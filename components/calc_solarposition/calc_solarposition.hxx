/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CALCSOLARPOSITION_H
#define CALCSOLARPOSITION_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QList>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

class CalcSolarPosition : public CPD3::Data::SegmentProcessingStage {
    struct Processing {
        CPD3::Data::DynamicSequenceSelection *operateFlags;
        CPD3::Data::DynamicSequenceSelection *operateAzimuth;
        CPD3::Data::DynamicSequenceSelection *operateElevation;
        CPD3::Data::DynamicSequenceSelection *operateMagnitude;

        CPD3::Data::DynamicInput *inputMagnitude;
        CPD3::Data::DynamicInput *inputLatitude;
        CPD3::Data::DynamicInput *inputLongitude;

        CPD3::Data::DynamicInput *darkAngle;

        double latitude;
        double longitude;
        double previousTime;
        CPD3::Data::SequenceName::Component stationLocation;

        Processing() : operateFlags(NULL),
                       operateAzimuth(NULL),
                       operateElevation(NULL),
                       operateMagnitude(NULL),
                       inputMagnitude(NULL),
                       inputLatitude(NULL),
                       inputLongitude(NULL), darkAngle(NULL),
                       latitude(CPD3::FP::undefined()),
                       longitude(CPD3::FP::undefined()),
                       previousTime(CPD3::FP::undefined())
        { }
    };

    CPD3::Data::DynamicInput *defaultMagnitude;
    CPD3::Data::DynamicInput *defaultLatitude;
    CPD3::Data::DynamicInput *defaultLongitude;
    CPD3::Data::DynamicInput *defaultDarkAngle;
    bool defaultEnablePosition;
    CPD3::Data::SequenceName::ComponentSet filterSuffixes;

    bool restrictedInputs;

    void handleOptions(const CPD3::ComponentOptions &options);

    std::vector<Processing> processing;

    void handleNewProcessing(const CPD3::Data::SequenceName &unit,
                             int id,
                             CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control);

    void registerPossibleInput(const CPD3::Data::SequenceName &unit,
                               CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                               int filterID = -1);

    void createPositionInputs(Processing &p, const CPD3::Data::SequenceName::Component &station);

public:
    CalcSolarPosition();

    CalcSolarPosition(const CPD3::ComponentOptions &options);

    CalcSolarPosition(const CPD3::ComponentOptions &options,
                      double start, double end, const QList<CPD3::Data::SequenceName> &inputs);

    CalcSolarPosition(double start,
                      double end,
                      const CPD3::Data::SequenceName::Component &station,
                      const CPD3::Data::SequenceName::Component &archive,
                      const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CalcSolarPosition();

    void unhandled(const CPD3::Data::SequenceName &unit,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;

    QSet<double> metadataBreaks(int id) override;

    CalcSolarPosition(QDataStream &stream);

    void serialize(QDataStream &stream) override;
};

class CalcSolarPositionComponent
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.calc_solarposition"
                              FILE
                              "calc_solarposition.json")
public:
    virtual QString getBasicSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterDynamic(const CPD3::ComponentOptions &options);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined(const CPD3::ComponentOptions &options,
                                                                 double start,
                                                                 double end,
                                                                 const QList<
                                                                         CPD3::Data::SequenceName> &inputs);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                         double end,
                                                                         const CPD3::Data::SequenceName::Component &station,
                                                                         const CPD3::Data::SequenceName::Component &archive,
                                                                         const CPD3::Data::ValueSegment::Transfer &config);

    virtual CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream);
};

#endif
