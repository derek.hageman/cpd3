/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "core/environment.hxx"
#include "algorithms/solar.hxx"

#include "calc_solarposition.hxx"

using namespace CPD3;
using namespace CPD3::Data;


void CalcSolarPosition::handleOptions(const ComponentOptions &options)
{
    if (options.isSet("magnitude")) {
        defaultMagnitude = qobject_cast<DynamicInputOption *>(options.get("magnitude"))->getInput();
    } else {
        defaultMagnitude = new DynamicInput::Constant(1.0);
    }

    if (options.isSet("latitude")) {
        defaultLatitude = qobject_cast<DynamicInputOption *>(options.get("latitude"))->getInput();
    } else {
        defaultLatitude = NULL;
    }

    if (options.isSet("longitude")) {
        defaultLongitude = qobject_cast<DynamicInputOption *>(options.get("longitude"))->getInput();
    } else {
        defaultLongitude = NULL;
    }

    if (options.isSet("dark")) {
        defaultDarkAngle = qobject_cast<DynamicInputOption *>(options.get("dark"))->getInput();
    } else {
        defaultDarkAngle = new DynamicInput::Constant(0.0);
    }

    if (options.isSet("position")) {
        defaultEnablePosition =
                qobject_cast<ComponentOptionBoolean *>(options.get("position"))->get();
    } else {
        defaultEnablePosition = false;
    }

    restrictedInputs = false;

    if (options.isSet("flags") ||
            options.isSet("output-azimuth") ||
            options.isSet("output-elevation") ||
            options.isSet("output-magnitude")) {
        restrictedInputs = true;

        Processing p;

        if (defaultMagnitude != NULL) {
            p.inputMagnitude = defaultMagnitude->clone();
        } else {
            p.inputMagnitude = new DynamicInput::Constant;
        }

        if (defaultDarkAngle != NULL) {
            p.darkAngle = defaultDarkAngle->clone();
        } else {
            p.darkAngle = new DynamicInput::Constant;
        }

        SequenceName::Component suffix;
        SequenceName::Component station;
        SequenceName::Component archive;
        SequenceName::Flavors flavors;
        if (options.isSet("flags")) {
            p.operateFlags = qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("flags"))->getOperator();
            for (const auto &name : p.operateFlags->getAllUnits()) {
                suffix = Util::suffix(name.getVariable(), '_');
                station = name.getStation();
                archive = name.getArchive();
                flavors = name.getFlavors();
                if (suffix.length() != 0)
                    break;
            }
        }

        if (options.isSet("output-azimuth")) {
            p.operateAzimuth = qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("output-azimuth"))->getOperator();
            for (const auto &name : p.operateAzimuth->getAllUnits()) {
                suffix = Util::suffix(name.getVariable(), '_');
                station = name.getStation();
                archive = name.getArchive();
                flavors = name.getFlavors();
                if (suffix.length() != 0)
                    break;
            }
        }
        if (options.isSet("output-elevation")) {
            p.operateElevation =
                    qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-elevation"))
                            ->getOperator();
            for (const auto &name : p.operateElevation->getAllUnits()) {
                suffix = Util::suffix(name.getVariable(), '_');
                station = name.getStation();
                archive = name.getArchive();
                flavors = name.getFlavors();
                if (suffix.length() != 0)
                    break;
            }
        }
        if (options.isSet("output-magnitude")) {
            p.operateMagnitude =
                    qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-magnitude"))
                            ->getOperator();
            for (const auto &name : p.operateMagnitude->getAllUnits()) {
                suffix = Util::suffix(name.getVariable(), '_');
                station = name.getStation();
                archive = name.getArchive();
                flavors = name.getFlavors();
                if (suffix.length() != 0)
                    break;
            }
        }

        if (p.operateFlags == NULL) {
            p.operateFlags = new DynamicSequenceSelection::Single(
                    SequenceName(station, archive, "F1_" + suffix, flavors));
        }

        if (p.operateAzimuth == NULL) {
            if (defaultEnablePosition) {
                p.operateAzimuth = new DynamicSequenceSelection::Single(
                        SequenceName(station, archive, "ZSolAz_" + suffix, flavors));
            } else {
                p.operateAzimuth = new DynamicSequenceSelection::None;
            }
        }

        if (p.operateElevation == NULL) {
            if (defaultEnablePosition) {
                p.operateElevation = new DynamicSequenceSelection::Single(
                        SequenceName(station, archive, "ZSolEl_" + suffix, flavors));
            } else {
                p.operateElevation = new DynamicSequenceSelection::None;
            }
        }

        if (p.operateMagnitude == NULL) {
            if (defaultEnablePosition) {
                p.operateMagnitude = new DynamicSequenceSelection::Single(
                        SequenceName(station, archive, "ZSolMa_" + suffix, flavors));
            } else {
                p.operateMagnitude = new DynamicSequenceSelection::None;
            }
        }

        createPositionInputs(p, station);

        processing.push_back(p);
    }

    if (options.isSet("suffix")) {
        filterSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->get());
    }
}


CalcSolarPosition::CalcSolarPosition()
{ Q_ASSERT(false); }

CalcSolarPosition::CalcSolarPosition(const ComponentOptions &options)
{
    handleOptions(options);
}

CalcSolarPosition::CalcSolarPosition(const ComponentOptions &options,
                                     double start,
                                     double end,
                                     const QList<SequenceName> &inputs)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    handleOptions(options);

    for (QList<SequenceName>::const_iterator unit = inputs.constBegin(), endU = inputs.constEnd();
            unit != endU;
            ++unit) {
        CalcSolarPosition::unhandled(*unit, NULL);
    }
}

CalcSolarPosition::CalcSolarPosition(double start,
                                     double end,
                                     const SequenceName::Component &station,
                                     const SequenceName::Component &archive,
                                     const ValueSegment::Transfer &config)
{
    defaultMagnitude = NULL;
    defaultLatitude = NULL;
    defaultLongitude = NULL;
    defaultDarkAngle = NULL;
    defaultEnablePosition = false;
    restrictedInputs = true;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    for (const auto &child : children) {
        Processing p;

        p.operateFlags = DynamicSequenceSelection::fromConfiguration(config,
                                                                     QString("%1/Flags").arg(
                                                                             QString::fromStdString(
                                                                                     child)), start,
                                                                     end);

        p.operateAzimuth = DynamicSequenceSelection::fromConfiguration(config,
                                                                       QString("%1/Azimuth").arg(
                                                                               QString::fromStdString(
                                                                                       child)),
                                                                       start, end);
        p.operateElevation = DynamicSequenceSelection::fromConfiguration(config,
                                                                         QString("%1/Elevation").arg(
                                                                                 QString::fromStdString(
                                                                                         child)),
                                                                         start, end);
        p.operateMagnitude = DynamicSequenceSelection::fromConfiguration(config,
                                                                         QString("%1/Magnitude").arg(
                                                                                 QString::fromStdString(
                                                                                         child)),
                                                                         start, end);


        p.inputLatitude = DynamicInput::fromConfiguration(config, QString("%1/Latitude").arg(
                QString::fromStdString(child)), start, end);
        p.inputLongitude = DynamicInput::fromConfiguration(config, QString("%1/Longitude").arg(
                QString::fromStdString(child)), start, end);
        p.inputMagnitude = DynamicInput::fromConfiguration(config, QString("%1/InputMagnitude").arg(
                QString::fromStdString(child)), start, end);

        p.darkAngle = DynamicInput::fromConfiguration(config, QString("%1/DarkAngle").arg(
                QString::fromStdString(child)), start, end);

        p.operateFlags->registerExpected(station, archive);
        p.operateAzimuth->registerExpected(station, archive);
        p.operateElevation->registerExpected(station, archive);
        p.operateMagnitude->registerExpected(station, archive);
        p.inputLatitude->registerExpected(station, archive);
        p.inputLongitude->registerExpected(station, archive);
        p.inputMagnitude->registerExpected(station, archive);
        p.darkAngle->registerExpected(station, archive);

        processing.push_back(p);
    }
}


CalcSolarPosition::~CalcSolarPosition()
{
    if (defaultMagnitude != NULL)
        delete defaultMagnitude;
    if (defaultLatitude != NULL)
        delete defaultLatitude;
    if (defaultLongitude != NULL)
        delete defaultLongitude;
    if (defaultDarkAngle != NULL)
        delete defaultDarkAngle;

    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        delete p->operateFlags;
        delete p->operateAzimuth;
        delete p->operateElevation;
        delete p->operateMagnitude;
        delete p->inputMagnitude;
        delete p->inputLatitude;
        delete p->inputLongitude;
        delete p->darkAngle;
    }
}

void CalcSolarPosition::handleNewProcessing(const SequenceName &unit,
                                            int id,
                                            SegmentProcessingStage::SequenceHandlerControl *control)
{
    Processing *p = &processing[id];

    SequenceName::Set reg;

    p->operateFlags->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->operateFlags->getAllUnits(), reg);

    p->operateAzimuth
     ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->operateAzimuth->getAllUnits(), reg);

    p->operateElevation
     ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->operateElevation->getAllUnits(), reg);

    p->operateMagnitude
     ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->operateMagnitude->getAllUnits(), reg);

    if (!control)
        return;

    for (const auto &n : reg) {
        bool isNew = !control->isHandled(n);
        control->filterUnit(n, id);
        if (isNew)
            registerPossibleInput(n, control, id);
    }
}

void CalcSolarPosition::registerPossibleInput(const SequenceName &unit,
                                              SegmentProcessingStage::SequenceHandlerControl *control,
                                              int filterID)
{
    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        bool usedAsInput = false;
        if (processing[id].inputMagnitude->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }
        if (processing[id].inputLatitude->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }
        if (processing[id].inputLongitude->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }
        if (processing[id].darkAngle->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }
        if (usedAsInput && id != filterID)
            handleNewProcessing(unit, id, control);
    }
    if (defaultMagnitude != NULL && defaultMagnitude->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultLatitude != NULL && defaultLatitude->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultLongitude != NULL && defaultLongitude->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultDarkAngle != NULL && defaultDarkAngle->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
}

void CalcSolarPosition::createPositionInputs(Processing &p, const SequenceName::Component &station)
{
    if (p.inputLatitude == NULL) {
        if (defaultLatitude != NULL) {
            p.inputLatitude = defaultLatitude->clone();
        } else {
            DynamicInput::Variable *in = new DynamicInput::Variable;
            p.inputLatitude = in;
            in->set(SequenceMatch::OrderedLookup(
                    SequenceMatch::Element(QString::fromStdString(station), "configuration",
                                           "site")), Calibration(), FP::undefined(),
                    "Location/Latitude");
            in->registerExpected(station, "configuration", "site", SequenceName::Flavors());
        }
    }

    if (p.inputLongitude == NULL) {
        if (defaultLongitude != NULL) {
            p.inputLongitude = defaultLongitude->clone();
        } else {
            DynamicInput::Variable *in = new DynamicInput::Variable;
            p.inputLongitude = in;
            in->set(SequenceMatch::OrderedLookup(
                    SequenceMatch::Element(QString::fromStdString(station), "configuration",
                                           "site")), Calibration(), FP::undefined(),
                    "Location/Longitude");
            in->registerExpected(station, "configuration", "site", SequenceName::Flavors());
        }
    }
}

void CalcSolarPosition::unhandled(const SequenceName &unit,
                                  SegmentProcessingStage::SequenceHandlerControl *control)
{
    registerPossibleInput(unit, control);

    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].operateFlags->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            handleNewProcessing(unit, id, control);
            return;
        }
        if (processing[id].operateAzimuth->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            handleNewProcessing(unit, id, control);
            return;
        }
        if (processing[id].operateElevation->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            handleNewProcessing(unit, id, control);
            return;
        }
        if (processing[id].operateMagnitude->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            handleNewProcessing(unit, id, control);
            return;
        }
    }

    if (restrictedInputs)
        return;

    if (unit.hasFlavor(SequenceName::flavor_cover) || unit.hasFlavor(SequenceName::flavor_stats))
        return;

    if ((defaultLatitude == NULL || defaultLongitude == NULL) &&
            unit.getArchive() == "configuration" &&
            unit.getVariable() == "site" &&
            !unit.isDefaultStation()) {
        for (int id = 0, max = (int) processing.size(); id < max; id++) {
            if (processing[id].stationLocation == unit.getStation())
                return;
        }

        Processing p;

        p.stationLocation = unit.getStation();

        p.operateFlags = new DynamicSequenceSelection::None;
        p.operateAzimuth = new DynamicSequenceSelection::None;
        p.operateElevation = new DynamicSequenceSelection::None;
        p.operateMagnitude = new DynamicSequenceSelection::None;
        p.inputMagnitude = new DynamicInput::Constant;
        p.darkAngle = new DynamicInput::Constant;

        createPositionInputs(p, unit.getStation());
        p.inputLatitude->registerInput(unit);
        p.inputLongitude->registerInput(unit);

        int id = (int) processing.size();
        processing.push_back(p);

        if (control != NULL) {
            control->inputUnit(unit, id);
        }

        return;
    }

    QRegExp reCheck("F1?_(.+)");
    if (!reCheck.exactMatch(unit.getVariableQString()))
        return;
    auto suffix = reCheck.cap(1).toStdString();
    if (suffix.empty())
        return;
    if (!filterSuffixes.empty() && !filterSuffixes.count(suffix))
        return;

    Processing p;

    bool hit = false;
    for (const auto &check : processing) {
        if (check.stationLocation != unit.getStation())
            continue;

        hit = true;

        p.latitude = check.latitude;
        p.longitude = check.longitude;

        p.inputLatitude = check.inputLatitude->clone();
        p.inputLongitude = check.inputLongitude->clone();

        break;
    }

    if (!hit) {
        Processing psite;

        psite.stationLocation = unit.getStation();

        psite.operateFlags = new DynamicSequenceSelection::None;
        psite.operateAzimuth = new DynamicSequenceSelection::None;
        psite.operateElevation = new DynamicSequenceSelection::None;
        psite.operateMagnitude = new DynamicSequenceSelection::None;
        psite.inputMagnitude = new DynamicInput::Constant;
        psite.darkAngle = new DynamicInput::Constant;

        createPositionInputs(psite, unit.getStation());
        psite.inputLatitude->registerInput(unit);
        psite.inputLongitude->registerInput(unit);

        int id = (int) processing.size();
        processing.push_back(psite);

        registerPossibleInput(unit, control);

        p.latitude = processing[id].latitude;
        p.longitude = processing[id].longitude;

        p.inputLatitude = processing[id].inputLatitude->clone();
        p.inputLongitude = processing[id].inputLongitude->clone();
    }

    p.inputLatitude->registerInput(unit);
    p.inputLongitude->registerInput(unit);

    SequenceName azimuthUnit
            (unit.getStation(), unit.getArchive(), "ZSolAz_" + suffix, unit.getFlavors());
    SequenceName elevationUnit
            (unit.getStation(), unit.getArchive(), "ZSolEl_" + suffix, unit.getFlavors());
    SequenceName magnitudeUnit
            (unit.getStation(), unit.getArchive(), "ZSolMa_" + suffix, unit.getFlavors());

    p.operateFlags =
            new DynamicSequenceSelection::Match(unit.getStationQString(), unit.getArchiveQString(),
                                                QString::fromStdString("F1?_" + suffix),
                                                unit.getFlavors());
    p.operateFlags->registerInput(unit);

    if (defaultMagnitude != NULL) {
        p.inputMagnitude = defaultMagnitude->clone();
    } else {
        p.inputMagnitude = new DynamicInput::Constant;
    }

    if (defaultDarkAngle != NULL) {
        p.darkAngle = defaultDarkAngle->clone();
    } else {
        p.darkAngle = new DynamicInput::Constant;
    }

    if (defaultEnablePosition) {
        p.operateFlags->registerInput(azimuthUnit);
        p.operateFlags->registerInput(elevationUnit);
        p.operateFlags->registerInput(magnitudeUnit);

        p.inputLatitude->registerInput(azimuthUnit);
        p.inputLatitude->registerInput(elevationUnit);
        p.inputLatitude->registerInput(magnitudeUnit);

        p.inputLongitude->registerInput(azimuthUnit);
        p.inputLongitude->registerInput(elevationUnit);
        p.inputLongitude->registerInput(magnitudeUnit);

        p.operateAzimuth = new DynamicSequenceSelection::Single(azimuthUnit);
        p.operateAzimuth->registerInput(unit);
        p.operateAzimuth->registerInput(azimuthUnit);
        p.operateAzimuth->registerInput(elevationUnit);
        p.operateAzimuth->registerInput(magnitudeUnit);

        p.operateElevation = new DynamicSequenceSelection::Single(elevationUnit);
        p.operateElevation->registerInput(unit);
        p.operateElevation->registerInput(azimuthUnit);
        p.operateElevation->registerInput(elevationUnit);
        p.operateElevation->registerInput(magnitudeUnit);

        p.operateMagnitude = new DynamicSequenceSelection::Single(magnitudeUnit);
        p.operateMagnitude->registerInput(unit);
        p.operateMagnitude->registerInput(azimuthUnit);
        p.operateMagnitude->registerInput(elevationUnit);
        p.operateMagnitude->registerInput(magnitudeUnit);
    } else {
        p.operateAzimuth = new DynamicSequenceSelection::None;
        p.operateElevation = new DynamicSequenceSelection::None;
        p.operateMagnitude = new DynamicSequenceSelection::None;
    }

    int id = (int) processing.size();
    processing.push_back(p);

    if (control != NULL) {
        for (const auto &n : p.inputLatitude->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : p.inputLongitude->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : p.inputMagnitude->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : p.darkAngle->getUsedInputs()) {
            control->inputUnit(n, id);
        }

        control->filterUnit(unit, id);

        if (defaultEnablePosition) {
            control->filterUnit(azimuthUnit, id);
            control->filterUnit(elevationUnit, id);
            control->filterUnit(magnitudeUnit, id);
        }
    }
}

static const Variant::Flag FLAG_DARK = "SolarDark";
static const Variant::Flag FLAG_NOON = "SolarNoon";

void CalcSolarPosition::process(int id, SequenceSegment &data)
{
    Processing *proc = &processing[id];

    double time = data.getStart();
    if (!FP::defined(time)) {
        time = data.getEnd();
        if (!FP::defined(time))
            return;
    } else if (FP::defined(data.getEnd())) {
        time += data.getEnd();
        time /= 2.0;
    }

    {
        double v = proc->inputLatitude->get(data);
        if (FP::defined(v))
            proc->latitude = v;
        v = proc->inputLongitude->get(data);
        if (FP::defined(v))
            proc->longitude = v;
    }

    Algorithms::Solar solar(time, proc->latitude, proc->longitude);
    Algorithms::Solar::Position position = solar.position();

    bool isDark = position.isDark(proc->darkAngle->get(data));
    bool isNoon = false;
    double solarNoon = solar.noon();
    if (FP::defined(solarNoon)) {
        if (FP::defined(data.getStart()) &&
                FP::defined(data.getEnd()) &&
                data.getStart() < solarNoon &&
                data.getEnd() >= solarNoon) {
            isNoon = true;
        } else if (FP::defined(proc->previousTime) &&
                FP::defined(data.getStart()) &&
                proc->previousTime < solarNoon &&
                data.getStart() >= solarNoon) {
            isNoon = true;
        }
    }

    proc->previousTime = data.getEnd();

    if (isDark || isNoon) {
        for (const auto &i : proc->operateFlags->get(data)) {
            if (!data.exists(i))
                continue;
            auto d = data[i];
            if (isDark)
                d.applyFlag(FLAG_DARK);
            if (isNoon)
                d.applyFlag(FLAG_NOON);
        }
    }

    for (const auto &i : proc->operateAzimuth->get(data)) {
        data[i].setDouble(position.azimuth());
    }

    for (const auto &i : proc->operateElevation->get(data)) {
        data[i].setDouble(position.elevation());
    }

    double mag = proc->inputMagnitude->get(data);
    for (const auto &i : proc->operateMagnitude->get(data)) {
        data[i].setDouble(mag);
    }
}


void CalcSolarPosition::processMeta(int id, SequenceSegment &data)
{
    Processing *p = &processing[id];

    Variant::Root meta;

    meta["By"].setString("calc_solarposition");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    meta["Parameters"].hash("Latitude") = p->inputLatitude->describe(data);
    meta["Parameters"].hash("Longitude") = p->inputLongitude->describe(data);

    for (auto i : p->operateFlags->get(data)) {
        i.setMeta();
        if (!data.exists(i))
            continue;

        data[i].metadata("Processing").toArray().after_back().set(meta);

        auto flagMeta = data[i].metadataSingleFlag(FLAG_DARK);
        flagMeta.hash("Origin").toArray().after_back().setString("calc_solarposition");
        flagMeta.hash("Description").setString("Sun position is below the horizon");
        flagMeta.hash("Angle").setString(p->darkAngle->describe(data));

        flagMeta = data[i].metadataSingleFlag(FLAG_NOON);
        flagMeta.hash("Origin").toArray().after_back().setString("calc_solarposition");
        flagMeta.hash("Description").setString("Sun position is at solar noon");
    }

    auto azimuth = p->operateAzimuth->get(data);
    auto elevation = p->operateElevation->get(data);
    auto magnitude = p->operateMagnitude->get(data);

    meta["Parameters"].hash("Magnitude") = p->inputMagnitude->describe(data);

    for (auto i : azimuth) {
        i.setMeta();
        if (!data[i].exists()) {
            data[i].metadataReal("Description").setString("Solar azimuth angle from true north");
            data[i].metadataReal("Format").setString("000.0");
            data[i].metadataReal("Units").setString("degrees");

            data[i].metadataReal("Smoothing").hash("Mode").setString("Vector3D");
            data[i].metadataReal("Smoothing")
                   .hash("Parameters")
                   .hash("Azimuth")
                   .setString(i.getVariable());
            if (!elevation.empty()) {
                data[i].metadataReal("Smoothing")
                       .hash("Parameters")
                       .hash("Elevation")
                       .setString(elevation.begin()->getVariable());
            }
            if (!magnitude.empty()) {
                data[i].metadataReal("Smoothing")
                       .hash("Parameters")
                       .hash("Magnitude")
                       .setString(magnitude.begin()->getVariable());
            }
        }
        data[i].metadata("Processing").toArray().after_back().set(meta);
    }

    for (auto i : elevation) {
        i.setMeta();
        if (!data[i].exists()) {
            data[i].metadataReal("Description")
                   .setString("Solar elevation angle above the ideal horizon");
            data[i].metadataReal("Format").setString("00.0");
            data[i].metadataReal("Units").setString("degrees");

            data[i].metadataReal("Smoothing").hash("Mode").setString("Vector3D");
            data[i].metadataReal("Smoothing")
                   .hash("Parameters")
                   .hash("Elevation")
                   .setString(i.getVariable());
            if (!azimuth.empty()) {
                data[i].metadataReal("Smoothing")
                       .hash("Parameters")
                       .hash("Azimuth")
                       .setString(azimuth.begin()->getVariable());
            }
            if (!magnitude.empty()) {
                data[i].metadataReal("Smoothing")
                       .hash("Parameters")
                       .hash("Magnitude")
                       .setString(magnitude.begin()->getVariable());
            }
        }
        data[i].metadata("Processing").toArray().after_back().set(meta);
    }

    for (auto i : magnitude) {
        i.setMeta();
        if (!data[i].exists()) {
            Variant::Read metabase = Variant::Read::empty();
            for (auto c : p->inputMagnitude->getUsedInputs()) {
                c.setMeta();
                if (!data.exists(c))
                    continue;
                metabase = data[c];
                if (metabase.exists())
                    break;
            }

            if (metabase.getType() != Variant::Type::MetadataReal) {
                data[i].metadataReal("Description").setString("Solar magnitude vector value");
            } else {
                data[i].set(metabase);
                data[i].metadata("Smoothing").setEmpty();
            }

            data[i].metadata("Smoothing").hash("Mode").setString("Vector3D");
            data[i].metadata("Smoothing")
                   .hash("Parameters")
                   .hash("Magnitude")
                   .setString(i.getVariable());
            if (!azimuth.empty()) {
                data[i].metadata("Smoothing")
                       .hash("Parameters")
                       .hash("Azimuth")
                       .setString(azimuth.begin()->getVariable());
            }
            if (!elevation.empty()) {
                data[i].metadata("Smoothing")
                       .hash("Parameters")
                       .hash("Elevation")
                       .setString(elevation.begin()->getVariable());
            }
        }
        data[i].metadata("Processing").toArray().after_back().set(meta);
    }
}


SequenceName::Set CalcSolarPosition::requestedInputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        Util::merge(p->inputMagnitude->getUsedInputs(), out);
        Util::merge(p->inputLatitude->getUsedInputs(), out);
        Util::merge(p->inputLongitude->getUsedInputs(), out);
        Util::merge(p->darkAngle->getUsedInputs(), out);
    }
    return out;
}

SequenceName::Set CalcSolarPosition::predictedOutputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        Util::merge(p->operateAzimuth->getAllUnits(), out);
        Util::merge(p->operateElevation->getAllUnits(), out);
        Util::merge(p->operateMagnitude->getAllUnits(), out);
    }
    return out;
}

QSet<double> CalcSolarPosition::metadataBreaks(int id)
{
    Processing *p = &processing[id];
    QSet<double> result;
    Util::merge(p->operateFlags->getChangedPoints(), result);
    Util::merge(p->operateAzimuth->getChangedPoints(), result);
    Util::merge(p->operateElevation->getChangedPoints(), result);
    Util::merge(p->operateMagnitude->getChangedPoints(), result);
    Util::merge(p->inputLatitude->getChangedPoints(), result);
    Util::merge(p->inputLongitude->getChangedPoints(), result);
    Util::merge(p->inputMagnitude->getChangedPoints(), result);
    Util::merge(p->darkAngle->getChangedPoints(), result);
    return result;
}

CalcSolarPosition::CalcSolarPosition(QDataStream &stream)
{
    stream >> defaultMagnitude;
    stream >> defaultLatitude;
    stream >> defaultLongitude;
    stream >> defaultDarkAngle;
    stream >> defaultEnablePosition;
    stream >> filterSuffixes;
    stream >> restrictedInputs;

    quint32 n;
    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        Processing p;
        stream >> p.operateFlags;
        stream >> p.operateAzimuth;
        stream >> p.operateElevation;
        stream >> p.operateMagnitude;
        stream >> p.inputMagnitude;
        stream >> p.inputLatitude;
        stream >> p.inputLongitude;
        stream >> p.darkAngle;
        stream >> p.latitude;
        stream >> p.longitude;
        stream >> p.longitude;
        stream >> p.previousTime;
        stream >> p.stationLocation;
        processing.push_back(p);
    }
}

void CalcSolarPosition::serialize(QDataStream &stream)
{
    stream << defaultMagnitude;
    stream << defaultLatitude;
    stream << defaultLongitude;
    stream << defaultDarkAngle;
    stream << defaultEnablePosition;
    stream << filterSuffixes;
    stream << restrictedInputs;

    quint32 n = (quint32) processing.size();
    stream << n;
    for (int i = 0; i < (int) n; i++) {
        stream << processing[i].operateFlags;
        stream << processing[i].operateAzimuth;
        stream << processing[i].operateElevation;
        stream << processing[i].operateMagnitude;
        stream << processing[i].inputMagnitude;
        stream << processing[i].inputLatitude;
        stream << processing[i].inputLongitude;
        stream << processing[i].darkAngle;
        stream << processing[i].latitude;
        stream << processing[i].longitude;
        stream << processing[i].longitude;
        stream << processing[i].previousTime;
        stream << processing[i].stationLocation;

    }
}


QString CalcSolarPositionComponent::getBasicSerializationName() const
{ return QString::fromLatin1("calc_solarposition"); }

ComponentOptions CalcSolarPositionComponent::getOptions()
{
    ComponentOptions options;

    options.add("magnitude", new DynamicInputOption(tr("magnitude", "name"), tr("Input magnitude"),
                                                    tr("This is the magnitude set for the solar position vector.  "
                                                       "This value is used when data are averaged."),
                                                    tr("1")));

    options.add("dark", new DynamicInputOption(tr("dark", "name"), tr("Dark threshold angle"),
                                               tr("This is the number of degrees below the horizon the sun must be "
                                                  "for it to be considered dark and the flag set."),
                                               tr("0")));

    options.add("latitude", new DynamicInputOption(tr("latitude", "name"), tr("Latitude"),
                                                   tr("This is the latitude in degrees north to calculate parameter for."),
                                                   tr("Automatic from data")));

    options.add("longitude", new DynamicInputOption(tr("longitude", "name"), tr("Latitude"),
                                                    tr("This is the longitude in degrees east to calculate parameter for."),
                                                    tr("Automatic from data")));

    options.add("position", new ComponentOptionBoolean(tr("position", "name"),
                                                       tr("Enable solar position output"),
                                                       tr("This enables the generation of variables that contain the solar position.  "
                                                          "This is the current azimuth and elevation angles as well as a magnitude vector "
                                                          "used in averaging."), QString()));

    options.add("flags", new DynamicSequenceSelectionOption(tr("flags", "name"), tr("Output flags"),
                                                            tr("This is flags variable that the solar state flags are added to.  "
                                                               "The calculation adds flags for dark (below the horizon) and a flag "
                                                               "on the data point nearest solar noon."),
                                                            tr("All available flags variables")));

    options.add("output-azimuth", new DynamicSequenceSelectionOption(tr("output-azimuth", "name"),
                                                                     tr("Output azimuth angle"),
                                                                     tr("This is the output that contains the calculated solar azimuth angle."),
                                                                     tr("Automatic if enabled")));

    options.add("output-elevation",
                new DynamicSequenceSelectionOption(tr("output-elevation", "name"),
                                                   tr("Output elevation angle"),
                                                   tr("This is the output that contains the calculated solar elevation angle."),
                                                   tr("Automatic if enabled")));

    options.add("output-magnitude",
                new DynamicSequenceSelectionOption(tr("output-magnitude", "name"),
                                                   tr("Output vector magngitude"),
                                                   tr("This is the output that contains the specified vector magnitude for use "
                                                      "in averaging."),
                                                   tr("Automatic if enabled")));

    options.add("suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments", "name"),
                                                                 tr("Instrument suffixes to correct"),
                                                                 tr("These are the instrument suffixes to calculate flows for.  "
                                                                    "For example P01 would usually specifies the stack flow pitot "
                                                                    "sensor.  This option is mutually exclusive with manual "
                                                                    "variable specification."),
                                                                 tr("All instrument suffixes")));
    options.exclude("flags", "suffix");
    options.exclude("output-azimuth", "suffix");
    options.exclude("output-elevation", "suffix");
    options.exclude("output-magnitude", "suffix");
    options.exclude("suffix", "flags");
    options.exclude("suffix", "output-azimuth");
    options.exclude("suffix", "output-elevation");
    options.exclude("suffix", "output-magnitude");

    return options;
}

QList<ComponentExample> CalcSolarPositionComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will generate solar noon and dark flagging for all available input flags.")));

    options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")))->add("P01");
    (qobject_cast<ComponentOptionBoolean *>(options.get("position")))->set(true);
    examples.append(ComponentExample(options, tr("Single instrument with position"),
                                     tr("This will generate flagging for only the instrument P01 and add "
                                        "solar position variables to the output.")));

    options = getOptions();
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-azimuth")))->set("", "",
                                                                                         "ZAz_X1");
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-elevation")))->set("", "",
                                                                                           "ZEl_X1");
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-magnitude")))->set("", "",
                                                                                           "ZMa_X1");
    (qobject_cast<DynamicInputOption *>(options.get("latitude")))->set(40.0);
    (qobject_cast<DynamicInputOption *>(options.get("longitude")))->set(-105.0);
    (qobject_cast<DynamicInputOption *>(options.get("magnitude")))->set(
            SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("T_X1")), Calibration());
    examples.append(ComponentExample(options, tr("Single position generation"),
                                     tr("This will the solar position for T_X1 and generate the vector "
                                        "components using it, assuming explicitly specified "
                                        "location coordinates.")));

    return examples;
}

SegmentProcessingStage *CalcSolarPositionComponent::createBasicFilterDynamic(const ComponentOptions &options)
{ return new CalcSolarPosition(options); }

SegmentProcessingStage *CalcSolarPositionComponent::createBasicFilterPredefined(const ComponentOptions &options,
                                                                                double start,
                                                                                double end,
                                                                                const QList<
                                                                                        SequenceName> &inputs)
{ return new CalcSolarPosition(options, start, end, inputs); }

SegmentProcessingStage *CalcSolarPositionComponent::createBasicFilterEditing(double start,
                                                                             double end,
                                                                             const SequenceName::Component &station,
                                                                             const SequenceName::Component &archive,
                                                                             const ValueSegment::Transfer &config)
{ return new CalcSolarPosition(start, end, station, archive, config); }

SegmentProcessingStage *CalcSolarPositionComponent::deserializeBasicFilter(QDataStream &stream)
{ return new CalcSolarPosition(stream); }
