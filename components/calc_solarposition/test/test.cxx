/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"
#include "algorithms/dewpoint.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Algorithms;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    class ID {
    public:
        ID() : filter(), input()
        { }

        ID(const QSet<SequenceName> &setFilter, const QSet<SequenceName> &setInput) : filter(
                setFilter), input(setInput)
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }

        QSet<SequenceName> filter;
        QSet<SequenceName> input;
    };

    QHash<int, ID> contents;

    virtual void filterUnit(const SequenceName &unit, int targetID)
    {
        contents[targetID].input.remove(unit);
        contents[targetID].filter.insert(unit);
    }

    virtual void inputUnit(const SequenceName &unit, int targetID)
    {
        if (contents[targetID].filter.contains(unit))
            return;
        contents[targetID].input.insert(unit);
    }

    virtual bool isHandled(const SequenceName &unit)
    {
        for (QHash<int, ID>::const_iterator it = contents.constBegin();
                it != contents.constEnd();
                ++it) {
            if (it.value().filter.contains(unit))
                return true;
            if (it.value().input.contains(unit))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("calc_solarposition"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("magnitude")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("latitude")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("longitude")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("dark")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("position")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("flags")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-azimuth")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-elevation")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-magnitude")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")));

        QVERIFY(options.excluded().value("flags").contains("suffix"));
        QVERIFY(options.excluded().value("output-azimuth").contains("suffix"));
        QVERIFY(options.excluded().value("output-elevation").contains("suffix"));
        QVERIFY(options.excluded().value("output-magnitude").contains("suffix"));
        QVERIFY(options.excluded().value("suffix").contains("flags"));
        QVERIFY(options.excluded().value("suffix").contains("output-azimuth"));
        QVERIFY(options.excluded().value("suffix").contains("output-elevation"));
        QVERIFY(options.excluded().value("suffix").contains("output-magnitude"));
    }

    void dynamicDefaults()
    {
        ComponentOptions options(component->getOptions());
        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        SegmentProcessingStage *filter2;
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "T_A11"), &controller);
        QVERIFY(controller.contents.isEmpty());
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2 != NULL);
            filter2->unhandled(SequenceName("brw", "raw", "T_A11"), &controller);
            QVERIFY(controller.contents.isEmpty());
            delete filter2;
        }

        SequenceName F1_S11_1("brw", "raw", "F1_S11");
        SequenceName confsite_1("brw", "configuration", "site");
        SequenceName F1_S11_2("sgp", "raw", "F1_S11");
        SequenceName confsite_2("sgp", "configuration", "site");
        SequenceName F1_S12_1("brw", "raw", "F1_S12");

        filter->unhandled(F1_S11_1, &controller);
        filter->unhandled(confsite_1, &controller);
        QCOMPARE(controller.contents.size(), 2);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << F1_S11_1,
                                                 QSet<SequenceName>() << confsite_1));
        QCOMPARE(idL.size(), 1);
        int S11_1 = idL.at(0);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>(),
                                                 QSet<SequenceName>() << confsite_1));
        QCOMPARE(idL.size(), 1);
        int site_1 = idL.at(0);

        filter->unhandled(SequenceName("brw", "raw", "X_P01"), &controller);
        QCOMPARE(controller.contents.size(), 2);

        filter->unhandled(confsite_2, &controller);
        QCOMPARE(controller.contents.size(), 3);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>(),
                                                 QSet<SequenceName>() << confsite_2));
        QCOMPARE(idL.size(), 1);
        int site_2 = idL.at(0);

        filter->unhandled(F1_S11_2, &controller);
        QCOMPARE(controller.contents.size(), 4);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << F1_S11_2,
                                                 QSet<SequenceName>() << confsite_2));
        QCOMPARE(idL.size(), 1);
        int S11_2 = idL.at(0);

        filter->unhandled(F1_S12_1, &controller);
        QCOMPARE(controller.contents.size(), 5);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << F1_S12_1,
                                                 QSet<SequenceName>() << confsite_1));
        QCOMPARE(idL.size(), 1);
        int S12_1 = idL.at(0);

        data.setStart(1505487600.0);
        data.setEnd(1505487601.0);

        Variant::Root pos;
        pos["Location/Latitude"] = 40.0;
        pos["Location/Longitude"] = -105.0;
        data.setValue(confsite_1, pos);

        data.setValue(F1_S11_1, Variant::Root(Variant::Flags()));
        filter->process(S11_1, data);
        QVERIFY(!data.value(F1_S11_1).testFlag("SolarDark"));
        QVERIFY(!data.value(F1_S11_1).testFlag("SolarNoon"));

        {
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
            }
            QVERIFY(filter2 != NULL);
            data.setValue(F1_S11_1, Variant::Root(Variant::Flags()));
            filter->process(S11_1, data);
            QVERIFY(!data.value(F1_S11_1).testFlag("SolarDark"));
            QVERIFY(!data.value(F1_S11_1).testFlag("SolarNoon"));
            delete filter2;
        }

        pos["Location/Longitude"] = 105.0;
        data.setValue(confsite_2, pos);
        filter->process(site_2, data);
        filter->process(site_1, data);

        data.setValue(F1_S11_2, Variant::Root(Variant::Flags()));
        filter->process(S11_2, data);
        QVERIFY(data.value(F1_S11_2).testFlag("SolarDark"));
        QVERIFY(!data.value(F1_S11_2).testFlag("SolarNoon"));

        data.setStart(1505505600.0);
        data.setEnd(1505505601.0);

        data.setValue(F1_S11_1, Variant::Root(Variant::Flags()));
        filter->process(S11_1, data);
        QVERIFY(!data.value(F1_S11_1).testFlag("SolarDark"));
        QVERIFY(data.value(F1_S11_1).testFlag("SolarNoon"));

        data.setStart(1505505601.0);
        data.setEnd(1505505602.0);

        data.setValue(F1_S11_1, Variant::Root(Variant::Flags()));
        filter->process(S11_1, data);
        QVERIFY(!data.value(F1_S11_1).testFlag("SolarDark"));
        QVERIFY(!data.value(F1_S11_1).testFlag("SolarNoon"));

        data.setValue(F1_S12_1, Variant::Root(Variant::Flags()));
        filter->process(S12_1, data);
        QVERIFY(!data.value(F1_S12_1).testFlag("SolarDark"));
        QVERIFY(!data.value(F1_S12_1).testFlag("SolarNoon"));

        Variant::Root meta;
        meta.write().setType(Variant::Type::MetadataFlags);
        data.setValue(F1_S11_1.toMeta(), meta);

        filter->processMeta(S11_1, data);
        QVERIFY(data.value(F1_S11_1.toMeta()).metadataSingleFlag("SolarDark").exists());
        QVERIFY(data.value(F1_S11_1.toMeta()).metadataSingleFlag("SolarNoon").exists());
        QCOMPARE(data.value(F1_S11_1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);

        delete filter;
    }

    void dynamicOptions()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<DynamicInputOption *>(options.get("latitude"))->set(40.0);
        qobject_cast<DynamicInputOption *>(options.get("longitude"))->set(-105.0);
        qobject_cast<DynamicInputOption *>(options.get("magnitude"))->set(1.5);
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("flags"))->set("brw", "raw",
                                                                                  "F2_S11",
                                                                                  SequenceName::Flavors());
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-azimuth"))->set("brw",
                                                                                           "raw",
                                                                                           "Az_X1",
                                                                                           SequenceName::Flavors());
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-elevation"))->set("brw",
                                                                                             "raw",
                                                                                             "El_X1",
                                                                                             SequenceName::Flavors());
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-magnitude"))->set("brw",
                                                                                             "raw",
                                                                                             "Ma_X1",
                                                                                             SequenceName::Flavors());

        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "F1_S11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "F1_S12"), &controller);
        filter->unhandled(SequenceName("brw", "configuration", "site"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName F2_S11("brw", "raw", "F2_S11");
        SequenceName Az_X1("brw", "raw", "Az_X1");
        SequenceName El_X1("brw", "raw", "El_X1");
        SequenceName Ma_X1("brw", "raw", "Ma_X1");
        filter->unhandled(Az_X1, &controller);
        filter->unhandled(El_X1, &controller);
        filter->unhandled(F2_S11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << F2_S11 << Az_X1 << El_X1 << Ma_X1,
                                QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int id = idL.at(0);

        data.setStart(1505451600.0);
        data.setEnd(1505451601.0);

        data.setValue(F2_S11, Variant::Root(Variant::Flags()));
        filter->process(id, data);
        QVERIFY(data.value(F2_S11).testFlag("SolarDark"));
        QVERIFY(!data.value(F2_S11).testFlag("SolarNoon"));
        QCOMPARE(data.value(Az_X1).toDouble(), 321.353379205580552);
        QCOMPARE(data.value(El_X1).toDouble(), -39.574674984037699);
        QCOMPARE(data.value(Ma_X1).toDouble(), 1.5);

        delete filter;


        options = component->getOptions();
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->add("V11");
        qobject_cast<ComponentOptionBoolean *>(options.get("position"))->set(true);

        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        SequenceName site("brw", "configuration", "site");
        SequenceName F1_V11("brw", "raw", "F1_V11");
        SequenceName ZSolAz_V11("brw", "raw", "ZSolAz_V11");
        SequenceName ZSolEl_V11("brw", "raw", "ZSolEl_V11");
        SequenceName ZSolMa_V11("brw", "raw", "ZSolMa_V11");

        filter->unhandled(SequenceName("brw", "raw", "F1_P01"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "ZSolAz_S12"), &controller);
        QVERIFY(controller.contents.isEmpty());

        filter->unhandled(F1_V11, &controller);
        filter->unhandled(site, &controller);

        QCOMPARE(controller.contents.size(), 2);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>(),
                                                 QSet<SequenceName>() << site));
        QCOMPARE(idL.size(), 1);

        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << F1_V11 << ZSolAz_V11 << ZSolEl_V11
                                                     << ZSolMa_V11, QSet<SequenceName>() << site));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        Variant::Root pos;
        pos["Location/Latitude"] = 40.0;
        pos["Location/Longitude"] = -105.0;
        data.setValue(site, pos);

        data.setValue(F1_V11, Variant::Root(Variant::Flags()));
        filter->process(id, data);
        QVERIFY(data.value(F2_S11).testFlag("SolarDark"));
        QVERIFY(!data.value(F2_S11).testFlag("SolarNoon"));
        QCOMPARE(data.value(ZSolAz_V11).toDouble(), 321.353379205580552);
        QCOMPARE(data.value(ZSolEl_V11).toDouble(), -39.574674984037699);
        QCOMPARE(data.value(ZSolMa_V11).toDouble(), 1.0);

        delete filter;
    }

    void predefined()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionBoolean *>(options.get("position"))->set(true);

        SequenceName F1_A11("brw", "raw", "F1_A11");
        SequenceName ZSolAz_A11("brw", "raw", "ZSolAz_A11");
        SequenceName ZSolEl_A11("brw", "raw", "ZSolEl_A11");
        SequenceName ZSolMa_A11("brw", "raw", "ZSolMa_A11");
        SequenceName site("brw", "configuration", "site");
        QList<SequenceName> input;
        input << F1_A11;

        SegmentProcessingStage *filter =
                component->createBasicFilterPredefined(options, FP::undefined(), FP::undefined(),
                                                       input);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{site}));
        QCOMPARE(filter->predictedOutputs(),
                 (SequenceName::Set{ZSolAz_A11, ZSolEl_A11, ZSolMa_A11}));

        delete filter;
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["S11/Flags"] = "::F1_S11:=";
        cv["S11/Latitude"] = 40.0;
        cv["S11/Longitude"] = -106.0;
        cv["S11/DarkAngle"] = 18.0;
        config.emplace_back(FP::undefined(), 1505451599.0, cv);
        cv.write().setEmpty();
        cv["S11/Azimuth"] = "::Az_S11:=";
        cv["S11/Elevation"] = "::El_S11:=";
        cv["S11/Magnitude"] = "::Ma_S11:=";
        cv["S11/InputMagnitude"] = "::X_S11:=";
        cv["S11/Latitude"] = "::lat:=";
        cv["S11/Longitude"] = "::lon:=";
        cv["S11/DarkAngle"] = 18.0;
        config.emplace_back(1505451599.0, FP::undefined(), cv);

        SegmentProcessingStage *filter =
                component->createBasicFilterEditing(1505451400.0, 1505451650.0, "brw", "raw",
                                                    config);
        QVERIFY(filter != NULL);
        TestController controller;

        SequenceName F1_S11("brw", "raw", "F1_S11");
        SequenceName X_S11("brw", "raw", "X_S11");
        SequenceName Az_S11("brw", "raw", "Az_S11");
        SequenceName El_S11("brw", "raw", "El_S11");
        SequenceName Ma_S11("brw", "raw", "Ma_S11");
        SequenceName lat("brw", "raw", "lat");
        SequenceName lon("brw", "raw", "lon");

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{lat, lon, X_S11}));
        QCOMPARE(filter->predictedOutputs(), (SequenceName::Set{Az_S11, El_S11, Ma_S11}));

        filter->unhandled(F1_S11, &controller);
        filter->unhandled(X_S11, &controller);
        filter->unhandled(lat, &controller);
        filter->unhandled(lon, &controller);
        filter->unhandled(SequenceName("brw", "configuration", "site"), &controller);

        QList<int> idL = controller.contents
                                   .keys(TestController::ID(
                                           QSet<SequenceName>() << F1_S11 << Az_S11 << El_S11
                                                                << Ma_S11,
                                           QSet<SequenceName>() << X_S11 << lat << lon));
        QCOMPARE(idL.size(), 1);
        int S11 = idL.at(0);

        QCOMPARE(filter->metadataBreaks(S11), QSet<double>() << 1505451599.0);

        SequenceSegment data1;
        data1.setStart(1505451598.0);
        data1.setEnd(1505451599.0);

        data1.setValue(F1_S11, Variant::Root(Variant::Flags()));
        filter->process(S11, data1);
        QVERIFY(data1.value(F1_S11).testFlag("SolarDark"));
        QVERIFY(!data1.value(F1_S11).testFlag("SolarNoon"));

        Variant::Root meta;
        meta.write().setType(Variant::Type::MetadataFlags);
        data1.setValue(F1_S11.toMeta(), meta);

        filter->processMeta(S11, data1);
        QVERIFY(data1.value(F1_S11.toMeta()).metadataSingleFlag("SolarDark").exists());
        QVERIFY(data1.value(F1_S11.toMeta()).metadataSingleFlag("SolarNoon").exists());
        QCOMPARE(data1.value(F1_S11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QVERIFY(!data1.value(Az_S11.toMeta()).exists());
        QVERIFY(!data1.value(El_S11.toMeta()).exists());
        QVERIFY(!data1.value(Ma_S11.toMeta()).exists());


        SequenceSegment data2;
        data2.setStart(1505451600.0);
        data2.setEnd(1505451601.0);

        data2.setValue(lat, Variant::Root(40.0));
        data2.setValue(lon, Variant::Root(-105.0));
        data2.setValue(X_S11, Variant::Root(1.25));
        filter->process(S11, data2);
        QVERIFY(!data2.exists(F1_S11));
        QCOMPARE(data2.value(Az_S11).toDouble(), 321.353379205580552);
        QCOMPARE(data2.value(El_S11).toDouble(), -39.574674984037699);
        QCOMPARE(data2.value(Ma_S11).toDouble(), 1.25);

        filter->processMeta(S11, data2);
        QCOMPARE(data2.value(Az_S11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data2.value(El_S11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data2.value(Ma_S11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QVERIFY(!data2.value(F1_S11.toMeta()).exists());

        delete filter;
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
