/*
 * Copyright (c) 2020 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QRegularExpression>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "core/environment.hxx"
#include "core/qtcompat.hxx"

#include "calc_purpleair.hxx"

using namespace CPD3;
using namespace CPD3::Data;


void CalcPurpleAir::handleOptions(const ComponentOptions &options)
{
    if (options.isSet("weight-a")) {
        defaultWeightA.reset(
                qobject_cast<DynamicInputOption *>(options.get("weight-a"))->getInput());
    }
    if (options.isSet("weight-b")) {
        defaultWeightB.reset(
                qobject_cast<DynamicInputOption *>(options.get("weight-b"))->getInput());
    }
    if (options.isSet("efficiency")) {
        defaultScatteringEfficiency.reset(
                qobject_cast<DynamicCalibrationOption *>(options.get("efficiency"))->getInput());
    }
    if (options.isSet("wavelength")) {
        defaultScatteringWavelength.reset(
                qobject_cast<DynamicDoubleOption *>(options.get("wavelength"))->getInput());
    }

    restrictedInputs = false;

    if (options.isSet("input-a") ||
            options.isSet("input-b") ||
            options.isSet("output-intensity") ||
            options.isSet("output-scattering")) {
        restrictedInputs = true;

        Processing proc;

        SequenceName::Component suffix;
        SequenceName::Component station;
        SequenceName::Component archive;
        SequenceName::Flavors flavors;
        if (options.isSet("output-intensity")) {
            proc.operateIntensity
                .reset(qobject_cast<DynamicSequenceSelectionOption *>(
                        options.get("output-intensity"))->getOperator());
            for (const auto &name : proc.operateIntensity->getAllUnits()) {
                suffix = Util::suffix(name.getVariable(), '_');
                station = name.getStation();
                archive = name.getArchive();
                flavors = name.getFlavors();
                if (!suffix.empty())
                    break;
            }
        }
        if (options.isSet("output-scattering")) {
            proc.operateScattering
                .reset(qobject_cast<DynamicSequenceSelectionOption *>(
                        options.get("output-scattering"))->getOperator());
            for (const auto &name : proc.operateScattering->getAllUnits()) {
                suffix = Util::suffix(name.getVariable(), '_');
                station = name.getStation();
                archive = name.getArchive();
                flavors = name.getFlavors();
                if (!suffix.empty())
                    break;
            }
        }

        proc.inputs.resize(2);
        if (options.isSet("input-a")) {
            proc.inputs[0].intensity
                          .reset(qobject_cast<DynamicInputOption *>(
                                  options.get("input-a"))->getInput());
            if (suffix.empty()) {
                for (const auto &name : proc.inputs[0].intensity->getUsedInputs()) {
                    suffix = Util::suffix(name.getVariable(), '_');
                    station = name.getStation();
                    archive = name.getArchive();
                    flavors = name.getFlavors();
                    if (!suffix.empty())
                        break;
                }
            }
        }
        if (options.isSet("input-b")) {
            proc.inputs[1].intensity
                          .reset(qobject_cast<DynamicInputOption *>(
                                  options.get("input-b"))->getInput());
            if (suffix.empty()) {
                for (const auto &name : proc.inputs[1].intensity->getUsedInputs()) {
                    suffix = Util::suffix(name.getVariable(), '_');
                    station = name.getStation();
                    archive = name.getArchive();
                    flavors = name.getFlavors();
                    if (!suffix.empty())
                        break;
                }
            }
        }

        if (!proc.operateIntensity) {
            if (suffix.empty()) {
                proc.operateIntensity
                    .reset(new DynamicSequenceSelection::Match(QString::fromStdString(station),
                                                               QString::fromStdString(archive),
                                                               "Ip_.*", flavors));
            } else {
                proc.operateIntensity
                    .reset(new DynamicSequenceSelection::Basic(
                            SequenceName(station, archive, "Ip_" + suffix, flavors)));
            }
        }
        if (!proc.operateScattering) {
            if (suffix.empty()) {
                proc.operateScattering
                    .reset(new DynamicSequenceSelection::Match(QString::fromStdString(station),
                                                               QString::fromStdString(archive),
                                                               "Bs_.*", flavors));
            } else {
                proc.operateScattering
                    .reset(new DynamicSequenceSelection::Basic(
                            SequenceName(station, archive, "Bs_" + suffix, flavors)));
            }
        }

        if (!proc.inputs[0].intensity) {
            DynamicInput::Variable *sel = new DynamicInput::Variable;
            proc.inputs[0].intensity.reset(sel);
            if (suffix.empty()) {
                sel->set(SequenceMatch::OrderedLookup(
                        SequenceMatch::Element(station, archive, "Ipa_.*", flavors)),
                         Calibration());
            } else {
                sel->set(SequenceMatch::OrderedLookup(
                        SequenceMatch::Element(station, archive, "Ipa_" + suffix, flavors)),
                         Calibration());
            }
        }
        if (defaultWeightA) {
            proc.inputs[0].weight.reset(defaultWeightA->clone());
        } else {
            proc.inputs[0].weight.reset(new DynamicInput::Constant);
        }

        if (!proc.inputs[1].intensity) {
            DynamicInput::Variable *sel = new DynamicInput::Variable;
            proc.inputs[1].intensity.reset(sel);
            if (suffix.empty()) {
                sel->set(SequenceMatch::OrderedLookup(
                        SequenceMatch::Element(station, archive, "Ipb_.*", flavors)),
                         Calibration());
            } else {
                sel->set(SequenceMatch::OrderedLookup(
                        SequenceMatch::Element(station, archive, "Ipb_" + suffix, flavors)),
                         Calibration());
            }
        }
        if (defaultWeightB) {
            proc.inputs[1].weight.reset(defaultWeightB->clone());
        } else {
            proc.inputs[1].weight.reset(new DynamicInput::Constant);
        }


        if (defaultScatteringEfficiency) {
            proc.scatteringEfficiency.reset(defaultScatteringEfficiency->clone());
        } else {
            proc.scatteringEfficiency
                .reset(new DynamicCalibration::Constant(Calibration(QVector<double>{0, 0.015})));
        }

        if (defaultScatteringWavelength) {
            proc.scatteringWavelength.reset(defaultScatteringWavelength->clone());
        } else {
            proc.scatteringWavelength.reset(new DynamicDouble::Constant(550.0));
        }

        processing.emplace_back(std::move(proc));
    }

    if (options.isSet("suffix")) {
        filterSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->get());
    }
}

CalcPurpleAir::CalcPurpleAir(const ComponentOptions &options)
{ handleOptions(options); }

CalcPurpleAir::CalcPurpleAir(const ComponentOptions &options,
                             double,
                             double,
                             const QList<SequenceName> &inputs)
{
    handleOptions(options);

    for (const auto &name : inputs) {
        CalcPurpleAir::unhandled(name, nullptr);
    }
}

CalcPurpleAir::CalcPurpleAir(double start,
                             double end,
                             const SequenceName::Component &station,
                             const SequenceName::Component &archive,
                             const ValueSegment::Transfer &config)
{
    restrictedInputs = true;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    for (const auto &child : children) {
        Processing proc;

        proc.operateIntensity
            .reset(DynamicSequenceSelection::fromConfiguration(config,
                                                               QString("%1/CalculateIntensity").arg(
                                                                       QString::fromStdString(
                                                                               child)), start,
                                                               end));

        proc.operateScattering
            .reset(DynamicSequenceSelection::fromConfiguration(config,
                                                               QString("%1/CalculateScattering").arg(
                                                                       QString::fromStdString(
                                                                               child)), start,
                                                               end));

        proc.scatteringEfficiency
            .reset(DynamicCalibrationOption::fromConfiguration(config,
                                                               QString("%1/ScatteringEfficiency").arg(
                                                                       QString::fromStdString(
                                                                               child)), start,
                                                               end));
        proc.scatteringWavelength
            .reset(DynamicDoubleOption::fromConfiguration(config, QString("%1/Wavelength").arg(
                    QString::fromStdString(child)), start, end));


        proc.operateIntensity->registerExpected(station, archive);
        proc.operateScattering->registerExpected(station, archive);


        std::unordered_set<Variant::PathElement::HashIndex> operateChildren;
        for (const auto &add : config) {
            Util::merge(add[QString("%1/Input").arg(QString::fromStdString(child))].toHash().keys(),
                        operateChildren);
        }
        operateChildren.erase(Variant::PathElement::HashIndex());

        for (const auto &operate : operateChildren) {
            Processing::Input in;

            in.intensity
              .reset(DynamicInput::fromConfiguration(config, QString("%1/Input/%2/Intensity").arg(
                      QString::fromStdString(child), QString::fromStdString(operate)), start, end));

            in.weight
              .reset(DynamicInput::fromConfiguration(config, QString("%1/Input/%2/Weight").arg(
                      QString::fromStdString(child), QString::fromStdString(operate)), start, end));

            in.intensity->registerExpected(station, archive);
            in.weight->registerExpected(station, archive);

            proc.inputs.emplace_back(std::move(in));
        }

        processing.emplace_back(std::move(proc));
    }
}


CalcPurpleAir::~CalcPurpleAir() = default;

void CalcPurpleAir::handleNewProcessing(const SequenceName &name,
                                        std::size_t id,
                                        SegmentProcessingStage::SequenceHandlerControl *control)
{
    auto &proc = processing[id];

    SequenceName::Set reg;

    proc.operateIntensity
        ->registerExpected(name.getStation(), name.getArchive(), {}, name.getFlavors());
    Util::merge(proc.operateIntensity->getAllUnits(), reg);

    proc.operateScattering
        ->registerExpected(name.getStation(), name.getArchive(), {}, name.getFlavors());
    Util::merge(proc.operateScattering->getAllUnits(), reg);

    if (!control)
        return;

    for (const auto &n : reg) {
        bool isNew = !control->isHandled(n);
        control->filterUnit(n, id);
        if (isNew)
            registerPossibleInput(n, control, id);
    }
}

void CalcPurpleAir::registerPossibleInput(const SequenceName &name,
                                          SegmentProcessingStage::SequenceHandlerControl *control,
                                          std::size_t filterID)
{
    for (std::size_t id = 0, max = processing.size(); id < max; id++) {
        bool usedAsInput = false;
        auto &proc = processing[id];
        for (auto &in : proc.inputs) {
            if (in.intensity->registerInput(name)) {
                if (control)
                    control->inputUnit(name, id);
                usedAsInput = true;
            }
            if (in.weight->registerInput(name)) {
                if (control)
                    control->inputUnit(name, id);
                usedAsInput = true;
            }
        }

        if (usedAsInput && id != filterID)
            handleNewProcessing(name, id, control);
    }
    if (defaultWeightA && defaultWeightA->registerInput(name) && control)
        control->deferHandling(name);
    if (defaultWeightB && defaultWeightB->registerInput(name) && control)
        control->deferHandling(name);
}

CalcPurpleAir::Processing::Input::Input() : metadataWeight(1.0)
{ }

void CalcPurpleAir::unhandled(const SequenceName &name,
                              SegmentProcessingStage::SequenceHandlerControl *control)
{
    registerPossibleInput(name, control);

    for (std::size_t id = 0, max = processing.size(); id < max; id++) {
        if (processing[id].operateIntensity->registerInput(name)) {
            if (control)
                control->filterUnit(name, id);
            handleNewProcessing(name, id, control);
            return;
        }
        if (processing[id].operateScattering->registerInput(name)) {
            if (control)
                control->filterUnit(name, id);
            handleNewProcessing(name, id, control);
            return;
        }
    }

    if (restrictedInputs)
        return;

    if (name.hasFlavor("cover") || name.hasFlavor("stats"))
        return;

    auto reCheck = QRegularExpression("^Ip[ab]_(.+)$").match(name.getVariableQString());
    if (!reCheck.hasMatch())
        return;
    auto suffix = reCheck.captured(1).toStdString();
    if (suffix.empty())
        return;
    if (filterSuffixes.empty()) {
        if (suffix.front() != 'S')
            return;
    } else {
        if (!filterSuffixes.count(suffix))
            return;
    }

    SequenceName
            intensityA(name.getStation(), name.getArchive(), "Ipa_" + suffix, name.getFlavors());
    SequenceName
            intensityB(name.getStation(), name.getArchive(), "Ipb_" + suffix, name.getFlavors());
    SequenceName
            intensityName(name.getStation(), name.getArchive(), "Ip_" + suffix, name.getFlavors());
    SequenceName
            scatteringName(name.getStation(), name.getArchive(), "Bs_" + suffix, name.getFlavors());

    Processing proc;

    proc.operateIntensity.reset(new DynamicSequenceSelection::Single(intensityName));
    proc.operateIntensity->registerInput(name);
    proc.operateIntensity->registerInput(intensityA);
    proc.operateIntensity->registerInput(intensityB);
    proc.operateIntensity->registerInput(intensityName);
    proc.operateIntensity->registerInput(scatteringName);

    proc.operateScattering.reset(new DynamicSequenceSelection::Single(scatteringName));
    proc.operateScattering->registerInput(name);
    proc.operateScattering->registerInput(intensityA);
    proc.operateScattering->registerInput(intensityB);
    proc.operateScattering->registerInput(intensityName);
    proc.operateScattering->registerInput(scatteringName);

    proc.inputs.resize(2);

    proc.inputs[0].intensity.reset(new DynamicInput::Basic(intensityA));
    if (defaultWeightA) {
        proc.inputs[0].weight.reset(defaultWeightA->clone());
    } else {
        proc.inputs[0].weight.reset(new DynamicInput::Constant);
    }

    proc.inputs[1].intensity.reset(new DynamicInput::Basic(intensityB));
    if (defaultWeightB) {
        proc.inputs[1].weight.reset(defaultWeightB->clone());
    } else {
        proc.inputs[1].weight.reset(new DynamicInput::Constant);
    }

    for (auto &in : proc.inputs) {
        in.intensity->registerInput(name);
        in.intensity->registerInput(intensityA);
        in.intensity->registerInput(intensityB);
        in.intensity->registerInput(intensityName);
        in.intensity->registerInput(scatteringName);

        in.weight->registerInput(name);
        in.weight->registerInput(intensityA);
        in.weight->registerInput(intensityB);
        in.weight->registerInput(intensityName);
        in.weight->registerInput(scatteringName);
    }

    if (defaultScatteringEfficiency) {
        proc.scatteringEfficiency.reset(defaultScatteringEfficiency->clone());
    } else {
        proc.scatteringEfficiency
            .reset(new DynamicCalibration::Constant(Calibration(QVector<double>{0, 0.015})));
    }

    if (defaultScatteringWavelength) {
        proc.scatteringWavelength.reset(defaultScatteringWavelength->clone());
    } else {
        proc.scatteringWavelength.reset(new DynamicDouble::Constant(550.0));
    }

    auto id = processing.size();

    if (control) {
        for (const auto &in : proc.inputs) {
            for (const auto &n : in.intensity->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : in.weight->getUsedInputs()) {
                control->inputUnit(n, id);
            }
        }

        control->inputUnit(name, id);

        control->filterUnit(intensityName, id);
        control->filterUnit(scatteringName, id);
    }

    processing.emplace_back(std::move(proc));
}

void CalcPurpleAir::process(int id, SequenceSegment &data)
{
    auto &proc = processing[id];

    double totalIntensity = 0;
    double totalWeight = 0;
    for (const auto &in : proc.inputs) {
        double addIntensity = in.intensity->get(data);
        if (!FP::defined(addIntensity))
            continue;

        double addWeight = in.weight->get(data);
        if (!FP::defined(addWeight))
            addWeight = in.metadataWeight;
        if (!FP::defined(addWeight) || addWeight <= 0.0)
            continue;

        totalIntensity += addIntensity * addWeight;
        totalWeight += addWeight;
    }

    double intensity = FP::undefined();
    if (totalWeight > 0.0)
        intensity = totalIntensity / totalWeight;
    for (const auto &i : proc.operateIntensity->get(data)) {
        data[i].setReal(intensity);
    }

    double scattering = proc.scatteringEfficiency->get(data).apply(intensity);
    for (const auto &i : proc.operateScattering->get(data)) {
        data[i].setReal(scattering);
    }
}

void CalcPurpleAir::processMeta(int id, SequenceSegment &data)
{
    auto &proc = processing[id];

    Variant::Root meta;

    meta["By"].setString("calc_purpleair");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    Variant::Read base = Variant::Read::empty();
    for (auto &in : proc.inputs) {
        for (auto i : in.intensity->getUsedInputs()) {
            i.setMeta();
            if (!data.exists(i))
                continue;
            if (!base.exists())
                base = data[i];
            double weight = data[i].metadata("DetectorWeight").toReal();
            if (!FP::defined(weight))
                continue;
            in.metadataWeight = weight;
            break;
        }
    }

    for (auto i : proc.operateIntensity->get(data)) {
        i.setMeta();
        if (!data[i].exists())
            data[i].set(base);

        data[i].metadataReal("Description").setString("Combined detector intensity count");
        data[i].metadataReal("Format").setString("0000.00");
        data[i].metadataReal("Processing").toArray().after_back().set(meta);

        data[i].metadataReal("MVC").remove();
        data[i].metadataReal("GroupUnits").remove();
        data[i].metadataReal("Smoothing").remove();
    }

    Variant::Composite::fromCalibration(meta["Parameters"].hash("ScatteringEfficiency"),
                                        proc.scatteringEfficiency->get(data));

    for (auto i : proc.operateScattering->get(data)) {
        i.setMeta();
        if (!data[i].exists())
            data[i].set(base);

        data[i].metadataReal("Description").setString("Aerosol light scattering coefficient");
        data[i].metadataReal("Format").setString("0000.00");
        data[i].metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        data[i].metadataReal("Wavelength").setReal(proc.scatteringWavelength->get(data));
        data[i].metadataReal("Processing").toArray().after_back().set(meta);

        data[i].metadataReal("MVC").remove();
        data[i].metadataReal("GroupUnits").remove();
        data[i].metadataReal("Smoothing").remove();
    }
}


SequenceName::Set CalcPurpleAir::requestedInputs()
{
    SequenceName::Set out;
    for (const auto &p : processing) {
        for (const auto &in : p.inputs) {
            Util::merge(in.intensity->getUsedInputs(), out);
            Util::merge(in.weight->getUsedInputs(), out);
        }
    }
    return out;
}

SequenceName::Set CalcPurpleAir::predictedOutputs()
{
    SequenceName::Set out;
    for (const auto &p : processing) {
        Util::merge(p.operateIntensity->getAllUnits(), out);
        Util::merge(p.operateScattering->getAllUnits(), out);
    }
    return out;
}

QSet<double> CalcPurpleAir::metadataBreaks(int id)
{
    const auto &p = processing[id];
    QSet<double> result;
    Util::merge(p.operateIntensity->getChangedPoints(), result);
    Util::merge(p.operateScattering->getChangedPoints(), result);
    Util::merge(p.scatteringWavelength->getChangedPoints(), result);
    Util::merge(p.scatteringEfficiency->getChangedPoints(), result);
    return result;
}

CalcPurpleAir::CalcPurpleAir(QDataStream &stream)
{
    stream >> defaultWeightA;
    stream >> defaultWeightB;
    stream >> defaultScatteringEfficiency;
    stream >> defaultScatteringWavelength;
    stream >> filterSuffixes;
    stream >> restrictedInputs;

    Deserialize::container(stream, processing, [&stream]() {
        Processing proc;

        stream >> proc.operateIntensity;
        stream >> proc.operateScattering;
        stream >> proc.scatteringEfficiency;
        stream >> proc.scatteringWavelength;

        Deserialize::container(stream, proc.inputs, [&stream]() {
            Processing::Input in;

            stream >> in.weight;
            stream >> in.intensity;
            stream >> in.metadataWeight;

            return std::move(in);
        });

        return std::move(proc);
    });
}

void CalcPurpleAir::serialize(QDataStream &stream)
{
    stream << defaultWeightA;
    stream << defaultWeightB;
    stream << defaultScatteringEfficiency;
    stream << defaultScatteringWavelength;
    stream << filterSuffixes;
    stream << restrictedInputs;

    Serialize::container(stream, processing, [&stream](const Processing &proc) {
        stream << proc.operateIntensity;
        stream << proc.operateScattering;
        stream << proc.scatteringEfficiency;
        stream << proc.scatteringWavelength;

        Serialize::container(stream, proc.inputs, [&stream](const Processing::Input &in) {
            stream << in.weight;
            stream << in.intensity;
            stream << in.metadataWeight;
        });
    });
}


QString CalcPurpleAirComponent::getBasicSerializationName() const
{ return QString::fromLatin1("calc_purpleair"); }

ComponentOptions CalcPurpleAirComponent::getOptions()
{
    ComponentOptions options;

    options.add("weight-a",
                new DynamicInputOption(tr("weight-a", "name"), tr("Detector A weighting"),
                                       tr("This is the relative weight applied to the intensity reading from detector A.  The combined intensity is generated by applying the relative weights to the input intensities and normalizing by the total weight."),
                                       tr("1.0 unless set in metadata")));

    options.add("weight-b",
                new DynamicInputOption(tr("weight-b", "name"), tr("Detector B weighting"),
                                       tr("This is the relative weight applied to the intensity reading from detector B.  The combined intensity is generated by applying the relative weights to the input intensities and normalizing by the total weight."),
                                       tr("1.0 unless set in metadata")));

    options.add("efficiency",
                new DynamicCalibrationOption(tr("efficiency", "name"), tr("Scattering efficiency"),
                                             tr("This is the calibration applied to the combined intensity to generate the output light scattering value."),
                                             tr("Intensity/55")));

    options.add("wavelength",
                new DynamicDoubleOption(tr("wavelength", "name"), tr("Scattering wavelength"),
                                        tr("This is the wavelength set in the metadata for the output light scattering."),
                                        tr("550 nm")));

    options.add("input-a",
                new DynamicInputOption(tr("input-a", "name"), tr("Detector A count intensity"),
                                       tr("This is the count reading from detector A."),
                                       tr("All available detectors")));

    options.add("input-b",
                new DynamicInputOption(tr("input-b", "name"), tr("Detector B count intensity"),
                                       tr("This is the count reading from detector B."),
                                       tr("All available detectors")));


    options.add("output-intensity",
                new DynamicSequenceSelectionOption(tr("output-intensity", "name"),
                                                   tr("Output intensity"),
                                                   tr("This is the output for the combined intensity created from the weighted average of the input detector readings."),
                                                   tr("Calculated from input intensities")));

    options.add("output-scattering",
                new DynamicSequenceSelectionOption(tr("output-scattering", "name"),
                                                   tr("Output scattering"),
                                                   tr("This is the output for the combined scattering created by applying the scattering efficiency to the combined detector reading."),
                                                   tr("Calculated from input intensities")));


    options.add("suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments", "name"),
                                                                 tr("Instrument suffixes to correct"),
                                                                 tr("These are the instrument suffixes to calculate combined outputs for.  "
                                                                    "This option is mutually exclusive with manual "
                                                                    "variable specification."),
                                                                 tr("All scattering instrument suffixes")));
    options.exclude("input-a", "suffix");
    options.exclude("input-b", "suffix");
    options.exclude("output-intensity", "suffix");
    options.exclude("output-scattering", "suffix");
    options.exclude("suffix", "input-a");
    options.exclude("suffix", "input-b");
    options.exclude("suffix", "output-intensity");
    options.exclude("suffix", "output-scattering");

    return options;
}

QList<ComponentExample> CalcPurpleAirComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will calculate combined intensities and scatterings for all PurpleAir intensity inputs.  "
                                        "For example, it will calculate Ip_S81 and Bs_S81 from Ipa_S81 and Ipb_S81.  "
                                        "The weights used to combine the intensities are read from the metadata or assumed to be 1.0 if not included in the metadata.")));
    examples.back().setInputTypes(0);
    examples.back().setInputAuxiliary({"Ipa_S81", "Ipa_S81"});

    options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")))->add("S91");
    (qobject_cast<DynamicInputOption *>(options.get("weight-a")))->set(0.0);
    examples.append(ComponentExample(options, tr("Single instrument ignoring detector A"),
                                     tr("This will calculate the outputs, using only detector B for the measured intensities.")));
    examples.back().setInputTypes(0);
    examples.back().setInputAuxiliary({"Ipa_S91", "Ipa_S91"});

    options = getOptions();
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-scattering")))->set("", "",
                                                                                            "BsG_S81");
    (qobject_cast<DynamicInputOption *>(options.get("input-a")))->set(
            SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("Ipa_S82")),
            Calibration());
    (qobject_cast<DynamicInputOption *>(options.get("input-b")))->set(
            SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("Ipa_S81")),
            Calibration());
    examples.append(ComponentExample(options, tr("Single variable"),
                                     tr("This will calculate BsG_S81 and Ip_S81 from Ipa_S82 and Ipa_S81 using the default weights.")));
    examples.back().setInputTypes(0);
    examples.back().setInputAuxiliary({"Ipa_S81", "Ipa_S82"});

    return examples;
}

SegmentProcessingStage *CalcPurpleAirComponent::createBasicFilterDynamic(const ComponentOptions &options)
{ return new CalcPurpleAir(options); }

SegmentProcessingStage *CalcPurpleAirComponent::createBasicFilterPredefined(const ComponentOptions &options,
                                                                            double start,
                                                                            double end,
                                                                            const QList<
                                                                                    SequenceName> &inputs)
{ return new CalcPurpleAir(options, start, end, inputs); }

SegmentProcessingStage *CalcPurpleAirComponent::createBasicFilterEditing(double start,
                                                                         double end,
                                                                         const SequenceName::Component &station,
                                                                         const SequenceName::Component &archive,
                                                                         const ValueSegment::Transfer &config)
{ return new CalcPurpleAir(start, end, station, archive, config); }

SegmentProcessingStage *CalcPurpleAirComponent::deserializeBasicFilter(QDataStream &stream)
{ return new CalcPurpleAir(stream); }
