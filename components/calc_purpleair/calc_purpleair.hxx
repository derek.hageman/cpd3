/*
 * Copyright (c) 2020 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CALCPURPLEAIR_HXX
#define CALCPURPLEAIR_HXX

#include "core/first.hxx"

#include <vector>
#include <memory>
#include <unordered_set>
#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

class CalcPurpleAir : public CPD3::Data::SegmentProcessingStage {
    struct Processing {
        std::unique_ptr<CPD3::Data::DynamicSequenceSelection> operateIntensity;
        std::unique_ptr<CPD3::Data::DynamicSequenceSelection> operateScattering;

        std::unique_ptr<CPD3::Data::DynamicCalibration> scatteringEfficiency;
        std::unique_ptr<CPD3::Data::DynamicDouble> scatteringWavelength;

        struct Input {
            std::unique_ptr<CPD3::Data::DynamicInput> intensity;
            std::unique_ptr<CPD3::Data::DynamicInput> weight;
            double metadataWeight;

            Input();
        };

        std::vector<Input> inputs;
    };
    std::vector<Processing> processing;

    std::unique_ptr<CPD3::Data::DynamicInput> defaultWeightA;
    std::unique_ptr<CPD3::Data::DynamicInput> defaultWeightB;
    std::unique_ptr<CPD3::Data::DynamicCalibration> defaultScatteringEfficiency;
    std::unique_ptr<CPD3::Data::DynamicDouble> defaultScatteringWavelength;
    CPD3::Data::SequenceName::ComponentSet filterSuffixes;

    bool restrictedInputs;

    void handleOptions(const CPD3::ComponentOptions &options);

    void handleNewProcessing(const CPD3::Data::SequenceName &name,
                             std::size_t id,
                             CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control);

    void registerPossibleInput(const CPD3::Data::SequenceName &name,
                               CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                               std::size_t filterID = static_cast<std::size_t>(-1));

public:
    CalcPurpleAir() = delete;

    CalcPurpleAir(const CPD3::ComponentOptions &options);

    CalcPurpleAir(const CPD3::ComponentOptions &options,
                  double start,
                  double end,
                  const QList<CPD3::Data::SequenceName> &inputs);

    CalcPurpleAir(double start,
                  double end,
                  const CPD3::Data::SequenceName::Component &station,
                  const CPD3::Data::SequenceName::Component &archive,
                  const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CalcPurpleAir();

    void unhandled(const CPD3::Data::SequenceName &name,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;

    QSet<double> metadataBreaks(int id) override;

    CalcPurpleAir(QDataStream &stream);

    void serialize(QDataStream &stream) override;
};

class CalcPurpleAirComponent
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT Q_INTERFACES(CPD3::Data::ProcessingStageComponent CPD3
                              ::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.calc_purpleair"
                              FILE
                              "calc_purpleair.json")
public:
    QString getBasicSerializationName() const override;

    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    CPD3::Data::SegmentProcessingStage *createBasicFilterDynamic(const CPD3::ComponentOptions &options) override;

    CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined(const CPD3::ComponentOptions &options,
                                                                    double start,
                                                                    double end,
                                                                    const QList<
                                                                            CPD3::Data::SequenceName> &inputs) override;

    CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                 double end,
                                                                 const CPD3::Data::SequenceName::Component &station,
                                                                 const CPD3::Data::SequenceName::Component &archive,
                                                                 const CPD3::Data::ValueSegment::Transfer &config) override;

    CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream) override;
};

#endif
