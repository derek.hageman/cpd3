/*
 * Copyright (c) 2020 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    struct ID {
        SequenceName::Set filter;
        SequenceName::Set input;

        ID() = default;

        ID(SequenceName::Set filter, SequenceName::Set input) : filter(std::move(filter)),
                                                                input(std::move(input))
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }
    };

    std::unordered_map<int, ID> contents;

    int find(const SequenceName::Set &filter, const SequenceName::Set &input = {}) const
    {
        for (const auto &check : contents) {
            if (check.second.filter != filter)
                continue;
            if (check.second.input != input)
                continue;
            return check.first;
        }
        return -1;
    }

    void filterUnit(const SequenceName &name, int targetID) override
    {
        contents[targetID].input.erase(name);
        contents[targetID].filter.insert(name);
    }

    void inputUnit(const SequenceName &name, int targetID) override
    {
        if (contents[targetID].filter.count(name))
            return;
        contents[targetID].input.insert(name);
    }

    bool isHandled(const SequenceName &name) override
    {
        for (const auto &check : contents) {
            if (check.second.filter.count(name))
                return true;
            if (check.second.input.count(name))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("calc_purpleair"));
        QVERIFY(component);
    }

    void dynamicDefaults()
    {
        ComponentOptions options(component->getOptions());
        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterDynamic(options));
        QVERIFY(filter.get());
        TestController controller;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "T_S11"), &controller);
        QVERIFY(controller.contents.empty());
        {
            std::unique_ptr<SegmentProcessingStage> filter2;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2.reset(component->deserializeBasicFilter(stream));
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2.get());
            filter2->unhandled(SequenceName("brw", "raw", "T_S11"), &controller);
            QVERIFY(controller.contents.empty());
        }

        SequenceName Ipa_S11_1("brw", "raw", "Ipa_S11");
        SequenceName Ipb_S11_1("brw", "raw", "Ipb_S11");
        SequenceName Ip_S11_1("brw", "raw", "Ip_S11");
        SequenceName Bs_S11_1("brw", "raw", "Bs_S11");
        SequenceName Ipa_S11_2("sgp", "raw", "Ipa_S11");
        SequenceName Ipb_S11_2("sgp", "raw", "Ipb_S11");
        SequenceName Ip_S11_2("sgp", "raw", "Ip_S11");
        SequenceName Bs_S11_2("sgp", "raw", "Bs_S11");
        SequenceName Ipa_S81_1("brw", "raw", "Ipa_S81");
        SequenceName Ipb_S81_1("brw", "raw", "Ipb_S81");
        SequenceName Ip_S81_1("brw", "raw", "Ip_S81");
        SequenceName Bs_S81_1("brw", "raw", "Bs_S81");

        filter->unhandled(Ipa_S11_1, &controller);
        filter->unhandled(Bs_S11_1, &controller);
        QCOMPARE((int) controller.contents.size(), 1);
        int S11_1 = controller.find({Ip_S11_1, Bs_S11_1}, {Ipa_S11_1, Ipb_S11_1});
        QVERIFY(S11_1 != -1);

        filter->unhandled(SequenceName("brw", "raw", "Bs_P01"), &controller);
        QCOMPARE((int) controller.contents.size(), 1);

        filter->unhandled(Ipb_S11_2, &controller);
        QCOMPARE((int) controller.contents.size(), 2);
        int S11_2 = controller.find({Ip_S11_2, Bs_S11_2}, {Ipa_S11_2, Ipb_S11_2});
        QVERIFY(S11_2 != -1);

        filter->unhandled(Bs_S81_1, &controller);
        filter->unhandled(Ipa_S81_1, &controller);
        QCOMPARE((int) controller.contents.size(), 3);
        int S81_1 = controller.find({Ip_S81_1, Bs_S81_1}, {Ipa_S81_1, Ipb_S81_1});
        QVERIFY(S81_1 != -1);

        data.setStart(15.0);
        data.setEnd(16.0);

        data.setValue(Ipa_S11_1, Variant::Root(100.0));
        filter->process(S11_1, data);
        QCOMPARE(data.value(Ip_S11_1).toReal(), 100.0);
        QCOMPARE(data.value(Bs_S11_1).toReal(), 1.5);

        {
            std::unique_ptr<SegmentProcessingStage> filter2;
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2.reset(component->deserializeBasicFilter(stream));
            }
            QVERIFY(filter2.get());
            data.setValue(Ip_S11_1, Variant::Root());
            data.setValue(Bs_S11_1, Variant::Root());
            data.setValue(Ipa_S11_1, Variant::Root(100.0));
            filter->process(S11_1, data);
            QCOMPARE(data.value(Ip_S11_1).toReal(), 100.0);
            QCOMPARE(data.value(Bs_S11_1).toReal(), 1.5);
        }

        data.setValue(Ipa_S11_2, Variant::Root(110.0));
        data.setValue(Ipb_S11_2, Variant::Root(120.0));
        filter->process(S11_2, data);
        QCOMPARE(data.value(Ip_S11_2).toReal(), 115.0);
        QCOMPARE(data.value(Bs_S11_2).toReal(), 1.725);

        data.setValue(Ipb_S81_1, Variant::Root(230.0));
        filter->process(S81_1, data);
        QCOMPARE(data.value(Ip_S81_1).toReal(), 230.0);
        QCOMPARE(data.value(Bs_S81_1).toReal(), 3.45);

        data.setValue(Ipa_S11_1, Variant::Root(FP::undefined()));
        data.setValue(Ipb_S11_1, Variant::Root(FP::undefined()));
        filter->process(S11_1, data);
        QVERIFY(!FP::defined(data.value(Ip_S11_1).toReal()));
        QVERIFY(!FP::defined(data.value(Bs_S11_1).toReal()));

        filter->processMeta(S11_1, data);
        QCOMPARE(data.value(Ip_S11_1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data.value(Bs_S11_1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data.value(Bs_S11_1.toMeta()).metadata("Wavelength").toReal(), 550.0);
    }

    void dynamicOptions()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<DynamicDoubleOption *>(options.get("wavelength"))->set(700.0);
        qobject_cast<DynamicInputOption *>(options.get("input-a"))->set(50.0);
        qobject_cast<DynamicInputOption *>(options.get("weight-a"))->set(0.5);
        qobject_cast<DynamicInputOption *>(options.get("weight-b"))->set(2.0);
        qobject_cast<DynamicCalibrationOption *>(options.get("efficiency"))->set(
                Calibration(QVector<double>{1.0, 1 / 90.0}));
        qobject_cast<DynamicInputOption *>(options.get("input-b"))->set(
                SequenceMatch::OrderedLookup(
                        SequenceMatch::Element(QString("brw"), QString("raw"), "Ipx_S11",
                                               SequenceName::Flavors())), Calibration());
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-intensity"))->set("brw",
                                                                                             "raw",
                                                                                             "Ix_S11",
                                                                                             SequenceName::Flavors());
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-scattering"))->set("brw",
                                                                                              "raw",
                                                                                              "BsR_S11",
                                                                                              SequenceName::Flavors());

        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterDynamic(options));
        QVERIFY(filter.get());
        TestController controller;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "Ipx_S12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BsR_S12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "Ix_S12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "Ipa_S11"), &controller);
        QVERIFY(controller.contents.empty());

        SequenceName Ipx_S11("brw", "raw", "Ipx_S11");
        SequenceName Ix_S11("brw", "raw", "Ix_S11");
        SequenceName BsR_S11("brw", "raw", "BsR_S11");
        filter->unhandled(Ipx_S11, &controller);
        filter->unhandled(Ix_S11, &controller);

        QCOMPARE((int) controller.contents.size(), 1);
        int id = controller.find({Ix_S11, BsR_S11}, {Ipx_S11});
        QVERIFY(id != -1);

        data.setValue(Ipx_S11, Variant::Root(100.0));
        filter->process(id, data);
        QCOMPARE(data.value(Ix_S11).toReal(), 90.0);
        QCOMPARE(data.value(BsR_S11).toReal(), 2.0);

        filter.reset();


        options = component->getOptions();
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->add("N11");

        filter.reset(component->createBasicFilterDynamic(options));
        QVERIFY(filter != NULL);
        controller.contents.clear();

        SequenceName Ipa_N11("brw", "raw", "Ipa_N11");
        SequenceName Ipb_N11("brw", "raw", "Ipb_N11");
        SequenceName Ip_N11("brw", "raw", "Ip_N11");
        SequenceName Bs_N11("brw", "raw", "Bs_N11");

        filter->unhandled(SequenceName("brw", "raw", "VA_P01"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "X_P01"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "X_N12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "VA_N12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "T_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "P_V11"), &controller);
        QVERIFY(controller.contents.empty());

        filter->unhandled(Ipa_N11, &controller);

        QCOMPARE((int) controller.contents.size(), 1);
        id = controller.find({Ip_N11, Bs_N11}, {Ipa_N11, Ipb_N11});
        QVERIFY(id != -1);

        data[Ipa_N11.toMeta()].metadataReal("DetectorWeight").setReal(0.2);
        filter->processMeta(id, data);

        data.setValue(Ipa_N11, Variant::Root(50.0));
        data.setValue(Ipb_N11, Variant::Root(110.0));
        data.setValue(Ip_N11, Variant::Root());
        data.setValue(Bs_N11, Variant::Root());
        filter->process(id, data);
        QCOMPARE(data.value(Ip_N11).toReal(), 100.0);
        QCOMPARE(data.value(Bs_N11).toReal(), 1.5);

        filter.reset();
    }

    void predefined()
    {
        ComponentOptions options(component->getOptions());

        SequenceName Ipa_S11("brw", "raw", "Ipa_S11");
        SequenceName Ipb_S11("brw", "raw", "Ipb_S11");
        SequenceName Ip_S11("brw", "raw", "Ip_S11");
        SequenceName Bs_S11("brw", "raw", "Bs_S11");
        QList<SequenceName> input;
        input << Ipa_S11;

        std::unique_ptr<SegmentProcessingStage> filter
                (component->createBasicFilterPredefined(options, FP::undefined(), FP::undefined(),
                                                        input));
        QVERIFY(filter.get());
        TestController controller;
        QList<int> idL;

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{Ipa_S11, Ipb_S11}));
        QCOMPARE(filter->predictedOutputs(), (SequenceName::Set{Ip_S11, Bs_S11}));
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["S11/CalculateIntensity"] = "::Ip_XM1:=";
        cv["S11/CalculateScattering"] = "::Bs_XM1:=";
        cv["S11/ScatteringEfficiency/#0"] = 0.0;
        cv["S11/ScatteringEfficiency/#1"] = 0.5;
        cv["S11/Wavelength"] = 550.0;
        cv["S11/Input/A/Intensity"] = "::Ipa_XM1:=";
        cv["S11/Input/B/Intensity"] = "::Ipb_XM1:=";
        config.emplace_back(FP::undefined(), 13.0, cv);
        cv.write().setEmpty();
        cv["S11/CalculateIntensity"] = "::Ip_XM1:=";
        cv["S11/CalculateScattering"] = "::Bs_XM1:=";
        cv["S11/ScatteringEfficiency/#0"] = 0.0;
        cv["S11/ScatteringEfficiency/#1"] = 0.5;
        cv["S11/Wavelength"] = 600.0;
        cv["S11/Input/A/Intensity"] = "::Ipa_XM1:=";
        cv["S11/Input/B/Intensity"] = "::Qs_XM1:=";
        cv["S11/Input/B/Weight"] = 2.0;
        config.emplace_back(13.0, FP::undefined(), cv);

        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config));
        QVERIFY(filter.get());
        TestController controller;

        SequenceName Ip_XM1("brw", "raw", "Ip_XM1");
        SequenceName Bs_XM1("brw", "raw", "Bs_XM1");
        SequenceName Ipa_XM1("brw", "raw", "Ipa_XM1");
        SequenceName Ipb_XM1("brw", "raw", "Ipb_XM1");
        SequenceName Qs_XM1("brw", "raw", "Qs_XM1");

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{Ipa_XM1, Ipb_XM1, Qs_XM1}));
        QCOMPARE(filter->predictedOutputs(), (SequenceName::Set{Ip_XM1, Bs_XM1}));

        filter->unhandled(Qs_XM1, &controller);
        filter->unhandled(Bs_XM1, &controller);
        filter->unhandled(Ipb_XM1, &controller);
        filter->unhandled(Ipa_XM1, &controller);

        int id = controller.find({Ip_XM1, Bs_XM1}, {Ipa_XM1, Ipb_XM1, Qs_XM1});
        QVERIFY(id != -1);

        QCOMPARE(filter->metadataBreaks(id), QSet<double>() << 13.0);

        SequenceSegment data1;
        data1.setStart(12.0);
        data1.setEnd(13.0);

        data1.setValue(Ipa_XM1, Variant::Root(30.0));
        data1.setValue(Ipb_XM1, Variant::Root(50.0));
        data1.setValue(Qs_XM1, Variant::Root(2.0));
        filter->process(id, data1);
        QCOMPARE(data1.value(Ip_XM1).toReal(), 40.0);
        QCOMPARE(data1.value(Bs_XM1).toReal(), 20.0);

        data1[Ipa_XM1.toMeta()].metadataReal("DetectorWeight").setReal(3.0);
        filter->processMeta(id, data1);
        QCOMPARE(data1.value(Ip_XM1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);

        data1.setValue(Ipa_XM1, Variant::Root(30.0));
        data1.setValue(Ipb_XM1, Variant::Root(50.0));
        data1.setValue(Qs_XM1, Variant::Root(3.0));
        filter->process(id, data1);
        QCOMPARE(data1.value(Ip_XM1).toReal(), 35.0);
        QCOMPARE(data1.value(Bs_XM1).toReal(), 17.5);

        data1.setValue(Ipa_XM1, Variant::Root(30.0));
        data1.setValue(Ipb_XM1, Variant::Root(FP::undefined()));
        filter->process(id, data1);
        QCOMPARE(data1.value(Ip_XM1).toReal(), 30.0);
        QCOMPARE(data1.value(Bs_XM1).toReal(), 15.0);

        data1.setValue(Ipa_XM1, Variant::Root(FP::undefined()));
        data1.setValue(Ipb_XM1, Variant::Root(40.0));
        filter->process(id, data1);
        QCOMPARE(data1.value(Ip_XM1).toReal(), 40.0);
        QCOMPARE(data1.value(Bs_XM1).toReal(), 20.0);


        SequenceSegment data2;
        data2.setStart(13.0);
        data2.setEnd(15.0);

        data2.setValue(Ipa_XM1, Variant::Root(30.0));
        data2.setValue(Ipb_XM1, Variant::Root(1.0));
        data2.setValue(Qs_XM1, Variant::Root(50.0));
        filter->process(id, data2);
        QCOMPARE(data2.value(Ip_XM1).toReal(), 38.0);
        QCOMPARE(data2.value(Bs_XM1).toReal(), 19.0);

        data2.setValue(Ipa_XM1, Variant::Root(30.0));
        data2.setValue(Ipb_XM1, Variant::Root(FP::undefined()));
        data2.setValue(Qs_XM1, Variant::Root(50.0));
        filter->process(id, data2);
        QCOMPARE(data2.value(Ip_XM1).toReal(), 38.0);
        QCOMPARE(data2.value(Bs_XM1).toReal(), 19.0);

        data2.setValue(Ipa_XM1, Variant::Root(30.0));
        data2.setValue(Ipb_XM1, Variant::Root(FP::undefined()));
        data2.setValue(Qs_XM1, Variant::Root(FP::undefined()));
        filter->process(id, data2);
        QCOMPARE(data2.value(Ip_XM1).toReal(), 30.0);
        QCOMPARE(data2.value(Bs_XM1).toReal(), 15.0);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
