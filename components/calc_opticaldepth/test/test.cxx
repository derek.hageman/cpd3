/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "smoothing/baseline.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    class ID {
    public:
        ID() : filter(), input()
        { }

        ID(const QSet<SequenceName> &setFilter, const QSet<SequenceName> &setInput) : filter(
                setFilter), input(setInput)
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }

        QSet<SequenceName> filter;
        QSet<SequenceName> input;
    };

    QHash<int, ID> contents;

    virtual void filterUnit(const SequenceName &unit, int targetID)
    {
        contents[targetID].input.remove(unit);
        contents[targetID].filter.insert(unit);
    }

    virtual void inputUnit(const SequenceName &unit, int targetID)
    {
        if (contents[targetID].filter.contains(unit))
            return;
        contents[targetID].input.insert(unit);
    }

    virtual bool isHandled(const SequenceName &unit)
    {
        for (QHash<int, ID>::const_iterator it = contents.constBegin();
                it != contents.constEnd();
                ++it) {
            if (it.value().filter.contains(unit))
                return true;
            if (it.value().input.contains(unit))
                return true;
        }
        return false;
    }
};

static SequenceName::Flavors fm(const SequenceName::Flavors &a, const SequenceName::Flavors &b) {
    SequenceName::Flavors result = a;
    Util::merge(b, result);
    return result;
}

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("calc_opticaldepth"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("length-start")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("length-end")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("delta-l")));
        QVERIFY(qobject_cast<DynamicBoolOption *>(options.get("synchronous-zero")));
        QVERIFY(qobject_cast<TimeIntervalSelectionOption *>(options.get("reset-gap")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("calculate-l")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("scattering")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("backscattering")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("absorption")));;
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("extinction")));;
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(
                options.get("scattering-suffix")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(
                options.get("absorption-suffix")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(
                options.get("extinction-suffix")));

        QVERIFY(options.excluded().value("absorption").contains("absorption-suffix"));
        QVERIFY(options.excluded().value("absorption-suffix").contains("absorption"));
        QVERIFY(options.excluded().value("scattering").contains("scattering-suffix"));
        QVERIFY(options.excluded().value("scattering-suffix").contains("scattering"));
        QVERIFY(options.excluded().value("extinction").contains("extinction-suffix"));
        QVERIFY(options.excluded().value("extinction-suffix").contains("extinction"));
    }

    void dynamicDefaults()
    {
        ComponentOptions options(component->getOptions());
        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        SegmentProcessingStage *filter2;
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "T_A11q"), &controller);
        QVERIFY(controller.contents.isEmpty());
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2 != NULL);
            filter2->unhandled(SequenceName("brw", "raw", "T_A11q"), &controller);
            QVERIFY(controller.contents.isEmpty());
            delete filter2;
        }

        SequenceName::Flavors pm1{"pm1"};
        SequenceName::Flavors end{"end"};
        SequenceName::Flavors cover{"cover"};

        SequenceName BsB_S11_1("brw", "raw", "BsB_S11");
        SequenceName BsR_S11_1("brw", "raw", "BsR_S11");
        SequenceName BbsB_S11_1("brw", "raw", "BbsB_S11");
        SequenceName BbsR_S11_1("brw", "raw", "BbsR_S11");
        SequenceName BaB_A11_1("brw", "raw", "BaB_A11");
        SequenceName BaR_A11_1("brw", "raw", "BaR_A11");
        SequenceName BsB_S11_1c("brw", "raw", "BsB_S11", cover);
        SequenceName BsR_S11_1c("brw", "raw", "BsR_S11", cover);
        SequenceName BbsB_S11_1c("brw", "raw", "BbsB_S11", cover);
        SequenceName BbsR_S11_1c("brw", "raw", "BbsR_S11", cover);
        SequenceName BaB_A11_1c("brw", "raw", "BaB_A11", cover);
        SequenceName BaR_A11_1c("brw", "raw", "BaR_A11", cover);
        SequenceName IrB_A11_1("brw", "raw", "IrB_A11");
        SequenceName IrR_A11_1("brw", "raw", "IrR_A11");
        SequenceName L_A11_1("brw", "raw", "L_A11");
        SequenceName L_A11_1e("brw", "raw", "L_A11", end);
        SequenceName Ff_A11_1("brw", "raw", "Ff_A11");
        SequenceName Fn_A11_1("brw", "raw", "Fn_A11");
        SequenceName OsB_A11_1("brw", "raw", "OsB_A11");
        SequenceName OsR_A11_1("brw", "raw", "OsR_A11");
        SequenceName OaB_A11_1("brw", "raw", "OaB_A11");
        SequenceName OaR_A11_1("brw", "raw", "OaR_A11");
        SequenceName OeB_A11_1("brw", "raw", "OeB_A11");
        SequenceName OeR_A11_1("brw", "raw", "OeR_A11");
        SequenceName ZGB_A11_1("brw", "raw", "ZGB_A11");
        SequenceName ZGR_A11_1("brw", "raw", "ZGR_A11");
        SequenceName OsB_A11_1e("brw", "raw", "OsB_A11", end);
        SequenceName OsR_A11_1e("brw", "raw", "OsR_A11", end);
        SequenceName OaB_A11_1e("brw", "raw", "OaB_A11", end);
        SequenceName OaR_A11_1e("brw", "raw", "OaR_A11", end);
        SequenceName ZGB_A11_1e("brw", "raw", "ZGB_A11", end);
        SequenceName ZGR_A11_1e("brw", "raw", "ZGR_A11", end);
        SequenceName OeB_A11_1e("brw", "raw", "OeB_A11", end);
        SequenceName OeR_A11_1e("brw", "raw", "OeR_A11", end);

        SequenceName BsB_S11_2("sgp", "raw", "BsB_S11");
        SequenceName BsR_S11_2("sgp", "raw", "BsR_S11");
        SequenceName BbsB_S11_2("sgp", "raw", "BbsB_S11");
        SequenceName BbsR_S11_2("sgp", "raw", "BbsR_S11");
        SequenceName BaB_A11_2("sgp", "raw", "BaB_A11");
        SequenceName BaR_A11_2("sgp", "raw", "BaR_A11");
        SequenceName BsB_S11_2c("sgp", "raw", "BsB_S11", cover);
        SequenceName BsR_S11_2c("sgp", "raw", "BsR_S11", cover);
        SequenceName BbsB_S11_2c("sgp", "raw", "BbsB_S11", cover);
        SequenceName BbsR_S11_2c("sgp", "raw", "BbsR_S11", cover);
        SequenceName BaB_A11_2c("sgp", "raw", "BaB_A11", cover);
        SequenceName BaR_A11_2c("sgp", "raw", "BaR_A11", cover);
        SequenceName IrB_A11_2("sgp", "raw", "IrB_A11");
        SequenceName IrR_A11_2("sgp", "raw", "IrR_A11");
        SequenceName L_A11_2("sgp", "raw", "L_A11");
        SequenceName L_A11_2e("sgp", "raw", "L_A11", end);
        SequenceName Ff_A11_2("sgp", "raw", "Ff_A11");
        SequenceName Fn_A11_2("sgp", "raw", "Fn_A11");
        SequenceName OsB_A11_2("sgp", "raw", "OsB_A11");
        SequenceName OsR_A11_2("sgp", "raw", "OsR_A11");
        SequenceName OaB_A11_2("sgp", "raw", "OaB_A11");
        SequenceName OaR_A11_2("sgp", "raw", "OaR_A11");
        SequenceName OeB_A11_2("sgp", "raw", "OeB_A11");
        SequenceName OeR_A11_2("sgp", "raw", "OeR_A11");
        SequenceName ZGB_A11_2("sgp", "raw", "ZGB_A11");
        SequenceName ZGR_A11_2("sgp", "raw", "ZGR_A11");
        SequenceName OsB_A11_2e("sgp", "raw", "OsB_A11", end);
        SequenceName OsR_A11_2e("sgp", "raw", "OsR_A11", end);
        SequenceName OaB_A11_2e("sgp", "raw", "OaB_A11", end);
        SequenceName OaR_A11_2e("sgp", "raw", "OaR_A11", end);
        SequenceName ZGB_A11_2e("sgp", "raw", "ZGB_A11", end);
        SequenceName ZGR_A11_2e("sgp", "raw", "ZGR_A11", end);
        SequenceName OeB_A11_2e("sgp", "raw", "OeB_A11", end);
        SequenceName OeR_A11_2e("sgp", "raw", "OeR_A11", end);

        SequenceName BsB_S11_1p("brw", "raw", "BsB_S11", pm1);
        SequenceName BsR_S11_1p("brw", "raw", "BsR_S11", pm1);
        SequenceName BbsB_S11_1p("brw", "raw", "BbsB_S11", pm1);
        SequenceName BbsR_S11_1p("brw", "raw", "BbsR_S11", pm1);
        SequenceName BaB_A11_1p("brw", "raw", "BaB_A11", pm1);
        SequenceName BaR_A11_1p("brw", "raw", "BaR_A11", pm1);
        SequenceName BsB_S11_1pc("brw", "raw", "BsB_S11", fm(cover, pm1));
        SequenceName BsR_S11_1pc("brw", "raw", "BsR_S11", fm(cover, pm1));
        SequenceName BbsB_S11_1pc("brw", "raw", "BbsB_S11", fm(cover, pm1));
        SequenceName BbsR_S11_1pc("brw", "raw", "BbsR_S11", fm(cover, pm1));
        SequenceName BaB_A11_1pc("brw", "raw", "BaB_A11", fm(cover, pm1));
        SequenceName BaR_A11_1pc("brw", "raw", "BaR_A11", fm(cover, pm1));
        SequenceName IrB_A11_1p("brw", "raw", "IrB_A11", pm1);
        SequenceName IrR_A11_1p("brw", "raw", "IrR_A11", pm1);
        SequenceName L_A11_1p("brw", "raw", "L_A11", pm1);
        SequenceName L_A11_1pe("brw", "raw", "L_A11", fm(end, pm1));
        SequenceName OsB_A11_1p("brw", "raw", "OsB_A11", pm1);
        SequenceName OsR_A11_1p("brw", "raw", "OsR_A11", pm1);
        SequenceName OaB_A11_1p("brw", "raw", "OaB_A11", pm1);
        SequenceName OaR_A11_1p("brw", "raw", "OaR_A11", pm1);
        SequenceName OeB_A11_1p("brw", "raw", "OeB_A11", pm1);
        SequenceName OeR_A11_1p("brw", "raw", "OeR_A11", pm1);
        SequenceName ZGB_A11_1p("brw", "raw", "ZGB_A11", pm1);
        SequenceName ZGR_A11_1p("brw", "raw", "ZGR_A11", pm1);
        SequenceName OsB_A11_1pe("brw", "raw", "OsB_A11", fm(end, pm1));
        SequenceName OsR_A11_1pe("brw", "raw", "OsR_A11", fm(end, pm1));
        SequenceName OaB_A11_1pe("brw", "raw", "OaB_A11", fm(end, pm1));
        SequenceName OaR_A11_1pe("brw", "raw", "OaR_A11", fm(end, pm1));
        SequenceName ZGB_A11_1pe("brw", "raw", "ZGB_A11", fm(end, pm1));
        SequenceName ZGR_A11_1pe("brw", "raw", "ZGR_A11", fm(end, pm1));
        SequenceName OeB_A11_1pe("brw", "raw", "OeB_A11", fm(end, pm1));
        SequenceName OeR_A11_1pe("brw", "raw", "OeR_A11", fm(end, pm1));


        filter->unhandled(IrB_A11_1, &controller);
        filter->unhandled(BsB_S11_1, &controller);
        filter->unhandled(BsR_S11_1, &controller);
        filter->unhandled(BbsB_S11_1, &controller);
        filter->unhandled(BbsR_S11_1, &controller);
        filter->unhandled(BaB_A11_1, &controller);
        filter->unhandled(BaR_A11_1, &controller);
        filter->unhandled(IrR_A11_1, &controller);
        filter->unhandled(L_A11_1, &controller);
        filter->unhandled(Ff_A11_1, &controller);

        QVERIFY(controller.contents.size() >= 2);
        idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                               OsB_A11_1 <<
                                                                               OsR_A11_1 <<
                                                                               OaB_A11_1 <<
                                                                               OaR_A11_1 <<
                                                                               OeB_A11_1 <<
                                                                               OeR_A11_1 <<
                                                                               ZGB_A11_1 <<
                                                                               ZGR_A11_1 <<
                                                                               OsB_A11_1e <<
                                                                               OsR_A11_1e <<
                                                                               OaB_A11_1e <<
                                                                               OaR_A11_1e <<
                                                                               OeB_A11_1e <<
                                                                               OeR_A11_1e <<
                                                                               ZGB_A11_1e << ZGR_A11_1e,
                                                          QSet<SequenceName>() <<
                                                                               BsB_S11_1 <<
                                                                               BsR_S11_1 <<
                                                                               BbsB_S11_1 <<
                                                                               BbsR_S11_1 <<
                                                                               BaB_A11_1 <<
                                                                               BaR_A11_1 <<
                                                                               BsB_S11_1c <<
                                                                               BsR_S11_1c <<
                                                                               BbsB_S11_1c <<
                                                                               BbsR_S11_1c <<
                                                                               BaB_A11_1c <<
                                                                               BaR_A11_1c <<
                                                                               L_A11_1 <<
                                                                               L_A11_1e));
        QCOMPARE(idL.size(), 1);
        int A11_1_accum = idL.at(0);

        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>(), QSet<SequenceName>() <<
                                                                                            IrB_A11_1 <<
                                                                                            IrR_A11_1 <<
                                                                                            Ff_A11_1 <<
                                                                                            Fn_A11_1));
        QCOMPARE(idL.size(), 1);
        int A11_1_aux = idL.at(0);

        QVERIFY(A11_1_aux < A11_1_accum);


        filter->unhandled(L_A11_2, &controller);
        filter->unhandled(Fn_A11_2, &controller);
        filter->unhandled(BsB_S11_2, &controller);
        filter->unhandled(BaR_A11_2, &controller);
        filter->unhandled(BsR_S11_2, &controller);
        filter->unhandled(BbsB_S11_2, &controller);
        filter->unhandled(BbsR_S11_2, &controller);
        filter->unhandled(BaB_A11_2, &controller);
        filter->unhandled(IrR_A11_2, &controller);
        filter->unhandled(IrB_A11_2, &controller);

        QVERIFY(controller.contents.size() >= 4);
        idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                               OsB_A11_2 <<
                                                                               OsR_A11_2 <<
                                                                               OaB_A11_2 <<
                                                                               OaR_A11_2 <<
                                                                               OeB_A11_2 <<
                                                                               OeR_A11_2 <<
                                                                               ZGB_A11_2 <<
                                                                               ZGR_A11_2 <<
                                                                               OsB_A11_2e <<
                                                                               OsR_A11_2e <<
                                                                               OaB_A11_2e <<
                                                                               OaR_A11_2e <<
                                                                               OeB_A11_2e <<
                                                                               OeR_A11_2e <<
                                                                               ZGB_A11_2e << ZGR_A11_2e,
                                                          QSet<SequenceName>() <<
                                                                               BsB_S11_2 <<
                                                                               BsR_S11_2 <<
                                                                               BbsB_S11_2 <<
                                                                               BbsR_S11_2 <<
                                                                               BaB_A11_2 <<
                                                                               BaR_A11_2 <<
                                                                               BsB_S11_2c <<
                                                                               BsR_S11_2c <<
                                                                               BbsB_S11_2c <<
                                                                               BbsR_S11_2c <<
                                                                               BaB_A11_2c <<
                                                                               BaR_A11_2c <<
                                                                               L_A11_2 <<
                                                                               L_A11_2e));
        QCOMPARE(idL.size(), 1);
        int A11_2_accum = idL.at(0);

        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>(), QSet<SequenceName>() <<
                                                                                            IrB_A11_2 <<
                                                                                            IrR_A11_2 <<
                                                                                            Ff_A11_2 <<
                                                                                            Fn_A11_2));
        QCOMPARE(idL.size(), 1);
        int A11_2_aux = idL.at(0);

        QVERIFY(A11_2_aux < A11_2_accum);


        filter->unhandled(IrB_A11_1p, &controller);
        filter->unhandled(IrR_A11_1p, &controller);
        filter->unhandled(L_A11_1p, &controller);
        filter->unhandled(BsB_S11_1p, &controller);
        filter->unhandled(BsR_S11_1p, &controller);
        filter->unhandled(BbsB_S11_1p, &controller);
        filter->unhandled(BaB_A11_1p, &controller);
        filter->unhandled(BaR_A11_1p, &controller);
        filter->unhandled(BbsR_S11_1p, &controller);

        QVERIFY(controller.contents.size() >= 6);
        idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                               OsB_A11_1p <<
                                                                               OsR_A11_1p <<
                                                                               OaB_A11_1p <<
                                                                               OaR_A11_1p <<
                                                                               OeB_A11_1p <<
                                                                               OeR_A11_1p <<
                                                                               ZGB_A11_1p <<
                                                                               ZGR_A11_1p <<
                                                                               OsB_A11_1pe <<
                                                                               OsR_A11_1pe <<
                                                                               OaB_A11_1pe <<
                                                                               OaR_A11_1pe <<
                                                                               OeB_A11_1pe <<
                                                                               OeR_A11_1pe <<
                                                                               ZGB_A11_1pe << ZGR_A11_1pe,
                                                          QSet<SequenceName>() <<
                                                                               BsB_S11_1p <<
                                                                               BsR_S11_1p <<
                                                                               BbsB_S11_1p <<
                                                                               BbsR_S11_1p <<
                                                                               BaB_A11_1p <<
                                                                               BaR_A11_1p <<
                                                                               BsB_S11_1pc <<
                                                                               BsR_S11_1pc <<
                                                                               BbsB_S11_1pc <<
                                                                               BbsR_S11_1pc <<
                                                                               BaB_A11_1pc <<
                                                                               BaR_A11_1pc <<
                                                                               L_A11_1p <<
                                                                               L_A11_1pe));
        QCOMPARE(idL.size(), 1);
        int A11_1p_accum = idL.at(0);

        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>(), QSet<SequenceName>() <<
                                                                                            IrB_A11_1p <<
                                                                                            IrR_A11_1p <<
                                                                                            Ff_A11_1 <<
                                                                                            Fn_A11_1));
        QCOMPARE(idL.size(), 1);
        int A11_1p_aux = idL.at(0);

        QVERIFY(A11_1p_aux < A11_1p_accum);


        data.setStart(100.0);
        data.setEnd(200.0);

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(450.0);
            data.setValue(BsB_S11_1.toMeta(), meta);
            data.setValue(BsB_S11_1p.toMeta(), meta);
            data.setValue(BbsB_S11_1.toMeta(), meta);
            data.setValue(BbsB_S11_1p.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(440.0);
            data.setValue(BsB_S11_2.toMeta(), meta);
            data.setValue(BbsB_S11_2.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(700.0);
            data.setValue(BsR_S11_1.toMeta(), meta);
            data.setValue(BsR_S11_1p.toMeta(), meta);
            data.setValue(BbsR_S11_1.toMeta(), meta);
            data.setValue(BbsR_S11_1p.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(720.0);
            data.setValue(BsR_S11_2.toMeta(), meta);
            data.setValue(BbsR_S11_2.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(467.0);
            data.setValue(IrB_A11_1.toMeta(), meta);
            data.setValue(IrB_A11_1p.toMeta(), meta);
            data.setValue(BaB_A11_1.toMeta(), meta);
            data.setValue(BaB_A11_1p.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(450.0);
            data.setValue(IrB_A11_2.toMeta(), meta);
            data.setValue(BaB_A11_2.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(660.0);
            data.setValue(IrR_A11_1.toMeta(), meta);
            data.setValue(IrR_A11_1p.toMeta(), meta);
            data.setValue(BaR_A11_1.toMeta(), meta);
            data.setValue(BaR_A11_1p.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(690.0);
            data.setValue(IrR_A11_2.toMeta(), meta);
            data.setValue(BaR_A11_2.toMeta(), meta);

            filter->processMeta(A11_1_aux, data);
            filter->processMeta(A11_1_accum, data);
            QCOMPARE(data.value(OaB_A11_1.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QCOMPARE(data.value(OaB_A11_1.toMeta()).metadata("Wavelength").toDouble(), 467.0);
            QCOMPARE(data.value(OsB_A11_1.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QCOMPARE(data.value(OsB_A11_1.toMeta()).metadata("Wavelength").toDouble(), 467.0);
            QCOMPARE(data.value(ZGB_A11_1.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QCOMPARE(data.value(ZGB_A11_1.toMeta()).metadata("Wavelength").toDouble(), 467.0);

            filter->processMeta(A11_1p_aux, data);
            filter->processMeta(A11_1p_accum, data);
            QCOMPARE(data.value(OaB_A11_1p.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QCOMPARE(data.value(OaB_A11_1p.toMeta()).metadata("Wavelength").toDouble(), 467.0);
            QCOMPARE(data.value(OsB_A11_1p.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QCOMPARE(data.value(OsB_A11_1p.toMeta()).metadata("Wavelength").toDouble(), 467.0);
            QCOMPARE(data.value(ZGB_A11_1p.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QCOMPARE(data.value(ZGB_A11_1p.toMeta()).metadata("Wavelength").toDouble(), 467.0);

            filter->processMeta(A11_2_aux, data);
            filter->processMeta(A11_2_accum, data);
        }

        data.setStart(100.0);
        data.setEnd(110.0);

        data.setValue(Fn_A11_1, Variant::Root(1));
        data.setValue(Ff_A11_1, Variant::Root(2));
        data.setValue(IrR_A11_1, Variant::Root(1.0));
        data.setValue(IrB_A11_1, Variant::Root(1.0));
        filter->process(A11_1_aux, data);

        data.setValue(BsB_S11_1, Variant::Root(10.0));
        data.setValue(BsR_S11_1, Variant::Root(8.0));
        data.setValue(BbsB_S11_1, Variant::Root(2.0));
        data.setValue(BbsR_S11_1, Variant::Root(1.0));
        data.setValue(BaB_A11_1, Variant::Root(6.0));
        data.setValue(BaR_A11_1, Variant::Root(4.0));
        data.setValue(L_A11_1, Variant::Root(0.0));
        data.setValue(L_A11_1e, Variant::Root(100.0));
        filter->process(A11_1_accum, data);
        QCOMPARE(data.value(OsB_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OsB_A11_1e).toDouble(), 0.00098144651735884952219);
        QCOMPARE(data.value(OsR_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OsR_A11_1e).toDouble(), 0.00082413024259926799791);
        QCOMPARE(data.value(OaB_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OaB_A11_1e).toDouble(), 0.0006);
        QCOMPARE(data.value(OaR_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OaR_A11_1e).toDouble(), 0.0004);
        QCOMPARE(data.value(ZGB_A11_1e).toDouble(), 0.45239398252416762602);
        QCOMPARE(data.value(ZGR_A11_1e).toDouble(), 0.57719740797356744455);

        data.setStart(110.0);
        data.setEnd(120.0);
        {
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
            }
            QVERIFY(filter2 != NULL);
            data.setValue(Fn_A11_1, Variant::Root(1));
            data.setValue(Ff_A11_1, Variant::Root(2));
            data.setValue(IrR_A11_1, Variant::Root(0.99));
            data.setValue(IrB_A11_1, Variant::Root(0.98));
            filter2->process(A11_1_aux, data);

            data.setValue(BsB_S11_1, Variant::Root(11.0));
            data.setValue(BsR_S11_1, Variant::Root(9.0));
            data.setValue(BbsB_S11_1, Variant::Root(3.0));
            data.setValue(BbsR_S11_1, Variant::Root(2.0));
            data.setValue(BaB_A11_1, Variant::Root(7.0));
            data.setValue(BaR_A11_1, Variant::Root(5.0));
            data.setValue(L_A11_1, Variant::Root(100.0));
            data.setValue(L_A11_1e, Variant::Root(190.0));
            filter2->process(A11_1_accum, data);
            QCOMPARE(data.value(OsB_A11_1).toDouble(), 0.00098144651735884952219);
            QCOMPARE(data.value(OsB_A11_1e).toDouble(), 0.0019549128728802564123);
            QCOMPARE(data.value(OsR_A11_1).toDouble(), 0.00082413024259926799791);
            QCOMPARE(data.value(OsR_A11_1e).toDouble(), 0.0016560685643666646361);
            QCOMPARE(data.value(OaB_A11_1).toDouble(), 0.0006);
            QCOMPARE(data.value(OaB_A11_1e).toDouble(), 0.00123);
            QCOMPARE(data.value(OaR_A11_1).toDouble(), 0.0004);
            QCOMPARE(data.value(OaR_A11_1e).toDouble(), 0.00085);
            QCOMPARE(data.value(ZGB_A11_1).toDouble(), 0.45239398252416762602);
            QCOMPARE(data.value(ZGB_A11_1e).toDouble(), 0.38559010920380520915);
            QCOMPARE(data.value(ZGR_A11_1).toDouble(), 0.57719740797356744455);
            QCOMPARE(data.value(ZGR_A11_1e).toDouble(), 0.47417643950580140588);
            delete filter2;
        }


        data.setValue(Fn_A11_1, Variant::Root(1));
        data.setValue(Ff_A11_1, Variant::Root(2));
        data.setValue(IrR_A11_1, Variant::Root(0.8));
        data.setValue(IrB_A11_1, Variant::Root(0.7));
        filter->process(A11_1_aux, data);

        data.setValue(BsB_S11_1, Variant::Root(11.0));
        data.setValue(BsR_S11_1, Variant::Root(9.0));
        data.setValue(BbsB_S11_1, Variant::Root(3.0));
        data.setValue(BbsR_S11_1, Variant::Root(2.0));
        data.setValue(BaB_A11_1, Variant::Root(7.0));
        data.setValue(BaR_A11_1, Variant::Root(5.0));
        data.setValue(L_A11_1, Variant::Root(100.0));
        data.setValue(L_A11_1e, Variant::Root(190.0));
        filter->process(A11_1_accum, data);
        QCOMPARE(data.value(OsB_A11_1).toDouble(), 0.00098144651735884952219);
        QCOMPARE(data.value(OsB_A11_1e).toDouble(), 0.0019549128728802564123);
        QCOMPARE(data.value(OsR_A11_1).toDouble(), 0.00082413024259926799791);
        QCOMPARE(data.value(OsR_A11_1e).toDouble(), 0.0016560685643666646361);
        QCOMPARE(data.value(OaB_A11_1).toDouble(), 0.0006);
        QCOMPARE(data.value(OaB_A11_1e).toDouble(), 0.00123);
        QCOMPARE(data.value(OaR_A11_1).toDouble(), 0.0004);
        QCOMPARE(data.value(OaR_A11_1e).toDouble(), 0.00085);
        QCOMPARE(data.value(ZGB_A11_1).toDouble(), 0.45239398252416762602);
        QCOMPARE(data.value(ZGB_A11_1e).toDouble(), 0.38559010920380520915);
        QCOMPARE(data.value(ZGR_A11_1).toDouble(), 0.57719740797356744455);
        QCOMPARE(data.value(ZGR_A11_1e).toDouble(), 0.47417643950580140588);


        data.setStart(120.0);
        data.setEnd(130.0);

        data.setValue(Fn_A11_1, Variant::Root(1));
        data.setValue(Ff_A11_1, Variant::Root(2));
        data.setValue(IrR_A11_1, Variant::Root(1.0));
        data.setValue(IrB_A11_1, Variant::Root(1.0));
        filter->process(A11_1_aux, data);

        data.setValue(BsB_S11_1, Variant::Root(10.0));
        data.setValue(BsR_S11_1, Variant::Root(8.0));
        data.setValue(BbsB_S11_1, Variant::Root(2.0));
        data.setValue(BbsR_S11_1, Variant::Root(1.0));
        data.setValue(BaB_A11_1, Variant::Root(6.0));
        data.setValue(BaR_A11_1, Variant::Root(4.0));
        data.setValue(L_A11_1, Variant::Root(0.0));
        data.setValue(L_A11_1e, Variant::Root(100.0));
        filter->process(A11_1_accum, data);
        QCOMPARE(data.value(OsB_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OsB_A11_1e).toDouble(), 0.00098144651735884952219);
        QCOMPARE(data.value(OsR_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OsR_A11_1e).toDouble(), 0.00082413024259926799791);
        QCOMPARE(data.value(OaB_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OaB_A11_1e).toDouble(), 0.0006);
        QCOMPARE(data.value(OaR_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OaR_A11_1e).toDouble(), 0.0004);
        QCOMPARE(data.value(ZGB_A11_1e).toDouble(), 0.45239398252416762602);
        QCOMPARE(data.value(ZGR_A11_1e).toDouble(), 0.57719740797356744455);


        data.setStart(130.0);
        data.setEnd(140.0);

        data.setValue(Fn_A11_1, Variant::Root(1));
        data.setValue(Ff_A11_1, Variant::Root(3));
        data.setValue(IrR_A11_1, Variant::Root(1.0));
        data.setValue(IrB_A11_1, Variant::Root(1.0));
        filter->process(A11_1_aux, data);

        data.setValue(BsB_S11_1, Variant::Root(10.0));
        data.setValue(BsR_S11_1, Variant::Root(8.0));
        data.setValue(BbsB_S11_1, Variant::Root(2.0));
        data.setValue(BbsR_S11_1, Variant::Root(1.0));
        data.setValue(BaB_A11_1, Variant::Root(6.0));
        data.setValue(BaR_A11_1, Variant::Root(4.0));
        data.setValue(L_A11_1, Variant::Root(0.0));
        data.setValue(L_A11_1e, Variant::Root(100.0));
        filter->process(A11_1_accum, data);
        QCOMPARE(data.value(OsB_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OsB_A11_1e).toDouble(), 0.00098144651735884952219);
        QCOMPARE(data.value(OsR_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OsR_A11_1e).toDouble(), 0.00082413024259926799791);
        QCOMPARE(data.value(OaB_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OaB_A11_1e).toDouble(), 0.0006);
        QCOMPARE(data.value(OaR_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OaR_A11_1e).toDouble(), 0.0004);
        QCOMPARE(data.value(ZGB_A11_1e).toDouble(), 0.45239398252416762602);
        QCOMPARE(data.value(ZGR_A11_1e).toDouble(), 0.57719740797356744455);


        data.setStart(140.0);
        data.setEnd(150.0);

        data.setValue(Fn_A11_1, Variant::Root(2));
        data.setValue(Ff_A11_1, Variant::Root(3));
        data.setValue(IrR_A11_1, Variant::Root(1.0));
        data.setValue(IrB_A11_1, Variant::Root(1.0));
        filter->process(A11_1_aux, data);

        data.setValue(BsB_S11_1, Variant::Root(10.0));
        data.setValue(BsR_S11_1, Variant::Root(8.0));
        data.setValue(BbsB_S11_1, Variant::Root(2.0));
        data.setValue(BbsR_S11_1, Variant::Root(1.0));
        data.setValue(BaB_A11_1, Variant::Root(6.0));
        data.setValue(BaR_A11_1, Variant::Root(4.0));
        data.setValue(L_A11_1, Variant::Root(500.0));
        data.setValue(L_A11_1e, Variant::Root(600.0));
        filter->process(A11_1_accum, data);
        QCOMPARE(data.value(OsB_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OsB_A11_1e).toDouble(), 0.00098144651735884952219);
        QCOMPARE(data.value(OsR_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OsR_A11_1e).toDouble(), 0.00082413024259926799791);
        QCOMPARE(data.value(OaB_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OaB_A11_1e).toDouble(), 0.0006);
        QCOMPARE(data.value(OaR_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OaR_A11_1e).toDouble(), 0.0004);
        QCOMPARE(data.value(ZGB_A11_1e).toDouble(), 0.45239398252416762602);
        QCOMPARE(data.value(ZGR_A11_1e).toDouble(), 0.57719740797356744455);

        data.setStart(150.0);
        data.setEnd(160.0);

        data.setValue(Fn_A11_1, Variant::Root(2));
        data.setValue(Ff_A11_1, Variant::Root(3));
        data.setValue(IrR_A11_1, Variant::Root(0.99));
        data.setValue(IrB_A11_1, Variant::Root(0.99));
        filter->process(A11_1_aux, data);

        data.setValue(BsB_S11_1, Variant::Root(11.0));
        data.setValue(BsR_S11_1, Variant::Root(9.0));
        data.setValue(BbsB_S11_1, Variant::Root(3.0));
        data.setValue(BbsR_S11_1, Variant::Root(2.0));
        data.setValue(BaB_A11_1, Variant::Root(7.0));
        data.setValue(BaR_A11_1, Variant::Root(5.0));
        data.setValue(L_A11_1, Variant::Root(600.0));
        data.setValue(L_A11_1e, Variant::Root(690.0));
        filter->process(A11_1_accum, data);
        QCOMPARE(data.value(OsB_A11_1).toDouble(), 0.00098144651735884952219);
        QCOMPARE(data.value(OsB_A11_1e).toDouble(), 0.0019549128728802564123);
        QCOMPARE(data.value(OsR_A11_1).toDouble(), 0.00082413024259926799791);
        QCOMPARE(data.value(OsR_A11_1e).toDouble(), 0.0016560685643666646361);
        QCOMPARE(data.value(OaB_A11_1).toDouble(), 0.0006);
        QCOMPARE(data.value(OaB_A11_1e).toDouble(), 0.00123);
        QCOMPARE(data.value(OaR_A11_1).toDouble(), 0.0004);
        QCOMPARE(data.value(OaR_A11_1e).toDouble(), 0.00085);
        QCOMPARE(data.value(ZGB_A11_1).toDouble(), 0.45239398252416762602);
        QCOMPARE(data.value(ZGB_A11_1e).toDouble(), 0.38559010920380520915);
        QCOMPARE(data.value(ZGR_A11_1).toDouble(), 0.57719740797356744455);
        QCOMPARE(data.value(ZGR_A11_1e).toDouble(), 0.47417643950580140588);

        data.setStart(160.0);
        data.setEnd(170.0);

        data.setValue(Fn_A11_1, Variant::Root(2));
        data.setValue(Ff_A11_1, Variant::Root(3));
        data.setValue(IrR_A11_1, Variant::Root(1.0));
        data.setValue(IrB_A11_1, Variant::Root(1.0));
        filter->process(A11_1_aux, data);

        data.setValue(BsB_S11_1, Variant::Root(10.0));
        data.setValue(BsR_S11_1, Variant::Root(8.0));
        data.setValue(BbsB_S11_1, Variant::Root(2.0));
        data.setValue(BbsR_S11_1, Variant::Root(1.0));
        data.setValue(BaB_A11_1, Variant::Root(6.0));
        data.setValue(BaR_A11_1, Variant::Root(4.0));
        data.setValue(L_A11_1, Variant::Root(0.0));
        data.setValue(L_A11_1e, Variant::Root(100.0));
        filter->process(A11_1_accum, data);
        QCOMPARE(data.value(OsB_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OsB_A11_1e).toDouble(), 0.00098144651735884952219);
        QCOMPARE(data.value(OsR_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OsR_A11_1e).toDouble(), 0.00082413024259926799791);
        QCOMPARE(data.value(OaB_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OaB_A11_1e).toDouble(), 0.0006);
        QCOMPARE(data.value(OaR_A11_1).toDouble(), 0.0);
        QCOMPARE(data.value(OaR_A11_1e).toDouble(), 0.0004);
        QCOMPARE(data.value(ZGB_A11_1e).toDouble(), 0.45239398252416762602);
        QCOMPARE(data.value(ZGR_A11_1e).toDouble(), 0.57719740797356744455);


        data.setValue(Fn_A11_1, Variant::Root(2));
        data.setValue(Ff_A11_1, Variant::Root(3));
        data.setValue(IrR_A11_1p, Variant::Root(0.8));
        data.setValue(IrB_A11_1p, Variant::Root(0.7));
        filter->process(A11_1p_aux, data);

        data.setValue(BsB_S11_1p, Variant::Root(11.0));
        data.setValue(BsR_S11_1p, Variant::Root(9.0));
        data.setValue(BbsB_S11_1p, Variant::Root(3.0));
        data.setValue(BbsR_S11_1p, Variant::Root(2.0));
        data.setValue(BaB_A11_1p, Variant::Root(7.0));
        data.setValue(BaR_A11_1p, Variant::Root(5.0));
        data.setValue(L_A11_1p, Variant::Root(100.0));
        data.setValue(L_A11_1pe, Variant::Root(190.0));
        filter->process(A11_1p_accum, data);
        QCOMPARE(data.value(OsB_A11_1p).toDouble(), 0.00098144651735884952219);
        QCOMPARE(data.value(OsB_A11_1pe).toDouble(), 0.0019549128728802564123);
        QCOMPARE(data.value(OsR_A11_1p).toDouble(), 0.00082413024259926799791);
        QCOMPARE(data.value(OsR_A11_1pe).toDouble(), 0.0016560685643666646361);
        QCOMPARE(data.value(OaB_A11_1p).toDouble(), 0.0006);
        QCOMPARE(data.value(OaB_A11_1pe).toDouble(), 0.00123);
        QCOMPARE(data.value(OaR_A11_1p).toDouble(), 0.0004);
        QCOMPARE(data.value(OaR_A11_1pe).toDouble(), 0.00085);
        QCOMPARE(data.value(ZGB_A11_1p).toDouble(), 0.45239398252416762602);
        QCOMPARE(data.value(ZGB_A11_1pe).toDouble(), 0.38559010920380520915);
        QCOMPARE(data.value(ZGR_A11_1p).toDouble(), 0.57719740797356744455);
        QCOMPARE(data.value(ZGR_A11_1pe).toDouble(), 0.47417643950580140588);


        data.setValue(Fn_A11_2, Variant::Root(1));
        data.setValue(Ff_A11_2, Variant::Root(2));
        data.setValue(IrR_A11_2, Variant::Root(1.0));
        data.setValue(IrB_A11_2, Variant::Root(1.0));
        filter->process(A11_2_aux, data);

        data.setValue(BsB_S11_2, Variant::Root(20.0));
        data.setValue(BsR_S11_2, Variant::Root(18.0));
        data.setValue(BbsB_S11_2, Variant::Root(5.0));
        data.setValue(BbsR_S11_2, Variant::Root(4.0));
        data.setValue(BaB_A11_2, Variant::Root(8.0));
        data.setValue(BaR_A11_2, Variant::Root(7.0));
        data.setValue(L_A11_2, Variant::Root(0.0));
        data.setValue(L_A11_2e, Variant::Root(60.0));
        filter->process(A11_2_accum, data);
        QCOMPARE(data.value(OsB_A11_2).toDouble(), 0.0);
        QCOMPARE(data.value(OsB_A11_2e).toDouble(), 0.0011942444304184379918);
        QCOMPARE(data.value(OsR_A11_2).toDouble(), 0.0);
        QCOMPARE(data.value(OsR_A11_2e).toDouble(), 0.0010898785340485766995);
        QCOMPARE(data.value(OaB_A11_2).toDouble(), 0.0);
        QCOMPARE(data.value(OaB_A11_2e).toDouble(), 0.00048);
        QCOMPARE(data.value(OaR_A11_2).toDouble(), 0.0);
        QCOMPARE(data.value(OaR_A11_2e).toDouble(), 0.00042);
        QCOMPARE(data.value(ZGB_A11_2e).toDouble(), 0.3554104603774624338);
        QCOMPARE(data.value(ZGR_A11_2e).toDouble(), 0.3948544678848139311);

        delete filter;
    }

    void dynamicOptionsDirect()
    {
        ComponentOptions options(component->getOptions());

        qobject_cast<DynamicInputOption *>(options.get("length-start"))->set(100);
        qobject_cast<DynamicInputOption *>(options.get("length-end"))->set(200);
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("transmittance"))->set("", "",
                                                                                          "Ir.*");
        qobject_cast<TimeIntervalSelectionOption *>(options.get("reset-gap"))->set(Time::Second, 1);
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("absorption"))->set("", "",
                                                                                       "Ba.*");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("scattering"))->set("", "",
                                                                                       "Bs.*");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("backscattering"))->set("", "",
                                                                                           "Bbs.*");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("extinction"))->set("", "",
                                                                                       "Be.*");

        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "F1_A11"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName::Flavors end{"end"};
        SequenceName::Flavors cover{"cover"};

        SequenceName Ba1_IN("brw", "raw", "Ba1_A12");
        SequenceName Ba2_IN("brw", "raw", "Ba1_A13");
        SequenceName Bs1_IN("brw", "raw", "Bs1_S11");
        SequenceName Bs2_IN("brw", "raw", "Bs1_S12");
        SequenceName Bbs1_IN("brw", "raw", "Bbs_S13");
        SequenceName Bbs2_IN("brw", "raw", "Bbs1_S14");
        SequenceName Be1_IN("brw", "raw", "Be_E11");
        SequenceName Be2_IN("brw", "raw", "Be1_E12");
        SequenceName Ba1_INc("brw", "raw", "Ba1_A12", cover);
        SequenceName Ba2_INc("brw", "raw", "Ba1_A13", cover);
        SequenceName Bs1_INc("brw", "raw", "Bs1_S11", cover);
        SequenceName Bs2_INc("brw", "raw", "Bs1_S12", cover);
        SequenceName Bbs1_INc("brw", "raw", "Bbs_S13", cover);
        SequenceName Bbs2_INc("brw", "raw", "Bbs1_S14", cover);
        SequenceName Be1_INc("brw", "raw", "Be_E11", cover);
        SequenceName Be2_INc("brw", "raw", "Be1_E12", cover);

        SequenceName Ff_IN("brw", "raw", "Ff_A11");
        SequenceName Fn_IN("brw", "raw", "Fn_A11");
        SequenceName Ir_IN("brw", "raw", "Ir_A11");
        SequenceName Os_OUT("brw", "raw", "Os_A11");
        SequenceName Oa_OUT("brw", "raw", "Oa_A11");
        SequenceName Oe_OUT("brw", "raw", "Oe_A11");
        SequenceName ZG_OUT("brw", "raw", "ZG_A11");
        SequenceName Os_OUTe("brw", "raw", "Os_A11", end);
        SequenceName Oa_OUTe("brw", "raw", "Oa_A11", end);
        SequenceName Oe_OUTe("brw", "raw", "Oe_A11", end);
        SequenceName ZG_OUTe("brw", "raw", "ZG_A11", end);

        filter->unhandled(Ba1_IN, &controller);
        filter->unhandled(Bs1_IN, &controller);
        filter->unhandled(Bbs1_IN, &controller);
        filter->unhandled(Be1_IN, &controller);
        filter->unhandled(Ir_IN, &controller);
        filter->unhandled(Ba2_IN, &controller);
        filter->unhandled(Bs2_IN, &controller);
        filter->unhandled(Bbs2_IN, &controller);
        filter->unhandled(Be2_IN, &controller);

        QVERIFY(controller.contents.size() >= 2);
        idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                               Os_OUT <<
                                                                               Oa_OUT <<
                                                                               Oe_OUT <<
                                                                               ZG_OUT <<
                                                                               Os_OUTe <<
                                                                               Oa_OUTe <<
                                                                               Oe_OUTe << ZG_OUTe,
                                                          QSet<SequenceName>() <<
                                                                               Ba1_IN <<
                                                                               Ba2_IN <<
                                                                               Ba1_INc <<
                                                                               Ba2_INc <<
                                                                               Bs1_IN <<
                                                                               Bs2_IN <<
                                                                               Bs1_INc <<
                                                                               Bs2_INc <<
                                                         Bbs1_IN <<
                                                         Bbs2_IN <<
                                                                               Bbs1_INc <<
                                                                               Bbs2_INc <<
                                                                               Be1_IN <<
                                                                               Be2_IN <<
                                                                               Be1_INc <<
                                                                               Be2_INc));
        QCOMPARE(idL.size(), 1);
        int accum = idL.at(0);

        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>(),
                                                 QSet<SequenceName>() << Ir_IN << Ff_IN << Fn_IN));
        QCOMPARE(idL.size(), 1);
        int aux = idL.at(0);

        QVERIFY(aux < accum);

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(400.0);
            data.setValue(Ba1_IN.toMeta(), meta);
            data.setValue(Bs1_IN.toMeta(), meta);
            data.setValue(Bbs1_IN.toMeta(), meta);
            data.setValue(Be1_IN.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(500.0);
            data.setValue(Ba2_IN.toMeta(), meta);
            data.setValue(Bs2_IN.toMeta(), meta);
            data.setValue(Bbs2_IN.toMeta(), meta);
            data.setValue(Be2_IN.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(450.0);
            data.setValue(Ir_IN.toMeta(), meta);

            filter->processMeta(aux, data);
            filter->processMeta(accum, data);
            QCOMPARE(data.value(Os_OUT.toMeta()).metadata("Wavelength").toDouble(), 450.0);
            QCOMPARE(data.value(Oa_OUT.toMeta()).metadata("Wavelength").toDouble(), 450.0);
            QCOMPARE(data.value(Oe_OUT.toMeta()).metadata("Wavelength").toDouble(), 450.0);
            QCOMPARE(data.value(ZG_OUT.toMeta()).metadata("Wavelength").toDouble(), 450.0);
            QCOMPARE(data.value(Os_OUT.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
        }

        data.setStart(100);
        data.setEnd(200);
        data.setValue(Ba1_IN, Variant::Root(10.0));
        data.setValue(Ba2_IN, Variant::Root(20.0));
        data.setValue(Bs1_IN, Variant::Root(30.0));
        data.setValue(Bs2_IN, Variant::Root(40.0));
        data.setValue(Bbs1_IN, Variant::Root(2.0));
        data.setValue(Bbs2_IN, Variant::Root(4.0));
        data.setValue(Be1_IN, Variant::Root(70.0));
        data.setValue(Be2_IN, Variant::Root(80.0));
        filter->process(accum, data);
        QCOMPARE(data.value(Oa_OUT).toDouble(), 0.0);
        QCOMPARE(data.value(Oa_OUTe).toDouble(), 0.001441764233862422);
        QCOMPARE(data.value(Os_OUT).toDouble(), 0.0);
        QCOMPARE(data.value(Os_OUTe).toDouble(), 0.003491952487430488);
        QCOMPARE(data.value(Oe_OUT).toDouble(), 0.0);
        QCOMPARE(data.value(Oe_OUTe).toDouble(), 0.007511181125684738);
        QCOMPARE(data.value(ZG_OUTe).toDouble(), 0.7088763423613439);

        data.setStart(200);
        data.setEnd(300);
        data.setValue(Ba1_IN, Variant::Root(10.0));
        data.setValue(Ba2_IN, Variant::Root(20.0));
        data.setValue(Bs1_IN, Variant::Root(30.0));
        data.setValue(Bs2_IN, Variant::Root(40.0));
        data.setValue(Bbs1_IN, Variant::Root(2.0));
        data.setValue(Bbs2_IN, Variant::Root(4.0));
        data.setValue(Be1_IN, Variant::Root(70.0));
        data.setValue(Be2_IN, Variant::Root(80.0));
        filter->process(accum, data);
        QCOMPARE(data.value(Oa_OUT).toDouble(), 0.001441764233862422);
        QCOMPARE(data.value(Oa_OUTe).toDouble(), 0.002883528467724844);
        QCOMPARE(data.value(Os_OUT).toDouble(), 0.003491952487430488);
        QCOMPARE(data.value(Os_OUTe).toDouble(), 0.006983904974860977);
        QCOMPARE(data.value(Oe_OUT).toDouble(), 0.007511181125684738);
        QCOMPARE(data.value(Oe_OUTe).toDouble(), 0.01502236225136948);
        QCOMPARE(data.value(ZG_OUT).toDouble(), 0.7088763423613439);
        QCOMPARE(data.value(ZG_OUTe).toDouble(), 0.7088763423613439);

        data.setStart(400);
        data.setEnd(500);
        data.setValue(Ba1_IN, Variant::Root(10.0));
        data.setValue(Ba2_IN, Variant::Root(20.0));
        data.setValue(Bs1_IN, Variant::Root(30.0));
        data.setValue(Bs2_IN, Variant::Root(40.0));
        data.setValue(Bbs1_IN, Variant::Root(2.0));
        data.setValue(Bbs2_IN, Variant::Root(4.0));
        data.setValue(Be1_IN, Variant::Root(70.0));
        data.setValue(Be2_IN, Variant::Root(80.0));
        filter->process(accum, data);
        QCOMPARE(data.value(Oa_OUT).toDouble(), 0.0);
        QCOMPARE(data.value(Oa_OUTe).toDouble(), 0.001441764233862422);
        QCOMPARE(data.value(Os_OUT).toDouble(), 0.0);
        QCOMPARE(data.value(Os_OUTe).toDouble(), 0.003491952487430488);
        QCOMPARE(data.value(Oe_OUT).toDouble(), 0.0);
        QCOMPARE(data.value(Oe_OUTe).toDouble(), 0.007511181125684738);
        QCOMPARE(data.value(ZG_OUTe).toDouble(), 0.7088763423613439);

        data.setStart(500);
        data.setEnd(600);
        data.setValue(Ba1_IN, Variant::Root(FP::undefined()));
        data.setValue(Ba2_IN, Variant::Root(FP::undefined()));
        data.setValue(Bs1_IN, Variant::Root(FP::undefined()));
        data.setValue(Bs2_IN, Variant::Root(FP::undefined()));
        data.setValue(Bbs1_IN, Variant::Root(FP::undefined()));
        data.setValue(Bbs2_IN, Variant::Root(FP::undefined()));
        data.setValue(Be1_IN, Variant::Root(FP::undefined()));
        data.setValue(Be2_IN, Variant::Root(FP::undefined()));
        filter->process(accum, data);
        QCOMPARE(data.value(Oa_OUT).toDouble(), 0.001441764233862422);
        QCOMPARE(data.value(Oa_OUTe).toDouble(), 0.002883528467724844);
        QCOMPARE(data.value(Os_OUT).toDouble(), 0.003491952487430488);
        QCOMPARE(data.value(Os_OUTe).toDouble(), 0.006983904974860977);
        QCOMPARE(data.value(Oe_OUT).toDouble(), 0.007511181125684738);
        QCOMPARE(data.value(Oe_OUTe).toDouble(), 0.01502236225136948);
        QCOMPARE(data.value(ZG_OUT).toDouble(), 0.7088763423613439);
        QCOMPARE(data.value(ZG_OUTe).toDouble(), 0.7088763423613439);

        delete filter;
    }

    void dynamicOptionsSuffix()
    {
        ComponentOptions options(component->getOptions());

        qobject_cast<DynamicInputOption *>(options.get("delta-l"))->set(100.0);
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->add("A11");
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("scattering-suffix"))->add(
                "S11");
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("absorption-suffix"))->add(
                "A12");
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("absorption-suffix"))->add(
                "A12");
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("extinction-suffix"))->add(
                "E11");
        qobject_cast<ComponentOptionBoolean *>(options.get("calculate-l"))->set(true);
        qobject_cast<DynamicBoolOption *>(options.get("synchronous-zero"))->set(true);

        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "IrG_A12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BaG_A11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BsG_S12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BbsG_S12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BeG_E12"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName::Flavors end{"end"};
        SequenceName::Flavors cover{"cover"};

        SequenceName Ba1_IN("brw", "raw", "Ba1_A12");
        SequenceName Ba2_IN("brw", "raw", "Ba2_A12");
        SequenceName F_Ba("brw", "raw", "F1_A12");
        SequenceName Bs1_IN("brw", "raw", "Bs1_S11");
        SequenceName Bs2_IN("brw", "raw", "Bs2_S11");
        SequenceName F_Bs("brw", "raw", "F1_S11");
        SequenceName Bbs1_IN("brw", "raw", "Bbs1_S11");
        SequenceName Bbs2_IN("brw", "raw", "Bbs2_S11");
        SequenceName Be1_IN("brw", "raw", "Be1_E11");
        SequenceName Be2_IN("brw", "raw", "Be2_E11");
        SequenceName F_Be("brw", "raw", "F1_E11");
        SequenceName Ba1_INc("brw", "raw", "Ba1_A12", cover);
        SequenceName Ba2_INc("brw", "raw", "Ba2_A12", cover);
        SequenceName Bs1_INc("brw", "raw", "Bs1_S11", cover);
        SequenceName Bs2_INc("brw", "raw", "Bs2_S11", cover);
        SequenceName Bbs1_INc("brw", "raw", "Bbs1_S11", cover);
        SequenceName Bbs2_INc("brw", "raw", "Bbs2_S11", cover);
        SequenceName Be1_INc("brw", "raw", "Be1_E11", cover);
        SequenceName Be2_INc("brw", "raw", "Be2_E11", cover);

        SequenceName Ff_IN("brw", "raw", "Ff_A11");
        SequenceName Fn_IN("brw", "raw", "Fn_A11");
        SequenceName Ir_IN("brw", "raw", "Ir_A11");
        SequenceName Q_IN("brw", "raw", "Q_A11");
        SequenceName ZSPOT_IN("brw", "raw", "ZSPOT_A11");
        SequenceName L_OUT("brw", "raw", "L_A11");
        SequenceName Os_OUT("brw", "raw", "Os_A11");
        SequenceName Oa_OUT("brw", "raw", "Oa_A11");
        SequenceName Oe_OUT("brw", "raw", "Oe_A11");
        SequenceName ZG_OUT("brw", "raw", "ZG_A11");
        SequenceName L_OUTe("brw", "raw", "L_A11", end);
        SequenceName Os_OUTe("brw", "raw", "Os_A11", end);
        SequenceName Oa_OUTe("brw", "raw", "Oa_A11", end);
        SequenceName Oe_OUTe("brw", "raw", "Oe_A11", end);
        SequenceName ZG_OUTe("brw", "raw", "ZG_A11", end);

        filter->unhandled(Ir_IN, &controller);
        filter->unhandled(Ba1_IN, &controller);
        filter->unhandled(Bs1_IN, &controller);
        filter->unhandled(Bbs1_IN, &controller);
        filter->unhandled(Be1_IN, &controller);
        filter->unhandled(Ba2_IN, &controller);
        filter->unhandled(Bs2_IN, &controller);
        filter->unhandled(Bbs2_IN, &controller);
        filter->unhandled(Be2_IN, &controller);

        QVERIFY(controller.contents.size() >= 2);
        idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                               Os_OUT <<
                                                                               Oa_OUT <<
                                                                               Oe_OUT <<
                                                                               ZG_OUT <<
                                                                               Os_OUTe <<
                                                                               Oa_OUTe <<
                                                                               Oe_OUTe <<
                                                                               ZG_OUTe <<
                                                                               L_OUT << L_OUTe,
                                                          QSet<SequenceName>() <<
                                                                               Ba1_IN <<
                                                                               Ba2_IN <<
                                                                               Ba1_INc <<
                                                                               Ba2_INc <<
                                                                               Bs1_IN <<
                                                                               Bs2_IN <<
                                                                               Bs1_INc <<
                                                                               Bs2_INc <<
                                                         Bbs1_IN <<
                                                         Bbs2_IN <<
                                                         Bbs1_INc <<
                                                         Bbs2_INc <<
                                                         Be1_IN <<
                                                                               Be2_IN <<
                                                                               Be1_INc <<
                                                                               Be2_INc <<
                                                                               F_Ba <<
                                                                               F_Bs <<
                                                                               F_Be));
        QCOMPARE(idL.size(), 1);
        int accum = idL.at(0);

        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>(), QSet<SequenceName>() <<
                                                                                            Ir_IN <<
                                                                                            Ff_IN <<
                                                                                            Fn_IN <<
                                                                                            Q_IN <<
                                                                                            ZSPOT_IN));
        QCOMPARE(idL.size(), 1);
        int aux = idL.at(0);

        QVERIFY(aux < accum);

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(400.0);
            data.setValue(Ba1_IN.toMeta(), meta);
            data.setValue(Bs1_IN.toMeta(), meta);
            data.setValue(Bbs1_IN.toMeta(), meta);
            data.setValue(Be1_IN.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(500.0);
            data.setValue(Ba2_IN.toMeta(), meta);
            data.setValue(Bs2_IN.toMeta(), meta);
            data.setValue(Bbs2_IN.toMeta(), meta);
            data.setValue(Be2_IN.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(450.0);
            data.setValue(Ir_IN.toMeta(), meta);

            filter->processMeta(aux, data);
            filter->processMeta(accum, data);
            QCOMPARE(data.value(Os_OUT.toMeta()).metadata("Wavelength").toDouble(), 450.0);
            QCOMPARE(data.value(Oa_OUT.toMeta()).metadata("Wavelength").toDouble(), 450.0);
            QCOMPARE(data.value(Oe_OUT.toMeta()).metadata("Wavelength").toDouble(), 450.0);
            QCOMPARE(data.value(ZG_OUT.toMeta()).metadata("Wavelength").toDouble(), 450.0);
            QCOMPARE(data.value(Os_OUT.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
        }

        {
            data.setValue(Q_IN, Variant::Root(0.6));
            data.setValue(Ff_IN, Variant::Root(1));
            Variant::Root spot;
            spot["Area"].setDouble(10.0);
            data.setValue(ZSPOT_IN, spot);
            filter->process(aux, data);
        }

        data.setStart(100);
        data.setEnd(200);
        data.setValue(F_Ba, Variant::Root(Variant::Type::Flags));
        data.setValue(F_Bs, Variant::Root(Variant::Type::Flags));
        data.setValue(F_Be, Variant::Root(Variant::Type::Flags));
        data.setValue(Ba1_IN, Variant::Root(10.0));
        data.setValue(Ba2_IN, Variant::Root(20.0));
        data.setValue(Bs1_IN, Variant::Root(30.0));
        data.setValue(Bs2_IN, Variant::Root(40.0));
        data.setValue(Bbs1_IN, Variant::Root(2.0));
        data.setValue(Bbs2_IN, Variant::Root(4.0));
        data.setValue(Be1_IN, Variant::Root(70.0));
        data.setValue(Be2_IN, Variant::Root(80.0));
        filter->process(accum, data);
        QCOMPARE(data.value(L_OUT).toDouble(), 0.0);
        QCOMPARE(data.value(L_OUTe).toDouble(), 100.0);
        QCOMPARE(data.value(Oa_OUT).toDouble(), 0.0);
        QCOMPARE(data.value(Oa_OUTe).toDouble(), 0.001441764233862422);
        QCOMPARE(data.value(Os_OUT).toDouble(), 0.0);
        QCOMPARE(data.value(Os_OUTe).toDouble(), 0.003491952487430488);
        QCOMPARE(data.value(Oe_OUT).toDouble(), 0.0);
        QCOMPARE(data.value(Oe_OUTe).toDouble(), 0.007511181125684738);
        QCOMPARE(data.value(ZG_OUTe).toDouble(), 0.7088763423613439);

        data.setStart(200);
        data.setEnd(300);
        data.setValue(F_Ba, Variant::Root(Variant::Type::Flags));
        data.setValue(F_Bs, Variant::Root(Variant::Type::Flags));
        data.setValue(F_Be, Variant::Root(Variant::Type::Flags));
        data.setValue(Ba1_IN, Variant::Root(10.0));
        data.setValue(Ba2_IN, Variant::Root(20.0));
        data.setValue(Bs1_IN, Variant::Root(30.0));
        data.setValue(Bs2_IN, Variant::Root(40.0));
        data.setValue(Bbs1_IN, Variant::Root(2.0));
        data.setValue(Bbs2_IN, Variant::Root(4.0));
        data.setValue(Be1_IN, Variant::Root(70.0));
        data.setValue(Be2_IN, Variant::Root(80.0));
        filter->process(accum, data);
        QCOMPARE(data.value(L_OUT).toDouble(), 100.0);
        QCOMPARE(data.value(L_OUTe).toDouble(), 200.0);
        QCOMPARE(data.value(Oa_OUT).toDouble(), 0.001441764233862422);
        QCOMPARE(data.value(Oa_OUTe).toDouble(), 0.002883528467724844);
        QCOMPARE(data.value(Os_OUT).toDouble(), 0.003491952487430488);
        QCOMPARE(data.value(Os_OUTe).toDouble(), 0.006983904974860977);
        QCOMPARE(data.value(Oe_OUT).toDouble(), 0.007511181125684738);
        QCOMPARE(data.value(Oe_OUTe).toDouble(), 0.01502236225136948);
        QCOMPARE(data.value(ZG_OUT).toDouble(), 0.7088763423613439);
        QCOMPARE(data.value(ZG_OUTe).toDouble(), 0.7088763423613439);

        data.setStart(300);
        data.setEnd(400);
        {
            data.setValue(Q_IN, Variant::Root(0.6));
            data.setValue(Ff_IN, Variant::Root(2));
            Variant::Root spot;
            spot["Area"].setDouble(10.0);
            data.setValue(ZSPOT_IN, spot);
            filter->process(aux, data);
        }
        data.setValue(F_Ba, Variant::Root(Variant::Type::Flags));
        data.setValue(F_Bs, Variant::Root(Variant::Type::Flags));
        data.setValue(F_Be, Variant::Root(Variant::Type::Flags));
        data.setValue(Ba1_IN, Variant::Root(10.0));
        data.setValue(Ba2_IN, Variant::Root(20.0));
        data.setValue(Bs1_IN, Variant::Root(30.0));
        data.setValue(Bs2_IN, Variant::Root(40.0));
        data.setValue(Bbs1_IN, Variant::Root(2.0));
        data.setValue(Bbs2_IN, Variant::Root(4.0));
        data.setValue(Be1_IN, Variant::Root(70.0));
        data.setValue(Be2_IN, Variant::Root(80.0));
        filter->process(accum, data);
        QCOMPARE(data.value(L_OUT).toDouble(), 0.0);
        QCOMPARE(data.value(L_OUTe).toDouble(), 100.0);
        QCOMPARE(data.value(Oa_OUT).toDouble(), 0.0);
        QCOMPARE(data.value(Oa_OUTe).toDouble(), 0.001441764233862422);
        QCOMPARE(data.value(Os_OUT).toDouble(), 0.0);
        QCOMPARE(data.value(Os_OUTe).toDouble(), 0.003491952487430488);
        QCOMPARE(data.value(Oe_OUT).toDouble(), 0.0);
        QCOMPARE(data.value(Oe_OUTe).toDouble(), 0.007511181125684738);
        QCOMPARE(data.value(ZG_OUTe).toDouble(), 0.7088763423613439);

        data.setValue(F_Ba, Variant::Root(Variant::Flags{"Zero"}));
        data.setValue(F_Bs, Variant::Root(Variant::Flags{"Zero"}));
        data.setValue(F_Be, Variant::Root(Variant::Flags{"Zero"}));
        data.setValue(Ba1_IN, Variant::Root(FP::undefined()));
        data.setValue(Ba2_IN, Variant::Root(FP::undefined()));
        data.setValue(Bs1_IN, Variant::Root(FP::undefined()));
        data.setValue(Bs2_IN, Variant::Root(FP::undefined()));
        data.setValue(Bbs1_IN, Variant::Root(FP::undefined()));
        data.setValue(Bbs2_IN, Variant::Root(FP::undefined()));
        data.setValue(Be1_IN, Variant::Root(FP::undefined()));
        data.setValue(Be2_IN, Variant::Root(FP::undefined()));
        filter->process(accum, data);
        QCOMPARE(data.value(L_OUT).toDouble(), 100.0);
        QCOMPARE(data.value(L_OUTe).toDouble(), 200.0);
        QCOMPARE(data.value(Oa_OUT).toDouble(), 0.001441764233862422);
        QCOMPARE(data.value(Oa_OUTe).toDouble(), 0.001441764233862422);
        QCOMPARE(data.value(Os_OUT).toDouble(), 0.003491952487430488);
        QCOMPARE(data.value(Os_OUTe).toDouble(), 0.003491952487430488);
        QCOMPARE(data.value(Oe_OUT).toDouble(), 0.007511181125684738);
        QCOMPARE(data.value(Oe_OUTe).toDouble(), 0.007511181125684738);
        QCOMPARE(data.value(ZG_OUT).toDouble(), 0.7088763423613439);
        QCOMPARE(data.value(ZG_OUTe).toDouble(), 0.7088763423613439);

        data.setStart(500);
        data.setEnd(600);
        data.setValue(Ba1_IN, Variant::Root(10.0));
        data.setValue(Ba2_IN, Variant::Root(20.0));
        data.setValue(Bs1_IN, Variant::Root(30.0));
        data.setValue(Bs2_IN, Variant::Root(40.0));
        data.setValue(Bbs1_IN, Variant::Root(2.0));
        data.setValue(Bbs2_IN, Variant::Root(4.0));
        data.setValue(Be1_IN, Variant::Root(70.0));
        data.setValue(Be2_IN, Variant::Root(80.0));
        filter->process(accum, data);
        QCOMPARE(data.value(L_OUT).toDouble(), 300.0);
        QCOMPARE(data.value(L_OUTe).toDouble(), 400.0);
        QCOMPARE(data.value(Oa_OUT).toDouble(), 0.002883528467724844);
        QCOMPARE(data.value(Oa_OUTe).toDouble(), 0.004325292701587265);
        QCOMPARE(data.value(Os_OUT).toDouble(), 0.006983904974860976);
        QCOMPARE(data.value(Os_OUTe).toDouble(), 0.01047585746229146);
        QCOMPARE(data.value(Oe_OUT).toDouble(), 0.01502236225136948);
        QCOMPARE(data.value(Oe_OUTe).toDouble(), 0.02253354337705421);
        QCOMPARE(data.value(ZG_OUT).toDouble(), 0.7088763423613439);
        QCOMPARE(data.value(ZG_OUTe).toDouble(), 0.7088763423613439);

        delete filter;
    }

    void predefined()
    {
        ComponentOptions options(component->getOptions());

        SequenceName::Flavors end{"end"};
        SequenceName::Flavors cover{"cover"};

        SequenceName IrG_A11("brw", "raw", "IrG_A11");
        SequenceName BsG_A11("brw", "raw", "BsG_A11");
        SequenceName BbsG_A11("brw", "raw", "BbsG_A11");
        SequenceName BaG_A11("brw", "raw", "BaG_A11");
        SequenceName BeG_A11("brw", "raw", "BeG_A11");
        SequenceName BsG_A11c("brw", "raw", "BsG_A11", cover);
        SequenceName BbsG_A11c("brw", "raw", "BbsG_A11", cover);
        SequenceName BaG_A11c("brw", "raw", "BaG_A11", cover);
        SequenceName BeG_A11c("brw", "raw", "BeG_A11", cover);
        SequenceName L_A11("brw", "raw", "L_A11");
        SequenceName L_A11e("brw", "raw", "L_A11", end);
        SequenceName Ff_A11("brw", "raw", "Ff_A11");
        SequenceName Fn_A11("brw", "raw", "Fn_A11");

        SequenceName OsG_A11("brw", "raw", "OsG_A11");
        SequenceName OsG_A11e("brw", "raw", "OsG_A11", end);
        SequenceName OaG_A11("brw", "raw", "OaG_A11");
        SequenceName OaG_A11e("brw", "raw", "OaG_A11", end);
        SequenceName OeG_A11("brw", "raw", "OeG_A11");
        SequenceName OeG_A11e("brw", "raw", "OeG_A11", end);
        SequenceName ZGG_A11("brw", "raw", "ZGG_A11");
        SequenceName ZGG_A11e("brw", "raw", "ZGG_A11", end);

        QList<SequenceName> input;
        input << IrG_A11 << BsG_A11 << BbsG_A11 << BaG_A11 << BeG_A11;

        SegmentProcessingStage *filter =
                component->createBasicFilterPredefined(options, FP::undefined(), FP::undefined(),
                                                       input);
        QVERIFY(filter != NULL);
        TestController controller;

        QCOMPARE(filter->requestedInputs(),
                 (SequenceName::Set{IrG_A11, BsG_A11, BbsG_A11, BaG_A11, BeG_A11, BsG_A11c,
                                    BbsG_A11c, BaG_A11c, BeG_A11c, L_A11, L_A11e, Ff_A11, Fn_A11}));
        QCOMPARE(filter->predictedOutputs(),
                 (SequenceName::Set{OsG_A11, OsG_A11e, OaG_A11, OaG_A11e, OeG_A11, OeG_A11e,
                                    ZGG_A11, ZGG_A11e}));

        delete filter;
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["A11/Scattering/Input"] = "::Bs[BGR]_S11:=";
        cv["A11/Backscattering/Input"] = "::Bbs[BGR]_S11:=";
        cv["A11/Absorption/Input"] = "::Ba[BGR]_A11:=";
        cv["A11/Extinction/Input"] = "::Be[BGR]_E11:=";
        cv["A11/Filter"] = "::F[fn]_A11:=";
        cv["A11/Calculate/Green/Target"] = "::IrG_A11:=";
        cv["A11/Calculate/Green/Transmittance"] = "::IrG_A11:=";
        cv["A11/Calculate/Green/Length/Start"] = "::L_A11:=";
        cv["A11/Calculate/Green/Length/End"] = "::L_A11:=end";
        cv["A11/Calculate/Green/AsymmetryParameter/Output"] = "::ZGG_A11:=";
        cv["A11/Calculate/Green/AsymmetryParameter/ResetGap"] = 1;
        cv["A11/Calculate/Green/ScatteringOpticalDepth/Output"] = "::OsG_A11:=";
        cv["A11/Calculate/Green/ScatteringOpticalDepth/ResetGap"] = 1;
        cv["A11/Calculate/Green/BackscatteringOpticalDepth/Output"] = "::ObsG_A11:=";
        cv["A11/Calculate/Green/BackscatteringOpticalDepth/ResetGap"] = 1;
        cv["A11/Calculate/Green/AbsorptionOpticalDepth/Output"] = "::OaG_A11:=";
        cv["A11/Calculate/Green/AbsorptionOpticalDepth/ResetGap"] = 1;
        cv["A11/Calculate/Green/ExtinctionOpticalDepth/Output"] = "::OeG_A11:=";
        cv["A11/Calculate/Green/ExtinctionOpticalDepth/ResetGap"] = 1;
        cv["A11/Calculate/Blue/Target"] = "::IrB_A11:=";
        cv["A11/Calculate/Blue/Transmittance"] = "::IrB_A11:=";
        cv["A11/Calculate/Blue/Length/Start"] = "::L_A11:=";
        cv["A11/Calculate/Blue/Length/End"] = "::L_A11:=end";
        cv["A11/Calculate/Blue/AsymmetryParameter/Output"] = "::ZGB_A11:=";
        cv["A11/Calculate/Blue/ScatteringOpticalDepth/Output"] = "::OsB_A11:=";
        cv["A11/Calculate/Blue/BackscatteringOpticalDepth/Output"] = "::ObsB_A11:=";
        cv["A11/Calculate/Blue/AbsorptionOpticalDepth/Output"] = "::OaB_A11:=";
        cv["A11/Calculate/Blue/ExtinctionOpticalDepth/Output"] = "::OeB_A11:=";

        cv["A12/Scattering/Input"] = "::Bs[BGR]_S11:=";
        cv["A12/Backscattering/Input"] = "::Bbs[BGR]_S11:=";
        cv["A12/Absorption/Input"] = "::Ba[BGR]_A11:=";
        cv["A12/Extinction/Input"] = "::Be[BGR]_E11:=";
        cv["A12/Filter"] = "::F[fn]_A12:=";
        cv["A12/Calculate/Green/Target"] = "::IrG_A12:=";
        cv["A12/Calculate/Green/Transmittance"] = "::IrG_A12:=";
        cv["A12/Calculate/Green/Length/Start"] = "::L_A12:=";
        cv["A12/Calculate/Green/Length/End"] = "::L_A12:=end";
        cv["A12/Calculate/Green/AsymmetryParameter/Output"] = "::ZGG_A12:=";
        cv["A12/Calculate/Green/ScatteringOpticalDepth/Output"] = "::OsG_A12:=";
        cv["A12/Calculate/Green/BackscatteringOpticalDepth/Output"] = "::ObsG_A12:=";
        cv["A12/Calculate/Green/AbsorptionOpticalDepth/Output"] = "::OaG_A12:=";
        cv["A12/Calculate/Green/ExtinctionOpticalDepth/Output"] = "::OeG_A12:=";

        config.emplace_back(FP::undefined(), 1000.0, cv);
        cv.write().setEmpty();
        cv["A11/Scattering/Input"] = "::Bs[BGR]_S11:=";
        cv["A11/Backscattering/Input"] = "::Bbs[BGR]_S11:=";
        cv["A11/Absorption/Input"] = "::Ba[BGR]_A11:=";
        cv["A11/Extinction/Input"] = "::Be[BGR]_A11:=";
        cv["A11/Filter"] = "::Ff_A11:=";
        cv["A11/Length/Output"] = "::L_A11:=";
        cv["A11/Length/Delta"] = 50.0;
        cv["A11/Calculate/Green/Wavelength"] = 550.0;
        cv["A11/Calculate/Green/Transmittance"] = "::IrG_A11:=";
        cv["A11/Calculate/Green/Length/Start"] = "::L_A11:=";
        cv["A11/Calculate/Green/Length/End"] = "::L_A11:=end";
        cv["A11/Calculate/Green/AsymmetryParameter/Output"] = "::ZGG_A11:=";
        cv["A11/Calculate/Green/AsymmetryParameter/ResetGap"] = 1;
        cv["A11/Calculate/Green/ScatteringOpticalDepth/Output"] = "::OsG_A11:=";
        cv["A11/Calculate/Green/ScatteringOpticalDepth/ResetGap"] = 1;
        cv["A11/Calculate/Green/BackscatteringOpticalDepth/Output"] = "::ObsG_A11:=";
        cv["A11/Calculate/Green/BackscatteringOpticalDepth/ResetGap"] = 1;
        cv["A11/Calculate/Green/AbsorptionOpticalDepth/Output"] = "::OaG_A11:=";
        cv["A11/Calculate/Green/AbsorptionOpticalDepth/ResetGap"] = 1;
        cv["A11/Calculate/Green/ExtinctionOpticalDepth/Output"] = "::OeG_A11:=";
        cv["A11/Calculate/Green/ExtinctionOpticalDepth/ResetGap"] = 1;
        config.emplace_back(1000.0, FP::undefined(), cv);

        SegmentProcessingStage
                *filter = component->createBasicFilterEditing(100.0, 2000.0, "brw", "raw", config);
        QVERIFY(filter != NULL);
        TestController controller;

        SequenceName::Flavors end{"end"};
        SequenceName::Flavors cover{"cover"};

        SequenceName BsB_S11("brw", "raw", "BsB_S11");
        SequenceName BsG_S11("brw", "raw", "BsG_S11");
        SequenceName BbsB_S11("brw", "raw", "BbsB_S11");
        SequenceName BbsG_S11("brw", "raw", "BbsG_S11");
        SequenceName BaB_A11("brw", "raw", "BaB_A11");
        SequenceName BaG_A11("brw", "raw", "BaG_A11");
        SequenceName BeB_E11("brw", "raw", "BeB_E11");
        SequenceName BeG_E11("brw", "raw", "BeG_E11");
        SequenceName BsB_S11c("brw", "raw", "BsB_S11", cover);
        SequenceName BsG_S11c("brw", "raw", "BsG_S11", cover);
        SequenceName BbsB_S11c("brw", "raw", "BbsB_S11", cover);
        SequenceName BbsG_S11c("brw", "raw", "BbsG_S11", cover);
        SequenceName BaB_A11c("brw", "raw", "BaB_A11", cover);
        SequenceName BaG_A11c("brw", "raw", "BaG_A11", cover);
        SequenceName BeB_E11c("brw", "raw", "BeB_E11", cover);
        SequenceName BeG_E11c("brw", "raw", "BeG_E11", cover);

        SequenceName Ff_A11("brw", "raw", "Ff_A11");
        SequenceName Fn_A11("brw", "raw", "Fn_A11");
        SequenceName L_A11("brw", "raw", "L_A11");
        SequenceName L_A11e("brw", "raw", "L_A11", end);
        SequenceName IrB_A11("brw", "raw", "IrB_A11");
        SequenceName IrG_A11("brw", "raw", "IrG_A11");
        SequenceName OsB_A11("brw", "raw", "OsB_A11");
        SequenceName OsG_A11("brw", "raw", "OsG_A11");
        SequenceName OaB_A11("brw", "raw", "OaB_A11");
        SequenceName OaG_A11("brw", "raw", "OaG_A11");
        SequenceName OeB_A11("brw", "raw", "OeB_A11");
        SequenceName OeG_A11("brw", "raw", "OeG_A11");
        SequenceName ZGB_A11("brw", "raw", "ZGB_A11");
        SequenceName ZGG_A11("brw", "raw", "ZGG_A11");
        SequenceName OsB_A11e("brw", "raw", "OsB_A11", end);
        SequenceName OsG_A11e("brw", "raw", "OsG_A11", end);
        SequenceName OaB_A11e("brw", "raw", "OaB_A11", end);
        SequenceName OaG_A11e("brw", "raw", "OaG_A11", end);
        SequenceName OeB_A11e("brw", "raw", "OeB_A11", end);
        SequenceName OeG_A11e("brw", "raw", "OeG_A11", end);
        SequenceName ZGB_A11e("brw", "raw", "ZGB_A11", end);
        SequenceName ZGG_A11e("brw", "raw", "ZGG_A11", end);

        SequenceName Ff_A12("brw", "raw", "Ff_A12");
        SequenceName L_A12("brw", "raw", "L_A12");
        SequenceName L_A12e("brw", "raw", "L_A12", end);
        SequenceName IrG_A12("brw", "raw", "IrG_A12");
        SequenceName OsG_A12("brw", "raw", "OsG_A12");
        SequenceName OaG_A12("brw", "raw", "OaG_A12");
        SequenceName OeG_A12("brw", "raw", "OeG_A12");
        SequenceName ZGG_A12("brw", "raw", "ZGG_A12");
        SequenceName OsG_A12e("brw", "raw", "OsG_A12", end);
        SequenceName OaG_A12e("brw", "raw", "OaG_A12", end);
        SequenceName OeG_A12e("brw", "raw", "OeG_A12", end);
        SequenceName ZGG_A12e("brw", "raw", "ZGG_A12", end);

        QCOMPARE(filter->requestedInputs(),
                 (SequenceName::Set{L_A11, IrB_A11, IrG_A11, Ff_A11, L_A12, IrG_A12}));
        QCOMPARE(filter->predictedOutputs(),
                 (SequenceName::Set{OsB_A11, OsB_A11e, OaB_A11, OaB_A11e, OeB_A11, OeB_A11e,
                                    ZGB_A11, ZGB_A11e, OsG_A11, OsG_A11e, OaG_A11, OaG_A11e,
                                    OeG_A11, OeG_A11e, ZGG_A11, ZGG_A11e, L_A11, L_A11e, OsG_A12,
                                    OsG_A12e, OaG_A12, OaG_A12e, OeG_A12, OeG_A12e, ZGG_A12,
                                    ZGG_A12e}));

        filter->unhandled(BsB_S11, &controller);
        filter->unhandled(BsG_S11, &controller);
        filter->unhandled(BbsB_S11, &controller);
        filter->unhandled(BbsG_S11, &controller);
        filter->unhandled(BaB_A11, &controller);
        filter->unhandled(BaG_A11, &controller);
        filter->unhandled(BeB_E11, &controller);
        filter->unhandled(BeG_E11, &controller);
        filter->unhandled(IrB_A11, &controller);
        filter->unhandled(IrG_A11, &controller);
        filter->unhandled(L_A11, &controller);
        filter->unhandled(L_A11e, &controller);
        filter->unhandled(Ff_A11, &controller);
        filter->unhandled(Fn_A11, &controller);

        QVERIFY(controller.contents.size() >= 2);

        QList<int> idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                                          L_A11 <<
                                                                                          L_A11e <<
                                                                                          OsB_A11 <<
                                                                                          OsB_A11e <<
                                                                                          OaB_A11 <<
                                                                                          OaB_A11e <<
                                                                                          OeB_A11 <<
                                                                    OeB_A11e <<
                                                                    ZGB_A11 <<
                                                                    ZGB_A11e <<
                                                                    OsG_A11 <<
                                                                                          OsG_A11e <<
                                                                                          OaG_A11 <<
                                                                                          OaG_A11e <<
                                                                                          OeG_A11 <<
                                                                                          OeG_A11e <<
                                                                                          ZGG_A11 <<
                                                                                          ZGG_A11e,
                                                                     QSet<SequenceName>() <<
                                                                                          BsB_S11 <<
                                                                                          BsG_S11 <<
                                                                                          BbsB_S11 <<
                                                                                          BbsG_S11 <<
                                                                                          BaB_A11 <<
                                                                                          BaG_A11 <<
                                                                                          BeB_E11 <<
                                                                    BeG_E11 <<
                                                                    BsB_S11c <<
                                                                    BsG_S11c <<
                                                                    BbsB_S11c <<
                                                                                          BbsG_S11c <<
                                                                                          BaB_A11c <<
                                                                                          BaG_A11c <<
                                                                                          BeB_E11c <<
                                                                                          BeG_E11c));
        QCOMPARE(idL.size(), 1);
        int A11_accum = idL.at(0);

        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>(), QSet<SequenceName>() <<
                                                                                            IrG_A11 <<
                                                                                            IrB_A11 <<
                                                                                            Ff_A11 <<
                                                                                            Fn_A11));
        QCOMPARE(idL.size(), 1);
        int A11_aux = idL.at(0);

        QVERIFY(A11_aux < A11_accum);
        QCOMPARE(filter->metadataBreaks(A11_accum), QSet<double>() << 1000.0);

        filter->unhandled(IrG_A12, &controller);
        filter->unhandled(L_A12, &controller);
        filter->unhandled(L_A12e, &controller);
        filter->unhandled(Ff_A12, &controller);

        QVERIFY(controller.contents.size() >= 4);

        idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                               OsG_A12 <<
                                                                               OsG_A12e <<
                                                                               OaG_A12 <<
                                                                               OaG_A12e <<
                                                                               OeG_A12 <<
                                                                               OeG_A12e <<
                                                                               ZGG_A12 << ZGG_A12e,
                                                          QSet<SequenceName>() <<
                                                                               L_A12 <<
                                                                               L_A12e <<
                                                                               BsB_S11 <<
                                                                               BsG_S11 <<
                                                                               BbsB_S11 <<
                                                                               BbsG_S11 <<
                                                                               BaB_A11 <<
                                                                               BaG_A11 <<
                                                         BeB_E11 <<
                                                         BeG_E11 <<
                                                         BsB_S11c <<
                                                         BsG_S11c <<
                                                                               BbsB_S11c <<
                                                                               BbsG_S11c <<
                                                                               BaB_A11c <<
                                                                               BaG_A11c <<
                                                                               BeB_E11c <<
                                                                               BeG_E11c));
        QCOMPARE(idL.size(), 1);
        int A12_accum = idL.at(0);

        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>(),
                                                 QSet<SequenceName>() << IrG_A12 << Ff_A12));
        QCOMPARE(idL.size(), 1);
        int A12_aux = idL.at(0);

        QVERIFY(A12_aux < A12_accum);
        QCOMPARE(filter->metadataBreaks(A12_accum), QSet<double>() << 1000.0);

        SequenceSegment data;
        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(400.0);
            data.setValue(IrB_A11.toMeta(), meta);
            data.setValue(BaB_A11.toMeta(), meta);
            data.setValue(BsB_S11.toMeta(), meta);
            data.setValue(BbsB_S11.toMeta(), meta);
            data.setValue(BeB_E11.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(550.0);
            data.setValue(IrG_A11.toMeta(), meta);
            data.setValue(IrG_A12.toMeta(), meta);
            data.setValue(BaG_A11.toMeta(), meta);
            data.setValue(BsG_S11.toMeta(), meta);
            data.setValue(BbsG_S11.toMeta(), meta);
            data.setValue(BeG_E11.toMeta(), meta);

            filter->processMeta(A11_aux, data);
            filter->processMeta(A11_accum, data);
            QCOMPARE(data.value(OsB_A11.toMeta()).metadata("Wavelength").toDouble(), 400.0);

            filter->processMeta(A12_aux, data);
            filter->processMeta(A12_accum, data);
            QCOMPARE(data.value(OsG_A12.toMeta()).metadata("Wavelength").toDouble(), 550.0);
        }

        data.setStart(100.0);
        data.setEnd(200.0);
        data.setValue(BaB_A11, Variant::Root(10.0));
        data.setValue(BaG_A11, Variant::Root(8.0));
        data.setValue(BsB_S11, Variant::Root(20.0));
        data.setValue(BsG_S11, Variant::Root(19.0));
        data.setValue(BbsB_S11, Variant::Root(3.0));
        data.setValue(BbsG_S11, Variant::Root(2.0));
        data.setValue(BeB_E11, Variant::Root(30.0));
        data.setValue(BeG_E11, Variant::Root(25.0));
        data.setValue(IrB_A11, Variant::Root(0.7));
        data.setValue(IrG_A11, Variant::Root(0.8));
        data.setValue(L_A11, Variant::Root(100.0));
        data.setValue(L_A11e, Variant::Root(200.0));
        filter->process(A11_aux, data);
        filter->process(A11_accum, data);
        QCOMPARE(data.value(L_A11).toDouble(), 100.0);
        QCOMPARE(data.value(L_A11e).toDouble(), 200.0);
        QCOMPARE(data.value(OsB_A11).toDouble(), 0.0);
        QCOMPARE(data.value(OsB_A11e).toDouble(), 0.002);
        QCOMPARE(data.value(OsG_A11).toDouble(), 0.0);
        QCOMPARE(data.value(OsG_A11e).toDouble(), 0.0019);
        QCOMPARE(data.value(OaB_A11).toDouble(), 0.0);
        QCOMPARE(data.value(OaB_A11e).toDouble(), 0.001);
        QCOMPARE(data.value(OaG_A11).toDouble(), 0.0);
        QCOMPARE(data.value(OaG_A11e).toDouble(), 0.0008);
        QCOMPARE(data.value(OeB_A11).toDouble(), 0.0);
        QCOMPARE(data.value(OeB_A11e).toDouble(), 0.003);
        QCOMPARE(data.value(OeG_A11).toDouble(), 0.0);
        QCOMPARE(data.value(OeG_A11e).toDouble(), 0.0025);
        QCOMPARE(data.value(ZGB_A11e).toDouble(), 0.5385983375);
        QCOMPARE(data.value(ZGG_A11e).toDouble(), 0.646454687272197);

        data.setValue(IrG_A12, Variant::Root(1.0));
        data.setValue(L_A12, Variant::Root(100.0));
        data.setValue(L_A12e, Variant::Root(300.0));
        filter->process(A12_aux, data);
        filter->process(A12_accum, data);
        QCOMPARE(data.value(L_A12).toDouble(), 100.0);
        QCOMPARE(data.value(L_A12e).toDouble(), 300.0);
        QCOMPARE(data.value(OsG_A12).toDouble(), 0.0);
        QCOMPARE(data.value(OsG_A12e).toDouble(), 0.0038);
        QCOMPARE(data.value(OaG_A12).toDouble(), 0.0);
        QCOMPARE(data.value(OaG_A12e).toDouble(), 0.0016);
        QCOMPARE(data.value(OeG_A12).toDouble(), 0.0);
        QCOMPARE(data.value(OeG_A12e).toDouble(), 0.005);
        QCOMPARE(data.value(ZGG_A12e).toDouble(), 0.646454687272197);


        data.setStart(1000.0);
        data.setEnd(2000.0);
        data.setValue(L_A11, Variant::Root());
        data.setValue(L_A11e, Variant::Root());
        data.setValue(OsB_A11, Variant::Root());
        data.setValue(OsB_A11e, Variant::Root());
        data.setValue(OaB_A11, Variant::Root());
        data.setValue(OaB_A11e, Variant::Root());
        data.setValue(OeB_A11, Variant::Root());
        data.setValue(OeB_A11e, Variant::Root());
        data.setValue(ZGB_A11, Variant::Root());
        data.setValue(ZGB_A11e, Variant::Root());
        filter->process(A11_aux, data);
        filter->process(A11_accum, data);
        QCOMPARE(data.value(L_A11).toDouble(), 0.0);
        QCOMPARE(data.value(L_A11e).toDouble(), 50.0);
        QCOMPARE(data.value(OsG_A11).toDouble(), 0.0);
        QCOMPARE(data.value(OsG_A11e).toDouble(), 0.00095);
        QCOMPARE(data.value(OaG_A11).toDouble(), 0.0);
        QCOMPARE(data.value(OaG_A11e).toDouble(), 0.0004);
        QCOMPARE(data.value(OeG_A11).toDouble(), 0.0);
        QCOMPARE(data.value(OeG_A11e).toDouble(), 0.00125);
        QCOMPARE(data.value(ZGG_A11e).toDouble(), 0.646454687272197);
        QVERIFY(!data.value(OsB_A11).exists());
        QVERIFY(!data.value(OaB_A11).exists());
        QVERIFY(!data.value(OeB_A11).exists());
        QVERIFY(!data.value(ZGB_A11).exists());

        delete filter;
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
