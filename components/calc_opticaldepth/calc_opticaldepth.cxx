/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <memory>
#include <QRegExp>
#include <datacore/archive/access.hxx>

#include "core/timeutils.hxx"
#include "core/environment.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/variant/composite.hxx"

#include "calc_opticaldepth.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Editing;
using namespace CPD3::Smoothing;

/* The auxiliary ones have the lower ID so they get update first, so that
 * wavelengths are available */
static inline int toMain(int input)
{ return (input << 1) | 1; }

static inline int toAux(int input)
{ return (input << 1) | 0; }

static bool isMain(int input)
{ return (input & 1) == 1; }

static bool isAux(int input)
{ return (input & 1) == 0; }

static int toID(int input)
{ return input >> 1; }

void CalcOpticalDepth::handleOptions(const ComponentOptions &options)
{
    if (options.isSet("transmittance")) {
        defaultTransmittance = qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("transmittance"))->getOperator();
    } else {
        defaultTransmittance = NULL;
    }

    if (options.isSet("length-start")) {
        defaultLengthStart =
                qobject_cast<DynamicInputOption *>(options.get("length-start"))->getInput();
    } else {
        defaultLengthStart = NULL;
    }

    if (options.isSet("length-end")) {
        defaultLengthEnd =
                qobject_cast<DynamicInputOption *>(options.get("length-end"))->getInput();
    } else {
        defaultLengthEnd = NULL;
    }

    if (options.isSet("delta-l")) {
        defaultDeltaLength = qobject_cast<DynamicInputOption *>(options.get("delta-l"))->getInput();
    } else {
        defaultDeltaLength = NULL;
    }

    if (options.isSet("synchronous-zero")) {
        defaultSynchronousZero =
                qobject_cast<DynamicBoolOption *>(options.get("synchronous-zero"))->getInput();
    } else {
        defaultSynchronousZero = NULL;
    }

    if (options.isSet("reset-gap")) {
        defaultResetGap = qobject_cast<TimeIntervalSelectionOption *>(
                options.get("reset-gap"))->getInterval();
    } else {
        defaultResetGap = NULL;
    }

    if (options.isSet("scattering")) {
        defaultScattering = WavelengthAdjust::fromInput(
                qobject_cast<DynamicSequenceSelectionOption *>(
                        options.get("scattering"))->getOperator());
    } else {
        defaultScattering = NULL;
    }
    if (options.isSet("backscattering")) {
        defaultBackscattering = WavelengthAdjust::fromInput(
                qobject_cast<DynamicSequenceSelectionOption *>(
                        options.get("backscattering"))->getOperator());
    } else {
        defaultBackscattering = NULL;
    }
    if (options.isSet("absorption")) {
        defaultAbsorption = WavelengthAdjust::fromInput(
                qobject_cast<DynamicSequenceSelectionOption *>(
                        options.get("absorption"))->getOperator());
    } else {
        defaultAbsorption = NULL;
    }
    if (options.isSet("extinction")) {
        defaultExtiniction = WavelengthAdjust::fromInput(
                qobject_cast<DynamicSequenceSelectionOption *>(
                        options.get("extinction"))->getOperator());
    } else {
        defaultExtiniction = NULL;
    }

    if (options.isSet("scattering-suffix")) {
        scatteringSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("scattering-suffix"))
                        ->get());
    }
    if (options.isSet("absorption-suffix")) {
        absorptionSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("absorption-suffix"))
                        ->get());
    }
    if (options.isSet("extinction-suffix")) {
        extinctionSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("extinction-suffix"))
                        ->get());
    }
    if (options.isSet("suffix")) {
        filterSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->get());
    }

    if (options.isSet("calculate-l")) {
        defaultCalculateL =
                qobject_cast<ComponentOptionBoolean *>(options.get("calculate-l"))->get();
    } else {
        defaultCalculateL = false;
    }

    restrictedInputs = false;
}


CalcOpticalDepth::CalcOpticalDepth()
{ Q_ASSERT(false); }

CalcOpticalDepth::CalcOpticalDepth(const ComponentOptions &options)
{
    handleOptions(options);
}

CalcOpticalDepth::CalcOpticalDepth(const ComponentOptions &options,
                                   double start,
                                   double end,
                                   const QList<SequenceName> &inputs)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    handleOptions(options);

    for (QList<SequenceName>::const_iterator unit = inputs.constBegin(), endU = inputs.constEnd();
            unit != endU;
            ++unit) {
        CalcOpticalDepth::unhandled(*unit, NULL);
    }
}

CalcOpticalDepth::CalcOpticalDepth(double start,
                                   double end,
                                   const SequenceName::Component &station,
                                   const SequenceName::Component &archive,
                                   const ValueSegment::Transfer &config)
{
    defaultSynchronousZero = NULL;
    defaultResetGap = NULL;
    defaultScattering = NULL;
    defaultBackscattering = NULL;
    defaultAbsorption = NULL;
    defaultExtiniction = NULL;
    defaultTransmittance = NULL;
    defaultLengthStart = NULL;
    defaultLengthEnd = NULL;
    defaultDeltaLength = NULL;
    defaultCalculateL = false;
    restrictedInputs = true;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    for (const auto &child : children) {
        Processing p;

        p.inputScattering = WavelengthAdjust::fromConfiguration(config,
                                                                QString("%1/Scattering").arg(
                                                                        QString::fromStdString(
                                                                                child)), start,
                                                                end);
        p.inputBackscattering = WavelengthAdjust::fromConfiguration(config,
                                                                    QString("%1/Backscattering").arg(
                                                                            QString::fromStdString(
                                                                                    child)), start,
                                                                    end);
        p.inputAbsorption = WavelengthAdjust::fromConfiguration(config,
                                                                QString("%1/Absorption").arg(
                                                                        QString::fromStdString(
                                                                                child)), start,
                                                                end);
        p.inputExtinction = WavelengthAdjust::fromConfiguration(config,
                                                                QString("%1/Extinction").arg(
                                                                        QString::fromStdString(
                                                                                child)), start,
                                                                end);
        p.inputScattering->registerExpected(station, archive);
        p.inputBackscattering->registerExpected(station, archive);
        p.inputAbsorption->registerExpected(station, archive);
        p.inputExtinction->registerExpected(station, archive);

        p.inputDeltaLength = DynamicInput::fromConfiguration(config, QString("%1/Length/Delta").arg(
                QString::fromStdString(child)), start, end);
        p.length.resetGap = DynamicTimeInterval::fromConfiguration(config,
                                                                   QString("%1/Length/ResetGap").arg(
                                                                           QString::fromStdString(
                                                                                   child)), start,
                                                                   end);
        p.length.output = DynamicSequenceSelection::fromConfiguration(config,
                                                                      QString("%1/Length/Output").arg(
                                                                              QString::fromStdString(
                                                                                      child)),
                                                                      start, end);
        p.inputDeltaLength->registerExpected(station, archive);
        p.length.output->registerExpected(station, archive);

        p.filterChange = DynamicSequenceSelection::fromConfiguration(config,
                                                                     QString("%1/Filter").arg(
                                                                             QString::fromStdString(
                                                                                     child)), start,
                                                                     end);
        p.spotData = DynamicSequenceSelection::fromConfiguration(config, QString("%1/Spot").arg(
                QString::fromStdString(child)), start, end);
        p.flowData = DynamicSequenceSelection::fromConfiguration(config, QString("%1/Flow").arg(
                QString::fromStdString(child)), start, end);
        p.filterChange->registerExpected(station, archive);
        p.spotData->registerExpected(station, archive);
        p.flowData->registerExpected(station, archive);

        std::unordered_set<Variant::PathElement::HashIndex> operateChildren;
        for (const auto &add : config) {
            Util::merge(
                    add[QString("%1/Calculate").arg(QString::fromStdString(child))].toHash().keys(),
                    operateChildren);
        }
        operateChildren.erase(Variant::PathElement::HashIndex());

        for (const auto &operate : operateChildren) {
            Processing::OpticalWavelength o;

            o.tracking = DynamicSequenceSelection::fromConfiguration(config,
                                                                     QString("%1/Calculate/%2/Target")
                                                                             .arg(QString::fromStdString(
                                                                                     child),
                                                                                  QString::fromStdString(
                                                                                          operate)),
                                                                     start, end);
            o.wavelength = DynamicDoubleOption::fromConfiguration(config,
                                                                  QString("%1/Calculate/%2/Wavelength")
                                                                          .arg(QString::fromStdString(
                                                                                  child),
                                                                               QString::fromStdString(
                                                                                       operate)),
                                                                  start, end);

            o.transmittanceData = DynamicSequenceSelection::fromConfiguration(config,
                                                                              QString("%1/Calculate/%2/Transmittance")
                                                                                      .arg(QString::fromStdString(
                                                                                              child),
                                                                                           QString::fromStdString(
                                                                                                   operate)),
                                                                              start, end);

            o.lengthStart = DynamicInput::fromConfiguration(config,
                                                            QString("%1/Calculate/%2/Length/Start").arg(
                                                                    QString::fromStdString(child),
                                                                    QString::fromStdString(
                                                                            operate)), start, end);
            o.lengthEnd = DynamicInput::fromConfiguration(config,
                                                          QString("%1/Calculate/%2/Length/End").arg(
                                                                  QString::fromStdString(child),
                                                                  QString::fromStdString(operate)),
                                                          start, end);
            o.deltaLength = DynamicInput::fromConfiguration(config,
                                                            QString("%1/Calculate/%2/Length/Delta").arg(
                                                                    QString::fromStdString(child),
                                                                    QString::fromStdString(
                                                                            operate)), start, end);

            o.outputAsymmetry = DynamicSequenceSelection::fromConfiguration(config,
                                                                            QString("%1/Calculate/%2/AsymmetryParameter/Output")
                                                                                    .arg(QString::fromStdString(
                                                                                            child),
                                                                                         QString::fromStdString(
                                                                                                 operate)),
                                                                            start, end);

            o.tracking->registerExpected(station, archive);
            o.transmittanceData->registerExpected(station, archive);
            o.lengthStart->registerExpected(station, archive);
            o.lengthEnd->registerExpected(station, archive);
            o.deltaLength->registerExpected(station, archive);
            o.outputAsymmetry->registerExpected(station, archive);

            o.scattering.synchronousZero = DynamicBoolOption::fromConfiguration(config,
                                                                                QString("%1/Calculate/%2/ScatteringOpticalDepth/SynchronousZero")
                                                                                        .arg(QString::fromStdString(
                                                                                                child),
                                                                                             QString::fromStdString(
                                                                                                     operate)),
                                                                                start, end);
            o.scattering.resetGap = DynamicTimeInterval::fromConfiguration(config,
                                                                           QString("%1/Calculate/%2/ScatteringOpticalDepth/ResetGap")
                                                                                   .arg(QString::fromStdString(
                                                                                           child),
                                                                                        QString::fromStdString(
                                                                                                operate)),
                                                                           start, end);
            o.scattering.flags = DynamicSequenceSelection::fromConfiguration(config,
                                                                             QString("%1/Calculate/%2/ScatteringOpticalDepth/Flags")
                                                                                     .arg(QString::fromStdString(
                                                                                             child),
                                                                                          QString::fromStdString(
                                                                                                  operate)),
                                                                             start, end);
            o.scattering.output = DynamicSequenceSelection::fromConfiguration(config,
                                                                              QString("%1/Calculate/%2/ScatteringOpticalDepth/Output")
                                                                                      .arg(QString::fromStdString(
                                                                                              child),
                                                                                           QString::fromStdString(
                                                                                                   operate)),
                                                                              start, end);
            o.scattering.output->registerExpected(station, archive);
            o.scattering.flags->registerExpected(station, archive);

            o.backScattering.synchronousZero = DynamicBoolOption::fromConfiguration(config,
                                                                                    QString("%1/Calculate/%2/BackscatteringOpticalDepth/SynchronousZero")
                                                                                            .arg(QString::fromStdString(
                                                                                                    child),
                                                                                                 QString::fromStdString(
                                                                                                         operate)),
                                                                                    start, end);
            o.backScattering.resetGap = DynamicTimeInterval::fromConfiguration(config,
                                                                               QString("%1/Calculate/%2/BackscatteringOpticalDepth/ResetGap")
                                                                                       .arg(QString::fromStdString(
                                                                                               child),
                                                                                            QString::fromStdString(
                                                                                                    operate)),
                                                                               start, end);
            o.backScattering.flags = DynamicSequenceSelection::fromConfiguration(config,
                                                                                 QString("%1/Calculate/%2/BackscatteringOpticalDepth/Flags")
                                                                                         .arg(QString::fromStdString(
                                                                                                 child),
                                                                                              QString::fromStdString(
                                                                                                      operate)),
                                                                                 start, end);
            o.backScattering.output = DynamicSequenceSelection::fromConfiguration(config,
                                                                                  QString("%1/Calculate/%2/BackscatteringOpticalDepth/Output")
                                                                                          .arg(QString::fromStdString(
                                                                                                  child),
                                                                                               QString::fromStdString(
                                                                                                       operate)),
                                                                                  start, end);
            o.backScattering.output->registerExpected(station, archive);
            o.backScattering.flags->registerExpected(station, archive);

            o.absorption.synchronousZero = DynamicBoolOption::fromConfiguration(config,
                                                                                QString("%1/Calculate/%2/AbsorptionOpticalDepth/SynchronousZero")
                                                                                        .arg(QString::fromStdString(
                                                                                                child),
                                                                                             QString::fromStdString(
                                                                                                     operate)),
                                                                                start, end);
            o.absorption.resetGap = DynamicTimeInterval::fromConfiguration(config,
                                                                           QString("%1/Calculate/%2/AbsorptionOpticalDepth/ResetGap")
                                                                                   .arg(QString::fromStdString(
                                                                                           child),
                                                                                        QString::fromStdString(
                                                                                                operate)),
                                                                           start, end);
            o.absorption.flags = DynamicSequenceSelection::fromConfiguration(config,
                                                                             QString("%1/Calculate/%2/AbsorptionOpticalDepth/Flags")
                                                                                     .arg(QString::fromStdString(
                                                                                             child),
                                                                                          QString::fromStdString(
                                                                                                  operate)),
                                                                             start, end);
            o.absorption.output = DynamicSequenceSelection::fromConfiguration(config,
                                                                              QString("%1/Calculate/%2/AbsorptionOpticalDepth/Output")
                                                                                      .arg(QString::fromStdString(
                                                                                              child),
                                                                                           QString::fromStdString(
                                                                                                   operate)),
                                                                              start, end);
            o.absorption.output->registerExpected(station, archive);
            o.absorption.flags->registerExpected(station, archive);

            o.extinction.synchronousZero = DynamicBoolOption::fromConfiguration(config,
                                                                                QString("%1/Calculate/%2/ExtinctionOpticalDepth/SynchronousZero")
                                                                                        .arg(QString::fromStdString(
                                                                                                child),
                                                                                             QString::fromStdString(
                                                                                                     operate)),
                                                                                start, end);
            o.extinction.resetGap = DynamicTimeInterval::fromConfiguration(config,
                                                                           QString("%1/Calculate/%2/ExtinctionOpticalDepth/ResetGap")
                                                                                   .arg(QString::fromStdString(
                                                                                           child),
                                                                                        QString::fromStdString(
                                                                                                operate)),
                                                                           start, end);
            o.extinction.flags = DynamicSequenceSelection::fromConfiguration(config,
                                                                             QString("%1/Calculate/%2/ExtinctionOpticalDepth/Flags")
                                                                                     .arg(QString::fromStdString(
                                                                                             child),
                                                                                          QString::fromStdString(
                                                                                                  operate)),
                                                                             start, end);
            o.extinction.output = DynamicSequenceSelection::fromConfiguration(config,
                                                                              QString("%1/Calculate/%2/ExtinctionOpticalDepth/Output")
                                                                                      .arg(QString::fromStdString(
                                                                                              child),
                                                                                           QString::fromStdString(
                                                                                                   operate)),
                                                                              start, end);
            o.extinction.output->registerExpected(station, archive);
            o.extinction.flags->registerExpected(station, archive);


            p.optical.push_back(o);
        }

        processing.push_back(p);
    }
}

template<typename ConditionEvaluate>
static bool extendToCondition(double &start,
                              double &end,
                              const SequenceName::Component &station,
                              const SequenceName::Component &archive,
                              const std::vector<
                                      std::unique_ptr<DynamicSequenceSelection>> &operators,
                              Archive::Access *access,
                              ConditionEvaluate cond)
{
    Q_UNUSED(end);

    if (!FP::defined(start))
        return false;

    std::unordered_set<SequenceName> names;
    for (const auto &op : operators) {
        auto add = op->getAllUnits();
        names.insert(add.begin(), add.end());
    }

    double readBegin = start - 86400.0;
    double readEnd = start + 1;
    for (int t = 0; t < 14; t++, readEnd = readBegin + 1, readBegin -= 86400.0) {
        Archive::Selection::List selections;
        for (const auto &add : names) {
            Archive::Selection sel(add, readBegin, readEnd);
            sel.includeMetaArchive = false;
            selections.push_back(sel);
        }
        if (selections.empty())
            return false;

        auto segments = SequenceSegment::Stream::read(selections, access);
        for (int i = segments.size() - 2; i >= 0; i--) {
            SequenceSegment &prior = segments[i];
            SequenceSegment &current = segments[i + 1];

            for (const auto &op : operators) {
                for (const auto &check : op->get(prior.getStart(), current.getEnd())) {
                    if (cond(prior[check], current[check])) {
                        if (current.getStart() < start)
                            start = current.getStart();
                        return true;
                    }
                }
            }
        }
    }

    return false;
}

static bool filterChangedCondition(const Variant::Read &prior, const Variant::Read &current)
{
    if (Variant::Composite::isDefined(prior)) {
        if (!Variant::Composite::isDefined(current))
            return false;
        return prior != current;
    } else {
        if (Variant::Composite::isDefined(current))
            return true;
    }
    return false;
}

static bool extendToFilter(double &start,
                           double &end,
                           const SequenceName::Component &station,
                           const SequenceName::Component &archive,
                           const ValueSegment::Transfer &config,
                           Archive::Access *access)
{
    if (!FP::defined(start))
        return false;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    std::vector<std::unique_ptr<DynamicSequenceSelection>> operators;
    for (const auto &child : children) {
        operators.emplace_back(DynamicSequenceSelection::fromConfiguration(config,
                                                                           QString("%1/Filter").arg(
                                                                                   QString::fromStdString(
                                                                                           child)),
                                                                           start, end));
    }

    return extendToCondition(start, end, station, archive, operators, access,
                             filterChangedCondition);
}

static bool lengthFilterChange(double dL)
{
    if (!FP::defined(dL))
        return false;
    return dL < -200.0;
}

static bool lengthFilterChange(double prior, double current)
{
    if (!FP::defined(prior) || !FP::defined(current))
        return false;
    return lengthFilterChange(current - prior);
}

static bool transmittanceFilterChange(double prior, double current)
{
    if (!FP::defined(prior))
        return false;
    if (!FP::defined(current))
        return false;
    if (fabs(current - 1.0) > 0.05)
        return false;
    return (current - prior) > 0.1;
}

static bool transmittanceFilterChangeCondition(const Variant::Read &prior,
                                               const Variant::Read &current)
{
    return transmittanceFilterChange(prior.toDouble(), current.toDouble());
}

static bool extendToTransmittance(double &start,
                                  double &end,
                                  const SequenceName::Component &station,
                                  const SequenceName::Component &archive,
                                  const ValueSegment::Transfer &config,
                                  Archive::Access *access)
{
    if (!FP::defined(start))
        return false;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    std::vector<std::unique_ptr<DynamicSequenceSelection>> operators;
    for (const auto &child : children) {
        std::unordered_set<Variant::PathElement::HashIndex> operateChildren;
        for (const auto &add : config) {
            Util::merge(
                    add[QString("%1/Calculate").arg(QString::fromStdString(child))].toHash().keys(),
                    operateChildren);
        }
        operateChildren.erase(Variant::PathElement::HashIndex());

        for (const auto &operate : operateChildren) {
            operators.emplace_back(DynamicSequenceSelection::fromConfiguration(config,
                                                                               QString("%1/Calculate/%2/Transmittance")
                                                                                       .arg(QString::fromStdString(
                                                                                               child),
                                                                                            QString::fromStdString(
                                                                                                    operate)),
                                                                               start, end));
            operators.back()->registerExpected(station, archive);
        }
    }

    return extendToCondition(start, end, station, archive, operators, access,
                             transmittanceFilterChangeCondition);
}

static bool lengthFilterChangeCondition(const Variant::Read &prior, const Variant::Read &current)
{
    return lengthFilterChange(prior.toDouble(), current.toDouble());
}

static bool extendToLength(double &start,
                           double &end,
                           const SequenceName::Component &station,
                           const SequenceName::Component &archive,
                           const ValueSegment::Transfer &config,
                           Archive::Access *access)
{
    if (!FP::defined(start))
        return false;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    std::vector<std::unique_ptr<DynamicSequenceSelection>> operators;
    for (const auto &child : children) {
        std::unordered_set<Variant::PathElement::HashIndex> operateChildren;
        for (const auto &add : config) {
            Util::merge(
                    add[QString("%1/Calculate").arg(QString::fromStdString(child))].toHash().keys(),
                    operateChildren);
        }
        operateChildren.erase(Variant::PathElement::HashIndex());

        for (const auto &operate : operateChildren) {
            operators.emplace_back(DynamicSequenceSelection::fromConfiguration(config,
                                                                               QString("%1/Calculate/%2/Length/Start")
                                                                                       .arg(QString::fromStdString(
                                                                                               child),
                                                                                            QString::fromStdString(
                                                                                                    operate)),
                                                                               start, end));
            operators.back()->registerExpected(station, archive);
        }
    }

    return extendToCondition(start, end, station, archive, operators, access,
                             lengthFilterChangeCondition);
}

void CalcOpticalDepthComponent::extendBasicFilterEditing(double &start,
                                                         double &end,
                                                         const SequenceName::Component &station,
                                                         const SequenceName::Component &archive,
                                                         const ValueSegment::Transfer &config,
                                                         Archive::Access *access)
{
    std::unique_ptr<Archive::Access> localAccess;
    if (!access) {
        localAccess.reset(new Archive::Access);
        access = localAccess.get();
    }
    Archive::Access::ReadLock lock(*access);

    if (extendToFilter(start, end, station, archive, config, access))
        return;
    if (extendToLength(start, end, station, archive, config, access))
        return;
    if (extendToTransmittance(start, end, station, archive, config, access))
        return;
}


CalcOpticalDepth::~CalcOpticalDepth()
{
    if (defaultSynchronousZero != NULL)
        delete defaultSynchronousZero;
    if (defaultResetGap != NULL)
        delete defaultResetGap;
    if (defaultScattering != NULL)
        delete defaultScattering;
    if (defaultBackscattering != NULL)
        delete defaultBackscattering;
    if (defaultAbsorption != NULL)
        delete defaultAbsorption;
    if (defaultExtiniction != NULL)
        delete defaultExtiniction;
    if (defaultTransmittance != NULL)
        delete defaultTransmittance;
    if (defaultLengthStart != NULL)
        delete defaultLengthStart;
    if (defaultLengthEnd != NULL)
        delete defaultLengthEnd;
    if (defaultDeltaLength != NULL)
        delete defaultDeltaLength;

    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        if (p->inputScattering != NULL)
            delete p->inputScattering;
        if (p->inputBackscattering != NULL)
            delete p->inputBackscattering;
        if (p->inputAbsorption != NULL)
            delete p->inputAbsorption;
        if (p->inputExtinction != NULL)
            delete p->inputExtinction;

        delete p->inputDeltaLength;
        delete p->filterChange;
        delete p->spotData;
        delete p->flowData;

        Q_ASSERT(p->length.synchronousZero == NULL);
        delete p->length.resetGap;
        Q_ASSERT(p->length.flags == NULL);
        delete p->length.output;

        for (std::vector<Processing::OpticalWavelength>::const_iterator o = p->optical.begin(),
                endO = p->optical.end(); o != endO; ++o) {
            delete o->tracking;
            delete o->wavelength;
            delete o->transmittanceData;
            delete o->lengthStart;
            delete o->lengthEnd;
            delete o->deltaLength;
            delete o->outputAsymmetry;

            delete o->scattering.synchronousZero;
            delete o->scattering.resetGap;
            if (o->scattering.flags != NULL)
                delete o->scattering.flags;
            delete o->scattering.output;

            delete o->backScattering.synchronousZero;
            delete o->backScattering.resetGap;
            if (o->backScattering.flags != NULL)
                delete o->backScattering.flags;
            delete o->backScattering.output;

            delete o->absorption.synchronousZero;
            delete o->absorption.resetGap;
            if (o->absorption.flags != NULL)
                delete o->absorption.flags;
            delete o->absorption.output;

            delete o->extinction.synchronousZero;
            delete o->extinction.resetGap;
            if (o->extinction.flags != NULL)
                delete o->extinction.flags;
            delete o->extinction.output;
        }
    }
}

static SequenceName::Set withEndFlavor(const SequenceName::Set &input)
{
    SequenceName::Set result;
    for (const auto &i : input) {
        result.insert(i.withFlavor(SequenceName::flavor_end));
    }
    return result;
}

static SequenceName::Set withCoverFlavor(const SequenceName::Set &input)
{
    SequenceName::Set result;
    for (const auto &i : input) {
        result.insert(i.withFlavor(SequenceName::flavor_cover));
    }
    return result;
}

static SequenceName::Set withoutEndFlavor(const SequenceName::Set &input)
{
    SequenceName::Set result;
    for (const auto &i : input) {
        result.insert(i.withoutFlavor(SequenceName::flavor_end));
    }
    return result;
}

static SequenceName::Set withAndWithoutEndFlavor(const SequenceName::Set &input)
{
    SequenceName::Set result;
    for (const auto &i : input) {
        result.insert(i.withFlavor(SequenceName::flavor_end));
        result.insert(i.withoutFlavor(SequenceName::flavor_end));
    }
    return result;
}

void CalcOpticalDepth::handleNewProcessing(const SequenceName &unit,
                                           int id,
                                           SegmentProcessingStage::SequenceHandlerControl *control)
{
    Processing *p = &processing[id];

    SequenceName::Set reg;

    for (std::vector<Processing::OpticalWavelength>::iterator it = p->optical.begin(),
            end = p->optical.end(); it != end; ++it) {
        it->outputAsymmetry
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(withAndWithoutEndFlavor(it->outputAsymmetry->getAllUnits()), reg);

        it->scattering
          .output
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(withAndWithoutEndFlavor(it->scattering.output->getAllUnits()), reg);

        it->absorption
          .output
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->absorption.output->getAllUnits(), reg);
        Util::merge(withEndFlavor(it->absorption.output->getAllUnits()), reg);

        it->extinction
          .output
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(withAndWithoutEndFlavor(it->extinction.output->getAllUnits()), reg);
    }

    p->length.output->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(withAndWithoutEndFlavor(p->length.output->getAllUnits()), reg);

    if (!control)
        return;

    for (const auto &n : reg) {
        bool isNew = !control->isHandled(n);
        control->filterUnit(n, toMain(id));
        if (isNew)
            registerPossibleInput(n, control, id);
    }
}

void CalcOpticalDepth::createOptical(const SequenceName &unit,
                                     int id,
                                     SegmentProcessingStage::SequenceHandlerControl *control,
                                     const SequenceName::Component &wavelengthSuffix)
{
    Processing *p = &processing[id];
    Processing::OpticalWavelength o;

    SequenceName transmittanceUnit
            (unit.getStation(), unit.getArchive(), "Ir" + wavelengthSuffix, unit.getFlavors());
    SequenceName sodStartUnit
            (unit.getStation(), unit.getArchive(), "Os" + wavelengthSuffix, unit.getFlavors());
    SequenceName sodEndUnit(sodStartUnit.withFlavor(SequenceName::flavor_end));
    SequenceName bsodStartUnit
            (unit.getStation(), unit.getArchive(), "Obs" + wavelengthSuffix, unit.getFlavors());
    SequenceName bsodEndUnit(sodStartUnit.withFlavor(SequenceName::flavor_end));
    SequenceName aodStartUnit
            (unit.getStation(), unit.getArchive(), "Oa" + wavelengthSuffix, unit.getFlavors());
    SequenceName aodEndUnit(aodStartUnit.withFlavor(SequenceName::flavor_end));
    SequenceName eodStartUnit
            (unit.getStation(), unit.getArchive(), "Oe" + wavelengthSuffix, unit.getFlavors());
    SequenceName eodEndUnit(eodStartUnit.withFlavor(SequenceName::flavor_end));
    SequenceName asymmetryStartUnit
            (unit.getStation(), unit.getArchive(), "ZG" + wavelengthSuffix, unit.getFlavors());
    SequenceName asymmetryEndUnit(asymmetryStartUnit.withFlavor(SequenceName::flavor_end));
    SequenceName lengthStartUnit
            (unit.getStation(), unit.getArchive(), "L_" + p->suffixUnit.getVariable(),
             unit.getFlavors());
    SequenceName lengthEndUnit(lengthStartUnit.withFlavor(SequenceName::flavor_end));

    SequenceName::Set registerUnits;
    registerUnits.insert(unit);
    registerUnits.insert(transmittanceUnit);
    registerUnits.insert(sodStartUnit);
    registerUnits.insert(bsodEndUnit);
    registerUnits.insert(bsodStartUnit);
    registerUnits.insert(sodEndUnit);
    registerUnits.insert(aodStartUnit);
    registerUnits.insert(aodEndUnit);
    registerUnits.insert(eodStartUnit);
    registerUnits.insert(eodEndUnit);
    registerUnits.insert(asymmetryStartUnit);
    registerUnits.insert(asymmetryEndUnit);
    registerUnits.insert(lengthStartUnit);
    registerUnits.insert(lengthEndUnit);

    o.tracking = new DynamicSequenceSelection::Single(transmittanceUnit);
    if (control != NULL)
        control->inputUnit(transmittanceUnit, toAux(id));

    o.wavelength = new DynamicPrimitive<double>::Constant(FP::undefined());

    if (defaultTransmittance == NULL) {
        o.transmittanceData = new DynamicSequenceSelection::Single(transmittanceUnit);
    } else {
        o.transmittanceData = defaultTransmittance->clone();
    }

    if (defaultLengthStart == NULL) {
        if (!defaultCalculateL) {
            o.lengthStart = new DynamicInput::Basic(lengthStartUnit);
        } else {
            o.lengthStart = new DynamicInput::Constant;
        }
    } else {
        o.lengthStart = defaultLengthStart->clone();
    }
    if (defaultLengthEnd == NULL) {
        if (!defaultCalculateL) {
            o.lengthEnd = new DynamicInput::Basic(lengthEndUnit);
        } else {
            o.lengthEnd = new DynamicInput::Constant;
        }
    } else {
        o.lengthEnd = defaultLengthEnd->clone();
    }
    if (defaultDeltaLength == NULL) {
        o.deltaLength = new DynamicInput::Constant;
    } else {
        o.deltaLength = defaultDeltaLength->clone();
    }

    o.outputAsymmetry = new DynamicSequenceSelection::Single(asymmetryStartUnit);

    o.scattering.output = new DynamicSequenceSelection::Single(sodStartUnit);
    if (defaultSynchronousZero != NULL && !p->scatteringSuffix.empty()) {
        SequenceName flagUnit(unit.getStation(), unit.getArchive(), "F1_" + p->scatteringSuffix,
                              unit.getFlavors());
        o.scattering.flags = new DynamicSequenceSelection::Single(flagUnit);
        registerUnits.insert(flagUnit);
    }
    if (defaultSynchronousZero == NULL) {
        o.scattering.synchronousZero = new DynamicPrimitive<bool>::Constant(false);
    } else {
        o.scattering.synchronousZero = defaultSynchronousZero->clone();
    }
    if (defaultResetGap == NULL) {
        o.scattering.resetGap = new DynamicTimeInterval::Undefined;
    } else {
        o.scattering.resetGap = defaultResetGap->clone();
    }

    o.backScattering.output = new DynamicSequenceSelection::Single(bsodStartUnit);
    if (defaultSynchronousZero != NULL && !p->backScatteringSuffix.empty()) {
        SequenceName flagUnit(unit.getStation(), unit.getArchive(), "F1_" + p->backScatteringSuffix,
                              unit.getFlavors());
        o.backScattering.flags = new DynamicSequenceSelection::Single(flagUnit);
        registerUnits.insert(flagUnit);
    }
    if (defaultSynchronousZero == NULL) {
        o.backScattering.synchronousZero = new DynamicPrimitive<bool>::Constant(false);
    } else {
        o.backScattering.synchronousZero = defaultSynchronousZero->clone();
    }
    if (defaultResetGap == NULL) {
        o.backScattering.resetGap = new DynamicTimeInterval::Undefined;
    } else {
        o.backScattering.resetGap = defaultResetGap->clone();
    }

    o.absorption.output = new DynamicSequenceSelection::Single(aodStartUnit);
    if (defaultSynchronousZero != NULL && !p->absorptionSuffix.empty()) {
        SequenceName flagUnit(unit.getStation(), unit.getArchive(), "F1_" + p->absorptionSuffix,
                              unit.getFlavors());
        o.absorption.flags = new DynamicSequenceSelection::Single(flagUnit);
        registerUnits.insert(flagUnit);
    }
    if (defaultSynchronousZero == NULL) {
        o.absorption.synchronousZero = new DynamicPrimitive<bool>::Constant(false);
    } else {
        o.absorption.synchronousZero = defaultSynchronousZero->clone();
    }
    if (defaultResetGap == NULL) {
        o.absorption.resetGap = new DynamicTimeInterval::Undefined;
    } else {
        o.absorption.resetGap = defaultResetGap->clone();
    }

    o.extinction.output = new DynamicSequenceSelection::Single(eodStartUnit);
    if (defaultSynchronousZero != NULL && !p->extinctionSuffix.empty()) {
        SequenceName flagUnit(unit.getStation(), unit.getArchive(), "F1_" + p->extinctionSuffix,
                              unit.getFlavors());
        o.extinction.flags = new DynamicSequenceSelection::Single(flagUnit);
        registerUnits.insert(flagUnit);
    }
    if (defaultSynchronousZero == NULL) {
        o.extinction.synchronousZero = new DynamicPrimitive<bool>::Constant(false);
    } else {
        o.extinction.synchronousZero = defaultSynchronousZero->clone();
    }
    if (defaultResetGap == NULL) {
        o.extinction.resetGap = new DynamicTimeInterval::Undefined;
    } else {
        o.extinction.resetGap = defaultResetGap->clone();
    }

    for (const auto &n : registerUnits) {
        o.tracking->registerInput(n);
        o.transmittanceData->registerInput(n);
        o.lengthStart->registerInput(n);
        o.lengthEnd->registerInput(n);
        o.deltaLength->registerInput(n);
        o.outputAsymmetry->registerInput(n);
        o.scattering.output->registerInput(n);
        o.backScattering.output->registerInput(n);
        o.absorption.output->registerInput(n);
        o.extinction.output->registerInput(n);

        if (o.scattering.flags)
            o.scattering.flags->registerInput(n);
        if (o.backScattering.flags)
            o.backScattering.flags->registerInput(n);
        if (o.absorption.flags)
            o.absorption.flags->registerInput(n);
        if (o.extinction.flags)
            o.extinction.flags->registerInput(n);
    }

    if (control) {
        for (const auto &n : o.lengthStart->getUsedInputs()) {
            control->inputUnit(n, toMain(id));
        }
        for (const auto &n : o.lengthEnd->getUsedInputs()) {
            control->inputUnit(n, toMain(id));
        }
        for (const auto &n : o.deltaLength->getUsedInputs()) {
            control->inputUnit(n, toMain(id));
        }

        if (o.scattering.flags) {
            for (const auto &n : o.scattering.flags->getAllUnits()) {
                control->injectUnit(n, toMain(id));
            }
        }
        if (o.backScattering.flags) {
            for (const auto &n : o.backScattering.flags->getAllUnits()) {
                control->injectUnit(n, toMain(id));
            }
        }
        if (o.absorption.flags) {
            for (const auto &n : o.absorption.flags->getAllUnits()) {
                control->injectUnit(n, toMain(id));
            }
        }
        if (o.extinction.flags) {
            for (const auto &n : o.extinction.flags->getAllUnits()) {
                control->injectUnit(n, toMain(id));
            }
        }
    }

    p->optical.push_back(o);
}

static WavelengthAdjust *createAdjuster(const SequenceName::Component &prefix,
                                        const SequenceName::Component &station,
                                        const SequenceName::Component &archive,
                                        const SequenceName::Component &suffix,
                                        const SequenceName::Flavors &flavors)
{
    return WavelengthAdjust::fromInput(
            new DynamicSequenceSelection::Match(QString::fromStdString(station),
                                                QString::fromStdString(archive),
                                                QString::fromStdString(prefix + "_" + suffix),
                                                flavors));
}

void CalcOpticalDepth::createScattering(const SequenceName &unit,
                                        int id,
                                        SegmentProcessingStage::SequenceHandlerControl *control,
                                        const SequenceName::Component &suffix)
{
    if (processing[id].inputScattering)
        return;

    SequenceName::Set registerUnits;
    registerUnits.insert(unit);

    if (defaultSynchronousZero) {
        SequenceName flagUnit = unit;
        flagUnit.setVariable("F1_" + suffix);
        registerUnits.insert(flagUnit);

        for (std::vector<Processing::OpticalWavelength>::iterator
                it = processing[id].optical.begin(), end = processing[id].optical.end();
                it != end;
                ++it) {
            if (!it->scattering.flags)
                it->scattering.flags = new DynamicSequenceSelection::Single(flagUnit);

            for (const auto &n : registerUnits) {
                it->scattering.flags->registerInput(n);
            }
            for (const auto &n : it->scattering.flags->getAllUnits()) {
                handleNewProcessing(n, id, control);
                control->injectUnit(n, toMain(id));
            }
        }
    }

    processing[id].inputScattering =
            createAdjuster("Bs[A-Z0-9]*", unit.getStation(), unit.getArchive(), suffix,
                           unit.getFlavors());
    for (const auto &n : registerUnits) {
        processing[id].inputScattering->registerInput(n);
    }

    if (control) {
        for (const auto &n : processing[id].inputScattering->getAllUnits()) {
            handleNewProcessing(n, id, control);
            control->injectUnit(n, toMain(id));
            control->injectUnit(n.withFlavor(SequenceName::flavor_cover), toMain(id));
        }
    }


    processing[id].scatteringSuffix = suffix;
}

void CalcOpticalDepth::createBackscattering(const SequenceName &unit,
                                            int id,
                                            SegmentProcessingStage::SequenceHandlerControl *control,
                                            const SequenceName::Component &suffix)
{
    if (processing[id].inputBackscattering)
        return;

    SequenceName::Set registerUnits;
    registerUnits.insert(unit);

    if (defaultSynchronousZero) {
        SequenceName flagUnit = unit;
        flagUnit.setVariable("F1_" + suffix);
        registerUnits.insert(flagUnit);

        for (std::vector<Processing::OpticalWavelength>::iterator
                it = processing[id].optical.begin(), end = processing[id].optical.end();
                it != end;
                ++it) {
            if (!it->backScattering.flags)
                it->backScattering.flags = new DynamicSequenceSelection::Single(flagUnit);

            for (const auto &n : registerUnits) {
                it->backScattering.flags->registerInput(n);
            }
            for (const auto &n : it->backScattering.flags->getAllUnits()) {
                handleNewProcessing(n, id, control);
                control->injectUnit(n, toMain(id));
            }
        }
    }

    processing[id].inputBackscattering =
            createAdjuster("Bbs[A-Z0-9]*", unit.getStation(), unit.getArchive(), suffix,
                           unit.getFlavors());
    for (const auto &n : registerUnits) {
        processing[id].inputBackscattering->registerInput(n);
    }

    if (control) {
        for (const auto &n : processing[id].inputBackscattering->getAllUnits()) {
            handleNewProcessing(n, id, control);
            control->injectUnit(n, toMain(id));
            control->injectUnit(n.withFlavor(SequenceName::flavor_cover), toMain(id));
        }
    }


    processing[id].backScatteringSuffix = suffix;
}

void CalcOpticalDepth::createAbsorption(const SequenceName &unit,
                                        int id,
                                        SegmentProcessingStage::SequenceHandlerControl *control,
                                        const SequenceName::Component &suffix)
{
    if (processing[id].inputAbsorption)
        return;

    SequenceName::Set registerUnits;
    registerUnits.insert(unit);

    if (defaultSynchronousZero) {
        SequenceName flagUnit = unit;
        flagUnit.setVariable("F1_" + suffix);
        registerUnits.insert(flagUnit);

        for (std::vector<Processing::OpticalWavelength>::iterator
                it = processing[id].optical.begin(), end = processing[id].optical.end();
                it != end;
                ++it) {
            if (!it->absorption.flags)
                it->absorption.flags = new DynamicSequenceSelection::Single(flagUnit);

            for (const auto &n : registerUnits) {
                it->absorption.flags->registerInput(n);
            }
            for (const auto &n : it->absorption.flags->getAllUnits()) {
                handleNewProcessing(n, id, control);
                control->injectUnit(n, toMain(id));
            }
        }
    }

    processing[id].inputAbsorption =
            createAdjuster("Ba[A-Z0-9]*", unit.getStation(), unit.getArchive(), suffix,
                           unit.getFlavors());
    for (const auto &n : registerUnits) {
        processing[id].inputAbsorption->registerInput(n);
    }

    if (control) {
        for (const auto &n : processing[id].inputAbsorption->getAllUnits()) {
            handleNewProcessing(n, id, control);
            control->injectUnit(n, toMain(id));
            control->injectUnit(n.withFlavor(SequenceName::flavor_cover), toMain(id));
        }
    }


    processing[id].absorptionSuffix = suffix;
}

void CalcOpticalDepth::createExtinction(const SequenceName &unit,
                                        int id,
                                        SegmentProcessingStage::SequenceHandlerControl *control,
                                        const SequenceName::Component &suffix)
{
    if (processing[id].inputExtinction)
        return;

    SequenceName::Set registerUnits;
    registerUnits.insert(unit);

    if (defaultSynchronousZero) {
        SequenceName flagUnit = unit;
        flagUnit.setVariable("F1_" + suffix);
        registerUnits.insert(flagUnit);

        for (std::vector<Processing::OpticalWavelength>::iterator
                it = processing[id].optical.begin(), end = processing[id].optical.end();
                it != end;
                ++it) {
            if (!it->extinction.flags)
                it->extinction.flags = new DynamicSequenceSelection::Single(flagUnit);

            for (const auto &n : registerUnits) {
                it->extinction.flags->registerInput(n);
            }
            for (const auto &n : it->extinction.flags->getAllUnits()) {
                handleNewProcessing(n, id, control);
                control->injectUnit(n, toMain(id));
            }
        }
    }

    processing[id].inputExtinction =
            createAdjuster("Be[A-Z0-9]*", unit.getStation(), unit.getArchive(), suffix,
                           unit.getFlavors());
    for (const auto &n : registerUnits) {
        processing[id].inputExtinction->registerInput(n);
    }

    if (control) {
        for (const auto &n : processing[id].inputExtinction->getAllUnits()) {
            handleNewProcessing(n, id, control);
            control->injectUnit(n, toMain(id));
            control->injectUnit(n.withFlavor(SequenceName::flavor_cover), toMain(id));
        }
    }


    processing[id].extinctionSuffix = suffix;
}

CalcOpticalDepth::AuxiliaryVariable &CalcOpticalDepth::lookupAuxiliary(const SequenceName &unit,
                                                                       Processing &processing)
{
    processing.auxiliaryBind.insert(unit);

    QHash<SequenceName, AuxiliaryVariable>::iterator check = auxiliary.find(unit);
    if (check != auxiliary.end())
        return check.value();
    check = auxiliary.insert(unit, AuxiliaryVariable());
    return check.value();
}

void CalcOpticalDepth::registerPossibleInput(const SequenceName &unit,
                                             SegmentProcessingStage::SequenceHandlerControl *control,
                                             int filterID)
{
    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        bool usedAsInput = false;

        if (processing[id].inputScattering != NULL &&
                processing[id].inputScattering->registerInput(unit)) {
            if (control != NULL) {
                control->injectUnit(unit, toMain(id));
                control->injectUnit(unit.withFlavor(SequenceName::flavor_cover), toMain(id));
            }
            usedAsInput = true;
        }

        if (processing[id].inputBackscattering != NULL &&
                processing[id].inputBackscattering->registerInput(unit)) {
            if (control != NULL) {
                control->injectUnit(unit, toMain(id));
                control->injectUnit(unit.withFlavor(SequenceName::flavor_cover), toMain(id));
            }
            usedAsInput = true;
        }

        if (processing[id].inputAbsorption != NULL &&
                processing[id].inputAbsorption->registerInput(unit)) {
            if (control != NULL) {
                control->injectUnit(unit, toMain(id));
                control->injectUnit(unit.withFlavor(SequenceName::flavor_cover), toMain(id));
            }
            usedAsInput = true;
        }

        if (processing[id].inputExtinction != NULL &&
                processing[id].inputExtinction->registerInput(unit)) {
            if (control != NULL) {
                control->injectUnit(unit, toMain(id));
                control->injectUnit(unit.withFlavor(SequenceName::flavor_cover), toMain(id));
            }
            usedAsInput = true;
        }

        if (processing[id].inputDeltaLength->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, toMain(id));
            usedAsInput = true;
        }

        if (processing[id].filterChange->registerInput(unit)) {
            lookupAuxiliary(unit, processing[id]);
            if (control != NULL)
                control->inputUnit(unit, toAux(id));
            usedAsInput = true;
        }
        if (processing[id].spotData->registerInput(unit)) {
            lookupAuxiliary(unit, processing[id]);
            if (control != NULL)
                control->inputUnit(unit, toAux(id));
            usedAsInput = true;
        }
        if (processing[id].flowData->registerInput(unit)) {
            lookupAuxiliary(unit, processing[id]);
            if (control != NULL)
                control->inputUnit(unit, toAux(id));
            usedAsInput = true;
        }


        for (std::vector<Processing::OpticalWavelength>::iterator
                it = processing[id].optical.begin(), end = processing[id].optical.end();
                it != end;
                ++it) {
            if (it->tracking->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, toAux(id));
                lookupAuxiliary(unit, processing[id]);
                usedAsInput = true;
            }

            if (it->transmittanceData->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, toAux(id));
                lookupAuxiliary(unit, processing[id]);
                usedAsInput = true;
            }

            if (it->lengthStart->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, toMain(id));
                usedAsInput = true;
            }
            if (it->lengthEnd->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, toMain(id));
                usedAsInput = true;
            }
            if (it->deltaLength->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, toMain(id));
                usedAsInput = true;
            }

            if (it->scattering.flags != NULL && it->scattering.flags->registerInput(unit)) {
                if (control != NULL)
                    control->injectUnit(unit, toMain(id));
                usedAsInput = true;
            }
            if (it->backScattering.flags != NULL && it->backScattering.flags->registerInput(unit)) {
                if (control != NULL)
                    control->injectUnit(unit, toMain(id));
                usedAsInput = true;
            }
            if (it->absorption.flags != NULL && it->absorption.flags->registerInput(unit)) {
                if (control != NULL)
                    control->injectUnit(unit, toMain(id));
                usedAsInput = true;
            }
            if (it->extinction.flags != NULL && it->extinction.flags->registerInput(unit)) {
                if (control != NULL)
                    control->injectUnit(unit, toMain(id));
                usedAsInput = true;
            }
        }

        if (usedAsInput && id != filterID)
            handleNewProcessing(unit, id, control);

        if (control != NULL && processing[id].auxiliaryBind.count(unit) != 0) {
            control->inputUnit(unit, toAux(id));
        }
    }

    if (defaultScattering != NULL && defaultScattering->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultBackscattering != NULL &&
            defaultBackscattering->registerInput(unit) &&
            control != NULL)
        control->deferHandling(unit);
    if (defaultAbsorption != NULL && defaultAbsorption->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultExtiniction != NULL && defaultExtiniction->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultTransmittance != NULL &&
            defaultTransmittance->registerInput(unit) &&
            control != NULL)
        control->deferHandling(unit);
    if (defaultLengthStart != NULL && defaultLengthStart->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultLengthEnd != NULL && defaultLengthEnd->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultDeltaLength != NULL && defaultDeltaLength->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
}

bool CalcOpticalDepth::initialHandler(const SequenceName &unit,
                                      SegmentProcessingStage::SequenceHandlerControl *control)
{
    registerPossibleInput(unit, control);

    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].length.output->registerInput(unit)) {
            SequenceName other(unit);
            if (unit.hasFlavor(SequenceName::flavor_end)) {
                other.removeFlavor(SequenceName::flavor_end);
            } else {
                other.addFlavor(SequenceName::flavor_end);
            }
            if (control != NULL) {
                control->filterUnit(unit, toMain(id));
                control->filterUnit(other, toMain(id));
            }
            registerPossibleInput(other, control, id);
            handleNewProcessing(unit, id, control);
            handleNewProcessing(other, id, control);
            return true;
        }
        for (std::vector<Processing::OpticalWavelength>::iterator
                it = processing[id].optical.begin(), end = processing[id].optical.end();
                it != end;
                ++it) {
            if (it->outputAsymmetry->registerInput(unit)) {
                SequenceName other(unit);
                if (unit.hasFlavor(SequenceName::flavor_end)) {
                    other.removeFlavor(SequenceName::flavor_end);
                } else {
                    other.addFlavor(SequenceName::flavor_end);
                }
                if (control != NULL) {
                    control->filterUnit(unit, toMain(id));
                    control->filterUnit(other, toMain(id));
                }
                registerPossibleInput(other, control, id);
                handleNewProcessing(unit, id, control);
                handleNewProcessing(other, id, control);
                return true;
            }
            if (it->scattering.output->registerInput(unit)) {
                SequenceName other(unit);
                if (unit.hasFlavor(SequenceName::flavor_end)) {
                    other.removeFlavor(SequenceName::flavor_end);
                } else {
                    other.addFlavor(SequenceName::flavor_end);
                }
                if (control != NULL) {
                    control->filterUnit(unit, toMain(id));
                    control->filterUnit(other, toMain(id));
                }
                registerPossibleInput(other, control, id);
                handleNewProcessing(unit, id, control);
                handleNewProcessing(other, id, control);
                return true;
            }
            if (it->backScattering.output->registerInput(unit)) {
                handleNewProcessing(unit, id, control);
                return true;
            }
            if (it->absorption.output->registerInput(unit)) {
                SequenceName other(unit);
                if (unit.hasFlavor(SequenceName::flavor_end)) {
                    other.removeFlavor(SequenceName::flavor_end);
                } else {
                    other.addFlavor(SequenceName::flavor_end);
                }
                if (control != NULL) {
                    control->filterUnit(unit, toMain(id));
                    control->filterUnit(other, toMain(id));
                }
                registerPossibleInput(other, control, id);
                handleNewProcessing(unit, id, control);
                handleNewProcessing(other, id, control);
                return true;
            }
            if (it->extinction.output->registerInput(unit)) {
                SequenceName other(unit);
                if (unit.hasFlavor(SequenceName::flavor_end)) {
                    other.removeFlavor(SequenceName::flavor_end);
                } else {
                    other.addFlavor(SequenceName::flavor_end);
                }
                if (control != NULL) {
                    control->filterUnit(unit, toMain(id));
                    control->filterUnit(other, toMain(id));
                }
                registerPossibleInput(other, control, id);
                handleNewProcessing(unit, id, control);
                handleNewProcessing(other, id, control);
                return true;
            }
        }
    }
    return false;
}


void CalcOpticalDepth::unhandled(const SequenceName &unit,
                                 SegmentProcessingStage::SequenceHandlerControl *control)
{
    if (initialHandler(unit, control))
        return;
    if (restrictedInputs)
        return;

    if (unit.hasFlavor("stats") || unit.hasFlavor("cover"))
        return;
    if (unit.hasFlavor("end")) {
        if (defaultCalculateL || defaultLengthStart == NULL || defaultLengthEnd == NULL) {
            if (Util::starts_with(unit.getVariable(), "L_"))
                control->deferHandling(unit);
        }
        return;
    }

    enum {
        Scattering, Backscattering, Absorption, Extinction, Transmittance,
    } type;
    SequenceName::Component suffix;
    SequenceName::Component wavelengthSuffix;
    {
        QRegExp reCheck("((?:Bb?s)|(?:Ba)|(?:Be)|(?:Ir))([A-Z0-9]*)_(.+)");
        if (!reCheck.exactMatch(unit.getVariableQString())) {
            if (control != NULL) {
                if (Util::starts_with(unit.getVariable(), "F1_") ||
                        Util::starts_with(unit.getVariable(), "Ff_") ||
                        Util::starts_with(unit.getVariable(), "Fn_"))
                    control->deferHandling(unit);
                if (defaultCalculateL) {
                    if (Util::starts_with(unit.getVariable(), "ZSPOT_") ||
                            Util::starts_with(unit.getVariable(), "Q_") ||
                            Util::starts_with(unit.getVariable(), "L_"))
                        control->deferHandling(unit);
                }
                if (defaultLengthStart == NULL || defaultLengthEnd == NULL) {
                    if (Util::starts_with(unit.getVariable(), "L_"))
                        control->deferHandling(unit);
                }
            }
            return;
        }
        if (reCheck.cap(1) == "Bs")
            type = Scattering;
        else if (reCheck.cap(1) == "Bbs")
            type = Backscattering;
        else if (reCheck.cap(1) == "Ba")
            type = Absorption;
        else if (reCheck.cap(1) == "Be")
            type = Extinction;
        else if (reCheck.cap(1) == "Ir")
            type = Transmittance;
        else
            return;
        suffix = reCheck.cap(3).toStdString();
        wavelengthSuffix = reCheck.cap(2).toStdString() + "_" + suffix;
    }

    switch (type) {
    case Scattering:
    case Backscattering:
        if (!scatteringSuffixes.empty() && !scatteringSuffixes.count(suffix))
            return;
        break;
    case Absorption:
        if (!absorptionSuffixes.empty() && !absorptionSuffixes.count(suffix))
            return;
        break;
    case Extinction:
        if (!extinctionSuffixes.empty() && !extinctionSuffixes.count(suffix))
            return;
        break;
    case Transmittance:
        if (!filterSuffixes.empty() && !filterSuffixes.count(suffix))
            return;
        break;
    }

    bool matchesExisting = false;
    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].suffixUnit.getStation() != unit.getStation())
            continue;
        if (processing[id].suffixUnit.getArchive() != unit.getArchive())
            continue;
        if (processing[id].suffixUnit.getFlavors() != unit.getFlavors())
            continue;

        switch (type) {
        case Scattering:
            if (processing[id].inputScattering != NULL) {
                if (processing[id].inputScattering->registerInput(unit)) {
                    control->injectUnit(unit, toMain(id));
                    control->injectUnit(unit.withFlavor(SequenceName::flavor_cover), toMain(id));
                    break;
                }
                continue;
            }
            createScattering(unit, id, control, suffix);
            break;
        case Backscattering:
            if (processing[id].inputBackscattering != NULL) {
                if (processing[id].inputBackscattering->registerInput(unit)) {
                    control->injectUnit(unit, toMain(id));
                    control->injectUnit(unit.withFlavor(SequenceName::flavor_cover), toMain(id));
                    break;
                }
                continue;
            }
            createBackscattering(unit, id, control, suffix);
            break;
        case Absorption:
            if (processing[id].inputAbsorption != NULL) {
                if (processing[id].inputAbsorption->registerInput(unit)) {
                    control->injectUnit(unit, toMain(id));
                    control->injectUnit(unit.withFlavor(SequenceName::flavor_cover), toMain(id));
                    break;
                }
                continue;
            }
            createAbsorption(unit, id, control, suffix);
            break;
        case Extinction:
            if (processing[id].inputExtinction != NULL) {
                if (processing[id].inputExtinction->registerInput(unit)) {
                    control->injectUnit(unit, toMain(id));
                    control->injectUnit(unit.withFlavor(SequenceName::flavor_cover), toMain(id));
                    break;
                }
                continue;
            }
            createExtinction(unit, id, control, suffix);
            break;
        case Transmittance:
            if (processing[id].suffixUnit.getVariable() != suffix)
                continue;
            createOptical(unit, id, control, wavelengthSuffix);
            break;
        }

        handleNewProcessing(unit, id, control);
        if (processing[id].suffixUnit.getVariable() != suffix)
            continue;
        matchesExisting = true;
    }
    if (matchesExisting)
        return;

    Processing p;
    int id = (int) processing.size();

    p.suffixUnit = SequenceName(unit.getStation(), unit.getArchive(), suffix, unit.getFlavors());

    if (defaultCalculateL && defaultDeltaLength != NULL) {
        p.inputDeltaLength = defaultDeltaLength->clone();
    } else {
        p.inputDeltaLength = new DynamicInput::Constant;
    }

    p.filterChange = new DynamicSequenceSelection::Basic(
            SequenceName::Set{{unit.getStation(), unit.getArchive(), "Fn_" + suffix},
                              {unit.getStation(), unit.getArchive(), "Ff_" + suffix}});

    if (defaultResetGap == NULL) {
        p.length.resetGap = new DynamicTimeInterval::Undefined;
    } else {
        p.length.resetGap = defaultResetGap->clone();
    }

    if (defaultCalculateL) {
        p.spotData = new DynamicSequenceSelection::Single(
                SequenceName(unit.getStation(), unit.getArchive(), "ZSPOT_" + suffix));
        p.flowData = new DynamicSequenceSelection::Single(
                SequenceName(unit.getStation(), unit.getArchive(), "Q_" + suffix));

        p.length.output = new DynamicSequenceSelection::Single(
                SequenceName(unit.getStation(), unit.getArchive(), "L_" + suffix,
                             unit.getFlavors()));
    } else {
        p.spotData = new DynamicSequenceSelection::None;
        p.flowData = new DynamicSequenceSelection::None;
        p.length.output = new DynamicSequenceSelection::None;
    }

    if (defaultScattering != NULL) {
        p.inputScattering = new WavelengthAdjust(*defaultScattering);
    } else {
        for (std::vector<Processing>::const_iterator it = processing.begin(),
                end = processing.end(); it != end; ++it) {
            if (it->inputScattering == NULL)
                continue;
            if (it->suffixUnit.getStation() != unit.getStation())
                continue;
            if (it->suffixUnit.getArchive() != unit.getArchive())
                continue;
            if (it->suffixUnit.getFlavors() != unit.getFlavors())
                continue;
            p.inputScattering = new WavelengthAdjust(*it->inputScattering);
            p.scatteringSuffix = it->scatteringSuffix;
            break;
        }
    }
    if (defaultBackscattering != NULL) {
        p.inputBackscattering = new WavelengthAdjust(*defaultBackscattering);
    } else {
        for (std::vector<Processing>::const_iterator it = processing.begin(),
                end = processing.end(); it != end; ++it) {
            if (it->inputBackscattering == NULL)
                continue;
            if (it->suffixUnit.getStation() != unit.getStation())
                continue;
            if (it->suffixUnit.getArchive() != unit.getArchive())
                continue;
            if (it->suffixUnit.getFlavors() != unit.getFlavors())
                continue;
            p.inputBackscattering = new WavelengthAdjust(*it->inputBackscattering);
            p.backScatteringSuffix = it->backScatteringSuffix;
            break;
        }
    }
    if (defaultAbsorption != NULL) {
        p.inputAbsorption = new WavelengthAdjust(*defaultAbsorption);
    } else {
        for (std::vector<Processing>::const_iterator it = processing.begin(),
                end = processing.end(); it != end; ++it) {
            if (it->inputAbsorption == NULL)
                continue;
            if (it->suffixUnit.getStation() != unit.getStation())
                continue;
            if (it->suffixUnit.getArchive() != unit.getArchive())
                continue;
            if (it->suffixUnit.getFlavors() != unit.getFlavors())
                continue;
            p.inputAbsorption = new WavelengthAdjust(*it->inputAbsorption);
            p.absorptionSuffix = it->absorptionSuffix;
            break;
        }
    }
    if (defaultExtiniction != NULL) {
        p.inputExtinction = new WavelengthAdjust(*defaultExtiniction);
    } else {
        for (std::vector<Processing>::const_iterator it = processing.begin(),
                end = processing.end(); it != end; ++it) {
            if (it->inputExtinction == NULL)
                continue;
            if (it->suffixUnit.getStation() != unit.getStation())
                continue;
            if (it->suffixUnit.getArchive() != unit.getArchive())
                continue;
            if (it->suffixUnit.getFlavors() != unit.getFlavors())
                continue;
            p.inputExtinction = new WavelengthAdjust(*it->inputExtinction);
            p.extinctionSuffix = it->extinctionSuffix;
            break;
        }
    }

    processing.push_back(p);

    switch (type) {
    case Scattering:
        createScattering(unit, id, control, suffix);
        break;
    case Backscattering:
        createBackscattering(unit, id, control, suffix);
        break;
    case Absorption:
        createAbsorption(unit, id, control, suffix);
        break;
    case Extinction:
        createExtinction(unit, id, control, suffix);
        break;
    case Transmittance:
        createOptical(unit, id, control, wavelengthSuffix);
        break;
    }

    initialHandler(unit, control);

    if (!control)
        return;

    SequenceName::Set reg;
    if (processing[id].inputScattering)
        Util::merge(processing[id].inputScattering->getAllUnits(), reg);
    if (processing[id].inputBackscattering)
        Util::merge(processing[id].inputBackscattering->getAllUnits(), reg);
    if (processing[id].inputAbsorption)
        Util::merge(processing[id].inputAbsorption->getAllUnits(), reg);
    if (processing[id].inputExtinction)
        Util::merge(processing[id].inputExtinction->getAllUnits(), reg);
    Util::merge(processing[id].inputDeltaLength->getUsedInputs(), reg);
    Util::merge(processing[id].filterChange->getAllUnits(), reg);
    Util::merge(processing[id].spotData->getAllUnits(), reg);
    Util::merge(processing[id].flowData->getAllUnits(), reg);
    for (std::vector<Processing::OpticalWavelength>::iterator it = processing[id].optical.begin(),
            endO = processing[id].optical.end(); it != endO; ++it) {
        Util::merge(it->tracking->getAllUnits(), reg);
        Util::merge(it->transmittanceData->getAllUnits(), reg);
    }
    reg.insert(unit);

    /* Since we have static names for things, do input requests for all of them */
    for (const auto &n : reg) {
        initialHandler(n, control);
    }
}

static double calculateAsymmetry(double Bs, double Bbs)
{
    if (!FP::defined(Bs) || !FP::defined(Bbs))
        return FP::undefined();
    if (Bs < 1E-7)
        Bs = 1E-7;

    double Bfr = Bbs / Bs;
    if (Bfr < 0.01) Bfr = 0.01; else if (Bfr > 0.5) Bfr = 0.5;

    static const Calibration g(QVector<double>() << 0.9893 << -3.9636 << 7.4644 << -7.1439);
    return g.apply(Bfr);
}

static double calculateDeltaLength(double flow, double area, double start, double end)
{
    if (!FP::defined(flow) || !FP::defined(area) || !FP::defined(start) || !FP::defined(end))
        return FP::undefined();
    double dT = end - start;
    if (dT < 0.0)
        return FP::undefined();
    if (area <= 0.0)
        return FP::undefined();
    flow /= 60000.0;
    if (area <= 0.1) {
        /* in m^2 */
        return (flow * dT) / area;
    } else {
        /* in mm^2 */
        return (flow * dT) / (area / 1E6);
    }
}

static double calculateMissingTime(double lastEnd, double start, double end)
{
    if (!FP::defined(lastEnd) || !FP::defined(start) || !FP::defined(end))
        return FP::undefined();
    double missing = start - lastEnd;
    if (missing <= 0.0)
        return FP::undefined();
    return missing;
}

static double calculateMissingLength(double dL, double missingTime, double start, double end)
{
    if (!FP::defined(dL) || !FP::defined(missingTime) || missingTime <= 0.0)
        return 0.0;
    if (!FP::defined(start) || !FP::defined(end))
        return 0.0;
    if (!FP::defined(dL))
        return 0.0;
    double dT = end - start;
    if (dT < 1E-3)
        return 0.0;
    return (dL / dT) * missingTime;
}

void CalcOpticalDepth::AuxiliaryVariable::update(SequenceSegment &data, const SequenceName &unit)
{
    auto value = data[unit];
    if (Variant::Composite::isDefined(prior)) {
        if (value != prior.read())
            changeTime = data.getStart();
    } else {
        if (Variant::Composite::isDefined(value))
            changeTime = data.getStart();
    }
    prior.write().set(value);
}

void CalcOpticalDepth::Accumulator::reset()
{
    lastEndTime = FP::undefined();
    sum = FP::undefined();
}

void CalcOpticalDepth::Accumulator::process(SequenceSegment &data,
                                            double add,
                                            const SequenceName &output,
                                            AccumulationParameters &parameters)
{
    Q_ASSERT(!output.hasFlavor(SequenceName::flavor_end));

    double gapCheck = parameters.resetGap->apply(data.getStart(), lastEndTime, true);
    double missingTime;
    if (FP::defined(gapCheck) && FP::defined(data.getStart()) && gapCheck < data.getStart()) {
        reset();
        missingTime = FP::undefined();
    } else {
        missingTime = calculateMissingTime(lastEndTime, data.getStart(), data.getEnd());
    }
    lastEndTime = data.getEnd();

    if (!FP::defined(add)) {
        add = priorInput;
    } else {
        priorInput = add;
    }

    if (!FP::defined(sum)) {
        if (FP::defined(add)) {
            sum = 0.0;
        } else {
            if (output.isValid()) {
                data[output].setDouble(FP::undefined());
                data[output.withFlavor(SequenceName::flavor_end)].setDouble(FP::undefined());
            }
            return;
        }
    } else {
        sum += calculateMissingLength(add, missingTime, data.getStart(), data.getEnd());
    }

    Q_ASSERT(FP::defined(sum));

    if (output.isValid())
        data[output].setDouble(sum);
    if (FP::defined(add))
        sum += add;
    if (output.isValid())
        data[output.withFlavor(SequenceName::flavor_end)].setDouble(sum);
}

void CalcOpticalDepth::Accumulator::process(SequenceSegment &data,
                                            WavelengthAdjust *input,
                                            double wavelength,
                                            double dL,
                                            const SequenceName &output,
                                            AccumulationParameters &parameters)
{
    Q_ASSERT(!output.hasFlavor(SequenceName::flavor_end));

    double gapCheck = parameters.resetGap->apply(data.getStart(), lastEndTime, true);
    double missingTime;
    if (FP::defined(gapCheck) && FP::defined(data.getStart()) && gapCheck < data.getStart()) {
        reset();
        missingTime = FP::undefined();
    } else {
        missingTime = calculateMissingTime(lastEndTime, data.getStart(), data.getEnd());
    }
    lastEndTime = data.getEnd();

    Q_ASSERT(input != NULL);
    double add = input->getAdjusted(wavelength).toReal();

    bool rememberAdd = true;
    if (parameters.synchronousZero->get(data) && parameters.flags) {
        static const Variant::Flags flagsInZero{"Zero", "Blank", "Spancheck"};

        bool inZero = false;
        for (const auto &i : parameters.flags->get(data)) {
            auto flagCheck = data[i];
            if (flagCheck.testAnyFlags(flagsInZero)) {
                inZero = true;
                break;
            }
        }
        if (inZero) {
            rememberAdd = false;
            if (!FP::defined(add)) {
                add = 0.0;
            } else {
                double effectiveFraction = 1.0;
                for (const auto &i : input->getAllUnits()) {
                    double cover = data[i.withFlavor(SequenceName::flavor_cover)].toDouble();
                    if (!FP::defined(cover))
                        continue;
                    if (cover <= 0.0 || cover > 1.0)
                        continue;
                    effectiveFraction = qMin(cover, effectiveFraction);
                    break;
                }

                add *= effectiveFraction;
            }
        }
    }
    if (!FP::defined(add)) {
        add = priorInput;
    } else if (rememberAdd) {
        priorInput = add;
    }

    if (!FP::defined(sum)) {
        if (FP::defined(add) && FP::defined(dL)) {
            sum = 0.0;
        } else {
            if (output.isValid()) {
                data[output].setDouble(FP::undefined());
                data[output.withFlavor(SequenceName::flavor_end)].setDouble(FP::undefined());
            }
            return;
        }
    }

    if (FP::defined(dL) && FP::defined(add)) {
        add *= 1E-6;
        sum += add * calculateMissingLength(dL, missingTime, data.getStart(), data.getEnd());
        add *= dL;
    } else {
        add = FP::undefined();
    }

    Q_ASSERT(FP::defined(sum));

    if (output.isValid())
        data[output].setDouble(sum);
    if (FP::defined(add))
        sum += add;
    if (output.isValid())
        data[output.withFlavor(SequenceName::flavor_end)].setDouble(sum);
}

void CalcOpticalDepth::process(int id, SequenceSegment &data)
{
    if (isAux(id)) {
        id = toID(id);
        Processing *p = &processing[id];

        for (const auto &i : p->auxiliaryBind) {
            auxiliary[i].update(data, i);
        }
        return;
    }

    id = toID(id);
    Processing *p = &processing[id];

    if (p->inputScattering != NULL)
        p->inputScattering->incoming(data);
    if (p->inputBackscattering != NULL)
        p->inputBackscattering->incoming(data);
    if (p->inputAbsorption != NULL)
        p->inputAbsorption->incoming(data);
    if (p->inputExtinction != NULL)
        p->inputExtinction->incoming(data);

    /* First pass, detect a filter change */
    bool filterChanged = false;
    if (!FP::defined(p->filterChangeTime)) {
        p->filterChangeTime = data.getStart();
    } else {
        for (const auto &i : p->filterChange->get(data)) {
            double t = lookupAuxiliary(i, *p).changeTime;
            if (!FP::defined(t))
                continue;
            if (t <= p->filterChangeTime)
                continue;
            filterChanged = true;
            break;
        }
        for (std::vector<Processing::OpticalWavelength>::iterator o = p->optical.begin(),
                endO = p->optical.end(); o != endO; ++o) {
            for (const auto &i : o->transmittanceData->get(data)) {
                double IrCurrent = lookupAuxiliary(i, *p).prior.read().toDouble();
                if (transmittanceFilterChange(o->lastTransmittance, IrCurrent)) {
                    filterChanged = true;
                }
                o->lastTransmittance = IrCurrent;
            }

            double startL = FP::undefined();
            double endL = FP::undefined();
            if (o->lengthStart != NULL)
                startL = o->lengthStart->get(data);
            if (o->lengthEnd != NULL)
                endL = o->lengthEnd->get(data);

            double dL = o->deltaLength->get(data);
            if (!FP::defined(dL) && FP::defined(startL) && FP::defined(endL))
                dL = endL - startL;

            if (!FP::defined(startL) && FP::defined(endL) && FP::defined(dL))
                startL = endL - dL;

            if (lengthFilterChange(o->lastLength, startL)) {
                filterChanged = true;
            }
            o->lastLength = startL;
        }
    }

    if (filterChanged) {
        p->filterChangeTime = data.getStart();

        for (auto i : p->length.output->get(data)) {
            i.clearFlavors();
            accumulators[i].reset();
        }
    }

    double dLCalculated = p->inputDeltaLength->get(data);
    {
        auto s = withoutEndFlavor(p->length.output->get(data));
        if (!s.empty()) {
            if (!FP::defined(dLCalculated)) {
                double area = FP::undefined();
                for (const auto &i : p->spotData->get(data)) {
                    area = lookupAuxiliary(i, *p).prior["Area"].toDouble();
                    if (FP::defined(area))
                        break;
                }

                double flow = FP::undefined();
                for (const auto &i : p->flowData->get(data)) {
                    flow = lookupAuxiliary(i, *p).prior.read().toDouble();
                    if (FP::defined(flow))
                        break;
                }

                dLCalculated = calculateDeltaLength(flow, area, data.getStart(), data.getEnd());
            }
            for (const auto &i : s) {
                accumulators[i.withoutAllFlavors()].process(data, dLCalculated, i, p->length);
            }
        }
    }

    /* Second pass, integrate */
    for (std::vector<Processing::OpticalWavelength>::iterator o = p->optical.begin(),
            endO = p->optical.end(); o != endO; ++o) {
        double wavelength = o->wavelength->get(data);
        if (!FP::defined(wavelength)) {
            for (const auto &i : o->tracking->get(data)) {
                wavelength = p->wavelengthTracker.get(i);
                if (FP::defined(wavelength))
                    break;
            }
        }

        double startL = FP::undefined();
        double endL = FP::undefined();
        if (o->lengthStart != NULL)
            startL = o->lengthStart->get(data);
        if (o->lengthEnd != NULL)
            endL = o->lengthEnd->get(data);

        double dL = o->deltaLength->get(data);
        if (!FP::defined(dL) && FP::defined(startL) && FP::defined(endL))
            dL = endL - startL;

        if (!FP::defined(dL))
            dL = dLCalculated;

        if (filterChanged) {
            o->lastTransmittance = FP::undefined();
            o->lastLength = FP::undefined();

            for (auto i : o->scattering.output->get(data)) {
                i.clearFlavors();
                accumulators[i].reset();
            }
            for (auto i : o->backScattering.output->get(data)) {
                i.clearFlavors();
                accumulators[i].reset();
            }
            for (auto i : o->absorption.output->get(data)) {
                i.clearFlavors();
                accumulators[i].reset();
            }
            for (auto i : o->extinction.output->get(data)) {
                i.clearFlavors();
                accumulators[i].reset();
            }
            if (!FP::defined(dL) || dL < 0.0)
                dL = o->lastValidDeltaLength;
        }

        if (FP::defined(dL)) {
            if (dL < 0.0)
                dL = 0.0;
            else
                o->lastValidDeltaLength = dL;
        }

        if (p->inputScattering && p->inputBackscattering) {
            double Bs = FP::undefined();
            for (auto i : o->scattering.output->get(data)) {
                i.clearFlavors();
                Bs = accumulators[i].sum;
                if (FP::defined(Bs))
                    break;
            }

            double Bbs = FP::undefined();
            for (auto i : o->backScattering.output->get(data)) {
                i.clearFlavors();
                Bbs = accumulators[i].sum;
                if (FP::defined(Bbs))
                    break;
            }

            for (const auto &i : o->outputAsymmetry->get(data)) {
                data[i] = calculateAsymmetry(Bs, Bbs);
            }
        }

        if (p->inputScattering) {
            for (const auto &i : withoutEndFlavor(o->scattering.output->get(data))) {
                accumulators[i.withoutAllFlavors()].process(data, p->inputScattering, wavelength,
                                                            dL, i, o->scattering);
            }
        }

        if (p->inputBackscattering) {
            for (const auto &i : withoutEndFlavor(o->backScattering.output->get(data))) {
                accumulators[i.withoutAllFlavors()].process(data, p->inputBackscattering,
                                                            wavelength, dL, SequenceName(),
                                                            o->backScattering);
            }
        }

        if (p->inputAbsorption) {
            for (const auto &i : withoutEndFlavor(o->absorption.output->get(data))) {
                accumulators[i.withoutAllFlavors()].process(data, p->inputAbsorption, wavelength,
                                                            dL, i, o->absorption);
            }
        }

        if (p->inputExtinction) {
            for (const auto &i : withoutEndFlavor(o->extinction.output->get(data))) {
                accumulators[i.withoutAllFlavors()].process(data, p->inputExtinction, wavelength,
                                                            dL, i, o->extinction);
            }
        }

        if (p->inputScattering && p->inputBackscattering) {
            double Bs = FP::undefined();
            for (auto i : o->scattering.output->get(data)) {
                i.clearFlavors();
                Bs = accumulators[i].sum;
                if (FP::defined(Bs))
                    break;
            }

            double Bbs = FP::undefined();
            for (auto i : o->backScattering.output->get(data)) {
                i.clearFlavors();
                Bbs = accumulators[i].sum;
                if (FP::defined(Bbs))
                    break;
            }

            for (auto i : o->outputAsymmetry->get(data)) {
                i.addFlavor(SequenceName::flavor_end);
                data[i] = calculateAsymmetry(Bs, Bbs);
            }
        }
    }
}

void CalcOpticalDepth::AccumulationParameters::opticalDepthMetadata(SequenceSegment &data,
                                                                    const SequenceName &output,
                                                                    const WavelengthAdjust *input,
                                                                    double wavelength) const
{
    Variant::Read base = Variant::Read::empty();
    for (auto i : input->getAllUnits()) {
        i.setMeta();
        auto check = data[i];
        if (!check.exists())
            continue;
        if (!base.exists()) {
            base = check;
            continue;
        }
        double checkWavelength = check.metadata("Wavelength").toDouble();
        if (!FP::defined(checkWavelength))
            continue;
        double baseWavelength = base.metadata("Wavelength").toDouble();
        if (!FP::defined(baseWavelength)) {
            base = check;
            continue;
        }
        if (!FP::defined(wavelength))
            continue;
        if (fabs(checkWavelength - wavelength) < fabs(baseWavelength - wavelength)) {
            base = check;
            continue;
        }
    }

    if (!data[output].exists())
        data[output].set(base);

    data[output].metadataReal("Format").setString("00.0000000");
    data[output].metadataReal("MVC").remove();
    data[output].metadataReal("Units").remove();
    data[output].metadataReal("GroupUnits").setString("OpticalDepth");
    if (FP::defined(wavelength))
        data[output].metadataReal("Wavelength").setDouble(wavelength);
    data[output].metadataReal("Smoothing").remove();
    data[output].metadataReal("Smoothing").hash("Mode").setString("Difference");
}

void CalcOpticalDepth::processMeta(int id, SequenceSegment &data)
{
    if (isAux(id)) {
        id = toID(id);
        Processing *p = &processing[id];
        for (std::vector<Processing::OpticalWavelength>::iterator o = p->optical.begin(),
                endO = p->optical.end(); o != endO; ++o) {
            p->wavelengthTracker.incomingMeta(data, o->tracking->get(data));
        }
        return;
    }

    id = toID(id);
    Processing *p = &processing[id];

    if (p->inputScattering != NULL)
        p->inputScattering->incomingMeta(data);
    if (p->inputBackscattering != NULL)
        p->inputBackscattering->incomingMeta(data);
    if (p->inputAbsorption != NULL)
        p->inputAbsorption->incomingMeta(data);
    if (p->inputExtinction != NULL)
        p->inputExtinction->incomingMeta(data);

    Variant::Root meta;

    meta["By"].setString("calc_opticaldepth");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    {
        const auto &s = p->length.output->get(data);
        if (!s.empty()) {
            auto proc = meta;
            proc["Parameters"].hash("ResetGap") = p->length.resetGap->describe(data.getStart());

            for (auto i : s) {
                i.setMeta();
                data[i].metadataReal("Format").setString("00000.0000");
                data[i].metadataReal("Units").setString("m");
                data[i].metadataReal("Description").setString("Integrated sample length, Qt/A");
                data[i].metadataReal("ReportT").setDouble(0);
                data[i].metadataReal("ReportP").setDouble(1013.25);
                data[i].metadataReal("Processing").toArray().after_back().set(proc);
                data[i].metadataReal("Smoothing").hash("Mode").setString("Difference");
            }
        }
    }


    if (!p->scatteringSuffix.empty())
        meta["Parameters"].hash("Bs").setString(p->scatteringSuffix);
    if (!p->backScatteringSuffix.empty())
        meta["Parameters"].hash("Bbs").setString(p->backScatteringSuffix);
    if (!p->absorptionSuffix.empty())
        meta["Parameters"].hash("Ba").setString(p->absorptionSuffix);
    if (!p->extinctionSuffix.empty())
        meta["Parameters"].hash("Be").setString(p->extinctionSuffix);

    for (std::vector<Processing::OpticalWavelength>::iterator o = p->optical.begin(),
            end = p->optical.end(); o != end; ++o) {
        double wavelength = o->wavelength->get(data);
        if (!FP::defined(wavelength)) {
            for (const auto &i : o->tracking->get(data)) {
                wavelength = p->wavelengthTracker.get(i);
                if (FP::defined(wavelength))
                    break;
            }
        }

        meta["Parameters"].hash("L") = o->lengthStart->describe(data);

        if (p->inputScattering != NULL) {
            auto proc = meta;
            if (o->scattering.synchronousZero->get(data)) {
                proc["Parameters"].hash("SynchronousZero").setBoolean(true);
            }
            proc["Parameters"].hash("ResetGap") = o->scattering.resetGap->describe(data.getStart());

            for (auto i : o->scattering.output->get(data)) {
                i.setMeta();
                o->scattering.opticalDepthMetadata(data, i, p->inputScattering, wavelength);

                data[i].metadata("Description").setString("Total filter scattering optical depth");
                data[i].metadata("Processing").toArray().after_back().set(proc);
            }
        }

        if (p->inputAbsorption != NULL) {
            auto proc = meta;
            if (o->absorption.synchronousZero->get(data)) {
                proc["Parameters"].hash("SynchronousZero").setBool(true);
            }
            proc["Parameters"].hash("ResetGap") = o->absorption.resetGap->describe(data.getStart());

            for (auto i : o->absorption.output->get(data)) {
                i.setMeta();
                o->absorption.opticalDepthMetadata(data, i, p->inputAbsorption, wavelength);

                data[i].metadata("Description").setString("Total filter absorption optical depth");
                data[i].metadata("Processing").toArray().after_back().set(proc);
            }
        }

        if (p->inputExtinction != NULL) {
            auto proc = meta;
            if (o->extinction.synchronousZero->get(data)) {
                proc["Parameters"].hash("SynchronousZero").setBool(true);
            }
            proc["Parameters"].hash("ResetGap") = o->extinction.resetGap->describe(data.getStart());

            for (auto i : o->extinction.output->get(data)) {
                i.setMeta();
                o->extinction.opticalDepthMetadata(data, i, p->inputExtinction, wavelength);

                data[i].metadata("Description").setString("Total filter extinction optical depth");
                data[i].metadata("Processing").toArray().after_back().set(proc);
            }
        }

        if (p->inputScattering && p->inputBackscattering) {
            for (auto i : o->outputAsymmetry->get(data)) {
                i.setMeta();
                data[i].metadataReal("Processing").toArray().after_back().set(meta);
                data[i].metadataReal("Wavelength").setDouble(wavelength);
                data[i].metadataReal("GroupUnits").setString("AsymmetryParameter");
                data[i].metadataReal("Format").setString("00.0000000");
                data[i].metadataReal("MVC").remove();
                data[i].metadataReal("Description")
                       .setString("Total filter light scattering asymmetry parameter");
                if (FP::defined(wavelength)) {
                    data[i].metadataReal("Wavelength").setDouble(wavelength);
                }
                data[i].metadataReal("Smoothing").hash("Mode").setString("Difference");
            }
        }
    }
}

SequenceName::Set CalcOpticalDepth::requestedInputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        if (p->inputScattering != NULL) {
            Util::merge(p->inputScattering->getAllUnits(), out);
            Util::merge(withCoverFlavor(p->inputScattering->getAllUnits()), out);
        }
        if (p->inputBackscattering != NULL) {
            Util::merge(p->inputBackscattering->getAllUnits(), out);
            Util::merge(withCoverFlavor(p->inputBackscattering->getAllUnits()), out);
        }
        if (p->inputAbsorption != NULL) {
            Util::merge(p->inputAbsorption->getAllUnits(), out);
            Util::merge(withCoverFlavor(p->inputAbsorption->getAllUnits()), out);
        }
        if (p->inputExtinction != NULL) {
            Util::merge(p->inputExtinction->getAllUnits(), out);
            Util::merge(withCoverFlavor(p->inputExtinction->getAllUnits()), out);
        }

        Util::merge(p->inputDeltaLength->getUsedInputs(), out);

        Util::merge(p->filterChange->getAllUnits(), out);
        Util::merge(p->spotData->getAllUnits(), out);
        Util::merge(p->flowData->getAllUnits(), out);

        for (std::vector<Processing::OpticalWavelength>::const_iterator o = p->optical.begin(),
                endO = p->optical.end(); o != endO; ++o) {
            Util::merge(o->tracking->getAllUnits(), out);

            Util::merge(o->lengthStart->getUsedInputs(), out);
            Util::merge(o->lengthEnd->getUsedInputs(), out);
            Util::merge(o->deltaLength->getUsedInputs(), out);

            if (o->scattering.flags != NULL)
                Util::merge(o->scattering.flags->getAllUnits(), out);
            if (o->backScattering.flags != NULL)
                Util::merge(o->backScattering.flags->getAllUnits(), out);
            if (o->absorption.flags != NULL)
                Util::merge(o->absorption.flags->getAllUnits(), out);
            if (o->extinction.flags != NULL)
                Util::merge(o->extinction.flags->getAllUnits(), out);
        }
    }
    return out;
}

SequenceName::Set CalcOpticalDepth::predictedOutputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        Util::merge(p->length.output->getAllUnits(), out);
        Util::merge(withEndFlavor(p->length.output->getAllUnits()), out);

        for (std::vector<Processing::OpticalWavelength>::const_iterator o = p->optical.begin(),
                endO = p->optical.end(); o != endO; ++o) {
            Util::merge(o->outputAsymmetry->getAllUnits(), out);
            Util::merge(withEndFlavor(o->outputAsymmetry->getAllUnits()), out);

            Util::merge(o->scattering.output->getAllUnits(), out);
            Util::merge(withEndFlavor(o->scattering.output->getAllUnits()), out);

            Util::merge(o->absorption.output->getAllUnits(), out);
            Util::merge(withEndFlavor(o->absorption.output->getAllUnits()), out);

            Util::merge(o->extinction.output->getAllUnits(), out);
            Util::merge(withEndFlavor(o->extinction.output->getAllUnits()), out);
        }
    }
    return out;
}

QSet<double> CalcOpticalDepth::metadataBreaks(int id)
{
    QSet<double> breaks;
    if (!isMain(id))
        return breaks;
    id = toID(id);

    if (processing[id].inputScattering != NULL)
        Util::merge(processing[id].inputScattering->getChangedPoints(), breaks);
    if (processing[id].inputBackscattering != NULL)
        Util::merge(processing[id].inputBackscattering->getChangedPoints(), breaks);
    if (processing[id].inputAbsorption != NULL)
        Util::merge(processing[id].inputAbsorption->getChangedPoints(), breaks);
    if (processing[id].inputExtinction != NULL)
        Util::merge(processing[id].inputExtinction->getChangedPoints(), breaks);

    Util::merge(processing[id].inputDeltaLength->getChangedPoints(), breaks);
    Util::merge(processing[id].filterChange->getChangedPoints(), breaks);
    Util::merge(processing[id].spotData->getChangedPoints(), breaks);
    Util::merge(processing[id].flowData->getChangedPoints(), breaks);

    Util::merge(processing[id].length.output->getChangedPoints(), breaks);
    Util::merge(processing[id].length.resetGap->getChangedPoints(), breaks);

    for (std::vector<Processing::OpticalWavelength>::const_iterator
            o = processing[id].optical.begin(); o != processing[id].optical.end(); ++o) {
        Util::merge(o->tracking->getChangedPoints(), breaks);
        Util::merge(o->wavelength->getChangedPoints(), breaks);
        Util::merge(o->lengthStart->getChangedPoints(), breaks);
        Util::merge(o->lengthEnd->getChangedPoints(), breaks);
        Util::merge(o->deltaLength->getChangedPoints(), breaks);

        Util::merge(o->outputAsymmetry->getChangedPoints(), breaks);

        Util::merge(o->scattering.output->getChangedPoints(), breaks);
        if (o->scattering.flags != NULL)
            Util::merge(o->scattering.flags->getChangedPoints(), breaks);
        Util::merge(o->scattering.synchronousZero->getChangedPoints(), breaks);
        Util::merge(o->scattering.resetGap->getChangedPoints(), breaks);

        Util::merge(o->backScattering.output->getChangedPoints(), breaks);
        if (o->backScattering.flags != NULL)
            Util::merge(o->backScattering.flags->getChangedPoints(), breaks);
        Util::merge(o->backScattering.synchronousZero->getChangedPoints(), breaks);
        Util::merge(o->backScattering.resetGap->getChangedPoints(), breaks);

        Util::merge(o->absorption.output->getChangedPoints(), breaks);
        if (o->absorption.flags != NULL)
            Util::merge(o->absorption.flags->getChangedPoints(), breaks);
        Util::merge(o->absorption.synchronousZero->getChangedPoints(), breaks);
        Util::merge(o->absorption.resetGap->getChangedPoints(), breaks);

        Util::merge(o->extinction.output->getChangedPoints(), breaks);
        if (o->extinction.flags != NULL)
            Util::merge(o->extinction.flags->getChangedPoints(), breaks);
        Util::merge(o->extinction.synchronousZero->getChangedPoints(), breaks);
        Util::merge(o->extinction.resetGap->getChangedPoints(), breaks);
    }
    return breaks;
}

CalcOpticalDepth::CalcOpticalDepth(QDataStream &stream)
{
    stream >> defaultSynchronousZero;
    stream >> defaultResetGap;
    stream >> defaultScattering;
    stream >> defaultBackscattering;
    stream >> defaultAbsorption;
    stream >> defaultExtiniction;
    stream >> defaultTransmittance;
    stream >> defaultLengthStart;
    stream >> defaultLengthEnd;
    stream >> defaultDeltaLength;
    stream >> filterSuffixes;
    stream >> scatteringSuffixes;
    stream >> absorptionSuffixes;
    stream >> extinctionSuffixes;
    stream >> defaultCalculateL;
    stream >> restrictedInputs;

    quint32 n;
    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        SequenceName u;
        stream >> u;

        Accumulator a;
        stream >> a.lastEndTime;
        stream >> a.priorInput;
        stream >> a.sum;

        accumulators.insert(u, a);
    }

    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        SequenceName u;
        stream >> u;

        AuxiliaryVariable a;
        stream >> a.prior;
        stream >> a.changeTime;

        auxiliary.insert(u, a);
    }

    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        Processing p;
        stream >> p.inputScattering;
        stream >> p.inputBackscattering;
        stream >> p.inputAbsorption;
        stream >> p.inputExtinction;
        stream >> p.wavelengthTracker;
        stream >> p.inputDeltaLength;
        stream >> p.filterChange;
        stream >> p.spotData;
        stream >> p.flowData;
        stream >> p.filterChangeTime;
        stream >> p.suffixUnit;
        stream >> p.scatteringSuffix;
        stream >> p.backScatteringSuffix;
        stream >> p.absorptionSuffix;
        stream >> p.extinctionSuffix;
        stream >> p.auxiliaryBind;

        stream >> p.length.resetGap;
        stream >> p.length.output;

        quint32 m;
        stream >> m;
        for (int j = 0; j < (int) m; j++) {
            Processing::OpticalWavelength o;
            stream >> o.tracking;
            stream >> o.wavelength;
            stream >> o.transmittanceData;
            stream >> o.lengthStart;
            stream >> o.lengthEnd;
            stream >> o.deltaLength;
            stream >> o.outputAsymmetry;
            stream >> o.lastTransmittance;
            stream >> o.lastLength;
            stream >> o.lastValidDeltaLength;

            stream >> o.scattering.synchronousZero;
            stream >> o.scattering.resetGap;
            stream >> o.scattering.flags;
            stream >> o.scattering.output;

            stream >> o.backScattering.synchronousZero;
            stream >> o.backScattering.resetGap;
            stream >> o.backScattering.flags;
            stream >> o.backScattering.output;

            stream >> o.absorption.synchronousZero;
            stream >> o.absorption.resetGap;
            stream >> o.absorption.flags;
            stream >> o.absorption.output;

            stream >> o.extinction.synchronousZero;
            stream >> o.extinction.resetGap;
            stream >> o.extinction.flags;
            stream >> o.extinction.output;

            p.optical.push_back(o);
        }

        processing.push_back(p);
    }
}

void CalcOpticalDepth::serialize(QDataStream &stream)
{
    stream << defaultSynchronousZero;
    stream << defaultResetGap;
    stream << defaultScattering;
    stream << defaultBackscattering;
    stream << defaultAbsorption;
    stream << defaultExtiniction;
    stream << defaultTransmittance;
    stream << defaultLengthStart;
    stream << defaultLengthEnd;
    stream << defaultDeltaLength;
    stream << filterSuffixes;
    stream << scatteringSuffixes;
    stream << absorptionSuffixes;
    stream << extinctionSuffixes;
    stream << defaultCalculateL;
    stream << restrictedInputs;

    quint32 n = accumulators.size();
    stream << n;
    for (QHash<SequenceName, Accumulator>::const_iterator a = accumulators.constBegin(),
            endA = accumulators.constEnd(); a != endA; ++a) {
        stream << a.key();

        stream << a->lastEndTime;
        stream << a->priorInput;
        stream << a->sum;
    }

    n = auxiliary.size();
    stream << n;
    for (QHash<SequenceName, AuxiliaryVariable>::const_iterator a = auxiliary.constBegin(),
            endA = auxiliary.constEnd(); a != endA; ++a) {
        stream << a.key();

        stream << a->prior;
        stream << a->changeTime;
    }

    n = processing.size();
    stream << n;
    for (int i = 0; i < (int) n; i++) {
        stream << processing[i].inputScattering;
        stream << processing[i].inputBackscattering;
        stream << processing[i].inputAbsorption;
        stream << processing[i].inputExtinction;
        stream << processing[i].wavelengthTracker;
        stream << processing[i].inputDeltaLength;
        stream << processing[i].filterChange;
        stream << processing[i].spotData;
        stream << processing[i].flowData;
        stream << processing[i].filterChangeTime;
        stream << processing[i].suffixUnit;
        stream << processing[i].scatteringSuffix;
        stream << processing[i].backScatteringSuffix;
        stream << processing[i].absorptionSuffix;
        stream << processing[i].extinctionSuffix;
        stream << processing[i].auxiliaryBind;

        stream << processing[i].length.resetGap;
        stream << processing[i].length.output;

        quint32 m = processing[i].optical.size();
        stream << m;
        for (int j = 0; j < (int) m; j++) {
            stream << processing[i].optical[j].tracking;
            stream << processing[i].optical[j].wavelength;
            stream << processing[i].optical[j].transmittanceData;
            stream << processing[i].optical[j].lengthStart;
            stream << processing[i].optical[j].lengthEnd;
            stream << processing[i].optical[j].deltaLength;
            stream << processing[i].optical[j].outputAsymmetry;
            stream << processing[i].optical[j].lastTransmittance;
            stream << processing[i].optical[j].lastLength;
            stream << processing[i].optical[j].lastValidDeltaLength;

            stream << processing[i].optical[j].scattering.synchronousZero;
            stream << processing[i].optical[j].scattering.resetGap;
            stream << processing[i].optical[j].scattering.flags;
            stream << processing[i].optical[j].scattering.output;

            stream << processing[i].optical[j].backScattering.synchronousZero;
            stream << processing[i].optical[j].backScattering.resetGap;
            stream << processing[i].optical[j].backScattering.flags;
            stream << processing[i].optical[j].backScattering.output;

            stream << processing[i].optical[j].absorption.synchronousZero;
            stream << processing[i].optical[j].absorption.resetGap;
            stream << processing[i].optical[j].absorption.flags;
            stream << processing[i].optical[j].absorption.output;

            stream << processing[i].optical[j].extinction.synchronousZero;
            stream << processing[i].optical[j].extinction.resetGap;
            stream << processing[i].optical[j].extinction.flags;
            stream << processing[i].optical[j].extinction.output;
        }
    }
}


QString CalcOpticalDepthComponent::getBasicSerializationName() const
{ return QString::fromLatin1("calc_opticaldepth"); }

ComponentOptions CalcOpticalDepthComponent::getOptions()
{
    ComponentOptions options;

    options.add("transmittance", new DynamicSequenceSelectionOption(tr("transmittance", "name"),
                                                                    tr("Input transmittance"),
                                                                    tr("This is the input transmittance used to detect filter "
                                                                       "changes.  When set this is used for all channels and all "
                                                                       "instruments so it normally only makes sense for a single "
                                                                       "channel."),
                                                                    tr("Resolved automatically by instrument suffix")));

    options.add("length-start",
                new DynamicInputOption(tr("length-start", "name"), tr("Input start sample length"),
                                       tr("This is the input start sample length used in the calculation.  "
                                          "When this option is set, this length is used for all "
                                          "channels."),
                                       tr("Resolved automatically by instrument suffix")));

    options.add("length-end",
                new DynamicInputOption(tr("length-end", "name"), tr("Input end sample length"),
                                       tr("This is the input end sample length used in the calculation.  "
                                          "When this option is set, this length is used for all "
                                          "channels."),
                                       tr("Resolved automatically by instrument suffix")));

    options.add("delta-l", new DynamicInputOption(tr("dl", "name"), tr("Input delta sample length"),
                                                  tr("This is the change in sample length used for each calculation "
                                                     "interval.  When omitted it is calculated from the start and end "
                                                     "sample lengths."), QString()));

    options.add("synchronous-zero", new DynamicBoolOption(tr("synchronous-zero", "name"),
                                                          tr("Enable synchronous zeroing"),
                                                          tr("When enabled, this option causes zero change to the integrated "
                                                             "optical depths when their corresponding instrument reports a "
                                                             "zero in its flags.  When not present the last known value "
                                                             "continues to be integrated.  This is normally used when the "
                                                             "filter based instrument is sampling from a pickoff downstream "
                                                             "of the zeroing instrument."),
                                                          QString()));

    TimeIntervalSelectionOption *t = new TimeIntervalSelectionOption(tr("reset-gap", "name"),
                                                                     tr("Maximum gap before resetting"),
                                                                     tr("This is the maximum time allowed between accumulated "
                                                                        "measurements.  When this time is exceeded, the integration is "
                                                                        "reset (a new filter begins).  Invalid but present measurements "
                                                                        "are not considered gaps."),
                                                                     QString());
    t->setAllowZero(true);
    t->setAllowUndefined(true);
    options.add("reset-gap", t);

    options.add("calculate-l", new ComponentOptionBoolean(tr("calculate-l", "name"),
                                                          tr("Enable length recalculation"),
                                                          tr("This enables recalculation of the sample length from the spot "
                                                             "area and flow.  If the variables containing the spot area and "
                                                             "flow are not present in the input then the option to set the "
                                                             "incremental change in length most be set."),
                                                          QString()));


    options.add("scattering",
                new DynamicSequenceSelectionOption(tr("scattering", "name"), tr("Input scattering"),
                                                   tr("This is the input light scattering to use in optical depth "
                                                      "generation.  This option is mutually exclusive with "
                                                      "scattering instrument specification."),
                                                   tr("All light scatterings")));

    options.add("backscattering", new DynamicSequenceSelectionOption(tr("backscattering", "name"),
                                                                     tr("Input backscattering"),
                                                                     tr("This is the input light backscattering to use in optical depth "
                                                                        "generation.  This option is mutually exclusive with "
                                                                        "scattering instrument specification."),
                                                                     tr("All light backscatterings")));

    options.add("absorption",
                new DynamicSequenceSelectionOption(tr("absorption", "name"), tr("Input absorption"),
                                                   tr("This is the input light absorption to use in optical depth "
                                                      "generation.  This option is mutually exclusive with "
                                                      "absorption instrument specification."),
                                                   tr("All light absorptions")));

    options.add("extinction",
                new DynamicSequenceSelectionOption(tr("extinction", "name"), tr("Input extinction"),
                                                   tr("This is the input light extinction to use in optical depth "
                                                      "generation.  This option is mutually exclusive with "
                                                      "extinction instrument specification."),
                                                   tr("All light extinctions")));


    options.add("scattering-suffix",
                new ComponentOptionInstrumentSuffixSet(tr("scattering-instruments", "name"),
                                                       tr("Scattering instrument suffixes"),
                                                       tr("These are the instrument suffixes to use scattering data from.  "
                                                          "For example S11 would usually specifies the reference "
                                                          "nephelometer.  This option is mutually exclusive with "
                                                          "manual scattering variable specification."),
                                                       tr("All instrument suffixes")));
    options.exclude("scattering", "scattering-suffix");
    options.exclude("backscattering", "scattering-suffix");
    options.exclude("scattering-suffix", "scattering");

    options.add("absorption-suffix",
                new ComponentOptionInstrumentSuffixSet(tr("absorption-instruments", "name"),
                                                       tr("Absorption instrument suffixes"),
                                                       tr("These are the instrument suffixes to use absorption data from.  "
                                                          "For example A11 would usually specifies the primary absorption "
                                                          "instrument.  This option is mutually exclusive with "
                                                          "manual absorption variable specification."),
                                                       tr("All instrument suffixes")));
    options.exclude("absorption", "absorption-suffix");
    options.exclude("absorption-suffix", "absorption");

    options.add("extinction-suffix",
                new ComponentOptionInstrumentSuffixSet(tr("extinction-instruments", "name"),
                                                       tr("Extinction instrument suffixes"),
                                                       tr("These are the instrument suffixes to use extinction data from.  "
                                                          "For example E11 would usually specifies the first extinction "
                                                          "instrument.  This option is mutually exclusive with "
                                                          "manual extinction variable specification."),
                                                       tr("All instrument suffixes")));
    options.exclude("extinction", "extinction-suffix");
    options.exclude("extinction-suffix", "extinction");

    options.add("suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments", "name"),
                                                                 tr("Instrument suffixes to calculate"),
                                                                 tr("These are the instrument suffixes to calculate for.  "
                                                                    "For example S11 would usually specifies the reference nephelometer."),
                                                                 tr("All instrument suffixes")));

    return options;
}

QList<ComponentExample> CalcOpticalDepthComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will calculate all optical depths for any instrument that "
                                        "provides a transmittance using any available extensive "
                                        "parameters.")));

    options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")))->add("A11");
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("scattering-suffix")))->add(
            "S12");
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("absorption-suffix")))->add(
            "A12");
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("extinction-suffix")))->clear();
    examples.append(ComponentExample(options, tr("Restricted instruments"),
                                     tr("This will calculate scattering optical depth from S12 and "
                                        "absorption optical depth from A12 based on the filter of A11 "
                                        "regardless of what data is present in the input.")));

    options = getOptions();
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("scattering")))->set("", "",
                                                                                     "BsG_S11");
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("backscattering")))->set("", "",
                                                                                         "BbsG_S11");
    examples.append(ComponentExample(options, tr("Explicit scattering"),
                                     tr("This will calculate optical depth using only green scattering from "
                                        "the S11 instrument.")));

    return examples;
}

SegmentProcessingStage *CalcOpticalDepthComponent::createBasicFilterDynamic(const ComponentOptions &options)
{ return new CalcOpticalDepth(options); }

SegmentProcessingStage *CalcOpticalDepthComponent::createBasicFilterPredefined(const ComponentOptions &options,
                                                                               double start,
                                                                               double end,
                                                                               const QList<
                                                                                       SequenceName> &inputs)
{ return new CalcOpticalDepth(options, start, end, inputs); }

SegmentProcessingStage *CalcOpticalDepthComponent::createBasicFilterEditing(double start,
                                                                            double end,
                                                                            const SequenceName::Component &station,
                                                                            const SequenceName::Component &archive,
                                                                            const ValueSegment::Transfer &config)
{ return new CalcOpticalDepth(start, end, station, archive, config); }

SegmentProcessingStage *CalcOpticalDepthComponent::deserializeBasicFilter(QDataStream &stream)
{ return new CalcOpticalDepth(stream); }
