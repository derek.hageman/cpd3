/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CALCOPTICALDEPTH_H
#define CALCOPTICALDEPTH_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QList>

#include "core/component.hxx"
#include "core/number.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "editing/wavelengthadjust.hxx"

class CalcOpticalDepth : public CPD3::Data::SegmentProcessingStage {
    struct AccumulationParameters {
        CPD3::Data::DynamicBool *synchronousZero;
        CPD3::Data::DynamicTimeInterval *resetGap;
        CPD3::Data::DynamicSequenceSelection *flags;

        CPD3::Data::DynamicSequenceSelection *output;

        AccumulationParameters() : synchronousZero(NULL), resetGap(NULL), flags(NULL), output(NULL)
        { }

        void opticalDepthMetadata(CPD3::Data::SequenceSegment &data,
                                  const CPD3::Data::SequenceName &output,
                                  const CPD3::Editing::WavelengthAdjust *input,
                                  double wavelength) const;
    };

    struct Accumulator {
        double lastEndTime;
        double priorInput;
        double sum;

        void reset();

        void process(CPD3::Data::SequenceSegment &data,
                     double add, const CPD3::Data::SequenceName &output,
                     AccumulationParameters &parameters);

        void process(CPD3::Data::SequenceSegment &data,
                     CPD3::Editing::WavelengthAdjust *input,
                     double wavelength,
                     double dL, const CPD3::Data::SequenceName &output,
                     AccumulationParameters &parameters);

        Accumulator() : lastEndTime(CPD3::FP::undefined()),
                        priorInput(CPD3::FP::undefined()),
                        sum(CPD3::FP::undefined())
        { }
    };

    QHash<CPD3::Data::SequenceName, Accumulator> accumulators;

    struct AuxiliaryVariable {
        CPD3::Data::Variant::Root prior;
        double changeTime;

        void update(CPD3::Data::SequenceSegment &data, const CPD3::Data::SequenceName &unit);

        AuxiliaryVariable() : prior(), changeTime(CPD3::FP::undefined())
        { }
    };

    QHash<CPD3::Data::SequenceName, AuxiliaryVariable> auxiliary;

    struct Processing {
        CPD3::Editing::WavelengthAdjust *inputScattering;
        CPD3::Editing::WavelengthAdjust *inputBackscattering;
        CPD3::Editing::WavelengthAdjust *inputAbsorption;
        CPD3::Editing::WavelengthAdjust *inputExtinction;
        CPD3::Editing::WavelengthTracker wavelengthTracker;

        CPD3::Data::DynamicInput *inputDeltaLength;

        CPD3::Data::DynamicSequenceSelection *filterChange;
        CPD3::Data::DynamicSequenceSelection *spotData;
        CPD3::Data::DynamicSequenceSelection *flowData;

        double filterChangeTime;

        AccumulationParameters length;

        struct OpticalWavelength {
            CPD3::Data::DynamicSequenceSelection *tracking;
            CPD3::Data::DynamicDouble *wavelength;

            CPD3::Data::DynamicSequenceSelection *transmittanceData;
            CPD3::Data::DynamicInput *lengthStart;
            CPD3::Data::DynamicInput *lengthEnd;
            CPD3::Data::DynamicInput *deltaLength;

            CPD3::Data::DynamicSequenceSelection *outputAsymmetry;

            double lastTransmittance;
            double lastLength;
            double lastValidDeltaLength;

            AccumulationParameters scattering;
            AccumulationParameters backScattering;
            AccumulationParameters absorption;
            AccumulationParameters extinction;

            OpticalWavelength() : tracking(NULL),
                                  wavelength(NULL),
                                  transmittanceData(NULL),
                                  lengthStart(NULL),
                                  lengthEnd(NULL),
                                  deltaLength(NULL),
                                  outputAsymmetry(NULL),
                                  lastTransmittance(CPD3::FP::undefined()),
                                  lastLength(CPD3::FP::undefined()),
                                  lastValidDeltaLength(CPD3::FP::undefined())
            { }
        };

        std::vector<OpticalWavelength> optical;

        CPD3::Data::SequenceName suffixUnit;

        CPD3::Data::SequenceName::Component scatteringSuffix;
        CPD3::Data::SequenceName::Component backScatteringSuffix;
        CPD3::Data::SequenceName::Component absorptionSuffix;
        CPD3::Data::SequenceName::Component extinctionSuffix;

        CPD3::Data::SequenceName::Set auxiliaryBind;

        Processing() : inputScattering(NULL),
                       inputBackscattering(NULL),
                       inputAbsorption(NULL),
                       inputExtinction(NULL),
                       wavelengthTracker(),
                       inputDeltaLength(NULL),
                       filterChange(NULL),
                       spotData(NULL),
                       flowData(NULL),
                       filterChangeTime(CPD3::FP::undefined())
        { }
    };

    CPD3::Data::DynamicBool *defaultSynchronousZero;
    CPD3::Data::DynamicTimeInterval *defaultResetGap;
    CPD3::Editing::WavelengthAdjust *defaultScattering;
    CPD3::Editing::WavelengthAdjust *defaultBackscattering;
    CPD3::Editing::WavelengthAdjust *defaultAbsorption;
    CPD3::Editing::WavelengthAdjust *defaultExtiniction;
    CPD3::Data::DynamicSequenceSelection *defaultTransmittance;
    CPD3::Data::DynamicInput *defaultLengthStart;
    CPD3::Data::DynamicInput *defaultLengthEnd;
    CPD3::Data::DynamicInput *defaultDeltaLength;
    CPD3::Data::SequenceName::ComponentSet filterSuffixes;
    CPD3::Data::SequenceName::ComponentSet scatteringSuffixes;
    CPD3::Data::SequenceName::ComponentSet absorptionSuffixes;
    CPD3::Data::SequenceName::ComponentSet extinctionSuffixes;
    bool defaultCalculateL;

    bool restrictedInputs;

    void handleOptions(const CPD3::ComponentOptions &options);

    std::vector<Processing> processing;

    AuxiliaryVariable &lookupAuxiliary(const CPD3::Data::SequenceName &unit,
                                       Processing &processing);

    void handleNewProcessing(const CPD3::Data::SequenceName &unit,
                             int id,
                             CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control);

    void createScattering(const CPD3::Data::SequenceName &unit,
                          int id,
                          CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                          const CPD3::Data::SequenceName::Component &suffix);

    void createBackscattering(const CPD3::Data::SequenceName &unit,
                              int id,
                              CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                              const CPD3::Data::SequenceName::Component &suffix);

    void createAbsorption(const CPD3::Data::SequenceName &unit,
                          int id,
                          CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                          const CPD3::Data::SequenceName::Component &suffix);

    void createExtinction(const CPD3::Data::SequenceName &unit,
                          int id,
                          CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                          const CPD3::Data::SequenceName::Component &suffix);

    void createOptical(const CPD3::Data::SequenceName &unit,
                       int id,
                       CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                       const CPD3::Data::SequenceName::Component &wavelengthSuffix);

    void registerPossibleInput(const CPD3::Data::SequenceName &unit,
                               CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                               int filterID = -1);

    bool initialHandler(const CPD3::Data::SequenceName &unit,
                        CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control);

public:
    CalcOpticalDepth();

    CalcOpticalDepth(const CPD3::ComponentOptions &options);

    CalcOpticalDepth(const CPD3::ComponentOptions &options,
                     double start, double end, const QList<CPD3::Data::SequenceName> &inputs);

    CalcOpticalDepth(double start,
                     double end,
                     const CPD3::Data::SequenceName::Component &station,
                     const CPD3::Data::SequenceName::Component &archive,
                     const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CalcOpticalDepth();

    void unhandled(const CPD3::Data::SequenceName &unit,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;

    QSet<double> metadataBreaks(int id) override;

    CalcOpticalDepth(QDataStream &stream);

    void serialize(QDataStream &stream) override;
};

class CalcOpticalDepthComponent
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.calc_opticaldepth"
                              FILE
                              "calc_opticaldepth.json")

public:
    virtual QString getBasicSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Data::SegmentProcessingStage
            *createBasicFilterDynamic(const CPD3::ComponentOptions &options);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined
            (const CPD3::ComponentOptions &options,
             double start,
             double end, const QList<CPD3::Data::SequenceName> &inputs);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                         double end,
                                                                         const CPD3::Data::SequenceName::Component &station,
                                                                         const CPD3::Data::SequenceName::Component &archive,
                                                                         const CPD3::Data::ValueSegment::Transfer &config);

    virtual void extendBasicFilterEditing(double &start,
                                          double &end,
                                          const CPD3::Data::SequenceName::Component &station,
                                          const CPD3::Data::SequenceName::Component &archive,
                                          const CPD3::Data::ValueSegment::Transfer &config,
                                          CPD3::Data::Archive::Access *access);

    virtual CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream);
};

#endif
