/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef IMPORTCPD2_H
#define IMPORTCPD2_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QIODevice>
#include <QString>
#include <QHash>
#include <QVector>
#include <QList>

#include "core/component.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/root.hxx"
#include "core/waitutils.hxx"

class CPD2Record {
public:
    CPD2Record();

    double start;
    CPD3::Data::SequenceName::Component station;
    CPD3::Data::SequenceName::Component suffix;
    QHash<CPD3::Data::SequenceName, double> reals;
    QHash<CPD3::Data::SequenceName, QVector<double> > arrayReals;
    QHash<CPD3::Data::SequenceName, qint64> integers;
    QHash<CPD3::Data::SequenceName, QString> strings;
    CPD3::Data::Variant::Flags localFlags;
    CPD3::Data::SequenceName::Flavors allFlavors;

    inline double getStart() const
    { return start; }
};

class CPD2LookupParser {
private:
    CPD3::Data::SequenceName unit;
    QString mvc;
protected:
    CPD2LookupParser(const CPD3::Data::SequenceName &unit, const QString &setMVC = QString());

public:
    CPD2LookupParser();

    virtual ~CPD2LookupParser()
    { };

    virtual void parse(const QString &str,
                       CPD2Record &target,
                       CPD2Record *prior = NULL,
                       CPD2Record *next = NULL) const;

    inline CPD3::Data::SequenceName getUnit() const
    { return unit; }

    inline QString getMVC() const
    { return mvc; }
};

class CPD2Lookup {
public:
    CPD2Lookup();

    CPD2Lookup(const CPD2Lookup &other);

    CPD2Lookup &operator=(const CPD2Lookup &other);

    CPD2Lookup(int setType);

    int type;
    CPD3::Data::SequenceName::Component suffix;

    void apply(const QStringList &fields,
               CPD2Record &target,
               CPD2Record *prior = NULL,
               CPD2Record *next = NULL) const;

    QVector<CPD2LookupParser *> parsers;
    QVector<QString> mvcs;

    int idxStation;
    int idxEpoch;
    int idxDateTime;
    int idxYear;
    int idxDOY;
};

class DumpSortLookup;

class ImportCPD2 : public CPD3::Data::ExternalSourceProcessor {
    QByteArray processBuffer;
    bool processEnded;

    CPD3::Data::SequenceName::Map<CPD3::Data::Variant::Root> metaConstant;
    CPD3::Data::SequenceValue::Transfer metaTime;

    QVector<QList<CPD2Record> > records;
    QVector<CPD2Record> nextRecords;
    QVector<double> priorDurations;

    QHash<QString, CPD2Lookup> lookup;

    bool inHeader;
    QHash<QString, QString> headers;

    CPD3::ElapsedTimer dumpTimer;

    CPD3::Data::SequenceName::Component archive;
    int maxBuffered;
    int maxBufferTime;
    bool translateLegacyFlags;
    bool translateSystemFlags;
    bool translateNephFlags;
    bool translatePSAPFlags;
    bool translateArrays;
    bool translateEnds;
    bool enableCutSizeFlags;
    bool explicitCutSizeFlags;

    CPD3::Data::SequenceName::Set outputPrediction;

    void processHeaders();

    void dumpBuffers(bool all);

    void runOutputPrediction();

    void processLine(const QString &line);

    bool handleIncoming(bool inPrepare);

public:
    ImportCPD2(const CPD3::ComponentOptions &options);

    ~ImportCPD2() override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;

protected:
    bool prepare() override;

    bool process() override;
};

class ImportCPD2Component : public QObject, virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.import_cpd2"
                              FILE
                              "import_cpd2.json")

public:
    virtual QString getComponentName() const;

    virtual QString getComponentDisplayName() const;

    virtual QString getComponentDescription() const;

    virtual CPD3::ComponentOptions getOptions();

    QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Data::ExternalConverter
            *createDataIngress(const CPD3::ComponentOptions &options = CPD3::ComponentOptions());
};


#endif
