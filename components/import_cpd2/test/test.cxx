/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QBuffer>

#include "datacore/externalsource.hxx"
#include "core/component.hxx"
#include "core/range.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    ExternalConverterComponent *component;

    bool contains(const SequenceValue::Transfer &results, const SequenceValue &check)
    {
        for (const auto &r : results) {
            if (Range::compareStart(r.getStart(), check.getStart()) != 0)
                continue;
            if (Range::compareEnd(r.getEnd(), check.getEnd()) != 0)
                continue;
            if (r.getUnit() != check.getUnit())
                continue;
            if (r.getValue() != check.getValue())
                continue;
            return true;
        }
        return false;
    }

    bool containsMeta(const SequenceValue::Transfer &results, const SequenceValue &check)
    {
        for (auto r = results.begin(), end = results.end(); r != end; ++r) {
            if (Range::compareStart(r->getStart(), check.getStart()) != 0)
                continue;
            if (r->getUnit() != check.getUnit())
                continue;
            if (r->getValue().getType() != check.getValue().getType())
                continue;
            auto merged = Variant::Root::overlay(r->root(), check.root());
            if (merged.read() != r->read())
                continue;

            double lastEnd = r->getEnd();
            ++r;
            for (; r != end; ++r) {
                if (Range::compareStartEnd(r->getStart(), lastEnd) > 0)
                    break;
                if (r->getUnit() != check.getUnit())
                    continue;
                if (r->getValue().getType() != check.getValue().getType())
                    continue;
                merged = Variant::Root::overlay(r->root(), check.root());
                if (merged.read() != r->read())
                    break;
                lastEnd = r->getEnd();
            }

            if (Range::compareEnd(lastEnd, check.getEnd()) != 0)
                return false;

            return true;
        }
        return false;
    }

private slots:

    void initTestCase()
    {
        component =
                qobject_cast<ExternalConverterComponent *>(ComponentLoader::create("import_cpd2"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("archive")));
        QVERIFY(qobject_cast<ComponentOptionSingleInteger *>(options.get("buffer")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("wait")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("translate-legacy-flags")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("translate-system-flags")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("translate-neph-flags")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("translate-psap-flags")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("translate-arrays")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("enable-cut-flags")));
    }

    void importBasic()
    {
        QByteArray data;
        QBuffer device(&data);
        device.open(QIODevice::WriteOnly);
        ComponentOptions options(component->getOptions());
        ExternalConverter *inp = component->createDataIngress(options);
        QVERIFY(inp != NULL);

        inp->start();
        StreamSink::Buffer output;
        inp->setEgress(&output);

        inp->incomingData(QByteArray("!row;colhdr;S11a,S11a;STN;EPOCH;DateTime;BsB_S11\n"));
        inp->incomingData(QByteArray("!row;mvc;S11a,S11a;ZZZ;0;9999-99-99T99:99:99Z;9999.99\n"));
        inp->incomingData(
                QByteArray("!row;varfmt;S11a,S11a;%s;%u;%04d-%02d-%02dT%02d:%02d:%02dZ;%07.2f\n"));
        inp->incomingData(QByteArray("S11a,APP,1262304000,2010-01-01T00:00:00Z,0001.00\n"));
        inp->incomingData(QByteArray("S11a,APP,1262304060,2010-01-01T00:01:00Z,0002.00\n"));
        inp->incomingData(QByteArray("S11a,APP,1262304120,2010-01-01T00:02:00Z,0003.00\n"));
        inp->incomingData(QByteArray("S11a,APP,1262304180,2010-01-01T00:03:00Z,0004.00\n"));
        inp->endData();

        QVERIFY(inp->wait());
        auto predicted = inp->predictedOutputs();
        delete inp;

        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "BsB_S11"), Variant::Root(1.0),
                                       1262304000.0, 1262304060.0)));
        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "BsB_S11"), Variant::Root(2.0),
                                       1262304060.0, 1262304120.0)));
        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "BsB_S11"), Variant::Root(3.0),
                                       1262304120.0, 1262304180.0)));
        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "BsB_S11"), Variant::Root(4.0),
                                       1262304180.0, 1262304240.0)));
        Variant::Root meta;
        meta.write().metadataReal("Format").setString("%07.2f");
        meta.write().metadataReal("MVC").setString("9999.99");
        meta.write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        QVERIFY(containsMeta(output.values(),
                             SequenceValue(SequenceName("app", "raw_meta", "BsB_S11"), meta,
                                           1262304000.0, 1262304240.0)));

        QVERIFY(predicted.count(SequenceName("app", "raw", "BsB_S11")) != 0);
    }

    void buffering()
    {
        QByteArray data;
        QBuffer device(&data);
        device.open(QIODevice::WriteOnly);
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionSingleInteger *>(options.get("buffer"))->set(2);
        qobject_cast<ComponentOptionSingleDouble *>(options.get("wait"))->set(0.0);
        ExternalConverter *inp = component->createDataIngress(options);
        QVERIFY(inp != NULL);

        inp->start();
        StreamSink::Buffer output;
        inp->setEgress(&output);

        inp->incomingData(QByteArray("!row;colhdr;S11a,S11a;STN;EPOCH;DateTime;BsB_S11\n"));
        QTest::qSleep(50);
        inp->incomingData(QByteArray("!row;mvc;S11a,S11a;ZZZ;0;9999-99-99T99:99:99Z;9999.99\n"));
        QTest::qSleep(50);
        inp->incomingData(
                QByteArray("!row;varfmt;S11a,S11a;%s;%u;%04d-%02d-%02dT%02d:%02d:%02dZ;%07.2f\n"));
        QTest::qSleep(50);
        inp->incomingData(QByteArray("S11a,APP,1262304000,2010-01-01T00:00:00Z,0001.00\n"));
        QTest::qSleep(50);
        inp->incomingData(QByteArray("S11a,APP,1262304061,2010-01-01T00:01:01Z,0002.00\n"));
        QTest::qSleep(50);
        inp->incomingData(QByteArray("S11a,APP,1262304123,2010-01-01T00:02:03Z,0003.00\n"));
        QTest::qSleep(50);
        inp->incomingData(QByteArray("S11a,APP,1262304180,2010-01-01T00:03:00Z,0004.00\n"));
        QTest::qSleep(50);
        inp->endData();

        QVERIFY(inp->wait());
        delete inp;

        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "BsB_S11"), Variant::Root(1.0),
                                       1262304000.0, 1262304061.0)));
        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "BsB_S11"), Variant::Root(2.0),
                                       1262304061.0, 1262304122.0)));
        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "BsB_S11"), Variant::Root(3.0),
                                       1262304123.0, 1262304180.0)));
        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "BsB_S11"), Variant::Root(4.0),
                                       1262304180.0, 1262304237.0)));
        Variant::Root meta;
        meta.write().metadataReal("Format").setString("%07.2f");
        meta.write().metadataReal("MVC").setString("9999.99");
        meta.write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        QVERIFY(containsMeta(output.values(),
                             SequenceValue(SequenceName("app", "raw_meta", "BsB_S11"), meta,
                                           1262304000.0, 1262304122.0)));
        QVERIFY(containsMeta(output.values(),
                             SequenceValue(SequenceName("app", "raw_meta", "BsB_S11"), meta,
                                           1262304123.0, 1262304237.0)));
    }

    void convertMeta()
    {
        QByteArray data;
        QBuffer device(&data);
        device.open(QIODevice::WriteOnly);
        ComponentOptions options(component->getOptions());
        ExternalConverter *inp = component->createDataIngress(options);
        QVERIFY(inp != NULL);

        inp->start();
        StreamSink::Buffer output;
        inp->setEgress(&output);

        inp->incomingData(QByteArray("!row;colhdr;S11a,S11a;STN;EPOCH;DateTime;ZBlarg_S11\n"));
        inp->incomingData(QByteArray("!row;mvc;S11a,S11a;ZZZ;0;9999-99-99T99:99:99Z;9999.99\n"));
        inp->incomingData(
                QByteArray("!row;varfmt;S11a,S11a;%s;%u;%04d-%02d-%02dT%02d:%02d:%02dZ;%07.2f\n"));
        inp->incomingData(QByteArray("!var;ZBlarg_S11;FieldDesc,Some description (A Unit)\n"));
        inp->incomingData(QByteArray("!var;ZBlarg_S11;Wavelength;0,450;WL1\n"));
        inp->incomingData(QByteArray("!var;ZBlarg_S11;Wavelength;2010-01-01T00:01:00Z,451;WL2\n"));
        inp->incomingData(QByteArray("S11a,APP,1262304000,2010-01-01T00:00:00Z,0001.00\n"));
        inp->incomingData(QByteArray("S11a,APP,1262304060,2010-01-01T00:01:00Z,0002.00\n"));
        inp->endData();

        QVERIFY(inp->wait());
        delete inp;

        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "ZBlarg_S11"), Variant::Root(1.0),
                                       1262304000.0, 1262304060.0)));
        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "ZBlarg_S11"), Variant::Root(2.0),
                                       1262304060.0, 1262304120.0)));
        Variant::Root meta;
        meta.write().metadataReal("Format").setString("%07.2f");
        meta.write().metadataReal("MVC").setString("9999.99");
        meta.write().metadataReal("Description").setString("Some description");
        meta.write().metadataReal("Units").setString("A Unit");
        QVERIFY(containsMeta(output.values(),
                             SequenceValue(SequenceName("app", "raw_meta", "ZBlarg_S11"), meta,
                                           1262304000.0, 1262304120.0)));
        meta.write().setEmpty();
        meta.write().metadataReal("Wavelength").setReal(450.0);
        meta.write().metadataReal("Source").setString("WL1");
        QVERIFY(containsMeta(output.values(),
                             SequenceValue(SequenceName("app", "raw_meta", "ZBlarg_S11"), meta,
                                           1262304000.0, 1262304060.0)));
        meta.write().metadataReal("Wavelength").setReal(451.0);
        meta.write().metadataReal("Source").setString("WL2");
        QVERIFY(containsMeta(output.values(),
                             SequenceValue(SequenceName("app", "raw_meta", "ZBlarg_S11"), meta,
                                           1262304060.0, 1262304120.0)));
    }

    void convertFlags()
    {
        QByteArray data;
        QBuffer device(&data);
        device.open(QIODevice::WriteOnly);
        ComponentOptions options(component->getOptions());
        ExternalConverter *inp = component->createDataIngress(options);
        QVERIFY(inp != NULL);

        inp->start();
        StreamSink::Buffer output;
        inp->setEgress(&output);

        inp->incomingData(
                QByteArray("!row;colhdr;S11a,S11a;STN;EPOCH;DateTime;F1_S11;F2_S11;F3_S11\n"));
        inp->incomingData(
                QByteArray("!row;mvc;S11a,S11a;ZZZ;0;9999-99-99T99:99:99Z;FFFF;FFFF;F\n"));
        inp->incomingData(QByteArray(
                "!row;varfmt;S11a,S11a;%s;%u;%04d-%02d-%02dT%02d:%02d:%02dZ;%04X;%04X;%01X\n"));
        inp->incomingData(QByteArray("!row;colhdr;A11a,A11a;STN;EPOCH;DateTime;F_aer;F2_A11\n"));
        inp->incomingData(QByteArray("!row;mvc;A11a,A11a;ZZZ;0;9999-99-99T99:99:99Z;FFFF,FFFF\n"));
        inp->incomingData(QByteArray(
                "!row;varfmt;A11a,A11a;%s;%u;%04d-%02d-%02dT%02d:%02d:%02dZ;%04X;%04X;%01X\n"));
        inp->incomingData(QByteArray("S11a,APP,1262304000,2010-01-01T00:00:00Z,0010,0001,1\n"));
        inp->incomingData(QByteArray("A11a,APP,1262304000,2010-01-01T00:00:00Z,0010,0001\n"));
        inp->incomingData(QByteArray("S11a,APP,1262304060,2010-01-01T00:01:00Z,0000,0000,2\n"));
        inp->incomingData(QByteArray("S11a,APP,1262304120,2010-01-01T00:02:00Z,0100,1000,3\n"));
        inp->incomingData(QByteArray("A11a,APP,1262304120,2010-01-01T00:02:00Z,0001,0000\n"));
        inp->incomingData(QByteArray("S11a,APP,1262304180,2010-01-01T00:03:00Z,0000,0000,A\n"));
        inp->endData();

        QVERIFY(inp->wait());
        delete inp;

        QVERIFY(contains(output.values(), SequenceValue(
                SequenceName("app", "raw", "F3_S11", {"pm1"}), Variant::Root(0x1),
                1262304000.0, 1262304060.0)));
        QVERIFY(contains(output.values(), SequenceValue(
                SequenceName("app", "raw", "F3_S11", {"pm10"}), Variant::Root(0x2),
                1262304060.0, 1262304120.0)));
        QVERIFY(contains(output.values(), SequenceValue(
                SequenceName("app", "raw", "F3_S11", {"pm10"}), Variant::Root(0x3),
                1262304120.0, 1262304180.0)));
        QVERIFY(contains(output.values(), SequenceValue(
                SequenceName("app", "raw", "F3_S11", {"pm10"}), Variant::Root(0xA),
                1262304180.0, 1262304240.0)));

        Variant::Root flags;
        flags.write().applyFlag("FilterChanging");
        QVERIFY(contains(output.values(), SequenceValue(
                SequenceName("app", "raw", "F1_A11", {"pm1"}), flags, 1262304000.0,
                1262304120.0)));
        flags.write().setEmpty();
        flags.write().applyFlag("ContaminationAutomatic");
        QVERIFY(contains(output.values(), SequenceValue(
                SequenceName("app", "raw", "F1_A11", {"pm10"}), flags,
                1262304120.0, 1262304240.0)));

        flags.write().setEmpty();
        flags.write().applyFlag("LampPowerError");
        QVERIFY(contains(output.values(), SequenceValue(
                SequenceName("app", "raw", "F1_S11", {"pm1"}), flags, 1262304000.0,
                1262304060.0)));
        flags.write().setEmpty();
        flags.write().setType(Variant::Type::Flags);
        QVERIFY(contains(output.values(), SequenceValue(
                SequenceName("app", "raw", "F1_S11", {"pm10"}), flags,
                1262304060.0, 1262304120.0)));
        flags.write().setEmpty();
        flags.write().applyFlag("BackscatterDisabled");
        flags.write().applyFlag("STP");
        QVERIFY(contains(output.values(), SequenceValue(
                SequenceName("app", "raw", "F1_S11", {"pm10"}), flags,
                1262304120.0, 1262304180.0)));
        flags.write().setEmpty();
        flags.write().setType(Variant::Type::Flags);
        QVERIFY(contains(output.values(), SequenceValue(
                SequenceName("app", "raw", "F1_S11", {"pm10"}), flags,
                1262304180.0, 1262304240.0)));
    }

    void convertArray()
    {
        QByteArray data;
        QBuffer device(&data);
        device.open(QIODevice::WriteOnly);
        ComponentOptions options(component->getOptions());
        ExternalConverter *inp = component->createDataIngress(options);
        QVERIFY(inp != NULL);

        inp->start();
        StreamSink::Buffer output;
        inp->setEgress(&output);

        inp->incomingData(QByteArray(
                "!row;colhdr;N11a,N11a;STN;EPOCH;DateTime;N1_N11;N2_N11;N3_N11;N1s_N11;N2s_N11\n"));
        inp->incomingData(QByteArray(
                "!row;mvc;N11a,N11a;ZZZ;0;9999-99-99T99:99:99Z;99.9;99.9;99.9;99.9;99.9\n"));
        inp->incomingData(QByteArray(
                "!row;varfmt;N11a,N11a;%s;%u;%04d-%02d-%02dT%02d:%02d:%02dZ;%04.1f;%04.1f;%04.1f;%04.1f;%04.1f\n"));
        inp->incomingData(
                QByteArray("N11a,APP,1262304000,2010-01-01T00:00:00Z,01.0,02.0,03.0,04.0,05.0\n"));
        inp->incomingData(
                QByteArray("N11a,APP,1262304060,2010-01-01T00:01:00Z,01.1,02.1,03.1,04.1,05.1\n"));
        inp->incomingData(
                QByteArray("N11a,APP,1262304120,2010-01-01T00:02:00Z,01.2,02.2,03.2,04.2,05.2\n"));
        inp->endData();

        QVERIFY(inp->wait());
        delete inp;

        Variant::Root a(Variant::Type::Array);
        a.write().array(0).setDouble(1.0);
        a.write().array(1).setDouble(2.0);
        a.write().array(2).setDouble(3.0);

        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "Nn_N11"), a, 1262304000.0,
                                       1262304060.0)));
        a.write().setEmpty();
        a.write().array(0).setDouble(4.0);
        a.write().array(1).setDouble(5.0);
        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "Ns_N11"), a, 1262304000.0,
                                       1262304060.0)));

        a.write().setEmpty();
        a.write().array(0).setDouble(1.1);
        a.write().array(1).setDouble(2.1);
        a.write().array(2).setDouble(3.1);
        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "Nn_N11"), a, 1262304060.0,
                                       1262304120.0)));
        a.write().setEmpty();
        a.write().array(0).setDouble(4.1);
        a.write().array(1).setDouble(5.1);
        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "Ns_N11"), a, 1262304060.0,
                                       1262304120.0)));

        a.write().setEmpty();
        a.write().array(0).setDouble(1.2);
        a.write().array(1).setDouble(2.2);
        a.write().array(2).setDouble(3.2);
        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "Nn_N11"), a, 1262304120.0,
                                       1262304180.0)));
        a.write().setEmpty();
        a.write().array(0).setDouble(4.2);
        a.write().array(1).setDouble(5.2);
        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "Ns_N11"), a, 1262304120.0,
                                       1262304180.0)));

        Variant::Root meta;
        meta.write().metadataArray("Children").metadataReal("Format").setString("%04.1f");
        meta.write().metadataArray("Children").metadataReal("MVC").setString("99.9");
        meta.write().metadataArray("Count").setInt64(3);
        QVERIFY(containsMeta(output.values(),
                             SequenceValue(SequenceName("app", "raw_meta", "Nn_N11"), meta,
                                           1262304000.0, 1262304180.0)));

        meta.write().metadataArray("Count").setInt64(2);
        QVERIFY(containsMeta(output.values(),
                             SequenceValue(SequenceName("app", "raw_meta", "Ns_N11"), meta,
                                           1262304000.0, 1262304180.0)));
    }

    void averageCondense()
    {
        QByteArray data;
        QBuffer device(&data);
        device.open(QIODevice::WriteOnly);
        ComponentOptions options(component->getOptions());
        ExternalConverter *inp = component->createDataIngress(options);
        QVERIFY(inp != NULL);

        inp->start();
        StreamSink::Buffer output;
        inp->setEgress(&output);

        inp->incomingData(QByteArray("!fil;ProcessedBy;S11a,data.avg\n"));
        inp->incomingData(QByteArray(
                "!row;colhdr;S11a,S11a;STN;EPOCH;DateTime;F1_S11;BsG0_S11;BsG1_S11;U_S11\n"));
        inp->incomingData(QByteArray(
                "!row;mvc;S11a,S11a;ZZZ;0;9999-99-99T99:99:99Z;FFFF;9999.99;9999.99;999.9\n"));
        inp->incomingData(QByteArray(
                "!row;varfmt;S11a,S11a;%s;%u;%04d-%02d-%02dT%02d:%02d:%02dZ;%04X;%07.2f;%07.2f;%05.1f\n"));
        inp->incomingData(QByteArray(
                "S11a,APP,1262304000,2010-01-01T00:00:00Z,0010,0001.00,0002.00,003.0\n"));
        inp->incomingData(QByteArray(
                "S11a,APP,1262304060,2010-01-01T00:01:00Z,0010,0004.00,0005.00,006.0\n"));
        inp->incomingData(QByteArray(
                "S11a,APP,1262304120,2010-01-01T00:02:00Z,0000,0007.00,0008.00,009.0\n"));
        inp->endData();

        QVERIFY(inp->wait());
        delete inp;

        QVERIFY(contains(output.values(), SequenceValue(
                SequenceName("app", "raw", "BsG_S11", {"pm10"}), Variant::Root(1.0),
                1262304000.0, 1262304060.0)));
        QVERIFY(contains(output.values(), SequenceValue(
                SequenceName("app", "raw", "BsG_S11", {"pm1"}), Variant::Root(2.0),
                1262304000.0, 1262304060.0)));
        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "U_S11"), Variant::Root(3.0),
                                       1262304000.0, 1262304060.0)));

        QVERIFY(contains(output.values(), SequenceValue(
                SequenceName("app", "raw", "BsG_S11", {"pm10"}), Variant::Root(4.0),
                1262304060.0, 1262304120.0)));
        QVERIFY(contains(output.values(), SequenceValue(
                SequenceName("app", "raw", "BsG_S11", {"pm1"}), Variant::Root(5.0),
                1262304060.0, 1262304120.0)));
        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "U_S11"), Variant::Root(6.0),
                                       1262304060.0, 1262304120.0)));

        QVERIFY(contains(output.values(), SequenceValue(
                SequenceName("app", "raw", "BsG_S11", {"pm10"}), Variant::Root(7.0),
                1262304120.0, 1262304180.0)));
        QVERIFY(contains(output.values(), SequenceValue(
                SequenceName("app", "raw", "BsG_S11", {"pm1"}), Variant::Root(8.0),
                1262304120.0, 1262304180.0)));
        QVERIFY(contains(output.values(),
                         SequenceValue(SequenceName("app", "raw", "U_S11"), Variant::Root(9.0),
                                       1262304120.0, 1262304180.0)));

        Variant::Root meta;
        meta.write().metadataReal("Format").setString("%07.2f");
        meta.write().metadataReal("MVC").setString("9999.99");
        QVERIFY(containsMeta(output.values(), SequenceValue(
                SequenceName("app", "raw_meta", "BsG_S11", {"pm10"}), meta,
                1262304000.0, 1262304180.0)));
        QVERIFY(containsMeta(output.values(), SequenceValue(
                SequenceName("app", "raw_meta", "BsG_S11", {"pm1"}), meta,
                1262304000.0, 1262304180.0)));

        meta.write().setEmpty();
        meta.write().metadataReal("Format").setString("%05.1f");
        meta.write().metadataReal("MVC").setString("999.9");
        QVERIFY(containsMeta(output.values(),
                             SequenceValue(SequenceName("app", "raw_meta", "U_S11"), meta,
                                           1262304000.0, 1262304180.0)));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
