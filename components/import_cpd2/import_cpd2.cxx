/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "core/component.hxx"
#include "core/range.hxx"
#include "core/timeparse.hxx"
#include "core/csv.hxx"
#include "core/environment.hxx"
#include "datacore/externalsource.hxx"

#include "import_cpd2.hxx"

using namespace CPD3;
using namespace CPD3::Data;


ImportCPD2::ImportCPD2(const ComponentOptions &options) : processBuffer(), processEnded(false)
{

    inHeader = true;
    dumpTimer.start();

    if (options.isSet("archive")) {
        archive = qobject_cast<ComponentOptionSingleString *>(options.get("archive"))->get()
                                                                                     .toStdString();
    }
    if (archive.length() == 0)
        archive = "raw";

    maxBuffered = 10000;
    if (options.isSet("buffer")) {
        maxBuffered =
                (int) qobject_cast<ComponentOptionSingleInteger *>(options.get("buffer"))->get();
    }
    if (maxBuffered < 2)
        maxBuffered = 2;

    maxBufferTime = 1000;
    if (options.isSet("wait")) {
        double seconds = qobject_cast<ComponentOptionSingleDouble *>(options.get("wait"))->get();
        if (!FP::defined(seconds)) {
            maxBufferTime = -1;
        } else if (seconds <= 0.0) {
            maxBufferTime = 0;
        } else {
            maxBufferTime = (int) ceil(seconds * 1000.0);
            if (maxBufferTime < 1)
                maxBufferTime = 1;
        }
    }

    if (options.isSet("translate-legacy-flags")) {
        translateLegacyFlags = qobject_cast<ComponentOptionBoolean *>(
                options.get("translate-legacy-flags"))->get();
    } else {
        translateLegacyFlags = true;
    }
    if (options.isSet("translate-system-flags")) {
        translateSystemFlags = qobject_cast<ComponentOptionBoolean *>(
                options.get("translate-system-flags"))->get();
    } else {
        translateSystemFlags = true;
    }
    if (options.isSet("translate-neph-flags")) {
        translateNephFlags =
                qobject_cast<ComponentOptionBoolean *>(options.get("translate-neph-flags"))->get();
    } else {
        translateNephFlags = true;
    }
    if (options.isSet("translate-psap-flags")) {
        translatePSAPFlags =
                qobject_cast<ComponentOptionBoolean *>(options.get("translate-psap-flags"))->get();
    } else {
        translatePSAPFlags = true;
    }
    if (options.isSet("translate-arrays")) {
        translateArrays =
                qobject_cast<ComponentOptionBoolean *>(options.get("translate-arrays"))->get();
    } else {
        translateArrays = true;
    }
    if (options.isSet("translate-ends")) {
        translateEnds =
                qobject_cast<ComponentOptionBoolean *>(options.get("translate-ends"))->get();
    } else {
        translateEnds = true;
    }
    if (options.isSet("enable-cut-flags")) {
        enableCutSizeFlags =
                qobject_cast<ComponentOptionBoolean *>(options.get("enable-cut-flags"))->get();
        explicitCutSizeFlags = true;
    } else {
        enableCutSizeFlags = true;
        explicitCutSizeFlags = false;
    }
}

ImportCPD2::~ImportCPD2()
{
    for (QHash<QString, CPD2Lookup>::iterator i = lookup.begin(); i != lookup.end(); ++i) {
        qDeleteAll(i.value().parsers);
    }
}

class HeaderTimeData {
public:
    double start;
    QString value;

    HeaderTimeData(double setStart, const QString &setValue) : start(setStart), value(setValue)
    { }

    HeaderTimeData() : start(FP::undefined()), value()
    { }

    HeaderTimeData(const HeaderTimeData &other) : start(other.start), value(other.value)
    { }

    HeaderTimeData &operator=(const HeaderTimeData &other)
    {
        if (&other == this) return *this;
        start = other.start;
        value = other.value;
        return *this;
    }

    bool operator<(const HeaderTimeData &rhs) const
    { return Range::compareStart(start, rhs.start) < 0; }
};


CPD2Record::CPD2Record() : start(FP::undefined()),
                           station(),
                           suffix(),
                           reals(),
                           arrayReals(),
                           integers(),
                           strings(),
                           localFlags(),
                           allFlavors()
{ }

CPD2Lookup::CPD2Lookup() : type(-1),
                           suffix(),
                           parsers(),
                           mvcs(),
                           idxStation(-1),
                           idxEpoch(-1),
                           idxDateTime(-1),
                           idxYear(-1),
                           idxDOY(-1)
{ }

CPD2Lookup::CPD2Lookup(const CPD2Lookup &other) : type(other.type),
                                                  suffix(other.suffix),
                                                  parsers(other.parsers),
                                                  idxStation(other.idxStation),
                                                  idxEpoch(other.idxEpoch),
                                                  idxDateTime(other.idxDateTime),
                                                  idxYear(other.idxYear),
                                                  idxDOY(other.idxDOY)
{ }

CPD2Lookup &CPD2Lookup::operator=(const CPD2Lookup &other)
{
    if (&other == this) return *this;
    type = other.type;
    suffix = other.suffix;
    parsers = other.parsers;
    idxStation = other.idxStation;
    idxEpoch = other.idxEpoch;
    idxDateTime = other.idxDateTime;
    idxYear = other.idxYear;
    idxDOY = other.idxDOY;
    return *this;
}

CPD2Lookup::CPD2Lookup(int setType) : type(setType),
                                      suffix(),
                                      parsers(),
                                      idxStation(-1),
                                      idxEpoch(-1),
                                      idxDateTime(-1),
                                      idxYear(-1),
                                      idxDOY(-1)
{ }

void CPD2Lookup::apply(const QStringList &fields,
                       CPD2Record &target,
                       CPD2Record *prior,
                       CPD2Record *next) const
{
    for (int i = 0, max = qMin(parsers.size(), fields.size()); i < max; i++) {
        parsers.at(i)->parse(fields.at(i), target, prior, next);
    }
    target.suffix = suffix;
    if (idxStation >= 0 && idxStation < fields.length()) {
        target.station = fields.at(idxStation).toStdString();
    }
    if (idxEpoch >= 0 && idxEpoch < fields.length()) {
        bool ok;
        target.start = fields.at(idxEpoch).toDouble(&ok);
        if (ok) return;
    }
    if (idxYear >= 0 && idxYear < fields.length() && idxDOY >= 0 && idxDOY < fields.length()) {
        bool ok;
        int year = fields.at(idxYear).toInt(&ok);
        if (ok) {
            double doy = fields.at(idxEpoch).toDouble(&ok);
            if (ok && doy >= 1.0 && doy <= 366.0) {
                target.start = Time::convertYearDOY(year, doy);
                return;
            }
        }
    }
    if (idxDateTime >= 0 && idxDateTime < fields.length()) {
        try {
            target.start = TimeParse::parseTime(fields.at(idxDateTime));
            if (FP::defined(target.start))
                return;
        } catch (TimeParsingException tpe) {
        }
    }
}


CPD2LookupParser::CPD2LookupParser(const SequenceName &setUnit, const QString &setMVC) : unit(
        setUnit), mvc(setMVC)
{ }

CPD2LookupParser::CPD2LookupParser() : unit(), mvc()
{ }

void CPD2LookupParser::parse(const QString &str,
                             CPD2Record &target,
                             CPD2Record *prior,
                             CPD2Record *next) const
{
    Q_UNUSED(str);
    Q_UNUSED(target);
    Q_UNUSED(prior);
    Q_UNUSED(next);
}

class CPD2ParserFP : public CPD2LookupParser {
    bool hasEnd;
    bool isNext;
    SequenceName endUnit;
public:
    CPD2ParserFP(const SequenceName &unit, const QString &mvc, bool end, bool next)
            : CPD2LookupParser(unit, mvc), hasEnd(end), isNext(next), endUnit()
    {
        if (hasEnd)
            endUnit = getUnit().withFlavor(SequenceName::flavor_end);
    }

    virtual ~CPD2ParserFP()
    { }

    virtual void parse(const QString &str,
                       CPD2Record &target,
                       CPD2Record *prior = NULL,
                       CPD2Record *next = NULL) const
    {
        if (str == getMVC()) {
            if (isNext && next != NULL) {
                next->reals.insert(getUnit(), FP::undefined());
                return;
            }
            target.reals.insert(getUnit(), FP::undefined());
            return;
        }
        bool ok;
        double v = str.toDouble(&ok);
        if (!ok)
            v = FP::undefined();
        if (isNext && next != NULL) {
            next->reals.insert(getUnit(), v);
            if (hasEnd)
                target.reals.insert(endUnit, v);
            return;
        }

        target.reals.insert(getUnit(), v);
        if (hasEnd && prior != NULL) {
            prior->reals.insert(endUnit, v);
        }
    }
};

class CPD2ParserFPArray : public CPD2LookupParser {
    bool hasEnd;
    bool isNext;
    int idx;
    SequenceName endUnit;

    void insertInto(CPD2Record &target, const SequenceName &unit, double value) const
    {
        QHash<SequenceName, QVector<double> >::iterator lookup = target.arrayReals.find(unit);
        if (lookup == target.arrayReals.end()) {
            lookup = target.arrayReals.insert(unit, QVector<double>(idx + 1, FP::undefined()));
        }
        lookup.value().reserve(idx + 1);
        while (lookup.value().size() <= idx) {
            lookup.value().append(FP::undefined());
        }

        lookup.value()[idx] = value;
    }

public:
    CPD2ParserFPArray(const SequenceName &unit, const QString &mvc, bool end, bool next, int index)
            : CPD2LookupParser(unit, mvc), hasEnd(end), isNext(next), idx(index), endUnit()
    {
        if (hasEnd)
            endUnit = getUnit().withFlavor(SequenceName::flavor_end);
    }

    virtual ~CPD2ParserFPArray()
    { }

    virtual void parse(const QString &str,
                       CPD2Record &target,
                       CPD2Record *prior = NULL,
                       CPD2Record *next = NULL) const
    {
        if (str == getMVC()) {
            if (isNext && next != NULL) {
                insertInto(*next, getUnit(), FP::undefined());
                return;
            }
            insertInto(target, getUnit(), FP::undefined());
            return;
        }
        bool ok;
        double v = str.toDouble(&ok);
        if (!ok)
            v = FP::undefined();
        if (isNext && next != NULL) {
            insertInto(*next, getUnit(), v);
            if (hasEnd)
                insertInto(target, endUnit, v);
            return;
        }

        insertInto(target, getUnit(), v);
        if (hasEnd && prior != NULL) {
            insertInto(*prior, endUnit, v);
        }
    }
};

class CPD2ParserInteger : public CPD2LookupParser {
private:
    int base;
public:
    CPD2ParserInteger(const SequenceName &unit, const QString &mvc, int setBase = 0)
            : CPD2LookupParser(unit, mvc), base(setBase)
    { }

    virtual ~CPD2ParserInteger()
    { }

    virtual void parse(const QString &str,
                       CPD2Record &target,
                       CPD2Record *prior = NULL,
                       CPD2Record *next = NULL) const
    {
        Q_UNUSED(prior);
        Q_UNUSED(next);
        if (str == getMVC()) {
            target.integers.insert(getUnit(), INTEGER::undefined());
            return;
        }
        bool ok;
        qint64 v = str.toLongLong(&ok, base);
        if (!ok)
            v = INTEGER::undefined();
        target.integers.insert(getUnit(), v);
    }
};

class CPD2ParserHex : public CPD2LookupParser {
public:
    CPD2ParserHex(const SequenceName &unit, const QString &mvc) : CPD2LookupParser(unit, mvc)
    { }

    virtual ~CPD2ParserHex()
    { }

    virtual void parse(const QString &str,
                       CPD2Record &target,
                       CPD2Record *prior = NULL,
                       CPD2Record *next = NULL) const
    {
        Q_UNUSED(prior);
        Q_UNUSED(next);
        if (str == getMVC()) {
            target.integers.insert(getUnit(), INTEGER::undefined());
            return;
        }
        qint64 v;
        bool ok;
        if (str.startsWith("0x")) {
            v = str.mid(2).toLongLong(&ok, 16);
        } else {
            v = str.toLongLong(&ok, 16);
        }
        if (!ok)
            v = INTEGER::undefined();
        target.integers.insert(getUnit(), v);
    }
};

class CPD2ParserString : public CPD2LookupParser {
public:
    CPD2ParserString(const SequenceName &unit, const QString &mvc) : CPD2LookupParser(unit, mvc)
    { }

    virtual ~CPD2ParserString()
    { }

    virtual void parse(const QString &str,
                       CPD2Record &target,
                       CPD2Record *prior = NULL,
                       CPD2Record *next = NULL) const
    {
        Q_UNUSED(prior);
        Q_UNUSED(next);
        if (str == getMVC()) {
            target.strings.insert(getUnit(), QString());
            return;
        }
        target.strings.insert(getUnit(), str);
    }
};

static const Variant::Flag legacyFlagsBits[12] =
        {"ContaminationAutomatic", "ContaminationManual", "ContaminationWindRuntime",
         "ContaminationWindPost",

         {}, "LowTransmittance", {}, "ZeroSubtraction",

         "STP", "Bond1999", "AndersonOgren1998", "Dilution"};
static const Variant::Flag systemFlagsBits[12] =
        {"ContaminationAutomatic", "ContaminationManual", "ContaminationWindRuntime",
         "ContaminationWindPost",

         {}, {}, {}, {},

         "STP", {}, {}, "Dilution"};
static const Variant::Flag
        nephFlagsBits[16] = {"LampPowerError", "ValveFault", "ChopperFault", "ShutterFault",

                             "HeaterUnstable", "PressureOutOfRange", "TemperatureOutOfRange",
                             "InletTemperatureOutOfRange",

                             "RHOutOfRange", "STP", "AndersonOgren1998", {},

                             "BackscatterDisabled", "Zero", "Blank", {}};
static const Variant::Flag psapFlagsBits[16] =
        {"FilterChanging", "FlowError", "BlueTransmittanceMedium", "BlueTransmittanceLow",

         "GreenTransmittanceMedium", "GreenTransmittanceLow", "RedTransmittanceMedium",
         "RedTransmittanceLow",

         "LampError", "TemperatureOutOfRange", "CaseTemperatureUnstable", "InstrumentTransmittance",

         "Weiss", "Bond1999", "CTS", {}};

class CPD2ParserFlagsConstant : public CPD2LookupParser {
private:
    const Variant::Flag *list;
    std::size_t n;
    qint64 pm1Bits;
public:
    CPD2ParserFlagsConstant(const SequenceName &unit,
                            const QString &mvc,
                            const Variant::Flag *setList,
                            std::size_t setN,
                            qint64 setPM1Bits) : CPD2LookupParser(unit, mvc),
                                                 list(setList),
                                                 n(setN),
                                                 pm1Bits(setPM1Bits)
    { }

    virtual ~CPD2ParserFlagsConstant()
    { }

    virtual void parse(const QString &str,
                       CPD2Record &target,
                       CPD2Record *prior = NULL,
                       CPD2Record *next = NULL) const
    {
        Q_UNUSED(prior);
        Q_UNUSED(next);
        if (str == getMVC())
            return;
        bool ok;
        std::uint64_t bits = str.toUShort(&ok, 16);
        if (!ok) return;
        for (std::size_t i = 0; i < n; i++) {
            if (bits & (Q_INT64_C(1) << i) && list[i].length() > 0)
                target.localFlags.insert(list[i]);
        }
        if (pm1Bits) {
            if (bits & pm1Bits) {
                target.allFlavors.insert(SequenceName::flavor_pm1);
            } else {
                target.allFlavors.insert(SequenceName::flavor_pm10);
            }
        }
    }
};


static bool dataTimeSort(const SequenceValue &d1, const SequenceValue &d2)
{ return Range::compareStart(d1.getStart(), d2.getStart()) < 0; }

void ImportCPD2::processHeaders()
{
    QHash<QString, QStringList> names;
    QHash<QString, QStringList> formats;
    QHash<QString, QStringList> mvcs;
    QHash<QString, QHash<QString, QString> > variableData;
    QHash<QString, QHash<QString, QList<HeaderTimeData> > > variableTimeData;
    bool isAveraged = false;

    for (QHash<QString, QString>::const_iterator header = headers.constBegin();
            header != headers.constEnd();
            ++header) {
        QString hkey(header.key().toLower());

        if (hkey.startsWith("fil;processedby") &&
                header.value().contains("data.avg", Qt::CaseInsensitive)) {
            isAveraged = true;
        }

        if (hkey.startsWith("row;colhdr;")) {
            QString record(header.key().mid(11));
            QStringList n(header.value().split(';'));
            if (n.size() < 2)
                continue;
            n.removeFirst();
            names.insert(record, n);
            continue;
        }

        if (hkey.startsWith("row;varfmt;")) {
            QString record(header.key().mid(11));
            QStringList n(header.value().split(';'));
            if (n.size() < 2)
                continue;
            n.removeFirst();
            formats.insert(record, n);
            continue;
        }

        if (hkey.startsWith("row;mvc;")) {
            QString record(header.key().mid(8));
            QStringList n(header.value().split(';'));
            if (n.size() < 2)
                continue;
            n.removeFirst();
            mvcs.insert(record, n);
            continue;
        }

        if (hkey.startsWith("var;")) {
            QStringList vd(header.key().split(';'));
            vd.removeFirst();

            if (vd.size() == 2) {
                variableData[vd.at(0)][vd.at(1).toLower()] = header.value();
            } else if (vd.size() == 3) {
                double time;
                if (vd.at(2) == "0") {
                    time = FP::undefined();
                } else {
                    try {
                        time = TimeParse::parseTime(vd.at(2));
                    } catch (TimeParsingException tpe) {
                        continue;
                    }
                }

                variableTimeData[vd.at(0)][vd.at(1).toLower()].append(
                        HeaderTimeData(time, header.value()));
            }

            continue;
        }
    }

    QHash<SequenceName, int> arrayCountSet;
    QHash<QString, QList<SequenceName> > variableFanout;

    Variant::Root processing;
    processing["By"].setString("import_cpd2");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    QRegExp cpd2Number("\\*@?([0 +]?)@?(\\d+)(?:\\.(\\d+))?f", Qt::CaseInsensitive);
    QRegExp cpd2Exp
            ("\\*@?([0 +]?)@?(\\d+)(?:\\.0?(\\d+)(?:\\.(0?\\d+))?)?(e)", Qt::CaseInsensitive);
    QRegExp variablesArray("(N\\D*)(\\d+)(\\D*_N.*)");
    QRegExp variablesMm1("^Bb?[aes][BGRZQO]c?\\d*g?_");
    QRegExp variablescm3("^N\\d*[nb]?\\d*_N");
    QRegExp variablesum("^N\\d*s\\d*_N");
    QRegExp variablesC("^Tu?\\d*g?_");
    QRegExp variablesPCT("^Uu?\\d*g?_");
    QRegExp variableshPa("^P\\d*g?_");
    QRegExp variablesdhPa("^Pd\\d*g?_");
    QRegExp variablesTr("^Ir[BGRZQO]?\\d*g?_");
    QRegExp variablesWithEnd("^(?:(?:Ir[BGRZQO]?\\d*g?)|(?:L\\d*))_");
    QRegExp variablesAtEnd("^(?:(?:L\\d*))_");
    QRegExp descriptionUnitExtract
            ("(.+)\\s*\\(([^\\(]+)\\)(?:\\s+\\(PM(?:1|10|25|2\\.5)\\))?\\s*", Qt::CaseInsensitive);
    for (QHash<QString, QStringList>::const_iterator ni = names.constBegin();
            ni != names.constEnd();
            ++ni) {
        QString record(ni.key());
        QStringList recordVariables(ni.value());
        QStringList recordFormats(formats.value(record));
        QStringList recordMVCs(mvcs.value(record));

        QSet<QString> recordVariableContents(QSet<QString>::fromList(recordVariables));

        bool recordAveraged = isAveraged;
        if (!recordAveraged) {
            for (QSet<QString>::const_iterator variable = recordVariableContents.constBegin(),
                    end = recordVariableContents.constEnd(); variable != end; ++variable) {
                QStringList components(variable->split('_'));
                if (components.isEmpty())
                    continue;
                if (components.at(0).endsWith('0')) {
                    components[0].chop(1);
                    components[0].append('1');
                    if (recordVariableContents.contains(components.join("_"))) {
                        recordAveraged = true;
                        break;
                    }
                } else if (components.at(0).endsWith('1')) {
                    components[0].chop(1);
                    components[0].append('0');
                    if (recordVariableContents.contains(components.join("_"))) {
                        recordAveraged = true;
                        break;
                    }
                }

                QString desc(variableData.value(*variable).value("fielddesc"));
                if (desc.contains("(PM1)", Qt::CaseInsensitive) ||
                        desc.contains("(PM10)", Qt::CaseInsensitive)) {
                    recordAveraged = true;
                    break;
                }
            }
        }

        CPD2Lookup rlookup(records.size());
        for (int vi = 0, max = recordVariables.size(); vi < max; vi++) {
            QString variable(recordVariables.at(vi));
            QString format;
            if (vi < recordFormats.size())
                format = recordFormats.at(vi).trimmed();
            Variant::Root meta;
            auto outputMeta = meta.write();

            if (variable.length() == 0) {
                rlookup.parsers.append(new CPD2LookupParser());
                continue;
            }

            QString mvc;
            if (vi < recordMVCs.size())
                mvc = recordMVCs.at(vi).trimmed();

            if (variable == "STN") {
                rlookup.idxStation = rlookup.parsers.size();
                rlookup.parsers.append(new CPD2LookupParser());
                continue;
            }
            if (variable == "EPOCH") {
                rlookup.idxEpoch = rlookup.parsers.size();
                rlookup.parsers.append(new CPD2LookupParser());
                continue;
            }
            if (variable == "DateTime") {
                rlookup.idxDateTime = rlookup.parsers.size();
                rlookup.parsers.append(new CPD2LookupParser());
                continue;
            }
            if (variable == "Year") {
                rlookup.idxYear = rlookup.parsers.size();
                rlookup.parsers.append(new CPD2LookupParser());
                continue;
            }
            if (variable == "DOY") {
                rlookup.idxDOY = rlookup.parsers.size();
                rlookup.parsers.append(new CPD2LookupParser());
                continue;
            }

            if (rlookup.suffix.length() == 0 && variable != "F_aer" && variable.contains('_')) {
                rlookup.suffix = variable.section('_', -1).toStdString();
            }

            SequenceName unit({}, archive, variable.toStdString());
            if (recordAveraged && !explicitCutSizeFlags) {
                QStringList components(variable.split('_'));
                if (!components.isEmpty()) {
                    if (components.at(0).endsWith('0')) {
                        components[0].chop(1);
                        components[0].append('1');
                        if (recordVariableContents.contains(components.join("_"))) {
                            components[0].chop(1);
                            unit.setVariable(components.join("_"));
                            unit.addFlavor(SequenceName::flavor_pm10);
                        }
                    } else if (components.at(0).endsWith('1')) {
                        components[0].chop(1);
                        components[0].append('0');
                        if (recordVariableContents.contains(components.join("_"))) {
                            components[0].chop(1);
                            unit.setVariable(components.join("_"));
                            unit.addFlavor(SequenceName::flavor_pm1);
                        }
                    }
                }
            }
            variableFanout[variable].append(unit);

            bool hasEnd = false;

            format = format.trimmed();
            if (translateLegacyFlags && variable == "F_aer") {
                outputMeta.setType(Variant::Type::MetadataFlags);
                if (record.length() < 2) {
                    for (int vi2 = 0, max = recordVariables.size(); vi2 < max; vi2++) {
                        QString vcheck(recordVariables.at(vi2));
                        if (vcheck.contains("_")) {
                            variable = vcheck.section('_', -1);
                            variable.prepend("F1_");
                            break;
                        }
                    }
                } else {
                    variable = record;
                    variable.chop(1);
                    variable.prepend("F1_");
                }
                rlookup.parsers
                       .append(new CPD2ParserFlagsConstant(unit, mvc, legacyFlagsBits, 12,
                                                           enableCutSizeFlags &&
                                                                   (explicitCutSizeFlags ||
                                                                           !recordAveraged) ? 0x0010
                                                                                            : 0000));

                outputMeta.metadataSingleFlag("ContaminationAutomatic")
                          .hash("Description")
                          .setString("Contamination flagged by runtime automatic detection");
                outputMeta.metadataSingleFlag("ContaminationAutomatic")
                          .hash("Bits")
                          .setInt64(0x0001);

                outputMeta.metadataSingleFlag("ContaminationManual")
                          .hash("Description")
                          .setString("Contamination flagged by user intervention");
                outputMeta.metadataSingleFlag("ContaminationManual").hash("Bits").setInt64(0x0002);

                outputMeta.metadataSingleFlag("ContaminationWindRuntime")
                          .hash("Description")
                          .setString("Contamination flagged by runtime wind data");
                outputMeta.metadataSingleFlag("ContaminationWindRuntime")
                          .hash("Bits")
                          .setInt64(0x0004);

                outputMeta.metadataSingleFlag("ContaminationWindPost")
                          .hash("Description")
                          .setString("Contamination flagged by post-processing wind data");
                outputMeta.metadataSingleFlag("ContaminationWindPost")
                          .hash("Bits")
                          .setInt64(0x0008);

                outputMeta.metadataSingleFlag("LowTransmittance")
                          .hash("Description")
                          .setString("Transmittance is less than 0.7");

                outputMeta.metadataSingleFlag("ZeroSubtraction")
                          .hash("Description")
                          .setString("Zero subtraction applied manually");

                outputMeta.metadataSingleFlag("STP")
                          .hash("Description")
                          .setString("STP corrections applied");
                outputMeta.metadataSingleFlag("STP").hash("Bits").setInt64(0x0100);

                outputMeta.metadataSingleFlag("Bond1999")
                          .hash("Description")
                          .setString("Bond1999 correction applied");
                outputMeta.metadataSingleFlag("Bond1999").hash("Bits").setInt64(0x0200);

                outputMeta.metadataSingleFlag("AndersonOgren1998")
                          .hash("Description")
                          .setString("Anderson and Ogren 1998 truncation correction applied");
                outputMeta.metadataSingleFlag("AndersonOgren1998").hash("Bits").setInt64(0x0400);

                outputMeta.metadataSingleFlag("Dilution")
                          .hash("Description")
                          .setString("Dilution corrections applied");
                outputMeta.metadataSingleFlag("Dilution").hash("Bits").setInt64(0x0800);

                format = "%04X";
                mvc = "FFFF";
            } else if (translateSystemFlags &&
                    (variable.startsWith("F1_") || variable.startsWith("F_"))) {
                outputMeta.setType(Variant::Type::MetadataFlags);
                variable = variable.section('_', -1);
                variable.prepend("F1_");
                rlookup.parsers
                       .append(new CPD2ParserFlagsConstant(unit, mvc, systemFlagsBits, 12,
                                                           enableCutSizeFlags &&
                                                                   (explicitCutSizeFlags ||
                                                                           !recordAveraged) ? 0x0010
                                                                                            : 0000));

                outputMeta.metadataSingleFlag("ContaminationAutomatic")
                          .hash("Description")
                          .setString("Contamination flagged by runtime automatic detection");
                outputMeta.metadataSingleFlag("ContaminationAutomatic")
                          .hash("Bits")
                          .setInt64(0x0001);

                outputMeta.metadataSingleFlag("ContaminationManual")
                          .hash("Description")
                          .setString("Contamination flagged by user intervention");
                outputMeta.metadataSingleFlag("ContaminationManual").hash("Bits").setInt64(0x0002);

                outputMeta.metadataSingleFlag("ContaminationWindRuntime")
                          .hash("Description")
                          .setString("Contamination flagged by runtime wind data");
                outputMeta.metadataSingleFlag("ContaminationWindRuntime")
                          .hash("Bits")
                          .setInt64(0x0004);

                outputMeta.metadataSingleFlag("ContaminationWindPost")
                          .hash("Description")
                          .setString("Contamination flagged by post-processing wind data");
                outputMeta.metadataSingleFlag("ContaminationWindPost")
                          .hash("Bits")
                          .setInt64(0x0008);

                outputMeta.metadataSingleFlag("STP")
                          .hash("Description")
                          .setString("STP corrections applied");
                outputMeta.metadataSingleFlag("STP").hash("Bits").setInt64(0x0100);

                outputMeta.metadataSingleFlag("Dilution")
                          .hash("Description")
                          .setString("Dilution corrections applied");
                outputMeta.metadataSingleFlag("Dilution").hash("Bits").setInt64(0x0800);

                format = "%04X";
                mvc = "FFFF";
            } else if (translateNephFlags && variable.startsWith("F2_S")) {
                outputMeta.setType(Variant::Type::MetadataFlags);
                variable = variable.section('_', -1);
                variable.prepend("F1_");
                rlookup.parsers
                       .append(new CPD2ParserFlagsConstant(
                               SequenceName({}, archive, variable.toStdString()), mvc,
                               nephFlagsBits, 16, 0));

                outputMeta.metadataSingleFlag("LampPowerError")
                          .hash("Description")
                          .setString("Lamp power out of range");
                outputMeta.metadataSingleFlag("ValveFault")
                          .hash("Description")
                          .setString("Valve fault");
                outputMeta.metadataSingleFlag("ChopperFault")
                          .hash("Description")
                          .setString("Chopper fault");
                outputMeta.metadataSingleFlag("ShutterFault")
                          .hash("Description")
                          .setString("Shutter fault");
                outputMeta.metadataSingleFlag("HeaterUnstable")
                          .hash("Description")
                          .setString("Heater unstable");
                outputMeta.metadataSingleFlag("PressureOutOfRange")
                          .hash("Description")
                          .setString("Pressure out of range");
                outputMeta.metadataSingleFlag("TemperatureOutOfRange")
                          .hash("Description")
                          .setString("Sample temperature out of range");
                outputMeta.metadataSingleFlag("InletTemperatureOutOfRange")
                          .hash("Description")
                          .setString("Inlet temperature out of range");
                outputMeta.metadataSingleFlag("RHOutOfRange")
                          .hash("Description")
                          .setString("RH out of range");

                outputMeta.metadataSingleFlag("BackscatterDisabled")
                          .hash("Description")
                          .setString("Backscatter shutter disabled");
                outputMeta.metadataSingleFlag("Zero").hash("Description").setString("In zero mode");
                outputMeta.metadataSingleFlag("Blank").hash("Description").setString("Blanking");

                outputMeta.metadataSingleFlag("STP")
                          .hash("Description")
                          .setString("STP corrections applied");
                outputMeta.metadataSingleFlag("STP").hash("Bits").setInt64(0x0100);

                outputMeta.metadataSingleFlag("AndersonOgren1998")
                          .hash("Description")
                          .setString("Anderson and Ogren 1998 truncation correction applied");
                outputMeta.metadataSingleFlag("AndersonOgren1998").hash("Bits").setInt64(0x0400);

                format = "%08X";
                mvc = "FFFFFFFF";
            } else if (translatePSAPFlags && variable.startsWith("F2_A")) {
                outputMeta.setType(Variant::Type::MetadataFlags);
                variable = variable.section('_', -1);
                variable.prepend("F1_");
                rlookup.parsers
                       .append(new CPD2ParserFlagsConstant(unit, mvc, psapFlagsBits, 16, 0));

                outputMeta.metadataSingleFlag("FilterChanging")
                          .hash("Description")
                          .setString("Changing filter");
                outputMeta.metadataSingleFlag("FilterChanging").hash("Bits").setInt64(0x00010000);

                outputMeta.metadataSingleFlag("FlowError")
                          .hash("Description")
                          .setString("Flow error");
                outputMeta.metadataSingleFlag("FlowError").hash("Bits").setInt64(0x00020000);

                outputMeta.metadataSingleFlag("BlueTransmittanceMedium")
                          .hash("Description")
                          .setString("Blue transmittance less than 0.7");
                outputMeta.metadataSingleFlag("BlueTransmittanceMedium")
                          .hash("Bits")
                          .setInt64(0x00040000);

                outputMeta.metadataSingleFlag("BlueTransmittanceLow")
                          .hash("Description")
                          .setString("Blue transmittance less than 0.5");
                outputMeta.metadataSingleFlag("BlueTransmittanceMedium")
                          .hash("Bits")
                          .setInt64(0x00080000);

                outputMeta.metadataSingleFlag("GreenTransmittanceMedium")
                          .hash("Description")
                          .setString("Green transmittance less than 0.7");
                outputMeta.metadataSingleFlag("GreenTransmittanceMedium")
                          .hash("Bits")
                          .setInt64(0x00100000);

                outputMeta.metadataSingleFlag("GreenTransmittanceLow")
                          .hash("Description")
                          .setString("Green transmittance less than 0.5");
                outputMeta.metadataSingleFlag("GreenTransmittanceLow")
                          .hash("Bits")
                          .setInt64(0x00200000);

                outputMeta.metadataSingleFlag("RedTransmittanceMedium")
                          .hash("Description")
                          .setString("Red transmittance less than 0.7");
                outputMeta.metadataSingleFlag("RedTransmittanceMedium")
                          .hash("Bits")
                          .setInt64(0x00400000);

                outputMeta.metadataSingleFlag("RedTransmittanceLow")
                          .hash("Description")
                          .setString("Red transmittance less than 0.5");
                outputMeta.metadataSingleFlag("RedTransmittanceLow")
                          .hash("Bits")
                          .setInt64(0x00800000);

                outputMeta.metadataSingleFlag("LampError")
                          .hash("Description")
                          .setString("Lamp error");
                outputMeta.metadataSingleFlag("LampError").hash("Bits").setInt64(0x01000000);

                outputMeta.metadataSingleFlag("TemperatureOutOfRange")
                          .hash("Description")
                          .setString("Temperature out of range");
                outputMeta.metadataSingleFlag("TemperatureOutOfRange")
                          .hash("Bits")
                          .setInt64(0x02000000);

                outputMeta.metadataSingleFlag("CaseTemperatureUnstable")
                          .hash("Description")
                          .setString("Case temperature too far from setpoint");
                outputMeta.metadataSingleFlag("CaseTemperatureUnstable")
                          .hash("Bits")
                          .setInt64(0x04000000);

                outputMeta.metadataSingleFlag("InstrumentTransmittance")
                          .hash("Description")
                          .setString("Transmittances are as reported by the instrument");
                outputMeta.metadataSingleFlag("InstrumentTransmittance")
                          .hash("Bits")
                          .setInt64(0x08000000);

                outputMeta.metadataSingleFlag("Weiss")
                          .hash("Description")
                          .setString("Weiss correction applied");

                outputMeta.metadataSingleFlag("Bond1999")
                          .hash("Description")
                          .setString("Bond1999 correction applied");
                outputMeta.metadataSingleFlag("Bond1999").hash("Bits").setInt64(0x0200);

                outputMeta.metadataSingleFlag("CTS")
                          .hash("Description")
                          .setString("CTS correction applied");
                outputMeta.metadataSingleFlag("CTS").hash("Bits").setInt64(0x0200);

                format = "%08X";
                mvc = "FFFFFFFF";
            } else if (format.endsWith('f') || format.endsWith('e') || format.endsWith('E')) {
                hasEnd = hasEnd || (translateEnds && variablesWithEnd.indexIn(variable) != -1);
                bool isNext = (variablesAtEnd.indexIn(variable) != -1);
                if (translateArrays && variablesArray.exactMatch(variable)) {
                    QString prefix(variablesArray.cap(1));
                    QString indexString(variablesArray.cap(2));
                    QString suffix(variablesArray.cap(3));
                    if (prefix == "N" && suffix.startsWith("_")) {
                        variable = "Nn";
                        variable.append(suffix);
                    } else {
                        variable = prefix;
                        variable.append(suffix);
                    }
                    unit.setVariable(variable);

                    bool ok = false;
                    int idx = indexString.toInt(&ok);
                    if (!ok) {
                        idx = 0;
                    } else {
                        --idx;
                    }

                    if (arrayCountSet.value(unit, 0) <= idx) {
                        arrayCountSet.insert(unit, idx + 1);
                    }

                    outputMeta.setType(Variant::Type::MetadataArray);
                    outputMeta.metadataArray("Children").setType(Variant::Type::MetadataReal);
                    outputMeta = outputMeta.metadataArray("Children");

                    rlookup.parsers.append(new CPD2ParserFPArray(unit, mvc, hasEnd, isNext, idx));
                } else {
                    outputMeta.setType(Variant::Type::MetadataReal);
                    rlookup.parsers.append(new CPD2ParserFP(unit, mvc, hasEnd, isNext));
                }
            } else if (format.endsWith('x') || format.endsWith('X')) {
                outputMeta.setType(Variant::Type::MetadataInteger);
                rlookup.parsers.append(new CPD2ParserHex(unit, mvc));
            } else if (format.endsWith('o')) {
                outputMeta.setType(Variant::Type::MetadataInteger);
                rlookup.parsers.append(new CPD2ParserInteger(unit, mvc, 8));
            } else if (format.endsWith('d') || format.endsWith('i') || format.endsWith('u')) {
                outputMeta.setType(Variant::Type::MetadataInteger);
                rlookup.parsers.append(new CPD2ParserInteger(unit, mvc));
            } else {
                outputMeta.setType(Variant::Type::MetadataString);
                rlookup.parsers.append(new CPD2ParserString(unit, mvc));
            }

            if (format.length() > 0) {
                /* Translate CPD2 style */
                if (cpd2Number.exactMatch(format)) {
                    QString padding(cpd2Number.cap(1));
                    QString beforeDecimal(cpd2Number.cap(2));
                    QString afterDecimal(cpd2Number.cap(3));
                    int width = beforeDecimal.toInt();
                    if (afterDecimal.length() > 0) {
                        int after = afterDecimal.toInt();
                        format = QString("%1%2.%3f").arg(padding).arg(width + after + 1).arg(after);
                    } else {
                        format = QString("%1%2.6f").arg(padding).arg(width + 7);
                    }
                    format.prepend('%');
                } else if (cpd2Exp.exactMatch(format)) {
                    QString padding(cpd2Exp.cap(1));
                    QString integerWidth(cpd2Exp.cap(2));
                    QString fractionalWidth(cpd2Exp.cap(3));
                    QString expontentWidth(cpd2Exp.cap(4));
                    QString expontentType(cpd2Exp.cap(5));

                    int iWidth;
                    if (integerWidth.length() > 0)
                        iWidth = integerWidth.toInt();
                    else
                        iWidth = 1;
                    int fWidth;
                    if (fractionalWidth.length() > 0)
                        fWidth = fractionalWidth.toInt();
                    else
                        fWidth = 6;
                    int eWidth;
                    if (fractionalWidth.length() > 0)
                        eWidth = fractionalWidth.toInt();
                    else
                        eWidth = 2;

                    format = QString("%1%2.%3%4").arg(padding)
                                                 .arg(iWidth + fWidth + eWidth + 3)
                                                 .arg(fWidth)
                                                 .prepend('%');
                }
                outputMeta.metadata("Format").setString(format);
            }

            if (mvc.length() > 0)
                outputMeta.metadata("MVC").setString(mvc);

            QHash<QString, QString> data(variableData.value(variable));

            QString description(data.value("fielddesc"));
            if (description.length() > 0) {
                if (descriptionUnitExtract.exactMatch(description)) {
                    description = descriptionUnitExtract.cap(1).trimmed();
                    QString units(descriptionUnitExtract.cap(2).trimmed());
                    if (units != "PM1" && units != "PM10")
                        outputMeta.metadata("Units").setString(units);
                }
                meta.write().metadata("Description").setString(description);
            }

            if (variablesMm1.indexIn(variable) != -1) {
                outputMeta.metadata("Units").setString("Mm\xE2\x81\xBB¹");
            } else if (variablescm3.indexIn(variable) != -1) {
                outputMeta.metadata("Units").setString("cm\xE2\x81\xBB³");
            } else if (variablesum.indexIn(variable) != -1) {
                outputMeta.metadata("Units").setString("\xCE\xBCm");
            } else if (variablesC.indexIn(variable) != -1) {
                outputMeta.metadata("Units").setString("\xC2\xB0\x43");
            } else if (variablesPCT.indexIn(variable) != -1) {
                outputMeta.metadata("Units").setString("%");
            } else if (variableshPa.indexIn(variable) != -1) {
                outputMeta.metadata("Units").setString("hPa");
            } else if (variablesdhPa.indexIn(variable) != -1) {
                outputMeta.metadata("Units").setString("hPa");
                outputMeta.metadata("GroupUnits").setString("dhPa");
            } else if (variablesTr.indexIn(variable) != -1) {
                outputMeta.metadata("GroupUnits").setString("Transmittance");
            }

            meta.write().metadata("Processing").toArray().after_back().set(processing);

            if (hasEnd) {
                outputMeta.metadata("Smoothing").hash("Mode").setString("Difference");
            }

            auto ml = metaConstant.find(unit);
            if (ml == metaConstant.end()) {
                metaConstant.emplace(unit, std::move(meta));
            } else {
                ml->second = Variant::Root::overlay(ml->second, meta);
            }
        }

        if (rlookup.suffix.length() == 0 && record.length() > 1) {
            rlookup.suffix = record.toStdString();
            rlookup.suffix.erase(rlookup.suffix.end()-1);
        }

        records.append(QList<CPD2Record>());
        nextRecords.append(CPD2Record());
        priorDurations.append(60.0);
        lookup.insert(record, rlookup);
    }

    for (QHash<SequenceName, int>::const_iterator setCount = arrayCountSet.constBegin();
            setCount != arrayCountSet.constEnd();
            ++setCount) {
        auto ml = metaConstant.find(setCount.key());
        if (ml == metaConstant.end())
            continue;
        if (ml->second.read().getType() != Variant::Type::MetadataArray)
            continue;
        ml->second.write().metadata("Count").setInt64(setCount.value());
    }

    for (QHash<QString, QHash<QString, QList<HeaderTimeData> > >::iterator
            vi = variableTimeData.begin(); vi != variableTimeData.end(); ++vi) {
        QList<SequenceName> targets(variableFanout.value(vi.key()));
        for (QList<SequenceName>::const_iterator unit = targets.constBegin(),
                endUnits = targets.constEnd(); unit != endUnits; ++unit) {
            for (QHash<QString, QList<HeaderTimeData> >::iterator di = vi.value().begin();
                    di != vi.value().end();
                    ++di) {
                std::sort(di.value().begin(), di.value().end());

                QString field(di.key());
                QList<HeaderTimeData> data(di.value());

                if (field == "wavelength") {
                    for (int i = 0, max = data.length(); i < max; i++) {
                        QString fd(data.at(i).value);
                        int idxS = fd.indexOf(';');
                        QString wl;
                        QString source;
                        if (idxS == -1) {
                            wl = fd;
                        } else {
                            wl = fd.left(idxS);
                            source = fd.mid(idxS + 1);
                        }

                        SequenceValue dv(unit->toMeta(), Variant::Root(), data.at(i).start,
                                         FP::undefined());
                        if (i < max - 1)
                            dv.setEnd(data.at(i + 1).start);
                        Variant::Type metaType = Variant::Type::Empty;
                        {
                            auto check = metaConstant.find(*unit);
                            if (check != metaConstant.end())
                                metaType = check->second.read().getType();
                        }
                        dv.write().setType(metaType);

                        bool ok;
                        double wln = wl.toDouble(&ok);
                        if (ok && wln > 0.0)
                            dv.write().metadata("Wavelength").setDouble(wln);
                        if (source.length() > 0)
                            dv.write().metadata("Source").setString(source);

                        metaTime.emplace_back(std::move(dv));
                    }
                }
            }
        }
    }

    std::sort(metaTime.begin(), metaTime.end(), dataTimeSort);
}

class FlagsOverlay {
public:
    double start;
    double end;
    Variant::Flags flags;

    FlagsOverlay() : start(FP::undefined()), end(FP::undefined()), flags()
    { }

    FlagsOverlay(const FlagsOverlay &) = default;

    FlagsOverlay &operator=(const FlagsOverlay &) = default;

    FlagsOverlay(FlagsOverlay &&) = default;

    FlagsOverlay &operator=(FlagsOverlay &&) = default;

    FlagsOverlay(const Variant::Flags &setFlags, double setStart, double setEnd) : start(setStart),
                                                                                   end(setEnd),
                                                                                   flags(setFlags)
    { }

    FlagsOverlay(Variant::Flags &&setFlags, double setStart, double setEnd) : start(setStart),
                                                                              end(setEnd),
                                                                              flags(std::move(
                                                                                      setFlags))
    { }

    FlagsOverlay(const FlagsOverlay &other, double setStart, double setEnd) : start(setStart),
                                                                              end(setEnd),
                                                                              flags(other.flags)
    { }

    FlagsOverlay(const FlagsOverlay &under,
                 const FlagsOverlay &over,
                 double setStart,
                 double setEnd) : start(setStart), end(setEnd), flags(under.flags)
    { Util::merge(over.flags, flags); }

    inline double getStart() const
    { return start; }

    inline double getEnd() const
    { return end; }

    inline void setStart(double v)
    { start = v; }

    inline void setEnd(double v)
    { end = v; }
};

void ImportCPD2::dumpBuffers(bool all)
{
    double endTime = FP::undefined();
    int nRecords = records.size();

    if (all) {
        for (int i = 0; i < nRecords; i++) {
            if (records.at(i).isEmpty())
                continue;
            double rTime = records.at(i).last().start;
            if (!FP::defined(endTime) || endTime < rTime)
                endTime = rTime;
        }
        if (!FP::defined(endTime))
            return;
    } else {
        bool missed = false;
        double forceTime = FP::undefined();
        int nBuffered = 0;
        for (int i = 0; i < nRecords; i++) {
            int n = records.at(i).size();
            if (n < 2) {
                if (n == 1)
                    missed = true;
                continue;
            }
            nBuffered += n;

            double rTime = records.at(i).at(n - 1).start;
            if (!FP::defined(endTime) || endTime > rTime) {
                endTime = rTime;
            }
            if (n > maxBuffered) {
                rTime = records.at(i).at(maxBuffered).start;
                if (!FP::defined(forceTime) || rTime > forceTime)
                    forceTime = rTime;
            }
        }
        if (!FP::defined(endTime))
            return;

        if (missed) {
            if (!FP::defined(forceTime))
                return;
            endTime = forceTime;
        } else if (FP::defined(forceTime)) {
            if (endTime < forceTime)
                endTime = forceTime;
        } else if (maxBufferTime < 0 || dumpTimer.elapsed() < maxBufferTime) {
            /* Throttled so we don't output tons of metadata when reading
             * from pipes (lots of dump calls due to IO latency) */
            return;
        } else if (nBuffered < maxBuffered / 100 && dumpTimer.elapsed() < maxBufferTime * 5) {
            /* Even in the realtime case reduce the output of tiny buffers */
            return;
        }
        dumpTimer.start();
    }

    SequenceValue::Transfer results;
    std::unordered_map<SequenceName::Component, std::unordered_map<SequenceName::Flavors,
                                                                   std::unordered_map<
                                                                           SequenceName::Component,
                                                                           std::deque<
                                                                                   FlagsOverlay> > > >
            localFlags;
    SequenceName::ComponentSet seenStations;
    std::unordered_set<SequenceName::Flavors> seenFlavors;

    double spanStart = FP::undefined();
    double spanEnd = FP::undefined();

    for (int rIdx = 0; rIdx < nRecords; rIdx++) {
        if (records.at(rIdx).isEmpty())
            continue;

        if (!FP::defined(spanStart) || records.at(rIdx).at(0).start < spanStart)
            spanStart = records.at(rIdx).at(0).start;

        int n = 0;
        int size = records.at(rIdx).size();
        if (all) {
            n = size;
        } else {
            QList<CPD2Record>::const_iterator iLast =
                    Range::findLowerBound(records.at(rIdx).constBegin(),
                                          records.at(rIdx).constEnd(), endTime);
            if (iLast == records.at(rIdx).constEnd())
                --iLast;
            QList<CPD2Record>::const_iterator iCheck = iLast + 1;
            if (iCheck != records.at(rIdx).constEnd() && iCheck->getStart() < endTime)
                ++iLast;
            n = (iLast - records.at(rIdx).constBegin()) + 1;
        }

        double end = 0;
        for (int i = 0; i < n; i++) {
            CPD2Record record(records.at(rIdx).at(i));
            double start = record.start;

            seenStations.insert(record.station);
            seenFlavors.insert(record.allFlavors);

            double duration;
            if (i < size - 1) {
                duration = records.at(rIdx).at(i + 1).start - record.start;
            } else {
                duration = priorDurations.at(rIdx);
            }
            if (!FP::defined(duration) || duration <= 0.0)
                continue;
            end = start + duration;

            auto suffix = record.suffix;
            for (QHash<SequenceName, double>::const_iterator v = record.reals.constBegin(),
                    ve = record.reals.constEnd(); v != ve; ++v) {
                SequenceName unit(v.key());
                unit.setStation(record.station);
                if (!record.allFlavors.empty())
                    unit.addFlavors(record.allFlavors);
                if (suffix.length() == 0)
                    suffix = Util::suffix(unit.getVariable(), '_');
                results.emplace_back(SequenceIdentity(std::move(unit), start, end),
                                     Variant::Root(v.value()));
            }
            for (QHash<SequenceName, QVector<double> >::const_iterator
                    v = record.arrayReals.constBegin(), ve = record.arrayReals.constEnd();
                    v != ve;
                    ++v) {
                Variant::Root outputArray(Variant::Type::Array);
                auto outputWrite = outputArray.write().toArray();
                for (auto av : v.value()) {
                    outputWrite.after_back().setReal(av);
                }

                SequenceName unit(v.key());
                unit.setStation(record.station);
                if (!record.allFlavors.empty())
                    unit.addFlavors(record.allFlavors);
                if (suffix.length() == 0)
                    suffix = Util::suffix(unit.getVariable(), '_');
                results.emplace_back(SequenceIdentity(std::move(unit), start, end),
                                     std::move(outputArray));
            }
            for (QHash<SequenceName, qint64>::const_iterator v = record.integers.constBegin(),
                    ve = record.integers.constEnd(); v != ve; ++v) {
                SequenceName unit(v.key());
                unit.setStation(record.station);
                if (!record.allFlavors.empty())
                    unit.addFlavors(record.allFlavors);
                if (suffix.length() == 0)
                    suffix = Util::suffix(unit.getVariable(), '_');
                results.emplace_back(SequenceIdentity(std::move(unit), start, end),
                                     Variant::Root(v.value()));
            }
            for (QHash<SequenceName, QString>::const_iterator v = record.strings.constBegin(),
                    ve = record.strings.constEnd(); v != ve; ++v) {
                SequenceName unit(v.key());
                unit.setStation(record.station);
                if (!record.allFlavors.empty())
                    unit.addFlavors(record.allFlavors);
                if (suffix.length() == 0)
                    suffix = Util::suffix(unit.getVariable(), '_');
                results.emplace_back(SequenceIdentity(unit, start, end), Variant::Root(v.value()));
            }

            Range::overlayFragmenting(localFlags[record.station][record.allFlavors][suffix],
                                      FlagsOverlay(record.localFlags, start, end));

            if (i == n - 1 || i == size - 2)
                priorDurations[rIdx] = duration;
        }

        if (!FP::defined(spanEnd) || end > spanEnd)
            spanEnd = end;

        if (n == size) {
            records[rIdx].clear();
        } else {
            records[rIdx].erase(records[rIdx].begin(), records[rIdx].begin() + n);
        }
    }

    /* Insert flags */
    for (const auto &station : localFlags) {
        for (const auto &flavors : station.second) {
            for (const auto &flags : flavors.second) {
                SequenceName::Component variable = "F1_";
                variable += flags.first;
                SequenceName unit(station.first, archive, variable, flavors.first);
                for (const auto &segment : flags.second) {
                    results.emplace_back(SequenceIdentity(unit, segment.start, segment.end),
                                         Variant::Root(segment.flags));
                }
            }
        }
    }

    /* Insert metadata */
    for (const auto &meta : metaConstant) {
        for (const auto &station : seenStations) {
            for (const auto &flavors : seenFlavors) {
                SequenceName unit(meta.first.toMeta());
                unit.setStation(station);
                unit.addFlavors(flavors);
                results.emplace_back(SequenceIdentity(std::move(unit), spanStart, spanEnd),
                                     meta.second);
            }
        }
    }

    if (!metaTime.empty()) {
        if (all) {
            for (const auto &i : metaTime) {
                if (Range::compareStartEnd(i.getStart(), spanEnd) >= 0)
                    break;
                if (Range::compareStartEnd(spanStart, i.getEnd()) >= 0)
                    continue;
                SequenceValue dv = i;
                if (Range::compareStart(dv.getStart(), spanStart) < 0)
                    dv.setStart(spanStart);
                if (Range::compareEnd(dv.getEnd(), spanEnd) > 0)
                    dv.setEnd(spanEnd);
                if (FP::defined(dv.getStart()) &&
                        FP::defined(dv.getEnd()) &&
                        dv.getStart() >= dv.getEnd())
                    continue;

                for (const auto &station : seenStations) {
                    dv.setStation(station);
                    for (const auto &flavors : seenFlavors) {
                        dv.setFlavors(flavors);
                        results.emplace_back(dv);
                    }
                }
            }
            metaTime.clear();
        } else {
            for (auto i = metaTime.begin(); i != metaTime.end();) {
                if (Range::compareStartEnd(i->getStart(), spanEnd) >= 0)
                    break;
                if (Range::compareStartEnd(spanStart, i->getEnd()) >= 0) {
                    i = metaTime.erase(i);
                    continue;
                }
                SequenceValue dv = *i;
                if (Range::compareStart(dv.getStart(), spanStart) < 0)
                    dv.setStart(spanStart);
                if (Range::compareEnd(dv.getEnd(), spanEnd) > 0)
                    dv.setEnd(spanEnd);
                if (FP::defined(dv.getStart()) &&
                        FP::defined(dv.getEnd()) &&
                        dv.getStart() >= dv.getEnd()) {
                    i = metaTime.erase(i);
                    continue;
                }

                for (const auto &station : seenStations) {
                    dv.setStation(station);
                    for (const auto &flavors : seenFlavors) {
                        dv.setFlavors(flavors);
                        results.emplace_back(dv);
                    }
                }

                ++i;
            }
        }
    }

    std::sort(results.begin(), results.end(), dataTimeSort);
    outputData(results);
}


void ImportCPD2::processLine(const QString &line)
{
    QStringList fields(CSV::split(line));
    if (fields.isEmpty()) return;
    QString record(fields.takeFirst());
    if (fields.isEmpty()) return;

    QHash<QString, CPD2Lookup>::const_iterator l = lookup.constFind(record);
    if (l == lookup.constEnd()) return;
    int type = l.value().type;
    Q_ASSERT(type < records.size());
    CPD2Record rec;
    qSwap(rec, nextRecords[type]);
    if (records.at(type).isEmpty())
        l.value().apply(fields, rec, NULL, &nextRecords[type]);
    else
        l.value().apply(fields, rec, &records[type].last(), &nextRecords[type]);
    records[type].append(rec);
}

void ImportCPD2::runOutputPrediction()
{
    SequenceName::Component station = "nil";
    for (int r = 0, mr = records.size(); r < mr; r++) {
        if (records.at(r).isEmpty())
            continue;
        station = records.at(r).first().station;
        break;
    }
    for (const auto &i : metaConstant) {
        SequenceName unit = i.first;
        unit.setStation(station);
        outputPrediction.insert(unit);
    }
}

static const std::size_t bufferProcessAbortCheckIterations = 2048;

bool ImportCPD2::handleIncoming(bool inPrepare)
{
    int length = processBuffer.length();
    Q_ASSERT(length != 0);

    char *start = processBuffer.data();
    char *end = start + length;
    char *data = start;
    char *lastPossible = end - 1;
    char *ptrN = (char *) memchr(data, '\n', length);
    char *ptrR = (char *) memchr(data, '\r', length);

    std::size_t counter = 0;
    for (;;) {
        if (++counter % bufferProcessAbortCheckIterations == 0) {
            if (inPrepare) {
                break;
            } else {
                dumpBuffers(false);
                break;
            }
        }

        if (data >= lastPossible)
            break;

        char *next;
        if (ptrN == NULL) {
            if (ptrR == NULL)
                break;
            next = ptrR;
            if (next < lastPossible)
                ptrR = (char *) memchr(next + 1, '\r', (int) (end - next) - 1);
            else
                ptrR = NULL;
            if (next == data) {
                data = next + 1;
                continue;
            }
        } else if (ptrR == NULL || ptrN < ptrR) {
            next = ptrN;
            if (next < lastPossible)
                ptrN = (char *) memchr(next + 1, '\n', (int) (end - next) - 1);
            else
                ptrN = NULL;
            if (next == data) {
                data = next + 1;
                continue;
            }
        } else {
            next = ptrR;
            if (next < lastPossible)
                ptrR = (char *) memchr(next + 1, '\r', (int) (end - next) - 1);
            else
                ptrR = NULL;
            if (next == data) {
                data = next + 1;
                continue;
            }
        }

        QString line(QString::fromUtf8(data, (int) (next - data)));
        data = next + 1;

        if (line.isEmpty())
            continue;

        if (inHeader && line.startsWith('!')) {
            QString value;

            int cIdx = line.indexOf(',');
            if (cIdx > 0) {
                if (cIdx < line.length() - 1) {
                    value = line.mid(cIdx + 1);
                }
                line.truncate(cIdx);
            }
            line.remove(0, 1);

            headers.insert(line, value);
        } else {
            if (inHeader) {
                inHeader = false;
                processHeaders();

                /* Process the line first, so we have a station for
                 * predicted outputs */
                processLine(line);

                {
                    std::unique_lock<std::mutex> lock;
                    if (!inPrepare)
                        lock = processingRelock();
                    runOutputPrediction();
                }

                /* Egress potentially unlocked, so re-run processing */
                processBuffer.remove(0, (int) (data - start));
                return false;
            } else {
                processLine(line);
            }
        }
    }

    if (data > start) {
        processBuffer.remove(0, (int) (data - start));
    }

    return true;
}

SequenceName::Set ImportCPD2::predictedOutputs()
{
    auto lock = acquireLock();
    return outputPrediction;
}

bool ImportCPD2::prepare()
{
    /* Don't copy out of buffers while we can't output, so the limiting
     * works while paused */
    if (egress == nullptr && !dataEnded)
        return false;

    processEnded = dataEnded;
    if (!buffer.empty()) {
        processBuffer += buffer.toQByteArrayRef();
        buffer.clear();

        /* Have to run this here so we can do the output prediction before
         * an egress is set */
        if (inHeader && !egress) {
            handleIncoming(true);
        }

        return true;
    }
    return processEnded;
}

bool ImportCPD2::process()
{
    if (!processBuffer.isEmpty()) {
        if (processEnded) {
            if (!processBuffer.endsWith('\n') && !processBuffer.endsWith('\r'))
                processBuffer.append('\n');
        }
        if (!handleIncoming(false))
            return true;
    }

    if (processEnded) {
        dumpBuffers(true);
        return false;
    }

    dumpBuffers(false);
    return true;
}


QString ImportCPD2Component::getComponentName() const
{ return QString::fromLatin1("import_cpd2"); }

QString ImportCPD2Component::getComponentDisplayName() const
{ return tr("CPD2 Data Import"); }

QString ImportCPD2Component::getComponentDescription() const
{
    return tr("This component provides a mechanism to directly ingest CPD2 style data.");
}

ComponentOptions ImportCPD2Component::getOptions()
{
    ComponentOptions options;

    options.add("archive", new ComponentOptionSingleString(tr("archive", "name"),
                                                           tr("The archive that to import the data as"),
                                                           tr("This is the archive that the data is imported as.  For example, "
                                                              "\"raw\" or \"clean\""),
                                                           tr("raw", "default archive")));

    ComponentOptionSingleInteger *buffer = new ComponentOptionSingleInteger(tr("buffer", "name"),
                                                                            tr("The maximum number of a type of record to buffer"),
                                                                            tr("This is the maximum number of instances of a type of record that "
                                                                               "can be buffered before output is forced.  This limit only takes "
                                                                               "effect if a record type stops appearing, which forces all other "
                                                                               "records to be buffered."),
                                                                            tr("10000",
                                                                               "buffer size"), 1);
    buffer->setMinimum(2);
    options.add("buffer", buffer);

    ComponentOptionSingleDouble *wait = new ComponentOptionSingleDouble(tr("wait", "name"),
                                                                        tr("The maximum number of seconds to wait for data"),
                                                                        tr("This is the maximum number of seconds to wait for data before "
                                                                           "flushing buffers.  This is used to keep realtime data output "
                                                                           "responsive, but it can be disabled (set to undefined) for "
                                                                           "batch output.  A shorter wait time increased the amount of "
                                                                           "overhead generated in the output."),
                                                                        tr("1 second",
                                                                           "buffer wait"), 1);
    wait->setMinimum(0, true);
    wait->setMaximum(86400, true);
    wait->setAllowUndefined(true);
    options.add("wait", wait);

    options.add("translate-legacy-flags",
                new ComponentOptionBoolean(tr("translate-legacy-flags", "name"),
                                           tr("Translate legacy (CPD1) flags"),
                                           tr("If set then translation is performed on legacy CPD1 style flags.  "
                                              "This translates the bits into semantic flags for CPD3 processing."),
                                           tr("Enabled")));

    options.add("translate-system-flags",
                new ComponentOptionBoolean(tr("translate-system-flags", "name"),
                                           tr("Translate system (CPD2) flags"),
                                           tr("If set then translation is performed CPD2 style system flags.  "
                                              "This translates the bits into semantic flags for CPD3 processing."),
                                           tr("Enabled")));

    options.add("translate-neph-flags",
                new ComponentOptionBoolean(tr("translate-neph-flags", "name"),
                                           tr("Translate CPD2 nephelometers flags"),
                                           tr("If set then translation is performed CPD2 nephelometers flags.  "
                                              "This translates the bits into semantic flags for CPD3 processing."),
                                           tr("Enabled")));

    options.add("translate-psap-flags",
                new ComponentOptionBoolean(tr("translate-psap-flags", "name"),
                                           tr("Translate CPD2 PSAP/CLAP flags"),
                                           tr("If set then translation is performed CPD2 PSAP or CLAP flags.  "
                                              "This translates the bits into semantic flags for CPD3 processing."),
                                           tr("Enabled")));

    options.add("translate-arrays", new ComponentOptionBoolean(tr("translate-arrays", "name"),
                                                               tr("Translate array variables (e.x. size distributions)"),
                                                               tr("If set then variables that would form arrays in CPD3 are "
                                                                  "translated accordingly.  For example, size distributions are "
                                                                  "converted to CPD3 arrays."),
                                                               tr("Enabled")));

    options.add("translate-ends", new ComponentOptionBoolean(tr("translate-ends", "name"),
                                                             tr("Translate end variables (e.x. transmtittance)"),
                                                             tr("If set then variables that have difference measurement ends "
                                                                "are translated to CPD3 equivalents.  For example, transmitance measurements."),
                                                             tr("Enabled")));

    options.add("enable-cut-flags", new ComponentOptionBoolean(tr("enable-cut-flags", "name"),
                                                               tr("Translate any available cut size flags"),
                                                               tr("If set then any available cut size flags are translated."),
                                                               tr("Enabled")));

    return options;
}

QList<ComponentExample> ImportCPD2Component::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This imports CPD2 data using all flags translation and sets the "
                                        "archive to raw.")));

    return examples;
}

ExternalConverter *ImportCPD2Component::createDataIngress(const ComponentOptions &options)
{ return new ImportCPD2(options); }
