/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>
#include <QDateTime>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;

    int countRate;
    double Qsample;
    double Qinlet;
    double Tsaturator;
    double Tcondenser;
    double Toptics;
    double Tcabinet;
    double Pambient;
    double Porifice;
    double Pnozzle;
    double Alaser;

    quint16 instrumentErrors;
    bool liquidFull;
    QDateTime currentTime;

    QString firmwareVersion;
    QString serialNumber;
    QString modelNumber;

    double unpolledRemaining;
    enum {
        Unpolled_Format1, Unpolled_Format2_3776, Unpolled_Format2_3771,
    } unpolledMode;
    bool supportsSCD;

    ModelInstrument() : incoming(), outgoing()
    {
        countRate = 10000;
        Qsample = 0.3;
        Qinlet = 1.0;
        Tsaturator = 39.0;
        Tcondenser = 14.0;
        Toptics = 40.0;
        Tcabinet = 23.8;

        Pambient = 1000.0;
        Porifice = 508.0;
        Pnozzle = 0.28;
        Alaser = 70.0;

        instrumentErrors = 0;
        liquidFull = true;
        currentTime = QDateTime(QDate(2013, 1, 1), QTime(15, 0, 0), Qt::UTC);

        firmwareVersion = QString::fromLatin1("2.3.1");
        serialNumber = QString::fromLatin1("70514396");
        modelNumber = QString::fromLatin1("3775");

        unpolledRemaining = FP::undefined();
        unpolledMode = Unpolled_Format1;
        supportsSCD = true;
    }

    void writeD()
    {
        outgoing.append("1\r");
        outgoing.append(QByteArray::number(countRate));
        outgoing.append(",0\r");
        for (int i = 0; i < 15; i++) {
            outgoing.append("0,0\r");
        }
    }

    void writeUnpolled10(double value)
    {
        for (int i = 0; i < 10; i++) {
            outgoing.append(',');
            outgoing.append(QByteArray::number(value));
        }
    }

    void writeUnpolled10(int value)
    {
        for (int i = 0; i < 10; i++) {
            outgoing.append(',');
            outgoing.append(QByteArray::number(value));
        }
    }

    void writeLiquidLevel()
    {
        if (liquidFull)
            outgoing.append("FULL (2471)");
        else
            outgoing.append("NOTFULL (500)");
    }

    double concentration() const
    {
        return (double) countRate / (Qsample * (1000.0 / 60.0));
    }

    void advance(double seconds)
    {
        currentTime = currentTime.addMSecs((qint64) (seconds * 1000.0));

        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR));

            if (line == "D") {
                writeD();
            } else if (line == "RFV") {
                outgoing.append(firmwareVersion.toUtf8());
                outgoing.append('\r');
            } else if (line == "RSF") {
                outgoing.append(QByteArray::number(Qsample * 1000.0, 'f', 1));
                outgoing.append('\r');
            } else if (line == "RIF") {
                outgoing.append(QByteArray::number(Qinlet, 'f', 1));
                outgoing.append('\r');
            } else if (line == "RTS" || line == "R2") {
                outgoing.append(QByteArray::number(Tsaturator, 'f', 1));
                outgoing.append('\r');
            } else if (line == "RTC" || line == "R1") {
                outgoing.append(QByteArray::number(Tcondenser, 'f', 1));
                outgoing.append('\r');
            } else if (line == "RTO" || line == "R3") {
                outgoing.append(QByteArray::number(Toptics, 'f', 1));
                outgoing.append('\r');
            } else if (line == "RTA") {
                outgoing.append(QByteArray::number(Tcabinet, 'f', 1));
                outgoing.append('\r');
            } else if (line == "RCT") {
                outgoing.append(currentTime.toString("ddd mmm dd hh:mm:ss yyyy").toUtf8());
                outgoing.append('\r');
            } else if (line == "RIE") {
                outgoing.append(QByteArray::number(instrumentErrors, 16));
                outgoing.append('\r');
            } else if (line == "RPA") {
                outgoing.append(QByteArray::number(Pambient / 10.0, 'f', 1));
                outgoing.append('\r');
            } else if (line == "RPO") {
                outgoing.append(QByteArray::number(Porifice / 10.0, 'f', 1));
                outgoing.append('\r');
            } else if (line == "RPN") {
                outgoing.append(QByteArray::number(Pnozzle / 10.0, 'f', 3));
                outgoing.append('\r');
            } else if (line == "RPS") {
                outgoing.append(QByteArray::number(Pambient / 2.4909, 'f', 3));
                outgoing.append('\r');
            } else if (line == "RSN") {
                outgoing.append(serialNumber.toUtf8());
                outgoing.append('\r');
            } else if (line == "RAI") {
                outgoing.append("5.22,3.65\r");
            } else if (line == "RALL") {
                outgoing.append(QByteArray::number(concentration(), 'f', 1));
                outgoing.append(',');
                outgoing.append(QByteArray::number(instrumentErrors, 16).rightJustified(4, '0'));
                outgoing.append(',');
                outgoing.append(QByteArray::number(Tsaturator, 'f', 1));
                outgoing.append(',');
                outgoing.append(QByteArray::number(Tcondenser, 'f', 1));
                outgoing.append(',');
                outgoing.append(QByteArray::number(Toptics, 'f', 1));
                outgoing.append(',');
                outgoing.append(QByteArray::number(Tcabinet, 'f', 1));
                outgoing.append(',');
                outgoing.append(QByteArray::number(Pambient / 10.0, 'f', 1));
                outgoing.append(',');
                outgoing.append(QByteArray::number(Porifice / 10.0, 'f', 1));
                outgoing.append(',');
                outgoing.append(QByteArray::number(Pnozzle / 10.0, 'f', 3));
                outgoing.append(',');
                outgoing.append(QByteArray::number(Alaser, 'f', 0));
                outgoing.append(',');
                writeLiquidLevel();
                outgoing.append('\r');
            } else if (line == "RLP") {
                outgoing.append(QByteArray::number(Alaser, 'f', 0));
                outgoing.append('\r');
            } else if (line == "RLL") {
                writeLiquidLevel();
                outgoing.append('\r');
            } else if (line == "RMN") {
                outgoing.append(modelNumber.toUtf8());
                outgoing.append('\r');
            } else if (line == "R0") {
                if (liquidFull)
                    outgoing.append("FULL\r");
                else
                    outgoing.append("NOTFULL\r");
            } else if (line == "R5") {
                if (liquidFull && instrumentErrors == 0)
                    outgoing.append("READY\r");
                else
                    outgoing.append("NOTREADY\r");
            } else if (line == "R7") {
                outgoing.append("2.013\r");
            } else if (line == "RD") {
                outgoing.append(QByteArray::number(concentration(), 'f', 1));
                outgoing.append('\r');
            } else if (line == "RV") {
                outgoing.append("Model ");
                outgoing.append(modelNumber.toUtf8());
                outgoing.append(" Ver ");
                outgoing.append(firmwareVersion.toUtf8());
                outgoing.append(" S/N ");
                outgoing.append(serialNumber.toUtf8());
                outgoing.append('\r');
            } else if (line == "RCOUNT1" || line == "RCOUNT2") {
                outgoing.append(QByteArray::number(countRate));
                outgoing.append('\r');
            } else if (line == "SCD") {
                if (supportsSCD) {
                    outgoing.append("020313,010313,1\r");
                } else {
                    outgoing.append("ERROR\r");
                }
            } else if (line.startsWith("SAV") ||
                    line.startsWith("SSTART") ||
                    line.startsWith("SCM") ||
                    line.startsWith("STS") ||
                    line.startsWith("STC") ||
                    line.startsWith("STO") ||
                    line.startsWith("SAWR") ||
                    line.startsWith("SAO") ||
                    line.startsWith("SCOM") ||
                    line.startsWith("S3776FLOW") ||
                    line.startsWith("SFILL") ||
                    line.startsWith("SDRAIN") ||
                    line.startsWith("SCC") ||
                    line.startsWith("SAF") ||
                    line.startsWith("SCCM") ||
                    line.startsWith("SCD") ||
                    line.startsWith("SCCR") ||
                    line.startsWith("ZT") ||
                    line.startsWith("ZV") ||
                    line.startsWith("COM2") ||
                    line.startsWith("DEL") ||
                    line.startsWith("CD") ||
                    line.startsWith("CAL3775") ||
                    line.startsWith("Help") ||
                    line == "ZB" ||
                    line == "ZE" ||
                    line == "ZU" ||
                    line == "X2" ||
                    line == "X3" ||
                    line == "X7" ||
                    line == "X8" ||
                    line == "FORMAT" ||
                    line == "DIR") {
                outgoing.append("OK\r");
            } else if (line.startsWith("SHOUR,")) {
                bool ok = false;
                int val = line.mid(6).toInt(&ok, 10);
                if (!ok || val < 0 || val > 23) {
                    outgoing.append("ERROR\r");
                } else {
                    outgoing.append("OK\r");
                    currentTime.setTime(
                            QTime(val, currentTime.time().minute(), currentTime.time().second(),
                                  currentTime.time().msec()));
                }
            } else if (line.startsWith("SMINUTE,")) {
                bool ok = false;
                int val = line.mid(8).toInt(&ok, 10);
                if (!ok || val < 0 || val > 59) {
                    outgoing.append("ERROR\r");
                } else {
                    outgoing.append("OK\r");
                    currentTime.setTime(
                            QTime(currentTime.time().hour(), val, currentTime.time().second(),
                                  currentTime.time().msec()));
                }
            } else if (line.startsWith("SSECOND,")) {
                bool ok = false;
                int val = line.mid(8).toInt(&ok, 10);
                if (!ok || val < 0 || val > 60) {
                    outgoing.append("ERROR\r");
                } else {
                    outgoing.append("OK\r");
                    currentTime.setTime(
                            QTime(currentTime.time().hour(), currentTime.time().minute(), val,
                                  currentTime.time().msec()));
                }
            } else if (line.startsWith("SYEAR,")) {
                bool ok = false;
                int val = line.mid(6).toInt(&ok, 10);
                if (!ok || val < 0 || val > 99) {
                    outgoing.append("ERROR\r");
                } else {
                    val += 2000;
                    outgoing.append("OK\r");
                    currentTime.setDate(
                            QDate(val, currentTime.date().month(), currentTime.date().day()));
                }
            } else if (line.startsWith("SDAY,")) {
                bool ok = false;
                int val = line.mid(5).toInt(&ok, 10);
                if (!ok || val < 1 || val > 31) {
                    outgoing.append("ERROR\r");
                } else {
                    val += 2000;
                    outgoing.append("OK\r");
                    currentTime.setDate(
                            QDate(currentTime.date().year(), currentTime.date().month(), val));
                }
            } else if (line.startsWith("SMONTH,")) {
                bool ok = false;
                int val = line.mid(7).toInt(&ok, 10);
                if (!ok || val < 1 || val > 12) {
                    outgoing.append("ERROR\r");
                } else {
                    outgoing.append("OK\r");
                    currentTime.setDate(
                            QDate(currentTime.date().year(), val, currentTime.date().day()));
                }
            } else {
                outgoing.append("ERROR\r");
            }

            if (idxCR >= incoming.size() - 1) {
                incoming.clear();
                break;
            }
            incoming = incoming.mid(idxCR + 1);
        }

        if (FP::defined(unpolledRemaining)) {
            unpolledRemaining -= seconds;
            while (unpolledRemaining <= 0.0) {
                unpolledRemaining += 1.0;

                outgoing.append('U');
                outgoing.append(QByteArray::number(currentTime.toTime_t() % 1000));

                switch (unpolledMode) {
                case Unpolled_Format1:
                    writeUnpolled10((int) (countRate / 10));
                    writeUnpolled10(concentration());
                    outgoing.append(",0.0,0.0,");
                    outgoing.append(QByteArray::number(instrumentErrors, 16));
                    break;
                case Unpolled_Format2_3776:
                    writeUnpolled10(concentration());
                    writeUnpolled10((int) (countRate / 10));
                    writeUnpolled10(Qsample * 1000.0 / 60.0 / 10.0);
                    writeUnpolled10(0.0);
                    break;
                case Unpolled_Format2_3771:
                    writeUnpolled10(concentration());
                    writeUnpolled10((int) (countRate / 10));
                    outgoing.append(',');
                    outgoing.append(QByteArray::number(Qsample * 1000.0 / 60.0));
                    outgoing.append(",0.0");
                    writeUnpolled10(0.0);
                    break;
                }

                outgoing.append('\r');
            }
        }
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("N", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T4", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Qu", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("P", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Pd1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Pd2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("A", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("C", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZND", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZQ", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZFULL", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZLIQUID", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("N"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("T3"))
            return false;
        if (!stream.checkContiguous("T4"))
            return false;
        if (!stream.checkContiguous("Q"))
            return false;
        if (!stream.checkContiguous("Qu"))
            return false;
        if (!stream.checkContiguous("Pd1"))
            return false;
        if (!stream.checkContiguous("Pd2"))
            return false;
        if (!stream.checkContiguous("P"))
            return false;
        if (!stream.checkContiguous("A"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream, const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("N", Variant::Root(model.concentration()), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.Tsaturator), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.Tcondenser), time))
            return false;
        if (!stream.hasAnyMatchingValue("T3", Variant::Root(model.Toptics), time))
            return false;
        if (!stream.hasAnyMatchingValue("T4", Variant::Root(model.Tcabinet), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q", Variant::Root(model.Qsample), time))
            return false;
        if (!stream.hasAnyMatchingValue("Qu", Variant::Root(model.Qinlet), time))
            return false;
        if (!stream.hasAnyMatchingValue("Pd1", Variant::Root(model.Pnozzle), time))
            return false;
        if (!stream.hasAnyMatchingValue("Pd2", Variant::Root(model.Porifice), time))
            return false;
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.Pambient), time))
            return false;
        if (!stream.hasAnyMatchingValue("A", Variant::Root(model.Alaser), time))
            return false;
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("C", Variant::Root((double) model.countRate), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZQ", Variant::Root(model.Qsample), time))
            return false;
        return true;
    }

    bool checkUnpolledContiguous(StreamCapture &stream, bool checkFlow = false)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("N"))
            return false;
        if (checkFlow && !stream.checkContiguous("Q"))
            return false;
        return true;
    }

    bool checkUnpolledValues(StreamCapture &stream, const ModelInstrument &model,
                             bool checkFlow = false,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("N", Variant::Root(model.concentration()), time))
            return false;
        if (checkFlow && !stream.hasAnyMatchingValue("Q", Variant::Root(model.Qsample), time))
            return false;
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkUnpolledRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                                     bool checkFlow = false,
                                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("C", Variant::Root((double) model.countRate), time))
            return false;
        if (checkFlow && !stream.hasAnyMatchingValue("ZQ", Variant::Root(model.Qsample), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_tsi_cpc377x"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_tsi_cpc377x"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        StreamCapture realtime;
        interface->setRealtimeEgress(&realtime);

        for (int i = 0; i < 20; i++) {
            control.externalControl("RALL\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("RIF\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("RSF\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("RCOUNT1\r");
            control.advance(0.1);
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 10; i++) {
            control.externalControl("RALL\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("RIF\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("RSF\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("RCOUNT1\r");
            control.advance(0.1);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void unpolledFormat1()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.0;
        instrument.unpolledMode = ModelInstrument::Unpolled_Format1;

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        StreamCapture realtime;
        interface->setRealtimeEgress(&realtime);

        for (int i = 0; i < 60; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 60; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkUnpolledContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkUnpolledValues(logging, instrument));
        QVERIFY(checkUnpolledValues(realtime, instrument));
        QVERIFY(checkUnpolledRealtimeValues(realtime, instrument));

    }

    void unpolledFormat3776()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.0;
        instrument.unpolledMode = ModelInstrument::Unpolled_Format2_3776;

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        StreamCapture realtime;
        interface->setRealtimeEgress(&realtime);

        for (int i = 0; i < 60; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 60; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkUnpolledContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkUnpolledValues(logging, instrument));
        QVERIFY(checkUnpolledValues(realtime, instrument));
        QVERIFY(checkUnpolledRealtimeValues(realtime, instrument));

    }

    void unpolledFormat3771()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.0;
        instrument.unpolledMode = ModelInstrument::Unpolled_Format2_3771;

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        StreamCapture realtime;
        interface->setRealtimeEgress(&realtime);

        for (int i = 0; i < 60; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 60; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkUnpolledContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkUnpolledValues(logging, instrument));
        QVERIFY(checkUnpolledValues(realtime, instrument));
        QVERIFY(checkUnpolledRealtimeValues(realtime, instrument));

    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.liquidFull = false;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(0.1);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        Variant::Write source = Variant::Write::empty();
        source.hash("SerialNumber").setInt64(70514396);
        source.hash("FirmwareVersion").setString("2.3.1");
        source.hash("CalibrationLabel").setString("020313,010313,1");
        source.hash("Model").setString("3775");
        source.hash("Manufacturer").setString("TSI");
        QVERIFY(logging.hasAnyMatchingMetaValue("F1", source, FP::undefined(), "^Source"));

    }

    void interactiveAutoprobeAlternate()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.liquidFull = false;
        instrument.supportsSCD = false;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(0.1);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        Variant::Write source = Variant::Write::empty();
        source.hash("SerialNumber").setInt64(70514396);
        source.hash("FirmwareVersion").setString("2.3.1");
        source.hash("Model").setString("3775");
        source.hash("Manufacturer").setString("TSI");
        QVERIFY(logging.hasAnyMatchingMetaValue("F1", source, FP::undefined(), "^Source"));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        double checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        control.advance(0.125);
        checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
