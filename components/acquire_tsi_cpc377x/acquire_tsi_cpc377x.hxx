/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIRETSICPC377X_H
#define ACQUIRETSICPC377X_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QHash>
#include <QSet>
#include <QString>
#include <QDateTime>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class AcquireTSICPC377X : public CPD3::Acquisition::FramedInstrument {
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum {
        /* Passive autoprobe initialization, ignorging the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,

        /* Acquiring data in interactive mode, RALL response */
                RESP_INTERACTIVE_RUN_READ_ALL,
        /* Acquiring data in interactive mode, sample flow */
                RESP_INTERACTIVE_RUN_READ_SAMPLEFLOW,
        /* Acquiring data in interactive mode, inlet flow */
                RESP_INTERACTIVE_RUN_READ_INLETFLOW,
        /* Acquiring data in interactive mode, counts */
                RESP_INTERACTIVE_RUN_READ_COUNTS,
        /* Interactive waiting for the next poll time */
                RESP_INTERACTIVE_RUN_WAIT,

        /* Handling an external command */
                RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND,

        /* Starting communications, stopping unpolled */
                RESP_INTERACTIVE_START_STOPREPORTS,
        /* Starting communications, reading the model number */
                RESP_INTERACTIVE_START_READ_MODELNUMBER,
        /* Starting communications, retrying reading the model number */
                RESP_INTERACTIVE_START_READ_MODELNUMBER_RETRY,
        /* Starting communications, reading the serial number */
                RESP_INTERACTIVE_START_READ_SERIALNUMBER,
        /* Starting communications, reading the firmware version */
                RESP_INTERACTIVE_START_READ_FIRMWAREVERSION,
        /* Starting communications, reading the calibration date */
                RESP_INTERACTIVE_START_READ_CALIBRATION,
        /* Starting communications, reading error codes */
                RESP_INTERACTIVE_START_READ_ERRORS,
        /* Starting communications, set concentration mode (SCM,0) */
                RESP_INTERACTIVE_START_SET_OPERATINGMODE,
        /* Starting communications, set coincidence correction (SCC,1) */
                RESP_INTERACTIVE_START_SET_CORRECTED,
        /* Setting the instrument RTC */
                RESP_INTERACTIVE_START_SET_YEAR,
        RESP_INTERACTIVE_START_SET_MONTH,
        RESP_INTERACTIVE_START_SET_DAY,
        RESP_INTERACTIVE_START_SET_HOUR,
        RESP_INTERACTIVE_START_SET_MINUTE,
        RESP_INTERACTIVE_START_SET_SECOND,
        /* Starting communications, reading the "all" record */
                RESP_INTERACTIVE_START_READ_ALL,

        /* Waiting before attempting a communications restart */
                RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
                RESP_INTERACTIVE_INITIALIZE,
    } responseState;
    int autoprobePassiveValidRecords;

    enum CommandType {
        /* Unknown command, so all responses until another command is
         * received are discarded. */
                COMMAND_IGNORED,

        /* RSF, Read sample flow rate */
                COMMAND_RSF, /* RIF, Read inlet flow rwate */
                COMMAND_RIF, /* RTS, Read saturator temperature */
                COMMAND_RTS, /* RTC, Read condenser temperature */
                COMMAND_RTC, /* RTO, Read optics temperature */
                COMMAND_RTO, /* RTA, Read cabinet temperature */
                COMMAND_RTA, /* RIE, Read instrument errors */
                COMMAND_RIE, /* RPA, Read ambient pressure */
                COMMAND_RPA, /* RPO, Read orifice pressure */
                COMMAND_RPO, /* RPN, Read nozzle pressure */
                COMMAND_RPN, /* RPS, Read ambient pressure in inches of water */
                COMMAND_RPS, /* RLP, Read laser power */
                COMMAND_RLP, /* RLL, Read liquid level */
                COMMAND_RLL, /* RCOUNT1, Read corrected counts */
                COMMAND_RCOUNT1, /* RCOUNT2, Read uncorrected counts */
                COMMAND_RCOUNT2,

        /* RALL, Read "all" current values */
                COMMAND_RALL,

        /* RFV, Read the firmware version */
                COMMAND_RFV, /* RSN, Read the serial number */
                COMMAND_RSN, /* RMN, Read model number */
                COMMAND_RMN, /* SCD, Read/set calibration date */
                COMMAND_SCD,

        /* D, Legacy, read counts */
                COMMAND_D,

        /* A generic command responding with a single "OK" */
                COMMAND_OK,

        /* A specifically invalid command */
                COMMAND_INVALID,
    };

    class Command {
        CommandType type;
        int counter;
    public:
        Command();

        Command(CommandType t);

        Command(const Command &other);

        Command &operator=(const Command &other);

        inline CommandType getType() const
        { return type; }

        inline int getCounter() const
        { return counter; }

        inline void setCounter(int i)
        { counter = i; }

        CPD3::Data::Variant::Root stateDescription() const;
    };

    QList<Command> commandQueue;

    enum LogStreamID {
        LogStream_Metadata = 0,
        LogStream_State,

        LogStream_Counts,
        LogStream_SaturatorTemperature,
        LogStream_CondenserTemperature,
        LogStream_OpticsTemperature,
        LogStream_CabinetTemperature,
        LogStream_SampleFlow,
        LogStream_InletFlow,
        LogStream_AmbientPressure,
        LogStream_NozzlePressure,
        LogStream_OrificePressure,
        LogStream_LaserCurrent,

        LogStream_TOTAL,
        LogStream_RecordBaseStart = LogStream_Counts,
    };
    CPD3::Data::StaticMultiplexer loggingMux;
    quint32 streamAge[LogStream_TOTAL];
    double streamTime[LogStream_TOTAL];

    class Configuration {
        double start;
        double end;
    public:
        double flow;
        double pollInterval;
        bool doTimeSet;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    CPD3::Data::Variant::Root instrumentMeta;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool forceRealtimeStateEmit;

    qint64 countSeconds;
    qint64 countPulses;

    QSet<QString> errorFlags;
    bool liquidLow;
    bool hadUncorrectedCounts;
    bool haveCoincidenceCorrection;
    double lastReportedFlow;

    QDateTime instrumentSetTime;

    std::deque<CPD3::Util::ByteArray> externalCommandQueue;

    void setDefaultInvalid();

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void logValue(double startTime,
                  double endTime,
                  int streamID,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    void emitMetadata(double frameTime);

    void streamAdvance(int streamID, double time);

    void describeState(CPD3::Data::Variant::Write &info, const Command &command = Command()) const;

    void configurationChanged();

    void configurationAdvance(double frameTime);

    int translateFlags(const CPD3::Util::ByteView &field);

    int processResponse(const CPD3::Util::ByteView &frame, double frameTime);

    double getDefaultFlow() const;

    int translateLiquidLevel(double frameTime, CPD3::Util::ByteView field);

    void invalidateLogValues(double frameTime);

public:
    AcquireTSICPC377X(const CPD3::Data::ValueSegment::Transfer &config,
                      const std::string &loggingContext);

    AcquireTSICPC377X(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireTSICPC377X();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    virtual CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    virtual void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingInstrumentTimeout(double time);

    virtual void discardDataCompleted(double time);
};

class AcquireTSICPC377XComponent
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_tsi_cpc377x"
                              FILE
                              "acquire_tsi_cpc377x.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
