/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_tsi_cpc377x.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

static const char *instrumentFlagTranslation[16] = {"SaturatorTemperatureError",    /* 0x0001 */
                                                    "CondenserTemperatureError",    /* 0x0002 */
                                                    "OpticsTemperatureError",       /* 0x0004 */
                                                    "InletFlowError",               /* 0x0008 */
                                                    "SampleFlowError",              /* 0x0010 */
                                                    "LaserPowerError",              /* 0x0020 */
                                                    "LiquidLow",                    /* 0x0040 */
                                                    "ConcentrationOutOfRange",      /* 0x0080 */
                                                    "",                             /* 0x0100 */
                                                    NULL,                           /* 0x0200 */
                                                    NULL,                           /* 0x0400 */
                                                    NULL,                           /* 0x0800 */
                                                    NULL,                           /* 0x1000 */
                                                    NULL,                           /* 0x2000 */
                                                    NULL,                           /* 0x4000 */
                                                    NULL,                           /* 0x8000 */
};

AcquireTSICPC377X::Configuration::Configuration() : start(FP::undefined()),
                                                    end(FP::undefined()),
                                                    flow(FP::undefined()),
                                                    pollInterval(0.5),
                                                    doTimeSet(false)
{ }

AcquireTSICPC377X::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          flow(other.flow),
          pollInterval(other.pollInterval),
          doTimeSet(other.doTimeSet)
{ }

AcquireTSICPC377X::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s), end(e), flow(FP::undefined()), pollInterval(0.5), doTimeSet(false)
{
    setFromSegment(other);
}

AcquireTSICPC377X::Configuration::Configuration(const Configuration &under,
                                                const ValueSegment &over,
                                                double s,
                                                double e) : start(s),
                                                            end(e),
                                                            flow(under.flow),
                                                            pollInterval(under.pollInterval),
                                                            doTimeSet(under.doTimeSet)
{
    setFromSegment(over);
}

void AcquireTSICPC377X::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("TSI");
    instrumentMeta["Model"].setString("377x");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    forceRealtimeStateEmit = true;

    memset(streamAge, 0, sizeof(streamAge));
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
    }

    countSeconds = 0;
    countPulses = 0;
    errorFlags.clear();
    liquidLow = false;
    hadUncorrectedCounts = false;
    haveCoincidenceCorrection = true;
    lastReportedFlow = FP::undefined();
}

void AcquireTSICPC377X::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["Flow"].exists())
        flow = config["Flow"].toDouble();
    if (FP::defined(config["PollInterval"].toDouble()))
        pollInterval = config["PollInterval"].toDouble();
    if (config["SetInstrumentTime"].exists())
        doTimeSet = config["SetInstrumentTime"].toBool();
}

AcquireTSICPC377X::AcquireTSICPC377X(const ValueSegment::Transfer &configData,
                                     const std::string &loggingContext) : FramedInstrument(
        "tsi377x", loggingContext),
                                                                          autoprobeStatus(
                                                                                  AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                          responseState(
                                                                                  RESP_PASSIVE_WAIT),
                                                                          autoprobePassiveValidRecords(
                                                                                  0),
                                                                          loggingMux(
                                                                                  LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }

    configurationChanged();
}

void AcquireTSICPC377X::setToAutoprobe()
{ responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE; }

void AcquireTSICPC377X::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireTSICPC377XComponent::getBaseOptions()
{
    ComponentOptions options;

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(tr("q", "name"), tr("Flow rate"),
                                            tr("This is the sample flow rate of the CPC at lpm measured at ambient "
                                               "conditions."), tr("0.03 lpm", "default flow rate"),
                                            1);
    d->setMinimum(0.0, false);
    options.add("q", d);

    return options;
}

AcquireTSICPC377X::AcquireTSICPC377X(const ComponentOptions &options,
                                     const std::string &loggingContext) : FramedInstrument(
        "tsi377x", loggingContext),
                                                                          autoprobeStatus(
                                                                                  AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                          responseState(
                                                                                  RESP_PASSIVE_WAIT),
                                                                          autoprobePassiveValidRecords(
                                                                                  0),
                                                                          loggingMux(
                                                                                  LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());

    if (options.isSet("q")) {
        double value = qobject_cast<ComponentOptionSingleDouble *>(options.get("q"))->get();
        if (FP::defined(value) && value > 0.0)
            config.last().flow = value;
    }

    configurationChanged();
}

AcquireTSICPC377X::~AcquireTSICPC377X()
{
}

SequenceValue::Transfer AcquireTSICPC377X::buildLogMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_tsi_cpc377x");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    processing["CalibrationLabel"].set(instrumentMeta["CalibrationLabel"]);
    processing["Model"].set(instrumentMeta["Model"]);

    result.emplace_back(SequenceName({}, "raw_meta", "N"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Condensation nuclei concentration");
    if (FP::defined(config.first().flow)) {
        result.back().write().metadataReal("SampleFlow").setDouble(config.first().flow);
    }
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Saturator temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Saturator"));

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Condenser temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Condenser"));

    result.emplace_back(SequenceName({}, "raw_meta", "T3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Optics temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Optics"));

    result.emplace_back(SequenceName({}, "raw_meta", "T4"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Cabinet temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Cabinet"));

    result.emplace_back(SequenceName({}, "raw_meta", "Q"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));

    result.emplace_back(SequenceName({}, "raw_meta", "Qu"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Inlet flow rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Inlet"));

    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));

    result.emplace_back(SequenceName({}, "raw_meta", "Pd1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("GroupUnits").setString("dhPa");
    result.back().write().metadataReal("Description").setString("Nozzle pressure drop");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Nozzle"));

    result.emplace_back(SequenceName({}, "raw_meta", "Pd2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("GroupUnits").setString("dhPa");
    result.back().write().metadataReal("Description").setString("Orifice pressure drop");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Orifice"));

    result.emplace_back(SequenceName({}, "raw_meta", "A"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000");
    result.back().write().metadataReal("Units").setString("mA");
    result.back().write().metadataReal("Description").setString("Laser current");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Laser"));

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("SaturatorTemperatureError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc377x");
    result.back()
          .write()
          .metadataSingleFlag("SaturatorTemperatureError")
          .hash("Bits")
          .setInt64(0x00010000);
    result.back()
          .write()
          .metadataSingleFlag("SaturatorTemperatureError")
          .hash("Description")
          .setString("Saturator temperature error detected");

    result.back()
          .write()
          .metadataSingleFlag("CondenserTemperatureError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc377x");
    result.back()
          .write()
          .metadataSingleFlag("CondenserTemperatureError")
          .hash("Bits")
          .setInt64(0x00020000);
    result.back()
          .write()
          .metadataSingleFlag("CondenserTemperatureError")
          .hash("Description")
          .setString("Condenser temperature error detected");

    result.back()
          .write()
          .metadataSingleFlag("OpticsTemperatureError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc377x");
    result.back()
          .write()
          .metadataSingleFlag("OpticsTemperatureError")
          .hash("Bits")
          .setInt64(0x00040000);
    result.back()
          .write()
          .metadataSingleFlag("OpticsTemperatureError")
          .hash("Description")
          .setString("Optics temperature error detected");

    result.back()
          .write()
          .metadataSingleFlag("InletFlowError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc377x");
    result.back().write().metadataSingleFlag("InletFlowError").hash("Bits").setInt64(0x00080000);
    result.back()
          .write()
          .metadataSingleFlag("InletFlowError")
          .hash("Description")
          .setString("Inlet flow error detected");

    result.back()
          .write()
          .metadataSingleFlag("SampleFlowError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc377x");
    result.back().write().metadataSingleFlag("SampleFlowError").hash("Bits").setInt64(0x00100000);
    result.back()
          .write()
          .metadataSingleFlag("SampleFlowError")
          .hash("Description")
          .setString("Sample flow error detected");

    result.back()
          .write()
          .metadataSingleFlag("LaserPowerError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc377x");
    result.back().write().metadataSingleFlag("LaserPowerError").hash("Bits").setInt64(0x00200000);
    result.back()
          .write()
          .metadataSingleFlag("LaserPowerError")
          .hash("Description")
          .setString("Laser power error detected");

    result.back()
          .write()
          .metadataSingleFlag("LiquidLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc377x");
    result.back().write().metadataSingleFlag("LiquidLow").hash("Bits").setInt64(0x00400000);
    result.back()
          .write()
          .metadataSingleFlag("LiquidLow")
          .hash("Description")
          .setString("Water level low");

    result.back()
          .write()
          .metadataSingleFlag("ConcentrationOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc377x");
    result.back()
          .write()
          .metadataSingleFlag("ConcentrationOutOfRange")
          .hash("Bits")
          .setInt64(0x00800000);
    result.back()
          .write()
          .metadataSingleFlag("ConcentrationOutOfRange")
          .hash("Description")
          .setString("Concentration out of range");

    result.back()
          .write()
          .metadataSingleFlag("CoincidenceCorrected")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc377x");
    result.back()
          .write()
          .metadataSingleFlag("CoincidenceCorrected")
          .hash("Description")
          .setString("Particle concentration has a coincidence correction applied");

    return result;
}

SequenceValue::Transfer AcquireTSICPC377X::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_tsi_cpc377x");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    processing["CalibrationLabel"].set(instrumentMeta["CalibrationLabel"]);
    processing["Model"].set(instrumentMeta["Model"]);

    result.emplace_back(SequenceName({}, "raw_meta", "C"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000000.0");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Count rate"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZND"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Displayed condensation nuclei concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Display concentration"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZQ"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Reported flow rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Display flow"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZFULL"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataBoolean("Description").setString("Current fill state");
    result.back().write().metadataBoolean("Source").set(instrumentMeta);
    result.back().write().metadataBoolean("Processing").toArray().after_back().set(processing);
    result.back().write().metadataBoolean("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataBoolean("Realtime").hash("Name").setString("Liquid");
    result.back().write().metadataBoolean("Realtime").hash("Units").setString(QString());
    result.back()
          .write()
          .metadataBoolean("Realtime")
          .hash("Translation")
          .hash("TRUE")
          .setString(QObject::tr("FULL", "full liquid"));
    result.back()
          .write()
          .metadataBoolean("Realtime")
          .hash("Translation")
          .hash("FALSE")
          .setString(QObject::tr("EMPTY", "full liquid"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZLIQUID"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataInteger("Format").setString("0000");
    result.back().write().metadataInteger("Description").setString("Current full level");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataInteger("Realtime").hash("Name").setString("Level");
    result.back().write().metadataInteger("Realtime").hash("Units").setString(QString());

    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveStopReports")
          .setString(QObject::tr("STARTING COMMS: Stopping unpolled records"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadErrors")
          .setString(QObject::tr("STARTING COMMS: Reading instrument errors"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadFirmwareVersion")
          .setString(QObject::tr("STARTING COMMS: Reading firmware version"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadSerialNumber")
          .setString(QObject::tr("STARTING COMMS: Reading serial number"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadCalibration")
          .setString(QObject::tr("STARTING COMMS: Reading calibration time"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetOperatingMode")
          .setString(QObject::tr("STARTING COMMS: Setting concentration mode"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadModelNumber")
          .setString(QObject::tr("STARTING COMMS: Reading model number"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetCorrected")
          .setString(QObject::tr("STARTING COMMS: Setting coincidence correction"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetTime")
          .setString(QObject::tr("STARTING COMMS: Setting time"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadAll")
          .setString(QObject::tr("STARTING COMMS: Reading initial report"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SaturatorTemperatureError")
          .setString(QObject::tr("Saturator temperature out of range"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("CondenserTemperatureError")
          .setString(QObject::tr("Condenser temperature out of range"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("OpticsTemperatureError")
          .setString(QObject::tr("Optics temperature out of range"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InletFlowError")
          .setString(QObject::tr("Inlet flow rate out of range"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SampleFlowError")
          .setString(QObject::tr("Sample flow rate out of range"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("LaserPowerError")
          .setString(QObject::tr("Laser power out of range"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("LiquidLow")
          .setString(QObject::tr("Water level low"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("ConcentrationOutOfRange")
          .setString(QObject::tr("Concentration out of range"));


    return result;
}

SequenceMatch::Composite AcquireTSICPC377X::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    return sel;
}

void AcquireTSICPC377X::logValue(double startTime,
                                 double endTime,
                                 int streamID,
                                 SequenceName::Component name,
                                 Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    if (!FP::defined(startTime) || !FP::defined(endTime)) {
        if (realtimeEgress && FP::defined(endTime)) {
            realtimeEgress->emplaceData(SequenceName({}, "raw", std::move(name)), std::move(value),
                                        endTime, endTime + 1.0);
        }
        return;
    }
    SequenceValue
            dv(SequenceName({}, "raw", std::move(name)), std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        loggingMux.incoming(streamID, std::move(dv), loggingEgress);
        return;
    }
    if (loggingEgress) {
        loggingMux.incoming(streamID, dv, loggingEgress);
    }
    dv.setStart(endTime);
    dv.setEnd(endTime + 1.0);
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireTSICPC377X::realtimeValue(double time,
                                      SequenceName::Component name,
                                      Variant::Root &&value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

void AcquireTSICPC377X::emitMetadata(double frameTime)
{
    if (loggingEgress != NULL) {
        if (!haveEmittedLogMeta) {
            haveEmittedLogMeta = true;
            loggingMux.incoming(LogStream_Metadata, buildLogMeta(frameTime), loggingEgress);
        }
        loggingMux.advance(LogStream_Metadata, frameTime, loggingEgress);
    } else {
        invalidateLogValues(frameTime);
    }

    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;

        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }
}

void AcquireTSICPC377X::streamAdvance(int streamID, double time)
{
    quint32 bits = 1 << streamID;
    if (loggingEgress != NULL) {
        for (int i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
            if (i == streamID)
                continue;
            if (!(streamAge[i] & bits)) {
                streamAge[i] |= bits;
                continue;
            }
            streamAge[i] = bits;
            streamTime[i] = FP::undefined();
            loggingMux.advance(i, time, loggingEgress);
        }

        streamAge[streamID] = bits;
        streamTime[streamID] = time;
        loggingMux.advance(streamID, time, loggingEgress);
    } else {
        streamAge[streamID] = 0;
        streamTime[streamID] = FP::undefined();
    }

    if (!(streamAge[LogStream_State] & bits)) {
        streamAge[LogStream_State] |= bits;
    } else {
        double startTime = streamTime[LogStream_State];
        double endTime = time;

        if (loggingEgress != NULL) {
            streamAge[LogStream_State] = bits;
            streamTime[LogStream_State] = endTime;
        } else {
            streamAge[LogStream_State] = 0;
            streamTime[LogStream_State] = FP::undefined();
        }

        Variant::Flags flags;
        if (liquidLow)
            flags.insert("LiquidLow");
        if (!hadUncorrectedCounts)
            flags.insert("CoincidenceCorrected");
        hadUncorrectedCounts = !haveCoincidenceCorrection;

        logValue(startTime, endTime, LogStream_State, "F1", Variant::Root(std::move(flags)));
        loggingMux.advance(LogStream_State, endTime, loggingEgress);
    }
}

void AcquireTSICPC377X::describeState(Variant::Write &info, const Command &command) const
{
    if (command.getType() != COMMAND_INVALID) {
        info.hash("Command").set(command.stateDescription());
    } else if (!commandQueue.isEmpty()) {
        info.hash("Command").set(commandQueue.first().stateDescription());
    }

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        info.hash("ResponseState").setString("AutoprobePassiveInitialize");
        break;
    case RESP_AUTOPROBE_PASSIVE_WAIT:
        info.hash("ResponseState").setString("AutoprobePassiveWait");
        break;
    case RESP_PASSIVE_WAIT:
        info.hash("ResponseState").setString("PassiveWait");
        break;
    case RESP_PASSIVE_RUN:
        info.hash("ResponseState").setString("PassiveRun");
        break;
    case RESP_INTERACTIVE_INITIALIZE:
        info.hash("ResponseState").setString("InteractiveInitialize");
        break;
    case RESP_INTERACTIVE_RESTART_WAIT:
        info.hash("ResponseState").setString("InteractiveRestartWait");
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS:
        info.hash("ResponseState").setString("StartInteractiveStopReports");
        break;
    case RESP_INTERACTIVE_START_READ_MODELNUMBER:
        info.hash("ResponseState").setString("InteractiveStartModelNumber");
        break;
    case RESP_INTERACTIVE_START_READ_MODELNUMBER_RETRY:
        info.hash("ResponseState").setString("InteractiveStartModelNumberRetry");
        break;
    case RESP_INTERACTIVE_START_READ_SERIALNUMBER:
        info.hash("ResponseState").setString("InteractiveStartSerialNumber");
        break;
    case RESP_INTERACTIVE_START_READ_FIRMWAREVERSION:
        info.hash("ResponseState").setString("InteractiveStartFirmwareVersion");
        break;
    case RESP_INTERACTIVE_START_READ_CALIBRATION:
        info.hash("ResponseState").setString("InteractiveStartCalibration");
        break;
    case RESP_INTERACTIVE_START_READ_ERRORS:
        info.hash("ResponseState").setString("InteractiveStartErrors");
        break;
    case RESP_INTERACTIVE_START_SET_OPERATINGMODE:
        info.hash("ResponseState").setString("InteractiveStartOperatingMode");
        break;
    case RESP_INTERACTIVE_START_SET_CORRECTED:
        info.hash("ResponseState").setString("InteractiveStartCorrected");
        break;
    case RESP_INTERACTIVE_START_SET_YEAR:
        info.hash("ResponseState").setString("InteractiveStartYear");
        break;
    case RESP_INTERACTIVE_START_SET_MONTH:
        info.hash("ResponseState").setString("InteractiveStartMonth");
        break;
    case RESP_INTERACTIVE_START_SET_DAY:
        info.hash("ResponseState").setString("InteractiveStartDay");
        break;
    case RESP_INTERACTIVE_START_SET_HOUR:
        info.hash("ResponseState").setString("InteractiveStartHour");
        break;
    case RESP_INTERACTIVE_START_SET_MINUTE:
        info.hash("ResponseState").setString("InteractiveStartMinute");
        break;
    case RESP_INTERACTIVE_START_SET_SECOND:
        info.hash("ResponseState").setString("InteractiveStartSecond");
        break;
    case RESP_INTERACTIVE_START_READ_ALL:
        info.hash("ResponseState").setString("InteractiveStartAll");
        break;

    case RESP_INTERACTIVE_RUN_READ_ALL:
        info.hash("ResponseState").setString("InteractiveRunAll");
        break;
    case RESP_INTERACTIVE_RUN_READ_SAMPLEFLOW:
        info.hash("ResponseState").setString("InteractiveRunSampleFlow");
        break;
    case RESP_INTERACTIVE_RUN_READ_INLETFLOW:
        info.hash("ResponseState").setString("InteractiveRunInletFlow");
        break;
    case RESP_INTERACTIVE_RUN_READ_COUNTS:
        info.hash("ResponseState").setString("InteractiveRunCounts");
        break;
    case RESP_INTERACTIVE_RUN_WAIT:
        info.hash("ResponseState").setString("InteractiveRunWait");
        break;
    case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
        info.hash("ResponseState").setString("InteractiveRunExternal");
        break;
    }
}

AcquireTSICPC377X::Command::Command() : type(COMMAND_INVALID), counter(0)
{ }

AcquireTSICPC377X::Command::Command(CommandType t) : type(t), counter(0)
{ }

AcquireTSICPC377X::Command::Command(const Command &other) : type(other.type), counter(other.counter)
{ }

AcquireTSICPC377X::Command &AcquireTSICPC377X::Command::operator=(const Command &other)
{
    if (&other == this)
        return *this;
    type = other.type;
    counter = other.counter;
    return *this;
}

Variant::Root AcquireTSICPC377X::Command::stateDescription() const
{
    Variant::Root result;

    switch (type) {
    default:
        result["Type"].setString("Unknown");
        result["Counter"].setInt64(counter);
        break;
    case COMMAND_D:
        result["Type"].setString("D");
        result["Counter"].setInt64(counter);
        break;
    case COMMAND_RSF:
        result["Type"].setString("RSF");
        break;
    case COMMAND_RIF:
        result["Type"].setString("RIF");
        break;
    case COMMAND_RTS:
        result["Type"].setString("RTS");
        break;
    case COMMAND_RTC:
        result["Type"].setString("RTC");
        break;
    case COMMAND_RTO:
        result["Type"].setString("RTO");
        break;
    case COMMAND_RTA:
        result["Type"].setString("RTA");
        break;
    case COMMAND_RIE:
        result["Type"].setString("RIE");
        break;
    case COMMAND_RPA:
        result["Type"].setString("RPA");
        break;
    case COMMAND_RPO:
        result["Type"].setString("RPO");
        break;
    case COMMAND_RPN:
        result["Type"].setString("RPN");
        break;
    case COMMAND_RPS:
        result["Type"].setString("RPS");
        break;
    case COMMAND_RLP:
        result["Type"].setString("RLP");
        break;
    case COMMAND_RLL:
        result["Type"].setString("RLL");
        break;
    case COMMAND_RCOUNT1:
        result["Type"].setString("RCOUNT1");
        break;
    case COMMAND_RCOUNT2:
        result["Type"].setString("RCOUNT2");
        break;
    case COMMAND_RALL:
        result["Type"].setString("RALL");
        break;
    case COMMAND_RFV:
        result["Type"].setString("RFV");
        break;
    case COMMAND_RSN:
        result["Type"].setString("RSN");
        break;
    case COMMAND_RMN:
        result["Type"].setString("RMN");
        break;
    case COMMAND_SCD:
        result["Type"].setString("SCD");
        break;
    }
    return result;
}

void AcquireTSICPC377X::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
}

void AcquireTSICPC377X::configurationAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

static bool validDigit(char c)
{ return c >= '0' && c <= '9'; }

static bool isOnlyDigits(const Util::ByteView &frame)
{
    if (frame.empty())
        return false;
    for (auto p : frame) {
        if (!validDigit(p))
            return false;
    }
    return true;
}

static bool isUnpolledRecord(const Util::ByteView &frame)
{
    if (frame.size() < 3)
        return false;
    if (!frame.string_start("U"))
        return false;
    /* Look for "UX,Y" where X is an integer and Y is a digit */
    for (const char *p = frame.data<const char *>(1),
            *endP = frame.data<const char *>() + frame.size(); p != endP; ++p) {
        if (!validDigit(*p)) {
            if (*p != ',')
                return false;
            p++;
            if (p == endP)
                return false;
            if (!validDigit(*p))
                return false;
            p++;
            if (p == endP)
                return false;
            return true;
        }
    }
    return false;
}

static double calculateRate(double CNT, double LT)
{
    // Per JAO email 2022-03-16, do not use the live time correction
    return CNT;
    /*if (!FP::defined(CNT) || !FP::defined(LT))
        return FP::undefined();
    if (CNT < 0.0 || LT <= 0.0)
        return FP::undefined();
    return CNT / LT;*/
}

static double calculateConcentration(double C, double Q)
{
    if (!FP::defined(C) || !FP::defined(Q))
        return FP::undefined();
    if (Q <= 0.0)
        return FP::undefined();
    return C / (Q * (1000.0 / 60.0));
}

int AcquireTSICPC377X::translateFlags(const Util::ByteView &field)
{
    bool ok = false;
    Variant::Root flags(field.parse_i64(&ok, 16));
    if (!ok) return 1;
    if (!INTEGER::defined(flags.read().toInteger())) return 2;
    remap("FRAW", flags);

    qint64 bits = flags.read().toInt64();
    if (!INTEGER::defined(bits))
        return 0;

    QSet<QString> oldFlags;
    qSwap(oldFlags, errorFlags);
    for (int i = 0;
            i < (int) (sizeof(instrumentFlagTranslation) / sizeof(instrumentFlagTranslation[0]));
            i++) {
        if (bits & (Q_INT64_C(1) << i)) {
            if (instrumentFlagTranslation[i] != NULL) {
                errorFlags |= QString::fromLatin1(instrumentFlagTranslation[i]);
            } else {
                continue;
            }
        }
    }
    if (errorFlags != oldFlags)
        forceRealtimeStateEmit = true;

    return 0;
}

double AcquireTSICPC377X::getDefaultFlow() const
{
    if (FP::defined(config.first().flow))
        return config.first().flow;
    const auto &model = instrumentMeta["Model"].toString();
    if (model == "3771" || model == "3772")
        return 1.0;
    /* 3775 */
    return 0.3;
}

int AcquireTSICPC377X::translateLiquidLevel(double frameTime, Util::ByteView field)
{
    qint64 adc = INTEGER::undefined();
    if (field.string_start("FULL (")) {
        auto idxEnd = field.indexOf(')');
        if (idxEnd != field.npos)
            field = field.mid(0, idxEnd);
        bool ok = false;
        adc = field.mid(6).parse_i64(&ok, 10);
        if (!ok) return 1;
        if (!INTEGER::defined(adc)) return 2;
        liquidLow = false;
    } else if (field.string_start("NOTFULL (")) {
        auto idxEnd = field.indexOf(')');
        if (idxEnd != field.npos)
            field = field.mid(0, idxEnd);
        bool ok = false;
        adc = field.mid(9).parse_i64(&ok, 10);
        if (!ok) return 3;
        if (!INTEGER::defined(adc)) return 4;
        liquidLow = true;
    } else if (field == "FULL") {
        liquidLow = false;
    } else if (field == "NOTFULL") {
        liquidLow = true;
    } else {
        return 5;
    }

    realtimeValue(frameTime, "ZLIQUID", Variant::Root(adc));
    realtimeValue(frameTime, "ZFULL", Variant::Root(!liquidLow));

    return 0;
}

int AcquireTSICPC377X::processResponse(const Util::ByteView &frame, double frameTime)
{
    Util::ByteView field;
    bool ok = false;

    if (isUnpolledRecord(frame)) {
        /* This is an unpolled response that could come in at any time, so
         * just handle it and continue as normal */

        auto fields = Util::as_deque(frame.split(','));
        if (fields.empty()) return 99001;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (!field.string_start("U")) return 99002;
        field = field.mid(1);
        qint64 elapsed = field.parse_i64(&ok);
        if (!ok) return 99003;
        if (elapsed < 0) return 99004;

        if (fields.size() == 23) {
            /* Data format 1 */

            qint64 countSum = 0;
            for (int i = 0; i < 10; i++) {
                if (fields.empty()) return 99100 + i;
                field = fields.front().string_trimmed();
                fields.pop_front();
                qint64 add = field.parse_i64(&ok);
                if (!ok) return 99120 + i;
                if (add < 0) return 99140 + i;
                countSum += add;
            }
            Variant::Root C((double) countSum);
            remap("C", C);

            double sumConcentration = 0.0;
            int countConcentration = 0;
            for (int i = 0; i < 10; i++) {
                if (fields.empty()) return 99200 + i;
                field = fields.front().string_trimmed();
                fields.pop_front();
                double add = field.parse_real(&ok);
                if (!ok) return 99220 + i;
                if (!FP::defined(add)) return 99230 + i;
                sumConcentration += add;
                countConcentration++;
            }
            Variant::Root ZND;
            if (countConcentration > 0)
                ZND.write().setDouble(sumConcentration / (double) countConcentration);
            remap("ZND", ZND);


            /* Analog input 1 */
            if (fields.empty()) return 99005;
            fields.pop_front();

            /* Analog input 2 */
            if (fields.empty()) return 99006;
            fields.pop_front();

            if (fields.empty()) return 99007;
            field = fields.front().string_trimmed();
            fields.pop_front();
            int code = translateFlags(field.string_trimmed());
            if (code != 0)
                return 99990 + code;

            double Q = lastReportedFlow;
            if (!FP::defined(Q)) {
                Q = getDefaultFlow();
                Variant::Root v(Q);
                remap("Q", v);
                Q = v.read().toDouble();
            }

            Variant::Root N(calculateConcentration(C.read().toDouble(), Q));
            remap("N", N);

            emitMetadata(frameTime);
            logValue(streamTime[LogStream_Counts], frameTime, LogStream_Counts, "N", std::move(N));

            streamAdvance(LogStream_Counts, frameTime);

            realtimeValue(frameTime, "C", std::move(C));
            realtimeValue(frameTime, "ZND", std::move(ZND));
        } else if (fields.size() == 40) {
            /* Data format 2, 3776 */
            if (instrumentMeta["Model"].toString() != "3776") {
                instrumentMeta["Model"].setString("3776");
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }
            haveCoincidenceCorrection = false;

            double sumConcentration = 0.0;
            int countConcentration = 0;
            for (int i = 0; i < 10; i++) {
                if (fields.empty()) return 99300 + i;
                field = fields.front().string_trimmed();
                fields.pop_front();
                double add = field.parse_real(&ok);
                if (!ok) return 99320 + i;
                if (!FP::defined(add)) return 99340 + i;
                sumConcentration += add;
                countConcentration++;
            }
            Variant::Root ZND;
            if (countConcentration > 0)
                ZND.write().setDouble(sumConcentration / (double) countConcentration);
            remap("ZND", ZND);

            qint64 countSum = 0;
            for (int i = 0; i < 10; i++) {
                if (fields.empty()) return 99400 + i;
                field = fields.front().string_trimmed();
                fields.pop_front();
                qint64 add = field.parse_i64(&ok);
                if (!ok) return 99420 + i;
                if (add < 0) return 99440 + i;
                countSum += add;
            }
            Variant::Root CNT((double) countSum);
            remap("CNT", CNT);

            double sumFlow = 0.0;
            for (int i = 0; i < 10; i++) {
                if (fields.empty()) return 99500 + i;
                field = fields.front().string_trimmed();
                fields.pop_front();
                double add = field.parse_real(&ok);
                if (!ok) return 99520 + i;
                if (!FP::defined(add)) return 99540 + i;
                sumFlow += add;
            }
            Variant::Root Q(sumFlow * (60.0 / 1000.0));
            Variant::Root ZQ = Q;
            remap("ZQ", ZQ);
            if (FP::defined(config.first().flow))
                Q.write().setDouble(config.first().flow);
            remap("Q", Q);
            lastReportedFlow = Q.read().toDouble();

            double sumDT = 0.0;
            for (int i = 0; i < 10; i++) {
                if (fields.empty()) return 99600 + i;
                field = fields.front().string_trimmed();
                fields.pop_front();
                double add = field.parse_real(&ok);
                if (!ok) return 99620 + i;
                if (!FP::defined(add)) return 99640 + i;
                sumDT += add;
            }
            Variant::Root LT(1.0 - sumDT);
            remap("LTIME", LT);

            Variant::Root C(calculateRate(CNT.read().toDouble(), LT.read().toDouble()));
            remap("C", C);

            Variant::Root N(calculateConcentration(C.read().toDouble(), Q.read().toDouble()));
            remap("N", N);

            emitMetadata(frameTime);
            logValue(streamTime[LogStream_SampleFlow], frameTime, LogStream_SampleFlow, "Q",
                     std::move(Q));
            logValue(streamTime[LogStream_Counts], frameTime, LogStream_Counts, "N", std::move(N));

            streamAdvance(LogStream_SampleFlow, frameTime);
            streamAdvance(LogStream_Counts, frameTime);

            realtimeValue(frameTime, "C", std::move(C));
            realtimeValue(frameTime, "ZND", std::move(ZND));
            realtimeValue(frameTime, "ZQ", std::move(ZQ));
        } else if (fields.size() == 32) {
            /* Data format 2, 3771/3372/3775 */

            double sumConcentration = 0.0;
            int countConcentration = 0;
            for (int i = 0; i < 10; i++) {
                if (fields.empty()) return 99700 + i;
                field = fields.front().string_trimmed();
                fields.pop_front();
                double add = field.parse_real(&ok);
                if (!ok) return 99720 + i;
                if (!FP::defined(add)) return 99740 + i;
                sumConcentration += add;
                countConcentration++;
            }
            Variant::Root ZND;
            if (countConcentration > 0)
                ZND.write().setDouble(sumConcentration / (double) countConcentration);
            remap("ZND", ZND);

            qint64 countSum = 0;
            for (int i = 0; i < 10; i++) {
                if (fields.empty()) return 99800 + i;
                field = fields.front().string_trimmed();
                fields.pop_front();
                qint64 add = field.parse_i64(&ok);
                if (!ok) return 99820 + i;
                if (add < 0) return 99840 + i;
                countSum += add;
            }
            Variant::Root CNT((double) countSum);
            remap("CNT", CNT);

            if (fields.empty()) return 99008;
            field = fields.front().string_trimmed();
            fields.pop_front();
            double value = field.parse_real(&ok);
            if (!ok) return 99009;
            if (!FP::defined(value)) return 99010;
            Variant::Root Q(value * (60.0 / 1000.0));
            Variant::Root ZQ = Q;
            remap("ZQ", ZQ);
            if (FP::defined(config.first().flow))
                Q.write().setDouble(config.first().flow);
            remap("Q", Q);
            lastReportedFlow = Q.read().toDouble();

            /* Dead time correction */
            if (fields.empty()) return 99011;
            fields.pop_front();

            double sumDT = 0.0;
            for (int i = 0; i < 10; i++) {
                if (fields.empty()) return 99900 + i;
                field = fields.front().string_trimmed();
                fields.pop_front();
                double add = field.parse_real(&ok);
                if (!ok) return 99920 + i;
                if (!FP::defined(add)) return 99940 + i;
                sumDT += add;
            }
            Variant::Root LT(1.0 - sumDT);
            remap("LTIME", LT);

            Variant::Root C(calculateRate(CNT.read().toDouble(), LT.read().toDouble()));
            remap("C", C);

            Variant::Root N(calculateConcentration(C.read().toDouble(), Q.read().toDouble()));
            remap("N", N);

            emitMetadata(frameTime);
            logValue(streamTime[LogStream_SampleFlow], frameTime, LogStream_SampleFlow, "Q",
                     std::move(Q));
            logValue(streamTime[LogStream_Counts], frameTime, LogStream_Counts, "N", std::move(N));

            streamAdvance(LogStream_SampleFlow, frameTime);
            streamAdvance(LogStream_Counts, frameTime);

            realtimeValue(frameTime, "C", std::move(C));
            realtimeValue(frameTime, "ZND", std::move(ZND));
            realtimeValue(frameTime, "ZQ", std::move(ZQ));
        } else {
            return 99998;
        }

        switch (responseState) {
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
            if (commandQueue.isEmpty())
                return 0;
            break;
        default:
            break;
        }
        /* Unless we're in passive mode with no other responses pending, 
         * we don't consider this to be a real response */
        return -1;
    }

    if (commandQueue.isEmpty()) {
        if (!isOnlyDigits(frame))
            return -1;
        /* If we have no command and we get a line of only digits, assume
         * it's the start of a D command so we can use the unpolled
         * mode */
        commandQueue.append(Command(COMMAND_D));
    }

    Command command(commandQueue.takeFirst());
    switch (command.getType()) {
    case COMMAND_INVALID:
        return 2;

    case COMMAND_IGNORED:
        return -1;

    case COMMAND_D:
        if (command.getCounter() == 0) {
            countSeconds = Util::ByteView(frame).string_trimmed().parse_i64(&ok, 10);
            if (!ok) return 1000;
            if (countSeconds < 0) return 1001;
            command.setCounter(1);
            commandQueue.prepend(command);
            return -2;
        } else {
            auto fields = Util::as_deque(frame.split(','));
            if (fields.size() != 2) return 1002;
            if (command.getCounter() == 1) {
                field = fields.front().string_trimmed();
                fields.pop_front();
                if (!isOnlyDigits(field)) return 1003;
                countPulses = field.parse_i64(&ok, 10);
                if (countPulses < 0) return 1004;
                if (!INTEGER::defined(countPulses)) return 1005;
            } else {
                field = fields.front().string_trimmed();
                fields.pop_front();
                if (!isOnlyDigits(field)) return 1006;
                field.parse_i64(&ok, 10);
            }
            if (!ok) return 1007;
            field = fields.front().string_trimmed();
            fields.pop_front();
            if (!isOnlyDigits(field)) return 1008;
            field.parse_i64(&ok, 10);
            if (!ok) return 1009;
            Q_ASSERT(fields.empty());

            if (command.getCounter() < 16) {
                command.setCounter(command.getCounter() + 1);
                commandQueue.prepend(command);
                return -2;
            }
        }

        switch (responseState) {
        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);

        if (countSeconds > 0) {
            double startTime = streamTime[LogStream_Counts];
            double endTime = frameTime;
            double rate = (double) countPulses / (double) countSeconds;

            Variant::Root C(rate);
            remap("C", C);

            double Q = lastReportedFlow;
            if (!FP::defined(Q)) {
                Q = getDefaultFlow();
                Variant::Root v(Q);
                remap("Q", v);
                Q = v.read().toDouble();
            }

            Variant::Root N(calculateConcentration(C.read().toDouble(), Q));
            remap("N", N);

            logValue(startTime, endTime, LogStream_Counts, "N", std::move(N));
            realtimeValue(frameTime, "C", std::move(C));

            streamAdvance(LogStream_Counts, frameTime);
        }

        return -1;

    case COMMAND_RFV: {
        field = Util::ByteView(frame).string_trimmed();
        if (field == "ERROR") return 1000;
        if (field.empty()) return 1001;
        auto label = field.toString();
        if (instrumentMeta["FirmwareVersion"].toString() != label) {
            instrumentMeta["FirmwareVersion"].setString(std::move(label));
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_START_READ_FIRMWAREVERSION:
            if (controlStream != NULL) {
                controlStream->writeControl("SCD\r");
            }
            responseState = RESP_INTERACTIVE_START_READ_CALIBRATION;
            commandQueue.append(Command(COMMAND_SCD));
            timeoutAt(frameTime + 2.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadCabliration"), frameTime, FP::undefined()));
            }
            break;

        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        default:
            return -1;
        }
        break;
    }

    case COMMAND_RSN: {
        field = Util::ByteView(frame).string_trimmed();
        if (field == "ERROR") return 1000;
        if (field.empty()) return 1001;

        Variant::Root value;
        if (isOnlyDigits(field))
            value.write().setInt64(field.parse_i64());
        else
            value.write().setString(field.toString());

        if (instrumentMeta["SerialNumber"] != value.read()) {
            instrumentMeta["SerialNumber"].set(value);
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_START_READ_SERIALNUMBER:
            if (controlStream != NULL) {
                controlStream->writeControl("RFV\r");
            }
            responseState = RESP_INTERACTIVE_START_READ_FIRMWAREVERSION;
            commandQueue.append(Command(COMMAND_RFV));
            timeoutAt(frameTime + 2.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadFirmwareVersion"), frameTime, FP::undefined()));
            }
            break;

        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        default:
            return -1;
        }
        break;
    }

    case COMMAND_RMN: {
        field = Util::ByteView(frame).string_trimmed();
        if (field == "ERROR") return 1000;
        if (field.empty()) return 1001;
        auto label = field.toString();
        if (instrumentMeta["Model"].toString() != label) {
            instrumentMeta["Model"].setString(label);
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        if (label == "3771" || label == "3772")
            haveCoincidenceCorrection = true;
        else
            haveCoincidenceCorrection = false;

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_START_READ_MODELNUMBER:
        case RESP_INTERACTIVE_START_READ_MODELNUMBER_RETRY:
            if (controlStream != NULL) {
                controlStream->writeControl("RSN\r");
            }
            responseState = RESP_INTERACTIVE_START_READ_SERIALNUMBER;
            commandQueue.append(Command(COMMAND_RSN));
            timeoutAt(frameTime + 2.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadSerialNumber"), frameTime, FP::undefined()));
            }
            break;

        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        default:
            return -1;
        }
        break;
    }

    case COMMAND_RSF: {
        field = Util::ByteView(frame).string_trimmed();
        double value = field.parse_real(&ok);
        if (!ok) return 2000;
        if (!FP::defined(value)) return 2001;
        Variant::Root Q(value / 1000.0);
        Variant::Root ZQ = Q;
        remap("ZQ", ZQ);
        if (FP::defined(config.first().flow))
            Q.write().setDouble(config.first().flow);
        remap("Q", Q);
        lastReportedFlow = Q.read().toDouble();

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_READ_SAMPLEFLOW:
            if (controlStream != NULL) {
                controlStream->writeControl("RIF\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_INLETFLOW;
            commandQueue.append(Command(COMMAND_RIF));
            break;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);
        logValue(streamTime[LogStream_SampleFlow], frameTime, LogStream_SampleFlow, "Q",
                 std::move(Q));
        realtimeValue(frameTime, "ZQ", std::move(ZQ));

        streamAdvance(LogStream_SampleFlow, frameTime);
        break;
    }

    case COMMAND_RIF: {
        field = Util::ByteView(frame).string_trimmed();
        double value = field.parse_real(&ok);
        if (!ok) return 3000;
        if (!FP::defined(value)) return 3001;
        Variant::Root Qu(value);
        remap("Qu", Qu);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_READ_INLETFLOW:
            if (controlStream != NULL) {
                controlStream->writeControl("RCOUNT2\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_COUNTS;
            commandQueue.append(Command(COMMAND_RCOUNT2));
            break;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);
        logValue(streamTime[LogStream_InletFlow], frameTime, LogStream_InletFlow, "Qu",
                 std::move(Qu));

        streamAdvance(LogStream_InletFlow, frameTime);
        break;
    }

    case COMMAND_RTS: {
        field = Util::ByteView(frame).string_trimmed();
        Variant::Root T1(field.parse_real(&ok));
        if (!ok) return 4000;
        if (!FP::defined(T1.read().toReal())) return 4001;
        remap("T1", T1);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);
        logValue(streamTime[LogStream_SaturatorTemperature], frameTime,
                 LogStream_SaturatorTemperature, "T1", std::move(T1));

        streamAdvance(LogStream_SaturatorTemperature, frameTime);
        return 0;
    }

    case COMMAND_RTC: {
        field = Util::ByteView(frame).string_trimmed();
        Variant::Root T2(field.parse_real(&ok));
        if (!ok) return 5000;
        if (!FP::defined(T2.read().toReal())) return 5001;
        remap("T2", T2);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);
        logValue(streamTime[LogStream_CondenserTemperature], frameTime,
                 LogStream_CondenserTemperature, "T2", std::move(T2));

        streamAdvance(LogStream_CondenserTemperature, frameTime);
        return 0;
    }

    case COMMAND_RTO: {
        field = Util::ByteView(frame).string_trimmed();
        Variant::Root T3(field.parse_real(&ok));
        if (!ok) return 6000;
        if (!FP::defined(T3.read().toReal())) return 6001;
        remap("T3", T3);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);
        logValue(streamTime[LogStream_OpticsTemperature], frameTime, LogStream_OpticsTemperature,
                 "T3", std::move(T3));

        streamAdvance(LogStream_OpticsTemperature, frameTime);
        return 0;
    }

    case COMMAND_RTA: {
        field = Util::ByteView(frame).string_trimmed();
        Variant::Root T4(field.parse_real(&ok));
        if (!ok) return 7000;
        if (!FP::defined(T4.read().toReal())) return 7001;
        remap("T4", T4);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);
        logValue(streamTime[LogStream_CabinetTemperature], frameTime, LogStream_CabinetTemperature,
                 "T4", std::move(T4));

        streamAdvance(LogStream_CabinetTemperature, frameTime);
        return 0;
    }

    case COMMAND_RIE: {
        int code = translateFlags(Util::ByteView(frame).string_trimmed());
        if (code != 0)
            return 8000 + code;

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_INTERACTIVE_START_READ_ERRORS:
            if (controlStream != NULL) {
                controlStream->writeControl("SCM,0\r");
            }
            responseState = RESP_INTERACTIVE_START_SET_OPERATINGMODE;
            commandQueue.append(Command(COMMAND_OK));
            timeoutAt(frameTime + 2.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveSetOperatingMode"), frameTime, FP::undefined()));
            }
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        return 0;
    }

    case COMMAND_RPA: {
        field = Util::ByteView(frame).string_trimmed();
        double value = field.parse_real(&ok);
        if (!ok) return 9000;
        if (!FP::defined(value)) return 9001;
        Variant::Root P(value * 10.0);
        remap("P", P);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);
        logValue(streamTime[LogStream_AmbientPressure], frameTime, LogStream_AmbientPressure, "P",
                 std::move(P));

        streamAdvance(LogStream_AmbientPressure, frameTime);
        return 0;
    }

    case COMMAND_RPO: {
        field = Util::ByteView(frame).string_trimmed();
        double value = field.parse_real(&ok);
        if (!ok) return 10000;
        if (!FP::defined(value)) return 10001;
        Variant::Root Pd2(value * 10.0);
        remap("Pd2", Pd2);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);
        logValue(streamTime[LogStream_OrificePressure], frameTime, LogStream_OrificePressure, "Pd2",
                 std::move(Pd2));

        streamAdvance(LogStream_OrificePressure, frameTime);
        return 0;
    }

    case COMMAND_RPN: {
        field = Util::ByteView(frame).string_trimmed();
        double value = field.parse_real(&ok);
        if (!ok) return 10000;
        if (!FP::defined(value)) return 10001;
        Variant::Root Pd1(value * 10.0);
        remap("Pd1", Pd1);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);
        logValue(streamTime[LogStream_NozzlePressure], frameTime, LogStream_NozzlePressure, "Pd1",
                 std::move(Pd1));

        streamAdvance(LogStream_NozzlePressure, frameTime);
        return 0;
    }

    case COMMAND_RPS: {
        field = Util::ByteView(frame).string_trimmed();
        double value = field.parse_real(&ok);
        if (!ok) return 11000;
        if (!FP::defined(value)) return 11001;
        Variant::Root P(value * 2.4909);
        remap("P", P);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);
        logValue(streamTime[LogStream_AmbientPressure], frameTime, LogStream_AmbientPressure, "P",
                 std::move(P));

        streamAdvance(LogStream_AmbientPressure, frameTime);
        return 0;
    }

    case COMMAND_RLP: {
        field = Util::ByteView(frame).string_trimmed();
        Variant::Root A(field.parse_real(&ok));
        if (!ok) return 12000;
        if (!FP::defined(A.read().toReal())) return 12001;
        remap("A", A);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);
        logValue(streamTime[LogStream_LaserCurrent], frameTime, LogStream_LaserCurrent, "A",
                 std::move(A));

        streamAdvance(LogStream_LaserCurrent, frameTime);
        return 0;
    }

    case COMMAND_RLL: {
        int code = translateLiquidLevel(frameTime, Util::ByteView(frame).string_trimmed());
        if (code != 0)
            return 13000 + code;

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        return 0;
    }

    case COMMAND_RCOUNT1: {
        field = Util::ByteView(frame).string_trimmed();
        Variant::Root C(field.parse_real(&ok));
        if (!ok) return 14000;
        if (!FP::defined(C.read().toReal())) return 14001;
        remap("C", C);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_INTERACTIVE_RUN_READ_COUNTS:
            if (!externalCommandQueue.empty())
                break;
            responseState = RESP_INTERACTIVE_RUN_WAIT;
            timeoutAt(frameTime + config.first().pollInterval);
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        double Q = lastReportedFlow;
        if (!FP::defined(Q)) {
            Q = getDefaultFlow();
            Variant::Root v(Q);
            remap("Q", v);
            Q = v.read().toDouble();
        }

        Variant::Root N(calculateConcentration(C.read().toDouble(), Q));
        remap("N", N);

        logValue(streamTime[LogStream_Counts], frameTime, LogStream_Counts, "N", std::move(N));
        realtimeValue(frameTime, "C", std::move(C));

        streamAdvance(LogStream_Counts, frameTime);
        break;
    }

    case COMMAND_RCOUNT2: {
        field = Util::ByteView(frame).string_trimmed();
        Variant::Root C(field.parse_real(&ok));
        if (!ok) return 15000;
        if (!FP::defined(C.read().toReal())) return 15001;
        remap("C", C);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_INTERACTIVE_RUN_READ_COUNTS:
            if (!externalCommandQueue.empty())
                break;
            responseState = RESP_INTERACTIVE_RUN_WAIT;
            timeoutAt(frameTime + config.first().pollInterval);
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        double Q = lastReportedFlow;
        if (!FP::defined(Q)) {
            Q = getDefaultFlow();
            Variant::Root v(Q);
            remap("Q", v);
            Q = v.read().toDouble();
        }

        hadUncorrectedCounts = true;
        Variant::Root N(calculateConcentration(C.read().toDouble(), Q));
        remap("N", N);

        logValue(streamTime[LogStream_Counts], frameTime, LogStream_Counts, "N", std::move(N));
        realtimeValue(frameTime, "C", std::move(C));

        streamAdvance(LogStream_Counts, frameTime);
        return 0;
    }

    case COMMAND_RALL: {
        auto fields = Util::as_deque(frame.split(','));

        if (fields.empty()) return 16000;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root ZND(field.parse_real(&ok));
        if (!ok) return 16001;
        if (!FP::defined(ZND.read().toReal())) return 16002;
        remap("ZND", ZND);

        if (fields.empty()) return 16003;
        field = fields.front().string_trimmed();
        fields.pop_front();
        int code = translateFlags(field);
        if (code != 0) return 16100 + code;

        if (fields.empty()) return 16004;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root T1(field.parse_real(&ok));
        if (!ok) return 16005;
        if (!FP::defined(T1.read().toReal())) return 16006;
        remap("T1", T1);

        if (fields.empty()) return 16007;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root T2(field.parse_real(&ok));
        if (!ok) return 16008;
        if (!FP::defined(T2.read().toReal())) return 16009;
        remap("T2", T2);

        if (fields.empty()) return 16010;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root T3(field.parse_real(&ok));
        if (!ok) return 16011;
        if (!FP::defined(T3.read().toReal())) return 16012;
        remap("T3", T3);

        if (fields.empty()) return 16011;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root T4(field.parse_real(&ok));
        if (!ok) return 16012;
        if (!FP::defined(T4.read().toReal())) return 16013;
        remap("T4", T4);

        if (fields.empty()) return 16014;
        field = fields.front().string_trimmed();
        fields.pop_front();
        double value = field.parse_real(&ok);
        if (!ok) return 16015;
        if (!FP::defined(value)) return 16016;
        Variant::Root P(value * 10.0);
        remap("P", P);

        if (fields.empty()) return 16017;
        field = fields.front().string_trimmed();
        fields.pop_front();
        value = field.parse_real(&ok);
        if (!ok) return 16018;
        if (!FP::defined(value)) return 16019;
        Variant::Root Pd2(value * 10.0);
        remap("Pd2", Pd2);

        if (fields.empty()) return 16020;
        field = fields.front().string_trimmed();
        fields.pop_front();
        value = field.parse_real(&ok);
        if (!ok) return 16021;
        if (!FP::defined(value)) return 16022;
        Variant::Root Pd1(value * 10.0);
        remap("Pd1", Pd1);

        if (fields.empty()) return 16023;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root A(field.parse_real(&ok));
        if (!ok) return 16024;
        if (!FP::defined(A.read().toReal())) return 16025;
        remap("A", A);

        if (fields.empty()) return 16026;
        field = fields.front().string_trimmed();
        fields.pop_front();
        code = translateLiquidLevel(frameTime, field);
        if (code != 0) return 16200 + code;

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_RUN_READ_ALL:
            if (controlStream != NULL) {
                controlStream->writeControl("RSF\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_SAMPLEFLOW;
            commandQueue.append(Command(COMMAND_RSF));
            break;

        case RESP_INTERACTIVE_START_READ_ALL: {
            Variant::Write info = Variant::Write::empty();
            describeState(info, command);
            event(frameTime, QObject::tr("Communications established."), false, info);

            if (controlStream != NULL) {
                controlStream->writeControl("RSF\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_SAMPLEFLOW;
            commandQueue.append(Command(COMMAND_RSF));
            forceRealtimeStateEmit = true;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                      FP::undefined()));
            }
            break;
        }

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);
        logValue(streamTime[LogStream_SaturatorTemperature], frameTime,
                 LogStream_SaturatorTemperature, "T1", std::move(T1));
        logValue(streamTime[LogStream_CondenserTemperature], frameTime,
                 LogStream_CondenserTemperature, "T2", std::move(T2));
        logValue(streamTime[LogStream_OpticsTemperature], frameTime, LogStream_OpticsTemperature,
                 "T3", std::move(T3));
        logValue(streamTime[LogStream_CabinetTemperature], frameTime, LogStream_CabinetTemperature,
                 "T4", std::move(T4));
        logValue(streamTime[LogStream_AmbientPressure], frameTime, LogStream_AmbientPressure, "P",
                 std::move(P));
        logValue(streamTime[LogStream_NozzlePressure], frameTime, LogStream_NozzlePressure, "Pd1",
                 std::move(Pd1));
        logValue(streamTime[LogStream_OrificePressure], frameTime, LogStream_OrificePressure, "Pd2",
                 std::move(Pd2));
        logValue(streamTime[LogStream_LaserCurrent], frameTime, LogStream_LaserCurrent, "A",
                 std::move(A));

        realtimeValue(frameTime, "ZND", std::move(ZND));

        streamAdvance(LogStream_SaturatorTemperature, frameTime);
        streamAdvance(LogStream_CondenserTemperature, frameTime);
        streamAdvance(LogStream_OpticsTemperature, frameTime);
        streamAdvance(LogStream_CabinetTemperature, frameTime);
        streamAdvance(LogStream_AmbientPressure, frameTime);
        streamAdvance(LogStream_NozzlePressure, frameTime);
        streamAdvance(LogStream_OrificePressure, frameTime);
        streamAdvance(LogStream_LaserCurrent, frameTime);
        break;
    }

    case COMMAND_SCD: {
        field = Util::ByteView(frame).string_trimmed();
        if (field.empty()) return 17000;
        if (field != "ERROR") {
            auto label = field.toString();
            if (instrumentMeta["CalibrationLabel"].toString() != label) {
                instrumentMeta["CalibrationLabel"].setString(std::move(label));
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }
        }

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_START_READ_CALIBRATION:
            if (controlStream != NULL) {
                controlStream->writeControl("RIE\r");
            }
            responseState = RESP_INTERACTIVE_START_READ_ERRORS;
            commandQueue.append(Command(COMMAND_RIE));
            timeoutAt(frameTime + 2.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadErrors"), frameTime, FP::undefined()));
            }
            break;

        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        default:
            return -1;
        }
        break;
    }

    case COMMAND_OK:
        if (frame != "OK")
            return 3;

        switch (responseState) {
        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            /* Fall through */
        case RESP_INTERACTIVE_RUN_READ_ALL:
        case RESP_INTERACTIVE_RUN_READ_SAMPLEFLOW:
        case RESP_INTERACTIVE_RUN_READ_INLETFLOW:
        case RESP_INTERACTIVE_RUN_READ_COUNTS:
        case RESP_INTERACTIVE_RUN_WAIT:
            if (controlStream != NULL) {
                controlStream->writeControl("RALL\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_ALL;
            commandQueue.append(Command(COMMAND_RALL));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_INTERACTIVE_START_READ_MODELNUMBER:
        case RESP_INTERACTIVE_START_READ_MODELNUMBER_RETRY:
        case RESP_INTERACTIVE_START_READ_SERIALNUMBER:
        case RESP_INTERACTIVE_START_READ_FIRMWAREVERSION:
        case RESP_INTERACTIVE_START_READ_CALIBRATION:
        case RESP_INTERACTIVE_START_READ_ERRORS:
        case RESP_INTERACTIVE_START_READ_ALL:
            return 4;

        case RESP_INTERACTIVE_START_STOPREPORTS:
            /* Handled at discard completion */
            return -1;

        case RESP_INTERACTIVE_START_SET_OPERATINGMODE:
            if (haveCoincidenceCorrection) {
                if (controlStream != NULL) {
                    controlStream->writeControl("SCC,1\r");
                }
                responseState = RESP_INTERACTIVE_START_SET_CORRECTED;
                commandQueue.append(Command(COMMAND_OK));
                timeoutAt(frameTime + 2.0);

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveSetCorrected"), frameTime, FP::undefined()));
                }
                return 0;
            }

            /* Fall through */
        case RESP_INTERACTIVE_START_SET_CORRECTED:
            if (config.first().doTimeSet) {
                instrumentSetTime = Time::toDateTime(frameTime + 0.5);
                if (controlStream != NULL) {
                    controlStream->writeControl("SYEAR," +
                                                        QByteArray::number(
                                                                instrumentSetTime.date().year() %
                                                                        100) +
                                                        "\r");
                }
                responseState = RESP_INTERACTIVE_START_SET_YEAR;
                commandQueue.append(Command(COMMAND_OK));
                timeoutAt(frameTime + 5.0);

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveSetTime"), frameTime, FP::undefined()));
                }
            } else {
                if (controlStream != NULL) {
                    controlStream->writeControl("RALL\r");
                }
                responseState = RESP_INTERACTIVE_START_READ_ALL;
                commandQueue.append(Command(COMMAND_RALL));
                timeoutAt(frameTime + 2.0);

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveReadAll"), frameTime, FP::undefined()));
                }
            }
            return 0;

        case RESP_INTERACTIVE_START_SET_YEAR:
        case RESP_INTERACTIVE_START_SET_MONTH:
        case RESP_INTERACTIVE_START_SET_DAY:
        case RESP_INTERACTIVE_START_SET_HOUR:
        case RESP_INTERACTIVE_START_SET_MINUTE:
        case RESP_INTERACTIVE_START_SET_SECOND:
            discardData(frameTime + 2.0);
            timeoutAt(frameTime + 5.0);
            return 0;

        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -1;

        default:
            break;
        }

        break;
    }

    if (!externalCommandQueue.empty()) {
        switch (responseState) {
        case RESP_INTERACTIVE_RUN_READ_ALL:
        case RESP_INTERACTIVE_RUN_READ_SAMPLEFLOW:
        case RESP_INTERACTIVE_RUN_READ_INLETFLOW:
        case RESP_INTERACTIVE_RUN_READ_COUNTS:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND: {
            Util::ByteArray issue(std::move(externalCommandQueue.front()));
            externalCommandQueue.pop_front();

            responseState = RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND;
            incomingControlFrame(issue, frameTime);

            if (controlStream) {
                issue += "\r";
                controlStream->writeControl(std::move(issue));
            }

            timeoutAt(frameTime + 2.0);
            break;
        }

        default:
            break;
        }
    }

    if (forceRealtimeStateEmit && realtimeEgress != NULL) {
        forceRealtimeStateEmit = false;
        if (errorFlags.size() == 1) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(*errorFlags.constBegin()),
                                  frameTime, FP::undefined()));
        } else if (!errorFlags.isEmpty()) {
            qint64 bits = 0;
            for (int i = 0;
                    i <
                            (int) (sizeof(instrumentFlagTranslation) /
                                    sizeof(instrumentFlagTranslation[0]));
                    i++) {
                if (instrumentFlagTranslation[i] == NULL)
                    continue;
                if (errorFlags.contains(instrumentFlagTranslation[i]))
                    bits |= (Q_INT64_C(1) << i);
            }
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    QObject::tr("Instrument errors: %1").arg(QString::number(bits, 16).toUpper())),
                                                       frameTime, FP::undefined()));
        } else {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                  FP::undefined()));
        }
    }

    return 0;
}

void AcquireTSICPC377X::invalidateLogValues(double frameTime)
{
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;

        if (loggingEgress != NULL)
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    loggingMux.clear();
    loggingLost(frameTime);
}

void AcquireTSICPC377X::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configurationAdvance(frameTime);

    Command command;
    if (!commandQueue.isEmpty())
        command = commandQueue.first();

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        int code = processResponse(frame, frameTime);
        if (code == 0) {
            if (++autoprobePassiveValidRecords > 10) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_PASSIVE_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                timeoutAt(frameTime + 2.0);

                forceRealtimeStateEmit = true;

                Variant::Write info = Variant::Write::empty();
                describeState(info, command);
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(
                            SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime, FP::undefined()));
                }
            }
        } else if (code > 0) {
            qCDebug(log) << "Passive autoprobe failed at" << Logging::time(frameTime) << ":"
                        << frame << "rejected with code" << code;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            responseState = RESP_PASSIVE_WAIT;
            autoprobePassiveValidRecords = 0;

            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_PASSIVE_WAIT:
        if (processResponse(frame, frameTime) == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);
            responseState = RESP_PASSIVE_RUN;
            generalStatusUpdated();

            timeoutAt(frameTime + 2.0);

            Variant::Write info = Variant::Write::empty();
            describeState(info, command);
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);

            forceRealtimeStateEmit = true;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                      FP::undefined()));
            }
        } else {
            invalidateLogValues(frameTime);
        }
        break;

    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN_READ_ALL:
    case RESP_INTERACTIVE_RUN_READ_SAMPLEFLOW:
    case RESP_INTERACTIVE_RUN_READ_INLETFLOW:
    case RESP_INTERACTIVE_RUN_READ_COUNTS:
    case RESP_INTERACTIVE_RUN_WAIT:
    case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND: {
        int code = processResponse(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + 2.0);
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();
            describeState(info, command);
            if (responseState == RESP_PASSIVE_RUN) {
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
            } else {
                if (controlStream != NULL) {
                    controlStream->writeControl("\r\r\r\r\r\rSSTART,0\r");
                }
                responseState = RESP_INTERACTIVE_START_STOPREPORTS;
                timeoutAt(frameTime + 10.0);
                discardData(frameTime + 1.0);
            }
            generalStatusUpdated();

            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
        } /* Ignored line */
        break;
    }

    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_READ_MODELNUMBER_RETRY:
    case RESP_INTERACTIVE_START_READ_SERIALNUMBER:
    case RESP_INTERACTIVE_START_READ_FIRMWAREVERSION:
    case RESP_INTERACTIVE_START_READ_CALIBRATION:
    case RESP_INTERACTIVE_START_READ_ERRORS:
    case RESP_INTERACTIVE_START_SET_OPERATINGMODE:
    case RESP_INTERACTIVE_START_SET_CORRECTED:
    case RESP_INTERACTIVE_START_SET_YEAR:
    case RESP_INTERACTIVE_START_SET_MONTH:
    case RESP_INTERACTIVE_START_SET_DAY:
    case RESP_INTERACTIVE_START_SET_HOUR:
    case RESP_INTERACTIVE_START_SET_MINUTE:
    case RESP_INTERACTIVE_START_SET_SECOND:
    case RESP_INTERACTIVE_START_READ_ALL: {
        int code = processResponse(frame, frameTime);
        if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected during start communications state" << responseState
                         << "with code" << code;

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            discardData(frameTime + 10.0);
            timeoutAt(frameTime + 30.0);
            generalStatusUpdated();

            invalidateLogValues(frameTime);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
        } /* Ignored line */
        break;
    }

    case RESP_INTERACTIVE_START_READ_MODELNUMBER: {
        int code = processResponse(frame, frameTime);
        if (code > 0) {
            responseState = RESP_INTERACTIVE_START_READ_MODELNUMBER_RETRY;
            discardData(frameTime + 1.0);
            timeoutAt(frameTime + 10.0);
        }
        /* Ignored line or success, meaning no further action needed */
        break;
    }

    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        break;
    }
}

void AcquireTSICPC377X::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        /* Fall through */
    case RESP_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
        commandQueue.clear();
        invalidateLogValues(frameTime);
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            event(frameTime, QObject::tr("Timeout waiting for response.  Communications Dropped."),
                  true, info);
        }

        commandQueue.clear();
        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_RUN_READ_ALL:
    case RESP_INTERACTIVE_RUN_READ_SAMPLEFLOW:
    case RESP_INTERACTIVE_RUN_READ_INLETFLOW:
    case RESP_INTERACTIVE_RUN_READ_COUNTS:
    case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
        qCDebug(log) << "Timeout in interactive state" << responseState << "at"
                     << Logging::time(frameTime);

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            event(frameTime, QObject::tr("Timeout waiting for response.  Communications Dropped."),
                  true, info);
        }

        commandQueue.clear();
        if (controlStream != NULL) {
            controlStream->writeControl("\r\r\r\r\r\rSSTART,0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        commandQueue.append(Command(COMMAND_OK));

        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_RUN_WAIT:
        if (controlStream != NULL) {
            controlStream->writeControl("RALL\r");
        }
        responseState = RESP_INTERACTIVE_RUN_READ_ALL;
        commandQueue.append(Command(COMMAND_RALL));
        timeoutAt(frameTime + 2.0);
        break;

    case RESP_INTERACTIVE_START_READ_MODELNUMBER:
        responseState = RESP_INTERACTIVE_START_READ_MODELNUMBER_RETRY;
        discardData(frameTime + 1.0);
        timeoutAt(frameTime + 10.0);
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_READ_SERIALNUMBER:
    case RESP_INTERACTIVE_START_READ_MODELNUMBER_RETRY:
    case RESP_INTERACTIVE_START_READ_FIRMWAREVERSION:
    case RESP_INTERACTIVE_START_READ_CALIBRATION:
    case RESP_INTERACTIVE_START_READ_ERRORS:
    case RESP_INTERACTIVE_START_SET_OPERATINGMODE:
    case RESP_INTERACTIVE_START_SET_CORRECTED:
    case RESP_INTERACTIVE_START_SET_YEAR:
    case RESP_INTERACTIVE_START_SET_MONTH:
    case RESP_INTERACTIVE_START_SET_DAY:
    case RESP_INTERACTIVE_START_SET_HOUR:
    case RESP_INTERACTIVE_START_SET_MINUTE:
    case RESP_INTERACTIVE_START_SET_SECOND:
    case RESP_INTERACTIVE_START_READ_ALL:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        commandQueue.clear();
        if (controlStream != NULL)
            controlStream->resetControl();
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);
        invalidateLogValues(frameTime);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        commandQueue.clear();
        if (controlStream != NULL) {
            controlStream->writeControl("\r\r\r\r\r\rSSTART,0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        commandQueue.append(Command(COMMAND_OK));
        invalidateLogValues(frameTime);
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;
    }
}

void AcquireTSICPC377X::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_INTERACTIVE_RESTART_WAIT:
        commandQueue.clear();
        if (controlStream != NULL) {
            controlStream->writeControl("\r\r\r\r\r\rSSTART,0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        commandQueue.append(Command(COMMAND_OK));
        invalidateLogValues(frameTime);

        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveStopReports"),
                                                       frameTime, FP::undefined()));
        }
        break;
    case RESP_INTERACTIVE_START_STOPREPORTS:
        commandQueue.clear();
        if (controlStream != NULL) {
            controlStream->writeControl("RMN\r");
        }
        responseState = RESP_INTERACTIVE_START_READ_MODELNUMBER;
        commandQueue.append(Command(COMMAND_RMN));
        invalidateLogValues(frameTime);

        timeoutAt(frameTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadModelNumber"), frameTime, FP::undefined()));
        }
        break;
    case RESP_INTERACTIVE_START_READ_MODELNUMBER:
        commandQueue.clear();
        if (controlStream != NULL) {
            controlStream->writeControl("RMN\r");
        }
        responseState = RESP_INTERACTIVE_START_READ_MODELNUMBER_RETRY;
        commandQueue.append(Command(COMMAND_RMN));
        invalidateLogValues(frameTime);

        timeoutAt(frameTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadModelNumber"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SET_YEAR:
        if (controlStream != NULL) {
            controlStream->writeControl(
                    "SMONTH," + QByteArray::number(instrumentSetTime.date().month()) + "\r");
        }
        responseState = RESP_INTERACTIVE_START_SET_MONTH;
        commandQueue.append(Command(COMMAND_OK));
        timeoutAt(frameTime + 5.0);
        break;
    case RESP_INTERACTIVE_START_SET_MONTH:
        if (controlStream != NULL) {
            controlStream->writeControl(
                    "SDAY," + QByteArray::number(instrumentSetTime.date().day()) + "\r");
        }
        responseState = RESP_INTERACTIVE_START_SET_DAY;
        commandQueue.append(Command(COMMAND_OK));
        timeoutAt(frameTime + 5.0);
        break;
    case RESP_INTERACTIVE_START_SET_DAY:
        if (controlStream != NULL) {
            controlStream->writeControl(
                    "SHOUR," + QByteArray::number(instrumentSetTime.time().hour()) + "\r");
        }
        responseState = RESP_INTERACTIVE_START_SET_HOUR;
        commandQueue.append(Command(COMMAND_OK));
        timeoutAt(frameTime + 5.0);
        break;
    case RESP_INTERACTIVE_START_SET_HOUR:
        if (controlStream != NULL) {
            controlStream->writeControl(
                    "SMINUTE," + QByteArray::number(instrumentSetTime.time().minute()) + "\r");
        }
        responseState = RESP_INTERACTIVE_START_SET_MINUTE;
        commandQueue.append(Command(COMMAND_OK));
        timeoutAt(frameTime + 5.0);
        break;
    case RESP_INTERACTIVE_START_SET_MINUTE:
        if (controlStream != NULL) {
            controlStream->writeControl(
                    "SSECOND," + QByteArray::number(instrumentSetTime.time().second()) + "\r");
        }
        responseState = RESP_INTERACTIVE_START_SET_SECOND;
        commandQueue.append(Command(COMMAND_OK));
        timeoutAt(frameTime + 5.0);
        break;
    case RESP_INTERACTIVE_START_SET_SECOND:
        if (controlStream != NULL) {
            controlStream->writeControl("RALL\r");
        }
        responseState = RESP_INTERACTIVE_START_READ_ALL;
        commandQueue.append(Command(COMMAND_RALL));
        timeoutAt(frameTime + 5.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveReadAll"),
                                  frameTime, FP::undefined()));
        }
        break;

    default:
        break;
    }
}


void AcquireTSICPC377X::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    if (frame == "RFV") {
        commandQueue.append(Command(COMMAND_RFV));
    } else if (frame == "RSF") {
        commandQueue.append(Command(COMMAND_RSF));
    } else if (frame == "RIF") {
        commandQueue.append(Command(COMMAND_RIF));
    } else if (frame == "RTS") {
        commandQueue.append(Command(COMMAND_RTS));
    } else if (frame == "RTC") {
        commandQueue.append(Command(COMMAND_RTC));
    } else if (frame == "RTO") {
        commandQueue.append(Command(COMMAND_RTO));
    } else if (frame == "RTA") {
        commandQueue.append(Command(COMMAND_RTA));
    } else if (frame == "RCT") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame == "RIE") {
        commandQueue.append(Command(COMMAND_RIE));
    } else if (frame == "RPA") {
        commandQueue.append(Command(COMMAND_RPA));
    } else if (frame == "RPO") {
        commandQueue.append(Command(COMMAND_RPO));
    } else if (frame == "RPN") {
        commandQueue.append(Command(COMMAND_RPN));
    } else if (frame == "RAI") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame == "RALL") {
        commandQueue.append(Command(COMMAND_RALL));
    } else if (frame == "RLP") {
        commandQueue.append(Command(COMMAND_RLP));
    } else if (frame == "RLL") {
        commandQueue.append(Command(COMMAND_RLL));
    } else if (frame == "RMN") {
        commandQueue.append(Command(COMMAND_RMN));
    } else if (frame == "R0") {
        commandQueue.append(Command(COMMAND_RLL));
    } else if (frame == "R1") {
        commandQueue.append(Command(COMMAND_RTC));
    } else if (frame == "R2") {
        commandQueue.append(Command(COMMAND_RTS));
    } else if (frame == "R3") {
        commandQueue.append(Command(COMMAND_RTO));
    } else if (frame == "RD") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame == "RV") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame == "RCOUNT1") {
        commandQueue.append(Command(COMMAND_RCOUNT1));
    } else if (frame == "RCOUNT2") {
        commandQueue.append(Command(COMMAND_RCOUNT2));
    } else if (frame.string_start("SAV")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("SSTART")) {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SCM")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("STS")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("STC")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("STO")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("SAWR")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("SVO")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("SAO")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("SCOM")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("SHOUR")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("SMINUTE")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("SSECOND")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("SYEAR")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("SDAY")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("SMONTH")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("SFILL")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("SDRAIN")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("SCC")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("SCCM")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SCD") {
        commandQueue.append(Command(COMMAND_SCD));
    } else if (frame.string_start("SCD")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("SCCR")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "ZB") {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "ZE") {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("ZT")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "ZU") {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("SV")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("COM2")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "D") {
        commandQueue.append(Command(COMMAND_D));
    } else if (frame == "DEL") {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "FORMAT") {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "DIR") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("CD")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("Help")) {
        commandQueue.append(Command(COMMAND_IGNORED));
    }
}

Variant::Root AcquireTSICPC377X::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireTSICPC377X::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN_READ_ALL:
    case RESP_INTERACTIVE_RUN_READ_SAMPLEFLOW:
    case RESP_INTERACTIVE_RUN_READ_INLETFLOW:
    case RESP_INTERACTIVE_RUN_READ_COUNTS:
    case RESP_INTERACTIVE_RUN_WAIT:
    case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

AcquisitionInterface::AutoprobeStatus AcquireTSICPC377X::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireTSICPC377X::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_READ_ALL:
    case RESP_INTERACTIVE_RUN_READ_SAMPLEFLOW:
    case RESP_INTERACTIVE_RUN_READ_INLETFLOW:
    case RESP_INTERACTIVE_RUN_READ_COUNTS:
    case RESP_INTERACTIVE_RUN_WAIT:
    case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_READ_MODELNUMBER:
    case RESP_INTERACTIVE_START_READ_MODELNUMBER_RETRY:
    case RESP_INTERACTIVE_START_READ_SERIALNUMBER:
    case RESP_INTERACTIVE_START_READ_FIRMWAREVERSION:
    case RESP_INTERACTIVE_START_READ_CALIBRATION:
    case RESP_INTERACTIVE_START_READ_ERRORS:
    case RESP_INTERACTIVE_START_SET_OPERATINGMODE:
    case RESP_INTERACTIVE_START_SET_CORRECTED:
    case RESP_INTERACTIVE_START_SET_YEAR:
    case RESP_INTERACTIVE_START_SET_MONTH:
    case RESP_INTERACTIVE_START_SET_DAY:
    case RESP_INTERACTIVE_START_SET_HOUR:
    case RESP_INTERACTIVE_START_SET_MINUTE:
    case RESP_INTERACTIVE_START_SET_SECOND:
    case RESP_INTERACTIVE_START_READ_ALL:
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from passive state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        discardData(time + 0.5);
        timeoutAt(time + 30.0);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireTSICPC377X::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobePassiveValidRecords = 0;
    commandQueue.clear();
    discardData(time + 0.5, 1);
    timeoutAt(time + 15.0);
    generalStatusUpdated();
}

void AcquireTSICPC377X::autoprobePromote(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_READ_ALL:
    case RESP_INTERACTIVE_RUN_READ_SAMPLEFLOW:
    case RESP_INTERACTIVE_RUN_READ_INLETFLOW:
    case RESP_INTERACTIVE_RUN_READ_COUNTS:
    case RESP_INTERACTIVE_RUN_WAIT:
    case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_READ_MODELNUMBER:
    case RESP_INTERACTIVE_START_READ_MODELNUMBER_RETRY:
    case RESP_INTERACTIVE_START_READ_SERIALNUMBER:
    case RESP_INTERACTIVE_START_READ_FIRMWAREVERSION:
    case RESP_INTERACTIVE_START_READ_CALIBRATION:
    case RESP_INTERACTIVE_START_READ_ERRORS:
    case RESP_INTERACTIVE_START_SET_OPERATINGMODE:
    case RESP_INTERACTIVE_START_SET_CORRECTED:
    case RESP_INTERACTIVE_START_SET_YEAR:
    case RESP_INTERACTIVE_START_SET_MONTH:
    case RESP_INTERACTIVE_START_SET_DAY:
    case RESP_INTERACTIVE_START_SET_HOUR:
    case RESP_INTERACTIVE_START_SET_MINUTE:
    case RESP_INTERACTIVE_START_SET_SECOND:
    case RESP_INTERACTIVE_START_READ_ALL:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 2.0);
        break;

    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        discardData(time + 0.5);
        timeoutAt(time + 30.0);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireTSICPC377X::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 2.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_RUN_READ_ALL:
    case RESP_INTERACTIVE_RUN_READ_SAMPLEFLOW:
    case RESP_INTERACTIVE_RUN_READ_INLETFLOW:
    case RESP_INTERACTIVE_RUN_READ_COUNTS:
    case RESP_INTERACTIVE_RUN_WAIT:
    case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_WAIT;
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireTSICPC377X::getDefaults()
{
    AutomaticDefaults result;
    result.name = "N$1$2";
    result.setSerialN81(115200);
    return result;
}


ComponentOptions AcquireTSICPC377XComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options);
    return options;
}

ComponentOptions AcquireTSICPC377XComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireTSICPC377XComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireTSICPC377XComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireTSICPC377XComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options);
}

QList<ComponentExample> AcquireTSICPC377XComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data with the default flow rate.")));

    options = getBaseOptions();
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("q")))->set(0.3);
    examples.append(ComponentExample(options, tr("Explicitly set flow rate.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireTSICPC377XComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireTSICPC377X(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireTSICPC377XComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireTSICPC377X(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireTSICPC377XComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{
    std::unique_ptr<AcquireTSICPC377X> i(new AcquireTSICPC377X(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireTSICPC377XComponent::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{
    std::unique_ptr<AcquireTSICPC377X> i(new AcquireTSICPC377X(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
