/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/variant/root.hxx"

bool operator==(const CPD3::Data::Variant::Root &a, const CPD3::Data::Variant::Root &b)
{
    return a.read() == b.read();
}

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/wavelength.hxx"
#include "core/environment.hxx"

#include "calc_intensives.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Editing;

void CalcIntensives::handleOptions(const ComponentOptions &options)
{
    restrictedInputs = false;

    if (options.isSet("wavelengths")) {
        defaultWavelengths =
                qobject_cast<ComponentOptionDoubleSet *>(options.get("wavelengths"))->get();
    } else {
        defaultWavelengths |= 450.0;
        defaultWavelengths |= 550.0;
        defaultWavelengths |= 700.0;
    }

    if (options.isSet("bfr")) {
        defaultBackScatterFraction =
                qobject_cast<DynamicInputOption *>(options.get("bfr"))->getInput();
    } else {
        defaultBackScatterFraction = NULL;
    }

    if (options.isSet("ssa")) {
        defaultSingleScatteringAlbedo =
                qobject_cast<DynamicInputOption *>(options.get("ssa"))->getInput();
    } else {
        defaultSingleScatteringAlbedo = NULL;
    }

    if (options.isSet("mac")) {
        defaultMassAbsorptionCalibration =
                qobject_cast<DynamicInputOption *>(options.get("mac"))->getInput();
    } else {
        defaultMassAbsorptionCalibration = new DynamicInput::Constant(14625.0);
    }

    if (options.isSet("angstrom")) {
        defaultFallbackAngstrom =
                qobject_cast<DynamicInputOption *>(options.get("angstrom"))->getInput();
        if (options.isSet("angstrom-wavelength")) {
            defaultFallbackAngstromWavelength = qobject_cast<DynamicDoubleOption *>(
                    options.get("angstrom-wavelength"))->getInput();
            if (options.isSet("angstrom-distance")) {
                defaultFallbackAngstromDistance = qobject_cast<DynamicDoubleOption *>(
                        options.get("angstrom-distance"))->getInput();
            } else {
                defaultFallbackAngstromDistance = NULL;
            }
        } else {
            defaultFallbackAngstromWavelength = NULL;
            defaultFallbackAngstromDistance = NULL;
        }
    } else {
        defaultFallbackAngstrom = NULL;
        defaultFallbackAngstromWavelength = NULL;
        defaultFallbackAngstromDistance = NULL;
    }


    if (options.isSet("scattering")) {
        defaultScattering = WavelengthAdjust::fromInput(
                qobject_cast<DynamicSequenceSelectionOption *>(
                        options.get("scattering"))->getOperator());
        configureAdjuster(defaultScattering);
    } else {
        defaultScattering = NULL;
    }
    if (options.isSet("backscattering")) {
        defaultBackScattering = WavelengthAdjust::fromInput(
                qobject_cast<DynamicSequenceSelectionOption *>(
                        options.get("backscattering"))->getOperator());
        configureAdjuster(defaultBackScattering);
    } else {
        defaultBackScattering = NULL;
    }
    if (options.isSet("absorption")) {
        defaultAbsorption = WavelengthAdjust::fromInput(
                qobject_cast<DynamicSequenceSelectionOption *>(
                        options.get("absorption"))->getOperator());
        configureAdjuster(defaultAbsorption);
    } else {
        defaultAbsorption = NULL;
    }
    if (options.isSet("extinction")) {
        defaultExtinction = WavelengthAdjust::fromInput(
                qobject_cast<DynamicSequenceSelectionOption *>(
                        options.get("extinction"))->getOperator());
        configureAdjuster(defaultExtinction);
    } else {
        defaultExtinction = NULL;
    }
    if (options.isSet("counts")) {
        defaultCounts = qobject_cast<DynamicInputOption *>(options.get("counts"))->getInput();
    } else {
        defaultCounts = NULL;
    }

    if (options.isSet("scattering-suffix")) {
        scatteringSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("scattering-suffix"))
                        ->get());
        if (scatteringSuffixes.empty() && defaultScattering == NULL) {
            defaultScattering = WavelengthAdjust::fromInput(new DynamicSequenceSelection::None);
        }
        if (scatteringSuffixes.empty() && defaultBackScattering == NULL) {
            defaultBackScattering = WavelengthAdjust::fromInput(new DynamicSequenceSelection::None);
        }
    }
    if (options.isSet("absorption-suffix")) {
        absorptionSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("absorption-suffix"))
                        ->get());
        if (absorptionSuffixes.empty() && defaultAbsorption == NULL) {
            defaultAbsorption = WavelengthAdjust::fromInput(new DynamicSequenceSelection::None);
        }
    }
    if (options.isSet("extinction-suffix")) {
        extinctionSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("extinction-suffix"))
                        ->get());
        if (extinctionSuffixes.empty() && defaultExtinction == NULL) {
            defaultExtinction = WavelengthAdjust::fromInput(new DynamicSequenceSelection::None);
        }
    }
    if (options.isSet("counts-suffix")) {
        countsSuffixes = Util::set_from_qstring(qobject_cast<ComponentOptionInstrumentSuffixSet *>(
                options.get("counts-suffix"))->get());
        if (countsSuffixes.empty() && defaultCounts == NULL) {
            defaultCounts = new DynamicInput::Constant;
        }
    }

    if (options.isSet("output-suffix")) {
        outputSuffixes.clear();
        for (const auto &add : qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("output-suffix"))
                ->get()) {
            outputSuffixes.emplace_back(add.toStdString());
        }
        std::sort(outputSuffixes.begin(), outputSuffixes.end());
    }
}


CalcIntensives::CalcIntensives()
{ Q_ASSERT(false); }

CalcIntensives::CalcIntensives(const ComponentOptions &options)
{
    handleOptions(options);
}

CalcIntensives::CalcIntensives(const ComponentOptions &options,
                               double start,
                               double end,
                               const QList<SequenceName> &inputs)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    handleOptions(options);

    for (QList<SequenceName>::const_iterator unit = inputs.constBegin(), endU = inputs.constEnd();
            unit != endU;
            ++unit) {
        unhandled(*unit, NULL);
    }
}

static Variant::Root convertMetadata(const Variant::Read &v)
{
    return Variant::Root(v);
}

CalcIntensives::CalcIntensives(double start,
                               double end,
                               const SequenceName::Component &station,
                               const SequenceName::Component &archive,
                               const ValueSegment::Transfer &config)
{
    restrictedInputs = true;
    defaultBackScatterFraction = NULL;
    defaultSingleScatteringAlbedo = NULL;
    defaultMassAbsorptionCalibration = NULL;
    defaultFallbackAngstrom = NULL;
    defaultFallbackAngstromWavelength = NULL;
    defaultFallbackAngstromDistance = NULL;
    defaultScattering = NULL;
    defaultBackScattering = NULL;
    defaultAbsorption = NULL;
    defaultExtinction = NULL;
    defaultCounts = NULL;

    struct ChildData {
        std::unordered_set<Variant::PathElement::HashIndex> optical;
        std::unordered_set<Variant::PathElement::HashIndex> direct;
        std::unordered_set<Variant::PathElement::HashIndex> paired;
    };
    std::unordered_map<Variant::PathElement::HashIndex, ChildData> children;
    for (const auto &add : config) {
        auto h = add.read().toHash();
        for (auto child : h) {
            auto &target = children[child.first];
            Util::merge(child.second["Optical"].toHash().keys(), target.optical);
            Util::merge(child.second["Direct"].toHash().keys(), target.direct);
            Util::merge(child.second["Paired"].toHash().keys(), target.paired);
        }
    }
    children.erase(Variant::PathElement::HashIndex());

    for (auto &child : children) {
        child.second.optical.erase(Variant::PathElement::HashIndex());
        child.second.direct.erase(Variant::PathElement::HashIndex());
        child.second.paired.erase(Variant::PathElement::HashIndex());

        Processing p;

        p.inputScattering = WavelengthAdjust::fromConfiguration(config,
                                                                QString("%1/Scattering").arg(
                                                                        QString::fromStdString(
                                                                                child.first)),
                                                                start, end);
        p.inputBackScattering = WavelengthAdjust::fromConfiguration(config,
                                                                    QString("%1/BackScattering").arg(
                                                                            QString::fromStdString(
                                                                                    child.first)),
                                                                    start, end);
        p.inputAbsorption = WavelengthAdjust::fromConfiguration(config,
                                                                QString("%1/Absorption").arg(
                                                                        QString::fromStdString(
                                                                                child.first)),
                                                                start, end);
        p.inputExtinction = WavelengthAdjust::fromConfiguration(config,
                                                                QString("%1/Extinction").arg(
                                                                        QString::fromStdString(
                                                                                child.first)),
                                                                start, end);
        p.inputScattering->registerExpected(station, archive);
        p.inputBackScattering->registerExpected(station, archive);
        p.inputAbsorption->registerExpected(station, archive);
        p.inputExtinction->registerExpected(station, archive);

        p.inputMassAbsorptionCalibration = DynamicInput::fromConfiguration(config,
                                                                           QString("%1/MassAbsorptionCalibration")
                                                                                   .arg(QString::fromStdString(
                                                                                           child.first)),
                                                                           start, end);
        p.inputMassAbsorptionCalibration->registerExpected(station, archive);

        for (const auto &wl : child.second.optical) {
            Processing::OpticalWavelength o;

            o.wavelength = DynamicDoubleOption::fromConfiguration(config,
                                                                  QString("%1/Optical/%2/Wavelength")
                                                                          .arg(QString::fromStdString(
                                                                                  child.first),
                                                                               QString::fromStdString(
                                                                                       wl)), start,
                                                                  end);

            o.inputBackscatterFraction = DynamicInput::fromConfiguration(config,
                                                                         QString("%1/Optical/%2/Override/BackScatterFraction")
                                                                                 .arg(QString::fromStdString(
                                                                                         child.first),
                                                                                      QString::fromStdString(
                                                                                              wl)),
                                                                         start, end);
            o.inputBackscatterFraction->registerExpected(station, archive);

            o.inputSingleScatteringAlbedo = DynamicInput::fromConfiguration(config,
                                                                            QString("%1/Optical/%2/Override/SingleScatteringAlbedo")
                                                                                    .arg(QString::fromStdString(
                                                                                            child.first),
                                                                                         QString::fromStdString(
                                                                                                 wl)),
                                                                            start, end);
            o.inputSingleScatteringAlbedo->registerExpected(station, archive);

            o.inputMassAbsorptionEfficiency = DynamicInput::fromConfiguration(config,
                                                                              QString("%1/Optical/%2/MassAbsorptionEfficiency")
                                                                                      .arg(QString::fromStdString(
                                                                                              child.first),
                                                                                           QString::fromStdString(
                                                                                                   wl)),
                                                                              start, end);
            o.inputMassAbsorptionEfficiency->registerExpected(station, archive);

            o.outputScattering = DynamicSequenceSelection::fromConfiguration(config,
                                                                             QString("%1/Optical/%2/Scattering")
                                                                                     .arg(QString::fromStdString(
                                                                                             child.first),
                                                                                          QString::fromStdString(
                                                                                                  wl)),
                                                                             start, end);
            o.outputAbsorption = DynamicSequenceSelection::fromConfiguration(config,
                                                                             QString("%1/Optical/%2/Absorption")
                                                                                     .arg(QString::fromStdString(
                                                                                             child.first),
                                                                                          QString::fromStdString(
                                                                                                  wl)),
                                                                             start, end);
            o.outputExtinction = DynamicSequenceSelection::fromConfiguration(config,
                                                                             QString("%1/Optical/%2/Extinction")
                                                                                     .arg(QString::fromStdString(
                                                                                             child.first),
                                                                                          QString::fromStdString(
                                                                                                  wl)),
                                                                             start, end);
            o.outputSSA = DynamicSequenceSelection::fromConfiguration(config,
                                                                      QString("%1/Optical/%2/SingleScatteringAlbedo")
                                                                              .arg(QString::fromStdString(
                                                                                      child.first),
                                                                                   QString::fromStdString(
                                                                                           wl)),
                                                                      start, end);
            o.outputAngstromScattering = DynamicSequenceSelection::fromConfiguration(config,
                                                                                     QString("%1/Optical/%2/ScatteringAngstrom")
                                                                                             .arg(QString::fromStdString(
                                                                                                     child.first),
                                                                                                  QString::fromStdString(
                                                                                                          wl)),
                                                                                     start, end);
            o.outputAngstromAbsorption = DynamicSequenceSelection::fromConfiguration(config,
                                                                                     QString("%1/Optical/%2/AbsorptionAngstrom")
                                                                                             .arg(QString::fromStdString(
                                                                                                     child.first),
                                                                                                  QString::fromStdString(
                                                                                                          wl)),
                                                                                     start, end);
            o.outputAngstromExtinction = DynamicSequenceSelection::fromConfiguration(config,
                                                                                     QString("%1/Optical/%2/ExtinctionAngstrom")
                                                                                             .arg(QString::fromStdString(
                                                                                                     child.first),
                                                                                                  QString::fromStdString(
                                                                                                          wl)),
                                                                                     start, end);
            o.outputBackscatterFraction = DynamicSequenceSelection::fromConfiguration(config,
                                                                                      QString("%1/Optical/%2/BackScatterFraction")
                                                                                              .arg(QString::fromStdString(
                                                                                                      child.first),
                                                                                                   QString::fromStdString(
                                                                                                           wl)),
                                                                                      start, end);
            o.outputAsymmetry = DynamicSequenceSelection::fromConfiguration(config,
                                                                            QString("%1/Optical/%2/AsymmetryParameter")
                                                                                    .arg(QString::fromStdString(
                                                                                            child.first),
                                                                                         QString::fromStdString(
                                                                                                 wl)),
                                                                            start, end);
            o.outputRFE = DynamicSequenceSelection::fromConfiguration(config,
                                                                      QString("%1/Optical/%2/RadiativeForcingEfficiency")
                                                                              .arg(QString::fromStdString(
                                                                                      child.first),
                                                                                   QString::fromStdString(
                                                                                           wl)),
                                                                      start, end);
            o.outputEBC = DynamicSequenceSelection::fromConfiguration(config,
                                                                      QString("%1/Optical/%2/EquivalentBlackCarbon")
                                                                              .arg(QString::fromStdString(
                                                                                      child.first),
                                                                                   QString::fromStdString(
                                                                                           wl)),
                                                                      start, end);
            o.outputScattering->registerExpected(station, archive);
            o.outputAbsorption->registerExpected(station, archive);
            o.outputExtinction->registerExpected(station, archive);
            o.outputSSA->registerExpected(station, archive);
            o.outputAngstromScattering->registerExpected(station, archive);
            o.outputAngstromAbsorption->registerExpected(station, archive);
            o.outputAngstromExtinction->registerExpected(station, archive);
            o.outputBackscatterFraction->registerExpected(station, archive);
            o.outputAsymmetry->registerExpected(station, archive);
            o.outputRFE->registerExpected(station, archive);
            o.outputEBC->registerExpected(station, archive);

            p.optical.push_back(o);
        }

        for (const auto &cp : child.second.direct) {
            Processing::CopyVariable c;

            c.input = DynamicInput::fromConfiguration(config, QString("%1/Direct/%2/Input").arg(
                    QString::fromStdString(child.first), QString::fromStdString(cp)), start, end);
            c.output = DynamicSequenceSelection::fromConfiguration(config,
                                                                   QString("%1/Direct/%2/Output").arg(
                                                                           QString::fromStdString(
                                                                                   child.first),
                                                                           QString::fromStdString(
                                                                                   cp)), start,
                                                                   end);
            c.smoothing = DynamicMetadata::fromConfiguration(convertMetadata, config,
                                                             QString("%1/Direct/%2/Smoothing").arg(
                                                                     QString::fromStdString(
                                                                             child.first),
                                                                     QString::fromStdString(cp)),
                                                             start, end);

            c.input->registerExpected(station, archive);
            c.output->registerExpected(station, archive);

            p.copy.push_back(c);
        }

        for (const auto &wl : child.second.paired) {
            Processing::PairedWavelengths w;

            w.input = DynamicSequenceSelection::fromConfiguration(config,
                                                                  QString("%1/Paired/%2/Input").arg(
                                                                          QString::fromStdString(
                                                                                  child.first),
                                                                          QString::fromStdString(
                                                                                  wl)), start, end);
            w.outputAngstrom = DynamicSequenceSelection::fromConfiguration(config,
                                                                           QString("%1/Paired/%2/Angstrom")
                                                                                   .arg(QString::fromStdString(
                                                                                           child.first),
                                                                                        QString::fromStdString(
                                                                                                wl)),
                                                                           start, end);
            w.input->registerExpected(station, archive);
            w.outputAngstrom->registerExpected(station, archive);

            p.paired.push_back(w);
        }

        processing.push_back(p);
    }
}


CalcIntensives::~CalcIntensives()
{
    if (defaultBackScatterFraction != NULL)
        delete defaultBackScatterFraction;
    if (defaultSingleScatteringAlbedo != NULL)
        delete defaultSingleScatteringAlbedo;
    if (defaultMassAbsorptionCalibration != NULL)
        delete defaultMassAbsorptionCalibration;
    if (defaultFallbackAngstrom != NULL)
        delete defaultFallbackAngstrom;
    if (defaultFallbackAngstromWavelength != NULL)
        delete defaultFallbackAngstromWavelength;
    if (defaultFallbackAngstromDistance != NULL)
        delete defaultFallbackAngstromDistance;
    if (defaultScattering != NULL)
        delete defaultScattering;
    if (defaultBackScattering != NULL)
        delete defaultBackScattering;
    if (defaultAbsorption != NULL)
        delete defaultAbsorption;
    if (defaultExtinction != NULL)
        delete defaultExtinction;
    if (defaultCounts != NULL)
        delete defaultCounts;
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        if (p->inputScattering != NULL)
            delete p->inputScattering;
        if (p->inputBackScattering != NULL)
            delete p->inputBackScattering;
        if (p->inputAbsorption != NULL)
            delete p->inputAbsorption;
        if (p->inputExtinction != NULL)
            delete p->inputExtinction;

        delete p->inputMassAbsorptionCalibration;
        for (std::vector<Processing::OpticalWavelength>::const_iterator o = p->optical.begin(),
                endO = p->optical.end(); o != endO; ++o) {
            delete o->wavelength;
            delete o->inputBackscatterFraction;
            delete o->inputSingleScatteringAlbedo;
            delete o->inputMassAbsorptionEfficiency;
            delete o->outputScattering;
            delete o->outputAbsorption;
            delete o->outputExtinction;
            delete o->outputSSA;
            delete o->outputAngstromScattering;
            delete o->outputAngstromAbsorption;
            delete o->outputAngstromExtinction;
            delete o->outputBackscatterFraction;
            delete o->outputAsymmetry;
            delete o->outputRFE;
            delete o->outputEBC;
        }
        for (std::vector<Processing::CopyVariable>::const_iterator c = p->copy.begin(),
                endC = p->copy.end(); c != endC; ++c) {
            delete c->input;
            delete c->output;
            delete c->smoothing;
        }
        for (std::vector<Processing::PairedWavelengths>::const_iterator w = p->paired.begin(),
                endW = p->paired.end(); w != endW; ++w) {
            delete w->input;
            delete w->outputAngstrom;
        }
    }
}

void CalcIntensives::handleNewProcessing(const SequenceName &unit,
                                         int id,
                                         SegmentProcessingStage::SequenceHandlerControl *control)
{
    Processing *p = &processing[id];

    SequenceName::Set reg;

    {
        QRegExp reCheck("(Bs|Ba|Be)([A-Z0-9]*)_(.+)");
        if (reCheck.exactMatch(unit.getVariableQString())) {
            createAllPaired(unit, id, control, reCheck.cap(3).toStdString(),
                            reCheck.cap(1).toStdString(), reCheck.cap(2).toStdString());
        }
    }

    for (std::vector<Processing::OpticalWavelength>::iterator it = p->optical.begin(),
            end = p->optical.end(); it != end; ++it) {
        it->outputScattering
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->outputScattering->getAllUnits(), reg);

        it->outputAbsorption
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->outputAbsorption->getAllUnits(), reg);

        it->outputExtinction
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->outputExtinction->getAllUnits(), reg);

        it->outputSSA
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->outputSSA->getAllUnits(), reg);

        it->outputAngstromScattering
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->outputAngstromScattering->getAllUnits(), reg);

        it->outputAngstromAbsorption
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->outputAngstromAbsorption->getAllUnits(), reg);

        it->outputAngstromExtinction
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->outputAngstromExtinction->getAllUnits(), reg);

        it->outputBackscatterFraction
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->outputBackscatterFraction->getAllUnits(), reg);

        it->outputAsymmetry
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->outputAsymmetry->getAllUnits(), reg);

        it->outputRFE
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->outputRFE->getAllUnits(), reg);

        it->outputEBC
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->outputEBC->getAllUnits(), reg);
    }

    for (std::vector<Processing::CopyVariable>::iterator it = p->copy.begin(), end = p->copy.end();
            it != end;
            ++it) {
        it->output->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->output->getAllUnits(), reg);
    }

    for (std::vector<Processing::PairedWavelengths>::iterator it = p->paired.begin(),
            end = p->paired.end(); it != end; ++it) {
        it->outputAngstrom
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->outputAngstrom->getAllUnits(), reg);
    }

    if (!control)
        return;

    for (const auto &n : reg) {
        bool isNew = !control->isHandled(n);
        control->filterUnit(n, id);
        if (isNew)
            registerPossibleInput(n, control, id);
    }
}

void CalcIntensives::configureAdjuster(WavelengthAdjust *adj)
{
    if (defaultFallbackAngstrom != NULL) {
        adj->addFallbackAngstrom(defaultFallbackAngstrom->clone(),
                                 defaultFallbackAngstromWavelength != NULL
                                 ? defaultFallbackAngstromWavelength->clone() : NULL,
                                 defaultFallbackAngstromDistance != NULL
                                 ? defaultFallbackAngstromDistance->clone() : NULL);
    }
}

WavelengthAdjust *CalcIntensives::createAdjuster(const SequenceName::Component &prefix,
                                                 const SequenceName::Component &station,
                                                 const SequenceName::Component &archive,
                                                 const SequenceName::Component &suffix,
                                                 const SequenceName::Flavors &flavors)
{
    WavelengthAdjust *adj = WavelengthAdjust::fromInput(
            new DynamicSequenceSelection::Match(QString::fromStdString(station),
                                                QString::fromStdString(archive),
                                                QString::fromStdString(prefix + "_" + suffix),
                                                flavors));
    configureAdjuster(adj);
    return adj;
}

static DynamicSequenceSelection *createOutput(const SequenceName::Component &prefix,
                                              const SequenceName &suffix)
{
    return new DynamicSequenceSelection::Single(
            SequenceName(suffix.getStation(), suffix.getArchive(),
                         prefix + "_" + suffix.getVariable(), suffix.getFlavors()));
}

bool CalcIntensives::createOpticalIfNeeded(const SequenceName &unit,
                                           int id,
                                           SegmentProcessingStage::SequenceHandlerControl *control)
{
    Q_UNUSED(control);

    Processing *p = &processing[id];
    bool usedAsInput = false;

    if (!p->optical.empty() || defaultWavelengths.isEmpty())
        return usedAsInput;

    bool canUseColorCodes = true;
    {
        std::unordered_set<std::string> usedCodes;
        for (auto wl : defaultWavelengths) {
            const auto &check = Wavelength::code(wl);
            if (check.empty()) {
                canUseColorCodes = false;
                break;
            }
            if (usedCodes.count(check)) {
                canUseColorCodes = false;
                break;
            }
            usedCodes.insert(check);
        }
    }

    QList<double> sortedWavelengths(defaultWavelengths.values());
    std::sort(sortedWavelengths.begin(), sortedWavelengths.end());
    for (int i = 0; i < sortedWavelengths.size(); i++) {
        Processing::OpticalWavelength o;

        SequenceName::Component color;
        if (canUseColorCodes) {
            color = Wavelength::code(sortedWavelengths.at(i));
            Q_ASSERT(!color.empty());
        } else if (sortedWavelengths.size() > 1) {
            color = std::to_string(i + 1);
        }

        o.wavelength = new DynamicPrimitive<double>::Constant(sortedWavelengths.at(i));

        if (defaultBackScatterFraction == NULL) {
            o.inputBackscatterFraction = new DynamicInput::Constant(FP::undefined());
        } else {
            o.inputBackscatterFraction = defaultBackScatterFraction->clone();
        }
        if (o.inputBackscatterFraction->registerInput(unit))
            usedAsInput = true;

        if (defaultSingleScatteringAlbedo == NULL) {
            o.inputSingleScatteringAlbedo = new DynamicInput::Constant(FP::undefined());
        } else {
            o.inputSingleScatteringAlbedo = defaultSingleScatteringAlbedo->clone();
        }
        if (o.inputBackscatterFraction->registerInput(unit))
            usedAsInput = true;

        o.inputMassAbsorptionEfficiency = new DynamicInput::Constant(FP::undefined());

        o.outputScattering = createOutput("Bs" + color, p->suffixUnit);
        o.outputAbsorption = createOutput("Ba" + color, p->suffixUnit);
        o.outputExtinction = createOutput("Be" + color, p->suffixUnit);
        o.outputSSA = createOutput("ZSSA" + color, p->suffixUnit);
        o.outputAngstromScattering = createOutput("ZAngBs" + color, p->suffixUnit);
        o.outputAngstromAbsorption = createOutput("ZAngBa" + color, p->suffixUnit);
        o.outputAngstromExtinction = createOutput("ZAngBe" + color, p->suffixUnit);
        o.outputBackscatterFraction = createOutput("ZBfr" + color, p->suffixUnit);
        o.outputAsymmetry = createOutput("ZG" + color, p->suffixUnit);
        o.outputRFE = createOutput("ZRFE" + color, p->suffixUnit);
        o.outputEBC = createOutput("X" + color, p->suffixUnit);

        p->optical.push_back(o);
    }

    return usedAsInput;
}

void CalcIntensives::createScattering(const SequenceName &unit,
                                      int id,
                                      SegmentProcessingStage::SequenceHandlerControl *control,
                                      const SequenceName::Component &suffix)
{
    bool usedAsInput = createOpticalIfNeeded(unit, id, control);

    if (processing[id].inputScattering != NULL)
        delete processing[id].inputScattering;
    processing[id].inputScattering =
            createAdjuster("Bs[A-Z0-9]*", unit.getStation(), unit.getArchive(), suffix,
                           unit.getFlavors());
    if (processing[id].inputScattering->registerInput(unit))
        usedAsInput = true;

    if (usedAsInput) {
        if (control != NULL)
            control->inputUnit(unit, id);
    }

    processing[id].scatteringSuffix = suffix;
}

void CalcIntensives::createBackScattering(const SequenceName &unit,
                                          int id,
                                          SegmentProcessingStage::SequenceHandlerControl *control,
                                          const SequenceName::Component &suffix)
{
    bool usedAsInput = createOpticalIfNeeded(unit, id, control);

    if (processing[id].inputBackScattering != NULL)
        delete processing[id].inputBackScattering;
    processing[id].inputBackScattering =
            createAdjuster("Bbs[A-Z0-9]*", unit.getStation(), unit.getArchive(), suffix,
                           unit.getFlavors());
    if (processing[id].inputBackScattering->registerInput(unit))
        usedAsInput = true;

    if (usedAsInput) {
        if (control != NULL)
            control->inputUnit(unit, id);
    }

    processing[id].backScatteringSuffix = suffix;
}

void CalcIntensives::createAbsorption(const SequenceName &unit,
                                      int id,
                                      SegmentProcessingStage::SequenceHandlerControl *control,
                                      const SequenceName::Component &suffix)
{
    bool usedAsInput = createOpticalIfNeeded(unit, id, control);

    if (processing[id].inputAbsorption != NULL)
        delete processing[id].inputAbsorption;
    processing[id].inputAbsorption =
            createAdjuster("Ba[A-Z0-9]*", unit.getStation(), unit.getArchive(), suffix,
                           unit.getFlavors());
    if (processing[id].inputAbsorption->registerInput(unit))
        usedAsInput = true;

    if (usedAsInput) {
        if (control != NULL)
            control->inputUnit(unit, id);
    }

    processing[id].absorptionSuffix = suffix;
}

void CalcIntensives::createExtinction(const SequenceName &unit,
                                      int id,
                                      SegmentProcessingStage::SequenceHandlerControl *control,
                                      const SequenceName::Component &suffix)
{
    bool usedAsInput = createOpticalIfNeeded(unit, id, control);

    if (processing[id].inputExtinction != NULL)
        delete processing[id].inputExtinction;
    processing[id].inputExtinction =
            createAdjuster("Be[A-Z0-9]*", unit.getStation(), unit.getArchive(), suffix,
                           unit.getFlavors());
    if (processing[id].inputExtinction->registerInput(unit))
        usedAsInput = true;

    if (usedAsInput) {
        if (control != NULL)
            control->inputUnit(unit, id);
    }

    processing[id].extinctionSuffix = suffix;
}

void CalcIntensives::createCounts(const SequenceName &unit,
                                  int id,
                                  SegmentProcessingStage::SequenceHandlerControl *control,
                                  const SequenceName::Component &suffix)
{
    SequenceName countsUnit
            (processing[id].suffixUnit.getStation(), processing[id].suffixUnit.getArchive(),
             "N_" + processing[id].suffixUnit.getVariable(),
             processing[id].suffixUnit.getFlavors());

    Processing::CopyVariable c;
    if (defaultCounts != NULL) {
        if (!defaultCounts->registerInput(unit))
            return;
        c.input = defaultCounts->clone();
    } else {
        c.input = new DynamicInput::Basic(unit);
    }
    c.output = new DynamicSequenceSelection::Single(countsUnit);
    c.smoothing = new DynamicPrimitive<Variant::Root>::Constant();

    if (c.input->registerInput(unit)) {
        if (control != NULL)
            control->inputUnit(unit, id);
    }
    if (c.input->registerInput(countsUnit)) {
        if (control != NULL)
            control->inputUnit(countsUnit, id);
    }

    if (c.output->registerInput(unit)) {
        if (control != NULL)
            control->inputUnit(unit, id);
    }

    c.output->registerInput(countsUnit);
    if (control != NULL) {
        control->filterUnit(countsUnit, id);
    }

    processing[id].copy.push_back(c);
    if (defaultCounts != NULL) {
        processing[id].countsSuffix =
                defaultCounts->describe(FP::undefined(), FP::undefined()).toStdString();
    }
    if (processing[id].countsSuffix.empty())
        processing[id].countsSuffix = suffix;
}

bool CalcIntensives::shouldCreatePaired(double lower, double upper) const
{
    for (QSet<double>::const_iterator check = defaultWavelengths.constBegin(),
            endCheck = defaultWavelengths.constEnd(); check != endCheck; ++check) {
        if (!FP::defined(*check))
            continue;
        if (FP::defined(lower) && *check < lower)
            continue;
        if (FP::defined(upper) && *check >= upper)
            continue;
        return true;
    }
    return false;
}

void CalcIntensives::createAllPaired(const SequenceName &unit,
                                     int id,
                                     SegmentProcessingStage::SequenceHandlerControl *control,
                                     const SequenceName::Component &suffix,
                                     const SequenceName::Component &type,
                                     const SequenceName::Component &code)
{
    if (code == "B") {
        if (shouldCreatePaired(400, 500) && shouldCreatePaired(500, 600))
            createPaired(unit, id, control, suffix, type, "B", "G");
        if (shouldCreatePaired(400, 500) && shouldCreatePaired(600, 750))
            createPaired(unit, id, control, suffix, type, "B", "R");
    } else if (code == "G") {
        if (shouldCreatePaired(400, 500) && shouldCreatePaired(500, 600))
            createPaired(unit, id, control, suffix, type, "B", "G");
        if (shouldCreatePaired(500, 600) && shouldCreatePaired(600, 750))
            createPaired(unit, id, control, suffix, type, "G", "R");
    } else if (code == "R") {
        if (shouldCreatePaired(400, 500) && shouldCreatePaired(600, 750))
            createPaired(unit, id, control, suffix, type, "B", "R");
        if (shouldCreatePaired(500, 600) && shouldCreatePaired(600, 750))
            createPaired(unit, id, control, suffix, type, "G", "R");
    }
}

void CalcIntensives::createPaired(const SequenceName &unit,
                                  int id,
                                  SegmentProcessingStage::SequenceHandlerControl *control,
                                  const SequenceName::Component &suffix,
                                  const SequenceName::Component &type,
                                  const SequenceName::Component &first,
                                  const SequenceName::Component &second)
{
    auto checkCode = type + first + second;
    for (const auto &check : processing[id].paired) {
        if (check.pairCode == checkCode)
            return;
    }


    SequenceName angstromUnit
            (processing[id].suffixUnit.getStation(), processing[id].suffixUnit.getArchive(),
             "ZAng" + type + first + second + "_" + processing[id].suffixUnit.getVariable(),
             processing[id].suffixUnit.getFlavors());

    Processing::PairedWavelengths w;
    w.pairCode = checkCode;

    w.input =
            new DynamicSequenceSelection::Match(unit.getStationQString(), unit.getArchiveQString(),
                                                QString("%1(%2|%3)_%4").arg(
                                                        QString::fromStdString(type),
                                                        QString::fromStdString(first),
                                                        QString::fromStdString(second),
                                                        QString::fromStdString(suffix)),
                                                unit.getFlavors());

    w.outputAngstrom = new DynamicSequenceSelection::Single(angstromUnit);

    if (w.input->registerInput(unit)) {
        if (control != NULL)
            control->inputUnit(unit, id);
    }

    if (w.outputAngstrom->registerInput(unit)) {
        if (control != NULL)
            control->inputUnit(unit, id);
    }

    processing[id].paired.push_back(w);
}

void CalcIntensives::registerPossibleInput(const SequenceName &unit,
                                           SegmentProcessingStage::SequenceHandlerControl *control,
                                           int filterID)
{
    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        bool usedAsInput = false;

        if (processing[id].inputScattering != NULL &&
                processing[id].inputScattering->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }

        if (processing[id].inputBackScattering != NULL &&
                processing[id].inputBackScattering->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }

        if (processing[id].inputAbsorption != NULL &&
                processing[id].inputAbsorption->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }

        if (processing[id].inputExtinction != NULL &&
                processing[id].inputExtinction->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }

        if (processing[id].inputMassAbsorptionCalibration->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }

        for (std::vector<Processing::OpticalWavelength>::iterator
                it = processing[id].optical.begin(), end = processing[id].optical.end();
                it != end;
                ++it) {
            if (it->inputBackscatterFraction->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
                usedAsInput = true;
            }
            if (it->inputSingleScatteringAlbedo->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
                usedAsInput = true;
            }
            if (it->inputMassAbsorptionEfficiency->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
                usedAsInput = true;
            }
        }

        for (std::vector<Processing::CopyVariable>::iterator it = processing[id].copy.begin(),
                end = processing[id].copy.end(); it != end; ++it) {
            if (it->input->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
                usedAsInput = true;
            }
        }

        for (std::vector<Processing::PairedWavelengths>::iterator
                it = processing[id].paired.begin(), end = processing[id].paired.end();
                it != end;
                ++it) {
            if (it->input->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
                usedAsInput = true;
            }
        }

        if (usedAsInput && id != filterID)
            handleNewProcessing(unit, id, control);
    }

    if (defaultBackScatterFraction != NULL &&
            defaultBackScatterFraction->registerInput(unit) &&
            control != NULL)
        control->deferHandling(unit);
    if (defaultSingleScatteringAlbedo != NULL &&
            defaultSingleScatteringAlbedo->registerInput(unit) &&
            control != NULL)
        control->deferHandling(unit);
    if (defaultFallbackAngstrom != NULL &&
            defaultFallbackAngstrom->registerInput(unit) &&
            control != NULL)
        control->deferHandling(unit);
    if (defaultScattering != NULL && defaultScattering->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultBackScattering != NULL &&
            defaultBackScattering->registerInput(unit) &&
            control != NULL)
        control->deferHandling(unit);
    if (defaultAbsorption != NULL && defaultAbsorption->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultExtinction != NULL && defaultExtinction->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultCounts != NULL && defaultCounts->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultMassAbsorptionCalibration != NULL &&
            defaultMassAbsorptionCalibration->registerInput(unit))
        control->deferHandling(unit);
}

static SequenceName::Component indexToSuffix(size_t index)
{
    //static const char digits[] = "IJKLMNOPQRSTUVWXYZABCDEFGH";
    static const char digits[] = "IJKLNOPQRSTUVWYZABCDEFG";
    static const size_t radix = sizeof(digits);
    SequenceName::Component result;
    if (index == 0) {
        result += digits[0];
    } else {
        while (index != 0) {
            result.insert(0, 1, digits[index % radix]);
            index /= radix;
        }
    }
    result.insert(0, 1, 'X');
    return result;
}

void CalcIntensives::unhandled(const SequenceName &unit,
                               SegmentProcessingStage::SequenceHandlerControl *control)
{
    registerPossibleInput(unit, control);

    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        for (std::vector<Processing::OpticalWavelength>::iterator
                it = processing[id].optical.begin(), end = processing[id].optical.end();
                it != end;
                ++it) {
            if (it->outputScattering->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return;
            }
            if (it->outputAbsorption->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return;
            }
            if (it->outputExtinction->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return;
            }
            if (it->outputSSA->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return;
            }
            if (it->outputAngstromScattering->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return;
            }
            if (it->outputAngstromAbsorption->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return;
            }
            if (it->outputAngstromExtinction->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return;
            }
            if (it->outputBackscatterFraction->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return;
            }
            if (it->outputAsymmetry->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return;
            }
            if (it->outputRFE->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return;
            }
            if (it->outputEBC->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return;
            }
        }
        for (std::vector<Processing::CopyVariable>::iterator it = processing[id].copy.begin(),
                end = processing[id].copy.end(); it != end; ++it) {
            if (it->output->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return;
            }
        }
        for (std::vector<Processing::PairedWavelengths>::iterator
                it = processing[id].paired.begin(), end = processing[id].paired.end();
                it != end;
                ++it) {
            if (it->outputAngstrom->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return;
            }
        }
    }

    if (restrictedInputs)
        return;

    if (unit.hasFlavor(SequenceName::flavor_cover) || unit.hasFlavor(SequenceName::flavor_stats))
        return;

    enum {
        Scattering, BackScattering, Absorption, Extinction, Counts,
    } type;
    SequenceName::Component suffix;
    if (Util::starts_with(unit.getVariable(), "N_")) {
        suffix = Util::suffix(unit.getVariable(), '_');
        type = Counts;
    } else {
        QRegExp reCheck("((?:Bb?s)|(?:Ba)|(?:Be))[A-Z0-9]*_(.+)");
        if (!reCheck.exactMatch(unit.getVariableQString()))
            return;
        if (reCheck.cap(1) == "Bs")
            type = Scattering;
        else if (reCheck.cap(1) == "Bbs")
            type = BackScattering;
        else if (reCheck.cap(1) == "Ba")
            type = Absorption;
        else if (reCheck.cap(1) == "Be")
            type = Extinction;
        else
            return;
        suffix = reCheck.cap(2).toStdString();
    }

    switch (type) {
    case Scattering:
    case BackScattering:
        if (!scatteringSuffixes.empty() && !scatteringSuffixes.count(suffix))
            return;
        break;
    case Absorption:
        if (!absorptionSuffixes.empty() && !absorptionSuffixes.count(suffix))
            return;
        break;
    case Extinction:
        if (!extinctionSuffixes.empty() && !extinctionSuffixes.count(suffix))
            return;
        break;
    case Counts:
        if (!countsSuffixes.empty() && !countsSuffixes.count(suffix))
            return;
        break;
    }

    size_t matchedSuffixes = 0;
    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].suffixUnit.getStation() != unit.getStation())
            continue;
        if (processing[id].suffixUnit.getArchive() != unit.getArchive())
            continue;
        if (processing[id].suffixUnit.getFlavors() != unit.getFlavors())
            continue;
        matchedSuffixes++;

        switch (type) {
        case Scattering:
            if (processing[id].inputScattering != NULL) {
                if (processing[id].inputScattering->registerInput(unit))
                    break;
                continue;
            }
            createScattering(unit, id, control, suffix);
            break;
        case BackScattering:
            if (processing[id].inputBackScattering != NULL) {
                if (processing[id].inputBackScattering->registerInput(unit))
                    break;
                continue;
            }
            createBackScattering(unit, id, control, suffix);
            break;
        case Absorption:
            if (processing[id].inputAbsorption != NULL) {
                if (processing[id].inputAbsorption->registerInput(unit))
                    break;
                continue;
            }
            createAbsorption(unit, id, control, suffix);
            break;
        case Extinction:
            if (processing[id].inputExtinction != NULL) {
                if (processing[id].inputExtinction->registerInput(unit))
                    break;
                continue;
            }
            createExtinction(unit, id, control, suffix);
            break;
        case Counts:
            if (!processing[id].countsSuffix.empty())
                continue;
            createCounts(unit, id, control, suffix);
            break;
        }

        handleNewProcessing(unit, id, control);
        return;
    }

    /* If we have defaults set, the creating type must match them or we
     * abort */
    if (defaultScattering != NULL && type == Scattering) {
        if (!defaultScattering->registerInput(unit))
            return;
    }
    if (defaultBackScattering != NULL && type == BackScattering) {
        if (!defaultBackScattering->registerInput(unit))
            return;
    }
    if (defaultAbsorption != NULL && type == Absorption) {
        if (!defaultAbsorption->registerInput(unit))
            return;
    }
    if (defaultExtinction != NULL && type == Extinction) {
        if (!defaultExtinction->registerInput(unit))
            return;
    }
    if (defaultCounts != NULL && type == Counts) {
        if (!defaultCounts->registerInput(unit))
            return;
    }

    SequenceName::Component outputSuffix;
    if (matchedSuffixes < outputSuffixes.size()) {
        outputSuffix = outputSuffixes[matchedSuffixes];
    } else {
        matchedSuffixes -= outputSuffixes.size();
    }
    if (outputSuffix.empty()) {
        for (int i = 0; i < 100; i++, matchedSuffixes++) {
            outputSuffix = indexToSuffix(matchedSuffixes);
            if (std::find(outputSuffixes.begin(), outputSuffixes.end(), outputSuffix) ==
                    outputSuffixes.end())
                break;
        }
    }

    Processing p;
    p.suffixUnit =
            SequenceName(unit.getStation(), unit.getArchive(), outputSuffix, unit.getFlavors());

    if (defaultScattering != NULL)
        p.inputScattering = new WavelengthAdjust(*defaultScattering);
    if (defaultBackScattering != NULL)
        p.inputBackScattering = new WavelengthAdjust(*defaultBackScattering);
    if (defaultAbsorption != NULL)
        p.inputAbsorption = new WavelengthAdjust(*defaultAbsorption);
    if (defaultExtinction != NULL)
        p.inputExtinction = new WavelengthAdjust(*defaultExtinction);

    if (defaultMassAbsorptionCalibration != NULL) {
        p.inputMassAbsorptionCalibration = defaultMassAbsorptionCalibration->clone();
    } else {
        p.inputMassAbsorptionCalibration = new DynamicInput::Constant(FP::undefined());
    }

    int id = processing.size();
    processing.push_back(p);

    switch (type) {
    case Scattering:
        if (processing[id].inputScattering == NULL) {
            createScattering(unit, id, control, suffix);
        } else {
            if (processing[id].inputScattering->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            createOpticalIfNeeded(unit, id, control);
        }
        break;
    case BackScattering:
        if (processing[id].inputBackScattering == NULL) {
            createBackScattering(unit, id, control, suffix);
        } else {
            if (processing[id].inputBackScattering->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            createOpticalIfNeeded(unit, id, control);
        }
        break;
    case Absorption:
        if (processing[id].inputAbsorption == NULL) {
            createAbsorption(unit, id, control, suffix);
        } else {
            if (processing[id].inputAbsorption->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            createOpticalIfNeeded(unit, id, control);
        }
        break;
    case Extinction:
        if (processing[id].inputExtinction == NULL) {
            createExtinction(unit, id, control, suffix);
        } else {
            if (processing[id].inputExtinction->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            createOpticalIfNeeded(unit, id, control);
        }
        break;
    case Counts:
        if (processing[id].countsSuffix.empty()) {
            createCounts(unit, id, control, suffix);
        }
        break;
    }

    handleNewProcessing(unit, id, control);
}


static void setAll(SequenceSegment &data, const SequenceName::Set &s, double value)
{
    for (const auto &i : s) {
        data[i].setDouble(value);
    }
}

static double calculateAsymmetry(double Bfr)
{
    static const Calibration g(QVector<double>() << 0.9893 << -3.9636 << 7.4644 << -7.1439);
    return g.apply(Bfr);
}

static const Calibration rfeBeta(QVector<double>() << 0.0817 << 1.8495 << -2.9682);

static double calculateRFE(double Bfr,
                           double SSA,
                           const Calibration &betaCoefficients = rfeBeta,
                           double daylightFraction = 0.5,
                           double solorConstant = 1370.0,
                           double atmosphericTransmission = 0.76,
                           double cloudFraction = 0.6,
                           double surfaceAlbedo = 0.15)
{
    if (!FP::defined(Bfr) || !FP::defined(SSA))
        return FP::undefined();

    double beta = betaCoefficients.apply(Bfr);
    if (!FP::defined(beta) || fabs(beta) < 1E-10)
        return FP::undefined();

    double sas = 1.0 - surfaceAlbedo;
    if (fabs(sas) < 1E-10)
        return FP::undefined();
    sas = sas * sas;

    if (fabs(SSA) < 1E-10)
        return FP::undefined();

    return -1.0 *
            daylightFraction *
            solorConstant *
            atmosphericTransmission *
            atmosphericTransmission *
            (1.0 - cloudFraction) *
            SSA *
            beta *
            (sas - (2.0 * surfaceAlbedo / beta * ((1.0 / SSA) - 1)));
}

static double calculateEBC(double Ba, double sigma)
{
    if (!FP::defined(Ba) || !FP::defined(sigma))
        return FP::undefined();
    if (sigma <= 0.0)
        return FP::undefined();
    return Ba / sigma;
}

static double calculateAngstrom(double wl, double wu, double vl, double vu)
{
    if (!FP::defined(wl) || wl <= 0.0)
        return FP::undefined();
    if (!FP::defined(wu) || wu <= 0.0)
        return FP::undefined();
    if (!FP::defined(vl) || vl <= 0.0)
        return FP::undefined();
    if (!FP::defined(vu) || vu <= 0.0)
        return FP::undefined();
    return log(vl / vu) / log(wu / wl);
}


void CalcIntensives::process(int id, SequenceSegment &data)
{
    Processing *p = &processing[id];

    if (p->inputScattering != NULL)
        p->inputScattering->incoming(data);
    if (p->inputBackScattering != NULL)
        p->inputBackScattering->incoming(data);
    if (p->inputAbsorption != NULL)
        p->inputAbsorption->incoming(data);
    if (p->inputExtinction != NULL)
        p->inputExtinction->incoming(data);

    for (std::vector<Processing::OpticalWavelength>::iterator it = p->optical.begin(),
            end = p->optical.end(); it != end; ++it) {
        double wavelength = it->wavelength->get(data);

        double Bs = FP::undefined();
        double AngBs = FP::undefined();
        if (p->inputScattering) {
            Bs = p->inputScattering->getAdjusted(wavelength).toReal();
            AngBs = p->inputScattering
                     ->getAngstrom(wavelength,
                                   CPD3::Data::Wavelength::BracketingExactBehavior::ExcludeExact,
                                   FP::undefined(), true)
                     .toReal();
        }

        double Bbs = FP::undefined();
        if (p->inputBackScattering)
            Bbs = p->inputBackScattering->getAdjusted(wavelength).toReal();
        double Ba = FP::undefined();
        double AngBa = FP::undefined();
        if (p->inputAbsorption) {
            Ba = p->inputAbsorption->getAdjusted(wavelength).toReal();
            AngBa = p->inputAbsorption
                     ->getAngstrom(wavelength,
                                   CPD3::Data::Wavelength::BracketingExactBehavior::ExcludeExact,
                                   FP::undefined(), true)
                     .toReal();
        }
        double Be = FP::undefined();
        double AngBe = FP::undefined();
        if (p->inputExtinction) {
            Be = p->inputExtinction->getAdjusted(wavelength).toReal();
            AngBe = p->inputExtinction
                     ->getAngstrom(wavelength,
                                   CPD3::Data::Wavelength::BracketingExactBehavior::ExcludeExact,
                                   FP::undefined(), true)
                     .toReal();
        }
        double Bfr = it->inputBackscatterFraction->get(data);
        double SSA = it->inputSingleScatteringAlbedo->get(data);

        if (!FP::defined(Be) && FP::defined(Bs) && FP::defined(Ba))
            Be = Bs + Ba;
        else if (!FP::defined(Bs) && FP::defined(Be) && FP::defined(Ba))
            Bs = Be - Ba;
        else if (!FP::defined(Ba) && FP::defined(Be) && FP::defined(Bs))
            Ba = Be - Bs;

        if (!FP::defined(Bfr) && FP::defined(Bs) && FP::defined(Bbs) && fabs(Bbs) > 1E-10) {
            Bfr = Bbs / Bs;
        } else if (!FP::defined(Bbs) && FP::defined(Bs) && FP::defined(Bfr)) {
            Bbs = Bs * Bfr;
        } else if (!FP::defined(Bs) && FP::defined(Bbs) && FP::defined(Bfr) && fabs(Bfr) > 1E-10) {
            Bs = Bbs / Bfr;
            if (!FP::defined(Be) && FP::defined(Ba))
                Be = Bs + Ba;
            else if (!FP::defined(Ba) && FP::defined(Be))
                Ba = Be - Bs;
        }

        if (!FP::defined(SSA) && FP::defined(Bs) && FP::defined(Be) && fabs(Be) > 1E-10) {
            SSA = Bs / Be;
        } else if (FP::defined(SSA) && !FP::defined(Bs) && FP::defined(Be)) {
            Bs = SSA * Be;
            if (!FP::defined(Ba))
                Ba = Be - Bs;
        } else if (FP::defined(SSA) && !FP::defined(Be) && FP::defined(Bs) && fabs(SSA) > 1E-10) {
            Be = Bs / SSA;
            if (!FP::defined(Ba))
                Ba = Be - Bs;
        }

        double sigmaEBC = it->inputMassAbsorptionEfficiency->get(data);
        if (!FP::defined(sigmaEBC) && FP::defined(wavelength) && wavelength != 0.0) {
            sigmaEBC = p->inputMassAbsorptionCalibration->get(data);
            if (FP::defined(sigmaEBC))
                sigmaEBC /= wavelength;
        }

        setAll(data, it->outputScattering->get(data), Bs);
        setAll(data, it->outputAbsorption->get(data), Ba);
        setAll(data, it->outputExtinction->get(data), Be);
        setAll(data, it->outputSSA->get(data), SSA);
        setAll(data, it->outputAngstromScattering->get(data), AngBs);
        setAll(data, it->outputAngstromAbsorption->get(data), AngBa);
        setAll(data, it->outputAngstromExtinction->get(data), AngBe);
        setAll(data, it->outputBackscatterFraction->get(data), Bfr);
        setAll(data, it->outputAsymmetry->get(data), calculateAsymmetry(Bfr));
        setAll(data, it->outputRFE->get(data), calculateRFE(Bfr, SSA));
        setAll(data, it->outputEBC->get(data), calculateEBC(Ba, sigmaEBC));
    }

    for (std::vector<Processing::PairedWavelengths>::iterator it = p->paired.begin(),
            end = p->paired.end(); it != end; ++it) {
        double v1 = FP::undefined();
        double w1 = FP::undefined();
        double v2 = FP::undefined();
        double w2 = FP::undefined();

        for (const auto &i : it->input->get(data)) {
            double out = data[i].toReal();
            if (!FP::defined(out))
                continue;

            double wl = it->wavelengthTracker.get(i);
            if (!FP::defined(wl))
                continue;

            if (!FP::defined(w1)) {
                w1 = wl;
                v1 = out;
            } else {
                w2 = wl;
                v2 = out;
                break;
            }
        }

        if (!FP::defined(w1) || !FP::defined(w2)) {
            for (const auto &i : it->outputAngstrom->get(data)) {
                data[i].setReal(FP::undefined());
            }
            continue;
        }

        double ang = calculateAngstrom(w1, w2, v1, v2);

        setAll(data, it->outputAngstrom->get(data), ang);
    }

    for (std::vector<Processing::CopyVariable>::iterator it = p->copy.begin(), end = p->copy.end();
            it != end;
            ++it) {
        setAll(data, it->output->get(data), it->input->get(data));
    }
}

void CalcIntensives::processMeta(int id, SequenceSegment &data)
{
    Processing *p = &processing[id];

    if (p->inputScattering != NULL)
        p->inputScattering->incomingMeta(data);
    if (p->inputBackScattering != NULL)
        p->inputBackScattering->incomingMeta(data);
    if (p->inputAbsorption != NULL)
        p->inputAbsorption->incomingMeta(data);
    if (p->inputExtinction != NULL)
        p->inputExtinction->incomingMeta(data);

    Variant::Root meta;

    meta["By"].setString("calc_intensives");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    if (!p->scatteringSuffix.empty())
        meta["Parameters"].hash("Bs").setString(p->scatteringSuffix);
    if (!p->backScatteringSuffix.empty())
        meta["Parameters"].hash("Bbs").setString(p->backScatteringSuffix);
    if (!p->absorptionSuffix.empty())
        meta["Parameters"].hash("Ba").setString(p->absorptionSuffix);
    if (!p->extinctionSuffix.empty())
        meta["Parameters"].hash("Be").setString(p->extinctionSuffix);
    if (!p->countsSuffix.empty())
        meta["Parameters"].hash("N").setString(p->countsSuffix);

    Variant::Read reportT = Variant::Read::empty();
    Variant::Read reportP = Variant::Read::empty();
    if (p->inputScattering) {
        for (auto i : p->inputScattering->getAllUnits()) {
            i.setMeta();
            if (!reportT.exists())
                reportT = data[i].metadata("ReportT");
            if (!reportP.exists())
                reportP = data[i].metadata("ReportP");
        }
    }
    if (p->inputBackScattering) {
        for (auto i : p->inputBackScattering->getAllUnits()) {
            i.setMeta();
            if (!reportT.exists())
                reportT = data[i].metadata("ReportT");
            if (!reportP.exists())
                reportP = data[i].metadata("ReportP");
        }
    }
    if (p->inputAbsorption) {
        for (auto i : p->inputAbsorption->getAllUnits()) {
            i.setMeta();
            if (!reportT.exists())
                reportT = data[i].metadata("ReportT");
            if (!reportP.exists())
                reportP = data[i].metadata("ReportP");
        }
    }
    if (p->inputExtinction) {
        for (auto i : p->inputExtinction->getAllUnits()) {
            i.setMeta();
            if (!reportT.exists())
                reportT = data[i].metadata("ReportT");
            if (!reportP.exists())
                reportP = data[i].metadata("ReportP");
        }
    }

    for (std::vector<Processing::CopyVariable>::iterator c = p->copy.begin(), end = p->copy.end();
            c != end;
            ++c) {
        Variant::Root input;
        for (auto i : c->input->getUsedInputs()) {
            i.setMeta();
            input = Variant::Root(data[i]);
            if (input.read().getType() == Variant::Type::MetadataReal)
                break;
        }
        if (input.read().getType() != Variant::Type::MetadataReal) {
            input = Variant::Root(Variant::Type::MetadataReal);
        } else {
            input.write().metadata("Smoothing").remove();
        }
        {
            auto v = c->smoothing->get(data).read();
            if (v.exists()) {
                input.write().metadata("Smoothing").set(v);
            }
        }

        meta["Parameters"].hash("Input").setString(c->input->describe(data));
        input.write().metadata("Processing").toArray().after_back().set(meta);

        for (auto i : c->output->get(data)) {
            i.setMeta();
            data[i].set(input);
        }
    }

    meta["Parameters"].hash("Input").remove();
    for (std::vector<Processing::PairedWavelengths>::iterator w = p->paired.begin(),
            end = p->paired.end(); w != end; ++w) {
        const auto &s = w->input->get(data);
        w->wavelengthTracker.incomingMeta(data, s);

        Variant::Read localReportT = Variant::Read::empty();
        Variant::Read localReportP = Variant::Read::empty();
        auto mType = Variant::Type::Real;
        QSet<QString> variableNames;
        for (auto i : s) {
            i.setMeta();
            if (!localReportT.exists())
                localReportT = data[i].metadata("ReportT");
            if (!localReportP.exists())
                localReportP = data[i].metadata("ReportP");

            variableNames.insert(i.getVariableQString());

            switch (data[i].getType()) {
            case Variant::Type::Matrix:
                mType = Variant::Type::Matrix;
                break;
            case Variant::Type::Array:
                if (mType == Variant::Type::Matrix)
                    break;
                mType = Variant::Type::Array;
                break;
            default:
                break;
            }
        }

        QStringList sortedVariableNames(variableNames.values());
        std::sort(sortedVariableNames.begin(), sortedVariableNames.end());

        meta["Parameters"].hash("Input").remove();
        for (const auto &var : sortedVariableNames) {
            meta["Parameters"].hash("Input").toArray().after_back().setString(var);
        }

        for (auto i : w->outputAngstrom->get(data)) {
            i.setMeta();
            data[i].setType(mType);
            data[i].metadata("Processing").toArray().after_back().set(meta);
            data[i].metadata("ReportT").set(localReportT);
            data[i].metadata("ReportP").set(localReportP);
            data[i].metadata("GroupUnits").setString("AngstromExponent");
            data[i].metadata("Format").setString("00.000");
            data[i].metadata("Description")
                   .setString(
                           "\xC3\x85ngstr\xC3\xB6m exponent derived from specific wavelength pairs");
        }
    }

    meta["Parameters"].hash("Input").remove();
    for (std::vector<Processing::OpticalWavelength>::iterator o = p->optical.begin(),
            end = p->optical.end(); o != end; ++o) {
        double wavelength = o->wavelength->get(data);

        meta["Parameters"].hash("Wavelegnth").setDouble(o->wavelength->get(data));
        meta["Parameters"].hash("Bfr") = o->inputBackscatterFraction->describe(data);
        meta["Parameters"].hash("SSA") = o->inputSingleScatteringAlbedo->describe(data);

        for (auto i : o->outputScattering->get(data)) {
            i.setMeta();
            data[i].metadataReal("Processing").toArray().after_back().set(meta);
            data[i].metadataReal("ReportT").set(reportT);
            data[i].metadataReal("ReportP").set(reportP);
            data[i].metadataReal("Wavelength").setDouble(wavelength);
            data[i].metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
            data[i].metadataReal("Format").setString("0000.00");
            data[i].metadataReal("Description").setString("Aerosol light scattering coefficient");
        }

        for (auto i : o->outputAbsorption->get(data)) {
            i.setMeta();
            data[i].metadataReal("Processing").toArray().after_back().set(meta);
            data[i].metadataReal("ReportT").set(reportT);
            data[i].metadataReal("ReportP").set(reportP);
            data[i].metadataReal("Wavelength").setDouble(wavelength);
            data[i].metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
            data[i].metadataReal("Format").setString("0000.00");
            data[i].metadataReal("Description").setString("Aerosol light absorption coefficient");
        }

        for (auto i : o->outputExtinction->get(data)) {
            i.setMeta();
            data[i].metadataReal("Processing").toArray().after_back().set(meta);
            data[i].metadataReal("ReportT").set(reportT);
            data[i].metadataReal("ReportP").set(reportP);
            data[i].metadataReal("Wavelength").setDouble(wavelength);
            data[i].metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
            data[i].metadataReal("Format").setString("0000.00");
            data[i].metadataReal("Description").setString("Aerosol light extinction coefficient");
        }

        for (auto i : o->outputSSA->get(data)) {
            i.setMeta();
            data[i].metadataReal("Processing").toArray().after_back().set(meta);
            data[i].metadataReal("ReportT").set(reportT);
            data[i].metadataReal("ReportP").set(reportP);
            data[i].metadataReal("Wavelength").setDouble(wavelength);
            data[i].metadataReal("GroupUnits").setString("Albedo");
            data[i].metadataReal("Format").setString("00.000");
            data[i].metadataReal("Description").setString("Single scattering albedo");
        }

        for (auto i : o->outputAngstromScattering->get(data)) {
            i.setMeta();
            data[i].metadataReal("Processing").toArray().after_back().set(meta);
            data[i].metadataReal("ReportT").set(reportT);
            data[i].metadataReal("ReportP").set(reportP);
            data[i].metadataReal("Wavelength").setDouble(wavelength);
            data[i].metadataReal("GroupUnits").setString("Angstrom");
            data[i].metadataReal("Format").setString("00.000");
            data[i].metadataReal("Description")
                   .setString(
                           "\xC3\x85ngstr\xC3\xB6m exponent derived from scattering coefficients");
        }

        for (auto i : o->outputAngstromAbsorption->get(data)) {
            i.setMeta();
            data[i].metadataReal("Processing").toArray().after_back().set(meta);
            data[i].metadataReal("ReportT").set(reportT);
            data[i].metadataReal("ReportP").set(reportP);
            data[i].metadataReal("Wavelength").setDouble(wavelength);
            data[i].metadataReal("GroupUnits").setString("AngstromExponent");
            data[i].metadataReal("Format").setString("00.000");
            data[i].metadataReal("Description")
                   .setString(
                           "\xC3\x85ngstr\xC3\xB6m exponent derived from absorption coefficients");
        }

        for (auto i : o->outputAngstromExtinction->get(data)) {
            i.setMeta();
            data[i].metadataReal("Processing").toArray().after_back().set(meta);
            data[i].metadataReal("ReportT").set(reportT);
            data[i].metadataReal("ReportP").set(reportP);
            data[i].metadataReal("Wavelength").setDouble(wavelength);
            data[i].metadataReal("GroupUnits").setString("AngstromExponent");
            data[i].metadataReal("Format").setString("00.000");
            data[i].metadataReal("Description")
                   .setString(
                           "\xC3\x85ngstr\xC3\xB6m exponent derived from extinction coefficients");
        }

        for (auto i : o->outputBackscatterFraction->get(data)) {
            i.setMeta();
            data[i].metadataReal("Processing").toArray().after_back().set(meta);
            data[i].metadataReal("ReportT").set(reportT);
            data[i].metadataReal("ReportP").set(reportP);
            data[i].metadataReal("Wavelength").setDouble(wavelength);
            data[i].metadataReal("GroupUnits").setString("BackscatterFraction");
            data[i].metadataReal("Format").setString("00.000");
            data[i].metadataReal("Description").setString("Backscattering fraction");
        }

        for (auto i : o->outputAsymmetry->get(data)) {
            i.setMeta();
            data[i].metadataReal("Processing").toArray().after_back().set(meta);
            data[i].metadataReal("ReportT").set(reportT);
            data[i].metadataReal("ReportP").set(reportP);
            data[i].metadataReal("Wavelength").setDouble(wavelength);
            data[i].metadataReal("GroupUnits").setString("AsymmetryParameter");
            data[i].metadataReal("Format").setString("00.000");
            data[i].metadataReal("Description").setString("Light scattering asymmetry parameter");
        }

        for (auto i : o->outputRFE->get(data)) {
            i.setMeta();
            data[i].metadataReal("Processing").toArray().after_back().set(meta);
            data[i].metadataReal("ReportT").set(reportT);
            data[i].metadataReal("ReportP").set(reportP);
            data[i].metadataReal("Wavelength").setDouble(wavelength);
            data[i].metadataReal("GroupUnits").setString("RadiativeForcingEfficiency");
            data[i].metadataReal("Format").setString("0000.00");
            data[i].metadataReal("Description").setString("Radiative forcing efficiency");
        }

        for (auto i : o->outputEBC->get(data)) {
            i.setMeta();

            double e = o->inputMassAbsorptionEfficiency->constant(data);
            if (!FP::defined(e) && FP::defined(wavelength) && wavelength != 0.0) {
                e = p->inputMassAbsorptionCalibration->constant(data);
                if (FP::defined(e))
                    e /= wavelength;
            }
            {
                auto mp = meta;
                mp["Parameters"].hash("Efficiency").setDouble(e);
                data[i].metadataReal("Processing").toArray().after_back().set(mp);
            }

            data[i].metadataReal("ReportT").set(reportT);
            data[i].metadataReal("ReportP").set(reportP);
            data[i].metadataReal("Wavelength").setDouble(wavelength);
            data[i].metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
            data[i].metadataReal("Format").setString("0000.000");
            data[i].metadataReal("Description").setString("Equivalent black carbon");
        }
    }
}


SequenceName::Set CalcIntensives::requestedInputs()
{
    SequenceName::Set out;
    if (defaultBackScatterFraction != NULL)
        Util::merge(defaultBackScatterFraction->getUsedInputs(), out);
    if (defaultSingleScatteringAlbedo != NULL)
        Util::merge(defaultSingleScatteringAlbedo->getUsedInputs(), out);
    if (defaultScattering != NULL)
        Util::merge(defaultScattering->getAllUnits(), out);
    if (defaultFallbackAngstrom != NULL)
        Util::merge(defaultFallbackAngstrom->getUsedInputs(), out);
    if (defaultBackScattering != NULL)
        Util::merge(defaultBackScattering->getAllUnits(), out);
    if (defaultAbsorption != NULL)
        Util::merge(defaultAbsorption->getAllUnits(), out);
    if (defaultExtinction != NULL)
        Util::merge(defaultExtinction->getAllUnits(), out);
    if (defaultCounts != NULL)
        Util::merge(defaultCounts->getUsedInputs(), out);
    for (std::vector<Processing>::iterator p = processing.begin(); p != processing.end(); ++p) {
        if (p->inputScattering != NULL)
            Util::merge(p->inputScattering->getAllUnits(), out);
        if (p->inputBackScattering != NULL)
            Util::merge(p->inputBackScattering->getAllUnits(), out);
        if (p->inputAbsorption != NULL)
            Util::merge(p->inputAbsorption->getAllUnits(), out);
        if (p->inputExtinction != NULL)
            Util::merge(p->inputExtinction->getAllUnits(), out);

        Util::merge(p->inputMassAbsorptionCalibration->getUsedInputs(), out);

        for (std::vector<Processing::OpticalWavelength>::iterator o = p->optical.begin();
                o != p->optical.end();
                ++o) {
            Util::merge(o->inputBackscatterFraction->getUsedInputs(), out);
            Util::merge(o->inputSingleScatteringAlbedo->getUsedInputs(), out);
            Util::merge(o->inputMassAbsorptionEfficiency->getUsedInputs(), out);
        }
        for (std::vector<Processing::PairedWavelengths>::iterator w = p->paired.begin();
                w != p->paired.end();
                ++w) {
            Util::merge(w->input->getAllUnits(), out);
        }
        for (std::vector<Processing::CopyVariable>::iterator c = p->copy.begin();
                c != p->copy.end();
                ++c) {
            Util::merge(c->input->getUsedInputs(), out);
        }
    }
    return out;
}

SequenceName::Set CalcIntensives::predictedOutputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::iterator p = processing.begin(); p != processing.end(); ++p) {
        for (std::vector<Processing::OpticalWavelength>::iterator o = p->optical.begin();
                o != p->optical.end();
                ++o) {
            Util::merge(o->outputScattering->getAllUnits(), out);
            Util::merge(o->outputAbsorption->getAllUnits(), out);
            Util::merge(o->outputExtinction->getAllUnits(), out);
            Util::merge(o->outputSSA->getAllUnits(), out);
            Util::merge(o->outputAngstromScattering->getAllUnits(), out);
            Util::merge(o->outputAngstromAbsorption->getAllUnits(), out);
            Util::merge(o->outputAngstromExtinction->getAllUnits(), out);
            Util::merge(o->outputBackscatterFraction->getAllUnits(), out);
            Util::merge(o->outputAsymmetry->getAllUnits(), out);
            Util::merge(o->outputRFE->getAllUnits(), out);
            Util::merge(o->outputEBC->getAllUnits(), out);
        }
        for (std::vector<Processing::CopyVariable>::iterator c = p->copy.begin();
                c != p->copy.end();
                ++c) {
            Util::merge(c->output->getAllUnits(), out);
        }
        for (std::vector<Processing::PairedWavelengths>::iterator w = p->paired.begin();
                w != p->paired.end();
                ++w) {
            Util::merge(w->outputAngstrom->getAllUnits(), out);
        }
    }
    return out;
}

QSet<double> CalcIntensives::metadataBreaks(int id)
{
    QSet<double> breaks;
    if (processing[id].inputScattering != NULL)
        Util::merge(processing[id].inputScattering->getChangedPoints(), breaks);
    if (processing[id].inputBackScattering != NULL)
        Util::merge(processing[id].inputBackScattering->getChangedPoints(), breaks);
    if (processing[id].inputAbsorption != NULL)
        Util::merge(processing[id].inputAbsorption->getChangedPoints(), breaks);
    if (processing[id].inputExtinction != NULL)
        Util::merge(processing[id].inputExtinction->getChangedPoints(), breaks);

    Util::merge(processing[id].inputMassAbsorptionCalibration->getChangedPoints(), breaks);
    for (std::vector<Processing::OpticalWavelength>::const_iterator
            o = processing[id].optical.begin(); o != processing[id].optical.end(); ++o) {
        Util::merge(o->wavelength->getChangedPoints(), breaks);
        Util::merge(o->inputBackscatterFraction->getChangedPoints(), breaks);
        Util::merge(o->inputSingleScatteringAlbedo->getChangedPoints(), breaks);
        Util::merge(o->inputMassAbsorptionEfficiency->getChangedPoints(), breaks);

        Util::merge(o->outputScattering->getChangedPoints(), breaks);
        Util::merge(o->outputAbsorption->getChangedPoints(), breaks);
        Util::merge(o->outputExtinction->getChangedPoints(), breaks);
        Util::merge(o->outputSSA->getChangedPoints(), breaks);
        Util::merge(o->outputAngstromScattering->getChangedPoints(), breaks);
        Util::merge(o->outputAngstromAbsorption->getChangedPoints(), breaks);
        Util::merge(o->outputAngstromExtinction->getChangedPoints(), breaks);
        Util::merge(o->outputBackscatterFraction->getChangedPoints(), breaks);
        Util::merge(o->outputAsymmetry->getChangedPoints(), breaks);
        Util::merge(o->outputRFE->getChangedPoints(), breaks);
        Util::merge(o->outputEBC->getChangedPoints(), breaks);
    }
    for (std::vector<Processing::CopyVariable>::iterator c = processing[id].copy.begin();
            c != processing[id].copy.end();
            ++c) {
        Util::merge(c->input->getChangedPoints(), breaks);
        Util::merge(c->output->getChangedPoints(), breaks);
        Util::merge(c->smoothing->getChangedPoints(), breaks);
    }
    for (std::vector<Processing::PairedWavelengths>::iterator w = processing[id].paired.begin();
            w != processing[id].paired.end();
            ++w) {
        Util::merge(w->input->getChangedPoints(), breaks);
        Util::merge(w->outputAngstrom->getChangedPoints(), breaks);
    }
    return breaks;
}

CalcIntensives::CalcIntensives(QDataStream &stream)
{
    stream >> defaultWavelengths;
    stream >> defaultBackScatterFraction;
    stream >> defaultSingleScatteringAlbedo;
    stream >> defaultMassAbsorptionCalibration;
    stream >> defaultFallbackAngstrom;
    stream >> defaultFallbackAngstromWavelength;
    stream >> defaultFallbackAngstromDistance;
    stream >> defaultScattering;
    stream >> defaultBackScattering;
    stream >> defaultAbsorption;
    stream >> defaultExtinction;
    stream >> defaultCounts;
    stream >> scatteringSuffixes;
    stream >> absorptionSuffixes;
    stream >> extinctionSuffixes;
    stream >> countsSuffixes;
    stream >> outputSuffixes;
    stream >> restrictedInputs;

    quint32 n;
    stream >> n;
    for (quint32 i = 0; i < n; i++) {
        Processing p;
        stream >> p.inputScattering;
        stream >> p.inputBackScattering;
        stream >> p.inputAbsorption;
        stream >> p.inputExtinction;
        stream >> p.inputMassAbsorptionCalibration;

        stream >> p.suffixUnit;
        stream >> p.scatteringSuffix;
        stream >> p.backScatteringSuffix;
        stream >> p.absorptionSuffix;
        stream >> p.extinctionSuffix;
        stream >> p.countsSuffix;

        quint32 m;
        stream >> m;
        for (quint32 j = 0; j < m; j++) {
            Processing::OpticalWavelength o;

            stream >> o.wavelength;
            stream >> o.inputBackscatterFraction;
            stream >> o.inputSingleScatteringAlbedo;
            stream >> o.inputMassAbsorptionEfficiency;

            stream >> o.outputScattering;
            stream >> o.outputAbsorption;
            stream >> o.outputExtinction;
            stream >> o.outputSSA;
            stream >> o.outputAngstromScattering;
            stream >> o.outputAngstromAbsorption;
            stream >> o.outputAngstromExtinction;
            stream >> o.outputBackscatterFraction;
            stream >> o.outputAsymmetry;
            stream >> o.outputRFE;
            stream >> o.outputEBC;

            p.optical.push_back(o);
        }

        stream >> m;
        for (quint32 j = 0; j < m; j++) {
            Processing::CopyVariable c;

            stream >> c.input;
            stream >> c.output;
            stream >> c.smoothing;

            p.copy.push_back(c);
        }

        stream >> m;
        for (quint32 j = 0; j < m; j++) {
            Processing::PairedWavelengths w;

            stream >> w.input;
            stream >> w.wavelengthTracker;
            stream >> w.outputAngstrom;

            p.paired.push_back(w);
        }

        processing.push_back(p);
    }
}

void CalcIntensives::serialize(QDataStream &stream)
{
    stream << defaultWavelengths;
    stream << defaultBackScatterFraction;
    stream << defaultSingleScatteringAlbedo;
    stream << defaultMassAbsorptionCalibration;
    stream << defaultFallbackAngstrom;
    stream << defaultFallbackAngstromWavelength;
    stream << defaultFallbackAngstromDistance;
    stream << defaultScattering;
    stream << defaultBackScattering;
    stream << defaultAbsorption;
    stream << defaultExtinction;
    stream << defaultCounts;
    stream << scatteringSuffixes;
    stream << absorptionSuffixes;
    stream << extinctionSuffixes;
    stream << countsSuffixes;
    stream << outputSuffixes;
    stream << restrictedInputs;

    stream << (quint32) processing.size();
    for (std::vector<Processing>::iterator p = processing.begin(); p != processing.end(); ++p) {
        stream << p->inputScattering;
        stream << p->inputBackScattering;
        stream << p->inputAbsorption;
        stream << p->inputExtinction;
        stream << p->inputMassAbsorptionCalibration;

        stream << p->suffixUnit;
        stream << p->scatteringSuffix;
        stream << p->backScatteringSuffix;
        stream << p->absorptionSuffix;
        stream << p->extinctionSuffix;
        stream << p->countsSuffix;

        stream << (quint32) p->optical.size();
        for (std::vector<Processing::OpticalWavelength>::iterator o = p->optical.begin();
                o != p->optical.end();
                ++o) {
            stream << o->wavelength;
            stream << o->inputBackscatterFraction;
            stream << o->inputSingleScatteringAlbedo;
            stream << o->inputMassAbsorptionEfficiency;

            stream << o->outputScattering;
            stream << o->outputAbsorption;
            stream << o->outputExtinction;
            stream << o->outputSSA;
            stream << o->outputAngstromScattering;
            stream << o->outputAngstromAbsorption;
            stream << o->outputAngstromExtinction;
            stream << o->outputBackscatterFraction;
            stream << o->outputAsymmetry;
            stream << o->outputRFE;
            stream << o->outputEBC;
        }

        stream << (quint32) p->copy.size();
        for (std::vector<Processing::CopyVariable>::iterator c = p->copy.begin();
                c != p->copy.end();
                ++c) {
            stream << c->input;
            stream << c->output;
            stream << c->smoothing;
        }

        stream << (quint32) p->paired.size();
        for (std::vector<Processing::PairedWavelengths>::iterator w = p->paired.begin();
                w != p->paired.end();
                ++w) {
            stream << w->input;
            stream << w->wavelengthTracker;
            stream << w->outputAngstrom;
        }
    }
}


QString CalcIntensivesComponent::getBasicSerializationName() const
{ return QString::fromLatin1("calc_intensives"); }

ComponentOptions CalcIntensivesComponent::getOptions()
{
    ComponentOptions options;

    ComponentOptionDoubleSet *wl =
            new ComponentOptionDoubleSet(tr("wavelengths", "name"), tr("Wavelengths to generate"),
                                         tr("These are the output wavelengths to calculate the intensive "
                                            "parameters at.  Input data will be adjusted to these wavelengths "
                                            "before use."), tr("450,550,700"));
    wl->setMinimum(0.0, false);
    options.add("wavelengths", wl);

    options.add("bfr", new DynamicInputOption(tr("bfr", "name"), tr("Input backscatter fraction"),
                                              tr("When set, this specifies a backscatter fraction to use.  This "
                                                 "allows other parameters to be calculated when there is no "
                                                 "backscattering data available by assuming a fixed backscatter "
                                                 "ratio."),
                                              tr("Calculated from the scattering data")));

    options.add("ssa",
                new DynamicInputOption(tr("ssa", "name"), tr("Input single scattering albedo"),
                                       tr("When set, this specifies a single scattering albedo to use.  This "
                                          "allows other parameters to be calculated assuming a fixed "
                                          "value."), tr("Calculated from data")));

    options.add("mac", new DynamicInputOption(tr("mac", "name"), tr("Mass absorption calibration"),
                                              tr("This is the calibration factor used to calculate equivalent "
                                                 "black carbon concentrations.  The efficiency for each wavelength "
                                                 "is this value divided by the wavelength in nm."),
                                              tr("14625")));

    options.add("angstrom", new DynamicInputOption(tr("angstrom", "name"),
                                                   tr("Adjustment fallback \xC3\x85ngstr\xC3\xB6m exponent"),
                                                   tr("This is the \xC3\x85ngstr\xC3\xB6m exponent used to perform "
                                                          "wavelength adjustment when interpolation is not possible.  When "
                                                          "not set, data are simply left as undefined when they cannot be "
                                                          "adjusted to the target wavelengths."),
                                                   QString()));

    DynamicDoubleOption *angWL = new DynamicDoubleOption(tr("angstrom-wavelength", "name"),
                                                         tr("Adjustment fallback \xC3\x85ngstr\xC3\xB6m wavelength"),
                                                         tr("This is the wavelength of the \xC3\x85ngstr\xC3\xB6m exponent "
                                                                "used to perform wavelength adjustment when interpolation is "
                                                                "not possible.  This is used to limit how far data are "
                                                                "extrapolated when used in conjunction with angstrom-distance."),
                                                         QString());
    angWL->setMinimum(0.0, false);
    angWL->setAllowUndefined(true);
    options.add("angstrom-wavelength", angWL);

    DynamicDoubleOption *angDist = new DynamicDoubleOption(tr("angstrom-distance", "name"),
                                                           tr("Adjustment fallback \xC3\x85ngstr\xC3\xB6m distance"),
                                                           tr("This is the maximum distance that the fallback "
                                                                  "\xC3\x85gstr\xC3\xB6m exponent is valid for.  This is used in "
                                                                  "conjunction with angstrom-wavelength to limit how far data are "
                                                                  "extrapolated."), QString());
    angDist->setMinimum(0.0, true);
    angDist->setAllowUndefined(true);
    options.add("angstrom-distance", angDist);


    options.add("counts", new DynamicInputOption(tr("counts", "name"), tr("Input concentrations"),
                                                 tr("This is the input concentration to forward and rename in the "
                                                    "output.  This option is mutually exclusive with "
                                                    "concentration instrument specification."),
                                                 tr("All concentrations")));

    options.add("scattering",
                new DynamicSequenceSelectionOption(tr("scattering", "name"), tr("Input scattering"),
                                                   tr("This is the input light scattering to use in intensives "
                                                      "generation.  This option is mutually exclusive with "
                                                      "scattering instrument specification."),
                                                   tr("All light scatterings")));

    options.add("backscattering", new DynamicSequenceSelectionOption(tr("backscattering", "name"),
                                                                     tr("Input backscattering"),
                                                                     tr("This is the input light backscattering to use in intensives "
                                                                        "generation.  This option is mutually exclusive with "
                                                                        "scattering instrument specification."),
                                                                     tr("All light backscatterings")));

    options.add("absorption",
                new DynamicSequenceSelectionOption(tr("absorption", "name"), tr("Input absorption"),
                                                   tr("This is the input light absorption to use in intensives "
                                                      "generation.  This option is mutually exclusive with "
                                                      "absorption instrument specification."),
                                                   tr("All light absorptions")));

    options.add("extinction",
                new DynamicSequenceSelectionOption(tr("extinction", "name"), tr("Input extinction"),
                                                   tr("This is the input light extinction to use in intensives "
                                                      "generation.  This option is mutually exclusive with "
                                                      "extinction instrument specification."),
                                                   tr("All light extinctions")));


    options.add("scattering-suffix",
                new ComponentOptionInstrumentSuffixSet(tr("scattering-instruments", "name"),
                                                       tr("Scattering instrument suffixes"),
                                                       tr("These are the instrument suffixes to use scattering data from.  "
                                                          "For example S11 would usually specifies the reference "
                                                          "nephelometer.  This option is mutually exclusive with "
                                                          "manual scattering variable specification."),
                                                       tr("All instrument suffixes")));
    options.exclude("scattering", "scattering-suffix");
    options.exclude("backscattering", "scattering-suffix");
    options.exclude("scattering-suffix", "scattering");

    options.add("absorption-suffix",
                new ComponentOptionInstrumentSuffixSet(tr("absorption-instruments", "name"),
                                                       tr("Absorption instrument suffixes"),
                                                       tr("These are the instrument suffixes to use absorption data from.  "
                                                          "For example A11 would usually specifies the primary absorption "
                                                          "instrument.  This option is mutually exclusive with "
                                                          "manual absorption variable specification."),
                                                       tr("All instrument suffixes")));
    options.exclude("absorption", "absorption-suffix");
    options.exclude("absorption-suffix", "absorption");

    options.add("extinction-suffix",
                new ComponentOptionInstrumentSuffixSet(tr("extinction-instruments", "name"),
                                                       tr("Extinction instrument suffixes"),
                                                       tr("These are the instrument suffixes to use extinction data from.  "
                                                          "For example E11 would usually specifies the first extinction "
                                                          "instrument.  This option is mutually exclusive with "
                                                          "manual extinction variable specification."),
                                                       tr("All instrument suffixes")));
    options.exclude("extinction", "extinction-suffix");
    options.exclude("extinction-suffix", "extinction");

    options.add("counts-suffix",
                new ComponentOptionInstrumentSuffixSet(tr("counts-instruments", "name"),
                                                       tr("Concentration instrument suffixes"),
                                                       tr("These are the instrument suffixes to use particle concentrations "
                                                          "from.  For example N61 or N71 usually specifies the system CPC.  "
                                                          "This option is mutually exclusive with manual counts variable "
                                                          "specification."),
                                                       tr("All instrument suffixes")));
    options.exclude("counts", "counts-suffix");
    options.exclude("counts-suffix", "counts");

    options.add("output-suffix", new ComponentOptionInstrumentSuffixSet(tr("output", "name"),
                                                                        tr("Output variable suffix"),
                                                                        tr("These are the instrument suffixes that output data will be "
                                                                           "generated with first.  If the number of outputs exceeds the "
                                                                           "number of suffixes defined then the defaults are used."),
                                                                        tr("XI, XJ, etc")));

    return options;
}

QList<ComponentExample> CalcIntensivesComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will generate intensives for all input data.  Note that the "
                                        "output variable assignment is un-ordered in the case of multiple "
                                        "sources of extensive parameters,")));

    options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("scattering-suffix")))->add(
            "S12");
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("absorption-suffix")))->add(
            "A12");
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("extinction-suffix")))->clear();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("counts-suffix")))->add("N61");
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("output-suffix")))->add("XJ");
    examples.append(ComponentExample(options, tr("Restricted instruments"),
                                     tr("This will calculate intensives from only S12, A12, and N61 "
                                        "regardless of what data is present in the input.  Output "
                                        "variables will have the XJ suffix (for example BsG_XJ).")));

    options = getOptions();
    (qobject_cast<ComponentOptionDoubleSet *>(options.get("wavelengths")))->set(
            QSet<double>() << 467 << 528 << 652);
    (qobject_cast<DynamicInputOption *>(options.get("bfr")))->set(0.2);
    examples.append(ComponentExample(options,
                                     tr("Non-standard wavelengths and manual backscatter fraction"),
                                     tr("This will calculate intensives at 467, 528, and 652nm using a "
                                        "fixed intensives fraction of 0.2 instead of calculating it "
                                        "from the data.")));

    options = getOptions();
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("scattering")))->set("", "",
                                                                                     "BsG_S11");
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("backscattering")))->set("", "",
                                                                                         "BbsG_S11");
    examples.append(ComponentExample(options, tr("Explicit scattering"),
                                     tr("This will calculate intensives using only green scattering from "
                                        "the S11 instrument.")));

    return examples;
}

SegmentProcessingStage *CalcIntensivesComponent::createBasicFilterDynamic(const ComponentOptions &options)
{ return new CalcIntensives(options); }

SegmentProcessingStage *CalcIntensivesComponent::createBasicFilterPredefined(const ComponentOptions &options,
                                                                             double start,
                                                                             double end,
                                                                             const QList<
                                                                                     SequenceName> &inputs)
{ return new CalcIntensives(options, start, end, inputs); }

SegmentProcessingStage *CalcIntensivesComponent::createBasicFilterEditing(double start,
                                                                          double end,
                                                                          const SequenceName::Component &station,
                                                                          const SequenceName::Component &archive,
                                                                          const ValueSegment::Transfer &config)
{ return new CalcIntensives(start, end, station, archive, config); }

SegmentProcessingStage *CalcIntensivesComponent::deserializeBasicFilter(QDataStream &stream)
{ return new CalcIntensives(stream); }
