/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CALCINTENSIVES_H
#define CALCINTENSIVES_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QStringList>
#include <QSet>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "editing/wavelengthadjust.hxx"

class CalcIntensives : public CPD3::Data::SegmentProcessingStage {
    typedef CPD3::Data::DynamicPrimitive<CPD3::Data::Variant::Root> DynamicMetadata;

    struct Processing {
        CPD3::Editing::WavelengthAdjust *inputScattering;
        CPD3::Editing::WavelengthAdjust *inputBackScattering;
        CPD3::Editing::WavelengthAdjust *inputAbsorption;
        CPD3::Editing::WavelengthAdjust *inputExtinction;

        CPD3::Data::DynamicInput *inputMassAbsorptionCalibration;

        struct OpticalWavelength {
            CPD3::Data::DynamicDouble *wavelength;

            CPD3::Data::DynamicInput *inputBackscatterFraction;
            CPD3::Data::DynamicInput *inputSingleScatteringAlbedo;
            CPD3::Data::DynamicInput *inputMassAbsorptionEfficiency;

            CPD3::Data::DynamicSequenceSelection *outputScattering;
            CPD3::Data::DynamicSequenceSelection *outputAbsorption;
            CPD3::Data::DynamicSequenceSelection *outputExtinction;
            CPD3::Data::DynamicSequenceSelection *outputSSA;
            CPD3::Data::DynamicSequenceSelection *outputAngstromScattering;
            CPD3::Data::DynamicSequenceSelection *outputAngstromAbsorption;
            CPD3::Data::DynamicSequenceSelection *outputAngstromExtinction;
            CPD3::Data::DynamicSequenceSelection *outputBackscatterFraction;
            CPD3::Data::DynamicSequenceSelection *outputAsymmetry;
            CPD3::Data::DynamicSequenceSelection *outputRFE;
            CPD3::Data::DynamicSequenceSelection *outputEBC;

            OpticalWavelength() : wavelength(NULL),
                                  inputBackscatterFraction(NULL),
                                  inputSingleScatteringAlbedo(NULL),
                                  inputMassAbsorptionEfficiency(NULL),
                                  outputScattering(NULL),
                                  outputAbsorption(NULL),
                                  outputExtinction(NULL),
                                  outputSSA(NULL),
                                  outputAngstromScattering(NULL),
                                  outputAngstromAbsorption(NULL),
                                  outputAngstromExtinction(NULL),
                                  outputBackscatterFraction(NULL),
                                  outputAsymmetry(NULL),
                                  outputRFE(NULL)
            { }
        };

        std::vector<OpticalWavelength> optical;

        struct CopyVariable {
            CPD3::Data::DynamicInput *input;
            CPD3::Data::DynamicSequenceSelection *output;
            DynamicMetadata *smoothing;

            CopyVariable() : input(NULL), output(NULL), smoothing(NULL)
            { }
        };

        std::vector<CopyVariable> copy;

        struct PairedWavelengths {
            CPD3::Data::DynamicSequenceSelection *input;

            CPD3::Editing::WavelengthTracker wavelengthTracker;

            CPD3::Data::DynamicSequenceSelection *outputAngstrom;

            CPD3::Data::SequenceName::Component pairCode;

            PairedWavelengths() : input(NULL), wavelengthTracker(), outputAngstrom(NULL), pairCode()
            { }
        };

        std::vector<PairedWavelengths> paired;

        CPD3::Data::SequenceName suffixUnit;

        CPD3::Data::SequenceName::Component scatteringSuffix;
        CPD3::Data::SequenceName::Component backScatteringSuffix;
        CPD3::Data::SequenceName::Component absorptionSuffix;
        CPD3::Data::SequenceName::Component extinctionSuffix;
        CPD3::Data::SequenceName::Component countsSuffix;

        Processing() : inputScattering(NULL),
                       inputBackScattering(NULL),
                       inputAbsorption(NULL),
                       inputExtinction(NULL)
        { }
    };

    QSet<double> defaultWavelengths;
    CPD3::Data::DynamicInput *defaultBackScatterFraction;
    CPD3::Data::DynamicInput *defaultSingleScatteringAlbedo;
    CPD3::Data::DynamicInput *defaultMassAbsorptionCalibration;
    CPD3::Data::DynamicInput *defaultFallbackAngstrom;
    CPD3::Data::DynamicDouble *defaultFallbackAngstromWavelength;
    CPD3::Data::DynamicDouble *defaultFallbackAngstromDistance;
    CPD3::Editing::WavelengthAdjust *defaultScattering;
    CPD3::Editing::WavelengthAdjust *defaultBackScattering;
    CPD3::Editing::WavelengthAdjust *defaultAbsorption;
    CPD3::Editing::WavelengthAdjust *defaultExtinction;
    CPD3::Data::DynamicInput *defaultCounts;

    CPD3::Data::SequenceName::ComponentSet scatteringSuffixes;
    CPD3::Data::SequenceName::ComponentSet absorptionSuffixes;
    CPD3::Data::SequenceName::ComponentSet extinctionSuffixes;
    CPD3::Data::SequenceName::ComponentSet countsSuffixes;
    std::vector<CPD3::Data::SequenceName::Component> outputSuffixes;

    bool restrictedInputs;

    std::vector<Processing> processing;

    void handleOptions(const CPD3::ComponentOptions &options);

    void handleNewProcessing(const CPD3::Data::SequenceName &unit,
                             int id,
                             CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control);

    void configureAdjuster(CPD3::Editing::WavelengthAdjust *adj);

    CPD3::Editing::WavelengthAdjust *createAdjuster(const CPD3::Data::SequenceName::Component &prefix,
                                                    const CPD3::Data::SequenceName::Component &station,
                                                    const CPD3::Data::SequenceName::Component &archive,
                                                    const CPD3::Data::SequenceName::Component &suffix,
                                                    const CPD3::Data::SequenceName::Flavors &flavors);

    bool createOpticalIfNeeded(const CPD3::Data::SequenceName &unit,
                               int id,
                               CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control);

    void createScattering(const CPD3::Data::SequenceName &unit,
                          int id,
                          CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                          const CPD3::Data::SequenceName::Component &suffix);

    void createBackScattering(const CPD3::Data::SequenceName &unit,
                              int id,
                              CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                              const CPD3::Data::SequenceName::Component &suffix);

    void createAbsorption(const CPD3::Data::SequenceName &unit,
                          int id,
                          CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                          const CPD3::Data::SequenceName::Component &suffix);

    void createExtinction(const CPD3::Data::SequenceName &unit,
                          int id,
                          CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                          const CPD3::Data::SequenceName::Component &suffix);

    void createCounts(const CPD3::Data::SequenceName &unit,
                      int id,
                      CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                      const CPD3::Data::SequenceName::Component &suffix);

    bool shouldCreatePaired(double lower, double upper) const;

    void createAllPaired(const CPD3::Data::SequenceName &unit,
                         int id,
                         CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                         const CPD3::Data::SequenceName::Component &suffix,
                         const CPD3::Data::SequenceName::Component &type,
                         const CPD3::Data::SequenceName::Component &code);

    void createPaired(const CPD3::Data::SequenceName &unit,
                      int id,
                      CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                      const CPD3::Data::SequenceName::Component &suffix,
                      const CPD3::Data::SequenceName::Component &type,
                      const CPD3::Data::SequenceName::Component &first,
                      const CPD3::Data::SequenceName::Component &second);

    void registerPossibleInput(const CPD3::Data::SequenceName &unit,
                               CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                               int filterID = -1);

public:
    CalcIntensives();

    CalcIntensives(const CPD3::ComponentOptions &options);

    CalcIntensives(const CPD3::ComponentOptions &options,
                   double start,
                   double end, const QList<CPD3::Data::SequenceName> &inputs);

    CalcIntensives(double start,
                   double end,
                   const CPD3::Data::SequenceName::Component &station,
                   const CPD3::Data::SequenceName::Component &archive,
                   const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CalcIntensives();

    void unhandled(const CPD3::Data::SequenceName &unit,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;

    QSet<double> metadataBreaks(int id) override;

    CalcIntensives(QDataStream &stream);

    void serialize(QDataStream &stream) override;
};

class CalcIntensivesComponent
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.calc_intensives"
                              FILE
                              "calc_intensives.json")
public:
    virtual QString getBasicSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterDynamic(const CPD3::ComponentOptions &options);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined(const CPD3::ComponentOptions &options,
                                                                 double start,
                                                                 double end,
                                                                 const QList<
                                                                         CPD3::Data::SequenceName> &inputs);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                         double end,
                                                                         const CPD3::Data::SequenceName::Component &station,
                                                                         const CPD3::Data::SequenceName::Component &archive,
                                                                         const CPD3::Data::ValueSegment::Transfer &config);

    virtual CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream);
};

#endif
