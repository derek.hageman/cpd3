/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    class ID {
    public:
        ID() : filter(), input()
        { }

        ID(const QSet<SequenceName> &setFilter, const QSet<SequenceName> &setInput) : filter(
                setFilter), input(setInput)
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }

        QSet<SequenceName> filter;
        QSet<SequenceName> input;
    };

    QHash<int, ID> contents;

    virtual void filterUnit(const SequenceName &unit, int targetID)
    {
        contents[targetID].input.remove(unit);
        contents[targetID].filter.insert(unit);
    }

    virtual void inputUnit(const SequenceName &unit, int targetID)
    {
        if (contents[targetID].filter.contains(unit))
            return;
        contents[targetID].input.insert(unit);
    }

    virtual bool isHandled(const SequenceName &unit)
    {
        for (QHash<int, ID>::const_iterator it = contents.constBegin();
                it != contents.constEnd();
                ++it) {
            if (it.value().filter.contains(unit))
                return true;
            if (it.value().input.contains(unit))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

    static Variant::Root coefficientMeta(double wavelength)
    {
        Variant::Root m;
        m.write().metadataReal("Wavelength").setDouble(wavelength);
        return m;
    }

    static double adjust(double target, double wl, double wu, double vl, double vu)
    {
        if (vl > 0.0 && vu > 0.0) {
            double f = log(target / wl) / log(wu / wl);
            double ll = log(vl);
            double lu = log(vu);
            return exp(ll + (lu - ll) * f);
        }

        double f = (target - wl) / (wu - wl);
        return vl + (vu - vl) * f;
    }

    static double angstrom(double wl, double wu, double vl, double vu)
    {
        return log(vl / vu) / log(wu / wl);
    }

    static double asymmetry(double Bfr,
                            const Calibration &cal = Calibration(
                                    QVector<double>() << 0.9893 << -3.9636 << 7.4644 << -7.1439))
    {
        return cal.apply(Bfr);
    }

    static double rfe(double Bs, double Bbs, double Ba)
    {
        static const Calibration betaCal(QVector<double>() << 0.0817 << 1.8495 << -2.9682);
        static const double daylightFraction = 0.5;
        static const double solarConstant = 1370;
        static const double atmosphericTransmission = 0.76;
        static const double cloudFraction = 0.6;
        static const double surfaceAlbedo = 0.15;

        double ssa = Bs / (Bs + Ba);
        double beta = betaCal.apply(Bbs / Bs);
        double disa = 1.0 - surfaceAlbedo;
        disa = disa * disa;

        return -1.0 *
                daylightFraction *
                solarConstant *
                atmosphericTransmission *
                atmosphericTransmission *
                (1.0 - cloudFraction) *
                ssa *
                beta *
                (disa - (2.0 * surfaceAlbedo / beta * ((1.0 / ssa) - 1)));
    }

    static double ebc(double Ba, double sigma)
    {
        return Ba / sigma;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("calc_intensives"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionDoubleSet *>(options.get("wavelengths")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(
                options.get("scattering-suffix")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(
                options.get("absorption-suffix")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(
                options.get("extinction-suffix")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("counts-suffix")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("bfr")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("ssa")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("counts")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("scattering")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("backscattering")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("absorption")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("extinction")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("angstrom")));
        QVERIFY(qobject_cast<DynamicDoubleOption *>(options.get("angstrom-distance")));
        QVERIFY(qobject_cast<DynamicDoubleOption *>(options.get("angstrom-wavelength")));
    }

    void dynamicDefaults()
    {
        ComponentOptions options(component->getOptions());
        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        SegmentProcessingStage *filter2;
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "T_S11"), &controller);
        QVERIFY(controller.contents.isEmpty());
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2 != NULL);
            filter2->unhandled(SequenceName("brw", "raw", "T_S11"), &controller);
            QVERIFY(controller.contents.isEmpty());
            delete filter2;
        }

        SequenceName BsG_S11_1("brw", "raw", "BsG_S11");
        SequenceName BsB_S11_1("brw", "raw", "BsB_S11");
        SequenceName BbsG_S11_1("brw", "raw", "BbsG_S11");
        SequenceName BbsB_S11_1("brw", "raw", "BbsB_S11");
        SequenceName BaG_A11_1("brw", "raw", "BaG_A11");
        SequenceName BaB_A11_1("brw", "raw", "BaB_A11");
        SequenceName N_N11_1("brw", "raw", "N_N11");
        SequenceName BsB_XI_1("brw", "raw", "BsB_XI");
        SequenceName BsG_XI_1("brw", "raw", "BsG_XI");
        SequenceName BsR_XI_1("brw", "raw", "BsR_XI");
        SequenceName BaB_XI_1("brw", "raw", "BaB_XI");
        SequenceName BaG_XI_1("brw", "raw", "BaG_XI");
        SequenceName BaR_XI_1("brw", "raw", "BaR_XI");
        SequenceName BeB_XI_1("brw", "raw", "BeB_XI");
        SequenceName BeG_XI_1("brw", "raw", "BeG_XI");
        SequenceName BeR_XI_1("brw", "raw", "BeR_XI");
        SequenceName ZSSAB_XI_1("brw", "raw", "ZSSAB_XI");
        SequenceName ZSSAG_XI_1("brw", "raw", "ZSSAG_XI");
        SequenceName ZSSAR_XI_1("brw", "raw", "ZSSAR_XI");
        SequenceName ZAngBsB_XI_1("brw", "raw", "ZAngBsB_XI");
        SequenceName ZAngBsG_XI_1("brw", "raw", "ZAngBsG_XI");
        SequenceName ZAngBsR_XI_1("brw", "raw", "ZAngBsR_XI");
        SequenceName ZAngBaB_XI_1("brw", "raw", "ZAngBaB_XI");
        SequenceName ZAngBaG_XI_1("brw", "raw", "ZAngBaG_XI");
        SequenceName ZAngBaR_XI_1("brw", "raw", "ZAngBaR_XI");
        SequenceName ZAngBeB_XI_1("brw", "raw", "ZAngBeB_XI");
        SequenceName ZAngBeG_XI_1("brw", "raw", "ZAngBeG_XI");
        SequenceName ZAngBeR_XI_1("brw", "raw", "ZAngBeR_XI");
        SequenceName ZBfrB_XI_1("brw", "raw", "ZBfrB_XI");
        SequenceName ZBfrG_XI_1("brw", "raw", "ZBfrG_XI");
        SequenceName ZBfrR_XI_1("brw", "raw", "ZBfrR_XI");
        SequenceName ZGB_XI_1("brw", "raw", "ZGB_XI");
        SequenceName ZGG_XI_1("brw", "raw", "ZGG_XI");
        SequenceName ZGR_XI_1("brw", "raw", "ZGR_XI");
        SequenceName ZRFEB_XI_1("brw", "raw", "ZRFEB_XI");
        SequenceName ZRFEG_XI_1("brw", "raw", "ZRFEG_XI");
        SequenceName ZRFER_XI_1("brw", "raw", "ZRFER_XI");
        SequenceName XB_XI_1("brw", "raw", "XB_XI");
        SequenceName XG_XI_1("brw", "raw", "XG_XI");
        SequenceName XR_XI_1("brw", "raw", "XR_XI");
        SequenceName N_XI_1("brw", "raw", "N_XI");
        SequenceName ZAngBsBG_XI_1("brw", "raw", "ZAngBsBG_XI");
        SequenceName ZAngBsGR_XI_1("brw", "raw", "ZAngBsGR_XI");
        SequenceName ZAngBsBR_XI_1("brw", "raw", "ZAngBsBR_XI");
        SequenceName ZAngBaBG_XI_1("brw", "raw", "ZAngBaBG_XI");
        SequenceName ZAngBaGR_XI_1("brw", "raw", "ZAngBaGR_XI");
        SequenceName ZAngBaBR_XI_1("brw", "raw", "ZAngBaBR_XI");

        filter->unhandled(BsG_S11_1, &controller);
        filter->unhandled(BsB_S11_1, &controller);
        filter->unhandled(BbsG_S11_1, &controller);
        filter->unhandled(BbsB_S11_1, &controller);
        filter->unhandled(BaG_A11_1, &controller);
        filter->unhandled(BaB_A11_1, &controller);
        filter->unhandled(N_N11_1, &controller);
        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << N_XI_1 << BsB_XI_1 << BsG_XI_1 << BsR_XI_1
                                                     << BaB_XI_1 << BaG_XI_1 << BaR_XI_1
                                                     << BeB_XI_1 << BeG_XI_1 << BeR_XI_1
                                                     << ZSSAB_XI_1 << ZSSAG_XI_1 << ZSSAR_XI_1
                                                     << ZAngBsB_XI_1 << ZAngBsG_XI_1
                                                     << ZAngBsR_XI_1 << ZAngBaB_XI_1
                                                      << ZAngBaG_XI_1 << ZAngBaR_XI_1
                                                      << ZAngBeB_XI_1 << ZAngBeG_XI_1
                                                      << ZAngBeR_XI_1 << ZBfrB_XI_1 << ZBfrG_XI_1
                                                     << ZBfrR_XI_1 << ZGB_XI_1 << ZGG_XI_1
                                                     << ZGR_XI_1 << ZRFEB_XI_1 << ZRFEG_XI_1
                                                     << ZRFER_XI_1 << XB_XI_1 << XG_XI_1 << XR_XI_1
                                                     << ZAngBsBG_XI_1 << ZAngBsGR_XI_1
                                                     << ZAngBsBR_XI_1 << ZAngBaBG_XI_1
                                                     << ZAngBaGR_XI_1 << ZAngBaBR_XI_1,
                                QSet<SequenceName>() << N_N11_1 << BsG_S11_1 << BsB_S11_1
                                                     << BbsG_S11_1 << BbsB_S11_1 << BaG_A11_1
                                                     << BaB_A11_1));
        QCOMPARE(idL.size(), 1);
        int XI_1 = idL.at(0);


        SequenceName BsG_S11_2("sgp", "raw", "BsG_S11");
        SequenceName BsB_S11_2("sgp", "raw", "BsB_S11");
        SequenceName BbsG_S11_2("sgp", "raw", "BbsG_S11");
        SequenceName BbsB_S11_2("sgp", "raw", "BbsB_S11");
        SequenceName BaG_A11_2("sgp", "raw", "BaG_A11");
        SequenceName BaB_A11_2("sgp", "raw", "BaB_A11");
        SequenceName N_N11_2("sgp", "raw", "N_N11");
        SequenceName BsB_XI_2("sgp", "raw", "BsB_XI");
        SequenceName BsG_XI_2("sgp", "raw", "BsG_XI");
        SequenceName BsR_XI_2("sgp", "raw", "BsR_XI");
        SequenceName BaB_XI_2("sgp", "raw", "BaB_XI");
        SequenceName BaG_XI_2("sgp", "raw", "BaG_XI");
        SequenceName BaR_XI_2("sgp", "raw", "BaR_XI");
        SequenceName BeB_XI_2("sgp", "raw", "BeB_XI");
        SequenceName BeG_XI_2("sgp", "raw", "BeG_XI");
        SequenceName BeR_XI_2("sgp", "raw", "BeR_XI");
        SequenceName ZSSAB_XI_2("sgp", "raw", "ZSSAB_XI");
        SequenceName ZSSAG_XI_2("sgp", "raw", "ZSSAG_XI");
        SequenceName ZSSAR_XI_2("sgp", "raw", "ZSSAR_XI");
        SequenceName ZAngBsB_XI_2("sgp", "raw", "ZAngBsB_XI");
        SequenceName ZAngBsG_XI_2("sgp", "raw", "ZAngBsG_XI");
        SequenceName ZAngBsR_XI_2("sgp", "raw", "ZAngBsR_XI");
        SequenceName ZAngBaB_XI_2("sgp", "raw", "ZAngBaB_XI");
        SequenceName ZAngBaG_XI_2("sgp", "raw", "ZAngBaG_XI");
        SequenceName ZAngBaR_XI_2("sgp", "raw", "ZAngBaR_XI");
        SequenceName ZAngBeB_XI_2("sgp", "raw", "ZAngBeB_XI");
        SequenceName ZAngBeG_XI_2("sgp", "raw", "ZAngBeG_XI");
        SequenceName ZAngBeR_XI_2("sgp", "raw", "ZAngBeR_XI");
        SequenceName ZBfrB_XI_2("sgp", "raw", "ZBfrB_XI");
        SequenceName ZBfrG_XI_2("sgp", "raw", "ZBfrG_XI");
        SequenceName ZBfrR_XI_2("sgp", "raw", "ZBfrR_XI");
        SequenceName ZGB_XI_2("sgp", "raw", "ZGB_XI");
        SequenceName ZGG_XI_2("sgp", "raw", "ZGG_XI");
        SequenceName ZGR_XI_2("sgp", "raw", "ZGR_XI");
        SequenceName ZRFEB_XI_2("sgp", "raw", "ZRFEB_XI");
        SequenceName ZRFEG_XI_2("sgp", "raw", "ZRFEG_XI");
        SequenceName ZRFER_XI_2("sgp", "raw", "ZRFER_XI");
        SequenceName XB_XI_2("sgp", "raw", "XB_XI");
        SequenceName XG_XI_2("sgp", "raw", "XG_XI");
        SequenceName XR_XI_2("sgp", "raw", "XR_XI");
        SequenceName N_XI_2("sgp", "raw", "N_XI");
        SequenceName ZAngBsBG_XI_2("sgp", "raw", "ZAngBsBG_XI");
        SequenceName ZAngBsGR_XI_2("sgp", "raw", "ZAngBsGR_XI");
        SequenceName ZAngBsBR_XI_2("sgp", "raw", "ZAngBsBR_XI");
        SequenceName ZAngBaBG_XI_2("sgp", "raw", "ZAngBaBG_XI");
        SequenceName ZAngBaGR_XI_2("sgp", "raw", "ZAngBaGR_XI");
        SequenceName ZAngBaBR_XI_2("sgp", "raw", "ZAngBaBR_XI");

        filter->unhandled(BsG_S11_2, &controller);
        filter->unhandled(BsB_S11_2, &controller);
        filter->unhandled(BbsG_S11_2, &controller);
        filter->unhandled(BbsB_S11_2, &controller);
        filter->unhandled(BaG_A11_2, &controller);
        filter->unhandled(BaB_A11_2, &controller);
        filter->unhandled(N_N11_2, &controller);
        QCOMPARE(controller.contents.size(), 2);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << N_XI_2 << BsB_XI_2 << BsG_XI_2 << BsR_XI_2
                                                     << BaB_XI_2 << BaG_XI_2 << BaR_XI_2
                                                     << BeB_XI_2 << BeG_XI_2 << BeR_XI_2
                                                     << ZSSAB_XI_2 << ZSSAG_XI_2 << ZSSAR_XI_2
                                                     << ZAngBsB_XI_2 << ZAngBsG_XI_2
                                                     << ZAngBsR_XI_2 << ZAngBaB_XI_2
                                                      << ZAngBaG_XI_2 << ZAngBaR_XI_2
                                                      << ZAngBeB_XI_2 << ZAngBeG_XI_2
                                                      << ZAngBeR_XI_2 << ZBfrB_XI_2 << ZBfrG_XI_2
                                                     << ZBfrR_XI_2 << ZGB_XI_2 << ZGG_XI_2
                                                     << ZGR_XI_2 << ZRFEB_XI_2 << ZRFEG_XI_2
                                                     << ZRFER_XI_2 << XB_XI_2 << XG_XI_2 << XR_XI_2
                                                     << ZAngBsBG_XI_2 << ZAngBsGR_XI_2
                                                     << ZAngBsBR_XI_2 << ZAngBaBG_XI_2
                                                     << ZAngBaGR_XI_2 << ZAngBaBR_XI_2,
                                QSet<SequenceName>() << N_N11_2 << BsG_S11_2 << BsB_S11_2
                                                     << BbsG_S11_2 << BbsB_S11_2 << BaG_A11_2
                                                     << BaB_A11_2));
        QCOMPARE(idL.size(), 1);
        int XI_2 = idL.at(0);


        SequenceName BsG_S12_1("brw", "raw", "BsG_S12");
        SequenceName BsB_S12_1("brw", "raw", "BsB_S12");
        SequenceName BbsG_S12_1("brw", "raw", "BbsG_S12");
        SequenceName BbsB_S12_1("brw", "raw", "BbsB_S12");
        SequenceName BaG_A12_1("brw", "raw", "BaG_A12");
        SequenceName BaB_A12_1("brw", "raw", "BaB_A12");
        SequenceName N_N12_1("brw", "raw", "N_N12");
        SequenceName BsB_XJ_1("brw", "raw", "BsB_XJ");
        SequenceName BsG_XJ_1("brw", "raw", "BsG_XJ");
        SequenceName BsR_XJ_1("brw", "raw", "BsR_XJ");
        SequenceName BaB_XJ_1("brw", "raw", "BaB_XJ");
        SequenceName BaG_XJ_1("brw", "raw", "BaG_XJ");
        SequenceName BaR_XJ_1("brw", "raw", "BaR_XJ");
        SequenceName BeB_XJ_1("brw", "raw", "BeB_XJ");
        SequenceName BeG_XJ_1("brw", "raw", "BeG_XJ");
        SequenceName BeR_XJ_1("brw", "raw", "BeR_XJ");
        SequenceName ZSSAB_XJ_1("brw", "raw", "ZSSAB_XJ");
        SequenceName ZSSAG_XJ_1("brw", "raw", "ZSSAG_XJ");
        SequenceName ZSSAR_XJ_1("brw", "raw", "ZSSAR_XJ");
        SequenceName ZAngBsB_XJ_1("brw", "raw", "ZAngBsB_XJ");
        SequenceName ZAngBsG_XJ_1("brw", "raw", "ZAngBsG_XJ");
        SequenceName ZAngBsR_XJ_1("brw", "raw", "ZAngBsR_XJ");
        SequenceName ZAngBaB_XJ_1("brw", "raw", "ZAngBaB_XJ");
        SequenceName ZAngBaG_XJ_1("brw", "raw", "ZAngBaG_XJ");
        SequenceName ZAngBaR_XJ_1("brw", "raw", "ZAngBaR_XJ");
        SequenceName ZAngBeB_XJ_1("brw", "raw", "ZAngBeB_XJ");
        SequenceName ZAngBeG_XJ_1("brw", "raw", "ZAngBeG_XJ");
        SequenceName ZAngBeR_XJ_1("brw", "raw", "ZAngBeR_XJ");
        SequenceName ZBfrB_XJ_1("brw", "raw", "ZBfrB_XJ");
        SequenceName ZBfrG_XJ_1("brw", "raw", "ZBfrG_XJ");
        SequenceName ZBfrR_XJ_1("brw", "raw", "ZBfrR_XJ");
        SequenceName ZGB_XJ_1("brw", "raw", "ZGB_XJ");
        SequenceName ZGG_XJ_1("brw", "raw", "ZGG_XJ");
        SequenceName ZGR_XJ_1("brw", "raw", "ZGR_XJ");
        SequenceName ZRFEB_XJ_1("brw", "raw", "ZRFEB_XJ");
        SequenceName ZRFEG_XJ_1("brw", "raw", "ZRFEG_XJ");
        SequenceName ZRFER_XJ_1("brw", "raw", "ZRFER_XJ");
        SequenceName XB_XJ_1("brw", "raw", "XB_XJ");
        SequenceName XG_XJ_1("brw", "raw", "XG_XJ");
        SequenceName XR_XJ_1("brw", "raw", "XR_XJ");
        SequenceName N_XJ_1("brw", "raw", "N_XJ");
        SequenceName ZAngBsBG_XJ_1("brw", "raw", "ZAngBsBG_XJ");
        SequenceName ZAngBsGR_XJ_1("brw", "raw", "ZAngBsGR_XJ");
        SequenceName ZAngBsBR_XJ_1("brw", "raw", "ZAngBsBR_XJ");
        SequenceName ZAngBaBG_XJ_1("brw", "raw", "ZAngBaBG_XJ");
        SequenceName ZAngBaGR_XJ_1("brw", "raw", "ZAngBaGR_XJ");
        SequenceName ZAngBaBR_XJ_1("brw", "raw", "ZAngBaBR_XJ");

        filter->unhandled(BsG_S12_1, &controller);
        filter->unhandled(BsB_S12_1, &controller);
        filter->unhandled(BbsG_S12_1, &controller);
        filter->unhandled(BbsB_S12_1, &controller);
        filter->unhandled(BaG_A12_1, &controller);
        filter->unhandled(BaB_A12_1, &controller);
        filter->unhandled(N_N12_1, &controller);
        QCOMPARE(controller.contents.size(), 3);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << N_XJ_1 << BsB_XJ_1 << BsG_XJ_1 << BsR_XJ_1
                                                     << BaB_XJ_1 << BaG_XJ_1 << BaR_XJ_1
                                                     << BeB_XJ_1 << BeG_XJ_1 << BeR_XJ_1
                                                     << ZSSAB_XJ_1 << ZSSAG_XJ_1 << ZSSAR_XJ_1
                                                     << ZAngBsB_XJ_1 << ZAngBsG_XJ_1
                                                     << ZAngBsR_XJ_1 << ZAngBaB_XJ_1
                                                      << ZAngBaG_XJ_1 << ZAngBaR_XJ_1
                                                      << ZAngBeB_XJ_1 << ZAngBeG_XJ_1
                                                      << ZAngBeR_XJ_1 << ZBfrB_XJ_1 << ZBfrG_XJ_1
                                                     << ZBfrR_XJ_1 << ZGB_XJ_1 << ZGG_XJ_1
                                                     << ZGR_XJ_1 << ZRFEB_XJ_1 << ZRFEG_XJ_1
                                                     << ZRFER_XJ_1 << XB_XJ_1 << XG_XJ_1 << XR_XJ_1
                                                     << ZAngBsBG_XJ_1 << ZAngBsGR_XJ_1
                                                     << ZAngBsBR_XJ_1 << ZAngBaBG_XJ_1
                                                     << ZAngBaGR_XJ_1 << ZAngBaBR_XJ_1,
                                QSet<SequenceName>() << N_N12_1 << BsG_S12_1 << BsB_S12_1
                                                     << BbsG_S12_1 << BbsB_S12_1 << BaG_A12_1
                                                     << BaB_A12_1));
        QCOMPARE(idL.size(), 1);
        int XJ_1 = idL.at(0);

        data.setStart(15.0);
        data.setEnd(16.0);

        data.setValue(BsG_S11_1.toMeta(), coefficientMeta(550.0));
        data.setValue(BsB_S11_1.toMeta(), coefficientMeta(450.0));
        data.setValue(BbsG_S11_1.toMeta(), coefficientMeta(550.0));
        data.setValue(BbsB_S11_1.toMeta(), coefficientMeta(450.0));
        data.setValue(BaG_A11_1.toMeta(), coefficientMeta(528.0));
        data.setValue(BaB_A11_1.toMeta(), coefficientMeta(467.0));

        filter->processMeta(XI_1, data);
        QCOMPARE(data.value(BsB_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(BsG_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(BsR_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(BaB_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(BaG_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(BaR_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(BeB_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(BeG_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(BeR_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZSSAB_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZSSAG_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZSSAR_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZAngBsB_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZAngBsG_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZAngBsR_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZAngBaB_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZAngBaG_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZAngBaR_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZAngBeB_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZAngBeG_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZAngBeR_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZBfrB_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZBfrG_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZBfrR_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZGB_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZGG_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZGR_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZRFEB_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZRFEG_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZRFER_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(XB_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(XG_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(XR_XI_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QVERIFY(data.value(ZAngBsBG_XI_1.toMeta()).metadataReal("Format").exists());
        QVERIFY(data.value(ZAngBsGR_XI_1.toMeta()).metadataReal("Format").exists());
        QVERIFY(data.value(ZAngBsBR_XI_1.toMeta()).metadataReal("Format").exists());
        QVERIFY(data.value(ZAngBaBG_XI_1.toMeta()).metadataReal("Format").exists());
        QVERIFY(data.value(ZAngBaGR_XI_1.toMeta()).metadataReal("Format").exists());
        QVERIFY(data.value(ZAngBaBR_XI_1.toMeta()).metadataReal("Format").exists());


        data.setValue(BsG_S11_1, Variant::Root(10.0));
        data.setValue(BsB_S11_1, Variant::Root(20.0));
        data.setValue(BbsG_S11_1, Variant::Root(1.0));
        data.setValue(BbsB_S11_1, Variant::Root(2.0));
        data.setValue(BaG_A11_1, Variant::Root(4.0));
        data.setValue(BaB_A11_1, Variant::Root(8.0));
        data.setValue(N_N11_1, Variant::Root(100.0));
        filter->process(XI_1, data);

        double BsB = 20.0;
        double BsG = 10.0;
        double BsR = adjust(700, 550, 450, 10, 20);
        double BbsB = 2.0;
        double BbsG = 1.0;
        double BbsR = adjust(700, 550, 450, 1, 2);
        double BaB = adjust(450, 528, 467, 4, 8);
        double BaG = adjust(550, 528, 467, 4, 8);
        double BaR = adjust(700, 528, 467, 4, 8);

        QCOMPARE(data.value(BsB_XI_1).toDouble(), BsB);
        QCOMPARE(data.value(BsG_XI_1).toDouble(), BsG);
        QCOMPARE(data.value(BsR_XI_1).toDouble(), BsR);
        QCOMPARE(data.value(BaB_XI_1).toDouble(), BaB);
        QCOMPARE(data.value(BaG_XI_1).toDouble(), BaG);
        QCOMPARE(data.value(BaR_XI_1).toDouble(), BaR);
        QCOMPARE(data.value(BeB_XI_1).toDouble(), BsB + BaB);
        QCOMPARE(data.value(BeG_XI_1).toDouble(), BsG + BaG);
        QCOMPARE(data.value(BeR_XI_1).toDouble(), BsR + BaR);
        QCOMPARE(data.value(ZSSAB_XI_1).toDouble(), BsB / (BsB + BaB));
        QCOMPARE(data.value(ZSSAG_XI_1).toDouble(), BsG / (BsG + BaG));
        QCOMPARE(data.value(ZSSAR_XI_1).toDouble(), BsR / (BsR + BaR));
        QCOMPARE(data.value(ZAngBsB_XI_1).toDouble(), angstrom(550, 450, 10, 20));
        QCOMPARE(data.value(ZAngBsG_XI_1).toDouble(), angstrom(550, 450, 10, 20));
        QCOMPARE(data.value(ZAngBsR_XI_1).toDouble(), angstrom(550, 450, 10, 20));
        QCOMPARE(data.value(ZAngBaB_XI_1).toDouble(), angstrom(528, 467, 4, 8));
        QCOMPARE(data.value(ZAngBaG_XI_1).toDouble(), angstrom(528, 467, 4, 8));
        QCOMPARE(data.value(ZAngBaR_XI_1).toDouble(), angstrom(528, 467, 4, 8));
        QVERIFY(!FP::defined(data.value(ZAngBeB_XI_1).toReal()));
        QVERIFY(!FP::defined(data.value(ZAngBeG_XI_1).toReal()));
        QVERIFY(!FP::defined(data.value(ZAngBeR_XI_1).toReal()));
        QCOMPARE(data.value(ZBfrB_XI_1).toDouble(), BbsB / BsB);
        QCOMPARE(data.value(ZBfrG_XI_1).toDouble(), BbsG / BsG);
        QCOMPARE(data.value(ZBfrR_XI_1).toDouble(), BbsR / BsR);
        QCOMPARE(data.value(ZGB_XI_1).toDouble(), asymmetry(BbsB / BsB));
        QCOMPARE(data.value(ZGG_XI_1).toDouble(), asymmetry(BbsG / BsG));
        QCOMPARE(data.value(ZGR_XI_1).toDouble(), asymmetry(BbsR / BsR));
        QCOMPARE(data.value(ZRFEB_XI_1).toDouble(), rfe(BsB, BbsB, BaB));
        QCOMPARE(data.value(ZRFEG_XI_1).toDouble(), rfe(BsG, BbsG, BaG));
        QCOMPARE(data.value(ZRFER_XI_1).toDouble(), rfe(BsR, BbsR, BaR));
        QCOMPARE(data.value(XB_XI_1).toDouble(), ebc(BaB, 14625.0 / 450.0));
        QCOMPARE(data.value(XG_XI_1).toDouble(), ebc(BaG, 14625.0 / 550.0));
        QCOMPARE(data.value(XR_XI_1).toDouble(), ebc(BaR, 14625.0 / 700.0));
        QCOMPARE(data.value(N_XI_1).toDouble(), 100.0);
        QCOMPARE(data.value(ZAngBsBG_XI_1).toDouble(), angstrom(550, 450, 10, 20));
        QVERIFY(!FP::defined(data.value(ZAngBsGR_XI_1).toReal()));
        QVERIFY(!FP::defined(data.value(ZAngBsBR_XI_1).toReal()));
        QCOMPARE(data.value(ZAngBaBG_XI_1).toDouble(), angstrom(528, 467, 4, 8));
        QVERIFY(!FP::defined(data.value(ZAngBaGR_XI_1).toReal()));
        QVERIFY(!FP::defined(data.value(ZAngBaBR_XI_1).toReal()));


        data.setValue(BsG_S11_2.toMeta(), coefficientMeta(500.0));
        data.setValue(BsB_S11_2.toMeta(), coefficientMeta(400.0));
        data.setValue(BbsG_S11_2.toMeta(), coefficientMeta(500.0));
        data.setValue(BbsB_S11_2.toMeta(), coefficientMeta(400.0));
        data.setValue(BaG_A11_2.toMeta(), coefficientMeta(590.0));
        data.setValue(BaB_A11_2.toMeta(), coefficientMeta(420.0));

        filter->processMeta(XI_2, data);
        QCOMPARE(data.value(BsB_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(BsG_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(BsR_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(BaB_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(BaG_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(BaR_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(BeB_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(BeG_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(BeR_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZSSAB_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZSSAG_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZSSAR_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZAngBsB_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZAngBsG_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZAngBsR_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZAngBaB_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZAngBaG_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZAngBaR_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZAngBeB_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZAngBeG_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZAngBeR_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZBfrB_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZBfrG_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZBfrR_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZGB_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZGG_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZGR_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZRFEB_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZRFEG_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZRFER_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(XB_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(XG_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(XR_XI_2.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QVERIFY(data.value(ZAngBsBG_XI_2.toMeta()).metadataReal("Format").exists());
        QVERIFY(data.value(ZAngBsGR_XI_2.toMeta()).metadataReal("Format").exists());
        QVERIFY(data.value(ZAngBsBR_XI_2.toMeta()).metadataReal("Format").exists());
        QVERIFY(data.value(ZAngBaBG_XI_2.toMeta()).metadataReal("Format").exists());
        QVERIFY(data.value(ZAngBaGR_XI_2.toMeta()).metadataReal("Format").exists());
        QVERIFY(data.value(ZAngBaBR_XI_2.toMeta()).metadataReal("Format").exists());

        data.setValue(BsG_S11_2, Variant::Root(30.0));
        data.setValue(BsB_S11_2, Variant::Root(40.0));
        data.setValue(BbsG_S11_2, Variant::Root(5.0));
        data.setValue(BbsB_S11_2, Variant::Root(6.0));
        data.setValue(BaG_A11_2, Variant::Root(9.0));
        data.setValue(BaB_A11_2, Variant::Root(11.0));
        data.setValue(N_N11_2, Variant::Root(200.0));
        filter->process(XI_2, data);

        BsB = adjust(450, 500, 400, 30, 40);
        BsG = adjust(550, 500, 400, 30, 40);
        BsR = adjust(700, 500, 400, 30, 40);
        BbsB = adjust(450, 500, 400, 5, 6);
        BbsG = adjust(550, 500, 400, 5, 6);
        BbsR = adjust(700, 500, 400, 5, 6);
        BaB = adjust(450, 590, 420, 9, 11);
        BaG = adjust(550, 590, 420, 9, 11);
        BaR = adjust(700, 590, 420, 9, 11);

        QCOMPARE(data.value(BsB_XI_2).toDouble(), BsB);
        QCOMPARE(data.value(BsG_XI_2).toDouble(), BsG);
        QCOMPARE(data.value(BsR_XI_2).toDouble(), BsR);
        QCOMPARE(data.value(BaB_XI_2).toDouble(), BaB);
        QCOMPARE(data.value(BaG_XI_2).toDouble(), BaG);
        QCOMPARE(data.value(BaR_XI_2).toDouble(), BaR);
        QCOMPARE(data.value(BeB_XI_2).toDouble(), BsB + BaB);
        QCOMPARE(data.value(BeG_XI_2).toDouble(), BsG + BaG);
        QCOMPARE(data.value(BeR_XI_2).toDouble(), BsR + BaR);
        QCOMPARE(data.value(ZSSAB_XI_2).toDouble(), BsB / (BsB + BaB));
        QCOMPARE(data.value(ZSSAG_XI_2).toDouble(), BsG / (BsG + BaG));
        QCOMPARE(data.value(ZSSAR_XI_2).toDouble(), BsR / (BsR + BaR));
        QCOMPARE(data.value(ZAngBsB_XI_2).toDouble(), angstrom(500, 400, 30, 40));
        QCOMPARE(data.value(ZAngBsG_XI_2).toDouble(), angstrom(500, 400, 30, 40));
        QCOMPARE(data.value(ZAngBsR_XI_2).toDouble(), angstrom(500, 400, 30, 40));
        QCOMPARE(data.value(ZAngBaB_XI_2).toDouble(), angstrom(590, 420, 9, 11));
        QCOMPARE(data.value(ZAngBaG_XI_2).toDouble(), angstrom(590, 420, 9, 11));
        QCOMPARE(data.value(ZAngBaR_XI_2).toDouble(), angstrom(590, 420, 9, 11));
        QVERIFY(!FP::defined(data.value(ZAngBeB_XI_2).toReal()));
        QVERIFY(!FP::defined(data.value(ZAngBeG_XI_2).toReal()));
        QVERIFY(!FP::defined(data.value(ZAngBeR_XI_2).toReal()));
        QCOMPARE(data.value(ZBfrB_XI_2).toDouble(), BbsB / BsB);
        QCOMPARE(data.value(ZBfrG_XI_2).toDouble(), BbsG / BsG);
        QCOMPARE(data.value(ZBfrR_XI_2).toDouble(), BbsR / BsR);
        QCOMPARE(data.value(ZGB_XI_2).toDouble(), asymmetry(BbsB / BsB));
        QCOMPARE(data.value(ZGG_XI_2).toDouble(), asymmetry(BbsG / BsG));
        QCOMPARE(data.value(ZGR_XI_2).toDouble(), asymmetry(BbsR / BsR));
        QCOMPARE(data.value(ZRFEB_XI_2).toDouble(), rfe(BsB, BbsB, BaB));
        QCOMPARE(data.value(ZRFEG_XI_2).toDouble(), rfe(BsG, BbsG, BaG));
        QCOMPARE(data.value(ZRFER_XI_2).toDouble(), rfe(BsR, BbsR, BaR));
        QCOMPARE(data.value(XB_XI_2).toDouble(), ebc(BaB, 14625.0 / 450.0));
        QCOMPARE(data.value(XG_XI_2).toDouble(), ebc(BaG, 14625.0 / 550.0));
        QCOMPARE(data.value(XR_XI_2).toDouble(), ebc(BaR, 14625.0 / 700.0));
        QCOMPARE(data.value(N_XI_2).toDouble(), 200.0);
        QCOMPARE(data.value(ZAngBsBG_XI_2).toDouble(), angstrom(500, 400, 30, 40));
        QVERIFY(!FP::defined(data.value(ZAngBsGR_XI_2).toReal()));
        QVERIFY(!FP::defined(data.value(ZAngBsBR_XI_2).toReal()));
        QCOMPARE(data.value(ZAngBaBG_XI_2).toDouble(), angstrom(590, 420, 9, 11));
        QVERIFY(!FP::defined(data.value(ZAngBaGR_XI_2).toReal()));
        QVERIFY(!FP::defined(data.value(ZAngBaBR_XI_2).toReal()));

        {
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
            }
            QVERIFY(filter2 != NULL);

            SequenceSegment data2;
            data2.setStart(15.0);
            data2.setEnd(16.0);
            data2.setValue(BsG_S11_2, Variant::Root(30.0));
            data2.setValue(BsB_S11_2, Variant::Root(40.0));
            data2.setValue(BbsG_S11_2, Variant::Root(5.0));
            data2.setValue(BbsB_S11_2, Variant::Root(6.0));
            data2.setValue(BaG_A11_2, Variant::Root(9.0));
            data2.setValue(BaB_A11_2, Variant::Root(11.0));
            data2.setValue(N_N11_2, Variant::Root(100.0));

            filter2->process(XI_2, data2);
            QCOMPARE(data2.value(BsB_XI_2).toDouble(), BsB);
            QCOMPARE(data2.value(BsG_XI_2).toDouble(), BsG);
            QCOMPARE(data2.value(BsR_XI_2).toDouble(), BsR);
            QCOMPARE(data2.value(BaB_XI_2).toDouble(), BaB);
            QCOMPARE(data2.value(BaG_XI_2).toDouble(), BaG);
            QCOMPARE(data2.value(BaR_XI_2).toDouble(), BaR);
            QCOMPARE(data2.value(BeB_XI_2).toDouble(), BsB + BaB);
            QCOMPARE(data2.value(BeG_XI_2).toDouble(), BsG + BaG);
            QCOMPARE(data2.value(BeR_XI_2).toDouble(), BsR + BaR);
            QCOMPARE(data2.value(ZSSAB_XI_2).toDouble(), BsB / (BsB + BaB));
            QCOMPARE(data2.value(ZSSAG_XI_2).toDouble(), BsG / (BsG + BaG));
            QCOMPARE(data2.value(ZSSAR_XI_2).toDouble(), BsR / (BsR + BaR));
            QCOMPARE(data2.value(ZAngBsB_XI_2).toDouble(), angstrom(500, 400, 30, 40));
            QCOMPARE(data2.value(ZAngBsG_XI_2).toDouble(), angstrom(500, 400, 30, 40));
            QCOMPARE(data2.value(ZAngBsR_XI_2).toDouble(), angstrom(500, 400, 30, 40));
            QCOMPARE(data2.value(ZAngBaB_XI_2).toDouble(), angstrom(590, 420, 9, 11));
            QCOMPARE(data2.value(ZAngBaG_XI_2).toDouble(), angstrom(590, 420, 9, 11));
            QCOMPARE(data2.value(ZAngBaR_XI_2).toDouble(), angstrom(590, 420, 9, 11));
            QVERIFY(!FP::defined(data2.value(ZAngBeB_XI_2).toReal()));
            QVERIFY(!FP::defined(data2.value(ZAngBeG_XI_2).toReal()));
            QVERIFY(!FP::defined(data2.value(ZAngBeR_XI_2).toReal()));
            QCOMPARE(data2.value(ZBfrB_XI_2).toDouble(), BbsB / BsB);
            QCOMPARE(data2.value(ZBfrG_XI_2).toDouble(), BbsG / BsG);
            QCOMPARE(data2.value(ZBfrR_XI_2).toDouble(), BbsR / BsR);
            QCOMPARE(data2.value(ZGB_XI_2).toDouble(), asymmetry(BbsB / BsB));
            QCOMPARE(data2.value(ZGG_XI_2).toDouble(), asymmetry(BbsG / BsG));
            QCOMPARE(data2.value(ZGR_XI_2).toDouble(), asymmetry(BbsR / BsR));
            QCOMPARE(data2.value(ZRFEB_XI_2).toDouble(), rfe(BsB, BbsB, BaB));
            QCOMPARE(data2.value(ZRFEG_XI_2).toDouble(), rfe(BsG, BbsG, BaG));
            QCOMPARE(data2.value(ZRFER_XI_2).toDouble(), rfe(BsR, BbsR, BaR));
            QCOMPARE(data.value(XB_XI_2).toDouble(), ebc(BaB, 14625.0 / 450.0));
            QCOMPARE(data.value(XG_XI_2).toDouble(), ebc(BaG, 14625.0 / 550.0));
            QCOMPARE(data.value(XR_XI_2).toDouble(), ebc(BaR, 14625.0 / 700.0));
            QCOMPARE(data.value(N_XI_2).toDouble(), 200.0);
            QCOMPARE(data.value(ZAngBsBG_XI_2).toDouble(), angstrom(500, 400, 30, 40));
            QVERIFY(!FP::defined(data.value(ZAngBsGR_XI_2).toReal()));
            QVERIFY(!FP::defined(data.value(ZAngBsBR_XI_2).toReal()));
            QCOMPARE(data.value(ZAngBaBG_XI_2).toDouble(), angstrom(590, 420, 9, 11));
            QVERIFY(!FP::defined(data.value(ZAngBaGR_XI_2).toReal()));
            QVERIFY(!FP::defined(data.value(ZAngBaBR_XI_2).toReal()));
            delete filter2;
        }


        data.setValue(BsG_S12_1.toMeta(), coefficientMeta(550.0));
        data.setValue(BsB_S12_1.toMeta(), coefficientMeta(450.0));
        data.setValue(BbsG_S12_1.toMeta(), coefficientMeta(550.0));
        data.setValue(BbsB_S12_1.toMeta(), coefficientMeta(450.0));
        data.setValue(BaG_A12_1.toMeta(), coefficientMeta(528.0));
        data.setValue(BaB_A12_1.toMeta(), coefficientMeta(467.0));

        filter->processMeta(XJ_1, data);
        QCOMPARE(data.value(BsB_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(BsG_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(BsR_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(BaB_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(BaG_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(BaR_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(BeB_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(BeG_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(BeR_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZSSAB_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZSSAG_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZSSAR_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZAngBsB_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZAngBsG_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZAngBsR_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZAngBaB_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZAngBaG_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZAngBaR_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZAngBeB_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZAngBeG_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZAngBeR_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZBfrB_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZBfrG_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZBfrR_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZGB_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZGG_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZGR_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(ZRFEB_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(ZRFEG_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(ZRFER_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QCOMPARE(data.value(XB_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 450.0);
        QCOMPARE(data.value(XG_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 550.0);
        QCOMPARE(data.value(XR_XJ_1.toMeta()).metadataReal("Wavelength").toDouble(), 700.0);
        QVERIFY(data.value(ZAngBsBG_XJ_1.toMeta()).metadataReal("Format").exists());
        QVERIFY(data.value(ZAngBsGR_XJ_1.toMeta()).metadataReal("Format").exists());
        QVERIFY(data.value(ZAngBsBR_XJ_1.toMeta()).metadataReal("Format").exists());
        QVERIFY(data.value(ZAngBaBG_XJ_1.toMeta()).metadataReal("Format").exists());
        QVERIFY(data.value(ZAngBaGR_XJ_1.toMeta()).metadataReal("Format").exists());
        QVERIFY(data.value(ZAngBaBR_XJ_1.toMeta()).metadataReal("Format").exists());


        data.setValue(BsG_S12_1, Variant::Root(10.0));
        data.setValue(BsB_S12_1, Variant::Root(20.0));
        data.setValue(BbsG_S12_1, Variant::Root(1.0));
        data.setValue(BbsB_S12_1, Variant::Root(2.0));
        data.setValue(BaG_A12_1, Variant::Root(4.0));
        data.setValue(BaB_A12_1, Variant::Root(8.0));
        data.setValue(N_N12_1, Variant::Root(300.0));
        filter->process(XJ_1, data);

        BsB = 20.0;
        BsG = 10.0;
        BsR = adjust(700, 550, 450, 10, 20);
        BbsB = 2.0;
        BbsG = 1.0;
        BbsR = adjust(700, 550, 450, 1, 2);
        BaB = adjust(450, 528, 467, 4, 8);
        BaG = adjust(550, 528, 467, 4, 8);
        BaR = adjust(700, 528, 467, 4, 8);

        QCOMPARE(data.value(BsB_XJ_1).toDouble(), BsB);
        QCOMPARE(data.value(BsG_XJ_1).toDouble(), BsG);
        QCOMPARE(data.value(BsR_XJ_1).toDouble(), BsR);
        QCOMPARE(data.value(BaB_XJ_1).toDouble(), BaB);
        QCOMPARE(data.value(BaG_XJ_1).toDouble(), BaG);
        QCOMPARE(data.value(BaR_XJ_1).toDouble(), BaR);
        QCOMPARE(data.value(BeB_XJ_1).toDouble(), BsB + BaB);
        QCOMPARE(data.value(BeG_XJ_1).toDouble(), BsG + BaG);
        QCOMPARE(data.value(BeR_XJ_1).toDouble(), BsR + BaR);
        QCOMPARE(data.value(ZSSAB_XJ_1).toDouble(), BsB / (BsB + BaB));
        QCOMPARE(data.value(ZSSAG_XJ_1).toDouble(), BsG / (BsG + BaG));
        QCOMPARE(data.value(ZSSAR_XJ_1).toDouble(), BsR / (BsR + BaR));
        QCOMPARE(data.value(ZAngBsB_XJ_1).toDouble(), angstrom(550, 450, 10, 20));
        QCOMPARE(data.value(ZAngBsG_XJ_1).toDouble(), angstrom(550, 450, 10, 20));
        QCOMPARE(data.value(ZAngBsR_XJ_1).toDouble(), angstrom(550, 450, 10, 20));
        QCOMPARE(data.value(ZAngBaB_XJ_1).toDouble(), angstrom(528, 467, 4, 8));
        QCOMPARE(data.value(ZAngBaG_XJ_1).toDouble(), angstrom(528, 467, 4, 8));
        QCOMPARE(data.value(ZAngBaR_XJ_1).toDouble(), angstrom(528, 467, 4, 8));
        QVERIFY(!FP::defined(data.value(ZAngBeB_XJ_1).toReal()));
        QVERIFY(!FP::defined(data.value(ZAngBeG_XJ_1).toReal()));
        QVERIFY(!FP::defined(data.value(ZAngBeR_XJ_1).toReal()));
        QCOMPARE(data.value(ZBfrB_XJ_1).toDouble(), BbsB / BsB);
        QCOMPARE(data.value(ZBfrG_XJ_1).toDouble(), BbsG / BsG);
        QCOMPARE(data.value(ZBfrR_XJ_1).toDouble(), BbsR / BsR);
        QCOMPARE(data.value(ZGB_XJ_1).toDouble(), asymmetry(BbsB / BsB));
        QCOMPARE(data.value(ZGG_XJ_1).toDouble(), asymmetry(BbsG / BsG));
        QCOMPARE(data.value(ZGR_XJ_1).toDouble(), asymmetry(BbsR / BsR));
        QCOMPARE(data.value(ZRFEB_XJ_1).toDouble(), rfe(BsB, BbsB, BaB));
        QCOMPARE(data.value(ZRFEG_XJ_1).toDouble(), rfe(BsG, BbsG, BaG));
        QCOMPARE(data.value(ZRFER_XJ_1).toDouble(), rfe(BsR, BbsR, BaR));
        QCOMPARE(data.value(XB_XJ_1).toDouble(), ebc(BaB, 14625.0 / 450.0));
        QCOMPARE(data.value(XG_XJ_1).toDouble(), ebc(BaG, 14625.0 / 550.0));
        QCOMPARE(data.value(XR_XJ_1).toDouble(), ebc(BaR, 14625.0 / 700.0));
        QCOMPARE(data.value(N_XJ_1).toDouble(), 300.0);
        QCOMPARE(data.value(ZAngBsBG_XJ_1).toDouble(), angstrom(550, 450, 10, 20));
        QVERIFY(!FP::defined(data.value(ZAngBsGR_XJ_1).toReal()));
        QVERIFY(!FP::defined(data.value(ZAngBsBR_XJ_1).toReal()));
        QCOMPARE(data.value(ZAngBaBG_XJ_1).toDouble(), angstrom(528, 467, 4, 8));
        QVERIFY(!FP::defined(data.value(ZAngBaGR_XJ_1).toReal()));
        QVERIFY(!FP::defined(data.value(ZAngBaBR_XJ_1).toReal()));

        delete filter;
    }

    void dynamicOptions()
    {
        ComponentOptions options(component->getOptions());

        qobject_cast<ComponentOptionDoubleSet *>(options.get("wavelengths"))->add(530);
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("scattering"))->set("", "",
                                                                                       "Bs.*_S12");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("backscattering"))->set("", "",
                                                                                "Bbs.*_S12");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("absorption"))->set("", "",
                                                                                       "Ba.*_A12");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("extinction"))->set("", "",
                                                                                       "Be.*_E12");

        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "BsG_S11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BbsG_S11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BaG_A11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BeG_E11"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName BsG_S12("brw", "raw", "BsG_S12");
        SequenceName BbsG_S12("brw", "raw", "BbsG_S12");
        SequenceName BaG_A12("brw", "raw", "BaG_A12");
        SequenceName BeG_E12("brw", "raw", "BeG_E12");
        filter->unhandled(BsG_S12, &controller);
        filter->unhandled(BbsG_S12, &controller);
        filter->unhandled(BaG_A12, &controller);
        filter->unhandled(BeG_E12, &controller);

        SequenceName BsG_XI("brw", "raw", "BsG_XI");
        SequenceName BaG_XI("brw", "raw", "BaG_XI");
        SequenceName BeG_XI("brw", "raw", "BeG_XI");
        SequenceName ZSSAG_XI("brw", "raw", "ZSSAG_XI");
        SequenceName ZAngBsG_XI("brw", "raw", "ZAngBsG_XI");
        SequenceName ZAngBaG_XI("brw", "raw", "ZAngBaG_XI");
        SequenceName ZAngBeG_XI("brw", "raw", "ZAngBeG_XI");
        SequenceName ZBfrG_XI("brw", "raw", "ZBfrG_XI");
        SequenceName ZGG_XI("brw", "raw", "ZGG_XI");
        SequenceName ZRFEG_XI("brw", "raw", "ZRFEG_XI");
        SequenceName XG_XI("brw", "raw", "XG_XI");

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << BsG_XI << BaG_XI << BeG_XI << ZSSAG_XI
                                                     << ZAngBsG_XI << ZAngBaG_XI << ZAngBeG_XI
                                                     << ZBfrG_XI << ZGG_XI << ZRFEG_XI << XG_XI,
                                QSet<SequenceName>() << BsG_S12 << BbsG_S12 << BaG_A12
                                                     << BeG_E12));
        QCOMPARE(idL.size(), 1);
        int id = idL.at(0);

        data.setValue(BsG_S12.toMeta(), coefficientMeta(530.0));
        data.setValue(BbsG_S12.toMeta(), coefficientMeta(530.0));
        data.setValue(BaG_A12.toMeta(), coefficientMeta(530.0));
        data.setValue(BeG_E12.toMeta(), coefficientMeta(530.0));
        filter->processMeta(id, data);
        QCOMPARE(data.value(BsG_XI.toMeta()).metadataReal("Wavelength").toDouble(), 530.0);

        data.setValue(BsG_S12, Variant::Root(10.0));
        data.setValue(BeG_E12, Variant::Root(15.0));

        filter->process(id, data);
        QCOMPARE(data.value(BaG_XI).toDouble(), 5.0);

        delete filter;


        options = component->getOptions();

        qobject_cast<ComponentOptionDoubleSet *>(options.get("wavelengths"))->add(530);
        qobject_cast<DynamicInputOption *>(options.get("bfr"))->set(0.25);
        qobject_cast<DynamicInputOption *>(options.get("mac"))->set(12345.0);
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("scattering-suffix"))->add(
                "S12");
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("absorption-suffix"))->add(
                "A12");
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("extinction-suffix"))->add(
                "E12");
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("counts-suffix"))->add(
                "N12");
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("output-suffix"))->add("XJ");

        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        filter->unhandled(SequenceName("brw", "raw", "BsG_S11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BbsG_S11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BaG_A11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BeG_E11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "N_N11"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName N_N12("brw", "raw", "N_N12");
        filter->unhandled(BsG_S12, &controller);
        filter->unhandled(BbsG_S12, &controller);
        filter->unhandled(BaG_A12, &controller);
        filter->unhandled(BeG_E12, &controller);
        filter->unhandled(N_N12, &controller);

        SequenceName BsG_XJ("brw", "raw", "BsG_XJ");
        SequenceName BaG_XJ("brw", "raw", "BaG_XJ");
        SequenceName BeG_XJ("brw", "raw", "BeG_XJ");
        SequenceName ZSSAG_XJ("brw", "raw", "ZSSAG_XJ");
        SequenceName ZAngBsG_XJ("brw", "raw", "ZAngBsG_XJ");
        SequenceName ZAngBaG_XJ("brw", "raw", "ZAngBaG_XJ");
        SequenceName ZAngBeG_XJ("brw", "raw", "ZAngBeG_XJ");
        SequenceName ZBfrG_XJ("brw", "raw", "ZBfrG_XJ");
        SequenceName ZGG_XJ("brw", "raw", "ZGG_XJ");
        SequenceName ZRFEG_XJ("brw", "raw", "ZRFEG_XJ");
        SequenceName XG_XJ("brw", "raw", "XG_XJ");
        SequenceName N_XJ("brw", "raw", "N_XJ");

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << BsG_XJ << BaG_XJ << BeG_XJ << ZSSAG_XJ
                                                     << ZAngBsG_XJ << ZAngBaG_XJ << ZAngBeG_XJ
                                                     << ZBfrG_XJ << ZGG_XJ << ZRFEG_XJ << XG_XJ
                                                     << N_XJ,
                                QSet<SequenceName>() << BsG_S12 << BbsG_S12 << BaG_A12 << BeG_E12
                                                     << N_N12));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        data = SequenceSegment();
        data.setValue(BsG_S12.toMeta(), coefficientMeta(530.0));
        data.setValue(BbsG_S12.toMeta(), coefficientMeta(530.0));
        data.setValue(BaG_A12.toMeta(), coefficientMeta(530.0));
        data.setValue(BeG_E12.toMeta(), coefficientMeta(530.0));
        filter->processMeta(id, data);
        QCOMPARE(data.value(BsG_XJ.toMeta()).metadataReal("Wavelength").toDouble(), 530.0);

        data.setValue(BaG_A12, Variant::Root(10.0));
        data.setValue(BeG_E12, Variant::Root(14.0));

        filter->process(id, data);
        QCOMPARE(data.value(BsG_XJ).toDouble(), 4.0);
        QCOMPARE(data.value(ZBfrG_XJ).toDouble(), 0.25);

        delete filter;
    }

    void predefined()
    {
        ComponentOptions options(component->getOptions());

        SequenceName BsG_S11("brw", "raw", "BsG_S11");
        SequenceName BsB_S11("brw", "raw", "BsB_S11");
        SequenceName BbsG_S11("brw", "raw", "BbsG_S11");
        SequenceName BbsB_S11("brw", "raw", "BbsB_S11");
        SequenceName BaG_A11("brw", "raw", "BaG_A11");
        SequenceName BaB_A11("brw", "raw", "BaB_A11");
        SequenceName N_N11("brw", "raw", "N_N11");
        QList<SequenceName> input;
        input << BsG_S11 << BsB_S11 << BbsG_S11 << BbsB_S11 << BaG_A11 << BaB_A11 << N_N11;

        SegmentProcessingStage *filter =
                component->createBasicFilterPredefined(options, FP::undefined(), FP::undefined(),
                                                       input);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;

        QCOMPARE(filter->requestedInputs(),
                 (SequenceName::Set{BsG_S11, BsB_S11, BbsG_S11, BbsB_S11, BaG_A11, BaB_A11,
                                    N_N11}));

        delete filter;
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Main/BackScattering/Input"] = "::Bbs[BGR]_S11:=";
        cv["Main/Absorption/Input"] = "::Ba[BGR]_A11:=";
        cv["Main/Optical/Blue/Wavelength"] = 450.0;
        cv["Main/Optical/Blue/Scattering"] = "::BsB_XI:=";
        cv["Main/Optical/Blue/Extinction"] = "::BeB_XI:=";
        cv["Main/Optical/Blue/Override/BackScatterFraction"] = 0.1;

        config.emplace_back(FP::undefined(), 13.0, cv);
        cv["Main/Optical/Blue/Override/BackScatterFraction"] = 0.2;
        cv["Main/Optical/Blue/Extinction"] = "::BeBq_XI:=";
        config.emplace_back(13.0, FP::undefined(), cv);

        SegmentProcessingStage
                *filter = component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config);
        QVERIFY(filter != NULL);
        TestController controller;

        SequenceName BbsB_S11("brw", "raw", "BbsB_S11");
        SequenceName BaB_A11("brw", "raw", "BaB_A11");
        SequenceName BsB_XI("brw", "raw", "BsB_XI");
        SequenceName BeB_XI("brw", "raw", "BeB_XI");
        SequenceName BeBq_XI("brw", "raw", "BeBq_XI");

        filter->unhandled(BbsB_S11, &controller);
        filter->unhandled(BaB_A11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        QList<int> idL = controller.contents
                                   .keys(TestController::ID(
                                           QSet<SequenceName>() << BsB_XI << BeB_XI << BeBq_XI,
                                           QSet<SequenceName>() << BbsB_S11 << BaB_A11));
        QCOMPARE(idL.size(), 1);
        int XI = idL.at(0);

        QCOMPARE(filter->metadataBreaks(XI), QSet<double>() << 13.0);

        SequenceSegment data;
        data.setStart(12.0);
        data.setEnd(13.0);

        data.setValue(BbsB_S11.toMeta(), coefficientMeta(450.0));
        data.setValue(BaB_A11.toMeta(), coefficientMeta(450.0));
        filter->processMeta(XI, data);

        data.setValue(BbsB_S11, Variant::Root(1.0));
        data.setValue(BaB_A11, Variant::Root(5.0));

        filter->process(XI, data);
        QCOMPARE(data.value(BsB_XI).toDouble(), 10.0);
        QCOMPARE(data.value(BeB_XI).toDouble(), 15.0);


        data = SequenceSegment();
        data.setStart(13.0);
        data.setEnd(14.0);

        data.setValue(BbsB_S11.toMeta(), coefficientMeta(450.0));
        data.setValue(BaB_A11.toMeta(), coefficientMeta(450.0));
        filter->processMeta(XI, data);

        data.setValue(BbsB_S11, Variant::Root(3.0));
        data.setValue(BaB_A11, Variant::Root(6.0));

        filter->process(XI, data);
        QCOMPARE(data.value(BsB_XI).toDouble(), 15.0);
        QCOMPARE(data.value(BeBq_XI).toDouble(), 21.0);

        delete filter;
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
