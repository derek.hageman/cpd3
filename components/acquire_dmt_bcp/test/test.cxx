/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;

    std::uint_fast16_t housekeeping[8];
    std::uint_fast16_t averageTransit;
    std::uint_fast16_t dtBandwidth;
    std::uint_fast16_t dt;
    std::uint_fast32_t adcOverflow;
    std::uint_fast32_t counts[10];

    ModelInstrument() : incoming(), outgoing()
    {
        housekeeping[0] = 200;
        housekeeping[1] = 300;
        housekeeping[2] = 400;
        housekeeping[3] = 2000;
        housekeeping[4] = 3000;
        housekeeping[5] = 500;
        housekeeping[6] = 600;
        housekeeping[7] = 700;

        averageTransit = 50;
        dtBandwidth = 10;
        dt = 30;
        adcOverflow = 0;

        for (std::size_t i = 0; i < 10; i++) {
            counts[i] = 1000 * i + 100;
        }
    }

    void writeDataPacket()
    {
        Util::ByteArray output;
        for (std::size_t i = 0; i < 8; i++) {
            output.appendNumber<quint16>(housekeeping[i]);
        }
        output.appendNumber<quint32>(0);
        output.appendNumber<quint32>(0);
        output.appendNumber<quint16>(averageTransit);
        output.appendNumber<quint16>(dtBandwidth);
        output.appendNumber<quint16>(dt);

        output.appendNumber<quint32>(adcOverflow);
        for (std::size_t i = 0; i < 10; i++) {
            output.appendNumber<quint32>(counts[i]);
        }

        std::uint_fast16_t csum = 0;
        for (auto add : output) {
            csum = (csum + static_cast<std::uint_fast16_t>(add)) & 0xFFFF;
        }
        output.appendNumber<quint16>(csum);

        outgoing.append(output.toQByteArray());
    }

    std::size_t parseFrame(const Util::ByteView &frame)
    {
        if (frame.size() < 3)
            return 0;

        std::size_t offset = 1;
        switch (frame.front()) {
        case 0x01:
            offset = 99;
            break;
        case 0x02:
            break;
        default:
            return static_cast<std::size_t>(-1);
        }

        if (offset + 2 > frame.size())
            return 0;

        std::uint_fast16_t csum = 0x1B;
        for (std::size_t i = 0; i < offset; i++) {
            csum = (csum + static_cast<std::uint_fast16_t>(frame[i])) & 0xFFFF;
        }
        if (frame.mid(offset).readNumber<quint16>() != csum)
            return static_cast<std::size_t>(-1);

        switch (frame.front()) {
        case 0x01:
            outgoing.append(0x06);
            outgoing.append(0x06);
            outgoing.append(0x12);
            outgoing.append(0x34);
            break;
        case 0x02:
            writeDataPacket();
            break;
        default:
            return static_cast<std::size_t>(-1);
        }

        return offset + 2;
    }

    void advance(double seconds)
    {

        int idxEscape = -1;
        while ((idxEscape = incoming.indexOf((char) 0x1B)) >= 0) {
            Util::ByteView frame(incoming);
            auto consumed = parseFrame(frame.mid(idxEscape + 1));
            if (!consumed)
                break;
            if (consumed == static_cast<std::size_t>(-1))
                consumed = 0;

            incoming = incoming.mid(idxEscape + consumed + 1);
        }
    }

    double binConcentration(std::size_t idx, double Qsample = 1.0) const
    {
        return counts[idx] / (Qsample * (1000.0 / 60.0));
    }

    Variant::Root allBinsCounts() const
    {
        Variant::Root result;
        double sum = 0.0;
        for (std::size_t i = 0; i < 10; i++) {
            sum += counts[i];
            result.write().array(i).setDouble(sum);
        }
        return result;
    }

    Variant::Root allBinsConcentrations(double Qsample = 1.0) const
    {
        Variant::Root result;
        for (std::size_t i = 0; i < 10; i++) {
            result.write().array(i).setDouble(binConcentration(i, Qsample));
        }
        return result;
    }

    double totalCount() const
    {
        double sum = 0.0;
        for (std::size_t i = 0; i < 10; i++) {
            sum += counts[i];
        }
        return sum;
    }

    double totalConcentration(double Qsample = 1.0) const
    {
        double sum = 0.0;
        for (std::size_t i = 0; i < 10; i++) {
            sum += binConcentration(i, Qsample);
        }
        return sum;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined(), bool checkSizes = true)
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("N", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Nb", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZINPUTS", Variant::Root(), QString(), time))
            return false;
        if (checkSizes && !stream.hasMeta("Ns", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("C", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Cb", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZTransit", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZBandwidth", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZDT", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZOverflow", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream, bool checkSizes = true)
    {
        if (!stream.checkContiguous("N"))
            return false;
        if (!stream.checkContiguous("Nb"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("V1"))
            return false;
        if (!stream.checkContiguous("V2"))
            return false;
        if (!stream.checkContiguous("ZINPUTS"))
            return false;
        if (checkSizes && !stream.checkContiguous("Ns"))
            return false;
        return true;
    }

    static double toVoltage(std::uint_fast16_t adc)
    {
        return static_cast<double>(adc) * 5.0 / 4096.0;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined(),
                     bool checkSizes = true)
    {
        if (!stream.hasAnyMatchingValue("N", Variant::Root(model.totalConcentration()), time,
                                        QString()))
            return false;
        if (!stream.hasAnyMatchingValue("Nb", model.allBinsConcentrations(), time, QString()))
            return false;
        {
            double T = (1.0 /
                    ((1.0 / 3900.0) *
                            std::log((4096.0 / model.housekeeping[3] - 1.0) + (1.0 / 298.0))) -
                    273.0);
            if (!stream.hasAnyMatchingValue("T1", Variant::Root(T), time))
                return false;
        }
        {
            double T = (model.housekeeping[4] - 819) * 0.06104;
            if (!stream.hasAnyMatchingValue("T2", Variant::Root(T), time))
                return false;
        }
        if (!stream.hasAnyMatchingValue("V1", Variant::Root(toVoltage(model.housekeeping[0])),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("V2", Variant::Root(toVoltage(model.housekeeping[1])),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        {
            Variant::Root ZINPUTS(Variant::Type::Array);
            ZINPUTS.write().array(0).setReal(toVoltage(model.housekeeping[2]));
            ZINPUTS.write().array(1).setReal(toVoltage(model.housekeeping[5]));
            ZINPUTS.write().array(2).setReal(toVoltage(model.housekeeping[6]));
            ZINPUTS.write().array(3).setReal(toVoltage(model.housekeeping[7]));
            if (!stream.hasAnyMatchingValue("ZINPUTS", ZINPUTS, time))
                return false;
        }
        if (checkSizes && !stream.hasAnyMatchingValue("Ns", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("C", Variant::Root(model.totalCount()), time))
            return false;
        if (!stream.hasAnyMatchingValue("Cb", model.allBinsCounts(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZTransit",
                                        Variant::Root(static_cast<double>(model.averageTransit)),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBandwidth",
                                        Variant::Root(static_cast<double>(model.dtBandwidth)),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("ZDT", Variant::Root(static_cast<double>(model.dt)), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZOverflow",
                                        Variant::Root(static_cast<double>(model.adcOverflow)),
                                        time))
            return false;
        return true;
    }

    static QByteArray makeCommand(const QByteArray &data)
    {
        QByteArray result;
        result.append((char) 0x1B);
        result.append(data);
        std::uint_fast16_t sum = 0;
        for (auto add : result) {
            sum = (sum + static_cast<std::uint_fast16_t>(add)) & 0xFFFF;
        }
        auto offset = result.size();
        result.resize(offset + 2);
        qToLittleEndian<quint16>(sum, reinterpret_cast<uchar *>(result.data() + offset));
        return result;
    }

private slots:

    void initTestCase()
    {
        component =
                qobject_cast<AcquisitionComponent *>(ComponentLoader::create("acquire_dmt_bcp"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_dmt_bcp"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["ConstantTime"] = 1.0;
        for (std::size_t i = 0; i < 10; i++) {
            cv["Diameter"].array(i).setDouble(i + 1);
        }
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        while (control.time() < 10.0) {
            control.externalControl(makeCommand("\x02"));
            control.advance(0.2);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 10.0);

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(0.2);
            control.externalControl(makeCommand("\x02"));
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["ConstantTime"] = 1.0;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 20; i++) {
            control.advance(0.1);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging, false));

        QVERIFY(checkMeta(logging, FP::undefined(), false));
        QVERIFY(checkMeta(realtime, FP::undefined(), false));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, FP::undefined(), false));
        QVERIFY(checkValues(realtime, instrument, FP::undefined(), false));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["ConstantTime"] = 1.0;
        for (std::size_t i = 0; i < 10; i++) {
            cv["Diameter"].array(i).setDouble(i + 1);
        }
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.externalControl(makeCommand("\x02"));
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        control.advance(0.1);

        for (int i = 0; i < 20; i++) {
            control.externalControl(makeCommand("\x02"));
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(logging.hasAnyMatchingValue("F1"));

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["ConstantTime"] = 1.0;
        for (std::size_t i = 0; i < 10; i++) {
            cv["Diameter"].array(i).setDouble(i + 1);
        }
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 100; i++) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
