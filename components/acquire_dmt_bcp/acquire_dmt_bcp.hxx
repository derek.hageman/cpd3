/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREDMTBCP_H
#define ACQUIREDMTBCP_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class AcquireDMTBCP : public CPD3::Acquisition::FramedInstrument {
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum {
        /* Passive autoprobe initializing, ignoring the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,
        /* Acquiring data in interactive mode, reading the response to a
         * query. */
        RESP_INTERACTIVE_RUN,
        /* Acquiring data in interactive mode, sleeping until the next
         * time to query. */
        RESP_INTERACTIVE_RUN_WAIT,

        /* Waiting for the completion of set parameters command. */
        RESP_INTERACTIVE_START_SET_PARAMETERS,
        /* Discarding the first report, after starting communications */
        RESP_INTERACTIVE_START_DISCARDFIRST,
        /* Waiting for the first valid record */
        RESP_INTERACTIVE_START_FIRSTVALID,
        /* Waiting for a retry during initial report autoprobing */
        RESP_INTERACTIVE_START_FIRSTVALID_WAIT,

        /* Waiting before attempting a communications restart */
        RESP_INTERACTIVE_RESTART_WAIT,

        /* Initial state for interactive start */
        RESP_INTERACTIVE_INITIALIZE,
    } responseState;
    int autoprobeValidRecords;

    enum CommandType {
        /* Set parameters */
        COMMAND_PARAMETERS = 0x01,

        /* Read data */
        COMMAND_DATA = 0x02,

        /* A specifically invalid command */
        COMMAND_INVALID = 0x100,
    };

    class Command {
        CommandType type;
        CPD3::Util::ByteArray packet;
    public:
        Command();

        Command(CommandType t, CPD3::Util::ByteArray p = {});

        Command(const Command &);

        Command &operator=(const Command &);

        Command(Command &&);

        Command &operator=(Command &&);

        inline CommandType getType() const
        { return type; }

        inline const CPD3::Util::ByteArray &getPacket() const
        { return packet; }

        CPD3::Data::Variant::Root stateDescription() const;
    };

    std::deque<Command> commandQueue;

    class Configuration {
        double start;
        double end;

    public:
        std::vector<double> binDiameter;
        double flow;
        double pollInterval;
        double constantTime;
        CPD3::Data::Variant::Root parameters;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under,
                      const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    CPD3::Data::Variant::Root instrumentMeta;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    double lastRecordTime;

    void setDefaultInvalid();

    void describeState(CPD3::Data::Variant::Write &info) const;

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    int processRecord(const CPD3::Util::ByteView &line, double frameTime);

    void queueCommand(CommandType command, const CPD3::Util::ByteView &payload = {});

public:
    AcquireDMTBCP(const CPD3::Data::ValueSegment::Transfer &config,
                  const std::string &loggingContext);

    AcquireDMTBCP(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireDMTBCP();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus() override;

    CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables() override;

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    std::size_t dataFrameStart(std::size_t offset,
                               const CPD3::Util::ByteArray &input) const override;

    std::size_t dataFrameEnd(std::size_t start, const CPD3::Util::ByteArray &input) const override;

    std::size_t controlFrameStart(std::size_t offset,
                                  const CPD3::Util::ByteArray &input) const override;

    std::size_t controlFrameEnd(std::size_t start,
                                const CPD3::Util::ByteArray &input) const override;

    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;
};

class AcquireDMTBCPComponent
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_dmt_bcp"
                              FILE
                              "acquire_dmt_bcp.json")

    CPD3::ComponentOptions getBaseOptions();

public:

    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
