/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cmath>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "core/util.hxx"
#include "datacore/stream.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_dmt_bcp.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

AcquireDMTBCP::Configuration::Configuration() : start(FP::undefined()),
                                                end(FP::undefined()), binDiameter(),
                                                flow(1.0),
                                                pollInterval(0.5),
                                                constantTime(FP::undefined()),
                                                parameters()
{ }

AcquireDMTBCP::Configuration::Configuration(const Configuration &other, double s, double e) : start(
        s),
                                                                                              end(e),
                                                                                              binDiameter(
                                                                                                      other.binDiameter),
                                                                                              flow(other.flow),
                                                                                              pollInterval(
                                                                                                      other.pollInterval),
                                                                                              constantTime(
                                                                                                      other.constantTime),
                                                                                              parameters(
                                                                                                      other.parameters)
{ }

void AcquireDMTBCP::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("DMT");
    instrumentMeta["Model"].setString("BCP");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    lastRecordTime = FP::undefined();
}

AcquireDMTBCP::Configuration::Configuration(const ValueSegment &other, double s, double e) : start(
        s),
                                                                                             end(e),
                                                                                             binDiameter(),
                                                                                             flow(1.0),
                                                                                             pollInterval(
                                                                                                     0.5),
                                                                                             constantTime(
                                                                                                     FP::undefined()),
                                                                                             parameters()
{
    setFromSegment(other);
}

AcquireDMTBCP::Configuration::Configuration(const Configuration &under,
                                            const ValueSegment &over,
                                            double s,
                                            double e) : start(s),
                                                        end(e), binDiameter(under.binDiameter),
                                                        flow(under.flow),
                                                        pollInterval(under.pollInterval),
                                                        constantTime(under.constantTime),
                                                        parameters(under.parameters)
{
    setFromSegment(over);
}

void AcquireDMTBCP::Configuration::setFromSegment(const ValueSegment &config)
{
    double d = config["Flow"].toDouble();
    if (FP::defined(d) && d > 0.0)
        flow = d;

    if (FP::defined(config["PollInterval"].toDouble()))
        pollInterval = config["PollInterval"].toDouble();

    if (config["ConstantTime"].exists())
        constantTime = config["ConstantTime"].toDouble();

    if (config["Diameter"].exists()) {
        binDiameter.clear();
        for (auto add : config["Diameter"].toArray()) {
            binDiameter.push_back(add.toDouble());
        }
    }

    parameters = Variant::Root::overlay(parameters, Variant::Root(config["Hardware"]));
}

AcquireDMTBCP::AcquireDMTBCP(const ValueSegment::Transfer &configData,
                             const std::string &loggingContext) : FramedInstrument("dmtbcp",
                                                                                   loggingContext),
                                                                  autoprobeStatus(
                                                                          AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                  responseState(RESP_PASSIVE_WAIT),
                                                                  autoprobeValidRecords(0),
                                                                  commandQueue()
{
    setDefaultInvalid();
    config.append(Configuration());

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireDMTBCP::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void AcquireDMTBCP::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireDMTBCPComponent::getBaseOptions()
{
    ComponentOptions options;

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(tr("q", "name"), tr("Flow rate"),
                                            tr("This is the flow rate of the CPC at lpm measured at ambient "
                                               "conditions."),
                                            tr("1.4210 lpm", "default flow rate"), 1);
    d->setMinimum(0.0, false);
    options.add("q", d);

    return options;
}

AcquireDMTBCP::AcquireDMTBCP(const ComponentOptions &options, const std::string &loggingContext)
        : FramedInstrument("dmtbcp", loggingContext),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          responseState(RESP_PASSIVE_WAIT),
          autoprobeValidRecords(0),
          commandQueue()
{
    setDefaultInvalid();

    Configuration base;

    if (options.isSet("q")) {
        double value = qobject_cast<ComponentOptionSingleDouble *>(options.get("q"))->get();
        if (FP::defined(value) && value > 0.0)
            base.flow = value;
    }

    config.append(base);
}

AcquireDMTBCP::~AcquireDMTBCP() = default;

void AcquireDMTBCP::logValue(double startTime,
                             double endTime,
                             SequenceName::Component name,
                             Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress) return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireDMTBCP::realtimeValue(double time, SequenceName::Component name, Variant::Root &&value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value), time,
                                time + 1.0);
}

SequenceValue::Transfer AcquireDMTBCP::buildLogMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_dmt_bcp");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);

    result.emplace_back(SequenceName({}, "raw_meta", "N"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Condensation nuclei concentration");
    if (FP::defined(config.first().flow)) {
        result.back().write().metadataReal("SampleFlow").setDouble(config.first().flow);
    }
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Optics block temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Optics"));

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Electronics temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Electronics"));

    result.emplace_back(SequenceName({}, "raw_meta", "V1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("First stage monitor");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("1st stage monitor"));

    result.emplace_back(SequenceName({}, "raw_meta", "V2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Baseline monitor");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Basline monitor"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZINPUTS"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back()
          .write()
          .metadataArray("Description")
          .setString("Raw input values from all analog channels");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Count").setInt64(4);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("0.000");
    result.back().write().metadataArray("Children").metadataReal("Units").setString("V");

    result.emplace_back(SequenceName({}, "raw_meta", "Nb"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataArray("Count").setInt64(10);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("00000.0");
    result.back()
          .write()
          .metadataArray("Children")
          .metadataReal("Units")
          .setString("cm\xE2\x81\xBB³");
    result.back().write().metadataArray("Description").setString("Bin concentration");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataArray("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));
    result.back().write().metadataArray("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInt64(1);


    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);


    if (!config.first().binDiameter.empty()) {
        result.emplace_back(SequenceName({}, "raw_meta", "Ns"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataArray("Count").setInt64(10);
        result.back().write().metadataArray("Children").metadataReal("Format").setString("00.00");
        result.back().write().metadataArray("Units").setString("\xCE\xBCm");
        result.back().write().metadataArray("Description").setString("Bin center diameter");
        result.back().write().metadataArray("Source").set(instrumentMeta);
        result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
        result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Name")
              .setString(QObject::tr("Diameter"));
        result.back().write().metadataArray("Realtime").hash("RowOrder").setInt64(0);
        result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInt64(1);
    }

    return result;
}

SequenceValue::Transfer AcquireDMTBCP::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_dmt_bcp");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);

    result.emplace_back(SequenceName({}, "raw_meta", "C"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Count rate"));

    result.emplace_back(SequenceName({}, "raw_meta", "Vx1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Housekeeping channel 3");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Aux 1"));

    result.emplace_back(SequenceName({}, "raw_meta", "Vx2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Housekeeping channel 6");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Aux 2"));

    result.emplace_back(SequenceName({}, "raw_meta", "Vx3"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Housekeeping channel 7");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Aux 3"));

    result.emplace_back(SequenceName({}, "raw_meta", "Vx4"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Housekeeping channel 8");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Aux 4"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZTransit"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back().write().metadataReal("Description").setString("Raw count average transit");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Avg Transit"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZBandwidth"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Baseline ADC dynamic threshold bandwidth");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("DT bandwidth"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZDT"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Baseline ADC dynamic threshold end count");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("DT end"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZOverflow"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back().write().metadataReal("Description").setString("ADC overflow count");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("ADC overflow"));

    result.emplace_back(SequenceName({}, "raw_meta", "Cb"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataArray("Count").setInt64(10);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("000000");
    result.back().write().metadataArray("Children").metadataReal("Units").setString("Hz");
    result.back().write().metadataArray("Description").setString("Bin counts");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataArray("Realtime")
          .hash("Name")
          .setString(QObject::tr("Cumulative"));
    result.back().write().metadataArray("Realtime").hash("RowOrder").setInt64(2);
    result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataString("Realtime").hash("Hide").setBool(true);
    /*result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetParameters")
          .setString(QObject::tr("STARTING COMMS: Setting instrument parameters"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveDiscardFirst")
          .setString(QObject::tr("STARTING COMMS: Flushing initial record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveStartReadFirstRecord")
          .setString(QObject::tr("STARTING COMMS: Waiting for first record"));*/

    return result;
}

SequenceMatch::Composite AcquireDMTBCP::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    return sel;
}

static std::uint16_t calculateChecksum(const Util::ByteView &input, bool impliedEscape = true)
{
    std::uint16_t csum = 0;
    if (impliedEscape)
        csum += 0x1B;

    for (auto add : input) {
        csum = static_cast<std::uint16_t>((csum + static_cast<std::uint8_t>(add)) & 0xFFFF);
    }
    return csum;
}

template<typename Integer = quint16>
Integer readAndAdvance(Util::ByteView &input, bool &ok)
{
    if (input.size() < sizeof(Integer)) {
        ok = false;
        return 0;
    }
    ok = true;
    auto result = input.readNumber<Integer>();
    input = input.mid(sizeof(Integer));
    return result;
}

static double convertVoltage(std::uint16_t input)
{
    if (input >= 4096)
        return FP::undefined();
    return (static_cast<double>(input) * 5.0) / 4096.0;
}

static double calculateRate(std::uint_fast64_t total, double dT)
{
    if (!FP::defined(dT))
        return FP::undefined();
    if (dT <= 0.0)
        return FP::undefined();
    return static_cast<double>(total) / dT;
}

static double calculateConcentration(double counts, double flow)
{
    if (!FP::defined(counts) || !FP::defined(flow))
        return FP::undefined();
    if (flow <= 0.0)
        return FP::undefined();
    return counts / (flow * (1000.0 / 60.0));
}


int AcquireDMTBCP::processRecord(const Util::ByteView &line, double frameTime)
{
    Q_ASSERT(!config.isEmpty());

    if (commandQueue.empty())
        return -1;

    if (commandQueue.front().getType() != COMMAND_DATA)
        return -1;

    double endTime = frameTime;
    double startTime;
    if (!FP::defined(lastRecordTime)) {
        startTime = FP::undefined();
        lastRecordTime = frameTime;
    } else {
        startTime = lastRecordTime;
        if (lastRecordTime != frameTime)
            lastRecordTime = frameTime;
    }

    double dT = FP::undefined();
    if (FP::defined(config.front().constantTime)) {
        dT = config.front().constantTime;
    } else if (FP::defined(startTime) && FP::defined(endTime)) {
        dT = endTime - startTime;
    }

    Util::ByteView input(line);
    bool ok = false;

    auto u16 = readAndAdvance(input, ok);
    if (!ok) return 10;
    Variant::Root V1(convertVoltage(u16));
    if (!FP::defined(V1.read().toReal())) return 11;
    remap("V1", V1);

    u16 = readAndAdvance(input, ok);
    if (!ok) return 12;
    Variant::Root V2(convertVoltage(u16));
    if (!FP::defined(V2.read().toReal())) return 13;
    remap("V2", V2);

    u16 = readAndAdvance(input, ok);
    if (!ok) return 14;
    Variant::Root Vx1(convertVoltage(u16));
    if (!FP::defined(Vx1.read().toReal())) return 15;
    remap("Vx1", Vx1);

    u16 = readAndAdvance(input, ok);
    if (!ok) return 16;
    if (u16 >= 4096) return 17;
    Variant::Root T1(1.0 /
                             ((1.0 / 3900.0) *
                                     std::log((4096.0 / static_cast<double>(u16) - 1.0) +
                                                      (1.0 / 298.0))) - 273.0);
    remap("T1", T1);

    u16 = readAndAdvance(input, ok);
    if (!ok) return 18;
    if (u16 >= 4096) return 19;
    Variant::Root T2((static_cast<double>(u16) - 819.0) * 0.06104);
    remap("T2", T2);

    u16 = readAndAdvance(input, ok);
    if (!ok) return 20;
    Variant::Root Vx2(convertVoltage(u16));
    if (!FP::defined(Vx2.read().toReal())) return 21;
    remap("Vx2", Vx2);

    u16 = readAndAdvance(input, ok);
    if (!ok) return 22;
    Variant::Root Vx3(convertVoltage(u16));
    if (!FP::defined(Vx3.read().toReal())) return 23;
    remap("Vx3", Vx3);

    u16 = readAndAdvance(input, ok);
    if (!ok) return 24;
    Variant::Root Vx4(convertVoltage(u16));
    if (!FP::defined(Vx4.read().toReal())) return 25;
    remap("Vx4", Vx4);

    /* Unused 1*/
    if (input.size() < 4) return 26;
    input = input.mid(4);

    /* Unused 2*/
    if (input.size() < 4) return 27;
    input = input.mid(4);

    u16 = readAndAdvance(input, ok);
    if (!ok) return 28;
    Variant::Root ZTransit(static_cast<double>(u16));
    remap("ZTransit", ZTransit);

    u16 = readAndAdvance(input, ok);
    if (!ok) return 29;
    Variant::Root ZBandwidth(Variant::Type::Real);
    if (u16 > 0 && u16 < 4096)
        ZBandwidth.write().setReal(u16);
    remap("ZBandwidth", ZBandwidth);

    u16 = readAndAdvance(input, ok);
    if (!ok) return 30;
    Variant::Root ZDT(Variant::Type::Real);
    if (u16 > 0 && u16 < 4096)
        ZDT.write().setReal(u16);
    remap("ZDT", ZDT);

    auto u32 = readAndAdvance<quint32>(input, ok);
    if (!ok) return 31;
    Variant::Root ZOverflow(static_cast<double>(u32));
    remap("ZOverflow", ZOverflow);

    Variant::Root Q(config.front().flow);
    remap("Q", Q);

    Variant::Root Cb(Variant::Type::Array);
    Variant::Root Nb(Variant::Type::Array);
    Variant::Root Ns;
    double total = FP::undefined();
    for (std::size_t i = 0; i < 10; i++) {
        u32 = readAndAdvance<quint32>(input, ok);
        if (!ok) return 100 + i;
        Variant::Root rate(calculateRate(u32, dT));
        remap("ZRate" + std::to_string(i + 1), rate);
        double add = rate.read().toReal();
        if (FP::defined(add)) {
            if (!FP::defined(total))
                total = add;
            else
                total += add;
        }
        Variant::Root bin(total);
        remap("Cb" + std::to_string(i + 1), bin);
        Cb.write().array(i).set(bin);

        Variant::Root ZNb(calculateConcentration(add, Q.read().toDouble()));
        remap("Nb" + std::to_string(i + 1), ZNb);
        Nb.write().array(i).set(ZNb);

        if (i < config.front().binDiameter.size()) {
            Variant::Root ZNs(config.front().binDiameter[i]);
            remap("Ns" + std::to_string(i + 1), ZNs);
            Ns.write().array(i).set(ZNs);
        }
    }
    remap("Cb", Cb);
    remap("Nb", Nb);
    remap("Ns", Ns);

    Variant::Root C(total);
    remap("C", C);

    Variant::Root N(calculateConcentration(total, config.front().flow));
    remap("N", N);

    if (!haveEmittedLogMeta && loggingEgress && FP::defined(startTime)) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }


    logValue(startTime, endTime, "F1", Variant::Root(Variant::Type::Flags));
    logValue(startTime, endTime, "N", std::move(N));
    logValue(startTime, endTime, "Nb", std::move(Nb));
    logValue(startTime, endTime, "T1", std::move(T1));
    logValue(startTime, endTime, "T2", std::move(T2));
    logValue(startTime, endTime, "V1", std::move(V1));
    logValue(startTime, endTime, "V2", std::move(V2));
    if (Ns.read().exists())
        logValue(startTime, endTime, "Ns", std::move(Ns));

    Variant::Root ZINPUTS(Variant::Type::Array);
    ZINPUTS.write().array(0).set(Vx1);
    ZINPUTS.write().array(1).set(Vx2);
    ZINPUTS.write().array(2).set(Vx3);
    ZINPUTS.write().array(3).set(Vx4);
    logValue(startTime, endTime, "ZINPUTS", std::move(ZINPUTS));

    realtimeValue(endTime, "C", std::move(C));
    realtimeValue(endTime, "Cb", std::move(Cb));
    realtimeValue(endTime, "Vx1", std::move(Vx1));
    realtimeValue(endTime, "Vx2", std::move(Vx2));
    realtimeValue(endTime, "Vx3", std::move(Vx3));
    realtimeValue(endTime, "Vx4", std::move(Vx4));
    realtimeValue(endTime, "ZTransit", std::move(ZTransit));
    realtimeValue(endTime, "ZBandwidth", std::move(ZBandwidth));
    realtimeValue(endTime, "ZDT", std::move(ZDT));
    realtimeValue(endTime, "ZOverflow", std::move(ZOverflow));

    return 0;
}


void AcquireDMTBCP::describeState(Variant::Write &info) const
{
    if (!commandQueue.empty()) {
        info.hash("Command").set(commandQueue.front().stateDescription());
    }

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        info.hash("ResponseState").setString("AutoprobePassiveInitialize");
        break;
    case RESP_AUTOPROBE_PASSIVE_WAIT:
        info.hash("ResponseState").setString("AutoprobePassiveWait");
        break;
    case RESP_PASSIVE_WAIT:
        info.hash("ResponseState").setString("PassiveWait");
        break;
    case RESP_PASSIVE_RUN:
        info.hash("ResponseState").setString("PassiveRun");
        break;
    case RESP_INTERACTIVE_RUN:
        info.hash("ResponseState").setString("InteractiveRun");
        break;
    case RESP_INTERACTIVE_RUN_WAIT:
        info.hash("ResponseState").setString("InteractiveRunWait");
        break;
    case RESP_INTERACTIVE_INITIALIZE:
        info.hash("ResponseState").setString("InteractiveInitialize");
        break;
    case RESP_INTERACTIVE_RESTART_WAIT:
        info.hash("ResponseState").setString("InteractiveRestartWait");
        break;
    case RESP_INTERACTIVE_START_SET_PARAMETERS:
        info.hash("ResponseState").setString("InteractiveStartSetParameters");
        break;
    case RESP_INTERACTIVE_START_DISCARDFIRST:
        info.hash("ResponseState").setString("StartInteractiveDiscardFirst");
        break;
    case RESP_INTERACTIVE_START_FIRSTVALID:
        info.hash("ResponseState").setString("StartInteractiveStartReadFirstRecord");
        break;
    case RESP_INTERACTIVE_START_FIRSTVALID_WAIT:
        info.hash("ResponseState").setString("StartInteractiveStartReadFirstRecordWait");
        break;
    }
}

AcquireDMTBCP::Command::Command() : type(COMMAND_INVALID), packet()
{ }

AcquireDMTBCP::Command::Command(CommandType t, Util::ByteArray p) : type(t), packet(std::move(p))
{ }

AcquireDMTBCP::Command::Command(const Command &) = default;

AcquireDMTBCP::Command &AcquireDMTBCP::Command::operator=(const Command &) = default;

AcquireDMTBCP::Command::Command(Command &&) = default;

AcquireDMTBCP::Command &AcquireDMTBCP::Command::operator=(Command &&) = default;

Variant::Root AcquireDMTBCP::Command::stateDescription() const
{
    Variant::Root result;
    result["Packet"].setBinary(packet);

    switch (type) {
    default:
        result["Type"].setString("Unknown");
        break;
    case COMMAND_DATA:
        result["Type"].setString("Data");
        break;
    case COMMAND_PARAMETERS:
        result["Type"].setString("Parameters");
        break;
    }
    return result;
}

std::size_t AcquireDMTBCP::dataFrameStart(std::size_t offset, const Util::ByteArray &input) const
{
    if (commandQueue.empty())
        return input.npos;
    return offset;
}

std::size_t AcquireDMTBCP::dataFrameEnd(std::size_t start, const Util::ByteArray &input) const
{
    if (commandQueue.empty())
        return input.npos;

    auto availableLength = input.size() - start;
    switch (commandQueue.front().getType()) {
    case COMMAND_PARAMETERS:
        if (availableLength < 4)
            return input.npos;
        return start + 4;
    case COMMAND_DATA:
        if (availableLength < 76)
            return input.npos;
        return start + 76;
    case COMMAND_INVALID:
    default:
        break;
    }

    return input.size();
}

std::size_t AcquireDMTBCP::controlFrameStart(std::size_t offset, const Util::ByteArray &input) const
{
    for (auto max = input.size(); offset < max; ++offset) {
        if (input[offset] != 0x1B)
            continue;
        if (offset + 3 > max)
            break;
        auto command = input[offset + 1];
        if (command != 0x01 && command != 0x02)
            continue;
        return offset + 1;
    }
    return input.npos;
}

std::size_t AcquireDMTBCP::controlFrameEnd(std::size_t start, const Util::ByteArray &input) const
{
    int packetSize = input.size() - start;
    if (packetSize < 3)
        return input.npos;

    auto command = input[start];
    switch (command) {
    case 0x01:
        if (packetSize < 101)
            return input.npos;
        return start + 101;
    case 0x02:
        return start + 3;
    default:
        break;
    }
    return input.npos;
}

void AcquireDMTBCP::queueCommand(CommandType command, const Util::ByteView &payload)
{
    Q_ASSERT(command != COMMAND_INVALID);

    Util::ByteArray frame;
    frame.push_back(0x1B);
    frame.push_back(command);
    frame += payload;
    auto csum = calculateChecksum(frame, false);
    auto offset = frame.size();
    frame.resize(offset + 2);
    qToLittleEndian<quint16>(csum, frame.data<uchar *>(offset));

    commandQueue.emplace_back(command, Util::ByteArray(frame.mid(1, frame.size() - 3)));
    if (controlStream) {
        controlStream->writeControl(std::move(frame));
    }
}


void AcquireDMTBCP::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    {
        int oldSize = config.size();
        Q_ASSERT(!config.isEmpty());
        double oldFlow = config.first().flow;
        if (!Range::intersectShift(config, frameTime)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.isEmpty());
        if (oldSize != config.size()) {
            if (!FP::equal(oldFlow, config.first().flow)) {
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
            }
        }
    }

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 5) {
                qCDebug(log) << "Autoprobe succeeded at" << Logging::time(frameTime);

                if (realtimeEgress) {
                    realtimeEgress->incomingData(
                            SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                          FP::undefined()));
                }

                Variant::Write info = Variant::Write::empty();
                describeState(info);
                event(frameTime, QObject::tr("Autoprobe succeeded."), false, info);

                responseState = RESP_PASSIVE_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                if (commandQueue.empty())
                    timeoutAt(frameTime + config.first().pollInterval + 1.0);
            } else {
                if (commandQueue.empty())
                    timeoutAt(frameTime + config.first().pollInterval + 1.0);

                responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
            }
        } else if (code > 0) {
            autoprobeValidRecords = 0;
            loggingLost(frameTime);
        }
        break;
    }
    case RESP_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                      FP::undefined()));
            }

            Variant::Write info = Variant::Write::empty();
            describeState(info);
            event(frameTime, QObject::tr("Passive communications established."), false, info);

            responseState = RESP_PASSIVE_RUN;
            ++autoprobeValidRecords;
            generalStatusUpdated();

            if (commandQueue.empty())
                timeoutAt(frameTime + config.first().pollInterval + 2.0);
        } else if (code > 0) {
            autoprobeValidRecords = 0;
            loggingLost(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_FIRSTVALID:
    case RESP_INTERACTIVE_START_FIRSTVALID_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 5) {
                qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

                if (realtimeEgress) {
                    realtimeEgress->incomingData(
                            SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                          FP::undefined()));
                }

                Variant::Write info = Variant::Write::empty();
                describeState(info);
                event(frameTime, QObject::tr("Communications established."), false, info);

                timeoutAt(frameTime + config.first().pollInterval);
                responseState = RESP_INTERACTIVE_RUN_WAIT;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();
            } else {
                if (config.first().pollInterval > 0.0) {
                    responseState = RESP_INTERACTIVE_START_FIRSTVALID_WAIT;
                    timeoutAt(frameTime + config.first().pollInterval);
                } else {
                    responseState = RESP_INTERACTIVE_START_FIRSTVALID;
                    timeoutAt(frameTime + 1.0);
                    queueCommand(COMMAND_DATA);
                }
            }
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime)
                         << "with code" << code;

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            Variant::Write info = Variant::Write::empty();
            describeState(info);
            info.hash("Code").setInt64(code);
            info.hash("Packet").setBinary(frame);
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

            commandQueue.clear();
            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            autoprobeValidRecords = 0;
            if (controlStream)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            loggingLost(frameTime);
        } /* Ignored line */
        break;
    }

    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_RUN_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (responseState != RESP_PASSIVE_RUN) {
                if (config.first().pollInterval > 0.0) {
                    responseState = RESP_INTERACTIVE_RUN_WAIT;
                    timeoutAt(frameTime + config.first().pollInterval);
                } else {
                    responseState = RESP_INTERACTIVE_RUN;
                    timeoutAt(frameTime + 1.0);
                    queueCommand(COMMAND_DATA);
                }
            } else {
                timeoutAt(frameTime + config.first().pollInterval + 1.0);
            }
        } else if (code > 0) {
            qCDebug(log) << "Packet at" << Logging::time(frameTime) << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();
            describeState(info);
            info.hash("Code").setInt64(code);
            info.hash("Packet").setBinary(frame);
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            commandQueue.clear();
            if (responseState == RESP_PASSIVE_RUN) {
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
            } else {
                responseState = RESP_INTERACTIVE_RESTART_WAIT;
                discardData(frameTime + 1.0);
            }
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            loggingLost(frameTime);

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
        } /* Ignored line */
        break;
    }

    case RESP_INTERACTIVE_START_SET_PARAMETERS: {
        if (frame.size() < 4 || frame[0] != 0x06 || frame[1] != 0x06) {
            qCDebug(log) << "Parameter set response at" << Logging::time(frameTime) << "rejected";

            commandQueue.clear();
            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            autoprobeValidRecords = 0;
            if (controlStream)
                controlStream->resetControl();
            discardData(frameTime + 10.0);
            timeoutAt(frameTime + 30.0);
            generalStatusUpdated();

            loggingLost(frameTime);

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
        } else {
            Variant::Root oldReveision(instrumentMeta["FirmwareVersion"]);
            instrumentMeta["FirmwareVersion"].setInteger(
                    Util::ByteView(frame).mid(2).readNumber<quint16>());
            if (oldReveision.read() != instrumentMeta["FirmwareVersion"]) {
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            commandQueue.clear();
            responseState = RESP_INTERACTIVE_START_DISCARDFIRST;
            discardData(frameTime + 1.0);
            timeoutAt(frameTime + 5.0);
            queueCommand(COMMAND_DATA);

            if (realtimeEgress) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveDiscardFirst"), frameTime, FP::undefined()));
            }
        }
    }

    default:
        break;
    }
}

void AcquireDMTBCP::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));

    switch (responseState) {
    case RESP_INTERACTIVE_RUN:
        qCDebug(log) << "Timeout in interactive mode at" << Logging::time(frameTime);
        timeoutAt(frameTime + 30.0);

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        autoprobeValidRecords = 0;
        discardData(frameTime + 1.0);
        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        loggingLost(frameTime);
        break;

    case RESP_INTERACTIVE_START_FIRSTVALID_WAIT:
        responseState = RESP_INTERACTIVE_START_FIRSTVALID;
        timeoutAt(frameTime + 1.0);
        queueCommand(COMMAND_DATA);
        break;

    case RESP_INTERACTIVE_RUN_WAIT:
        responseState = RESP_INTERACTIVE_RUN;
        timeoutAt(frameTime + 1.0);
        queueCommand(COMMAND_DATA);
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        responseState = RESP_PASSIVE_WAIT;
        autoprobeValidRecords = 0;
        generalStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        loggingLost(frameTime);
        break;

    case RESP_INTERACTIVE_START_SET_PARAMETERS:
    case RESP_INTERACTIVE_START_DISCARDFIRST:
    case RESP_INTERACTIVE_START_FIRSTVALID:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        autoprobeValidRecords = 0;
        if (controlStream)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        loggingLost(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        commandQueue.clear();
        timeoutAt(frameTime + 30.0);
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        autoprobeValidRecords = 0;
        discardData(frameTime + 0.5);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        loggingLost(frameTime);
        break;

    default:
        loggingLost(frameTime);
        break;
    }
}

static void parameterOrDefault(Util::ByteArray &parameters,
                               const Variant::Read &input,
                               std::uint_fast16_t def = 0)
{
    auto add = input.toInt64();
    if (!INTEGER::defined(add) || add < 0 || add > 0xFFFF) {
        parameters.appendNumber<quint16>(def);
        return;
    }
    parameters.appendNumber<quint16>(static_cast<quint16>(add));
}

static void parameterBoolean(Util::ByteArray &parameters,
                             const Variant::Read &input,
                             bool def = false)
{
    if (!input.exists()) {
        parameters.appendNumber<quint16>(def ? 1 : 0);
        return;
    }
    parameters.appendNumber<quint16>(input.toBoolean() ? 1 : 0);
}

void AcquireDMTBCP::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    Q_ASSERT(!config.isEmpty());

    switch (responseState) {
    case RESP_INTERACTIVE_START_DISCARDFIRST:
        responseState = RESP_INTERACTIVE_START_FIRSTVALID;
        timeoutAt(frameTime + 2.0);
        queueCommand(COMMAND_DATA);

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveStartReadFirstRecord"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        commandQueue.clear();

        if (config.front().parameters.read().exists()) {
            Util::ByteArray parameterData;

            auto raw = config.front().parameters.read();
            switch (raw.getType()) {
            case Variant::Type::Bytes:
                parameterData = raw.toBytes();
                parameterData.resize(98);
                break;
            default: {
                parameterOrDefault(parameterData, raw["ADCThreshold"], 0x0400);
                /* Enable average transit time rejection, disabled on a BCP */
                parameterData.appendNumber<quint16>(0);
                std::uint_fast16_t binCount = 20;
                {
                    auto add = raw["Count"].toInteger();
                    if (!INTEGER::defined(add) && raw["Bins"].exists())
                        add = raw["Bins"].toArray().size();
                    if (!INTEGER::defined(add) && !config.front().binDiameter.empty())
                        add = config.front().binDiameter.size();
                    if (!INTEGER::defined(add))
                        add = 20;
                    else if (add <= 10)
                        add = 10;
                    else if (add <= 20)
                        add = 20;
                    else if (add <= 30)
                        add = 30;
                    else
                        add = 40;
                    binCount = static_cast<quint16>(add);
                }
                parameterData.appendNumber<quint16>(binCount);
                /* Depth of field rejection, disabled on a BCP */
                parameterData.appendNumber<quint16>(0);
                /* Probe range [0,3], but not used on a BCP */
                parameterData.appendNumber<quint16>(0);
                /* Average transit rejection weight, fixed at 64 on a BCP */
                parameterData.appendNumber<quint16>(64);
                /* Percent of average time acceptance, not used on a BCP */
                parameterData.appendNumber<quint16>(0);
                /* Divisor flag, not used on a BCP */
                parameterData.appendNumber<quint16>(0);
                parameterBoolean(parameterData, raw["CountFromPeak"]);
                auto boundaries = raw["Bins"].toArray();
                uint_fast16_t prior = 0;
                for (uint_fast16_t bin = 0; bin < binCount - 1; ++bin) {
                    auto add = boundaries[bin].toInteger();
                    if (!INTEGER::defined(add) || add < 0)
                        add = (static_cast<std::int_fast64_t>(bin) * 4096) / binCount;
                    if (add > 4095)
                        add = 4095;
                    if (static_cast<uint_fast16_t>(add) < prior)
                        add = prior;
                    parameterData.appendNumber<quint16>(add);
                    prior = static_cast<uint_fast16_t>(add);
                }
                for (uint_fast16_t bin = binCount - 1; bin < 40; ++bin) {
                    parameterData.appendNumber<quint16>(0xFFFF);
                }
                break;
            }
            }

            responseState = RESP_INTERACTIVE_START_SET_PARAMETERS;
            timeoutAt(frameTime + 5.0);
            queueCommand(COMMAND_PARAMETERS, parameterData);
        } else {
            commandQueue.clear();
            responseState = RESP_INTERACTIVE_START_FIRSTVALID;
            discardData(frameTime + 1.0);
            timeoutAt(frameTime + 10.0);
            queueCommand(COMMAND_DATA);

            responseState = RESP_INTERACTIVE_START_DISCARDFIRST;

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                      frameTime, FP::undefined()));
            }
        }
        break;

    default:
        break;
    }
}


void AcquireDMTBCP::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));

    if (frame.size() < 3)
        return;
    if (Util::ByteView(frame).mid(frame.size() - 2).readNumber<quint16>() !=
            calculateChecksum(Util::ByteView(frame).mid(0, frame.size() - 2)))
        return;

    commandQueue.emplace_back(static_cast<CommandType>(frame[0]),
                              Util::ByteArray(frame.mid(1, frame.size() - 3)));

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        timeoutAt(frameTime + 1.0);
        break;

    default:
        break;
    }
}

Variant::Root AcquireDMTBCP::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireDMTBCP::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_RUN_WAIT:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

AcquisitionInterface::AutoprobeStatus AcquireDMTBCP::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireDMTBCP::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_RUN_WAIT:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_SET_PARAMETERS:
    case RESP_INTERACTIVE_START_DISCARDFIRST:
    case RESP_INTERACTIVE_START_FIRSTVALID:
    case RESP_INTERACTIVE_START_FIRSTVALID_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        commandQueue.clear();
        timeoutAt(time + 30.0);
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        autoprobeValidRecords = 0;
        discardData(time + 0.5);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireDMTBCP::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    Q_ASSERT(!config.isEmpty());

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobeValidRecords = 0;
    commandQueue.clear();
    discardData(time + 0.5, 1);
    timeoutAt(time + config.first().pollInterval * 7.0 + 3.0);
    generalStatusUpdated();
}

void AcquireDMTBCP::autoprobePromote(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Promoted from interactive wait to interactive acquisition at"
                     << Logging::time(time);

        timeoutAt(time);
        break;
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_START_SET_PARAMETERS:
    case RESP_INTERACTIVE_START_DISCARDFIRST:
    case RESP_INTERACTIVE_START_FIRSTVALID:
    case RESP_INTERACTIVE_START_FIRSTVALID_WAIT:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + config.first().pollInterval + 2.0);
        break;

    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        commandQueue.clear();
        timeoutAt(time + 30.0);
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        autoprobeValidRecords = 0;
        discardData(time + 0.5);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireDMTBCP::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    Q_ASSERT(!config.isEmpty());

    timeoutAt(time + config.first().pollInterval + 2.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_RUN:
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from interactive to passive acquisition at"
                     << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireDMTBCP::getDefaults()
{
    AutomaticDefaults result;
    result.name = "N$1$2";
    result.setSerialN81(38400);
    result.autoprobeInitialOrder = 80;
    result.autoprobeLevelPriority = 20;
    return result;
}


ComponentOptions AcquireDMTBCPComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options);
    return options;
}

ComponentOptions AcquireDMTBCPComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireDMTBCPComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireDMTBCPComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireDMTBCPComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options);
}

QList<ComponentExample> AcquireDMTBCPComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data using the default flow rate")));

    options = getBaseOptions();
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("q")))->set(25.78);
    examples.append(ComponentExample(options, tr("Explicitly set flow rate.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireDMTBCPComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                               const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireDMTBCP(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireDMTBCPComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                               const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireDMTBCP(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireDMTBCPComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                 const std::string &loggingContext)
{
    std::unique_ptr<AcquireDMTBCP> i(new AcquireDMTBCP(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireDMTBCPComponent::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                   const std::string &loggingContext)
{
    std::unique_ptr<AcquireDMTBCP> i(new AcquireDMTBCP(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
