/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CALCLWCPROBE_H
#define CALCLWCPROBE_H

#include "core/first.hxx"

#include <vector>
#include <memory>
#include <unordered_set>
#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

class CalcLWCProbe : public CPD3::Data::SegmentProcessingStage {
    struct Processing {
        std::unique_ptr<CPD3::Data::DynamicSequenceSelection> operateConcentration;

        std::unique_ptr<CPD3::Data::DynamicInput> inputPower;
        std::unique_ptr<CPD3::Data::DynamicInput> inputAirTemperature;
        std::unique_ptr<CPD3::Data::DynamicInput> inputAirPressure;
        std::unique_ptr<CPD3::Data::DynamicInput> inputAirSpeed;
        std::unique_ptr<CPD3::Data::DynamicInput> inputProbeTemperature;
        std::unique_ptr<CPD3::Data::DynamicInput> inputProbeLength;
        std::unique_ptr<CPD3::Data::DynamicInput> inputProbeDiameter;
    };
    std::vector<Processing> processing;

    std::unique_ptr<CPD3::Data::DynamicInput> defaultAirTemperature;
    std::unique_ptr<CPD3::Data::DynamicInput> defaultAirPressure;
    std::unique_ptr<CPD3::Data::DynamicInput> defaultAirSpeed;
    std::unique_ptr<CPD3::Data::DynamicInput> defaultProbeTemperature;
    std::unique_ptr<CPD3::Data::DynamicInput> defaultProbeLength;
    std::unique_ptr<CPD3::Data::DynamicInput> defaultProbeDiameter;
    CPD3::Data::SequenceName::ComponentSet filterSuffixes;

    bool restrictedInputs;

    void handleOptions(const CPD3::ComponentOptions &options);

    void handleNewProcessing(const CPD3::Data::SequenceName &name,
                             std::size_t id,
                             CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control);

    void registerPossibleInput(const CPD3::Data::SequenceName &name,
                               CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                               std::size_t filterID = static_cast<std::size_t>(-1));

public:
    CalcLWCProbe();

    CalcLWCProbe(const CPD3::ComponentOptions &options);

    CalcLWCProbe(const CPD3::ComponentOptions &options,
                 double start,
                 double end,
                 const QList<CPD3::Data::SequenceName> &inputs);

    CalcLWCProbe(double start,
                 double end,
                 const CPD3::Data::SequenceName::Component &station,
                 const CPD3::Data::SequenceName::Component &archive,
                 const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CalcLWCProbe();

    void unhandled(const CPD3::Data::SequenceName &name,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;

    QSet<double> metadataBreaks(int id) override;

    CalcLWCProbe(QDataStream &stream);

    void serialize(QDataStream &stream) override;
};

class CalcLWCProbeComponent
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.calc_lwcprobe"
                              FILE
                              "calc_lwcprobe.json")
public:
    QString getBasicSerializationName() const override;

    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    CPD3::Data::SegmentProcessingStage *createBasicFilterDynamic(const CPD3::ComponentOptions &options) override;

    CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined(const CPD3::ComponentOptions &options,
                                                                    double start,
                                                                    double end,
                                                                    const QList<
                                                                            CPD3::Data::SequenceName> &inputs) override;

    CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                 double end,
                                                                 const CPD3::Data::SequenceName::Component &station,
                                                                 const CPD3::Data::SequenceName::Component &archive,
                                                                 const CPD3::Data::ValueSegment::Transfer &config) override;

    CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream) override;
};

#endif
