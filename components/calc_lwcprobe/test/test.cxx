/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    struct ID {
        SequenceName::Set filter;
        SequenceName::Set input;

        ID() = default;

        ID(SequenceName::Set filter, SequenceName::Set input) : filter(std::move(filter)),
                                                                input(std::move(input))
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }
    };

    std::unordered_map<int, ID> contents;

    int find(const SequenceName::Set &filter, const SequenceName::Set &input = {}) const
    {
        for (const auto &check : contents) {
            if (check.second.filter != filter)
                continue;
            if (check.second.input != input)
                continue;
            return check.first;
        }
        return -1;
    }

    void filterUnit(const SequenceName &name, int targetID) override
    {
        contents[targetID].input.erase(name);
        contents[targetID].filter.insert(name);
    }

    void inputUnit(const SequenceName &name, int targetID) override
    {
        if (contents[targetID].filter.count(name))
            return;
        contents[targetID].input.insert(name);
    }

    bool isHandled(const SequenceName &name) override
    {
        for (const auto &check : contents) {
            if (check.second.filter.count(name))
                return true;
            if (check.second.input.count(name))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("calc_lwcprobe"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("input-air-t")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("input-air-p")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("input-air-speed")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("input-probe-temperature")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("input-probe-length")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("input-probe-diameter")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("input-power")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("output-concentration")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")));

        QVERIFY(options.excluded().value("output-concentration").contains("suffix"));
        QVERIFY(options.excluded().value("input-power").contains("suffix"));
        QVERIFY(options.excluded().value("suffix").contains("output-concentration"));
        QVERIFY(options.excluded().value("suffix").contains("input-power"));
    }

    void dynamicDefaults()
    {
        ComponentOptions options(component->getOptions());
        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterDynamic(options));
        QVERIFY(filter.get());
        TestController controller;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "Q_X1"), &controller);
        QVERIFY(controller.contents.empty());
        {
            std::unique_ptr<SegmentProcessingStage> filter2;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2.reset(component->deserializeBasicFilter(stream));
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2.get());
            filter2->unhandled(SequenceName("brw", "raw", "Q_X1"), &controller);
            QVERIFY(controller.contents.empty());
        }

        SequenceName VA_X1_1("brw", "raw", "VA_X1");
        SequenceName X_X1_1("brw", "raw", "X_X1");
        SequenceName VA_X1_2("sgp", "raw", "VA_X1");
        SequenceName X_X1_2("sgp", "raw", "X_X1");
        SequenceName VA_XM1_1("brw", "raw", "VA_XM1");
        SequenceName X_XM1_1("brw", "raw", "X_XM1");

        filter->unhandled(VA_X1_1, &controller);
        filter->unhandled(X_X1_1, &controller);
        QCOMPARE((int) controller.contents.size(), 1);
        int X1_1 = controller.find({X_X1_1}, {VA_X1_1});
        QVERIFY(X1_1 != -1);

        filter->unhandled(SequenceName("brw", "raw", "X_P01"), &controller);
        QCOMPARE((int) controller.contents.size(), 1);

        filter->unhandled(VA_X1_2, &controller);
        QCOMPARE((int) controller.contents.size(), 2);
        int X1_2 = controller.find({X_X1_2}, {VA_X1_2});
        QVERIFY(X1_2 != -1);

        filter->unhandled(X_XM1_1, &controller);
        filter->unhandled(VA_XM1_1, &controller);
        QCOMPARE((int) controller.contents.size(), 3);
        int XM_1 = controller.find({X_XM1_1}, {VA_XM1_1});
        QVERIFY(XM_1 != -1);

        data.setStart(15.0);
        data.setEnd(16.0);

        data.setValue(VA_X1_1, Variant::Root(20.0));
        filter->process(X1_1, data);
        QCOMPARE(data.value(X_X1_1).toReal(), 226.4927216738653328);

        {
            std::unique_ptr<SegmentProcessingStage> filter2;
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2.reset(component->deserializeBasicFilter(stream));
            }
            QVERIFY(filter2.get());
            data.setValue(X_X1_1, Variant::Root());
            data.setValue(VA_X1_1, Variant::Root(20.0));
            filter->process(X1_1, data);
            QCOMPARE(data.value(X_X1_1).toReal(), 226.4927216738653328);
        }

        data.setValue(VA_X1_2, Variant::Root(15.0));
        filter->process(X1_2, data);
        QCOMPARE(data.value(X_X1_2).toReal(), 167.5595037044941762);

        data.setValue(VA_XM1_1, Variant::Root(35.0));
        filter->process(XM_1, data);
        QCOMPARE(data.value(X_XM1_1).toReal(), 403.2923755819788880);

        data.setValue(VA_X1_1, Variant::Root(FP::undefined()));
        filter->process(X1_1, data);
        QVERIFY(!FP::defined(data.value(X_X1_1).toReal()));

        filter->processMeta(X1_1, data);
        QCOMPARE(data.value(X_X1_1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
    }

    void dynamicOptions()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<DynamicInputOption *>(options.get("input-air-t"))->set(20.0);
        qobject_cast<DynamicInputOption *>(options.get("input-air-p"))->set(950.0);
        qobject_cast<DynamicInputOption *>(options.get("input-air-speed"))->set(1.25);
        qobject_cast<DynamicInputOption *>(options.get("input-probe-temperature"))->set(120.0);
        qobject_cast<DynamicInputOption *>(options.get("input-probe-length"))->set(1.9);
        qobject_cast<DynamicInputOption *>(options.get("input-probe-diameter"))->set(0.17);
        qobject_cast<DynamicInputOption *>(options.get("input-power"))->set(
                SequenceMatch::OrderedLookup(
                        SequenceMatch::Element(QString("brw"), QString("raw"), "VAx_S11",
                                               SequenceName::Flavors())), Calibration());
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-concentration"))->set(
                "brw", "raw", "Xc_S11", SequenceName::Flavors());

        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterDynamic(options));
        QVERIFY(filter.get());
        TestController controller;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "VAx_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "VA_S11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "X_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "X_S12"), &controller);
        QVERIFY(controller.contents.empty());

        SequenceName VAx_S11("brw", "raw", "VAx_S11");
        SequenceName Xc_S11("brw", "raw", "Xc_S11");
        filter->unhandled(VAx_S11, &controller);
        filter->unhandled(Xc_S11, &controller);

        QCOMPARE((int) controller.contents.size(), 1);
        int id = controller.find({Xc_S11}, {VAx_S11});
        QVERIFY(id != -1);

        data.setValue(VAx_S11, Variant::Root(9.5));
        filter->process(id, data);
        QCOMPARE(data.value(Xc_S11).toReal(), 95.9808679141784609);

        filter.reset();


        options = component->getOptions();
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->add("N11");

        filter.reset(component->createBasicFilterDynamic(options));
        QVERIFY(filter != NULL);
        controller.contents.clear();

        SequenceName VA_N11("brw", "raw", "VA_N11");
        SequenceName X_N11("brw", "raw", "X_N11");

        filter->unhandled(SequenceName("brw", "raw", "VA_P01"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "X_P01"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "X_N12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "VA_N12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "T_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "P_V11"), &controller);
        QVERIFY(controller.contents.empty());

        filter->unhandled(VA_N11, &controller);

        QCOMPARE((int) controller.contents.size(), 1);
        id = controller.find({X_N11}, {VA_N11});
        QVERIFY(id != -1);

        data.setValue(VA_N11, Variant::Root(9.5));
        data.setValue(Xc_S11, Variant::Root());
        filter->process(id, data);
        QCOMPARE(data.value(X_N11).toReal(), 102.7329639381858755);

        filter.reset();
    }

    void predefined()
    {
        ComponentOptions options(component->getOptions());

        SequenceName VA_A11("brw", "raw", "VA_A11");
        SequenceName X_A11("brw", "raw", "X_A11");
        QList<SequenceName> input;
        input << VA_A11;

        std::unique_ptr<SegmentProcessingStage> filter
                (component->createBasicFilterPredefined(options, FP::undefined(), FP::undefined(),
                                                        input));
        QVERIFY(filter.get());
        TestController controller;
        QList<int> idL;

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{VA_A11}));
        QCOMPARE(filter->predictedOutputs(), (SequenceName::Set{X_A11}));
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["XM1/CalculateConcentration"] = "::X_XM1:=";
        cv["XM1/Power"] = "::VA_XM1:=";
        cv["XM1/Air/Temperature"] = 25.0;
        cv["XM1/Air/Pressure"] = "::P_XM1:=";
        cv["XM1/Air/Speed"] = "::Qs_XM1:=";
        cv["XM1/Probe/Temperature"] = 125.0;
        cv["XM1/Probe/Length"] = 2.0;
        cv["XM1/Probe/Diameter"] = 0.18;
        config.emplace_back(FP::undefined(), 13.0, cv);
        cv.write().setEmpty();
        cv["XM1/CalculateConcentration"] = "::X_XM1:=";
        cv["XM1/Power"] = "::VA_XM1:=";
        cv["XM1/Air/Temperature"] = "::T_XM1:=";
        cv["XM1/Air/Pressure"] = "::P_XM1:=";
        cv["XM1/Air/Speed"] = 1.5;
        cv["XM1/Probe/Temperature"] = 126.0;
        cv["XM1/Probe/Length"] = 2.1;
        cv["XM1/Probe/Diameter"] = 0.17;
        config.emplace_back(13.0, FP::undefined(), cv);

        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config));
        QVERIFY(filter.get());
        TestController controller;

        SequenceName X_XM1("brw", "raw", "X_XM1");
        SequenceName VA_XM1("brw", "raw", "VA_XM1");
        SequenceName T_XM1("brw", "raw", "T_XM1");
        SequenceName P_XM1("brw", "raw", "P_XM1");
        SequenceName Qs_XM1("brw", "raw", "Qs_XM1");

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{VA_XM1, T_XM1, P_XM1, Qs_XM1}));
        QCOMPARE(filter->predictedOutputs(), (SequenceName::Set{X_XM1}));

        filter->unhandled(T_XM1, &controller);
        filter->unhandled(P_XM1, &controller);
        filter->unhandled(VA_XM1, &controller);
        filter->unhandled(Qs_XM1, &controller);

        int id = controller.find({X_XM1}, {VA_XM1, T_XM1, P_XM1, Qs_XM1});
        QVERIFY(id != -1);

        QCOMPARE(filter->metadataBreaks(id), QSet<double>() << 13.0);

        SequenceSegment data1;
        data1.setStart(12.0);
        data1.setEnd(13.0);

        data1.setValue(VA_XM1, Variant::Root(10.0));
        data1.setValue(P_XM1, Variant::Root(1000.0));
        data1.setValue(Qs_XM1, Variant::Root(2.0));
        filter->process(id, data1);
        QCOMPARE(data1.value(X_XM1).toReal(), 55.4213166667081651);

        data1.setValue(VA_XM1, Variant::Root());
        data1.setValue(P_XM1, Variant::Root(1000.0));
        data1.setValue(Qs_XM1, Variant::Root(2.0));
        filter->process(id, data1);
        QVERIFY(!FP::defined(data1.value(X_XM1).toReal()));

        data1.setValue(VA_XM1, Variant::Root(10.0));
        data1.setValue(P_XM1, Variant::Root());
        data1.setValue(Qs_XM1, Variant::Root(2.0));
        filter->process(id, data1);
        QVERIFY(!FP::defined(data1.value(X_XM1).toReal()));

        data1.setValue(VA_XM1, Variant::Root(10.0));
        data1.setValue(P_XM1, Variant::Root(1000.0));
        data1.setValue(Qs_XM1, Variant::Root());
        filter->process(id, data1);
        QVERIFY(!FP::defined(data1.value(X_XM1).toReal()));

        filter->processMeta(id, data1);
        QCOMPARE(data1.value(X_XM1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);


        SequenceSegment data2;
        data2.setStart(13.0);
        data2.setEnd(15.0);

        data2.setValue(VA_XM1, Variant::Root(5.0));
        data2.setValue(T_XM1, Variant::Root(20.0));
        data2.setValue(P_XM1, Variant::Root(850.0));
        filter->process(id, data2);
        QCOMPARE(data2.value(X_XM1).toReal(), 34.5297568921404263);

        data2.setValue(VA_XM1, Variant::Root());
        data2.setValue(T_XM1, Variant::Root(20.0));
        data2.setValue(P_XM1, Variant::Root(850.0));
        filter->process(id, data2);
        QVERIFY(!FP::defined(data1.value(X_XM1).toReal()));

        data2.setValue(VA_XM1, Variant::Root(5.0));
        data2.setValue(T_XM1, Variant::Root());
        data2.setValue(P_XM1, Variant::Root(850.0));
        filter->process(id, data2);
        QVERIFY(!FP::defined(data1.value(X_XM1).toReal()));

        filter->processMeta(id, data2);
        QCOMPARE(data2.value(X_XM1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
