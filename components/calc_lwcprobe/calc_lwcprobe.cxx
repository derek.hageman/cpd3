/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cmath>
#include <QRegularExpression>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "core/environment.hxx"
#include "core/qtcompat.hxx"

#include "calc_lwcprobe.hxx"

#ifndef M_PI
#define M_PI    3.14159265358979
#endif

using namespace CPD3;
using namespace CPD3::Data;


void CalcLWCProbe::handleOptions(const ComponentOptions &options)
{
    if (options.isSet("input-air-t")) {
        defaultAirTemperature.reset(
                qobject_cast<DynamicInputOption *>(options.get("input-air-t"))->getInput());
    }
    if (options.isSet("input-air-p")) {
        defaultAirPressure.reset(
                qobject_cast<DynamicInputOption *>(options.get("input-air-p"))->getInput());
    }
    if (options.isSet("input-air-speed")) {
        defaultAirSpeed.reset(
                qobject_cast<DynamicInputOption *>(options.get("input-air-speed"))->getInput());
    }
    if (options.isSet("input-probe-temperature")) {
        defaultProbeTemperature.reset(qobject_cast<DynamicInputOption *>(
                options.get("input-probe-temperature"))->getInput());
    }
    if (options.isSet("input-probe-length")) {
        defaultProbeLength.reset(
                qobject_cast<DynamicInputOption *>(options.get("input-probe-length"))->getInput());
    }
    if (options.isSet("input-probe-diameter")) {
        defaultProbeDiameter.reset(qobject_cast<DynamicInputOption *>(
                options.get("input-probe-diameter"))->getInput());
    }

    restrictedInputs = false;

    if (options.isSet("input-power") || options.isSet("output-concentration")) {
        restrictedInputs = true;

        Processing proc;

        SequenceName::Component suffix;
        SequenceName::Component station;
        SequenceName::Component archive;
        SequenceName::Flavors flavors;
        if (options.isSet("output-concentration")) {
            proc.operateConcentration
                .reset(qobject_cast<DynamicSequenceSelectionOption *>(
                        options.get("output-concentration"))->getOperator());
            for (const auto &name : proc.operateConcentration->getAllUnits()) {
                suffix = Util::suffix(name.getVariable(), '_');
                station = name.getStation();
                archive = name.getArchive();
                flavors = name.getFlavors();
                if (suffix.length() != 0)
                    break;
            }
        }
        if (options.isSet("input-power")) {
            proc.inputPower
                .reset(qobject_cast<DynamicInputOption *>(options.get("input-power"))->getInput());
            if (suffix.length() == 0) {
                for (const auto &name : proc.inputPower->getUsedInputs()) {
                    suffix = Util::suffix(name.getVariable(), '_');
                    station = name.getStation();
                    archive = name.getArchive();
                    flavors = name.getFlavors();
                    if (suffix.length() != 0)
                        break;
                }
            }
        }

        if (!proc.operateConcentration) {
            if (suffix.length() == 0) {
                proc.operateConcentration
                    .reset(new DynamicSequenceSelection::Match(QString::fromStdString(station),
                                                               QString::fromStdString(archive),
                                                               "X\\d*_.*", flavors));
            } else {
                proc.operateConcentration
                    .reset(new DynamicSequenceSelection::Basic(
                            SequenceName(station, archive, "X_" + suffix, flavors)));
            }
        }
        if (!proc.inputPower) {
            DynamicInput::Variable *sel = new DynamicInput::Variable;
            proc.inputPower.reset(sel);
            if (suffix.length() == 0) {
                sel->set(SequenceMatch::OrderedLookup(
                        SequenceMatch::Element(station, archive, "VA\\d*_.*", flavors)),
                         Calibration());
            } else {
                sel->set(SequenceMatch::OrderedLookup(
                        SequenceMatch::Element(station, archive, "VA_" + suffix, flavors)),
                         Calibration());
            }
        }

        if (defaultAirTemperature) {
            proc.inputAirTemperature.reset(defaultAirTemperature->clone());
        } else {
            proc.inputAirTemperature.reset(new DynamicInput::Constant(0.0));
        }

        if (defaultAirPressure) {
            proc.inputAirPressure.reset(defaultAirPressure->clone());
        } else {
            proc.inputAirPressure.reset(new DynamicInput::Constant(1013.25));
        }

        if (defaultAirSpeed) {
            proc.inputAirSpeed.reset(defaultAirSpeed->clone());
        } else {
            proc.inputAirSpeed.reset(new DynamicInput::Constant(1.0));
        }

        if (defaultProbeTemperature) {
            proc.inputProbeTemperature.reset(defaultProbeTemperature->clone());
        } else {
            proc.inputProbeTemperature.reset(new DynamicInput::Constant(125.0));
        }

        if (defaultProbeLength) {
            proc.inputProbeLength.reset(defaultProbeLength->clone());
        } else {
            proc.inputProbeLength.reset(new DynamicInput::Constant(2.0));
        }

        if (defaultProbeDiameter) {
            proc.inputProbeDiameter.reset(defaultProbeDiameter->clone());
        } else {
            proc.inputProbeDiameter.reset(new DynamicInput::Constant(0.18));
        }

        processing.emplace_back(std::move(proc));
    }

    if (options.isSet("suffix")) {
        filterSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->get());
    }
}


CalcLWCProbe::CalcLWCProbe()
{ Q_ASSERT(false); }

CalcLWCProbe::CalcLWCProbe(const ComponentOptions &options)
{
    handleOptions(options);
}

CalcLWCProbe::CalcLWCProbe(const ComponentOptions &options,
                           double,
                           double,
                           const QList<SequenceName> &inputs)
{
    handleOptions(options);

    for (QList<SequenceName>::const_iterator unit = inputs.constBegin(), endU = inputs.constEnd();
            unit != endU;
            ++unit) {
        CalcLWCProbe::unhandled(*unit, nullptr);
    }
}

CalcLWCProbe::CalcLWCProbe(double start,
                           double end,
                           const SequenceName::Component &station,
                           const SequenceName::Component &archive,
                           const ValueSegment::Transfer &config)
{
    restrictedInputs = true;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    for (const auto &child : children) {
        Processing proc;

        proc.operateConcentration
            .reset(DynamicSequenceSelection::fromConfiguration(config,
                                                               QString("%1/CalculateConcentration").arg(
                                                                       QString::fromStdString(
                                                                               child)), start,
                                                               end));

        proc.inputPower
            .reset(DynamicInput::fromConfiguration(config, QString("%1/Power").arg(
                    QString::fromStdString(child)), start, end));
        proc.inputAirTemperature
            .reset(DynamicInput::fromConfiguration(config, QString("%1/Air/Temperature").arg(
                    QString::fromStdString(child)), start, end));
        proc.inputAirPressure
            .reset(DynamicInput::fromConfiguration(config, QString("%1/Air/Pressure").arg(
                    QString::fromStdString(child)), start, end));
        proc.inputAirSpeed
            .reset(DynamicInput::fromConfiguration(config, QString("%1/Air/Speed").arg(
                    QString::fromStdString(child)), start, end));
        proc.inputProbeTemperature
            .reset(DynamicInput::fromConfiguration(config, QString("%1/Probe/Temperature").arg(
                    QString::fromStdString(child)), start, end));
        proc.inputProbeLength
            .reset(DynamicInput::fromConfiguration(config, QString("%1/Probe/Length").arg(
                    QString::fromStdString(child)), start, end));
        proc.inputProbeDiameter
            .reset(DynamicInput::fromConfiguration(config, QString("%1/Probe/Diameter").arg(
                    QString::fromStdString(child)), start, end));

        proc.operateConcentration->registerExpected(station, archive);
        proc.inputPower->registerExpected(station, archive);
        proc.inputAirTemperature->registerExpected(station, archive);
        proc.inputAirPressure->registerExpected(station, archive);
        proc.inputAirSpeed->registerExpected(station, archive);
        proc.inputProbeTemperature->registerExpected(station, archive);
        proc.inputProbeLength->registerExpected(station, archive);
        proc.inputProbeDiameter->registerExpected(station, archive);

        processing.emplace_back(std::move(proc));
    }
}


CalcLWCProbe::~CalcLWCProbe() = default;

void CalcLWCProbe::handleNewProcessing(const SequenceName &name,
                                       std::size_t id,
                                       SegmentProcessingStage::SequenceHandlerControl *control)
{
    auto &proc = processing[id];

    SequenceName::Set reg;

    proc.operateConcentration
        ->registerExpected(name.getStation(), name.getArchive(), {}, name.getFlavors());
    Util::merge(proc.operateConcentration->getAllUnits(), reg);

    if (!control)
        return;

    for (const auto &n : reg) {
        bool isNew = !control->isHandled(n);
        control->filterUnit(n, id);
        if (isNew)
            registerPossibleInput(n, control, id);
    }
}

void CalcLWCProbe::registerPossibleInput(const SequenceName &name,
                                         SegmentProcessingStage::SequenceHandlerControl *control,
                                         std::size_t filterID)
{
    for (std::size_t id = 0, max = processing.size(); id < max; id++) {
        bool usedAsInput = false;
        auto &proc = processing[id];
        if (proc.inputPower->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputAirTemperature->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputAirPressure->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputAirSpeed->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputProbeTemperature->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputProbeLength->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputProbeDiameter->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (usedAsInput && id != filterID)
            handleNewProcessing(name, id, control);
    }
    if (defaultAirTemperature && defaultAirTemperature->registerInput(name) && control)
        control->deferHandling(name);
    if (defaultAirPressure && defaultAirPressure->registerInput(name) && control)
        control->deferHandling(name);
    if (defaultAirSpeed && defaultAirSpeed->registerInput(name) && control)
        control->deferHandling(name);
    if (defaultProbeTemperature && defaultProbeTemperature->registerInput(name) && control)
        control->deferHandling(name);
    if (defaultProbeLength && defaultProbeLength->registerInput(name) && control)
        control->deferHandling(name);
    if (defaultProbeDiameter && defaultProbeDiameter->registerInput(name) && control)
        control->deferHandling(name);
}

void CalcLWCProbe::unhandled(const SequenceName &name,
                             SegmentProcessingStage::SequenceHandlerControl *control)
{
    registerPossibleInput(name, control);

    for (std::size_t id = 0, max = processing.size(); id < max; id++) {
        if (processing[id].operateConcentration->registerInput(name)) {
            if (control)
                control->filterUnit(name, id);
            handleNewProcessing(name, id, control);
            return;
        }
    }

    if (restrictedInputs)
        return;

    if (name.hasFlavor("cover") || name.hasFlavor("stats"))
        return;

    auto reCheck = QRegularExpression("^VA(\\d*)_(.+)$").match(name.getVariableQString());
    if (!reCheck.hasMatch())
        return;
    auto suffix = reCheck.captured(2).toStdString();
    if (suffix.empty())
        return;
    if (!filterSuffixes.empty() && !filterSuffixes.count(suffix))
        return;
    auto prefix = reCheck.captured(1).toStdString();

    SequenceName concentrationName
            (name.getStation(), name.getArchive(), "X" + prefix + "_" + suffix, name.getFlavors());

    Processing proc;

    proc.operateConcentration.reset(new DynamicSequenceSelection::Single(concentrationName));
    proc.operateConcentration->registerInput(name);
    proc.operateConcentration->registerInput(concentrationName);

    proc.inputPower.reset(new DynamicInput::Basic(name));
    proc.inputPower->registerInput(name);
    proc.inputPower->registerInput(concentrationName);

    if (defaultAirTemperature) {
        proc.inputAirTemperature.reset(defaultAirTemperature->clone());
    } else {
        proc.inputAirTemperature.reset(new DynamicInput::Constant(0.0));
    }
    proc.inputAirTemperature->registerInput(name);
    proc.inputAirTemperature->registerInput(concentrationName);

    if (defaultAirPressure) {
        proc.inputAirPressure.reset(defaultAirPressure->clone());
    } else {
        proc.inputAirPressure.reset(new DynamicInput::Constant(1013.25));
    }
    proc.inputAirPressure->registerInput(name);
    proc.inputAirPressure->registerInput(concentrationName);

    if (defaultAirSpeed) {
        proc.inputAirSpeed.reset(defaultAirSpeed->clone());
    } else {
        proc.inputAirSpeed.reset(new DynamicInput::Constant(1.0));
    }
    proc.inputAirSpeed->registerInput(name);
    proc.inputAirSpeed->registerInput(concentrationName);

    if (defaultProbeTemperature) {
        proc.inputProbeTemperature.reset(defaultProbeTemperature->clone());
    } else {
        proc.inputProbeTemperature.reset(new DynamicInput::Constant(125.0));
    }
    proc.inputProbeTemperature->registerInput(name);
    proc.inputProbeTemperature->registerInput(concentrationName);

    if (defaultProbeLength) {
        proc.inputProbeLength.reset(defaultProbeLength->clone());
    } else {
        proc.inputProbeLength.reset(new DynamicInput::Constant(2.0));
    }
    proc.inputProbeLength->registerInput(name);
    proc.inputProbeLength->registerInput(concentrationName);

    if (defaultProbeDiameter) {
        proc.inputProbeDiameter.reset(defaultProbeDiameter->clone());
    } else {
        proc.inputProbeDiameter.reset(new DynamicInput::Constant(0.18));
    }
    proc.inputProbeDiameter->registerInput(name);
    proc.inputProbeDiameter->registerInput(concentrationName);

    auto id = processing.size();

    if (control) {
        for (const auto &n : proc.inputPower->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : proc.inputAirTemperature->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : proc.inputAirSpeed->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : proc.inputProbeTemperature->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : proc.inputProbeLength->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : proc.inputProbeDiameter->getUsedInputs()) {
            control->inputUnit(n, id);
        }

        control->inputUnit(name, id);

        control->filterUnit(concentrationName, id);
    }

    processing.emplace_back(std::move(proc));
}

static double calculateConcentration(double airTemperature,
                                     double airPressure,
                                     double airSpeed,
                                     double sensorPower,
                                     double probeTemperature = 125.0,
                                     double probeLength = 2.0,
                                     double probeDiameter = 0.18)
{
    if (!FP::defined(airTemperature))
        return FP::undefined();
    if (airTemperature < 100.0)
        airTemperature += 273.15;
    if (airTemperature <= 0.0)
        return FP::undefined();

    if (!FP::defined(probeTemperature))
        return FP::undefined();
    /* Should be above boiling, so use a different threshold */
    if (probeTemperature < 300.0)
        probeTemperature += 273.15;
    if (probeTemperature <= 0.0)
        return FP::undefined();

    if (!FP::defined(airSpeed) || airSpeed <= 0.0)
        return FP::undefined();
    if (!FP::defined(airPressure) || airPressure < 10.0)
        return FP::undefined();
    if (!FP::defined(probeLength) || probeLength <= 0.0)
        return FP::undefined();
    if (!FP::defined(probeDiameter) || probeDiameter <= 0.0)
        return FP::undefined();

    /* LWC manual, page 12 */
    double TK = airTemperature;
    double PMB = airPressure;
    double TAS = airSpeed;
    double TWK = probeTemperature;
    double L = probeLength;
    double D = probeDiameter;
    double TFLM = (TWK + TK) / 2.0;
    double CND = 5.8E-5 * (398. / (125. + TFLM)) * std::pow(TFLM / 273., 1.5);
    double CNDW = 5.8E-5 * (398. / (125. + TWK)) * std::pow(TWK / 273., 1.5);
    double VISC = 1.718E-4 * (393. / (120. + TFLM)) * std::pow(TFLM / 273., 1.5);
    double VSCW = 1.718E-4 * (393. / (120. + TWK)) * std::pow(TWK / 273., 1.5);
    double DENS = PMB / (2870.5 * TFLM);
    double FCT = M_PI * L * CND * (TWK - TK);
    double RE = 100. * DENS * TAS * D / VISC;
    double PRF = 0.24 * VISC / CND;
    double PRW = 0.24 * VSCW / CNDW;
    double DRYP = 0.26 *
            std::pow(RE, 0.6) *
            std::pow(PRF, 0.37) *
            std::pow(PRF / PRW, 0.25) *
            FCT /
            0.239;
    double FACT = 1.238E6 * 0.239 / (L * D * TAS * 100. * (597.3 + 373.16 - TK));
    double LWC = (sensorPower - DRYP) * FACT;

    return LWC;
}

void CalcLWCProbe::process(int id, SequenceSegment &data)
{
    auto &proc = processing[id];

    double X = calculateConcentration(proc.inputAirTemperature->get(data),
                                      proc.inputAirPressure->get(data),
                                      proc.inputAirSpeed->get(data), proc.inputPower->get(data),
                                      proc.inputProbeTemperature->get(data),
                                      proc.inputProbeLength->get(data),
                                      proc.inputProbeDiameter->get(data));

    for (const auto &i : proc.operateConcentration->get(data)) {
        data[i].setReal(X);
    }
}


void CalcLWCProbe::processMeta(int id, SequenceSegment &data)
{
    auto &proc = processing[id];

    Variant::Root meta;

    meta["By"].setString("calc_lwcprobe");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    meta["Parameters"].hash("VA") = proc.inputPower->describe(data);
    meta["Parameters"].hash("Air").hash("T") = proc.inputAirTemperature->describe(data);
    meta["Parameters"].hash("Air").hash("P") = proc.inputAirPressure->describe(data);
    meta["Parameters"].hash("Air").hash("Qs") = proc.inputAirSpeed->describe(data);
    meta["Parameters"].hash("Probe").hash("T") = proc.inputProbeTemperature->describe(data);
    meta["Parameters"].hash("Probe").hash("L") = proc.inputProbeLength->describe(data);
    meta["Parameters"].hash("Probe").hash("D") = proc.inputProbeDiameter->describe(data);

    for (auto i : proc.operateConcentration->get(data)) {
        i.setMeta();
        if (!data[i].exists()) {
            data[i].metadataReal("Description").setString("Calculated liquid water concentration");
            data[i].metadataReal("Format").setString("000.000");
            data[i].metadataReal("Units").setString("g/m³");
        }
        data[i].metadata("Processing").toArray().after_back().set(meta);
    }
}


SequenceName::Set CalcLWCProbe::requestedInputs()
{
    SequenceName::Set out;
    for (const auto &p : processing) {
        Util::merge(p.inputPower->getUsedInputs(), out);
        Util::merge(p.inputAirTemperature->getUsedInputs(), out);
        Util::merge(p.inputAirPressure->getUsedInputs(), out);
        Util::merge(p.inputAirSpeed->getUsedInputs(), out);
        Util::merge(p.inputProbeTemperature->getUsedInputs(), out);
        Util::merge(p.inputProbeLength->getUsedInputs(), out);
        Util::merge(p.inputProbeDiameter->getUsedInputs(), out);
    }
    return out;
}

SequenceName::Set CalcLWCProbe::predictedOutputs()
{
    SequenceName::Set out;
    for (const auto &p : processing) {
        Util::merge(p.operateConcentration->getAllUnits(), out);
    }
    return out;
}

QSet<double> CalcLWCProbe::metadataBreaks(int id)
{
    const auto &p = processing[id];
    QSet<double> result;
    Util::merge(p.operateConcentration->getChangedPoints(), result);
    Util::merge(p.inputPower->getChangedPoints(), result);
    Util::merge(p.inputAirTemperature->getChangedPoints(), result);
    Util::merge(p.inputAirPressure->getChangedPoints(), result);
    Util::merge(p.inputAirSpeed->getChangedPoints(), result);
    Util::merge(p.inputProbeTemperature->getChangedPoints(), result);
    Util::merge(p.inputProbeLength->getChangedPoints(), result);
    Util::merge(p.inputProbeDiameter->getChangedPoints(), result);
    return result;
}

CalcLWCProbe::CalcLWCProbe(QDataStream &stream)
{
    stream >> defaultAirTemperature;
    stream >> defaultAirPressure;
    stream >> defaultAirSpeed;
    stream >> defaultProbeTemperature;
    stream >> defaultProbeLength;
    stream >> defaultProbeDiameter;
    stream >> filterSuffixes;
    stream >> restrictedInputs;

    Deserialize::container(stream, processing, [&stream]() {
        Processing proc;

        stream >> proc.operateConcentration;
        stream >> proc.inputPower;
        stream >> proc.inputAirTemperature;
        stream >> proc.inputAirPressure;
        stream >> proc.inputAirSpeed;
        stream >> proc.inputProbeTemperature;
        stream >> proc.inputProbeLength;
        stream >> proc.inputProbeDiameter;

        return std::move(proc);
    });
}

void CalcLWCProbe::serialize(QDataStream &stream)
{
    stream << defaultAirTemperature;
    stream << defaultAirPressure;
    stream << defaultAirSpeed;
    stream << defaultProbeTemperature;
    stream << defaultProbeLength;
    stream << defaultProbeDiameter;
    stream << filterSuffixes;
    stream << restrictedInputs;

    Serialize::container(stream, processing, [&stream](const Processing &proc) {
        stream << proc.operateConcentration;
        stream << proc.inputPower;
        stream << proc.inputAirTemperature;
        stream << proc.inputAirPressure;
        stream << proc.inputAirSpeed;
        stream << proc.inputProbeTemperature;
        stream << proc.inputProbeLength;
        stream << proc.inputProbeDiameter;
    });
}


QString CalcLWCProbeComponent::getBasicSerializationName() const
{ return QString::fromLatin1("calc_lwcprobe"); }

ComponentOptions CalcLWCProbeComponent::getOptions()
{
    ComponentOptions options;

    options.add("input-air-t",
                new DynamicInputOption(tr("input-air-t", "name"), tr("Input air temperature"),
                                       tr("This is the air temperature to use in calculations.  This is the ambient air temperature being sampled."),
                                       tr("273.15 K")));

    options.add("input-air-p",
                new DynamicInputOption(tr("input-air-p", "name"), tr("Input pressure"),
                                       tr("This is the air pressure to use in calculations."),
                                       tr("1013.25 hPa")));

    options.add("input-air-speed",
                new DynamicInputOption(tr("input-air-speed", "name"), tr("Input air speed"),
                                       tr("This is the air speed past the sensor used in calculations."),
                                       tr("1 m/s")));

    options.add("input-probe-temperature",
                new DynamicInputOption(tr("input-probe-temperature", "name"), tr("Input air speed"),
                                       tr("This is temperature the probe sensing element is being maintained at."),
                                       tr("125 \xC2\xB0\x43")));

    options.add("input-probe-length",
                new DynamicInputOption(tr("input-probe-length", "name"), tr("Input probe length"),
                                       tr("This is the length of the sensing element in the probe."),
                                       tr("2.0 cm")));

    options.add("input-probe-diameter", new DynamicInputOption(tr("input-probe-diameter", "name"),
                                                               tr("Input probe diameter"),
                                                               tr("This is the diameter of the sensing element in the probe."),
                                                               tr("0.18 cm")));

    options.add("input-power",
                new DynamicInputOption(tr("input-power", "name"), tr("Sensor power consumption"),
                                       tr("This is the power in watts being consumed by the sensing element.  This option is mutually exclusive with with instrument specification."),
                                       tr("All powers")));

    options.add("output-concentration",
                new DynamicSequenceSelectionOption(tr("output-concentration", "name"),
                                                   tr("Output concentration"),
                                                   tr("This is the output concentration that is calculated.  This option is "
                                                      "mutually exclusive with with instrument specification."),
                                                   tr("Calculated from input powers")));


    options.add("suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments", "name"),
                                                                 tr("Instrument suffixes to correct"),
                                                                 tr("These are the instrument suffixes to calculate concentrations for.  "
                                                                    "This option is mutually exclusive with manual "
                                                                    "variable specification."),
                                                                 tr("All instrument suffixes")));
    options.exclude("input-power", "suffix");
    options.exclude("output-concentration", "suffix");
    options.exclude("suffix", "input-power");
    options.exclude("suffix", "output-concentration");

    return options;
}

QList<ComponentExample> CalcLWCProbeComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will calculate concentrations for all power readings in the input using assumed defaults.  For example, "
                                        "it will calculate X_N11 from VA_N11.")));

    options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")))->add("N11");
    (qobject_cast<DynamicInputOption *>(options.get("input-air-t")))->set(
            SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("T_V01")), Calibration());
    (qobject_cast<DynamicInputOption *>(options.get("input-air-p")))->set(
            SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("P_XM")), Calibration());
    (qobject_cast<DynamicInputOption *>(options.get("input-air-speed")))->set(10.0);
    examples.append(ComponentExample(options,
                                     tr("Single instrument with measured temperature and pressure"),
                                     tr("This will calculate the concentration for the N11 instrument using "
                                        "temperature T_V01 and the pressure P_XM and assumed air speed.")));

    options = getOptions();
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-concentration")))->set("",
                                                                                               "",
                                                                                               "X_X1");
    (qobject_cast<DynamicInputOption *>(options.get("input-power")))->set(
            SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("VA_XM")), Calibration());
    examples.append(ComponentExample(options, tr("Single variable"),
                                     tr("This will calculate X_X1 from VA_XM for assumed probe parameters.")));

    return examples;
}

SegmentProcessingStage *CalcLWCProbeComponent::createBasicFilterDynamic(const ComponentOptions &options)
{ return new CalcLWCProbe(options); }

SegmentProcessingStage *CalcLWCProbeComponent::createBasicFilterPredefined(const ComponentOptions &options,
                                                                           double start,
                                                                           double end,
                                                                           const QList<
                                                                                   SequenceName> &inputs)
{ return new CalcLWCProbe(options, start, end, inputs); }

SegmentProcessingStage *CalcLWCProbeComponent::createBasicFilterEditing(double start,
                                                                        double end,
                                                                        const SequenceName::Component &station,
                                                                        const SequenceName::Component &archive,
                                                                        const ValueSegment::Transfer &config)
{ return new CalcLWCProbe(start, end, station, archive, config); }

SegmentProcessingStage *CalcLWCProbeComponent::deserializeBasicFilter(QDataStream &stream)
{ return new CalcLWCProbe(stream); }
