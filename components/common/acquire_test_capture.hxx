/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIRE_TEST_CAPTURE_HXX
#define ACQUIRE_TEST_CAPTURE_HXX

#include "core/first.hxx"

#include <mutex>
#include <QTest>

#include "datacore/stream.hxx"
#include "core/range.hxx"
#include "core/number.hxx"

class StreamCapture : public CPD3::Data::StreamSink::Buffer {
    std::mutex mutex;

    static bool doubleCompare(double a, double b, double epsilon)
    {
        if (!CPD3::FP::defined(epsilon))
            return qFuzzyCompare(a, b);

        double delta = std::max(std::abs(a), std::abs(b)) * epsilon;
        return std::abs(a - b) <= delta;
    }

    static bool valueCompare(const CPD3::Data::Variant::Read &v,
                             const CPD3::Data::Variant::Read &expected,
                             const QString &child = QString(),
                             double epsilon = CPD3::FP::undefined())
    {
        auto ref = v.getPath(child);
        if (ref.getType() == CPD3::Data::Variant::Type::Real &&
                expected.getType() == CPD3::Data::Variant::Type::Real) {
            double a = ref.toDouble();
            double b = expected.toDouble();
            if (!CPD3::FP::defined(a)) {
                return !CPD3::FP::defined(b);
            } else if (!CPD3::FP::defined(b)) {
                return false;
            }
            return doubleCompare(a, b, epsilon);
        } else if (ref.getType() == CPD3::Data::Variant::Type::Array &&
                expected.getType() == CPD3::Data::Variant::Type::Array) {
            if (ref.toArray().size() != expected.toArray().size())
                return false;
            for (std::size_t i = 0, max = ref.toArray().size(); i < max; i++) {
                if (!valueCompare(ref.array(i), expected.array(i), QString(), epsilon))
                    return false;
            }
            return true;
        }

        return ref == expected;
    }

    static double timeCompare(double a, double b, double delta)
    {
        if (!CPD3::FP::defined(a)) {
            return !CPD3::FP::defined(b);
        } else if (!CPD3::FP::defined(b)) {
            return false;
        }

        if (!CPD3::FP::defined(delta))
            return a == b;

        return std::fabs(a - b) <= delta;
    }

public:

    StreamCapture() = default;

    virtual ~StreamCapture() = default;

    void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        CPD3::Data::StreamSink::Buffer::incomingData(values);
    }

    void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        CPD3::Data::StreamSink::Buffer::incomingData(std::move(values));
    }

    void incomingData(const CPD3::Data::SequenceValue &value) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        CPD3::Data::StreamSink::Buffer::incomingData(value);
    }

    void incomingData(CPD3::Data::SequenceValue &&value) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        CPD3::Data::StreamSink::Buffer::incomingData(std::move(value));
    }

    void endData() override
    {
        std::lock_guard<std::mutex> lock(mutex);
        CPD3::Data::StreamSink::Buffer::endData();
    }

    static bool findValue(const CPD3::Data::SequenceValue::Transfer &values,
                          const CPD3::Data::SequenceName::Component &archive,
                          const CPD3::Data::SequenceName::Component &name,
                          const CPD3::Data::Variant::Read &expected,
                          const QString &child = QString(),
                          double epsilon = CPD3::FP::undefined(),
                          double checkStart = CPD3::FP::undefined(),
                          double checkEnd = CPD3::FP::undefined())
    {
        for (const auto &v : values) {
            if (v.getVariable() != name)
                continue;
            if (v.getArchive() != archive)
                continue;
            if ((CPD3::FP::defined(checkStart) || CPD3::FP::defined(checkEnd)) &&
                    (!CPD3::FP::equal(checkStart, v.getStart()) ||
                            !CPD3::FP::equal(checkEnd, v.getEnd())))
                continue;
            if (expected.exists()) {
                if (!valueCompare(v.getValue(), expected, child, epsilon))
                    continue;
            }
            return true;
        }
        return false;
    }

    bool hasAnyMatchingValue(const CPD3::Data::SequenceName::Component &name,
                             const CPD3::Data::Variant::Read &expected = CPD3::Data::Variant::Read::empty(),
                             double timeAfter = CPD3::FP::undefined(),
                             const QString &child = QString(),
                             double epsilon = CPD3::FP::undefined()) const
    {
        std::lock_guard<std::mutex> lock(const_cast<StreamCapture *>(this)->mutex);
        for (const auto &v : values()) {
            if (v.getVariable() != name)
                continue;
            if (v.getArchive() != "raw")
                continue;
            if (CPD3::FP::defined(timeAfter) &&
                    CPD3::FP::defined(v.getStart()) &&
                    v.getStart() < timeAfter)
                continue;
            if (expected.exists()) {
                if (!valueCompare(v.getValue(), expected, child, epsilon))
                    continue;
            }
            return true;
        }
        return false;
    }

    bool hasAnyMatchingType(const CPD3::Data::SequenceName::Component &name,
                            CPD3::Data::Variant::Type expected,
                            double timeAfter = CPD3::FP::undefined()) const
    {
        std::lock_guard<std::mutex> lock(const_cast<StreamCapture *>(this)->mutex);
        for (const auto &v : values()) {
            if (v.getVariable() != name)
                continue;
            if (v.getArchive() != "raw")
                continue;
            if (CPD3::FP::defined(timeAfter) &&
                    CPD3::FP::defined(v.getStart()) &&
                    v.getStart() < timeAfter)
                continue;
            if (v.getValue().getType() != expected)
                continue;
            return true;
        }
        return false;
    }

    bool hasAnyMatchingMetaValue(const CPD3::Data::SequenceName::Component &name,
                                 const CPD3::Data::Variant::Read &expected = CPD3::Data::Variant::Read::empty(),
                                 double timeAfter = CPD3::FP::undefined(),
                                 const QString &child = QString()) const
    {
        std::lock_guard<std::mutex> lock(const_cast<StreamCapture *>(this)->mutex);
        for (const auto &v : values()) {
            if (v.getVariable() != name)
                continue;
            if (v.getArchive() != "raw_meta")
                continue;
            if (CPD3::FP::defined(timeAfter) &&
                    CPD3::FP::defined(v.getStart()) &&
                    v.getStart() < timeAfter)
                continue;
            if (v.getValue().getPath(child) != expected)
                continue;
            return true;
        }
        return false;
    }

    bool hasMeta(const CPD3::Data::SequenceName::Component &name,
                 const CPD3::Data::Variant::Read &expected,
                 const QString &child = QString(),
                 double checkStart = CPD3::FP::undefined(),
                 double checkEnd = CPD3::FP::undefined(),
                 double epsilon = CPD3::FP::undefined()) const
    {
        std::lock_guard<std::mutex> lock(const_cast<StreamCapture *>(this)->mutex);
        return findValue(values(), "raw_meta", name, expected, child, epsilon, checkStart,
                         checkEnd);
    }

    bool hasValue(const CPD3::Data::SequenceName::Component &name,
                  const CPD3::Data::Variant::Read &expected = CPD3::Data::Variant::Read::empty(),
                  const QString &child = QString(),
                  double checkStart = CPD3::FP::undefined(),
                  double checkEnd = CPD3::FP::undefined(),
                  double epsilon = CPD3::FP::undefined()) const
    {
        std::lock_guard<std::mutex> lock(const_cast<StreamCapture *>(this)->mutex);
        return findValue(values(), "raw", name, expected, child, epsilon, checkStart, checkEnd);
    }

    CPD3::Data::Variant::Read getLatestValue(const CPD3::Data::SequenceName::Component &name,
                                             double timeAfter = CPD3::FP::undefined(),
                                             const CPD3::Data::SequenceName::Component &archive = "raw") const
    {
        std::lock_guard<std::mutex> lock(const_cast<StreamCapture *>(this)->mutex);
        const auto &val = this->values();
        for (auto v = val.crbegin(), endV = val.crend(); v != endV; ++v) {
            if (v->getVariable() != name)
                continue;
            if (v->getArchive() != archive)
                continue;
            if (CPD3::FP::defined(timeAfter) &&
                    CPD3::FP::defined(v->getStart()) &&
                    v->getStart() < timeAfter)
                continue;
            return v->read();
        }
        return CPD3::Data::Variant::Read::empty();
    }

    template<typename ListType>
    bool verifyValues(const CPD3::Data::SequenceName::Component &name,
                      const ListType &times,
                      const ListType &values,
                      bool checkContiguous = true,
                      double epsilon = CPD3::FP::undefined(),
                      double timeDelta = CPD3::FP::undefined()) const
    {
        std::lock_guard<std::mutex> lock(const_cast<StreamCapture *>(this)->mutex);
        Q_ASSERT(times.size() >= values.size());
        decltype(values.size()) index = 0;
        for (const auto &v : this->values()) {
            if (v.getVariable() != name)
                continue;
            if (v.getArchive() != "raw")
                continue;

            if (!timeCompare(v.getStart(), times.at(index), timeDelta))
                continue;
            if (checkContiguous &&
                    index != times.size() - 1 &&
                    !timeCompare(v.getEnd(), times.at(index + 1), timeDelta))
                return false;
            if (!CPD3::FP::defined(v.getValue().toDouble())) {
                if (CPD3::FP::defined(values.at(index)))
                    return false;
            } else {
                if (!doubleCompare(v.getValue().toDouble(), values.at(index), epsilon))
                    return false;
            }

            ++index;
            if (index >= values.size())
                break;
        }
        if (index != values.size())
            return false;
        return true;
    }

    double latestTime() const
    {
        std::lock_guard<std::mutex> lock(const_cast<StreamCapture *>(this)->mutex);
        if (values().empty())
            return CPD3::FP::undefined();
        return values().back().getStart();
    }

    bool checkAscending() const
    {
        std::lock_guard<std::mutex> lock(const_cast<StreamCapture *>(this)->mutex);
        const auto &values = this->values();
        if (values.empty())
            return true;
        for (auto v = values.cbegin() + 1, pv = values.cbegin(), end = values.cend();
                v != end;
                pv = v, ++v) {
            if (CPD3::Range::compareStart(v->getStart(), pv->getStart()) < 0)
                return false;
        }
        return true;
    }

    bool checkContiguous(const CPD3::Data::SequenceName::Component &name) const
    {
        std::lock_guard<std::mutex> lock(const_cast<StreamCapture *>(this)->mutex);
        double lastTime = CPD3::FP::undefined();
        for (const auto &v : values()) {
            if (v.getVariable() != name)
                continue;
            if (v.getArchive() != "raw")
                continue;
            if (!CPD3::FP::defined(lastTime)) {
                lastTime = v.getEnd();
                continue;
            }
            if (CPD3::Range::compareStartEnd(v.getStart(), lastTime) != 0)
                return false;
            lastTime = v.getEnd();
        }
        return true;
    }
};


#endif