/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIRE_TEST_CONTROL_HXX
#define ACQUIRE_TEST_CONTROL_HXX

#include "core/first.hxx"

#include <mutex>
#include <QByteArray>
#include <QString>

#include "acquisition/acquisitioncomponent.hxx"
#include "datacore/stream.hxx"
#include "core/number.hxx"

template<typename ModelInstrument>
class ModelInstrumentControl
        : public CPD3::Acquisition::AcquisitionControlStream,
          public CPD3::Acquisition::AcquisitionState {
    double requestedTimeout;
    double requestedWakeup;
    std::mutex mutex;

    CPD3::Acquisition::AcquisitionInterface *interface;
    double currentTime;
    ModelInstrument &instrument;

    CPD3::Acquisition::AcquisitionInterface::IncomingDataType advanceType;

protected:
    virtual void executeAdvance(double seconds, ModelInstrument &instrument)
    {
        instrument.advance(seconds);
    }

public:

    ModelInstrumentControl(ModelInstrument &instrument,
                           CPD3::Acquisition::AcquisitionInterface *interface,
                           double time = 0.0,
                           CPD3::Acquisition::AcquisitionInterface::IncomingDataType advanceType = CPD3::Acquisition::AcquisitionInterface::IncomingDataType::Stream)
            : requestedTimeout(CPD3::FP::undefined()),
              requestedWakeup(CPD3::FP::undefined()),
              mutex(),
              interface(interface),
              currentTime(time),
              instrument(instrument),
              advanceType(advanceType)
    { }

    void attach(CPD3::Acquisition::AcquisitionInterface *interface)
    { this->interface = interface; }

    virtual ~ModelInstrumentControl() = default;

    void writeControl(const CPD3::Util::ByteView &data) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        instrument.incoming.append(data.toQByteArray());
    }

    void resetControl() override
    {
        std::lock_guard<std::mutex> lock(mutex);
        instrument.incoming.clear();
    }

    void requestTimeout(double time) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        requestedTimeout = time;
    }

    void setBypassFlag(const CPD3::Data::Variant::Flag &) override
    { }

    void clearBypassFlag(const CPD3::Data::Variant::Flag &) override
    { }

    void setSystemFlag(const CPD3::Data::Variant::Flag &) override
    { }

    void clearSystemFlag(const CPD3::Data::Variant::Flag &) override
    { }

    void requestFlush(double) override
    { }

    void setSystemFlavors(const CPD3::Data::SequenceName::Flavors &) override
    { }

    void setAveragingTime(CPD3::Time::LogicalTimeUnit, int, bool) override
    { }

    void sendCommand(const std::string &, const CPD3::Data::Variant::Read &) override
    { }

    void requestStateSave() override
    { }

    void requestGlobalStateSave() override
    { }

    void event(double, const QString &, bool, const CPD3::Data::Variant::Read &) override
    { }

    void requestDataWakeup(double time) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        requestedWakeup = time;
    }

    void externalControl(const QByteArray &add)
    {
        std::unique_lock<std::mutex> lock(mutex);
        instrument.incoming.append(add);
        double now = currentTime;
        lock.unlock();
        interface->incomingControl(add, now, advanceType);
    }

    void advance(double seconds)
    {
        std::unique_lock<std::mutex> lock(mutex);
        currentTime += seconds;
        double now = currentTime;
        executeAdvance(seconds, instrument);
        if (!instrument.outgoing.isEmpty()) {
            QByteArray data = std::move(instrument.outgoing);
            instrument.outgoing.clear();
            lock.unlock();
            interface->incomingData(data, now, advanceType);
        } else if (CPD3::FP::defined(requestedWakeup) && currentTime >= requestedWakeup) {
            requestedWakeup = CPD3::FP::undefined();
            lock.unlock();
            interface->incomingData(QByteArray(), now);
        } else if (CPD3::FP::defined(requestedTimeout) && currentTime >= requestedTimeout) {
            requestedTimeout = CPD3::FP::undefined();
            lock.unlock();
            interface->incomingTimeout(now);
        } else {
            lock.unlock();
            interface->incomingAdvance(now);
        }
    }

    double time()
    {
        std::lock_guard<std::mutex> lock(mutex);
        return currentTime;
    }

    template<typename F>
    auto instrumentOperation(F f) -> decltype(f(instrument))
    {
        std::lock_guard<std::mutex> lock(mutex);
        return f(instrument);
    }
};

#endif
