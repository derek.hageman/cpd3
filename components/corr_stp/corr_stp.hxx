/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CORRSTP_H
#define CORRSTP_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QList>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"

class CorrSTP : public CPD3::Data::SegmentProcessingStage {
    class Processing {
    public:
        CPD3::Data::DynamicSequenceSelection *operateInverse;
        CPD3::Data::DynamicSequenceSelection *operateForward;
        CPD3::Data::DynamicSequenceSelection *operateFlags;

        CPD3::Data::DynamicInput *sampleT;
        CPD3::Data::DynamicInput *sampleP;
        CPD3::Data::DynamicInput *standardT;
        CPD3::Data::DynamicInput *standardP;

        Processing() : operateInverse(NULL),
                       operateForward(NULL),
                       operateFlags(NULL),
                       sampleT(NULL),
                       sampleP(NULL),
                       standardT(NULL),
                       standardP(NULL)
        { }
    };

    CPD3::Data::DynamicInput *defaultSampleT;
    CPD3::Data::DynamicInput *defaultSampleP;
    CPD3::Data::DynamicInput *defaultStandardT;
    CPD3::Data::DynamicInput *defaultStandardP;
    CPD3::Data::SequenceName::ComponentSet filterSuffixes;

    bool restrictedInputs;

    void handleOptions(const CPD3::ComponentOptions &options);

    std::vector<Processing> processing;

public:
    CorrSTP();

    CorrSTP(const CPD3::ComponentOptions &options);

    CorrSTP(const CPD3::ComponentOptions &options,
            double start, double end, const QList<CPD3::Data::SequenceName> &inputs);

    CorrSTP(double start,
            double end,
            const CPD3::Data::SequenceName::Component &station,
            const CPD3::Data::SequenceName::Component &archive,
            const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CorrSTP();

    void unhandled(const CPD3::Data::SequenceName &unit,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    QSet<double> metadataBreaks(int id) override;

    CorrSTP(QDataStream &stream);

    void serialize(QDataStream &stream) override;
};

class CorrSTPComponent
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.corr_stp"
                              FILE
                              "corr_stp.json")

public:
    virtual QString getBasicSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Data::SegmentProcessingStage
            *createBasicFilterDynamic(const CPD3::ComponentOptions &options);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined
            (const CPD3::ComponentOptions &options,
             double start,
             double end, const QList<CPD3::Data::SequenceName> &inputs);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                         double end,
                                                                         const CPD3::Data::SequenceName::Component &station,
                                                                         const CPD3::Data::SequenceName::Component &archive,
                                                                         const CPD3::Data::ValueSegment::Transfer &config);

    virtual CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream);
};

#endif
