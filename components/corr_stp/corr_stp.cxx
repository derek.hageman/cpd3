/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QRegExp>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/variant/composite.hxx"
#include "core/environment.hxx"

#include "corr_stp.hxx"

using namespace CPD3;
using namespace CPD3::Data;

void CorrSTP::handleOptions(const ComponentOptions &options)
{
    if (options.isSet("t")) {
        defaultSampleT = qobject_cast<DynamicInputOption *>(options.get("t"))->getInput();
    } else {
        defaultSampleT = NULL;
    }
    if (options.isSet("p")) {
        defaultSampleP = qobject_cast<DynamicInputOption *>(options.get("p"))->getInput();
    } else {
        defaultSampleP = NULL;
    }

    if (options.isSet("stp-t")) {
        defaultStandardT = qobject_cast<DynamicInputOption *>(options.get("stp-t"))->getInput();
    } else {
        defaultStandardT = new DynamicInput::Constant(0.0);
    }
    if (options.isSet("stp-p")) {
        defaultStandardP = qobject_cast<DynamicInputOption *>(options.get("stp-p"))->getInput();
    } else {
        defaultStandardP = new DynamicInput::Constant(1013.25);
    }

    restrictedInputs = false;

    if (options.isSet("forward") || options.isSet("inverse")) {
        restrictedInputs = true;

        Processing p;

        SequenceName::Component station;
        SequenceName::Component archive;
        SequenceName::Component suffix;
        SequenceName::Flavors flavors;
        if (options.isSet("inverse")) {
            p.operateInverse = qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("inverse"))->getOperator();
            for (const auto &name : p.operateInverse->getAllUnits()) {
                suffix = Util::suffix(name.getVariable(), '_');
                station = name.getStation();
                archive = name.getArchive();
                flavors = name.getFlavors();
                if (suffix.length() != 0)
                    break;
            }
        } else {
            p.operateInverse = new DynamicSequenceSelection::None();
        }
        if (options.isSet("forward")) {
            p.operateForward = qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("forward"))->getOperator();

            if (suffix.length() == 0) {
                for (const auto &name : p.operateForward->getAllUnits()) {
                    suffix = Util::suffix(name.getVariable(), '_');
                    station = name.getStation();
                    archive = name.getArchive();
                    flavors = name.getFlavors();
                    if (suffix.length() != 0)
                        break;
                }
            }
        } else {
            p.operateForward = new DynamicSequenceSelection::None();
        }

        p.operateFlags = new DynamicSequenceSelection::Single(
                SequenceName(station, archive, "F1_" + suffix, flavors));

        if (defaultSampleT != NULL) {
            p.sampleT = defaultSampleT->clone();
        } else {
            p.sampleT =
                    new DynamicInput::Basic(SequenceName(station, archive, "T_" + suffix, flavors));
        }
        if (defaultSampleP != NULL) {
            p.sampleP = defaultSampleP->clone();
        } else {
            p.sampleP =
                    new DynamicInput::Basic(SequenceName(station, archive, "P_" + suffix, flavors));
        }
        p.standardT = defaultStandardT->clone();
        p.standardP = defaultStandardP->clone();

        processing.push_back(p);
    }

    if (options.isSet("suffix")) {
        filterSuffixes = Util::set_from_qstring(qobject_cast<ComponentOptionInstrumentSuffixSet *>(
                options.get("suffix"))->get());
    }
}


CorrSTP::CorrSTP()
{ Q_ASSERT(false); }

CorrSTP::CorrSTP(const ComponentOptions &options)
{
    handleOptions(options);
}

CorrSTP::CorrSTP(const ComponentOptions &options,
                 double start,
                 double end,
                 const QList<SequenceName> &inputs)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    handleOptions(options);

    for (QList<SequenceName>::const_iterator unit = inputs.constBegin(), endC = inputs.constEnd();
            unit != endC;
            ++unit) {
        CorrSTP::unhandled(*unit, NULL);
    }
}

CorrSTP::CorrSTP(double start,
                 double end,
                 const SequenceName::Component &station,
                 const SequenceName::Component &archive,
                 const ValueSegment::Transfer &config)
{
    defaultSampleT = NULL;
    defaultSampleP = NULL;
    defaultStandardT = NULL;
    defaultStandardP = NULL;
    restrictedInputs = true;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    for (const auto &child : children) {
        Processing p;

        p.operateInverse = DynamicSequenceSelection::fromConfiguration(config,
                                                                       QString("%1/CorrectConcentrations")
                                                                               .arg(QString::fromStdString(
                                                                                       child)),
                                                                       start, end);
        p.operateForward = DynamicSequenceSelection::fromConfiguration(config,
                                                                       QString("%1/CorrectVolumes").arg(
                                                                               QString::fromStdString(
                                                                                       child)),
                                                                       start, end);
        p.operateFlags = DynamicSequenceSelection::fromConfiguration(config,
                                                                     QString("%1/Flags").arg(
                                                                             QString::fromStdString(
                                                                                     child)), start,
                                                                     end);

        p.sampleT = DynamicInput::fromConfiguration(config, QString("%1/SampleT").arg(
                QString::fromStdString(child)), start, end);
        p.sampleP = DynamicInput::fromConfiguration(config, QString("%1/SampleP").arg(
                QString::fromStdString(child)), start, end);

        p.standardT = DynamicInput::fromConfiguration(config, QString("%1/StandardT").arg(
                QString::fromStdString(child)), start, end);
        p.standardP = DynamicInput::fromConfiguration(config, QString("%1/StandardP").arg(
                QString::fromStdString(child)), start, end);

        p.operateInverse->registerExpected(station, archive);
        p.operateForward->registerExpected(station, archive);
        p.operateFlags->registerExpected(station, archive);
        p.sampleT->registerExpected(station, archive);
        p.sampleP->registerExpected(station, archive);
        p.standardT->registerExpected(station, archive);
        p.standardP->registerExpected(station, archive);

        processing.push_back(p);
    }
}


CorrSTP::~CorrSTP()
{
    if (defaultSampleT != NULL)
        delete defaultSampleT;
    if (defaultSampleP != NULL)
        delete defaultSampleP;
    if (defaultStandardT != NULL)
        delete defaultStandardT;
    if (defaultStandardP != NULL)
        delete defaultStandardP;

    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        delete p->sampleT;
        delete p->sampleP;
        delete p->standardT;
        delete p->standardP;
        delete p->operateInverse;
        delete p->operateForward;
        delete p->operateFlags;
    }
}

void CorrSTP::unhandled(const SequenceName &unit,
                        SegmentProcessingStage::SequenceHandlerControl *control)
{
    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].sampleT->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
        }
        if (processing[id].sampleP->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
        }
        if (processing[id].standardT->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
        }
        if (processing[id].standardP->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
        }
    }
    if (defaultSampleT != NULL && defaultSampleT->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultSampleP != NULL && defaultSampleP->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultStandardT != NULL && defaultStandardT->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultStandardP != NULL && defaultStandardP->registerInput(unit) && control != NULL)
        control->deferHandling(unit);

    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].operateInverse->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            return;
        }
        if (processing[id].operateForward->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            return;
        }
        if (processing[id].operateFlags->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            return;
        }
    }

    if (restrictedInputs)
        return;

    if (unit.hasFlavor(SequenceName::flavor_cover) || unit.hasFlavor(SequenceName::flavor_stats))
        return;

    auto suffix = Util::suffix(unit.getVariable(), '_');
    if (suffix.empty())
        return;
    if (!filterSuffixes.empty() && !filterSuffixes.count(suffix))
        return;

    if (!Util::starts_with(unit.getVariable(), "B") &&
            !Util::starts_with(unit.getVariable(), "N") &&
            !Util::starts_with(unit.getVariable(), "X") &&
            !Util::starts_with(unit.getVariable(), "Q")) {
        if (control != NULL) {
            if (Util::starts_with(unit.getVariable(), "F1_")) {
                control->deferHandling(unit);
            }
        }
        return;
    }

    Processing p;

    p.operateInverse =
            new DynamicSequenceSelection::Match(unit.getStationQString(), unit.getArchiveQString(),
                                                QString("(?:[BX][^n_]*_%1)|(?:[BX][^n][^_]*_%1)|(?:[N](?:(?:ns?)|b|v)?[^mnsbv_]*_%1)")
                                                        .arg(QRegExp::escape(
                                                                QString::fromStdString(suffix))),
                                                unit.getFlavors());
    p.operateInverse->registerInput(unit);
    p.operateForward =
            new DynamicSequenceSelection::Match(unit.getStationQString(), unit.getArchiveQString(),
                                                QString("(?:Q[^n_]*_%1)|(?:Q[^n][^_]*_%1)|(?:[N]mv?[^mnsbv_]*_%1)")
                                                        .arg(QRegExp::escape(
                                                                QString::fromStdString(suffix))),
                                                unit.getFlavors());
    p.operateInverse->registerInput(unit);

    SequenceName flagUnit(unit.getStation(), unit.getArchive(), "F1_" + suffix, unit.getFlavors());
    p.operateFlags = new DynamicSequenceSelection::Single(flagUnit);
    p.operateFlags->registerInput(unit);
    p.operateFlags->registerInput(flagUnit);

    if (defaultSampleT != NULL) {
        p.sampleT = defaultSampleT->clone();
    } else {
        p.sampleT = new DynamicInput::Basic(
                SequenceName(unit.getStation(), unit.getArchive(), "T_" + suffix,
                             unit.getFlavors()));
    }
    if (defaultSampleP != NULL) {
        p.sampleP = defaultSampleP->clone();
    } else {
        p.sampleP = new DynamicInput::Basic(
                SequenceName(unit.getStation(), unit.getArchive(), "P_" + suffix,
                             unit.getFlavors()));
    }
    p.standardT = defaultStandardT->clone();
    p.standardP = defaultStandardP->clone();

    int id = (int) processing.size();
    processing.push_back(p);

    if (control) {
        SequenceName::Set add;
        Util::merge(p.sampleT->getUsedInputs(), add);
        Util::merge(p.sampleP->getUsedInputs(), add);
        Util::merge(p.standardT->getUsedInputs(), add);
        Util::merge(p.standardP->getUsedInputs(), add);
        for (const auto &n : add) {
            control->inputUnit(n, id);
        }

        control->filterUnit(unit, id);
        if (unit != flagUnit)
            control->filterUnit(flagUnit, id);
    }
}

static const Variant::Flag FLAG_NAME = "STP";

void CorrSTP::process(int id, SequenceSegment &data)
{
    Processing *proc = &processing[id];

    double t = proc->sampleT->get(data);
    double p = proc->sampleP->get(data);
    double stpt = proc->standardT->get(data);
    double stpp = proc->standardP->get(data);

    if (FP::defined(t) && t < 150.0) t += 273.15;
    if (FP::defined(stpt) && stpt < 150.0) stpt += 273.15;

    for (const auto &i : proc->operateFlags->get(data)) {
        if (!data.exists(i))
            continue;
        auto d = data[i];
        if (!d.exists())
            continue;
        d.applyFlag(FLAG_NAME);
    }

    if (!FP::defined(t) ||
            !FP::defined(p) ||
            !FP::defined(stpt) ||
            !FP::defined(stpp) ||
            t < 100.0 ||
            t > 350.0 ||
            p < 10.0 ||
            p > 2000.0 ||
            stpt < 100.0 ||
            stpt > 350.0 ||
            stpp < 10.0 ||
            stpp > 2000.0) {
        for (const auto &i : proc->operateInverse->get(data)) {
            if (!data.exists(i))
                continue;
            data[i].clear();
        }
        for (const auto &i : proc->operateForward->get(data)) {
            if (!data.exists(i))
                continue;
            data[i].clear();
        }
        return;
    }

    double den = (p / stpp) * (stpt / t);

    for (const auto &i : proc->operateInverse->get(data)) {
        if (!data.exists(i))
            continue;
        Variant::Composite::applyInplace(data[i], [=](Variant::Write &d) {
            double v = d.toDouble();
            if (FP::defined(v))
                d.setReal(v / den);
            else
                Variant::Composite::invalidate(d);
        });
    }
    for (const auto &i : proc->operateForward->get(data)) {
        if (!data.exists(i))
            continue;
        Variant::Composite::applyInplace(data[i], [=](Variant::Write &d) {
            double v = d.toDouble();
            if (FP::defined(v))
                d.setReal(v * den);
            else
                Variant::Composite::invalidate(d);
        });
    }
}

void CorrSTP::processMeta(int id, SequenceSegment &data)
{
    Processing *p = &processing[id];

    Variant::Root meta;

    meta["By"].setString("corr_stp");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    Variant::Root reportT(p->standardT->describe(data));
    Variant::Root reportP(p->standardP->describe(data));

    meta["Parameters"].hash("StandardT").set(reportT);
    meta["Parameters"].hash("StandardP").set(reportP);
    meta["Parameters"].hash("SampleT") = p->sampleT->describe(data);
    meta["Parameters"].hash("SampleP") = p->sampleP->describe(data);

    double check = p->standardT->constant(data);
    if (FP::defined(check))
        reportT.write().setDouble(check);
    check = p->standardP->constant(data);
    if (FP::defined(check))
        reportP.write().setDouble(check);

    for (const auto &i : p->operateInverse->get(data)) {
        SequenceName dvu = i.toMeta();
        if (!data.exists(dvu))
            continue;
        data[dvu].metadata("Processing").toArray().after_back().set(meta);
        data[dvu].metadata("ReportT").set(reportT);
        data[dvu].metadata("ReportP").set(reportP);
        clearPropagatedSmoothing(data[dvu].metadata("Smoothing"));
    }
    for (const auto &i : p->operateForward->get(data)) {
        SequenceName dvu = i.toMeta();
        if (!data.exists(dvu))
            continue;
        data[dvu].metadata("Processing").toArray().after_back().set(meta);
        data[dvu].metadata("ReportT").set(reportT);
        data[dvu].metadata("ReportP").set(reportP);
        clearPropagatedSmoothing(data[dvu].metadata("Smoothing"));
    }

    for (const auto &i : p->operateFlags->get(data)) {
        if (!data.exists(i.toMeta()))
            continue;
        auto flagMeta = data[i.toMeta()].metadataSingleFlag(FLAG_NAME);
        flagMeta.hash("Origin").toArray().after_back().setString("corr_stp");
        flagMeta.hash("Bits").setInt64(0x0200);
        flagMeta.hash("Description").setString("STP correction applied");
    }
}


SequenceName::Set CorrSTP::requestedInputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        Util::merge(p->sampleT->getUsedInputs(), out);
        Util::merge(p->sampleP->getUsedInputs(), out);
        Util::merge(p->standardT->getUsedInputs(), out);
        Util::merge(p->standardP->getUsedInputs(), out);
        Util::merge(p->operateForward->getAllUnits(), out);
        Util::merge(p->operateInverse->getAllUnits(), out);
        Util::merge(p->operateFlags->getAllUnits(), out);
    }
    return out;
}

QSet<double> CorrSTP::metadataBreaks(int id)
{
    Processing *p = &processing[id];
    return p->operateForward->getChangedPoints() |
            p->operateInverse->getChangedPoints() |
            p->operateFlags->getChangedPoints() |
            p->standardT->getChangedPoints() |
            p->standardP->getChangedPoints();
}

CorrSTP::CorrSTP(QDataStream &stream)
{
    stream >> defaultSampleT;
    stream >> defaultSampleP;
    stream >> defaultStandardT;
    stream >> defaultStandardP;
    stream >> filterSuffixes;
    stream >> restrictedInputs;

    quint32 n;
    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        Processing p;
        stream >> p.operateInverse;
        stream >> p.operateForward;
        stream >> p.operateFlags;
        stream >> p.sampleT;
        stream >> p.sampleP;
        stream >> p.standardT;
        stream >> p.standardP;
        processing.push_back(p);
    }
}

void CorrSTP::serialize(QDataStream &stream)
{
    stream << defaultSampleT;
    stream << defaultSampleP;
    stream << defaultStandardT;
    stream << defaultStandardP;
    stream << filterSuffixes;
    stream << restrictedInputs;

    quint32 n = (quint32) processing.size();
    stream << n;
    for (int i = 0; i < (int) n; i++) {
        stream << processing[i].operateInverse;
        stream << processing[i].operateForward;
        stream << processing[i].operateFlags;
        stream << processing[i].sampleT;
        stream << processing[i].sampleP;
        stream << processing[i].standardT;
        stream << processing[i].standardP;
    }
}


QString CorrSTPComponent::getBasicSerializationName() const
{ return QString::fromLatin1("corr_stp"); }

ComponentOptions CorrSTPComponent::getOptions()
{
    ComponentOptions options;

    options.add("t", new DynamicInputOption(tr("t", "name"), tr("Sample temperature"),
                                            tr("This is the temperature that the measurements where sampled at.  "
                                               "The result is the correction of the data from this temperature STP.  "
                                               "This is assumed to be in kelvin if it is greater than 150, "
                                               "otherwise it is in degrees celsius."),
                                            tr("Resolved automatically by instrument suffix")));
    options.add("p", new DynamicInputOption(tr("p", "name"), tr("Sample pressure"),
                                            tr("This is the pressure that the measurements where sampled at in hPa.  "
                                               "The result is the correction of the data from this pressure STP."),
                                            tr("Resolved automatically by instrument suffix")));

    options.add("stp-t", new DynamicInputOption(tr("stp-t", "name"), tr("Standard temperature"),
                                                tr("This is the temperature that the input is corrected to.  "
                                                   "This is assumed to be in kelvin if it is greater than 150, "
                                                   "otherwise it is in degrees celsius."),
                                                tr("273.15 K")));
    options.add("stp-p", new DynamicInputOption(tr("stp-p", "name"), tr("Standard pressure"),
                                                tr("This is the pressure that the input is corrected to in hPa."),
                                                tr("1013.25 hPa")));

    options.add("forward", new DynamicSequenceSelectionOption(tr("correct-volume", "name"),
                                                              tr("Volume proportional variables"),
                                                              tr("These are the variables to correct that are proportional to the "
                                                                 "ambient air volume.  For example, volumetric flows.  This option "
                                                                 "is mutually exclusive with instrument specification."),
                                                              tr("All flows")));
    options.add("inverse", new DynamicSequenceSelectionOption(tr("correct-concentration", "name"),
                                                              tr("Concentration proportional variables"),
                                                              tr("These are the variables to correct that are inversely "
                                                                 "proportional to the ambient air volume.  For example, light "
                                                                 "scattering.  This option is mutually exclusive with "
                                                                 "instrument specification."),
                                                              tr("All counts and extinction components")));
    options.add("suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments", "name"),
                                                                 tr("Instrument suffixes to correct"),
                                                                 tr("These are the instrument suffixes to correct.  "
                                                                    "For example S11 would usually specifies the reference nephelometer.  "
                                                                    "This option is mutually exclusive with manual variable specification."),
                                                                 tr("All instrument suffixes")));
    options.exclude("forward", "suffix");
    options.exclude("inverse", "suffix");
    options.exclude("suffix", "forward");
    options.exclude("suffix", "inverse");

    return options;
}

QList<ComponentExample> CorrSTPComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will correct all counts, flow rates, and light scattering, "
                                        "absorption, or extinction values present in the data stream.  "
                                        "The values are corrected using a temperature and pressure from "
                                        "the same suffix (e.x. BsG_S11 uses T_S11 and P_S11).  They are "
                                        "corrected to 273.15 K and 1013.25 hPa.")));

    options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")))->add("S11");
    (qobject_cast<DynamicInputOption *>(options.get("p")))->set(880.0);
    examples.append(ComponentExample(options, tr("Single instrument with manual pressure"),
                                     tr("This will correct all parameters for S11 to its temperature "
                                        "(from T_S11 for example) and 880.0 hPa.")));

    options = getOptions();
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("inverse")))->set("", "",
                                                                                  "BsG_S11");
    (qobject_cast<DynamicInputOption *>(options.get("t")))->set(
            SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("Tu_S11")),
            Calibration());
    examples.append(ComponentExample(options, tr("Single variable and alternate temperature"),
                                     tr("This will correct only the variable BsG_S11 and specifies an "
                                        "alternate sample temperature (Tu_S11).")));

    return examples;
}

SegmentProcessingStage *CorrSTPComponent::createBasicFilterDynamic(const ComponentOptions &options)
{ return new CorrSTP(options); }

SegmentProcessingStage *CorrSTPComponent::createBasicFilterPredefined(const ComponentOptions &options,
                                                                      double start,
                                                                      double end,
                                                                      const QList<
                                                                              SequenceName> &inputs)
{ return new CorrSTP(options, start, end, inputs); }

SegmentProcessingStage *CorrSTPComponent::createBasicFilterEditing(double start,
                                                                   double end,
                                                                   const SequenceName::Component &station,
                                                                   const SequenceName::Component &archive,
                                                                   const ValueSegment::Transfer &config)
{ return new CorrSTP(start, end, station, archive, config); }

SegmentProcessingStage *CorrSTPComponent::deserializeBasicFilter(QDataStream &stream)
{ return new CorrSTP(stream); }
