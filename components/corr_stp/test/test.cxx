/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    class ID {
    public:
        ID() : filter(), input()
        { }

        ID(const QSet<SequenceName> &setFilter, const QSet<SequenceName> &setInput) : filter(
                setFilter), input(setInput)
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }

        QSet<SequenceName> filter;
        QSet<SequenceName> input;
    };

    QHash<int, ID> contents;

    virtual void filterUnit(const SequenceName &unit, int targetID)
    {
        contents[targetID].input.remove(unit);
        contents[targetID].filter.insert(unit);
    }

    virtual void inputUnit(const SequenceName &unit, int targetID)
    {
        if (contents[targetID].filter.contains(unit))
            return;
        contents[targetID].input.insert(unit);
    }

    virtual bool isHandled(const SequenceName &unit)
    {
        for (QHash<int, ID>::const_iterator it = contents.constBegin();
                it != contents.constEnd();
                ++it) {
            if (it.value().filter.contains(unit))
                return true;
            if (it.value().input.contains(unit))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("corr_stp"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("t")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("p")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("stp-t")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("stp-p")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("forward")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("inverse")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")));

        QVERIFY(options.excluded().value("forward").contains("suffix"));
        QVERIFY(options.excluded().value("inverse").contains("suffix"));
        QVERIFY(options.excluded().value("suffix").contains("forward"));
        QVERIFY(options.excluded().value("suffix").contains("inverse"));
    }

    void dynamicDefaults()
    {
        ComponentOptions options(component->getOptions());
        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        SegmentProcessingStage *filter2;
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "Vx_S11q"), &controller);
        QVERIFY(controller.contents.isEmpty());
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2 != NULL);
            filter2->unhandled(SequenceName("brw", "raw", "Vx_S11q"), &controller);
            QVERIFY(controller.contents.isEmpty());
            delete filter2;
        }

        SequenceName P_S11_1("brw", "raw", "P_S11");
        SequenceName T_S11_1("brw", "raw", "T_S11");
        SequenceName F1_S11_1("brw", "raw", "F1_S11");
        SequenceName BsB_S11_1("brw", "raw", "BsB_S11");
        SequenceName BsG_S11_1("brw", "raw", "BsG_S11");
        SequenceName F1_S11_2("mlo", "raw", "F1_S11");
        SequenceName T_S11_2("mlo", "raw", "T_S11");
        SequenceName P_S11_2("mlo", "raw", "P_S11");
        SequenceName BsB_S11_2("mlo", "raw", "BsB_S11");
        SequenceName Q_S11_2("mlo", "raw", "Q_S11");
        SequenceName F1_S12_1("brw", "raw", "F1_S12");
        SequenceName P_S12_1("brw", "raw", "P_S12");
        SequenceName T_S12_1("brw", "raw", "T_S12");
        SequenceName Q_S12_1("brw", "raw", "Q_S12");

        filter->unhandled(P_S11_1, &controller);
        filter->unhandled(T_S11_1, &controller);
        filter->unhandled(BsB_S11_1, &controller);
        filter->unhandled(BsG_S11_1, &controller);
        filter->unhandled(F1_S11_1, &controller);
        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << BsB_S11_1 << BsG_S11_1 << F1_S11_1,
                                QSet<SequenceName>() << P_S11_1 << T_S11_1));
        QCOMPARE(idL.size(), 1);
        int S11_1 = idL.at(0);

        filter->unhandled(BsB_S11_2, &controller);
        filter->unhandled(Q_S11_2, &controller);
        QCOMPARE(controller.contents.size(), 2);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << BsB_S11_2 << Q_S11_2 << F1_S11_2,
                                QSet<SequenceName>() << P_S11_2 << T_S11_2));
        QCOMPARE(idL.size(), 1);
        int S11_2 = idL.at(0);

        filter->unhandled(F1_S12_1, &controller);
        filter->unhandled(T_S12_1, &controller);
        filter->unhandled(Q_S12_1, &controller);
        filter->unhandled(P_S12_1, &controller);
        QCOMPARE(controller.contents.size(), 3);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << Q_S12_1 << F1_S12_1,
                                                 QSet<SequenceName>() << P_S12_1 << T_S12_1));
        QCOMPARE(idL.size(), 1);
        int S12_1 = idL.at(0);

        data.setStart(15.0);
        data.setEnd(16.0);

        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(P_S11_1, Variant::Root(1013.25));
        data.setValue(T_S11_1, Variant::Root(273.15));
        data.setValue(BsB_S11_1, Variant::Root(12.123));
        data.setValue(BsG_S11_1, Variant::Root(15.0));
        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        filter->process(S11_1, data);
        QCOMPARE(data.value(BsB_S11_1).toDouble(), 12.123);
        QCOMPARE(data.value(BsG_S11_1).toDouble(), 15.0);
        QVERIFY(data.value(F1_S11_1).testFlag("STP"));

        {
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
            }
            QVERIFY(filter2 != NULL);
            data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
            data.setValue(P_S11_1, Variant::Root(1013.25));
            data.setValue(T_S11_1, Variant::Root(273.15));
            data.setValue(BsB_S11_1, Variant::Root(12.123));
            data.setValue(BsG_S11_1, Variant::Root(15.0));
            data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
            filter2->process(S11_1, data);
            QCOMPARE(data.value(BsB_S11_1).toDouble(), 12.123);
            QCOMPARE(data.value(BsG_S11_1).toDouble(), 15.0);
            QVERIFY(data.value(F1_S11_1).testFlag("STP"));
            delete filter2;
        }

        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(P_S11_1, Variant::Root(1000.00));
        data.setValue(T_S11_1, Variant::Root(23.0));
        data.setValue(BsB_S11_1, Variant::Root(12.123));
        data.setValue(BsG_S11_1, Variant::Root(15.0));
        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        filter->process(S11_1, data);
        QCOMPARE(data.value(BsB_S11_1).toDouble(), 13.3179460020593);
        QCOMPARE(data.value(BsG_S11_1).toDouble(), 16.4785275947282);
        QVERIFY(data.value(F1_S11_1).testFlag("STP"));

        data.setValue(T_S11_2, Variant::Root(280.123));
        data.setValue(P_S11_2, Variant::Root(1020.23));
        data.setValue(BsB_S11_2, Variant::Root(0.23));
        data.setValue(Q_S11_2, Variant::Root(30.05));
        filter->process(S11_2, data);
        QCOMPARE(data.value(BsB_S11_2).toDouble(), 0.234257725655352);
        QCOMPARE(data.value(Q_S11_2).toDouble(), 29.5038295136889);

        data.setValue(P_S12_1, Variant::Root(1023.25));
        data.setValue(T_S12_1, Variant::Root(273.15));
        data.setValue(Q_S12_1, Variant::Root(35.0));
        filter->process(S12_1, data);
        QCOMPARE(data.value(Q_S12_1).toDouble(), 35.0);

        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(P_S11_1, Variant::Root(1013.25));
        data.setValue(T_S11_1, Variant::Root(273.15));
        data.setValue(BsB_S11_1, Variant::Root());
        data.setValue(BsG_S11_1, Variant::Root(15.0));
        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        filter->process(S11_1, data);
        QVERIFY(!FP::defined(data.value(BsB_S11_1).toDouble()));
        QCOMPARE(data.value(BsG_S11_1).toDouble(), 15.0);
        QVERIFY(data.value(F1_S11_1).testFlag("STP"));

        data.setValue(F1_S11_1, Variant::Root());
        data.setValue(P_S11_1, Variant::Root());
        data.setValue(T_S11_1, Variant::Root(273.15));
        data.setValue(BsB_S11_1, Variant::Root(23.0));
        data.setValue(BsG_S11_1, Variant::Root(15.0));
        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        filter->process(S11_1, data);
        QVERIFY(!FP::defined(data.value(BsB_S11_1).toDouble()));
        QVERIFY(!FP::defined(data.value(BsG_S11_1).toDouble()));
        QVERIFY(data.value(F1_S11_1).testFlag("STP"));

        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(P_S11_1, Variant::Root(1013.25));
        data.setValue(T_S11_1, Variant::Root());
        data.setValue(BsB_S11_1, Variant::Root(23.0));
        data.setValue(BsG_S11_1, Variant::Root(15.0));
        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        filter->process(S11_1, data);
        QVERIFY(!FP::defined(data.value(BsB_S11_1).toDouble()));
        QVERIFY(!FP::defined(data.value(BsG_S11_1).toDouble()));
        QVERIFY(data.value(F1_S11_1).testFlag("STP"));

        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(P_S11_1, Variant::Root(1013.25));
        data.setValue(T_S11_1, Variant::Root(900.0));
        data.setValue(BsB_S11_1, Variant::Root(23.0));
        data.setValue(BsG_S11_1, Variant::Root(15.0));
        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        filter->process(S11_1, data);
        QVERIFY(!FP::defined(data.value(BsB_S11_1).toDouble()));
        QVERIFY(!FP::defined(data.value(BsG_S11_1).toDouble()));
        QVERIFY(data.value(F1_S11_1).testFlag("STP"));

        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(P_S11_1, Variant::Root(1013.25));
        data.setValue(T_S11_1, Variant::Root(-500.0));
        data.setValue(BsB_S11_1, Variant::Root(23.0));
        data.setValue(BsG_S11_1, Variant::Root(15.0));
        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        filter->process(S11_1, data);
        QVERIFY(!FP::defined(data.value(BsB_S11_1).toDouble()));
        QVERIFY(!FP::defined(data.value(BsG_S11_1).toDouble()));
        QVERIFY(data.value(F1_S11_1).testFlag("STP"));

        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(P_S11_1, Variant::Root(5.0));
        data.setValue(T_S11_1, Variant::Root(273.15));
        data.setValue(BsB_S11_1, Variant::Root(23.0));
        data.setValue(BsG_S11_1, Variant::Root(15.0));
        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        filter->process(S11_1, data);
        QVERIFY(!FP::defined(data.value(BsB_S11_1).toDouble()));
        QVERIFY(!FP::defined(data.value(BsG_S11_1).toDouble()));
        QVERIFY(data.value(F1_S11_1).testFlag("STP"));

        data.setValue(F1_S11_1, Variant::Root());
        data.setValue(P_S11_1, Variant::Root(3000.0));
        data.setValue(T_S11_1, Variant::Root(273.15));
        data.setValue(BsB_S11_1, Variant::Root(23.0));
        data.setValue(BsG_S11_1, Variant::Root(15.0));
        filter->process(S11_1, data);
        QVERIFY(!FP::defined(data.value(BsB_S11_1).toDouble()));
        QVERIFY(!FP::defined(data.value(BsG_S11_1).toDouble()));
        QVERIFY(!data.value(F1_S11_1).exists());

        data.setValue(BsB_S11_1.toMeta(), Variant::Root(Variant::Type::MetadataReal));
        data.setValue(BsG_S11_1.toMeta(), Variant::Root(Variant::Type::MetadataReal));
        data.setValue(F1_S11_1.toMeta(), Variant::Root(Variant::Type::MetadataFlags));

        filter->processMeta(S11_1, data);
        QVERIFY(data.value(F1_S11_1.toMeta()).metadataSingleFlag("STP").exists());
        QCOMPARE(data.value(BsB_S11_1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data.value(BsB_S11_1.toMeta()).metadata("ReportT").toDouble(), 0.0);
        QCOMPARE(data.value(BsB_S11_1.toMeta()).metadata("ReportP").toDouble(), 1013.25);

        delete filter;
    }

    void dynamicOptions()
    {
        ComponentOptions options(component->getOptions());

        qobject_cast<DynamicInputOption *>(options.get("t"))->set(280.0);
        qobject_cast<DynamicInputOption *>(options.get("p"))->set(980.0);
        qobject_cast<DynamicInputOption *>(options.get("stp-t"))->set(290.0);
        qobject_cast<DynamicInputOption *>(options.get("stp-p"))->set(1050.0);
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("inverse"))->set("", "", "I.*");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("forward"))->set("", "", "O.*");

        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "P_S11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "T_S11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BsG_S11"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName I_S11("brw", "raw", "I_S11");
        SequenceName O_S11("brw", "raw", "O_S11");
        SequenceName O_S12("brw", "raw", "O_S12");
        filter->unhandled(I_S11, &controller);
        filter->unhandled(O_S11, &controller);
        filter->unhandled(O_S12, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << I_S11 << O_S11 << O_S12,
                                                 QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int id = idL.at(0);

        data.setValue(I_S11, Variant::Root(1.0));
        data.setValue(O_S11, Variant::Root(2.0));
        data.setValue(O_S12, Variant::Root(3.0));
        filter->process(id, data);
        QCOMPARE(data.value(I_S11).toDouble(), 1.03448275862069);
        QCOMPARE(data.value(O_S11).toDouble(), 1.93333333333333);
        QCOMPARE(data.value(O_S12).toDouble(), 2.9);

        delete filter;


        options = component->getOptions();
        qobject_cast<DynamicInputOption *>(options.get("t"))->set(280.0);
        qobject_cast<DynamicInputOption *>(options.get("stp-t"))->set(290.0);
        qobject_cast<DynamicInputOption *>(options.get("stp-p"))->set(1050.0);
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->add("S11");
        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        filter->unhandled(SequenceName("brw", "raw", "P_S12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "T_S12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BsG_S12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "Q_S12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "F1_S12"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName F1_S11("brw", "raw", "F1_S11");
        SequenceName T_S11("brw", "raw", "T_S11");
        SequenceName P_S11("brw", "raw", "P_S11");
        SequenceName BsG_S11("brw", "raw", "BsG_S11");
        filter->unhandled(F1_S11, &controller);
        filter->unhandled(T_S11, &controller);
        filter->unhandled(P_S11, &controller);
        filter->unhandled(BsG_S11, &controller);
        filter->unhandled(SequenceName("brw", "raw", "BsG_S12"), &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << F1_S11 << BsG_S11,
                                                 QSet<SequenceName>() << P_S11));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        data.setValue(P_S11, Variant::Root(990.0));
        data.setValue(BsG_S11, Variant::Root(10.0));
        data.setValue(F1_S11, Variant::Root(Variant::Type::Flags));
        filter->process(id, data);
        QCOMPARE(data.value(BsG_S11).toDouble(), 10.2403343782654);
        QVERIFY(data.value(F1_S11).testFlag("STP"));

        delete filter;
    }

    void predefined()
    {
        ComponentOptions options(component->getOptions());

        SequenceName F1_S11("brw", "raw", "F1_S11");
        SequenceName T_S11("brw", "raw", "T_S11");
        SequenceName P_S11("brw", "raw", "P_S11");
        SequenceName BsG_S11("brw", "raw", "BsG_S11");
        QList<SequenceName> input;
        input << T_S11 << BsG_S11;

        SegmentProcessingStage *filter =
                component->createBasicFilterPredefined(options, FP::undefined(), FP::undefined(),
                                                       input);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{T_S11, P_S11, BsG_S11, F1_S11}));

        delete filter;
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["S11/CorrectConcentrations"] = "::Bs[BGR]_S11:=";
        cv["S11/Flags"] = "::F1_S11:=";
        cv["S11/SampleT"] = "::T_S11:=";
        cv["S11/SampleP"] = "::P_S11:=";
        cv["S11/StandardT"] = 273.15;
        cv["S11/StandardP"] = 1013.25;
        cv["S12/CorrectVolumes"] = "::Q_S12:=";
        cv["S12/SampleT"] = "::T_S12:=";
        cv["S12/SampleP"] = "::P_S12:=";
        cv["S12/StandardT"] = 273.15;
        cv["S12/StandardP"] = 1013.25;
        config.emplace_back(FP::undefined(), 13.0, cv);
        cv.write().setEmpty();
        cv["S11/CorrectConcentrations"] = "::BsG_S11:=";
        cv["S11/Flags"] = "::F1_S11:=";
        cv["S11/SampleT"] = "::T_S11:=";
        cv["S11/SampleP"] = "::P_S11:=";
        cv["S11/StandardT"] = 273.15;
        cv["S11/StandardP"] = 1013.25;
        config.emplace_back(13.0, FP::undefined(), cv);

        SegmentProcessingStage
                *filter = component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config);
        QVERIFY(filter != NULL);
        TestController controller;

        SequenceName F1_S11("brw", "raw", "F1_S11");
        SequenceName BsB_S11("brw", "raw", "BsB_S11");
        SequenceName BsG_S11("brw", "raw", "BsG_S11");
        SequenceName BsR_S11("brw", "raw", "BsR_S11");
        SequenceName T_S11("brw", "raw", "T_S11");
        SequenceName P_S11("brw", "raw", "P_S11");
        SequenceName Q_S12("brw", "raw", "Q_S12");
        SequenceName T_S12("brw", "raw", "T_S12");
        SequenceName P_S12("brw", "raw", "P_S12");

        QCOMPARE(filter->requestedInputs(),
                 (SequenceName::Set{T_S11, P_S11, F1_S11, BsG_S11, T_S12, P_S12, Q_S12}));

        filter->unhandled(F1_S11, &controller);
        filter->unhandled(BsB_S11, &controller);
        filter->unhandled(BsG_S11, &controller);
        filter->unhandled(BsR_S11, &controller);
        filter->unhandled(T_S11, &controller);
        filter->unhandled(P_S11, &controller);
        filter->unhandled(Q_S12, &controller);
        filter->unhandled(T_S12, &controller);
        filter->unhandled(P_S12, &controller);

        QList<int> idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                                          BsB_S11 <<
                                                                                          BsG_S11 <<
                                                                                          BsR_S11 << F1_S11,
                                                                     QSet<SequenceName>() <<
                                                                                          T_S11 <<
                                                                                          P_S11));
        QCOMPARE(idL.size(), 1);
        int S11 = idL.at(0);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << Q_S12,
                                                 QSet<SequenceName>() << T_S12 << P_S12));
        QCOMPARE(idL.size(), 1);
        int S12 = idL.at(0);

        QCOMPARE(filter->metadataBreaks(S11), QSet<double>() << 13.0);
        QCOMPARE(filter->metadataBreaks(S12), QSet<double>() << 13.0);

        SequenceSegment data1;
        data1.setStart(12.0);
        data1.setEnd(13.0);

        data1.setValue(BsB_S11, Variant::Root());
        data1.setValue(BsG_S11, Variant::Root(1.0));
        data1.setValue(BsR_S11, Variant::Root(2.0));
        data1.setValue(T_S11, Variant::Root(280.0));
        data1.setValue(P_S11, Variant::Root(1010.0));
        data1.setValue(F1_S11, Variant::Root(Variant::Type::Flags));
        filter->process(S11, data1);
        QVERIFY(!data1.value(BsB_S11).exists());
        QCOMPARE(data1.value(BsG_S11).toDouble(), 1.02837631374340);
        QCOMPARE(data1.value(BsR_S11).toDouble(), 2.05675262748680);
        QVERIFY(data1.value(F1_S11).testFlag("STP"));

        data1.setValue(Q_S12, Variant::Root(3.0));
        data1.setValue(T_S12, Variant::Root(282.0));
        data1.setValue(P_S12, Variant::Root(1008.0));
        filter->process(S12, data1);
        QCOMPARE(data1.value(Q_S12).toDouble(), 2.89079484070114);

        data1.setValue(BsB_S11.toMeta(), Variant::Root(Variant::Type::MetadataReal));
        data1.setValue(BsG_S11.toMeta(), Variant::Root(Variant::Type::MetadataReal));
        data1.setValue(BsR_S11.toMeta(), Variant::Root(Variant::Type::MetadataReal));
        data1.setValue(F1_S11.toMeta(), Variant::Root(Variant::Type::MetadataFlags));

        filter->processMeta(S11, data1);
        QCOMPARE(data1.value(BsB_S11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data1.value(BsG_S11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data1.value(BsR_S11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QVERIFY(data1.value(F1_S11.toMeta()).metadataSingleFlag("STP").exists());

        data1.setValue(Q_S12.toMeta(), Variant::Root(Variant::Type::MetadataReal));

        filter->processMeta(S12, data1);
        QCOMPARE(data1.value(Q_S12.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);


        SequenceSegment data2;
        data2.setStart(13.0);
        data2.setEnd(15.0);

        data2.setValue(BsB_S11, Variant::Root(4.0));
        data2.setValue(BsG_S11, Variant::Root(5.0));
        data2.setValue(BsR_S11, Variant::Root(5.0));
        data2.setValue(T_S11, Variant::Root(281.0));
        data2.setValue(P_S11, Variant::Root(1011.0));
        data2.setValue(F1_S11, Variant::Root(Variant::Type::Flags));
        filter->process(S11, data2);
        QCOMPARE(data2.value(BsB_S11).toDouble(), 4.0);
        QCOMPARE(data2.value(BsG_S11).toDouble(), 5.15514133113457);
        QCOMPARE(data2.value(BsR_S11).toDouble(), 5.0);
        QVERIFY(data2.value(F1_S11).testFlag("STP"));

        data2.setValue(Q_S12, Variant::Root(3.0));
        data2.setValue(T_S12, Variant::Root(282.0));
        data2.setValue(P_S12, Variant::Root(1008.0));
        filter->process(S12, data2);
        QCOMPARE(data2.value(Q_S12).toDouble(), 3.0);

        data2.setValue(BsG_S11.toMeta(), Variant::Root(Variant::Type::MetadataReal));
        data2.setValue(F1_S11.toMeta(), Variant::Root(Variant::Type::MetadataFlags));

        filter->processMeta(S11, data2);
        QVERIFY(!data2.value(BsB_S11.toMeta()).exists());
        QCOMPARE(data2.value(BsG_S11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QVERIFY(!data2.value(BsR_S11.toMeta()).exists());
        QVERIFY(data2.value(F1_S11.toMeta()).metadataSingleFlag("STP").exists());

        filter->processMeta(S12, data2);
        QVERIFY(!data2.value(Q_S12.toMeta()).exists());

        delete filter;
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
