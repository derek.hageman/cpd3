/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef INDIRECT_HXX
#define INDIRECT_HXX

#include "core/first.hxx"

#include <vector>
#include <memory>
#include <unordered_set>
#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/variant/composite.hxx"

class Indirect : public CPD3::Data::SegmentProcessingStage {
    typedef CPD3::Data::DynamicPrimitive<CPD3::Data::Variant::Root> DynamicLiteral;

    struct Processing {
        std::unique_ptr<CPD3::Data::DynamicSequenceSelection> operateOutput;
        std::unique_ptr<CPD3::Data::DynamicString> pathOutput;
        std::unique_ptr<CPD3::Data::DynamicBool> disableOutputMetadata;
        std::unique_ptr<DynamicLiteral> baseOutput;
        std::unique_ptr<DynamicLiteral> metadataOutput;

        std::unique_ptr<CPD3::Data::DynamicSequenceSelection> inputFrom;
        std::unique_ptr<CPD3::Data::DynamicString> pathFrom;

        std::unique_ptr<CPD3::Data::DynamicSequenceSelection> inputInto;
        std::unique_ptr<CPD3::Data::DynamicString> pathInto;

        std::unique_ptr<CPD3::Data::DynamicString> inputStringLiteral;

        CPD3::Data::Variant::Root effectiveMetadataOutput;
        CPD3::Data::Variant::Root effectiveMetadataFrom;
        CPD3::Data::Variant::Root effectiveMetadataInto;
    };
    std::vector<Processing> processing;

    void handleNewProcessing(const CPD3::Data::SequenceName &name,
                             std::size_t id,
                             CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control);

    void registerPossibleInput(const CPD3::Data::SequenceName &name,
                               CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                               std::size_t filterID = static_cast<std::size_t>(-1));

public:
    Indirect() = delete;

    Indirect(const CPD3::ComponentOptions &options);

    Indirect(const CPD3::ComponentOptions &options,
             double start,
             double end,
             const QList<CPD3::Data::SequenceName> &inputs);

    Indirect(double start,
             double end,
             const CPD3::Data::SequenceName::Component &station,
             const CPD3::Data::SequenceName::Component &archive,
             const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~Indirect();

    void unhandled(const CPD3::Data::SequenceName &name,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;

    QSet<double> metadataBreaks(int id) override;

    Indirect(QDataStream &stream);

    void serialize(QDataStream &stream) override;
};

class IndirectComponent
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.indirect"
                              FILE
                              "indirect.json")
public:
    QString getBasicSerializationName() const override;

    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    CPD3::Data::SegmentProcessingStage *createBasicFilterDynamic(const CPD3::ComponentOptions &options) override;

    CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined(const CPD3::ComponentOptions &options,
                                                                    double start,
                                                                    double end,
                                                                    const QList<
                                                                            CPD3::Data::SequenceName> &inputs) override;

    CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                 double end,
                                                                 const CPD3::Data::SequenceName::Component &station,
                                                                 const CPD3::Data::SequenceName::Component &archive,
                                                                 const CPD3::Data::ValueSegment::Transfer &config) override;

    CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream) override;
};

#endif
