/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cmath>
#include <QRegularExpression>

#include "core/component.hxx"
#include "datacore/variant/root.hxx"

bool operator==(const CPD3::Data::Variant::Root &a, const CPD3::Data::Variant::Root &b)
{
    return a.read() == b.read();
}

#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "core/environment.hxx"
#include "core/qtcompat.hxx"

#include "indirect.hxx"

using namespace CPD3;
using namespace CPD3::Data;


Indirect::Indirect(const ComponentOptions &options)
{
    Processing proc;

    if (options.isSet("output")) {
        proc.operateOutput
            .reset(qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("output"))->getOperator());
    } else {
        proc.operateOutput.reset(new DynamicSequenceSelection::None);
    }

    if (options.isSet("target-path")) {
        proc.pathOutput
            .reset(qobject_cast<DynamicStringOption *>(options.get("target-path"))->getInput());
    } else {
        proc.pathOutput.reset(new DynamicPrimitive<QString>::Constant);
    }

    proc.disableOutputMetadata.reset(new DynamicPrimitive<bool>::Constant(false));
    proc.baseOutput.reset(new DynamicLiteral::Constant);
    proc.metadataOutput.reset(new DynamicLiteral::Constant);

    if (options.isSet("from")) {
        proc.inputFrom
            .reset(qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("from"))->getOperator());
    } else {
        proc.inputFrom.reset(new DynamicSequenceSelection::None);
    }

    if (options.isSet("reference-path")) {
        proc.pathFrom
            .reset(qobject_cast<DynamicStringOption *>(options.get("reference-path"))->getInput());
    } else {
        proc.pathFrom.reset(new DynamicPrimitive<QString>::Constant);
    }

    if (options.isSet("into")) {
        proc.inputInto
            .reset(qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("into"))->getOperator());
    } else {
        proc.inputInto.reset(new DynamicSequenceSelection::None);
    }

    if (options.isSet("path")) {
        proc.pathInto.reset(qobject_cast<DynamicStringOption *>(options.get("path"))->getInput());
    } else {
        proc.pathInto.reset(new DynamicPrimitive<QString>::Constant);
    }

    if (options.isSet("string")) {
        proc.inputStringLiteral
            .reset(qobject_cast<DynamicStringOption *>(options.get("string"))->getInput());
    } else {
        proc.inputStringLiteral.reset(new DynamicPrimitive<QString>::Constant);
    }

    processing.emplace_back(std::move(proc));
}

Indirect::Indirect(const ComponentOptions &options,
                   double,
                   double,
                   const QList<SequenceName> &inputs) : Indirect(options)
{
    for (const auto &name :inputs) {
        Indirect::unhandled(name, nullptr);
    }
}

static Variant::Root convertLiteral(const Variant::Read &value)
{ return Variant::Root(value); }

Indirect::Indirect(double start,
                   double end,
                   const SequenceName::Component &station,
                   const SequenceName::Component &archive,
                   const ValueSegment::Transfer &config)
{
    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    for (const auto &child : children) {
        Processing proc;

        proc.operateOutput
            .reset(DynamicSequenceSelection::fromConfiguration(config,
                                                               QString("%1/Output/Data").arg(
                                                                       QString::fromStdString(
                                                                               child)), start,
                                                               end));
        proc.pathOutput
            .reset(DynamicStringOption::fromConfiguration(config, QString("%1/Output/Path").arg(
                    QString::fromStdString(child)), start, end));
        proc.disableOutputMetadata
            .reset(DynamicBoolOption::fromConfiguration(config,
                                                        QString("%1/Output/DisableMetadata").arg(
                                                                QString::fromStdString(child)),
                                                        start, end));

        proc.baseOutput
            .reset(DynamicLiteral::fromConfiguration(convertLiteral, config,
                                                     QString("%1/Output/Base").arg(
                                                             QString::fromStdString(child)), start,
                                                     end));
        proc.metadataOutput
            .reset(DynamicLiteral::fromConfiguration(convertLiteral, config,
                                                     QString("%1/Output/Metadata").arg(
                                                             QString::fromStdString(child)), start,
                                                     end));
        proc.inputStringLiteral
            .reset(DynamicStringOption::fromConfiguration(config,
                                                          QString("%1/Output/StringLiteral").arg(
                                                                  QString::fromStdString(child)),
                                                          start, end));

        proc.inputFrom
            .reset(DynamicSequenceSelection::fromConfiguration(config, QString("%1/From/Data").arg(
                    QString::fromStdString(child)), start, end));
        proc.pathFrom
            .reset(DynamicStringOption::fromConfiguration(config, QString("%1/From/Path").arg(
                    QString::fromStdString(child)), start, end));

        proc.inputInto
            .reset(DynamicSequenceSelection::fromConfiguration(config, QString("%1/Into/Data").arg(
                    QString::fromStdString(child)), start, end));
        proc.pathInto
            .reset(DynamicStringOption::fromConfiguration(config, QString("%1/Into/Path").arg(
                    QString::fromStdString(child)), start, end));

        proc.operateOutput->registerExpected(station, archive);
        proc.inputFrom->registerExpected(station, archive);
        proc.inputInto->registerExpected(station, archive);

        processing.emplace_back(std::move(proc));
    }
}


Indirect::~Indirect() = default;

void Indirect::handleNewProcessing(const SequenceName &name,
                                   std::size_t id,
                                   SegmentProcessingStage::SequenceHandlerControl *control)
{
    auto &proc = processing[id];

    SequenceName::Set reg;

    proc.operateOutput
        ->registerExpected(name.getStation(), name.getArchive(), {}, name.getFlavors());
    Util::merge(proc.operateOutput->getAllUnits(), reg);

    if (!control)
        return;

    for (const auto &n : reg) {
        bool isNew = !control->isHandled(n);
        control->filterUnit(n, id);
        if (isNew)
            registerPossibleInput(n, control, id);
    }
}

void Indirect::registerPossibleInput(const SequenceName &name,
                                     SegmentProcessingStage::SequenceHandlerControl *control,
                                     std::size_t filterID)
{
    for (std::size_t id = 0, max = processing.size(); id < max; id++) {
        bool usedAsInput = false;
        auto &proc = processing[id];
        if (proc.inputFrom->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputInto->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (usedAsInput && id != filterID)
            handleNewProcessing(name, id, control);
    }
}

void Indirect::unhandled(const SequenceName &name,
                         SegmentProcessingStage::SequenceHandlerControl *control)
{
    registerPossibleInput(name, control);

    for (std::size_t id = 0, max = processing.size(); id < max; id++) {
        if (processing[id].operateOutput->registerInput(name)) {
            if (control)
                control->filterUnit(name, id);
            handleNewProcessing(name, id, control);
            return;
        }
    }
}

static std::pair<Variant::Path, bool> convertPath(const QString &basePath,
                                                  Variant::Composite::SubstitutionStack &substitutions)
{
    if (basePath.isEmpty())
        return {{}, true};
    auto path = substitutions.apply(basePath);
    if (path.isEmpty())
        return {{}, false};

    auto parsedPath = Variant::PathElement::parse(path.toStdString());
    if (parsedPath.empty()) {
        if (path != "/")
            return {{}, false};
    }

    return {parsedPath, true};
}

void Indirect::process(int id, SequenceSegment &data)
{
    auto &proc = processing[id];

    Variant::Composite::SubstitutionStack substitutions;
    substitutions.setTime("start", data.getStart());
    substitutions.setTime("end", data.getEnd());
    substitutions.setTime("time", data.getStart());
    substitutions.setVariant("metadataoutput", proc.effectiveMetadataOutput);
    substitutions.setVariant("metadatafrom", proc.effectiveMetadataFrom);
    substitutions.setVariant("metadatareference", proc.effectiveMetadataFrom);
    substitutions.setVariant("metadatainto", proc.effectiveMetadataInto);
    substitutions.setVariant("metadata", proc.effectiveMetadataFrom);

    Variant::Read from = Variant::Read::empty();
    auto fromPath = convertPath(proc.pathFrom->get(data), substitutions);
    if (fromPath.second) {
        for (const auto &i : proc.inputFrom->get(data)) {
            from = data[i].getPath(fromPath.first);
            if (Variant::Composite::isDefined(from))
                break;
        }
    }
    substitutions.setVariant("from", from);
    substitutions.setVariant("reference", from);

    Variant::Read into = Variant::Read::empty();
    auto intoPath = convertPath(proc.pathInto->get(data), substitutions);
    if (intoPath.second) {
        for (const auto &i : proc.inputInto->get(data)) {
            into = data[i].getPath(intoPath.first);
            if (Variant::Composite::isDefined(into))
                break;
        }
    }
    substitutions.setVariant("into", into);

    {
        auto check = proc.inputStringLiteral->get(data);
        if (!check.isEmpty()) {
            check = substitutions.apply(check);
            into = Variant::Root(check).read();
        }
    }

    auto outputPath = convertPath(proc.pathOutput->get(data), substitutions);
    auto base = proc.baseOutput->get(data).read();
    for (const auto &i : proc.operateOutput->get(data)) {
        if (base.exists())
            data[i].set(base);
        data[i].getPath(outputPath.first).set(into);
    }
}

void Indirect::processMeta(int id, SequenceSegment &data)
{
    auto &proc = processing[id];

    for (auto i : proc.inputFrom->get(data)) {
        if (i.isMeta())
            continue;
        i.setMeta();
        if (!data.exists(i))
            continue;
        auto from = data[i];
        if (!from.exists())
            continue;
        proc.effectiveMetadataFrom.write().set(from);
        break;
    }
    for (auto i : proc.inputInto->get(data)) {
        if (i.isMeta())
            continue;
        i.setMeta();
        if (!data.exists(i))
            continue;
        auto into = data[i];
        if (!into.exists())
            continue;
        proc.effectiveMetadataInto.write().set(into);
        break;
    }

    if (proc.disableOutputMetadata->get(data))
        return;

    Variant::Root meta;

    meta["By"].setString("indirect");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    meta["Parameters/OutputPath"] = proc.pathOutput->get(data);
    meta["Parameters/FromPath"] = proc.pathFrom->get(data);
    meta["Parameters/IntoPath"] = proc.pathInto->get(data);

    const auto &defineMetadata = proc.metadataOutput->get(data);

    Variant::Composite::SubstitutionStack substitutions;
    substitutions.setTime("start", data.getStart());
    substitutions.setTime("end", data.getEnd());
    substitutions.setTime("time", data.getStart());
    substitutions.setVariant("metadataoutput", proc.effectiveMetadataOutput);
    substitutions.setVariant("metadatafrom", proc.effectiveMetadataFrom);
    substitutions.setVariant("metadatareference", proc.effectiveMetadataFrom);
    substitutions.setVariant("metadatainto", proc.effectiveMetadataInto);
    substitutions.setVariant("metadata", proc.effectiveMetadataFrom);

    std::pair<Variant::Path, bool> intoPath({}, false);
    if (!defineMetadata.read().exists()) {
        intoPath = convertPath(proc.pathInto->get(data), substitutions);
        intoPath.first = Variant::PathElement::toMetadata(std::move(intoPath.first));
    }

    auto outputPath = convertPath(proc.pathOutput->get(data), substitutions);
    outputPath.first = Variant::PathElement::toMetadata(std::move(outputPath.first));

    for (auto i : proc.operateOutput->get(data)) {
        if (i.isMeta())
            continue;
        i.setMeta();
        if (!data.exists(i)) {
            if (defineMetadata.read().exists()) {
                data[i].getPath(outputPath.first).set(defineMetadata);
            } else if (intoPath.second && outputPath.first.empty()) {
                auto check = proc.effectiveMetadataInto.read().getPath(intoPath.first);
                if (check.exists() && check.isMetadata()) {
                    data[i].set(check);
                } else {
                    continue;
                }
            } else {
                continue;
            }
        } else {
            auto target = data[i].getPath(outputPath.first);
            if (defineMetadata.read().exists()) {
                target.set(defineMetadata);
            } else if (intoPath.second && !target.exists()) {
                auto check = proc.effectiveMetadataInto.read().getPath(intoPath.first);
                if (check.exists() && check.isMetadata()) {
                    target.set(check);
                }
            }
        }
        data[i].metadata("Processing").toArray().after_back().set(meta);

        proc.effectiveMetadataOutput.write().set(data[i]);
    }
}

SequenceName::Set Indirect::requestedInputs()
{
    SequenceName::Set out;
    for (const auto &p : processing) {
        Util::merge(p.operateOutput->getAllUnits(), out);
        Util::merge(p.inputFrom->getAllUnits(), out);
        Util::merge(p.inputInto->getAllUnits(), out);
    }
    return out;
}

SequenceName::Set Indirect::predictedOutputs()
{
    SequenceName::Set out;
    for (const auto &p : processing) {
        Util::merge(p.operateOutput->getAllUnits(), out);
    }
    return out;
}

QSet<double> Indirect::metadataBreaks(int id)
{
    const auto &p = processing[id];
    QSet<double> result;
    Util::merge(p.operateOutput->getChangedPoints(), result);
    Util::merge(p.pathOutput->getChangedPoints(), result);
    Util::merge(p.disableOutputMetadata->getChangedPoints(), result);
    Util::merge(p.metadataOutput->getChangedPoints(), result);
    Util::merge(p.inputFrom->getChangedPoints(), result);
    Util::merge(p.pathFrom->getChangedPoints(), result);
    Util::merge(p.inputInto->getChangedPoints(), result);
    Util::merge(p.pathInto->getChangedPoints(), result);
    return result;
}

Indirect::Indirect(QDataStream &stream)
{
    Deserialize::container(stream, processing, [&stream]() {
        Processing proc;

        stream >> proc.operateOutput;
        stream >> proc.pathOutput;
        stream >> proc.disableOutputMetadata;
        stream >> proc.baseOutput;
        stream >> proc.metadataOutput;
        stream >> proc.inputFrom;
        stream >> proc.pathFrom;
        stream >> proc.inputInto;
        stream >> proc.pathInto;
        stream >> proc.inputStringLiteral;
        stream >> proc.effectiveMetadataOutput;
        stream >> proc.effectiveMetadataFrom;
        stream >> proc.effectiveMetadataInto;

        return std::move(proc);
    });
}

void Indirect::serialize(QDataStream &stream)
{
    Serialize::container(stream, processing, [&stream](const Processing &proc) {
        stream << proc.operateOutput;
        stream << proc.pathOutput;
        stream << proc.disableOutputMetadata;
        stream << proc.baseOutput;
        stream << proc.metadataOutput;
        stream << proc.inputFrom;
        stream << proc.pathFrom;
        stream << proc.inputInto;
        stream << proc.pathInto;
        stream << proc.inputStringLiteral;
        stream << proc.effectiveMetadataOutput;
        stream << proc.effectiveMetadataFrom;
        stream << proc.effectiveMetadataInto;
    });
}


QString IndirectComponent::getBasicSerializationName() const
{ return QString::fromLatin1("indirect"); }

ComponentOptions IndirectComponent::getOptions()
{
    ComponentOptions options;

    options.add("output",
                new DynamicSequenceSelectionOption(tr("output", "name"), tr("Output value"),
                                                   tr("This is the output that the lookup generates."),
                                                   {}));
    options.add("target-path",
                new DynamicStringOption(tr("target-path", "name"), tr("Path in the output"),
                                        tr("This is the path within the output that the result is placed into."),
                                        {}));

    options.add("from", new DynamicSequenceSelectionOption(tr("from", "name"), tr("Origin value"),
                                                           tr("This is the input that specifies the origin or master of the lookup."),
                                                           {}));
    options.add("reference-path",
                new DynamicStringOption(tr("reference-path", "name"), tr("Path in the origin"),
                                        tr("This is the path within the into origin that is selected for substitution."),
                                        {}));

    options.add("into", new DynamicSequenceSelectionOption(tr("into", "name"), tr("Target value"),
                                                           tr("This is the value that the indirect lookup is performed into."),
                                                           {}));
    options.add("path", new DynamicStringOption(tr("path", "name"), tr("Path to look up"),
                                                tr("This is the path within the indirect to look up the value at."),
                                                {}));
    options.add("string", new DynamicStringOption(tr("string", "name"), tr("Output string"),
                                                  tr("If not empty, then this string is used as the output value, after substitutions have been applied."),
                                                  {}));

    return options;
}

QList<ComponentExample> IndirectComponent::getExamples()
{
    QList<ComponentExample> examples;

    return examples;
}

SegmentProcessingStage *IndirectComponent::createBasicFilterDynamic(const ComponentOptions &options)
{ return new Indirect(options); }

SegmentProcessingStage *IndirectComponent::createBasicFilterPredefined(const ComponentOptions &options,
                                                                       double start,
                                                                       double end,
                                                                       const QList<
                                                                               SequenceName> &inputs)
{ return new Indirect(options, start, end, inputs); }

SegmentProcessingStage *IndirectComponent::createBasicFilterEditing(double start,
                                                                    double end,
                                                                    const SequenceName::Component &station,
                                                                    const SequenceName::Component &archive,
                                                                    const ValueSegment::Transfer &config)
{ return new Indirect(start, end, station, archive, config); }

SegmentProcessingStage *IndirectComponent::deserializeBasicFilter(QDataStream &stream)
{ return new Indirect(stream); }
