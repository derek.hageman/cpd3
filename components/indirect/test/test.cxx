/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    struct ID {
        SequenceName::Set filter;
        SequenceName::Set input;

        ID() = default;

        ID(SequenceName::Set filter, SequenceName::Set input) : filter(std::move(filter)),
                                                                input(std::move(input))
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }
    };

    std::unordered_map<int, ID> contents;

    int find(const SequenceName::Set &filter, const SequenceName::Set &input = {}) const
    {
        for (const auto &check : contents) {
            if (check.second.filter != filter)
                continue;
            if (check.second.input != input)
                continue;
            return check.first;
        }
        return -1;
    }

    void filterUnit(const SequenceName &name, int targetID) override
    {
        contents[targetID].input.erase(name);
        contents[targetID].filter.insert(name);
    }

    void inputUnit(const SequenceName &name, int targetID) override
    {
        if (contents[targetID].filter.count(name))
            return;
        contents[targetID].input.insert(name);
    }

    bool isHandled(const SequenceName &name) override
    {
        for (const auto &check : contents) {
            if (check.second.filter.count(name))
                return true;
            if (check.second.input.count(name))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("indirect"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("output")));
        QVERIFY(qobject_cast<DynamicStringOption *>(options.get("target-path")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("from")));
        QVERIFY(qobject_cast<DynamicStringOption *>(options.get("reference-path")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("into")));
        QVERIFY(qobject_cast<DynamicStringOption *>(options.get("path")));
        QVERIFY(qobject_cast<DynamicStringOption *>(options.get("string")));
    }

    void basic()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("output"))->set({}, {},
                                                                                   "ZOut_X1");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("from"))->set({}, {},
                                                                                 "ZFrom_X1");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("into"))->set({}, {},
                                                                                 "ZInto_X1");
        qobject_cast<DynamicStringOption *>(options.get("path"))->set("/Base/${FROM|STR}");
        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterDynamic(options));
        QVERIFY(filter.get());
        TestController controller;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "Q_X1"), &controller);
        QVERIFY(controller.contents.empty());
        {
            std::unique_ptr<SegmentProcessingStage> filter2;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2.reset(component->deserializeBasicFilter(stream));
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2.get());
            filter2->unhandled(SequenceName("brw", "raw", "Q_X1"), &controller);
            QVERIFY(controller.contents.empty());
        }

        SequenceName out("brw", "raw", "ZOut_X1");
        SequenceName from("brw", "raw", "ZFrom_X1");
        SequenceName into("brw", "raw", "ZInto_X1");

        filter->unhandled(out, &controller);
        filter->unhandled(from, &controller);
        filter->unhandled(into, &controller);
        QCOMPARE((int) controller.contents.size(), 1);
        int set_1 = controller.find({out}, {from, into});
        QVERIFY(set_1 != -1);

        data.setStart(15.0);
        data.setEnd(16.0);

        Variant::Root lookupBase;
        lookupBase["/Base/First"].setString("A string");
        lookupBase["/Base/Second"].setReal(42.0);

        data.setValue(into, lookupBase);
        data.setValue(from, Variant::Root("Second"));
        filter->process(set_1, data);
        QCOMPARE(data.value(out).toReal(), 42.0);

        {
            std::unique_ptr<SegmentProcessingStage> filter2;
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2.reset(component->deserializeBasicFilter(stream));
            }
            QVERIFY(filter2.get());
            data.setValue(out, Variant::Root());
            data.setValue(from, Variant::Root("Second"));
            filter->process(set_1, data);
            QCOMPARE(data.value(out).toReal(), 42.0);
        }

        data.setValue(from, Variant::Root("First"));
        filter->process(set_1, data);
        QCOMPARE(data.value(out).toQString(), QString("A string"));

        data.setValue(from, Variant::Root("Third"));
        filter->process(set_1, data);
        QVERIFY(!data.value(out).exists());

        Variant::Root meta;
        meta.write().setType(Variant::Type::MetadataReal);
        data.setValue(out.toMeta(), std::move(meta));
        filter->processMeta(set_1, data);
        QCOMPARE(data.value(out.toMeta()).metadata("Processing").getType(), Variant::Type::Array);
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["S1/Output/Data"] = "::ZOut_X1:=";
        cv["S1/Output/Path"] = "/Indirected";
        cv["S1/Output/Base/Value"] = 1.0;
        cv["S1/Output/Metadata/*dDescription"] = "A description";
        cv["S1/From/Data"] = "::ZFrom_X1:=";
        cv["S1/Into/Data"] = "::ZInto_X1:=";
        cv["S1/Into/Path"] = "/${METADATA|STR|/^Source/SerialNumber||Unavailable}";
        config.emplace_back(FP::undefined(), 13.0, cv);
        cv.write().setEmpty();
        cv["S1/Output/Data"] = "::ZOut_X1:=";
        cv["S1/Output/StringLiteral"] = "Got ${INTO|STR}";
        cv["S1/From/Data"] = "::ZFrom_X1:=";
        cv["S1/From/Path"] = "/Origin";
        cv["S1/Into/Data"] = "::ZInto_X1:=";
        cv["S1/Into/Path"] = "/${FROM|STR}";
        config.emplace_back(13.0, FP::undefined(), cv);

        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config));
        QVERIFY(filter.get());
        TestController controller;

        SequenceName out("brw", "raw", "ZOut_X1");
        SequenceName from("brw", "raw", "ZFrom_X1");
        SequenceName into("brw", "raw", "ZInto_X1");

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{out, from, into}));
        QCOMPARE(filter->predictedOutputs(), (SequenceName::Set{out}));

        filter->unhandled(out, &controller);
        filter->unhandled(from, &controller);
        filter->unhandled(into, &controller);

        int id = controller.find({out}, {from, into});
        QVERIFY(id != -1);

        QCOMPARE(filter->metadataBreaks(id), QSet<double>() << 13.0);


        SequenceSegment data1;
        data1.setStart(12.0);
        data1.setEnd(13.0);

        Variant::Root wr;
        wr["/*dSource/SerialNumber"] = "SN";
        data1.setValue(from.toMeta(), wr);

        wr.write().setEmpty();
        wr.write().setType(Variant::Type::MetadataReal);
        data1.setValue(out.toMeta(), wr);

        filter->processMeta(id, data1);
        QCOMPARE(data1.value(out.toMeta()).metadata("Processing").getType(), Variant::Type::Array);

        wr.write().setEmpty();
        wr["/SN"] = 2.0;
        wr["/Unavailable"] = 3.0;
        data1.setValue(into, wr);

        data1.setValue(out, Variant::Root());
        data1.setValue(from, Variant::Root());
        filter->process(id, data1);

        wr.write().setEmpty();
        wr["/Value"] = 1.0;
        wr["/Indirected"] = 2.0;
        QCOMPARE(data1.value(out), wr.write());

        wr.write().setEmpty();
        wr["/*dZZZZ"] = "BAD";
        data1.setValue(from.toMeta(), wr);
        filter->processMeta(id, data1);

        wr.write().setEmpty();
        wr["/SN"] = 2.0;
        wr["/Unavailable"] = 3.0;
        data1.setValue(into, wr);

        data1.setValue(out, Variant::Root());
        data1.setValue(from, Variant::Root());
        filter->process(id, data1);

        wr.write().setEmpty();
        wr["/Value"] = 1.0;
        wr["/Indirected"] = 3.0;
        QCOMPARE(data1.value(out), wr.write());


        SequenceSegment data2;
        data2.setStart(13.0);
        data2.setEnd(15.0);

        wr.write().setEmpty();
        wr["/Origin"] = "First";
        data2.setValue(from, wr);

        wr.write().setEmpty();
        wr["/First"] = "Second";
        data2.setValue(into, wr);

        filter->process(id, data2);
        QCOMPARE(data2.value(out).toQString(), QString("Got Second"));

        filter->processMeta(id, data2);
        QVERIFY(!data2.value(out.toMeta()).metadata("Processing").exists());
    }

};

QTEST_MAIN(TestComponent)

#include "test.moc"
