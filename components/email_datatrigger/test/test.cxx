/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/externalsource.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Data {
namespace Variant {
bool operator==(const Root &a, const Root &b)
{ return a.read() == b.read(); }
}
}
}

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    ExternalSourceComponent *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ExternalSourceComponent *>(
                ComponentLoader::create("email_datatrigger"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("profile")));
    }

    void basic()
    {
        Variant::Root v;
        v["DataTrigger/aerosol/Neph/Input/Variable"].setString("F1_S11");
        v["DataTrigger/aerosol/Neph/Parameters/#0/Trigger/#0/Type"].setString("HasFlag");
        v["DataTrigger/aerosol/Neph/Parameters/#0/Trigger/#0/Input/Variable"].setString("F1_.*");
        v["DataTrigger/aerosol/Neph/Parameters/#0/Trigger/#0/Flags"].setFlags({"ChopperError"});
        v["DataTrigger/aerosol/Neph/Parameters/#0/Trigger/#1/Type"].setString("HasFlag");
        v["DataTrigger/aerosol/Neph/Parameters/#0/Trigger/#1/Input/Variable"].setString("F1_.*");
        v["DataTrigger/aerosol/Neph/Parameters/#0/Trigger/#1/Flags"].setFlags({"ShutterError"});
        v["DataTrigger/aerosol/Neph/Parameters/#0/Trigger/#2/Type"].setString("HasFlag");
        v["DataTrigger/aerosol/Neph/Parameters/#0/Trigger/#2/Input/Variable"].setString("F1_.*");
        v["DataTrigger/aerosol/Neph/Parameters/#0/Trigger/#2/Flags"].setFlags({"OtherFlag"});
        v["DataTrigger/aerosol/Neph/Parameters/#1/Trigger/Type"].setString("HasFlag");
        v["DataTrigger/aerosol/Neph/Parameters/#1/Trigger/Input/Variable"].setString("F1_.*");
        v["DataTrigger/aerosol/Neph/Parameters/#1/Trigger/Flags"].setFlags({"ChopperError"});
        v["DataTrigger/aerosol/Neph/Parameters/#1/Output"].setString(
                "Chopper error for ${PERCENT|00}% (${SECONDS} seconds)");
        v["DataTrigger/aerosol/Neph/Parameters/#2/Trigger/Type"].setString("HasFlag");
        v["DataTrigger/aerosol/Neph/Parameters/#2/Trigger/Input/Variable"].setString("F1_.*");
        v["DataTrigger/aerosol/Neph/Parameters/#2/Trigger/Flags"].setFlags({"ShutterError"});
        v["DataTrigger/aerosol/Neph/Parameters/#2/Output"].setString(
                "Shutter error for ${PERCENT|00}% (${DURATION|||SECONDS} seconds)");
        v["DataTrigger/aerosol/Neph/Output"].setString(
                "Status for ${START} to ${END} - ${0|PERCENT|00}% (${0|DURATION|||AUTOUNITS}) of data in error");

        SequenceValue::Transfer toAdd;
        toAdd.push_back(SequenceValue({"sfa", "configuration", "email"}, v, FP::undefined(),
                                      FP::undefined()));

        toAdd.push_back(
                SequenceValue({"brw", "raw", "F1_S11"}, Variant::Root(Variant::Flags{"OtherFlag"}),
                              1412121600, 1412121660));

        toAdd.push_back(SequenceValue({"sfa", "raw", "F1_S11"}, Variant::Root(Variant::Type::Flags),
                                      1412121600,
                                      1412121660));
        toAdd.push_back(SequenceValue({"sfa", "raw", "F1_S11"},
                                      Variant::Root(Variant::Flags{"ChopperError"}),
                                      1412121660, 1412121720));
        toAdd.push_back(SequenceValue({"sfa", "raw", "F1_S11"},
                                      Variant::Root(Variant::Flags{"ShutterError"}),
                                      1412121720, 1412121840));
        toAdd.push_back(SequenceValue({"sfa", "raw", "F1_S11"},
                                      Variant::Root(Variant::Flags{"ShutterError", "OtherFlag"}),
                                      1412121720, 1412121780));
        toAdd.push_back(SequenceValue({"sfa", "raw", "F1_S11"},
                                      Variant::Root(Variant::Flags{"ShutterError", "OtherFlag"}),
                                      1412121780, 1412121840));

        Archive::Access(databaseFile).writeSynchronous(toAdd);

        ComponentOptions options(component->getOptions());
        ExternalConverter
                *input = component->createExternalIngress(options, 1412121600, 1412208000);
        QVERIFY(input != NULL);
        input->start();

        StreamSink::Buffer ingress;
        input->setEgress(&ingress);

        QVERIFY(input->wait());
        delete input;

        QVERIFY(ingress.ended());
        std::vector<Variant::Root> outputValues;
        for (const auto &dv : ingress.values()) {
            outputValues.emplace_back(dv.root());
        }

        std::vector<Variant::Root> values;
        v.write().setEmpty();
        v["Type"].setString("Break");
        values.emplace_back(v);

        v.write().setEmpty();
        v["Type"].setString("Text");
        v["Text"].setString(
                "Status for 2014-10-01T00:00:00Z to 2014-10-02T00:00:00Z - 75% (3 minute(s)) of data in error");
        values.emplace_back(v);

        v.write().setEmpty();
        v["Type"].setString("Text");
        v["Text"].setString("Chopper error for 25% (60 seconds)");
        values.emplace_back(v);

        v.write().setEmpty();
        v["Type"].setString("Text");
        v["Text"].setString("Shutter error for 50% (120 seconds)");
        values.emplace_back(v);

        QCOMPARE(outputValues, values);
    }

    void latest()
    {
        Variant::Root v;
        v["DataTrigger/aerosol/Neph/Input/Variable"].setString("F1_S11");
        v["DataTrigger/aerosol/Neph/Parameters/#0/Trigger/#0/Type"].setString("HasFlag");
        v["DataTrigger/aerosol/Neph/Parameters/#0/Trigger/#0/Input/Variable"].setString("F1_.*");
        v["DataTrigger/aerosol/Neph/Parameters/#0/Trigger/#0/Flags"].setFlags({"TestFlag"});
        v["DataTrigger/aerosol/Neph/Parameters/#0/Output"].setString("Condition met at ${LATEST}");
        v["DataTrigger/aerosol/Neph/Parameters/#0/RequireLatest"].setBool(true);

        SequenceValue::Transfer toAdd;
        toAdd.push_back(SequenceValue({"sfa", "configuration", "email"}, v, FP::undefined(),
                                      FP::undefined()));

        toAdd.push_back(
                SequenceValue({"brw", "raw", "F1_S11"}, Variant::Root(Variant::Flags{"TestFlag"}),
                              1412121600, 1412121660));

        toAdd.push_back(SequenceValue({"sfa", "raw", "F1_S11"}, Variant::Root(Variant::Type::Flags),
                                      1412121600,
                                      1412121660));
        toAdd.push_back(SequenceValue({"sfa", "raw", "F1_S11"}, Variant::Root(Variant::Type::Flags),
                                      1412121660,
                                      1412121720));
        toAdd.push_back(
                SequenceValue({"sfa", "raw", "F1_S11"}, Variant::Root(Variant::Flags{"TestFlag"}),
                              1412121720, 1412121840));
        toAdd.push_back(SequenceValue({"sfa", "raw", "F1_S11"}, Variant::Root(Variant::Type::Flags),
                                      1412121840,
                                      1412121900));

        Archive::Access(databaseFile).writeSynchronous(toAdd);

        ComponentOptions options(component->getOptions());
        ExternalConverter
                *input = component->createExternalIngress(options, 1412121600, 1412121840);
        QVERIFY(input != NULL);
        input->start();

        StreamSink::Buffer ingress;
        input->setEgress(&ingress);

        QVERIFY(input->wait());
        delete input;

        QVERIFY(ingress.ended());
        std::vector<Variant::Root> outputValues;
        for (const auto &dv : ingress.values()) {
            outputValues.emplace_back(dv.root());
        }

        std::vector<Variant::Root> values;
        v.write().setEmpty();
        v["Type"].setString("Break");
        values.emplace_back(v);

        v.write().setEmpty();
        v["Type"].setString("Text");
        v["Text"].setString("Condition met at 2014-10-01T00:04:00Z");
        values.emplace_back(v);

        QCOMPARE(outputValues, values);


        ingress.reset();
        options = component->getOptions();
        input = component->createExternalIngress(options, 1412121600, 1412121720);
        QVERIFY(input != NULL);
        input->start();

        input->setEgress(&ingress);

        QVERIFY(input->wait());
        delete input;

        QVERIFY(ingress.ended());
        QVERIFY(ingress.values().empty());


        ingress.reset();
        options = component->getOptions();
        input = component->createExternalIngress(options, 1412121600, 1412121900);
        QVERIFY(input != NULL);
        input->start();

        input->setEgress(&ingress);

        QVERIFY(input->wait());
        delete input;

        QVERIFY(ingress.ended());
        QVERIFY(ingress.values().empty());
    }

    void beforeAfter()
    {
        Variant::Root v;
        v["DataTrigger/aerosol/Neph/Input/Variable"].setString("F1_S11");
        v["DataTrigger/aerosol/Neph/Before/#0/Text"].setString("Before");
        v["DataTrigger/aerosol/Neph/After/#0/Text"].setString("After");
        v["DataTrigger/aerosol/Neph/Parameters/#0/Trigger/#0/Type"].setString("HasFlag");
        v["DataTrigger/aerosol/Neph/Parameters/#0/Trigger/#0/Input/Variable"].setString("F1_.*");
        v["DataTrigger/aerosol/Neph/Parameters/#0/Trigger/#0/Flags"].setFlags({"TestFlag"});
        v["DataTrigger/aerosol/Neph/Parameters/#0/Output"].setString("Condition met at ${LATEST}");
        v["DataTrigger/aerosol/Neph/Parameters/#0/RequireLatest"].setBool(true);

        SequenceValue::Transfer toAdd;
        toAdd.push_back(SequenceValue({"sfa", "configuration", "email"}, v, FP::undefined(),
                                      FP::undefined()));

        toAdd.push_back(
                SequenceValue({"brw", "raw", "F1_S11"}, Variant::Root(Variant::Flags{"TestFlag"}),
                              1412121600, 1412121660));

        toAdd.push_back(
                SequenceValue({"sfa", "raw", "F1_S11"}, Variant::Root(Variant::Flags()), 1412121600,
                              1412121660));
        toAdd.push_back(
                SequenceValue({"sfa", "raw", "F1_S11"}, Variant::Root(Variant::Flags()), 1412121660,
                              1412121720));
        toAdd.push_back(
                SequenceValue({"sfa", "raw", "F1_S11"}, Variant::Root(Variant::Flags{"TestFlag"}),
                              1412121720, 1412121840));
        toAdd.push_back(
                SequenceValue({"sfa", "raw", "F1_S11"}, Variant::Root(Variant::Flags()), 1412121840,
                              1412121900));

        Archive::Access(databaseFile).writeSynchronous(toAdd);

        ComponentOptions options(component->getOptions());
        ExternalConverter
                *input = component->createExternalIngress(options, 1412121600, 1412121840);
        QVERIFY(input != NULL);
        input->start();

        StreamSink::Buffer ingress;
        input->setEgress(&ingress);

        QVERIFY(input->wait());
        delete input;

        QVERIFY(ingress.ended());
        std::vector<Variant::Root> outputValues;
        for (const auto &dv : ingress.values()) {
            outputValues.emplace_back(dv.getValue());
        }

        std::vector<Variant::Root> values;
        v.write().setEmpty();
        v["Text"].setString("Before");
        values.emplace_back(v);

        v.write().setEmpty();
        v["Type"].setString("Text");
        v["Text"].setString("Condition met at 2014-10-01T00:04:00Z");
        values.emplace_back(v);

        v.write().setEmpty();
        v["Text"].setString("After");
        values.emplace_back(v);

        QCOMPARE(outputValues, values);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
