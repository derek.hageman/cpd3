/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <vector>
#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/valueoptionparse.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/segment.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/stream.hxx"
#include "editing/directivecontainer.hxx"

#include "email_datatrigger.hxx"


Q_LOGGING_CATEGORY(log_email_datatrigger, "cpd3.email.datatrigger", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Editing;

EmailDataTrigger::EmailDataTrigger()
        : profile(), stations(), start(FP::undefined()), end(FP::undefined())
{
    profile = SequenceName::impliedProfile(&archive);
}

EmailDataTrigger::EmailDataTrigger(const ComponentOptions &options,
                                   double start,
                                   double end,
                                   const std::vector<SequenceName::Component> &setStations)
        : profile(), stations(setStations), start(start), end(end)
{
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    } else {
        profile = SequenceName::impliedProfile(&archive);
    }
}

EmailDataTrigger::~EmailDataTrigger()
{
}

void EmailDataTrigger::incomingData(const CPD3::Util::ByteView &)
{ externalNotify(); }

void EmailDataTrigger::endData()
{
    externalNotify();
}


struct DataTriggerOutput {
    double time;
    qint64 count;
    double latestEnd;

    DataTriggerOutput() : time(FP::undefined()), count(0), latestEnd(FP::undefined())
    { }
};

struct DataTriggerResult {
    DataTriggerOutput total;
    DataTriggerOutput passed;
};

namespace {
struct TriggerData {
    SequenceName::Component station;
    QString code;
    Variant::Read config;
};
struct OutputData {
    SequenceName::Component station;
    Variant::Read config;
    QHash<QString, DataTriggerResult *> results;
    std::unordered_map<QString, Variant::Read> messages;
};

class TriggerEngine : public AsyncProcessingStage {
    EditDirective directive;

    class Target;

    friend class Target;

    class Target : public EditDispatchTarget {
        TriggerEngine *engine;
    public:
        Target(TriggerEngine *e) : engine(e)
        { }

        virtual ~Target()
        { }

        void incomingData(const SequenceValue &value) override
        { engine->egress->incomingData(value); }

        void incomingData(const SequenceValue::Transfer &values) override
        { engine->egress->incomingData(values); }

        void incomingData(SequenceValue &&value) override
        { engine->egress->incomingData(std::move(value)); }

        void incomingData(SequenceValue::Transfer &&values) override
        { engine->egress->incomingData(std::move(values)); }
    };

    Target target;
public:
    explicit TriggerEngine(Trigger *trigger,
                           const EditDirective::FanoutMode &fanout = EditDirective::Fanout_Default)
            : directive(FP::undefined(), FP::undefined(), trigger, new ActionRemove(
            SequenceMatch::Composite(SequenceMatch::Element::SpecialMatch::All)), fanout,
                        EditDirective::Fanout_None), target(this)
    {
        Q_ASSERT(!directive.mustSynchronizeOutput());
        directive.setOutput(&target);
    }

    virtual ~TriggerEngine() = default;

    SequenceName::Set requestedInputs() override
    {
        SequenceName::Set result;
        Util::merge(directive.requestedInputs(), result);
        return result;
    }

    SequenceName::Set predictedOutputs() override
    {
        SequenceName::Set result;
        Util::merge(directive.predictedOutputs(), result);
        return result;
    }

protected:
    virtual void process(SequenceValue::Transfer &&incoming)
    { directive.incomingData(std::move(incoming)); }

    virtual void finish()
    {
        directive.finalize();
        egress->endData();
    }

    virtual bool finalize()
    {
        directive.signalTerminate();
        return true;
    }
};

class ResultIngress : public StreamSink {
public:
    DataTriggerOutput *target;
    SequenceMatch::Composite selection;
    SequenceName::Map<bool> accept;
    ValueSegment::Stream reader;

    ResultIngress(DataTriggerOutput *t, const SequenceMatch::Composite &sel) : target(t),
                                                                               selection(sel)
    { }

    virtual ~ResultIngress() = default;

    void incomingData(const SequenceValue::Transfer &values) override
    {
        for (const auto &add : values) {
            auto check = accept.find(add.getUnit());
            if (check == accept.end()) {
                bool result = selection.matches(add.getUnit());
                accept.emplace(add.getUnit(), result);
                if (!result) {
                    process(reader.completedAdvance(add.getStart()));
                    continue;
                }
            } else if (!check->second) {
                process(reader.completedAdvance(add.getStart()));
                continue;
            }
            process(reader.add(add));
        }
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        for (const auto &add : values) {
            auto check = accept.find(add.getUnit());
            if (check == accept.end()) {
                bool result = selection.matches(add.getUnit());
                accept.emplace(add.getUnit(), result);
                if (!result) {
                    process(reader.completedAdvance(add.getStart()));
                    continue;
                }
            } else if (!check->second) {
                process(reader.completedAdvance(add.getStart()));
                continue;
            }
            process(reader.add(std::move(add)));
        }
    }

    void incomingData(const SequenceValue &add) override
    {
        auto check = accept.find(add.getUnit());
        if (check == accept.end()) {
            bool result = selection.matches(add.getUnit());
            accept.emplace(add.getUnit(), result);
            if (!result) {
                process(reader.completedAdvance(add.getStart()));
                return;
            }
        } else if (!check->second) {
            process(reader.completedAdvance(add.getStart()));
            return;
        }
        process(reader.add(add));
    }

    void incomingData(SequenceValue &&add) override
    {
        auto check = accept.find(add.getUnit());
        if (check == accept.end()) {
            bool result = selection.matches(add.getUnit());
            accept.emplace(add.getUnit(), result);
            if (!result) {
                process(reader.completedAdvance(add.getStart()));
                return;
            }
        } else if (!check->second) {
            process(reader.completedAdvance(add.getStart()));
            return;
        }
        process(reader.add(std::move(add)));
    }

    void endData() override
    { process(reader.finish()); }

private:
    void process(ValueSegment::Transfer &&incoming)
    {
        for (auto &value : incoming) {
            if (!value.read().exists())
                continue;
            target->count++;

            if (!FP::defined(value.getStart()) || !FP::defined(value.getEnd()))
                continue;

            double dT = value.getEnd() - value.getStart();
            if (!FP::defined(target->time)) {
                target->time = dT;
            } else {
                target->time += dT;
            }
            target->latestEnd = value.getEnd();
        }
    }
};

struct RunData {
    TriggerEngine *engine;
    ResultIngress *input;
    ResultIngress *output;
    TriggerData data;
};

class InputFanout : public StreamSink {
    std::vector<StreamSink *> targets;
public:
    explicit InputFanout(const QList<RunData> &t)
    {
        Q_ASSERT(!t.empty());
        for (const auto &run : t) {
            targets.push_back(run.engine);
            targets.push_back(run.input);
        }
    }

    virtual ~InputFanout() = default;

    void incomingData(const SequenceValue::Transfer &values) override
    {
        for (auto t : targets) {
            t->incomingData(values);
        }
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        auto t = targets.begin();
        for (auto end = targets.end() - 1; t != end; ++t) {
            (*t)->incomingData(values);
        }
        (*t)->incomingData(std::move(values));
    }

    void incomingData(const SequenceValue &value) override
    {
        for (auto t : targets) {
            t->incomingData(value);
        }
    }

    void incomingData(SequenceValue &&value) override
    {
        auto t = targets.begin();
        for (auto end = targets.end() - 1; t != end; ++t) {
            (*t)->incomingData(value);
        }
        (*t)->incomingData(std::move(value));
    }

    void endData() override
    {
        for (auto t : targets) {
            t->endData();
        }
    }
};

};

EmailDataTrigger::Substitutions::Substitutions() : defaultResults()
{
    defaultResults.push_back(NULL);
}

EmailDataTrigger::Substitutions::~Substitutions()
{ }

void EmailDataTrigger::Substitutions::push()
{
    TextSubstitutionStack::push();
    defaultResults.push_back(NULL);
}

void EmailDataTrigger::Substitutions::push(DataTriggerResult *defaultResult)
{
    TextSubstitutionStack::push();
    defaultResults.push_back(defaultResult);
}

void EmailDataTrigger::Substitutions::pop()
{
    TextSubstitutionStack::pop();
    Q_ASSERT(defaultResults.size() > 1);
    defaultResults.pop_back();
}

void EmailDataTrigger::Substitutions::clear()
{
    TextSubstitutionStack::clear();
    defaultResults.clear();
    defaultResults.push_back(NULL);
}

QString EmailDataTrigger::Substitutions::substitution(const QStringList &elements) const
{
    if (defaultResults.back() != NULL) {
        if (elements.isEmpty() || !keyClaimed(elements.first()))
            return formatResult(defaultResults.back(), elements);
    }
    return TextSubstitutionStack::substitution(elements);
}

void EmailDataTrigger::Substitutions::setResult(const QString &key, const DataTriggerResult *value)
{
    setReplacement(key, new ResultReplacer(value));
}

static double percentageOrZero(const DataTriggerResult *input)
{
    if (!FP::defined(input->total.time) || !FP::defined(input->passed.time))
        return 0.0;
    if (input->total.time <= 0.0 || input->passed.time <= 0.0)
        return 0.0;
    if (input->passed.time >= input->total.time)
        return 100.0;
    return (input->passed.time / input->total.time) * 100.0;
}

QString EmailDataTrigger::Substitutions::formatResult(const DataTriggerResult *result,
                                                      const QStringList &elements)
{
    int index = 0;
    QString type;
    if (index < elements.size())
        type = elements.at(index++).toLower();
    if (type == "percent") {
        return formatDouble(percentageOrZero(result), elements.mid(index));
    } else if (type == "n" || type == "count") {
        return formatInteger(result->passed.count, elements.mid(index));
    } else if (type == "latest") {
        return formatTime(result->passed.latestEnd, elements.mid(index));
    } else if (type == "dataend") {
        return formatTime(result->total.latestEnd, elements.mid(index));
    } else if (type == "total") {
        return formatDuration(result->total.time, elements.mid(index));
    }

    return formatDuration(result->passed.time, elements.mid(index));
}

EmailDataTrigger::Substitutions::ResultReplacer::ResultReplacer(const DataTriggerResult *r)
        : result(r)
{ }

EmailDataTrigger::Substitutions::ResultReplacer::~ResultReplacer()
{ }

QString EmailDataTrigger::Substitutions::ResultReplacer::get(const QStringList &elements) const
{ return formatResult(result, elements); }


bool EmailDataTrigger::begin()
{
    QList<OutputData> outputs;

    do {
        SequenceSegment configSegment;
        {
            Archive::Access::ReadLock lock(archive);
            auto allStations = archive.availableStations(stations);
            if (allStations.empty()) {
                qCDebug(log_email_datatrigger) << "No stations available";
                return false;
            }
            stations.clear();
            std::copy(allStations.begin(), allStations.end(), Util::back_emplacer(stations));

            double tnow = Time::time();
            auto confList = SequenceSegment::Stream::read(
                    Archive::Selection(tnow - 1.0, tnow + 1.0, stations, {"configuration"},
                                       {"email"}), &archive);
            auto f = Range::findIntersecting(confList, tnow);
            if (f == confList.end())
                break;
            configSegment = *f;
        }

        for (const auto &station : stations) {
            if (isTerminated())
                break;
            SequenceName stationUnit(station, "configuration", "email");
            auto base =
                    configSegment.takeValue(stationUnit).read().hash("DataTrigger").hash(profile);
            if (!base.exists())
                continue;
            for (auto block : base.toChildren()) {
                if (isTerminated())
                    break;

                SequenceMatch::OrderedLookup inputSelection(block["Input"],
                                                            SequenceMatch::Element::PatternList{
                                                                    QString::fromStdString(
                                                                            station)}, {"raw"}, {});
                inputSelection.registerExpected(station, "raw");

                std::vector<TriggerData> triggers;
                {
                    auto children = block["Parameters"].toChildren();
                    for (auto add = children.begin(), endAdd = children.end();
                            add != endAdd;
                            ++add) {
                        TriggerData data;
                        data.station = station;
                        data.config = add.value();
                        auto check = add.stringKey();
                        if (!check.empty())
                            data.code = QString::fromStdString(check);
                        else
                            data.code = QString::number(static_cast<int>(add.integerKey()));
                        triggers.emplace_back(std::move(data));
                    }
                }

                double loadStart = start;
                double loadEnd = end;

                QSet<SequenceName> additionalInputs;
                QList<RunData> active;
                OutputData outputData;
                outputData.station = station;
                outputData.config = block;

                for (auto &t : triggers) {
                    DataTriggerResult *result = new DataTriggerResult;
                    Q_ASSERT(!outputData.results.contains(t.code));
                    outputData.results.insert(t.code, result);
                    outputData.messages.emplace(t.code, t.config);

                    Trigger *trigger =
                            DirectiveContainer::createTrigger(t.config["Trigger"], station, "raw");
                    trigger->setInverted(!trigger->inverted());

                    if (trigger->isNeverActive(start, end)) {
                        delete trigger;
                        continue;
                    }

                    double checkStart = start;
                    double checkEnd = end;
                    trigger->extendProcessing(checkStart, checkEnd);
                    if (Range::compareStart(checkStart, loadStart) < 0)
                        loadStart = checkStart;
                    if (Range::compareEnd(checkEnd, loadEnd) > 0)
                        loadEnd = checkEnd;

                    RunData run;

                    SequenceMatch::OrderedLookup outputSelection;
                    if (!t.config["Data"].exists()) {
                        outputSelection = inputSelection;
                    } else {
                        outputSelection =
                                SequenceMatch::OrderedLookup(t.config["Data"],  SequenceMatch::Element::PatternList{
                                                                     QString::fromStdString(
                                                                             station)}, {"raw"},
                                                             {});
                    }
                    outputSelection.registerExpected(station, "raw");
                    Util::merge(inputSelection.knownInputs(), additionalInputs);
                    Util::merge(outputSelection.knownInputs(), additionalInputs);

                    run.engine = new TriggerEngine(trigger, DirectiveContainer::toFanout(
                            t.config["Trigger/Fanout"]));
                    run.data = std::move(t);

                    Util::merge(run.engine->requestedInputs(), additionalInputs);

                    run.input = new ResultIngress(&result->total, outputSelection);
                    run.output = new ResultIngress(&result->passed, outputSelection);

                    run.engine->setEgress(run.output);
                    run.engine->start();

                    active.append(run);
                }
                triggers.clear();

                if (active.isEmpty()) {
                    qDeleteAll(outputData.results);
                    continue;
                }

                qCDebug(log_email_datatrigger) << "Starting processing for" << active.size()
                                               << "trigger(s)";

                auto readSelections = inputSelection.toArchiveSelections();
                for (auto &mod : readSelections) {
                    mod.start = loadStart;
                    mod.end = loadEnd;
                }
                for (const auto &add : additionalInputs) {
                    readSelections.emplace_back(add, loadStart, loadEnd);
                }
                {
                    InputFanout fanout(active);
                    archive.waitForLocks();
                    Archive::Access::ReadLock lock(archive, true);
                    auto reader = archive.readStream(readSelections, &fanout);
                    reader->wait();
                }

                for (QList<RunData>::const_iterator run = active.constBegin(),
                        end = active.constEnd(); run != end; ++run) {
                    run->engine->wait();
                }

                bool anyPassed = false;

                qint64 minimumEvents = block["Threshold/Count"].toInt64();
                if (!INTEGER::defined(minimumEvents) || minimumEvents < 1)
                    minimumEvents = 1;
                double minimumTime = block["Threshold/Time"].toReal();

                for (const auto &run : active) {
                    auto total = run.input->target;
                    auto passed = run.output->target;

                    delete run.engine;
                    delete run.input;
                    delete run.output;

                    if (passed->count < minimumEvents)
                        continue;
                    if (FP::defined(minimumTime)) {
                        if (!FP::defined(passed->time))
                            continue;
                        if (passed->time < minimumTime)
                            continue;
                    }

                    if (run.data.config["RequireLatest"].toBool()) {
                        if (!FP::defined(passed->latestEnd))
                            continue;
                        if (!FP::defined(total->latestEnd))
                            continue;
                        if (total->latestEnd > passed->latestEnd)
                            continue;
                    }

                    anyPassed = true;
                }
                if (!anyPassed) {
                    qDeleteAll(outputData.results);
                    continue;
                }

                outputs.append(outputData);
            }
        }

        if (isTerminated())
            break;

        qCDebug(log_email_datatrigger) << "Generated" << outputs.size() << "data set(s)";
    } while (false);

    substitutions.clear();
    substitutions.setTime("start", start);
    substitutions.setTime("end", end);

    for (QList<OutputData>::const_iterator data = outputs.constBegin(),
            endOutputs = outputs.constEnd(); data != endOutputs; ++data) {
        TextSubstitutionStack::Context substation(substitutions);
        substitutions.setString("station", QString::fromStdString(data->station).toLower());

        for (QHash<QString, DataTriggerResult *>::const_iterator sub = data->results.constBegin(),
                endSub = data->results.constEnd(); sub != endSub; ++sub) {
            substitutions.setResult(sub.key(), sub.value());
        }

        for (auto m : data->config["Before"].toArray()) {
            messages.emplace_back(m);
        }

        QString output(data->config["Output"].toQString());
        output = substitutions.apply(output);
        if (!output.isEmpty()) {
            if (messages.empty()) {
                Variant::Root addBreak(data->config["Message"]);
                addBreak["Type"].setString("Break");
                messages.emplace_back(std::move(addBreak));
            }

            Variant::Root addMessage(data->config["Message"]);
            addMessage["Type"].setString("Text");
            addMessage["Text"].setString(output);
            messages.emplace_back(std::move(addMessage));
        }

        QStringList resultOrder(data->results.keys());
        std::sort(resultOrder.begin(), resultOrder.end());
        for (QStringList::const_iterator code = resultOrder.constBegin(),
                endCode = resultOrder.constEnd(); code != endCode; ++code) {
            QHash<QString, DataTriggerResult *>::const_iterator
                    sub = data->results.constFind(*code);
            Q_ASSERT(sub != data->results.constEnd());
            if (sub.value()->passed.count <= 0)
                continue;

            Q_ASSERT(data->messages.count(sub.key()));
            Variant::Read config = Variant::Read::empty();
            {
                auto check = data->messages.find(sub.key());
                if (check != data->messages.end())
                    config = check->second;
            }

            output = config["Output"].toQString();
            if (output.isEmpty())
                continue;

            if (config["RequireLatest"].toBool()) {
                if (!FP::defined(sub.value()->passed.latestEnd))
                    continue;
                if (!FP::defined(sub.value()->total.latestEnd))
                    continue;
                if (sub.value()->total.latestEnd > sub.value()->passed.latestEnd)
                    continue;
            }

            substitutions.push(sub.value());
            output = substitutions.apply(output);
            substitutions.pop();

            if (output.isEmpty())
                continue;

            Variant::Root baseline(data->config["Message"]);
            auto over = config["Message"];
            if (over.exists()) {
                baseline = Variant::Root::overlay(baseline, Variant::Root(over));
            }

            if (messages.empty()) {
                Variant::Root addBreak(baseline);
                addBreak["Type"].setString("Break");
                messages.emplace_back(std::move(addBreak));
            }

            Variant::Root addMessage(baseline);
            addMessage["Type"].setString("Text");
            addMessage["Text"].setString(output);
            messages.emplace_back(std::move(addMessage));
        }

        for (auto m : data->config["After"].toArray()) {
            messages.emplace_back(m);
        }

        qDeleteAll(data->results);
    }

    if (isTerminated())
        return false;

    return true;
}

bool EmailDataTrigger::prepare()
{ return true; }

bool EmailDataTrigger::process()
{
    SequenceValue::Transfer toOutput;
    double tnow = Time::time();
    qCDebug(log_email_datatrigger) << "Generated" << messages.size() << "message(s)";
    int priority = 0;
    for (auto &v : messages) {
        toOutput.emplace_back(
                SequenceIdentity({"message", "message", "message"}, tnow, tnow, priority++),
                std::move(v));
    }

    messages.clear();
    outputData(toOutput);
    return false;
}

void EmailDataTrigger::signalTerminate()
{
    archive.signalTerminate();
    ExternalSourceProcessor::signalTerminate();
}


ComponentOptions EmailDataTriggerComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Execution profile"),
                                                tr("This is the profile name to execute.  Multiple profiles can be "
                                                   "defined to allow for different output types for a single "
                                                   "station.  Consult the station configuration for available profiles."),
                                                tr("aerosol")));

    return options;
}

QList<ComponentExample> EmailDataTriggerComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(
            ComponentExample(options, tr("Generate output for new values", "default example name"),
                             tr("This will generate the new value outputs for the \"aerosol\" "
                                "profile.")));

    return examples;
}

int EmailDataTriggerComponent::ingressAllowStations()
{ return INT_MAX; }

bool EmailDataTriggerComponent::ingressRequiresTime()
{ return true; }

ExternalConverter *EmailDataTriggerComponent::createExternalIngress(const ComponentOptions &options,
                                                                    double start,
                                                                    double end,
                                                                    const std::vector<
                                                                            SequenceName::Component> &stations)
{ return new EmailDataTrigger(options, start, end, stations); }
