/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef EMAILDATATRIGGER_H
#define EMAILDATATRIGGER_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>
#include <QStringList>

#include "core/component.hxx"
#include "core/textsubstitution.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/archive/access.hxx"

struct DataTriggerResult;

class EmailDataTrigger : public CPD3::Data::ExternalSourceProcessor {
    std::string profile;
    std::vector<CPD3::Data::SequenceName::Component> stations;
    double start;
    double end;

    CPD3::Data::Archive::Access archive;

    class Substitutions : public CPD3::TextSubstitutionStack {
        QList<const DataTriggerResult *> defaultResults;

        class ResultReplacer : public Replacer {
            const DataTriggerResult *result;
        public:
            ResultReplacer(const DataTriggerResult *result);

            virtual ~ResultReplacer();

            virtual QString get(const QStringList &elements) const;
        };

    public:

        Substitutions();

        virtual ~Substitutions();

        virtual void push();

        virtual void push(DataTriggerResult *defaultResult);

        virtual void pop();

        virtual void clear();

        void setResult(const QString &key, const DataTriggerResult *value);

        static QString formatResult
                (const DataTriggerResult *result, const QStringList &elements = QStringList());

    protected:
        virtual QString substitution(const QStringList &elements) const;
    };

    Substitutions substitutions;

    std::vector<CPD3::Data::Variant::Root> messages;

public:
    EmailDataTrigger();

    EmailDataTrigger(const CPD3::ComponentOptions &options,
                     double start,
                     double end,
                     const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~EmailDataTrigger();

    void incomingData(const CPD3::Util::ByteView &data) override;

    void endData() override;

    void signalTerminate() override;

protected:
    bool prepare() override;

    bool process() override;

    bool begin() override;
};

class EmailDataTriggerComponent
        : public QObject, virtual public CPD3::Data::ExternalSourceComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalSourceComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.email_datatrigger"
                              FILE
                              "email_datatrigger.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int ingressAllowStations();

    virtual bool ingressRequiresTime();

    virtual CPD3::Data::ExternalConverter *createExternalIngress(const CPD3::ComponentOptions &options,
                                                                 double start,
                                                                 double end,
                                                                 const std::vector<CPD3::Data::SequenceName::Component> &stations = {});
};

#endif
