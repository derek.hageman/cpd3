/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CONTROLTRIGGER_H
#define CONTROLTRIGGER_H

#include "core/first.hxx"

#include <vector>
#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QHash>
#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/timeutils.hxx"
#include "core/threading.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/processingtapchain.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"
#include "luascript/engine.hxx"

class ControlExport : public CPD3::Acquisition::AcquisitionInterface {
    std::string loggingName;
    QLoggingCategory log;

    bool terminated;
    CPD3::Acquisition::AcquisitionControlStream *controlStream;
    CPD3::Acquisition::AcquisitionState *state;

    bool enabled;
    bool loadBacklog;

    CPD3::Data::Variant::Root instrumentMeta;

    class RealtimeIngress : public CPD3::Data::StreamSink,
                            public CPD3::Data::ProcessingTapChain::ArchiveBackend {
        ControlExport *control;
        std::mutex realtimeMutex;
        bool isFinished;

        struct TargetData {
            std::unique_ptr<CPD3::Data::SequenceMatch::Basic> match;
            CPD3::Data::StreamSink *sink;
            double discardBefore;

            TargetData(const CPD3::Data::Archive::Selection::List &selections,
                       CPD3::Data::StreamSink *target);
        };

        std::vector<std::unique_ptr<TargetData>> targets;
        CPD3::Data::SequenceName::Map<std::vector<TargetData *>> dispatch;

        const std::vector<TargetData *> &lookup(const CPD3::Data::SequenceName &name);

    public:
        RealtimeIngress(ControlExport *ce);

        virtual ~RealtimeIngress();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        virtual void endData();

        void finish();

        virtual void issueArchiveRead(const CPD3::Data::Archive::Selection::List &selections,
                                      CPD3::Data::StreamSink *target);
    };

    friend class RealtimeIngress;

    RealtimeIngress realtimeIngress;

    class FilterIngress : public CPD3::Data::StreamSink {
        ControlExport *control;
        std::mutex mutex;
        CPD3::Data::SequenceName::Map<bool> dispatch;

        bool lookup(const CPD3::Data::SequenceName &name);
    public:
        FilterIngress(ControlExport *ce);

        virtual ~FilterIngress();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        virtual void endData();

        void reset();
    };

    friend class FilterIngress;

    FilterIngress filterIngress;

    std::mutex filterMutex;
    bool updatePending;
    bool openSegment;
    std::deque<CPD3::Data::SequenceSegment> buffer;
    CPD3::Data::SequenceSegment::Stream reader;

    CPD3::Data::ProcessingTapChain *chain;

    class ExportColumn {
    protected:
        ExportColumn(const ExportColumn &other);

    public:
        ExportColumn(const CPD3::Data::Variant::Read &config);

        virtual ~ExportColumn();

        virtual CPD3::Data::SequenceMatch::Composite getSelection() const;

        virtual bool registerInput(const CPD3::Data::SequenceName &unit);

        virtual void initialize(const CPD3::Data::SequenceName::ComponentSet &defaultStations);

        virtual QString getValue(CPD3::Data::SequenceSegment &segment) = 0;

        virtual ExportColumn *clone() const = 0;
    };

    class ExportDateTime : public ExportColumn {
        bool local;
        QString format;
    protected:
        ExportDateTime(const ExportDateTime &other);

    public:
        ExportDateTime(const CPD3::Data::Variant::Read &config);

        virtual ~ExportDateTime();

        virtual QString getValue(CPD3::Data::SequenceSegment &segment);

        virtual ExportColumn *clone() const;
    };

    class ExportSelectedValue : public ExportColumn {
    protected:
        std::string path;
        CPD3::Data::SequenceMatch::OrderedLookup selection;

        ExportSelectedValue(const ExportSelectedValue &other);

        CPD3::Data::Variant::Read selectedValue(CPD3::Data::SequenceSegment &segment);

    public:
        ExportSelectedValue(const CPD3::Data::Variant::Read &config);

        virtual ~ExportSelectedValue();

        virtual CPD3::Data::SequenceMatch::Composite getSelection() const;

        virtual bool registerInput(const CPD3::Data::SequenceName &unit);

        virtual void initialize(const CPD3::Data::SequenceName::ComponentSet &defaultStations);
    };

    class ExportSingleNumber : public ExportSelectedValue {
        QChar pad;
        CPD3::Calibration calibration;
        bool overrideFormat;
        CPD3::NumberFormat format;
        bool overrideMVC;
        QString mvc;

    protected:
        ExportSingleNumber(const ExportSingleNumber &other);

    public:
        ExportSingleNumber(const CPD3::Data::Variant::Read &config);

        virtual ~ExportSingleNumber();

        virtual QString getValue(CPD3::Data::SequenceSegment &segment);

        virtual ExportColumn *clone() const;
    };

    class ExportFlagsString : public ExportSelectedValue {
        QString join;
        QString mvc;
        std::unordered_map<CPD3::Data::Variant::Flag, QString> remap;
    protected:
        ExportFlagsString(const ExportFlagsString &other);

    public:
        ExportFlagsString(const CPD3::Data::Variant::Read &config);

        virtual ~ExportFlagsString();

        virtual QString getValue(CPD3::Data::SequenceSegment &segment);

        virtual ExportColumn *clone() const;
    };

    class ExportFlagsBits : public ExportSelectedValue {
        QChar pad;
        bool overrideBits;
        std::unordered_map<CPD3::Data::Variant::Flag, qint64> bits;
        bool overrideFormat;
        CPD3::NumberFormat format;
        bool overrideMVC;
        QString mvc;
    protected:
        ExportFlagsBits(const ExportFlagsBits &other);

    public:
        ExportFlagsBits(const CPD3::Data::Variant::Read &config);

        virtual ~ExportFlagsBits();

        virtual QString getValue(CPD3::Data::SequenceSegment &segment);

        virtual ExportColumn *clone() const;
    };

    class ExportScript : public ExportColumn {
        std::string code;
        CPD3::Lua::Engine engine;
        CPD3::Lua::Engine::Frame root;
        CPD3::Lua::Engine::Reference invoke;

        CPD3::Data::SequenceMatch::Composite selection;

        void initialize();
    protected:
        ExportScript(const ExportScript &other);

    public:
        ExportScript(const CPD3::Data::Variant::Read &config);

        virtual ~ExportScript();

        virtual CPD3::Data::SequenceMatch::Composite getSelection() const;

        virtual bool registerInput(const CPD3::Data::SequenceName &unit);

        virtual QString getValue(CPD3::Data::SequenceSegment &segment);

        virtual ExportColumn *clone() const;
    };


    friend class Configuration;

    class Configuration {
        double start;
        double end;

        void parseColumn(const CPD3::Data::Variant::Read &config,
                         const QString &defaultName = QString());

    public:
        struct ColumnData {
            QString header;
            ExportColumn *column;
        };
        QList<ColumnData> columns;

        bool outputHeader;
        QString separator;
        QString quote;
        QString quoteEscape;
        QString linePrefix;
        QString lineSuffix;

        QString fileTarget;
        bool append;
        bool asBinary;
        bool noFile;

        QString command;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        QString join(const QStringList &fields) const;

        Configuration();

        ~Configuration();

        Configuration(const Configuration &other);

        Configuration &operator=(const Configuration &other);

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    CPD3::Data::DynamicTimeInterval *bufferDuration;

    void executeExport();

    QHash<CPD3::Data::SequenceName, bool> dispatch;

    CPD3::Threading::Receiver receiver;

    void advanceTime(double executeTime);

public:
    virtual ~ControlExport();

    ControlExport(const CPD3::Data::ValueSegment::Transfer &config,
                  const std::string &loggingContext);

    void setControlStream(CPD3::Acquisition::AcquisitionControlStream *controlStream) override;

    void setState(CPD3::Acquisition::AcquisitionState *state) override;

    void setRealtimeEgress(CPD3::Data::StreamSink *egress) override;

    void setLoggingEgress(CPD3::Data::StreamSink *egress) override;

    void incomingData(const CPD3::Util::ByteView &data,
                      double time,
                      AcquisitionInterface::IncomingDataType type = IncomingDataType::Stream) override;

    void incomingControl(const CPD3::Util::ByteView &data,
                         double time,
                         AcquisitionInterface::IncomingDataType type = IncomingDataType::Stream) override;

    void incomingTimeout(double time) override;

    void incomingAdvance(double time) override;

    CPD3::Data::StreamSink *getRealtimeIngress() override;

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    AutomaticDefaults getDefaults() override;

    bool isCompleted() override;

    void signalTerminate() override;
};

class ControlExportComponent
        : public QObject, virtual public CPD3::Acquisition::AcquisitionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.control_export"
                              FILE
                              "control_export.json")
public:
    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config = {},
                                                                                const std::string &loggingContext = {}) override;
};

#endif
