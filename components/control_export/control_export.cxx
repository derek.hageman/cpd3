/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <thread>
#include <QDateTime>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/textsubstitution.hxx"
#include "core/qtcompat.hxx"
#include "core/util.hxx"
#include "datacore/stream.hxx"
#include "datacore/segment.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "luascript/libs/sequencesegment.hxx"
#include "io/process.hxx"
#include "io/drivers/file.hxx"

#include "control_export.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;


ControlExport::ExportColumn::ExportColumn(const Variant::Read &)
{ }

ControlExport::ExportColumn::ExportColumn(const ExportColumn &other) = default;

ControlExport::ExportColumn::~ExportColumn() = default;

SequenceMatch::Composite ControlExport::ExportColumn::getSelection() const
{ return SequenceMatch::Composite(); }

bool ControlExport::ExportColumn::registerInput(const SequenceName &unit)
{
    Q_UNUSED(unit);
    return false;
}

void ControlExport::ExportColumn::initialize(const SequenceName::ComponentSet &defaultStations)
{ }

ControlExport::ExportDateTime::ExportDateTime(const Variant::Read &config) : ExportColumn(config),
                                                                             local(config["LocalTime"]
                                                                                           .toBool()),
                                                                             format(config["TimeFormat"]
                                                                                            .toQString())
{
    if (format.isEmpty()) {
        if (local) {
            format = "yyyy-MM-ddThh:mm:ssZZZ";
        } else {
            format = "yyyy-MM-ddThh:mm:ssZ";
        }
    }
}

ControlExport::ExportDateTime::ExportDateTime(const ExportDateTime &other) = default;

ControlExport::ExportDateTime::~ExportDateTime() = default;

QString ControlExport::ExportDateTime::getValue(SequenceSegment &segment)
{
    double time = segment.getStart();
    if (!FP::defined(time))
        return QString();
    QDateTime dt = QDateTime::fromMSecsSinceEpoch(static_cast<qint64>(std::round(time * 1000.0)));

    QString effectiveFormat(format);
    if (local) {
        dt = dt.toLocalTime();
        if (dt.toUTC().time() == dt.time()) {
            dt = dt.toUTC();
            effectiveFormat.replace("ZZZZZZ", "Z");
            effectiveFormat.replace("ZZZ", "Z");
        } else {
            int deltaTZ = dt.toUTC().time().secsTo(dt.time());
            if (deltaTZ > 86400 / 2)
                deltaTZ -= 86400;
            else if (deltaTZ < -86400 / 2)
                deltaTZ += 86400;

            if (deltaTZ >= 86400)
                deltaTZ = 86400 - 1;
            else if (deltaTZ <= -86400)
                deltaTZ = -(86400 - 1);

            QString hours;
            if (deltaTZ < 0) {
                hours.append('-');
                deltaTZ = -deltaTZ;
            } else {
                hours.append('+');
            }
            hours.append(QString::number((deltaTZ / 3600) % 24).rightJustified(2, '0'));

            QString minutes(hours);
            minutes.append(':');
            minutes.append(QString::number((deltaTZ / 60) % 60).rightJustified(2, '0'));

            effectiveFormat.replace("ZZZZZZ", minutes);
            effectiveFormat.replace("ZZZ", hours);
        }
    } else {
        dt = dt.toUTC();
        effectiveFormat.replace("ZZZZZZ", "Z");
        effectiveFormat.replace("ZZZ", "Z");
    }

    return dt.toString(effectiveFormat);
}

ControlExport::ExportColumn *ControlExport::ExportDateTime::clone() const
{ return new ExportDateTime(*this); }


ControlExport::ExportSelectedValue::ExportSelectedValue(const CPD3::Data::Variant::Read &config)
        : ExportColumn(config), path(config["Path"].toString()), selection(config["Input"])
{ }

ControlExport::ExportSelectedValue::ExportSelectedValue(const ExportSelectedValue &other) = default;

ControlExport::ExportSelectedValue::~ExportSelectedValue() = default;

Variant::Read ControlExport::ExportSelectedValue::selectedValue(CPD3::Data::SequenceSegment &segment)
{ return selection.lookup(segment).getPath(path); }

CPD3::Data::SequenceMatch::Composite ControlExport::ExportSelectedValue::getSelection() const
{ return selection; }

bool ControlExport::ExportSelectedValue::registerInput(const SequenceName &unit)
{
    if (!unit.isMeta())
        return selection.registerInput(unit);
    return selection.registerInput(unit.fromMeta());
}


void ControlExport::ExportSelectedValue::initialize(const SequenceName::ComponentSet &defaultStations)
{
    for (const auto &station : defaultStations) {
        selection.registerExpected(station, "raw");
        selection.registerExpected(station, "rt_instant");
        selection.registerExpected(station, "rt_boxcar");
    }
}


ControlExport::ExportSingleNumber::ExportSingleNumber(const Variant::Read &config)
        : ExportSelectedValue(config),
          pad('0'),
          calibration(Variant::Composite::toCalibration(config["Calibration"])),
          overrideFormat(config["Format"].exists()),
          format(config["Format"].toQString()),
          overrideMVC(config["MVC"].exists()),
          mvc(config["MVC"].toQString())
{
    if (config["Pad"].exists()) {
        QString check(config["Pad"].toQString());
        if (check.length() > 0)
            pad = check.at(0);
        else
            pad = QChar();
    }
}

ControlExport::ExportSingleNumber::ExportSingleNumber(const ExportSingleNumber &other)
        : ExportSelectedValue(other),
          pad(other.pad),
          calibration(other.calibration),
          overrideFormat(other.overrideFormat),
          format(other.format),
          overrideMVC(other.overrideMVC),
          mvc(other.mvc)
{ }

ControlExport::ExportSingleNumber::~ExportSingleNumber() = default;

QString ControlExport::ExportSingleNumber::getValue(SequenceSegment &segment)
{
    double v = selectedValue(segment).toDouble();
    if (!FP::defined(v)) {
        if (overrideMVC)
            return mvc;

        for (const auto &n : selection.knownInputs()) {
            Util::ReferenceCopy<SequenceName> effective(n);
            if (!effective().isMeta())
                effective->setMeta();
            auto check = segment.getValue(effective()).metadata("MVC");
            if (check.exists())
                return check.toQString();
        }
    }

    v = calibration.apply(v);

    if (overrideFormat)
        return format.apply(v, pad);

    NumberFormat fmt;
    for (const auto &n : selection.knownInputs()) {
        Util::ReferenceCopy<SequenceName> effective(n);
        if (!effective().isMeta())
            effective->setMeta();
        QString check(segment.getValue(effective()).metadata("Format").toQString());
        if (!check.isEmpty()) {
            fmt = NumberFormat(check);
            break;
        }

        check = segment.getValue(effective()).metadata("MVC").toQString();
        if (!check.isEmpty())
            fmt = NumberFormat(check);
    }
    return fmt.apply(v, pad);
}

ControlExport::ExportColumn *ControlExport::ExportSingleNumber::clone() const
{ return new ExportSingleNumber(*this); }


ControlExport::ExportFlagsString::ExportFlagsString(const Variant::Read &config)
        : ExportSelectedValue(config),
          join(config["Join"].toQString()),
          mvc(config["MVC"].toQString()),
          remap()
{
    if (!config["Join"].exists())
        join = ";";
    if (config["FlagsRename"].exists()) {
        for (auto add : config["FlagsRename"].toHash()) {
            this->remap.emplace(add.first, add.second.toQString());
        }
    }
}

ControlExport::ExportFlagsString::ExportFlagsString(const ExportFlagsString &other)
        : ExportSelectedValue(other), join(other.join), mvc(other.mvc), remap(other.remap)
{ }

ControlExport::ExportFlagsString::~ExportFlagsString()
{ }

QString ControlExport::ExportFlagsString::getValue(CPD3::Data::SequenceSegment &segment)
{
    auto rawValue = selectedValue(segment);
    if (rawValue.getType() != Variant::Type::Flags)
        return mvc;

    QSet<QString> outputValue;
    for (const auto &f : rawValue.toFlags()) {
        if (f.empty())
            continue;
        auto check = remap.find(f);
        if (check == remap.end())
            check = remap.find(Variant::Flag());
        if (check != remap.end()) {
            if (check->second.isEmpty())
                continue;
            outputValue.insert(check->second);
            continue;
        }

        outputValue.insert(QString::fromStdString(f));
    }

    QStringList sorted(outputValue.values());
    std::sort(sorted.begin(), sorted.end());
    return sorted.join(join);
}

ControlExport::ExportColumn *ControlExport::ExportFlagsString::clone() const
{ return new ExportFlagsString(*this); }


ControlExport::ExportFlagsBits::ExportFlagsBits(const Variant::Read &config) : ExportSelectedValue(
        config),
                                                                               pad('0'),
                                                                               overrideBits(false),
                                                                               bits(),
                                                                               overrideFormat(
                                                                                       config["Format"]
                                                                                               .exists()),
                                                                               format(config["Format"]
                                                                                              .toQString()),
                                                                               overrideMVC(
                                                                                       config["MVC"]
                                                                                               .exists()),
                                                                               mvc(config["MVC"].toQString())
{
    if (config["Pad"].exists()) {
        QString check(config["Pad"].toQString());
        if (check.length() > 0)
            pad = check.at(0);
        else
            pad = QChar();
    }
    if (config["Bits"].exists()) {
        overrideBits = true;
        for (auto add : config["Bits"].toHash()) {
            qint64 i = add.second.toInt64();
            if (!INTEGER::defined(i) || i == 0)
                continue;
            this->bits.emplace(add.first, i);
        }
    }
}

ControlExport::ExportFlagsBits::ExportFlagsBits(const ExportFlagsBits &other) : ExportSelectedValue(
        other),
                                                                                pad(other.pad),
                                                                                overrideBits(
                                                                                        other.overrideBits),
                                                                                bits(other.bits),
                                                                                overrideFormat(
                                                                                        other.overrideFormat),
                                                                                format(other.format),
                                                                                overrideMVC(
                                                                                        other.overrideMVC),
                                                                                mvc(other.mvc)
{ }

ControlExport::ExportFlagsBits::~ExportFlagsBits() = default;

QString ControlExport::ExportFlagsBits::getValue(SequenceSegment &segment)
{
    auto raw = selectedValue(segment);
    qint64 bits = INTEGER::undefined();
    if (!raw.exists()) {
        if (overrideMVC)
            return mvc;

        for (const auto &n : selection.knownInputs()) {
            Util::ReferenceCopy<SequenceName> effective(n);
            if (!effective().isMeta())
                effective->setMeta();
            auto check = segment.getValue(effective()).metadata("MVC");
            if (check.exists())
                return check.toQString();
        }
    } else {
        bits = 0;
        const auto &flags = raw.toFlags();
        if (overrideBits) {
            for (const auto &f : flags) {
                auto check = this->bits.find(f);
                if (check == this->bits.end())
                    continue;
                bits |= check->second;
            }
        } else {
            for (const auto &n : selection.knownInputs()) {
                Util::ReferenceCopy<SequenceName> effective(n);
                if (!effective().isMeta())
                    effective->setMeta();
                for (const auto &f : flags) {
                    qint64 add = segment.getValue(effective())
                                        .metadataSingleFlag(f)
                                        .hash("Bits")
                                        .toInt64();
                    if (INTEGER::defined(add))
                        bits |= add;
                }
            }
        }
    }

    if (overrideFormat)
        return format.apply(bits, pad);

    NumberFormat fmt;
    for (const auto &n : selection.knownInputs()) {
        Util::ReferenceCopy<SequenceName> effective(n);
        if (!effective().isMeta())
            effective->setMeta();
        QString check(segment.getValue(effective()).metadata("Format").toQString());
        if (!check.isEmpty()) {
            fmt = NumberFormat(check);
            break;
        }

        check = segment.getValue(effective()).metadata("MVC").toQString();
        if (!check.isEmpty())
            fmt = NumberFormat(check);
    }
    return fmt.apply(bits, pad);
}

ControlExport::ExportColumn *ControlExport::ExportFlagsBits::clone() const
{ return new ExportFlagsBits(*this); }


Q_LOGGING_CATEGORY(log_control_export_script, "cpd3.control.export.script", QtWarningMsg)

void ControlExport::ExportScript::initialize()
{
    {
        Lua::Engine::Assign assign(root, root.global(), "print");
        assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                qCInfo(log_control_export_script) << entry[i].toOutputString();
            }
        }));
    }

    if (!root.pushChunk(code, false)) {
        engine.clearError();
        return;
    }
    invoke = root.back();
}

ControlExport::ExportScript::ExportScript(const Variant::Read &config) : ExportColumn(config),
                                                                         code(config["Code"].toString()),
                                                                         engine(),
                                                                         root(engine),
                                                                         selection(config["Input"])
{ initialize(); }

ControlExport::ExportScript::ExportScript(const ExportScript &other) : ExportColumn(other),
                                                                       code(other.code),
                                                                       engine(),
                                                                       root(engine),
                                                                       selection(other.selection)
{ initialize(); }

ControlExport::ExportScript::~ExportScript() = default;

SequenceMatch::Composite ControlExport::ExportScript::getSelection() const
{ return selection; }

bool ControlExport::ExportScript::registerInput(const SequenceName &unit)
{
    if (!unit.isMeta())
        return selection.matches(unit);
    return selection.matches(unit.fromMeta());
}

QString ControlExport::ExportScript::getValue(SequenceSegment &segment)
{
    if (!invoke.isAssigned())
        return {};
    {
        Lua::Engine::Assign assign(root, engine.global(), "data");
        assign.pushData<Lua::Libs::SequenceSegment>(segment);
    }

    Lua::Engine::Call call(root, invoke);
    if (!call.execute(1)) {
        engine.clearError();
        return {};
    }

    return call.back().toQString();
}

ControlExport::ExportColumn *ControlExport::ExportScript::clone() const
{ return new ExportScript(*this); }

static std::string assembleLoggingCategory(const std::string &loggingContext)
{
    std::string result = "cpd3.acquisition.control.export";
    if (!loggingContext.empty()) {
        result += '.';
        result += loggingContext;
    }
    return result;
}

ControlExport::ControlExport(const ValueSegment::Transfer &configData,
                             const std::string &loggingContext) : loggingName(
        assembleLoggingCategory(loggingContext)),
                                                                  log(loggingName.data()),
                                                                  terminated(false),
                                                                  controlStream(nullptr),
                                                                  state(nullptr),
                                                                  enabled(true),
                                                                  loadBacklog(false),
                                                                  realtimeIngress(this),
                                                                  filterIngress(this)
{
    instrumentMeta["InstrumentCode"].setString("controlexport");

    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
    auto hit = Range::findIntersecting(configData, Time::time());
    if (hit != configData.end()) {
        if (hit->value().hash("EnableExport").exists())
            enabled = hit->value().hash("EnableExport").toBool();
        loadBacklog = hit->value().hash("LoadArchived").toBool();
    }

    auto defaultStations = SequenceName::impliedStations();
    for (auto &s : config) {
        for (auto &c : s.columns) {
            c.column->initialize(defaultStations);
        }
    }

    bufferDuration = DynamicTimeInterval::fromConfiguration(configData, "Duration");

    Range::intersectShift(config, Time::time());

    double beginTime = bufferDuration->applyConst(Time::time(), Time::time(), false, -1);

    updatePending = true;
    openSegment = false;
    chain = new ProcessingTapChain;
    chain->setBackend(&realtimeIngress);
    chain->setDefaultSelection(beginTime, FP::undefined(),
                               Archive::Selection::Match{defaultStations.begin(),
                                                         defaultStations.end()},
                               {"raw", "raw_meta"});

    Variant::Read processing = Variant::Read::empty();
    if (hit != configData.end()) {
        processing = hit->value().hash("Processing");
    }

    SequenceMatch::Composite selection;
    for (const auto &s : config) {
        for (const auto &c : s.columns) {
            selection.append(c.column->getSelection());
        }
    }

    chain->add(&filterIngress, processing, selection);
    chain->start();
}

ControlExport::~ControlExport()
{
    realtimeIngress.finish();
    chain->signalTerminate();
    chain->wait();
    delete chain;

    delete bufferDuration;
}


ControlExport::Configuration::Configuration() : start(FP::undefined()),
                                                end(FP::undefined()),
                                                columns(),

                                                outputHeader(true),
                                                separator(','),
                                                quote('\"'),
                                                quoteEscape("\"\"\""),
                                                linePrefix(),
                                                lineSuffix("\n"),
                                                fileTarget(),
                                                append(false),
                                                asBinary(false),
                                                noFile(false),
                                                command()
{ }

ControlExport::Configuration::~Configuration()
{
    for (QList<ColumnData>::const_iterator c = columns.constBegin(), endC = columns.constEnd();
            c != endC;
            ++c) {
        delete c->column;
    }
}

ControlExport::Configuration::Configuration(const Configuration &other) : start(other.start),
                                                                          end(other.end),
                                                                          columns(),
                                                                          outputHeader(
                                                                                  other.outputHeader),
                                                                          separator(
                                                                                  other.separator),
                                                                          quote(other.quote),
                                                                          quoteEscape(
                                                                                  other.quoteEscape),
                                                                          linePrefix(
                                                                                  other.linePrefix),
                                                                          lineSuffix(
                                                                                  other.lineSuffix),
                                                                          fileTarget(
                                                                                  other.fileTarget),
                                                                          append(other.append),
                                                                          asBinary(other.asBinary),
                                                                          noFile(other.noFile),
                                                                          command(other.command)
{
    for (QList<ColumnData>::const_iterator c = other.columns.constBegin(),
            endC = other.columns.constEnd(); c != endC; ++c) {
        ColumnData add;
        add.header = c->header;
        add.column = c->column->clone();
        columns.append(add);
    }
}

ControlExport::Configuration &ControlExport::Configuration::operator=(const Configuration &other)
{
    if (&other == this)
        return *this;

    start = other.start;
    end = other.end;
    outputHeader = other.outputHeader;
    separator = other.separator;
    quote = other.quote;
    quoteEscape = other.quoteEscape;
    linePrefix = other.linePrefix;
    lineSuffix = other.lineSuffix;
    fileTarget = other.fileTarget;
    append = other.append;
    asBinary = other.asBinary;
    noFile = other.noFile;
    command = other.command;

    for (QList<ColumnData>::const_iterator c = columns.constBegin(), endC = columns.constEnd();
            c != endC;
            ++c) {
        delete c->column;
    }
    columns.clear();
    for (QList<ColumnData>::const_iterator c = other.columns.constBegin(),
            endC = other.columns.constEnd(); c != endC; ++c) {
        ColumnData add;
        add.header = c->header;
        add.column = c->column->clone();
        columns.append(add);
    }

    return *this;
}

ControlExport::Configuration::Configuration(const Configuration &other, double s, double e) : start(
        s),
                                                                                              end(e),
                                                                                              columns(),
                                                                                              outputHeader(
                                                                                                      other.outputHeader),
                                                                                              separator(
                                                                                                      other.separator),
                                                                                              quote(other.quote),
                                                                                              quoteEscape(
                                                                                                      other.quoteEscape),
                                                                                              linePrefix(
                                                                                                      other.linePrefix),
                                                                                              lineSuffix(
                                                                                                      other.lineSuffix),
                                                                                              fileTarget(
                                                                                                      other.fileTarget),
                                                                                              append(other.append),
                                                                                              asBinary(
                                                                                                      other.asBinary),
                                                                                              noFile(other.noFile),
                                                                                              command(other.command)
{
    for (QList<ColumnData>::const_iterator c = other.columns.constBegin(),
            endC = other.columns.constEnd(); c != endC; ++c) {
        ColumnData add;
        add.header = c->header;
        add.column = c->column->clone();
        columns.append(add);
    }
}

ControlExport::Configuration::Configuration(const ValueSegment &other, double s, double e) : start(
        s),
                                                                                             end(e),
                                                                                             columns(),
                                                                                             outputHeader(
                                                                                                     true),
                                                                                             separator(
                                                                                                     ','),
                                                                                             quote('\"'),
                                                                                             quoteEscape(
                                                                                                     "\"\"\""),
                                                                                             linePrefix(),
                                                                                             lineSuffix(
                                                                                                     "\n"),
                                                                                             fileTarget(),
                                                                                             append(false),
                                                                                             asBinary(
                                                                                                     false),
                                                                                             noFile(false),
                                                                                             command()
{
    setFromSegment(other);
}

ControlExport::Configuration::Configuration(const Configuration &under,
                                            const ValueSegment &over,
                                            double s,
                                            double e) : start(s),
                                                        end(e),
                                                        columns(),
                                                        outputHeader(under.outputHeader),
                                                        separator(under.separator),
                                                        quote(under.quote),
                                                        quoteEscape(under.quoteEscape),
                                                        linePrefix(under.linePrefix),
                                                        lineSuffix(under.lineSuffix),
                                                        fileTarget(under.fileTarget),
                                                        append(under.append),
                                                        asBinary(under.asBinary),
                                                        noFile(under.noFile),
                                                        command(under.command)
{
    for (QList<ColumnData>::const_iterator c = under.columns.constBegin(),
            endC = under.columns.constEnd(); c != endC; ++c) {
        ColumnData add;
        add.header = c->header;
        add.column = c->column->clone();
        columns.append(add);
    }
    setFromSegment(over);
}

void ControlExport::Configuration::parseColumn(const Variant::Read &config,
                                               const QString &defaultName)
{
    ColumnData data;

    const auto &type = config["Type"].toString();
    if (Util::equal_insensitive(type, "time", "datetime", "date")) {
        data.column = new ExportDateTime(config);
    } else if (Util::equal_insensitive(type, "flags", "bits", "flagsbits", "bitflags")) {
        data.column = new ExportFlagsBits(config);
    } else if (Util::equal_insensitive(type, "flagsname", "flagsstring", "stringflags")) {
        data.column = new ExportFlagsString(config);
    } else if (Util::equal_insensitive(type, "script")) {
        data.column = new ExportScript(config);
    } else {
        data.column = new ExportSingleNumber(config);
    }

    data.header = defaultName;
    if (data.header.isEmpty())
        data.header = config["Input/Variable"].toQString();
    if (config["Header"].exists())
        data.header = config["Header"].toQString();

    columns.append(data);
}

void ControlExport::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["Header"].exists())
        outputHeader = config["Header"].toBool();

    if (config["Join/Separator"].exists())
        separator = config["Join/Separator"].toQString();
    if (config["Join/Quote"].exists())
        quote = config["Join/Quote"].toQString();
    if (config["Join/QuoteEscape"].exists())
        quoteEscape = config["Join/QuoteEscape"].toQString();

    if (config["Line/Prefix"].exists())
        linePrefix = config["Line/Prefix"].toQString();
    if (config["Line/Suffix"].exists())
        lineSuffix = config["Line/Suffix"].toQString();

    if (config["File"].exists())
        fileTarget = config["File"].toQString();

    if (config["Append"].exists())
        append = config["Append"].toBool();
    if (config["BinaryMode"].exists())
        asBinary = config["BinaryMode"].toBool();
    if (config["NoFile"].exists())
        noFile = config["NoFile"].toBool();

    if (config["Command"].exists())
        command = config["Command"].toQString();


    if (config["Columns"].exists()) {
        for (QList<ColumnData>::const_iterator c = columns.constBegin(), endC = columns.constEnd();
                c != endC;
                ++c) {
            delete c->column;
        }
        columns.clear();

        auto children = config["Columns"].toChildren();
        for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
            parseColumn(add.value(), QString::fromStdString(add.stringKey()));
        }
    }
}

QString ControlExport::Configuration::join(const QStringList &fields) const
{ return linePrefix + CSV::join(fields, separator, quote, quoteEscape) + lineSuffix; }

void ControlExport::executeExport()
{
    TextSubstitutionStack substitutions;
    substitutions.setTime("time", Time::time());
    if (buffer.empty()) {
        substitutions.setTime("start", buffer.front().getStart());
        substitutions.setTime("end", buffer.back().getEnd());
        substitutions.setTime("laststart", buffer.back().getStart());
    }

    std::shared_ptr<IO::File::Backing> file;
    std::unique_ptr<IO::Generic::Block> writeTarget;
    if (!config.first().noFile) {
        IO::File::Mode openMode = IO::File::Mode::writeOnly();
        openMode.create = true;
        openMode.append = config.front().append;
        openMode.text = !config.front().asBinary;

        QString fileName(substitutions.apply(config.first().fileTarget));
        if (!fileName.isEmpty()) {
            file = IO::Access::file(fileName, openMode);
        } else {
            file = IO::Access::temporaryFile(false, openMode.text);
        }

        if (!file) {
            qCWarning(log) << "Failed to open" << fileName;
            return;
        }
        writeTarget = file->block();
        if (!file) {
            qCWarning(log) << "Failed to write to" << fileName;
            return;
        }
    }

    if (config.first().outputHeader) {
        QStringList fields;
        for (QList<Configuration::ColumnData>::const_iterator
                c = config.first().columns.constBegin(), endC = config.first().columns.constEnd();
                c != endC;
                ++c) {
            fields.append(c->header);
        }
        QString line(config.first().join(fields));
        if (writeTarget)
            writeTarget->write(line);
        if (controlStream)
            controlStream->writeControl(line.toUtf8());
    }
    for (auto &t : buffer) {
        QStringList fields;
        for (QList<Configuration::ColumnData>::const_iterator
                c = config.first().columns.constBegin(), endC = config.first().columns.constEnd();
                c != endC;
                ++c) {
            fields.append(c->column->getValue(t));
        }
        QString line(config.first().join(fields));
        if (writeTarget)
            writeTarget->write(line);
        if (controlStream)
            controlStream->writeControl(line.toUtf8());
    }

    if (!writeTarget)
        return;
    writeTarget.reset();

    substitutions.setFile("file", QString::fromStdString(file->filename()));
    auto command = substitutions.apply(config.first().command);
    if (command.isEmpty())
        return;

    auto proc = IO::Process::Spawn(command).forward().create();
    if (!proc)
        return;

    proc->exited.connect(receiver, [this, file, command](int exitCode, bool normalExit) {
        if (!normalExit) {
            qWarning(log) << "Command" << command << "exited abnormally";
        } else if (exitCode != 0) {
            qWarning(log) << "Command" << command << "exited with code:" << exitCode;
        } else {
            qCDebug(log) << "Command completed";
        }
    });

    if (!proc->start()) {
        qWarning(log) << "Failed to start command" << command;
        return;
    }

    qCDebug(log) << "Running command on file of size" << file->size() << "byte(s)";

    std::thread([proc, file] {
        proc->wait();
    }).detach();
}

void ControlExport::advanceTime(double executeTime)
{
    if (!FP::defined(executeTime))
        return;

    std::lock_guard<std::mutex> lock(filterMutex);

    int oldSize = config.size();
    if (!Range::intersectShift(config, executeTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    if (config.isEmpty())
        return;
    if (oldSize != config.size()) {
        filterIngress.reset();
    }

    if (!updatePending)
        return;
    updatePending = false;

    bool valid = false;
    SequenceSegment intermediate(reader.intermediateNext(&valid));
    if (!valid) {
        if (openSegment) {
            Q_ASSERT(!buffer.empty());
            buffer.pop_back();
            openSegment = false;
        }
    } else {
        if (!openSegment) {
            buffer.emplace_back(std::move(intermediate));
            openSegment = true;
        } else {
            Q_ASSERT(!buffer.empty());
            Q_ASSERT(FP::equal(buffer.back().getStart(), intermediate.getStart()));
            buffer.back() = std::move(intermediate);
        }
    }

    if (buffer.empty())
        return;

    double purgeTime = bufferDuration->apply(executeTime, buffer.back().getStart(), false, -1);
    if (!FP::defined(purgeTime)) {
        buffer.erase(buffer.begin(), buffer.end() - 1);
    } else {
        while (buffer.size() > 1 &&
                Range::compareStart(buffer.front().getStart(), purgeTime) <= 0) {
            buffer.pop_front();
        }
    }

    executeExport();
}

void ControlExport::setControlStream(AcquisitionControlStream *controlStream)
{
    this->controlStream = controlStream;
}

void ControlExport::setState(AcquisitionState *state)
{
    std::lock_guard<std::mutex> lock(filterMutex);
    this->state = state;
}

void ControlExport::setRealtimeEgress(StreamSink *egress)
{
    if (egress != NULL)
        egress->endData();
}

void ControlExport::setLoggingEgress(StreamSink *egress)
{
    if (egress != NULL)
        egress->endData();
}

Variant::Root ControlExport::getSourceMetadata()
{ return instrumentMeta; }

AcquisitionInterface::GeneralStatus ControlExport::getGeneralStatus()
{
    if (!enabled)
        return AcquisitionInterface::GeneralStatus::Disabled;
    return AcquisitionInterface::GeneralStatus::Normal;
}

void ControlExport::incomingData(const Util::ByteView &data,
                                 double time,
                                 AcquisitionInterface::IncomingDataType type)
{
    Q_UNUSED(data);
    Q_UNUSED(type);
    advanceTime(time);
    if (state != NULL)
        state->realtimeAdvanced(time);
}

void ControlExport::incomingControl(const Util::ByteView &data,
                                    double time,
                                    AcquisitionInterface::IncomingDataType type)
{
    Q_UNUSED(data);
    Q_UNUSED(type);
    advanceTime(time);
    if (state != NULL)
        state->realtimeAdvanced(time);
}

void ControlExport::incomingTimeout(double time)
{
    advanceTime(time);
    if (state != NULL)
        state->realtimeAdvanced(time);
}

void ControlExport::incomingAdvance(double time)
{
    advanceTime(time);
    if (state != NULL)
        state->realtimeAdvanced(time);
}

StreamSink *ControlExport::getRealtimeIngress()
{ return &realtimeIngress; }

ControlExport::RealtimeIngress::RealtimeIngress(ControlExport *ce) : control(ce),
                                                                     realtimeMutex(),
                                                                     isFinished(false)
{ }

ControlExport::RealtimeIngress::~RealtimeIngress() = default;

const std::vector<
        ControlExport::RealtimeIngress::TargetData *> &ControlExport::RealtimeIngress::lookup(const CPD3::Data::SequenceName &name)
{
    auto d = dispatch.find(name);
    if (d != dispatch.end())
        return d->second;

    std::vector<TargetData *> destination;
    for (const auto &check : targets) {
        if (!check->match->matches(name))
            continue;
        destination.emplace_back(check.get());
    }
    return dispatch.emplace(name, std::move(destination)).first->second;
}

void ControlExport::RealtimeIngress::incomingData(const SequenceValue::Transfer &values)
{
    std::lock_guard<std::mutex> lock(realtimeMutex);
    for (const auto &v : values) {
        for (const auto &t : lookup(v.getName())) {
            if (Range::compareStart(v.getStart(), t->discardBefore) < 0)
                continue;
            t->discardBefore = v.getStart();
            t->sink->incomingData(v);
        }
    }
}

void ControlExport::RealtimeIngress::incomingData(SequenceValue::Transfer &&values)
{
    std::lock_guard<std::mutex> lock(realtimeMutex);
    for (auto &v : values) {
        const auto &targets = lookup(v.getName());
        if (targets.empty())
            continue;
        auto t = targets.begin();
        for (auto end = targets.end() - 1; t != end; ++t) {
            if (Range::compareStart(v.getStart(), (*t)->discardBefore) < 0)
                continue;
            (*t)->discardBefore = v.getStart();
            (*t)->sink->incomingData(v);
        }
        if (Range::compareStart(v.getStart(), (*t)->discardBefore) < 0)
            continue;
        (*t)->discardBefore = v.getStart();
        (*t)->sink->incomingData(std::move(v));
    }
}

void ControlExport::RealtimeIngress::incomingData(const SequenceValue &v)
{
    std::lock_guard<std::mutex> lock(realtimeMutex);
    for (const auto &t : lookup(v.getName())) {
        if (Range::compareStart(v.getStart(), t->discardBefore) < 0)
            continue;
        t->discardBefore = v.getStart();
        t->sink->incomingData(v);
    }
}

void ControlExport::RealtimeIngress::incomingData(SequenceValue &&v)
{
    std::lock_guard<std::mutex> lock(realtimeMutex);
    const auto &targets = lookup(v.getName());
    if (targets.empty())
        return;
    auto t = targets.begin();
    for (auto end = targets.end() - 1; t != end; ++t) {
        if (Range::compareStart(v.getStart(), (*t)->discardBefore) < 0)
            continue;
        (*t)->discardBefore = v.getStart();
        (*t)->sink->incomingData(v);
    }
    if (Range::compareStart(v.getStart(), (*t)->discardBefore) < 0)
        return;
    (*t)->discardBefore = v.getStart();
    (*t)->sink->incomingData(std::move(v));
}

void ControlExport::RealtimeIngress::endData()
{ }

void ControlExport::RealtimeIngress::finish()
{
    std::lock_guard<std::mutex> lock(realtimeMutex);
    for (const auto &t : targets) {
        t->sink->endData();
    }
}

void ControlExport::RealtimeIngress::issueArchiveRead(const Archive::Selection::List &selections,
                                                      StreamSink *target)
{
    std::lock_guard<std::mutex> lock(realtimeMutex);
    if (isFinished) {
        target->endData();
        return;
    }

    targets.emplace_back(new TargetData(selections, target));
    dispatch.clear();

    if (!control->loadBacklog)
        return;

    auto values = Archive::Access().readSynchronous(selections);
    if (values.empty())
        return;
    targets.back()->discardBefore = values.back().getStart();
    target->incomingData(std::move(values));
}

ControlExport::RealtimeIngress::TargetData::TargetData(const Archive::Selection::List &selections,
                                                       StreamSink *target) : match(
        SequenceMatch::Basic::compile(selections)), sink(target), discardBefore(FP::undefined())
{ }


ControlExport::FilterIngress::FilterIngress(ControlExport *ce) : control(ce)
{ }

ControlExport::FilterIngress::~FilterIngress() = default;

bool ControlExport::FilterIngress::lookup(const CPD3::Data::SequenceName &name)
{
    auto check = dispatch.find(name);
    if (check != dispatch.end())
        return check->second;

    bool accept = false;

    for (const auto &c : control->config.first().columns) {
        if (c.column->registerInput(name))
            accept = true;
    }

    dispatch.emplace(name, accept);
    return accept;
}

void ControlExport::FilterIngress::incomingData(const SequenceValue::Transfer &values)
{
    bool updated = false;

    std::lock_guard<std::mutex> lock(control->filterMutex);
    for (const auto &v : values) {
        if (!lookup(v.getName()))
            continue;

        auto add = control->reader.add(v);
        if (add.empty())
            continue;

        if (control->openSegment) {
            Q_ASSERT(!control->buffer.empty());
            Q_ASSERT(FP::equal(add.front().getStart(), control->buffer.back().getStart()));
            control->buffer.pop_back();
            control->openSegment = false;
        }

        Util::append(std::move(add), control->buffer);
        control->updatePending = true;
        updated = true;
    }

    if (!updated)
        return;
    if (control->state)
        control->state->requestDataWakeup(0.0);
}

void ControlExport::FilterIngress::incomingData(SequenceValue::Transfer &&values)
{
    bool updated = false;

    std::lock_guard<std::mutex> lock(control->filterMutex);
    for (auto &v : values) {
        if (!lookup(v.getName()))
            continue;

        auto add = control->reader.add(std::move(v));
        if (add.empty())
            continue;

        if (control->openSegment) {
            Q_ASSERT(!control->buffer.empty());
            Q_ASSERT(FP::equal(add.front().getStart(), control->buffer.back().getStart()));
            control->buffer.pop_back();
            control->openSegment = false;
        }

        Util::append(std::move(add), control->buffer);
        control->updatePending = true;
        updated = true;
    }

    if (!updated)
        return;
    if (control->state)
        control->state->requestDataWakeup(0.0);
}

void ControlExport::FilterIngress::incomingData(const SequenceValue &v)
{
    std::lock_guard<std::mutex> lock(control->filterMutex);
    if (!lookup(v.getName()))
        return;

    auto add = control->reader.add(v);
    if (add.empty())
        return;

    if (control->openSegment) {
        Q_ASSERT(!control->buffer.empty());
        Q_ASSERT(FP::equal(add.front().getStart(), control->buffer.back().getStart()));
        control->buffer.pop_back();
        control->openSegment = false;
    }

    Util::append(std::move(add), control->buffer);
    control->updatePending = true;

    if (control->state)
        control->state->requestDataWakeup(0.0);
}

void ControlExport::FilterIngress::incomingData(SequenceValue &&v)
{
    std::lock_guard<std::mutex> lock(control->filterMutex);
    if (!lookup(v.getName()))
        return;

    auto add = control->reader.add(std::move(v));
    if (add.empty())
        return;

    if (control->openSegment) {
        Q_ASSERT(!control->buffer.empty());
        Q_ASSERT(FP::equal(add.front().getStart(), control->buffer.back().getStart()));
        control->buffer.pop_back();
        control->openSegment = false;
    }

    Util::append(std::move(add), control->buffer);
    control->updatePending = true;

    if (control->state)
        control->state->requestDataWakeup(0.0);
}

void ControlExport::FilterIngress::endData()
{ }

void ControlExport::FilterIngress::reset()
{
    std::lock_guard<std::mutex> lock(mutex);
    dispatch.clear();
}

AcquisitionInterface::AutomaticDefaults ControlExport::getDefaults()
{
    AutomaticDefaults result;
    result.name = "EXPORT$2";
    return result;
}

bool ControlExport::isCompleted()
{ return terminated; }

void ControlExport::signalTerminate()
{
    terminated = true;
    completed();
}


std::unique_ptr<
        AcquisitionInterface> ControlExportComponent::createAcquisitionPassive(const ComponentOptions &,
                                                                               const std::string &loggingContext)
{
    qWarning() << "Option based creation is not supported";
    return std::unique_ptr<AcquisitionInterface>(
            new ControlExport(ValueSegment::Transfer(), loggingContext));
}

std::unique_ptr<
        AcquisitionInterface> ControlExportComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                               const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(new ControlExport(config, loggingContext));
}

std::unique_ptr<
        AcquisitionInterface> ControlExportComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                 const std::string &loggingContext)
{ return {}; }