/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>
#include <QTemporaryFile>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/timeutils.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"
#include "datacore/archive/access.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class TextControlStream : public AcquisitionControlStream {
public:
    QByteArray control;

    TextControlStream()
    { }

    virtual ~TextControlStream()
    { }

    virtual void writeControl(const Util::ByteView &data)
    {
        control += data.toQByteArray();
    }

    virtual void resetControl()
    { }

    virtual void requestTimeout(double time)
    {
        Q_UNUSED(time);
    }
};

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    AcquisitionComponent *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<AcquisitionComponent *>(ComponentLoader::create("control_export"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void basic()
    {
        QTemporaryFile target;
        QVERIFY(target.open());

        Variant::Root config;

        config["Header"] = true;
        config["File"] = target.fileName();
        config["BinaryMode"] = true;
        config["Duration/Unit"] = "Minute";
        config["Duration/Count"] = 60;

        config["Columns/#0/Type"] = "Time";
        config["Columns/#0/Header"] = "DateTime";

        config["Columns/#1/Type"] = "Variable";
        config["Columns/#1/Header"] = "Green";
        config["Columns/#1/Input/Variable"] = "BsG_S11";
        config["Columns/#1/Format"] = "00.0";
        config["Columns/#1/MVC"] = "99";

        config["Columns/#2/Type"] = "Variable";
        config["Columns/#2/Header"] = "Blue";
        config["Columns/#2/Input/Variable"] = "BsB_S11";

        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());

        /* Make sure the chain is started before we start adding data */
        QTest::qSleep(250);

        interface->incomingAdvance(0.0);
        QTest::qSleep(100);

        Variant::Root meta;
        meta.write().metadataReal("Format") = "0.0";
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "raw_meta", "BsB_S11"), meta, 0.0,
                                              1388534520));

        interface->getRealtimeIngress()
                 ->incomingData(
                         SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(1.0),
                                       1388534400,
                                       1388534460));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"),
                                              Variant::Root(FP::undefined()),
                                              1388534400, 1388534460));
        interface->getRealtimeIngress()
                 ->incomingData(
                         SequenceValue(SequenceName("brw", "raw", "BsR_S11"), Variant::Root(0.0),
                                       1388534400,
                                       1388534460));

        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"),
                                              Variant::Root(FP::undefined()),
                                              1388534460, 1388534520));
        interface->getRealtimeIngress()
                 ->incomingData(
                         SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(2.0),
                                       1388534460,
                                       1388534520));

        interface->incomingAdvance(1388534530.0);

        QString check(QString::fromUtf8(target.readAll()));
        QCOMPARE(check, QString(
                "DateTime,Green,Blue\n"
                        "2014-01-01T00:00:00Z,01.0,9.9\n"
                        "2014-01-01T00:01:00Z,99,2.0\n"));
    }

    void flags()
    {
        QTemporaryFile target;
        QVERIFY(target.open());

        Variant::Root config;

        config["Header"] = true;
        config["File"] = target.fileName();
        config["BinaryMode"] = true;
        config["Duration/Unit"] = "Minute";
        config["Duration/Count"] = 60;

        config["Columns/#0/Type"] = "Time";
        config["Columns/#0/Header"] = "DateTime";

        config["Columns/#1/Type"] = "Flags";
        config["Columns/#1/Header"] = "Bits";
        config["Columns/#1/Input/Variable"] = "F1_S11";

        config["Columns/#2/Type"] = "FlagsString";
        config["Columns/#2/Header"] = "String";
        config["Columns/#2/Input/Variable"] = "F1_S11";

        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());

        /* Make sure the chain is started before we start adding data */
        QTest::qSleep(250);

        interface->incomingAdvance(0.0);
        QTest::qSleep(100);

        Variant::Root meta;
        meta.write().metadataFlags("Format").setString("FF");
        meta.write().metadataSingleFlag("Flag1").hash("Bits").setInt64(0x01);
        meta.write().metadataSingleFlag("Flag2").hash("Bits").setInt64(0x10);
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "raw_meta", "F1_S11"), meta, 0.0,
                                              1388534520));

        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "raw", "F1_S11"),
                                              Variant::Root(Variant::Flags{"Flag1", "Flag2"}),
                                              1388534400,
                                              1388534460));

        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "raw", "F1_S11"), Variant::Root(),
                                              1388534460,
                                              1388534520));

        interface->incomingAdvance(1388534530.0);

        QString check(QString::fromUtf8(target.readAll()));
        QCOMPARE(check, QString(
                "DateTime,Bits,String\n"
                        "2014-01-01T00:00:00Z,11,Flag1;Flag2\n"
                        "2014-01-01T00:01:00Z,FF,\n"));
    }

#ifdef Q_OS_UNIX

    void command()
    {
        QTemporaryFile target;
        QVERIFY(target.open());

        Variant::Root config;

        config["Header"] = true;
        config["BinaryMode"] = true;
        config["Duration/Unit"] = "Minute";
        config["Duration/Count"] = 60;
        config["Command"] = QString("cp ${FILE} %1").arg(target.fileName());

        config["Columns/#0/Type"] = "Time";
        config["Columns/#0/Header"] = "DateTime";

        config["Columns/#1/Type"] = "Variable";
        config["Columns/#1/Header"] = "Green";
        config["Columns/#1/Input/Variable"] = "BsG_S11";
        config["Columns/#1/Format"] = "00.0";
        config["Columns/#1/MVC"] = "99";

        config["Columns/#2/Type"] = "Variable";
        config["Columns/#2/Header"] = "Blue";
        config["Columns/#2/Input/Variable"] = "BsB_S11";

        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());

        /* Make sure the chain is started before we start adding data */
        QTest::qSleep(250);

        interface->incomingAdvance(0.0);
        QTest::qSleep(100);

        Variant::Root meta;
        meta.write().metadataReal("Format") = "0.0";
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "raw_meta", "BsB_S11"), meta, 0.0,
                                              1388534520));

        interface->getRealtimeIngress()
                 ->incomingData(
                         SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(1.0),
                                       1388534400,
                                       1388534460));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"),
                                              Variant::Root(FP::undefined()),
                                              1388534400, 1388534460));
        interface->getRealtimeIngress()
                 ->incomingData(
                         SequenceValue(SequenceName("brw", "raw", "BsR_S11"), Variant::Root(0.0),
                                       1388534400,
                                       1388534460));

        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"),
                                              Variant::Root(FP::undefined()),
                                              1388534460, 1388534520));
        interface->getRealtimeIngress()
                 ->incomingData(
                         SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(2.0),
                                       1388534460,
                                       1388534520));

        interface->incomingAdvance(1388534530.0);

        interface.reset();

        ElapsedTimer st;
        st.start();
        QString check;
        while (st.elapsed() < 30000) {
            target.seek(0);
            check = QString::fromUtf8(target.readAll());
            if (!check.isEmpty())
                break;

            QTest::qSleep(50);
            continue;
        }

        QCOMPARE(check, QString(
                "DateTime,Green,Blue\n"
                        "2014-01-01T00:00:00Z,01.0,9.9\n"
                        "2014-01-01T00:01:00Z,99,2.0\n"));
    }

#endif

    void scriptBacklog()
    {
        QTemporaryFile target;
        QVERIFY(target.open());

        Variant::Root config;

        config["File"] = target.fileName();
        config["Header"] = false;
        config["BinaryMode"] = true;
        config["LoadArchived"] = true;
        config["Duration/Unit"] = "Minute";
        config["Duration/Count"] = 60;
        config["Processing/Components/#0/Name"] = "corr_stp";
        config["Processing/Additional/#0/Variable"] = "(T|P)_S11";

        config["Columns/#0/Type"] = "Time";

        config["Columns/#1/Type"] = "Script";
        config["Columns/#1/Input/Variable"] = "BsG_S11";
        config["Columns/#1/Code"] = "return string.format('%.2f', data.BsG_S11);";

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "T_S11"}, Variant::Root(40.0), FP::undefined(),
                              FP::undefined()),
                SequenceValue({"brw", "raw", "P_S11"}, Variant::Root(1013.25), FP::undefined(),
                              FP::undefined())});

        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());

        /* Make sure the chain is started before we start adding data */
        QTest::qSleep(250);

        interface->incomingAdvance(0.0);
        /* Need to wait for the archive read to be completed, but no signal for it, so
         * just wait a while */
        QTest::qSleep(2000);

        interface->getRealtimeIngress()
                 ->incomingData(
                         SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(1.0),
                                       1388534400,
                                       1388534460));

        interface->getRealtimeIngress()
                 ->incomingData(
                         SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(2.0),
                                       1388534460,
                                       1388534520));

        interface->incomingAdvance(1388534520.0);
        double beginTime = 1388534521.0;

        ElapsedTimer st;
        st.start();
        QString check;
        while (st.elapsed() < 10000) {
            target.seek(0);
            check = QString::fromUtf8(target.readAll());
            if (!check.isEmpty() && check.contains("2014-01-01T00:01:00Z"))
                break;

            interface->getRealtimeIngress()
                     ->incomingData(
                             SequenceValue(SequenceName("brw", "raw", "T_S11"), Variant::Root(0.0),
                                           beginTime,
                                           beginTime + 1.0));
            beginTime += 1.0;
            interface->incomingAdvance(beginTime);

            QTest::qSleep(50);
        }

        QVERIFY(check.startsWith("2014-01-01T00:00:00Z,1.15\n"
                                         "2014-01-01T00:01:00Z,2.29\n"));
    }

    void controlStream()
    {
        TextControlStream output;

        Variant::Root config;

        config["Header"] = false;
        config["BinaryMode"] = true;
        config["NoFile"] = true;
        config["Line/Prefix"] = "\x02";
        config["Line/Suffix"] = "\n\x03";

        config["Columns/#0/Type"] = "Time";
        config["Columns/#0/Header"] = "DateTime";

        config["Columns/#1/Type"] = "Variable";
        config["Columns/#1/Header"] = "Green";
        config["Columns/#1/Input/Variable"] = "BsG_S11";
        config["Columns/#1/Format"] = "00.0";
        config["Columns/#1/MVC"] = "99";

        config["Columns/#2/Type"] = "Variable";
        config["Columns/#2/Header"] = "Blue";
        config["Columns/#2/Input/Variable"] = "BsB_S11";

        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());

        /* Make sure the chain is started before we start adding data */
        QTest::qSleep(250);

        interface->setControlStream(&output);
        interface->incomingAdvance(0.0);
        QTest::qSleep(100);

        Variant::Root meta;
        meta.write().metadataReal("Format") = "0.0";
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "raw_meta", "BsB_S11"), meta, 0.0,
                                              1388534520));

        interface->getRealtimeIngress()
                 ->incomingData(
                         SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(1.0),
                                       1388534400,
                                       1388534460));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"),
                                              Variant::Root(FP::undefined()),
                                              1388534400, 1388534460));
        interface->getRealtimeIngress()
                 ->incomingData(
                         SequenceValue(SequenceName("brw", "raw", "BsR_S11"), Variant::Root(0.0),
                                       1388534400,
                                       1388534460));

        interface->incomingAdvance(1388534460.0);

        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"),
                                              Variant::Root(FP::undefined()),
                                              1388534460, 1388534520));
        interface->getRealtimeIngress()
                 ->incomingData(
                         SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(2.0),
                                       1388534460,
                                       1388534520));

        interface->incomingAdvance(1388534530.0);

        QString check(QString::fromUtf8(output.control));
        QCOMPARE(check, QString::fromLatin1("\x02" "2014-01-01T00:00:00Z,01.0,9.9\n\x03"
                                                    "\x02" "2014-01-01T00:01:00Z,99,2.0\n\x03"));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
