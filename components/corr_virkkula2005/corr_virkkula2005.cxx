/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QRegExp>

#include "core/timeutils.hxx"
#include "core/environment.hxx"

#include "corr_virkkula2005.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Editing;
using namespace CPD3::Smoothing;

namespace {
struct DefaultParameters {
    double wavelength;
    double k0;
    double k1;
    double h0;
    double h1;
    double s;
};
static const DefaultParameters defaultParameters[] = {
        /* WL   k0          k1          h0          h1          s */
        {467, 0.377, -0.640, 1.16,  -0.63,  0.015},
        {530, 0.358, -0.640, 1.17,  -0.71,  0.017},
        {574, 0.354, -0.617, 1.192, -0.800, 0.023},
        {660, 0.352, -0.674, 1.14,  -0.72,  0.022},};

static const DefaultParameters *getDefaults(double wavelength)
{
    size_t best = 0;
    if (FP::defined(wavelength)) {
        for (size_t i = 1; i < sizeof(defaultParameters) / sizeof(defaultParameters[0]); i++) {
            if (fabs(defaultParameters[i].wavelength - wavelength) <
                    fabs(defaultParameters[best].wavelength - wavelength))
                best = i;
        }
    }
    return &defaultParameters[best];
}
};

static BaselineSmoother *makeDefaultSmoother()
{
    return new BaselineDigitalFilter(
            new DigitalFilterSinglePoleLowPass(new DynamicTimeInterval::Constant(Time::Minute, 3),
                                               new DynamicTimeInterval::Constant(Time::Minute, 35),
                                               false));
}

static QString makeSuffixMatcher(const QSet<QString> &input)
{
    if (input.isEmpty())
        return ".+";
    QStringList list(input.values());
    for (QStringList::iterator mod = list.begin(), endMod = list.end(); mod != endMod; ++mod) {
        *mod = QString("(?:%1)").arg(QRegExp::escape(*mod));
    }
    std::sort(list.begin(), list.end());
    return QString("(?:%1)").arg(list.join("|"));
}

void CorrVirkkula2005::handleOptions(const ComponentOptions &options)
{
    if (options.isSet("transmittance-start")) {
        defaultTransmittanceStart =
                qobject_cast<DynamicInputOption *>(options.get("transmittance-start"))->getInput();
    } else {
        defaultTransmittanceStart = NULL;
    }

    if (options.isSet("transmittance-end")) {
        defaultTransmittanceEnd =
                qobject_cast<DynamicInputOption *>(options.get("transmittance-end"))->getInput();
    } else {
        defaultTransmittanceEnd = NULL;
    }

    if (options.isSet("k0")) {
        defaultK0 = qobject_cast<DynamicInputOption *>(options.get("k0"))->getInput();
    } else {
        defaultK0 = NULL;
    }
    if (options.isSet("k1")) {
        defaultK1 = qobject_cast<DynamicInputOption *>(options.get("k1"))->getInput();
    } else {
        defaultK1 = NULL;
    }
    if (options.isSet("h0")) {
        defaultH0 = qobject_cast<DynamicInputOption *>(options.get("h0"))->getInput();
    } else {
        defaultH0 = NULL;
    }
    if (options.isSet("h1")) {
        defaultH1 = qobject_cast<DynamicInputOption *>(options.get("h1"))->getInput();
    } else {
        defaultH1 = NULL;
    }
    if (options.isSet("s")) {
        defaultS = qobject_cast<DynamicInputOption *>(options.get("s"))->getInput();
    } else {
        defaultS = NULL;
    }

    if (options.isSet("scattering")) {
        defaultScattering = qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("scattering"))->getOperator();
    } else if (options.isSet("scattering-suffix")) {
        defaultScattering = new DynamicSequenceSelection::Match(QString(), QString(),
                                                                QString("Bsn?[A-Z0-9]*_%1").arg(
                                                                        makeSuffixMatcher(
                                                                                qobject_cast<
                                                                                        ComponentOptionInstrumentSuffixSet *>(
                                                                                        options.get(
                                                                                                "scattering-suffix"))
                                                                                        ->get())));
    } else {
        defaultScattering = NULL;
    }

    if (options.isSet("extinction")) {
        defaultExtinction = qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("extinction"))->getOperator();
    } else if (options.isSet("extinction-suffix")) {
        defaultExtinction = new DynamicSequenceSelection::Match(QString(), QString(),
                                                                QString("Ben?[A-Z0-9]*_%1").arg(
                                                                        makeSuffixMatcher(
                                                                                qobject_cast<
                                                                                        ComponentOptionInstrumentSuffixSet *>(
                                                                                        options.get(
                                                                                                "extinction-suffix"))
                                                                                        ->get())));
    } else {
        defaultExtinction = NULL;
    }

    if (defaultScattering == NULL && defaultExtinction == NULL) {
        defaultScattering = new DynamicSequenceSelection::Match(QString(), QString(),
                                                                QString("Bsn?[A-Z0-9]*_S11"));
    }

    if (options.isSet("smoothing")) {
        defaultSmoother =
                qobject_cast<BaselineSmootherOption *>(options.get("smoothing"))->getSmoother();
    } else {
        defaultSmoother = NULL;
    }


    restrictedInputs = false;

    if (options.isSet("absorption")) {
        restrictedInputs = true;

        Processing p;

        SequenceName::Component station;
        SequenceName::Component archive;
        SequenceName::Component suffix;
        SequenceName::Component trSuffix;
        SequenceName::Flavors flavors;

        CorrectionSet s;
        s.operateAbsorption = qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("absorption"))->getOperator();

        for (const auto &name : s.operateAbsorption->getAllUnits()) {
            station = name.getStation();
            archive = name.getArchive();
            flavors = name.getFlavors();

            if (suffix.empty())
                suffix = Util::suffix(name.getVariable(), '_');

            if (Util::starts_with(name.getVariable(), "Ba"))
                trSuffix = name.getVariable().substr(2);
        }

        if (defaultScattering != NULL) {
            p.scattering = WavelengthAdjust::fromInput(defaultScattering->clone(),
                                                       defaultSmoother != NULL
                                                       ? defaultSmoother->clone()
                                                       : makeDefaultSmoother());
        } else {
            p.scattering = WavelengthAdjust::fromInput(new DynamicSequenceSelection::None);
        }

        if (defaultExtinction != NULL) {
            p.extinction = WavelengthAdjust::fromInput(defaultExtinction->clone(),
                                                       defaultSmoother != NULL
                                                       ? defaultSmoother->clone()
                                                       : makeDefaultSmoother());
        } else {
            p.extinction = WavelengthAdjust::fromInput(new DynamicSequenceSelection::None);
        }

        p.operateFlags = new DynamicSequenceSelection::Single(
                SequenceName(station, archive, "F1_" + suffix, flavors));

        if (defaultTransmittanceStart == NULL) {
            s.transmittanceStart = new DynamicInput::Basic(
                    SequenceName(station, archive, "Ir" + trSuffix, flavors));
        } else {
            s.transmittanceStart = defaultTransmittanceStart->clone();
        }

        if (defaultTransmittanceEnd == NULL) {
            s.transmittanceEnd = new DynamicInput::Basic(
                    SequenceName(station, archive, "Ir" + trSuffix, {SequenceName::flavor_end}));
        } else {
            s.transmittanceEnd = defaultTransmittanceEnd->clone();
        }

        if (defaultK0 == NULL) {
            s.inputK0 = new DynamicInput::Constant;
        } else {
            s.inputK0 = defaultK0->clone();
        }
        if (defaultK1 == NULL) {
            s.inputK1 = new DynamicInput::Constant;
        } else {
            s.inputK1 = defaultK1->clone();
        }
        if (defaultH0 == NULL) {
            s.inputH0 = new DynamicInput::Constant;
        } else {
            s.inputH0 = defaultH0->clone();
        }
        if (defaultH1 == NULL) {
            s.inputH1 = new DynamicInput::Constant;
        } else {
            s.inputH1 = defaultH1->clone();
        }
        if (defaultS == NULL) {
            s.inputS = new DynamicInput::Constant;
        } else {
            s.inputS = defaultS->clone();
        }

        p.operations.push_back(s);
        processing.push_back(p);
    }

    if (options.isSet("suffix")) {
        filterSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->get());
    }
}


CorrVirkkula2005::CorrVirkkula2005()
{ Q_ASSERT(false); }

CorrVirkkula2005::CorrVirkkula2005(const ComponentOptions &options)
{
    handleOptions(options);
}

CorrVirkkula2005::CorrVirkkula2005(const ComponentOptions &options,
                                   double start,
                                   double end,
                                   const QList<SequenceName> &inputs)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    handleOptions(options);

    for (QList<SequenceName>::const_iterator unit = inputs.constBegin(), endU = inputs.constEnd();
            unit != endU;
            ++unit) {
        CorrVirkkula2005::unhandled(*unit, NULL);
    }
}

CorrVirkkula2005::CorrVirkkula2005(double start,
                                   double end,
                                   const SequenceName::Component &station,
                                   const SequenceName::Component &archive,
                                   const ValueSegment::Transfer &config)
{
    defaultSmoother = NULL;
    defaultTransmittanceStart = NULL;
    defaultTransmittanceEnd = NULL;
    defaultK0 = NULL;
    defaultK1 = NULL;
    defaultH0 = NULL;
    defaultH1 = NULL;
    defaultS = NULL;
    defaultScattering = NULL;
    defaultExtinction = NULL;
    restrictedInputs = true;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    Variant::Root defaultAdjust;
    defaultAdjust["Smoothing/Type"].setString("SinglePoleLowPass");
    defaultAdjust["Smoothing/TimeConstant/Units"].setString("Minute");
    defaultAdjust["Smoothing/TimeConstant/Count"].setInt64(3);
    defaultAdjust["Smoothing/Gap/Units"].setString("Minute");
    defaultAdjust["Smoothing/Gap/Count"].setInt64(35);
    defaultAdjust["Smoothing/ResetOnUndefined"].setBool(false);

    for (const auto &child : children) {
        Processing p;

        p.operateFlags = DynamicSequenceSelection::fromConfiguration(config,
                                                                     QString("%1/Flags").arg(
                                                                             QString::fromStdString(
                                                                                     child)), start,
                                                                     end);
        p.scattering = WavelengthAdjust::fromConfiguration(config, QString("%1/Scattering").arg(
                QString::fromStdString(child)), start, end, defaultAdjust);
        p.extinction = WavelengthAdjust::fromConfiguration(config, QString("%1/Extinction").arg(
                QString::fromStdString(child)), start, end, defaultAdjust);

        p.operateFlags->registerExpected(station, archive);
        p.scattering->registerExpected(station, archive);
        p.extinction->registerExpected(station, archive);


        std::unordered_set<Variant::PathElement::HashIndex> operateChildren;
        for (const auto &add : config) {
            Util::merge(
                    add[QString("%1/Correct").arg(QString::fromStdString(child))].toHash().keys(),
                    operateChildren);
        }
        operateChildren.erase(Variant::PathElement::HashIndex());

        for (const auto &operate : operateChildren) {
            CorrectionSet s;

            s.operateAbsorption = DynamicSequenceSelection::fromConfiguration(config,
                                                                              QString("%1/Correct/%2/Absorption")
                                                                                      .arg(QString::fromStdString(
                                                                                              child),
                                                                                           QString::fromStdString(
                                                                                                   operate)),
                                                                              start, end);

            s.transmittanceStart = DynamicInput::fromConfiguration(config,
                                                                   QString("%1/Correct/%2/Transmittance/Start")
                                                                           .arg(QString::fromStdString(
                                                                                   child),
                                                                                QString::fromStdString(
                                                                                        operate)),
                                                                   start, end);
            s.transmittanceEnd = DynamicInput::fromConfiguration(config,
                                                                 QString("%1/Correct/%2/Transmittance/End")
                                                                         .arg(QString::fromStdString(
                                                                                 child),
                                                                              QString::fromStdString(
                                                                                      operate)),
                                                                 start, end);

            s.inputK0 = DynamicInput::fromConfiguration(config, QString("%1/Correct/%2/K0").arg(
                    QString::fromStdString(child), QString::fromStdString(operate)), start, end);
            s.inputK1 = DynamicInput::fromConfiguration(config, QString("%1/Correct/%2/K1").arg(
                    QString::fromStdString(child), QString::fromStdString(operate)), start, end);
            s.inputH0 = DynamicInput::fromConfiguration(config, QString("%1/Correct/%2/H0").arg(
                    QString::fromStdString(child), QString::fromStdString(operate)), start, end);
            s.inputH1 = DynamicInput::fromConfiguration(config, QString("%1/Correct/%2/H1").arg(
                    QString::fromStdString(child), QString::fromStdString(operate)), start, end);
            s.inputS = DynamicInput::fromConfiguration(config, QString("%1/Correct/%2/S").arg(
                    QString::fromStdString(child), QString::fromStdString(operate)), start, end);

            s.operateAbsorption->registerExpected(station, archive);
            s.transmittanceStart->registerExpected(station, archive);
            s.transmittanceEnd->registerExpected(station, archive);
            s.inputK0->registerExpected(station, archive);
            s.inputK1->registerExpected(station, archive);
            s.inputH0->registerExpected(station, archive);
            s.inputH1->registerExpected(station, archive);
            s.inputS->registerExpected(station, archive);

            p.operations.push_back(s);
        }

        processing.push_back(p);
    }
}

void CorrVirkkula2005Component::extendBasicFilterEditing(double &start,
                                                         double &end,
                                                         const SequenceName::Component &station,
                                                         const SequenceName::Component &archive,
                                                         const ValueSegment::Transfer &config,
                                                         Archive::Access *access)
{
    if (!FP::defined(start))
        return;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    Variant::Root defaultAdjust;
    defaultAdjust["Smoothing/Type"].setString("SinglePoleLowPass");
    defaultAdjust["Smoothing/TimeConstant/Units"].setString("Minute");
    defaultAdjust["Smoothing/TimeConstant/Count"].setInt64(3);
    defaultAdjust["Smoothing/Gap/Units"].setString("Minute");
    defaultAdjust["Smoothing/Gap/Count"].setInt64(35);
    defaultAdjust["Smoothing/ResetOnUndefined"].setBool(false);

    double result = start;
    for (const auto &child : children) {
        WavelengthAdjust *wl = WavelengthAdjust::fromConfiguration(config,
                                                                   QString("%1/Scattering").arg(
                                                                           QString::fromStdString(
                                                                                   child)), start,
                                                                   end, defaultAdjust);
        double check = wl->spinupTime(start);
        if (FP::defined(check) && check < result)
            result = check;
        delete wl;

        wl = WavelengthAdjust::fromConfiguration(config, QString("%1/Extinction").arg(
                QString::fromStdString(child)), start, end, defaultAdjust);
        check = wl->spinupTime(start);
        if (FP::defined(check) && check < result)
            result = check;
        delete wl;
    }
    start = result;
}


CorrVirkkula2005::~CorrVirkkula2005()
{
    if (defaultSmoother != NULL)
        delete defaultSmoother;
    if (defaultTransmittanceStart != NULL)
        delete defaultTransmittanceStart;
    if (defaultTransmittanceEnd != NULL)
        delete defaultTransmittanceEnd;
    if (defaultK0 != NULL)
        delete defaultK0;
    if (defaultK1 != NULL)
        delete defaultK1;
    if (defaultH0 != NULL)
        delete defaultH0;
    if (defaultH1 != NULL)
        delete defaultH1;
    if (defaultS != NULL)
        delete defaultS;
    if (defaultScattering != NULL)
        delete defaultScattering;
    if (defaultExtinction != NULL)
        delete defaultExtinction;

    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        delete p->operateFlags;
        delete p->scattering;
        delete p->extinction;
        for (std::vector<CorrectionSet>::const_iterator s = p->operations.begin(),
                endS = p->operations.end(); s != endS; ++s) {
            delete s->operateAbsorption;
            delete s->transmittanceStart;
            delete s->transmittanceEnd;
            delete s->inputK0;
            delete s->inputK1;
            delete s->inputH0;
            delete s->inputH1;
            delete s->inputS;
        }
    }
}

void CorrVirkkula2005::unhandled(const SequenceName &unit,
                                 SegmentProcessingStage::SequenceHandlerControl *control)
{
    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].scattering->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
        }
        if (processing[id].extinction->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
        }

        for (int s = 0, max = (int) processing[id].operations.size(); s < max; s++) {
            if (processing[id].operations[s].transmittanceStart->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].transmittanceEnd->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].inputK0->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].inputK1->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].inputH0->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].inputH1->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].inputS->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
        }
    }
    if (defaultTransmittanceStart != NULL &&
            defaultTransmittanceStart->registerInput(unit) &&
            control != NULL)
        control->deferHandling(unit);
    if (defaultTransmittanceEnd != NULL &&
            defaultTransmittanceEnd->registerInput(unit) &&
            control != NULL)
        control->deferHandling(unit);
    if (defaultK0 != NULL && defaultK0->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultK1 != NULL && defaultK1->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultH0 != NULL && defaultH0->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultH1 != NULL && defaultH1->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultS != NULL && defaultS->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultScattering != NULL && defaultScattering->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultExtinction != NULL && defaultExtinction->registerInput(unit) && control != NULL)
        control->deferHandling(unit);

    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].operateFlags->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            return;
        }
        for (int s = 0, max = (int) processing[id].operations.size(); s < max; s++) {
            if (processing[id].operations[s].operateAbsorption->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                return;
            }
        }
    }

    if (restrictedInputs)
        return;

    if (unit.hasFlavor(SequenceName::flavor_cover) || unit.hasFlavor(SequenceName::flavor_stats))
        return;

    auto suffix = Util::suffix(unit.getVariable(), '_');
    if (suffix.empty())
        return;
    if (!filterSuffixes.empty() && !filterSuffixes.count(suffix))
        return;

    if (!Util::starts_with(unit.getVariable(), "Ba")) {
        if (control) {
            if (Util::starts_with(unit.getVariable(), "F1_") ||
                    Util::starts_with(unit.getVariable(), "Ir")) {
                control->deferHandling(unit);
            }
        }
        return;
    }

    SequenceName suffixUnit(unit.getStation(), unit.getArchive(), suffix, unit.getFlavors());

    SequenceName transmittanceStartUnit
            (unit.getStation(), unit.getArchive(), "Ir" + unit.getVariable().substr(2),
             unit.getFlavors());
    SequenceName transmittanceEndUnit(transmittanceStartUnit.withFlavor(SequenceName::flavor_end));

    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].suffix != suffixUnit)
            continue;

        CorrectionSet s;

        s.operateAbsorption = new DynamicSequenceSelection::Single(unit);

        if (defaultTransmittanceStart == NULL) {
            s.transmittanceStart = new DynamicInput::Basic(transmittanceStartUnit);
        } else {
            s.transmittanceStart = defaultTransmittanceStart->clone();
        }
        s.transmittanceStart->registerInput(unit);
        s.transmittanceStart->registerInput(transmittanceStartUnit);
        s.transmittanceStart->registerInput(transmittanceEndUnit);

        if (defaultTransmittanceEnd == NULL) {
            s.transmittanceEnd = new DynamicInput::Basic(transmittanceEndUnit);
        } else {
            s.transmittanceEnd = defaultTransmittanceEnd->clone();
        }
        s.transmittanceEnd->registerInput(unit);
        s.transmittanceEnd->registerInput(transmittanceStartUnit);
        s.transmittanceEnd->registerInput(transmittanceEndUnit);

        if (defaultK0 == NULL)
            s.inputK0 = new DynamicInput::Constant;
        else
            s.inputK0 = defaultK0->clone();
        s.inputK0->registerInput(unit);
        s.inputK0->registerInput(transmittanceStartUnit);
        s.inputK0->registerInput(transmittanceEndUnit);

        if (defaultK1 == NULL)
            s.inputK1 = new DynamicInput::Constant;
        else
            s.inputK1 = defaultK1->clone();
        s.inputK1->registerInput(unit);
        s.inputK1->registerInput(transmittanceStartUnit);
        s.inputK1->registerInput(transmittanceEndUnit);

        if (defaultH0 == NULL)
            s.inputH0 = new DynamicInput::Constant;
        else
            s.inputH0 = defaultH0->clone();
        s.inputH0->registerInput(unit);
        s.inputH0->registerInput(transmittanceStartUnit);
        s.inputH0->registerInput(transmittanceEndUnit);

        if (defaultH1 == NULL)
            s.inputH1 = new DynamicInput::Constant;
        else
            s.inputH1 = defaultH1->clone();
        s.inputH1->registerInput(unit);
        s.inputH1->registerInput(transmittanceStartUnit);
        s.inputH1->registerInput(transmittanceEndUnit);

        if (defaultS == NULL)
            s.inputS = new DynamicInput::Constant;
        else
            s.inputS = defaultS->clone();
        s.inputS->registerInput(unit);
        s.inputS->registerInput(transmittanceStartUnit);
        s.inputS->registerInput(transmittanceEndUnit);

        processing[id].operations.push_back(s);

        if (control) {
            for (const auto &n : s.transmittanceStart->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.transmittanceEnd->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.inputK0->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.inputK1->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.inputH0->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.inputH1->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.inputS->getUsedInputs()) {
                control->inputUnit(n, id);
            }

            control->filterUnit(unit, id);
        }
        return;
    }

    Processing p;
    p.suffix = suffixUnit;

    SequenceName flagUnit
            (unit.getStation(), unit.getArchive(), "F1_" + suffix, unit.getFlavors());
    p.operateFlags = new DynamicSequenceSelection::Single(flagUnit);
    p.operateFlags->registerInput(unit);
    p.operateFlags->registerInput(flagUnit);
    p.operateFlags->registerInput(transmittanceStartUnit);
    p.operateFlags->registerInput(transmittanceEndUnit);


    if (defaultScattering != NULL) {
        DynamicSequenceSelection *s = defaultScattering->clone();
        s->forceUnit(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        p.scattering = WavelengthAdjust::fromInput(s, defaultSmoother != NULL
                                                      ? defaultSmoother->clone()
                                                      : makeDefaultSmoother());
    } else {
        p.scattering = WavelengthAdjust::fromInput(new DynamicSequenceSelection::None);
    }
    p.scattering->registerInput(unit);
    p.scattering->registerInput(flagUnit);
    p.scattering->registerInput(transmittanceStartUnit);
    p.scattering->registerInput(transmittanceEndUnit);

    if (defaultExtinction != NULL) {
        DynamicSequenceSelection *e = defaultExtinction->clone();
        e->forceUnit(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        p.extinction = WavelengthAdjust::fromInput(e, defaultSmoother != NULL
                                                      ? defaultSmoother->clone()
                                                      : makeDefaultSmoother());
    } else {
        p.extinction = WavelengthAdjust::fromInput(new DynamicSequenceSelection::None);
    }
    p.extinction->registerInput(unit);
    p.extinction->registerInput(flagUnit);
    p.extinction->registerInput(transmittanceStartUnit);
    p.extinction->registerInput(transmittanceEndUnit);


    CorrectionSet s;

    s.operateAbsorption = new DynamicSequenceSelection::Single(unit);

    if (defaultTransmittanceStart == NULL) {
        s.transmittanceStart = new DynamicInput::Basic(transmittanceStartUnit);
    } else {
        s.transmittanceStart = defaultTransmittanceStart->clone();
    }
    s.transmittanceStart->registerInput(unit);
    s.transmittanceStart->registerInput(flagUnit);
    s.transmittanceStart->registerInput(transmittanceStartUnit);
    s.transmittanceStart->registerInput(transmittanceEndUnit);

    if (defaultTransmittanceEnd == NULL) {
        s.transmittanceEnd = new DynamicInput::Basic(transmittanceEndUnit);
    } else {
        s.transmittanceEnd = defaultTransmittanceEnd->clone();
    }
    s.transmittanceEnd->registerInput(unit);
    s.transmittanceEnd->registerInput(flagUnit);
    s.transmittanceEnd->registerInput(transmittanceStartUnit);
    s.transmittanceEnd->registerInput(transmittanceEndUnit);

    if (defaultK0 == NULL)
        s.inputK0 = new DynamicInput::Constant;
    else
        s.inputK0 = defaultK0->clone();
    s.inputK0->registerInput(unit);
    s.inputK0->registerInput(flagUnit);
    s.inputK0->registerInput(transmittanceStartUnit);
    s.inputK0->registerInput(transmittanceEndUnit);

    if (defaultK1 == NULL)
        s.inputK1 = new DynamicInput::Constant;
    else
        s.inputK1 = defaultK1->clone();
    s.inputK1->registerInput(unit);
    s.inputK1->registerInput(flagUnit);
    s.inputK1->registerInput(transmittanceStartUnit);
    s.inputK1->registerInput(transmittanceEndUnit);

    if (defaultH0 == NULL)
        s.inputH0 = new DynamicInput::Constant;
    else
        s.inputH0 = defaultH0->clone();
    s.inputH0->registerInput(unit);
    s.inputH0->registerInput(flagUnit);
    s.inputH0->registerInput(transmittanceStartUnit);
    s.inputH0->registerInput(transmittanceEndUnit);

    if (defaultH1 == NULL)
        s.inputH1 = new DynamicInput::Constant;
    else
        s.inputH1 = defaultH1->clone();
    s.inputH1->registerInput(unit);
    s.inputH1->registerInput(flagUnit);
    s.inputH1->registerInput(transmittanceStartUnit);
    s.inputH1->registerInput(transmittanceEndUnit);

    if (defaultS == NULL)
        s.inputS = new DynamicInput::Constant;
    else
        s.inputS = defaultS->clone();
    s.inputS->registerInput(unit);
    s.inputS->registerInput(flagUnit);
    s.inputS->registerInput(transmittanceStartUnit);
    s.inputS->registerInput(transmittanceEndUnit);

    p.operations.push_back(s);

    int id = (int) processing.size();
    processing.push_back(p);

    if (control) {
        for (const auto &n : p.scattering->getAllUnits()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : p.extinction->getAllUnits()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.transmittanceStart->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.transmittanceEnd->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.inputK0->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.inputK1->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.inputH0->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.inputH1->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.inputS->getUsedInputs()) {
            control->inputUnit(n, id);
        }

        control->filterUnit(unit, id);
        if (unit != flagUnit)
            control->filterUnit(flagUnit, id);
    }
}

static double evaluateCorrection(double Ba0,
                                 double Ir,
                                 double Bs,
                                 double k0,
                                 double k1,
                                 double h0,
                                 double h1,
                                 double s,
                                 double w)
{
    return (k0 + k1 * (h0 + h1 * w) * log(Ir)) * Ba0 - s * Bs;
}

static double applyScattering(double Ba0,
                              double Ir,
                              double Bs,
                              double k0,
                              double k1,
                              double h0,
                              double h1,
                              double s)
{
    Q_ASSERT(FP::defined(Ba0));
    Q_ASSERT(FP::defined(Ir));
    Q_ASSERT(FP::defined(Bs));
    Q_ASSERT(FP::defined(k0));
    Q_ASSERT(FP::defined(k1));
    Q_ASSERT(FP::defined(h0));
    Q_ASSERT(FP::defined(h1));
    Q_ASSERT(FP::defined(s));
    if (Ir <= 0.0)
        return FP::undefined();

    double Ba = Ba0;
    for (int itter = 0; itter < 20; itter++) {
        double Be = Ba + Bs;
        if (Be == 0.0)
            return FP::undefined();
        double w = Bs / Be;
        double BaPrior = Ba;
        Ba = evaluateCorrection(Ba0, Ir, Bs, k0, k1, h0, h1, s, w);
        if (!FP::defined(Ba))
            return FP::undefined();
        if (fabs(Ba - BaPrior) < 1E-6)
            return Ba;
    }
    return Ba;
}

static double applyExtinction(double Ba0,
                              double Ir,
                              double Be,
                              double k0,
                              double k1,
                              double h0,
                              double h1,
                              double s)
{
    Q_ASSERT(FP::defined(Ba0));
    Q_ASSERT(FP::defined(Ir));
    Q_ASSERT(FP::defined(Be));
    Q_ASSERT(FP::defined(k0));
    Q_ASSERT(FP::defined(k1));
    Q_ASSERT(FP::defined(h0));
    Q_ASSERT(FP::defined(h1));
    Q_ASSERT(FP::defined(s));
    if (Ir <= 0.0 || Be == 0.0)
        return FP::undefined();

    double Ba = Ba0;
    for (int itter = 0; itter < 20; itter++) {
        double Bs = Be - Ba;
        double w = Bs / Be;
        double BaPrior = Ba;
        Ba = evaluateCorrection(Ba0, Ir, Bs, k0, k1, h0, h1, s, w);
        if (!FP::defined(Ba))
            return FP::undefined();
        if (fabs(Ba - BaPrior) < 1E-6)
            return Ba;
    }
    return Ba;
}

static double convertReferenced(const Variant::Read &base, const Variant::Read &reference)
{
    double a = base.getPath(reference.currentPath()).toReal();
    if (FP::defined(a))
        return a;
    switch (base.getType()) {
    case Variant::Type::Array:
    case Variant::Type::Matrix:
        if (reference.currentPath().empty()) {
            return base.array(0).toReal();
        }
        return FP::undefined();
    default:
        break;
    }

    return base.toReal();
}

static const Variant::Flag FLAG_NAME = "Virkkula2005";

void CorrVirkkula2005::process(int id, SequenceSegment &data)
{
    Processing *proc = &processing[id];

    for (const auto &i : proc->operateFlags->get(data)) {
        if (!data.exists(i))
            continue;
        auto d = data[i];
        if (!d.exists())
            continue;
        d.applyFlag(FLAG_NAME);
    }

    proc->scattering->incoming(data);
    proc->extinction->incoming(data);

    for (std::vector<CorrectionSet>::const_iterator set = proc->operations.begin(),
            endSet = proc->operations.end(); set != endSet; ++set) {
        double startIr = set->transmittanceStart->get(data);
        double endIr = set->transmittanceEnd->get(data);
        double Ir = FP::undefined();
        if (FP::defined(startIr)) {
            if (FP::defined(endIr)) {
                Ir = (startIr + endIr) / 2.0;
            } else {
                Ir = startIr;
            }
        } else if (FP::defined(endIr)) {
            Ir = endIr;
        }

        double overrideK0 = set->inputK0->get(data);
        double overrideK1 = set->inputK1->get(data);
        double overrideH0 = set->inputH0->get(data);
        double overrideH1 = set->inputH1->get(data);
        double overrideS = set->inputS->get(data);

        for (const auto &i : set->operateAbsorption->get(data)) {
            if (!data.exists(i))
                continue;
            double wavelength = proc->absorptionWavelength.get(i);
            auto scattering = proc->scattering->getAdjusted(wavelength);
            auto extinction = proc->extinction->getAdjusted(wavelength);
            const DefaultParameters *parameters = getDefaults(wavelength);

            double k0 = overrideK0;
            if (!FP::defined(k0))
                k0 = parameters->k0;
            double k1 = overrideK1;
            if (!FP::defined(k1))
                k1 = parameters->k1;
            double h0 = overrideH0;
            if (!FP::defined(h0))
                h0 = parameters->h0;
            double h1 = overrideH1;
            if (!FP::defined(h1))
                h1 = parameters->h1;
            double ss = overrideS;
            if (!FP::defined(ss))
                ss = parameters->s;

            Variant::Composite::applyInplace(data[i], [&](Variant::Write &d) {
                double v = d.toDouble();

                if (!FP::defined(v) ||
                        !FP::defined(Ir) ||
                        !FP::defined(k0) ||
                        !FP::defined(k1) ||
                        !FP::defined(h0) ||
                        !FP::defined(h1) ||
                        !FP::defined(ss)) {
                    Variant::Composite::invalidate(d);
                    return;
                }

                double Bs = convertReferenced(scattering, d);
                double Be = convertReferenced(extinction, d);

                if (FP::defined(Bs)) {
                    d.setDouble(applyScattering(v, Ir, Bs, k0, k1, h0, h1, ss));
                } else if (FP::defined(Be)) {
                    d.setDouble(applyExtinction(v, Ir, Be, k0, k1, h0, h1, ss));
                } else {
                    Variant::Composite::invalidate(d);
                }
            });
        }
    }
}

void CorrVirkkula2005::processMeta(int id, SequenceSegment &data)
{
    Processing *p = &processing[id];

    p->scattering->incomingMeta(data);
    p->extinction->incomingMeta(data);

    Variant::Root meta;

    meta["By"].setString("corr_virkkula2005");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    for (std::vector<CorrectionSet>::const_iterator set = p->operations.begin(),
            endSet = p->operations.end(); set != endSet; ++set) {
        const auto &s = set->operateAbsorption->get(data);
        p->absorptionWavelength.incomingMeta(data, s);
        for (const auto &i : s) {
            auto dvu = i.toMeta();
            if (!data.exists(dvu))
                continue;

            const DefaultParameters *def = getDefaults(p->absorptionWavelength.get(i));

            meta["Parameters"].hash("K0") =
                    set->inputK0->describe(data, FP::decimalFormat(def->k0));
            meta["Parameters"].hash("K1") =
                    set->inputK1->describe(data, FP::decimalFormat(def->k1));
            meta["Parameters"].hash("H0") =
                    set->inputH0->describe(data, FP::decimalFormat(def->h0));
            meta["Parameters"].hash("H1") =
                    set->inputH1->describe(data, FP::decimalFormat(def->h1));
            meta["Parameters"].hash("S") = set->inputS->describe(data, FP::decimalFormat(def->s));

            data[dvu].metadata("Processing").toArray().after_back().set(meta);
            clearPropagatedSmoothing(data[dvu].metadata("Smoothing"));
        }
    }

    for (auto i : p->operateFlags->get(data)) {
        i.setMeta();
        if (!data.exists(i))
            continue;
        auto flagMeta = data[i].metadataSingleFlag(FLAG_NAME);
        flagMeta.hash("Origin").toArray().after_back().setString("corr_virkkula2005");
        flagMeta.hash("Description").setString("Virkkula 2005 correction applied");
    }
}


SequenceName::Set CorrVirkkula2005::requestedInputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        Util::merge(p->operateFlags->getAllUnits(), out);
        Util::merge(p->scattering->getAllUnits(), out);
        Util::merge(p->extinction->getAllUnits(), out);
        for (std::vector<CorrectionSet>::const_iterator s = p->operations.begin(),
                endS = p->operations.end(); s != endS; ++s) {
            Util::merge(s->operateAbsorption->getAllUnits(), out);
            Util::merge(s->transmittanceStart->getUsedInputs(), out);
            Util::merge(s->transmittanceEnd->getUsedInputs(), out);
            Util::merge(s->inputK0->getUsedInputs(), out);
            Util::merge(s->inputK1->getUsedInputs(), out);
            Util::merge(s->inputH0->getUsedInputs(), out);
            Util::merge(s->inputH1->getUsedInputs(), out);
            Util::merge(s->inputS->getUsedInputs(), out);
        }
    }
    return out;
}

QSet<double> CorrVirkkula2005::metadataBreaks(int id)
{
    Processing *p = &processing[id];
    QSet<double> result;
    Util::merge(p->operateFlags->getChangedPoints(), result);
    for (std::vector<CorrectionSet>::const_iterator s = p->operations.begin(),
            end = p->operations.end(); s != end; ++s) {
        Util::merge(s->operateAbsorption->getChangedPoints(), result);
        Util::merge(s->transmittanceStart->getChangedPoints(), result);
        Util::merge(s->transmittanceEnd->getChangedPoints(), result);
        Util::merge(s->inputK0->getChangedPoints(), result);
        Util::merge(s->inputK1->getChangedPoints(), result);
        Util::merge(s->inputH0->getChangedPoints(), result);
        Util::merge(s->inputH1->getChangedPoints(), result);
        Util::merge(s->inputS->getChangedPoints(), result);
    }
    return result;
}

CorrVirkkula2005::CorrVirkkula2005(QDataStream &stream)
{
    stream >> defaultSmoother;
    stream >> defaultTransmittanceStart;
    stream >> defaultTransmittanceEnd;
    stream >> defaultK0;
    stream >> defaultK1;
    stream >> defaultH0;
    stream >> defaultH1;
    stream >> defaultS;
    stream >> defaultScattering;
    stream >> defaultExtinction;
    stream >> filterSuffixes;
    stream >> restrictedInputs;

    quint32 n;
    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        Processing p;
        stream >> p.operateFlags;
        stream >> p.scattering;
        stream >> p.extinction;
        stream >> p.absorptionWavelength;
        stream >> p.suffix;

        quint32 m;
        stream >> m;
        for (int j = 0; j < (int) m; j++) {
            CorrectionSet s;
            stream >> s.operateAbsorption;
            stream >> s.transmittanceStart;
            stream >> s.transmittanceEnd;
            stream >> s.inputK0;
            stream >> s.inputK1;
            stream >> s.inputH0;
            stream >> s.inputH1;
            stream >> s.inputS;

            p.operations.push_back(s);
        }

        processing.push_back(p);
    }
}

void CorrVirkkula2005::serialize(QDataStream &stream)
{
    stream << defaultSmoother;
    stream << defaultTransmittanceStart;
    stream << defaultTransmittanceEnd;
    stream << defaultK0;
    stream << defaultK1;
    stream << defaultH0;
    stream << defaultH1;
    stream << defaultS;
    stream << defaultScattering;
    stream << defaultExtinction;
    stream << filterSuffixes;
    stream << restrictedInputs;

    quint32 n = (quint32) processing.size();
    stream << n;
    for (int i = 0; i < (int) n; i++) {
        stream << processing[i].operateFlags;
        stream << processing[i].scattering;
        stream << processing[i].extinction;
        stream << processing[i].absorptionWavelength;
        stream << processing[i].suffix;

        quint32 m = (quint32) processing[i].operations.size();
        stream << m;
        for (int j = 0; j < (int) m; j++) {
            stream << processing[i].operations[j].operateAbsorption;
            stream << processing[i].operations[j].transmittanceStart;
            stream << processing[i].operations[j].transmittanceEnd;
            stream << processing[i].operations[j].inputK0;
            stream << processing[i].operations[j].inputK1;
            stream << processing[i].operations[j].inputH0;
            stream << processing[i].operations[j].inputH1;
            stream << processing[i].operations[j].inputS;
        }
    }
}


QString CorrVirkkula2005Component::getBasicSerializationName() const
{ return QString::fromLatin1("corr_virkkula2005"); }

ComponentOptions CorrVirkkula2005Component::getOptions()
{
    ComponentOptions options;

    options.add("transmittance-start", new DynamicInputOption(tr("transmittance-start", "name"),
                                                              tr("Input start transmittance"),
                                                              tr("This is the input start transmittance used in the correction.  "
                                                                 "When this option is set, this transmittance is used for all "
                                                                 "absorption channels.  Therefor, normally only a single absorption "
                                                                 "can be corrected while using this option."),
                                                              tr("Resolved automatically by instrument suffix")));

    options.add("transmittance-end", new DynamicInputOption(tr("transmittance-end", "name"),
                                                            tr("Input end transmittance"),
                                                            tr("This is the input end transmittance used in the correction.  "
                                                               "When this option is set, this transmittance is used for all "
                                                               "absorption channels.  Therefor, normally only a single absorption "
                                                               "can be corrected while using this option."),
                                                            tr("Resolved automatically by instrument suffix")));

    options.add("k0", new DynamicInputOption(tr("k0", "name"), tr("k0 constant"),
                                             tr("This is the k0 constant used to generate the absorption "
                                                    "scaling factor.  "
                                                    "Ba = [k0 + k1\xC3\x97(h0 + h1\xC3\x97\xCF\x89)\xC3\x97ln(Tr)]\xC3\x97\xCF\x83\x61\x30 - s\xC3\x97\xCF\x83\x73"),
                                             tr("Wavelength dependent")));
    options.add("k1", new DynamicInputOption(tr("k1", "name"), tr("k1 constant"),
                                             tr("This is the k1 constant used to generate the absorption "
                                                    "scaling factor.  "
                                                    "Ba = [k0 + k1\xC3\x97(h0 + h1\xC3\x97\xCF\x89)\xC3\x97ln(Tr)]\xC3\x97\xCF\x83\x61\x30 - s\xC3\x97\xCF\x83\x73"),
                                             tr("Wavelength dependent")));
    options.add("h0", new DynamicInputOption(tr("h0", "name"), tr("h0 constant"),
                                             tr("This is the h1 constant used to generate the absorption "
                                                    "scaling factor.  "
                                                    "Ba = [k0 + k1\xC3\x97(h0 + h1\xC3\x97\xCF\x89)\xC3\x97ln(Tr)]\xC3\x97\xCF\x83\x61\x30 - s\xC3\x97\xCF\x83\x73"),
                                             tr("Wavelength dependent")));
    options.add("h1", new DynamicInputOption(tr("h1", "name"), tr("h1 constant"),
                                             tr("This is the h1 constant used to generate the absorption "
                                                    "scaling factor.  "
                                                    "Ba = [k0 + k1\xC3\x97(h0 + h1\xC3\x97\xCF\x89)\xC3\x97ln(Tr)]\xC3\x97\xCF\x83\x61\x30 - s\xC3\x97\xCF\x83\x73"),
                                             tr("Wavelength dependent")));
    options.add("s", new DynamicInputOption(tr("s", "name"), tr("s constant"),
                                            tr("This is the s constant that is multiplied by the scattering and "
                                                   "subtracted from the absorption.  "
                                                   "Ba = [k0 + k1\xC3\x97(h0 + h1\xC3\x97\xCF\x89)\xC3\x97ln(Tr)]\xC3\x97\xCF\x83\x61\x30 - s\xC3\x97\xCF\x83\x73"),
                                            tr("Wavelength dependent")));

    BaselineSmootherOption *smoothing = new BaselineSmootherOption(tr("smoothing", "name"),
                                                                   tr("Scattering and extinction smoothing"),
                                                                   tr("This is the smoothing applied to the scattering and/or extinction "
                                                                      "inputs before any calculation using them."),
                                                                   tr("3-minute TC low pass filter"));
    smoothing->setStabilityDetection(false);
    smoothing->setSpikeDetection(false);
    smoothing->setDefault(makeDefaultSmoother());
    options.add("smoothing", smoothing);

    options.add("absorption", new DynamicSequenceSelectionOption(tr("correct", "name"),
                                                                 tr("Absorptions to correct"),
                                                                 tr("These are the variables to that the correction is applied to.  "
                                                                    "This option is mutually exclusive with instrument specification."),
                                                                 tr("All absorptions")));
    options.add("scattering", new DynamicSequenceSelectionOption(tr("scattering", "name"),
                                                                 tr("Input scatterings"),
                                                                 tr("These are the variables to the light scattering coefficients used "
                                                                    "by the correction are contained in.  This option is mutually "
                                                                    "exclusive with instrument specification."),
                                                                 tr("All scatterings from S11")));
    options.add("extinction", new DynamicSequenceSelectionOption(tr("extinction", "name"),
                                                                 tr("Input extinctions"),
                                                                 tr("These are the variables to the light extinction coefficients used "
                                                                    "by the correction are contained in.  By default none are used.  "
                                                                    "This option is mutually exclusive with instrument specification."),
                                                                 QString()));

    options.add("suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments", "name"),
                                                                 tr("Instrument suffixes to correct"),
                                                                 tr("These are the instrument suffixes to correct.  "
                                                                    "For example S11 would usually specifies the reference nephelometer.  "
                                                                    "This option is mutually exclusive with manual variable specification."),
                                                                 tr("All instrument suffixes")));
    options.add("scattering-suffix",
                new ComponentOptionInstrumentSuffixSet(tr("scattering-instruments", "name"),
                                                       tr("Scattering instrument suffixes"),
                                                       tr("These are the instrument suffixes that input light scatterings are "
                                                          "read from.  "
                                                          "For example S11 would usually specifies the reference nephelometer.  "
                                                          "This option is mutually exclusive with manual variable specification."),
                                                       tr("S11")));
    options.add("extinction-suffix",
                new ComponentOptionInstrumentSuffixSet(tr("extinction-instruments", "name"),
                                                       tr("Extinction instrument suffixes"),
                                                       tr("These are the instrument suffixes that input light extinctions are "
                                                          "read from.  "
                                                          "For example S11 would usually specifies the reference nephelometer.  "
                                                          "This option is mutually exclusive with manual variable specification."),
                                                       QString()));

    options.exclude("absorption", "suffix");
    options.exclude("scattering", "scattering-suffix");
    options.exclude("extinction", "extinction-suffix");
    options.exclude("suffix", "absorption");
    options.exclude("scattering-suffix", "scattering");
    options.exclude("extinction-suffix", "extinction");

    return options;
}

QList<ComponentExample> CorrVirkkula2005Component::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will correct all absorptions present using the scatterings "
                                        "from the S11 instrument.")));

    options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")))->add("A11");
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("extinction-suffix")))->add(
            "E11");
    (qobject_cast<DynamicInputOption *>(options.get("k0")))->set(0.534);
    (qobject_cast<DynamicInputOption *>(options.get("k1")))->set(-0.617);
    examples.append(ComponentExample(options, tr("Single instrument with manual constants"),
                                     tr("This will correct all absorptions from A11 using extinctions from "
                                        "E11.  Non-standard constants with k0 = 0.534 and k1 = -0.617 are "
                                        "also used.")));

    options = getOptions();
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("absorption")))->set("", "",
                                                                                     "BaG_A11");
    examples.append(ComponentExample(options, tr("Single variable"),
                                     tr("This will correct only the variable BaG_A11.")));

    return examples;
}

SegmentProcessingStage *CorrVirkkula2005Component::createBasicFilterDynamic(const ComponentOptions &options)
{
    return new CorrVirkkula2005(options);
}

SegmentProcessingStage *CorrVirkkula2005Component::createBasicFilterPredefined(const ComponentOptions &options,
                                                                               double start,
                                                                               double end,
                                                                               const QList<
                                                                                       SequenceName> &inputs)
{
    return new CorrVirkkula2005(options, start, end, inputs);
}

SegmentProcessingStage *CorrVirkkula2005Component::createBasicFilterEditing(double start,
                                                                            double end,
                                                                            const SequenceName::Component &station,
                                                                            const SequenceName::Component &archive,
                                                                            const ValueSegment::Transfer &config)
{
    return new CorrVirkkula2005(start, end, station, archive, config);
}

SegmentProcessingStage *CorrVirkkula2005Component::deserializeBasicFilter(QDataStream &stream)
{
    return new CorrVirkkula2005(stream);
}
