/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CORRVIRKKULA2005_H
#define CORRVIRKKULA2005_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QList>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "editing/wavelengthadjust.hxx"
#include "smoothing/baseline.hxx"

class CorrVirkkula2005 : public CPD3::Data::SegmentProcessingStage {
    class CorrectionSet {
    public:
        CPD3::Data::DynamicSequenceSelection *operateAbsorption;

        CPD3::Data::DynamicInput *transmittanceStart;
        CPD3::Data::DynamicInput *transmittanceEnd;

        CPD3::Data::DynamicInput *inputK0;
        CPD3::Data::DynamicInput *inputK1;
        CPD3::Data::DynamicInput *inputH0;
        CPD3::Data::DynamicInput *inputH1;
        CPD3::Data::DynamicInput *inputS;

        CorrectionSet() : operateAbsorption(NULL),
                          transmittanceStart(NULL),
                          transmittanceEnd(NULL),
                          inputK0(NULL),
                          inputK1(NULL),
                          inputH0(NULL),
                          inputH1(NULL),
                          inputS(NULL)
        { }
    };

    class Processing {
    public:
        std::vector<CorrectionSet> operations;

        CPD3::Data::DynamicSequenceSelection *operateFlags;

        CPD3::Editing::WavelengthAdjust *scattering;
        CPD3::Editing::WavelengthAdjust *extinction;
        CPD3::Editing::WavelengthTracker absorptionWavelength;

        CPD3::Data::SequenceName suffix;

        Processing()
                : operations(),
                  operateFlags(NULL),
                  scattering(NULL),
                  extinction(NULL),
                  absorptionWavelength(),
                  suffix()
        { }
    };

    CPD3::Smoothing::BaselineSmoother *defaultSmoother;
    CPD3::Data::DynamicInput *defaultTransmittanceStart;
    CPD3::Data::DynamicInput *defaultTransmittanceEnd;
    CPD3::Data::DynamicInput *defaultK0;
    CPD3::Data::DynamicInput *defaultK1;
    CPD3::Data::DynamicInput *defaultH0;
    CPD3::Data::DynamicInput *defaultH1;
    CPD3::Data::DynamicInput *defaultS;
    CPD3::Data::DynamicSequenceSelection *defaultScattering;
    CPD3::Data::DynamicSequenceSelection *defaultExtinction;
    CPD3::Data::SequenceName::ComponentSet filterSuffixes;

    bool restrictedInputs;

    void handleOptions(const CPD3::ComponentOptions &options);

    std::vector<Processing> processing;

public:
    CorrVirkkula2005();

    CorrVirkkula2005(const CPD3::ComponentOptions &options);

    CorrVirkkula2005(const CPD3::ComponentOptions &options,
                     double start, double end, const QList<CPD3::Data::SequenceName> &inputs);

    CorrVirkkula2005(double start,
                     double end,
                     const CPD3::Data::SequenceName::Component &station,
                     const CPD3::Data::SequenceName::Component &archive,
                     const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CorrVirkkula2005();

    void unhandled(const CPD3::Data::SequenceName &unit,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    QSet<double> metadataBreaks(int id) override;

    CorrVirkkula2005(QDataStream &stream);

    void serialize(QDataStream &stream) override;
};

class CorrVirkkula2005Component
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.corr_virkkula2005"
                              FILE
                              "corr_virkkula2005.json")

public:
    virtual QString getBasicSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Data::SegmentProcessingStage
            *createBasicFilterDynamic(const CPD3::ComponentOptions &options);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined
            (const CPD3::ComponentOptions &options,
             double start,
             double end, const QList<CPD3::Data::SequenceName> &inputs);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                         double end,
                                                                         const CPD3::Data::SequenceName::Component &station,
                                                                         const CPD3::Data::SequenceName::Component &archive,
                                                                         const CPD3::Data::ValueSegment::Transfer &config);

    virtual void extendBasicFilterEditing(double &start,
                                          double &end,
                                          const CPD3::Data::SequenceName::Component &station,
                                          const CPD3::Data::SequenceName::Component &archive,
                                          const CPD3::Data::ValueSegment::Transfer &config,
                                          CPD3::Data::Archive::Access *access);

    virtual CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream);
};

#endif
