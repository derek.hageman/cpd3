/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "sync_connect.hxx"


Q_LOGGING_CATEGORY(log_component_sync_connect, "cpd3.component.sync.connect", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Sync;
using namespace CPD3::Data;

SyncClient::SyncClient(const QString &host,
                       quint16 port, const Variant::Read &ssl,
                       const ComponentOptions &options) : Client(host, port, ssl), runUpdate(true)
{
    if (options.isSet("update")) {
        runUpdate = qobject_cast<ComponentOptionBoolean *>(options.get("update"))->get();
    }
}

SyncClient::~SyncClient()
{ }

void SyncClient::run()
{
    Client::run();
    if (runUpdate)
        invokeUpdated();
}

void SyncClient::executeAction(CPD3Action *action)
{
    if (action == NULL) {
        qCWarning(log_component_sync_connect) << "Unable to create action";
        return;
    }

    action->feedback.forward(feedback);
    connect(this, SIGNAL(terminateRequested()), action, SLOT(signalTerminate()),
            Qt::DirectConnection);

    action->start();
    action->wait();
    delete action;
}

void SyncClient::invokeUpdated()
{
    feedback.emitStage(tr("Checking for updated data"),
                       tr("The system is starting the passed data update."), false);

    QObject *component = ComponentLoader::create("update_passed");
    if (component == NULL) {
        qCWarning(log_component_sync_connect) << "Can't load updated passed component";
        return;
    }

    ActionComponentTime *actionComponentTime;
    if (!(actionComponentTime = qobject_cast<ActionComponentTime *>(component))) {
        qCWarning(log_component_sync_connect) << "Updated passed component is not a time action";
        return;
    }

    if (actionComponentTime->actionRequireStations() > 0) {
        qCWarning(log_component_sync_connect)
            << "Updated passed component does not accept the number of passed stations";
        return;
    }

    ComponentOptions o = actionComponentTime->getOptions();

    if (auto detachedOption = qobject_cast<ComponentOptionBoolean *>(o.get("detached"))) {
        detachedOption->set(true);
    }

    executeAction(actionComponentTime->createTimeAction(o));
}

void SyncClient::signalTerminate()
{
    Client::signalTerminate();
    emit terminateRequested();
}


ComponentOptions SyncConnectComponent::getOptions()
{
    ComponentOptions options;

    options.add("ssl-cert",
                new ComponentOptionFile(tr("ssl-cert", "name"), tr("The SSL certificate file"),
                                        tr("This sets the certificate used for the connection."),
                                        tr("sync.crt")));
    options.add("ssl-key", new ComponentOptionFile(tr("ssl-key", "name"), tr("The SSL key file"),
                                                   tr("This sets the key used for the connection."),
                                                   tr("sync.key")));

    options.add("server",
                new ComponentOptionSingleString(tr("server", "name"), tr("Synchronize server"),
                                                tr("This is the hostname of the server that synchronization is "
                                                           "performed with."),
                                                tr("aero1.cmdl.noaa.gov")));

    ComponentOptionSingleInteger *i =
            new ComponentOptionSingleInteger(tr("port", "name"), tr("The port to connect on"),
                                             tr("This is the port on the remote server that the connection is "
                                                        "established to."), tr("14231"));
    i->setAllowUndefined(false);
    i->setMinimum(1);
    i->setMaximum(65535);
    options.add("port", i);

    options.add("update",
                new ComponentOptionBoolean(tr("update", "name"), tr("Disable data update"),
                                           tr("When disabled, this causes the immediate data update to be "
                                                      "skipped.  The result is that the clean and averaged data are "
                                                      "not updated until the next time the station tasks are run at "
                                                      "the appropriate level.  This normally means that the data will "
                                                      "not be updated until the following night when the execution tasks "
                                                      "run automatically."), tr("Enabled")));

    return options;
}

QList<ComponentExample> SyncConnectComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Run synchronization", "default example name"),
                                     tr("This will run synchronization connecting to the default NOAA "
                                                "server.")));

    return examples;
}

int SyncConnectComponent::actionAllowStations()
{ return 0; }

int SyncConnectComponent::actionRequireStations()
{ return 0; }

CPD3Action *SyncConnectComponent::createAction(const ComponentOptions &options,
                                               const std::vector<std::string> &)
{
    QString cert("sync.crt");
    if (options.isSet("ssl-cert")) {
        cert = qobject_cast<ComponentOptionFile *>(options.get("ssl-cert"))->get();
    }

    QString key("sync.key");
    if (options.isSet("ssl-key")) {
        key = qobject_cast<ComponentOptionFile *>(options.get("ssl-key"))->get();
    }

    QString host("aero1.cmdl.noaa.gov");
    if (options.isSet("server")) {
        host = qobject_cast<ComponentOptionSingleString *>(options.get("server"))->get();
    }

    quint16 port = 14231;
    if (options.isSet("port")) {
        port = (quint16) qobject_cast<ComponentOptionSingleInteger *>(options.get("port"))->get();
    }

    Variant::Write ssl = Variant::Write::empty();
    ssl.hash("Certificate").setString(cert);
    ssl.hash("Key").setString(key);

    return new SyncClient(host, port, ssl, options);
}
