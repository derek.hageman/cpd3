/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef SYNCCONNECT_H
#define SYNCCONNECT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "sync/client.hxx"

class SyncClient : public CPD3::Sync::Client {
Q_OBJECT

    bool runUpdate;

    void executeAction(CPD3::CPD3Action *action);

    void invokeUpdated();

public:
    SyncClient(const QString &host,
               quint16 port,
               const CPD3::Data::Variant::Read &ssl,
               const CPD3::ComponentOptions &options);

    virtual ~SyncClient();

public slots:

    virtual void signalTerminate();

signals:

    void terminateRequested();

protected:
    virtual void run();
};

class SyncConnectComponent : public QObject, virtual public CPD3::ActionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::ActionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.sync_connect"
                              FILE
                              "sync_connect.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int actionAllowStations();

    virtual int actionRequireStations();

    CPD3::CPD3Action *createAction(const CPD3::ComponentOptions &options = {},
                                   const std::vector<std::string> &stations = {}) override;
};

#endif
