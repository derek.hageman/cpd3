/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/valueoptionparse.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/segment.hxx"
#include "datacore/sequencefilter.hxx"

#include "email_events.hxx"


Q_LOGGING_CATEGORY(log_email_events, "cpd3.email.events", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;

EmailEvents::EmailEvents() : profile(SequenceName::impliedProfile()),
                             stations(),
                             start(FP::undefined()),
                             end(FP::undefined())
{ }

EmailEvents::EmailEvents(const ComponentOptions &options,
                         double start,
                         double end,
                         const std::vector<SequenceName::Component> &setStations) : profile(
        SequenceName::impliedProfile()), stations(setStations), start(start), end(end)
{
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }
}

EmailEvents::~EmailEvents()
{
}

void EmailEvents::incomingData(const CPD3::Util::ByteView &)
{ externalNotify(); }

void EmailEvents::endData()
{ externalNotify(); }

namespace {
class EventMatcher {
    SequenceFilter filter;
    QRegExp sourceFilter;
    QRegExp textFilter;
    QString output;
public:
    EventMatcher() = default;

    EventMatcher(const EventMatcher &other) = default;

    EventMatcher &operator=(const EventMatcher &other) = default;

    EventMatcher(EventMatcher &&other) = default;

    EventMatcher &operator=(EventMatcher &&other) = default;


    EventMatcher(const Variant::Read &config, const SequenceName::Component &station)
            : filter(), sourceFilter(), textFilter(), output(config["Output"].toQString())
    {
        filter.configure(config["Filter"], {QString::fromStdString(station)}, {"events"});

        Qt::CaseSensitivity
                cs = config["CaseSensitive"].toBool() ? Qt::CaseSensitive : Qt::CaseInsensitive;
        if (config["Source"].exists()) {
            sourceFilter = QRegExp(config["Source"].toQString(), cs);
        } else {
            sourceFilter = QRegExp(".*");
        }
        if (config["Text"].exists()) {
            textFilter = QRegExp(config["Text"].toQString(), cs);
        } else {
            textFilter = QRegExp(".*");
        }
    }

    bool matches(const SequenceValue &value)
    {
        if (!filter.accept(value))
            return false;
        if (!sourceFilter.exactMatch(value.read()["Source"].toQString()))
            return false;
        return textFilter.exactMatch(value.read()["Text"].toQString());
    }

    inline QString getOutput() const
    { return output; }

    QStringList getTextCaptures() const
    { return textFilter.capturedTexts(); }
};
};

void EmailEvents::outputLiteral(const SequenceValue::Transfer &events,
                                const std::unordered_map<SequenceName::Component,
                                                         Variant::Read> &config)
{
    std::unordered_map<SequenceName::Component, std::vector<EventMatcher>> matchers;
    for (const auto &station : config) {
        auto stationConfig = station.second["Messages"];
        if (!stationConfig.exists()) {
            /* No default configuration */
#if 0
            Variant::Write temp = Variant::Write::empty();
            temp["#0/Filter/Variable"].setSting("acquisition");
            temp["#0/Source"].setString("EXTERNAL");
            temp["#0/Output"].setString("${TIME}:${EVENT|STRING|Author}: ${0}");
            temp["#1/Filter/Variable"].setSting("acquisition");
            temp["#1/Source"].setString("A\\d{2}");
            temp["#1/Text"].setString(".*filter change.*");
            temp["#1/Output"].setString("${TIME}:${SOURCE}: ${0}");
            stationConfig = temp;
#else
            continue;
#endif
        }

        switch (stationConfig.getType()) {
        case Variant::Type::Array: {
            for (auto add : stationConfig.toArray()) {
                matchers[station.first].emplace_back(add, station.first);
            }
            break;
        }
        default:
            matchers[station.first].emplace_back(stationConfig, station.first);
            break;
        }
    }
    if (matchers.empty())
        return;

    bool isFirst = true;
    for (const auto &event : events) {
        auto stationMatch = matchers.find(event.getStation());
        if (stationMatch == matchers.end())
            continue;
        auto hit = stationMatch->second.end();
        for (auto check = stationMatch->second.begin(), endCheck = stationMatch->second.end();
                check != endCheck;
                ++check) {
            if (check->matches(event)) {
                hit = check;
                break;
            }
        }
        if (hit == stationMatch->second.end())
            continue;

        QString output(hit->getOutput());
        if (output.isEmpty()) {
            output = event.read().hash("Text").toQString();
        } else {
            TextSubstitutionStack::Context subctx(substitutions);
            substitutions.setString("station", event.getStationQString().toLower());
            substitutions.setString("profile", QString::fromStdString(profile));
            substitutions.setTime("time", event.getStart());
            substitutions.setString("source", event.read()["Source"].toQString());
            QStringList caps(hit->getTextCaptures());
            for (int i = 0, max = caps.size(); i < max; i++) {
                substitutions.setString(QString::number(i), caps.at(i));
            }
            substitutions.setVariant("event", event.read());
            output = substitutions.apply(output);
        }

        if (output.isEmpty())
            continue;

        if (isFirst) {
            isFirst = false;
            Variant::Root addBreak;
            addBreak["Type"].setString("Break");
            messages.emplace_back(std::move(addBreak));
        }

        Variant::Root addMessage;
        addMessage["Type"].setString("Text");
        addMessage["Text"].setString(output);
        messages.emplace_back(std::move(addMessage));
    }
}

namespace {
struct SummaryData {
    QString message;
    QString countReplacement;
    int count;
};
}

void EmailEvents::outputSummary(const SequenceValue::Transfer &events,
                                const std::unordered_map<SequenceName::Component,
                                                         Variant::Read> &config)
{
    std::unordered_map<SequenceName::Component, std::vector<EventMatcher>> matchers;
    for (const auto &station : config) {
        auto stationConfig = station.second["Summary"];
        if (!stationConfig.exists()) {
            /* No default configuration */
#if 0
            Variant::Write temp = Variant::Write::empty();
            temp["#0/Filter/Variable"].setSting("acquisition");
            temp["#0/Text"].setString(".*comm.*(?:(?:dropped)|(?:lost)).*");
            temp["#0/Output"].setString("${SOURCE}: ${COUNT} Communications lost event(s)");
            stationConfig = temp;
#else
            continue;
#endif
        }

        switch (stationConfig.getType()) {
        case Variant::Type::Array: {
            for (auto add : stationConfig.toArray()) {
                matchers[station.first].emplace_back(add, station.first);
            }
            break;
        }
        default:
            matchers[station.first].emplace_back(stationConfig, station.first);
            break;
        }
    }
    if (matchers.empty())
        return;

    std::vector<SummaryData> summary;
    for (const auto &event : events) {
        auto stationMatch = matchers.find(event.getStation());
        if (stationMatch == matchers.end())
            continue;
        auto hit = stationMatch->second.end();
        for (auto check = stationMatch->second.begin(), endCheck = stationMatch->second.end();
                check != endCheck;
                ++check) {
            if (check->matches(event)) {
                hit = check;
                break;
            }
        }
        if (hit == stationMatch->second.end())
            continue;

        TextSubstitutionStack::Context subevent(substitutions);

        QString output(hit->getOutput());
        bool existing = false;
        if (output.isEmpty()) {
            output = event.read().hash("Text").toQString();
            for (auto &sd : summary) {
                if (sd.message == output) {
                    existing = true;
                    sd.count++;
                    break;
                }
            }
        } else {
            substitutions.setString("station", event.getStationQString().toLower());
            substitutions.setTime("time", event.getStart());
            substitutions.setString("source", event.read()["Source"].toQString());
            QStringList caps(hit->getTextCaptures());
            for (int i = 0, max = caps.size(); i < max; i++) {
                substitutions.setString(QString::number(i), caps.at(i));
            }

            for (auto &sd : summary) {
                TextSubstitutionStack::Context subsummary(substitutions);
                substitutions.setString("count", sd.countReplacement);
                substitutions.setVariant("event", event.read());
                QString check(substitutions.apply(output));
                if (sd.message == check) {
                    existing = true;
                    sd.count++;
                    break;
                }
            }
        }
        if (existing)
            continue;

        QString toReplace;
        for (int itter = 0; itter < 1000; itter++) {
            toReplace = "!@";
            toReplace.append(Random::string());
            if (!output.contains(toReplace))
                break;
        }

        substitutions.setString("count", toReplace);
        substitutions.setVariant("event", event.read());
        output = substitutions.apply(output);

        if (output.isEmpty())
            continue;

        SummaryData add;
        add.message = output;
        add.countReplacement = toReplace;
        add.count = 1;
        summary.emplace_back(std::move(add));
    }

    if (summary.empty())
        return;

    bool isFirst = true;
    for (const auto &sd : summary) {
        QString messageData(sd.message);
        messageData.replace(sd.countReplacement, QString::number(sd.count));

        if (messageData.isEmpty())
            continue;

        if (isFirst) {
            isFirst = false;
            Variant::Root addBreak;
            addBreak["Type"].setString("Break");
            messages.emplace_back(std::move(addBreak));
        }

        Variant::Root addMessage;
        addMessage["Type"].setString("Text");
        addMessage["Text"].setString(messageData);
        messages.emplace_back(std::move(addMessage));
    }
}

bool EmailEvents::begin()
{
    std::unordered_map<SequenceName::Component, Variant::Read> eventConfiguration;
    SequenceValue::Transfer allEvents;

    do {
        Archive::Access access;
        Archive::Access::ReadLock lock(access, true);

        auto allStations = access.availableStations(stations);
        if (allStations.empty()) {
            qCDebug(log_email_events) << "No stations available";
            return false;
        }
        stations.clear();
        std::copy(allStations.begin(), allStations.end(), Util::back_emplacer(stations));

        double tnow = Time::time();
        SequenceSegment configSegment;
        {
            auto confList = SequenceSegment::Stream::read(
                    Archive::Selection(tnow - 1.0, tnow + 1.0, stations, {"configuration"},
                                       {"email"}), &access);
            auto f = Range::findIntersecting(confList, tnow);
            if (f == confList.end())
                break;
            configSegment = *f;
        }

        for (const auto &station : stations) {
            SequenceName stationUnit(station, "configuration", "email");
            auto config = configSegment.takeValue(stationUnit).read().hash("Events").hash(profile);
            if (!config.exists())
                continue;
            config.detachFromRoot();

            eventConfiguration.emplace(station, std::move(config));
        }

        if (isTerminated())
            break;

        Archive::Selection::List selections;
        selections.push_back(Archive::Selection(start, end, stations, {"events"}, {}));

        for (auto &m : selections) {
            m.includeMetaArchive = false;
            m.includeDefaultStation = false;
        }
        allEvents = access.readSynchronous(selections);
        qCDebug(log_email_events) << "Loaded" << allEvents.size() << "event(s)";
    } while (false);

    if (isTerminated())
        return false;

    substitutions.clear();
    substitutions.setString("profile", QString::fromStdString(profile));
    substitutions.setTime("start", start);
    substitutions.setTime("end", end);

    outputLiteral(allEvents, eventConfiguration);
    if (isTerminated())
        return false;
    outputSummary(allEvents, eventConfiguration);

    return true;
}

bool EmailEvents::prepare()
{ return true; }

bool EmailEvents::process()
{
    SequenceValue::Transfer toOutput;
    double tnow = Time::time();
    qCDebug(log_email_events) << "Generated" << messages.size() << "message(s)";
    int priority = 0;
    for (auto &v : messages) {
        toOutput.emplace_back(
                SequenceIdentity({"message", "message", "message"}, tnow, tnow, priority++),
                std::move(v));
    }

    messages.clear();
    outputData(toOutput);
    return false;
}


ComponentOptions EmailEventsComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Execution profile"),
                                                tr("This is the profile name to execute.  Multiple profiles can be "
                                                   "defined to specify different output types for the same station.  "
                                                   "Consult the station configuration for available profiles."),
                                                tr("aerosol")));

    return options;
}

QList<ComponentExample> EmailEventsComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Generate output events", "default example name"),
                                     tr("This will generate the events for the \"aerosol\" profile.")));

    return examples;
}

int EmailEventsComponent::ingressAllowStations()
{ return INT_MAX; }

bool EmailEventsComponent::ingressRequiresTime()
{ return true; }

ExternalConverter *EmailEventsComponent::createExternalIngress(const ComponentOptions &options,
                                                               double start,
                                                               double end,
                                                               const std::vector<
                                                                       SequenceName::Component> &stations)
{ return new EmailEvents(options, start, end, stations); }
