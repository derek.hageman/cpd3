/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/externalsource.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Data {
namespace Variant {
bool operator==(const Root &a, const Root &b)
{ return a.read() == b.read(); }
}
}
}


class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    ExternalSourceComponent *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ExternalSourceComponent *>(
                ComponentLoader::create("email_events"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("profile")));
    }

    void basic()
    {
        Variant::Root v;
        v["Events/aerosol/Messages/#0/Filter/Variable"].setString("acquisition");
        v["Events/aerosol/Messages/#0/Source"].setString("EXTERNAL");
        v["Events/aerosol/Messages/#0/Output"].setString("${TIME}:${EVENT|STRING|Author}: ${0}");
        v["Events/aerosol/Summary/#0/Filter/Variable"].setString("acquisition");
        v["Events/aerosol/Summary/#0/Text"].setString(".*comm.*(?:(?:dropped)|(?:lost)).*");
        v["Events/aerosol/Summary/#0/Output"].setString(
                "${SOURCE}: ${COUNT} Communications lost event(s)");

        SequenceValue::Transfer toAdd;
        toAdd.push_back(SequenceValue({"sfa", "configuration", "email"}, v, FP::undefined(),
                                      FP::undefined()));

        v.write().setEmpty();
        v["Text"].setString("Some test message");
        v["Author"].setString("DCH");
        v["Source"].setString("EXTERNAL");
        toAdd.push_back(
                SequenceValue({"sfa", "events", "acquisition"}, v, 1368544380.0, 1368544380.0));

        v.write().setEmpty();
        v["Text"].setString("Some other test message");
        v["Author"].setString("DCH");
        v["Source"].setString("EXTERNAL");
        toAdd.push_back(
                SequenceValue({"sfa", "events", "acquisition"}, v, 1368544381.0, 1368544381.0));

        v.write().setEmpty();
        v["Text"].setString("Communications lost");
        v["Source"].setString("A11");
        toAdd.push_back(
                SequenceValue({"sfa", "events", "acquisition"}, v, 1368544382.0, 1368544382.0));
        toAdd.push_back(
                SequenceValue({"sfa", "events", "acquisition"}, v, 1368544383.0, 1368544383.0));

        Archive::Access(databaseFile).writeSynchronous(toAdd);

        ComponentOptions options(component->getOptions());
        ExternalConverter *input = component->createExternalIngress(options);
        QVERIFY(input != NULL);
        input->start();

        StreamSink::Buffer ingress;
        input->setEgress(&ingress);

        QVERIFY(input->wait());
        delete input;

        QVERIFY(ingress.ended());
        std::vector<Variant::Root> outputValues;
        for (const auto &dv : ingress.values()) {
            outputValues.emplace_back(dv.root());
        }

        std::vector<Variant::Root> values;
        v.write().setEmpty();
        v["Type"].setString("Break");
        values.emplace_back(v);

        v.write().setEmpty();
        v["Type"].setString("Text");
        v["Text"].setString("2013-05-14T15:13:00Z:DCH: Some test message");
        values.emplace_back(v);

        v.write().setEmpty();
        v["Type"].setString("Text");
        v["Text"].setString("2013-05-14T15:13:01Z:DCH: Some other test message");
        values.emplace_back(v);

        v.write().setEmpty();
        v["Type"].setString("Break");
        values.emplace_back(v);

        v.write().setEmpty();
        v["Type"].setString("Text");
        v["Text"].setString("A11: 2 Communications lost event(s)");
        values.emplace_back(v);

        QCOMPARE(outputValues, values);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
