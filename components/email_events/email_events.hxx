/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef EMAILEVENTS_H
#define EMAILEVENTS_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>
#include <QStringList>

#include "core/component.hxx"
#include "core/textsubstitution.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/variant/composite.hxx"

class EmailEvents : public CPD3::Data::ExternalSourceProcessor {
    std::string profile;
    std::vector<CPD3::Data::SequenceName::Component> stations;
    double start;
    double end;

    CPD3::Data::Variant::Composite::SubstitutionStack substitutions;

    std::vector<CPD3::Data::Variant::Root> messages;

    void outputLiteral(const CPD3::Data::SequenceValue::Transfer &events,
                       const std::unordered_map<CPD3::Data::SequenceName::Component,
                                                CPD3::Data::Variant::Read> &config);

    void outputSummary(const CPD3::Data::SequenceValue::Transfer &events,
                       const std::unordered_map<CPD3::Data::SequenceName::Component,
                                                CPD3::Data::Variant::Read> &config);

public:
    EmailEvents();

    EmailEvents(const CPD3::ComponentOptions &options,
                double start,
                double end,
                const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~EmailEvents();

    void incomingData(const CPD3::Util::ByteView &data) override;

    void endData() override;

protected:
    bool prepare() override;

    bool process() override;

    bool begin() override;
};

class EmailEventsComponent : public QObject, virtual public CPD3::Data::ExternalSourceComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalSourceComponent)


    Q_PLUGIN_METADATA(IID
                              "CPD3.email_events"
                              FILE
                              "email_events.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    QList<CPD3::ComponentExample> getExamples() override;

    int ingressAllowStations() override;

    bool ingressRequiresTime() override;

    CPD3::Data::ExternalConverter *createExternalIngress(const CPD3::ComponentOptions &options,
                                                         double start,
                                                         double end,
                                                         const std::vector<
                                                                 CPD3::Data::SequenceName::Component> &stations = {}) override;
};

#endif
