/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIRERRPSAP1W_H
#define ACQUIRERRPSAP1W_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"
#include "smoothing/baseline.hxx"

class AcquireRRPSAP1W : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Waiting for enough valid records to confirm autoprobe success */
        RESP_AUTOPROBE_WAIT,

        /* Waiting for a valid record to start acquisition */
        RESP_WAIT,

        /* Acquiring data in passive mode */
        RESP_RUN,

        /* Same as wait, but discard the first timeout */
        RESP_INITIALIZE,

        /* Same as initialize but autoprobing */
        RESP_AUTOPROBE_INITIALIZE,
    };
    ResponseState responseState;
    int autoprobeValidRecords;

    enum SampleState {
        /* Normal operation */
                SAMPLE_RUN,

        /* Sampling on a filter but waiting for the normalization */
                SAMPLE_FILTER_NORMALIZE, /* Filter currently changing */
                SAMPLE_FILTER_CHANGING,

        /* Sampling on a white filter but waiting for the normalization */
                SAMPLE_WHITE_FILTER_NORMALIZE, /* Changing a white filter */
                SAMPLE_WHITE_FILTER_CHANGING,

        /* A filter change is required */
                SAMPLE_REQUIRE_FILTER_CHANGE, /* A white filter change is required */
                SAMPLE_REQUIRE_WHITE_FILTER_CHANGE,
    };
    SampleState sampleState;

    class Configuration {
        double start;
        double end;
    public:
        CPD3::Calibration flowCalibration;
        double area;
        double wavelength;
        bool strictMode;
        double reportInterval;

        bool useMeasuredTime;

        bool enableNormalizationRecovery;
        bool enableAutodetectStart;
        bool enableAutodetectEnd;

        double verifyNormalizationBand;
        double verifyWhiteBand;


        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    class IntensityData {
    public:
        double startTime;
        double Ip;
        double If;
        double In;

        IntensityData();
    };

    friend QDataStream &operator<<(QDataStream &stream, const IntensityData &data);

    friend QDataStream &operator>>(QDataStream &stream, IntensityData &data);

    /* State that needs to be saved */
    double accumulatedVolume;
    double filterNormalization;
    bool filterIsNotWhite;
    double normalizationStartTime;
    IntensityData normalizationLatest;
    IntensityData filterStart;
    IntensityData filterWhite;

    bool haveAcceptedNormalization;
    double whiteFilterChangeStartTime;

    double priorIr;
    double priorIn;

    CPD3::Data::Variant::Root instrumentMeta;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool forceRealtimeStateEmit;

    bool forceFilterBreak;
    bool requireFilterBreak;

    CPD3::Data::DynamicTimeInterval *acceptNormalizationTime;
    CPD3::Smoothing::BaselineSmoother *filterNormalizeIp;
    CPD3::Smoothing::BaselineSmoother *filterNormalizeIf;
    CPD3::Smoothing::BaselineSmoother *filterNormalizeIn;
    CPD3::Smoothing::BaselineSmoother *filterChangeIf;
    CPD3::Smoothing::BaselineSmoother *filterChangeIrc;

    double lastTransmittanceCheckTime;
    double transmittanceWarningThreshold;

    void setDefaultInvalid();

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    CPD3::Data::Variant::Root buildFilterValueMeta() const;

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    CPD3::Data::Variant::Root buildFilterValue(const IntensityData &data) const;

    void emitFilterEnd(double endTime);

    void emitWhiteEnd(double endTime);

    void emitNormalizationEnd(double endTime);

    bool isSameNormalization(double currentTime, const CPD3::Data::Variant::Read &If) const;

    bool autodetectStart() const;

    bool autodetectEnd() const;

    bool normalizationDone() const;

    bool isWhiteFilter() const;

    bool whiteFilterTimeout(double time) const;

    void resetSmoothers();

    void insertDiagnosticFilterValues(CPD3::Data::Variant::Write &target,
                                      const QString &name,
                                      const IntensityData &source) const;

    void insertDiagnosticNormalization(CPD3::Data::Variant::Write &target) const;

    void insertDiagnosticFilterNormalization(CPD3::Data::Variant::Write &target) const;

    CPD3::Data::Variant::Root buildNormalizationValue() const;

    void configurationChanged();

    void configAdvance(double frameTime);

    int processRecord(const CPD3::Util::ByteView &line, double &frameTime);

public:
    AcquireRRPSAP1W(const CPD3::Data::ValueSegment::Transfer &config,
                    const std::string &loggingContext);

    AcquireRRPSAP1W(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireRRPSAP1W();

    virtual CPD3::Data::SequenceValue::Transfer getPersistentValues();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    CPD3::Data::Variant::Root getCommands() override;

    virtual CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    void serializeState(QDataStream &stream) override;

    void deserializeState(QDataStream &stream) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    virtual void command(const CPD3::Data::Variant::Read &value);

    virtual void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingInstrumentTimeout(double time);

    virtual void discardDataCompleted(double time);
};

class AcquireRRPSAP1WComponent
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_rr_psap1w"
                              FILE
                              "acquire_rr_psap1w.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
