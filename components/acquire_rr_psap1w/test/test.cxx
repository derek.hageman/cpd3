/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"
#include "smoothing/baseline.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Smoothing;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;
    QDateTime time;
    double accumulatedTime;

    double sample;
    double dSample;
    double reference;
    double dReference;
    double Q;

    double In0;

    ModelInstrument() : unpolledRemaining(1),
                        time(QDate(2013, 2, 3), QTime(0, 0, 0)),
                        accumulatedTime(0.0)
    {
        sample = 80000.0;
        dSample = -2;
        reference = 100000.0;
        dReference = -0.001;
        Q = 0.9;

        In0 = sample / reference;
    }

    void advance(double seconds)
    {
        time = time.addMSecs((qint64) qRound(seconds * 1000.0));

        unpolledRemaining -= seconds;
        while (unpolledRemaining <= 0.0) {
            unpolledRemaining += 1.0;
            accumulatedTime += 1.0;

            outgoing.append(QByteArray::number(time.date().year() % 100).rightJustified(2, '0'));
            outgoing.append(' ');
            outgoing.append(QByteArray::number(time.date().month()).rightJustified(2, '0'));
            outgoing.append(' ');
            outgoing.append(QByteArray::number(time.date().day()).rightJustified(2, '0'));
            outgoing.append(' ');
            outgoing.append(QByteArray::number(time.time().hour()).rightJustified(2, '0'));
            outgoing.append(' ');
            outgoing.append(QByteArray::number(time.time().minute()).rightJustified(2, '0'));
            outgoing.append(' ');
            outgoing.append(QByteArray::number(time.time().second()).rightJustified(2, '0'));
            outgoing.append("   1 ");
            outgoing.append(
                    QByteArray::number(internalBa(accumulatedTime - 1.0, accumulatedTime), 'f',
                                       1).rightJustified(6, ' '));
            outgoing.append(' ');
            outgoing.append(QByteArray::number(internalIr(), 'f', 3).rightJustified(5, ' '));
            outgoing.append(' ');
            outgoing.append(QByteArray::number(Q, 'f', 2).rightJustified(5, ' '));
            outgoing.append(' ');
            outgoing.append(QByteArray::number(expectedIp(), 'f', 0).rightJustified(7, ' '));
            outgoing.append(' ');
            outgoing.append(QByteArray::number(expectedIf(), 'f', 0).rightJustified(7, ' '));
            outgoing.append(' ');

            outgoing.append('\r');
        }
    }

    double expectedIp(double atTime = FP::undefined()) const
    {
        if (!FP::defined(atTime))
            atTime = accumulatedTime;
        return floor(sample + atTime * dSample);
    }

    double expectedIf(double atTime = FP::undefined()) const
    {
        if (!FP::defined(atTime))
            atTime = accumulatedTime;
        return floor(reference + atTime * dReference);
    }

    double internalBa(double previous, double atTime = FP::undefined(), double area = 17.83) const
    {
        if (!FP::defined(atTime))
            atTime = accumulatedTime;
        double In0 = expectedIp(previous) / expectedIf(previous);
        double In1 = expectedIp(atTime) / expectedIf(atTime);
        double dQt = Q * (atTime - previous) / 60000;
        return (area / dQt) * log(In0 / In1);
    }

    double internalIr(double atTime = FP::undefined()) const
    {
        if (!FP::defined(In0) || In0 == 0.0)
            return 1.0;
        double In1 = expectedIp(atTime) / expectedIf(atTime);
        return In1 / In0;
    }

    double expectedIn(double atTime = FP::undefined())
    {
        double IRef = expectedIf(atTime);
        double ISam = expectedIp(atTime);
        if (IRef == 0.0) return FP::undefined();
        return ISam / IRef;
    }

    double expectedIr(double t0, double t1)
    {
        double In0 = expectedIn(t0);
        double In = expectedIn(t1);
        if (!FP::defined(In0) || In0 == 0.0) return FP::undefined();
        return In / In0;
    }

    double expectedQt(double atTime = FP::undefined(), const Calibration &cal = Calibration())
    {
        if (!FP::defined(atTime))
            atTime = accumulatedTime;
        return cal.apply(Q) * atTime / 60000.0;
    }

    double expectedL(double atTime = FP::undefined(),
                     const Calibration &cal = Calibration(),
                     double area = 17.83)
    {
        if (!FP::defined(atTime))
            atTime = accumulatedTime;
        return expectedQt(atTime, cal) / (area * 1E-6);
    }

    double expectedBa(double t0,
                      double t1,
                      const Calibration &cal = Calibration(),
                      double area = 17.83)
    {
        if (!FP::defined(t0))
            t0 = 0.0;
        if (!FP::defined(t1))
            t1 = accumulatedTime;
        double dQt = cal.apply(Q) * (t1 - t0) / 60000.0;
        double In0 = expectedIn(t0);
        double In1 = expectedIn(t1);
        if (dQt <= 0.0 || !FP::defined(In0) || !FP::defined(In1) || In0 <= 0.0 || In1 <= 0.0)
            return FP::undefined();
        return (area / dQt) * log(In0 / In1);
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Ff", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Qt", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("L", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("IrG", Variant::Root(574.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BaG", Variant::Root(574.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IfG", Variant::Root(574.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IpG", Variant::Root(574.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("ZSPOT", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZWHITE", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZFILTER", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZQ", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("InG", Variant::Root(574.0), "^Wavelength", time))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream, ModelInstrument &instrument,
                     double t0,
                     double t1,
                     bool isRealtime = false,
                     double tNormalize = 0.0,
                     const Calibration &calQ = Calibration())
    {

        QList<double> times;
        QList<double> timesBa;
        QList<double> Q;
        QList<double> Qt;
        QList<double> L;
        QList<double> IrG;
        QList<double> BaG;
        QList<double> IfG;
        QList<double> IpG;
        for (double t = t0; t <= t1; t += 1.0) {
            if (!isRealtime)
                times.append(t - 1.0);
            else
                times.append(t);

            Q.append(calQ.apply(instrument.Q));
            Qt.append(instrument.expectedQt(t - tNormalize, calQ));
            L.append(instrument.expectedL(t - tNormalize, calQ));
            if (t >= tNormalize) {
                if (!isRealtime)
                    timesBa.append(t - 1.0);
                else
                    timesBa.append(t);

                IrG.append(instrument.expectedIr(tNormalize + 1.0, t));
                BaG.append(instrument.expectedBa(t - 1.0, t, calQ));
            }
            IfG.append(instrument.expectedIf(t));
            IpG.append(instrument.expectedIp(t));
        }

        if (!stream.verifyValues("Q", times, Q, !isRealtime, 1E-8))
            return false;
        if (!stream.verifyValues("Qt", times, Qt, !isRealtime, 1E-8))
            return false;
        if (!stream.verifyValues("L", times, L, !isRealtime, 1E-8))
            return false;
        if (!stream.verifyValues("IfG", times, IfG, !isRealtime, 1E-8))
            return false;
        if (!stream.verifyValues("IpG", times, IpG, !isRealtime, 1E-8))
            return false;
        if (!stream.verifyValues("IrG", timesBa, IrG, !isRealtime, 1E-8))
            return false;
        if (!stream.verifyValues("BaG", timesBa, BaG, !isRealtime, 1E-8))
            return false;

        if (!isRealtime)
            return true;

        QList<double> InG;
        for (double t = t0; t <= t1; t += 1.0) {
            InG.append(instrument.expectedIn(t));
        }

        if (!stream.verifyValues("InG", times, InG, !isRealtime, 1E-8))
            return false;

        return true;
    }

    bool checkState(StreamCapture &stream, ModelInstrument &instrument,
                    double time = FP::undefined())
    {
        Q_UNUSED(instrument);
        if (!stream.hasValue("Ff", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasValue("ZSTATE", Variant::Root(), QString(), time))
            return false;

        return true;
    }

    bool checkPersistent(const SequenceValue::Transfer &values, ModelInstrument &instrument)
    {
        Q_UNUSED(instrument);
        if (!StreamCapture::findValue(values, "raw", "Ff", Variant::Root()))
            return false;
        return true;
    }

    bool checkSpot(const SequenceValue::Transfer &values, double time, double area = 17.83)
    {
        if (!StreamCapture::findValue(values, "raw", "ZSPOT", Variant::Root(area), "Area", time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_rr_psap1w"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_rr_psap1w"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("area")));
        QVERIFY(qobject_cast<ComponentOptionSingleCalibration *>(options.get("cal-q")));
        QVERIFY(qobject_cast<BaselineSmootherOption *>(options.get("filter-normalize")));
        QVERIFY(qobject_cast<BaselineSmootherOption *>(options.get("filter-change")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("disable-recovery")));
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["StrictMode"].setBool(true);
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 10; i++) {
            control.advance(1.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 20; i++) {
            control.advance(1.0);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 8.0, 13.0, false));
        QVERIFY(checkValues(realtime, instrument, 8.0, 13.0, true));
        QVERIFY(checkState(realtime, instrument));

        QVERIFY(checkPersistent(realtime.values(), instrument));
        QVERIFY(checkPersistent(persistentValues, instrument));
        QVERIFY(checkSpot(persistentValues, 1.0));

    }

    void interactiveAutoprobe()
    {
        static const Calibration calQ(QVector<double>() << 0.01 << 1.05);
        ValueSegment::Transfer config;
        Variant::Root cv;
        Variant::Composite::fromCalibration(cv["FlowCalibration"], calQ);
        cv["StrictMode"].setBool(false);
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(1.0);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        double loggingStart = instrument.accumulatedTime;
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, loggingStart + 1.0, instrument.accumulatedTime,
                            false, 1.0, calQ));
        QVERIFY(checkValues(realtime, instrument, loggingStart + 1.0, instrument.accumulatedTime,
                            true, 1.0, calQ));
        QVERIFY(checkState(realtime, instrument));

        QVERIFY(checkPersistent(realtime.values(), instrument));
        QVERIFY(checkPersistent(persistentValues, instrument));
        QVERIFY(checkSpot(persistentValues, 2.0));

    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 2.0, 10.0, false));
        QVERIFY(checkValues(realtime, instrument, 2.0, 10.0, true));
        QVERIFY(checkState(realtime, instrument));

        QVERIFY(checkPersistent(realtime.values(), instrument));
        QVERIFY(checkPersistent(persistentValues, instrument));
        QVERIFY(checkSpot(persistentValues, 1.0));

    }

    void passiveAcquisitionOptions()
    {
        static const Calibration calQ(QVector<double>() << -0.02 << 0.9);
        ComponentOptions options(component->getPassiveOptions());
        qobject_cast<ComponentOptionSingleCalibration *>(options.get("cal-q"))->set(calQ);
        qobject_cast<BaselineSmootherOption *>(options.get("filter-normalize"))->overlay(
                new BaselineSinglePoint, FP::undefined(), FP::undefined());

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(options));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 2.0, 10.0, false, 0.0, calQ));
        QVERIFY(checkValues(realtime, instrument, 2.0, 10.0, true, 0.0, calQ));
        QVERIFY(checkState(realtime, instrument));

        QVERIFY(checkPersistent(realtime.values(), instrument));
        QVERIFY(checkPersistent(persistentValues, instrument));
        QVERIFY(checkSpot(persistentValues, 1.0));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        for (int i = 0; i < 15; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 2.0, 10.0, false));
        QVERIFY(checkValues(realtime, instrument, 2.0, 10.0, true));
        QVERIFY(checkState(realtime, instrument));

        QVERIFY(checkPersistent(realtime.values(), instrument));
        QVERIFY(checkPersistent(persistentValues, instrument));
        QVERIFY(checkSpot(persistentValues, 1.0));

    }

    void autodetectFilterChange()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Autodetect/Smoother/Type"].setString("FixedTime");
        cv["Autodetect/Smoother/Time"].setDouble(5.0);
        cv["Autodetect/Smoother/RSD"].setDouble(0.01);
        cv["Autodetect/Smoother/Band"].setDouble(2.0);
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(true);
        cv["Autodetect/End/Enable"].setBool(true);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.dReference = 1;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        control.advance(1.0);

        for (double dTE = control.time() + 6.0; control.time() <= dTE;) {
            control.advance(1.0);
        }
        instrument.reference = 2000;
        while (control.time() < 120.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        control.advance(1.0);

        double filterChangeEndTime = control.time();
        instrument.reference = 100000;
        instrument.dReference = -0.005;
        while (control.time() < 180.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), filterChangeEndTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkPersistent(persistentValues, instrument));
        QVERIFY(persistent.hasValue("Ff"));
        QVERIFY(StreamCapture::findValue(persistentValues, "raw", "ZFILTER", Variant::Root()));

    }

    void commandIssue()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        control.advance(1.0);

        double checkTime1 = control.time();
        Variant::Write cmd = Variant::Write::empty();
        cmd["StartFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 120.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        control.advance(1.0);

        double checkTime2 = control.time();
        cmd.setEmpty();
        cmd["StopFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 180.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime1))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);
        control.advance(1.0);

        cmd.setEmpty();
        cmd["StartFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 240.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange"), checkTime2))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        control.advance(1.0);

        checkTime1 = control.time();
        cmd.setEmpty();
        cmd["StartWhiteFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 300.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("WhiteFilterChange")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 300.0);
        control.advance(1.0);

        checkTime2 = control.time();
        cmd.setEmpty();
        cmd["StopFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 360.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime1))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 360.0);
        control.advance(1.0);

        checkTime1 = control.time();
        cmd.setEmpty();
        cmd["StartWhiteFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 420.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("WhiteFilterChange"),
                                             checkTime2))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 420.0);
        control.advance(1.0);

        cmd.setEmpty();
        cmd["StopFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 480.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime1))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 480.0);
        control.advance(1.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkPersistent(persistentValues, instrument));

    }

    void stateSaveRestore()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);

        ElapsedTimer timeoutCheck;
        timeoutCheck.start();
        for (double tCheck = realtime.latestTime();
                timeoutCheck.elapsed() < 30000 && (!FP::defined(tCheck) || tCheck < 4.0);
                tCheck = realtime.latestTime()) {
            QTest::qSleep(50);
        }

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface = component->createAcquisitionPassive(config);
            control.attach(interface.get());
            interface->start();
            interface->setLoggingEgress(&logging);
            interface->setRealtimeEgress(&realtime);
            interface->setPersistentEgress(&persistent);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }
        }

        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 2.0, 4.0, false));
        QVERIFY(checkValues(realtime, instrument, 2.0, 4.0, true));
        QVERIFY(checkValues(logging, instrument, 6.0, 10.0, false));
        QVERIFY(checkValues(realtime, instrument, 6.0, 10.0, true));
        QVERIFY(checkState(realtime, instrument));

        QVERIFY(checkPersistent(realtime.values(), instrument));
        QVERIFY(checkPersistent(persistentValues, instrument));
        QVERIFY(checkSpot(persistentValues, 1.0));

    }

    void resumeLogic()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.dSample = 0;
        instrument.dReference = 0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        double checkTime = control.time();
        control.advance(1.0);

        Variant::Write cmd = Variant::Write::empty();
        cmd["StartWhiteFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 120.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("WhiteFilterChange"),
                                             checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        checkTime = control.time();
        control.advance(1.0);

        cmd.setEmpty();
        cmd["StopFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 180.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);
        checkTime = control.time();
        control.advance(1.0);

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface = component->createAcquisitionInteractive(config);
            control.attach(interface.get());
            interface->setState(&control);
            interface->setControlStream(&control);
            interface->start();
            interface->setLoggingEgress(&logging);
            interface->setRealtimeEgress(&realtime);
            interface->setPersistentEgress(&persistent);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }
        }

        while (control.time() < 240.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        checkTime = control.time();
        control.advance(1.0);

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface = component->createAcquisitionInteractive(config);
            control.attach(interface.get());
            interface->setState(&control);
            interface->setControlStream(&control);
            interface->start();
            interface->setLoggingEgress(&logging);
            interface->setRealtimeEgress(&realtime);
            interface->setPersistentEgress(&persistent);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }
        }

        instrument.reference *= 0.5;
        while (control.time() < 300.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 300.0);
        checkTime = control.time();
        control.advance(1.0);

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface.reset();

            cv["Filter/EnableNormalizationRecovery"].setBool(false);
            config.clear();
            config.emplace_back(FP::undefined(), FP::undefined(), cv);
            interface = component->createAcquisitionInteractive(config);
            control.attach(interface.get());
            interface->setState(&control);
            interface->setControlStream(&control);
            interface->start();
            interface->setLoggingEgress(&logging);
            interface->setRealtimeEgress(&realtime);
            interface->setPersistentEgress(&persistent);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }
        }

        instrument.reference *= 3.0;
        while (control.time() < 360.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("RequireFilterChange"),
                                             checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 360.0);
        checkTime = control.time();
        control.advance(1.0);

        cmd.setEmpty();
        cmd["StartFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 420.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 420.0);
        checkTime = control.time();
        control.advance(1.0);

        cmd.setEmpty();
        cmd["StopFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 480.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 480.0);
        control.advance(1.0);

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkPersistent(persistentValues, instrument));

    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
