/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "datacore/wavelength.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_rr_psap1w.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Smoothing;

AcquireRRPSAP1W::Configuration::Configuration() : start(FP::undefined()),
                                                  end(FP::undefined()),
                                                  flowCalibration(),
                                                  area(17.83),
                                                  wavelength(574),
                                                  strictMode(true),
                                                  reportInterval(1.0),
                                                  useMeasuredTime(false),
                                                  enableNormalizationRecovery(true),
                                                  enableAutodetectStart(true),
                                                  enableAutodetectEnd(true),
                                                  verifyNormalizationBand(0.3),
                                                  verifyWhiteBand(0.9)
{ }

AcquireRRPSAP1W::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          flowCalibration(other.flowCalibration),
          area(other.area),
          wavelength(other.wavelength),
          strictMode(other.strictMode),
          reportInterval(other.reportInterval),
          useMeasuredTime(other.useMeasuredTime),
          enableNormalizationRecovery(other.enableNormalizationRecovery),
          enableAutodetectStart(other.enableAutodetectStart),
          enableAutodetectEnd(other.enableAutodetectEnd),
          verifyNormalizationBand(other.verifyNormalizationBand),
          verifyWhiteBand(other.verifyWhiteBand)
{ }

AcquireRRPSAP1W::IntensityData::IntensityData() : startTime(FP::undefined()),
                                                  Ip(FP::undefined()),
                                                  If(FP::undefined()),
                                                  In(FP::undefined())
{ }

QDataStream &operator<<(QDataStream &stream, const AcquireRRPSAP1W::IntensityData &data)
{
    stream << data.startTime << data.Ip << data.If << data.In;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, AcquireRRPSAP1W::IntensityData &data)
{
    stream >> data.startTime >> data.Ip >> data.If >> data.In;
    return stream;
}

void AcquireRRPSAP1W::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("RadianceResearch");
    instrumentMeta["Model"].setString("PSAP-1W");

    accumulatedVolume = FP::undefined();
    filterNormalization = FP::undefined();
    filterIsNotWhite = false;
    normalizationStartTime = FP::undefined();

    lastTransmittanceCheckTime = FP::undefined();
    transmittanceWarningThreshold = 0.002;

    haveAcceptedNormalization = true;
    whiteFilterChangeStartTime = FP::undefined();

    priorIr = FP::undefined();
    priorIn = FP::undefined();

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    forceRealtimeStateEmit = true;

    forceFilterBreak = false;
    requireFilterBreak = false;
}

AcquireRRPSAP1W::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          flowCalibration(),
          area(17.83),
          wavelength(574),
          strictMode(true),
          reportInterval(1.0),
          useMeasuredTime(false),
          enableNormalizationRecovery(true),
          enableAutodetectStart(true),
          enableAutodetectEnd(true),
          verifyNormalizationBand(0.3),
          verifyWhiteBand(0.9)
{
    setFromSegment(other);
}

AcquireRRPSAP1W::Configuration::Configuration(const Configuration &under,
                                              const ValueSegment &over,
                                              double s,
                                              double e) : start(s),
                                                          end(e),
                                                          flowCalibration(under.flowCalibration),
                                                          area(under.area),
                                                          wavelength(under.wavelength),
                                                          strictMode(under.strictMode),
                                                          reportInterval(under.reportInterval),
                                                          useMeasuredTime(under.useMeasuredTime),
                                                          enableNormalizationRecovery(
                                                                  under.enableNormalizationRecovery),
                                                          enableAutodetectStart(
                                                                  under.enableAutodetectStart),
                                                          enableAutodetectEnd(
                                                                  under.enableAutodetectEnd),
                                                          verifyNormalizationBand(
                                                                  under.verifyNormalizationBand),
                                                          verifyWhiteBand(under.verifyWhiteBand)
{
    setFromSegment(over);
}

void AcquireRRPSAP1W::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["FlowCalibration"].exists())
        flowCalibration = Variant::Composite::toCalibration(config["FlowCalibration"]);

    if (config["UseMeasuredTime"].exists())
        useMeasuredTime = config["UseMeasuredTime"].toBool();

    if (FP::defined(config["Area/#0"].toDouble())) {
        area = config["Area/#0"].toDouble();
        if (FP::defined(area) && area < 0.1)
            area *= 1E6;
    }
    if (FP::defined(config["Area"].toDouble())) {
        area = config["Area"].toDouble();
        if (FP::defined(area) && area < 0.1)
            area *= 1E6;
    }

    if (FP::defined(config["Wavelength/G"].toDouble()))
        wavelength = config["Wavelength/G"].toDouble();
    if (FP::defined(config["Wavelength"].toDouble()))
        wavelength = config["Wavelength"].toDouble();

    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    {
        double v = config["ReportInterval"].toDouble();
        if (FP::defined(v) && v > 0.0)
            reportInterval = v;
    }

    if (config["Autodetect/Start/Enable"].exists())
        enableAutodetectStart = config["Autodetect/Start/Enable"].toBool();
    if (config["Autodetect/End/Enable"].exists())
        enableAutodetectEnd = config["Autodetect/End/Enable"].toBool();

    if (config["Filter/EnableNormalizationRecovery"].exists())
        enableNormalizationRecovery = config["Filter/EnableNormalizationRecovery"].toBool();
    if (config["Filter/VerifyWhiteBand"].exists())
        verifyWhiteBand = config["Filter/VerifyWhiteBand"].toDouble();
    if (config["Filter/VerifyNormalizationBand"].exists())
        verifyNormalizationBand = config["Filter/VerifyNormalizationBand"].toDouble();
}

AcquireRRPSAP1W::AcquireRRPSAP1W(const ValueSegment::Transfer &configData,
                                 const std::string &loggingContext) : FramedInstrument("psap1w",
                                                                                       loggingContext),
                                                                      lastRecordTime(
                                                                              FP::undefined()),
                                                                      autoprobeStatus(
                                                                              AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                      responseState(RESP_WAIT),
                                                                      autoprobeValidRecords(0),
                                                                      sampleState(
                                                                              SAMPLE_FILTER_NORMALIZE)
{
    setDefaultInvalid();
    config.append(Configuration());

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }

    Variant::Write defaultSmoother = Variant::Write::empty();
    defaultSmoother["Type"].setString("FixedTime");
    defaultSmoother["RSD"].setDouble(0.001);
    defaultSmoother["Time"].setDouble(60.0);
    defaultSmoother["MinimumTime"].setDouble(30.0);
    defaultSmoother["DiscardTime"].setDouble(8.0);
    filterNormalizeIp =
            BaselineSmoother::fromConfiguration(defaultSmoother, configData, "Filter/Normalize");
    filterNormalizeIf =
            BaselineSmoother::fromConfiguration(defaultSmoother, configData, "Filter/Normalize");
    filterNormalizeIn =
            BaselineSmoother::fromConfiguration(defaultSmoother, configData, "Filter/Normalize");

    defaultSmoother["RSD"].setDouble(0.02);
    defaultSmoother["DiscardTime"].setEmpty();
    defaultSmoother["Band"].setDouble(2.0);
    filterChangeIf =
            BaselineSmoother::fromConfiguration(defaultSmoother, configData, "Autodetect/Smoother");
    filterChangeIrc =
            BaselineSmoother::fromConfiguration(defaultSmoother, configData, "Autodetect/Smoother");

    DynamicTimeInterval::Variable *defaultTime = new DynamicTimeInterval::Variable;
    defaultTime->set(Time::Hour, 2, false);
    acceptNormalizationTime =
            DynamicTimeInterval::fromConfiguration(configData, "Filter/ResumeTimeout",
                                                   FP::undefined(), FP::undefined(), true,
                                                   defaultTime);

    delete defaultTime;
}

void AcquireRRPSAP1W::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void AcquireRRPSAP1W::setToInteractive()
{ responseState = RESP_INITIALIZE; }


ComponentOptions AcquireRRPSAP1WComponent::getBaseOptions()
{
    ComponentOptions options;

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(tr("spot", "name"), tr("Area of the sample spot"),
                                            tr("This is the area of the sample spot.  If it is greater "
                                                   "than 0.1 it is assumed to be in mm\xC2\xB2, otherwise it is "
                                                   "treated as being in m\xC2\xB2."),
                                            tr("17.83 mm\xC2\xB2"), 1);
    d->setMinimum(0.0, false);
    options.add("area", d);

    ComponentOptionSingleCalibration *cal =
            new ComponentOptionSingleCalibration(tr("cal-q", "name"), tr("Flow rate calibration"),
                                                 tr("This is the calibration applied to the flow reported by "
                                                    "the instrument."), "");
    cal->setAllowInvalid(false);
    cal->setAllowConstant(true);
    options.add("cal-q", cal);

    BaselineSmootherOption *smoother = new BaselineSmootherOption(tr("filter-normalize", "name"),
                                                                  tr("Filter normalization smoother"),
                                                                  tr("This defines the smoothing used to establish a normalization at "
                                                                     "the start of the filter."),
                                                                  tr("60 seconds max, 30 seconds min, 8 second delay, <0.001 RSD",
                                                                     "normalize smoother"));
    smoother->setDefault(new BaselineFixedTime(30, 60, 0.001, FP::undefined(), 8.0));
    smoother->setSpikeDetection(false);
    smoother->setStabilityDetection(true);
    options.add("filter-normalize", smoother);

    smoother = new BaselineSmootherOption(tr("filter-change", "name"), tr("Filter change smoother"),
                                          tr("This defines the smoothing used to detect the start and end of "
                                             "filter changes."),
                                          tr("60 seconds max, 30 seconds min, <0.02 RSD, 2.0 spike factor",
                                             "normalize smoother"));
    smoother->setDefault(new BaselineFixedTime(30, 60, 0.02, 2.0, 0.0));
    smoother->setSpikeDetection(true);
    smoother->setStabilityDetection(true);
    options.add("filter-change", smoother);

    options.add("disable-recovery", new ComponentOptionBoolean(tr("disable-recovery", "name"),
                                                               tr("Disable normalization recovery"),
                                                               tr("When set, this disables recovery of the normalization values from "
                                                                  "the instrument reported transmittance.  This can be useful if "
                                                                  "the reported transmittance is not valid."),
                                                               QString()));

    return options;
}

AcquireRRPSAP1W::AcquireRRPSAP1W(const ComponentOptions &options, const std::string &loggingContext)
        : FramedInstrument("psap1w", loggingContext),
          lastRecordTime(FP::undefined()),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          responseState(RESP_WAIT),
          autoprobeValidRecords(0),
          sampleState(SAMPLE_FILTER_NORMALIZE)
{
    setDefaultInvalid();
    config.append(Configuration());

    if (options.isSet("area")) {
        double value = qobject_cast<ComponentOptionSingleDouble *>(options.get("area"))->get();
        Q_ASSERT(FP::defined(value) && value > 0.0);
        if (value < 0.1)
            value *= 1E6;
        config.first().area = value;
    }

    if (options.isSet("cal-q")) {
        config.first().flowCalibration =
                qobject_cast<ComponentOptionSingleCalibration *>(options.get("cal-q"))->get();
    }

    if (options.isSet("disable-recovery")) {
        config.first().enableNormalizationRecovery =
                !qobject_cast<ComponentOptionBoolean *>(options.get("disable-recovery"))->get();
    }

    BaselineSmootherOption *smootherOption =
            qobject_cast<BaselineSmootherOption *>(options.get("filter-normalize"));
    filterNormalizeIp = smootherOption->getSmoother();
    filterNormalizeIf = smootherOption->getSmoother();
    filterNormalizeIn = smootherOption->getSmoother();

    smootherOption = qobject_cast<BaselineSmootherOption *>(options.get("filter-change"));
    filterChangeIf = smootherOption->getSmoother();
    filterChangeIrc = smootherOption->getSmoother();

    /* Set these so it never rejects things (since it's under external/no
     * control). */
    config.first().verifyNormalizationBand = FP::undefined();
    acceptNormalizationTime = new DynamicTimeInterval::Undefined;

    /* We have no other indicator for filter changes, so we must use them */
    config.first().enableAutodetectStart = true;
    config.first().enableAutodetectEnd = true;
}

AcquireRRPSAP1W::~AcquireRRPSAP1W()
{
    delete acceptNormalizationTime;
    delete filterNormalizeIp;
    delete filterNormalizeIf;
    delete filterNormalizeIn;
    delete filterChangeIf;
    delete filterChangeIrc;
}

void AcquireRRPSAP1W::logValue(double startTime,
                               double endTime,
                               SequenceName::Component name,
                               Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireRRPSAP1W::realtimeValue(double time,
                                    SequenceName::Component name,
                                    Variant::Root &&value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

Variant::Root AcquireRRPSAP1W::buildFilterValueMeta() const
{
    Variant::Root result;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    result.write()
          .metadataHashChild("Ip" + wlCode)
          .metadataReal("Description")
          .setString("Sample detector count");
    result.write()
          .metadataHashChild("Ip" + wlCode)
          .metadataReal("Format")
          .setString("0000000.0");
    result.write()
          .metadataHashChild("Ip" + wlCode)
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelength);

    result.write()
          .metadataHashChild("If" + wlCode)
          .metadataReal("Description")
          .setString("Reference detector count");
    result.write()
          .metadataHashChild("If" + wlCode)
          .metadataReal("Format")
          .setString("0000000.0");
    result.write()
          .metadataHashChild("If" + wlCode)
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelength);

    result.write()
          .metadataHashChild("In" + wlCode)
          .metadataReal("Description")
          .setString("Normalized intensity");
    result.write()
          .metadataHashChild("In" + wlCode)
          .metadataReal("Format")
          .setString("00.0000000");
    result.write()
          .metadataHashChild("In" + wlCode)
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelength);

    return result;
}

SequenceValue::Transfer AcquireRRPSAP1W::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_rr_psap1w");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    result.emplace_back(SequenceName({}, "raw_meta", "Q"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Flow"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(7);

    result.emplace_back(SequenceName({}, "raw_meta", "Qt"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.00000");
    result.back().write().metadataReal("Units").setString("m³");
    result.back().write().metadataReal("Description").setString("Accumulated sample volume");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");

    result.emplace_back(SequenceName({}, "raw_meta", "L"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0000");
    result.back().write().metadataReal("Units").setString("m");
    result.back().write().metadataReal("Description").setString("Integrated sample length, Qt/A");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");

    result.emplace_back(SequenceName({}, "raw_meta", "Ir" + wlCode), Variant::Root(),
                        time, FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0.0000000");
    result.back().write().metadataReal("GroupUnits").setString("Transmittance");
    result.back().write().metadataReal("Description").setString("Transmittance");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Transmittance"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "Ba" + wlCode), Variant::Root(),
                        time, FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light absorption coefficient");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Mode")
          .setString("BeersLawAbsorptionInitial");
    result.back().write().metadataReal("Smoothing").hash("Parameters").hash("L").setString("L");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Transmittance")
          .setString("Ir" + wlCode);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Bap"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "If" + wlCode), Variant::Root(),
                        time, FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Reference detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("I reference"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "Ip" + wlCode), Variant::Root(),
                        time, FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Sample detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("I sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "Ff"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataInteger("Format").setString("0000000000");
    result.back().write().metadataInteger("GroupUnits").setString("FilterID");
    result.back().write().metadataInteger("Description").setString("Filter ID");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataInteger("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataInteger("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("STP")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_rr_psap1w");
    result.back().write().metadataSingleFlag("STP").hash("Bits").setInt64(0x0200);
    result.back()
          .write()
          .metadataSingleFlag("STP")
          .hash("Description")
          .setString("Data reported at STP");

    result.back()
          .write()
          .metadataSingleFlag("FilterChanging")
          .hash("Description")
          .setString("Changing filter");
    result.back().write().metadataSingleFlag("FilterChanging").hash("Bits").setInt64(0x00010000);
    result.back()
          .write()
          .metadataSingleFlag("FilterChanging")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_rr_psap1w");

    result.back()
          .write()
          .metadataSingleFlag("WhiteFilterChanging")
          .hash("Description")
          .setString("Changing to a known white filter");
    result.back()
          .write()
          .metadataSingleFlag("WhiteFilterChanging")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_rr_psap1w");

    result.back()
          .write()
          .metadataSingleFlag("Normalizing")
          .hash("Description")
          .setString("Establishing normalization factor for filter");
    result.back()
          .write()
          .metadataSingleFlag("Normalizing")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_rr_psap1w");

    result.back()
          .write()
          .metadataSingleFlag("WhiteFilterNormalizing")
          .hash("Description")
          .setString("Establishing normalization factor for a white filter");
    result.back()
          .write()
          .metadataSingleFlag("WhiteFilterNormalizing")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_rr_psap1w");

    result.back()
          .write()
          .metadataSingleFlag("TransmittanceMedium")
          .hash("Description")
          .setString("Transmittance less than 0.7");
    result.back()
          .write()
          .metadataSingleFlag("TransmittanceMedium")
          .hash("Bits")
          .setInt64(0x00100000);
    result.back()
          .write()
          .metadataSingleFlag("TransmittanceMedium")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_rr_psap1w");

    result.back()
          .write()
          .metadataSingleFlag("TransmittanceLow")
          .hash("Description")
          .setString("Transmittance less than 0.5");
    result.back().write().metadataSingleFlag("TransmittanceLow").hash("Bits").setInt64(0x00200000);
    result.back()
          .write()
          .metadataSingleFlag("TransmittanceLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_rr_psap1w");

    result.back()
          .write()
          .metadataSingleFlag("NonWhiteFilter")
          .hash("Description")
          .setString("Filter did not appear to be white");
    result.back()
          .write()
          .metadataSingleFlag("NonWhiteFilter")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_rr_psap1w");

    result.emplace_back(SequenceName({}, "raw_meta", "ZFILTER"), buildFilterValueMeta(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("Filter start parameters");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");

    result.emplace_back(SequenceName({}, "raw_meta", "ZWHITE"), buildFilterValueMeta(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("White filter parameters");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");

    result.emplace_back(SequenceName({}, "raw_meta", "ZSPOT"), buildFilterValueMeta(), time,
                        FP::undefined());
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataHash("Description").setString("Spot sampling parameters");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataHashChild("In" + wlCode)
          .metadataReal("Description")
          .setString("Spot start normalized intensity");
    result.back()
          .write()
          .metadataHashChild("In" + wlCode)
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelength);
    result.back()
          .write()
          .metadataHashChild("In" + wlCode)
          .metadataReal("Format")
          .setString("00.0000000");
    result.back()
          .write()
          .metadataHashChild("In" + wlCode + "Latest")
          .metadataReal("Description")
          .setString("Spot end normalized intensity");
    result.back()
          .write()
          .metadataHashChild("In" + wlCode + "Latest")
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelength);
    result.back()
          .write()
          .metadataHashChild("In" + wlCode + "Latest")
          .metadataReal("Format")
          .setString("00.0000000");
    result.back()
          .write()
          .metadataHashChild("Ir" + wlCode)
          .metadataReal("Description")
          .setString("Spot end transmittance");
    result.back()
          .write()
          .metadataHashChild("Ir" + wlCode)
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelength);
    result.back()
          .write()
          .metadataHashChild("In" + wlCode)
          .metadataReal("Format")
          .setString("0.0000000");
    result.back()
          .write()
          .metadataHashChild("Ff")
          .metadataInteger("Description")
          .setString("Filter ID");
    result.back().write().metadataHashChild("Ff").metadataInteger("Format").setString("0000000000");
    result.back()
          .write()
          .metadataHashChild("Qt")
          .metadataReal("Description")
          .setString("Total spot sample volume");
    result.back().write().metadataHashChild("Qt").metadataReal("Format").setString("000.00");
    result.back().write().metadataHashChild("Qt").metadataReal("Units").setString("m³");
    result.back().write().metadataHashChild("Qt").metadataReal("ReportT").setDouble(0);
    result.back().write().metadataHashChild("Qt").metadataReal("ReportP").setDouble(1013.25);
    result.back()
          .write()
          .metadataHashChild("L")
          .metadataReal("Description")
          .setString("Total integrated spot sample length, Qt/A");
    result.back().write().metadataHashChild("L").metadataReal("Format").setString("00000.0000");
    result.back().write().metadataHashChild("L").metadataReal("Units").setString("m");
    result.back().write().metadataHashChild("L").metadataReal("ReportT").setDouble(0);
    result.back().write().metadataHashChild("L").metadataReal("ReportP").setDouble(1013.25);
    result.back()
          .write()
          .metadataHashChild("Area")
          .metadataReal("Description")
          .setString("Spot area");
    result.back().write().metadataHashChild("Area").metadataReal("Format").setString("00.000");
    result.back().write().metadataHashChild("Area").metadataReal("Units").setString("mm²");
    result.back()
          .write()
          .metadataHashChild("WasWhite")
          .metadataBoolean("Description")
          .setString("Spot was determined to be white at the start of sampling");

    return result;
}

SequenceValue::Transfer AcquireRRPSAP1W::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    Variant::Root processing;
    processing["By"].setString("acquire_rr_psap1w");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    result.emplace_back(SequenceName({}, "raw_meta", "ZQ"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Un-calibrated flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Raw flow"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(8);

    result.emplace_back(SequenceName({}, "raw_meta", "In" + wlCode), Variant::Root(),
                        time, FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("00.0000000");
    result.back().write().metadataReal("GroupUnits").setString("NormalizedIntensity");
    result.back().write().metadataReal("Description").setString("Normalized sample spot intensity");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("Eavesdropper").hash("Hide").setBool(true);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("I normalized"));

    result.emplace_back(SequenceName({}, "raw_meta", "Ing" + wlCode), Variant::Root(),
                        time, FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("GroupUnits").setString("StabilityFactor");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Normalized sample spot intensity stability factor");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("Eavesdropper").hash("Hide").setBool(true);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("I norm stab"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(9);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Normalize")
          .setString(QObject::tr("Waiting for filter stability..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("WhiteFilterNormalize")
          .setString(QObject::tr("Waiting for white filter stability..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("FilterChange")
          .setString(QObject::tr("Changing filter..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("WhiteFilterChange")
          .setString(QObject::tr("Changing white filter..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("RequireFilterChange")
          .setString(QObject::tr("FILTER CHANGE REQUIRED"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("RequireWhiteFilterChange")
          .setString(QObject::tr("WHITE FILTER CHANGE REQUIRED"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Wait")
          .setString(QObject::tr("STARTING COMMS"));

    return result;
}

SequenceMatch::Composite AcquireRRPSAP1W::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "Ff");
    sel.append({}, {}, "ZFILTER");
    sel.append({}, {}, "ZSPOT");
    sel.append({}, {}, "ZWHITE");
    sel.append({}, {}, "ZSTATE");
    return sel;
}

Variant::Root AcquireRRPSAP1W::buildFilterValue(const IntensityData &data) const
{
    Variant::Root result;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    result.write().hash("Ip" + wlCode).setDouble(data.Ip);
    result.write().hash("If" + wlCode).setDouble(data.If);
    result.write().hash("In" + wlCode).setDouble(data.In);

    return result;
}

void AcquireRRPSAP1W::emitFilterEnd(double endTime)
{
    if (!FP::defined(filterStart.startTime))
        return;
    double startTime = filterStart.startTime;
    filterStart.startTime = FP::undefined();
    if (persistentEgress == NULL)
        return;

    SequenceValue result
            (SequenceName({}, "raw", "ZFILTER"), buildFilterValue(filterStart), startTime, endTime);
    persistentEgress->incomingData(result);
}

void AcquireRRPSAP1W::emitWhiteEnd(double endTime)
{
    if (!FP::defined(filterWhite.startTime))
        return;
    double startTime = filterWhite.startTime;
    filterWhite.startTime = FP::undefined();
    if (persistentEgress == NULL)
        return;

    SequenceValue result
            (SequenceName({}, "raw", "ZWHITE"), buildFilterValue(filterWhite), startTime, endTime);
    persistentEgress->incomingData(result);
}

void AcquireRRPSAP1W::emitNormalizationEnd(double endTime)
{
    if (!FP::defined(normalizationStartTime))
        return;
    double startTime = normalizationStartTime;
    normalizationStartTime = FP::undefined();
    if (persistentEgress == NULL)
        return;

    SequenceValue::Transfer result;
    result.emplace_back(SequenceName({}, "raw", "ZSPOT"), buildNormalizationValue(), startTime,
                        endTime);
    result.emplace_back(SequenceName({}, "raw", "Ff"),
                        Variant::Root(static_cast<qint64>(std::floor(normalizationStartTime))),
                        startTime, endTime);

    persistentEgress->incomingData(result);
}

bool AcquireRRPSAP1W::isSameNormalization(double currentTime, const Variant::Read &If) const
{
    double checkBand = config.first().verifyNormalizationBand;
    if (FP::defined(checkBand) && FP::defined(normalizationLatest.If)) {
        double v = If.toDouble();
        if (!FP::defined(v))
            return false;
        if (!BaselineSmoother::inRelativeBand(normalizationLatest.If, v, checkBand))
            return false;
    }
    if (FP::defined(currentTime) && FP::defined(normalizationLatest.startTime)) {
        if (currentTime < normalizationLatest.startTime)
            return false;
        double checkTime = acceptNormalizationTime->applyConst(normalizationLatest.startTime,
                                                               normalizationLatest.startTime, true);
        if (FP::defined(checkTime) && checkTime <= currentTime)
            return false;
    }
    return true;
}

bool AcquireRRPSAP1W::autodetectStart() const
{
    if (!config.first().enableAutodetectStart)
        return false;

    return filterChangeIf->spike();
}

bool AcquireRRPSAP1W::autodetectEnd() const
{
    if (!config.first().enableAutodetectEnd)
        return false;

    if (!filterChangeIrc->ready())
        return false;
    if (!filterChangeIrc->stable())
        return false;
    double v = filterChangeIrc->value();
    if (!FP::defined(v))
        return false;
    if (::fabs(v - 1.0) > 0.02)
        return false;
    return true;
}

bool AcquireRRPSAP1W::normalizationDone() const
{
    if (!filterNormalizeIn->ready())
        return false;
    if (!filterNormalizeIn->stable())
        return false;
    return true;
}

bool AcquireRRPSAP1W::isWhiteFilter() const
{
    double checkBand = config.first().verifyWhiteBand;
    if (FP::defined(checkBand)) {
        do {
            if (!FP::defined(filterStart.In))
                break;
            if (!FP::defined(filterWhite.In))
                break;
            if (!BaselineSmoother::inRelativeBand(filterWhite.In, filterStart.In, checkBand))
                return false;
        } while (0);
    }
    return true;
}

bool AcquireRRPSAP1W::whiteFilterTimeout(double time) const
{
    if (!FP::defined(whiteFilterChangeStartTime))
        return false;
    return time > whiteFilterChangeStartTime + 3600.0;
}

void AcquireRRPSAP1W::resetSmoothers()
{
    filterNormalizeIp->reset();
    filterNormalizeIf->reset();
    filterNormalizeIn->reset();
    filterChangeIrc->reset();
}

/* Use a macro so we don't have to pass a huge number of variables for a
 * purely diagnostic function */
#define insertDiagnosticCommonValues(target) do { \
    target.hash("Values").hash("Flags").set(outputFlags); \
    target.hash("Values").hash("Q").set(Q); \
    target.hash("Values").hash("Irc").set(Irc); \
    target.hash("Values").hash("If").set(If); \
    target.hash("Values").hash("Ip").set(Ip); \
    target.hash("Values").hash("In").set(In); \
} while(0)

void AcquireRRPSAP1W::insertDiagnosticFilterValues(Variant::Write &target,
                                                   const QString &name,
                                                   const IntensityData &source) const
{
    target.hash(name).hash("StartTime").setDouble(source.startTime);
    target.hash(name).hash("Ip").setDouble(source.Ip);
    target.hash(name).hash("If").setDouble(source.If);
    target.hash(name).hash("In").setDouble(source.In);
}

void AcquireRRPSAP1W::insertDiagnosticNormalization(Variant::Write &target) const
{
    target.hash("Normalization").hash("StartTime").setDouble(normalizationStartTime);
    target.hash("Normalization").hash("In").setDouble(normalizationLatest.In);
    target.hash("Normalization").hash("If").setDouble(normalizationLatest.If);
    target.hash("Normalization").hash("Ip").setDouble(normalizationLatest.Ip);
    target.hash("Normalization").hash("Volume").setDouble(accumulatedVolume);
    target.hash("Normalization").hash("LatestTime").setDouble(normalizationLatest.startTime);
}

void AcquireRRPSAP1W::insertDiagnosticFilterNormalization(Variant::Write &target) const
{
    target.hash("Baseline").hash("Ip").set(filterNormalizeIp->describeState());
    target.hash("Baseline").hash("If").set(filterNormalizeIf->describeState());
    target.hash("Baseline").hash("In").set(filterNormalizeIn->describeState());
    target.hash("Change").hash("If").set(filterChangeIf->describeState());
    target.hash("Change").hash("Irc").set(filterChangeIrc->describeState());
}

static double calculateIn(double Isam, double Iref)
{
    if (!FP::defined(Iref) || !FP::defined(Isam))
        return FP::undefined();
    if (Iref == 0.0)
        return FP::undefined();
    return Isam / Iref;
}

static double calculateIr(double In0, double In)
{
    if (!FP::defined(In0) || !FP::defined(In))
        return FP::undefined();
    if (In0 == 0.0)
        return FP::undefined();
    return In / In0;
}

static double recoverNormalization(double Ir, double In)
{
    if (!FP::defined(Ir) || !FP::defined(In))
        return FP::undefined();
    if (Ir <= 0.0)
        return FP::undefined();
    return In / Ir;
}

static double accumulateVolume(double startTime, double endTime, double Q)
{
    if (!FP::defined(Q) || !FP::defined(endTime))
        return FP::undefined();
    if (!FP::defined(startTime))
        startTime = endTime - 1.0;
    if (Q < 0.0)
        return FP::undefined();
    if (endTime < startTime)
        return FP::undefined();
    return (Q * (endTime - startTime)) / 60000.0;
}

static double calculateL(double Qt, double area)
{
    if (!FP::defined(Qt) || !FP::defined(area) || area <= 0.0 || Qt <= 0.0)
        return FP::undefined();
    return Qt / (area * 1E-6);
}

static double calculateBa(double Ir0, double Qt0, double Ir1, double Qt1, double area)
{
    if (!FP::defined(Ir0) ||
            !FP::defined(Qt0) ||
            !FP::defined(Ir1) ||
            !FP::defined(Qt1) ||
            !FP::defined(area))
        return FP::undefined();
    if (Ir0 <= 0.0 || Ir1 <= 0.0)
        return FP::undefined();
    double dQt = Qt1 - Qt0;
    if (dQt <= 0.0)
        return FP::undefined();
    return log(Ir0 / Ir1) * (area / dQt);
}

Variant::Root AcquireRRPSAP1W::buildNormalizationValue() const
{
    Variant::Root result;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    result.write().hash("In" + wlCode).setDouble(filterNormalization);
    result.write().hash("In" + wlCode + "Latest").setDouble(priorIn);
    result.write().hash("Ir" + wlCode).setDouble(priorIr);
    result.write().hash("L").setDouble(calculateL(accumulatedVolume, config.first().area));
    result.write().hash("Area").setDouble(config.first().area);
    if (FP::defined(normalizationStartTime))
        result.write().hash("Ff").setInt64((qint64) floor(normalizationStartTime));
    else
        result.write().hash("Ff").setInt64(INTEGER::undefined());
    result.write().hash("Qt").setDouble(accumulatedVolume);
    result.write().hash("WasWhite").setBool(!filterIsNotWhite);

    return result;
}

int AcquireRRPSAP1W::processRecord(const Util::ByteView &line, double &frameTime)
{
    Q_ASSERT(!config.isEmpty());

    if (line.size() < 3)
        return 1;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    auto fields = CSV::acquisitionSplit(line);
    bool ok = false;

    if (fields.empty()) return 2;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 iyear = field.parse_i32(&ok);
    if (!ok) return 3;
    if (iyear < 1900) {
        if (iyear < 0) return 4;
        if (iyear > 99) return 5;
        int century;
        if (FP::defined(frameTime)) {
            century = Time::toDateTime(frameTime).date().year() / 100;
        } else {
            /* Could set this based on the date the first one was manufactured,
             * but this is good enough */
            if (iyear < 90)
                century = 20;
            else
                century = 19;
        }
        iyear += century * 100;
    } else {
        if (iyear < 1990) return 6;
        if (iyear > 2999) return 7;
    }
    Variant::Root year(iyear);
    remap("YEAR", year);
    iyear = year.read().toInt64();

    if (fields.empty()) return 8;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 imonth = field.parse_i32(&ok);
    if (!ok) return 9;
    if (imonth < 1) return 10;
    if (imonth > 12) return 11;
    Variant::Root month(imonth);
    remap("MONTH", month);
    imonth = month.read().toInt64();

    if (fields.empty()) return 12;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 iday = field.parse_i32(&ok);
    if (!ok) return 13;
    if (iday < 1) return 14;
    if (iday > 31) return 15;
    Variant::Root day(iday);
    remap("DAY", day);
    iday = day.read().toInt64();

    if (fields.empty()) return 16;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 ihour = field.parse_i32(&ok);
    if (!ok) return 17;
    if (ihour < 0) return 18;
    if (ihour > 24) return 19;
    Variant::Root hour(ihour);
    remap("HOUR", hour);
    ihour = hour.read().toInt64();

    if (fields.empty()) return 20;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 iminute = field.parse_i32(&ok);
    if (!ok) return 21;
    if (iminute < 0) return 22;
    if (iminute > 59) return 23;
    Variant::Root minute(iminute);
    remap("MINUTE", minute);
    iminute = minute.read().toInt64();

    if (fields.empty()) return 24;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 isecond = field.parse_i32(&ok);
    if (!ok) return 25;
    if (isecond < 0) return 26;
    if (isecond > 60) return 27;
    Variant::Root second(isecond);
    remap("SECOND", second);
    isecond = second.read().toInt64();

    /* Averaging period (seconds), ignored */
    if (fields.empty()) return 28;
    fields.pop_front();

    /* Instrument calculated absorption, ignored */
    if (fields.empty()) return 29;
    fields.pop_front();

    if (fields.empty()) return 30;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Irc(field.parse_real(&ok));
    if (!ok) return 31;
    if (!FP::defined(Irc.read().toReal())) return 32;
    remap("Irc" + wlCode, Irc);

    if (fields.empty()) return 33;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZQ(field.parse_real(&ok));
    if (!ok) return 34;
    if (!FP::defined(ZQ.read().toReal())) return 35;
    remap("ZQ", ZQ);
    Variant::Root Q(config.first().flowCalibration.apply(ZQ.read().toDouble()));
    remap("Q", Q);

    if (fields.empty()) return 36;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Ip(field.parse_real(&ok));
    if (!ok) return 37;
    if (!FP::defined(Ip.read().toReal())) return 38;
    if (Ip.read().toDouble() == 33840.0)
        Ip.write().setDouble(FP::undefined());
    remap("Ip" + wlCode, Ip);

    if (fields.empty()) return 39;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root If(field.parse_real(&ok));
    if (!ok) return 40;
    if (!FP::defined(If.read().toReal())) return 41;
    if (If.read().toDouble() == 33840.0)
        If.write().setDouble(FP::undefined());
    remap("If" + wlCode, If);

    if (config.first().strictMode) {
        if (!fields.empty())
            return 42;
    }

    if (!FP::defined(frameTime) &&
            INTEGER::defined(iyear) &&
            iyear >= 1900 &&
            iyear <= 2999 &&
            INTEGER::defined(imonth) &&
            imonth >= 1 &&
            imonth <= 12 &&
            INTEGER::defined(iday) &&
            iday >= 1 &&
            iday <= 31 &&
            INTEGER::defined(ihour) &&
            ihour >= 0 &&
            ihour <= 23 &&
            INTEGER::defined(iminute) &&
            iminute >= 0 &&
            iminute <= 59 &&
            INTEGER::defined(isecond) &&
            isecond >= 0 &&
            isecond <= 60) {
        frameTime = Time::fromDateTime(QDateTime(QDate((int) iyear, (int) imonth, (int) iday),
                                                 QTime((int) ihour, (int) iminute, (int) isecond),
                                                 Qt::UTC));

        configAdvance(frameTime);
    }
    if (!FP::defined(frameTime))
        return -1;
    if (FP::defined(lastRecordTime) && frameTime < lastRecordTime)
        return -1;
    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;
    if (!haveEmittedLogMeta && FP::defined(startTime) && loggingEgress != NULL) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }

    if (!haveEmittedRealtimeMeta && FP::defined(frameTime) && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    Variant::Root In(calculateIn(Ip.read().toDouble(), If.read().toDouble()));
    remap("In" + wlCode, In);

    filterNormalizeIp->add(Ip.read().toDouble(), startTime, endTime);
    filterNormalizeIf->add(If.read().toDouble(), startTime, endTime);
    filterNormalizeIn->add(In.read().toDouble(), startTime, endTime);
    filterChangeIrc->add(Irc.read().toDouble(), startTime, endTime);
    filterChangeIf->add(If.read().toDouble(), startTime, endTime);

    /* Check sample state change */
    SampleState priorSampleState = sampleState;
    SampleState initialSampleState = sampleState;
    Variant::Root Ir;
    Variant::Root Ba;
    Variant::Root L;
    Variant::Root Qt;
    Variant::Root outputFlags;
    outputFlags.write().setType(Variant::Type::Flags);
    outputFlags.write().applyFlag("STP"); /* Flow meter is already at STP */
    double priorVolume = accumulatedVolume;
    do {
        priorSampleState = sampleState;

        switch (sampleState) {
        case SAMPLE_RUN:
            if (!haveAcceptedNormalization) {
                haveAcceptedNormalization = true;
                if (isSameNormalization(endTime, If)) {
                    qCDebug(log) << "Accepted pending normalization at" << Logging::time(endTime);

                    forceFilterBreak = true;
                    forceRealtimeStateEmit = true;
                    priorIr = FP::undefined();

                    double effectiveEnd = startTime;
                    if (!FP::defined(effectiveEnd)) {
                        effectiveEnd = endTime - config.first().reportInterval;
                    }
                    double add = accumulateVolume(normalizationLatest.startTime, effectiveEnd,
                                                  Q.read().toDouble());
                    if (FP::defined(add)) {
                        accumulatedVolume += add;
                    } else {
                        qCDebug(log)
                            << "Unable to add sample volume accumulated during down time, assuming zero";
                    }

                    break;
                } else if (config.first().enableNormalizationRecovery) {
                    qCDebug(log) << "Rebuilding normalization from the reported transmittance at"
                                 << Logging::time(endTime);

                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("Run");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);
                    event(endTime,
                          QObject::tr("Recovering normalization from reported transmittance."),
                          false, info);

                    forceFilterBreak = true;
                    forceRealtimeStateEmit = true;
                    priorIr = FP::undefined();
                    filterNormalization =
                            recoverNormalization(Irc.read().toDouble(), In.read().toDouble());
                    filterStart.startTime = FP::undefined();
                    filterStart.Ip = FP::undefined();
                    filterStart.If = FP::undefined();
                    filterStart.In = FP::undefined();
                    lastTransmittanceCheckTime = FP::undefined();

                    break;
                } else {
                    qCDebug(log) << "Rejected pending normalization at" << Logging::time(endTime);

                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("Run");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);
                    event(endTime,
                          QObject::tr("Rejected saved normalization; filter change required."),
                          false, info);

                    normalizationStartTime = FP::undefined();
                    filterNormalization = FP::undefined();
                    lastTransmittanceCheckTime = FP::undefined();
                    forceRealtimeStateEmit = true;
                    forceFilterBreak = true;
                    resetSmoothers();

                    sampleState = SAMPLE_REQUIRE_FILTER_CHANGE;
                    break;
                }
            }

            if (autodetectStart()) {
                qCDebug(log) << "Auto-detected filter change start at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("Run");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticFilterNormalization(info);
                insertDiagnosticNormalization(info);
                event(endTime, QObject::tr("Auto-detected filter change started."), true, info);
                sampleState = SAMPLE_FILTER_CHANGING;
                forceFilterBreak = true;

                emitFilterEnd(endTime);
                normalizationLatest.startTime = FP::undefined();
                resetSmoothers();
                break;
            }

            normalizationLatest.startTime = endTime;
            normalizationLatest.Ip = Ip.read().toDouble();
            normalizationLatest.If = If.read().toDouble();
            normalizationLatest.In = In.read().toDouble();

            break;

        case SAMPLE_FILTER_NORMALIZE:
            outputFlags.write().applyFlag("Normalizing");

            if (!haveAcceptedNormalization) {
                haveAcceptedNormalization = true;
                if (isSameNormalization(endTime, If)) {
                    qCDebug(log) << "Accepted pending normalization at" << Logging::time(endTime);

                    sampleState = SAMPLE_RUN;
                    forceFilterBreak = true;
                    forceRealtimeStateEmit = true;
                    priorIr = FP::undefined();

                    double effectiveEnd = startTime;
                    if (!FP::defined(effectiveEnd)) {
                        effectiveEnd = endTime - config.first().reportInterval;
                    }
                    double add = accumulateVolume(normalizationLatest.startTime, effectiveEnd,
                                                  Q.read().toDouble());
                    if (FP::defined(add)) {
                        accumulatedVolume += add;
                    } else {
                        qCDebug(log)
                            << "Unable to add sample volume accumulated during down time, assuming zero";
                    }

                    break;
                } else if (config.first().enableNormalizationRecovery) {
                    qCDebug(log) << "Rebuilding normalization from the reported transmittance at"
                                 << Logging::time(endTime);

                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("Normalize");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);
                    event(endTime,
                          QObject::tr("Recovering normalization from reported transmittance."),
                          false, info);

                    sampleState = SAMPLE_RUN;
                    forceFilterBreak = true;
                    forceRealtimeStateEmit = true;
                    priorIr = FP::undefined();
                    filterNormalization =
                            recoverNormalization(Irc.read().toDouble(), In.read().toDouble());
                    filterStart.startTime = FP::undefined();
                    filterStart.Ip = FP::undefined();
                    filterStart.If = FP::undefined();
                    filterStart.In = FP::undefined();

                    break;
                } else {
                    qCDebug(log) << "Rejected pending normalization at" << Logging::time(endTime);

                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("Normalize");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);
                    event(endTime,
                          QObject::tr("Rejected saved normalization; filter change required."),
                          false, info);

                    normalizationStartTime = FP::undefined();
                    filterNormalization = FP::undefined();
                    lastTransmittanceCheckTime = FP::undefined();
                    forceRealtimeStateEmit = true;

                    forceFilterBreak = true;
                    sampleState = SAMPLE_REQUIRE_FILTER_CHANGE;
                    break;
                }
            }

            if (autodetectStart()) {
                qCDebug(log) << "Auto-detected filter change start at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("Normalize");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticNormalization(info);
                event(endTime, QObject::tr("Auto-detected filter change started."), true, info);
                sampleState = SAMPLE_FILTER_CHANGING;
                forceFilterBreak = true;

                emitFilterEnd(endTime);
                normalizationLatest.startTime = FP::undefined();
                resetSmoothers();
                break;
            }

            if (!normalizationDone())
                break;
            emitFilterEnd(endTime);
            emitNormalizationEnd(endTime);
            normalizationStartTime = endTime;
            filterNormalization = filterNormalizeIn->value();

            normalizationLatest.startTime = endTime;
            normalizationLatest.Ip = Ip.read().toDouble();
            normalizationLatest.If = If.read().toDouble();
            normalizationLatest.In = In.read().toDouble();

            filterStart.startTime = endTime;
            filterStart.Ip = filterNormalizeIp->value();
            filterStart.If = filterNormalizeIf->value();
            filterStart.In = filterNormalizeIn->value();

            qCDebug(log) << "Normalization established at" << Logging::time(endTime);

            filterIsNotWhite = !isWhiteFilter();
            if (filterIsNotWhite) {
                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterNormalization(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                info.hash("State").setString("Normalize");
                event(endTime, QObject::tr(
                        "Filter normalization established on what does not appear to be a white filter."),
                      true, info);
            } else {
                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterNormalization(info);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                info.hash("State").setString("Normalize");
                event(endTime, QObject::tr("Filter normalization established."), false, info);
            }

            if (FP::defined(Irc.read().toDouble()) &&
                    std::fabs(Irc.read().toDouble() - 1.0) > 0.1) {
                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterNormalization(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                info.hash("State").setString("Normalize");
                info.hash("Irc" + wlCode).set(Irc);
                event(endTime, QObject::tr(
                        "Filter beginning with an instrument transmittance not near one; please make sure the reset transmittance switch was pressed"),
                      true, info);
            }

            haveAcceptedNormalization = true;
            forceRealtimeStateEmit = true;
            sampleState = SAMPLE_RUN;
            resetSmoothers();

            if (state != NULL)
                state->requestStateSave();

            persistentValuesUpdated();
            break;

        case SAMPLE_WHITE_FILTER_NORMALIZE:
            outputFlags.write().applyFlag("Normalizing");
            outputFlags.write().applyFlag("WhiteFilterNormalizing");

            if (!haveAcceptedNormalization) {
                haveAcceptedNormalization = true;
                if (isSameNormalization(endTime, If)) {
                    qCDebug(log) << "Accepted pending normalization at" << Logging::time(endTime);

                    sampleState = SAMPLE_RUN;
                    forceFilterBreak = true;
                    forceRealtimeStateEmit = true;
                    priorIr = FP::undefined();

                    double effectiveEnd = startTime;
                    if (!FP::defined(effectiveEnd)) {
                        effectiveEnd = endTime - config.first().reportInterval;
                    }
                    double add = accumulateVolume(normalizationLatest.startTime, effectiveEnd,
                                                  Q.read().toDouble());
                    if (FP::defined(add)) {
                        accumulatedVolume += add;
                    } else {
                        qCDebug(log)
                            << "Unable to add sample volume accumulated during down time, assuming zero";
                    }
                    priorVolume = accumulatedVolume;

                    break;
                } else if (config.first().enableNormalizationRecovery) {
                    qCDebug(log) << "Rebuilding normalization from the reported transmittance at"
                                 << Logging::time(endTime);

                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("WhiteFilterNormalize");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);
                    event(endTime,
                          QObject::tr("Recovering normalization from reported transmittance."),
                          false, info);

                    sampleState = SAMPLE_RUN;
                    forceFilterBreak = true;
                    forceRealtimeStateEmit = true;
                    priorIr = FP::undefined();
                    priorVolume = accumulatedVolume;
                    filterNormalization =
                            recoverNormalization(Irc.read().toDouble(), In.read().toDouble());
                    filterStart.startTime = FP::undefined();
                    filterStart.Ip = FP::undefined();
                    filterStart.If = FP::undefined();
                    filterStart.In = FP::undefined();
                    lastTransmittanceCheckTime = FP::undefined();

                    break;
                } else {
                    qCDebug(log) << "Rejected pending normalization at" << Logging::time(endTime);

                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("WhiteFilterNormalize");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);
                    event(endTime,
                          QObject::tr("Rejected saved normalization; filter change required."),
                          false, info);

                    normalizationStartTime = FP::undefined();
                    filterNormalization = FP::undefined();

                    forceRealtimeStateEmit = true;
                    forceFilterBreak = true;
                    sampleState = SAMPLE_REQUIRE_FILTER_CHANGE;
                    break;
                }
            }

            if (autodetectStart()) {
                qCDebug(log) << "Auto-detected filter change start at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("WhiteFilterNormalize");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticNormalization(info);
                event(endTime, QObject::tr("Auto-detected filter change started."), true, info);
                sampleState = SAMPLE_FILTER_CHANGING;
                forceFilterBreak = true;

                emitFilterEnd(endTime);
                normalizationStartTime = FP::undefined();
                normalizationLatest.startTime = FP::undefined();
                resetSmoothers();
                break;
            }

            if (!normalizationDone())
                break;
            emitWhiteEnd(endTime);
            emitFilterEnd(endTime);
            emitNormalizationEnd(endTime);
            normalizationStartTime = endTime;
            filterNormalization = filterNormalizeIn->value();

            normalizationLatest.startTime = endTime;
            normalizationLatest.Ip = Ip.read().toDouble();
            normalizationLatest.If = If.read().toDouble();
            normalizationLatest.In = In.read().toDouble();

            filterStart.startTime = endTime;
            filterStart.Ip = filterNormalizeIp->value();
            filterStart.If = filterNormalizeIf->value();
            filterStart.In = filterNormalizeIn->value();
            filterWhite = filterStart;

            qCDebug(log) << "White filter normalization established at" << Logging::time(endTime);

            filterIsNotWhite = false;
            {
                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterNormalization(info);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                info.hash("State").setString("WhiteFilterNormalize");
                event(endTime, QObject::tr("White filter normalization established."), false, info);
            }

            if (FP::defined(Irc.read().toDouble()) &&
                    std::fabs(Irc.read().toDouble() - 1.0) > 0.1) {
                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterNormalization(info);
                info.hash("State").setString("WhiteFilterNormalize");
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                info.hash("Irc" + wlCode).set(Irc);
                event(endTime, QObject::tr(
                        "White filter beginning with an instrument transmittance not near one; please make sure the reset transmittance switch was pressed."),
                      true, info);
            }

            haveAcceptedNormalization = true;
            forceRealtimeStateEmit = true;
            sampleState = SAMPLE_RUN;
            resetSmoothers();

            if (state != NULL)
                state->requestStateSave();

            persistentValuesUpdated();
            break;

        case SAMPLE_FILTER_CHANGING:
            outputFlags.write().applyFlag("FilterChanging");
            emitFilterEnd(endTime);

            if (autodetectEnd()) {
                qCDebug(log) << "Auto-detected filter change end at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("FilterChanging");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticFilterNormalization(info);
                event(endTime, QObject::tr("Auto-detected filter change end."), true, info);

                sampleState = SAMPLE_FILTER_NORMALIZE;
                break;
            }
            break;

        case SAMPLE_WHITE_FILTER_CHANGING:
            outputFlags.write().applyFlag("FilterChanging");
            outputFlags.write().applyFlag("WhiteFilterChanging");
            emitFilterEnd(endTime);

            if (!FP::defined(whiteFilterChangeStartTime))
                whiteFilterChangeStartTime = endTime;
            if (whiteFilterTimeout(endTime)) {
                qCDebug(log) << "White filter change timeout at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("WhiteFilterChanging");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterNormalization(info);
                event(endTime, QObject::tr("White filter change ending due to timeout."), true,
                      info);

                sampleState = SAMPLE_WHITE_FILTER_NORMALIZE;
            }

            break;

        case SAMPLE_REQUIRE_FILTER_CHANGE:
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
            emitFilterEnd(endTime);

            if (autodetectStart()) {
                qCDebug(log) << "Auto-detected filter change start at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                if (sampleState == SAMPLE_REQUIRE_FILTER_CHANGE)
                    info.hash("State").setString("RequireFilterChange");
                else
                    info.hash("State").setString("RequireWhiteFilterChange");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticFilterNormalization(info);
                event(endTime, QObject::tr("Auto-detected filter change started."), true, info);

                sampleState = SAMPLE_FILTER_CHANGING;
                forceFilterBreak = true;
                resetSmoothers();

                normalizationStartTime = FP::undefined();
                normalizationLatest.startTime = FP::undefined();
                break;
            }

            break;
        }


        if (haveAcceptedNormalization && sampleState == SAMPLE_RUN) {
            Ir.write().setDouble(calculateIr(filterNormalization, In.read().toDouble()));
        } else {
            Ir.write().setEmpty();
        }

        remap("Ir" + wlCode, Ir);

        if (sampleState == SAMPLE_RUN) {
            double add;
            if (config.first().useMeasuredTime)
                add = accumulateVolume(startTime, endTime, Q.read().toDouble());
            else
                add = accumulateVolume(0.0, 1.0, Q.read().toDouble());
            if (FP::defined(add)) {
                if (FP::defined(accumulatedVolume))
                    Qt.write().setDouble(accumulatedVolume + add);
                else
                    Qt.write().setDouble(add);
            } else {
                Qt.write().setDouble(accumulatedVolume);
            }
            remap("Qt", Qt);

            L.write().setDouble(calculateL(Qt.read().toDouble(), config.first().area));
            Ba.write()
              .setDouble(
                      calculateBa(priorIr, priorVolume, Ir.read().toDouble(), Qt.read().toDouble(),
                                  config.first().area));
        } else {
            Qt.write().setEmpty();
            L.write().setEmpty();
            Ba.write().setEmpty();
        }

        remap("Ba" + wlCode, Ba);
        remap("L", L);

        do {
            double check = Ir.read().toDouble();
            if (!FP::defined(check))
                break;
            if (check > 0.7)
                break;
            outputFlags.write().applyFlag("TransmittanceMedium");

            if (check > 0.5)
                break;
            outputFlags.write().applyFlag("TransmittanceLow");
        } while (0);
    } while (priorSampleState != sampleState);

    priorIr = Ir.read().toDouble();
    priorIn = In.read().toDouble();
    accumulatedVolume = Qt.read().toDouble();

    if (filterIsNotWhite) {
        outputFlags.write().applyFlag("NonWhiteFilter");
    }

    if (!FP::defined(lastTransmittanceCheckTime) || sampleState != SAMPLE_RUN) {
        lastTransmittanceCheckTime = frameTime;
        transmittanceWarningThreshold = 0.002;
    } else if (lastTransmittanceCheckTime + 3600.0 <= frameTime) {
        double IrInstrument = Irc.read().toDouble();
        double IrCalculated = Ir.read().toDouble();

        if (FP::defined(IrInstrument) && FP::defined(IrCalculated)) {
            double dIr = fabs(IrInstrument - IrCalculated);
            if (dIr > transmittanceWarningThreshold) {
                qCDebug(log) << "Transmittance discrepancy detected at" << Logging::time(frameTime)
                             << ", instrument:" << IrInstrument << ", calculated:" << IrCalculated
                             << ", threshold:" << transmittanceWarningThreshold;

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("Run");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticFilterNormalization(info);
                insertDiagnosticNormalization(info);
                info.hash("Values").hash("IrCalculated").set(Ir);
                info.hash("InstrumentTransmittance").set(Irc);
                info.hash("CalculatedTransmittance").set(Ir);
                info.hash("Threshold").setReal(transmittanceWarningThreshold);
                event(endTime, QObject::tr(
                        "Discrepancy between the instrument transmittance (%1) and the calculated (%2) detected.")
                              .arg(IrInstrument)
                              .arg(IrCalculated), true, info);

                transmittanceWarningThreshold = dIr + 0.001;
                lastTransmittanceCheckTime = frameTime;
            }
        }
    }

    if (forceRealtimeStateEmit && realtimeEgress != NULL) {
        Variant::Root v;
        if (FP::defined(normalizationStartTime))
            v.write().setInt64(static_cast<qint64>(std::floor(normalizationStartTime)));
        else
            v.write().setInt64(INTEGER::undefined());
        realtimeEgress->emplaceData(SequenceName({}, "raw", "Ff"), std::move(v), endTime,
                                    FP::undefined());
    }
    if (realtimeEgress != NULL && (initialSampleState != sampleState || forceRealtimeStateEmit)) {
        Variant::Root state;
        switch (sampleState) {
        case SAMPLE_RUN:
            state.write().setString("Run");
            break;
        case SAMPLE_FILTER_NORMALIZE:
            state.write().setString("Normalize");
            break;
        case SAMPLE_FILTER_CHANGING:
            state.write().setString("FilterChange");
            break;
        case SAMPLE_WHITE_FILTER_NORMALIZE:
            state.write().setString("WhiteFilterNormalize");
            break;
        case SAMPLE_WHITE_FILTER_CHANGING:
            state.write().setString("WhiteFilterChange");
            break;
        case SAMPLE_REQUIRE_FILTER_CHANGE:
            state.write().setString("RequireFilterChange");
            break;
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
            state.write().setString("RequireWhiteFilterChange");
            break;
        }
        realtimeEgress->emplaceData(SequenceName({}, "raw", "ZSTATE"), std::move(state), endTime,
                                    FP::undefined());
    }

    if (realtimeEgress != NULL)
        forceRealtimeStateEmit = false;

    logValue(startTime, endTime, "F1", std::move(outputFlags));
    logValue(startTime, endTime, "Q", std::move(Q));
    logValue(startTime, endTime, "Qt", std::move(Qt));
    logValue(startTime, endTime, "Ba" + wlCode, std::move(Ba));
    logValue(startTime, endTime, "Ip" + wlCode, std::move(Ip));
    logValue(startTime, endTime, "If" + wlCode, std::move(If));

    /* Force breaks in the data stream on filter changes */
    if (forceFilterBreak && requireFilterBreak) {
        realtimeValue(endTime, "L", Variant::Root(FP::undefined()));
        realtimeValue(endTime, "Ir" + wlCode, Variant::Root(FP::undefined()));
        requireFilterBreak = false;
    } else {
        logValue(startTime, endTime, "L", std::move(L));
        logValue(startTime, endTime, "Ir" + wlCode, std::move(Ir));
        requireFilterBreak = true;
    }
    forceFilterBreak = false;

    realtimeValue(endTime, "ZQ", std::move(ZQ));
    realtimeValue(endTime, "In" + wlCode, std::move(In));
    realtimeValue(endTime, "Ing" + wlCode,
                  Variant::Root(filterNormalizeIn->stabilityFactor()));

    return 0;
}

void AcquireRRPSAP1W::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
}

void AcquireRRPSAP1W::configAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}


void AcquireRRPSAP1W::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (FP::defined(frameTime))
        configAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 3) {
                qCDebug(log) << "Autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                timeoutAt(frameTime + config.first().reportInterval * 2.0 + 1.0);

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("AutoprobeWait");
                event(frameTime, QObject::tr("Autoprobe succeeded."), false, info);

                forceRealtimeStateEmit = true;
            }
        } else if (code > 0) {
            qCDebug(log) << "Autoprobe failed at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            responseState = RESP_WAIT;
            autoprobeValidRecords = 0;

            if (realtimeEgress != NULL && FP::defined(frameTime)) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        } else {
            autoprobeValidRecords = 0;

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    case RESP_WAIT:
    case RESP_INITIALIZE:
        if (processRecord(frame, frameTime) == 0) {
            qCDebug(log) << "Comms established at" << Logging::time(frameTime);

            responseState = RESP_RUN;
            ++autoprobeValidRecords;
            generalStatusUpdated();

            timeoutAt(frameTime + config.first().reportInterval * 2.0 + 1.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("Wait");
            event(frameTime, QObject::tr("Communications established."), false, info);

            forceRealtimeStateEmit = true;
        } else {
            autoprobeValidRecords = 0;
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;

    case RESP_RUN: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + config.first().reportInterval * 2.0 + 1.0);
            ++autoprobeValidRecords;
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            responseState = RESP_WAIT;
            discardData(frameTime + 0.5, 1);
            generalStatusUpdated();
            autoprobeValidRecords = 0;

            Variant::Write info = Variant::Write::empty();
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            info.hash("ResponseState").setString("Run");
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            if (realtimeEgress != NULL && FP::defined(frameTime)) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    default:
        break;
    }
}

void AcquireRRPSAP1W::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    switch (responseState) {
    case RESP_RUN: {
        qCDebug(log) << "Timeout in unpolled mode at" << Logging::time(frameTime);

        responseState = RESP_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        discardData(frameTime + 2.0, 1);
        generalStatusUpdated();
        autoprobeValidRecords = 0;

        Variant::Write info = Variant::Write::empty();
        info.hash("ResponseState").setString("Run");
        event(frameTime, QObject::tr("Timeout waiting for report.  Communications dropped."), true,
              info);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;
    }

    case RESP_AUTOPROBE_WAIT:
        qCDebug(log) << "Timeout waiting for autoprobe records at " << Logging::time(frameTime);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (controlStream != NULL)
            controlStream->resetControl();
        discardData(frameTime + 2.0, 1);
        responseState = RESP_WAIT;
        autoprobeValidRecords = 0;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_INITIALIZE:
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + config.first().reportInterval * 3.0 + 1.0);
        responseState = RESP_WAIT;
        break;

    case RESP_AUTOPROBE_INITIALIZE:
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + config.first().reportInterval * 3.0 + 1.0);
        responseState = RESP_AUTOPROBE_WAIT;
        break;

    default:
        discardData(frameTime + 0.5, 1);
        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;
    }
}

void AcquireRRPSAP1W::discardDataCompleted(double frameTime)
{
    Q_UNUSED(frameTime);
}

void AcquireRRPSAP1W::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frameTime);
    Q_UNUSED(frame);
}

SequenceValue::Transfer AcquireRRPSAP1W::getPersistentValues()
{
    PauseLock paused(*this);
    SequenceValue::Transfer result;

    if (FP::defined(filterStart.startTime)) {
        result.emplace_back(SequenceName({}, "raw", "ZFILTER"), buildFilterValue(filterStart),
                            filterStart.startTime, FP::undefined());
    }
    if (FP::defined(filterWhite.startTime)) {
        result.emplace_back(SequenceName({}, "raw", "ZWHITE"), buildFilterValue(filterWhite),
                            filterWhite.startTime, FP::undefined());
    }
    if (haveAcceptedNormalization && FP::defined(normalizationStartTime)) {
        result.emplace_back(SequenceName({}, "raw", "ZSPOT"), buildNormalizationValue(),
                            normalizationStartTime, FP::undefined());

        result.emplace_back(SequenceName({}, "raw", "Ff"),
                            Variant::Root(static_cast<qint64>(std::floor(normalizationStartTime))),
                            normalizationStartTime, FP::undefined());
    }

    return result;
}

Variant::Root AcquireRRPSAP1W::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireRRPSAP1W::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

void AcquireRRPSAP1W::command(const Variant::Read &command)
{

    if (command.hash("StartFilterChange").exists()) {
        switch (sampleState) {
        case SAMPLE_RUN:
        case SAMPLE_FILTER_NORMALIZE:
        case SAMPLE_WHITE_FILTER_NORMALIZE:
        case SAMPLE_REQUIRE_FILTER_CHANGE:
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
            qCDebug(log) << "Manual filter change started in state" << sampleState;

            {
                Variant::Write info = Variant::Write::empty();
                switch (sampleState) {
                case SAMPLE_RUN:
                    info.hash("State").setString("Run");
                    break;
                case SAMPLE_FILTER_NORMALIZE:
                    info.hash("State").setString("Normalize");
                    insertDiagnosticNormalization(info);
                    break;
                case SAMPLE_WHITE_FILTER_NORMALIZE:
                    info.hash("State").setString("WhiteFilterNormalize");
                    insertDiagnosticNormalization(info);
                    break;
                case SAMPLE_REQUIRE_FILTER_CHANGE:
                    info.hash("State").setString("RequireFilterChange");
                    break;
                case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
                    info.hash("State").setString("RequireWhiteFilterChange");
                    break;
                default:
                    break;
                }
                event(Time::time(), QObject::tr("Manual filter change started."), true, info);
            }
            sampleState = SAMPLE_FILTER_CHANGING;
            forceRealtimeStateEmit = true;
            forceFilterBreak = true;
            resetSmoothers();

            filterNormalization = FP::undefined();
            normalizationStartTime = FP::undefined();
            lastTransmittanceCheckTime = FP::undefined();
            break;

            /* No effect in these states (already changing) */
        case SAMPLE_FILTER_CHANGING:
        case SAMPLE_WHITE_FILTER_CHANGING:
            qCDebug(log) << "Discarding filter change start in state" << sampleState;
            break;
        }
    }

    if (command.hash("StartWhiteFilterChange").exists()) {
        switch (sampleState) {
        case SAMPLE_RUN:
        case SAMPLE_FILTER_NORMALIZE:
        case SAMPLE_WHITE_FILTER_NORMALIZE:
        case SAMPLE_REQUIRE_FILTER_CHANGE:
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
            qCDebug(log) << "White filter change started in state" << sampleState;

            {
                Variant::Write info = Variant::Write::empty();
                switch (sampleState) {
                case SAMPLE_RUN:
                    info.hash("State").setString("Run");
                    break;
                case SAMPLE_FILTER_NORMALIZE:
                    info.hash("State").setString("Normalize");
                    insertDiagnosticNormalization(info);
                    break;
                case SAMPLE_WHITE_FILTER_NORMALIZE:
                    info.hash("State").setString("WhiteFilterNormalize");
                    insertDiagnosticNormalization(info);
                    break;
                case SAMPLE_REQUIRE_FILTER_CHANGE:
                    info.hash("State").setString("RequireFilterChange");
                    break;
                case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
                    info.hash("State").setString("RequireWhiteFilterChange");
                    break;
                default:
                    break;
                }
                event(Time::time(), QObject::tr("White filter change started."), true, info);
            }
            sampleState = SAMPLE_WHITE_FILTER_CHANGING;
            forceRealtimeStateEmit = true;
            whiteFilterChangeStartTime = FP::undefined();
            forceFilterBreak = true;
            resetSmoothers();
            normalizationStartTime = FP::undefined();
            lastTransmittanceCheckTime = FP::undefined();
            filterNormalization = FP::undefined();

            break;

            /* Already changing, to just switch a white check */
        case SAMPLE_FILTER_CHANGING:
            qCDebug(log) << "Switch to white filter change";

            {
                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("FilterChanging");
                insertDiagnosticFilterNormalization(info);
                event(Time::time(), QObject::tr("Switching to white filter change mode."), true,
                      info);
            }

            sampleState = SAMPLE_WHITE_FILTER_CHANGING;
            forceRealtimeStateEmit = true;
            whiteFilterChangeStartTime = FP::undefined();
            break;

            /* No effect */
        case SAMPLE_WHITE_FILTER_CHANGING:
            qCDebug(log) << "Discarding white filter change start";
            break;
        }
    }

    if (command.hash("StopFilterChange").exists()) {
        switch (sampleState) {
        case SAMPLE_FILTER_CHANGING:
            qCDebug(log) << "Filter change ended";
            {
                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("FilterChanging");
                insertDiagnosticFilterNormalization(info);
                event(Time::time(), QObject::tr("Filter change ended."), true, info);
            }

            sampleState = SAMPLE_FILTER_NORMALIZE;
            forceRealtimeStateEmit = true;
            resetSmoothers();
            break;

        case SAMPLE_WHITE_FILTER_CHANGING:
            qCDebug(log) << "White filter change ended";
            {
                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("WhiteFilterChanging");
                insertDiagnosticFilterNormalization(info);
                event(Time::time(), QObject::tr("White filter change ended."), true, info);
            }

            sampleState = SAMPLE_WHITE_FILTER_NORMALIZE;
            forceRealtimeStateEmit = true;
            resetSmoothers();
            break;

            /* No effect */
        case SAMPLE_RUN:
        case SAMPLE_FILTER_NORMALIZE:
        case SAMPLE_WHITE_FILTER_NORMALIZE:
        case SAMPLE_REQUIRE_FILTER_CHANGE:
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
            qCDebug(log) << "Discarding filter change stop in state" << sampleState;
            break;
        }
    }
}

Variant::Root AcquireRRPSAP1W::getCommands()
{
    Variant::Root result;

    result["StartFilterChange"].hash("DisplayName").setString("Start &Filter Change");
    result["StartFilterChange"].hash("DisplayPriority").setInt64(-1);
    result["StartFilterChange"].hash("ToolTip")
                               .setString(
                                       "Start a filter change.  This will disable flow and allow you to change the filter in the instrument.");
    result["StartFilterChange"].hash("Exclude").array(0).hash("Type").setString("Flags");
    result["StartFilterChange"].hash("Exclude").array(0).hash("Flags").setFlags({"FilterChanging"});
    result["StartFilterChange"].hash("Exclude").array(0).hash("Variable").setString("F1");

    result["StopFilterChange"].hash("DisplayName").setString("End &Filter Change");
    result["StopFilterChange"].hash("DisplayPriority").setInt64(-1);
    result["StopFilterChange"].hash("ToolTip")
                              .setString(
                                      "This tells the instrument you are done changing the filter and it should resume sampling.");
    result["StopFilterChange"].hash("Include").array(0).hash("Type").setString("Flags");
    result["StopFilterChange"].hash("Include").array(0).hash("Flags").setFlags({"FilterChanging"});
    result["StopFilterChange"].hash("Include").array(0).hash("Variable").setString("F1");

    result["StartWhiteFilterChange"].hash("DisplayName").setString("Start a &White Filter Change");
    result["StartWhiteFilterChange"].hash("DisplayPriority").setInt64(1);
    result["StartWhiteFilterChange"].hash("ToolTip")
                                    .setString(
                                            "Start a filter change to a known to be white filter.  You should double check to make sure that only a single filter is placed in the instrument.");
    result["StartWhiteFilterChange"].hash("Exclude").array(0).hash("Type").setString("Flags");
    result["StartWhiteFilterChange"].hash("Exclude")
                                    .array(0)
                                    .hash("Flags")
                                    .setFlags({"WhiteFilterChanging"});
    result["StartWhiteFilterChange"].hash("Exclude").array(0).hash("Variable").setString("F1");

    return result;
}

AcquisitionInterface::AutoprobeStatus AcquireRRPSAP1W::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireRRPSAP1W::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_RUN:
        if (autoprobeValidRecords > 3) {
            /* Already running, just notify that we succeeded. */
            qCDebug(log) << "Interactive autoprobe requested with communications established at"
                         << Logging::time(time);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            break;
        }
        /* Fall through */

    default:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + config.first().reportInterval * 4.0 + 5.0);
        discardData(time + 0.5, 1);
        responseState = RESP_AUTOPROBE_WAIT;
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Wait"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireRRPSAP1W::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    timeoutAt(time + config.first().reportInterval * 5.0 + 5.0);
    discardData(time + 0.5, 1);
    responseState = RESP_AUTOPROBE_WAIT;
    generalStatusUpdated();
}

void AcquireRRPSAP1W::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + config.first().reportInterval * 2.0 + 1.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to interactive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from passive autoprobe to interactive acquisition at"
                     << Logging::time(time);

        responseState = RESP_WAIT;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Wait"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireRRPSAP1W::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + config.first().reportInterval * 2.0 + 1.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from passive autoprobe to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_WAIT;
        break;
    }
}

static const quint16 serializedStateVersion = 1;

void AcquireRRPSAP1W::serializeState(QDataStream &stream)
{
    PauseLock paused(*this);

    stream << serializedStateVersion;

    stream << accumulatedVolume << filterNormalization << filterIsNotWhite << normalizationStartTime
           << normalizationLatest << filterStart << filterWhite;
}

void AcquireRRPSAP1W::deserializeState(QDataStream &stream)
{
    quint16 version = 0;
    stream >> version;
    if (version != serializedStateVersion)
        return;

    PauseLock paused(*this);

    stream >> accumulatedVolume >> filterNormalization >> filterIsNotWhite >> normalizationStartTime
           >> normalizationLatest >> filterStart >> filterWhite;
    haveAcceptedNormalization = false;
}

AcquisitionInterface::AutomaticDefaults AcquireRRPSAP1W::getDefaults()
{
    AutomaticDefaults result;
    result.name = "A$1$2";
    result.setSerialN81(9600);
    return result;
}


ComponentOptions AcquireRRPSAP1WComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);
    return options;
}

ComponentOptions AcquireRRPSAP1WComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());
    return options;
}

QList<ComponentExample> AcquireRRPSAP1WComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples(true));
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireRRPSAP1WComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireRRPSAP1WComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireRRPSAP1WComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(
            ComponentExample(options, tr("Convert data with the default area and calibrations")));

    options = getBaseOptions();
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("area")))->set(18.01);
    examples.append(ComponentExample(options, tr("Explicitly defined area")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireRRPSAP1WComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                 const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireRRPSAP1W(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireRRPSAP1WComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                 const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireRRPSAP1W(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireRRPSAP1WComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                   const std::string &loggingContext)
{
    std::unique_ptr<AcquireRRPSAP1W> i(new AcquireRRPSAP1W(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireRRPSAP1WComponent::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{
    std::unique_ptr<AcquireRRPSAP1W> i(new AcquireRRPSAP1W(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
