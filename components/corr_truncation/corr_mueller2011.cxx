/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QRegExp>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "core/environment.hxx"

#include "corr_mueller2011.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;
using namespace CPD3::Editing;

static const Variant::Flag FLAG_NAME = "Mueller2011";

void CorrMueller2011::handleOptions(const ComponentOptions &options)
{
    if (options.isSet("tsi")) {
        defaultTSI = qobject_cast<ComponentOptionBoolean *>(options.get("tsi"))->get();
    } else {
        defaultTSI = false;
    }
}

CorrMueller2011::CorrMueller2011(const ComponentOptions &options) : CorrTruncation(FLAG_NAME,
                                                                                   options),
                                                                    defaultTSI(false)
{
    handleOptions(options);
}

CorrMueller2011::CorrMueller2011(const ComponentOptions &options,
                                 double start,
                                 double end, const QList<SequenceName> &inputs) : CorrTruncation(
        FLAG_NAME,
        options,
        start, end,
        inputs),
                                                                                  defaultTSI(false)
{
    handleOptions(options);
}

CorrMueller2011::CorrMueller2011(double start,
                                 double end,
                                 const SequenceName::Component &station,
                                 const SequenceName::Component &archive,
                                 const ValueSegment::Transfer &config)
        : CorrTruncation(FLAG_NAME,
                         start, end,
                         station,
                         archive,
                         config),
          defaultTSI(false)
{
    QStringList children(getProcessingNameOrder());
    for (QStringList::const_iterator child = children.constBegin(), endC = children.constEnd();
            child != endC;
            ++child) {
        Processing p;

        p.useTSI = DynamicBoolOption::fromConfiguration(config,
                                                        QString("%1/Constants/TSI").arg(*child),
                                                        start, end);

        processing.push_back(p);
    }
}

void CorrMueller2011Component::extendBasicFilterEditing(double &start,
                                                        double &end,
                                                        const SequenceName::Component &,
                                                        const SequenceName::Component &,
                                                        const ValueSegment::Transfer &config,
                                                        Archive::Access *)
{
    if (!FP::defined(start))
        return;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    Variant::Root defaultAdjust;
    defaultAdjust["Smoothing/Type"].setString("SinglePoleLowPass");
    defaultAdjust["Smoothing/TimeConstant/Units"].setString("Minute");
    defaultAdjust["Smoothing/TimeConstant/Count"].setInt64(3);
    defaultAdjust["Smoothing/Gap/Units"].setString("Minute");
    defaultAdjust["Smoothing/Gap/Count"].setInt64(35);
    defaultAdjust["Smoothing/ResetOnUndefined"].setBool(false);

    double result = start;
    for (const auto &child : children) {
        WavelengthAdjust *wl = WavelengthAdjust::fromConfiguration(config,
                                                                   QString("%1/Smoothing").arg(
                                                                           QString::fromStdString(
                                                                                   child)),
                                                                   start, end, defaultAdjust);
        double check = wl->spinupTime(start);
        if (FP::defined(check) && check < result)
            result = check;
        delete wl;
    }
    start = result;
}


CorrMueller2011::~CorrMueller2011()
{
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        delete p->useTSI;
    }
}

CorrMueller2011::CorrMueller2011(QDataStream &stream) : CorrTruncation(FLAG_NAME, stream)
{
    stream >> defaultTSI;

    quint32 n;
    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        Processing p;
        stream >> p.useTSI;
        stream >> p.latestTSI;
    }
}

void CorrMueller2011::serialize(QDataStream &stream)
{
    CorrTruncation::serialize(stream);

    stream << defaultTSI;

    quint32 n = (quint32) processing.size();
    stream << n;
    for (int i = 0; i < (int) n; i++) {
        stream << processing[i].useTSI;
        stream << processing[i].latestTSI;
    }
}

void CorrMueller2011::process(int id, SequenceSegment &data)
{
    if (id < (int) processing.size()) {
        Processing *p = &processing[id];
        bool isTSI = p->useTSI->get(data);
        if (isTSI != p->latestTSI) {
            p->latestTSI = isTSI;
            invalidateConstantCache(id);
        }
    }
    CorrTruncation::process(id, data);
}

namespace {
class Defaults {
public:
    QMap<double, QList<CorrTruncation::WavelengthConstants> > scattering;
    QMap<double, QList<CorrTruncation::WavelengthConstants> > backscattering;

    Defaults(bool tsi)
    {
        if (tsi) {
            scattering[450] <<
                    CorrTruncation::WavelengthConstants::coarse(1.30, Calibration(
                            QList<double>() << 1.345 << -0.146));
            scattering[450] <<
                    CorrTruncation::WavelengthConstants::fine(1.086, Calibration(
                            QList<double>() << 1.148 << -0.041));

            scattering[550] <<
                    CorrTruncation::WavelengthConstants::coarse(1.29, Calibration(
                            QList<double>() << 1.319 << -0.129));
            scattering[550] <<
                    CorrTruncation::WavelengthConstants::fine(1.066, Calibration(
                            QList<double>() << 1.137 << -0.040));

            scattering[700] <<
                    CorrTruncation::WavelengthConstants::coarse(1.26, Calibration(
                            QList<double>() << 1.279 << -0.105));
            scattering[700] <<
                    CorrTruncation::WavelengthConstants::fine(1.045, Calibration(
                            QList<double>() << 1.109 << -0.033));


            backscattering[450] << CorrTruncation::WavelengthConstants::coarse(0.983);
            backscattering[450] << CorrTruncation::WavelengthConstants::fine(0.950);

            backscattering[550] << CorrTruncation::WavelengthConstants::coarse(0.984);
            backscattering[550] << CorrTruncation::WavelengthConstants::fine(0.944);

            backscattering[700] << CorrTruncation::WavelengthConstants::coarse(0.988);
            backscattering[700] << CorrTruncation::WavelengthConstants::fine(0.954);
        } else {
            scattering[450] <<
                    CorrTruncation::WavelengthConstants::coarse(1.37, Calibration(
                            QList<double>() << 1.455 << -0.189));
            scattering[450] <<
                    CorrTruncation::WavelengthConstants::fine(1.125, Calibration(
                            QList<double>() << 1.213 << -0.060));

            scattering[525] <<
                    CorrTruncation::WavelengthConstants::coarse(1.38, Calibration(
                            QList<double>() << 1.434 << -0.176));
            scattering[525] <<
                    CorrTruncation::WavelengthConstants::fine(1.103, Calibration(
                            QList<double>() << 1.207 << -0.061));

            scattering[635] <<
                    CorrTruncation::WavelengthConstants::coarse(1.36, Calibration(
                            QList<double>() << 1.403 << -0.156));
            scattering[635] <<
                    CorrTruncation::WavelengthConstants::fine(1.078, Calibration(
                            QList<double>() << 1.176 << -0.053));


            backscattering[450] << CorrTruncation::WavelengthConstants::coarse(0.963);
            backscattering[450] << CorrTruncation::WavelengthConstants::fine(0.932);

            backscattering[525] << CorrTruncation::WavelengthConstants::coarse(0.971);
            backscattering[525] << CorrTruncation::WavelengthConstants::fine(0.935);

            backscattering[635] << CorrTruncation::WavelengthConstants::coarse(0.968);
            backscattering[635] << CorrTruncation::WavelengthConstants::fine(0.935);
        }
    }
};
}

static const Defaults defaults3563(true);
static const Defaults defaultsAurora3000(false);

template<typename K>
static K lookupNearest(double v, const QMap<double, K> &map)
{
    typename QMap<double, K>::const_iterator lb = map.lowerBound(v);
    if (lb == map.end()) {
        if (lb == map.begin())
            return K();
        return (lb - 1).value();
    } else if (lb == map.begin()) {
        Q_ASSERT(!map.isEmpty());
        return lb.value();
    }
    typename QMap<double, K>::const_iterator lb1 = lb - 1;
    if (fabs(lb.key() - v) < fabs(lb1.key() - v))
        return lb.value();
    return lb1.value();
}

QList<CorrTruncation::WavelengthConstants> CorrMueller2011::getDefaults(double wavelength,
                                                                        bool isBackscatter,
                                                                        int id) const
{
    if (!FP::defined(wavelength))
        return QList<CorrTruncation::WavelengthConstants>();
    bool isTSI = defaultTSI;
    if (id < (int) processing.size())
        isTSI = processing[id].latestTSI;
    if (isTSI) {
        if (isBackscatter)
            return lookupNearest(wavelength, defaults3563.backscattering);
        return lookupNearest(wavelength, defaults3563.scattering);
    } else {
        if (isBackscatter)
            return lookupNearest(wavelength, defaultsAurora3000.backscattering);
        return lookupNearest(wavelength, defaultsAurora3000.scattering);
    }
}

Variant::Root CorrMueller2011::getProcessingMetadata() const
{
    Variant::Root meta;
    meta["By"].setString("corr_mueller2011");
    meta["Revision"].setString(Environment::revision());
    return meta;
}

Variant::Root CorrMueller2011::getFlagMetadata() const
{
    Variant::Root meta;
    meta["Origin"].toArray().after_back().setString("corr_mueller2011");
    meta["Bits"].setInt64(0x0400);
    meta["Description"].setString("Mueller 2011 truncation correction applied");
    return meta;
}


QString CorrMueller2011Component::getBasicSerializationName() const
{ return QString::fromLatin1("corr_mueller2011"); }

ComponentOptions CorrMueller2011Component::getOptions()
{
    ComponentOptions options;

    CorrMueller2011::injectOptions(options);

    options.add("tsi", new ComponentOptionBoolean(tr("tsi", "name"), tr("Use TSI 3563 constants"),
                                                  tr("When enabled the correction constants used correspond to the "
                                                      "TSI 3563 nephelometer instead of the Ecotech Aurora 3000."),
                                                  QString()));


    return options;
}

QList<ComponentExample> CorrMueller2011Component::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will correct all scatterings and backscatterings using "
                                                "the default smoothing.  The constants used in the fits and "
                                                "no-angstrom multiplies are those described in M\xC3\xBCller "
                                                "et al (2011).")));

    options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")))->add("S11");
    examples.append(ComponentExample(options, tr("Single instrument"),
                                     tr("This will correct all scatterings and backscatterings for S11.")));

    options = getOptions();
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("backscatter")))->set("", "",
                                                                                      "BbsG_S11");
    examples.append(ComponentExample(options, tr("Single variable"),
                                     tr("This will correct only the variable BbsG_S11.")));

    return examples;
}

SegmentProcessingStage *CorrMueller2011Component::createBasicFilterDynamic(const ComponentOptions &options)
{ return new CorrMueller2011(options); }

SegmentProcessingStage *CorrMueller2011Component::createBasicFilterPredefined(const ComponentOptions &options,
                                                                              double start,
                                                                              double end,
                                                                              const QList<
                                                                           SequenceName> &inputs)
{ return new CorrMueller2011(options, start, end, inputs); }

SegmentProcessingStage *CorrMueller2011Component::createBasicFilterEditing(double start,
                                                                           double end,
                                                                           const SequenceName::Component &station,
                                                                           const SequenceName::Component &archive,
                                                                           const ValueSegment::Transfer &config)
{ return new CorrMueller2011(start, end, station, archive, config); }

SegmentProcessingStage *CorrMueller2011Component::deserializeBasicFilter(QDataStream &stream)
{ return new CorrMueller2011(stream); }
