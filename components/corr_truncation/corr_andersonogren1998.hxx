/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CORRANDERSONOGREN1998_H
#define CORRANDERSONOGREN1998_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QMultiMap>
#include <QHash>

#include "core/component.hxx"
#include "truncation.hxx"

class CorrAndersonOgren1998 : public CorrTruncation {
public:
    CorrAndersonOgren1998(const CPD3::ComponentOptions &options);

    CorrAndersonOgren1998(const CPD3::ComponentOptions &options,
                          double start,
                          double end, const QList<CPD3::Data::SequenceName> &inputs);

    CorrAndersonOgren1998(double start,
                          double end,
                          const CPD3::Data::SequenceName::Component &station,
                          const CPD3::Data::SequenceName::Component &archive,
                          const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CorrAndersonOgren1998();

    CorrAndersonOgren1998(QDataStream &stream);

protected:

    virtual QList<WavelengthConstants>
            getDefaults(double wavelength, bool isBackscatter, int id) const;

    virtual CPD3::Data::Variant::Root getProcessingMetadata() const;

    virtual CPD3::Data::Variant::Root getFlagMetadata() const;

};

class CorrAndersonOgren1998Component
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.corr_andersonogren1998"
                              FILE
                              "corr_andersonogren1998.json")

public:
    virtual QString getBasicSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Data::SegmentProcessingStage
            *createBasicFilterDynamic(const CPD3::ComponentOptions &options);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined
            (const CPD3::ComponentOptions &options,
             double start,
             double end, const QList<CPD3::Data::SequenceName> &inputs);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                         double end,
                                                                         const CPD3::Data::SequenceName::Component &station,
                                                                         const CPD3::Data::SequenceName::Component &archive,
                                                                         const CPD3::Data::ValueSegment::Transfer &config);

    virtual void extendBasicFilterEditing(double &start,
                                          double &end,
                                          const CPD3::Data::SequenceName::Component &station,
                                          const CPD3::Data::SequenceName::Component &archive,
                                          const CPD3::Data::ValueSegment::Transfer &config,
                                          CPD3::Data::Archive::Access *access);

    virtual CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream);
};

#endif
