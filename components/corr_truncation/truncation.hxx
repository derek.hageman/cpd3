/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CORRTRUNCATION_H
#define CORRTRUNCATION_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QMultiMap>
#include <QHash>

#include "core/component.hxx"
#include "core/number.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/stream.hxx"
#include "datacore/wavelength.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "smoothing/baseline.hxx"
#include "editing/wavelengthadjust.hxx"

class CorrTruncation : public CPD3::Data::SegmentProcessingStage {
public:
    struct WavelengthConstants {
        CPD3::Data::SequenceName::Flavors hasFlavors;
        CPD3::Data::SequenceName::Flavors lacksFlavors;

        double coefficient;
        CPD3::Calibration angstromFit;

        WavelengthConstants() : hasFlavors(),
                  lacksFlavors(),
                  coefficient(CPD3::FP::undefined()),
                  angstromFit(QVector<double>())
        { }

        WavelengthConstants(const CPD3::Data::Variant::Read &configuration);

        static QList<WavelengthConstants> coarse(double coefficient,
                                                 const CPD3::Calibration &angstromFit = CPD3::Calibration(
                                                         QVector<double>()));

        static QList<WavelengthConstants> fine(double coefficient,
                                               const CPD3::Calibration &angstromFit = CPD3::Calibration(
                                                       QVector<double>()));
    };

    struct WavelengthTimeConstants : public CPD3::Time::Bounds {
        QMap<double, QList<WavelengthConstants> > constants;

        WavelengthTimeConstants(double start,
                                double end,
                                const CPD3::Data::Variant::Read &configuration);

        WavelengthTimeConstants() : constants()
        { }
    };

    struct UnitLookupCacheElement {
        double wavelength;
        QList<WavelengthConstants> constants;
    };

    enum ConstantMode {
        Constant_Default,
        Constant_PM1,
        Constant_Total,
        Constant_IgnoreAngstrom,
        Constant_MaximumRatio,
        Constant_MinimumRatio,
    };
private:

    CPD3::Data::Variant::Flag flagName;
    QStringList processingSetNames;

    static ConstantMode toConstantMode(const CPD3::Data::Variant::Read &value);

    class CorrectionSet {
    public:
        CPD3::Data::DynamicSequenceSelection *operate;
        CPD3::Editing::WavelengthAdjust *lookup;
        QList<WavelengthTimeConstants> constants;
        QHash<CPD3::Data::SequenceName, UnitLookupCacheElement> cache;

        CorrectionSet() : operate(NULL), lookup(NULL), constants(), cache()
        { }
    };

    class Processing {
    public:
        CPD3::Data::DynamicSequenceSelection *operateFlags;

        CorrectionSet scatter;
        CorrectionSet backscatter;

        CPD3::Data::DynamicPrimitive<ConstantMode> *mode;
        ConstantMode priorDefaultMode;

        Processing() : operateFlags(NULL),
                       scatter(),
                       backscatter(),
                       mode(NULL),
                       priorDefaultMode(Constant_Default)
        { }
    };

    CPD3::Smoothing::BaselineSmoother *defaultWavelengthSmoother;
    ConstantMode defaultConstantMode;
    CPD3::Data::SequenceName::ComponentSet filterSuffixes;

    bool restrictedInputs;

    void handleOptions(const CPD3::ComponentOptions &options);

    std::vector<Processing> processing;

    static QList<WavelengthConstants> reduceToUnit(const CPD3::Data::SequenceName &unit,
                                                   const QList<WavelengthConstants> &input,
                                                   ConstantMode mode);

    void processOutput(CPD3::Data::Variant::Write &value,
                       double angstrom,
                       const QList<WavelengthConstants> &constants,
                       ConstantMode mode);

    void processChannel(int id, CPD3::Data::SequenceSegment &data,
                        CorrectionSet &set,
                        ConstantMode mode,
                        bool isBackscatter);

    CorrTruncation();

public:

    CorrTruncation(const CPD3::Data::Variant::Flag &flagName,
                   const CPD3::ComponentOptions &options);

    CorrTruncation(const CPD3::Data::Variant::Flag &flagName,
                   const CPD3::ComponentOptions &options,
                   double start,
                   double end, const QList<CPD3::Data::SequenceName> &inputs);

    CorrTruncation(const CPD3::Data::Variant::Flag &fn,
                   double start,
                   double end,
                   const CPD3::Data::SequenceName::Component &station,
                   const CPD3::Data::SequenceName::Component &archive,
                   const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CorrTruncation();

    void unhandled(const CPD3::Data::SequenceName &unit,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    QSet<double> metadataBreaks(int id) override;

    void serialize(QDataStream &stream) override;

    static void injectOptions(CPD3::ComponentOptions &options);

protected:
    CorrTruncation(const CPD3::Data::Variant::Flag &flagName, QDataStream &stream);

    QStringList getProcessingNameOrder() const
    { return processingSetNames; }

    void invalidateConstantCache(int id);

    virtual QList<WavelengthConstants>
            getDefaults(double wavelength, bool isBackscatter, int id) const = 0;

    virtual CPD3::Data::Variant::Root getProcessingMetadata() const = 0;

    virtual CPD3::Data::Variant::Root getFlagMetadata() const = 0;
};

#endif
