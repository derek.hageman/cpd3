/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QRegExp>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/variant/composite.hxx"
#include "core/environment.hxx"

#include "truncation.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;
using namespace CPD3::Editing;

static BaselineSmoother *defaultSmoother()
{
    return new BaselineDigitalFilter(
            new DigitalFilterSinglePoleLowPass(new DynamicTimeInterval::Constant(Time::Minute, 3),
                                               new DynamicTimeInterval::Constant(Time::Minute, 35),
                                               false));
}

void CorrTruncation::handleOptions(const ComponentOptions &options)
{
    if (options.isSet("smoothing")) {
        defaultWavelengthSmoother =
                qobject_cast<BaselineSmootherOption *>(options.get("smoothing"))->getSmoother();
    } else {
        defaultWavelengthSmoother = NULL;
    }

    restrictedInputs = false;

    if (options.isSet("mode")) {
        defaultConstantMode =
                (ConstantMode) (qobject_cast<ComponentOptionEnum *>(options.get("mode")))->get()
                                                                                         .getID();
    } else {
        defaultConstantMode = Constant_Default;
    }

    if (options.isSet("scatter") || options.isSet("backscatter")) {
        restrictedInputs = true;

        Processing p;

        SequenceName::Component station;
        SequenceName::Component archive;
        SequenceName::Component suffix;
        SequenceName::Flavors flavors;
        if (options.isSet("scatter")) {
            p.scatter.operate = qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("scatter"))->getOperator();
            for (const auto &name : p.scatter.operate->getAllUnits()) {
                suffix = Util::suffix(name.getVariable(), '_');
                station = name.getStation();
                archive = name.getArchive();
                flavors = name.getFlavors();
                if (suffix.length() != 0)
                    break;
            }
        } else {
            p.scatter.operate = new DynamicSequenceSelection::None();
        }
        if (options.isSet("backscatter")) {
            p.backscatter.operate = qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("backscatter"))->getOperator();

            if (suffix.length() == 0) {
                for (const auto &name : p.backscatter.operate->getAllUnits()) {
                    suffix = Util::suffix(name.getVariable(), '_');
                    station = name.getStation();
                    archive = name.getArchive();
                    flavors = name.getFlavors();
                    if (suffix.length() != 0)
                        break;
                }
            }
        } else {
            p.backscatter.operate = new DynamicSequenceSelection::None();
        }

        p.scatter.lookup = WavelengthAdjust::fromInput(p.scatter.operate->clone(),
                                                       defaultWavelengthSmoother != NULL
                                                       ? defaultWavelengthSmoother->clone()
                                                       : defaultSmoother());

        p.backscatter.lookup = WavelengthAdjust::fromInput(p.backscatter.operate->clone(),
                                                           defaultWavelengthSmoother != NULL
                                                           ? defaultWavelengthSmoother->clone()
                                                           : defaultSmoother());

        p.mode = new DynamicPrimitive<ConstantMode>::Constant(defaultConstantMode);

        p.operateFlags = new DynamicSequenceSelection::Single(
                SequenceName(station, archive, "F1_" + suffix, flavors));

        processing.push_back(p);
    }

    if (options.isSet("suffix")) {
        filterSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->get());
    }
}

void CorrTruncation::injectOptions(ComponentOptions &options)
{
    ComponentOptionEnum *mode =
            new ComponentOptionEnum(QObject::tr("mode", "name"), QObject::tr("Correction mode"),
                                    QObject::tr(
                                            "This is the correction mode.  This determined which set or sets "
                                            "of coefficients to apply.  For example, this option can be used "
                                            "to correct data as if it where PM1 (to handle very fine, but not size "
                                            "selected data) or to use the maximum correction factor available (to "
                                            "handle multimodal data)."), QObject::tr("Automatic"),
                                    -1);
    mode->add(Constant_Default, "auto", QObject::tr("automatic", "mode name"),
              QObject::tr("Automatically select the constants to use"));
    mode->alias(Constant_Default, QObject::tr("automatic", "mode name"));
    mode->add(Constant_PM1, "pm1", QObject::tr("PM1", "mode name"),
              QObject::tr("Always use PM1 fine mode constants"));
    mode->alias(Constant_PM1, QObject::tr("fine", "mode name"));
    mode->add(Constant_Total, "pm10", QObject::tr("PM10", "mode name"),
              QObject::tr("Always use PM10 coarse mode constants"));
    mode->alias(Constant_Total, QObject::tr("coarse", "mode name"));
    mode->alias(Constant_Total, QObject::tr("total", "mode name"));
    mode->add(Constant_MaximumRatio, "maximum", QObject::tr("maximum", "mode name"),
              QObject::tr("Use the constants that result in the largest correction ratio"));
    mode->alias(Constant_MaximumRatio, QObject::tr("max", "mode name"));
    mode->add(Constant_MinimumRatio, "minimum", QObject::tr("minimum", "mode name"),
              QObject::tr("Use the constants that result in the smallest correction ratio"));
    mode->alias(Constant_MinimumRatio, QObject::tr("min", "mode name"));
    mode->add(Constant_IgnoreAngstrom, "noang", QObject::tr("noang"),
              QObject::tr("Ignore \xC3\x85ngstr\xC3\xB6m exponent fit", "mode name"));
    options.add("mode", mode);

    options.add("scatter", new DynamicSequenceSelectionOption(QObject::tr("scatterings", "name"),
                                                              QObject::tr("Scattering variables"),
                                                              QObject::tr(
                                                                      "These are the scattering variables to correct.  This option is "
                                                                      "mutually exclusive with instrument specification."),
                                                              QObject::tr("All scatterings")));
    options.add("backscatter",
                new DynamicSequenceSelectionOption(QObject::tr("backscatterings", "name"),
                                                   QObject::tr("Back scattering variables"),
                                                   QObject::tr(
                                                           "These are the backscattering variables to correct.  This option is "
                                                           "mutually exclusive with instrument specification."),
                                                   QObject::tr("All backscatterings")));
    options.add("suffix", new ComponentOptionInstrumentSuffixSet(QObject::tr("instruments", "name"),
                                                                 QObject::tr(
                                                                         "Instrument suffixes to correct"),
                                                                 QObject::tr(
                                                                         "These are the instrument suffixes to correct.  "
                                                                         "For example S11 would usually specifies the reference nephelometer.  "
                                                                         "This option is mutually exclusive with manual variable specification."),
                                                                 QObject::tr(
                                                                         "All instrument suffixes")));
    options.exclude("scatter", "suffix");
    options.exclude("backscatter", "suffix");
    options.exclude("suffix", "scatter");
    options.exclude("suffix", "backscatter");

    BaselineSmootherOption *smoothing = new BaselineSmootherOption(QObject::tr("smoothing", "name"),
                                                                   QObject::tr(
                                                                           "\xC3\x85ngstr\xC3\xB6m exponent smoothing"),
                                                                   QObject::tr(
                                                                           "This is the smoothing applied to the scatterings before "
                                                                           "calculation of the \xC3\x85ngstr\xC3\xB6m exponent."),
                                                                   QObject::tr(
                                                                           "3-minute TC single pole low pass"));
    smoothing->setStabilityDetection(false);
    smoothing->setSpikeDetection(false);
    smoothing->setDefault(defaultSmoother());
    options.add("smoothing", smoothing);
}


CorrTruncation::CorrTruncation()
{ Q_ASSERT(false); }

CorrTruncation::CorrTruncation(const Variant::Flag &fn, const ComponentOptions &options) : flagName(
        fn)
{
    handleOptions(options);
}

CorrTruncation::CorrTruncation(const Variant::Flag &fn,
                               const ComponentOptions &options,
                               double start,
                               double end,
                               const QList<SequenceName> &inputs) : flagName(fn)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    handleOptions(options);

    for (QList<SequenceName>::const_iterator unit = inputs.constBegin(), endU = inputs.constEnd();
            unit != endU;
            ++unit) {
        CorrTruncation::unhandled(*unit, NULL);
    }
}

CorrTruncation::ConstantMode CorrTruncation::toConstantMode(const Variant::Read &value)
{
    const auto &type = value.toString();
    if (Util::equal_insensitive(type, "pm1", "fine"))
        return Constant_PM1;
    else if (Util::equal_insensitive(type, "pm10", "total", "coarse"))
        return Constant_Total;
    else if (Util::equal_insensitive(type, "max", "maximum"))
        return Constant_MaximumRatio;
    else if (Util::equal_insensitive(type, "min", "minimum"))
        return Constant_MinimumRatio;
    else if (Util::equal_insensitive(type, "noang", "noangstrom"))
        return Constant_IgnoreAngstrom;
    return Constant_Default;
}

CorrTruncation::WavelengthConstants::WavelengthConstants(const Variant::Read &configuration)
        : hasFlavors(SequenceName::toFlavors(configuration.hash("HasFlavors"))),
          lacksFlavors(SequenceName::toFlavors(configuration.hash("LacksFlavors"))),
          coefficient(configuration.hash("Coefficient").toDouble()),
          angstromFit(QVector<double>())
{
    if (configuration.hash("Fit").exists())
        angstromFit = Variant::Composite::toCalibration(configuration.hash("Fit"));
}

QList<CorrTruncation::WavelengthConstants> CorrTruncation::WavelengthConstants::coarse(double coefficient,
                                                                                       const Calibration &angstromFit)
{
    QList<WavelengthConstants> result;
    CorrTruncation::WavelengthConstants add;
    add.coefficient = coefficient;
    add.angstromFit = angstromFit;
    add.lacksFlavors.insert("pm1");
    add.lacksFlavors.insert("pm25");
    result.append(add);
    return result;
}

QList<CorrTruncation::WavelengthConstants> CorrTruncation::WavelengthConstants::fine(double coefficient,
                                                                                     const Calibration &angstromFit)
{
    QList<WavelengthConstants> result;
    {
        CorrTruncation::WavelengthConstants add;
        add.coefficient = coefficient;
        add.angstromFit = angstromFit;
        add.lacksFlavors.insert("pm1");
        result.append(add);
    }
    {
        CorrTruncation::WavelengthConstants add;
        add.coefficient = coefficient;
        add.angstromFit = angstromFit;
        add.lacksFlavors.insert("pm25");
        result.append(add);
    }
    return result;
}


CorrTruncation::WavelengthTimeConstants::WavelengthTimeConstants(double start,
                                                                 double end,
                                                                 const Variant::Read &configuration)
        : Time::Bounds(start, end), constants()
{
    for (auto child : configuration.toKeyframe()) {
        if (!FP::defined(child.first))
            continue;
        switch (child.second.getType()) {
        case Variant::Type::Array:
        case Variant::Type::Matrix: {
            for (auto v : child.second.toChildren()) {
                constants[child.first].append(WavelengthConstants(v));
            }
            break;
        }
        default:
            constants.insert(child.first,
                             QList<WavelengthConstants>() << WavelengthConstants(child.second));
            break;
        }
    }
}

CorrTruncation::CorrTruncation(const Variant::Flag &fn,
                               double start,
                               double end,
                               const SequenceName::Component &station,
                               const SequenceName::Component &archive,
                               const ValueSegment::Transfer &config) : flagName(fn)
{
    defaultWavelengthSmoother = NULL;
    defaultConstantMode = Constant_Default;
    restrictedInputs = true;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    Variant::Root defaultSmoothing;
    defaultSmoothing["Type"].setString("SinglePoleLowPass");
    defaultSmoothing["TimeConstant/Units"].setString("Minute");
    defaultSmoothing["TimeConstant/Count"].setInt64(3);
    defaultSmoothing["Gap/Units"].setString("Minute");
    defaultSmoothing["Gap/Count"].setInt64(35);
    defaultSmoothing["ResetOnUndefined"].setBool(false);

    for (const auto &child : children) {
        Processing p;

        p.operateFlags = DynamicSequenceSelection::fromConfiguration(config,
                                                                     QString("%1/Flags").arg(
                                                                             QString::fromStdString(
                                                                                     child)), start,
                                                                     end);

        p.scatter.operate = DynamicSequenceSelection::fromConfiguration(config,
                                                                        QString("%1/CorrectScatter")
                                                                                .arg(QString::fromStdString(
                                                                                        child)),
                                                                        start, end);
        p.backscatter.operate = DynamicSequenceSelection::fromConfiguration(config,
                                                                            QString("%1/CorrectBackscatter")
                                                                                    .arg(QString::fromStdString(
                                                                                            child)),
                                                                            start, end);

        p.scatter.lookup = WavelengthAdjust::fromInput(p.scatter.operate->clone(),
                                                       BaselineSmoother::fromConfiguration(
                                                               defaultSmoothing, config,
                                                               QString("%1/Smoothing").arg(
                                                                       QString::fromStdString(
                                                                               child)), start,
                                                               end));
        p.backscatter.lookup = WavelengthAdjust::fromInput(p.backscatter.operate->clone(),
                                                           BaselineSmoother::fromConfiguration(
                                                                   defaultSmoothing, config,
                                                                   QString("%1/Smoothing").arg(
                                                                           QString::fromStdString(
                                                                                   child)), start,
                                                                   end));

        p.mode = DynamicPrimitive<ConstantMode>::fromConfiguration(toConstantMode, config,
                                                                   QString("%1/Mode").arg(
                                                                           QString::fromStdString(
                                                                                   child)), start,
                                                                   end);

        for (const auto &add : config) {
            auto constants =
                    add.getValue().hash(QString::fromStdString(child)).getPath("Constants/Scatter");
            if (constants.exists()) {
                p.scatter
                 .constants
                 .append(WavelengthTimeConstants(add.getStart(), add.getEnd(), constants));
            }

            constants = add.getValue()
                           .hash(QString::fromStdString(child))
                           .getPath("Constants/Backscatter");
            if (constants.exists()) {
                p.backscatter
                 .constants
                 .append(WavelengthTimeConstants(add.getStart(), add.getEnd(), constants));
            }
        }


        p.operateFlags->registerExpected(station, archive);
        p.scatter.operate->registerExpected(station, archive);
        p.scatter.lookup->registerExpected(station, archive);
        p.backscatter.operate->registerExpected(station, archive);
        p.backscatter.lookup->registerExpected(station, archive);

        processing.push_back(p);
        processingSetNames.append(QString::fromStdString(child));
    }
}

CorrTruncation::~CorrTruncation()
{
    if (defaultWavelengthSmoother != NULL)
        delete defaultWavelengthSmoother;

    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        delete p->operateFlags;
        delete p->mode;

        delete p->scatter.operate;
        delete p->scatter.lookup;

        delete p->backscatter.operate;
        delete p->backscatter.lookup;
    }
}

void CorrTruncation::unhandled(const SequenceName &unit,
                               SegmentProcessingStage::SequenceHandlerControl *control)
{
    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].scatter.lookup->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
        }
        if (processing[id].backscatter.lookup->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
        }
    }
    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].scatter.operate->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            return;
        }
        if (processing[id].backscatter.operate->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            return;
        }
        if (processing[id].operateFlags->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            return;
        }
    }

    if (restrictedInputs)
        return;

    if (unit.hasFlavor(SequenceName::flavor_cover) || unit.hasFlavor(SequenceName::flavor_stats))
        return;

    auto suffix = Util::suffix(unit.getVariable(), '_');
    if (suffix.empty())
        return;
    if (!filterSuffixes.empty() && !filterSuffixes.count(suffix))
        return;

    if (!Util::starts_with(unit.getVariable(), "Bs") &&
            !Util::starts_with(unit.getVariable(), "Bbs")) {
        if (control) {
            if (Util::starts_with(unit.getVariable(), "F1_")) {
                control->deferHandling(unit);
            }
        }
        return;
    }

    QRegExp reCheck("Bb?s[A-Z0-9]*_.+");
    if (!reCheck.exactMatch(unit.getVariableQString()))
        return;

    Processing p;

    p.scatter.operate =
            new DynamicSequenceSelection::Match(unit.getStationQString(), unit.getArchiveQString(),
                                                QString("Bs[A-Z0-9]*_%1").arg(QRegExp::escape(
                                                        QString::fromStdString(suffix))),
                                                unit.getFlavors());
    p.scatter.operate->registerInput(unit);
    p.backscatter.operate =
            new DynamicSequenceSelection::Match(unit.getStationQString(), unit.getArchiveQString(),
                                                QString("Bbs[A-Z0-9]*_%1").arg(QRegExp::escape(
                                                        QString::fromStdString(suffix))),
                                                unit.getFlavors());
    p.backscatter.operate->registerInput(unit);

    SequenceName flagUnit
            (unit.getStation(), unit.getArchive(), "F1_" + suffix, unit.getFlavors());
    p.operateFlags = new DynamicSequenceSelection::Single(flagUnit);
    p.operateFlags->registerInput(unit);
    p.operateFlags->registerInput(flagUnit);

    p.scatter.lookup = WavelengthAdjust::fromInput(p.scatter.operate->clone(),
                                                   defaultWavelengthSmoother != NULL
                                                   ? defaultWavelengthSmoother->clone()
                                                   : defaultSmoother());
    p.backscatter.lookup = WavelengthAdjust::fromInput(p.backscatter.operate->clone(),
                                                       defaultWavelengthSmoother != NULL
                                                       ? defaultWavelengthSmoother->clone()
                                                       : defaultSmoother());

    p.mode = new DynamicPrimitive<ConstantMode>::Constant(defaultConstantMode);


    int id = (int) processing.size();
    processing.push_back(p);

    if (control != NULL) {
        control->filterUnit(unit, id);
        if (unit != flagUnit)
            control->filterUnit(flagUnit, id);
    }
}

template<typename K>
static K lookupNearest(double v, const QMap<double, K> &map)
{
    typename QMap<double, K>::const_iterator lb = map.lowerBound(v);
    if (lb == map.end()) {
        if (lb == map.begin())
            return K();
        return (lb - 1).value();
    } else if (lb == map.begin()) {
        Q_ASSERT(!map.isEmpty());
        return lb.value();
    }
    typename QMap<double, K>::const_iterator lb1 = lb - 1;
    if (fabs(lb.key() - v) < fabs(lb1.key() - v))
        return lb.value();
    return lb1.value();
}

QList<CorrTruncation::WavelengthConstants> CorrTruncation::reduceToUnit(const SequenceName &unit,
                                                                        const QList<
                                                                                WavelengthConstants> &input,
                                                                        ConstantMode mode)
{
    QList<WavelengthConstants> result;
    for (QList<WavelengthConstants>::const_iterator add = input.constBegin(),
            endAdd = input.constEnd(); add != endAdd; ++add) {
        switch (mode) {
        case Constant_Default:
        case Constant_IgnoreAngstrom:
            break;
        case Constant_PM1:
            if (!add->hasFlavors.count(SequenceName::flavor_pm1))
                continue;
            result.append(*add);
            continue;
        case Constant_Total:
            if (add->hasFlavors.count(SequenceName::flavor_pm1))
                continue;
            if (add->hasFlavors.count(SequenceName::flavor_pm25))
                continue;
            result.append(*add);
            continue;
        case Constant_MaximumRatio:
        case Constant_MinimumRatio:
            result.append(*add);
            continue;
        }

        auto flavors = unit.getFlavors();
        bool ok = true;
        for (const auto &check : add->hasFlavors) {
            if (!flavors.count(check)) {
                ok = false;
                break;
            }
        }
        if (!ok)
            continue;

        ok = true;
        for (const auto &check : add->lacksFlavors) {
            if (flavors.count(check)) {
                ok = false;
                break;
            }
        }
        if (!ok)
            continue;

        result.append(*add);
    }

    return result;
}

void CorrTruncation::processOutput(Variant::Write &value,
                                   double angstrom,
                                   const QList<WavelengthConstants> &constants,
                                   ConstantMode mode)
{
    double v = value.toDouble();
    if (!FP::defined(v)) {
        value.setEmpty();
        return;
    }

    switch (mode) {
    case Constant_Default:
    case Constant_PM1:
    case Constant_Total: {
        double out = FP::undefined();
        for (QList<WavelengthConstants>::const_iterator c = constants.constBegin(),
                endC = constants.constEnd(); c != endC; ++c) {
            double af = c->angstromFit.apply(angstrom);
            if (FP::defined(af)) {
                out = v * af;
                break;
            }
            if (FP::defined(c->coefficient) && !FP::defined(out)) {
                out = v * c->coefficient;
            }
        }
        if (!FP::defined(out))
            value.setEmpty();
        else
            value.setDouble(out);
        break;
    }
    case Constant_IgnoreAngstrom: {
        double out = FP::undefined();
        for (QList<WavelengthConstants>::const_iterator c = constants.constBegin(),
                endC = constants.constEnd(); c != endC; ++c) {
            if (FP::defined(c->coefficient) && !FP::defined(out)) {
                out = v * c->coefficient;
                break;
            }
        }
        if (!FP::defined(out))
            value.setEmpty();
        else
            value.setDouble(out);
        break;
    }
    case Constant_MaximumRatio: {
        if (v == 0.0)
            return;
        double out = FP::undefined();
        double bestRatio = FP::undefined();
        for (QList<WavelengthConstants>::const_iterator c = constants.constBegin(),
                endC = constants.constEnd(); c != endC; ++c) {
            double corrected;
            double af = c->angstromFit.apply(angstrom);
            if (FP::defined(af)) {
                corrected = v * af;
            } else if (FP::defined(c->coefficient)) {
                corrected = v * c->coefficient;
            } else {
                continue;
            }
            Q_ASSERT(FP::defined(corrected));

            double ratio = ::fabs(corrected / v);
            if (!FP::defined(bestRatio) || ratio > bestRatio) {
                out = corrected;
                bestRatio = ratio;
            }
        }
        if (!FP::defined(out))
            value.setEmpty();
        else
            value.setDouble(out);
        break;
    }
    case Constant_MinimumRatio: {
        if (v == 0.0)
            return;
        double out = FP::undefined();
        double bestRatio = FP::undefined();
        for (QList<WavelengthConstants>::const_iterator c = constants.constBegin(),
                endC = constants.constEnd(); c != endC; ++c) {
            double corrected;
            double af = c->angstromFit.apply(angstrom);
            if (FP::defined(af)) {
                corrected = v * af;
            } else if (FP::defined(c->coefficient)) {
                corrected = v * c->coefficient;
            } else {
                continue;
            }
            Q_ASSERT(FP::defined(corrected));

            double ratio = ::fabs(corrected / v);
            if (!FP::defined(bestRatio) || ratio < bestRatio) {
                out = corrected;
                bestRatio = ratio;
            }
        }
        if (!FP::defined(out))
            value.setEmpty();
        else
            value.setDouble(out);
        break;
    }
    }
}

static double convertReferenced(const Variant::Read &base, const Variant::Read &reference)
{
    double a = base.getPath(reference.currentPath()).toReal();
    if (FP::defined(a))
        return a;
    switch (base.getType()) {
    case Variant::Type::Array:
    case Variant::Type::Matrix:
        if (reference.currentPath().empty()) {
            return base.array(0).toReal();
        }
        return FP::undefined();
    default:
        break;
    }

    return base.toReal();
}

void CorrTruncation::processChannel(int id,
                                    CPD3::Data::SequenceSegment &data,
                                    CorrectionSet &set,
                                    ConstantMode mode,
                                    bool isBackscatter)
{
    set.lookup->incoming(data);

    QMap<double, QList<WavelengthConstants> > constants;
    {
        int oldSize = set.constants.size();
        if (Range::intersectShift(set.constants, data)) {
            constants = set.constants.first().constants;
        }
        if (oldSize != set.constants.size())
            set.cache.clear();
    }

    for (const auto &i : set.operate->get(data)) {
        if (!data.exists(i))
            continue;

        double wavelength = set.lookup->getWavelength(i);

        QHash<SequenceName, UnitLookupCacheElement>::iterator cached = set.cache.find(i);
        if (cached == set.cache.end() || !FP::equal(wavelength, cached.value().wavelength)) {
            UnitLookupCacheElement add;
            add.wavelength = wavelength;
            if (!constants.isEmpty() && FP::defined(wavelength)) {
                add.constants = reduceToUnit(i, lookupNearest(wavelength, constants), mode);
            } else {
                add.constants = reduceToUnit(i, getDefaults(wavelength, isBackscatter, id), mode);
            }
            cached = set.cache.insert(i, add);
        }

        auto angstrom = set.lookup->getAngstrom(i);
        Variant::Composite::applyInplace(data[i], [&](Variant::Write &d) {
            processOutput(d, convertReferenced(angstrom, d), cached.value().constants, mode);
        });
    }
}

void CorrTruncation::invalidateConstantCache(int id)
{
    Processing *proc = &processing[id];
    proc->scatter.cache.clear();
    proc->backscatter.cache.clear();
}

void CorrTruncation::process(int id, SequenceSegment &data)
{
    Processing *proc = &processing[id];

    for (const auto &i : proc->operateFlags->get(data)) {
        if (!data.exists(i))
            continue;
        auto d = data[i];
        if (!d.exists())
            continue;
        d.applyFlag(flagName);
    }

    ConstantMode defaultMode = proc->mode->get(data);
    if (defaultMode != proc->priorDefaultMode) {
        proc->priorDefaultMode = defaultMode;
        proc->scatter.cache.clear();
        proc->backscatter.cache.clear();
    }

    processChannel(id, data, proc->scatter, defaultMode, false);
    processChannel(id, data, proc->backscatter, defaultMode, true);
}

void CorrTruncation::processMeta(int id, SequenceSegment &data)
{
    Processing *p = &processing[id];

    p->scatter.lookup->incomingMeta(data);
    p->backscatter.lookup->incomingMeta(data);

    auto meta = getProcessingMetadata();

    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());

    for (auto i : p->scatter.operate->get(data)) {
        i.setMeta();
        if (!data.exists(i))
            continue;
        data[i].metadata("Processing").toArray().after_back().set(meta);
        clearPropagatedSmoothing(data[i].metadata("Smoothing"));
    }
    for (auto i : p->backscatter.operate->get(data)) {
        i.setMeta();
        if (!data.exists(i))
            continue;
        data[i].metadata("Processing").toArray().after_back().set(meta);
        clearPropagatedSmoothing(data[i].metadata("Smoothing"));
    }

    for (auto i : p->operateFlags->get(data)) {
        i.setMeta();
        if (!data.exists(i))
            continue;
        auto flagMeta = data[i].metadataSingleFlag(flagName);
        flagMeta.set(getFlagMetadata());
    }
}


SequenceName::Set CorrTruncation::requestedInputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        Util::merge(p->operateFlags->getAllUnits(), out);
        Util::merge(p->scatter.operate->getAllUnits(), out);
        Util::merge(p->scatter.lookup->getAllUnits(), out);
        Util::merge(p->backscatter.operate->getAllUnits(), out);
        Util::merge(p->backscatter.lookup->getAllUnits(), out);
    }
    return out;
}

QSet<double> CorrTruncation::metadataBreaks(int id)
{
    Processing *p = &processing[id];
    return p->operateFlags->getChangedPoints() |
            p->scatter.operate->getChangedPoints() |
            p->scatter.lookup->getChangedPoints() |
            p->backscatter.operate->getChangedPoints() |
            p->backscatter.lookup->getChangedPoints();
}

QDataStream &operator>>(QDataStream &stream, CorrTruncation::WavelengthConstants &wc)
{
    stream >> wc.hasFlavors;
    stream >> wc.lacksFlavors;
    stream >> wc.coefficient;
    stream >> wc.angstromFit;
    return stream;
}

QDataStream &operator<<(QDataStream &stream, const CorrTruncation::WavelengthConstants &wc)
{
    stream << wc.hasFlavors;
    stream << wc.lacksFlavors;
    stream << wc.coefficient;
    stream << wc.angstromFit;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, CorrTruncation::WavelengthTimeConstants &wtc)
{
    stream >> wtc.start;
    stream >> wtc.end;
    stream >> wtc.constants;
    return stream;
}

QDataStream &operator<<(QDataStream &stream, const CorrTruncation::WavelengthTimeConstants &wtc)
{
    stream << wtc.start;
    stream << wtc.end;
    stream << wtc.constants;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, CorrTruncation::ConstantMode &m)
{
    quint8 i;
    stream >> i;
    m = (CorrTruncation::ConstantMode) i;
    return stream;
}

QDataStream &operator<<(QDataStream &stream, CorrTruncation::ConstantMode m)
{
    stream << (quint8) m;
    return stream;
}

CorrTruncation::CorrTruncation(const Variant::Flag &fn, QDataStream &stream) : flagName(fn)
{
    stream >> defaultWavelengthSmoother;
    stream >> defaultConstantMode;
    stream >> filterSuffixes;
    stream >> restrictedInputs;

    quint32 n;
    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        Processing p;
        stream >> p.operateFlags;
        stream >> p.mode;

        stream >> p.scatter.operate;
        stream >> p.scatter.lookup;
        stream >> p.scatter.constants;

        stream >> p.backscatter.operate;
        stream >> p.backscatter.lookup;
        stream >> p.backscatter.constants;

        processing.push_back(p);
    }
}

void CorrTruncation::serialize(QDataStream &stream)
{
    stream << defaultWavelengthSmoother;
    stream << defaultConstantMode;
    stream << filterSuffixes;
    stream << restrictedInputs;

    quint32 n = (quint32) processing.size();
    stream << n;
    for (int i = 0; i < (int) n; i++) {
        stream << processing[i].operateFlags;
        stream << processing[i].mode;

        stream << processing[i].scatter.operate;
        stream << processing[i].scatter.lookup;
        stream << processing[i].scatter.constants;

        stream << processing[i].backscatter.operate;
        stream << processing[i].backscatter.lookup;
        stream << processing[i].backscatter.constants;
    }
}

