/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "smoothing/baseline.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    class ID {
    public:
        ID() : filter(), input()
        { }

        ID(const QSet<SequenceName> &setFilter, const QSet<SequenceName> &setInput) : filter(
                setFilter), input(setInput)
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }

        QSet<SequenceName> filter;
        QSet<SequenceName> input;
    };

    QHash<int, ID> contents;

    virtual void filterUnit(const SequenceName &unit, int targetID)
    {
        contents[targetID].input.remove(unit);
        contents[targetID].filter.insert(unit);
    }

    virtual void inputUnit(const SequenceName &unit, int targetID)
    {
        if (contents[targetID].filter.contains(unit))
            return;
        contents[targetID].input.insert(unit);
    }

    virtual bool isHandled(const SequenceName &unit)
    {
        for (QHash<int, ID>::const_iterator it = contents.constBegin();
                it != contents.constEnd();
                ++it) {
            if (it.value().filter.contains(unit))
                return true;
            if (it.value().input.contains(unit))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("corr_andersonogren1998"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("scatter")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("backscatter")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")));
        QVERIFY(qobject_cast<BaselineSmootherOption *>(options.get("smoothing")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("mode")));

        QVERIFY(options.excluded().value("scatter").contains("suffix"));
        QVERIFY(options.excluded().value("backscatter").contains("suffix"));
        QVERIFY(options.excluded().value("suffix").contains("scatter"));
        QVERIFY(options.excluded().value("suffix").contains("backscatter"));
    }

    void dynamicDefaults()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<BaselineSmootherOption *>(options.get("smoothing"))->overlay(
                new BaselineSinglePoint, FP::undefined(), FP::undefined());
        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        SegmentProcessingStage *filter2;
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "Vx_S11q"), &controller);
        QVERIFY(controller.contents.isEmpty());
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2 != NULL);
            filter2->unhandled(SequenceName("brw", "raw", "Vx_S11q"), &controller);
            QVERIFY(controller.contents.isEmpty());
            delete filter2;
        }

        SequenceName F1_S11_1("brw", "raw", "F1_S11");
        SequenceName BsB_S11_1("brw", "raw", "BsB_S11");
        SequenceName BsG_S11_1("brw", "raw", "BsG_S11");
        SequenceName BsR_S11_1("brw", "raw", "BsR_S11");
        SequenceName BbsR_S11_1("brw", "raw", "BbsR_S11");
        SequenceName F1_S11_2("mlo", "raw", "F1_S11", {"pm1"});
        SequenceName BsB_S11_2("mlo", "raw", "BsB_S11", {"pm1"});
        SequenceName BsG_S11_2("mlo", "raw", "BsG_S11", {"pm1"});
        SequenceName BbsB_S11_2("mlo", "raw", "BbsB_S11", {"pm1"});
        SequenceName F1_S12_1("brw", "raw", "F1_S12", {"pm1"});
        SequenceName BbsB_S12_1("brw", "raw", "BbsB_S12", {"pm1"});

        filter->unhandled(BsB_S11_1, &controller);
        filter->unhandled(F1_S11_1, &controller);
        filter->unhandled(BsG_S11_1, &controller);
        filter->unhandled(BsR_S11_1, &controller);
        filter->unhandled(BbsR_S11_1, &controller);
        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                               BsB_S11_1 <<
                                                                               BsG_S11_1 <<
                                                                               BsR_S11_1 <<
                                                                               BbsR_S11_1 << F1_S11_1,
                                                          QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int S11_1 = idL.at(0);

        filter->unhandled(BbsB_S11_2, &controller);
        filter->unhandled(BsB_S11_2, &controller);
        filter->unhandled(BsG_S11_2, &controller);
        QCOMPARE(controller.contents.size(), 2);
        idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                               BsB_S11_2 <<
                                                                               BsG_S11_2 <<
                                                                               BbsB_S11_2 << F1_S11_2,
                                                          QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int S11_2 = idL.at(0);

        filter->unhandled(F1_S12_1, &controller);
        filter->unhandled(BbsB_S12_1, &controller);
        QCOMPARE(controller.contents.size(), 3);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << BbsB_S12_1 << F1_S12_1,
                                                 QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int S12_1 = idL.at(0);

        data.setStart(15.0);
        data.setEnd(16.0);

        {
            SequenceSegment mdata;
            Variant::Root meta;
            mdata.setStart(15.0);
            mdata.setEnd(FP::undefined());
            meta.write().metadataReal("Wavelength").setDouble(450.0);
            mdata.setValue(BsB_S11_1.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(550.0);
            mdata.setValue(BsG_S11_1.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(700.0);
            mdata.setValue(BsR_S11_1.toMeta(), meta);
            mdata.setValue(BbsR_S11_1.toMeta(), meta);
            mdata.setValue(F1_S11_1.toMeta(), Variant::Root(Variant::Type::MetadataFlags));
            filter->processMeta(S11_1, mdata);
            QVERIFY(mdata.value(F1_S11_1.toMeta())
                         .metadataSingleFlag("AndersonOgren1998").exists());
            QCOMPARE(mdata.value(BsB_S11_1.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QCOMPARE(mdata.value(BbsR_S11_1.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);

            mdata = SequenceSegment();
            mdata.setStart(15.0);
            mdata.setEnd(FP::undefined());
            meta.write().metadataReal("Wavelength").setDouble(450.0);
            mdata.setValue(BsB_S11_2.toMeta(), meta);
            mdata.setValue(BbsB_S11_2.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(550.0);
            mdata.setValue(BsG_S11_2.toMeta(), meta);
            filter->processMeta(S11_2, mdata);

            mdata = SequenceSegment();
            mdata.setStart(15.0);
            mdata.setEnd(FP::undefined());
            meta.write().metadataReal("Wavelength").setDouble(450.0);
            mdata.setValue(BbsB_S12_1.toMeta(), meta);
            filter->processMeta(S12_1, mdata);
        }

        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(BsB_S11_1, Variant::Root(15.0));
        data.setValue(BsG_S11_1, Variant::Root(14.0));
        data.setValue(BsR_S11_1, Variant::Root(13.0));
        data.setValue(BbsR_S11_1, Variant::Root(5.0));
        filter->process(S11_1, data);
        QCOMPARE(data.value(BsB_S11_1).toDouble(), 19.6704813386795);
        QCOMPARE(data.value(BsG_S11_1).toDouble(), 18.0922636133521);
        QCOMPARE(data.value(BsR_S11_1).toDouble(), 16.4095831704592);
        QCOMPARE(data.value(BbsR_S11_1).toDouble(), 4.925);
        QVERIFY(data.value(F1_S11_1).testFlag("AndersonOgren1998"));

        {
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
            }
            QVERIFY(filter2 != NULL);
            data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
            data.setValue(BsB_S11_1, Variant::Root(15.0));
            data.setValue(BsG_S11_1, Variant::Root(14.0));
            data.setValue(BsR_S11_1, Variant::Root(13.0));
            data.setValue(BbsR_S11_1, Variant::Root(5.0));
            filter2->process(S11_1, data);
            QCOMPARE(data.value(BsB_S11_1).toDouble(), 19.6704813386795);
            QCOMPARE(data.value(BsG_S11_1).toDouble(), 18.0922636133521);
            QCOMPARE(data.value(BsR_S11_1).toDouble(), 16.4095831704592);
            QCOMPARE(data.value(BbsR_S11_1).toDouble(), 4.925);
            QVERIFY(data.value(F1_S11_1).testFlag("AndersonOgren1998"));
            delete filter2;
        }

        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(BsB_S11_1, Variant::Root(15.0));
        data.setValue(BsG_S11_1, Variant::Root(14.0));
        data.setValue(BsR_S11_1, Variant::Root(13.0));
        data.setValue(BbsR_S11_1, Variant::Root(5.0));
        filter->process(S11_1, data);
        QCOMPARE(data.value(BsB_S11_1).toDouble(), 19.6704813386795);
        QCOMPARE(data.value(BsG_S11_1).toDouble(), 18.0922636133521);
        QCOMPARE(data.value(BsR_S11_1).toDouble(), 16.4095831704592);
        QCOMPARE(data.value(BbsR_S11_1).toDouble(), 4.925);
        QVERIFY(data.value(F1_S11_1).testFlag("AndersonOgren1998"));

        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(BsB_S11_1, Variant::Root(15.0));
        data.setValue(BsG_S11_1, Variant::Root(-14.0));
        data.setValue(BsR_S11_1, Variant::Root(-13.0));
        data.setValue(BbsR_S11_1, Variant::Root(5.0));
        filter->process(S11_1, data);
        QCOMPARE(data.value(BsB_S11_1).toDouble(), 19.35);
        QCOMPARE(data.value(BsG_S11_1).toDouble(), -18.06);
        QCOMPARE(data.value(BsR_S11_1).toDouble(), -16.38);
        QCOMPARE(data.value(BbsR_S11_1).toDouble(), 4.925);
        QVERIFY(data.value(F1_S11_1).testFlag("AndersonOgren1998"));

        data.setValue(F1_S11_2, Variant::Root(Variant::Type::Flags));
        data.setValue(BsB_S11_2, Variant::Root(15.0));
        data.setValue(BsG_S11_2, Variant::Root(14.0));
        data.setValue(BbsB_S11_2, Variant::Root(5.0));
        filter->process(S11_2, data);
        QCOMPARE(data.value(BsB_S11_2).toDouble(), 17.2377701383286);
        QCOMPARE(data.value(BsG_S11_2).toDouble(), 15.9162121814643);
        QCOMPARE(data.value(BbsB_S11_2).toDouble(), 4.755);
        QVERIFY(data.value(F1_S11_2).testFlag("AndersonOgren1998"));

        data.setValue(F1_S11_2, Variant::Root(Variant::Type::Flags));
        data.setValue(BsB_S11_2, Variant::Root(-15.0));
        data.setValue(BsG_S11_2, Variant::Root(14.0));
        data.setValue(BbsB_S11_2, Variant::Root(5.0));
        filter->process(S11_2, data);
        QCOMPARE(data.value(BsB_S11_2).toDouble(), -16.41);
        QCOMPARE(data.value(BsG_S11_2).toDouble(), 15.022);
        QCOMPARE(data.value(BbsB_S11_2).toDouble(), 4.755);
        QVERIFY(data.value(F1_S11_2).testFlag("AndersonOgren1998"));

        data.setValue(F1_S11_2, Variant::Root(Variant::Type::Flags));
        data.setValue(BsB_S11_2, Variant::Root());
        data.setValue(BsG_S11_2, Variant::Root(14.0));
        data.setValue(BbsB_S11_2, Variant::Root());
        filter->process(S11_2, data);
        QVERIFY(!data.value(BsB_S11_2).exists());
        QCOMPARE(data.value(BsG_S11_2).toDouble(), 15.022);
        QVERIFY(!data.value(BbsB_S11_2).exists());
        QVERIFY(data.value(F1_S11_2).testFlag("AndersonOgren1998"));

        delete filter;
    }

    void dynamicOptions()
    {
        ComponentOptions options(component->getOptions());

        qobject_cast<DynamicSequenceSelectionOption *>(options.get("scatter"))->set("", "", "C.*");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("backscatter"))->set("", "",
                                                                                        "A.*");

        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "BbsG_S11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BsG_S11"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName C1_S11("brw", "raw", "C1_S11");
        SequenceName C2_S11("brw", "raw", "C2_S11");
        SequenceName A_S11("brw", "raw", "A_S11");
        SequenceName A_S12("brw", "raw", "A_S12");
        filter->unhandled(C1_S11, &controller);
        filter->unhandled(C2_S11, &controller);
        filter->unhandled(A_S11, &controller);
        filter->unhandled(A_S12, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << C1_S11 << C2_S11 << A_S11 << A_S12,
                                QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int id = idL.at(0);

        data.setStart(15.0);
        data.setEnd(16.0);

        {
            SequenceSegment mdata;
            Variant::Root meta;
            mdata.setStart(15.0);
            mdata.setEnd(FP::undefined());
            meta.write().metadataReal("Wavelength").setDouble(450.0);
            mdata.setValue(C1_S11.toMeta(), meta);
            mdata.setValue(A_S12.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(550.0);
            mdata.setValue(C2_S11.toMeta(), meta);
            mdata.setValue(A_S11.toMeta(), meta);
            filter->processMeta(id, mdata);
            QCOMPARE(mdata.value(C1_S11.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QCOMPARE(mdata.value(A_S12.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
        }

        data.setValue(C1_S11, Variant::Root(15.0));
        data.setValue(C2_S11, Variant::Root(14.0));
        data.setValue(A_S12, Variant::Root(5.0));
        data.setValue(A_S11, Variant::Root(6.0));
        filter->process(id, data);
        QCOMPARE(data.value(C1_S11).toDouble(), 19.6704813386795);
        QCOMPARE(data.value(C2_S11).toDouble(), 18.0537563873200);
        QCOMPARE(data.value(A_S12).toDouble(), 4.905);
        QCOMPARE(data.value(A_S11).toDouble(), 5.892);

        delete filter;


        options = component->getOptions();
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->add("S11");
        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        filter->unhandled(SequenceName("brw", "raw", "BsG_S12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BbsG_S12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "F1_S12"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName F1_S11_1("brw", "raw", "F1_S11");
        SequenceName BsB_S11_1("brw", "raw", "BsB_S11");
        SequenceName BsG_S11_1("brw", "raw", "BsG_S11");
        SequenceName BsR_S11_1("brw", "raw", "BsR_S11");
        SequenceName BbsR_S11_1("brw", "raw", "BbsR_S11");
        filter->unhandled(BsB_S11_1, &controller);
        filter->unhandled(F1_S11_1, &controller);
        filter->unhandled(BsG_S11_1, &controller);
        filter->unhandled(BsR_S11_1, &controller);
        filter->unhandled(BbsR_S11_1, &controller);
        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                               BsB_S11_1 <<
                                                                               BsG_S11_1 <<
                                                                               BsR_S11_1 <<
                                                                               BbsR_S11_1 << F1_S11_1,
                                                          QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        data.setStart(15.0);
        data.setEnd(16.0);

        {
            SequenceSegment mdata;
            Variant::Root meta;
            mdata.setStart(15.0);
            mdata.setEnd(FP::undefined());
            meta.write().metadataReal("Wavelength").setDouble(450.0);
            mdata.setValue(BsB_S11_1.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(550.0);
            mdata.setValue(BsG_S11_1.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(700.0);
            mdata.setValue(BsR_S11_1.toMeta(), meta);
            mdata.setValue(BbsR_S11_1.toMeta(), meta);
            mdata.setValue(F1_S11_1.toMeta(), Variant::Root(Variant::Type::MetadataFlags));
            filter->processMeta(id, mdata);
            QVERIFY(mdata.value(F1_S11_1.toMeta())
                         .metadataSingleFlag("AndersonOgren1998").exists());
            QCOMPARE(mdata.value(BsB_S11_1.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QCOMPARE(mdata.value(BbsR_S11_1.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
        }

        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(BsB_S11_1, Variant::Root(15.0));
        data.setValue(BsG_S11_1, Variant::Root(14.0));
        data.setValue(BsR_S11_1, Variant::Root(13.0));
        data.setValue(BbsR_S11_1, Variant::Root(5.0));
        filter->process(id, data);
        QCOMPARE(data.value(BsB_S11_1).toDouble(), 19.6704813386795);
        QCOMPARE(data.value(BsG_S11_1).toDouble(), 18.0922636133521);
        QCOMPARE(data.value(BsR_S11_1).toDouble(), 16.4095831704592);
        QCOMPARE(data.value(BbsR_S11_1).toDouble(), 4.925);
        QVERIFY(data.value(F1_S11_1).testFlag("AndersonOgren1998"));

        delete filter;
    }

    void predefined()
    {
        ComponentOptions options(component->getOptions());

        SequenceName F1_S11("brw", "raw", "F1_S11");
        SequenceName BbsG_S11("brw", "raw", "BbsG_S11");
        SequenceName BsG_S11("brw", "raw", "BsG_S11");
        QList<SequenceName> input;
        input << BbsG_S11 << BsG_S11;

        SegmentProcessingStage *filter =
                component->createBasicFilterPredefined(options, FP::undefined(), FP::undefined(),
                                                       input);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{BsG_S11, BbsG_S11, F1_S11}));

        delete filter;
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["S11/CorrectScatter"] = "::Bs[BGR]_S11:=";
        cv["S11/CorrectBackscatter"] = "::Bbs[BGR]_S11:=";
        cv["S11/Flags"] = "::F1_S11:=";
        cv["S11/Smoothing/Type"] = "SinglePoint";
        cv["S11/Constants/Backscatter/@400/Coefficient"] = 0.5;
        cv["S11/Constants/Backscatter/@700/Coefficient"] = 0.6;
        cv["S12/CorrectScatter"] = "::Bs[QO]_S12:=";
        cv["S12/CorrectBackscatter"] = "::BbsQ_S12:=";
        cv["S12/Constants/Backscatter/@0/Coefficient"] = 0.75;
        cv["S12/Constants/Scatter/@0/Fit/#0"] = 1.0;
        cv["S12/Constants/Scatter/@0/Fit/#1"] = -0.1;
        config.emplace_back(FP::undefined(), 13.0, cv);
        cv.write().setEmpty();
        cv["S11/CorrectScatter"] = "::BsG_S11:=";
        cv["S11/Flags"] = "::F1_S11:=";
        cv["S11/Mode"] = "NoAngstrom";
        config.emplace_back(13.0, FP::undefined(), cv);

        SegmentProcessingStage
                *filter = component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config);
        QVERIFY(filter != NULL);
        TestController controller;

        SequenceName F1_S11("brw", "raw", "F1_S11");
        SequenceName BsB_S11("brw", "raw", "BsB_S11");
        SequenceName BsG_S11("brw", "raw", "BsG_S11");
        SequenceName BsR_S11("brw", "raw", "BsR_S11");
        SequenceName BbsG_S11("brw", "raw", "BbsG_S11");
        SequenceName BbsR_S11("brw", "raw", "BbsR_S11");
        SequenceName BsQ_S12("brw", "raw", "BsQ_S12");
        SequenceName BsO_S12("brw", "raw", "BsO_S12");
        SequenceName BbsQ_S12("brw", "raw", "BbsQ_S12");

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{F1_S11, BbsQ_S12, BsG_S11}));

        filter->unhandled(F1_S11, &controller);
        filter->unhandled(BsB_S11, &controller);
        filter->unhandled(BsG_S11, &controller);
        filter->unhandled(BsR_S11, &controller);
        filter->unhandled(BbsG_S11, &controller);
        filter->unhandled(BbsR_S11, &controller);
        filter->unhandled(BsQ_S12, &controller);
        filter->unhandled(BsO_S12, &controller);
        filter->unhandled(BbsQ_S12, &controller);

        QList<int> idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                                          F1_S11 <<
                                                                                          BsB_S11 <<
                                                                                          BsG_S11 <<
                                                                                          BsR_S11 <<
                                                                                          BbsG_S11 <<
                                                                                          BbsR_S11,
                                                                     QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int S11 = idL.at(0);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << BsQ_S12 << BsO_S12 << BbsQ_S12,
                                QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int S12 = idL.at(0);

        QCOMPARE(filter->metadataBreaks(S11), QSet<double>() << 13.0);
        QCOMPARE(filter->metadataBreaks(S12), QSet<double>() << 13.0);

        {
            SequenceSegment mdata;
            Variant::Root meta;
            mdata.setStart(12.0);
            mdata.setEnd(13.0);
            meta.write().metadataReal("Wavelength").setDouble(450.0);
            mdata.setValue(BsB_S11.toMeta(), meta);
            mdata.setValue(BbsG_S11.toMeta(), meta);
            mdata.setValue(BsQ_S12.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(550.0);
            mdata.setValue(BsG_S11.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(700.0);
            mdata.setValue(BsR_S11.toMeta(), meta);
            mdata.setValue(BbsR_S11.toMeta(), meta);
            mdata.setValue(BsO_S12.toMeta(), meta);
            mdata.setValue(BbsQ_S12.toMeta(), meta);
            mdata.setValue(F1_S11.toMeta(), Variant::Root(Variant::Type::MetadataFlags));
            filter->processMeta(S11, mdata);
            QVERIFY(mdata.value(F1_S11.toMeta()).metadataSingleFlag("AndersonOgren1998").exists());
            QCOMPARE(mdata.value(BsB_S11.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QCOMPARE(mdata.value(BbsR_S11.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);

            filter->processMeta(S12, mdata);
        }

        SequenceSegment data1;
        data1.setStart(12.0);
        data1.setEnd(13.0);

        data1.setValue(F1_S11, Variant::Root(Variant::Type::Flags));
        data1.setValue(BsB_S11, Variant::Root(3.0));
        data1.setValue(BsG_S11, Variant::Root(2.0));
        data1.setValue(BsR_S11, Variant::Root(1.0));
        data1.setValue(BbsR_S11, Variant::Root(4.0));
        data1.setValue(BbsG_S11, Variant::Root(5.0));
        filter->process(S11, data1);
        QCOMPARE(data1.value(BsB_S11).toDouble(), 3.14938275300934);
        QCOMPARE(data1.value(BsG_S11).toDouble(), 1.98772902957429);
        QCOMPARE(data1.value(BsR_S11).toDouble(), 0.972215776324442);
        QCOMPARE(data1.value(BbsR_S11).toDouble(), 2.4);
        QCOMPARE(data1.value(BbsG_S11).toDouble(), 2.5);
        QVERIFY(data1.value(F1_S11).testFlag("AndersonOgren1998"));

        data1.setValue(BsQ_S12, Variant::Root(8.0));
        data1.setValue(BsO_S12, Variant::Root(7.0));
        data1.setValue(BbsQ_S12, Variant::Root(9.0));
        filter->process(S12, data1);
        QCOMPARE(data1.value(BsQ_S12).toDouble(), 7.75822273575556);
        QCOMPARE(data1.value(BsO_S12).toDouble(), 6.78844489378611);
        QCOMPARE(data1.value(BbsQ_S12).toDouble(), 6.75);


        SequenceSegment data2;
        data2.setStart(13.0);
        data2.setEnd(15.0);

        {
            SequenceSegment mdata;
            Variant::Root meta;
            mdata.setStart(13.0);
            mdata.setEnd(15.0);

            meta.write().metadataReal("Wavelength").setDouble(550.0);
            mdata.setValue(BsG_S11.toMeta(), meta);
            mdata.setValue(F1_S11.toMeta(), Variant::Root(Variant::Type::MetadataFlags));
            filter->processMeta(S11, mdata);
            QVERIFY(!mdata.value(BsB_S11.toMeta()).exists());
            QVERIFY(!mdata.value(BbsR_S11.toMeta()).exists());
            QCOMPARE(mdata.value(BsG_S11.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);

            filter->processMeta(S12, mdata);
            QVERIFY(!mdata.value(BsQ_S12.toMeta()).exists());
            QVERIFY(!mdata.value(BbsQ_S12.toMeta()).exists());
        }

        data2.setValue(F1_S11, Variant::Root());
        data2.setValue(BsB_S11, Variant::Root(10.0));
        data2.setValue(BsG_S11, Variant::Root(20.0));
        data2.setValue(BsR_S11, Variant::Root(30.0));
        data2.setValue(BbsR_S11, Variant::Root(40.0));
        data2.setValue(BbsG_S11, Variant::Root(50.0));
        filter->process(S11, data2);
        QCOMPARE(data2.value(BsB_S11).toDouble(), 10.0);
        QCOMPARE(data2.value(BsG_S11).toDouble(), 25.8);
        QCOMPARE(data2.value(BsR_S11).toDouble(), 30.0);
        QCOMPARE(data2.value(BbsR_S11).toDouble(), 40.0);
        QCOMPARE(data2.value(BbsG_S11).toDouble(), 50.0);
        QVERIFY(!data2.value(F1_S11).exists());

        data2.setValue(BsQ_S12, Variant::Root(50.0));
        data2.setValue(BsO_S12, Variant::Root(60.0));
        data2.setValue(BbsQ_S12, Variant::Root(70.0));
        filter->process(S12, data2);
        QCOMPARE(data2.value(BsQ_S12).toDouble(), 50.0);
        QCOMPARE(data2.value(BsO_S12).toDouble(), 60.0);
        QCOMPARE(data2.value(BbsQ_S12).toDouble(), 70.0);

        delete filter;
    }

    void extend()
    {
        double start = 3600;
        double end = 7200;
        Variant::Root cv;
        cv["S11/CorrectScatter"] = "::Bs[BGR]_S11:=";
        cv["S11/CorrectBackscatter"] = "::Bbs[BGR]_S11:=";
        cv["S11/Flags"] = "::F1_S11:=";
        component->extendBasicFilterEditing(start, end, "bnd", "raw", ValueSegment::Transfer{
                ValueSegment(FP::undefined(), FP::undefined(), cv)});
        QVERIFY(start <= 3600 - 15 * 60);
    }
};

QTEST_MAIN(TestComponent)

#include "test_andersonogren1998.moc"
