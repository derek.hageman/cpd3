/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QRegExp>

#include "core/component.hxx"
#include "core/environment.hxx"

#include "corr_andersonogren1998.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;
using namespace CPD3::Editing;

static const Variant::Flag FLAG_NAME = "AndersonOgren1998";

CorrAndersonOgren1998::CorrAndersonOgren1998(const ComponentOptions &options) : CorrTruncation(
        FLAG_NAME, options)
{ }

CorrAndersonOgren1998::CorrAndersonOgren1998(const ComponentOptions &options,
                                             double start,
                                             double end, const QList<SequenceName> &inputs)
        : CorrTruncation(
        FLAG_NAME, options, start, end, inputs)
{ }

CorrAndersonOgren1998::CorrAndersonOgren1998(double start,
                                             double end,
                                             const SequenceName::Component &station,
                                             const SequenceName::Component &archive,
                                             const ValueSegment::Transfer &config) : CorrTruncation(
        FLAG_NAME, start, end, station, archive, config)
{ }

void CorrAndersonOgren1998Component::extendBasicFilterEditing(double &start,
                                                              double &end,
                                                              const SequenceName::Component &,
                                                              const SequenceName::Component &,
                                                              const ValueSegment::Transfer &config,
                                                              Archive::Access *)
{
    if (!FP::defined(start))
        return;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    Variant::Root defaultAdjust;
    defaultAdjust["Smoothing/Type"].setString("SinglePoleLowPass");
    defaultAdjust["Smoothing/TimeConstant/Units"].setString("Minute");
    defaultAdjust["Smoothing/TimeConstant/Count"].setInt64(3);
    defaultAdjust["Smoothing/Gap/Units"].setString("Minute");
    defaultAdjust["Smoothing/Gap/Count"].setInt64(35);
    defaultAdjust["Smoothing/ResetOnUndefined"].setBool(false);

    double result = start;
    for (const auto &child : children) {
        WavelengthAdjust *wl = WavelengthAdjust::fromConfiguration(config,
                                                                   QString("%1/Smoothing").arg(
                                                                           QString::fromStdString(
                                                                                   child)),
                                                                   start, end, defaultAdjust);
        double check = wl->spinupTime(start);
        if (FP::defined(check) && check < result)
            result = check;
        delete wl;
    }
    start = result;
}


CorrAndersonOgren1998::~CorrAndersonOgren1998()
{ }

CorrAndersonOgren1998::CorrAndersonOgren1998(QDataStream &stream) : CorrTruncation(FLAG_NAME,
                                                                                   stream)
{ }


namespace {
class Defaults {
public:
    QMap<double, QList<CorrTruncation::WavelengthConstants> > scattering;
    QMap<double, QList<CorrTruncation::WavelengthConstants> > backscattering;

    Defaults()
    {
        scattering[450] <<
                CorrTruncation::WavelengthConstants::coarse(1.29, Calibration(
                        QList<double>() << 1.365 << -0.156));
        scattering[450] <<
                CorrTruncation::WavelengthConstants::fine(1.094, Calibration(
                        QList<double>() << 1.165 << -0.046));

        scattering[550] <<
                CorrTruncation::WavelengthConstants::coarse(1.29, Calibration(
                        QList<double>() << 1.337 << -0.138));
        scattering[550] <<
                CorrTruncation::WavelengthConstants::fine(1.073, Calibration(
                        QList<double>() << 1.152 << -0.044));

        scattering[700] <<
                CorrTruncation::WavelengthConstants::coarse(1.26, Calibration(
                        QList<double>() << 1.297 << -0.113));
        scattering[700] <<
                CorrTruncation::WavelengthConstants::fine(1.049, Calibration(
                        QList<double>() << 1.120 << -0.035));


        backscattering[450] << CorrTruncation::WavelengthConstants::coarse(0.981);
        backscattering[450] << CorrTruncation::WavelengthConstants::fine(0.951);

        backscattering[550] << CorrTruncation::WavelengthConstants::coarse(0.982);
        backscattering[550] << CorrTruncation::WavelengthConstants::fine(0.947);

        backscattering[700] << CorrTruncation::WavelengthConstants::coarse(0.985);
        backscattering[700] << CorrTruncation::WavelengthConstants::fine(0.952);
    }
};
}

static Defaults defaults;

template<typename K>
static K lookupNearest(double v, const QMap<double, K> &map)
{
    typename QMap<double, K>::const_iterator lb = map.lowerBound(v);
    if (lb == map.end()) {
        if (lb == map.begin())
            return K();
        return (lb - 1).value();
    } else if (lb == map.begin()) {
        Q_ASSERT(!map.isEmpty());
        return lb.value();
    }
    typename QMap<double, K>::const_iterator lb1 = lb - 1;
    if (fabs(lb.key() - v) < fabs(lb1.key() - v))
        return lb.value();
    return lb1.value();
}

QList<CorrTruncation::WavelengthConstants> CorrAndersonOgren1998::getDefaults(double wavelength,
                                                                              bool isBackscatter,
                                                                              int id) const
{
    Q_UNUSED(id);
    if (!FP::defined(wavelength))
        return QList<CorrTruncation::WavelengthConstants>();
    if (isBackscatter)
        return lookupNearest(wavelength, defaults.backscattering);
    return lookupNearest(wavelength, defaults.scattering);
}

Variant::Root CorrAndersonOgren1998::getProcessingMetadata() const
{
    Variant::Root meta;
    meta["By"].setString("corr_andersonogren1998");
    meta["Revision"].setString(Environment::revision());
    return meta;
}

Variant::Root CorrAndersonOgren1998::getFlagMetadata() const
{
    Variant::Root meta;
    meta["Origin"].toArray().after_back().setString("corr_andersonogren1998");
    meta["Bits"].setInt64(0x0400);
    meta["Description"].setString("Anderson and Ogren 1998 truncation correction applied");
    return meta;
}


QString CorrAndersonOgren1998Component::getBasicSerializationName() const
{ return QString::fromLatin1("corr_andersonogren1998"); }

ComponentOptions CorrAndersonOgren1998Component::getOptions()
{
    ComponentOptions options;
    CorrAndersonOgren1998::injectOptions(options);
    return options;
}

QList<ComponentExample> CorrAndersonOgren1998Component::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will correct all scatterings and backscatterings using "
                                                "the default smoothing.  The constants used in the fits and "
                                                "no-angstrom multiplies are those described in Anderson and Ogren "
                                                "(1998).")));

    options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")))->add("S11");
    examples.append(ComponentExample(options, tr("Single instrument"),
                                     tr("This will correct all scatterings and backscatterings for S11.")));

    options = getOptions();
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("backscatter")))->set("", "",
                                                                                      "BbsG_S11");
    examples.append(ComponentExample(options, tr("Single variable"),
                                     tr("This will correct only the variable BbsG_S11.")));

    return examples;
}

SegmentProcessingStage *CorrAndersonOgren1998Component::createBasicFilterDynamic(const ComponentOptions &options)
{ return new CorrAndersonOgren1998(options); }

SegmentProcessingStage *CorrAndersonOgren1998Component::createBasicFilterPredefined(const ComponentOptions &options,
                                                                                    double start,
                                                                                    double end,
                                                                                    const QList<
                                                                                 SequenceName> &inputs)
{ return new CorrAndersonOgren1998(options, start, end, inputs); }

SegmentProcessingStage *CorrAndersonOgren1998Component::createBasicFilterEditing(double start,
                                                                                 double end,
                                                                                 const SequenceName::Component &station,
                                                                                 const SequenceName::Component &archive,
                                                                                 const ValueSegment::Transfer &config)
{ return new CorrAndersonOgren1998(start, end, station, archive, config); }

SegmentProcessingStage *CorrAndersonOgren1998Component::deserializeBasicFilter(QDataStream &stream)
{ return new CorrAndersonOgren1998(stream); }
