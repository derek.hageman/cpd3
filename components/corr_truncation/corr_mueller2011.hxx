/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CORRMUELLER2011_H
#define CORRMUELLER2011_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QMultiMap>
#include <QHash>

#include "core/component.hxx"
#include "truncation.hxx"

class CorrMueller2011 : public CorrTruncation {
    class Processing {
    public:
        CPD3::Data::DynamicBool *useTSI;
        bool latestTSI;

        Processing() : useTSI(NULL), latestTSI(false)
        { }
    };

    bool defaultTSI;

    std::vector<Processing> processing;

    void handleOptions(const CPD3::ComponentOptions &options);

public:
    CorrMueller2011(const CPD3::ComponentOptions &options);

    CorrMueller2011(const CPD3::ComponentOptions &options,
                    double start,
                    double end, const QList<CPD3::Data::SequenceName> &inputs);

    CorrMueller2011(double start,
                    double end,
                    const CPD3::Data::SequenceName::Component &station,
                    const CPD3::Data::SequenceName::Component &archive,
                    const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CorrMueller2011();

    CorrMueller2011(QDataStream &stream);

    virtual void serialize(QDataStream &stream);

    virtual void process(int id, CPD3::Data::SequenceSegment &data);

protected:

    virtual QList<WavelengthConstants>
            getDefaults(double wavelength, bool isBackscatter, int id) const;

    virtual CPD3::Data::Variant::Root getProcessingMetadata() const;

    virtual CPD3::Data::Variant::Root getFlagMetadata() const;
};

class CorrMueller2011Component
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.corr_mueller2011"
                              FILE
                              "corr_mueller2011.json")

public:
    virtual QString getBasicSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Data::SegmentProcessingStage
            *createBasicFilterDynamic(const CPD3::ComponentOptions &options);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined
            (const CPD3::ComponentOptions &options,
             double start,
             double end, const QList<CPD3::Data::SequenceName> &inputs);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                         double end,
                                                                         const CPD3::Data::SequenceName::Component &station,
                                                                         const CPD3::Data::SequenceName::Component &archive,
                                                                         const CPD3::Data::ValueSegment::Transfer &config);

    virtual void extendBasicFilterEditing(double &start,
                                          double &end,
                                          const CPD3::Data::SequenceName::Component &station,
                                          const CPD3::Data::SequenceName::Component &archive,
                                          const CPD3::Data::ValueSegment::Transfer &config,
                                          CPD3::Data::Archive::Access *access);

    virtual CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream);
};

#endif
