/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/processingstage.hxx"
#include "core/component.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Data;


class TestComponent : public QObject {
Q_OBJECT

    ProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<ProcessingStageComponent *>(
                ComponentLoader::create("smooth_realtime"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionTimeOffset *>(options.get("interval")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("statistics")));
    }

    void general()
    {
        ComponentOptions options;
        options = component->getOptions();

        (qobject_cast<ComponentOptionTimeOffset *>(options.get("interval"))->set(Time::Second, 2,
                                                                                 false));

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName value("brw", "rt_instant", "T_S11");

        double t = 100.0;
        double begin = Time::time();
        for (; begin + 10.0 >= Time::time(); t += 0.5) {
            filter->incomingData(SequenceValue(value, Variant::Root(1.0), 60.0, FP::undefined()));
            QTest::qSleep(500);
        }
        filter->setEgress(NULL);

        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            filter->serialize(stream);
        }
        filter->signalTerminate();
        QVERIFY(filter->wait(30));
        delete filter;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            filter = component->deserializeGeneralFilter(stream);
            QVERIFY(stream.atEnd());
        }
        QVERIFY(filter != NULL);
        filter->start();
        filter->setEgress(&e);

        begin = Time::time();
        for (; begin + 10.0 >= Time::time(); t += 0.5) {
            filter->incomingData(SequenceValue(value, Variant::Root(1.0), 60.0, FP::undefined()));
            QTest::qSleep(500);
        }
        filter->endData();

        QVERIFY(filter->wait(30));
        delete filter;

        bool hitRaw = false;
        bool hitInstant = false;
        bool hitBoxcar = false;
        for (const auto &v : e.values()) {
            QCOMPARE(v.getStation(), SequenceName::Component("brw"));
            QCOMPARE(v.getVariable(), SequenceName::Component("T_S11"));
            if (v.getArchive() == "raw") {
                hitRaw = true;
            } else if (v.getArchive() == "rt_instant") {
                hitInstant = true;
            } else if (v.getArchive() == "rt_boxcar") {
                hitBoxcar = true;
            } else {
                QFAIL("Invalid archive");
            }
        }

        QVERIFY(hitRaw);
        QVERIFY(hitInstant);
        QVERIFY(hitBoxcar);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
