/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/variant/composite.hxx"
#include "smoothing/realtimechain.hxx"

#include "smooth_realtime.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;


QString SmoothRealtimeComponent::getGeneralSerializationName() const
{ return QString::fromLatin1("smooth_realtime"); }

ComponentOptions SmoothRealtimeComponent::getOptions()
{
    ComponentOptions options;

    ComponentOptionTimeOffset *tis =
            new ComponentOptionTimeOffset(tr("interval", "name"), tr("The averaging interval"),
                                          tr("This is width of the bin to average on.  All data within each "
                                                     "interval is averaged together to produce the output.  An "
                                                     "undefined interval averages all available data together."),
                                          tr("One minute", "default interval"), 1);
    tis->setAllowZero(true);
    tis->setAllowNegative(false);
    tis->setDefault(Time::Minute, 1, true);
    options.add("interval", tis);

    options.add("statistics", new ComponentOptionBoolean(tr("statistics", "name"),
                                                         tr("Enable statistics generation"),
                                                         tr("This enables statistics generation for all the averaged outputs."),
                                                         QString(), 1));

    return options;
}

QList<ComponentExample> SmoothRealtimeComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Default", "default example name"),
                                     tr("This creates a realtime smoother that generates data on one minute interval.")));

    return examples;
}

ProcessingStage *SmoothRealtimeComponent::createGeneralFilterDynamic(const ComponentOptions &options)
{
    bool stats = false;
    if (options.isSet("statistics")) {
        stats = qobject_cast<ComponentOptionBoolean *>(options.get("statistics"))->get();
    }

    ComponentOptionTimeOffset
            *avg = qobject_cast<ComponentOptionTimeOffset *>(options.get("interval"));

    RealtimeEngine *engine = new RealtimeEngine(stats);
    engine->setAveraging(avg->getUnit(), avg->getCount(), avg->getAlign());
    return engine;
}

ProcessingStage *SmoothRealtimeComponent::createGeneralFilterEditing(double start,
                                                                     double end,
                                                                     const SequenceName::Component &station,
                                                                     const SequenceName::Component &archive,
                                                                     const ValueSegment::Transfer &config)
{
    Q_UNUSED(station);
    Q_UNUSED(archive);

    int count = 1;
    bool align = true;
    Time::LogicalTimeUnit unit = Time::Minute;
    bool stats = false;

    if (!config.empty()) {
        unit = Variant::Composite::toTimeInterval(config.back().getValue().hash("Interval"), &count,
                                                  &align);
        stats = config.back().getValue().hash("EnableStatistics").toBool();
    }

    RealtimeEngine *engine = new RealtimeEngine(stats);
    engine->setAveraging(unit, count, align);
    return engine;
}

void SmoothRealtimeComponent::extendGeneralFilterEditing(double &start,
                                                         double &end,
                                                         const SequenceName::Component &,
                                                         const SequenceName::Component &,
                                                         const ValueSegment::Transfer &config,
                                                         Archive::Access *)
{
    int count = 1;
    bool align = true;
    Time::LogicalTimeUnit unit = Time::Minute;
    if (!config.empty()) {
        unit = Variant::Composite::toTimeInterval(config.back().getValue().hash("Interval"), &count,
                                                  &align);
    }

    start = Time::floor(start, unit, count);
    end = Time::ceil(end, unit, count);
}

ProcessingStage *SmoothRealtimeComponent::deserializeGeneralFilter(QDataStream &stream)
{
    RealtimeEngine *engine = new RealtimeEngine;
    engine->deserialize(stream);
    return engine;
}
