/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <array>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"
#include "algorithms/dewpoint.hxx"

#include "acquire_tsi_neph3563.hxx"


//#define SHOW_ANALOG_CALIBRATION_COMMANDS


using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Algorithms;

static const std::array<std::string, 3> colorLookup = {"B", "G", "R"};

static const char *instrumentFlagTranslation[16] = {"LampPowerError",           /* 0x0001 */
                                                    "ValveFault",               /* 0x0002 */
                                                    "ChopperFault",             /* 0x0004 */
                                                    "ShutterFault",             /* 0x0008 */
                                                    "HeaterUnstable",           /* 0x0010 */
                                                    "PressureOutOfRange",       /* 0x0020 */
                                                    "TemperatureOutOfRange",    /* 0x0040 */
                                                    "InletTemperatureOutOfRange",/* 0x0080 */
                                                    "RHOutOfRange",             /* 0x0100 */
                                                    NULL,                       /* 0x0200 */
                                                    NULL,                       /* 0x0400 */
                                                    NULL,                       /* 0x0800 */
                                                    NULL,                       /* 0x1000 */
                                                    NULL,                       /* 0x2000 */
                                                    NULL,                       /* 0x4000 */
                                                    NULL,                       /* 0x8000 */
};

static double roundExponent(double d, int digits = 3)
{
    if (digits <= 0 || !FP::defined(d))
        return d;
    double m = fabs(d);
    if (m != 0.0) {
        int base = (int) floor(log10(m));
        base -= digits;
        m = pow(10.0, base);
        d = qRound(d / m) * m;
    }
    return d;
}

static double roundDecimal(double d, int digits = 3)
{
    if (digits <= 0 || !FP::defined(d))
        return d;
    double m = pow(10.0, digits);
    return qRound(d * m) / m;
}

static bool undefinedOrEqual(qint64 a, qint64 b)
{
    if (!INTEGER::defined(a) || !INTEGER::defined(b))
        return true;
    return a == b;
}

static bool undefinedOrEqual(double a, double b)
{
    if (!FP::defined(a) || !FP::defined(b))
        return true;
    double norm = qMax(fabs(a), fabs(b));
    if (norm == 0.0)
        norm = 1.0;
    return (fabs(a - b) / norm) < 1E-6;
}

static void setIfDefined(qint64 &target, const CPD3::Data::Variant::Read &value)
{
    auto i = Variant::Composite::toInteger(value, true);
    if (!INTEGER::defined(i))
        return;
    target = i;
}

static void setIfDefined(double &target, const CPD3::Data::Variant::Read &value, int digits = 0)
{
    double d = Variant::Composite::toNumber(value, true);
    if (!FP::defined(d))
        return;
    if (digits > 0) {
        double m = pow(10.0, digits);
        d = qRound(d * m) / m;
    } else if (digits < 0) {
        d = roundExponent(d, -digits);
    }
    target = d;
}

static bool setIfOk(qint64 &target,
                    const Util::ByteView &input,
                    qint64 minimum = INTEGER::undefined(),
                    qint64 maximum = INTEGER::undefined(),
                    bool explicitZero = false)
{
    bool ok = false;
    qint64 v = input.toQByteArray().trimmed().toLongLong(&ok);
    if (!ok)
        return false;
    if (!INTEGER::defined(v))
        return false;
    if (!explicitZero || v != 0) {
        if (INTEGER::defined(minimum) && v < minimum)
            return false;
        if (INTEGER::defined(maximum) && v > maximum)
            return false;
    }
    target = v;
    return true;
}

static bool setIfOk(double &target,
                    const Util::ByteView &input,
                    double minimum = FP::undefined(),
                    double maximum = FP::undefined(),
                    double scale = 1.0)
{
    bool ok = false;
    double v = input.toQByteArray().trimmed().toDouble(&ok);
    if (!ok)
        return false;
    if (!FP::defined(v))
        return false;
    if (FP::defined(minimum) && v < minimum)
        return false;
    if (FP::defined(maximum) && v > maximum)
        return false;
    target = v * scale;
    return true;
}

AcquireTSINeph3563::GenericParameter::GenericParameter()
{ }

AcquireTSINeph3563::GenericParameter::~GenericParameter()
{ }

Util::ByteArray AcquireTSINeph3563::GenericParameter::getCommand() const
{ return parameter; }

bool AcquireTSINeph3563::GenericParameter::readAfterSet(const GenericParameter *)
{ return true; }

AcquireTSINeph3563::AnalogCalibration::AnalogCalibration() : adcLow(INTEGER::undefined()),
                                                             valueLow(FP::undefined()),
                                                             adcHigh(INTEGER::undefined()),
                                                             valueHigh(FP::undefined())
{ }

AcquireTSINeph3563::AnalogCalibration::~AnalogCalibration()
{ }

bool AcquireTSINeph3563::AnalogCalibration::equals(const GenericParameter *base) const
{
    const AnalogCalibration *other = static_cast<const AnalogCalibration *>(base);
    return undefinedOrEqual(adcLow, other->adcLow) &&
            undefinedOrEqual(valueLow, other->valueLow) &&
            undefinedOrEqual(adcHigh, other->adcHigh) &&
            undefinedOrEqual(valueHigh, other->valueHigh);
}

void AcquireTSINeph3563::AnalogCalibration::copy(const GenericParameter *base)
{
    const AnalogCalibration *other = static_cast<const AnalogCalibration *>(base);
    adcLow = other->adcLow;
    valueLow = other->valueLow;
    adcHigh = other->adcHigh;
    valueHigh = other->valueHigh;
}

void AcquireTSINeph3563::AnalogCalibration::fromValue(const Variant::Read &value)
{
    setIfDefined(adcLow, value.hash("Low").hash("ADC"));
    setIfDefined(valueLow, value.hash("Low").hash("Value"), 1);
    setIfDefined(adcHigh, value.hash("High").hash("ADC"));
    setIfDefined(valueHigh, value.hash("High").hash("Value"), 1);
}

Util::ByteArray AcquireTSINeph3563::AnalogCalibration::setCommand(const GenericParameter *base) const
{
    const AnalogCalibration *other = static_cast<const AnalogCalibration *>(base);
    auto result = parameter;
    if (INTEGER::defined(adcLow))
        result += QByteArray::number(adcLow);
    else if (INTEGER::defined(other->adcLow))
        result += QByteArray::number(other->adcLow);
    result += ",";
    if (FP::defined(valueLow))
        result += QByteArray::number((qint64) floor(valueLow * 10.0 + 0.5));
    else if (FP::defined(other->valueLow))
        result += QByteArray::number((qint64) floor(other->valueLow * 10.0 + 0.5));
    result += ",";
    if (INTEGER::defined(adcHigh))
        result += QByteArray::number(adcHigh);
    else if (INTEGER::defined(other->adcHigh))
        result += QByteArray::number(other->adcHigh);
    result += ",";
    if (FP::defined(valueHigh))
        result += QByteArray::number((qint64) floor(valueHigh * 10.0 + 0.5));
    else if (FP::defined(other->valueHigh))
        result += QByteArray::number((qint64) floor(other->valueHigh * 10.0 + 0.5));
    return result;
}

bool AcquireTSINeph3563::AnalogCalibration::fromResponse(const Util::ByteArray &response)
{
    auto fields = CSV::acquisitionSplit(response);
    if (fields.size() < 4)
        return false;
    if (parameter == "SCP") {
        if (!setIfOk(adcLow, fields.front(), 0, 65534))
            return false;
        fields.pop_front();
        if (!setIfOk(valueLow, fields.front(), 0, 11999.0, 0.1))
            return false;
        fields.pop_front();
        if (!setIfOk(adcHigh, fields.front(), 0, 65535))
            return false;
        fields.pop_front();
        if (!setIfOk(valueHigh, fields.front(), 1, 12000.0, 0.1))
            return false;
        fields.pop_front();
    } else if (parameter == "SCR") {
        if (!setIfOk(adcLow, fields.front(), 0, 65534))
            return false;
        fields.pop_front();
        if (!setIfOk(valueLow, fields.front(), 0, 999.0, 0.1))
            return false;
        fields.pop_front();
        if (!setIfOk(adcHigh, fields.front(), 0, 65535))
            return false;
        fields.pop_front();
        if (!setIfOk(valueHigh, fields.front(), 1, 1000.0, 0.1))
            return false;
        fields.pop_front();
    } else {
        if (!setIfOk(adcLow, fields.front(), 0, 65534))
            return false;
        fields.pop_front();
        if (!setIfOk(valueLow, fields.front(), 0, 3999.0, 0.1))
            return false;
        fields.pop_front();
        if (!setIfOk(adcHigh, fields.front(), 0, 65535))
            return false;
        fields.pop_front();
        if (!setIfOk(valueHigh, fields.front(), 1, 4000.0, 0.1))
            return false;
        fields.pop_front();
    }
    return true;
}

Variant::Root AcquireTSINeph3563::AnalogCalibration::toValue() const
{
    Variant::Root output;
    auto result = output.write();
    if (INTEGER::defined(adcLow))
        result.hash("Low").hash("ADC").setInt64(adcLow);
    if (FP::defined(valueLow))
        result.hash("Low").hash("Value").setDouble(valueLow);
    if (INTEGER::defined(adcHigh))
        result.hash("High").hash("ADC").setInt64(adcHigh);
    if (FP::defined(valueHigh))
        result.hash("High").hash("Value").setDouble(valueHigh);
    return output;
}

Variant::Root AcquireTSINeph3563::AnalogCalibration::metadata() const
{
    Variant::Root output;
    auto result = output.write();
    result.metadataHash("Description").setString("Analog calibration value");
    result.metadataHashChild("Low")
          .metadataHash("Description")
          .setString("Low side of the linear calibration");

    result.metadataHashChild("Low")
          .metadataHashChild("ADC")
          .metadataInteger("Description")
          .setString("ADC count value at the low side");
    result.metadataHashChild("Low").metadataHashChild("ADC").metadataInteger("Minimum").setInt64(0);
    result.metadataHashChild("Low")
          .metadataHashChild("ADC")
          .metadataInteger("Maximum")
          .setInt64(65534);

    result.metadataHashChild("Low")
          .metadataHashChild("Value")
          .metadataReal("Description")
          .setString("Physical units at the low side");
    result.metadataHashChild("Low")
          .metadataHashChild("Value")
          .metadataReal("Minimum")
          .setDouble(0.0);
    result.metadataHashChild("Low")
          .metadataHashChild("Value")
          .metadataReal("Format")
          .setString("0000.0");

    result.metadataHashChild("High")
          .metadataHashChild("ADC")
          .metadataInteger("Description")
          .setString("ADC count value at the high side");
    result.metadataHashChild("High")
          .metadataHashChild("ADC")
          .metadataInteger("Minimum")
          .setInt64(1);
    result.metadataHashChild("High")
          .metadataHashChild("ADC")
          .metadataInteger("Maximum")
          .setInt64(65535);

    result.metadataHashChild("High")
          .metadataHashChild("Value")
          .metadataReal("Description")
          .setString("Physical units at the high side");
    result.metadataHashChild("High")
          .metadataHashChild("Value")
          .metadataReal("Minimum")
          .setDouble(0.1);
    result.metadataHashChild("High")
          .metadataHashChild("Value")
          .metadataReal("Format")
          .setString("0000.0");

    if (parameter == "SCP") {
        result.metadataHashChild("Low")
              .metadataHashChild("Value")
              .metadataReal("Maximum")
              .setDouble(1199.9);
        result.metadataHashChild("High")
              .metadataHashChild("Value")
              .metadataReal("Maximum")
              .setDouble(1200.0);
    } else if (parameter == "SCR") {
        result.metadataHashChild("Low")
              .metadataHashChild("Value")
              .metadataReal("Maximum")
              .setDouble(99.9);
        result.metadataHashChild("High")
              .metadataHashChild("Value")
              .metadataReal("Maximum")
              .setDouble(100.0);
    } else {
        result.metadataHashChild("Low")
              .metadataHashChild("Value")
              .metadataReal("Maximum")
              .setDouble(399.9);
        result.metadataHashChild("High")
              .metadataHashChild("Value")
              .metadataReal("Maximum")
              .setDouble(400.0);
    }
    return output;
}

void AcquireTSINeph3563::AnalogCalibration::commandParameter(Variant::Write &target) const
{
#ifdef SHOW_ANALOG_CALIBRATION_COMMANDS
                                                                                                                            QString baseName;
    double maximumValue;
    if (parameter == "SCI") {
        baseName = QObject::tr("Inlet temperature");
        maximumValue = 400.0;
    } else if (parameter == "SCP") {
        baseName = QObject::tr("Pressure");
        maximumValue = 1200.0;
    } else if (parameter == "SCR") {
        baseName = QObject::tr("Sample RH");
        maximumValue = 100.0;
    } else if (parameter == "SCS") {
        baseName = QObject::tr("Sample temperature");
        maximumValue = 400.0;
    } else {
        baseName = parameter;
        maximumValue = 100.0;
    }

    QString sparam(parameter.toQString(false));
    QString v(QString("%1LowADC").arg(sparam));
    target.hash(v).hash("Variable").setString("ZPARAMETERS");
    target.hash(v).hash("Path").setString(
        QString("%1/Low/ADC").arg(sparam));
    target.hash(v).hash("Name").setString(QObject::tr(
        "%1 ADC Low").arg(baseName));
    target.hash(v).hash("Format").setString("00000");
    target.hash(v).hash("Minimum").setInt64(0);
    target.hash(v).hash("Maximum").setInt64(65534);

    v = QString("%1LowValue").arg(sparam);
    target.hash(v).hash("Variable").setString("ZPARAMETERS");
    target.hash(v).hash("Path").setString(
        QString("%1/Low/Value").arg(sparam));
    target.hash(v).hash("Name").setString(QObject::tr(
        "%1 Value Low").arg(baseName));
    target.hash(v).hash("Format").setString("0000.0");
    target.hash(v).hash("Minimum").setDouble(0.0);
    target.hash(v).hash("Maximum").setDouble(maximumValue - 0.1);

    v = QString("%1HighADC").arg(sparam);
    target.hash(v).hash("Variable").setString("ZPARAMETERS");
    target.hash(v).hash("Path").setString(
        QString("%1/High/ADC").arg(sparam));
    target.hash(v).hash("Name").setString(QObject::tr(
        "%1 ADC High").arg(baseName));
    target.hash(v).hash("Format").setString("00000");
    target.hash(v).hash("Minimum").setInt64(1);
    target.hash(v).hash("Maximum").setInt64(65535);

    v = QString("%1HighValue").arg(sparam);
    target.hash(v).hash("Variable").setString("ZPARAMETERS");
    target.hash(v).hash("Path").setString(
        QString("%1/High/Value").arg(sparam));
    target.hash(v).hash("Name").setString(QObject::tr(
        "%1 Value High").arg(baseName));
    target.hash(v).hash("Format").setString("0000.0");
    target.hash(v).hash("Minimum").setDouble(0.0);
    target.hash(v).hash("Maximum").setDouble(maximumValue);
#else
    Q_UNUSED(target);
#endif
}

void AcquireTSINeph3563::AnalogCalibration::fromCommand(const Variant::Read &parameters)
{
    QString sparam(parameter.toQString(false));
    setIfDefined(adcLow, parameters.hash(QString("%1LowADC").arg(sparam)).hash("Value"));
    setIfDefined(valueLow, parameters.hash(QString("%1LowValue").arg(sparam)).hash("Value"), 1);
    setIfDefined(adcHigh, parameters.hash(QString("%1HighADC").arg(sparam)).hash("Value"));
    setIfDefined(valueHigh, parameters.hash(QString("%1HighADC").arg(sparam)).hash("Value"), 1);
}

AcquireTSINeph3563::ChannelCalibration::ChannelCalibration() : K1(INTEGER::undefined()),
                                                               K2(FP::undefined()),
                                                               K3(FP::undefined()),
                                                               K4(FP::undefined())
{ }

AcquireTSINeph3563::ChannelCalibration::~ChannelCalibration()
{ }

bool AcquireTSINeph3563::ChannelCalibration::equals(const GenericParameter *base) const
{
    const ChannelCalibration *other = static_cast<const ChannelCalibration *>(base);
    return undefinedOrEqual(K1, other->K1) &&
            undefinedOrEqual(K2, other->K2) &&
            undefinedOrEqual(K3, other->K3) &&
            undefinedOrEqual(K4, other->K4);
}

void AcquireTSINeph3563::ChannelCalibration::copy(const GenericParameter *base)
{
    const ChannelCalibration *other = static_cast<const ChannelCalibration *>(base);
    K1 = other->K1;
    K2 = other->K2;
    K3 = other->K3;
    K4 = other->K4;
}

void AcquireTSINeph3563::ChannelCalibration::fromValue(const Variant::Read &value)
{
    setIfDefined(K1, value.hash("K1"));
    setIfDefined(K2, value.hash("K2"), -3);
    setIfDefined(K3, value.hash("K3"), -3);
    setIfDefined(K4, value.hash("K4"), 3);
}

Util::ByteArray AcquireTSINeph3563::ChannelCalibration::setCommand(const GenericParameter *base) const
{
    const ChannelCalibration *other = static_cast<const ChannelCalibration *>(base);
    Util::ByteArray result = parameter;
    if (INTEGER::defined(K1))
        result += QByteArray::number(K1);
    else if (INTEGER::defined(other->K1))
        result += QByteArray::number(other->K1);
    result += ",";
    if (FP::defined(K2)) {
        result += FP::scientificFormat(K2, 3, 1, QString('e'), QString(), QString('+')).toLatin1();
    } else if (FP::defined(other->K2)) {
        result += FP::scientificFormat(other->K2, 3, 1, QString('e'), QString(),
                                       QString('+')).toLatin1();
    }
    result += ",";
    if (FP::defined(K3)) {
        result += FP::scientificFormat(K3, 3, 1, QString('e'), QString(), QString('+')).toLatin1();
    } else if (FP::defined(other->K3)) {
        result += FP::scientificFormat(other->K3, 3, 1, QString('e'), QString(),
                                       QString('+')).toLatin1();
    }
    result += ",";
    if (FP::defined(K4)) {
        result += QByteArray::number(K4, 'f', 3);
    } else if (FP::defined(other->K4)) {
        result += QByteArray::number(other->K4, 'f', 3);
    }
    return result;
}

bool AcquireTSINeph3563::ChannelCalibration::fromResponse(const Util::ByteArray &response)
{
    auto fields = CSV::acquisitionSplit(response);
    if (fields.size() < 4)
        return false;
    if (!setIfOk(K1, fields.front(), 0, 65535))
        return false;
    fields.pop_front();
    if (!setIfOk(K2, fields.front(), 0.0))
        return false;
    fields.pop_front();
    if (!setIfOk(K3, fields.front(), 0.0))
        return false;
    fields.pop_front();
    if (!setIfOk(K4, fields.front(), 0.0, 1.0))
        return false;
    fields.pop_front();
    return true;
}

Variant::Root AcquireTSINeph3563::ChannelCalibration::toValue() const
{
    Variant::Root output;
    auto result = output.write();
    if (INTEGER::defined(K1))
        result.hash("K1").setInt64(K1);
    if (FP::defined(K2))
        result.hash("K2").setDouble(K2);
    if (FP::defined(K3))
        result.hash("K3").setDouble(K3);
    if (FP::defined(K4))
        result.hash("K4").setDouble(K4);
    return output;
}

Variant::Root AcquireTSINeph3563::ChannelCalibration::metadata() const
{
    Variant::Root output;
    auto result = output.write();
    result.metadataHash("Description").setString("Wavelength channel calibration");

    result.metadataHashChild("K1")
          .metadataInteger("Description")
          .setString("Photomultiplier tube dead time");
    result.metadataHashChild("K1").metadataInteger("Units").setString("ps");
    result.metadataHashChild("K1").metadataInteger("Minimum").setInt64(1);
    result.metadataHashChild("K1").metadataInteger("Maximum").setInt64(65535);
    result.metadataHashChild("K1").metadataInteger("Format").setString("00000");

    result.metadataHashChild("K2")
          .metadataReal("Description")
          .setString("Total scattering calibration");
    result.metadataHashChild("K2").metadataReal("Units").setString("m\xE2\x81\xBB¹");
    result.metadataHashChild("K2").metadataReal("Format").setString("0.000E0");

    result.metadataHashChild("K3").metadataReal("Description").setString("Air Rayleigh scattering");
    result.metadataHashChild("K3").metadataReal("Units").setString("m\xE2\x81\xBB¹");
    result.metadataHashChild("K3").metadataReal("ReportT").setDouble(0.0);
    result.metadataHashChild("K3").metadataReal("ReportP").setDouble(1013.25);
    result.metadataHashChild("K3").metadataReal("Format").setString("0.000E0");

    result.metadataHashChild("K4")
          .metadataReal("Description")
          .setString("Backscattering Rayleigh contribution fraction");
    result.metadataHashChild("K4").metadataReal("Format").setString("0.000");
    return output;
}

void AcquireTSINeph3563::ChannelCalibration::commandParameter(Variant::Write &target) const
{
    QString sparam(parameter.toQString(false));
    QString v(QString("%1K1").arg(sparam));
    target.hash(v).hash("Variable").setString("ZPARAMETERS");
    target.hash(v).hash("Path").setString(QString("%1/K1").arg(sparam));
    target.hash(v).hash("Name").setString(QObject::tr("%1 K1 - PMT dead time (ps)").arg(sparam));
    target.hash(v).hash("Format").setString("00000");
    target.hash(v).hash("Minimum").setInt64(1);
    target.hash(v).hash("Maximum").setInt64(65535);

    v = QString("%1K2").arg(sparam);
    target.hash(v).hash("Variable").setString("ZPARAMETERS");
    target.hash(v).hash("Path").setString(QString("%1/K2").arg(sparam));
    target.hash(v)
          .hash("Name")
          .setString(QObject::tr("%1 K2 - Total Calibration (m\xE2\x81\xBB¹)").arg(sparam));
    target.hash(v).hash("Format").setString("0.000E0");

    v = QString("%1K3").arg(sparam);
    target.hash(v).hash("Variable").setString("ZPARAMETERS");
    target.hash(v).hash("Path").setString(QString("%1/K3").arg(sparam));
    target.hash(v)
          .hash("Name")
          .setString(QObject::tr("%1 K3 - Air Rayleigh Scattering (m\xE2\x81\xBB¹)").arg(sparam));
    target.hash(v).hash("Format").setString("0.000E0");

    v = QString("%1K4").arg(sparam);
    target.hash(v).hash("Variable").setString("ZPARAMETERS");
    target.hash(v).hash("Path").setString(QString("%1/K4").arg(sparam));
    target.hash(v)
          .hash("Name")
          .setString(QObject::tr("%1 K4 - Backscattering Rayleigh fraction").arg(sparam));
    target.hash(v).hash("Format").setString("0.000");
}

void AcquireTSINeph3563::ChannelCalibration::fromCommand(const Variant::Read &parameters)
{
    QString sparam(parameter.toQString(false));
    setIfDefined(K1, parameters.hash(QString("%1K1").arg(sparam)).hash("Value"));
    setIfDefined(K2, parameters.hash(QString("%1K2").arg(sparam)).hash("Value"), -3);
    setIfDefined(K3, parameters.hash(QString("%1K3").arg(sparam)).hash("Value"), -3);
    setIfDefined(K4, parameters.hash(QString("%1K4").arg(sparam)).hash("Value"), 3);
}

AcquireTSINeph3563::StringParameter::StringParameter() : value()
{ }

AcquireTSINeph3563::StringParameter::~StringParameter()
{ }

bool AcquireTSINeph3563::StringParameter::equals(const GenericParameter *base) const
{
    const StringParameter *other = static_cast<const StringParameter *>(base);
    if (value.empty() || other->value.empty())
        return true;
    return value == other->value;
}

void AcquireTSINeph3563::StringParameter::copy(const GenericParameter *base)
{
    value = static_cast<const StringParameter *>(base)->value;
}

void AcquireTSINeph3563::StringParameter::fromValue(const Variant::Read &value)
{
    const auto &check = value.toString();
    if (check.empty())
        return;
    this->value = check;
}

Util::ByteArray AcquireTSINeph3563::StringParameter::setCommand(const GenericParameter *base) const
{
    const StringParameter *other = static_cast<const StringParameter *>(base);
    Util::ByteArray result = parameter;
    if (!value.empty())
        result += value;
    else
        result += other->value;
    return result;
}

bool AcquireTSINeph3563::StringParameter::fromResponse(const Util::ByteArray &response)
{
    if (response.empty() || response == "ERROR")
        return false;
    value = response.toString();
    return true;
}

Variant::Root AcquireTSINeph3563::StringParameter::toValue() const
{ return Variant::Root(this->value); }

Variant::Root AcquireTSINeph3563::StringParameter::metadata() const
{ return Variant::Root(Variant::Type::MetadataString); }

void AcquireTSINeph3563::StringParameter::commandParameter(Variant::Write &target) const
{
    auto wr = target.hash(parameter.toString());
    wr.hash("Variable").setString("ZPARAMETERS");
    wr.hash("Path").setString(parameter.toString());
    wr.hash("Type").setString("String");
}

void AcquireTSINeph3563::StringParameter::fromCommand(const Variant::Read &parameters)
{
    fromValue(parameters.hash(parameter.toString()).hash("Value"));
}

AcquireTSINeph3563::BooleanParameter::BooleanParameter() : value(INTEGER::undefined())
{ }

AcquireTSINeph3563::BooleanParameter::~BooleanParameter()
{ }

bool AcquireTSINeph3563::BooleanParameter::equals(const GenericParameter *base) const
{
    return undefinedOrEqual(value, static_cast<const BooleanParameter *>(base)->value);
}

void AcquireTSINeph3563::BooleanParameter::copy(const GenericParameter *base)
{
    value = static_cast<const BooleanParameter *>(base)->value;
}

void AcquireTSINeph3563::BooleanParameter::fromValue(const Variant::Read &value)
{
    if (!value.exists())
        return;
    this->value = value.toBool() ? 1 : 0;
}

Util::ByteArray AcquireTSINeph3563::BooleanParameter::setCommand(const GenericParameter *base) const
{
    const BooleanParameter *other = static_cast<const BooleanParameter *>(base);
    Util::ByteArray result = parameter;
    if (INTEGER::defined(value))
        result += value ? "1" : "0";
    else if (INTEGER::defined(other->value))
        result += other->value ? "1" : "0";
    return result;
}

bool AcquireTSINeph3563::BooleanParameter::fromResponse(const Util::ByteArray &response)
{
    return setIfOk(value, response, 0, 1);
}

Variant::Root AcquireTSINeph3563::BooleanParameter::toValue() const
{
    if (!INTEGER::defined(this->value))
        return Variant::Root();
    return Variant::Root(this->value ? true : false);
}

Variant::Root AcquireTSINeph3563::BooleanParameter::metadata() const
{ return Variant::Root(Variant::Type::MetadataBoolean); }

void AcquireTSINeph3563::BooleanParameter::commandParameter(Variant::Write &target) const
{
    auto wr = target.hash(parameter.toString());
    wr.hash("Variable").setString("ZPARAMETERS");
    wr.hash("Path").setString(parameter.toString());
    wr.hash("Type").setString("Boolean");
}

void AcquireTSINeph3563::BooleanParameter::fromCommand(const Variant::Read &parameters)
{
    fromValue(parameters.hash(parameter.toString()).hash("Value"));
}

AcquireTSINeph3563::IntegerParameter::IntegerParameter() : value(INTEGER::undefined()),
                                                           minimum(INTEGER::undefined()),
                                                           maximum(INTEGER::undefined()),
                                                           explicitZero(false)
{ }

AcquireTSINeph3563::IntegerParameter::~IntegerParameter()
{ }

bool AcquireTSINeph3563::IntegerParameter::equals(const GenericParameter *base) const
{
    return undefinedOrEqual(value, static_cast<const IntegerParameter *>(base)->value);
}

void AcquireTSINeph3563::IntegerParameter::copy(const GenericParameter *base)
{
    value = static_cast<const IntegerParameter *>(base)->value;
}

void AcquireTSINeph3563::IntegerParameter::fromValue(const Variant::Read &value)
{
    qint64 check = Variant::Composite::toInteger(value);
    if (!INTEGER::defined(check))
        return;
    if (!explicitZero || check != 0) {
        if (INTEGER::defined(minimum) && check < minimum)
            return;
        if (INTEGER::defined(maximum) && check > maximum)
            return;
    }
    this->value = check;
}

Util::ByteArray AcquireTSINeph3563::IntegerParameter::setCommand(const GenericParameter *base) const
{
    const IntegerParameter *other = static_cast<const IntegerParameter *>(base);
    Util::ByteArray result(parameter);
    if (INTEGER::defined(value))
        result += QByteArray::number(value);
    else if (INTEGER::defined(other->value))
        result += QByteArray::number(other->value);
    return result;
}

bool AcquireTSINeph3563::IntegerParameter::fromResponse(const Util::ByteArray &response)
{
    return setIfOk(value, response, minimum, maximum, explicitZero);
}

Variant::Root AcquireTSINeph3563::IntegerParameter::toValue() const
{ return Variant::Root(this->value); }

Variant::Root AcquireTSINeph3563::IntegerParameter::metadata() const
{ return Variant::Root(Variant::Type::MetadataInteger); }

void AcquireTSINeph3563::IntegerParameter::commandParameter(Variant::Write &target) const
{
    auto wr = target.hash(parameter.toString());
    wr.hash("Variable").setString("ZPARAMETERS");
    wr.hash("Path").setString(parameter.toString());
    wr.hash("Format").setString("0");
    if (INTEGER::defined(minimum)) {
        if (explicitZero && minimum > 0)
            wr.hash("Minimum").setInt64(0);
        else
            wr.hash("Minimum").setInt64(minimum);
    }
    if (INTEGER::defined(minimum))
        wr.hash("Maximum").setInt64(maximum);
}

void AcquireTSINeph3563::IntegerParameter::fromCommand(const Variant::Read &parameters)
{
    fromValue(parameters.hash(parameter.toString()).hash("Value"));
}

AcquireTSINeph3563::ZeroValveParameter::ZeroValveParameter() : value(INTEGER::undefined()),
                                                               automatic(true)
{ }

AcquireTSINeph3563::ZeroValveParameter::~ZeroValveParameter()
{ }

bool AcquireTSINeph3563::ZeroValveParameter::equals(const GenericParameter *base) const
{
    const ZeroValveParameter *other = static_cast<const ZeroValveParameter *>(base);
    if (automatic && other->automatic)
        return true;
    if (automatic || other->automatic)
        return false;
    return undefinedOrEqual(value, other->value);
}

void AcquireTSINeph3563::ZeroValveParameter::copy(const GenericParameter *base)
{
    value = static_cast<const ZeroValveParameter *>(base)->value;
}

void AcquireTSINeph3563::ZeroValveParameter::fromValue(const Variant::Read &value)
{
    switch (value.getType()) {
    case Variant::Type::Empty:
        return;
    case Variant::Type::Boolean:
        this->automatic = false;
        this->value = value.toBool() ? 1 : 0;
        break;
    case Variant::Type::Integer: {
        qint64 check = value.toInt64();
        if (!INTEGER::defined(check)) {
            this->automatic = true;
            break;
        }
        this->automatic = false;
        this->value = check ? 1 : 0;
        break;
    }
    default: {
        const auto &check = value.toString();
        if (Util::equal_insensitive(check, "zero", "z")) {
            this->automatic = false;
            this->value = 1;
        } else if (Util::equal_insensitive(check, "normal", "n")) {
            this->automatic = false;
            this->value = 0;
        } else {
            this->automatic = true;
        }
        break;
    }
    }
}

Util::ByteArray AcquireTSINeph3563::ZeroValveParameter::setCommand(const GenericParameter *base) const
{
    const ZeroValveParameter *other = static_cast<const ZeroValveParameter *>(base);
    Util::ByteArray result = parameter;
    if (automatic) {
        result += "N";
    } else {
        if (INTEGER::defined(value))
            result += value ? "Z" : "N";
        else if (other->automatic)
            result += "N";
        else if (INTEGER::defined(other->value))
            result += other->value ? "Z" : "N";
    }
    return result;
}

bool AcquireTSINeph3563::ZeroValveParameter::fromResponse(const Util::ByteArray &response)
{
    if (response == "ZERO") {
        this->value = 1;
        return true;
    } else if (response == "NORMAL") {
        this->value = 0;
        return true;
    } else if (response == "FAULT") {
        this->value = INTEGER::undefined();
        return true;
    }
    return false;
}

bool AcquireTSINeph3563::ZeroValveParameter::readAfterSet(const GenericParameter *base)
{
    /* The neph will return FAULT if any read is performed before the valve is finished moving,
     * then it will continue returning FAULT until another V command is sent (and the valve
     * finishes moving for that one).  So we just avoid the issue and don't read back the
     * value at all. */
    const ZeroValveParameter *other = static_cast<const ZeroValveParameter *>(base);
    this->automatic = other->automatic;
    this->value = other->value;
    return false;
}

Variant::Root AcquireTSINeph3563::ZeroValveParameter::toValue() const
{
    if (this->automatic || !INTEGER::defined(this->value))
        return Variant::Root("Automatic");
    return Variant::Root(this->value ? "Zero" : "Normal");
}

Variant::Root AcquireTSINeph3563::ZeroValveParameter::metadata() const
{
    Variant::Root output(Variant::Type::MetadataString);
    auto result = output.write();
    result.metadata("Editor").setString("Enum");
    result.metadata("Possible").hash("Automatic").setString("Automatic");
    result.metadata("Possible").hash("Zero").setString("Zero");
    result.metadata("Possible").hash("Normal").setString("Normal");
    return output;
}

void AcquireTSINeph3563::ZeroValveParameter::commandParameter(Variant::Write &target) const
{
    auto wr = target.hash(parameter.toString());
    wr.hash("Variable").setString("ZPARAMETERS");
    wr.hash("Path").setString(parameter.toString());
    wr.hash("Type").setString("Enum");
    wr.hash("Possible").hash("Automatic").setString("Automatic");
    wr.hash("Possible").hash("Zero").setString("Zero");
    wr.hash("Possible").hash("Normal").setString("Normal");
}

void AcquireTSINeph3563::ZeroValveParameter::fromCommand(const Variant::Read &parameters)
{
    fromValue(parameters.hash(parameter.toString()).hash("Value"));
}

void AcquireTSINeph3563::Parameters::create()
{
    {
        AnalogCalibration *add = new AnalogCalibration;
        add->parameter = "SCI";
        parameters.emplace("SCI", std::unique_ptr<GenericParameter>(add));
    }
    {
        AnalogCalibration *add = new AnalogCalibration;
        add->parameter = "SCP";
        parameters.emplace("SCP", std::unique_ptr<GenericParameter>(add));
    }
    {
        AnalogCalibration *add = new AnalogCalibration;
        add->parameter = "SCR";
        parameters.emplace("SCR", std::unique_ptr<GenericParameter>(add));
    }
    {
        AnalogCalibration *add = new AnalogCalibration;
        add->parameter = "SCS";
        parameters.emplace("SCS", std::unique_ptr<GenericParameter>(add));
    }
    {
        ChannelCalibration *add = new ChannelCalibration;
        add->parameter = "SKB";
        parameters.emplace("SKB", std::unique_ptr<GenericParameter>(add));
    }
    {
        ChannelCalibration *add = new ChannelCalibration;
        add->parameter = "SKG";
        parameters.emplace("SKG", std::unique_ptr<GenericParameter>(add));
    }
    {
        ChannelCalibration *add = new ChannelCalibration;
        add->parameter = "SKR";
        parameters.emplace("SKR", std::unique_ptr<GenericParameter>(add));
    }
    {
        IntegerParameter *add = new IntegerParameter;
        add->parameter = "SMZ";
        add->minimum = 0;
        add->maximum = 24;
        parameters.emplace("SMZ", std::unique_ptr<GenericParameter>(add));
    }
    {
        IntegerParameter *add = new IntegerParameter;
        add->parameter = "SP";
        add->minimum = 0;
        add->maximum = 150;
        parameters.emplace("SP", std::unique_ptr<GenericParameter>(add));
    }
    {
        IntegerParameter *add = new IntegerParameter;
        add->parameter = "STA";
        add->minimum = 1;
        add->maximum = 9960;
        parameters.emplace("STA", std::unique_ptr<GenericParameter>(add));
    }
    {
        IntegerParameter *add = new IntegerParameter;
        add->parameter = "STB";
        add->minimum = 15;
        add->maximum = 999;
        parameters.emplace("STB", std::unique_ptr<GenericParameter>(add));
    }
    {
        IntegerParameter *add = new IntegerParameter;
        add->parameter = "STP";
        add->minimum = 10;
        /* Manual says 9999, but it actually goes higher */
        add->maximum = 32767;
        parameters.emplace("STP", std::unique_ptr<GenericParameter>(add));
    }
    {
        IntegerParameter *add = new IntegerParameter;
        add->parameter = "STZ";
        add->minimum = 1;
        add->maximum = 9999;
        parameters.emplace("STZ", std::unique_ptr<GenericParameter>(add));
    }
    {
        IntegerParameter *add = new IntegerParameter;
        add->parameter = "SVB";
        add->minimum = 800;
        add->maximum = 1200;
        parameters.emplace("SVB", std::unique_ptr<GenericParameter>(add));
    }
    {
        IntegerParameter *add = new IntegerParameter;
        add->parameter = "SVG";
        add->minimum = 800;
        add->maximum = 1200;
        parameters.emplace("SVG", std::unique_ptr<GenericParameter>(add));
    }
    {
        IntegerParameter *add = new IntegerParameter;
        add->parameter = "SVR";
        add->minimum = 800;
        add->maximum = 1200;
        parameters.emplace("SVR", std::unique_ptr<GenericParameter>(add));
    }
    {
        IntegerParameter *add = new IntegerParameter;
        add->parameter = "B";
        add->minimum = 0;
        add->maximum = 255;
        parameters.emplace("B", std::unique_ptr<GenericParameter>(add));
    }
    {
        BooleanParameter *add = new BooleanParameter;
        add->parameter = "H";
        parameters.emplace("H", std::unique_ptr<GenericParameter>(add));
    }
    {
        BooleanParameter *add = new BooleanParameter;
        add->parameter = "SMB";
        parameters.emplace("SMB", std::unique_ptr<GenericParameter>(add));
    }
    {
        StringParameter *add = new StringParameter;
        add->parameter = "SL";
        parameters.emplace("SL", std::unique_ptr<GenericParameter>(add));
    }
    {
        ZeroValveParameter *add = new ZeroValveParameter;
        add->parameter = "V";
        parameters.emplace("V", std::unique_ptr<GenericParameter>(add));
    }
}

AcquireTSINeph3563::Parameters::Parameters()
{ create(); }

AcquireTSINeph3563::Parameters::~Parameters() = default;

AcquireTSINeph3563::Parameters::Parameters(const Parameters &other)
{
    create();
    for (const auto &copy : other.parameters) {
        parameters[copy.first]->copy(copy.second.get());
    }
}

AcquireTSINeph3563::Parameters &AcquireTSINeph3563::Parameters::operator=(const Parameters &other)
{
    if (&other == this)
        return *this;
    for (const auto &copy : other.parameters) {
        parameters[copy.first]->copy(copy.second.get());
    }
    return *this;
}

AcquireTSINeph3563::Parameters::Parameters(Parameters &&other) = default;

AcquireTSINeph3563::Parameters &AcquireTSINeph3563::Parameters::operator=(Parameters &&other) = default;

bool AcquireTSINeph3563::Parameters::operator==(const Parameters &other) const
{
    for (const auto &check : parameters) {
        auto op = other.parameters.find(check.first);
        Q_ASSERT(op != other.parameters.end());
        if (!check.second->equals(op->second.get()))
            return false;
    }
    return true;
}

void AcquireTSINeph3563::Parameters::setDefaults()
{
    parameters["SMZ"]->fromValue(Variant::Root(1));
    parameters["STA"]->fromValue(Variant::Root(1));
    parameters["STB"]->fromValue(Variant::Root(62));
    parameters["STP"]->fromValue(Variant::Root(32000));
    parameters["STZ"]->fromValue(Variant::Root(300));
    parameters["SMB"]->fromValue(Variant::Root(true));
    parameters["SP"]->fromValue(Variant::Root(75));
    parameters["B"]->fromValue(Variant::Root(255));
}


AcquireTSINeph3563::Configuration::Configuration() : start(FP::undefined()),
                                                     end(FP::undefined()),
                                                     wavelengths(),
                                                     strictMode(false),
                                                     parameters(),
                                                     reportInterval(1.0),
                                                     maximumLampCurrent(7.0),
                                                     disableSpancheckValve(false)
{
    wavelengths[0] = 450;
    wavelengths[1] = 550;
    wavelengths[2] = 700;
    parameters.setDefaults();
}

AcquireTSINeph3563::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          wavelengths(),
          strictMode(other.strictMode),
          parameters(other.parameters),
          reportInterval(other.reportInterval),
          maximumLampCurrent(other.maximumLampCurrent),
          disableSpancheckValve(other.disableSpancheckValve)
{
    memcpy(wavelengths, other.wavelengths, sizeof(wavelengths));
}

AcquireTSINeph3563::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          wavelengths(),
          strictMode(false),
          parameters(),
          reportInterval(1.0),
          maximumLampCurrent(7.0),
          disableSpancheckValve(false)
{
    wavelengths[0] = 450;
    wavelengths[1] = 550;
    wavelengths[2] = 700;
    parameters.setDefaults();
    setFromSegment(other);
}

AcquireTSINeph3563::Configuration::Configuration(const Configuration &under,
                                                 const ValueSegment &over,
                                                 double s,
                                                 double e) : start(s),
                                                             end(e),
                                                             wavelengths(),
                                                             strictMode(under.strictMode),
                                                             parameters(under.parameters),
                                                             reportInterval(under.reportInterval),
                                                             maximumLampCurrent(
                                                                     under.maximumLampCurrent),
                                                             disableSpancheckValve(
                                                                     under.disableSpancheckValve)
{
    memcpy(wavelengths, under.wavelengths, sizeof(wavelengths));
    setFromSegment(over);
}

void AcquireTSINeph3563::Configuration::setFromSegment(const ValueSegment &config)
{
    if (FP::defined(config["Wavelength/B"].toDouble()))
        wavelengths[0] = config["Wavelength/B"].toDouble();
    if (FP::defined(config["Wavelength/G"].toDouble()))
        wavelengths[1] = config["Wavelength/G"].toDouble();
    if (FP::defined(config["Wavelength/R"].toDouble()))
        wavelengths[2] = config["Wavelength/R"].toDouble();

    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    for (const auto &set : parameters.parameters) {
        set.second->fromValue(config["Parameters"].hash(set.first));
    }

    {
        double v = config["ReportInterval"].toDouble();
        if (FP::defined(v) && v > 0.0)
            reportInterval = v;
    }

    {
        double v = config["MaximumLampCurrent"].toDouble();
        if (FP::defined(v) && v > 0.0)
            maximumLampCurrent = v;
    }

    if (config["DisableSpancheckValve"].exists())
        disableSpancheckValve = config["DisableSpancheckValve"].toBool();
}


static Variant::Root defaultSpancheckConfiguration()
{
    Variant::Root result;
    return result;
}


void AcquireTSINeph3563::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("TSI");
    instrumentMeta["Model"].setString("3563");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    realtimeStateUpdated = true;
    realtimeParametersUpdated = true;
    persistentParametersUpdated = true;

    lampLimitTime = FP::undefined();

    memset(streamAge, 0, sizeof(streamAge));
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
    }

    modeUpdateTime = FP::undefined();

    commandTry = 0;

    zeroEffectiveTime = FP::undefined();
    zeroPersistentUpdated = false;
    zeroRealtimeUpdated = false;
    zeroTCount = 0;
    zeroTSum = 0;
    zeroPCount = 0;
    zeroPSum = 0;

    lastMode.setUnit(SequenceName({}, "raw", "F2"));
    spancheckDetails.setUnit(SequenceName({}, "raw", "ZSPANCHECK"));
}


AcquireTSINeph3563::AcquireTSINeph3563(const ValueSegment::Transfer &configData,
                                       const std::string &loggingContext) : FramedInstrument(
        "tsi3563", loggingContext),
                                                                            lastRecordTime(
                                                                                    FP::undefined()),
                                                                            autoprobeStatus(
                                                                                    AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                            responseState(
                                                                                    RESP_PASSIVE_WAIT),
                                                                            sampleState(SAMPLE_RUN),
                                                                            spancheckInterface(
                                                                                    this),
                                                                            loggingMux(
                                                                                    LogStream_TOTAL)
{
    spancheckController =
            new NephelometerSpancheckController(ValueSegment::withPath(configData, "Spancheck"),
                                                defaultSpancheckConfiguration(),
                                                NephelometerSpancheckController::Chopper |
                                                        NephelometerSpancheckController::TSI3563Calibrations);
    spancheckController->setInterface(&spancheckInterface);

    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
    configurationChanged();
}

void AcquireTSINeph3563::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE;
    commandTry = 0;
}

void AcquireTSINeph3563::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireTSINeph3563Component::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireTSINeph3563::AcquireTSINeph3563(const ComponentOptions &options,
                                       const std::string &loggingContext) : FramedInstrument(
        "tsi3563", loggingContext),
                                                                            lastRecordTime(
                                                                                    FP::undefined()),
                                                                            autoprobeStatus(
                                                                                    AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                            responseState(
                                                                                    RESP_PASSIVE_WAIT),
                                                                            sampleState(SAMPLE_RUN),
                                                                            spancheckInterface(
                                                                                    this),
                                                                            loggingMux(
                                                                                    LogStream_TOTAL)
{
    spancheckController = new NephelometerSpancheckController(ValueSegment::Transfer(),
                                                              defaultSpancheckConfiguration(),
                                                              NephelometerSpancheckController::Chopper |
                                                                      NephelometerSpancheckController::TSI3563Calibrations);
    spancheckController->setInterface(&spancheckInterface);

    setDefaultInvalid();
    config.append(Configuration());
    configurationChanged();

    Q_UNUSED(options);
}

AcquireTSINeph3563::~AcquireTSINeph3563()
{
    delete spancheckController;
}


void AcquireTSINeph3563::logValue(double startTime,
                                  double endTime,
                                  SequenceName::Component name,
                                  Variant::Root &&value,
                                  int streamID)
{
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (FP::defined(startTime)) {
        if (!realtimeEgress) {
            loggingMux.incoming(streamID, std::move(dv), loggingEgress);
            return;
        }
        loggingMux.incoming(streamID, dv, loggingEgress);
    } else if (!realtimeEgress) {
        return;
    }
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireTSINeph3563::realtimeValue(double time,
                                       SequenceName::Component name,
                                       Variant::Root &&value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

void AcquireTSINeph3563::invalidateLogValues(double frameTime)
{
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;

        if (loggingEgress != NULL)
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    loggingMux.clear();
    loggingLost(frameTime);

    zeroTCount = 0;
    zeroTSum = 0;
    zeroPCount = 0;
    zeroPSum = 0;
}

SequenceValue::Transfer AcquireTSINeph3563::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_tsi_neph3563");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["CalibrationLabel"].set(instrumentMeta["CalibrationLabel"]);

    result.emplace_back(SequenceName({}, "raw_meta", "T"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("PageMask").setInt64((1 << 0) | (1 << 1));

    result.emplace_back(SequenceName({}, "raw_meta", "Tu"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Inlet temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Inlet"));
    result.back().write().metadataReal("Realtime").hash("PageMask").setInt64((1 << 0) | (1 << 1));

    result.emplace_back(SequenceName({}, "raw_meta", "U"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Sample RH");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("PageMask").setInt64((1 << 0) | (1 << 1));

    result.emplace_back(SequenceName({}, "raw_meta", "Uu"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Inlet RH");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("RHExtrapolate");
    result.back().write().metadataReal("Smoothing").hash("Parameters").hash("Tin").setString("T");
    result.back().write().metadataReal("Smoothing").hash("Parameters").hash("Tout").setString("Tu");
    result.back().write().metadataReal("Smoothing").hash("Parameters").hash("RHin").setString("U");
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Inlet"));
    result.back().write().metadataReal("Realtime").hash("PageMask").setInt64((1 << 0) | (1 << 1));

    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("PageMask").setInt64((1 << 0) | (1 << 1));

    result.emplace_back(SequenceName({}, "raw_meta", "Vl"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Lamp voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Lamp"));
    result.back().write().metadataReal("Realtime").hash("PageMask").setInt64((1 << 0) | (1 << 1));

    result.emplace_back(SequenceName({}, "raw_meta", "Al"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("A");
    result.back().write().metadataReal("Description").setString("Lamp current");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Lamp"));
    result.back().write().metadataReal("Realtime").hash("PageMask").setInt64((1 << 0) | (1 << 1));


    result.emplace_back(SequenceName({}, "raw_meta", "BsB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "BsG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "BsR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);


    result.emplace_back(SequenceName({}, "raw_meta", "BbsB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light backwards-hemispheric scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "BbsG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light backwards-hemispheric scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "BbsR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light backwards-hemispheric scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);


    result.emplace_back(SequenceName({}, "raw_meta", "CdB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Dark count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CDarkTS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "CdG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Dark count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CDarkTS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "CdR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Dark count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CDarkTS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);


    result.emplace_back(SequenceName({}, "raw_meta", "CbdB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Dark backwards-hemispheric count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CDarkBS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "CbdG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Dark backwards-hemispheric count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CDarkBS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "CbdR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Dark backwards-hemispheric count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CDarkBS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);


    result.emplace_back(SequenceName({}, "raw_meta", "CfB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Reference shutter count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("CRef"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "CfG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Reference shutter count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("CRef"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "CfR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Reference shutter count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("CRef"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);


    result.emplace_back(SequenceName({}, "raw_meta", "CsB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Measurement count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CMeasTS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "CsG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Measurement count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CMeasTS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "CsR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Measurement count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CMeasTS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);


    result.emplace_back(SequenceName({}, "raw_meta", "CbsB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Measurement backwards-hemispheric count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CMeasBS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "CbsG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Measurement backwards-hemispheric count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CMeasBS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "CbsR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Measurement backwards-hemispheric count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("CMeasBS"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);


    result.emplace_back(SequenceName({}, "raw_meta", "Tw"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature during zero");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "Pw"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure during zero");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("Hide").setBool(true);


    result.emplace_back(SequenceName({}, "raw_meta", "BswB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient from wall signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BspBK"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "BswG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient from wall signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BspBK"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "BswR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient from wall signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BspBK"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);


    result.emplace_back(SequenceName({}, "raw_meta", "BbswB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light backwards-hemispheric scattering coefficient from wall signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BbspBK"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "BbswG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light backwards-hemispheric scattering coefficient from wall signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BbspBK"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "BbswR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light backwards-hemispheric scattering coefficient from wall signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BbspBK"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);


    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("Blank")
          .hash("Description")
          .setString("Data removed in blank period");
    result.back().write().metadataSingleFlag("Blank").hash("Bits").setInt64(0x40000000);
    result.back()
          .write()
          .metadataSingleFlag("Blank")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("Zero")
          .hash("Description")
          .setString("Zero in progress");
    result.back().write().metadataSingleFlag("Zero").hash("Bits").setInt64(0x20000000);
    result.back()
          .write()
          .metadataSingleFlag("Zero")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("Spancheck")
          .hash("Description")
          .setString("Spancheck in progress");
    result.back()
          .write()
          .metadataSingleFlag("Spancheck")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("BackscatterDisabled")
          .hash("Description")
          .setString("Backscatter shutter disabled");
    result.back()
          .write()
          .metadataSingleFlag("BackscatterDisabled")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("LampPowerError")
          .hash("Description")
          .setString("Lamp power not within 10 percent of the setpoint");
    result.back().write().metadataSingleFlag("LampPowerError").hash("Bits").setInt64(0x00010000);
    result.back()
          .write()
          .metadataSingleFlag("LampPowerError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("ValveFault")
          .hash("Description")
          .setString("Valve fault detected");
    result.back().write().metadataSingleFlag("ValveFault").hash("Bits").setInt64(0x00020000);
    result.back()
          .write()
          .metadataSingleFlag("ValveFault")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("ChopperFault")
          .hash("Description")
          .setString("Chopper fault detected");
    result.back().write().metadataSingleFlag("ChopperFault").hash("Bits").setInt64(0x00040000);
    result.back()
          .write()
          .metadataSingleFlag("ChopperFault")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("ShutterFault")
          .hash("Description")
          .setString("Backscatter shutter fault detected");
    result.back().write().metadataSingleFlag("ShutterFault").hash("Bits").setInt64(0x00080000);
    result.back()
          .write()
          .metadataSingleFlag("ShutterFault")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("HeaterUnstable")
          .hash("Description")
          .setString("Heater active but not stable");
    result.back().write().metadataSingleFlag("HeaterUnstable").hash("Bits").setInt64(0x00100000);
    result.back()
          .write()
          .metadataSingleFlag("HeaterUnstable")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("PressureOutOfRange")
          .hash("Description")
          .setString("Pressure out of range");
    result.back()
          .write()
          .metadataSingleFlag("PressureOutOfRange")
          .hash("Bits")
          .setInt64(0x00200000);
    result.back()
          .write()
          .metadataSingleFlag("PressureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("TemperatureOutOfRange")
          .hash("Description")
          .setString("Sample temperature out of range");
    result.back()
          .write()
          .metadataSingleFlag("TemperatureOutOfRange")
          .hash("Bits")
          .setInt64(0x00400000);
    result.back()
          .write()
          .metadataSingleFlag("TemperatureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("InletTemperatureOutOfRange")
          .hash("Description")
          .setString("Inlet temperature out of range");
    result.back()
          .write()
          .metadataSingleFlag("InletTemperatureOutOfRange")
          .hash("Bits")
          .setInt64(0x00800000);
    result.back()
          .write()
          .metadataSingleFlag("InletTemperatureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("RHOutOfRange")
          .hash("Description")
          .setString("Relative humidity out of range");
    result.back().write().metadataSingleFlag("RHOutOfRange").hash("Bits").setInt64(0x01000000);
    result.back()
          .write()
          .metadataSingleFlag("RHOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");


    result.emplace_back(SequenceName({}, "raw_meta", "F2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataString("Description").setString("Instrument mode string");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataString("Realtime").hash("Name").setString("Mode");
    result.back().write().metadataString("Realtime").hash("PageMask").setInt64((1 << 0) | (1 << 1));
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);


    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECK"),
                        spancheckController->metadata(NephelometerSpancheckController::FullResults,
                                                      processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(2);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadata("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "PCTcB"), spancheckController->metadata(
            NephelometerSpancheckController::TotalPercentError, processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("Error"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Blue"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "PCTcG"), spancheckController->metadata(
            NephelometerSpancheckController::TotalPercentError, processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("Error"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Green"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "PCTcR"), spancheckController->metadata(
            NephelometerSpancheckController::TotalPercentError, processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("Error"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Red"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "PCTbcB"), spancheckController->metadata(
            NephelometerSpancheckController::BackPercentError, processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("ErrorBK"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Blue"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "PCTbcG"), spancheckController->metadata(
            NephelometerSpancheckController::BackPercentError, processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("ErrorBK"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Green"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "PCTbcR"), spancheckController->metadata(
            NephelometerSpancheckController::BackPercentError, processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("ErrorBK"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Red"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);


    result.emplace_back(SequenceName({}, "raw_meta", "CcB"), spancheckController->metadata(
            NephelometerSpancheckController::TotalSensitivityFactor, processing), time,
                        FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("Sens"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Blue"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(2);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "CcG"), spancheckController->metadata(
            NephelometerSpancheckController::TotalSensitivityFactor, processing), time,
                        FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("Sens"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Green"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(2);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "CcR"), spancheckController->metadata(
            NephelometerSpancheckController::TotalSensitivityFactor, processing), time,
                        FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("Sens"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Red"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(2);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "CbcB"), spancheckController->metadata(
            NephelometerSpancheckController::BackSensitivityFactor, processing), time,
                        FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("SensBK"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Blue"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(3);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "CbcG"), spancheckController->metadata(
            NephelometerSpancheckController::BackSensitivityFactor, processing), time,
                        FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("SensBK"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Green"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(3);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "CbcR"), spancheckController->metadata(
            NephelometerSpancheckController::BackSensitivityFactor, processing), time,
                        FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("SensBK"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Red"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(3);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);


    result.emplace_back(SequenceName({}, "raw_meta", "ZPARAMETERS"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("Instrument parameter values");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataHash("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataHash("Realtime").hash("Hide").setBool(true);
    result.back().write().metadataHash("Realtime").hash("NetworkPriority").setInt64(1);
    for (const auto &p : activeParameters.parameters) {
        result.back().write().metadataHashChild(p.first).set(p.second->metadata());
    }
    result.back()
          .write()
          .metadataHashChild("SL")
          .metadata("Description")
          .setString("Calibration label");
    result.back()
          .write()
          .metadataHashChild("SMB")
          .metadata("Description")
          .setString("Enable backscatter shutter");
    result.back()
          .write()
          .metadataHashChild("SMZ")
          .metadata("Description")
          .setString("Zero mode: 0=manual only, 1-24=autozero with average of last N zeros");
    result.back().write().metadataHashChild("SP").metadata("Description").setString("Lamp power");
    result.back().write().metadataHashChild("SP").metadata("Units").setString("W");
    result.back()
          .write()
          .metadataHashChild("STA")
          .metadata("Description")
          .setString("Averaging time");
    result.back().write().metadataHashChild("STA").metadata("Units").setString("s");
    result.back()
          .write()
          .metadataHashChild("STB")
          .metadata("Description")
          .setString("Blanking time");
    result.back().write().metadataHashChild("STB").metadata("Units").setString("s");
    result.back()
          .write()
          .metadataHashChild("STP")
          .metadata("Description")
          .setString("Autozero interval");
    result.back().write().metadataHashChild("STP").metadata("Units").setString("s");
    result.back().write().metadataHashChild("STZ").metadata("Description").setString("Zero length");
    result.back().write().metadataHashChild("STZ").metadata("Units").setString("s");
    result.back()
          .write()
          .metadataHashChild("SVB")
          .metadata("Description")
          .setString("Photomultiplier tube voltage");
    result.back().write().metadataHashChild("SVB").metadata("Units").setString("V");
    result.back()
          .write()
          .metadataHashChild("SVB")
          .metadata("Wavelength")
          .setDouble(config.first().wavelengths[0]);
    result.back()
          .write()
          .metadataHashChild("SVG")
          .metadata("Description")
          .setString("Photomultiplier tube voltage");
    result.back().write().metadataHashChild("SVG").metadata("Units").setString("V");
    result.back()
          .write()
          .metadataHashChild("SVG")
          .metadata("Wavelength")
          .setDouble(config.first().wavelengths[1]);
    result.back()
          .write()
          .metadataHashChild("SVR")
          .metadata("Description")
          .setString("Photomultiplier tube voltage");
    result.back().write().metadataHashChild("SVR").metadata("Units").setString("V");
    result.back()
          .write()
          .metadataHashChild("SVR")
          .metadata("Wavelength")
          .setDouble(config.first().wavelengths[0]);
    result.back()
          .write()
          .metadataHashChild("SKB")
          .metadata("Wavelength")
          .setDouble(config.first().wavelengths[0]);
    result.back()
          .write()
          .metadataHashChild("SKG")
          .metadata("Wavelength")
          .setDouble(config.first().wavelengths[1]);
    result.back()
          .write()
          .metadataHashChild("SKR")
          .metadata("Wavelength")
          .setDouble(config.first().wavelengths[2]);
    result.back()
          .write()
          .metadataHashChild("B")
          .metadata("Description")
          .setString("Blower power (0-255)");
    result.back().write().metadataHashChild("H").metadata("Description").setString("Enable heater");
    result.back()
          .write()
          .metadataHashChild("V")
          .metadata("Description")
          .setString("Zero valve state");

    return result;
}

SequenceValue::Transfer AcquireTSINeph3563::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_tsi_neph3563");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["CalibrationLabel"].set(instrumentMeta["CalibrationLabel"]);

    /* These exist so that the text mode clients show values during spanchecks
     * and zeros, but the plots and logged values do not. */
    result.emplace_back(SequenceName({}, "raw_meta", "ZBsB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("Bsp"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");

    result.emplace_back(SequenceName({}, "raw_meta", "ZBsG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("Bsp"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");

    result.emplace_back(SequenceName({}, "raw_meta", "ZBsR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("Bsp"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");


    result.emplace_back(SequenceName({}, "raw_meta", "ZBbsB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light backwards-hemispheric scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("Bbsp"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");

    result.emplace_back(SequenceName({}, "raw_meta", "ZBbsG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light backwards-hemispheric scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("Bbsp"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");

    result.emplace_back(SequenceName({}, "raw_meta", "ZBbsR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light backwards-hemispheric scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("Bbsp"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");


    result.emplace_back(SequenceName({}, "raw_meta", "Vx"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("BNC voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);


    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataString("Realtime").hash("PageMask").setInt64((1 << 0) | (1 << 1));
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(8);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Zero")
          .setString(QObject::tr("Zero in progress", "zero state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Spancheck")
          .setString(QObject::tr("Spancheck in progress", "spancheck state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Blank")
          .setString(QObject::tr("Blank in progress", "blank state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidRecord")
          .setString(QObject::tr("NO COMMS: Invalid record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveStopReportsAck")
          .setString(QObject::tr("STARTING COMMS: Stopping unpolled records"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveStopReportsPowerUp")
          .setString(QObject::tr("STARTING COMMS: Attempting power up"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetDelimiter")
          .setString(QObject::tr("STARTING COMMS: Setting delimiter"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetAnalogOut")
          .setString(QObject::tr("STARTING COMMS: Setting analog output"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadFirmwareVersion")
          .setString(QObject::tr("STARTING COMMS: Reading firmware version"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveEnableRecords")
          .setString(QObject::tr("STARTING COMMS: Enabling records"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetTime")
          .setString(QObject::tr("STARTING COMMS: Setting time"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadParameters")
          .setString(QObject::tr("STARTING COMMS: Reading parameters"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveWriteParameters")
          .setString(QObject::tr("STARTING COMMS: Writing parameters"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadRecordY")
          .setString(QObject::tr("STARTING COMMS: Reading Y record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadRecordD")
          .setString(QObject::tr("STARTING COMMS: Reading D record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadRecordZ")
          .setString(QObject::tr("STARTING COMMS: Reading Z record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveUnpolledStartFlushing")
          .setString(QObject::tr("STARTING COMMS: Flushing initial unpolled"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveUnpolledStartReadFirstRecord")
          .setString(QObject::tr("STARTING COMMS: Waiting for first record"));


    result.emplace_back(SequenceName({}, "raw_meta", "BswdB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient from wall signal shift");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BspBKShift"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "BswdG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient from wall signal shift");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BspBKShift"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "BswdR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient from wall signal shift");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BspBKShift"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);


    result.emplace_back(SequenceName({}, "raw_meta", "BbswdB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString(
                  "Aerosol light backwards-hemispheric scattering coefficient from wall signal shift");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BbspBKShift"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "BbswdG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString(
                  "Aerosol light backwards-hemispheric scattering coefficient from wall signal shift");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BbspBKShift"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "BbswdR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString(
                  "Aerosol light backwards-hemispheric scattering coefficient from wall signal shift");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("BbspBKShift"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);


    result.emplace_back(SequenceName({}, "raw_meta", "ZRTIME"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataInteger("Format").setString("00000");
    result.back().write().metadataInteger("Units").setString("s");
    result.back()
          .write()
          .metadataInteger("Description")
          .setString("Time remaining in current state");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataInteger("Realtime").hash("Name").setString("Time left");
    result.back()
          .write()
          .metadataInteger("Realtime")
          .hash("PageMask")
          .setInt64((1 << 0) | (1 << 1));
    result.back().write().metadataInteger("Realtime").hash("Persistent").setBool(true);

    /* This exists so the flags in the realtime window are what the instrument
     * outputs */
    result.emplace_back(SequenceName({}, "raw_meta", "ZINSTFLAGS"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);
    result.back().write().metadataFlags("Realtime").hash("Name").setString("Flags");
    result.back().write().metadataFlags("Realtime").hash("PageMask").setInt64((1 << 0) | (1 << 1));

    result.back()
          .write()
          .metadataSingleFlag("LampPowerError")
          .hash("Description")
          .setString("Lamp power not withint 10 percent of the setpoint");
    result.back().write().metadataSingleFlag("LampPowerError").hash("Bits").setInt64(0x0001);
    result.back()
          .write()
          .metadataSingleFlag("LampPowerError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("ValveFault")
          .hash("Description")
          .setString("Valve fault detected");
    result.back().write().metadataSingleFlag("ValveFault").hash("Bits").setInt64(0x0002);
    result.back()
          .write()
          .metadataSingleFlag("ValveFault")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("ChopperFault")
          .hash("Description")
          .setString("Chopper fault detected");
    result.back().write().metadataSingleFlag("ChopperFault").hash("Bits").setInt64(0x0004);
    result.back()
          .write()
          .metadataSingleFlag("ChopperFault")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("ShutterFault")
          .hash("Description")
          .setString("Backscatter shutter fault detected");
    result.back().write().metadataSingleFlag("ShutterFault").hash("Bits").setInt64(0x0008);
    result.back()
          .write()
          .metadataSingleFlag("ShutterFault")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("HeaterUnstable")
          .hash("Description")
          .setString("Heater active but not stable");
    result.back().write().metadataSingleFlag("HeaterUnstable").hash("Bits").setInt64(0x0010);
    result.back()
          .write()
          .metadataSingleFlag("HeaterUnstable")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("PressureOutOfRange")
          .hash("Description")
          .setString("Pressure out of range");
    result.back().write().metadataSingleFlag("PressureOutOfRange").hash("Bits").setInt64(0x0020);
    result.back()
          .write()
          .metadataSingleFlag("PressureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("TemperatureOutOfRange")
          .hash("Description")
          .setString("Sample temperature out of range");
    result.back().write().metadataSingleFlag("TemperatureOutOfRange").hash("Bits").setInt64(0x0040);
    result.back()
          .write()
          .metadataSingleFlag("TemperatureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("InletTemperatureOutOfRange")
          .hash("Description")
          .setString("Inlet temperature out of range");
    result.back()
          .write()
          .metadataSingleFlag("InletTemperatureOutOfRange")
          .hash("Bits")
          .setInt64(0x0080);
    result.back()
          .write()
          .metadataSingleFlag("InletTemperatureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");

    result.back()
          .write()
          .metadataSingleFlag("RHOutOfRange")
          .hash("Description")
          .setString("Relative humidity out of range");
    result.back().write().metadataSingleFlag("RHOutOfRange").hash("Bits").setInt64(0x0100);
    result.back()
          .write()
          .metadataSingleFlag("RHOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_neph3563");


    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKSTATE"),
                        spancheckController->metadata(
                                NephelometerSpancheckController::RealtimeState, processing), time,
                        FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);


    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKk2B"),
                        spancheckController->metadata(NephelometerSpancheckController::TSI3563K2,
                                                      processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("k2"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Blue"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(4);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKk2G"),
                        spancheckController->metadata(NephelometerSpancheckController::TSI3563K2,
                                                      processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("k2"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Green"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(4);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKk2R"),
                        spancheckController->metadata(NephelometerSpancheckController::TSI3563K2,
                                                      processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("k2"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Red"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(4);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKk4B"),
                        spancheckController->metadata(NephelometerSpancheckController::TSI3563K2,
                                                      processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("k4"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Blue"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(5);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKk4G"),
                        spancheckController->metadata(NephelometerSpancheckController::TSI3563K2,
                                                      processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("k4"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Green"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(5);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKk4R"),
                        spancheckController->metadata(NephelometerSpancheckController::TSI3563K2,
                                                      processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadata("Realtime").hash("Page").setInt64(2);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("GroupName").setString(QObject::tr("k4"));
    result.back().write().metadata("Realtime").hash("GroupColumn").setString(QObject::tr("Red"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(5);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    return result;
}

SequenceMatch::Composite AcquireTSINeph3563::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "F2");
    sel.append({}, {}, "Tw");
    sel.append({}, {}, "Pw");
    sel.append({}, {}, "Bb?swd?[BGR]");
    sel.append({}, {}, "Cb?c[BGR]");
    sel.append({}, {}, "PCTb?c[BGR]");
    sel.append({}, {}, "ZSPANCHECK.*");
    sel.append({}, {}, "ZRTIME");
    sel.append({}, {}, "ZPARAMETERS");
    sel.append({}, {}, "ZINSTFLAGS");
    sel.append({}, {}, "ZSTATE");
    return sel;
}

void AcquireTSINeph3563::handleZeroUpdate(double currentTime)
{
    if ((!zeroPersistentUpdated || persistentEgress == NULL) &&
            (!zeroRealtimeUpdated || realtimeEgress == NULL))
        return;
    if (!FP::defined(zeroEffectiveTime))
        return;

    SequenceValue::Transfer result;

    if (Tw.read().exists()) {
        result.emplace_back(SequenceIdentity({{}, "raw", "Tw"}, zeroEffectiveTime, FP::undefined()),
                            Tw);
    }
    if (Pw.read().exists()) {
        result.emplace_back(SequenceIdentity({{}, "raw", "Pw"}, zeroEffectiveTime, FP::undefined()),
                            Pw);
    }

    for (int color = 0; color < 3; ++color) {
        result.emplace_back(
                SequenceIdentity({{}, "raw", "Bsw" + colorLookup[color]}, zeroEffectiveTime,
                                 FP::undefined()), Bsw[color]);
        result.emplace_back(
                SequenceIdentity({{}, "raw", "Bbsw" + colorLookup[color]}, zeroEffectiveTime,
                                 FP::undefined()), Bbsw[color]);
    }

    if (zeroPersistentUpdated && persistentEgress) {
        zeroPersistentUpdated = false;
        persistentEgress->incomingData(result);
    }

    if (!zeroRealtimeUpdated || !realtimeEgress)
        return;
    zeroRealtimeUpdated = false;

    for (int color = 0; color < 3; ++color) {
        result.emplace_back(SequenceIdentity({{}, "raw", "Bswd" + colorLookup[color]}, currentTime,
                                             FP::undefined()), Bswd[color]);
        result.emplace_back(SequenceIdentity({{}, "raw", "Bbswd" + colorLookup[color]}, currentTime,
                                             FP::undefined()), Bbswd[color]);
    }

    realtimeEgress->incomingData(std::move(result));
}

void AcquireTSINeph3563::outputUpdated(double frameTime)
{
    if (loggingEgress == NULL) {
        loggingMux.clear(true);
        haveEmittedLogMeta = false;
    } else {
        if (!haveEmittedLogMeta) {
            haveEmittedLogMeta = true;
            loggingMux.incoming(LogStream_Metadata, buildLogMeta(frameTime), loggingEgress);
        }
        loggingMux.advance(LogStream_Metadata, frameTime, loggingEgress);
    }

    if (realtimeEgress == NULL) {
        haveEmittedRealtimeMeta = false;
    } else {
        if (!haveEmittedRealtimeMeta) {
            haveEmittedRealtimeMeta = true;
            SequenceValue::Transfer meta = buildLogMeta(frameTime);
            Util::append(buildRealtimeMeta(frameTime), meta);
            realtimeEgress->incomingData(std::move(meta));
        }

        if (realtimeStateUpdated &&
                (responseState == RESP_PASSIVE_RUN || responseState == RESP_UNPOLLED_RUN)) {
            realtimeStateUpdated = false;

            Variant::Root state;
            switch (sampleState) {
            case SAMPLE_RUN:
                if (reportedModeFlags.count("Blank"))
                    state.write().setString("Blank");
                else
                    state.write().setString("Run");
                break;
            case SAMPLE_ZERO_RUN:
                state.write().setString("Zero");
                break;
            case SAMPLE_SPANCHECK:
                state.write().setString("Spancheck");
                break;
            }

            realtimeEgress->emplaceData(
                    SequenceIdentity({{}, "raw", "ZSTATE"}, frameTime, FP::undefined()),
                    std::move(state));

            realtimeEgress->incomingData(lastMode);
        }

        if (realtimeParametersUpdated) {
            realtimeParametersUpdated = false;

            Variant::Root parameters;

            for (const auto &p : activeParameters.parameters) {
                parameters.write().hash(p.first).set(p.second->toValue());
            }

            realtimeEgress->emplaceData(
                    SequenceIdentity({{}, "raw", "ZPARAMETERS"}, frameTime, FP::undefined()),
                    std::move(parameters));
        }
    }

    if (persistentEgress != NULL) {
        if (persistentParametersUpdated) {
            persistentParametersUpdated = false;

            Variant::Root parameters;

            for (const auto &p : activeParameters.parameters) {
                parameters.write().hash(p.first).set(p.second->toValue());
            }

            persistentEgress->emplaceData(
                    SequenceIdentity({{}, "raw", "ZPARAMETERS"}, frameTime, FP::undefined()),
                    std::move(parameters));
        }
    }

    handleZeroUpdate(frameTime);
}

double AcquireTSINeph3563::streamAdvance(double frameTime, LogStreamID streamID)
{
    double start = streamTime[streamID];
    if (!FP::defined(start)) {
        for (int i = 0; i < LogStream_TOTAL; i++) {
            if (!FP::defined(streamTime[streamID]))
                continue;
            if (FP::defined(start) && start < streamTime[streamID])
                continue;
            start = streamTime[streamID];
        }
    }
    streamAge[streamID] = 0;
    streamTime[streamID] = frameTime;
    if (!FP::defined(start))
        return start;

    loggingMux.advance(streamID, start, loggingEgress);

    int bits = (1 << streamID);
    for (int i = LogStream_BEGINORDER; i < LogStream_TOTAL; i++) {
        if (i == streamID)
            continue;
        if (streamAge[i] & bits) {
            streamAge[i] = 0xFFFFFFFF;

            if (!FP::defined(streamTime[i]) || start > streamTime[i]) {
                streamTime[i] = start;
                loggingMux.advance(i, start, loggingEgress);
            }
            continue;
        }
        streamAge[i] |= bits;
    }


    bool outputState = false;

    auto effectiveFlags = reportedFlags;
    Util::merge(reportedModeFlags, effectiveFlags);
    switch (sampleState) {
    case SAMPLE_RUN:
        break;
    case SAMPLE_ZERO_RUN:
        effectiveFlags.insert("Zero");
        break;
    case SAMPLE_SPANCHECK:
        effectiveFlags.insert("Spancheck");
        break;
    }
    if (lastOutputFlags != effectiveFlags) {
        outputState = true;
    }
    if (streamAge[LogStream_State] & bits) {
        outputState = true;
    }

    if (outputState) {
        double startTime = streamTime[LogStream_State];
        double endTime = frameTime;
        if (FP::defined(startTime) && !FP::equal(startTime, endTime)) {
            loggingMux.incoming(LogStream_State,
                                SequenceValue({{}, "raw", "F1"}, Variant::Root(lastOutputFlags),
                                              startTime, endTime), loggingEgress);
        } else {
            loggingMux.advance(LogStream_State, endTime, loggingEgress);
        }

        lastOutputFlags = effectiveFlags;
        streamAge[LogStream_State] = 0;
        streamTime[LogStream_State] = endTime;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "F1"}, Variant::Root(effectiveFlags), frameTime,
                                  FP::undefined()));

            Variant::Flags instrumentOnlyFlags;
            for (int i = 0;
                    i <
                            (int) (sizeof(instrumentFlagTranslation) /
                                    sizeof(instrumentFlagTranslation[0]));
                    i++) {
                if (instrumentFlagTranslation[i] == NULL)
                    continue;
                Variant::Flag check = instrumentFlagTranslation[i];
                if (effectiveFlags.count(check))
                    instrumentOnlyFlags.insert(std::move(check));
            }

            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZINSTFLAGS"}, Variant::Root(instrumentOnlyFlags),
                                  frameTime, FP::undefined()));
        }
    } else {
        streamAge[LogStream_State] |= bits;
    }

    return start;
}

bool AcquireTSINeph3563::streamValid(LogStreamID streamID, LogStreamID excludeLaterID)
{
    bool wasValid = (streamAge[streamID] != 0xFFFFFFFF);
    if (excludeLaterID >= 0) {
        for (int i = excludeLaterID + 1; i < LogStream_TOTAL; i++) {
            streamAge[streamID] &= ~(1 << i);
        }
    }
    return wasValid;
}

static double stpFactor(double t, double p)
{
    if (!FP::defined(t) || !FP::defined(p))
        return FP::undefined();
    if (p < 10.0 || p > 2000.0)
        return FP::undefined();
    if (t < -100.0)
        return FP::undefined();
    else if (t < 150.0)
        t += 273.15;
    else if (t > 400.0)
        return FP::undefined();
    return (p / 1013.25) * (273.15 / t);
}

static double convertCounts(double input, double revs, double width, qint64 K1)
{
    if (!FP::defined(input) || !FP::defined(revs) || !FP::defined(K1))
        return FP::undefined();
    if (revs <= 0.0)
        return FP::undefined();

    double Cs = (360.0 * input * 22.994) / (width * revs);
    Cs *= (Cs * (double) K1 * 1E-12 + 1.0);
    return Cs;
}

static double calculateBs(double signal,
                          double dark,
                          double calibrate,
                          double calibrateDark,
                          double K2)
{
    if (!FP::defined(signal) ||
            !FP::defined(dark) ||
            !FP::defined(calibrate) ||
            !FP::defined(calibrateDark) ||
            !FP::defined(K2))
        return FP::undefined();
    double div = calibrate - calibrateDark;
    if (div == 0.0)
        return FP::undefined();
    return (K2 * (signal - dark) / div) * 1E6;
}

static double adjustBs(double Bs, double Bsr, double Bsw, double stp)
{
    if (!FP::defined(Bs) || !FP::defined(Bsr) || !FP::defined(Bsw) || !FP::defined(stp))
        return FP::undefined();
    if (stp == 0.0)
        return FP::undefined();

    Bsr /= stp;
    Bs -= (Bsw + Bsr);
    return Bs;
}

void AcquireTSINeph3563::calculateZeroChannel(double frameTime,
                                              double bs,
                                              double bsr,
                                              double bsrScale,
                                              double cbsr,
                                              double t,
                                              double p,
                                              Variant::Root &output,
                                              Variant::Root &difference)
{
    if (!FP::defined(bs)) {
        output.write().setDouble(FP::undefined());
        difference.write().setDouble(FP::undefined());
        return;
    }

    bool knownConditions = true;
    if (!FP::defined(t)) {
        t = 0.0;
        knownConditions = false;
    }
    if (!FP::defined(p)) {
        p = 1013.25;
        knownConditions = false;
    }

    if (FP::defined(cbsr)) {
        double f = stpFactor(t, p);
        if (FP::defined(f))
            cbsr *= f;
        else
            cbsr = FP::undefined();
    }

    if (!FP::defined(bsr)) {
        bsr = cbsr;

        if (!knownConditions && FP::defined(bsr)) {
            qCWarning(log)
                << "Unknown temperature and pressure for zero.  Rayleigh adjustment will be off";
        }
    } else {
        bsr *= bsrScale;

        if (knownConditions && FP::defined(cbsr)) {
            double div = qMax(fabs(bsr), fabs(cbsr));
            if (div < 0.01)
                div = 0.01;
            div = (bsr - cbsr) / div;
            if (fabs(div) > 0.03) {
                Variant::Write info = Variant::Write::empty();
                info.hash("Bsw").setDouble(bsr);
                info.hash("Bswc").setDouble(cbsr);
                info.hash("T").setDouble(t);
                info.hash("P").setDouble(p);
                info.hash("Difference").setDouble(div);
                event(frameTime,
                      QObject::tr("Discrepancy of %1% detected in Rayleigh scattering calculation.")
                              .arg(QString::number(div * 100.0, 'f', 1)), false, info);

                qCDebug(log) << "Rayleigh scattering calculation discrepancy (expected" << cbsr
                             << " got " << bsr << "at" << t << "," << p << ")";
            }
        }
    }

    if (!FP::defined(bsr)) {
        output.write().setDouble(FP::undefined());
        difference.write().setDouble(FP::undefined());
        return;
    }

    bs -= bsr;

    double vOld = output.read().toDouble();
    if (FP::defined(vOld)) {
        difference.write().setDouble(bs - vOld);
    } else {
        difference.write().setDouble(FP::undefined());
    }

    output.write().setDouble(bs);
}

bool AcquireTSINeph3563::zeroAccumulationActive() const
{
    return reportedModeFlags.count("Zero");
}

bool AcquireTSINeph3563::logScatteringsInvalid() const
{
    if (sampleState != SAMPLE_RUN)
        return true;
    if (reportedModeFlags.count("Blank"))
        return true;
    if (reportedModeFlags.count("Zero"))
        return true;
    return false;
}

void AcquireTSINeph3563::startOkCommand(const Util::ByteView &frame,
                                        double frameTime,
                                        const Util::ByteView &nextCommand,
                                        ResponseState nextState,
                                        const QString &nextStateName)
{
    if (frame != "OK") {
        qCDebug(log) << "Invalid response" << frame << "in start communications state"
                     << responseState;

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                  frameTime, FP::undefined()));
        }

        invalidateLogValues(frameTime);
        return;
    }

    if (controlStream != NULL) {
        controlStream->writeControl(nextCommand);
    }

    responseState = nextState;
    timeoutAt(frameTime + 2.0);
    discardData(FP::undefined());

    if (realtimeEgress != NULL) {
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(nextStateName), frameTime,
                              FP::undefined()));
    }
}

void AcquireTSINeph3563::startReadParameters(const Util::ByteArray &frame, double frameTime)
{
    std::vector<std::string> parameterNames;
    for (const auto &add : activeParameters.parameters) {
        parameterNames.emplace_back(add.first);
    }
    std::sort(parameterNames.begin(), parameterNames.end());

    if (parameterIndex < 0 || parameterIndex >= static_cast<int>(parameterNames.size())) {
        parameterIndex = 0;
    } else {
        if (frame == "ERROR" ||
                !activeParameters.parameters[parameterNames[parameterIndex]]->fromResponse(frame)) {
            qCDebug(log) << "Invalid response" << frame << "to read for parameter"
                         << parameterNames.at(parameterIndex);

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            invalidateLogValues(frameTime);
            return;
        }

        realtimeParametersUpdated = true;
        persistentParametersUpdated = true;

        parameterIndex = parameterIndex + 1;
        if (parameterIndex >= static_cast<int>(parameterNames.size())) {
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveWriteParameters"), frameTime, FP::undefined()));
            }

            if (!lastSeenLabel.empty()) {
                auto pcheck = activeParameters.parameters.find("SL");
                if (pcheck != activeParameters.parameters.end()) {
                    const auto &compare =
                            static_cast<const StringParameter *>(pcheck->second.get())->value;
                    if (lastSeenLabel != compare) {
                        qCDebug(log)
                            << "Parameters reset to configured defaults due to calibration label change from"
                            << lastSeenLabel << "to" << compare;
                        lastSeenLabel = compare;
                        targetParameters = config.first().parameters;
                    }
                }
            }

            parameterIndex = -1;
            startWriteParameters(frame, frameTime);
            return;
        }
    }

    if (controlStream != NULL) {
        Util::ByteArray readCommand
                (activeParameters.parameters[parameterNames[parameterIndex]]->getCommand());
        readCommand += "\r";
        controlStream->writeControl(readCommand);
    }

    timeoutAt(frameTime + 2.0);
}

int AcquireTSINeph3563::parameterUpdateHandler(const Util::ByteArray &frame, double frameTime)
{
    std::vector<std::string> parameterNames;
    for (const auto &add : activeParameters.parameters) {
        parameterNames.emplace_back(add.first);
    }
    std::sort(parameterNames.begin(), parameterNames.end());

    int max = parameterNames.size();
    if (parameterIndex < 0 || parameterIndex >= max) {
        parameterIndex = 0;
        commandTry = 0;
    } else {
        const auto &pname = parameterNames[parameterIndex];
        GenericParameter *pactive = activeParameters.parameters[pname].get();
        const GenericParameter *ptarget = targetParameters.parameters[pname].get();

        if (pname == "SL") {
            const auto &newValue = static_cast<const StringParameter *>(pactive)->value;
            if (instrumentMeta["CalibrationLabel"].toString() != newValue) {
                instrumentMeta["CalibrationLabel"].setString(newValue);
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }
            lastSeenLabel = newValue;
        }

        if (commandTry % 2 == 0) {
            /* A set attempt */
            if (frame != "OK") {
                qCDebug(log) << "Invalid response" << frame << "to write for parameter" << pname;
                /* A bad response to the set always fails */
                return -1;
            }

            /* Set succeeded, so a read back */
            if (pactive->readAfterSet(ptarget)) {
                ++commandTry;
                if (controlStream != NULL) {
                    Util::ByteArray readCommand(pactive->getCommand());
                    readCommand += "\r";
                    controlStream->writeControl(std::move(readCommand));
                }
                timeoutAt(frameTime + 2.0);
                return 1;
            }
        } else {
            if (frame == "ERROR" || frame == "FAULT") {
                qCDebug(log) << "Error reading back parameter" << pname;
                /* Always fail if the neph returns a hard error */
                return -2;
            }

            /* Read back, so check that it's equal to the target */
            bool parseOk = pactive->fromResponse(frame);
            bool valueOk = parseOk && pactive->equals(ptarget);
            if (!parseOk || !valueOk) {
                /* Read back value doesn't match, so try again */
                ++commandTry;
                if (commandTry >= 10) {
                    if (!parseOk) {
                        qCDebug(log) << "Failed parse value for parameter" << pname
                                     << "; last value was:" << frame;
                        return -3;
                    } else {
                        qCDebug(log) << "Failed read back correct value for parameter" << pname
                                     << "; last value was:" << frame;
                        return -4;
                    }
                }

                if (controlStream != NULL) {
                    Util::ByteArray setCommand = ptarget->setCommand(pactive);
                    setCommand += "\r";
                    controlStream->writeControl(std::move(setCommand));
                }
                timeoutAt(frameTime + 2.0);
                return 1;
            }

            /* Values match, so move on */
        }
        parameterIndex++;
        commandTry = 0;
    }

    for (; parameterIndex < max; ++parameterIndex) {
        const auto &pname = parameterNames[parameterIndex];
        const GenericParameter *pactive = activeParameters.parameters[pname].get();
        const GenericParameter *ptarget = targetParameters.parameters[pname].get();
        if (pname == "SL") {
            const auto &newValue = static_cast<const StringParameter *>(pactive)->value;
            if (instrumentMeta["CalibrationLabel"].toString() != newValue) {
                instrumentMeta["CalibrationLabel"].setString(newValue);
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }
            lastSeenLabel = newValue;
        }

        if (pactive->equals(ptarget))
            continue;

        Util::ByteArray setCommand = ptarget->setCommand(pactive);

        if (setCommand.empty())
            continue;
        if (controlStream) {
            qCDebug(log) << "Setting parameter" << pname << "with" << setCommand;

            setCommand += "\r";
            controlStream->writeControl(std::move(setCommand));
        }
        realtimeParametersUpdated = true;
        persistentParametersUpdated = true;

        if (pname == "SL") {
            const auto &newValue = static_cast<const StringParameter *>(pactive)->value;
            if (instrumentMeta["CalibrationLabel"].toString() != newValue) {
                instrumentMeta["CalibrationLabel"].setString(newValue);
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }
            lastSeenLabel = newValue;
        }

        timeoutAt(frameTime + 2.0);
        commandTry = 0;
        return 1;
    }

    return 0;
}

void AcquireTSINeph3563::startWriteParameters(const Util::ByteArray &frame, double frameTime)
{
    int code = parameterUpdateHandler(frame, frameTime);
    if (code < 0) {
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                  frameTime, FP::undefined()));
        }

        invalidateLogValues(frameTime);
        return;
    } else if (code != 0) {
        responseState = RESP_INTERACTIVE_START_PARAMETERS_WRITE;
        return;
    } else {
        if (controlStream != NULL) {
            controlStream->writeControl("RY\r");
        }

        responseState = RESP_INTERACTIVE_START_RECORD_Y;
        timeoutAt(frameTime + 2.0);
        discardData(FP::undefined());

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveReadRecordY"),
                                                       frameTime, FP::undefined()));
        }
    }
}

void AcquireTSINeph3563::updateWriteParameters(const Util::ByteArray &frame, double frameTime)
{
    int code = parameterUpdateHandler(frame, frameTime);
    if (code < 0) {
        qCDebug(log) << "Error updating parameters at" << Logging::time(frameTime) << "code"
                     << (-code) << "response:" << frame;
        timeoutAt(frameTime + 30.0);

        if (controlStream != NULL) {
            controlStream->writeControl("\r\r\r\r\r\r\r\r\r\r\rUE\rUE\rUE\rUE\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        commandTry = 0;
        discardData(frameTime + 2.0);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                  frameTime, FP::undefined()));
        }
        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("UpdateWriteParameters");
            info.hash("Line").setString(frame.toString());
            info.hash("Code").setInt64(-code);
            std::vector<std::string> parameterNames;
            for (const auto &add : activeParameters.parameters) {
                parameterNames.emplace_back(add.first);
            }
            std::sort(parameterNames.begin(), parameterNames.end());
            if (parameterIndex >= 0 && parameterIndex < static_cast<int>(parameterNames.size())) {
                info.hash("Parameter").setString(parameterNames[parameterIndex]);
            }
            event(frameTime,
                  QObject::tr("Error updating parameters (code %1).  Communications dropped.").arg(
                          -code), true, info);
        }

        invalidateLogValues(frameTime);
        return;
    } else if (code != 0) {
        return;
    } else {
        responseState = RESP_INTERACTIVE_UPDATE_COMMANDS;
        timeoutAt(frameTime + 2.0);
        discardData(frameTime + 0.25);
    }
}

void AcquireTSINeph3563::updateSendNextCommand(double frameTime)
{
    if (okCommandQueue.empty()) {
        Q_ASSERT(!config.isEmpty());
        double unpolledResponseTime = config.first().reportInterval;
        if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
            unpolledResponseTime = 1.0;

        if (controlStream != NULL) {
            controlStream->writeControl("UB\r");
        }
        memset(streamAge, 0, sizeof(streamAge));
        responseState = RESP_UNPOLLED_RUN;
        timeoutAt(frameTime + unpolledResponseTime * 2.0 + 5.0);
        discardData(frameTime + unpolledResponseTime + 0.5, 1);
        return;
    }

    if (controlStream != NULL) {
        Util::ByteArray send(okCommandQueue.at(0));
        send += "\r";
        controlStream->writeControl(std::move(send));
    }

    timeoutAt(frameTime + 2.0);
}

void AcquireTSINeph3563::updateCommandResponse(const Util::ByteArray &frame, double frameTime)
{
    if (frame != "OK") {
        Util::ByteArray command;
        if (!okCommandQueue.empty())
            command = okCommandQueue.at(0);
        qCDebug(log) << "Invalid response" << frame << "to command" << command.toString() << "at"
                     << Logging::time(frameTime);
        timeoutAt(frameTime + 30.0);

        if (controlStream != NULL) {
            controlStream->writeControl("\r\r\r\r\r\r\r\r\r\r\rUE\rUE\rUE\rUE\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        commandTry = 0;
        discardData(frameTime + 2.0);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                  frameTime, FP::undefined()));
        }
        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("UpdateCommands");
            info.hash("Line").setString(frame.toString());
            info.hash("Command").setString(command.toString());
            event(frameTime, QObject::tr("Error sending commands.  Communications dropped."), true,
                  info);
        }

        invalidateLogValues(frameTime);
        return;
    }
    okCommandQueue.pop_front();

    /* Delay slightly between commands */
    timeoutAt(frameTime + 2.0);
    discardData(frameTime + 0.25);
}

void AcquireTSINeph3563::configAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

void AcquireTSINeph3563::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    for (int color = 0; color < 3; color++) {
        double wl = config.first().wavelengths[color];
        spancheckController->setWavelength(color, wl, QString::fromStdString(colorLookup[color]));

        rayleighTotal[color] = Rayleigh::scattering(wl);
        rayleighBack[color] = Rayleigh::angleScattering(wl, 90.0);
    }
    targetParameters = config.first().parameters;
}

int AcquireTSINeph3563::processRecord(const Util::ByteArray &line, double &frameTime)
{
    Q_ASSERT(!config.isEmpty());

    if (!config.first().strictMode) {
        if (line == "OK" || line == "ERROR")
            return -1;
    }

    if (line.size() < 3)
        return 1;

    auto fields = CSV::acquisitionSplit(line);
    if (fields.size() < 2)
        return 2;
    char code = line[0];
    fields.pop_front();

    Util::ByteView field;
    bool ok = false;

    if (code == 'T') {
        if (fields.empty()) return 1000;
        field = fields.front().string_trimmed();
        fields.pop_front();
        qint64 iyear = field.parse_i32(&ok);
        if (!ok) return 1001;
        if (iyear < 1900) return 1002;
        if (iyear > 2999) return 1003;
        Variant::Root year(iyear);
        remap("YEAR", year);
        iyear = year.read().toInt64();

        if (fields.empty()) return 1004;
        field = fields.front().string_trimmed();
        fields.pop_front();
        qint64 imonth = field.parse_i32(&ok);
        if (!ok) return 1005;
        if (imonth < 1) return 1006;
        if (imonth > 12) return 1007;
        Variant::Root month(imonth);
        remap("MONTH", month);
        imonth = month.read().toInt64();

        if (fields.empty()) return 1008;
        field = fields.front().string_trimmed();
        fields.pop_front();
        qint64 iday = field.parse_i32(&ok);
        if (!ok) return 1009;
        if (iday < 1) return 1010;
        if (iday > 31) return 1011;
        Variant::Root day(iday);
        remap("DAY", day);
        iday = day.read().toInt64();

        if (fields.empty()) return 1012;
        field = fields.front().string_trimmed();
        fields.pop_front();
        qint64 ihour = field.parse_i32(&ok);
        if (!ok) return 1013;
        if (ihour < 0) return 1014;
        if (ihour > 23) return 1015;
        Variant::Root hour(ihour);
        remap("HOUR", hour);
        ihour = hour.read().toInt64();

        if (fields.empty()) return 1016;
        field = fields.front().string_trimmed();
        fields.pop_front();
        qint64 iminute = field.parse_i32(&ok);
        if (!ok) return 1017;
        if (iminute < 0) return 1018;
        if (iminute > 59) return 1019;
        Variant::Root minute(iminute);
        remap("MINUTE", minute);
        iminute = minute.read().toInt64();

        if (fields.empty()) return 1020;
        field = fields.front().string_trimmed();
        fields.pop_front();
        qint64 isecond = field.parse_i32(&ok);
        if (!ok) return 1021;
        if (isecond < 0) return 1022;
        if (isecond > 60) return 1023;
        Variant::Root second(isecond);
        remap("SECOND", second);
        isecond = second.read().toInt64();

        if (config.first().strictMode) {
            if (!fields.empty())
                return 1024;
        }

        if (!FP::defined(frameTime) &&
                INTEGER::defined(iyear) &&
                iyear >= 1900 &&
                iyear <= 2999 &&
                INTEGER::defined(imonth) &&
                imonth >= 1 &&
                imonth <= 12 &&
                INTEGER::defined(iday) &&
                iday >= 1 &&
                iday <= 31 &&
                INTEGER::defined(ihour) &&
                ihour >= 0 &&
                ihour <= 23 &&
                INTEGER::defined(iminute) &&
                iminute >= 0 &&
                iminute <= 59 &&
                INTEGER::defined(isecond) &&
                isecond >= 0 &&
                isecond <= 60) {
            frameTime = Time::fromDateTime(QDateTime(QDate((int) iyear, (int) imonth, (int) iday),
                                                     QTime((int) ihour, (int) iminute,
                                                           (int) isecond), Qt::UTC));
        }
        if (FP::defined(lastRecordTime) && FP::defined(frameTime) && frameTime < lastRecordTime)
            return -1;
        lastRecordTime = frameTime;

        if (FP::defined(frameTime)) {
            configAdvance(frameTime);
            outputUpdated(frameTime);
        }
    } else if (code == 'D') {
        if (fields.empty()) return 2001;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root F2(field.toString());
        remap("F2", F2);

        if (fields.empty()) return 2002;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root RTIME((qint64) field.parse_u64(&ok));
        if (!ok) return 2003;
        if (!INTEGER::defined(RTIME.read().toInteger())) return 2004;
        remap("RTIME", RTIME);

        Variant::Root Bs[3];
        for (int color = 0; color < 3; color++) {
            if (fields.empty()) return 2000 + (color + 1) * 10;
            field = fields.front().string_trimmed();
            fields.pop_front();
            double coefficient = field.parse_real(&ok);
            if (!ok) return 2001 + (color + 1) * 10;
            if (!FP::defined(coefficient)) return 2002 + (color + 1) * 10;
            coefficient *= 1E6;
            Bs[color].write().setReal(coefficient);
            remap("Bs" + colorLookup[color], Bs[color]);
        }

        Variant::Root Bbs[3];
        for (int color = 0; color < 3; color++) {
            if (fields.empty()) return 2000 + (color + 4) * 10;
            field = fields.front().string_trimmed();
            fields.pop_front();
            double coefficient = field.parse_real(&ok);
            if (!ok) return 2001 + (color + 4) * 10;
            if (!FP::defined(coefficient)) return 2002 + (color + 4) * 10;
            coefficient *= 1E6;
            Bbs[color].write().setReal(coefficient);
        }

        if (config.first().strictMode) {
            if (!fields.empty())
                return 2100;
        }

        const auto &modeString = F2.read().toString();
        reportedModeFlags.clear();
        if (modeString.size() > 0) {
            switch (modeString[0]) {
            case 'N':
                if (sampleState == SAMPLE_ZERO_RUN &&
                        (!FP::defined(modeUpdateTime) || modeUpdateTime < frameTime)) {
                    sampleState = SAMPLE_RUN;
                    modeUpdateTime = frameTime + 0.1;
                    realtimeStateUpdated = true;
                }
                break;
            case 'B':
                reportedModeFlags.insert("Blank");
                if (sampleState == SAMPLE_ZERO_RUN &&
                        (!FP::defined(modeUpdateTime) || modeUpdateTime < frameTime)) {
                    sampleState = SAMPLE_RUN;
                    modeUpdateTime = frameTime + 0.1;
                    realtimeStateUpdated = true;
                }
                break;
            case 'Z':
                reportedModeFlags.insert("Zero");
                if (sampleState == SAMPLE_RUN &&
                        (!FP::defined(modeUpdateTime) || modeUpdateTime < frameTime)) {
                    sampleState = SAMPLE_ZERO_RUN;
                    modeUpdateTime = frameTime + 0.1;
                    realtimeStateUpdated = true;

                    /* Blank these when entering zero state, in case we
                     * missed a Z record */
                    zeroTCount = 0;
                    zeroTSum = 0;
                    zeroPCount = 0;
                    zeroPSum = 0;
                }
                break;
            default:
                break;
            }
        }
        if (modeString.size() > 1) {
            switch (modeString[1]) {
            case 'B':
                break;
            case 'T':
                reportedModeFlags.insert("BackscatterDisabled");
                for (int color = 0; color < 3; color++) {
                    Bbs[color].write().setDouble(FP::undefined());
                }
                break;
            default:
                break;
            }
        }
        /* Do the remap here in case we want to change the mode override */
        for (int color = 0; color < 3; color++) {
            remap("Bbs" + colorLookup[color], Bbs[color]);
        }

        if (!FP::defined(frameTime))
            frameTime = lastRecordTime;

        if (FP::defined(frameTime)) {
            configAdvance(frameTime);
            outputUpdated(frameTime);

            double startTime = streamAdvance(frameTime, LogStream_Data);
            double endTime = frameTime;

            realtimeValue(endTime, "ZRTIME", std::move(RTIME));

            for (int color = 0; color < 3; ++color) {
                if (realtimeEgress) {
                    realtimeValue(endTime, "ZBs" + colorLookup[color], Variant::Root(Bs[color]));
                    realtimeValue(endTime, "ZBbs" + colorLookup[color], Variant::Root(Bbs[color]));
                }

                spancheckController->updateTotal(NephelometerSpancheckController::Scattering,
                                                 Bs[color].read().toDouble(), color);
                spancheckController->updateBack(NephelometerSpancheckController::Scattering,
                                                Bbs[color].read().toDouble(), color);

                if (logScatteringsInvalid()) {
                    logValue(startTime, endTime, "Bs" + colorLookup[color], Variant::Root(),
                             LogStream_Data);
                    logValue(startTime, endTime, "Bbs" + colorLookup[color], Variant::Root(),
                             LogStream_Data);
                } else {
                    logValue(startTime, endTime, "Bs" + colorLookup[color], std::move(Bs[color]),
                             LogStream_Data);
                    logValue(startTime, endTime, "Bbs" + colorLookup[color], std::move(Bbs[color]),
                             LogStream_Data);
                }
            }

            if (F2.read() != lastMode.read()) {
                realtimeStateUpdated = true;

                if (persistentEgress && FP::defined(lastMode.getStart())) {
                    lastMode.setEnd(frameTime);
                    persistentEgress->incomingData(lastMode);
                }
                lastMode.setRoot(std::move(F2));
                lastMode.setStart(frameTime);
                lastMode.setEnd(FP::undefined());

                persistentValuesUpdated();
            }
        }
    } else if (code == 'Y') {
        if (fields.empty()) return 3000;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root CfG(field.parse_real(&ok));
        if (!ok) return 3001;
        if (!FP::defined(CfG.read().toReal())) return 3002;
        remap("CfG", CfG);

        if (fields.empty()) return 3003;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root P(field.parse_real(&ok));
        if (!ok) return 3004;
        if (!FP::defined(P.read().toReal())) return 3005;
        remap("P", P);

        if (fields.empty()) return 3006;
        field = fields.front().string_trimmed();
        fields.pop_front();
        double correctValue = field.parse_real(&ok);
        if (!ok) return 3007;
        if (!FP::defined(correctValue)) return 3008;
        if (correctValue > 150.0)
            correctValue -= 273.15;
        Variant::Root T(correctValue);
        remap("T", T);

        if (fields.empty()) return 3009;
        field = fields.front().string_trimmed();
        fields.pop_front();
        correctValue = field.parse_real(&ok);
        if (!ok) return 3010;
        if (!FP::defined(correctValue)) return 3011;
        if (correctValue > 150.0)
            correctValue -= 273.15;
        Variant::Root Tu(correctValue);
        remap("Tu", Tu);

        if (fields.empty()) return 3012;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root U(field.parse_real(&ok));
        if (!ok) return 3013;
        if (!FP::defined(U.read().toReal())) return 3014;
        remap("U", U);

        if (fields.empty()) return 3015;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root Vl(field.parse_real(&ok));
        if (!ok) return 3016;
        if (!FP::defined(Vl.read().toReal())) return 3017;
        remap("Vl", Vl);

        if (fields.empty()) return 3018;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root Al(field.parse_real(&ok));
        if (!ok) return 3019;
        if (!FP::defined(Al.read().toReal())) return 3020;
        remap("Al", Al);

        if (fields.empty()) return 3021;
        field = fields.front().string_trimmed();
        fields.pop_front();
        correctValue = field.parse_real(&ok);
        if (!ok) return 3022;
        if (!FP::defined(correctValue)) return 3023;
        correctValue /= 1000.0;
        Variant::Root Vx(correctValue);
        remap("Vx", Vx);

        if (fields.empty()) return 3024;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root flags((qint64) field.parse_u16(&ok, 16));
        if (!ok) return 3025;
        remap("FRAW", flags);

        if (config.first().strictMode) {
            if (!fields.empty())
                return 3026;
        }

        Variant::Root Uu(Dewpoint::rhExtrapolate(T.read().toDouble(), U.read().toDouble(),
                                                 Tu.read().toDouble()));
        remap("Uu", Uu);

        if (INTEGER::defined(flags.read().toInt64())) {
            reportedFlags.clear();
            qint64 flagsBits(flags.read().toInt64());
            for (int i = 0;
                    i <
                            (int) (sizeof(instrumentFlagTranslation) /
                                    sizeof(instrumentFlagTranslation[0]));
                    i++) {
                if (flagsBits & (Q_INT64_C(1) << i)) {
                    if (instrumentFlagTranslation[i]) {
                        reportedFlags.insert(instrumentFlagTranslation[i]);
                    } else {
                        qCDebug(log) << "Unrecognized bit" << hex << ((1 << i))
                                     << "set in instrument flags";
                    }
                }
            }
        }

        if (!FP::defined(frameTime))
            frameTime = lastRecordTime;

        if (zeroAccumulationActive()) {
            double v = T.read().toDouble();
            if (FP::defined(v)) {
                zeroTCount++;
                zeroTSum += v;
            }
            v = P.read().toDouble();
            if (FP::defined(v)) {
                zeroPCount++;
                zeroPSum += v;
            }
        }

        if (sampleState != SAMPLE_SPANCHECK &&
                responseState == RESP_UNPOLLED_RUN &&
                (!FP::defined(lampLimitTime) ||
                        !FP::defined(frameTime) ||
                        lampLimitTime < frameTime) &&
                FP::defined(Al.read().toDouble()) &&
                Al.read().toDouble() > config.first().maximumLampCurrent) {
            double pwr = Vl.read().toDouble();
            if (FP::defined(pwr)) {
                pwr *= Al.read().toDouble();

                IntegerParameter *tsp = static_cast<IntegerParameter *>(
                        targetParameters.parameters["SP"].get());
                if (INTEGER::defined(tsp->value) && (qint64) pwr > tsp->value)
                    pwr = (double) tsp->value;

                int sp = static_cast<int>(std::floor(pwr * 0.9));
                if (sp < 0 || sp >= 150)
                    sp = 0;
                tsp->value = sp;

                {
                    Variant::Write info = Variant::Write::empty();
                    info.hash("ResponseState").setString("UnpolledRun");
                    info.hash("Power").setDouble(pwr);
                    info.hash("NewPower").setInt64(sp);
                    info.hash("Current").set(Al);
                    info.hash("Voltage").set(Vl);
                    event(frameTime, QObject::tr(
                            "Lamp current too high.  Reducing power to %1 watts.  Replace lamp immediately.")
                            .arg(sp), true, info);
                }

                qCDebug(log) << "Reducing lamp power to" << sp << "watts";

                if (FP::defined(frameTime))
                    lampLimitTime = frameTime + 60.0;
            } else {
                {
                    Variant::Write info = Variant::Write::empty();
                    info.hash("ResponseState").setString("UnpolledRun");
                    info.hash("Current").set(Al);
                    info.hash("Voltage").set(Vl);
                    event(frameTime, QObject::tr(
                            "Lamp current too high.  Unable to calculate power to reduce to.  Replace lamp immediately."),
                          true, info);
                }

                if (FP::defined(frameTime))
                    lampLimitTime = frameTime + 60.0;
            }
        }

        if (FP::defined(frameTime)) {
            configAdvance(frameTime);
            outputUpdated(frameTime);

            double startTime = streamAdvance(frameTime, LogStream_Auxiliary);
            double endTime = frameTime;

            spancheckController->update(NephelometerSpancheckController::Temperature,
                                        T.read().toDouble());
            spancheckController->update(NephelometerSpancheckController::Pressure,
                                        P.read().toDouble());

            logValue(startTime, endTime, "P", std::move(P), LogStream_Auxiliary);
            logValue(startTime, endTime, "T", std::move(T), LogStream_Auxiliary);
            logValue(startTime, endTime, "Tu", std::move(Tu), LogStream_Auxiliary);
            logValue(startTime, endTime, "U", std::move(U), LogStream_Auxiliary);
            logValue(startTime, endTime, "Uu", std::move(Uu), LogStream_Auxiliary);
            logValue(startTime, endTime, "Vl", std::move(Vl), LogStream_Auxiliary);
            logValue(startTime, endTime, "Al", std::move(Al), LogStream_Auxiliary);
            realtimeValue(frameTime, "Vx", std::move(Vx));

            if (!streamValid(LogStream_PhotonGreen)) {
                /* Can't update the spancheck controller with this, since
                 * it wants the raw photon counts, not the count rate */
                /*spancheckController->update(
                    NephelometerSpancheckController::ReferenceCounts,
                    Cfg[color].toDouble(), 1);*/

                logValue(startTime, endTime, "CfG", std::move(CfG), LogStream_Auxiliary);
            }
        }
    } else if (code == 'Z') {
        for (int color = 0; color < 3; color++) {
            Bsr[color].write().setReal(FP::undefined());
        }

        Variant::Root Bs[3];
        for (int color = 0; color < 3; color++) {
            if (fields.empty()) return 4000 + color * 10;
            field = fields.front().string_trimmed();
            fields.pop_front();
            double coefficient = field.parse_real(&ok);
            if (!ok) return 4001 + color * 10;
            if (!FP::defined(coefficient)) return 4002 + color * 10;
            coefficient *= 1E6;
            Bs[color].write().setReal(coefficient);
            remap("Bswr" + colorLookup[color], Bsw[color]);
        }

        Variant::Root Bbs[3];
        for (int color = 0; color < 3; color++) {
            if (fields.empty()) return 4000 + (color + 3) * 10;
            field = fields.front().string_trimmed();
            fields.pop_front();
            double coefficient = field.parse_real(&ok);
            if (!ok) return 4001 + (color + 3) * 10;
            if (!FP::defined(coefficient)) return 4002 + (color + 3) * 10;
            coefficient *= 1E6;
            Bbs[color].write().setReal(coefficient);
            remap("Bbswr" + colorLookup[color], Bbs[color]);
        }

        for (int color = 0; color < 3; color++) {
            if (fields.empty()) return 4000 + (color + 6) * 10;
            field = fields.front().string_trimmed();
            fields.pop_front();
            double coefficient = field.parse_real(&ok);
            if (!ok) return 4001 + (color + 6) * 10;
            if (!FP::defined(coefficient)) return 4002 + (color + 6) * 10;
            coefficient *= 1E6;
            Bsr[color].write().setReal(coefficient);
            remap("Bsr" + colorLookup[color], Bsr[color]);
        }

        if (config.first().strictMode) {
            if (!fields.empty())
                return 4100;
        }

        if (!FP::defined(frameTime))
            frameTime = lastRecordTime;


        if (zeroTCount != 0)
            Tw.write().setDouble(zeroTSum / (double) zeroTCount);
        else
            Tw.write().setEmpty();
        if (zeroPCount != 0)
            Pw.write().setDouble(zeroPSum / (double) zeroPCount);
        else
            Pw.write().setEmpty();

        remap("Tw", Tw);
        remap("Pw", Pw);

        for (int color = 0; color < 3; ++color) {
            calculateZeroChannel(frameTime, Bs[color].read().toDouble(),
                                 Bsr[color].read().toDouble(), 1.0, rayleighTotal[color],
                                 Tw.read().toDouble(), Pw.read().toDouble(), Bsw[color],
                                 Bswd[color]);
            remap("Bsw" + colorLookup[color], Bsw[color]);
            remap("Bswd" + colorLookup[color], Bswd[color]);

            calculateZeroChannel(frameTime, Bbs[color].read().toDouble(),
                                 Bsr[color].read().toDouble(), 0.5, rayleighBack[color],
                                 Tw.read().toDouble(), Pw.read().toDouble(), Bbsw[color],
                                 Bbswd[color]);
            remap("Bbsw" + colorLookup[color], Bbsw[color]);
            remap("Bbswd" + colorLookup[color], Bbswd[color]);
        }

        zeroPersistentUpdated = true;
        zeroRealtimeUpdated = true;
        zeroEffectiveTime = frameTime;

        /* Blank these here so we don't re-use them */
        zeroTCount = 0;
        zeroTSum = 0;
        zeroPCount = 0;
        zeroPSum = 0;

        if (FP::defined(frameTime)) {
            configAdvance(frameTime);
            outputUpdated(frameTime);
        }
    } else if (code == 'B' || code == 'G' || code == 'R') {
        int color = 0;
        LogStreamID streamID = LogStream_TOTAL;
        switch (code) {
        case 'B':
            color = 0;
            streamID = LogStream_PhotonBlue;
            break;
        case 'G':
            color = 1;
            streamID = LogStream_PhotonGreen;
            break;
        case 'R':
            color = 2;
            streamID = LogStream_PhotonRed;
            break;
        default:
            Q_ASSERT(false);
            return 99999;
        }

        if (fields.empty()) return 5000 + color * 1000;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root Cfx(field.parse_real(&ok));
        if (!ok) return 5001 + color * 1000;
        if (!FP::defined(Cfx.read().toReal())) return 5002 + color * 1000;
        remap("Cfx" + colorLookup[color], Cfx);

        if (fields.empty()) return 5003 + color * 1000;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root Cx(field.parse_real(&ok));
        if (!ok) return 5004 + color * 1000;
        if (!FP::defined(Cx.read().toReal())) return 5005 + color * 1000;
        remap("Cx" + colorLookup[color], Cx);

        if (fields.empty()) return 5006 + color * 1000;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root Cdx(field.parse_real(&ok));
        if (!ok) return 5007 + color * 1000;
        if (!FP::defined(Cdx.read().toReal())) return 5008 + color * 1000;
        remap("Cdx" + colorLookup[color], Cdx);

        if (fields.empty()) return 5009 + color * 1000;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root Revs(field.parse_real(&ok));
        if (!ok) return 5010 + color * 1000;
        if (!FP::defined(Revs.read().toReal())) return 5011 + color * 1000;
        remap("Revs" + colorLookup[color], Revs);

        if (fields.empty()) return 5012 + color * 1000;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root Cbfx(field.parse_real(&ok));
        if (!ok) return 5013 + color * 1000;
        if (!FP::defined(Cbfx.read().toReal())) return 5014 + color * 1000;
        remap("Cbfx" + colorLookup[color], Cbfx);

        if (fields.empty()) return 5015 + color * 1000;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root Cbx(field.parse_real(&ok));
        if (!ok) return 5016 + color * 1000;
        if (!FP::defined(Cbx.read().toReal())) return 5017 + color * 1000;
        remap("Cbx" + colorLookup[color], Cbx);

        if (fields.empty()) return 5018 + color * 1000;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root Cbdx(field.parse_real(&ok));
        if (!ok) return 5019 + color * 1000;
        if (!FP::defined(Cbdx.read().toReal())) return 5020 + color * 1000;
        remap("Cbdx" + colorLookup[color], Cbdx);

        if (fields.empty()) return 5021 + color * 1000;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root Revsb(field.parse_real(&ok));
        if (!ok) return 5022 + color * 1000;
        if (!FP::defined(Revsb.read().toReal())) return 5023 + color * 1000;
        remap("Revsb" + colorLookup[color], Revsb);

        if (fields.empty()) return 5024 + color * 1000;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root P(field.parse_real(&ok));
        if (!ok) return 5025 + color * 1000;
        if (!FP::defined(P.read().toReal())) return 5026 + color * 1000;
        remap("P", P);

        if (fields.empty()) return 5027 + color * 1000;
        field = fields.front().string_trimmed();
        fields.pop_front();
        double correctValue = field.parse_real(&ok);
        if (!ok) return 5028 + color * 1000;
        if (!FP::defined(correctValue)) return 5029 + color * 1000;
        if (correctValue > 150.0)
            correctValue -= 273.15;
        Variant::Root T(correctValue);
        remap("T", T);

        if (config.first().strictMode) {
            if (!fields.empty())
                return 5100 + color * 1000;
        }

        ChannelCalibration *channelCal = static_cast<ChannelCalibration *>(
                activeParameters.parameters["SK" + colorLookup[color]].get());
        qint64 K1 = channelCal->K1;
        if (!INTEGER::defined(K1))
            K1 = 20000;
        Variant::Root Cs(convertCounts(Cx.read().toDouble(), Revs.read().toDouble(), 140.0, K1));
        Variant::Root Cf(convertCounts(Cfx.read().toDouble(), Revs.read().toDouble(), 40.0, K1));
        Variant::Root Cd(convertCounts(Cdx.read().toDouble(), Revs.read().toDouble(), 60.0, K1));
        Variant::Root Cbs(convertCounts(Cbx.read().toDouble(), Revsb.read().toDouble(), 140.0, K1));
        Variant::Root Cbd(convertCounts(Cbdx.read().toDouble(), Revsb.read().toDouble(), 60.0, K1));

        remap("Cs" + colorLookup[color], Cs);
        remap("Cf" + colorLookup[color], Cf);
        remap("Cd" + colorLookup[color], Cd);
        remap("Cbs" + colorLookup[color], Cbs);
        remap("Cbd" + colorLookup[color], Cbd);

        if (!FP::defined(frameTime))
            frameTime = lastRecordTime;

        if (FP::defined(frameTime)) {
            configAdvance(frameTime);
            outputUpdated(frameTime);

            double startTime = streamAdvance(frameTime, streamID);
            double endTime = frameTime;

            /* The spancheck controller expects the raw count rates,
             * so don't give it the adjusted ones */
            spancheckController->updateTotal(NephelometerSpancheckController::MeasurementCounts,
                                             Cx.read().toDouble(), color);
            spancheckController->updateTotal(NephelometerSpancheckController::DarkCounts,
                                             Cdx.read().toDouble(), color);
            spancheckController->updateTotal(NephelometerSpancheckController::Revolutions,
                                             Revs.read().toDouble(), color);
            spancheckController->updateBack(NephelometerSpancheckController::MeasurementCounts,
                                            Cbx.read().toDouble(), color);
            spancheckController->updateBack(NephelometerSpancheckController::DarkCounts,
                                            Cbdx.read().toDouble(), color);
            spancheckController->updateBack(NephelometerSpancheckController::Revolutions,
                                            Revsb.read().toDouble(), color);
            spancheckController->update(NephelometerSpancheckController::ReferenceCounts,
                                        Cfx.read().toDouble(), color);

            if (!streamValid(LogStream_Data)) {
                double stp = stpFactor(T.read().toDouble(), P.read().toDouble());

                double Bsw = this->Bsw[color].read().toDouble();
                double Bbsw = this->Bsw[color].read().toDouble();
                if (!FP::defined(Bsw)) {
                    qCWarning(log)
                        << "Cannot recover scattering from photon counts without a zero, but none is available.  Zero wall scattering assumed (data will be offset by an unknown constant)";
                    Bsw = 0.0;
                    Bbsw = 0.0;
                    for (int check = 0; check < 3; ++check) {
                        if (!FP::defined(this->Bsw[check].read().toDouble()))
                            this->Bsw[check].write().setDouble(0.0);
                        if (!FP::defined(this->Bbsw[check].read().toDouble()))
                            this->Bbsw[check].write().setDouble(0.0);
                    }
                } else if (!FP::defined(Bbsw)) {
                    qCWarning(log)
                        << "Cannot recover scattering from photon counts without a zero, but none is available.  Zero wall scattering assumed (data will be offset by an unknown constant)";
                    Bsw = 0.0;
                    Bbsw = 0.0;
                    for (int check = 0; check < 3; ++check) {
                        if (!FP::defined(this->Bsw[check].read().toDouble()))
                            this->Bsw[check].write().setDouble(0.0);
                        if (!FP::defined(this->Bbsw[check].read().toDouble()))
                            this->Bbsw[check].write().setDouble(0.0);
                    }
                }

                double Bsr = this->Bsr[color].read().toDouble();
                if (!FP::defined(Bsr)) {
                    Bsr = rayleighTotal[color];
                }

                double Bbsr = this->Bsr[color].read().toDouble();
                if (!FP::defined(Bsr)) {
                    Bbsr = rayleighBack[color];
                } else if (FP::defined(channelCal->K4)) {
                    Bbsr *= channelCal->K4;
                } else {
                    Bbsr *= 0.5;
                }

                Variant::Root Bs(adjustBs(calculateBs(Cs.read().toDouble(), Cd.read().toDouble(),
                                                      Cf.read().toDouble(), Cd.read().toDouble(),
                                                      channelCal->K2), Bsr, Bsw, stp));
                remap("Bs" + colorLookup[color], Bs);

                Variant::Root Bbs(adjustBs(calculateBs(Cbs.read().toDouble(), Cbd.read().toDouble(),
                                                       Cf.read().toDouble(), Cd.read().toDouble(),
                                                       channelCal->K2), Bbsr, Bbsw, stp));
                remap("Bbs" + colorLookup[color], Bbs);

                if (realtimeEgress) {
                    realtimeValue(endTime, "ZBs" + colorLookup[color], Variant::Root(Bs));
                    realtimeValue(endTime, "ZBbs" + colorLookup[color], Variant::Root(Bbs));
                }

                spancheckController->updateTotal(NephelometerSpancheckController::Scattering,
                                                 Bs.read().toDouble(), color);
                spancheckController->updateBack(NephelometerSpancheckController::Scattering,
                                                Bbs.read().toDouble(), color);

                if (!logScatteringsInvalid()) {
                    logValue(startTime, endTime, "Bs" + colorLookup[color], Variant::Root(),
                             streamID);
                    logValue(startTime, endTime, "Bbs" + colorLookup[color], Variant::Root(),
                             streamID);
                } else {
                    logValue(startTime, endTime, "Bs" + colorLookup[color], std::move(Bs),
                             streamID);
                    logValue(startTime, endTime, "Bbs" + colorLookup[color], std::move(Bbs),
                             streamID);
                }
            }

            if (!streamValid(LogStream_Auxiliary, streamID)) {
                spancheckController->update(NephelometerSpancheckController::Temperature,
                                            T.read().toDouble());
                spancheckController->update(NephelometerSpancheckController::Pressure,
                                            P.read().toDouble());

                if (zeroAccumulationActive()) {
                    double v = T.read().toDouble();
                    if (FP::defined(v)) {
                        zeroTCount++;
                        zeroTSum += v;
                    }
                    v = P.read().toDouble();
                    if (FP::defined(v)) {
                        zeroPCount++;
                        zeroPSum += v;
                    }
                }

                logValue(startTime, endTime, "T", std::move(T), streamID);
                logValue(startTime, endTime, "P", std::move(P), streamID);
            }

            logValue(startTime, endTime, "Cs" + colorLookup[color], std::move(Cs), streamID);
            logValue(startTime, endTime, "Cf" + colorLookup[color], std::move(Cf), streamID);
            logValue(startTime, endTime, "Cd" + colorLookup[color], std::move(Cd), streamID);
            logValue(startTime, endTime, "Cbs" + colorLookup[color], std::move(Cbs), streamID);
            logValue(startTime, endTime, "Cbd" + colorLookup[color], std::move(Cbd), streamID);
        }
    } else {
        return 100;
    }

    return 0;
}


void AcquireTSINeph3563::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (FP::defined(frameTime))
        configAdvance(frameTime);

    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        memset(streamAge, 0, sizeof(streamAge));
        okCommandQueue.clear();
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++commandTry > 5) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_PASSIVE_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                timeoutAt(frameTime + unpolledResponseTime + 1.0);

                realtimeStateUpdated = true;

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
        } else if (code > 0) {
            commandTry = 0;
        }
        break;
    }
    case RESP_PASSIVE_WAIT:
        memset(streamAge, 0, sizeof(streamAge));
        okCommandQueue.clear();
        if (processRecord(frame, frameTime) == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);

            responseState = RESP_PASSIVE_RUN;
            generalStatusUpdated();

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        }
        break;

    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID: {
        memset(streamAge, 0, sizeof(streamAge));
        okCommandQueue.clear();

        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            timeoutAt(frameTime + unpolledResponseTime + 1.0);
            responseState = RESP_UNPOLLED_RUN;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            discardData(frameTime + 30.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

            invalidateLogValues(frameTime);
        }
    }
        break;

    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            spancheckController->advance(frameTime, realtimeEgress);

            timeoutAt(frameTime + unpolledResponseTime + 1.0);

            if (responseState == RESP_UNPOLLED_RUN) {
                if (!okCommandQueue.empty() || activeParameters != targetParameters) {
                    if (controlStream != NULL) {
                        controlStream->writeControl("UE\r");
                    }
                    commandTry = 0;
                    responseState = RESP_INTERACTIVE_UPDATE_STOPREPORTS;
                    discardData(frameTime + unpolledResponseTime + 0.5);
                    timeoutAt(frameTime + unpolledResponseTime + 5.0);
                }
            } else {
                okCommandQueue.clear();
            }
        } else if (code > 0) {
            qCDebug(log) << "Line at " << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();

            if (responseState == RESP_PASSIVE_RUN) {
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
                info.hash("ResponseState").setString("PassiveRun");
            } else {
                if (controlStream != NULL) {
                    controlStream->writeControl("\r\r\r\r\r\r\r\r\r\r\rUE\rUE\rUE\rUE\r");
                }
                responseState = RESP_INTERACTIVE_START_STOPREPORTS;
                commandTry = 0;
                discardData(frameTime + unpolledResponseTime + 2.0);
                timeoutAt(frameTime + unpolledResponseTime + 30.0);
                info.hash("ResponseState").setString("Run");
            }
            generalStatusUpdated();

            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_STOPREPORTS:
        if (frame != "OK") {
            if (controlStream != NULL) {
                controlStream->writeControl("PU\r");
            }

            responseState = RESP_INTERACTIVE_START_STOPREPORTS_POWERUP;
            commandTry = 0;
            discardData(frameTime + 5.0);
            timeoutAt(frameTime + 10.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveStopReportsPowerUp"), frameTime, FP::undefined()));
            }
        } else {
            if (controlStream != NULL) {
                controlStream->writeControl("SD0\r");
            }

            responseState = RESP_INTERACTIVE_START_SETDELIMITER;
            timeoutAt(frameTime + 2.0);
            discardData(FP::undefined());

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveSetDelimiter"), frameTime, FP::undefined()));
            }
        }
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS_POWERUP:
        if (frame == "OK") {
            if (controlStream != NULL) {
                controlStream->writeControl("SD0\r");
            }

            responseState = RESP_INTERACTIVE_START_SETDELIMITER;
            timeoutAt(frameTime + 2.0);
            discardData(FP::undefined());

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveSetDelimiter"), frameTime, FP::undefined()));
            }
        } else if (frame == "ERROR" && commandTry < 5) {
            ++commandTry;

            if (controlStream != NULL) {
                controlStream->writeControl("PU\r");
            }

            discardData(frameTime + 2.0);
            timeoutAt(frameTime + 5.0);
        } else {
            qCDebug(log) << "Invalid response" << frame << "while attempting to power up";

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            invalidateLogValues(frameTime);
            return;
        }
        break;

    case RESP_INTERACTIVE_START_SETDELIMITER:
        startOkCommand(frame, frameTime, "SB0,0\r", RESP_INTERACTIVE_START_SETANALOGOUT,
                       "StartInteractiveSetAnalogOut");
        break;
    case RESP_INTERACTIVE_START_SETANALOGOUT:
        if (!config.front().disableSpancheckValve) {
            startOkCommand(frame, frameTime, "SX0\r", RESP_INTERACTIVE_START_SETANALOGVALUE,
                           "StartInteractiveSetAnalogOut");
        } else {
            startOkCommand(frame, frameTime, "RV\r", RESP_INTERACTIVE_START_READFIRMWAREVERSION,
                           "StartInteractiveReadFirmwareVersion");
        }
        break;
    case RESP_INTERACTIVE_START_SETANALOGVALUE:
        startOkCommand(frame, frameTime, "RV\r", RESP_INTERACTIVE_START_READFIRMWAREVERSION,
                       "StartInteractiveReadFirmwareVersion");
        break;
    case RESP_INTERACTIVE_START_READFIRMWAREVERSION:
        if (frame == "ERROR" || frame.empty()) {
            qCDebug(log) << "Invalid response" << frame << "to firmware version read";

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            invalidateLogValues(frameTime);
        } else {
            auto newValue = frame.toString();
            if (instrumentMeta["FirmwareVersion"].toString() != newValue) {
                instrumentMeta["FirmwareVersion"].setString(newValue);
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            if (controlStream != NULL) {
                controlStream->writeControl("UT1\r");
            }

            responseState = RESP_INTERACTIVE_START_ENABLERECORDS_T;
            timeoutAt(frameTime + 2.0);
            discardData(FP::undefined());

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveEnableRecords"), frameTime, FP::undefined()));
            }
        }
        break;
    case RESP_INTERACTIVE_START_ENABLERECORDS_T:
        startOkCommand(frame, frameTime, "UD1\r", RESP_INTERACTIVE_START_ENABLERECORDS_D,
                       "StartInteractiveEnableRecords");
        break;
    case RESP_INTERACTIVE_START_ENABLERECORDS_D:
        startOkCommand(frame, frameTime, "UY1\r", RESP_INTERACTIVE_START_ENABLERECORDS_Y,
                       "StartInteractiveEnableRecords");
        break;
    case RESP_INTERACTIVE_START_ENABLERECORDS_Y:
        startOkCommand(frame, frameTime, "UP3\r", RESP_INTERACTIVE_START_ENABLERECORDS_P,
                       "StartInteractiveEnableRecords");
        break;
    case RESP_INTERACTIVE_START_ENABLERECORDS_P:
        startOkCommand(frame, frameTime, "UZ1\r", RESP_INTERACTIVE_START_ENABLERECORDS_Z,
                       "StartInteractiveEnableRecords");
        break;
    case RESP_INTERACTIVE_START_ENABLERECORDS_Z: {
        QDateTime dt(Time::toDateTime(frameTime + 0.5));
        Util::ByteArray send("STT");
        send += QByteArray::number(dt.date().year());
        send += ",";
        send += QByteArray::number(dt.date().month());
        send += ",";
        send += QByteArray::number(dt.date().day());
        send += ",";
        send += QByteArray::number(dt.time().hour());
        send += ",";
        send += QByteArray::number(dt.time().minute());
        send += ",";
        send += QByteArray::number(dt.time().second());
        send += "\r";
        startOkCommand(frame, frameTime, send, RESP_INTERACTIVE_START_SETTIME,
                       "StartInteractiveSetTime");
        break;
    }
    case RESP_INTERACTIVE_START_SETTIME:
        if (frame != "OK") {
            qCDebug(log) << "Invalid response" << frame << "to set time command";

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            invalidateLogValues(frameTime);
        } else {
            parameterIndex = -1;

            responseState = RESP_INTERACTIVE_START_PARAMETERS_READ;
            timeoutAt(frameTime + 2.0);
            discardData(FP::undefined());

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadParameters"), frameTime, FP::undefined()));
            }

            startReadParameters(frame, frameTime);
        }
        break;

    case RESP_INTERACTIVE_START_PARAMETERS_READ:
        startReadParameters(frame, frameTime);
        break;
    case RESP_INTERACTIVE_START_PARAMETERS_WRITE:
        startWriteParameters(frame, frameTime);
        break;

    case RESP_INTERACTIVE_START_RECORD_Y: {
        int code = -1;
        if (frame.size() < 1 || frame[0] != 'Y' || (code = processRecord(frame, frameTime)) != 0) {
            qCDebug(log) << "Invalid response (code " << code << "):" << frame
                         << "to read Y command";

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            invalidateLogValues(frameTime);
        } else {
            if (controlStream != NULL) {
                controlStream->writeControl("RD\r");
            }

            responseState = RESP_INTERACTIVE_START_RECORD_D;
            timeoutAt(frameTime + 2.0);
            discardData(FP::undefined());

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadRecordD"), frameTime, FP::undefined()));
            }
        }
        break;
    }

    case RESP_INTERACTIVE_START_RECORD_D: {
        int code = -1;
        if (frame.size() < 1 || frame[0] != 'D' || (code = processRecord(frame, frameTime)) != 0) {
            qCDebug(log) << "Invalid response (code " << code << "): " << frame
                         << "to read D command";

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            invalidateLogValues(frameTime);
        } else {
            if (controlStream != NULL) {
                controlStream->writeControl("RZ\r");
            }

            zeroTCount = 0;
            zeroPCount = 0;
            responseState = RESP_INTERACTIVE_START_RECORD_Z;
            timeoutAt(frameTime + 2.0);
            discardData(FP::undefined());

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadRecordZ"), frameTime, FP::undefined()));
            }
        }
        break;
    }

    case RESP_INTERACTIVE_START_RECORD_Z: {
        int code = -1;
        if (frame.size() < 1 || frame[0] != 'Z' || (code = processRecord(frame, frameTime)) != 0) {
            qCDebug(log) << "Invalid response (code " << code << "):" << frame
                         << "to read Z command";

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            invalidateLogValues(frameTime);
        } else {
            if (controlStream != NULL) {
                controlStream->writeControl("UB\r");
            }

            memset(streamAge, 0, sizeof(streamAge));
            okCommandQueue.clear();
            responseState = RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID;
            timeoutAt(frameTime + unpolledResponseTime * 2.0 + 5.0);
            discardData(frameTime + 0.5, 1);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveUnpolledStartFlushing"), frameTime, FP::undefined()));
            }
        }
        break;
    }


    case RESP_INTERACTIVE_UPDATE_STOPREPORTS:
        if (frame != "OK") {
            if (commandTry > 5) {
                qCDebug(log) << "Failed to stop unpolled reports, last response was:" << frame;

                responseState = RESP_INTERACTIVE_RESTART_WAIT;
                if (controlStream != NULL)
                    controlStream->resetControl();
                timeoutAt(FP::undefined());
                discardData(frameTime + 10.0);

                {
                    Variant::Write info = Variant::Write::empty();
                    info.hash("ResponseState").setString("UpdateStopReports");
                    info.hash("Line").setString(frame.toString());
                    event(frameTime, QObject::tr(
                            "Failed to stop reports for update.  Communications dropped."), true,
                          info);
                }

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
                }
                autoprobeStatusUpdated();
                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(
                            SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                          frameTime, FP::undefined()));
                }

                invalidateLogValues(frameTime);
                break;
            }

            ++commandTry;
            discardData(frameTime + 0.25);
            break;
        }

        parameterIndex = -1;
        commandTry = -1;
        if (activeParameters != targetParameters) {
            responseState = RESP_INTERACTIVE_UPDATE_PARAMETERS_WRITE;
        } else if (!okCommandQueue.empty()) {
            responseState = RESP_INTERACTIVE_UPDATE_COMMANDS;
        } else {
            if (controlStream != NULL) {
                controlStream->writeControl("UB\r");
            }
            memset(streamAge, 0, sizeof(streamAge));
            responseState = RESP_UNPOLLED_RUN;
            timeoutAt(frameTime + unpolledResponseTime * 2.0 + 5.0);
            discardData(frameTime + unpolledResponseTime + 0.5, 1);
            break;
        }
        commandTry = 0;
        discardData(frameTime + 0.5);
        break;

    case RESP_INTERACTIVE_UPDATE_PARAMETERS_WRITE:
        updateWriteParameters(frame, frameTime);
        break;

    case RESP_INTERACTIVE_UPDATE_COMMANDS:
        updateCommandResponse(frame, frameTime);
        break;

    default:
        break;
    }
}

void AcquireTSINeph3563::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configAdvance(frameTime);

    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
    case RESP_INTERACTIVE_UPDATE_STOPREPORTS:
    case RESP_INTERACTIVE_UPDATE_PARAMETERS_WRITE:
    case RESP_INTERACTIVE_UPDATE_COMMANDS:
        qCDebug(log) << "Timeout in unpolled state" << responseState << "at"
                     << Logging::time(frameTime);
        timeoutAt(frameTime + 30.0);

        if (controlStream != NULL) {
            controlStream->writeControl("\r\r\r\r\r\r\r\r\r\r\rUE\rUE\rUE\rUE\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        commandTry = 0;
        discardData(frameTime + 2.0);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("UnpolledRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS:
        if (controlStream != NULL) {
            controlStream->writeControl("\rPU\r");
        }

        responseState = RESP_INTERACTIVE_START_STOPREPORTS_POWERUP;
        commandTry = 0;
        discardData(frameTime + 2.0);
        timeoutAt(frameTime + 5.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveStopReportsPowerUp"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS_POWERUP:
    case RESP_INTERACTIVE_START_SETDELIMITER:
    case RESP_INTERACTIVE_START_SETANALOGOUT:
    case RESP_INTERACTIVE_START_SETANALOGVALUE:
    case RESP_INTERACTIVE_START_READFIRMWAREVERSION:
    case RESP_INTERACTIVE_START_ENABLERECORDS_T:
    case RESP_INTERACTIVE_START_ENABLERECORDS_D:
    case RESP_INTERACTIVE_START_ENABLERECORDS_Y:
    case RESP_INTERACTIVE_START_ENABLERECORDS_P:
    case RESP_INTERACTIVE_START_ENABLERECORDS_Z:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_RECORD_Y:
    case RESP_INTERACTIVE_START_RECORD_D:
    case RESP_INTERACTIVE_START_RECORD_Z:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_INTERACTIVE_START_PARAMETERS_READ:
    case RESP_INTERACTIVE_START_PARAMETERS_WRITE:
    case RESP_INTERACTIVE_RESTART_WAIT:
        if (responseState == RESP_INTERACTIVE_START_PARAMETERS_READ) {
            std::vector<std::string> parameterNames;
            for (const auto &add : activeParameters.parameters) {
                parameterNames.emplace_back(add.first);
            }
            std::sort(parameterNames.begin(), parameterNames.end());
            std::string pname;
            if (parameterIndex < 0 || parameterIndex >= static_cast<int>(parameterNames.size()))
                pname = "INVALID";
            else
                pname = parameterNames[parameterIndex];

            qCDebug(log) << "Timeout during interactive start parameters read for" << pname << "at"
                         << Logging::time(frameTime);
        } else if (responseState == RESP_INTERACTIVE_START_PARAMETERS_WRITE) {
            std::vector<std::string> parameterNames;
            for (const auto &add : activeParameters.parameters) {
                parameterNames.emplace_back(add.first);
            }
            std::sort(parameterNames.begin(), parameterNames.end());
            std::string pname;
            if (parameterIndex < 0 || parameterIndex >= static_cast<int>(parameterNames.size()))
                pname = "INVALID";
            else
                pname = parameterNames[parameterIndex];

            qCDebug(log) << "Timeout during interactive start parameters write for" << pname
                         << "try" << commandTry << "at" << Logging::time(frameTime);
        } else {
            qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                         << Logging::time(frameTime);
        }

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        timeoutAt(frameTime + unpolledResponseTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("\r\r\r\r\r\r\r\r\r\r\rUE\rUE\rUE\rUE\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        commandTry = 0;
        discardData(frameTime + unpolledResponseTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        invalidateLogValues(frameTime);
        break;

    default:
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireTSINeph3563::discardDataCompleted(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configAdvance(frameTime);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_INTERACTIVE_START_STOPREPORTS:
        if (commandTry > 5) {
            if (controlStream != NULL) {
                controlStream->writeControl("UE\r");
            }

            timeoutAt(frameTime + 1.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveStopReportsAck"), frameTime, FP::undefined()));
            }
        } else {
            ++commandTry;

            if (controlStream != NULL) {
                controlStream->writeControl("UE\r");
            }

            timeoutAt(frameTime + 10.0);
            discardData(frameTime + 2.0);
        }
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS_POWERUP:
        if (controlStream != NULL) {
            controlStream->writeControl("UE\r");
        }

        timeoutAt(frameTime + 1.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveStopReportsAck"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_UPDATE_STOPREPORTS:
        if (controlStream != NULL) {
            controlStream->writeControl("UE\r");
        }
        responseState = RESP_INTERACTIVE_UPDATE_STOPREPORTS;

        timeoutAt(frameTime + 1.0);
        break;

    case RESP_INTERACTIVE_UPDATE_PARAMETERS_WRITE:
        updateWriteParameters(Util::ByteArray(), frameTime);
        break;

    case RESP_INTERACTIVE_UPDATE_COMMANDS:
        updateSendNextCommand(frameTime);
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        timeoutAt(frameTime + unpolledResponseTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("\r\r\r\r\r\r\r\r\r\r\rUE\rUE\rUE\rUE\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        commandTry = 0;
        discardData(frameTime + unpolledResponseTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    default:
        break;
    }
}


void AcquireTSINeph3563::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frame);
    if (!FP::defined(frameTime)) {
        frameTime = lastRecordTime;
        if (!FP::defined(frameTime))
            return;
    }
    /* Maybe implement something to recover from a UE more gracefully than
     * timing out? */
}


SequenceValue::Transfer AcquireTSINeph3563::constructSpancheckVariables()
{
    SequenceValue::Transfer result;
    if (!spancheckDetails.read().exists())
        return result;

    result.emplace_back(spancheckDetails);

    for (int i = 0; i < 3; i++) {
        auto add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                            NephelometerSpancheckController::TotalPercentError,
                                                            QString::fromStdString(colorLookup[i]));
        if (add.read().exists()) {
            result.emplace_back(SequenceName({}, "raw", "PCTc" + colorLookup[i]), std::move(add),
                                spancheckDetails.getStart(), spancheckDetails.getEnd());
        }

        add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                       NephelometerSpancheckController::BackPercentError,
                                                       QString::fromStdString(colorLookup[i]));
        if (add.read().exists()) {
            result.emplace_back(SequenceName({}, "raw", "PCTbc" + colorLookup[i]), std::move(add),
                                spancheckDetails.getStart(), spancheckDetails.getEnd());
        }


        add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                       NephelometerSpancheckController::TotalSensitivityFactor,
                                                       QString::fromStdString(colorLookup[i]));
        if (add.read().exists()) {
            result.emplace_back(SequenceName({}, "raw", "Cc" + colorLookup[i]), std::move(add),
                                spancheckDetails.getStart(), spancheckDetails.getEnd());
        }

        add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                       NephelometerSpancheckController::BackSensitivityFactor,
                                                       QString::fromStdString(colorLookup[i]));
        if (add.read().exists()) {
            result.emplace_back(SequenceName({}, "raw", "Cbc" + colorLookup[i]), std::move(add),
                                spancheckDetails.getStart(), spancheckDetails.getEnd());
        }
    }

    return result;
}

SequenceValue::Transfer AcquireTSINeph3563::constructRealtimeSpancheckVariables()
{
    SequenceValue::Transfer result;
    if (!spancheckDetails.read().exists())
        return result;

    for (int i = 0; i < 3; i++) {
        auto add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                            NephelometerSpancheckController::TSI3563K2,
                                                            QString::fromStdString(colorLookup[i]));
        if (add.read().exists()) {
            result.emplace_back(SequenceName({}, "raw", "ZSPANCHECKk2" + colorLookup[i]),
                                std::move(add), spancheckDetails.getStart(),
                                spancheckDetails.getEnd());
        }

        add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                       NephelometerSpancheckController::TSI3563K4,
                                                       QString::fromStdString(colorLookup[i]));
        if (add.read().exists()) {
            result.emplace_back(SequenceName({}, "raw", "ZSPANCHECKk4" + colorLookup[i]),
                                std::move(add), spancheckDetails.getStart(),
                                spancheckDetails.getEnd());
        }
    }

    return result;
}

void AcquireTSINeph3563::updateSpancheckData()
{
    if (persistentEgress) {
        spancheckDetails.setEnd(lastRecordTime);
        persistentEgress->incomingData(constructSpancheckVariables());
    }

    spancheckDetails.setRoot(
            spancheckController->results(NephelometerSpancheckController::FullResults));
    spancheckDetails.setStart(lastRecordTime);
    spancheckDetails.setEnd(FP::undefined());

    persistentValuesUpdated();

    if (realtimeEgress) {
        SequenceValue::Transfer output(constructSpancheckVariables());
        Util::append(constructRealtimeSpancheckVariables(), output);
        realtimeEgress->incomingData(std::move(output));
    }
}

SequenceValue::Transfer AcquireTSINeph3563::getPersistentValues()
{
    PauseLock paused(*this);
    SequenceValue::Transfer result(constructSpancheckVariables());

    if (FP::defined(lastMode.getStart())) {
        result.emplace_back(lastMode);
    }

    return result;
}

Variant::Root AcquireTSINeph3563::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireTSINeph3563::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN:
    case RESP_INTERACTIVE_UPDATE_STOPREPORTS:
    case RESP_INTERACTIVE_UPDATE_PARAMETERS_WRITE:
    case RESP_INTERACTIVE_UPDATE_COMMANDS:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}


AcquireTSINeph3563::SpancheckInterface::SpancheckInterface(AcquireTSINeph3563 *neph) : parent(neph)
{ }

AcquireTSINeph3563::SpancheckInterface::~SpancheckInterface()
{ }

void AcquireTSINeph3563::SpancheckInterface::setBypass(bool enable)
{
    if (parent->state == NULL)
        return;
    if (enable) {
        parent->state->setBypassFlag("Spancheck");
        qCDebug(parent->log) << "Spancheck bypass flag set";
    } else {
        parent->state->clearBypassFlag("Spancheck");
        qCDebug(parent->log) << "Spancheck bypass flag cleared";
    }
}

void AcquireTSINeph3563::SpancheckInterface::switchToFilteredAir()
{
    Q_ASSERT(!parent->config.isEmpty());

    static_cast<IntegerParameter *>(parent->targetParameters.parameters["B"].get())->value = 255;

    if (!parent->config.front().disableSpancheckValve) {
        parent->okCommandQueue.emplace_back("SX0");
        parent->okCommandQueue.emplace_back("SX0");
    }

    qCDebug(parent->log) << "Spancheck switching to filtered air";
}

void AcquireTSINeph3563::SpancheckInterface::switchToGas(CPD3::Algorithms::Rayleigh::Gas)
{
    Q_ASSERT(!parent->config.empty());

    static_cast<IntegerParameter *>(parent->targetParameters.parameters["B"].get())->value = 0;

    if (!parent->config.front().disableSpancheckValve) {
        parent->okCommandQueue.emplace_back("SX5000");
        parent->okCommandQueue.emplace_back("SX5000");
    }

    qCDebug(parent->log) << "Spancheck switching to span gas";
}

void AcquireTSINeph3563::SpancheckInterface::issueZero()
{
    Q_ASSERT(!parent->config.isEmpty());

    /* Restore automatic control of the zero valve at this point and don't send a V command,
     * since we're going to send the zero right now anyway. */
    ZeroValveParameter
            *v = static_cast<ZeroValveParameter *>(parent->targetParameters.parameters["V"].get());
    v->automatic = true;
    v->value = 0;
    v = static_cast<ZeroValveParameter *>(parent->activeParameters.parameters["V"].get());
    v->automatic = true;
    v->value = 0;
    parent->realtimeParametersUpdated = true;

    parent->okCommandQueue.emplace_back("Z");

    if (FP::defined(parent->lastRecordTime)) {
        parent->modeUpdateTime =
                parent->lastRecordTime + parent->config.first().reportInterval + 1.0;
    }

    qCDebug(parent->log) << "Spancheck issuing zero";
}

void AcquireTSINeph3563::SpancheckInterface::issueCommand(const QString &target,
                                                          const CPD3::Data::Variant::Read &command)
{
    if (parent->state == NULL)
        return;
    parent->state->sendCommand(target.toStdString(), command);
}

bool AcquireTSINeph3563::SpancheckInterface::start()
{
    Q_ASSERT(!parent->config.isEmpty());

    switch (parent->responseState) {
    case AcquireTSINeph3563::RESP_PASSIVE_RUN:
    case AcquireTSINeph3563::RESP_UNPOLLED_RUN:
        break;
    default:
        return false;
    }
    switch (parent->sampleState) {
    case AcquireTSINeph3563::SAMPLE_RUN:
        break;
    default:
        return false;
    }

    qCDebug(parent->log) << "Beginning spancheck";
    parent->sampleState = SAMPLE_SPANCHECK;
    parent->realtimeStateUpdated = true;

    ZeroValveParameter
            *v = static_cast<ZeroValveParameter *>(parent->targetParameters.parameters["V"].get());
    v->value = 1;
    v->automatic = false;

    if (FP::defined(parent->lastRecordTime)) {
        parent->modeUpdateTime =
                parent->lastRecordTime + parent->config.first().reportInterval + 1.0;
    }

    parent->event(parent->lastRecordTime, QObject::tr("Spancheck initiated."), false);

    return true;
}

void AcquireTSINeph3563::SpancheckInterface::completed()
{
    Q_ASSERT(!parent->config.isEmpty());

    parent->updateSpancheckData();
    if (parent->sampleState == SAMPLE_SPANCHECK)
        parent->sampleState = SAMPLE_RUN;
    parent->realtimeStateUpdated = true;

    parent->targetParameters.parameters["B"]->copy(
            parent->config.first().parameters.parameters["B"].get());

    qCDebug(parent->log) << "Spancheck completed";

    parent->event(parent->lastRecordTime, QObject::tr("Spancheck completed."), false,
                  parent->spancheckDetails.getValue());
}

void AcquireTSINeph3563::SpancheckInterface::aborted()
{
    Q_ASSERT(!parent->config.isEmpty());

    switch (parent->responseState) {
    case AcquireTSINeph3563::RESP_PASSIVE_RUN:
    case AcquireTSINeph3563::RESP_UNPOLLED_RUN:
        break;
    default:
        return;
    }

    parent->targetParameters.parameters["B"]->copy(
            parent->config.first().parameters.parameters["B"].get());

    ZeroValveParameter
            *v = static_cast<ZeroValveParameter *>(parent->targetParameters.parameters["V"].get());
    v->value = 0;
    v->automatic = true;

    if (!parent->config.front().disableSpancheckValve) {
        parent->okCommandQueue.emplace_back("SX0");
        parent->okCommandQueue.emplace_back("SX0");
    }
    parent->okCommandQueue.emplace_back("Z");

    parent->sampleState = SAMPLE_RUN;
    parent->realtimeStateUpdated = true;

    if (FP::defined(parent->lastRecordTime)) {
        parent->modeUpdateTime =
                parent->lastRecordTime + parent->config.first().reportInterval + 1.0;
    }

    qCDebug(parent->log) << "Spancheck aborted";

    parent->event(parent->lastRecordTime, QObject::tr("Spancheck aborted."), false);
}

static double toAnalogOutput(const Variant::Read &value)
{
    if (!value.exists())
        return FP::undefined();
    switch (value.getType()) {
    case Variant::Type::Boolean:
        return value.toBool() ? 5.0 : 0.0;
    case Variant::Type::Real: {
        auto v = value.toDouble();
        if (FP::defined(v)) {
            if (v < 0.0)
                v = 0.0;
            else if (v > 5.0)
                v = 5.0;
            return v;
        }
        return 0.0;
    }
    case Variant::Type::Integer: {
        auto v = value.toInteger();
        if (INTEGER::defined(v)) {
            if (v < 0)
                v = 0;
            else if (v > 5)
                v = 5;
            return static_cast<double>(v);
        }
        return 0.0;
    }
    default:
        break;
    }
    return 0.0;
}

void AcquireTSINeph3563::command(const Variant::Read &command)
{
    Q_ASSERT(!config.isEmpty());

    spancheckController->command(command);

    if (command.hash("StartZero").exists()) {
        if (sampleState != SAMPLE_RUN) {
            qCDebug(log) << "Discarding zero request, sample state:" << sampleState
                         << ", response state:" << responseState;
        } else if (!static_cast<ZeroValveParameter *>(activeParameters.parameters["V"].get())->automatic) {
            qCDebug(log) << "Discarding zero request due to valve under manual control in state"
                         << (int) static_cast<ZeroValveParameter *>(activeParameters.parameters["V"]
                                 .get())->value;
        } else {
            switch (responseState) {
            case RESP_UNPOLLED_RUN:
            case RESP_INTERACTIVE_RESTART_WAIT:
            case RESP_INTERACTIVE_INITIALIZE:
            case RESP_INTERACTIVE_UPDATE_STOPREPORTS:
            case RESP_INTERACTIVE_UPDATE_PARAMETERS_WRITE:
            case RESP_INTERACTIVE_UPDATE_COMMANDS:
                qCDebug(log) << "Queued zero initiate command from state" << responseState;
                okCommandQueue.emplace_back("Z");

                if (FP::defined(lastRecordTime)) {
                    modeUpdateTime = lastRecordTime + config.first().reportInterval + 1.0;
                }
                break;

            case RESP_PASSIVE_RUN:
            case RESP_PASSIVE_WAIT:
            case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
            case RESP_AUTOPROBE_PASSIVE_WAIT:
                qCDebug(log) << "Discarding zero from passive state" << responseState;
                break;

            case RESP_INTERACTIVE_START_STOPREPORTS:
            case RESP_INTERACTIVE_START_STOPREPORTS_POWERUP:
            case RESP_INTERACTIVE_START_SETDELIMITER:
            case RESP_INTERACTIVE_START_SETANALOGOUT:
            case RESP_INTERACTIVE_START_SETANALOGVALUE:
            case RESP_INTERACTIVE_START_READFIRMWAREVERSION:
            case RESP_INTERACTIVE_START_ENABLERECORDS_T:
            case RESP_INTERACTIVE_START_ENABLERECORDS_D:
            case RESP_INTERACTIVE_START_ENABLERECORDS_Y:
            case RESP_INTERACTIVE_START_ENABLERECORDS_P:
            case RESP_INTERACTIVE_START_ENABLERECORDS_Z:
            case RESP_INTERACTIVE_START_SETTIME:
            case RESP_INTERACTIVE_START_PARAMETERS_READ:
            case RESP_INTERACTIVE_START_PARAMETERS_WRITE:
            case RESP_INTERACTIVE_START_RECORD_Y:
            case RESP_INTERACTIVE_START_RECORD_D:
            case RESP_INTERACTIVE_START_RECORD_Z:
            case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
                qCDebug(log) << "Discarding zero request during start communications state"
                             << responseState;
                break;
            }
        }
    }

    if (command.hash("ApplySpancheckCalibration").exists()) {
        switch (responseState) {
        case RESP_UNPOLLED_RUN:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
        case RESP_INTERACTIVE_UPDATE_STOPREPORTS:
        case RESP_INTERACTIVE_UPDATE_PARAMETERS_WRITE:
        case RESP_INTERACTIVE_UPDATE_COMMANDS: {
            Variant::Write info = Variant::Write::empty();
            double k2[3];
            double k4[3];
            bool ok = true;
            for (int color = 0; color < 3; ++color) {
                k2[color] = roundExponent(
                        NephelometerSpancheckController::results(spancheckDetails.read(),
                                                                 NephelometerSpancheckController::TSI3563K2,
                                                                 QString::fromStdString(
                                                                         colorLookup[color])).read()
                                                                                             .toReal());
                k4[color] = roundDecimal(
                        NephelometerSpancheckController::results(spancheckDetails.read(),
                                                                 NephelometerSpancheckController::TSI3563K4,
                                                                 QString::fromStdString(
                                                                         colorLookup[color])).read()
                                                                                             .toReal());

                /* Check for sanity (and the neph will reject it anyway) */
                if (k2[color] <= 0.0) {
                    ok = false;
                }
                if (k4[color] <= 0.0 || k4[color] >= 1.0) {
                    ok = false;
                }

                info.hash(colorLookup[color]).hash("K2").setDouble(k2[color]);
                info.hash(colorLookup[color]).hash("K4").setDouble(k4[color]);
            }
            info.hash("Results").set(spancheckDetails.read());

            if (ok) {
                for (int color = 0; color < 3; ++color) {
                    ChannelCalibration *cal = static_cast<ChannelCalibration *>(
                            targetParameters.parameters["SK" + colorLookup[color]].get());
                    cal->K2 = k2[color];
                    cal->K4 = k4[color];
                }
                event(lastRecordTime,
                      QObject::tr("Spancheck results applied to instrument calibration."), false,
                      info);

                qCDebug(log) << "Applying spancheck calibration (" << k2[0] << "," << k2[1] << ","
                             << k2[2] << "," << k4[0] << "," << k4[1] << "," << k4[2]
                             << ") from state" << responseState;
            } else {
                event(lastRecordTime,
                      QObject::tr("Invalid spancheck calibration, no parameters changed."), true,
                      info);

                qCDebug(log) << "Discarding attempt to apply invalid calibration (" << k2[0] << ","
                             << k2[1] << "," << k2[2] << "," << k4[0] << "," << k4[1] << ","
                             << k4[2] << ") from state" << responseState;
            }

            break;
        }

        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
            qCDebug(log) << "Discarding spancheck calibration apply request from passive state"
                         << responseState;
            break;

        case RESP_INTERACTIVE_START_STOPREPORTS:
        case RESP_INTERACTIVE_START_STOPREPORTS_POWERUP:
        case RESP_INTERACTIVE_START_SETDELIMITER:
        case RESP_INTERACTIVE_START_SETANALOGOUT:
        case RESP_INTERACTIVE_START_SETANALOGVALUE:
        case RESP_INTERACTIVE_START_READFIRMWAREVERSION:
        case RESP_INTERACTIVE_START_ENABLERECORDS_T:
        case RESP_INTERACTIVE_START_ENABLERECORDS_D:
        case RESP_INTERACTIVE_START_ENABLERECORDS_Y:
        case RESP_INTERACTIVE_START_ENABLERECORDS_P:
        case RESP_INTERACTIVE_START_ENABLERECORDS_Z:
        case RESP_INTERACTIVE_START_SETTIME:
        case RESP_INTERACTIVE_START_PARAMETERS_READ:
        case RESP_INTERACTIVE_START_PARAMETERS_WRITE:
        case RESP_INTERACTIVE_START_RECORD_Y:
        case RESP_INTERACTIVE_START_RECORD_D:
        case RESP_INTERACTIVE_START_RECORD_Z:
        case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
            qCDebug(log) << "Discarding spancheck apply request during start communications state"
                         << responseState;
            break;
        }
    }

    if (config.front().disableSpancheckValve && command.hash("SetAnalog").exists()) {
        double v = toAnalogOutput(
                command.hash("SetAnalog").hash("Parameters").hash("Value").hash("Value"));
        if (!FP::defined(v))
            v = toAnalogOutput(command.hash("SetAnalog").hash("Parameters").hash("Value"));
        if (!FP::defined(v))
            v = toAnalogOutput(command.hash("SetAnalog").hash("Value"));
        if (!FP::defined(v))
            v = toAnalogOutput(command.hash("SetAnalog"));
        if (!FP::defined(v))
            v = 5.0;

        qCDebug(log) << "Setting analog output to" << v << "volts";

        Util::ByteArray data("SX");
        data += QByteArray::number(static_cast<int>(std::round(v * 1000.0))).rightJustified(4, '0',
                                                                                            true);

        okCommandQueue.emplace_back(data);
        okCommandQueue.emplace_back(data);
    }

    if (command.hash("SetParameters").exists()) {
        switch (responseState) {
        case RESP_UNPOLLED_RUN:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
        case RESP_INTERACTIVE_UPDATE_STOPREPORTS:
        case RESP_INTERACTIVE_UPDATE_PARAMETERS_WRITE:
        case RESP_INTERACTIVE_UPDATE_COMMANDS: {
            qCDebug(log) << "Updating parameters in state" << responseState;
            for (const auto &p : targetParameters.parameters) {
                p.second->fromCommand(command.hash("SetParameters").hash("Parameters"));
            }
            break;
        }

        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
            qCDebug(log) << "Discarding parameter update request from passive state"
                         << responseState;
            break;

        case RESP_INTERACTIVE_START_STOPREPORTS:
        case RESP_INTERACTIVE_START_STOPREPORTS_POWERUP:
        case RESP_INTERACTIVE_START_SETDELIMITER:
        case RESP_INTERACTIVE_START_SETANALOGOUT:
        case RESP_INTERACTIVE_START_SETANALOGVALUE:
        case RESP_INTERACTIVE_START_READFIRMWAREVERSION:
        case RESP_INTERACTIVE_START_ENABLERECORDS_T:
        case RESP_INTERACTIVE_START_ENABLERECORDS_D:
        case RESP_INTERACTIVE_START_ENABLERECORDS_Y:
        case RESP_INTERACTIVE_START_ENABLERECORDS_P:
        case RESP_INTERACTIVE_START_ENABLERECORDS_Z:
        case RESP_INTERACTIVE_START_SETTIME:
        case RESP_INTERACTIVE_START_PARAMETERS_READ:
        case RESP_INTERACTIVE_START_PARAMETERS_WRITE:
        case RESP_INTERACTIVE_START_RECORD_Y:
        case RESP_INTERACTIVE_START_RECORD_D:
        case RESP_INTERACTIVE_START_RECORD_Z:
        case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
            qCDebug(log) << "Discarding parameter update request during start communications state"
                         << responseState;
            break;
        }
    }
}

Variant::Root AcquireTSINeph3563::getCommands()
{
    Q_ASSERT(!config.empty());

    Variant::Root result = spancheckController->getCommands();

    result["StartZero"].hash("DisplayName").setString("Start &Zero");
    result["StartZero"].hash("ToolTip").setString("Start a zero offset adjustment.");
    result["StartZero"].hash("Include").array(0).hash("Type").setString("Equal");
    result["StartZero"].hash("Include").array(0).hash("Value").setString("Run");
    result["StartZero"].hash("Include").array(0).hash("Variable").setString("ZSTATE");
    result["StartZero"].hash("Exclude").array(0).hash("Type").setString("NotEqual");
    result["StartZero"].hash("Exclude").array(0).hash("Value").setString("Automatic");
    result["StartZero"].hash("Exclude").array(0).hash("Variable").setString("ZPARAMETERS");
    result["StartZero"].hash("Exclude").array(0).hash("Path").setString("V");

    result["ApplySpancheckCalibration"].hash("DisplayName")
                                       .setString("Apply Spancheck &Calibration");
    result["ApplySpancheckCalibration"].hash("ToolTip")
                                       .setString(
                                               "Apply the result of the last spancheck to the instrument calibration.");
    result["ApplySpancheckCalibration"].hash("Confirm")
                                       .setString(
                                               "This will change the instrument calibration to match the last spancheck.  Continue?");
    result["ApplySpancheckCalibration"].hash("Exclude").array(0).hash("Type").setString("NotEqual");
    result["ApplySpancheckCalibration"].hash("Exclude").array(0).hash("Value").setString("Run");
    result["ApplySpancheckCalibration"].hash("Exclude")
                                       .array(0)
                                       .hash("Variable")
                                       .setString("ZSTATE");
    result["ApplySpancheckCalibration"].hash("Include").array(0).hash("Type").setString("Defined");
    result["ApplySpancheckCalibration"].hash("Include")
                                       .array(0)
                                       .hash("Variable")
                                       .setString("ZSPANCHECK");

    if (config.front().disableSpancheckValve) {
        result["SetAnalog"].hash("DisplayName").setString("Set the &Analog Output");
        result["SetAnalog"].hash("ToolTip")
                           .setString("Change the current output of the analog output.");
        result["SetAnalog"].hash("Parameters").hash("Value").hash("Name").setString("Output");
        result["SetAnalog"].hash("Parameters").hash("Value").hash("Format").setString("0.000");
        result["SetAnalog"].hash("Parameters").hash("Value").hash("Minimum").setDouble(0.0);
        result["SetAnalog"].hash("Parameters").hash("Value").hash("Maximum").setDouble(5.0);
        result["SetAnalog"].hash("Parameters").hash("Value").hash("Variable").setString("Vx");
    }

    result["SetParameters"].hash("DisplayName").setString("Set &Parameters");
    result["SetParameters"].hash("ToolTip").setString("Change instrument parameters.");
    result["SetParameters"].hash("Include").array(0).hash("Type").setString("Equal");
    result["SetParameters"].hash("Include").array(0).hash("Value").setString("Run");
    result["SetParameters"].hash("Include").array(0).hash("Variable").setString("ZSTATE");
    for (const auto &p : activeParameters.parameters) {
        auto target = result["SetParameters"].hash("Parameters");
        p.second->commandParameter(target);
    }
    result["SetParameters"].hash("Parameters").hash("SL").remove();
    result["SetParameters"].hash("Parameters")
                           .hash("SMB")
                           .hash("Name")
                           .setString("SMB - Enable backscatter");
    result["SetParameters"].hash("Parameters")
                           .hash("SMZ")
                           .hash("Name")
                           .setString("SMZ - Zero mode");
    result["SetParameters"].hash("Parameters")
                           .hash("SP")
                           .hash("Name")
                           .setString("SP - Lamp power (watts)");
    result["SetParameters"].hash("Parameters").hash("STA").remove();
    result["SetParameters"].hash("Parameters")
                           .hash("STB")
                           .hash("Name")
                           .setString("STB - Blank time (s)");
    result["SetParameters"].hash("Parameters").hash("STP").remove();
    result["SetParameters"].hash("Parameters")
                           .hash("STZ")
                           .hash("Name")
                           .setString("STZ - Zero length (s)");
    result["SetParameters"].hash("Parameters")
                           .hash("B")
                           .hash("Name")
                           .setString("B - Blower power (0-255)");
    result["SetParameters"].hash("Parameters")
                           .hash("H")
                           .hash("Name")
                           .setString("H - Enable sample heater");
    result["SetParameters"].hash("Parameters").hash("V").hash("Name").setString("V - Zero valve");

    return result;
}

AcquisitionInterface::AutoprobeStatus AcquireTSINeph3563::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireTSINeph3563::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
    case RESP_INTERACTIVE_UPDATE_STOPREPORTS:
    case RESP_INTERACTIVE_UPDATE_PARAMETERS_WRITE:
    case RESP_INTERACTIVE_UPDATE_COMMANDS:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_STOPREPORTS_POWERUP:
    case RESP_INTERACTIVE_START_SETDELIMITER:
    case RESP_INTERACTIVE_START_SETANALOGOUT:
    case RESP_INTERACTIVE_START_SETANALOGVALUE:
    case RESP_INTERACTIVE_START_READFIRMWAREVERSION:
    case RESP_INTERACTIVE_START_ENABLERECORDS_T:
    case RESP_INTERACTIVE_START_ENABLERECORDS_D:
    case RESP_INTERACTIVE_START_ENABLERECORDS_Y:
    case RESP_INTERACTIVE_START_ENABLERECORDS_P:
    case RESP_INTERACTIVE_START_ENABLERECORDS_Z:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_PARAMETERS_READ:
    case RESP_INTERACTIVE_START_PARAMETERS_WRITE:
    case RESP_INTERACTIVE_START_RECORD_Y:
    case RESP_INTERACTIVE_START_RECORD_D:
    case RESP_INTERACTIVE_START_RECORD_Z:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + unpolledResponseTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("\r\r\r\r\r\r\r\r\r\r\rUE\rUE\rUE\rUE\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        commandTry = 0;
        discardData(time + unpolledResponseTime + 2.0);

        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireTSINeph3563::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    discardData(time + 0.5, 1);
    timeoutAt(time + unpolledResponseTime * 7.0 + 3.0);
    generalStatusUpdated();
}

void AcquireTSINeph3563::autoprobePromote(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        qCDebug(log) << "Promoted from interactive run to interactive acquisition at"
                     << Logging::time(time);

        timeoutAt(time + unpolledResponseTime + 1.0);
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_STOPREPORTS_POWERUP:
    case RESP_INTERACTIVE_START_SETDELIMITER:
    case RESP_INTERACTIVE_START_SETANALOGOUT:
    case RESP_INTERACTIVE_START_SETANALOGVALUE:
    case RESP_INTERACTIVE_START_READFIRMWAREVERSION:
    case RESP_INTERACTIVE_START_ENABLERECORDS_T:
    case RESP_INTERACTIVE_START_ENABLERECORDS_D:
    case RESP_INTERACTIVE_START_ENABLERECORDS_Y:
    case RESP_INTERACTIVE_START_ENABLERECORDS_P:
    case RESP_INTERACTIVE_START_ENABLERECORDS_Z:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_PARAMETERS_READ:
    case RESP_INTERACTIVE_START_PARAMETERS_WRITE:
    case RESP_INTERACTIVE_START_RECORD_Y:
    case RESP_INTERACTIVE_START_RECORD_D:
    case RESP_INTERACTIVE_START_RECORD_Z:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_INTERACTIVE_UPDATE_STOPREPORTS:
    case RESP_INTERACTIVE_UPDATE_PARAMETERS_WRITE:
    case RESP_INTERACTIVE_UPDATE_COMMANDS:
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + unpolledResponseTime + 1.0);
        break;

    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + unpolledResponseTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("\r\r\r\r\r\r\r\r\r\r\rUE\rUE\rUE\rUE\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        commandTry = 0;
        discardData(time + unpolledResponseTime + 2.0);

        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireTSINeph3563::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    timeoutAt(time + unpolledResponseTime + 1.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_UNPOLLED_RUN:
        /* Already in unpolled, so don't reset timeout */
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from unpolled interactive state to passive acquisition at"
                     << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        memset(streamAge, 0, sizeof(streamAge));
        realtimeStateUpdated = true;

        discardData(FP::undefined());
        generalStatusUpdated();
        break;
    }
}

static const quint16 serializedStateVersion = 1;

void AcquireTSINeph3563::serializeState(QDataStream &stream)
{
    PauseLock paused(*this);

    stream << serializedStateVersion;

    stream << spancheckDetails.getStart();
    stream << spancheckDetails.read();
    /* Maybe serialize the zero values for photon count recovery? */
}

void AcquireTSINeph3563::deserializeState(QDataStream &stream)
{
    quint16 version = 0;
    stream >> version;
    if (version != serializedStateVersion)
        return;

    PauseLock paused(*this);

    double start;
    stream >> start;
    spancheckDetails.setStart(start);
    spancheckDetails.setEnd(FP::undefined());
    stream >> spancheckDetails.root();
}

AcquisitionInterface::AutomaticDefaults AcquireTSINeph3563::getDefaults()
{
    AutomaticDefaults result;
    result.name = "S$1$2";
    result.interface["Baud"].setInt64(9600);
    result.interface["Parity"].setString("Even");
    result.interface["DataBits"].setInt64(7);
    result.interface["StopBits"].setInt64(1);
    return result;
}


QString AcquireTSINeph3563Component::getComponentName() const
{ return QString::fromLatin1("acquire_tsi_neph3563"); }

QString AcquireTSINeph3563Component::getComponentDisplayName() const
{ return tr("Acquire TSI 3563 Integrating Nephelometer data"); }

QString AcquireTSINeph3563Component::getComponentDescription() const
{
    return tr("This component provides a parser and control to acquire data from a TSI 3563 "
              "Integrating Nephelometer.  This includes both offline parsing and online "
              "control.");
}

ComponentOptions AcquireTSINeph3563Component::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);
    return options;
}

ComponentOptions AcquireTSINeph3563Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireTSINeph3563Component::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireTSINeph3563Component::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireTSINeph3563Component::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireTSINeph3563Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireTSINeph3563Component::createAcquisitionPassive(const ComponentOptions &options,
                                                                                    const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireTSINeph3563(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireTSINeph3563Component::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                    const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireTSINeph3563(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireTSINeph3563Component::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                      const std::string &loggingContext)
{
    std::unique_ptr<AcquireTSINeph3563> i(new AcquireTSINeph3563(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireTSINeph3563Component::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireTSINeph3563> i(new AcquireTSINeph3563(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
