/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;

    enum {
        Record_T = 0x01,
        Record_D = 0x02,
        Record_Y = 0x04,
        Record_P = 0xF0,
        Record_PB = 0x10,
        Record_PG = 0x20,
        Record_PR = 0x40,
    };
    int enabledRecords;
    int pendingRealtimeOutput;

    enum {
        Sample, ZeroStartBlank, ZeroRun, ZeroEndBlank,
    } mode;
    double modeRemainingTime;

    double Cs[3];
    double Cbs[3];
    double Cd[3];
    double Cbd[3];
    double Cf[3];
    double Revs;
    double Revsb;
    double T;
    double Tu;
    double U;
    double P;
    double Vl;
    double Al;
    int Vx;
    double Bsw[3];
    double Bbsw[3];

    static bool readInt(const QByteArray &d, int &target, int min = INT_MIN, int max = INT_MAX)
    {
        bool ok = false;
        target = d.toInt(&ok);
        if (!ok)
            return false;
        if (target < min || target > max)
            return false;
        return true;
    }

    static bool readDouble(const QByteArray &d,
                           double &target,
                           double min = FP::undefined(),
                           double max = FP::undefined())
    {
        bool ok = false;
        target = d.toDouble(&ok);
        if (!ok)
            return false;
        if (FP::defined(min) && target < min)
            return false;
        if (FP::defined(max) && target > max)
            return false;
        return true;
    }

    static QByteArray writeExponent(double v)
    {
        return FP::scientificFormat(v, 3, 1, QString('e'), QString('+'), QString('+')).toLatin1();
    }

    struct AnalogCal {
        int alow;
        int vlow;
        int ahigh;
        int vhigh;

        AnalogCal() : alow(100), vlow(100), ahigh(1000), vhigh(999)
        { }

        QByteArray read(const QByteArray &d)
        {
            QList<QByteArray> fields(d.split(','));
            if (fields.size() < 4)
                return QByteArray("ERROR\r");
            if (!readInt(fields.takeFirst(), alow, 0, 65534))
                return QByteArray("ERROR\r");
            if (!readInt(fields.takeFirst(), vlow, 0, 11999))
                return QByteArray("ERROR\r");
            if (!readInt(fields.takeFirst(), ahigh, 1, 65535))
                return QByteArray("ERROR\r");
            if (!readInt(fields.takeFirst(), vhigh, 1, 12000))
                return QByteArray("ERROR\r");
            return QByteArray("OK\r");
        }

        QByteArray write()
        {
            QByteArray result;
            result.append(QByteArray::number(alow));
            result.append(',');
            result.append(QByteArray::number(vlow));
            result.append(',');
            result.append(QByteArray::number(ahigh));
            result.append(',');
            result.append(QByteArray::number(vhigh));
            result.append('\r');
            return result;
        }
    };

    AnalogCal SCI;
    AnalogCal SCP;
    AnalogCal SCR;
    AnalogCal SCS;

    struct ChannelCal {
        int K1;
        double K2;
        double K3;
        double K4;

        ChannelCal() : K1(20000), K2(4.652E-3), K3(2.2E-5), K4(0.497)
        { }

        QByteArray read(const QByteArray &d)
        {
            QList<QByteArray> fields(d.split(','));
            if (fields.size() < 4)
                return QByteArray("ERROR\r");
            if (!readInt(fields.takeFirst(), K1, 1, 65535))
                return QByteArray("ERROR\r");
            if (!readDouble(fields.takeFirst(), K2, 0.0))
                return QByteArray("ERROR\r");
            if (!readDouble(fields.takeFirst(), K3, 0.0))
                return QByteArray("ERROR\r");
            if (!readDouble(fields.takeFirst(), K4, 0.0, 1.0))
                return QByteArray("ERROR\r");
            return QByteArray("OK\r");
        }

        QByteArray write()
        {
            QByteArray result;
            result.append(QByteArray::number(K1));
            result.append(',');
            result.append(writeExponent(K2));
            result.append(',');
            result.append(writeExponent(K3));
            result.append(',');
            result.append(QByteArray::number(K4, 'f', 3));
            result.append('\r');
            return result;
        }
    };

    ChannelCal SK[3];
    QByteArray SL;
    int SMB;
    int SMZ;
    int SP;
    int STA;
    int STB;
    int STP;
    QDateTime time;
    int STZ;
    int SV[3];
    int B;
    int H;
    enum {
        ZeroValve, NormalValve, MovingToZero, MovingToNormal, FaultValve,
    } V;
    double valveRemainingTime;

    QByteArray tRecord() const
    {
        QByteArray result("T,");
        result.append(QByteArray::number(time.date().year()));
        result.append(',');
        result.append(QByteArray::number(time.date().month()));
        result.append(',');
        result.append(QByteArray::number(time.date().day()));
        result.append(',');
        result.append(QByteArray::number(time.time().hour()));
        result.append(',');
        result.append(QByteArray::number(time.time().minute()));
        result.append(',');
        result.append(QByteArray::number(time.time().second()));
        result.append('\r');
        return result;
    }

    QByteArray pRecord(int color) const
    {
        QByteArray result;
        switch (color) {
        case 0:
            result = "B";
            break;
        case 1:
            result = "G";
            break;
        case 2:
            result = "R";
            break;
        default:
            Q_ASSERT(false);
            return QByteArray();
        }
        result.append(',');
        result.append(QByteArray::number((int) qRound(Cf[color])));
        result.append(',');
        result.append(QByteArray::number((int) qRound(Cs[color])));
        result.append(',');
        result.append(QByteArray::number((int) qRound(Cd[color])));
        result.append(',');
        result.append(QByteArray::number((int) qRound(Revs)));
        result.append(',');
        result.append(QByteArray::number((int) qRound(Cf[color])));
        result.append(',');
        result.append(QByteArray::number((int) qRound(Cbs[color])));
        result.append(',');
        result.append(QByteArray::number((int) qRound(Cbd[color])));
        result.append(',');
        result.append(QByteArray::number((int) qRound(Revsb)));
        result.append(',');
        result.append(QByteArray::number(P, 'f', 1));
        result.append(',');
        result.append(QByteArray::number(T, 'f', 1));
        result.append('\r');
        return result;
    }

    QByteArray dRecord() const
    {
        QByteArray result("D,");
        switch (mode) {
        case Sample:
            result.append('N');
            break;
        case ZeroStartBlank:
            result.append('B');
            break;
        case ZeroRun:
            result.append('Z');
            break;
        case ZeroEndBlank:
            result.append('B');
            break;
        }
        if (SMB) {
            result.append('B');
        } else {
            result.append('T');
        }
        result.append("XX,");
        if (mode == Sample && !SMZ)
            result.append('0');
        else
            result.append(QByteArray::number((int) ceil(modeRemainingTime)));
        for (int color = 0; color < 3; ++color) {
            result.append(',');
            result.append(writeExponent(calcBs(color)));
        }
        for (int color = 0; color < 3; ++color) {
            result.append(',');
            result.append(writeExponent(calcBbs(color)));
        }
        result.append('\r');
        return result;
    }

    QByteArray yRecord() const
    {
        QByteArray result("Y,");
        result.append(QByteArray::number((int) qRound(calcCf(1))));
        result.append(',');
        result.append(QByteArray::number(P, 'f', 1));
        result.append(',');
        result.append(QByteArray::number(T, 'f', 1));
        result.append(',');
        result.append(QByteArray::number(Tu, 'f', 1));
        result.append(',');
        result.append(QByteArray::number(U, 'f', 1));
        result.append(',');
        result.append(QByteArray::number(Vl, 'f', 1));
        result.append(',');
        result.append(QByteArray::number(Al, 'f', 1));
        result.append(',');
        result.append(QByteArray::number(Vx));
        result.append(",0000\r");
        return result;
    }

    QByteArray zRecord() const
    {
        QByteArray result("Z");
        double Bsr[3];
        for (int color = 0; color < 3; ++color) {
            Bsr[color] = SK[color].K3 * (P / 1013.25) * (273.15 / T);

            result.append(',');
            result.append(writeExponent(Bsw[color] + Bsr[color]));
        }
        for (int color = 0; color < 3; ++color) {
            result.append(',');
            result.append(writeExponent(Bbsw[color] + Bsr[color] * 0.5));
        }
        for (int color = 0; color < 3; ++color) {
            result.append(',');
            result.append(writeExponent(Bsr[color]));
        }
        result.append('\r');
        return result;
    }

    static double roundDecimalDigits(double in, int digits = 1)
    {
        double m = pow(10.0, digits);
        return (double) qRound(in * m) / m;
    }

    ModelInstrument()
            : incoming(),
              outgoing(),
              unpolledRemaining(0),
              enabledRecords(0xFF),
              pendingRealtimeOutput(0),
              mode(Sample),
              modeRemainingTime(32766)
    {

        for (int color = 0; color < 3; ++color) {
            Cs[color] = 11000 + color;
            Cbs[color] = 9000 + color;
            Cd[color] = 50 + color;
            Cbd[color] = 45 + color;
            Cf[color] = 1000 + color * 100;
            Bsw[color] = (0.1 + color * 0.1) * 1E-6;
            Bbsw[color] = (0.05 + color * 0.02) * 1E-6;
            SV[color] = 900 + color;
        }
        Revs = 10;
        Revsb = 11;
        T = 280.0;
        Tu = 275.0;
        U = 40.0;
        P = 950.0;

        Al = 4.0;
        Vl = 75.0 / Al;
        Vx = 0;

        SL = "Some Cal";
        SMB = 1;
        SMZ = 4;
        SP = 75;
        STA = 1;
        STB = 62;
        STP = 32766;
        time = QDateTime(QDate(2013, 01, 01), QTime(0, 0, 0));
        STZ = 300;
        B = 1;
        H = 1;
        V = NormalValve;
        valveRemainingTime = 0;

        SK[0].K3 = 2.789E-5;
        SK[1].K3 = 1.226E-5;
        SK[2].K3 = 4.605E-6;

        T = roundDecimalDigits(T);
        Tu = roundDecimalDigits(Tu);
        U = roundDecimalDigits(U);
        P = roundDecimalDigits(P);
        Vl = roundDecimalDigits(Vl);
        Al = roundDecimalDigits(Al);

        unpolledRemaining = STA;
    }

    void advance(double seconds)
    {

        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR));

            if (line == "UE") {
                outgoing.clear();
                outgoing.append("OK\r");
                unpolledRemaining = FP::undefined();
                pendingRealtimeOutput = 0;
            } else if (FP::defined(unpolledRemaining)) {
                /* Ignored */
            } else if (line.startsWith("SCI")) {
                if (line.length() > 3) {
                    outgoing.append(SCI.read(line.mid(3)));
                } else {
                    outgoing.append(SCI.write());
                }
            } else if (line.startsWith("SCP")) {
                if (line.length() > 3) {
                    outgoing.append(SCP.read(line.mid(3)));
                } else {
                    outgoing.append(SCP.write());
                }
            } else if (line.startsWith("SCR")) {
                if (line.length() > 3) {
                    outgoing.append(SCR.read(line.mid(3)));
                } else {
                    outgoing.append(SCR.write());
                }
            } else if (line.startsWith("SCS")) {
                if (line.length() > 3) {
                    outgoing.append(SCS.read(line.mid(3)));
                } else {
                    outgoing.append(SCS.write());
                }
            } else if (line == "SB0,0") {
                outgoing.append("OK\r");
            } else if (line == "SD0") {
                outgoing.append("OK\r");
            } else if (line.startsWith("SKB")) {
                if (line.length() > 3) {
                    outgoing.append(SK[0].read(line.mid(3)));
                } else {
                    outgoing.append(SK[0].write());
                }
            } else if (line.startsWith("SKG")) {
                if (line.length() > 3) {
                    outgoing.append(SK[1].read(line.mid(3)));
                } else {
                    outgoing.append(SK[1].write());
                }
            } else if (line.startsWith("SKR")) {
                if (line.length() > 3) {
                    outgoing.append(SK[2].read(line.mid(3)));
                } else {
                    outgoing.append(SK[2].write());
                }
            } else if (line.startsWith("SL")) {
                if (line.length() > 2) {
                    SL = line.mid(2);
                    outgoing.append("OK\r");
                } else {
                    outgoing.append(SL);
                    outgoing.append('\r');
                }
            } else if (line.startsWith("SMB")) {
                if (line.length() > 3) {
                    if (readInt(line.mid(3), SMB, 0, 1))
                        outgoing.append("OK\r");
                    else
                        outgoing.append("ERROR\r");
                } else {
                    outgoing.append(QByteArray::number(SMB));
                    outgoing.append('\r');
                }
            } else if (line.startsWith("SMZ")) {
                if (line.length() > 3) {
                    if (readInt(line.mid(3), SMZ, 0, 24))
                        outgoing.append("OK\r");
                    else
                        outgoing.append("ERROR\r");
                } else {
                    outgoing.append(QByteArray::number(SMZ));
                    outgoing.append('\r');
                }
            } else if (line.startsWith("SP")) {
                if (line.length() > 2) {
                    if (readInt(line.mid(2), SP, 0, 150)) {
                        outgoing.append("OK\r");
                        Al = (double) SP / Vl;
                        Al = roundDecimalDigits(Al);
                    } else {
                        outgoing.append("ERROR\r");
                    }
                } else {
                    outgoing.append(QByteArray::number(SP));
                    outgoing.append('\r');
                }
            } else if (line.startsWith("STA")) {
                if (line.length() > 3) {
                    if (readInt(line.mid(3), STA, 1, 9960))
                        outgoing.append("OK\r");
                    else
                        outgoing.append("ERROR\r");
                } else {
                    outgoing.append(QByteArray::number(STA));
                    outgoing.append('\r');
                }
            } else if (line.startsWith("STB")) {
                if (line.length() > 3) {
                    if (readInt(line.mid(3), STB, 5, 999))
                        outgoing.append("OK\r");
                    else
                        outgoing.append("ERROR\r");
                } else {
                    outgoing.append(QByteArray::number(STB));
                    outgoing.append('\r');
                }
            } else if (line.startsWith("STP")) {
                if (line.length() > 3) {
                    if (readInt(line.mid(3), STP, 5, 65535))
                        outgoing.append("OK\r");
                    else
                        outgoing.append("ERROR\r");
                } else {
                    outgoing.append(QByteArray::number(STP));
                    outgoing.append('\r');
                }
            } else if (line.startsWith("STT")) {
                if (line.length() > 3) {
                    QList<QByteArray> fields(line.mid(3).split(','));
                    if (fields.size() == 6) {
                        time = QDateTime(QDate(fields.at(0).toInt(), fields.at(1).toInt(),
                                               fields.at(2).toInt()),
                                         QTime(fields.at(3).toInt(), fields.at(4).toInt(),
                                               fields.at(5).toInt()));
                        if (time.isValid())
                            outgoing.append("OK\r");
                        else
                            outgoing.append("ERROR\r");
                    } else {
                        outgoing.append("ERROR\r");
                    }
                } else {
                    outgoing.append(tRecord());
                }
            } else if (line.startsWith("STZ")) {
                if (line.length() > 3) {
                    if (readInt(line.mid(3), STZ, 1, 9999))
                        outgoing.append("OK\r");
                    else
                        outgoing.append("ERROR\r");
                } else {
                    outgoing.append(QByteArray::number(STZ));
                    outgoing.append('\r');
                }
            } else if (line.startsWith("SVB")) {
                if (line.length() > 3) {
                    if (readInt(line.mid(3), SV[0], 0, 1200))
                        outgoing.append("OK\r");
                    else
                        outgoing.append("ERROR\r");
                } else {
                    outgoing.append(QByteArray::number(SV[0]));
                    outgoing.append('\r');
                }
            } else if (line.startsWith("SVG")) {
                if (line.length() > 3) {
                    if (readInt(line.mid(3), SV[1], 0, 1200))
                        outgoing.append("OK\r");
                    else
                        outgoing.append("ERROR\r");
                } else {
                    outgoing.append(QByteArray::number(SV[1]));
                    outgoing.append('\r');
                }
            } else if (line.startsWith("SVR")) {
                if (line.length() > 3) {
                    if (readInt(line.mid(3), SV[2], 0, 1200))
                        outgoing.append("OK\r");
                    else
                        outgoing.append("ERROR\r");
                } else {
                    outgoing.append(QByteArray::number(SV[2]));
                    outgoing.append('\r');
                }
            } else if (line.startsWith("SX")) {
                if (line.length() > 2) {
                    if (readInt(line.mid(2), Vx, 0, 5000))
                        outgoing.append("OK\r");
                    else
                        outgoing.append("ERROR\r");
                } else {
                    outgoing.append(QByteArray::number(Vx));
                    outgoing.append('\r');
                }
            } else if (line.startsWith("B")) {
                if (line.length() > 1) {
                    if (readInt(line.mid(1), B, 0, 255))
                        outgoing.append("OK\r");
                    else
                        outgoing.append("ERROR\r");
                } else {
                    outgoing.append(QByteArray::number(B));
                    outgoing.append('\r');
                }
            } else if (line.startsWith("H")) {
                if (line.length() > 1) {
                    if (readInt(line.mid(1), H, 0, 1))
                        outgoing.append("OK\r");
                    else
                        outgoing.append("ERROR\r");
                } else {
                    outgoing.append(QByteArray::number(H));
                    outgoing.append('\r');
                }
            } else if (line == "PU") {
                outgoing.append("OK\r");
            } else if (line.startsWith("V")) {
                if (line.length() > 1) {
                    if (line.at(1) == 'N') {
                        V = MovingToNormal;
                        valveRemainingTime = 3.0;
                        outgoing.append("OK\r");
                    } else if (line.at(1) == 'Z') {
                        V = MovingToZero;
                        valveRemainingTime = 3.0;
                        outgoing.append("OK\r");
                    } else {
                        outgoing.append("ERROR\r");
                    }
                } else {
                    switch (V) {
                    case ZeroValve:
                        outgoing.append("ZERO");
                        break;
                    case NormalValve:
                        outgoing.append("NORMAL");
                        break;
                    case MovingToZero:
                    case MovingToNormal:
                        V = FaultValve;
                        /* Fall through */
                    case FaultValve:
                        outgoing.append("FAULT");
                        break;
                    }
                    outgoing.append('\r');
                }
            } else if (line == "Z") {
                switch (mode) {
                case Sample:
                    outgoing.append("OK\r");
                    mode = ZeroStartBlank;
                    modeRemainingTime = STB;
                    V = MovingToZero;
                    break;
                default:
                    outgoing.append("ERROR\r");
                    break;
                }
            } else if (line == "RD") {
                outgoing.append(dRecord());
            } else if (line == "RY") {
                outgoing.append(yRecord());
            } else if (line == "RP") {
                outgoing.append(pRecord(0));
                outgoing.append(pRecord(1));
                outgoing.append(pRecord(2));
            } else if (line == "RPG") {
                outgoing.append(pRecord(1));
            } else if (line == "RT") {
                outgoing.append(tRecord());
            } else if (line == "RU") {
                if (enabledRecords & Record_T)
                    outgoing.append(tRecord());
                if (enabledRecords & Record_P) {
                    for (int color = 0; color < 3; ++color) {
                        outgoing.append(pRecord(color));
                    }
                }
                if (enabledRecords & Record_D)
                    outgoing.append(dRecord());
                if (enabledRecords & Record_Y)
                    outgoing.append(yRecord());
            } else if (line == "RV") {
                outgoing.append("Revision E, May 1996\r");
            } else if (line == "RY") {
                outgoing.append(yRecord());
            } else if (line == "RZ") {
                outgoing.append(zRecord());
            } else if (line == "UB") {
                outgoing.append("OK\r");
                unpolledRemaining = 0.0;
            } else if (line == "UD0") {
                enabledRecords &= ~Record_D;
                outgoing.append("OK\r");
            } else if (line == "UD1") {
                enabledRecords |= Record_D;
                outgoing.append("OK\r");
            } else if (line == "UP0") {
                enabledRecords &= ~Record_P;
                outgoing.append("OK\r");
            } else if (line == "UP3") {
                enabledRecords |= Record_P;
                outgoing.append("OK\r");
            } else if (line == "UT0") {
                enabledRecords &= ~Record_T;
                outgoing.append("OK\r");
            } else if (line == "UT1") {
                enabledRecords |= Record_T;
                outgoing.append("OK\r");
            } else if (line == "UY0") {
                enabledRecords &= ~Record_Y;
                outgoing.append("OK\r");
            } else if (line == "UY1") {
                enabledRecords |= Record_Y;
                outgoing.append("OK\r");
            } else if (line == "UZ1") {
                outgoing.append("OK\r");
            } else {
                outgoing.append("ERROR\r");
            }

            if (idxCR >= incoming.size() - 1) {
                incoming.clear();
                break;
            }
            incoming = incoming.mid(idxCR + 1);
        }

        time = time.addMSecs((qint64) ceil(seconds * 1000.0));

        modeRemainingTime -= seconds;
        if (modeRemainingTime < 0.0) {
            switch (mode) {
            case Sample:
                if (SMZ) {
                    mode = ZeroStartBlank;
                    modeRemainingTime = STB;
                    V = MovingToZero;
                    valveRemainingTime = 3.0;
                }
                break;
            case ZeroStartBlank:
                mode = ZeroRun;
                modeRemainingTime = STZ;
                break;
            case ZeroRun:
                mode = ZeroEndBlank;
                modeRemainingTime = STB;
                V = MovingToNormal;
                valveRemainingTime = 3.0;
                outgoing.append(zRecord());
                break;
            case ZeroEndBlank:
                mode = Sample;
                modeRemainingTime = STP;
                break;
            }
        }

        if (FP::defined(valveRemainingTime)) {
            valveRemainingTime -= seconds;
            if (valveRemainingTime < 0.0) {
                switch (V) {
                case ZeroValve:
                case NormalValve:
                case FaultValve:
                    valveRemainingTime = FP::undefined();
                    break;
                case MovingToZero:
                    valveRemainingTime = FP::undefined();
                    V = ZeroValve;
                    break;
                case MovingToNormal:
                    valveRemainingTime = FP::undefined();
                    V = NormalValve;
                    break;
                }
            }
        }

        if (FP::defined(unpolledRemaining)) {
            unpolledRemaining -= seconds;
            while (unpolledRemaining <= 0.0) {
                unpolledRemaining += STA;
                if (pendingRealtimeOutput == 0)
                    pendingRealtimeOutput |= enabledRecords;
            }
        }

        if (pendingRealtimeOutput & Record_T) {
            outgoing.append(tRecord());
            pendingRealtimeOutput &= ~Record_T;
            return;
        }
        if (pendingRealtimeOutput & Record_PB) {
            outgoing.append(pRecord(0));
            pendingRealtimeOutput &= ~Record_PB;
            return;
        }
        if (pendingRealtimeOutput & Record_PG) {
            outgoing.append(pRecord(1));
            pendingRealtimeOutput &= ~Record_PG;
            return;
        }
        if (pendingRealtimeOutput & Record_PR) {
            outgoing.append(pRecord(2));
            pendingRealtimeOutput &= ~Record_PR;
            return;
        }
        if (pendingRealtimeOutput & Record_D) {
            outgoing.append(dRecord());
            pendingRealtimeOutput &= ~Record_D;
            return;
        }
        if (pendingRealtimeOutput & Record_Y) {
            outgoing.append(yRecord());
            pendingRealtimeOutput &= ~Record_Y;
            return;
        }
        pendingRealtimeOutput = 0;
    }

    double frequencyTotal(double C, double width) const
    {
        return (360.0 * C * 22.994) / (width * Revs);
    }

    double frequencyBack(double C, double width) const
    {
        return (360.0 * C * 22.994) / (width * Revsb);
    }

    double correctCounts(double C, int color) const
    {
        C *= (C * (double) SK[color].K1 * 1E-12 + 1.0);
        return C;
    }

    double calcCs(int color) const
    {
        return correctCounts(frequencyTotal(Cs[color], 140.0), color);
    }

    double calcCbs(int color) const
    {
        return correctCounts(frequencyBack(Cbs[color], 140.0), color);
    }

    double calcCd(int color) const
    {
        return correctCounts(frequencyTotal(Cd[color], 60.0), color);
    }

    double calcCbd(int color) const
    {
        return correctCounts(frequencyBack(Cbd[color], 60.0), color);
    }

    double calcCf(int color) const
    {
        return correctCounts(frequencyTotal(Cf[color], 40.0), color);
    }

    static double roundExponent(double d, int digits = 3)
    {
        if (digits <= 0)
            return d;
        double m = fabs(d);
        if (m != 0.0) {
            int base = (int) floor(log10(m));
            base -= digits;
            m = pow(10.0, base);
            d = qRound(d / m) * m;
        }
        return d;
    }

    double calcBs(int color, int digits = 3) const
    {
        double dark = calcCd(color);
        double Bs = SK[color].K2 * ((calcCs(color) - dark) / (calcCf(color) - dark));
        Bs -= Bsw[color];
        Bs -= SK[color].K3 * (P / 1013.25) * (273.15 / T);
        Bs = roundExponent(Bs, digits);
        return Bs;
    }

    double calcBbs(int color, int digits = 3) const
    {
        double dark = calcCbd(color);
        double Bs = SK[color].K2 * ((calcCbs(color) - dark) / (calcCf(color) - dark));
        Bs -= Bsw[color] * SK[color].K4;
        Bs -= SK[color].K3 * (P / 1013.25) * (273.15 / T) * 0.5;
        Bs = roundExponent(Bs, digits);
        return Bs;
    }

    double calcBsw(int color, int digits = 3) const
    {
        double Bsr = SK[color].K3 * (P / 1013.25) * (273.15 / T);
        double Bs = Bsr + Bsw[color];
        Bs = roundExponent(Bs, digits);
        Bsr = roundExponent(Bsr, digits);
        return Bs - Bsr;
    }

    double calcBbsw(int color, int digits = 3) const
    {
        double Bsr = SK[color].K3 * (P / 1013.25) * (273.15 / T);
        double Bs = Bsr * 0.5 + Bbsw[color];
        Bs = roundExponent(Bs, digits);
        Bsr = roundExponent(Bsr, digits);
        return Bs - Bsr * 0.5;
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("F2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Tu", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("U", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Uu", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("P", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Vl", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Al", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("BsB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BsG", Variant::Root(550.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BsR", Variant::Root(700.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BbsB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BbsG", Variant::Root(550.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BbsR", Variant::Root(700.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BswB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BswG", Variant::Root(550.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BswR", Variant::Root(700.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BbswB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BbswG", Variant::Root(550.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BbswR", Variant::Root(700.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CdB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CdG", Variant::Root(550.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CdR", Variant::Root(700.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CbdB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CbdG", Variant::Root(550.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CbdR", Variant::Root(700.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CfB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CfG", Variant::Root(550.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CfR", Variant::Root(700.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CsB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CsG", Variant::Root(550.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CsR", Variant::Root(700.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CbsB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CbsG", Variant::Root(550.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CbsR", Variant::Root(700.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("Tw", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Pw", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZSPANCHECK", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCTcB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("PCTcG", Variant::Root(550.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("PCTcR", Variant::Root(700.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("PCTbcB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("PCTbcG", Variant::Root(550.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("PCTbcR", Variant::Root(700.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CcB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CcG", Variant::Root(550.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CcR", Variant::Root(700.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CbcB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CbcG", Variant::Root(550.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("CbcR", Variant::Root(700.0), "^Wavelength", time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZBsB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("ZBsG", Variant::Root(550.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("ZBsR", Variant::Root(700.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("ZBbsB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("ZBbsG", Variant::Root(550.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("ZBbsR", Variant::Root(700.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("ZSTATE", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("BswdB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BswdG", Variant::Root(550.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BswdR", Variant::Root(700.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BbswdB", Variant::Root(450.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BbswdG", Variant::Root(550.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BbswdR", Variant::Root(700.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("ZRTIME", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZPARAMETERS", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("T"))
            return false;
        if (!stream.checkContiguous("Tu"))
            return false;
        if (!stream.checkContiguous("U"))
            return false;
        if (!stream.checkContiguous("Uu"))
            return false;
        if (!stream.checkContiguous("P"))
            return false;
        if (!stream.checkContiguous("Vl"))
            return false;
        if (!stream.checkContiguous("Al"))
            return false;
        if (!stream.checkContiguous("BsB"))
            return false;
        if (!stream.checkContiguous("BsG"))
            return false;
        if (!stream.checkContiguous("BsR"))
            return false;
        if (!stream.checkContiguous("BbsB"))
            return false;
        if (!stream.checkContiguous("BbsG"))
            return false;
        if (!stream.checkContiguous("BbsR"))
            return false;
        if (!stream.checkContiguous("CdB"))
            return false;
        if (!stream.checkContiguous("CdG"))
            return false;
        if (!stream.checkContiguous("CdR"))
            return false;
        if (!stream.checkContiguous("CbdB"))
            return false;
        if (!stream.checkContiguous("CbdG"))
            return false;
        if (!stream.checkContiguous("CbdR"))
            return false;
        if (!stream.checkContiguous("CfB"))
            return false;
        if (!stream.checkContiguous("CfG"))
            return false;
        if (!stream.checkContiguous("CfR"))
            return false;
        if (!stream.checkContiguous("CsB"))
            return false;
        if (!stream.checkContiguous("CsG"))
            return false;
        if (!stream.checkContiguous("CsR"))
            return false;
        if (!stream.checkContiguous("CbsB"))
            return false;
        if (!stream.checkContiguous("CbsG"))
            return false;
        if (!stream.checkContiguous("CbsR"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream, const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("T", Variant::Root(model.T - 273.15), time))
            return false;
        if (!stream.hasAnyMatchingValue("Tu", Variant::Root(model.Tu - 273.15), time))
            return false;
        if (!stream.hasAnyMatchingValue("U", Variant::Root(model.U), time))
            return false;
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.P), time))
            return false;
        if (!stream.hasAnyMatchingValue("Vl", Variant::Root(model.Vl), time))
            return false;
        if (!stream.hasAnyMatchingValue("Al", Variant::Root(model.Al), time))
            return false;
        if (!stream.hasAnyMatchingValue("BsB", Variant::Root(model.calcBs(0) * 1E6), time))
            return false;
        if (!stream.hasAnyMatchingValue("BsG", Variant::Root(model.calcBs(1) * 1E6), time))
            return false;
        if (!stream.hasAnyMatchingValue("BsR", Variant::Root(model.calcBs(2) * 1E6), time))
            return false;
        if (!stream.hasAnyMatchingValue("BbsB", Variant::Root(model.calcBbs(0) * 1E6), time))
            return false;
        if (!stream.hasAnyMatchingValue("BbsG", Variant::Root(model.calcBbs(1) * 1E6), time))
            return false;
        if (!stream.hasAnyMatchingValue("BbsR", Variant::Root(model.calcBbs(2) * 1E6), time))
            return false;
        if (!stream.hasAnyMatchingValue("CdB", Variant::Root(model.calcCd(0)), time))
            return false;
        if (!stream.hasAnyMatchingValue("CdG", Variant::Root(model.calcCd(1)), time))
            return false;
        if (!stream.hasAnyMatchingValue("CdR", Variant::Root(model.calcCd(2)), time))
            return false;
        if (!stream.hasAnyMatchingValue("CbdB", Variant::Root(model.calcCbd(0)), time))
            return false;
        if (!stream.hasAnyMatchingValue("CbdG", Variant::Root(model.calcCbd(1)), time))
            return false;
        if (!stream.hasAnyMatchingValue("CbdR", Variant::Root(model.calcCbd(2)), time))
            return false;
        if (!stream.hasAnyMatchingValue("CfB", Variant::Root(model.calcCf(0)), time))
            return false;
        if (!stream.hasAnyMatchingValue("CfG", Variant::Root(model.calcCf(1)), time))
            return false;
        if (!stream.hasAnyMatchingValue("CfR", Variant::Root(model.calcCf(2)), time))
            return false;
        if (!stream.hasAnyMatchingValue("CsB", Variant::Root(model.calcCs(0)), time))
            return false;
        if (!stream.hasAnyMatchingValue("CsG", Variant::Root(model.calcCs(1)), time))
            return false;
        if (!stream.hasAnyMatchingValue("CsR", Variant::Root(model.calcCs(2)), time))
            return false;
        if (!stream.hasAnyMatchingValue("CbsB", Variant::Root(model.calcCbs(0)), time))
            return false;
        if (!stream.hasAnyMatchingValue("CbsG", Variant::Root(model.calcCbs(1)), time))
            return false;
        if (!stream.hasAnyMatchingValue("CbsR", Variant::Root(model.calcCbs(2)), time))
            return false;
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("ZBsB", Variant::Root(model.calcBs(0) * 1E6), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBsG", Variant::Root(model.calcBs(1) * 1E6), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBsR", Variant::Root(model.calcBs(2) * 1E6), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBbsB", Variant::Root(model.calcBbs(0) * 1E6), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBbsG", Variant::Root(model.calcBbs(1) * 1E6), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBbsR", Variant::Root(model.calcBbs(2) * 1E6), time))
            return false;
        if (!stream.hasAnyMatchingValue("Vx", Variant::Root(model.Vx / 1000.0), time))
            return false;
        return true;
    }

    bool checkStreamSC(const Variant::Read &value,
                       const ModelInstrument::AnalogCal &cal,
                       const QString &name)
    {
        auto check = value.hash(name);
        if (check["Low/ADC"].toInt64() != cal.alow)
            return false;
        if ((int) (check["Low/Value"].toDouble() * 10) != cal.vlow)
            return false;
        if (check["High/ADC"].toInt64() != cal.ahigh)
            return false;
        if ((int) (check["High/Value"].toDouble() * 10) != cal.vhigh)
            return false;
        return true;
    }

    bool checkStreamSK(const Variant::Read &value,
                       const ModelInstrument::ChannelCal &cal,
                       const QString &name)
    {
        auto check = value.hash(name);
        if (check["K1"].toInt64() != cal.K1)
            return false;
        if (!qFuzzyCompare(check["K2"].toDouble(), cal.K2))
            return false;
        if (!qFuzzyCompare(check["K3"].toDouble(), cal.K3))
            return false;
        if (!qFuzzyCompare(check["K4"].toDouble(), cal.K4))
            return false;
        return true;
    }

    bool checkParameters(StreamCapture &stream, const ModelInstrument &model,
                         double time = FP::undefined())
    {
        auto params = stream.getLatestValue("ZPARAMETERS", time);
        if (!params.exists())
            return false;
        if (!checkStreamSC(params, model.SCI, "SCI"))
            return false;
        if (!checkStreamSC(params, model.SCP, "SCP"))
            return false;
        if (!checkStreamSC(params, model.SCR, "SCR"))
            return false;
        if (!checkStreamSC(params, model.SCS, "SCS"))
            return false;
        if (!checkStreamSK(params, model.SK[0], "SKB"))
            return false;
        if (!checkStreamSK(params, model.SK[1], "SKG"))
            return false;
        if (!checkStreamSK(params, model.SK[2], "SKR"))
            return false;
        if (params["SL"].toQString().toUtf8() != model.SL)
            return false;
        if (params["SMB"].toBool() != !!model.SMB)
            return false;
        if (params["SP"].toInt64() != model.SP)
            return false;
        if (params["STA"].toInt64() != model.STA)
            return false;
        if (params["STB"].toInt64() != model.STB)
            return false;
        if (params["STP"].toInt64() != model.STP)
            return false;
        if (params["STZ"].toInt64() != model.STZ)
            return false;
        if (params["SVB"].toInt64() != model.SV[0])
            return false;
        if (params["SVG"].toInt64() != model.SV[1])
            return false;
        if (params["SVR"].toInt64() != model.SV[2])
            return false;
        if (params["B"].toInteger() != model.B)
            return false;
        if (params["H"].toBool() != !!model.H)
            return false;
        return true;
    }

    bool checkZero(StreamCapture &stream, const ModelInstrument &model,
                   double time = FP::undefined(),
                   bool checkSampleConditions = false)
    {
        if (!stream.hasAnyMatchingValue("BswB", Variant::Root(model.calcBsw(0) * 1E6), time))
            return false;
        if (!stream.hasAnyMatchingValue("BswG", Variant::Root(model.calcBsw(1) * 1E6), time))
            return false;
        if (!stream.hasAnyMatchingValue("BswR", Variant::Root(model.calcBsw(2) * 1E6), time))
            return false;
        if (!stream.hasAnyMatchingValue("BbswB", Variant::Root(model.calcBbsw(0) * 1E6), time))
            return false;
        if (!stream.hasAnyMatchingValue("BbswG", Variant::Root(model.calcBbsw(1) * 1E6), time))
            return false;
        if (!stream.hasAnyMatchingValue("BbswR", Variant::Root(model.calcBbsw(2) * 1E6), time))
            return false;
        if (checkSampleConditions) {
            if (!stream.hasAnyMatchingValue("Tw", Variant::Root(model.T - 273.15), time))
                return false;
            if (!stream.hasAnyMatchingValue("Pw", Variant::Root(model.P), time))
                return false;
        }
        return true;
    }

    bool checkSpancheck(const SequenceValue::Transfer &values)
    {
        if (!StreamCapture::findValue(values, "raw", "ZSPANCHECK", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "PCTcB", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "PCTcG", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "PCTcR", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "PCTbcB", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "PCTbcG", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "PCTbcR", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "CcB", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "CcG", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "CcR", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "CbcB", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "CbcG", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "CbcR", Variant::Root()))
            return false;
        return true;
    }

    bool checkRealtimeSpancheck(const SequenceValue::Transfer &values)
    {
        if (!StreamCapture::findValue(values, "raw", "ZSPANCHECKk2B", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "ZSPANCHECKk2G", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "ZSPANCHECKk2R", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "ZSPANCHECKk4B", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "ZSPANCHECKk4G", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "ZSPANCHECKk4R", Variant::Root()))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_tsi_neph3563"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_tsi_neph3563"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["StrictMode"].setBool(true);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(0.1);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(0.1);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(StreamCapture::findValue(persistentValues, "raw", "F2", Variant::Root("NBXX")));

    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.125);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 40; i++) {
            control.advance(0.1);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
        QVERIFY(checkZero(persistent, instrument));
        QVERIFY(checkZero(realtime, instrument));

        QVERIFY(checkParameters(realtime, instrument));
        QVERIFY(checkParameters(persistent, instrument));

        QVERIFY(StreamCapture::findValue(persistentValues, "raw", "F2", Variant::Root("NBXX")));

        QVERIFY(logging.hasMeta("BsB", Variant::Root("Some Cal"), "^Source/CalibrationLabel"));
        QVERIFY(realtime.hasMeta("BsB", Variant::Root("Some Cal"), "^Source/CalibrationLabel"));

    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.STB = 10;
        instrument.STZ = 30;

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        control.advance(0.1);

        double zeroIssueTime = control.time();
        instrument.modeRemainingTime = 1;
        instrument.Bsw[1] = 0.05 * 1E-6;
        for (int i = 0; i < 20; i++) {
            control.advance(0.1);
        }

        while (control.time() < 120.0) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Zero"), zeroIssueTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        double inZeroTime = control.time();

        while (control.time() < 120.0) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), inZeroTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        for (int i = 0; i < 50; i++) {
            control.advance(0.1);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
        QVERIFY(checkZero(persistent, instrument, inZeroTime, true));
        QVERIFY(checkZero(realtime, instrument, inZeroTime, true));

        QVERIFY(persistent.hasAnyMatchingValue("F2", Variant::Root("NBXX")));
        QVERIFY(persistent.hasAnyMatchingValue("F2", Variant::Root("BBXX"), zeroIssueTime));
        QVERIFY(persistent.hasAnyMatchingValue("F2", Variant::Root("ZBXX"), zeroIssueTime));
        QVERIFY(persistent.hasAnyMatchingValue("F2", Variant::Root("BBXX"), inZeroTime));
        QVERIFY(StreamCapture::findValue(persistentValues, "raw", "F2", Variant::Root("NBXX")));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Parameters/STZ"].setInt64(30);
        cv["Parameters/STB"].setInt64(15);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        QVERIFY(instrument.V == ModelInstrument::NormalValve);

        QVERIFY(checkZero(persistent, instrument));
        QVERIFY(checkZero(realtime, instrument));

        Variant::Write cmd = Variant::Write::empty();
        cmd["StartZero"].setBool(true);
        double zeroIssueTime = control.time();
        interface->incomingCommand(cmd);
        instrument.Bsw[1] = 0.05 * 1E-6;
        control.advance(0.1);
        control.advance(0.1);

        while (control.time() < 120.0) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Zero"), zeroIssueTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        QVERIFY(instrument.V == ModelInstrument::ZeroValve ||
                        instrument.V == ModelInstrument::MovingToZero);
        double inZeroTime = control.time();

        while (control.time() < 120.0) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), inZeroTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        QVERIFY(instrument.V == ModelInstrument::NormalValve ||
                        instrument.V == ModelInstrument::MovingToNormal);

        double commandTime = control.time() + 10.0;
        cmd.setEmpty();
        cmd["SetParameters/Parameters/B/Value"].setInt64(100);
        interface->incomingCommand(cmd);
        while (control.time() < 180.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), commandTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);

        instrument.Al = 7.1;
        instrument.Vl = 10;
        double limitTime = control.time() + 10.0;
        while (control.time() < 240.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), limitTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
        QVERIFY(checkZero(persistent, instrument, inZeroTime, true));
        QVERIFY(checkZero(realtime, instrument, inZeroTime, true));

        QVERIFY(checkParameters(realtime, instrument));
        QVERIFY(checkParameters(persistent, instrument));

        QVERIFY(persistent.hasAnyMatchingValue("F2", Variant::Root("NBXX")));
        QVERIFY(persistent.hasAnyMatchingValue("F2", Variant::Root("BBXX"), zeroIssueTime));
        QVERIFY(persistent.hasAnyMatchingValue("F2", Variant::Root("ZBXX"), zeroIssueTime));
        QVERIFY(persistent.hasAnyMatchingValue("F2", Variant::Root("BBXX"), inZeroTime));
        QVERIFY(StreamCapture::findValue(persistentValues, "raw", "F2", Variant::Root("NBXX")));

        QCOMPARE(instrument.STZ, 30);
        QCOMPARE(instrument.STB, 15);
        QCOMPARE(instrument.B, 100);
        QCOMPARE(instrument.SP, 63);

        QVERIFY(logging.hasMeta("BsB", Variant::Root("Some Cal"), "^Source/CalibrationLabel"));
        QVERIFY(realtime.hasMeta("BsB", Variant::Root("Some Cal"), "^Source/CalibrationLabel"));

    }

    void interactiveSpancheck()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Spancheck/Gas/MinimumSample"].setDouble(60.0);
        cv["Spancheck/Air/MinimumSample"].setDouble(60.0);
        cv["Spancheck/Gas/Flush"].setDouble(10.0);
        cv["Spancheck/Air/Flush"].setDouble(10.0);
        cv["Parameters/STZ"].setInt64(30);
        cv["Parameters/STB"].setInt64(15);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));
        QVERIFY(instrument.V == ModelInstrument::NormalValve);

        while (control.time() < 60.0) {
            if (logging.hasAnyMatchingValue("BsB", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.5);
            QTest::qSleep(50);
        }
        while (control.time() < 60.0) {
            if (logging.hasAnyMatchingValue("F1", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.5);
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        QVERIFY(logging.hasAnyMatchingValue("BsB", Variant::Root(), FP::undefined()));

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkZero(persistent, instrument));
        QVERIFY(checkZero(realtime, instrument));

        QVERIFY(checkParameters(realtime, instrument));

        double checkTime = control.time();

        Variant::Write cmd = Variant::Write::empty();
        cmd["StartSpancheck"].setBool(true);
        interface->incomingCommand(cmd);

        while (control.time() < 240.0) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Spancheck"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Spancheck"), checkTime));
        QVERIFY(realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("GasAirFlush"),
                                             checkTime, "Current"));
        QCOMPARE(instrument.Vx, 0);
        QVERIFY(instrument.V == ModelInstrument::ZeroValve ||
                        instrument.V == ModelInstrument::MovingToZero);

        checkTime = control.time();
        while (control.time() < 360.0) {
            if (realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("AirSample"),
                                             checkTime, "Current"))
                break;
            control.advance(0.5);
            QTest::qSleep(50);
        }
        for (int color = 0; color < 3; ++color) {
            instrument.Cs[color] -= 100;
            instrument.Cbs[color] -= 100;
        }
        while (control.time() < 360.0) {
            if (realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("Inactive"),
                                             checkTime, "Current"))
                break;
            control.advance(0.5);
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 360.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("Inactive"),
                                             checkTime, "Current"));

        while (control.time() < 420.0) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Zero"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 420.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Zero"), checkTime));
        QCOMPARE(instrument.Vx, 0);
        QVERIFY(instrument.V == ModelInstrument::ZeroValve ||
                        instrument.V == ModelInstrument::MovingToZero);

        checkTime = control.time() + 10.0;
        cmd.setEmpty();
        cmd["ApplySpancheckCalibration"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 540.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 540.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(!checkSpancheck(persistent.values()));
        QVERIFY(checkSpancheck(persistentValues));
        QVERIFY(checkSpancheck(realtime.values()));
        QVERIFY(checkRealtimeSpancheck(realtime.values()));

        QVERIFY(checkParameters(realtime, instrument));

        QVERIFY(logging.hasMeta("BsB", Variant::Root("Some Cal"), "^Source/CalibrationLabel"));
        QVERIFY(realtime.hasMeta("BsB", Variant::Root("Some Cal"), "^Source/CalibrationLabel"));

        auto scResults = realtime.getLatestValue("ZSPANCHECK");
        QCOMPARE(ModelInstrument::roundExponent(scResults["B/K2"].toDouble()), instrument.SK[0].K2);
        QCOMPARE(ModelInstrument::roundExponent(scResults["G/K2"].toDouble()), instrument.SK[1].K2);
        QCOMPARE(ModelInstrument::roundExponent(scResults["R/K2"].toDouble()), instrument.SK[2].K2);
        QCOMPARE(ModelInstrument::roundDecimalDigits(scResults["B/K4"].toDouble(), 3),
                 instrument.SK[0].K4);
        QCOMPARE(ModelInstrument::roundDecimalDigits(scResults["G/K4"].toDouble(), 3),
                 instrument.SK[1].K4);
        QCOMPARE(ModelInstrument::roundDecimalDigits(scResults["R/K4"].toDouble(), 3),
                 instrument.SK[2].K4);

        auto activeParams = realtime.getLatestValue("ZPARAMETERS");
        QCOMPARE(activeParams["V"].toString(), std::string("Automatic"));

    }

    void interactiveValve()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["DisableSpancheckValve"].setBool(true);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));
        QVERIFY(instrument.V == ModelInstrument::NormalValve);

        while (control.time() < 60.0) {
            if (logging.hasAnyMatchingValue("BsB", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.5);
            QTest::qSleep(50);
        }
        while (control.time() < 60.0) {
            if (logging.hasAnyMatchingValue("F1", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.5);
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        QVERIFY(logging.hasAnyMatchingValue("BsB", Variant::Root(), FP::undefined()));


        Variant::Root cmd;
        cmd["SetAnalog/Parameters/Value"].setDouble(5.0);
        interface->incomingCommand(cmd);

        while (control.time() < 240.0) {
            control.advance(0.5);
            if (instrument.Vx != 0)
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        QCOMPARE(instrument.Vx, 5000);

        cmd["SetAnalog/Parameters/Value"].setBool(false);
        interface->incomingCommand(cmd);

        while (control.time() < 240.0) {
            control.advance(0.5);
            if (instrument.Vx == 0)
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        QCOMPARE(instrument.Vx, 0);

        cmd["SetAnalog"].setBool(true);
        interface->incomingCommand(cmd);

        while (control.time() < 240.0) {
            control.advance(0.5);
            if (instrument.Vx != 0)
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        QCOMPARE(instrument.Vx, 5000);


        interface->initiateShutdown();
        interface->wait();
        interface.reset();
    }

    void interactiveParameterReset()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Parameters/STZ"].setInt64(30);
        cv["Parameters/STB"].setInt64(15);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        double commandTime = control.time() + 10.0;
        Variant::Write cmd = Variant::Write::empty();
        cmd["SetParameters/Parameters/B/Value"].setInt64(100);
        interface->incomingCommand(cmd);
        while (control.time() < 120.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), commandTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        QCOMPARE(instrument.STZ, 30);
        QCOMPARE(instrument.STB, 15);
        QCOMPARE(instrument.B, 100);

        instrument.unpolledRemaining = FP::undefined();
        instrument.STZ = 120;
        instrument.STB = 60;
        instrument.B = 0;
        instrument.SL = "NewLabel";

        double restartTime = control.time();
        commandTime = control.time() + 10.0;
        control.advance(5.0);
        while (control.time() < 180.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), restartTime))
                break;
            QTest::qSleep(50);
        }
        while (control.time() < 180.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), commandTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);

        QCOMPARE(instrument.STZ, 30);
        QCOMPARE(instrument.STB, 15);
        QCOMPARE(instrument.B, 255);

        commandTime = control.time() + 10.0;
        cmd.setEmpty();
        cmd["SetParameters/Parameters/B/Value"].setInt64(100);
        interface->incomingCommand(cmd);
        while (control.time() < 240.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), commandTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        QCOMPARE(instrument.STZ, 30);
        QCOMPARE(instrument.STB, 15);
        QCOMPARE(instrument.B, 100);

        instrument.unpolledRemaining = FP::undefined();
        instrument.STZ = 120;
        instrument.STB = 60;
        instrument.B = 0;

        restartTime = control.time();
        commandTime = control.time() + 10.0;
        control.advance(5.0);
        while (control.time() < 300.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), restartTime))
                break;
            QTest::qSleep(50);
        }
        while (control.time() < 300.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), commandTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 300.0);

        QCOMPARE(instrument.STZ, 30);
        QCOMPARE(instrument.STB, 15);
        QCOMPARE(instrument.B, 100);

        interface->initiateShutdown();
        interface->wait();
        interface.reset();
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
