/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIRETSINEPH3563_H
#define ACQUIRETSINEPH3563_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "acquisition/spancheck.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"
#include "smoothing/baseline.hxx"

class AcquireTSINeph3563 : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Passive autoprobe initialization, ignorging the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,
        /* Acquiring data in interactive mode, but only reading unpolled data */
        RESP_UNPOLLED_RUN,

        /* Waiting for the completion of the "UE" command during start
         * communications as well as flushing out all remaining data reports */
                RESP_INTERACTIVE_START_STOPREPORTS,
        /* UE confirm failed, so attempt a PU and try again */
                RESP_INTERACTIVE_START_STOPREPORTS_POWERUP,

        /* Set comma delimiter, SD0 */
                RESP_INTERACTIVE_START_SETDELIMITER,
        /* Set manual analog output, SB0,0 */
                RESP_INTERACTIVE_START_SETANALOGOUT,
        /* Set analog output value to zero, SX0 */
                RESP_INTERACTIVE_START_SETANALOGVALUE,
        /* Read firmware version, RV */
                RESP_INTERACTIVE_START_READFIRMWAREVERSION,
        /* Enabling unpolled record types */
                RESP_INTERACTIVE_START_ENABLERECORDS_T,
        RESP_INTERACTIVE_START_ENABLERECORDS_D,
        RESP_INTERACTIVE_START_ENABLERECORDS_Y,
        RESP_INTERACTIVE_START_ENABLERECORDS_P,
        RESP_INTERACTIVE_START_ENABLERECORDS_Z,
        /* Set neph internal RTC, ST... */
                RESP_INTERACTIVE_START_SETTIME,
        /* Reading back all parameters that are currently set */
                RESP_INTERACTIVE_START_PARAMETERS_READ,
        /* Writing all parameters that need to be changed */
                RESP_INTERACTIVE_START_PARAMETERS_WRITE,
        /* Read initial records */
                RESP_INTERACTIVE_START_RECORD_Y,
        RESP_INTERACTIVE_START_RECORD_D,
        RESP_INTERACTIVE_START_RECORD_Z,
        /* Waiting for the first valid record */
                RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID,


        /* Waiting for the completion of the "UE" command so an update to
         * commands or parameters can be performed; all data is being
         * flushed out */
                RESP_INTERACTIVE_UPDATE_STOPREPORTS,
        /* Writing all parameters that need to be changed */
                RESP_INTERACTIVE_UPDATE_PARAMETERS_WRITE,
        /* Issuing commands */
                RESP_INTERACTIVE_UPDATE_COMMANDS,

        /* Waiting before attempting a communications restart */
                RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
                RESP_INTERACTIVE_INITIALIZE,
    };
    ResponseState responseState;

    enum {
        /* Normal operation. */
                SAMPLE_RUN,

        /* Zero in progress */
                SAMPLE_ZERO_RUN,

        /* In spancheck state, under the control of the spancheck controller. */
                SAMPLE_SPANCHECK,
    } sampleState;

    class SpancheckInterface
            : public CPD3::Acquisition::NephelometerSpancheckController::Interface {
        AcquireTSINeph3563 *parent;
    public:
        SpancheckInterface(AcquireTSINeph3563 *neph);

        virtual ~SpancheckInterface();

        void setBypass(bool enable) override;

        void switchToFilteredAir() override;

        void switchToGas(CPD3::Algorithms::Rayleigh::Gas gas) override;

        void issueZero() override;

        void issueCommand(const QString &target, const CPD3::Data::Variant::Read &command) override;

        bool start() override;

        void completed() override;

        void aborted() override;
    };

    SpancheckInterface spancheckInterface;

    enum LogStreamID {
        LogStream_Metadata = 0,
        LogStream_State,

        /* These must be in the order the neph reports them */
                LogStream_PhotonBlue,
        LogStream_PhotonGreen,
        LogStream_PhotonRed,
        LogStream_Data,
        LogStream_Auxiliary,

        LogStream_TOTAL,
        LogStream_BEGINORDER = LogStream_PhotonBlue
    };
    CPD3::Data::StaticMultiplexer loggingMux;
    quint32 streamAge[LogStream_TOTAL];
    double streamTime[LogStream_TOTAL];

    class GenericParameter {
    public:
        CPD3::Util::ByteArray parameter;

        GenericParameter();

        virtual ~GenericParameter();

        virtual void fromValue(const CPD3::Data::Variant::Read &value) = 0;

        virtual CPD3::Util::ByteArray setCommand(const GenericParameter *base) const = 0;

        virtual CPD3::Util::ByteArray getCommand() const;

        virtual bool readAfterSet(const GenericParameter *base);

        virtual bool fromResponse(const CPD3::Util::ByteArray &response) = 0;

        virtual bool equals(const GenericParameter *other) const = 0;

        virtual void copy(const GenericParameter *other) = 0;

        virtual CPD3::Data::Variant::Root toValue() const = 0;

        virtual CPD3::Data::Variant::Root metadata() const = 0;

        virtual void commandParameter(CPD3::Data::Variant::Write &target) const = 0;

        virtual void fromCommand(const CPD3::Data::Variant::Read &parameters) = 0;
    };

    class AnalogCalibration : public GenericParameter {
    public:
        qint64 adcLow;
        double valueLow;
        qint64 adcHigh;
        double valueHigh;

        AnalogCalibration();

        virtual ~AnalogCalibration();

        bool equals(const GenericParameter *other) const override;

        void copy(const GenericParameter *other) override;

        void fromValue(const CPD3::Data::Variant::Read &value) override;

        CPD3::Util::ByteArray setCommand(const GenericParameter *base) const override;

        bool fromResponse(const CPD3::Util::ByteArray &response) override;

        CPD3::Data::Variant::Root toValue() const override;

        CPD3::Data::Variant::Root metadata() const override;

        void commandParameter(CPD3::Data::Variant::Write &target) const override;

        void fromCommand(const CPD3::Data::Variant::Read &parameters) override;
    };

    class ChannelCalibration : public GenericParameter {
    public:
        qint64 K1;
        double K2;
        double K3;
        double K4;

        ChannelCalibration();

        virtual ~ChannelCalibration();

        bool equals(const GenericParameter *other) const override;

        void copy(const GenericParameter *other) override;

        void fromValue(const CPD3::Data::Variant::Read &value) override;

        CPD3::Util::ByteArray setCommand(const GenericParameter *base) const override;

        bool fromResponse(const CPD3::Util::ByteArray &response) override;

        CPD3::Data::Variant::Root toValue() const override;

        CPD3::Data::Variant::Root metadata() const override;

        void commandParameter(CPD3::Data::Variant::Write &target) const override;

        void fromCommand(const CPD3::Data::Variant::Read &parameters) override;
    };

    class StringParameter : public GenericParameter {
    public:
        std::string value;

        StringParameter();

        virtual ~StringParameter();

        bool equals(const GenericParameter *other) const override;

        void copy(const GenericParameter *other) override;

        void fromValue(const CPD3::Data::Variant::Read &value) override;

        CPD3::Util::ByteArray setCommand(const GenericParameter *base) const override;

        bool fromResponse(const CPD3::Util::ByteArray &response) override;

        CPD3::Data::Variant::Root toValue() const override;

        CPD3::Data::Variant::Root metadata() const override;

        void commandParameter(CPD3::Data::Variant::Write &target) const override;

        void fromCommand(const CPD3::Data::Variant::Read &parameters) override;
    };

    class BooleanParameter : public GenericParameter {
    public:
        qint64 value;

        BooleanParameter();

        virtual ~BooleanParameter();

        bool equals(const GenericParameter *other) const override;

        void copy(const GenericParameter *other) override;

        void fromValue(const CPD3::Data::Variant::Read &value) override;

        CPD3::Util::ByteArray setCommand(const GenericParameter *base) const override;

        bool fromResponse(const CPD3::Util::ByteArray &response) override;

        CPD3::Data::Variant::Root toValue() const override;

        CPD3::Data::Variant::Root metadata() const override;

        void commandParameter(CPD3::Data::Variant::Write &target) const override;

        void fromCommand(const CPD3::Data::Variant::Read &parameters) override;
    };

    class IntegerParameter : public GenericParameter {
    public:
        qint64 value;
        qint64 minimum;
        qint64 maximum;
        bool explicitZero;

        IntegerParameter();

        virtual ~IntegerParameter();

        bool equals(const GenericParameter *other) const override;

        void copy(const GenericParameter *other) override;

        void fromValue(const CPD3::Data::Variant::Read &value) override;

        CPD3::Util::ByteArray setCommand(const GenericParameter *base) const override;

        bool fromResponse(const CPD3::Util::ByteArray &response) override;

        CPD3::Data::Variant::Root toValue() const override;

        CPD3::Data::Variant::Root metadata() const override;

        void commandParameter(CPD3::Data::Variant::Write &target) const override;

        void fromCommand(const CPD3::Data::Variant::Read &parameters) override;
    };

    class ZeroValveParameter : public GenericParameter {
    public:
        qint64 value;
        bool automatic;

        ZeroValveParameter();

        virtual ~ZeroValveParameter();

        bool equals(const GenericParameter *other) const override;

        void copy(const GenericParameter *other) override;

        void fromValue(const CPD3::Data::Variant::Read &value) override;

        CPD3::Util::ByteArray setCommand(const GenericParameter *base) const override;

        bool fromResponse(const CPD3::Util::ByteArray &response) override;

        bool readAfterSet(const GenericParameter *base) override;

        CPD3::Data::Variant::Root toValue() const override;

        CPD3::Data::Variant::Root metadata() const override;

        void commandParameter(CPD3::Data::Variant::Write &target) const override;

        void fromCommand(const CPD3::Data::Variant::Read &parameters) override;
    };

    friend class Parameters;

    class Parameters {
        void create();

    public:
        std::unordered_map<std::string, std::unique_ptr<GenericParameter>> parameters;

        void setDefaults();

        Parameters();

        ~Parameters();

        Parameters(const Parameters &other);

        Parameters &operator=(const Parameters &other);

        Parameters(Parameters &&other);

        Parameters &operator=(Parameters &&other);

        bool operator==(const Parameters &other) const;

        inline bool operator!=(const Parameters &other) const
        { return !(*this == other); }
    };

    friend class Configuration;

    class Configuration {
        double start;
        double end;
    public:
        double wavelengths[3];
        bool strictMode;

        Parameters parameters;

        double reportInterval;
        double maximumLampCurrent;
        bool disableSpancheckValve;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    double rayleighTotal[3];
    double rayleighBack[3];

    Parameters activeParameters;
    Parameters targetParameters;
    std::deque<CPD3::Util::ByteArray> okCommandQueue;
    int parameterIndex;
    int commandTry;
    std::string lastSeenLabel;

    double lampLimitTime;

    friend class SpancheckInterface;

    CPD3::Acquisition::NephelometerSpancheckController *spancheckController;

    /* State that needs to be saved */
    CPD3::Data::SequenceValue spancheckDetails;

    CPD3::Data::Variant::Root instrumentMeta;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool realtimeStateUpdated;
    bool realtimeParametersUpdated;
    bool persistentParametersUpdated;

    CPD3::Data::Variant::Flags reportedFlags;
    CPD3::Data::Variant::Flags reportedModeFlags;
    CPD3::Data::Variant::Flags lastOutputFlags;
    CPD3::Data::SequenceValue lastMode;
    double modeUpdateTime;

    double zeroEffectiveTime;
    CPD3::Data::Variant::Root Tw;
    CPD3::Data::Variant::Root Pw;
    CPD3::Data::Variant::Root Bsw[3];
    CPD3::Data::Variant::Root Bbsw[3];
    CPD3::Data::Variant::Root Bswd[3];
    CPD3::Data::Variant::Root Bbswd[3];
    CPD3::Data::Variant::Root Bsr[3];
    bool zeroPersistentUpdated;
    bool zeroRealtimeUpdated;
    int zeroTCount;
    double zeroTSum;
    int zeroPCount;
    double zeroPSum;


    void setDefaultInvalid();

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value,
                  int streamID);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    void invalidateLogValues(double frameTime);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void handleZeroUpdate(double currentTime);

    void outputUpdated(double frameTime);

    double streamAdvance(double frameTime, LogStreamID streamID);

    bool streamValid(LogStreamID streamID, LogStreamID excludeLaterID = (LogStreamID) -1);

    void calculateZeroChannel(double frameTime,
                              double bs,
                              double bsr,
                              double bsrScale,
                              double cbsr,
                              double t,
                              double p,
                              CPD3::Data::Variant::Root &output,
                              CPD3::Data::Variant::Root &difference);

    bool zeroAccumulationActive() const;

    bool logScatteringsInvalid() const;

    int processRecord(const CPD3::Util::ByteArray &line, double &frameTime);

    void startOkCommand(const CPD3::Util::ByteView &frame,
                        double frameTime,
                        const CPD3::Util::ByteView &nextCommand,
                        ResponseState nextState,
                        const QString &nextStateName);

    inline void startOkCommand(const CPD3::Util::ByteView &frame,
                               double frameTime,
                               const char *nextCommand,
                               ResponseState nextState,
                               const QString &nextStateName)
    {
        return startOkCommand(frame, frameTime, CPD3::Util::ByteView(nextCommand), nextState,
                              nextStateName);
    }

    void startReadParameters(const CPD3::Util::ByteArray &frame, double frameTime);

    int parameterUpdateHandler(const CPD3::Util::ByteArray &frame, double frameTime);

    void startWriteParameters(const CPD3::Util::ByteArray &frame, double frameTime);

    void updateWriteParameters(const CPD3::Util::ByteArray &frame, double frameTime);

    void updateSendNextCommand(double frameTime);

    void updateCommandResponse(const CPD3::Util::ByteArray &frame, double frameTime);

    void configAdvance(double frameTime);

    void configurationChanged();

    CPD3::Data::SequenceValue::Transfer constructSpancheckVariables();

    CPD3::Data::SequenceValue::Transfer constructRealtimeSpancheckVariables();

    void updateSpancheckData();

public:
    AcquireTSINeph3563(const CPD3::Data::ValueSegment::Transfer &config,
                       const std::string &loggingContext);

    AcquireTSINeph3563(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireTSINeph3563();

    CPD3::Data::SequenceValue::Transfer getPersistentValues() override;

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    CPD3::Data::Variant::Root getCommands() override;

    CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables() override;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus() override;

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    void serializeState(QDataStream &stream) override;

    void deserializeState(QDataStream &stream) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    void command(const CPD3::Data::Variant::Read &value) override;

    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;
};

class AcquireTSINeph3563Component
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_tsi_neph3563"
                              FILE
                              "acquire_tsi_neph3563.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    virtual QString getComponentName() const;

    virtual QString getComponentDisplayName() const;

    virtual QString getComponentDescription() const;

    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
