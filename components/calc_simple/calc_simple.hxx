/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CALCSIMPLE_H
#define CALCSIMPLE_H

#include "core/first.hxx"

#include <vector>
#include <memory>
#include <unordered_set>
#include <QtGlobal>
#include <QObject>
#include <QRegularExpression>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

enum class Operation {
    none,

    add, subtract, multiply, divide, power, logarithm,

    add_all, subtract_all, multiply_all, divide_all,

    e_x, ln_x, log10_x, sin_x, cos_x, tan_x, asin_x, acos_x, atan_x, atan2, abs,

    sin_x_scale, cos_x_scale,

    ratio_sum, sum_ratio,

    mulitply_add, add_multiply,

    wrap_bound, wrap_range, max, min, limit, undefined_below, undefined_above, undefined_outside,
};

class CalcSimple : public CPD3::Data::SegmentProcessingStage {
    struct Processing {
        std::unique_ptr<CPD3::Data::DynamicSequenceSelection> operateOutput;

        std::unique_ptr<CPD3::Data::DynamicPrimitive<Operation>> operation;
        std::unique_ptr<CPD3::Data::DynamicCalibration> calibration;
        std::unique_ptr<CPD3::Data::DynamicBool> calibrationReverse;

        std::unique_ptr<CPD3::Data::DynamicInput> inputA;
        std::unique_ptr<CPD3::Data::DynamicInput> inputB;
        std::unique_ptr<CPD3::Data::DynamicInput> inputC;

        CPD3::Data::SequenceName keyName;
    };
    std::vector<Processing> processing;

    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> defaultOutput;
    std::unique_ptr<CPD3::Data::DynamicPrimitive<Operation>> defaultOperation;
    std::unique_ptr<CPD3::Data::DynamicCalibration> defaultCalibration;
    std::unique_ptr<CPD3::Data::DynamicBool> defaultCalibrationReverse;
    std::unique_ptr<CPD3::Data::DynamicInput> defaultInputA;
    std::unique_ptr<CPD3::Data::DynamicInput> defaultInputB;
    std::unique_ptr<CPD3::Data::DynamicInput> defaultInputC;
    std::unique_ptr<CPD3::Data::DynamicInput> fanoutInputA;
    std::unique_ptr<CPD3::Data::DynamicInput> fanoutInputB;
    std::unique_ptr<CPD3::Data::DynamicInput> fanoutInputC;
    bool usingFanout;
    QRegularExpression fanoutKey;

    void handleOptions(const CPD3::ComponentOptions &options);

    void handleNewProcessing(const CPD3::Data::SequenceName &name,
                             std::size_t id,
                             CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control);

    void registerPossibleInput(const CPD3::Data::SequenceName &name,
                               CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                               std::size_t filterID = static_cast<std::size_t>(-1));

public:
    CalcSimple() = delete;

    CalcSimple(const CPD3::ComponentOptions &options);

    CalcSimple(const CPD3::ComponentOptions &options,
               double start,
               double end,
               const QList<CPD3::Data::SequenceName> &inputs);

    CalcSimple(double start,
               double end,
               const CPD3::Data::SequenceName::Component &station,
               const CPD3::Data::SequenceName::Component &archive,
               const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CalcSimple();

    void unhandled(const CPD3::Data::SequenceName &name,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;

    QSet<double> metadataBreaks(int id) override;

    CalcSimple(QDataStream &stream);

    void serialize(QDataStream &stream) override;
};

class CalcSimpleComponent
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.calc_simple"
                              FILE
                              "calc_simple.json")
public:
    QString getBasicSerializationName() const override;

    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    CPD3::Data::SegmentProcessingStage *createBasicFilterDynamic(const CPD3::ComponentOptions &options) override;

    CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined(const CPD3::ComponentOptions &options,
                                                                    double start,
                                                                    double end,
                                                                    const QList<
                                                                            CPD3::Data::SequenceName> &inputs) override;

    CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                 double end,
                                                                 const CPD3::Data::SequenceName::Component &station,
                                                                 const CPD3::Data::SequenceName::Component &archive,
                                                                 const CPD3::Data::ValueSegment::Transfer &config) override;

    CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream) override;
};

#endif
