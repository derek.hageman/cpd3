/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cmath>
#include <QRegularExpression>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "core/environment.hxx"
#include "core/qtcompat.hxx"
#include "core/timeutils.hxx"

#include "calc_simple.hxx"

using namespace CPD3;
using namespace CPD3::Data;


void CalcSimple::handleOptions(const ComponentOptions &options)
{
    if (options.isSet("a")) {
        defaultInputA.reset(qobject_cast<DynamicInputOption *>(options.get("a"))->getInput());
    } else {
        defaultInputA.reset(new DynamicInput::Constant);
    }
    if (options.isSet("b")) {
        defaultInputB.reset(qobject_cast<DynamicInputOption *>(options.get("b"))->getInput());
    } else {
        defaultInputB.reset(new DynamicInput::Constant);
    }
    if (options.isSet("c")) {
        defaultInputC.reset(qobject_cast<DynamicInputOption *>(options.get("c"))->getInput());
    } else {
        defaultInputC.reset(new DynamicInput::Constant);
    }

    if (options.isSet("output")) {
        defaultOutput.reset(qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("output"))->getOperator());
    } else {
        defaultOutput.reset(new DynamicSequenceSelection::None);
    }

    if (options.isSet("calibration")) {
        defaultCalibration.reset(
                qobject_cast<DynamicCalibrationOption *>(options.get("calibration"))->getInput());
    } else {
        defaultCalibration.reset(new DynamicCalibration::Constant);
    }
    if (options.isSet("inverse-calibration")) {
        defaultCalibrationReverse.reset(
                qobject_cast<DynamicBoolOption *>(options.get("inverse-calibration"))->getInput());
    } else {
        defaultCalibrationReverse.reset(new DynamicBool::Constant(false));
    }

    Operation op = Operation::add;
    if (options.isSet("operation")) {
        op =
                static_cast<Operation>(qobject_cast<ComponentOptionEnum *>(options.get("operation"))
                        ->get()
                        .getID());
    }
    defaultOperation.reset(new DynamicPrimitive<Operation>::Constant(op));

    QString key;
    if (options.isSet("fanout-variable")) {
        key = qobject_cast<ComponentOptionSingleString *>(options.get("fanout-variable"))->get();
    }
    usingFanout = false;
    if (!key.isEmpty()) {
        usingFanout = true;
        fanoutKey = QRegularExpression(key);
        if (!fanoutKey.isValid()) {
            fanoutKey = QRegularExpression(QRegularExpression::escape(key));
        }

        fanoutInputA.reset(defaultInputA->clone());
        fanoutInputB.reset(defaultInputB->clone());
        fanoutInputC.reset(defaultInputC->clone());
    } else {
        Processing proc;

        proc.operateOutput = std::move(defaultOutput);
        proc.operation = std::move(defaultOperation);
        proc.calibration = std::move(defaultCalibration);
        proc.calibrationReverse = std::move(defaultCalibrationReverse);
        proc.inputA = std::move(defaultInputA);
        proc.inputB = std::move(defaultInputB);
        proc.inputC = std::move(defaultInputC);

        processing.emplace_back(std::move(proc));
    }

}


CalcSimple::CalcSimple(const ComponentOptions &options)
{
    handleOptions(options);
}

CalcSimple::CalcSimple(const ComponentOptions &options,
                       double,
                       double,
                       const QList<SequenceName> &inputs)
{
    handleOptions(options);

    for (const auto &name : inputs) {
        CalcSimple::unhandled(name, nullptr);
    }
}

static Operation convertOperation(const Variant::Read &value)
{
    const auto &type = value.toString();
    if (Util::equal_insensitive(type, "Add", "Addition"))
        return Operation::add;
    else if (Util::equal_insensitive(type, "Subtract", "Sub"))
        return Operation::subtract;
    else if (Util::equal_insensitive(type, "Multiply", "Product"))
        return Operation::multiply;
    else if (Util::equal_insensitive(type, "Divide", "Quotient"))
        return Operation::divide;
    else if (Util::equal_insensitive(type, "Power"))
        return Operation::power;
    else if (Util::equal_insensitive(type, "Logarithm", "log"))
        return Operation::logarithm;
    else if (Util::equal_insensitive(type, "AdditionAll", "AddAll"))
        return Operation::add_all;
    else if (Util::equal_insensitive(type, "SubtractAll", "SubAll"))
        return Operation::subtract_all;
    else if (Util::equal_insensitive(type, "MultiplyAll", "ProductAll"))
        return Operation::multiply_all;
    else if (Util::equal_insensitive(type, "DivideAll", "QuotientAll"))
        return Operation::divide_all;
    else if (Util::equal_insensitive(type, "e", "exp"))
        return Operation::e_x;
    else if (Util::equal_insensitive(type, "ln", "NaturalLogarithm"))
        return Operation::ln_x;
    else if (Util::equal_insensitive(type, "log10", "LogBase10"))
        return Operation::log10_x;
    else if (Util::equal_insensitive(type, "sin"))
        return Operation::sin_x;
    else if (Util::equal_insensitive(type, "cos"))
        return Operation::cos_x;
    else if (Util::equal_insensitive(type, "tan"))
        return Operation::tan_x;
    else if (Util::equal_insensitive(type, "asin"))
        return Operation::asin_x;
    else if (Util::equal_insensitive(type, "acos"))
        return Operation::acos_x;
    else if (Util::equal_insensitive(type, "atan"))
        return Operation::atan_x;
    else if (Util::equal_insensitive(type, "atan2"))
        return Operation::atan2;
    else if (Util::equal_insensitive(type, "abs"))
        return Operation::abs;
    else if (Util::equal_insensitive(type, "SinScale", "SinScaled"))
        return Operation::sin_x_scale;
    else if (Util::equal_insensitive(type, "CosScale", "CosScaled"))
        return Operation::cos_x_scale;
    else if (Util::equal_insensitive(type, "RatioSum"))
        return Operation::ratio_sum;
    else if (Util::equal_insensitive(type, "SumRatio"))
        return Operation::sum_ratio;
    else if (Util::equal_insensitive(type, "MultiplyAdd", "MultiplyThenAdd"))
        return Operation::mulitply_add;
    else if (Util::equal_insensitive(type, "AddMultiply", "AddThenMultiply"))
        return Operation::add_multiply;
    else if (Util::equal_insensitive(type, "Wrap", "WrapBounded"))
        return Operation::wrap_bound;
    else if (Util::equal_insensitive(type, "WrapRange"))
        return Operation::wrap_range;
    else if (Util::equal_insensitive(type, "max", "Maximum"))
        return Operation::max;
    else if (Util::equal_insensitive(type, "min", "Minimum"))
        return Operation::min;
    else if (Util::equal_insensitive(type, "Limit"))
        return Operation::limit;
    else if (Util::equal_insensitive(type, "UndefineBelow", "UndefinedBelow", "UndefineLower",
                                     "UndefinedLower"))
        return Operation::undefined_below;
    else if (Util::equal_insensitive(type, "UndefineAbove", "UndefinedAbove", "UndefineUpper",
                                     "UndefinedUpper"))
        return Operation::undefined_above;
    else if (Util::equal_insensitive(type, "UndefineOutside", "UndefinedOutside"))
        return Operation::undefined_outside;

    return Operation::none;
}

CalcSimple::CalcSimple(double start,
                       double end,
                       const SequenceName::Component &station,
                       const SequenceName::Component &archive,
                       const ValueSegment::Transfer &config)
{
    usingFanout = false;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    for (const auto &child : children) {
        Processing proc;

        proc.operateOutput
            .reset(DynamicSequenceSelection::fromConfiguration(config, QString("%1/Output").arg(
                    QString::fromStdString(child)), start, end));

        proc.inputA
            .reset(DynamicInput::fromConfiguration(config, QString("%1/A").arg(
                    QString::fromStdString(child)), start, end));
        proc.inputB
            .reset(DynamicInput::fromConfiguration(config, QString("%1/B").arg(
                    QString::fromStdString(child)), start, end));
        proc.inputC
            .reset(DynamicInput::fromConfiguration(config, QString("%1/C").arg(
                    QString::fromStdString(child)), start, end));

        proc.operation
            .reset(DynamicPrimitive<Operation>::fromConfiguration(convertOperation, config,
                                                                  QString("%1/Operation").arg(
                                                                          QString::fromStdString(
                                                                                  child)), start,
                                                                  end));

        proc.calibration
            .reset(DynamicCalibrationOption::fromConfiguration(config,
                                                               QString("%1/Calibration").arg(
                                                                       QString::fromStdString(
                                                                               child)), start,
                                                               end));
        proc.calibrationReverse
            .reset(DynamicBoolOption::fromConfiguration(config, QString("%1/InvertCalibration").arg(
                    QString::fromStdString(child)), start, end));

        proc.operateOutput->registerExpected(station, archive);
        proc.inputA->registerExpected(station, archive);
        proc.inputB->registerExpected(station, archive);
        proc.inputC->registerExpected(station, archive);

        processing.emplace_back(std::move(proc));
    }
}


CalcSimple::~CalcSimple() = default;

void CalcSimple::handleNewProcessing(const SequenceName &name,
                                     std::size_t id,
                                     SegmentProcessingStage::SequenceHandlerControl *control)
{
    auto &proc = processing[id];

    SequenceName::Set reg;

    proc.operateOutput
        ->registerExpected(name.getStation(), name.getArchive(), {}, name.getFlavors());
    Util::merge(proc.operateOutput->getAllUnits(), reg);

    if (!control)
        return;

    for (const auto &n : reg) {
        bool isNew = !control->isHandled(n);
        control->filterUnit(n, id);
        if (isNew)
            registerPossibleInput(n, control, id);
    }
}

void CalcSimple::registerPossibleInput(const SequenceName &name,
                                       SegmentProcessingStage::SequenceHandlerControl *control,
                                       std::size_t filterID)
{
    for (std::size_t id = 0, max = processing.size(); id < max; id++) {
        bool usedAsInput = false;
        auto &proc = processing[id];

        if (usingFanout) {
            if (proc.keyName.getStation() != name.getStation())
                continue;
            if (proc.keyName.getArchive() != name.getArchive())
                continue;
            if (proc.keyName.getFlavorsString() != name.getFlavorsString())
                continue;
        }

        if (proc.inputA->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputB->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputC->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (usedAsInput && id != filterID)
            handleNewProcessing(name, id, control);
    }

    if (defaultInputA && defaultInputA->registerInput(name) && control)
        control->deferHandling(name);
    if (defaultInputB && defaultInputB->registerInput(name) && control)
        control->deferHandling(name);
    if (defaultInputC && defaultInputC->registerInput(name) && control)
        control->deferHandling(name);
}

void CalcSimple::unhandled(const SequenceName &name,
                           SegmentProcessingStage::SequenceHandlerControl *control)
{
    registerPossibleInput(name, control);

    for (std::size_t id = 0, max = processing.size(); id < max; id++) {
        if (processing[id].operateOutput->registerInput(name)) {
            if (control)
                control->filterUnit(name, id);
            handleNewProcessing(name, id, control);
            return;
        }
    }

    if (!usingFanout)
        return;

    auto r = fanoutKey.match(name.getVariableQString(), 0, QRegularExpression::NormalMatch,
                             QRegularExpression::AnchoredMatchOption);
    if (!r.hasMatch())
        return;
    if (r.capturedLength() != (int)name.getVariable().length())
        return;

    auto key = name;
    key.setVariable(r.captured(1));
    for (std::size_t id = 0, max = processing.size(); id < max; id++) {
        if (processing[id].keyName == key) {
            handleNewProcessing(name, id, control);
            return;
        }
    }

    Processing proc;
    proc.keyName = key;

    proc.operateOutput.reset(defaultOutput->clone());
    proc.operateOutput->forceUnit(key.getStation(), key.getArchive(), {}, key.getFlavors());
    proc.operateOutput->registerInput(name);
    proc.operateOutput
        ->registerExpected(name.getStation(), name.getArchive(), {}, name.getFlavors());

    proc.inputA.reset(fanoutInputA->clone());
    proc.inputA->registerInput(name);
    proc.inputA->registerExpected(name.getStation(), name.getArchive(), {}, name.getFlavors());
    if (defaultInputA) {
        for (const auto &add : defaultInputA->getUsedInputs()) {
            if (add.getStation() != key.getStation())
                continue;
            if (add.getArchive() != key.getArchive())
                continue;
            if (add.getFlavors() != key.getFlavors())
                continue;
            proc.inputA->registerInput(add);
        }
    }

    proc.inputB.reset(fanoutInputB->clone());
    proc.inputB->registerInput(name);
    proc.inputB->registerExpected(name.getStation(), name.getArchive(), {}, name.getFlavors());
    if (defaultInputB) {
        for (const auto &add : defaultInputB->getUsedInputs()) {
            if (add.getStation() != key.getStation())
                continue;
            if (add.getArchive() != key.getArchive())
                continue;
            if (add.getFlavors() != key.getFlavors())
                continue;
            proc.inputB->registerInput(add);
        }
    }

    proc.inputC.reset(fanoutInputC->clone());
    proc.inputC->registerInput(name);
    proc.inputC->registerExpected(name.getStation(), name.getArchive(), {}, name.getFlavors());
    if (defaultInputC) {
        for (const auto &add : defaultInputC->getUsedInputs()) {
            if (add.getStation() != key.getStation())
                continue;
            if (add.getArchive() != key.getArchive())
                continue;
            if (add.getFlavors() != key.getFlavors())
                continue;
            proc.inputC->registerInput(add);
        }
    }

    proc.operation.reset(defaultOperation->clone());
    proc.calibration.reset(defaultCalibration->clone());
    proc.calibrationReverse.reset(defaultCalibrationReverse->clone());

    auto id = processing.size();

    if (control) {
        for (const auto &n : proc.operateOutput->getAllUnits()) {
            if (control->isHandled(n))
                continue;
            control->filterUnit(n, id);
        }

        for (const auto &n : proc.inputA->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : proc.inputB->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : proc.inputC->getUsedInputs()) {
            control->inputUnit(n, id);
        }
    }

    processing.emplace_back(std::move(proc));
}

static double apply_wrap(double value, double lower, double upper, double range)
{
    Q_ASSERT(range > 0.0);
    if (value < lower) {
        value += std::abs(std::floor((value - lower) / range)) * range;
    }
    if (value >= upper) {
        value -= std::abs(std::floor((value - lower) / range)) * range;
    }
    return value;
}

static double map_infinite(double input)
{
    if (!std::isfinite(input))
        return FP::undefined();
    return input;
}

static double calculate(Operation op, double a, double b, double c)
{
    switch (op) {
    case Operation::none:
        return a;
    case Operation::add:
        if (!FP::defined(a) || !FP::defined(b))
            return FP::undefined();
        return a + b;
    case Operation::subtract:
        if (!FP::defined(a) || !FP::defined(b))
            return FP::undefined();
        return a - b;
    case Operation::multiply:
        if (!FP::defined(a) || !FP::defined(b))
            return FP::undefined();
        return a * b;
    case Operation::divide:
        if (!FP::defined(a) || !FP::defined(b) || b == 0.0)
            return FP::undefined();
        return a / b;
    case Operation::power:
        if (!FP::defined(a) || !FP::defined(b) || (a == 0.0 && b == 0.0))
            return FP::undefined();
        return std::pow(a, b);
    case Operation::logarithm:
        if (!FP::defined(a) || !FP::defined(b) || a <= 0.0 || b <= 0.0)
            return FP::undefined();
        return std::log(a) / std::log(b);
    case Operation::add_all:
        if (!FP::defined(a) || !FP::defined(b) || !FP::defined(c))
            return FP::undefined();
        return a + b + c;
    case Operation::subtract_all:
        if (!FP::defined(a) || !FP::defined(b) || !FP::defined(c))
            return FP::undefined();
        return a - b - c;
    case Operation::multiply_all:
        if (!FP::defined(a) || !FP::defined(b) || !FP::defined(c))
            return FP::undefined();
        return a * b * c;
    case Operation::divide_all:
        if (!FP::defined(a) || !FP::defined(b) || !FP::defined(c) || b == 0.0 || c == 0.0)
            return FP::undefined();
        return a / b / c;
    case Operation::e_x:
        if (!FP::defined(a))
            return FP::undefined();
        return std::exp(a);
    case Operation::ln_x:
        if (!FP::defined(a) || a <= 0.0)
            return FP::undefined();
        return std::log(a);
    case Operation::log10_x:
        if (!FP::defined(a) || a <= 0.0)
            return FP::undefined();
        return std::log10(a);
    case Operation::sin_x:
        if (!FP::defined(a))
            return FP::undefined();
        return std::sin(a);
    case Operation::cos_x:
        if (!FP::defined(a))
            return FP::undefined();
        return std::cos(a);
    case Operation::tan_x:
        if (!FP::defined(a))
            return FP::undefined();
        return map_infinite(std::tan(a));
    case Operation::asin_x:
        if (!FP::defined(a) || a < -1.0 || a > 1.0)
            return FP::undefined();
        return std::asin(a);
    case Operation::acos_x:
        if (!FP::defined(a) || a < -1.0 || a > 1.0)
            return FP::undefined();
        return std::acos(a);
    case Operation::atan_x:
        if (!FP::defined(a))
            return FP::undefined();
        return std::atan(a);
    case Operation::atan2:
        if (!FP::defined(a) || !FP::defined(b))
            return FP::undefined();
        return std::atan2(a, b);
    case Operation::abs:
        if (!FP::defined(a))
            return FP::undefined();
        return std::abs(a);
    case Operation::sin_x_scale:
        if (!FP::defined(a) || !FP::defined(b))
            return FP::undefined();
        return std::sin(a) * b;
    case Operation::cos_x_scale:
        if (!FP::defined(a) || !FP::defined(b))
            return FP::undefined();
        return std::cos(a) * b;
    case Operation::ratio_sum: {
        if (!FP::defined(a) || !FP::defined(b) || !FP::defined(c))
            return FP::undefined();
        double v = b + c;
        if (v == 0.0)
            return FP::undefined();
        return a / v;
    }
    case Operation::sum_ratio:
        if (!FP::defined(a) || !FP::defined(b) || !FP::defined(c) || c == 0.0)
            return FP::undefined();
        return (a + b) / c;
    case Operation::mulitply_add:
        if (!FP::defined(a) || !FP::defined(b) || !FP::defined(c))
            return FP::undefined();
        return a * b + c;
    case Operation::add_multiply:
        if (!FP::defined(a) || !FP::defined(b) || !FP::defined(c))
            return FP::undefined();
        return a * (b + c);
    case Operation::wrap_bound:
        if (!FP::defined(a) || !FP::defined(b) || b <= 0.0)
            return FP::undefined();
        return apply_wrap(a, 0.0, b, b);
    case Operation::wrap_range:
        if (!FP::defined(a) || !FP::defined(b) || !FP::defined(c) || c <= b)
            return FP::undefined();
        return apply_wrap(a, b, c, c - b);
    case Operation::max:
        if (!FP::defined(a) || !FP::defined(b))
            return FP::undefined();
        return std::max(a, b);
    case Operation::min:
        if (!FP::defined(a) || !FP::defined(b))
            return FP::undefined();
        return std::min(a, b);
    case Operation::limit:
        if (!FP::defined(a) || !FP::defined(b) || !FP::defined(c))
            return FP::undefined();
        return std::min(std::max(a, b), c);
    case Operation::undefined_below:
        if (!FP::defined(a) || !FP::defined(b))
            return FP::undefined();
        if (a < b)
            return FP::undefined();
        return a;
    case Operation::undefined_above:
        if (!FP::defined(a) || !FP::defined(b))
            return FP::undefined();
        if (a > b)
            return FP::undefined();
        return a;
    case Operation::undefined_outside:
        if (!FP::defined(a) || !FP::defined(b) || !FP::defined(c))
            return FP::undefined();
        if (a < b)
            return FP::undefined();
        if (a > c)
            return FP::undefined();
        return a;
    }

    Q_ASSERT(false);
    return FP::undefined();
}

void CalcSimple::process(int id, SequenceSegment &data)
{
    auto &proc = processing[id];

    double a = proc.inputA->get(data);
    double b = proc.inputB->get(data);
    double c = proc.inputC->get(data);

    double value = calculate(proc.operation->get(data), a, b, c);
    if (proc.calibrationReverse->get(data)) {
        value = proc.calibration->get(data).inverse(value);
    } else {
        value = proc.calibration->get(data).apply(value);
    }

    for (const auto &i : proc.operateOutput->get(data)) {
        data[i].setReal(value);
    }
}

Variant::Read selectMeta(const Variant::Read &a, const Variant::Read &b, const Variant::Read &c)
{
    if (!a.exists())
        return Variant::Read::empty();

    if (b.exists()) {
        if (a.metadata("Units") != b.metadata("Units"))
            return Variant::Read::empty();
        if (a.metadata("GroupUnits") != b.metadata("GroupUnits"))
            return Variant::Read::empty();
    }

    if (c.exists()) {
        if (a.metadata("Units") != c.metadata("Units"))
            return Variant::Read::empty();
        if (a.metadata("GroupUnits") != c.metadata("GroupUnits"))
            return Variant::Read::empty();
    }

    return a;
}

static std::string describeOperation(Operation op)
{
    switch (op) {
    case Operation::none:
        return "A";
    case Operation::add:
        return "A + B";
    case Operation::subtract:
        return "A - B";
    case Operation::multiply:
        return "A * B";
    case Operation::divide:
        return "A / B";
    case Operation::power:
        return "Aᴮ";
    case Operation::logarithm:
        return "log_B(A)";
    case Operation::add_all:
        return "A + B + C";
    case Operation::subtract_all:
        return "A - B - C";
    case Operation::multiply_all:
        return "A * B * C";
    case Operation::divide_all:
        return "A / B / C";
    case Operation::e_x:
        return "eᴬ";
    case Operation::ln_x:
        return "ln(A)";
    case Operation::log10_x:
        return "log⏨(A)";
    case Operation::sin_x:
        return "sin(A)";
    case Operation::cos_x:
        return "cos(A)";
    case Operation::tan_x:
        return "tan(A)";
    case Operation::asin_x:
        return "asin(A)";
    case Operation::acos_x:
        return "acos(A)";
    case Operation::atan_x:
        return "atan(A)";
    case Operation::atan2:
        return "atan2(A, B)";
    case Operation::abs:
        return "|A|";
    case Operation::sin_x_scale:
        return "sin(A) * B";
    case Operation::cos_x_scale:
        return "sin(C) * B";
    case Operation::ratio_sum:
        return "A / (B + C)";
    case Operation::sum_ratio:
        return "(A + B) / C";
    case Operation::mulitply_add:
        return "A * B + C";
    case Operation::add_multiply:
        return "A * (B + C)";
    case Operation::wrap_bound:
        return "Wrap A in [0,B]";
    case Operation::wrap_range:
        return "Wrap A to [B,C]";
    case Operation::max:
        return "max(A, B)";
    case Operation::min:
        return "min(A, B)";
    case Operation::limit:
        return "min(max(A, B), C)";
    case Operation::undefined_below:
        return "A if A ≥ B";
    case Operation::undefined_above:
        return "A if A ≤ B";
    case Operation::undefined_outside:
        return "A if B ≤ A ≤ C";
    }
    Q_ASSERT(false);
    return {};
}

void CalcSimple::processMeta(int id, SequenceSegment &data)
{
    auto &proc = processing[id];

    Variant::Root meta;

    meta["By"].setString("calc_simple");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    auto operation = describeOperation(proc.operation->get(data));
    meta["Parameters"].hash("Operation") = operation;
    meta["Parameters"].hash("A") = proc.inputA->describe(data);
    meta["Parameters"].hash("B") = proc.inputB->describe(data);
    meta["Parameters"].hash("C") = proc.inputC->describe(data);

    Variant::Read metaA = Variant::Read::empty();
    for (auto i : proc.inputA->getUsedInputs()) {
        i.setMeta();
        if (!data[i].exists())
            continue;
        metaA = data[i];
        break;
    }
    Variant::Read metaB = Variant::Read::empty();
    for (auto i : proc.inputB->getUsedInputs()) {
        i.setMeta();
        if (!data[i].exists())
            continue;
        metaA = data[i];
        break;
    }
    Variant::Read metaC = Variant::Read::empty();
    for (auto i : proc.inputC->getUsedInputs()) {
        i.setMeta();
        if (!data[i].exists())
            continue;
        metaA = data[i];
        break;
    }

    auto base = selectMeta(metaA, metaB, metaC);
    for (auto i : proc.operateOutput->get(data)) {
        i.setMeta();
        if (!data[i].exists()) {
            data[i].metadataReal("Description").setString("Calculated value of " + operation);
            if (base.metadata("Format").exists()) {
                data[i].metadataReal("Format").set(base.metadata("Format"));
            }
            if (base.metadata("Units").exists()) {
                data[i].metadataReal("Units").set(base.metadata("Units"));
            }
            if (base.metadata("GroupUnits").exists()) {
                data[i].metadataReal("GroupUnits").set(base.metadata("GroupUnits"));
            }
        }
        data[i].metadata("Processing").toArray().after_back().set(meta);
    }
}


SequenceName::Set CalcSimple::requestedInputs()
{
    SequenceName::Set out;
    for (const auto &p : processing) {
        Util::merge(p.inputA->getUsedInputs(), out);
        Util::merge(p.inputB->getUsedInputs(), out);
        Util::merge(p.inputC->getUsedInputs(), out);
    }
    return out;
}

SequenceName::Set CalcSimple::predictedOutputs()
{
    SequenceName::Set out;
    for (const auto &p : processing) {
        Util::merge(p.operateOutput->getAllUnits(), out);
    }
    return out;
}

QSet<double> CalcSimple::metadataBreaks(int id)
{
    const auto &p = processing[id];
    QSet<double> result;
    Util::merge(p.operateOutput->getChangedPoints(), result);
    Util::merge(p.inputA->getChangedPoints(), result);
    Util::merge(p.inputB->getChangedPoints(), result);
    Util::merge(p.inputC->getChangedPoints(), result);
    return result;
}

QDataStream &operator>>(QDataStream &stream, Operation &op)
{
    quint32 n = 0;
    stream >> n;
    op = static_cast<Operation>(n);
    return stream;
}

QDataStream &operator<<(QDataStream &stream, Operation op)
{
    stream << static_cast<quint32>(op);
    return stream;
}

QDebug operator<<(QDebug stream, Operation op)
{
    stream << describeOperation(op).c_str();
    return stream;
}

CalcSimple::CalcSimple(QDataStream &stream)
{
    stream >> defaultOutput;
    stream >> defaultOperation;
    stream >> defaultCalibration;
    stream >> defaultCalibrationReverse;
    stream >> defaultInputA;
    stream >> defaultInputB;
    stream >> defaultInputC;
    stream >> fanoutInputA;
    stream >> fanoutInputB;
    stream >> fanoutInputC;
    {
        QString pattern;
        stream >> pattern;
        if (!pattern.isEmpty()) {
            usingFanout = true;
            fanoutKey = QRegularExpression(pattern);
        } else {
            usingFanout = false;
        }
    }

    Deserialize::container(stream, processing, [&stream]() {
        Processing proc;

        stream >> proc.operateOutput;
        stream >> proc.operation;
        stream >> proc.calibration;
        stream >> proc.calibrationReverse;
        stream >> proc.inputA;
        stream >> proc.inputB;
        stream >> proc.inputC;

        return proc;
    });
}

void CalcSimple::serialize(QDataStream &stream)
{
    stream << defaultOutput;
    stream << defaultOperation;
    stream << defaultCalibration;
    stream << defaultCalibrationReverse;
    stream << defaultInputA;
    stream << defaultInputB;
    stream << defaultInputC;
    stream << fanoutInputA;
    stream << fanoutInputB;
    stream << fanoutInputC;
    stream << fanoutKey.pattern();

    Serialize::container(stream, processing, [&stream](const Processing &proc) {
        stream << proc.operateOutput;
        stream << proc.operation;
        stream << proc.calibration;
        stream << proc.calibrationReverse;
        stream << proc.inputA;
        stream << proc.inputB;
        stream << proc.inputC;
    });
}


QString CalcSimpleComponent::getBasicSerializationName() const
{ return QString::fromLatin1("calc_simple"); }

ComponentOptions CalcSimpleComponent::getOptions()
{
    ComponentOptions options;

    options.add("a", new DynamicInputOption(tr("a", "name"), tr("Input A"),
                                            tr("The first input.  The usage depends on the operation selected."),
                                            {}));

    options.add("b", new DynamicInputOption(tr("b", "name"), tr("Input B"),
                                            tr("The second input.  The usage depends on the operation selected."),
                                            {}));

    options.add("c", new DynamicInputOption(tr("c", "name"), tr("Input C"),
                                            tr("The third input.  The usage depends on the operation selected."),
                                            {}));

    options.add("output",
                new DynamicSequenceSelectionOption(tr("output", "name"), tr("Output value"),
                                                   tr("This is output value of the calculation."),
                                                   {}));

    options.add("calibration",
                new DynamicCalibrationOption(tr("calibration", "name"), tr("Calibration applied"),
                                             tr("The calibration applied to the output after the calculation is performed."),
                                             {}));

    options.add("inverse-calibration", new DynamicBoolOption(tr("inverse-calibration", "name"),
                                                             tr("Invert the calibration"),
                                                             tr("When set, the calibration is inverted (backed out) instead of applied.  "
                                                                "This is used to get the original values of an already applied calibration."),
                                                             {}));


    options.add("fanout-variable", new ComponentOptionSingleString(tr("fanout-variable", "name"),
                                                                   tr("Fanout variable regular expression"),
                                                                   tr("When set, this matches input variables against the given regular expression.  "
                                                                      "When one matches, a new copy of the operation is created with the other parameters (station, archive, flavors).  "
                                                                      "This is most commonly used to handle cut-size selected data by matching one of the inputs that is size selected."),
                                                                   tr("Disabled")));

    ComponentOptionEnum *op =
            new ComponentOptionEnum(tr("operation", "name"), tr("Operation performed"),
                                    tr("This is the operation performed on the inputs to generate the output."),
                                    tr("None"), -1);
    op->add(static_cast<int>(Operation::none), "none", tr("None", "operation name"),
            tr("A", "operation description"));
    op->alias(static_cast<int>(Operation::none), tr("Identity", "operation name"));
    op->add(static_cast<int>(Operation::add), "add", tr("Add", "operation name"),
            tr("A + B", "operation description"));
    op->add(static_cast<int>(Operation::subtract), "subtract", tr("Subtract", "operation name"),
            tr("A - B", "operation description"));
    op->add(static_cast<int>(Operation::multiply), "multiply", tr("Multiply", "operation name"),
            tr("A * B", "operation description"));
    op->add(static_cast<int>(Operation::divide), "divide", tr("Divide", "operation name"),
            tr("A / B", "operation description"));
    op->add(static_cast<int>(Operation::power), "power", tr("Power", "operation name"),
            tr("Aᴮ", "operation description"));
    op->add(static_cast<int>(Operation::logarithm), "logarithm", tr("Logarithm", "operation name"),
            tr("log_B(A) (i.e. log(A)/log(B))", "operation description"));
    op->alias(static_cast<int>(Operation::logarithm), tr("Log", "operation name"));
    op->add(static_cast<int>(Operation::add_all), "addall", tr("AddAll", "operation name"),
            tr("A + B + C", "operation description"));
    op->add(static_cast<int>(Operation::subtract_all), "subtractall",
            tr("SubtractAll", "operation name"), tr("A - B - C", "operation description"));
    op->add(static_cast<int>(Operation::multiply_all), "multiplyall",
            tr("MultiplyAll", "operation name"), tr("A * B * C", "operation description"));
    op->add(static_cast<int>(Operation::divide_all), "divideall", tr("DivideAll", "operation name"),
            tr("A / B / C", "operation description"));
    op->add(static_cast<int>(Operation::e_x), "exp", tr("exp", "operation name"),
            tr("eᴬ", "operation description"));
    op->add(static_cast<int>(Operation::ln_x), "ln", tr("ln", "operation name"),
            tr("ln(A)", "operation description"));
    op->add(static_cast<int>(Operation::log10_x), "log10", tr("log10", "operation name"),
            tr("log⏨(A)", "operation description"));
    op->add(static_cast<int>(Operation::sin_x), "sin", tr("sin", "operation name"),
            tr("sin(A)", "operation description"));
    op->add(static_cast<int>(Operation::cos_x), "cos", tr("cos", "operation name"),
            tr("cos(A)", "operation description"));
    op->add(static_cast<int>(Operation::tan_x), "tan", tr("tan", "operation name"),
            tr("tan(A)", "operation description"));
    op->add(static_cast<int>(Operation::asin_x), "asin", tr("asin", "operation name"),
            tr("asin(A)", "operation description"));
    op->add(static_cast<int>(Operation::acos_x), "acos", tr("acos", "operation name"),
            tr("acos(A)", "operation description"));
    op->add(static_cast<int>(Operation::atan_x), "atan", tr("atan", "operation name"),
            tr("atan(A)", "operation description"));
    op->add(static_cast<int>(Operation::atan2), "atan2", tr("atan2", "operation name"),
            tr("atan2(A,B)", "operation description"));
    op->add(static_cast<int>(Operation::abs), "abs", tr("abs", "operation name"),
            tr("|A|", "operation description"));
    op->add(static_cast<int>(Operation::sin_x_scale), "sinscaled",
            tr("SinScaled", "operation name"), tr("sin(A) * B", "operation description"));
    op->add(static_cast<int>(Operation::cos_x_scale), "cosscaled",
            tr("CosScaled", "operation name"), tr("cos(A) * B", "operation description"));
    op->add(static_cast<int>(Operation::ratio_sum), "ratiosum", tr("RatioSum", "operation name"),
            tr("A / (B + C)", "operation description"));
    op->add(static_cast<int>(Operation::sum_ratio), "sumratio", tr("SumRatio", "operation name"),
            tr("(A + B) / C", "operation description"));
    op->add(static_cast<int>(Operation::mulitply_add), "multiplyadd",
            tr("MultiplyAdd", "operation name"), tr("A * B + C", "operation description"));
    op->add(static_cast<int>(Operation::add_multiply), "addmultiply",
            tr("AddMulitply", "operation name"), tr("A * (B + C)", "operation description"));
    op->add(static_cast<int>(Operation::wrap_bound), "wrap", tr("Wrap", "operation name"),
            tr("Wrap A in [0,B]", "operation description"));
    op->add(static_cast<int>(Operation::wrap_range), "wraprange", tr("WrapRange", "operation name"),
            tr("Wrap A in [B,C]", "operation description"));
    op->add(static_cast<int>(Operation::max), "max", tr("Max", "operation name"),
            tr("max(A, B) (i.e. A if A > B)", "operation description"));
    op->add(static_cast<int>(Operation::min), "min", tr("Min", "operation name"),
            tr("min(A, B) (i.e. A if A < B)", "operation description"));
    op->add(static_cast<int>(Operation::limit), "limit", tr("Limit", "operation name"),
            tr("min(max(A, B), C) (i.e. A if B ≤ A ≤ C)", "operation description"));
    op->add(static_cast<int>(Operation::undefined_below), "undefinedbelow",
            tr("UndefinedBelow", "operation name"), tr("A if A ≥ B", "operation description"));
    op->add(static_cast<int>(Operation::undefined_above), "undefinedabove",
            tr("UndefinedAbove", "operation name"), tr("A if A ≤ B", "operation description"));
    op->add(static_cast<int>(Operation::undefined_outside), "undefinedoutisde",
            tr("UndefinedOutside", "operation name"),
            tr("A if B ≤ A ≤ C", "operation description"));

    options.add("operation", op);

    return options;
}

QList<ComponentExample> CalcSimpleComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("output")))->set({}, {},
                                                                                 "ZBsfG_S11");
    (qobject_cast<DynamicInputOption *>(options.get("a")))->set(
            SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("BsG_S11")),
            Calibration());
    (qobject_cast<DynamicInputOption *>(options.get("b")))->set(
            SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("BswG_S11")),
            Calibration());
    (qobject_cast<ComponentOptionEnum *>(options.get("operation")))->set("add");
    examples.append(ComponentExample(options, tr("Calculate non-zero adjusted scattering"),
                                     tr("This calculates BsG_S11 + BswG_S11 and places the output into ZBsfG_S11.  "
                                        "That is, it generates the total scattering with the wall scattering added back in.")));
    examples.back().setInputAuxiliary({"BswG_S11"});

    return examples;
}

SegmentProcessingStage *CalcSimpleComponent::createBasicFilterDynamic(const ComponentOptions &options)
{ return new CalcSimple(options); }

SegmentProcessingStage *CalcSimpleComponent::createBasicFilterPredefined(const ComponentOptions &options,
                                                                         double start,
                                                                         double end,
                                                                         const QList<
                                                                                 SequenceName> &inputs)
{ return new CalcSimple(options, start, end, inputs); }

SegmentProcessingStage *CalcSimpleComponent::createBasicFilterEditing(double start,
                                                                      double end,
                                                                      const SequenceName::Component &station,
                                                                      const SequenceName::Component &archive,
                                                                      const ValueSegment::Transfer &config)
{ return new CalcSimple(start, end, station, archive, config); }

SegmentProcessingStage *CalcSimpleComponent::deserializeBasicFilter(QDataStream &stream)
{ return new CalcSimple(stream); }
