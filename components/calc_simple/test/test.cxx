/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    struct ID {
        SequenceName::Set filter;
        SequenceName::Set input;

        ID() = default;

        ID(SequenceName::Set filter, SequenceName::Set input) : filter(std::move(filter)),
                                                                input(std::move(input))
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }
    };

    std::unordered_map<int, ID> contents;

    int find(const SequenceName::Set &filter, const SequenceName::Set &input = {}) const
    {
        for (const auto &check : contents) {
            if (check.second.filter != filter)
                continue;
            if (check.second.input != input)
                continue;
            return check.first;
        }
        return -1;
    }

    void filterUnit(const SequenceName &name, int targetID) override
    {
        contents[targetID].input.erase(name);
        contents[targetID].filter.insert(name);
    }

    void inputUnit(const SequenceName &name, int targetID) override
    {
        if (contents[targetID].filter.count(name))
            return;
        contents[targetID].input.insert(name);
    }

    bool isHandled(const SequenceName &name) override
    {
        for (const auto &check : contents) {
            if (check.second.filter.count(name))
                return true;
            if (check.second.input.count(name))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("calc_simple"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("a")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("b")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("c")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("output")));
        QVERIFY(qobject_cast<DynamicCalibrationOption *>(options.get("calibration")));
        QVERIFY(qobject_cast<DynamicBoolOption *>(options.get("inverse-calibration")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("fanout-variable")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("operation")));
    }

    void basic()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<DynamicInputOption *>(options.get("a"))->set(
                SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("T_V11")),
                Calibration());
        qobject_cast<DynamicInputOption *>(options.get("b"))->set(2.0);
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("output"))->set({}, {},
                                                                                   "ZOut_X1");
        qobject_cast<ComponentOptionEnum *>(options.get("operation"))->set("add");
        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterDynamic(options));
        QVERIFY(filter.get());
        TestController controller;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "Q_X1"), &controller);
        QVERIFY(controller.contents.empty());
        {
            std::unique_ptr<SegmentProcessingStage> filter2;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2.reset(component->deserializeBasicFilter(stream));
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2.get());
            filter2->unhandled(SequenceName("brw", "raw", "Q_X1"), &controller);
            QVERIFY(controller.contents.empty());
        }

        SequenceName a("brw", "raw", "T_V11");
        SequenceName out("brw", "raw", "ZOut_X1");

        filter->unhandled(a, &controller);
        QCOMPARE((int) controller.contents.size(), 1);
        int set_1 = controller.find({out}, {a});
        QVERIFY(set_1 != -1);

        data.setStart(15.0);
        data.setEnd(16.0);

        data.setValue(a, Variant::Root(20.0));
        filter->process(set_1, data);
        QCOMPARE(data.value(a).toReal(), 20.0);
        QCOMPARE(data.value(out).toReal(), 22.0);

        {
            std::unique_ptr<SegmentProcessingStage> filter2;
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2.reset(component->deserializeBasicFilter(stream));
            }
            QVERIFY(filter2.get());
            data.setValue(out, Variant::Root());
            data.setValue(a, Variant::Root(20.0));
            filter->process(set_1, data);
            QCOMPARE(data.value(out).toReal(), 22.0);
        }

        data.setValue(a, Variant::Root(15.0));
        filter->process(set_1, data);
        QCOMPARE(data.value(out).toReal(), 17.0);

        filter->processMeta(set_1, data);
        QCOMPARE(data.value(out.toMeta()).metadata("Processing").getType(), Variant::Type::Array);
    }

    void fanout()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<DynamicInputOption *>(options.get("a"))->set(
                SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("T_V11")),
                Calibration());
        qobject_cast<DynamicInputOption *>(options.get("b"))->set(2.0);
        qobject_cast<DynamicInputOption *>(options.get("c"))->set(3.0);
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("output"))->set({}, {},
                                                                                   "ZOut_X1");
        qobject_cast<ComponentOptionEnum *>(options.get("operation"))->set("multiplyall");
        qobject_cast<ComponentOptionSingleString *>(options.get("fanout-variable"))->set("T_V11");

        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterDynamic(options));
        QVERIFY(filter.get());
        TestController controller;
        SequenceSegment data;

        SequenceName a_1("brw", "raw", "T_V11");
        SequenceName out_1("brw", "raw", "ZOut_X1");
        SequenceName a_2("brw", "raw", "T_V11", {"pm1"});
        SequenceName out_2("brw", "raw", "ZOut_X1", {"pm1"});

        filter->unhandled(a_1, &controller);
        QCOMPARE((int) controller.contents.size(), 1);
        int set_1 = controller.find({out_1}, {a_1});
        QVERIFY(set_1 != -1);

        data.setStart(15.0);
        data.setEnd(16.0);

        data.setValue(a_1, Variant::Root(4.0));
        filter->process(set_1, data);
        QCOMPARE(data.value(a_1).toReal(), 4.0);
        QCOMPARE(data.value(out_1).toReal(), 24.0);

        filter->unhandled(out_2, &controller);
        filter->unhandled(a_2, &controller);
        QCOMPARE((int) controller.contents.size(), 2);
        int set_2 = controller.find({out_2}, {a_2});
        QVERIFY(set_2 != -1);

        data.setValue(a_2, Variant::Root(5.0));
        filter->process(set_2, data);
        QCOMPARE(data.value(a_2).toReal(), 5.0);
        QCOMPARE(data.value(out_2).toReal(), 30.0);

        data.setValue(a_1, Variant::Root(4.0));
        filter->process(set_1, data);
        QCOMPARE(data.value(a_1).toReal(), 4.0);
        QCOMPARE(data.value(out_1).toReal(), 24.0);

        data.setValue(a_2, Variant::Root());
        filter->process(set_2, data);
        QVERIFY(!FP::defined(data.value(out_2).toReal()));
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["S1/Output"] = "::ZOut_X1:=";
        cv["S1/A"] = "::ZIn_X1:=";
        cv["S1/B"] = 10.0;
        cv["S1/Operation"] = "log";
        cv["S1/Calibration/#0"] = 0.5;
        cv["S1/Calibration/#1"] = 1.0;
        config.emplace_back(FP::undefined(), 13.0, cv);
        cv.write().setEmpty();
        cv["S1/Output"] = "::ZOut_X1:=";
        cv["S1/A"] = "::ZIn_X1:=";
        cv["S1/B"] = "::ZBase_X1:=";
        cv["S1/Operation"] = "log";
        cv["S1/Calibration/#0"] = 0.5;
        cv["S1/Calibration/#1"] = 1.0;
        config.emplace_back(13.0, FP::undefined(), cv);

        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config));
        QVERIFY(filter.get());
        TestController controller;

        SequenceName out("brw", "raw", "ZOut_X1");
        SequenceName a("brw", "raw", "ZIn_X1");
        SequenceName b("brw", "raw", "ZBase_X1");

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{a, b}));
        QCOMPARE(filter->predictedOutputs(), (SequenceName::Set{out}));

        filter->unhandled(out, &controller);
        filter->unhandled(b, &controller);
        filter->unhandled(a, &controller);

        int id = controller.find({out}, {a, b});
        QVERIFY(id != -1);

        QCOMPARE(filter->metadataBreaks(id), QSet<double>() << 13.0);

        SequenceSegment data1;
        data1.setStart(12.0);
        data1.setEnd(13.0);

        data1.setValue(out, Variant::Root(10.0));
        data1.setValue(a, Variant::Root(100.0));
        filter->process(id, data1);
        QCOMPARE(data1.value(out).toReal(), 2.5);

        data1.setValue(out, Variant::Root());
        data1.setValue(a, Variant::Root(-2.0));
        filter->process(id, data1);
        QVERIFY(!FP::defined(data1.value(out).toReal()));

        filter->processMeta(id, data1);
        QCOMPARE(data1.value(out.toMeta()).metadata("Processing").getType(), Variant::Type::Array);


        SequenceSegment data2;
        data2.setStart(13.0);
        data2.setEnd(15.0);

        data2.setValue(out, Variant::Root(5.0));
        data2.setValue(a, Variant::Root(10.0));
        data2.setValue(b, Variant::Root(10.0));
        filter->process(id, data2);
        QCOMPARE(data2.value(out).toReal(), 1.5);

        data2.setValue(out, Variant::Root());
        data2.setValue(a, Variant::Root(1024.0));
        data2.setValue(b, Variant::Root(16.0));
        filter->process(id, data2);
        QCOMPARE(data2.value(out).toReal(), 3.0);

        data2.setValue(out, Variant::Root(5.0));
        data2.setValue(a, Variant::Root());
        data2.setValue(b, Variant::Root(10.0));
        filter->process(id, data2);
        QVERIFY(!FP::defined(data1.value(out).toReal()));

        data2.setValue(out, Variant::Root(5.0));
        data2.setValue(a, Variant::Root(10.0));
        data2.setValue(b, Variant::Root());
        filter->process(id, data2);
        QVERIFY(!FP::defined(data1.value(out).toReal()));

        filter->processMeta(id, data2);
        QCOMPARE(data2.value(out.toMeta()).metadata("Processing").getType(), Variant::Type::Array);
    }

    void operations()
    {
        QFETCH(QString, operation);
        QFETCH(double, a);
        QFETCH(double, b);
        QFETCH(double, c);
        QFETCH(double, out);

        ComponentOptions options(component->getOptions());
        qobject_cast<DynamicInputOption *>(options.get("a"))->set(a);
        qobject_cast<DynamicInputOption *>(options.get("b"))->set(b);
        qobject_cast<DynamicInputOption *>(options.get("c"))->set(c);
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("output"))->set({}, {},
                                                                                   "ZOut_X1");
        qobject_cast<ComponentOptionEnum *>(options.get("operation"))->set(operation);
        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterDynamic(options));
        QVERIFY(filter.get());
        TestController controller;
        SequenceSegment data;

        SequenceName outputName("brw", "raw", "ZOut_X1");

        filter->unhandled(outputName, &controller);
        QCOMPARE((int) controller.contents.size(), 1);
        int set_1 = controller.find({outputName}, {});
        QVERIFY(set_1 != -1);

        data.setStart(15.0);
        data.setEnd(16.0);

        filter->process(set_1, data);
        if (!FP::defined(out)) {
            QVERIFY(!FP::defined(data.value(outputName).toReal()));
        } else {
            QCOMPARE(data.value(outputName).toReal(), out);
        }

        {
            std::unique_ptr<SegmentProcessingStage> filter2;
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2.reset(component->deserializeBasicFilter(stream));
            }
            QVERIFY(filter2.get());
            data.setValue(outputName, Variant::Root());
            filter->process(set_1, data);
            if (!FP::defined(out)) {
                QVERIFY(!FP::defined(data.value(outputName).toReal()));
            } else {
                QCOMPARE(data.value(outputName).toReal(), out);
            }
        }
    }

    void operations_data()
    {
        QTest::addColumn<QString>("operation");
        QTest::addColumn<double>("a");
        QTest::addColumn<double>("b");
        QTest::addColumn<double>("c");
        QTest::addColumn<double>("out");

        QTest::newRow("Noop simple") << "none" << 1.0 << FP::undefined() << FP::undefined() << 1.0;
        QTest::newRow("Noop invalid") << "none" << FP::undefined() << FP::undefined()
                                      << FP::undefined() << FP::undefined();

        QTest::newRow("Add simple") << "add" << 1.0 << 2.0 << FP::undefined() << 3.0;
        QTest::newRow("Add invalid 1") << "add" << 1.0 << FP::undefined() << FP::undefined()
                                       << FP::undefined();
        QTest::newRow("Add invalid 2") << "add" << FP::undefined() << 1.0 << FP::undefined()
                                       << FP::undefined();

        QTest::newRow("Subtract simple") << "subtract" << 1.0 << 3.0 << FP::undefined() << -2.0;
        QTest::newRow("Subtract invalid 1") << "subtract" << 1.0 << FP::undefined()
                                            << FP::undefined() << FP::undefined();
        QTest::newRow("Subtract invalid 2") << "subtract" << FP::undefined() << 1.0
                                            << FP::undefined() << FP::undefined();

        QTest::newRow("Multiply simple") << "multiply" << 2.0 << 3.0 << FP::undefined() << 6.0;
        QTest::newRow("Multiply invalid 1") << "multiply" << 1.0 << FP::undefined()
                                            << FP::undefined() << FP::undefined();
        QTest::newRow("Multiply invalid 2") << "multiply" << FP::undefined() << 1.0
                                            << FP::undefined() << FP::undefined();

        QTest::newRow("Divide simple") << "divide" << 6.0 << 2.0 << FP::undefined() << 3.0;
        QTest::newRow("Divide invalid 1") << "divide" << 1.0 << FP::undefined() << FP::undefined()
                                          << FP::undefined();
        QTest::newRow("Divide invalid 2") << "divide" << FP::undefined() << 1.0 << FP::undefined()
                                          << FP::undefined();
        QTest::newRow("Divide invalid 3") << "divide" << 1.0 << 0.0 << FP::undefined()
                                          << FP::undefined();

        QTest::newRow("Power simple") << "power" << 2.0 << 3.0 << FP::undefined() << 8.0;
        QTest::newRow("Power one") << "power" << 5.0 << 0.0 << FP::undefined() << 1.0;
        QTest::newRow("Power zero") << "power" << 0.0 << 5.0 << FP::undefined() << 0.0;
        QTest::newRow("Power invalid 1") << "power" << 1.0 << FP::undefined() << FP::undefined()
                                         << FP::undefined();
        QTest::newRow("Power invalid 2") << "power" << FP::undefined() << 1.0 << FP::undefined()
                                         << FP::undefined();
        QTest::newRow("Power invalid 3") << "power" << 0.0 << 0.0 << FP::undefined()
                                         << FP::undefined();

        QTest::newRow("Logarithm simple") << "logarithm" << 100.0 << 10.0 << FP::undefined() << 2.0;
        QTest::newRow("Logarithm invalid 1") << "logarithm" << 1.0 << FP::undefined()
                                             << FP::undefined() << FP::undefined();
        QTest::newRow("Logarithm invalid 2") << "logarithm" << FP::undefined() << 1.0
                                             << FP::undefined() << FP::undefined();
        QTest::newRow("Logarithm invalid 3") << "logarithm" << 1.0 << 0.0 << FP::undefined()
                                             << FP::undefined();
        QTest::newRow("Logarithm invalid 4") << "logarithm" << 0.0 << 1.0 << FP::undefined()
                                             << FP::undefined();

        QTest::newRow("Add all simple") << "addall" << 1.0 << 2.0 << 3.0 << 6.0;
        QTest::newRow("Add all invalid 1") << "addall" << 1.0 << 1.0 << FP::undefined()
                                           << FP::undefined();
        QTest::newRow("Add all invalid 2") << "addall" << 1.0 << FP::undefined() << 1.0
                                           << FP::undefined();
        QTest::newRow("Add all invalid 3") << "addall" << FP::undefined() << 1.0 << 1.0
                                           << FP::undefined();

        QTest::newRow("Subtract all simple") << "subtractall" << 10.0 << 2.0 << 3.0 << 5.0;
        QTest::newRow("Subtract all invalid 1") << "subtractall" << 1.0 << 1.0 << FP::undefined()
                                                << FP::undefined();
        QTest::newRow("Subtract all invalid 2") << "subtractall" << 1.0 << FP::undefined() << 1.0
                                                << FP::undefined();
        QTest::newRow("Subtract all invalid 3") << "subtractall" << FP::undefined() << 1.0 << 1.0
                                                << FP::undefined();

        QTest::newRow("Multiply all simple") << "multiplyall" << 2.0 << 3.0 << 4.0 << 24.0;
        QTest::newRow("Multiply all invalid 1") << "multiplyall" << 1.0 << 1.0 << FP::undefined()
                                                << FP::undefined();
        QTest::newRow("Multiply all invalid 2") << "multiplyall" << 1.0 << FP::undefined() << 1.0
                                                << FP::undefined();
        QTest::newRow("Multiply all invalid 3") << "multiplyall" << FP::undefined() << 1.0 << 1.0
                                                << FP::undefined();

        QTest::newRow("Divide all simple") << "divideall" << 64.0 << 2.0 << 4.0 << 8.0;
        QTest::newRow("Divide all invalid 1") << "divideall" << 1.0 << 1.0 << FP::undefined()
                                              << FP::undefined();
        QTest::newRow("Divide all invalid 2") << "divideall" << 1.0 << FP::undefined() << 1.0
                                              << FP::undefined();
        QTest::newRow("Divide all invalid 3") << "divideall" << FP::undefined() << 1.0 << 1.0
                                              << FP::undefined();
        QTest::newRow("Divide all invalid 4") << "divideall" << 1.0 << 1.0 << 0.0
                                              << FP::undefined();
        QTest::newRow("Divide all invalid 5") << "divideall" << 1.0 << 0.0 << 1.0
                                              << FP::undefined();

        QTest::newRow("exp simple") << "exp" << 2.0 << FP::undefined() << FP::undefined()
                                    << 7.38905609893065;
        QTest::newRow("exp invalid") << "exp" << FP::undefined() << FP::undefined()
                                     << FP::undefined() << FP::undefined();

        QTest::newRow("ln simple") << "ln" << 5.0 << FP::undefined() << FP::undefined()
                                   << 1.6094379124341003;
        QTest::newRow("ln invalid 1") << "ln" << FP::undefined() << FP::undefined()
                                      << FP::undefined() << FP::undefined();
        QTest::newRow("ln invalid 2") << "ln" << 0.0 << FP::undefined() << FP::undefined()
                                      << FP::undefined();
        QTest::newRow("ln invalid 3") << "ln" << -1.0 << FP::undefined() << FP::undefined()
                                      << FP::undefined();

        QTest::newRow("log10 simple") << "log10" << 100.0 << FP::undefined() << FP::undefined()
                                      << 2.0;
        QTest::newRow("log10 invalid 1") << "log10" << FP::undefined() << FP::undefined()
                                         << FP::undefined() << FP::undefined();
        QTest::newRow("log10 invalid 2") << "log10" << 0.0 << FP::undefined() << FP::undefined()
                                         << FP::undefined();
        QTest::newRow("log10 invalid 3") << "log10" << -1.0 << FP::undefined() << FP::undefined()
                                         << FP::undefined();

        QTest::newRow("sin simple") << "sin" << 2.0 << FP::undefined() << FP::undefined()
                                    << 0.9092974268256817;
        QTest::newRow("sin invalid") << "sin" << FP::undefined() << FP::undefined()
                                     << FP::undefined() << FP::undefined();

        QTest::newRow("cos simple") << "cos" << 2.0 << FP::undefined() << FP::undefined()
                                    << -0.4161468365471424;
        QTest::newRow("cos invalid") << "cos" << FP::undefined() << FP::undefined()
                                     << FP::undefined() << FP::undefined();

        QTest::newRow("tan simple") << "tan" << 2.0 << FP::undefined() << FP::undefined()
                                    << -2.185039863261519;
        QTest::newRow("tan invalid") << "tan" << FP::undefined() << FP::undefined()
                                     << FP::undefined() << FP::undefined();

        QTest::newRow("asin simple") << "asin" << 0.5 << FP::undefined() << FP::undefined()
                                     << 0.5235987755982989;
        QTest::newRow("asin invalid") << "asin" << FP::undefined() << FP::undefined()
                                      << FP::undefined() << FP::undefined();

        QTest::newRow("acos simple") << "acos" << 0.5 << FP::undefined() << FP::undefined()
                                     << 1.0471975511965979;
        QTest::newRow("acos invalid") << "acos" << FP::undefined() << FP::undefined()
                                      << FP::undefined() << FP::undefined();

        QTest::newRow("atan simple") << "atan" << 2.0 << FP::undefined() << FP::undefined()
                                     << 1.1071487177940904;
        QTest::newRow("atan invalid") << "atan" << FP::undefined() << FP::undefined()
                                      << FP::undefined() << FP::undefined();

        QTest::newRow("atan2 simple") << "atan2" << 2.0 << 3.0 << FP::undefined()
                                      << 0.5880026035475675;
        QTest::newRow("atan2 invalid 1") << "atan2" << 2.0 << FP::undefined() << FP::undefined()
                                         << FP::undefined();
        QTest::newRow("atan2 invalid 2") << "atan2" << FP::undefined() << 3.0 << FP::undefined()
                                         << FP::undefined();

        QTest::newRow("abs simple") << "abs" << -2.0 << FP::undefined() << FP::undefined() << 2.0;
        QTest::newRow("abs noop") << "abs" << 2.0 << FP::undefined() << FP::undefined() << 2.0;
        QTest::newRow("abs invalid") << "abs" << FP::undefined() << FP::undefined()
                                     << FP::undefined() << FP::undefined();

        QTest::newRow("sin scaled simple") << "sinscaled" << 2.0 << 3.0 << FP::undefined()
                                           << 2.727892280477045;
        QTest::newRow("sin scaled invalid 1") << "sinscaled" << 2.0 << FP::undefined()
                                              << FP::undefined() << FP::undefined();
        QTest::newRow("sin scaled invalid 2") << "sinscaled" << FP::undefined() << 3.0
                                              << FP::undefined() << FP::undefined();

        QTest::newRow("cos scaled simple") << "cosscaled" << 2.0 << 3.0 << FP::undefined()
                                           << -1.2484405096414273;
        QTest::newRow("cos scaled invalid 1") << "cosscaled" << 2.0 << FP::undefined()
                                              << FP::undefined() << FP::undefined();
        QTest::newRow("cos scaled invalid 2") << "cosscaled" << FP::undefined() << 3.0
                                              << FP::undefined() << FP::undefined();

        QTest::newRow("Ratio sum simple") << "ratiosum" << 30.0 << 2.0 << 4.0 << 5.0;
        QTest::newRow("Ratio sum invalid 1") << "ratiosum" << 1.0 << 1.0 << FP::undefined()
                                             << FP::undefined();
        QTest::newRow("Ratio sum invalid 2") << "ratiosum" << 1.0 << FP::undefined() << 1.0
                                             << FP::undefined();
        QTest::newRow("Ratio sum invalid 3") << "ratiosum" << FP::undefined() << 1.0 << 1.0
                                             << FP::undefined();
        QTest::newRow("Ratio sum invalid 4") << "ratiosum" << 1.0 << 1.0 << -1.0 << FP::undefined();

        QTest::newRow("Sum ratio simple") << "sumratio" << 7.0 << 5.0 << 2.0 << 6.0;
        QTest::newRow("Sum ratio invalid 1") << "sumratio" << 1.0 << 1.0 << FP::undefined()
                                             << FP::undefined();
        QTest::newRow("Sum ratio invalid 2") << "sumratio" << 1.0 << FP::undefined() << 1.0
                                             << FP::undefined();
        QTest::newRow("Sum ratio invalid 3") << "sumratio" << FP::undefined() << 1.0 << 1.0
                                             << FP::undefined();
        QTest::newRow("Sum ratio invalid 4") << "sumratio" << 1.0 << 1.0 << 0.0 << FP::undefined();

        QTest::newRow("Multiply add simple") << "multiplyadd" << 2.0 << 3.0 << 4.0 << 10.0;
        QTest::newRow("Multiply add invalid 1") << "multiplyadd" << 1.0 << 1.0 << FP::undefined()
                                                << FP::undefined();
        QTest::newRow("Multiply add invalid 2") << "multiplyadd" << 1.0 << FP::undefined() << 1.0
                                                << FP::undefined();
        QTest::newRow("Multiply add invalid 3") << "multiplyadd" << FP::undefined() << 1.0 << 1.0
                                                << FP::undefined();

        QTest::newRow("Add multiply simple") << "addmultiply" << 2.0 << 3.0 << 4.0 << 14.0;
        QTest::newRow("Add multiply invalid 1") << "addmultiply" << 1.0 << 1.0 << FP::undefined()
                                                << FP::undefined();
        QTest::newRow("Add multiply invalid 2") << "addmultiply" << 1.0 << FP::undefined() << 1.0
                                                << FP::undefined();
        QTest::newRow("Add multiply invalid 3") << "addmultiply" << FP::undefined() << 1.0 << 1.0
                                                << FP::undefined();

        QTest::newRow("Wrap none") << "wrap" << 2.0 << 5.0 << FP::undefined() << 2.0;
        QTest::newRow("Wrap upper") << "wrap" << 7.0 << 5.0 << FP::undefined() << 2.0;
        QTest::newRow("Wrap lower") << "wrap" << -2.0 << 5.0 << FP::undefined() << 3.0;
        QTest::newRow("Wrap invalid 1") << "wrap" << 1.0 << FP::undefined() << FP::undefined()
                                        << FP::undefined();
        QTest::newRow("Wrap invalid 2") << "wrap" << FP::undefined() << 1.0 << FP::undefined()
                                        << FP::undefined();

        QTest::newRow("Wrap range none") << "wraprange" << 2.0 << 1.0 << 4.0 << 2.0;
        QTest::newRow("Wrap range upper") << "wraprange" << 7.0 << 1.0 << 4.0 << 1.0;
        QTest::newRow("Wrap range lower") << "wraprange" << -1.5 << 1.0 << 4.0 << 1.5;
        QTest::newRow("Wrap range invalid 1") << "wraprange" << 1.0 << 1.0 << FP::undefined()
                                              << FP::undefined();
        QTest::newRow("Wrap range invalid 2") << "wraprange" << 1.0 << FP::undefined() << 1.0
                                              << FP::undefined();
        QTest::newRow("Wrap range invalid 3") << "wraprange" << FP::undefined() << 1.0 << 1.0
                                              << FP::undefined();
        QTest::newRow("Wrap range invalid 4") << "wraprange" << 0.0 << 1.0 << 1.0
                                              << FP::undefined();

        QTest::newRow("Max first") << "max" << 3.0 << 2.0 << FP::undefined() << 3.0;
        QTest::newRow("Max second") << "max" << 1.0 << 2.0 << FP::undefined() << 2.0;
        QTest::newRow("Max invalid 1") << "max" << 1.0 << FP::undefined() << FP::undefined()
                                       << FP::undefined();
        QTest::newRow("Max invalid 2") << "max" << FP::undefined() << 1.0 << FP::undefined()
                                       << FP::undefined();

        QTest::newRow("Min first") << "min" << 2.0 << 3.0 << FP::undefined() << 2.0;
        QTest::newRow("Min second") << "min" << 2.0 << 1.0 << FP::undefined() << 1.0;
        QTest::newRow("Min invalid 1") << "min" << 1.0 << FP::undefined() << FP::undefined()
                                       << FP::undefined();
        QTest::newRow("Min invalid 2") << "min" << FP::undefined() << 1.0 << FP::undefined()
                                       << FP::undefined();

        QTest::newRow("Limit none") << "limit" << 2.0 << 1.0 << 3.0 << 2.0;
        QTest::newRow("Limit lower") << "limit" << -1.0 << 1.0 << 3.0 << 1.0;
        QTest::newRow("Limit upper") << "limit" << 4.0 << 1.0 << 3.0 << 3.0;
        QTest::newRow("Limit invalid 1") << "limit" << 1.0 << 1.0 << FP::undefined()
                                         << FP::undefined();
        QTest::newRow("Limit invalid 2") << "limit" << 1.0 << FP::undefined() << 1.0
                                         << FP::undefined();
        QTest::newRow("Limit invalid 3") << "limit" << FP::undefined() << 1.0 << 1.0
                                         << FP::undefined();

        QTest::newRow("Undefine below none") << "undefinedbelow" << 3.0 << 2.0 << FP::undefined()
                                             << 3.0;
        QTest::newRow("Undefine below apply") << "undefinedbelow" << 1.0 << 2.0 << FP::undefined()
                                              << FP::undefined();
        QTest::newRow("Undefine below invalid 1") << "undefinedbelow" << 1.0 << FP::undefined()
                                                  << FP::undefined() << FP::undefined();
        QTest::newRow("Undefine below invalid 2") << "undefinedbelow" << FP::undefined() << 1.0
                                                  << FP::undefined() << FP::undefined();

        QTest::newRow("Undefine above none") << "undefinedabove" << 1.0 << 2.0 << FP::undefined()
                                             << 1.0;
        QTest::newRow("Undefine above apply") << "undefinedabove" << 3.0 << 2.0 << FP::undefined()
                                              << FP::undefined();
        QTest::newRow("Undefine above invalid 1") << "undefinedabove" << 1.0 << FP::undefined()
                                                  << FP::undefined() << FP::undefined();
        QTest::newRow("Undefine above invalid 2") << "undefinedabove" << FP::undefined() << 1.0
                                                  << FP::undefined() << FP::undefined();

        QTest::newRow("Undefine outside none") << "undefinedoutisde" << 3.0 << 2.0 << 4.0 << 3.0;
        QTest::newRow("Undefine outside lower") << "undefinedoutisde" << 1.0 << 2.0 << 4.0
                                                << FP::undefined();
        QTest::newRow("Undefine outside upper") << "undefinedoutisde" << 5.0 << 2.0 << 4.0
                                                << FP::undefined();
        QTest::newRow("Undefine outside invalid 1") << "undefinedabove" << 3.0 << 2.0
                                                    << FP::undefined() << FP::undefined();
        QTest::newRow("Undefine outside invalid 2") << "undefinedabove" << 3.0 << FP::undefined()
                                                    << 4.0 << FP::undefined();
        QTest::newRow("Undefine outside invalid 3") << "undefinedabove" << FP::undefined() << 2.0
                                                    << 4.0 << FP::undefined();
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
