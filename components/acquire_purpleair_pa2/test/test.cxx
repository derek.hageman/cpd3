/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <array>
#include <cstdint>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;
    QDateTime time;

    double Ipa;
    double Ipb;
    double ZXa;
    double ZXb;
    double T;
    double U;
    double P;

    double pm1_0_cf_1_b;

    bool corruptUnpolled;

    ModelInstrument() : unpolledRemaining(FP::undefined()),
                        time(QDateTime(QDate(2010, 10, 5), QTime(0, 0, 0)))
    {
        Ipa = 200;
        Ipb = 300;
        ZXa = 10;
        ZXb = 11;
        T = 70;
        U = 40;
        P = 800;
        corruptUnpolled = false;
    }

    QString convertTimestamp(const QDateTime &time) const
    {
        QString result;
        result += QByteArray::number(time.date().year()).rightJustified(4, '0');
        result += "/";
        result += QByteArray::number(time.date().month()).rightJustified(2, '0');
        result += "/";
        result += QByteArray::number(time.date().day()).rightJustified(2, '0');
        result += "T";
        result += QByteArray::number(time.time().hour()).rightJustified(2, '0');
        result += ":";
        result += QByteArray::number(time.time().minute()).rightJustified(2, '0');
        result += ":";
        result += QByteArray::number(time.time().second()).rightJustified(2, '0');
        result += "z";
        return result;
    }

    void jsonResponse()
    {
        /* http://192.168.0.98/json?live=true
         * {"SensorId":"84:f3:eb:d8:e0:48","DateTime":"2020/10/24T16:40:13z",
         * "Geo":"PurpleAir-e048","Mem":19120,"memfrag":22,"memfb":16064,"memcs":1104,"Id":42,
         * "lat":38.426300,"lon":-122.581001,"Adc":0.02,"loggingrate":15,"place":"inside",
         * "version":"6.03","uptime":1993,"rssi":-57,"period":119,"httpsuccess":98,"httpsends":98,
         * "hardwareversion":"3.0",
         * "hardwarediscovered":"3.0+OPENLOG+15802 MB+DS3231+BME280+BME680+PMSX003-A+PMSX003-B",
         * "current_temp_f":73,"current_humidity":21,"current_dewpoint_f":31,"pressure":825.33,
         * "current_temp_f_680":74,"current_humidity_680":21,"current_dewpoint_f_680":32,
         * "pressure_680":825.04,"gas_680":0.00,"p25aqic_b":"rgb(4,228,0)","pm2.5_aqi_b":13,
         * "pm1_0_cf_1_b":2.00,"p_0_3_um_b":456.00,"pm2_5_cf_1_b":3.00,"p_0_5_um_b":138.00,
         * "pm10_0_cf_1_b":3.00,"p_1_0_um_b":24.00,"pm1_0_atm_b":2.00,"p_2_5_um_b":2.00,
         * "pm2_5_atm_b":3.00,"p_5_0_um_b":0.00,"pm10_0_atm_b":3.00,"p_10_0_um_b":0.00,
         * "p25aqic":"rgb(19,230,0)","pm2.5_aqi":21,"pm1_0_cf_1":2.00,"p_0_3_um":522.00,
         * "pm2_5_cf_1":5.00,"p_0_5_um":152.00,"pm10_0_cf_1":6.00,"p_1_0_um":48.00,"pm1_0_atm":2.00,
         * "p_2_5_um":6.00,"pm2_5_atm":5.00,"p_5_0_um":2.00,"pm10_0_atm":6.00,"p_10_0_um":2.00,
         * "pa_latency":183,"response":201,"response_date":1603557533,"latency":379,
         * "key1_response":200,"key1_response_date":1603557527,"key1_count":701,"ts_latency":417,
         * "key2_response":200,"key2_response_date":1603557529,"key2_count":701,"ts_s_latency":405,
         * "key1_response_b":200,"key1_response_date_b":1603557530,"key1_count_b":702,
         * "ts_latency_b":399,"key2_response_b":200,"key2_response_date_b":1603557531,
         * "key2_count_b":701,"ts_s_latency_b":411,"wlstate":"Connected","status_0":2,
         * "status_1":2,"status_2":2,"status_3":2,"status_4":2,"status_5":2,"status_6":2,"status_7":0,
         * "status_8":2,"status_9":2,"ssid":"Purple"}
         */
        QJsonObject obj;

        obj["SensorId"] = "84:f3:eb:d8:e0:48";
        obj["DateTime"] = convertTimestamp(time);
        obj["Adc"] = 0.02;
        obj["version"] = "6.03";
        obj["hardwareversion"] = "3.0";
        obj["hardwarediscovered"] = "3.0+OPENLOG+15802 MB+DS3231+BME280+BME680+PMSX003-A+PMSX003-B";

        obj["current_temp_f"] = T;
        obj["current_humidity"] = U;
        obj["pressure"] = P;
        obj["p_0_3_um"] = Ipa;
        obj["p_0_3_um_b"] = Ipb;
        obj["pm2_5_cf_1"] = ZXa;
        obj["pm2_5_cf_1_b"] = ZXb;
        obj["pm1_0_cf_1_b"] = std::floor(pm1_0_cf_1_b + 0.5);

        QJsonDocument doc;
        doc.setObject(obj);
        outgoing += doc.toJson(QJsonDocument::Compact);
        outgoing += "\n";
    }

    void csvResponse()
    {
        /* UTCDateTime,mac_address,firmware_ver,hardware,current_temp_f,current_humidity,
         * current_dewpoint_f,pressure,adc,mem,rssi,uptime,pm1_0_cf_1,pm2_5_cf_1,pm10_0_cf_1,
         * pm1_0_atm,pm2_5_atm,pm10_0_atm,pm2.5_aqi_cf_1,pm2.5_aqi_atm,p_0_3_um,p_0_5_um,
         * p_1_0_um,p_2_5_um,p_5_0_um,p_10_0_um,pm1_0_cf_1_b,pm2_5_cf_1_b,pm10_0_cf_1_b,
         * pm1_0_atm_b,pm2_5_atm_b,pm10_0_atm_b,pm2.5_aqi_cf_1_b,pm2.5_aqi_atm_b,p_0_3_um_b,
         * p_0_5_um_b,p_1_0_um_b,p_2_5_um_b,p_5_0_um_b,p_10_0_um_b,gas */

        QByteArray data;

        data += convertTimestamp(time).toLatin1();
        data +=
                ",84:f3:eb:d8:e0:48,6.03,3.0+OPENLOG+15802 MB+DS3231+BME280+BME680+PMSX003-A+PMSX003-B,";
        data += QByteArray::number(T, 'f', 2);
        data += ",";
        data += QByteArray::number(U, 'f', 2);
        //data += ",current_dewpoint_f,";
        data += ",34.12,";
        data += QByteArray::number(P, 'f', 2);
        //data += ",1234,mem,rssi,uptime,pm1_0_cf_1,";
        data += ",0.02,36456,0,820,0.00,";
        data += QByteArray::number(ZXa, 'f', 2);
        //data += ",pm10_0_cf_1,pm1_0_atm,pm2_5_atm,pm10_0_atm,pm2.5_aqi_cf_1,pm2.5_aqi_atm,";
        data += ",0.00,0.00,0.00,0.00,0.00,0.00,";
        data += QByteArray::number(Ipa, 'f', 2);
        //data += ",p_0_5_um,p_1_0_um,p_2_5_um,p_5_0_um,p_10_0_um,";
        data += ",0.00,0.00,0.00,0.00,0.00,";
        data += QByteArray::number(std::floor(pm1_0_cf_1_b + 0.5), 'f', 0);
        data += ",";
        data += QByteArray::number(ZXb, 'f', 2);
        //data += ",pm10_0_cf_1_b,pm1_0_atm_b,pm2_5_atm_b,pm10_0_atm_b,pm2.5_aqi_cf_1_b,pm2.5_aqi_atm_b,";
        data += ",0.00,0.00,0.00,0.00,0.00,0.00,";
        data += QByteArray::number(Ipb, 'f', 2);
        //data += ",p_0_3_um_b,p_0_5_um_b,p_1_0_um_b,p_2_5_um_b,p_5_0_um_b,p_10_0_um_b,gas";
        data += ",0.00,0.00,0.00,0.00,0.00,0.00,0.00";

        if (corruptUnpolled) {
            outgoing += "\x1A\x1A\x1A";
            outgoing += "\ndisk\rls\nappend 20201230.csv\n";
        }
        if (corruptUnpolled && data.size() >= 227) {
            data.replace(227, 10, "append 20201229.csv");
        }
        outgoing += data;
        if (corruptUnpolled) {
            outgoing += "\x1A\x1A\x1A";
            outgoing += "append 20201230.log\n";
        }
        outgoing += "\n";
    }

    void advance(double seconds)
    {
        time = time.addMSecs(static_cast<qint64>(std::ceil(seconds * 1000.0)));

        pm1_0_cf_1_b += seconds;

        if (FP::defined(unpolledRemaining)) {
            incoming.clear();
            unpolledRemaining -= seconds;
            while (unpolledRemaining <= 0.0) {
                unpolledRemaining += 120.0;
                csvResponse();
            }
        } else {
            if (!incoming.isEmpty()) {
                incoming.clear();
                jsonResponse();
            }
        }
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("Ipa", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("Ipb", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("ZXa", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("ZXb", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("U", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("P", Variant::Root(), {}, time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZSTATE", Variant::Root(), {}, time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("Ipa"))
            return false;
        if (!stream.checkContiguous("Ipb"))
            return false;
        if (!stream.checkContiguous("ZXa"))
            return false;
        if (!stream.checkContiguous("ZXb"))
            return false;
        if (!stream.checkContiguous("T"))
            return false;
        if (!stream.checkContiguous("U"))
            return false;
        if (!stream.checkContiguous("P"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("T", Variant::Root((model.T - 32.0) * 5.0 / 9.0), time))
            return false;
        if (!stream.hasAnyMatchingValue("U", Variant::Root(model.U), time))
            return false;
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.P), time))
            return false;
        if (!stream.hasAnyMatchingValue("Ipa", Variant::Root(model.Ipa), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZXa", Variant::Root(model.ZXa), time))
            return false;

        if (model.corruptUnpolled)
            return true;

        if (!stream.hasAnyMatchingValue("Ipb", Variant::Root(model.Ipb), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZXb", Variant::Root(model.ZXb), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_purpleair_pa2"));
        QVERIFY(component);

        QLocale::setDefault(QLocale::C);
        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAcquisitionJSON()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get(), 0.0,
                                                        AcquisitionInterface::IncomingDataType::Complete);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.externalControl("\n");
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));
        control.advance(0.1);

        for (int i = 0; i < 300; i++) {
            control.externalControl("\n");
            control.advance(0.5);
            QTest::qSleep(10);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
    }

    void interactiveAcquisitionJSON()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get(), 0.0,
                                                        AcquisitionInterface::IncomingDataType::Complete);
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));

        for (int i = 0; i < 300; i++) {
            control.advance(0.5);
            QTest::qSleep(10);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
    }


    void passiveAutoprobeCSV()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(5.0);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(5.0);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
    }

    void interactiveAutoprobeCSV()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(5.0);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(5.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
    }

    void passiveAcquisitionCSV()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(5.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));
        control.advance(0.1);

        for (int i = 0; i < 50; i++) {
            control.advance(5.0);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
    }

    void interactiveAcquisitionCSV()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(5.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));

        for (int i = 0; i < 50; i++) {
            control.advance(5.0);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
    }

    void passiveAutoprobeCSVCorrupt()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.0;
        instrument.corruptUnpolled = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(5.0);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(5.0);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
