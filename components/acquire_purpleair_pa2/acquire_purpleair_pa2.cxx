/*
 * Copyright (c) 2020 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "core/csv.hxx"
#include "core/timeutils.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_purpleair_pa2.hxx"


using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

static const std::array<QString, 37> trackUniqueKeys
        {"Adc", "current_temp_f", "current_humidity", "current_dewpoint_f", "pressure",
         "current_temp_f_680", "current_humidity_680", "current_dewpoint_f_680", "pressure_680",
         "gas_680", "pm2.5_aqi_b", "pm1_0_cf_1_b", "p_0_3_um_b", "pm2_5_cf_1_b", "p_0_5_um_b",
         "pm10_0_cf_1_b", "p_1_0_um_b", "pm1_0_atm_b", "p_2_5_um_b", "pm2_5_atm_b", "p_5_0_um_b",
         "pm10_0_atm_b", "p_10_0_um_b", "p25aqic", "pm2.5_aqi", "pm1_0_cf_1", "p_0_3_um",
         "pm2_5_cf_1", "p_0_5_um", "pm10_0_cf_1", "p_1_0_um", "pm1_0_atm", "p_2_5_um", "pm2_5_atm",
         "p_5_0_um", "pm10_0_atm", "p_10_0_um"};


AcquirePurpleAirPA2::Configuration::Configuration() : start(FP::undefined()),
                                                      end(FP::undefined()),
                                                      pollInterval(0.5),
                                                      reportInterval(120.0),
                                                      responseType(ResponseType::Automatic)
{ }

AcquirePurpleAirPA2::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          pollInterval(other.pollInterval),
          reportInterval(other.reportInterval),
          responseType(other.responseType)
{ }

void AcquirePurpleAirPA2::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("PurpleAir");
    instrumentMeta["Model"].setString("PA-II");

    recordUniqueKeys.clear();
    autoprobeValidRecords = 0;

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    realtimeStateUpdated = false;
}

AcquirePurpleAirPA2::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          pollInterval(0.5),
          reportInterval(120.0),
          responseType(ResponseType::Automatic)
{
    setFromSegment(other);
}

AcquirePurpleAirPA2::Configuration::Configuration(const Configuration &under,
                                                  const ValueSegment &over,
                                                  double s,
                                                  double e) : start(s),
                                                              end(e),
                                                              pollInterval(under.pollInterval),
                                                              reportInterval(under.reportInterval),
                                                              responseType(under.responseType)
{
    setFromSegment(over);
}

void AcquirePurpleAirPA2::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["Interface"].exists()) {
        if (Util::equal_insensitive(config["Interface/Type"].toString(), "URL")) {
            responseType = ResponseType::JSON;
        }
    }

    if (config["PollInterval"].exists())
        pollInterval = config["PollInterval"].toDouble();

    {
        double v = config["ReportInterval"].toReal();
        if (FP::defined(v) && v >= 0.0)
            reportInterval = v;
    }

    if (config["ReportType"].exists()) {
        const auto &str = config["ReportType"].toString();
        if (Util::equal_insensitive(str, "JSON")) {
            responseType = ResponseType::JSON;
        } else if (Util::equal_insensitive(str, "CSV")) {
            responseType = ResponseType::CSV;
        } else {
            responseType = ResponseType::Automatic;
        }
    }
}

AcquirePurpleAirPA2::AcquirePurpleAirPA2(const ValueSegment::Transfer &configData,
                                         const std::string &loggingContext) : FramedInstrument(
        "pa2", loggingContext),
                                                                              lastRecordTime(
                                                                                      FP::undefined()),
                                                                              autoprobeStatus(
                                                                                      AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                              autoprobeValidRecords(
                                                                                      0),
                                                                              responseState(
                                                                                      ResponseState::Passive_Initialize)
{
    setDefaultInvalid();
    config.emplace_back();

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquirePurpleAirPA2::setToAutoprobe()
{ responseState = ResponseState::Autoprobe_Passive_Initialize; }

void AcquirePurpleAirPA2::setToInteractive()
{ responseState = ResponseState::Interactive_Initialize; }


ComponentOptions AcquirePurpleAirPA2Component::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquirePurpleAirPA2::AcquirePurpleAirPA2(const ComponentOptions &,
                                         const std::string &loggingContext) : FramedInstrument(
        "pa2", loggingContext),
                                                                              lastRecordTime(
                                                                                      FP::undefined()),
                                                                              autoprobeStatus(
                                                                                      AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                              autoprobeValidRecords(
                                                                                      0),
                                                                              responseState(
                                                                                      ResponseState::Passive_Initialize)
{
    setDefaultInvalid();
    config.emplace_back();
}

AcquirePurpleAirPA2::~AcquirePurpleAirPA2() = default;

void AcquirePurpleAirPA2::configurationAdvance(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.empty());
}

SequenceValue::Transfer AcquirePurpleAirPA2::buildLogMeta(double time) const
{
    Q_ASSERT(!config.empty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_purpleair_pa2");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    if (instrumentMeta["HardwareVersion"].exists())
        processing["HardwareVersion"].set(instrumentMeta["HardwareVersion"]);
    if (instrumentMeta["SensorHardware"].exists())
        processing["SensorHardware"].set(instrumentMeta["SensorHardware"]);
    if (instrumentMeta["MACAddress"].exists())
        processing["MACAddress"].set(instrumentMeta["MACAddress"]);

    result.emplace_back(SequenceName({}, "raw_meta", "Ipa"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Description").setString("Detector A intensity count");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("A"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);

    result.emplace_back(SequenceName({}, "raw_meta", "Ipb"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Description").setString("Detector B intensity count");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("B"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);

    result.emplace_back(SequenceName({}, "raw_meta", "ZXa"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Detector A reported PM2.5 concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("PM2.5 A"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);

    result.emplace_back(SequenceName({}, "raw_meta", "ZXb"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Detector B reported PM2.5 concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("PM2.5 B"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(3);

    result.emplace_back(SequenceName({}, "raw_meta", "T"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Description").setString("Sensor temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Temperature"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(4);

    result.emplace_back(SequenceName({}, "raw_meta", "U"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Sensor RH");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Humidity"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Description").setString("Ambient pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Pressure"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(6);

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    return result;
}

SequenceValue::Transfer AcquirePurpleAirPA2::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_purpleair_pa2");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    if (instrumentMeta["HardwareVersion"].exists())
        processing["HardwareVersion"].set(instrumentMeta["HardwareVersion"]);
    if (instrumentMeta["SensorHardware"].exists())
        processing["SensorHardware"].set(instrumentMeta["SensorHardware"]);
    if (instrumentMeta["MACAddress"].exists())
        processing["MACAddress"].set(instrumentMeta["MACAddress"]);


    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Name").setString(std::string());
    result.back().write().metadataString("Realtime").hash("Units").setString(std::string());
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataString("Realtime").hash("Hide").setBool(true);
    /*result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidRecord")
          .setString(QObject::tr("NO COMMS: Invalid record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));*/

    return result;
}

SequenceMatch::Composite AcquirePurpleAirPA2::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    return sel;
}

void AcquirePurpleAirPA2::logValue(double startTime,
                                   double endTime,
                                   SequenceName::Component name,
                                   Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquirePurpleAirPA2::realtimeValue(double time,
                                        SequenceName::Component name,
                                        Variant::Root &&value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

void AcquirePurpleAirPA2::invalidateLogValues(double frameTime)
{
    recordUniqueKeys.clear();
    autoprobeValidRecords = 0;
    lastRecordTime = FP::undefined();
    loggingLost(frameTime);
}

std::size_t AcquirePurpleAirPA2::dataFrameStart(std::size_t offset,
                                                const Util::ByteArray &input) const
{
    auto max = input.size();
    while (offset < max) {
        auto ch = input[offset];
        if (ch == '\r' || ch == '\n' || ch == 0x1A) {
            offset++;
            continue;
        }
        return offset;
    }
    return input.npos;
}

std::size_t AcquirePurpleAirPA2::dataFrameEnd(std::size_t start, const Util::ByteArray &input) const
{
    for (auto max = input.size(); start < max; start++) {
        auto ch = input[start];
        if (ch == '\r' || ch == '\n' || ch == 0x1A)
            return start;
    }
    return input.npos;
}

int AcquirePurpleAirPA2::processJSONRecord(const Util::ByteView &data, double &frameTime)
{
    Q_ASSERT(!config.empty());

    if (data.size() < 3)
        return 1;

    QJsonParseError err;
    QJsonDocument doc(QJsonDocument::fromJson(data.toQByteArrayRef(), &err));
    if (err.error != QJsonParseError::NoError)
        return 100 + static_cast<int>(err.error);

    QJsonObject obj(doc.object());
    bool ok = false;

    if (!obj.contains("DateTime")) return 2;
    auto field = obj.value("DateTime").toString().trimmed();
    auto dateFields = field.split('T', QString::KeepEmptyParts, Qt::CaseInsensitive);
    if (dateFields.size() != 2) return 3;
    auto dateString = dateFields[0];
    if (dateString.isEmpty()) return 4;
    auto timeString = dateFields[1];
    if (timeString.endsWith('z', Qt::CaseInsensitive)) timeString.chop(1);
    if (timeString.isEmpty()) return 5;

    auto subfields = dateString.split('/');
    if (subfields.size() != 3) return 6;

    Q_ASSERT(!subfields.empty());
    qint64 iyear = subfields.takeFirst().trimmed().toInt(&ok);
    if (!ok) return 7;
    if (iyear < 2005) return 8;
    if (iyear > 2999) return 9;
    Variant::Root year(iyear);
    remap("YEAR", year);
    iyear = year.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 imonth = subfields.takeFirst().trimmed().toInt(&ok);
    if (!ok) return 10;
    if (imonth < 1) return 11;
    if (imonth > 12) return 12;
    Variant::Root month(imonth);
    remap("MONTH", month);
    imonth = month.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 iday = subfields.takeFirst().trimmed().toInt(&ok);
    if (!ok) return 13;
    if (iday < 1) return 14;
    if (iday > 31) return 15;
    Variant::Root day(iday);
    remap("DAY", day);
    iday = day.read().toInt64();

    subfields = timeString.split(':');
    if (subfields.size() != 3) return 16;

    Q_ASSERT(!subfields.empty());
    qint64 ihour = subfields.takeFirst().trimmed().toInt(&ok);
    if (!ok) return 17;
    if (ihour < 0) return 18;
    if (ihour > 23) return 19;
    Variant::Root hour(ihour);
    remap("HOUR", hour);
    ihour = hour.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 iminute = subfields.takeFirst().trimmed().toInt(&ok);
    if (!ok) return 20;
    if (iminute < 0) return 21;
    if (iminute > 59) return 22;
    Variant::Root minute(iminute);
    remap("MINUTE", minute);
    iminute = minute.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 isecond = subfields.takeFirst().trimmed().toInt(&ok);
    if (!ok) return 23;
    if (isecond < 0) return 24;
    if (isecond > 60) return 25;
    Variant::Root second(isecond);
    remap("SECOND", second);
    isecond = second.read().toInt64();

    if (!FP::defined(frameTime) &&
            INTEGER::defined(iyear) &&
            iyear >= 1900 &&
            iyear <= 2999 &&
            INTEGER::defined(imonth) &&
            imonth >= 1 &&
            imonth <= 12 &&
            INTEGER::defined(iday) &&
            iday >= 1 &&
            iday <= 31 &&
            INTEGER::defined(ihour) &&
            ihour >= 0 &&
            ihour <= 23 &&
            INTEGER::defined(iminute) &&
            iminute >= 0 &&
            iminute <= 59 &&
            INTEGER::defined(isecond) &&
            isecond >= 0 &&
            isecond <= 60) {
        frameTime = Time::fromDateTime(QDateTime(
                QDate(static_cast<int>(iyear), static_cast<int>(imonth), static_cast<int>(iday)),
                QTime(static_cast<int>(ihour), static_cast<int>(iminute),
                      static_cast<int>(isecond)), Qt::UTC));
        configurationAdvance(frameTime);
    }
    if (FP::defined(lastRecordTime) && FP::defined(frameTime) && frameTime < lastRecordTime)
        return -1;

    if (!obj.contains("current_temp_f")) return 26;
    Variant::Root T;
    {
        double v = obj.value("current_temp_f").toDouble(FP::undefined());
        if (!FP::defined(v)) return 27;
        v = (v - 32.0) * 5.0 / 9.0;
        T.write().setReal(v);
    }
    remap("T", T);

    if (!obj.contains("current_humidity")) return 28;
    Variant::Root U(obj.value("current_humidity").toDouble(FP::undefined()));
    if (!FP::defined(U.read().toReal())) return 29;
    remap("U", U);

    if (!obj.contains("pressure")) return 30;
    Variant::Root P(obj.value("pressure").toDouble(FP::undefined()));
    if (!FP::defined(P.read().toReal())) return 31;
    remap("P", P);

    if (!obj.contains("p_0_3_um")) return 32;
    Variant::Root Ipa(obj.value("p_0_3_um").toDouble(FP::undefined()));
    if (!FP::defined(Ipa.read().toReal())) return 33;
    remap("Ipa", Ipa);

    if (!obj.contains("p_0_3_um_b")) return 34;
    Variant::Root Ipb(obj.value("p_0_3_um_b").toDouble(FP::undefined()));
    if (!FP::defined(Ipb.read().toReal())) return 35;
    remap("Ipb", Ipb);

    if (!obj.contains("pm2_5_cf_1")) return 36;
    Variant::Root ZXa(obj.value("pm2_5_cf_1").toDouble(FP::undefined()));
    if (!FP::defined(ZXa.read().toReal())) return 37;
    remap("ZXa", ZXa);

    if (!obj.contains("pm2_5_cf_1_b")) return 38;
    Variant::Root ZXb(obj.value("pm2_5_cf_1_b").toDouble(FP::undefined()));
    if (!FP::defined(ZXb.read().toReal())) return 39;
    remap("ZXb", ZXb);

    double measuredElapsed = frameTime - lastRecordTime;
    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;

    if (obj.contains("SensorId")) {
        auto str = obj.value("SensorId").toString();
        if (str != instrumentMeta["MACAddress"].toQString()) {
            haveEmittedRealtimeMeta = false;
            haveEmittedLogMeta = false;
            instrumentMeta["MACAddress"].setString(str);
        }
    }
    if (obj.contains("version")) {
        auto str = obj.value("version").toString();
        if (str != instrumentMeta["FirmwareVersion"].toQString()) {
            haveEmittedRealtimeMeta = false;
            haveEmittedLogMeta = false;
            instrumentMeta["FirmwareVersion"].setString(str);
        }
    }
    if (obj.contains("hardwareversion")) {
        auto str = obj.value("hardwareversion").toString();
        if (str != instrumentMeta["HardwareVersion"].toQString()) {
            haveEmittedRealtimeMeta = false;
            haveEmittedLogMeta = false;
            instrumentMeta["HardwareVersion"].setString(str);
        }
    }
    if (obj.contains("hardwarediscovered")) {
        auto str = obj.value("hardwarediscovered").toString();
        if (str != instrumentMeta["SensorHardware"].toQString()) {
            haveEmittedRealtimeMeta = false;
            haveEmittedLogMeta = false;
            instrumentMeta["SensorHardware"].setString(str);
        }
    }

    std::unordered_map<QString, QVariant> uniqueKeys;
    for (const auto &key : trackUniqueKeys) {
        if (!obj.contains(key))
            continue;
        uniqueKeys.emplace(key, obj.value(key).toVariant());
    }
    if (uniqueKeys == recordUniqueKeys && measuredElapsed < config.front().reportInterval)
        return -1;

    if (!FP::defined(startTime) || !FP::defined(endTime))
        return 0;

    if (!haveEmittedLogMeta && loggingEgress) {
        haveEmittedLogMeta = true;

        loggingEgress->incomingData(buildLogMeta(startTime));
    }

    if (!haveEmittedRealtimeMeta && realtimeEgress) {
        haveEmittedRealtimeMeta = true;

        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    logValue(startTime, endTime, "F1", Variant::Root(Variant::Type::Flags));
    logValue(startTime, endTime, "Ipa", std::move(Ipa));
    logValue(startTime, endTime, "Ipb", std::move(Ipb));
    logValue(startTime, endTime, "ZXa", std::move(ZXa));
    logValue(startTime, endTime, "ZXb", std::move(ZXb));
    logValue(startTime, endTime, "T", std::move(T));
    logValue(startTime, endTime, "U", std::move(U));
    logValue(startTime, endTime, "P", std::move(P));


    if (realtimeStateUpdated && realtimeEgress && FP::defined(frameTime)) {
        realtimeStateUpdated = false;

        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                              FP::undefined()));
    }

    return 0;
}

int AcquirePurpleAirPA2::processCSVRecord(const Util::ByteView &line, double &frameTime)
{
    Q_ASSERT(!config.empty());

    if (line.string_start("disk") || line.string_start("ls") || line.string_start("append "))
        return -2;

    if (line.size() < 3)
        return 1;

    auto fields = Util::as_deque(line.split(','));
    bool ok = false;

    if (fields.empty()) return 2;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    auto dateFields = field.split('T');
    if (dateFields.size() != 2) dateFields = field.split('t');
    if (dateFields.size() != 2) return 3;
    auto dateString = dateFields[0];
    if (dateString.empty()) return 4;
    auto timeString = dateFields[1];
    if (!timeString.empty() && (timeString.back() == 'z' || timeString.back() == 'Z')) {
        timeString = timeString.mid(0, timeString.size() - 1);
    }
    if (timeString.empty()) return 5;

    auto subfields = Util::as_deque(dateString.split('/'));
    if (subfields.size() != 3) return 6;

    Q_ASSERT(!subfields.empty());
    auto iyear = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 7;
    if (iyear < 2005) return 8;
    if (iyear > 2999) return 9;
    Variant::Root year(iyear);
    remap("YEAR", year);
    iyear = year.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 imonth = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 10;
    if (imonth < 1) return 11;
    if (imonth > 12) return 12;
    Variant::Root month(imonth);
    remap("MONTH", month);
    imonth = month.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 iday = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 13;
    if (iday < 1) return 14;
    if (iday > 31) return 15;
    Variant::Root day(iday);
    remap("DAY", day);
    iday = day.read().toInt64();

    subfields = Util::as_deque(timeString.split(':'));
    if (subfields.size() != 3) return 16;

    Q_ASSERT(!subfields.empty());
    qint64 ihour = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 17;
    if (ihour < 0) return 18;
    if (ihour > 23) return 19;
    Variant::Root hour(ihour);
    remap("HOUR", hour);
    ihour = hour.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 iminute = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 20;
    if (iminute < 0) return 21;
    if (iminute > 59) return 22;
    Variant::Root minute(iminute);
    remap("MINUTE", minute);
    iminute = minute.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 isecond = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 23;
    if (isecond < 0) return 24;
    if (isecond > 60) return 25;
    Variant::Root second(isecond);
    remap("SECOND", second);
    isecond = second.read().toInt64();

    if (!FP::defined(frameTime) &&
            INTEGER::defined(iyear) &&
            iyear >= 1900 &&
            iyear <= 2999 &&
            INTEGER::defined(imonth) &&
            imonth >= 1 &&
            imonth <= 12 &&
            INTEGER::defined(iday) &&
            iday >= 1 &&
            iday <= 31 &&
            INTEGER::defined(ihour) &&
            ihour >= 0 &&
            ihour <= 23 &&
            INTEGER::defined(iminute) &&
            iminute >= 0 &&
            iminute <= 59 &&
            INTEGER::defined(isecond) &&
            isecond >= 0 &&
            isecond <= 60) {
        frameTime = Time::fromDateTime(QDateTime(
                QDate(static_cast<int>(iyear), static_cast<int>(imonth), static_cast<int>(iday)),
                QTime(static_cast<int>(ihour), static_cast<int>(iminute),
                      static_cast<int>(isecond)), Qt::UTC));
        configurationAdvance(frameTime);
    }
    if (FP::defined(lastRecordTime) && FP::defined(frameTime) && frameTime < lastRecordTime)
        return -1;

    if (fields.empty()) return 26;
    auto mac_address = fields.front().string_trimmed();
    fields.pop_front();
    if (mac_address.empty()) return 27;
    {
        int n = 0;
        for (Util::ByteView::size_type start = 0;; ++n) {
            start = mac_address.indexOf(':', start);
            if (start == mac_address.npos)
                break;
            ++start;
        }
        if (n != 5) return 28;
    }

    if (fields.empty()) return 29;
    auto firmware_version = fields.front().string_trimmed();
    fields.pop_front();
    if (firmware_version.empty()) return 30;

    if (fields.empty()) return 31;
    auto hardware_discovered = fields.front().string_trimmed();
    fields.pop_front();
    if (hardware_discovered.empty()) return 32;
    if (hardware_discovered.indexOf('+') == hardware_discovered.npos) return 33;

    int validFieldsRemaining = 0;
    for (const auto &check : fields) {
        auto idxA = check.indexOf('a');
        if (idxA != check.npos) {
            if (check.mid(idxA).string_start("append"))
                break;
        }
        ++validFieldsRemaining;
    }

    if (fields.empty()) return 34;
    Variant::Root T;
    bool T_valid = false;
    if (--validFieldsRemaining >= 0) {
        double v = fields.front().string_trimmed().parse_real(&ok);
        fields.pop_front();
        if (!ok) return 35;
        if (!FP::defined(v)) return 36;
        v = (v - 32.0) * 5.0 / 9.0;
        T.write().setReal(v);
        remap("T", T);
        T_valid = true;
    }

    Variant::Root U;
    bool U_valid = false;
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 37;
        field = fields.front().string_trimmed();
        fields.pop_front();
        U.write().setReal(field.parse_real(&ok));
        if (!ok) return 38;
        if (!FP::defined(U.read().toReal())) return 39;
        remap("U", U);
        U_valid = true;
    }

    /* current_dewpoint_f */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 40;
        fields.pop_front();
    }

    Variant::Root P;
    bool P_valid = false;
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 41;
        field = fields.front().string_trimmed();
        fields.pop_front();
        P.write().setReal(field.parse_real(&ok));
        if (!ok) return 42;
        if (!FP::defined(P.read().toReal())) return 43;
        remap("U", P);
        P_valid = true;
    }

    /* adc */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 44;
        fields.pop_front();
    }

    /* mem */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 45;
        fields.pop_front();
    }

    /* rssi */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 46;
        fields.pop_front();
    }

    /* uptime */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 47;
        fields.pop_front();
    }

    /* pm1_0_cf_1 */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 48;
        fields.pop_front();
    }

    Variant::Root ZXa;
    bool ZXa_valid = false;
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 49;
        field = fields.front().string_trimmed();
        fields.pop_front();
        ZXa.write().setReal(field.parse_real(&ok));
        if (!ok) return 50;
        if (!FP::defined(ZXa.read().toReal())) return 51;
        remap("ZXa", ZXa);
        ZXa_valid = true;
    }

    /* pm10_0_cf_1 */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 52;
        fields.pop_front();
    }

    /* pm1_0_atm */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 53;
        fields.pop_front();
    }

    /* pm2_5_atm */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 54;
        fields.pop_front();
    }

    /* pm10_0_atm */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 55;
        fields.pop_front();
    }

    /* pm2.5_aqi_cf_1 */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 56;
        fields.pop_front();
    }

    /* pm2.5_aqi_atm */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 57;
        fields.pop_front();
    }

    Variant::Root Ipa;
    bool Ipa_valid = false;
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 58;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Ipa.write().setReal(field.parse_real(&ok));
        if (!ok) return 59;
        if (!FP::defined(Ipa.read().toReal())) return 60;
        remap("Ipa", Ipa);
        Ipa_valid = true;
    }

    /* p_0_5_um */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 61;
        fields.pop_front();
    }

    /* p_1_0_um */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 62;
        fields.pop_front();
    }

    /* p_2_5_um */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 63;
        fields.pop_front();
    }

    /* p_5_0_um */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 64;
        fields.pop_front();
    }

    /* p_10_0_um */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 65;
        fields.pop_front();
    }

    /* pm1_0_cf_1_b */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 66;
        fields.pop_front();
    }

    Variant::Root ZXb;
    bool ZXb_valid = false;
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 67;
        field = fields.front().string_trimmed();
        fields.pop_front();
        ZXb.write().setReal(field.parse_real(&ok));
        if (!ok) return 68;
        if (!FP::defined(ZXb.read().toReal())) return 69;
        remap("ZXb", ZXb);
        ZXb_valid = true;
    }

    /* pm10_0_cf_1_b */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 70;
        fields.pop_front();
    }

    /* pm1_0_atm_b */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 71;
        fields.pop_front();
    }

    /* pm2_5_atm_b */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 72;
        fields.pop_front();
    }

    /* pm10_0_atm_b */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 73;
        fields.pop_front();
    }

    /* pm2.5_aqi_cf_1_b */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 74;
        fields.pop_front();
    }

    /* pm2.5_aqi_atm_b */
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 75;
        fields.pop_front();
    }

    Variant::Root Ipb;
    bool Ipb_valid = false;
    if (--validFieldsRemaining >= 0) {
        if (fields.empty()) return 76;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Ipb.write().setReal(field.parse_real(&ok));
        if (!ok) return 77;
        if (!FP::defined(Ipb.read().toReal())) return 78;
        remap("Ipb", Ipb);
        Ipb_valid = true;
    }

    /* Don't even try and enforce strictness due to partially garbled records */

    if (!Ipa_valid && !Ipb_valid && !ZXa_valid && !ZXb_valid && !T_valid && !U_valid && !P_valid)
        return -3;


    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;

    if (!mac_address.empty()) {
        auto str = mac_address.toString();
        if (str != instrumentMeta["MACAddress"].toString()) {
            haveEmittedRealtimeMeta = false;
            haveEmittedLogMeta = false;
            instrumentMeta["MACAddress"].setString(std::move(str));
        }
    }
    if (!firmware_version.empty()) {
        auto str = firmware_version.toString();
        if (str != instrumentMeta["FirmwareVersion"].toString()) {
            haveEmittedRealtimeMeta = false;
            haveEmittedLogMeta = false;
            instrumentMeta["FirmwareVersion"].setString(std::move(str));
        }
    }
    if (!hardware_discovered.empty()) {
        auto str = hardware_discovered.toString();
        if (str != instrumentMeta["SensorHardware"].toString()) {
            haveEmittedRealtimeMeta = false;
            haveEmittedLogMeta = false;
            instrumentMeta["SensorHardware"].setString(std::move(str));
        }
    }

    if (!FP::defined(startTime) || !FP::defined(endTime))
        return 0;

    if (!haveEmittedLogMeta && loggingEgress) {
        haveEmittedLogMeta = true;

        loggingEgress->incomingData(buildLogMeta(startTime));
    }

    if (!haveEmittedRealtimeMeta && realtimeEgress) {
        haveEmittedRealtimeMeta = true;

        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    logValue(startTime, endTime, "F1", Variant::Root(Variant::Type::Flags));
    if (Ipa_valid) logValue(startTime, endTime, "Ipa", std::move(Ipa));
    if (Ipb_valid) logValue(startTime, endTime, "Ipb", std::move(Ipb));
    if (ZXa_valid) logValue(startTime, endTime, "ZXa", std::move(ZXa));
    if (ZXb_valid) logValue(startTime, endTime, "ZXb", std::move(ZXb));
    if (T_valid) logValue(startTime, endTime, "T", std::move(T));
    if (U_valid) logValue(startTime, endTime, "U", std::move(U));
    if (P_valid) logValue(startTime, endTime, "P", std::move(P));


    if (realtimeStateUpdated && realtimeEgress && FP::defined(frameTime)) {
        realtimeStateUpdated = false;

        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                              FP::undefined()));
    }

    return 0;
}

int AcquirePurpleAirPA2::processRecord(const Util::ByteArray &data, double &frameTime)
{
    switch (config.front().responseType) {
    case Configuration::ResponseType::Automatic:
        break;
    case Configuration::ResponseType::JSON: {
        int rc = processJSONRecord(data, frameTime);
        if (rc > 0)
            rc += 1000;
        return rc;
    }
    case Configuration::ResponseType::CSV: {
        int rc = processCSVRecord(data, frameTime);
        if (rc > 0)
            rc += 2000;
        return rc;
    }
    }

    {
        Util::ByteView tr(data);
        tr.string_trimmed();
        if (tr.empty())
            return 1;
        switch (tr.front()) {
        case '{': {
            int rc = processJSONRecord(tr, frameTime);
            if (rc > 0)
                rc += 3000;
            return rc;
        }
        case '2': {
            int rc = processCSVRecord(tr, frameTime);
            if (rc > 0)
                rc += 4000;
            return rc;
        }
        default:
            break;
        }
    }

    int rc = processJSONRecord(data, frameTime);
    if (rc < 0)
        return rc;
    if (rc == 0)
        return 0;

    rc = processCSVRecord(data, frameTime);
    if (rc > 0)
        rc += 5000;
    return rc;
}

double AcquirePurpleAirPA2::responseTimeout(bool expectPassive) const
{
    Q_ASSERT(!config.empty());

    double waitTime = config.front().reportInterval;
    if (!FP::defined(waitTime) || waitTime < 0.0)
        waitTime = 5.0;
    if (expectPassive)
        return waitTime;

    switch (config.front().responseType) {
    case Configuration::ResponseType::Automatic:
    case Configuration::ResponseType::CSV:
        return waitTime * 2.0 + 1.0;
    case Configuration::ResponseType::JSON:
        return 10.0;
    }
    return 10.0;
}

void AcquirePurpleAirPA2::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    configurationAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Passive_Run: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + responseTimeout(true));
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            timeoutAt(FP::undefined());
            responseState = ResponseState::Passive_Wait;
            generalStatusUpdated();

            info.hash("Code").setInteger(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 3) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                timeoutAt(frameTime + responseTimeout(true));

                responseState = ResponseState::Passive_Run;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                realtimeStateUpdated = true;

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case ResponseState::Passive_Initialize:
    case ResponseState::Passive_Wait: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);
            responseState = ResponseState::Passive_Run;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            timeoutAt(frameTime + responseTimeout(true));

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }


    case ResponseState::Interactive_Start_Read_Data: {
        recordUniqueKeys.clear();
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            if (FP::defined(config.front().pollInterval) && config.front().pollInterval > 0.0) {
                responseState = ResponseState::Interactive_Run_Wait;
                timeoutAt(frameTime + config.front().pollInterval * 2.0 + 1.0);
                discardData(frameTime + config.front().pollInterval);
            } else {
                responseState = ResponseState::Interactive_Run_Data;
                timeoutAt(frameTime + responseTimeout());
                if (controlStream)
                    controlStream->writeControl("\n");
            }

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            responseState = ResponseState::Interactive_Restart_Wait;
            autoprobeValidRecords = 0;
            if (controlStream)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case ResponseState::Interactive_Run_Data:
    case ResponseState::Interactive_Run_Wait:
    case ResponseState::Interactive_Run_Retry: {
        int code = processRecord(frame, frameTime);
        if (code == 0 || code == -1) {
            if (FP::defined(config.front().pollInterval) && config.front().pollInterval > 0.0) {
                responseState = ResponseState::Interactive_Run_Wait;
                timeoutAt(frameTime + config.front().pollInterval * 2.0 + 1.0);
                discardData(frameTime + config.front().pollInterval);
            } else {
                responseState = ResponseState::Interactive_Run_Data;
                timeoutAt(frameTime + responseTimeout());
                if (controlStream)
                    controlStream->writeControl("\n");
            }
        } else if (code > 0) {
            qCDebug(log) << "Response at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            responseState = ResponseState::Interactive_Start_Flush;
            timeoutAt(frameTime + 10.0);
            discardData(frameTime + 1.0);

            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("RunData");
            info.hash("Code").setInteger(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
            break;
        }

        break;
    }
    default:
        break;
    }
}

void AcquirePurpleAirPA2::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_Read_Data:
        qCDebug(log) << "Timeout in interactive start state" << static_cast<int>(responseState)
                     << "at" << Logging::time(frameTime);

        invalidateLogValues(frameTime);

        responseState = ResponseState::Interactive_Restart_Wait;
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);
        autoprobeValidRecords = 0;
        generalStatusUpdated();

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        break;


    case ResponseState::Passive_Run: {
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = ResponseState::Passive_Wait;
        generalStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;
    }

    case ResponseState::Interactive_Run_Data:
    case ResponseState::Interactive_Run_Wait:
        if (controlStream) {
            qCDebug(log) << "Retrying after timeout in interactive run state"
                         << static_cast<int>(responseState) << "at" << Logging::time(frameTime);

            responseState = ResponseState::Interactive_Run_Retry;
            timeoutAt(frameTime + responseTimeout());
            controlStream->writeControl("\n");
            break;
        }
        /* Fall through */
    case ResponseState::Interactive_Run_Retry: {
        qCDebug(log) << "Timeout in interactive run state" << static_cast<int>(responseState)
                     << "at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);

        generalStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            switch (responseState) {
            case ResponseState::Interactive_Run_Data:
                info.hash("ResponseState").setString("ReadData");
                break;
            case ResponseState::Interactive_Run_Wait:
                info.hash("ResponseState").setString("PollWait");
                break;
            default:
                break;
            }
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;
    }

    case ResponseState::Passive_Initialize:
    case ResponseState::Passive_Wait:
        responseState = ResponseState::Passive_Wait;
        autoprobeValidRecords = 0;
        invalidateLogValues(frameTime);
        break;

    case ResponseState::Autoprobe_Passive_Wait: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case ResponseState::Autoprobe_Passive_Initialize:
        responseState = ResponseState::Autoprobe_Passive_Wait;
        autoprobeValidRecords = 0;
        invalidateLogValues(frameTime);
        break;

    case ResponseState::Interactive_Restart_Wait:
    case ResponseState::Interactive_Initialize:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);

        generalStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;
    }
}

void AcquirePurpleAirPA2::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);
    Q_ASSERT(!config.empty());

    switch (responseState) {
    case ResponseState::Interactive_Start_Flush:
        responseState = ResponseState::Interactive_Start_Read_Data;

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadDatabaseVer"), frameTime, FP::undefined()));
        }

        timeoutAt(frameTime + responseTimeout());
        if (controlStream)
            controlStream->writeControl("\n");
        break;

    case ResponseState::Interactive_Restart_Wait:
    case ResponseState::Interactive_Initialize:
        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 5.0);

        if (controlStream) {
            controlStream->resetControl();
        }

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case ResponseState::Interactive_Run_Wait:
        responseState = ResponseState::Interactive_Run_Data;
        timeoutAt(frameTime + responseTimeout());
        if (controlStream)
            controlStream->writeControl("\n");
        break;

    default:
        break;
    }
}

void AcquirePurpleAirPA2::incomingControlFrame(const Util::ByteArray &, double)
{ }


Variant::Root AcquirePurpleAirPA2::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquirePurpleAirPA2::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case ResponseState::Passive_Run:
    case ResponseState::Interactive_Run_Data:
    case ResponseState::Interactive_Run_Wait:
    case ResponseState::Interactive_Run_Retry:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

AcquisitionInterface::AutoprobeStatus AcquirePurpleAirPA2::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquirePurpleAirPA2::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case ResponseState::Interactive_Run_Data:
    case ResponseState::Interactive_Run_Wait:
    case ResponseState::Interactive_Run_Retry:
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << static_cast<int>(responseState) << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_Read_Data:
    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        qCDebug(log) << "Interactive autoprobe started from state"
                     << static_cast<int>(responseState) << "at" << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 10.0);
        discardData(time + 1.0);

        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquirePurpleAirPA2::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    Q_ASSERT(!config.empty());

    discardData(time + 1.0, 1);
    double waitTime = config.front().reportInterval;
    if (!FP::defined(waitTime) || waitTime < 0.0)
        waitTime = 5.0;
    timeoutAt(time + waitTime * 5.0 + 1.0);

    qCDebug(log) << "Reset to passive autoprobe from state" << static_cast<int>(responseState)
                 << "at" << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = ResponseState::Autoprobe_Passive_Wait;
    autoprobeValidRecords = 0;
    generalStatusUpdated();
}

void AcquirePurpleAirPA2::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 2.0);

    switch (responseState) {
    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_Read_Data:
    case ResponseState::Interactive_Run_Data:
    case ResponseState::Interactive_Run_Wait:
    case ResponseState::Interactive_Run_Retry:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << static_cast<int>(responseState)
                     << "to interactive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        qCDebug(log) << "Promoted from passive state" << static_cast<int>(responseState)
                     << "to interactive acquisition at" << Logging::time(time);

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 10.0);
        discardData(time + 1.0);

        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquirePurpleAirPA2::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    Q_ASSERT(!config.empty());
    timeoutAt(time + responseTimeout(true));

    switch (responseState) {
    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from autoprobe passive state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        responseState = ResponseState::Passive_Initialize;
        break;

    case ResponseState::Interactive_Run_Data:
    case ResponseState::Interactive_Run_Wait:
    case ResponseState::Interactive_Run_Retry:
        discardData(FP::undefined());

        responseState = ResponseState::Passive_Run;
        generalStatusUpdated();

        /* Already in unpolled, so do nothing */
        qCDebug(log) << "Promoted from unpolled state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_Read_Data:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        /* We didn't have full communications, so just drop it and start
         * over. */
        qCDebug(log) << "Promoted from state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);

        discardData(FP::undefined());

        responseState = ResponseState::Passive_Initialize;
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquirePurpleAirPA2::getDefaults()
{
    AutomaticDefaults result;
    result.name = "S$1$2";
    result.setSerialN81(115200);
    result.autoprobeLevelPriority = 30;
    return result;
}


ComponentOptions AcquirePurpleAirPA2Component::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);
    return options;
}

ComponentOptions AcquirePurpleAirPA2Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquirePurpleAirPA2Component::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples(true));
    examples.append(getPassiveExamples());
    return examples;
}

QList<ComponentExample> AcquirePurpleAirPA2Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data")));

    return examples;
}

bool AcquirePurpleAirPA2Component::requiresInputDevice()
{ return true; }

ExternalConverter *AcquirePurpleAirPA2Component::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

std::unique_ptr<
        AcquisitionInterface> AcquirePurpleAirPA2Component::createAcquisitionPassive(const ComponentOptions &options,
                                                                                     const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquirePurpleAirPA2(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquirePurpleAirPA2Component::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquirePurpleAirPA2(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquirePurpleAirPA2Component::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{
    std::unique_ptr<AcquirePurpleAirPA2> i(new AcquirePurpleAirPA2(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquirePurpleAirPA2Component::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquirePurpleAirPA2> i(new AcquirePurpleAirPA2(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
