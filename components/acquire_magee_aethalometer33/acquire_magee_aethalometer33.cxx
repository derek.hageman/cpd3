/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"
#include "algorithms/numericsolve.hxx"
#include "algorithms/leastsquares.hxx"

#include "acquire_magee_aethalometer33.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Algorithms;

static double convertWavelength(int index, int maximum)
{
    switch (maximum) {
    case 7:
        switch (index) {
        case 0:
            return 370.0;
        case 1:
            return 470.0;
        case 2:
            return 520.0;
        case 3:
            return 590.0;
        case 4:
            return 660.0;
        case 5:
            return 880.0;
        case 6:
            return 950.0;
        default:
            break;
        }
        break;
    default:
        break;
    }
    return 0.0;
}


AcquireMageeAethalometer33::Configuration::Configuration() : start(FP::undefined()),
                                                             end(FP::undefined()),
                                                             timeMode(RoundedMeasured),
                                                             setInstrumentTime(false),
                                                             absorptionEfficiency(),
                                                             area(),
                                                             weingartnerConstant(),
                                                             ATNf1(),
                                                             ATNf2(),
                                                             kMin(),
                                                             kMax(),
                                                             leakageFactor(),
                                                             timeBase(1),
                                                             wavelengths(),
                                                             flowCalibrationTotal(),
                                                             flowCalibration(),
                                                             pollInterval(0.5),
                                                             strictMode(false),
                                                             disableEBCZeroCheck(false),
                                                             realtimeDiagnostics(true)
{
    for (int color = 0; color < 7; color++) {
        wavelengths.append(convertWavelength(color, 7));
    }
}

AcquireMageeAethalometer33::Configuration::Configuration(const Configuration &other,
                                                         double s,
                                                         double e) : start(s),
                                                                     end(e),
                                                                     timeMode(other.timeMode),
                                                                     setInstrumentTime(
                                                                             other.setInstrumentTime),
                                                                     absorptionEfficiency(
                                                                             other.absorptionEfficiency),
                                                                     area(other.area),
                                                                     weingartnerConstant(
                                                                             other.weingartnerConstant),
                                                                     ATNf1(other.ATNf1),
                                                                     ATNf2(other.ATNf2),
                                                                     kMin(other.kMin),
                                                                     kMax(other.kMax),
                                                                     leakageFactor(
                                                                             other.leakageFactor),
                                                                     timeBase(other.timeBase),
                                                                     flowCalibrationTotal(
                                                                             other.flowCalibrationTotal),
                                                                     flowCalibration(
                                                                             other.flowCalibration),
                                                                     pollInterval(
                                                                             other.pollInterval),
                                                                     strictMode(other.strictMode),
                                                                     disableEBCZeroCheck(
                                                                             other.disableEBCZeroCheck),
                                                                     realtimeDiagnostics(
                                                                             other.realtimeDiagnostics)
{ }

AcquireMageeAethalometer33::Configuration::Configuration(const ValueSegment &other,
                                                         double s,
                                                         double e) : start(s),
                                                                     end(e),
                                                                     timeMode(RoundedMeasured),
                                                                     setInstrumentTime(false),
                                                                     absorptionEfficiency(),
                                                                     area(),
                                                                     weingartnerConstant(),
                                                                     ATNf1(),
                                                                     ATNf2(),
                                                                     kMin(),
                                                                     kMax(),
                                                                     leakageFactor(),
                                                                     timeBase(1),
                                                                     wavelengths(),
                                                                     flowCalibrationTotal(),
                                                                     flowCalibration(),
                                                                     pollInterval(0.5),
                                                                     strictMode(false),
                                                                     disableEBCZeroCheck(false),
                                                                     realtimeDiagnostics(true)
{
    for (int color = 0; color < 7; color++) {
        wavelengths.append(convertWavelength(color, 7));
    }
    setFromSegment(other);
}

AcquireMageeAethalometer33::Configuration::Configuration(const Configuration &under,
                                                         const ValueSegment &over,
                                                         double s,
                                                         double e) : start(s),
                                                                     end(e),
                                                                     timeMode(under.timeMode),
                                                                     setInstrumentTime(
                                                                             under.setInstrumentTime),
                                                                     absorptionEfficiency(
                                                                             under.absorptionEfficiency),
                                                                     area(under.area),
                                                                     weingartnerConstant(
                                                                             under.weingartnerConstant),
                                                                     ATNf1(under.ATNf1),
                                                                     ATNf2(under.ATNf2),
                                                                     kMin(under.kMin),
                                                                     kMax(under.kMax),
                                                                     leakageFactor(
                                                                             under.leakageFactor),
                                                                     timeBase(under.timeBase),
                                                                     wavelengths(under.wavelengths),
                                                                     flowCalibrationTotal(
                                                                             under.flowCalibrationTotal),
                                                                     flowCalibration(
                                                                             under.flowCalibration),
                                                                     pollInterval(
                                                                             under.pollInterval),
                                                                     strictMode(under.strictMode),
                                                                     disableEBCZeroCheck(
                                                                             under.disableEBCZeroCheck),
                                                                     realtimeDiagnostics(
                                                                             under.realtimeDiagnostics)
{
    setFromSegment(over);
}

void AcquireMageeAethalometer33::Configuration::setFromSegment(const ValueSegment &config)
{
    switch (config["Area"].getType()) {
    case Variant::Type::Real: {
        double d = config["Area"].toDouble();
        if (FP::defined(d) && d > 0.0) {
            if (d < 0.1)
                d *= 1E6;
        } else {
            d = FP::undefined();
        }
        area[0].write().setReal(d);
        area[1].write().setReal(d);
        break;
    }
    case Variant::Type::Array: {
        for (int spot = 0; spot < 2; spot++) {
            if (config["Area"].array(spot).exists()) {
                double d = config["Area"].array(spot).toDouble();
                if (FP::defined(d) && d > 0.0) {
                    if (d < 0.1)
                        d *= 1E6;
                } else {
                    d = FP::undefined();
                }
                area[spot].write().setReal(d);
            }
        }
        break;
    }
    default:
        break;
    }
    if (config["Area1"].exists()) {
        double d = config["Area1"].toDouble();
        if (FP::defined(d) && d > 0.0) {
            if (d <= 0.1)
                d *= 1E6;
        } else {
            d = FP::undefined();
        }
        area[0].write().setReal(d);
    }
    if (config["Area2"].exists()) {
        double d = config["Area2"].toDouble();
        if (FP::defined(d) && d > 0.0) {
            if (d <= 0.1)
                d *= 1E6;
        } else {
            d = FP::undefined();
        }
        area[1].write().setReal(d);
    }

    if (config["UseMeasuredTime"].exists()) {
        switch (config["UseMeasuredTime"].getType()) {
        case Variant::Type::String: {
            const auto &v = config["UseMeasuredTime"].toString();
            if (Util::equal_insensitive(v, "measured")) {
                timeMode = PureMeasured;
            } else if (Util::equal_insensitive(v, "rounded")) {
                timeMode = RoundedMeasured;
            } else {
                timeMode = PureReported;
            }
            break;
        }
        default:
            timeMode = config["UseMeasuredTime"].toBool() ? PureMeasured : PureReported;
            break;
        }
    }

    /* Make this optional, since it looks like it can make the instrument lock up */
    if (config["SetInstrumentTime"].exists())
        setInstrumentTime = config["SetInstrumentTime"].toBool();

    if (config["TotalFlowCalibration"].exists())
        flowCalibrationTotal = Variant::Composite::toCalibration(config["TotalFlowCalibration"]);
    if (config["Spot1FlowCalibration"].exists())
        flowCalibration[0] = Variant::Composite::toCalibration(config["Spot1FlowCalibration"]);
    if (config["Spot2FlowCalibration"].exists())
        flowCalibration[1] = Variant::Composite::toCalibration(config["Spot2FlowCalibration"]);

    if (config["Wavelengths"].exists()) {
        wavelengths.clear();
        switch (config["Wavelengths"].getType()) {
        case Variant::Type::Array: {
            for (auto add : config["Wavelengths"].toArray()) {
                double wl = add.toDouble();
                if (!FP::defined(wl) || wl <= 0.0)
                    continue;
                wavelengths.append(wl);
            }
            break;
        }
        default: {
            qint64 n = config["Wavelengths"].toInt64();
            if (INTEGER::defined(n) && n > 0) {
                for (int i = 0, max = (int) n; i < max; i++) {
                    wavelengths.append(convertWavelength(i, max));
                    if (wavelengths.last() <= 0.0)
                        wavelengths.pop_back();
                }
            }
            break;
        }
        }
    }

    switch (config["AbsorptionEfficiency"].getType()) {
    case Variant::Type::Empty:
        break;
    case Variant::Type::Hash: {
        absorptionEfficiency.clear();
        for (auto child : config["AbsorptionEfficiency"].toHash()) {
            bool ok = false;
            double wl = QString::fromStdString(child.first).toDouble(&ok);
            if (!ok || FP::defined(wl) || wl < 0.0)
                continue;
            double e = child.second.toDouble();
            if (!FP::defined(e) || e <= 0.0)
                continue;
            absorptionEfficiency.insert(wl, e);
        }
        break;
    }
    case Variant::Type::Keyframe: {
        absorptionEfficiency.clear();
        for (auto child : config["AbsorptionEfficiency"].toKeyframe()) {
            if (child.first < 0.0)
                continue;
            double e = child.second.toDouble();
            if (!FP::defined(e) || e <= 0.0)
                continue;
            absorptionEfficiency.insert(child.first, e);
        }
        break;
    }
    case Variant::Type::Array: {
        absorptionEfficiency.clear();
        auto children = config["AbsorptionEfficiency"].toArray();
        for (int idx = 0, max = children.size(); idx < max; idx++) {
            double e = children[idx].toDouble();
            if (!FP::defined(e) || e <= 0.0)
                continue;
            absorptionEfficiency.insert(convertWavelength(idx, max), e);
        }
        break;
    }
    case Variant::Type::Boolean:
        absorptionEfficiency.clear();
        if (config["AbsorptionEfficiency"].toBool())
            absorptionEfficiency.insert(0.0, 6833.0);
        break;
    default: {
        absorptionEfficiency.clear();
        double e = config["AbsorptionEfficiency"].toDouble();
        if (!FP::defined(e) || e < 0.0)
            break;
        absorptionEfficiency.insert(0.0, e);
        break;
    }
    }

    if (config["Leakage"].exists())
        leakageFactor.write().set(config["Leakage"]);
    if (config["WeingartnerConstant"].exists())
        weingartnerConstant.write().set(config["WeingartnerConstant"]);
    if (config["ATNf1"].exists())
        ATNf1.write().set(config["ATNf1"]);
    if (config["ATNf2"].exists())
        ATNf2.write().set(config["ATNf2"]);
    if (config["kMin"].exists())
        kMin.write().set(config["kMin"]);
    if (config["kMax"].exists())
        kMax.write().set(config["kMax"]);
    if (config["TimeBase"].exists())
        timeBase.write().set(config["TimeBase"]);

    if (FP::defined(config["PollInterval"].toDouble()))
        pollInterval = config["PollInterval"].toDouble();

    if (config["DisableEBCZeroCheck"].exists())
        disableEBCZeroCheck = config["DisableEBCZeroCheck"].toBool();

    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    if (config["RealtimeDiagnostics"].exists())
        realtimeDiagnostics = config["RealtimeDiagnostics"].toBool();
}


AcquireMageeAethalometer33::InstrumentParameters::InstrumentParameters() : timeBase(-1),
                                                                           efficiency(),
                                                                           area(FP::undefined()),
                                                                           isUTC(true),
                                                                           isDST(false),
                                                                           leakageFactor(
                                                                                   FP::undefined()),
                                                                           weingartnerConstant(
                                                                                   FP::undefined()),
                                                                           ATNf1(FP::undefined()),
                                                                           ATNf2(FP::undefined()),
                                                                           kMax(FP::undefined()),
                                                                           kMin(FP::undefined()),
                                                                           timeIncorrect(false)
{
}

void AcquireMageeAethalometer33::InstrumentParameters::invalidate()
{
    timeBase = -1;
    efficiency.clear();
    area = FP::undefined();
    isUTC = true;
    isDST = false;
    leakageFactor = FP::undefined();
    weingartnerConstant = FP::undefined();
    ATNf1 = FP::undefined();
    ATNf2 = FP::undefined();
    kMax = FP::undefined();
    kMin = FP::undefined();
    timeIncorrect = false;
}

bool AcquireMageeAethalometer33::InstrumentParameters::needToChange(const Configuration &target)
{
    if (timeBase > 0 &&
            INTEGER::defined(target.timeBase.read().toInt64()) &&
            target.timeBase.read().toInt64() > 0 &&
            timeBase != (int) target.timeBase.read().toInt64())
        return true;
    if ((!isUTC || isDST) && target.setInstrumentTime)
        return true;
    if (FP::defined(leakageFactor) &&
            FP::defined(target.leakageFactor.read().toDouble()) &&
            leakageFactor != target.leakageFactor.read().toDouble())
        return true;
    if (FP::defined(weingartnerConstant) &&
            FP::defined(target.weingartnerConstant.read().toDouble()) &&
            weingartnerConstant != target.weingartnerConstant.read().toDouble())
        return true;
    if (FP::defined(ATNf1) &&
            FP::defined(target.ATNf1.read().toDouble()) &&
            ATNf1 != target.ATNf1.read().toDouble())
        return true;
    if (FP::defined(ATNf2) &&
            FP::defined(target.ATNf2.read().toDouble()) &&
            ATNf2 != target.ATNf2.read().toDouble())
        return true;
    if (FP::defined(kMin) &&
            FP::defined(target.kMin.read().toDouble()) &&
            kMin != target.kMin.read().toDouble())
        return true;
    if (FP::defined(kMax) &&
            FP::defined(target.kMax.read().toDouble()) &&
            kMax != target.kMax.read().toDouble())
        return true;
    if (timeIncorrect && target.setInstrumentTime)
        return true;
    return false;
}

Util::ByteArray AcquireMageeAethalometer33::InstrumentParameters::changeCommand(const Configuration &target,
                                                                                double currentTime)
{
    if (timeBase <= 0) {
        timeBase = (int) target.timeBase.read().toInt64();
    } else if (INTEGER::defined(target.timeBase.read().toInt64()) &&
            target.timeBase.read().toInt64() > 0 &&
            timeBase != (int) target.timeBase.read().toInt64()) {
        timeBase = target.timeBase.read().toInt64();
        return Util::ByteArray("$AE33:SSTimeBase " + QByteArray::number(timeBase) + "\r");
    }

    if (!FP::defined(leakageFactor)) {
        leakageFactor = target.leakageFactor.read().toDouble();
    } else if (FP::defined(target.leakageFactor.read().toDouble()) &&
            leakageFactor != target.leakageFactor.read().toDouble()) {
        leakageFactor = target.leakageFactor.read().toDouble();
        return Util::ByteArray("$AE33:SSZeta " + QByteArray::number(leakageFactor, 'f') + "\r");
    }

    if (!FP::defined(weingartnerConstant)) {
        weingartnerConstant = target.weingartnerConstant.read().toDouble();
    } else if (FP::defined(target.weingartnerConstant.read().toDouble()) &&
            weingartnerConstant != target.weingartnerConstant.read().toDouble()) {
        weingartnerConstant = target.weingartnerConstant.read().toDouble();
        return Util::ByteArray("$AE33:SSC " + QByteArray::number(weingartnerConstant, 'f') + "\r");
    }

    if (!FP::defined(ATNf1)) {
        ATNf1 = target.ATNf1.read().toDouble();
    } else if (FP::defined(target.ATNf1.read().toDouble()) &&
            ATNf1 != target.ATNf1.read().toDouble()) {
        ATNf1 = target.ATNf1.read().toDouble();
        return Util::ByteArray("$AE33:SSATNf1 " + QByteArray::number(ATNf1, 'f') + "\r");
    }

    if (!FP::defined(ATNf2)) {
        ATNf2 = target.ATNf2.read().toDouble();
    } else if (FP::defined(target.ATNf2.read().toDouble()) &&
            ATNf2 != target.ATNf2.read().toDouble()) {
        ATNf2 = target.ATNf2.read().toDouble();
        return Util::ByteArray("$AE33:SSATNf2 " + QByteArray::number(ATNf2, 'f') + "\r");
    }

    if (!FP::defined(kMin)) {
        kMin = target.kMin.read().toDouble();
    } else if (FP::defined(target.kMin.read().toDouble()) &&
            kMin != target.kMin.read().toDouble()) {
        kMin = target.kMin.read().toDouble();
        return Util::ByteArray("$AE33:SSKmin " + QByteArray::number(kMin, 'f') + "\r");
    }

    if (!FP::defined(kMax)) {
        kMax = target.kMax.read().toDouble();
    } else if (FP::defined(target.kMax.read().toDouble()) &&
            kMax != target.kMax.read().toDouble()) {
        kMax = target.kMax.read().toDouble();
        return Util::ByteArray("$AE33:SSKmax " + QByteArray::number(kMax, 'f') + "\r");
    }

    if (!isUTC && target.setInstrumentTime) {
        isUTC = true;
        return Util::ByteArray("$AE33:SSTimeZone Coordinated Universal Time\r");
    }

    if (isDST && target.setInstrumentTime) {
        isDST = false;
        return Util::ByteArray("$AE33:SSDaylightSavingTime 0\r");
    }

    if (timeIncorrect && target.setInstrumentTime) {
        timeIncorrect = false;

        QDateTime dt(Time::toDateTime(currentTime + 0.5));
        Util::ByteArray send("$AE33:T");
        send += QByteArray::number(dt.date().year()).rightJustified(4, '0');
        send += QByteArray::number(dt.date().month()).rightJustified(2, '0');
        send += QByteArray::number(dt.date().day()).rightJustified(2, '0');
        send += QByteArray::number(dt.time().hour()).rightJustified(2, '0');
        send += QByteArray::number(dt.time().minute()).rightJustified(2, '0');
        send += QByteArray::number(dt.time().second()).rightJustified(2, '0');
        send.push_back('\r');
        return send;
    }

    Q_ASSERT(false);
    return {};
}


void AcquireMageeAethalometer33::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Magee");
    instrumentMeta["Model"].setString("AE33");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    haveEmittedParameters = false;

    parameters.invalidate();

    lastReportTime = FP::undefined();

    instrumentStatus.setUnit(SequenceName({}, "raw", "F2"));
    spotAdvance.setUnit(SequenceName({}, "raw", "Fn"));

    normalizationUpdating = false;
    normalizationLastVolume[0] = FP::undefined();
    normalizationLastVolume[1] = FP::undefined();
    normalizationLastSpotAdvance = INTEGER::undefined();
    normalizationResumeTime = FP::undefined();

    fvrfFinal = FP::undefined();
    fvrfRecalculate = true;
}


AcquireMageeAethalometer33::AcquireMageeAethalometer33(const ValueSegment::Transfer &configData,
                                                       const std::string &loggingContext)
        : FramedInstrument("ae33", loggingContext),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          responseState(RESP_PASSIVE_WAIT),
          priorValues(),
          lastReportTime(FP::undefined())
{
    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireMageeAethalometer33::setToAutoprobe()
{ responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE; }

void AcquireMageeAethalometer33::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireMageeAethalometer33Component::getBaseOptions()
{
    ComponentOptions options;

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(tr("spot1", "name"), tr("Area of spot one"),
                                            tr("This is the sampling area of the first spot.  If it is "
                                                   "greater than 0.1 it is assumed to be in mm\xC2\xB2, otherwise "
                                                   "it is treated as being in m\xC2\xB2."),
                                            tr("78.5 mm\xC2\xB2"), 1);
    d->setMinimum(0.0, false);
    options.add("area1", d);

    d = new ComponentOptionSingleDouble(tr("spot2", "name"), tr("Area of spot two"),
                                        tr("This is the sampling area of the second spot.  If it is "
                                               "greater than 0.1 it is assumed to be in mm\xC2\xB2, otherwise "
                                               "it is treated as being in m\xC2\xB2."),
                                        tr("78.5 mm\xC2\xB2"), 1);
    d->setMinimum(0.0, false);
    options.add("area2", d);

    return options;
}

AcquireMageeAethalometer33::AcquireMageeAethalometer33(const ComponentOptions &options,
                                                       const std::string &loggingContext)
        : FramedInstrument("ae33", loggingContext),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          responseState(RESP_PASSIVE_WAIT),
          priorValues(),
          lastReportTime(FP::undefined())
{
    setDefaultInvalid();

    config.append(Configuration());

    if (options.isSet("area1")) {
        double value = qobject_cast<ComponentOptionSingleDouble *>(options.get("area1"))->get();
        if (FP::defined(value) && value > 0.0) {
            if (value < 0.1)
                value *= 1E6;
            config.last().area[0].write().setReal(value);
        }
    }

    if (options.isSet("area2")) {
        double value = qobject_cast<ComponentOptionSingleDouble *>(options.get("area2"))->get();
        if (FP::defined(value) && value > 0.0) {
            if (value < 0.1)
                value *= 1E6;
            config.last().area[1].write().setReal(value);
        }
    }

    if (options.isSet("cal-q1")) {
        config.last().flowCalibration[0] =
                qobject_cast<ComponentOptionSingleCalibration *>(options.get("cal-q1"))->get();
    }
    if (options.isSet("cal-q2")) {
        config.last().flowCalibration[1] =
                qobject_cast<ComponentOptionSingleCalibration *>(options.get("cal-q2"))->get();
    }
    if (options.isSet("cal-qc")) {
        config.last().flowCalibrationTotal =
                qobject_cast<ComponentOptionSingleCalibration *>(options.get("cal-qc"))->get();
    }
}

AcquireMageeAethalometer33::~AcquireMageeAethalometer33() = default;


void AcquireMageeAethalometer33::logValue(double startTime,
                                          double endTime,
                                          SequenceName::Component name,
                                          Variant::Root value)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireMageeAethalometer33::realtimeValue(double time,
                                               SequenceName::Component name,
                                               Variant::Root value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

static double calculateEfficiency(double wavelength,
                                  const QHash<double, double> &efficiency,
                                  const QVector<double> &instrumentEfficiency,
                                  int index)
{
    if (!FP::defined(wavelength)) {
        if (index >= 0 && index < instrumentEfficiency.size())
            return instrumentEfficiency.at(index);
        return FP::undefined();
    }
    double e = efficiency.value(wavelength, 0.0);
    if (e > 0.0)
        return e;
    e = efficiency.value(0.0, 0.0);
    if (e <= 0.0) {
        if (index >= 0 && index < instrumentEfficiency.size()) {
            e = instrumentEfficiency.at(index);
            if (e > 0.0)
                return e;
        }
        e = 6833.0;
    }
    return e / wavelength;
}

static void setWavelengths(Variant::Write &target, const QVector<double> &wl)
{
    for (int color = 0, max = wl.size(); color < max; color++) {
        target.array(color).setDouble(wl.at(color));
    }
}

void setWavelengths(Variant::Write &&target, const QVector<double> &wl)
{ return setWavelengths(target, wl); }

SequenceValue::Transfer AcquireMageeAethalometer33::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_magee_aethalometer33");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["HardwareVersion"].exists())
        processing["HardwareVersion"].set(instrumentMeta["HardwareVersion"]);
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    if (instrumentMeta["SerialNumber"].exists())
        processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);

    result.emplace_back(SequenceName({}, "raw_meta", "Q1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Spot one sample flow");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Spot 1"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "Q2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Spot two sample flow");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Spot 2"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Controller temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Control"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Supply temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Supply"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "T3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("LED board temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("LED"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "Qt1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.00000");
    result.back().write().metadataReal("Units").setString("m³");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Accumulated sample volume through spot one");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");

    result.emplace_back(SequenceName({}, "raw_meta", "Qt2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.00000");
    result.back().write().metadataReal("Units").setString("m³");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Accumulated sample volume through spot two");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");

    result.emplace_back(SequenceName({}, "raw_meta", "L1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0000");
    result.back().write().metadataReal("Units").setString("m");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Integrated sample length through spot one, Qt/A");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");

    result.emplace_back(SequenceName({}, "raw_meta", "L2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0000");
    result.back().write().metadataReal("Units").setString("m");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Integrated sample length through spot two, Qt/A");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("STP")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back().write().metadataSingleFlag("STP").hash("Bits").setInt64(0x0200);
    result.back()
          .write()
          .metadataSingleFlag("STP")
          .hash("Description")
          .setString("Data reported at STP");

    result.back()
          .write()
          .metadataSingleFlag("SpotAdvanced")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("SpotAdvanced")
          .hash("Description")
          .setString("The spot has just advanced and data may be suspect");

    result.back()
          .write()
          .metadataSingleFlag("NotMeasuring")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("NotMeasuring")
          .hash("Description")
          .setString("Not measuring due to tap advance or fast calibration");
    result.back().write().metadataSingleFlag("NotMeasuring").hash("Bits").setInt64(0x00010000);

    result.back()
          .write()
          .metadataSingleFlag("Calibrating")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("Calibrating")
          .hash("Description")
          .setString("Calibration of LED, flow, and/or tape sensors in progress");
    result.back().write().metadataSingleFlag("Calibrating").hash("Bits").setInt64(0x00020000);

    result.back()
          .write()
          .metadataSingleFlag("Stopped")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("Stopped")
          .hash("Description")
          .setString("Instrument in stop mode");
    result.back().write().metadataSingleFlag("Stopped").hash("Bits").setInt64(0x00030000);

    result.back()
          .write()
          .metadataSingleFlag("FlowOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("FlowOutOfRange")
          .hash("Description")
          .setString("Flow above or below target by more than 0.25 lpm");
    result.back().write().metadataSingleFlag("FlowOutOfRange").hash("Bits").setInt64(0x00040000);

    result.back()
          .write()
          .metadataSingleFlag("FlowCheckHistory")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("FlowCheckHistory")
          .hash("Description")
          .setString("Flow check status history");
    result.back().write().metadataSingleFlag("FlowCheckHistory").hash("Bits").setInt64(0x00080000);

    result.back()
          .write()
          .metadataSingleFlag("LEDCalibration")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("LEDCalibration")
          .hash("Description")
          .setString("LED calibration in progress");
    result.back().write().metadataSingleFlag("LEDCalibration").hash("Bits").setInt64(0x00100000);

    result.back()
          .write()
          .metadataSingleFlag("LEDCalibrationError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("LEDCalibrationError")
          .hash("Description")
          .setString("LED calibration error detected");
    result.back()
          .write()
          .metadataSingleFlag("LEDCalibrationError")
          .hash("Bits")
          .setInt64(0x00200000);

    result.back()
          .write()
          .metadataSingleFlag("LEDError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("LEDError")
          .hash("Description")
          .setString("All channels failed calibration or no communications with the LED module");
    result.back().write().metadataSingleFlag("LEDError").hash("Bits").setInt64(0x00300000);

    result.back()
          .write()
          .metadataSingleFlag("ChamberError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("ChamberError")
          .hash("Description")
          .setString("Chamber error detected");
    result.back().write().metadataSingleFlag("ChamberError").hash("Bits").setInt64(0x00400000);

    result.back()
          .write()
          .metadataSingleFlag("TapeLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("TapeLow")
          .hash("Description")
          .setString("Less than 10 spots remaining");
    result.back().write().metadataSingleFlag("TapeLow").hash("Bits").setInt64(0x01000000);

    result.back()
          .write()
          .metadataSingleFlag("TapeCritical")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("TapeCritical")
          .hash("Description")
          .setString("Less than five spots remaining");
    result.back().write().metadataSingleFlag("TapeCritical").hash("Bits").setInt64(0x02000000);

    result.back()
          .write()
          .metadataSingleFlag("TapeError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("TapeError")
          .hash("Description")
          .setString("Out of tape or tape not moving");
    result.back().write().metadataSingleFlag("TapeError").hash("Bits").setInt64(0x03000000);

    result.back()
          .write()
          .metadataSingleFlag("StabilityTest")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("StabilityTest")
          .hash("Description")
          .setString("Stability test in progress");
    result.back().write().metadataSingleFlag("StabilityTest").hash("Bits").setInt64(0x04000000);

    result.back()
          .write()
          .metadataSingleFlag("CleanAirTest")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("CleanAirTest")
          .hash("Description")
          .setString("Clean air test in progress");
    result.back().write().metadataSingleFlag("CleanAirTest").hash("Bits").setInt64(0x08000000);

    result.back()
          .write()
          .metadataSingleFlag("ChangeTapeTest")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("ChangeTapeTest")
          .hash("Description")
          .setString("Tape change test in progress");
    result.back().write().metadataSingleFlag("ChangeTapeTest").hash("Bits").setInt64(0x0C000000);

    result.back()
          .write()
          .metadataSingleFlag("ControllerNotReady")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("ControllerNotReady")
          .hash("Description")
          .setString("Controller reset and not ready (no date set)");

    result.back()
          .write()
          .metadataSingleFlag("ControllerBusy")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("ControllerBusy")
          .hash("Description")
          .setString("Controller busy");

    result.back()
          .write()
          .metadataSingleFlag("DetectorInitializationError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("DetectorInitializationError")
          .hash("Description")
          .setString("Detector initialization error reported");

    result.back()
          .write()
          .metadataSingleFlag("DetectorStopped")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("DetectorStopped")
          .hash("Description")
          .setString("Detector in stop state");

    result.back()
          .write()
          .metadataSingleFlag("DetectorLEDCalibration")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("DetectorLEDCalibration")
          .hash("Description")
          .setString("Detector in LED calibration state");

    result.back()
          .write()
          .metadataSingleFlag("DetectorFastLEDCalibration")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("DetectorFastLEDCalibration")
          .hash("Description")
          .setString("Detector in fast LED calibration state");

    result.back()
          .write()
          .metadataSingleFlag("DetectorReadNDF0")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("DetectorReadNDF0")
          .hash("Description")
          .setString("Detector in read NDF0 state");

    result.back()
          .write()
          .metadataSingleFlag("DetectorReadNDF1")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("DetectorReadNDF1")
          .hash("Description")
          .setString("Detector in read NDF1 state");

    result.back()
          .write()
          .metadataSingleFlag("DetectorReadNDF2")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("DetectorReadNDF2")
          .hash("Description")
          .setString("Detector in read NDF2 state");

    result.back()
          .write()
          .metadataSingleFlag("DetectorReadNDF3")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("DetectorReadNDF3")
          .hash("Description")
          .setString("Detector in read NDF3 state");

    result.back()
          .write()
          .metadataSingleFlag("DetectorReadNDFError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("DetectorReadNDFError")
          .hash("Description")
          .setString("Detector NDF error");

    result.back()
          .write()
          .metadataSingleFlag("FallbackCorrectionFactor")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer33");
    result.back()
          .write()
          .metadataSingleFlag("FallbackCorrectionFactor")
          .hash("Description")
          .setString(
                  "Acquisition calculated values are using the instrument reported correction factor as a fallback");


    result.emplace_back(SequenceName({}, "raw_meta", "F2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataHash("Description").setString("Instrument status bits");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataHashChild("Global")
          .metadataInteger("Description")
          .setString("Global status flags");
    result.back().write().metadataHashChild("Global").metadataInteger("Format").setString("FFFF");

    result.back()
          .write()
          .metadataHashChild("Controller")
          .metadataInteger("Description")
          .setString("Controller state");
    result.back()
          .write()
          .metadataHashChild("Controller")
          .metadataInteger("Format")
          .setString("000");

    result.back()
          .write()
          .metadataHashChild("Detector")
          .metadataInteger("Description")
          .setString("Detector state");
    result.back().write().metadataHashChild("Detector").metadataInteger("Format").setString("00");

    result.back()
          .write()
          .metadataHashChild("LED")
          .metadataInteger("Description")
          .setString("LED state");
    result.back().write().metadataHashChild("LED").metadataInteger("Format").setString("000");

    result.back()
          .write()
          .metadataHashChild("Valve")
          .metadataInteger("Description")
          .setString("Valve status");
    result.back().write().metadataHashChild("Valve").metadataInteger("Format").setString("00000");


    for (int idx = 0, max = config.first().wavelengths.size(); idx < max; ++idx) {
        double wavelength = config.first().wavelengths.at(idx);
        QString columnName(QString::number(wavelength, 'f', 0));

        auto tr1Variable = "Ir" + std::to_string(idx + 1);
        result.emplace_back(SequenceName({}, "raw_meta", tr1Variable), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Format").setString("0.0000000");
        result.back().write().metadataReal("GroupUnits").setString("Transmittance");
        result.back().write().metadataReal("Description").setString("Spot one transmittance");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Spot 1 Tr"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
        result.back().write().metadataReal("Realtime").hash("Format").setString("0.000");

        auto tr2Variable = "Irs" + std::to_string(idx + 1);
        result.emplace_back(SequenceName({}, "raw_meta", tr2Variable), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Format").setString("0.0000000");
        result.back().write().metadataReal("GroupUnits").setString("Transmittance");
        result.back().write().metadataReal("Description").setString("Spot two transmittance");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Spot 2 Tr"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(6);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
        result.back().write().metadataReal("Realtime").hash("Format").setString("0.000");

        result.emplace_back(SequenceName({}, "raw_meta", "Ba" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Aerosol light absorption coefficient on spot one");
        result.back().write().metadataReal("ReportT").setDouble(0);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back()
              .write()
              .metadataReal("Smoothing")
              .hash("Mode")
              .setString("BeersLawAbsorptionInitial");
        result.back()
              .write()
              .metadataReal("Smoothing")
              .hash("Parameters")
              .hash("L")
              .setString("L1");
        result.back()
              .write()
              .metadataReal("Smoothing")
              .hash("Parameters")
              .hash("Transmittance")
              .setString(tr1Variable);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Spot 1 Bap"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);

        result.emplace_back(SequenceName({}, "raw_meta", "Bas" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Aerosol light absorption coefficient on spot two");
        result.back().write().metadataReal("ReportT").setDouble(0);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back()
              .write()
              .metadataReal("Smoothing")
              .hash("Mode")
              .setString("BeersLawAbsorptionInitial");
        result.back()
              .write()
              .metadataReal("Smoothing")
              .hash("Parameters")
              .hash("L")
              .setString("L2");
        result.back()
              .write()
              .metadataReal("Smoothing")
              .hash("Parameters")
              .hash("Transmittance")
              .setString(tr2Variable);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Spot 2 Bap"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);

        result.emplace_back(SequenceName({}, "raw_meta", "Bac" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataReal("Description")
              .setString(
                      "Aerosol light absorption coefficient with acquisition system two spot correction");
        result.back().write().metadataReal("ReportT").setDouble(0);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Corrected Bap"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);

        result.emplace_back(SequenceName({}, "raw_meta", "If" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Format").setString("000000");
        result.back().write().metadataReal("GroupUnits").setString("Intensity");
        result.back().write().metadataReal("Description").setString("Reference detector signal");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("I reference"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(7);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);

        result.emplace_back(SequenceName({}, "raw_meta", "Ip" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Format").setString("000000");
        result.back().write().metadataReal("GroupUnits").setString("Intensity");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Sample spot one detector signal");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("I sample 1"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(8);

        result.emplace_back(SequenceName({}, "raw_meta", "Ips" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Format").setString("000000");
        result.back().write().metadataReal("GroupUnits").setString("Intensity");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Sample spot two detector signal");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("I sample 2"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(9);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);

        result.emplace_back(SequenceName({}, "raw_meta", "X" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
        result.back().write().metadataReal("Format").setString("0000.000");
        result.back()
              .write()
              .metadataReal("Description")
              .setString(
                      "Two spot corrected equivalent black carbon as reported by the instrument");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("ReportT").setDouble(0);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("EBC Rep"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);

        result.emplace_back(SequenceName({}, "raw_meta", "Xp" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
        result.back().write().metadataReal("Format").setString("0000.000");
        result.back()
              .write()
              .metadataReal("Description")
              .setString(
                      "Two spot corrected equivalent black carbon as calculated by the acquisition system");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        {
            auto mp = processing;
            mp["Parameters"].hash("Efficiency")
                            .setDouble(calculateEfficiency(wavelength,
                                                           config.first().absorptionEfficiency,
                                                           parameters.efficiency, idx));
            result.back().write().metadataReal("Processing").toArray().after_back().set(mp);
        }
        result.back().write().metadataReal("ReportT").setDouble(0);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("EBC Calc"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(idx);


        result.emplace_back(SequenceName({}, "raw_meta", "Xf" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
        result.back().write().metadataReal("Format").setString("0000.000");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Equivalent black carbon on spot one as reported by the instrument");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("ReportT").setDouble(0);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
        if (config.first().realtimeDiagnostics) {
            result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(4);
            result.back()
                  .write()
                  .metadataReal("Realtime")
                  .hash("GroupName")
                  .setString(QObject::tr("EBC inst 1"));
            result.back()
                  .write()
                  .metadataReal("Realtime")
                  .hash("GroupColumn")
                  .setString(columnName);
            result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
            result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
            result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(idx);
        }

        result.emplace_back(SequenceName({}, "raw_meta", "Xfs" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
        result.back().write().metadataReal("Format").setString("0000.000");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Equivalent black carbon on spot two as reported by the instrument");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("ReportT").setDouble(0);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
        if (config.first().realtimeDiagnostics) {
            result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(4);
            result.back()
                  .write()
                  .metadataReal("Realtime")
                  .hash("GroupName")
                  .setString(QObject::tr("EBC inst 2"));
            result.back()
                  .write()
                  .metadataReal("Realtime")
                  .hash("GroupColumn")
                  .setString(columnName);
            result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
            result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        }


        result.emplace_back(SequenceName({}, "raw_meta", "ZFACTOR" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("GroupUnits").setString("CorrectionFactor");
        result.back().write().metadataReal("Format").setString("00.000000");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Correction factor as reported by the instrument");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("K inst"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        if (config.first().realtimeDiagnostics) {
            result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);
            result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        } else {
            result.back().write().metadataReal("Realtime").hash("Format").setString("0.0E0");
            result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
        }
    }

    result.emplace_back(SequenceName({}, "raw_meta", "ZSPOT"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataHash("Description").setString("Spot sampling parameters");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataHashChild("Fn")
          .metadataInteger("Description")
          .setString("Tape advance count");
    result.back().write().metadataHashChild("Fn").metadataInteger("Format").setString("000");

    for (int spot = 0; spot < 2; spot++) {
        QString name(QString("In%1").arg(spot + 1));
        result.back()
              .write()
              .metadataHashChild(name)
              .metadataArray("Size")
              .setInt64(config.first().wavelengths.size());
        result.back()
              .write()
              .metadataHashChild(name)
              .metadataArray("Description")
              .setString(QString("Spot %1 start normalized intensities").arg(spot + 1));
        setWavelengths(result.back().write().metadataHashChild(name).metadataArray("Wavelengths"),
                       config.first().wavelengths);
        result.back()
              .write()
              .metadataHashChild(name)
              .metadataArray("Children")
              .metadataReal("Description")
              .setString("Spot start normalized intensity");
        result.back()
              .write()
              .metadataHashChild(name)
              .metadataArray("Children")
              .metadataReal("Format")
              .setString("00.0000000");

        name = QString("In%1Latest").arg(spot + 1);
        result.back()
              .write()
              .metadataHashChild(name)
              .metadataArray("Size")
              .setInt64(config.first().wavelengths.size());
        result.back()
              .write()
              .metadataHashChild(name)
              .metadataArray("Description")
              .setString(QString("Spot %1 end normalized intensities").arg(spot + 1));
        setWavelengths(result.back().write().metadataHashChild(name).metadataArray("Wavelengths"),
                       config.first().wavelengths);
        result.back()
              .write()
              .metadataHashChild(name)
              .metadataArray("Children")
              .metadataReal("Description")
              .setString("Spot end normalized intensity");
        result.back()
              .write()
              .metadataHashChild(name)
              .metadataArray("Children")
              .metadataReal("Format")
              .setString("00.0000000");

        name = QString("Ir%1").arg(spot + 1);
        result.back()
              .write()
              .metadataHashChild(name)
              .metadataArray("Size")
              .setInt64(config.first().wavelengths.size());
        result.back()
              .write()
              .metadataHashChild(name)
              .metadataArray("Description")
              .setString(QString("Spot %1 end transmittance").arg(spot + 1));
        setWavelengths(result.back().write().metadataHashChild(name).metadataArray("Wavelengths"),
                       config.first().wavelengths);
        result.back()
              .write()
              .metadataHashChild(name)
              .metadataArray("Children")
              .metadataReal("Description")
              .setString("Spot end transmittance");
        result.back()
              .write()
              .metadataHashChild(name)
              .metadataArray("Children")
              .metadataReal("Format")
              .setString("0.0000000");

        name = QString("Qt%1").arg(spot + 1);
        result.back()
              .write()
              .metadataHashChild(name)
              .metadataReal("Description")
              .setString(QString("Total sample volume on spot %1").arg(spot + 1));
        result.back().write().metadataHashChild(name).metadataReal("Format").setString("000.00");
        result.back().write().metadataHashChild(name).metadataReal("Units").setString("m³");
        result.back().write().metadataHashChild(name).metadataReal("ReportT").setDouble(0);
        result.back().write().metadataHashChild(name).metadataReal("ReportP").setDouble(1013.25);

        name = QString("L%1").arg(spot + 1);
        result.back()
              .write()
              .metadataHashChild(name)
              .metadataReal("Description")
              .setString(QString("Total integrated spot %1 sample length, Qt/A").arg(spot + 1));
        result.back()
              .write()
              .metadataHashChild(name)
              .metadataReal("Format")
              .setString("00000.0000");
        result.back().write().metadataHashChild(name).metadataReal("Units").setString("m");
        result.back().write().metadataHashChild(name).metadataReal("ReportT").setDouble(0);
        result.back().write().metadataHashChild(name).metadataReal("ReportP").setDouble(1013.25);

        name = QString("Area%1").arg(spot + 1);
        result.back()
              .write()
              .metadataHashChild(name)
              .metadataReal("Description")
              .setString(QString("Spot %1 area").arg(spot + 1));
        result.back().write().metadataHashChild(name).metadataReal("Format").setString("00.000");
        result.back().write().metadataHashChild(name).metadataReal("Units").setString("mm²");
    }


    result.emplace_back(SequenceName({}, "raw_meta", "ZPARAMETERS"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("SG parameter read data");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataHashChild("")
          .metadataString("Description")
          .setString("SG command key value");
    result.back()
          .write()
          .metadataHashChild("Frame")
          .metadataString("Description")
          .setString("Whole frame data (including trailing data record if applicable)");
    result.back()
          .write()
          .metadataHashChild("Raw")
          .metadataArray("Description")
          .setString("Raw field data");
    result.back()
          .write()
          .metadataHashChild("Raw")
          .metadataArray("Children")
          .metadataString("Description")
          .setString("Field value");
    result.back()
          .write()
          .metadataHashChild("Description")
          .metadataString("Description")
          .setString("Instrument description name");
    result.back()
          .write()
          .metadataHashChild("SerialNumber")
          .metadataString("Description")
          .setString("Instrument serial number");
    result.back()
          .write()
          .metadataHashChild("Model")
          .metadataString("Description")
          .setString("Instrument model number");
    result.back()
          .write()
          .metadataHashChild("Language")
          .metadataString("Description")
          .setString("Instrument language setting");
    result.back()
          .write()
          .metadataHashChild("NumberOfChannels")
          .metadataInteger("Description")
          .setString("Reported number of channels");
    result.back()
          .write()
          .metadataHashChild("About")
          .metadataInteger("Description")
          .setString("Reported About value");
    result.back()
          .write()
          .metadataHashChild("SetupTimes")
          .metadataArray("Description")
          .setString("Setup time fields");
    result.back()
          .write()
          .metadataHashChild("SetupTimes")
          .metadataArray("Children")
          .metadataString("Description")
          .setString("Time field value");
    result.back()
          .write()
          .metadataHashChild("DateFormat")
          .metadataInteger("Description")
          .setString("Reported date format setting");
    result.back()
          .write()
          .metadataHashChild("MeasureTimeStamp")
          .metadataInteger("Description")
          .setString("Reported measure time stamp setting");
    result.back()
          .write()
          .metadataHashChild("PumpPresetValue")
          .metadataInteger("Description")
          .setString("Reported pump preset setting");
    result.back()
          .write()
          .metadataHashChild("FlowSetpoint")
          .metadataReal("Description")
          .setString("Flow setpoint");
    result.back().write().metadataHashChild("FlowSetpoint").metadataReal("Units").setString("lpm");
    result.back()
          .write()
          .metadataHashChild("TimeBase")
          .metadataInteger("Description")
          .setString("Measurement time base");
    result.back().write().metadataHashChild("TimeBase").metadataInteger("Units").setString("s");
    result.back()
          .write()
          .metadataHashChild("Sigma")
          .metadataArray("Description")
          .setString("Absorption mass efficiency settings in channel order");
    result.back()
          .write()
          .metadataHashChild("Sigma")
          .metadataArray("Children")
          .metadataReal("Description")
          .setString("Absorption mass efficiency");
    result.back()
          .write()
          .metadataHashChild("Area")
          .metadataReal("Description")
          .setString("Instrument spot area setting");
    result.back().write().metadataHashChild("Area").metadataReal("Units").setString("mm²");
    result.back()
          .write()
          .metadataHashChild("SpotsPerAdvance")
          .metadataInteger("Description")
          .setString("Number of spots moved when the tap advances");
    result.back()
          .write()
          .metadataHashChild("TemperatureRHControl")
          .metadataInteger("Description")
          .setString("Temperature and RH control setting");
    result.back()
          .write()
          .metadataHashChild("FlowUnitsStandard")
          .metadataInteger("Description")
          .setString("Flow units setting (0 - standard, 1 - volumetric)");
    result.back()
          .write()
          .metadataHashChild("Advance")
          .metadataHash("Description")
          .setString("Tape advance settings");
    result.back()
          .write()
          .metadataHashChild("Advance")
          .metadataHashChild("Attenuation")
          .metadataReal("Description")
          .setString("Threshold attenuation");
    result.back()
          .write()
          .metadataHashChild("Advance")
          .metadataHashChild("Mode")
          .metadataInteger("Description")
          .setString(
                  "Advance condition (1 - attenuation threshold, 2 - time interval, 3 - time of day)");
    result.back()
          .write()
          .metadataHashChild("Advance")
          .metadataHashChild("Interval")
          .metadataInteger("Description")
          .setString("Advance interval in hours");
    result.back()
          .write()
          .metadataHashChild("Advance")
          .metadataHashChild("Flush")
          .metadataInteger("Description")
          .setString("Air flush time after an advance");
    result.back()
          .write()
          .metadataHashChild("Advance")
          .metadataHashChild("Flush")
          .metadataInteger("Units")
          .setString("min");
    result.back()
          .write()
          .metadataHashChild("TotalAdvances")
          .metadataInteger("Description")
          .setString("Number of total tape advances so far");
    result.back()
          .write()
          .metadataHashChild("FlowFormula")
          .metadataArray("Description")
          .setString("Flow formula constants in reported order");
    result.back()
          .write()
          .metadataHashChild("FlowFormula")
          .metadataArray("Children")
          .metadataReal("Description")
          .setString("Individual flow formula constant");
    result.back()
          .write()
          .metadataHashChild("TapeOffsetValid")
          .metadataInteger("Description")
          .setString("Tape offset currently valid");
    result.back()
          .write()
          .metadataHashChild("TapeFormula")
          .metadataArray("Description")
          .setString("Tape formula constants in reported order");
    result.back()
          .write()
          .metadataHashChild("TapeFormula")
          .metadataArray("Children")
          .metadataReal("Description")
          .setString("Individual tape formula constant");
    result.back()
          .write()
          .metadataHashChild("LeakageFactor")
          .metadataReal("Description")
          .setString("Leakage factor setting (flow loss at the sampling head)");
    result.back()
          .write()
          .metadataHashChild("WeingartnerConstant")
          .metadataReal("Description")
          .setString("Weingartner correction constant");
    result.back()
          .write()
          .metadataHashChild("ATNf1")
          .metadataReal("Description")
          .setString("Lower attenuation threshold for face velocity flow ration fitting");
    result.back()
          .write()
          .metadataHashChild("ATNf2")
          .metadataReal("Description")
          .setString(
                  "Upper attenuation threshold for face velocity flow ration fitting and lower threshold for correction factor smoothing");
    result.back()
          .write()
          .metadataHashChild("kMin")
          .metadataReal("Description")
          .setString("Minimum accepted correction factor value");
    result.back()
          .write()
          .metadataHashChild("kMax")
          .metadataReal("Description")
          .setString("Maximum accepted correction factor value");
    result.back()
          .write()
          .metadataHashChild("k")
          .metadataArray("Description")
          .setString("Reported correction constants");
    result.back()
          .write()
          .metadataHashChild("k")
          .metadataArray("Children")
          .metadataReal("Description")
          .setString("Correction constant");
    result.back()
          .write()
          .metadataHashChild("FlowReportingStandard")
          .metadataInteger("Description")
          .setString("Flow reporting standard setting");
    result.back()
          .write()
          .metadataHashChild("ExternalP")
          .metadataReal("Description")
          .setString("External pressure");
    result.back().write().metadataHashChild("ExternalP").metadataReal("Units").setString("hPa");
    result.back()
          .write()
          .metadataHashChild("ExternalT")
          .metadataReal("Description")
          .setString("External temperature");
    result.back()
          .write()
          .metadataHashChild("ExternalT")
          .metadataReal("Units")
          .setString("\xC2\xB0\x43");
    result.back()
          .write()
          .metadataHashChild("COM")
          .metadataArray("Description")
          .setString("External communications device settings");
    result.back()
          .write()
          .metadataHashChild("COM")
          .metadataArray("Children")
          .metadataInteger("Description")
          .setString("External communications device type");
    result.back()
          .write()
          .metadataHashChild("NetworkingAndAutotest")
          .metadataArray("Description")
          .setString("Networking and autotest settings in reported order");
    result.back()
          .write()
          .metadataHashChild("NetworkingAndAutotest")
          .metadataArray("Children")
          .metadataString("Description")
          .setString("Networking or autotest field value");
    result.back()
          .write()
          .metadataHashChild("Display")
          .metadataInteger("Description")
          .setString("Reported display setting");
    result.back()
          .write()
          .metadataHashChild("TimeZone")
          .metadataString("Description")
          .setString("Reported time zone setting");
    result.back()
          .write()
          .metadataHashChild("DaylightSavingTime")
          .metadataString("Description")
          .setString("Reported daylight saving time setting");
    result.back()
          .write()
          .metadataHashChild("Aff")
          .metadataInteger("Description")
          .setString("Reported Aff value");
    result.back()
          .write()
          .metadataHashChild("Abb")
          .metadataInteger("Description")
          .setString("Reported Abb value");
    result.back()
          .write()
          .metadataHashChild("HomeInfo")
          .metadataInteger("Description")
          .setString("Reported HomeInfo value");

    return result;
}

SequenceValue::Transfer AcquireMageeAethalometer33::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_magee_aethalometer33");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["HardwareVersion"].exists())
        processing["HardwareVersion"].set(instrumentMeta["HardwareVersion"]);
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    if (instrumentMeta["SerialNumber"].exists())
        processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataString("Realtime").hash("Hide").setBool(true);
    /*result.back().write().metadataString("Realtime").hash("Name").
        setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").
        setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").
        setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").
        setInt64(8);
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("Run").setString(QObject::tr("", "run state string"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("InvalidRecord").setString(QObject::tr("NO COMMS: Invalid record"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("Timeout").setString(QObject::tr("NO COMMS: Timeout"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractive").setString(QObject::tr("STARTING COMMS"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveParametersRead").setString(QObject::tr("STARTING COMMS: Reading parameters"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveTimeRead").setString(QObject::tr("STARTING COMMS: Reading instrument time"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveInstrumentStop").setString(QObject::tr("STARTING COMMS: Halting instrument"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveSetParameters").setString(QObject::tr("STARTING COMMS: Setting instrument parameters"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveInstrumentStart").setString(QObject::tr("STARTING COMMS: Resuming instrument"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveFirstValid").setString(QObject::tr("STARTING COMMS: Waiting for initial record")); */

    if (config.first().realtimeDiagnostics) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZQC"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.000");
        result.back().write().metadataReal("Units").setString("lpm");
        result.back().write().metadataReal("Description").setString("Combined spot flow");
        result.back().write().metadataReal("ReportT").setDouble(0);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(4);
        result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Total"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

        result.emplace_back(SequenceName({}, "raw_meta", "ZLEAKAGE"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("0.000");
        result.back().write().metadataReal("Description").setString("Calculated leakage factor");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(4);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Leakage"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    }

    for (int idx = 0, max = config.first().wavelengths.size(); idx < max; ++idx) {
        double wavelength = config.first().wavelengths.at(idx);
        QString columnName(QString::number(wavelength, 'f', 0));

        if (!config.first().realtimeDiagnostics)
            continue;

        result.emplace_back(SequenceName({}, "raw_meta", "ZFACTORc" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("GroupUnits").setString("CorrectionFactor");
        result.back().write().metadataReal("Format").setString("00.000000");
        result.back().write().metadataReal("Description").setString("Calculated correction factor");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(4);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("K calc"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

        result.emplace_back(SequenceName({}, "raw_meta", "ZFACTORp" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("GroupUnits").setString("CorrectionFactor");
        result.back().write().metadataReal("Format").setString("00.000000");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Calculated correction factor from the current filter only");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(4);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("K filter"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(6);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);


        result.emplace_back(SequenceName({}, "raw_meta", "ZATN" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Format").setString("000.000");
        result.back().write().metadataReal("GroupUnits").setString("Attenuation");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Optical attenuation of spot one, -100*ln(Ir)");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(4);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Spot 1 ATN calc"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(8);


        result.emplace_back(SequenceName({}, "raw_meta", "ZATNs" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Format").setString("000.000");
        result.back().write().metadataReal("GroupUnits").setString("Attenuation");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Optical attenuation of spot two, -100*ln(Ir)");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(4);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Spot 2 ATN calc"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(9);


        result.emplace_back(SequenceName({}, "raw_meta", "ZATNc" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Format").setString("000.000");
        result.back().write().metadataReal("GroupUnits").setString("Attenuation");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Optical attenuation of spot one, backed out from the corrected data");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(4);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Spot 1 ATN inst"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(7);


        result.emplace_back(SequenceName({}, "raw_meta", "In" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Format").setString("00.0000000");
        result.back().write().metadataReal("GroupUnits").setString("NormalizedIntensity");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Normalized spot one intensity");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(4);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Spot 1 sam/ref"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(10);

        result.emplace_back(SequenceName({}, "raw_meta", "Ins" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Format").setString("00.0000000");
        result.back().write().metadataReal("GroupUnits").setString("NormalizedIntensity");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Normalized spot two intensity");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(4);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Spot 2 sam/ref"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(11);


        result.emplace_back(SequenceName({}, "raw_meta", "ZX" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
        result.back().write().metadataReal("Format").setString("0000.000");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Equivalent black carbon on spot one");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        {
            auto mp = processing;
            mp["Parameters"].hash("Efficiency")
                            .setDouble(calculateEfficiency(wavelength,
                                                           config.first().absorptionEfficiency,
                                                           parameters.efficiency, idx));
            result.back().write().metadataReal("Processing").toArray().after_back().set(mp);
        }
        result.back().write().metadataReal("ReportT").setDouble(0);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(4);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("EBC calc 1"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);


        result.emplace_back(SequenceName({}, "raw_meta", "ZXs" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
        result.back().write().metadataReal("Format").setString("0000.000");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Equivalent black carbon on spot two");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        {
            auto mp = processing;
            mp["Parameters"].hash("Efficiency")
                            .setDouble(calculateEfficiency(wavelength,
                                                           config.first().absorptionEfficiency,
                                                           parameters.efficiency, idx));
            result.back().write().metadataReal("Processing").toArray().after_back().set(mp);
        }
        result.back().write().metadataReal("ReportT").setDouble(0);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(4);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("EBC calc 2"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

        result.emplace_back(SequenceName({}, "raw_meta", "ZFVRF" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("GroupUnits").setString("Ratio");
        result.back().write().metadataReal("Format").setString("00.0000");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Effective face velocity ratio factor");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(4);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("FVRF"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(12);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    }

    return result;
}

SequenceMatch::Composite AcquireMageeAethalometer33::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSPOT");
    sel.append({}, {}, "ZPARAMETERS");
    sel.append({}, {}, "ZSTATE");
    return sel;
}

static SequenceValue::Transfer buildInvalidate(const SequenceName::Component &base,
                                               double time,
                                               int prior,
                                               int next)
{
    Q_ASSERT(prior > next);

    SequenceValue::Transfer result;
    for (int idx = next + 1; idx <= prior; idx++) {
        result.emplace_back(SequenceName({}, "raw_meta", base + std::to_string(idx)),
                            Variant::Root(), time, FP::undefined());
    }
    return result;
}

SequenceValue::Transfer AcquireMageeAethalometer33::invalidateLogMetadata(double time,
                                                                          int prior,
                                                                          int next) const
{
    SequenceValue::Transfer result;

    Util::append(buildInvalidate("Ir", time, prior, next), result);
    Util::append(buildInvalidate("Ba", time, prior, next), result);
    Util::append(buildInvalidate("If", time, prior, next), result);
    Util::append(buildInvalidate("Ip", time, prior, next), result);
    Util::append(buildInvalidate("X", time, prior, next), result);
    Util::append(buildInvalidate("Xp", time, prior, next), result);
    Util::append(buildInvalidate("ZFACTOR", time, prior, next), result);

    return result;
}

SequenceValue::Transfer AcquireMageeAethalometer33::invalidateRealtimeMetadata(double time,
                                                                               int prior,
                                                                               int next,
                                                                               bool priorDiag,
                                                                               bool nextDiag) const
{
    SequenceValue::Transfer result;

    if (priorDiag)
        prior = 0;
    if (nextDiag)
        next = 0;
    Util::append(buildInvalidate("ZFACTORc", time, prior, next), result);
    Util::append(buildInvalidate("ZFACTORp", time, prior, next), result);
    Util::append(buildInvalidate("ZATN", time, prior, next), result);
    Util::append(buildInvalidate("ZATNs", time, prior, next), result);
    Util::append(buildInvalidate("ZATNc", time, prior, next), result);
    Util::append(buildInvalidate("In", time, prior, next), result);
    Util::append(buildInvalidate("Ins", time, prior, next), result);
    Util::append(buildInvalidate("ZX", time, prior, next), result);
    Util::append(buildInvalidate("ZXs", time, prior, next), result);
    Util::append(buildInvalidate("ZFVRF", time, prior, next), result);

    return result;
}

AcquireMageeAethalometer33::ReportedValues::ReportedValues() : timeBase(0),
                                                               If(),
                                                               Ip(),
                                                               Q1(FP::undefined()),
                                                               QC(FP::undefined()),
                                                               Treport(FP::undefined()),
                                                               Preport(FP::undefined()),
                                                               T1(FP::undefined()),
                                                               T2(FP::undefined()),
                                                               T3(FP::undefined()),
                                                               status(INTEGER::undefined()),
                                                               controlStatus(INTEGER::undefined()),
                                                               detectStatus(INTEGER::undefined()),
                                                               ledStatus(INTEGER::undefined()),
                                                               valveStatus(INTEGER::undefined()),
                                                               X(),
                                                               Xp(),
                                                               K(),
                                                               Fn(INTEGER::undefined())
{ }

bool AcquireMageeAethalometer33::ReportedValues::operator==(const ReportedValues &other) const
{
    return timeBase == other.timeBase &&
            If == other.If &&
            Ip[0] == other.Ip[0] &&
            Ip[1] == other.Ip[1] &&
            Q1 == other.Q1 &&
            QC == other.QC &&
            Treport == other.Treport &&
            Preport == other.Preport &&
            T1 == other.T1 &&
            T2 == other.T2 &&
            T3 == other.T3 &&
            status == other.status &&
            controlStatus == other.controlStatus &&
            detectStatus == other.detectStatus &&
            ledStatus == other.ledStatus &&
            valveStatus == other.valveStatus &&
            X == other.X &&
            Xp[0] == other.Xp[0] &&
            Xp[1] == other.Xp[1] &&
            K == other.K &&
            Fn == other.Fn;
}

static int auxiliaryFieldCount(int type)
{
    switch (type) {
    case 0:
        /* None */
        return 0;
    case 1:
        /* AMES_TPR159 */
        return 3;
    case 2:
        /* Comet_T0310 */
        return 1;
    case 3:
        /* Vaisala_GMP343 */
        return 3;
    case 4:
        /* TSI_4100 */
        return 1;
    case 5:
        /* Datalogger_AE33_protocol - port waiting */
        return 0;
    case 6:
        /* Aerosol_inlet_dryer */
        return 3;
    case 7:
        /* Datalogger_BH_protocol */
        return 0;
    case 8:
        /* Datalogger_Qair_protocol */
        return 0;
    case 9:
        /* AE33_DataStreaming */
        return 0;
    case 10:
        /* GPS GSTAR IV - Unknown number of extra fields, just assume 0 */
        return 0;
    case 11:
        /* AMES_VMT107 IV */
        return 2;
    case 12:
        /* Datalogger_BH_protocol2 */
        return 0;
    case 13:
        /* Datalogger_BH_protocol3 */
        return 0;
//    case XX:
//        /* Nephelo_Aurora4000 */
//        return 17;
    case 18:
        /* Weather station GMX300 */
        return 5;
    default:
        break;
    }
    return -1;
}

static bool isDelimiter(char c)
{
    switch (c) {
    case ' ':
    case '\n':
    case '\r':
    case '\t':
    case '\v':
    case ',':
        return true;
    default:
        return false;
    }
    return false;
}

int AcquireMageeAethalometer33::parseRecord(const Util::ByteView &line,
                                            double &frameTime,
                                            ReportedValues &output,
                                            bool authoritativeTime)
{
    Q_ASSERT(!config.isEmpty());

    auto fields = CSV::acquisitionSplit(line);
    bool ok = false;

    if (fields.empty()) return 1;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    auto subfields = Util::as_deque(field.split('/'));

    if (subfields.empty()) return 2;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    Variant::Root year;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0) return 3;
        if (field.size() == 2) {
            raw += 2000;
            year.write().setInt64(raw);
        } else if (field.size() == 4) {
            if (raw < 1900 || raw > 2100) return 4;
            year.write().setInt64(raw);
        } else {
            return 5;
        }
    }
    remap("YEAR", year);

    if (subfields.empty()) return 5;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    Variant::Root month;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 1 || raw > 12) return 6;
        month.write().setInt64(raw);
    }
    remap("MONTH", month);

    if (subfields.empty()) return 7;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    if (field.empty()) return 8;
    Variant::Root day;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 1 || raw > 31) return 9;
        day.write().setInt64(raw);
    }
    remap("DAY", day);

    if (!subfields.empty()) return 10;

    if (fields.empty()) return 11;
    field = fields.front().string_trimmed();
    fields.pop_front();
    subfields = Util::as_deque(field.split(':'));

    if (subfields.empty()) return 12;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    if (field.empty()) return 13;
    Variant::Root hour;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0 || raw > 23) return 14;
        hour.write().setInt64(raw);
    }
    remap("HOUR", hour);

    if (subfields.empty()) return 15;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    if (field.empty()) return 16;
    Variant::Root minute;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0 || raw > 59) return 17;
        minute.write().setInt64(raw);
    }
    remap("MINUTE", minute);

    if (subfields.empty()) return 18;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    if (field.empty()) return 19;
    Variant::Root second;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0 || raw > 60) return 20;
        second.write().setInt64(raw);
    }
    remap("SECOND", second);

    if (!subfields.empty()) return 21;


    if (fields.empty()) return 23;
    field = fields.front().string_trimmed();
    fields.pop_front();
    output.timeBase = field.parse_u64(&ok);
    if (!ok) return 24;
    if (!INTEGER::defined(output.timeBase) || output.timeBase <= 0) return 25;

    if (!FP::defined(frameTime)) {
        qint64 y = year.read().toInt64();
        qint64 mo = month.read().toInt64();
        qint64 d = day.read().toInt64();
        qint64 h = hour.read().toInt64();
        qint64 m = minute.read().toInt64();
        qint64 s = second.read().toInt64();

        if (INTEGER::defined(y) &&
                y > 1900 &&
                y < 2100 &&
                INTEGER::defined(mo) &&
                mo >= 1 &&
                mo <= 12 &&
                INTEGER::defined(d) &&
                d >= 1 &&
                d <= 31 &&
                INTEGER::defined(h) &&
                h >= 0 &&
                h <= 23 &&
                INTEGER::defined(m) &&
                h >= 0 &&
                h <= 59 &&
                INTEGER::defined(s) &&
                h >= 0 &&
                h <= 60) {
            QDate date((int) y, (int) mo, (int) d);
            QTime time((int) h, (int) m, (int) s);
            if (date.isValid() && time.isValid()) {
                frameTime = Time::fromDateTime(QDateTime(date, time, Qt::UTC));

                if (authoritativeTime) {
                    /* Assume it works like the AE31 and the time is at the
                     * start of sampling */
                    frameTime += (double) output.timeBase;

                    configurationAdvance(frameTime);
                }
            }
        }
    }

    Q_ASSERT(output.If.isEmpty());
    Q_ASSERT(output.Ip[0].isEmpty());
    Q_ASSERT(output.Ip[1].isEmpty());
    Q_ASSERT(output.X.isEmpty());
    Q_ASSERT(output.Xp[0].isEmpty());
    Q_ASSERT(output.Xp[1].isEmpty());
    Q_ASSERT(output.K.isEmpty());

    int errorCodeOffset = 1000;
    for (int color = 0, max = config.first().wavelengths.size();
            color < max;
            color++, errorCodeOffset += 1000) {
        double v;

        if (fields.empty()) return 0 + errorCodeOffset;
        field = fields.front().string_trimmed();
        fields.pop_front();
        v = field.parse_real(&ok);
        if (!ok) return 1 + errorCodeOffset;
        if (!FP::defined(v)) return 2 + errorCodeOffset;
        output.If.append(v);

        if (fields.empty()) return 3 + errorCodeOffset;
        field = fields.front().string_trimmed();
        fields.pop_front();
        v = field.parse_real(&ok);
        if (!ok) return 4 + errorCodeOffset;
        if (!FP::defined(v)) return 5 + errorCodeOffset;
        output.Ip[0].append(v);

        if (fields.empty()) return 6 + errorCodeOffset;
        field = fields.front().string_trimmed();
        fields.pop_front();
        v = field.parse_real(&ok);
        if (!ok) return 7 + errorCodeOffset;
        if (!FP::defined(v)) return 8 + errorCodeOffset;
        output.Ip[1].append(v);
    }

    if (fields.empty()) return 26;
    field = fields.front().string_trimmed();
    fields.pop_front();
    output.Q1 = field.parse_real(&ok);
    if (!ok) return 27;
    if (!FP::defined(output.Q1)) return 28;
    output.Q1 /= 1000.0;

    /* Flow through spot 2 (actually C - 1), ignored */
    if (fields.empty()) return 29;
    fields.pop_front();

    if (fields.empty()) return 31;
    field = fields.front().string_trimmed();
    fields.pop_front();
    output.QC = field.parse_real(&ok);
    if (!ok) return 32;
    if (!FP::defined(output.QC)) return 33;
    output.QC /= 1000.0;

    if (fields.empty()) return 34;
    field = fields.front().string_trimmed();
    fields.pop_front();
    output.Preport = field.parse_real(&ok);
    if (!ok) return 35;
    if (!FP::defined(output.Preport)) return 36;
    output.Preport /= 100.0;
    if (output.Preport < 100.0) return 37;
    if (output.Preport > 2000.0) return 38;

    if (fields.empty()) return 39;
    field = fields.front().string_trimmed();
    fields.pop_front();
    output.Treport = field.parse_real(&ok);
    if (!ok) return 40;
    if (!FP::defined(output.Treport)) return 41;
    if (output.Treport < -150.0 || output.Treport > 150.0) return 42;

    /* Sample RH, ignored */
    if (fields.empty()) return 43;
    fields.pop_front();

    if (fields.empty()) return 44;
    field = fields.front().string_trimmed();
    fields.pop_front();
    output.T1 = field.parse_real(&ok);
    if (!ok) return 45;
    if (!FP::defined(output.T1)) return 46;
    if (output.T1 < -150.0 || output.T1 > 150.0) return 47;

    if (fields.empty()) return 48;
    field = fields.front().string_trimmed();
    fields.pop_front();
    output.T2 = field.parse_real(&ok);
    if (!ok) return 49;
    if (!FP::defined(output.T2)) return 50;
    if (output.T2 < -150.0 || output.T2 > 150.0) return 51;

    if (fields.empty()) return 52;
    field = fields.front().string_trimmed();
    fields.pop_front();
    output.status = (qint64) field.parse_u64(&ok);
    if (!ok) return 53;

    if (fields.empty()) return 54;
    field = fields.front().string_trimmed();
    fields.pop_front();
    output.controlStatus = (qint64) field.parse_u64(&ok);
    if (!ok) return 55;

    if (fields.empty()) return 56;
    field = fields.front().string_trimmed();
    fields.pop_front();
    output.detectStatus = (qint64) field.parse_u64(&ok);
    if (!ok) return 57;

    if (fields.empty()) return 58;
    field = fields.front().string_trimmed();
    fields.pop_front();
    output.ledStatus = (qint64) field.parse_u64(&ok);
    if (!ok) return 59;

    if (fields.empty()) return 60;
    field = fields.front().string_trimmed();
    fields.pop_front();
    output.valveStatus = (qint64) field.parse_u64(&ok);
    if (!ok) return 61;

    if (fields.empty()) return 62;
    field = fields.front().string_trimmed();
    fields.pop_front();
    output.T3 = field.parse_real(&ok);
    if (!ok) return 63;
    if (!FP::defined(output.T3)) return 64;
    if (output.T3 < -150.0 || output.T3 > 150.0) return 65;

    for (int color = 0, max = config.first().wavelengths.size();
            color < max;
            color++, errorCodeOffset += 1000) {
        double v;

        if (fields.empty()) return 9 + errorCodeOffset;
        field = fields.front().string_trimmed();
        fields.pop_front();
        v = field.parse_real(&ok);
        if (!ok) return 10 + errorCodeOffset;
        if (!FP::defined(v)) return 11 + errorCodeOffset;
        v /= 1000.0;
        output.Xp[0].append(v);

        if (fields.empty()) return 12 + errorCodeOffset;
        field = fields.front().string_trimmed();
        fields.pop_front();
        v = field.parse_real(&ok);
        if (!ok) return 13 + errorCodeOffset;
        if (!FP::defined(v)) return 14 + errorCodeOffset;
        v /= 1000.0;
        output.Xp[1].append(v);

        if (fields.empty()) return 15 + errorCodeOffset;
        field = fields.front().string_trimmed();
        fields.pop_front();
        v = field.parse_real(&ok);
        if (!ok) return 16 + errorCodeOffset;
        if (!FP::defined(v)) return 17 + errorCodeOffset;
        v /= 1000.0;
        output.X.append(v);
    }

    for (int color = 0, max = config.first().wavelengths.size();
            color < max;
            color++, errorCodeOffset += 1000) {
        double v;

        if (fields.empty()) return 18 + errorCodeOffset;
        field = fields.front().string_trimmed();
        fields.pop_front();
        v = field.parse_real(&ok);
        if (!ok) return 19 + errorCodeOffset;
        if (!FP::defined(v)) return 20 + errorCodeOffset;
        output.K.append(v);
    }

    if (fields.empty()) return 66;
    field = fields.front().string_trimmed();
    fields.pop_front();
    output.Fn = field.parse_u64(&ok);
    if (!ok) return 67;

    /* Allow sensor fields to be omitted (logged data) */
    if (fields.empty()) return 0;

    /* External sensor type 1 */
    if (fields.empty()) return 68;
    field = fields.front().string_trimmed();
    fields.pop_front();
    int type = field.parse_i32(&ok);
    if (!ok) return 69;
    type = auxiliaryFieldCount(type);
    if (type < 0) return 70;
    int totalRemove = type;

    /* External sensor type 2 */
    if (fields.empty()) return 71;
    field = fields.front().string_trimmed();
    fields.pop_front();
    type = field.parse_i32(&ok);
    if (!ok) return 72;
    type = auxiliaryFieldCount(type);
    if (type < 0) return 73;
    totalRemove += type;

    /* External sensor type 3 */
    if (fields.empty()) return 74;
    field = fields.front().string_trimmed();
    fields.pop_front();
    type = field.parse_i32(&ok);
    if (!ok) return 75;
    type = auxiliaryFieldCount(type);
    if (type < 0) return 76;
    totalRemove += type;

    /* Just discard external sensors for now */
    if (totalRemove > 0) {
        if (totalRemove > fields.size()) return 77;
        fields.erase(fields.begin(), fields.begin() + totalRemove);
    }

    if (config.first().strictMode) {
        if (!fields.empty())
            return 100;
    }

    return 0;
}

static double firstValid(const Variant::Read &v1, double v2, double v3 = FP::undefined())
{
    if (v1.exists())
        return v1.toDouble();
    if (FP::defined(v2))
        return v2;
    return v3;
}

static double firstDefaulting(double v1, double v2, double def)
{
    if (FP::defined(v1))
        return v1;
    if (FP::defined(v2))
        return v2;
    return def;
}

static double calculateDensity(double T, double P)
{
    if (!FP::defined(T) || !FP::defined(P))
        return FP::undefined();
    if (T < 150.0) T += 273.15;
    if (T < 100.0 || T > 350.0 || P < 10.0 || P > 2000.0)
        return FP::undefined();
    return (P / 1013.25) * (273.15 / T);
}

static double applyLeakageFactor(double Q, double zeta)
{
    if (!FP::defined(zeta) || zeta >= 1.0)
        return Q;
    if (!FP::defined(Q))
        return FP::undefined();
    return Q * (1.0 - zeta);
}

static double calculateSpot2Flow(double Q1, double QC)
{
    if (!FP::defined(Q1) || !FP::defined(QC))
        return FP::undefined();
    if (Q1 > QC)
        return FP::undefined();
    return QC - Q1;
}

static double correctFlow(double Q, double T, double P)
{
    if (!FP::defined(Q))
        return FP::undefined();
    double den = calculateDensity(T, P);
    if (!FP::defined(den))
        return FP::undefined();
    return Q * den;
}

static double correctEBC(double EBC, double T, double P)
{
    if (!FP::defined(EBC))
        return FP::undefined();
    double den = calculateDensity(T, P);
    if (!FP::defined(den))
        return FP::undefined();
    if (den <= 0.0)
        return FP::undefined();
    return EBC / den;
}

static double calculateIn(double If, double Ip)
{
    if (!FP::defined(If) || !FP::defined(Ip))
        return FP::undefined();
    if (If == 0.0)
        return FP::undefined();
    return Ip / If;
}

static double integrateVolume(double Qt0, double Q, double dT)
{
    if (!FP::defined(Qt0))
        Qt0 = 0.0;
    if (!FP::defined(Q) || !FP::defined(dT))
        return Qt0;
    if (dT <= 0.0)
        return Qt0;
    return Qt0 + Q * dT / 60000.0;
}

static double calculateL(double Qt, double area)
{
    if (!FP::defined(Qt) || !FP::defined(area) || area <= 0.0 || Qt <= 0.0)
        return FP::undefined();
    return Qt / (area * 1E-6);
}

static double calculateIr(double In0, double In)
{
    if (!FP::defined(In0) || !FP::defined(In))
        return FP::undefined();
    if (In0 == 0.0)
        return FP::undefined();
    return In / In0;
}

static double calculateBa(double Ir0, double Qt0, double Ir1, double Qt1, double area)
{
    if (!FP::defined(Ir0) ||
            !FP::defined(Qt0) ||
            !FP::defined(Ir1) ||
            !FP::defined(Qt1) ||
            !FP::defined(area))
        return FP::undefined();
    if (Ir0 <= 0.0 || Ir1 <= 0.0)
        return FP::undefined();
    double dQt = Qt1 - Qt0;
    if (dQt <= 0.0)
        return FP::undefined();
    return log(Ir0 / Ir1) * (area / dQt);
}

static double calculateATN(double Ir)
{
    if (!FP::defined(Ir))
        return FP::undefined();
    if (Ir <= 0.0)
        return FP::undefined();
    return log(Ir) * -100.0;
}

static double applyFactor(double Ba, double ATN, double k)
{
    if (!FP::defined(Ba) || !FP::defined(ATN) || !FP::defined(k))
        return FP::undefined();
    return Ba / (1.0 - k * ATN);
}

static double applyWeingartner(double v, double C)
{
    if (!FP::defined(v) || !FP::defined(C))
        return FP::undefined();
    if (C <= 0.0)
        return FP::undefined();
    return v / C;
}

static double calculateEBC(double Ba, double e)
{
    if (!FP::defined(Ba) || !FP::defined(e))
        return FP::undefined();
    if (e <= 0.0)
        return FP::undefined();
    return Ba / e;
}

static double recoverATN(double BaCorrected, double BaInput, double k)
{
    if (!FP::defined(BaCorrected) || !FP::defined(BaInput) || !FP::defined(k))
        return FP::undefined();
    if (k == 0.0 || BaInput == 0.0 || BaCorrected == 0.0)
        return FP::undefined();
    return (1.0 - BaInput / BaCorrected) / k;
}

static bool shouldCalculateFVRF(double ATN, double ATNf1, double ATNf2)
{
    if (!FP::defined(ATN))
        return false;
    if (FP::defined(ATNf2) && ATN > ATNf2)
        return false;
    if (FP::defined(ATNf1) && ATN < ATNf1)
        return false;
    return true;
}

static double smoothFactor(double kPrior,
                           double ATNPrior,
                           double kCurrent,
                           double ATNCurrent,
                           double ATNf2,
                           double kMin,
                           double kMax)
{
    Q_ASSERT(FP::defined(kMin));
    Q_ASSERT(FP::defined(kMax));
    if (FP::defined(kCurrent) && (kCurrent < kMin || kCurrent > kMax))
        kCurrent = FP::undefined();
    if (FP::defined(kPrior) && (kPrior < kMin || kPrior > kMax))
        kPrior = FP::undefined();
    if (!FP::defined(kCurrent) || !FP::defined(ATNCurrent))
        return kPrior;
    if (!FP::defined(ATNf2) || !FP::defined(kPrior) || !FP::defined(ATNPrior))
        return kCurrent;
    if (ATNCurrent < ATNf2)
        return kPrior;
    if (ATNCurrent > ATNPrior)
        return kCurrent;

    double div = ATNPrior - ATNf2;
    if (div <= 0.0)
        return kCurrent;
    return ((ATNPrior - ATNCurrent) * kPrior + (ATNCurrent - ATNf2) * kCurrent) / div;
}

static double calculateATNRatio(double ATN1, double ATN2)
{
    if (!FP::defined(ATN1) || !FP::defined(ATN2))
        return FP::undefined();
    if (ATN1 <= 0.0 || ATN2 <= 0.0)
        return FP::undefined();
    return ATN2 / ATN1;
}

static void limitFVRFBuffers(QVector<double> &atn, QVector<double> &ratio)
{
    Q_ASSERT(atn.size() == ratio.size());
    /* Average the two closest points together until we're small enough */
    while (atn.size() > 86400) {
        int closestIndex = 0;
        double bestDifference = fabs(atn.last() - atn.first());
        for (int i = 1, max = atn.size() - 1; i < max; i++) {
            double difference = fabs(atn.at(i + 1) - atn.at(i));
            if (difference > bestDifference)
                continue;
            closestIndex = i;
        }

        double a1 = atn.at(closestIndex);
        double a2 = atn.at(closestIndex + 1);
        double r1 = ratio.at(closestIndex);
        double r2 = ratio.at(closestIndex + 1);

        atn.remove(closestIndex);
        atn[closestIndex] = (a1 + a2) / 2.0;
        ratio.remove(closestIndex);
        ratio[closestIndex] = (r1 + r2) / 2.0;
    }
}

static double calculateVolumeRatio(double F1, double F2)
{
    if (!FP::defined(F1) || !FP::defined(F2))
        return FP::undefined();
    if (F1 <= 0.0 || F2 <= 0.0)
        return FP::undefined();
    return F1 / F2;
}

static double calculateFVRF(double ATNi0, double vRatio)
{
    if (!FP::defined(ATNi0) || !FP::defined(vRatio))
        return FP::undefined();
    return ATNi0 * vRatio;
}

namespace {
struct FactorSolver : public NewtonRaphson<FactorSolver> {
    double ATN1;
    double ATN2;
    double guess;

    FactorSolver(double inATN1, double inATN2, double g = FP::undefined()) : ATN1(inATN1),
                                                                             ATN2(inATN2),
                                                                             guess(g)
    {
        Q_ASSERT(FP::defined(ATN1) && ATN1 > 0.0);
        Q_ASSERT(FP::defined(ATN2) && ATN2 > 0.0);
        if (!FP::defined(guess) || guess < -0.01 || guess > 0.01 || guess == 0.0)
            guess = 0.001;
    }

    double evaluate(double k) const
    {
        Q_ASSERT(FP::defined(k));
        if (k < -0.2 || k > 0.2)
            return FP::undefined();
        double r2 = 1.0 - k * ATN2;
        if (r2 <= 0.0)
            return FP::undefined();
        double r1 = 1.0 - k * ATN1;
        if (r1 <= 0.0 || r1 == 1.0)
            return FP::undefined();
        return log(r2) / log(r1);
    }

    double initial(double yTarget) const
    {
        Q_UNUSED(yTarget);
        return guess;
    }

    bool completed(double dX) const
    {
        return fabs(dX) < 1E-10;
    }
};
}

static double calculateFactor(double ATN1,
                              double F1,
                              double ATN2,
                              double F2,
                              double FVRF,
                              double kGuess = FP::undefined())
{
    if (!FP::defined(ATN1) || !FP::defined(F1) || !FP::defined(ATN2) || !FP::defined(F2))
        return FP::undefined();
    if (ATN1 <= 0.0 || F1 <= 0.0 || ATN2 <= 0.0 || F2 <= 0.0)
        return FP::undefined();

    double yTarget = F2 / F1;
    if (FP::defined(FVRF) && FVRF > 0.0)
        yTarget *= FVRF;

    return FactorSolver(ATN1, ATN2, kGuess).solve(yTarget);
}

void AcquireMageeAethalometer33::insertDiagnosticValues(Variant::Write &target,
                                                        const ReportedValues &values) const
{
    target.hash("Status").hash("Global").setInt64(values.status);
    target.hash("Status").hash("Controller").setInt64(values.controlStatus);
    target.hash("Status").hash("Detector").setInt64(values.detectStatus);
    target.hash("Status").hash("LED").setInt64(values.ledStatus);
    target.hash("Status").hash("Valve").setInt64(values.valveStatus);
    target.hash("Fn").setInt64(values.Fn);

    for (int color = 0, max = values.If.size(); color < max; color++) {
        target.hash("If").array(color).setDouble(values.If.at(color));
    }
    for (int color = 0, max = values.Ip[0].size(); color < max; color++) {
        target.hash("Ip").array(color).setDouble(values.Ip[0].at(color));
    }
    for (int color = 0, max = values.Ip[1].size(); color < max; color++) {
        target.hash("Ips").array(color).setDouble(values.Ip[1].at(color));
    }
    for (int color = 0, max = values.K.size(); color < max; color++) {
        target.hash("K").array(color).setDouble(values.K.at(color));
    }
    for (int color = 0, max = values.X.size(); color < max; color++) {
        target.hash("X").array(color).setDouble(values.X.at(color));
    }
    for (int color = 0, max = values.X.size(); color < max; color++) {
        target.hash("X").array(color).setDouble(values.X.at(color));
    }
    for (int color = 0, max = values.Xp[0].size(); color < max; color++) {
        target.hash("Xf").array(color).setDouble(values.Xp[0].at(color));
    }
    for (int color = 0, max = values.Xp[1].size(); color < max; color++) {
        target.hash("Xfs").array(color).setDouble(values.Xp[1].at(color));
    }
}

int AcquireMageeAethalometer33::processRecord(const Util::ByteView &line, double &frameTime)
{

    ReportedValues values;
    int code = parseRecord(line, frameTime, values);
    if (code != 0) return code;

    if (!FP::defined(frameTime))
        return -1;

    if (values == priorValues) {
        if (FP::defined(lastReportTime) &&
                (frameTime - lastReportTime) < (double) (values.timeBase * 2 + 1))
            return -1;
    }
    priorValues = values;

    Q_ASSERT(values.Ip[0].size() == values.If.size());
    Q_ASSERT(values.Ip[1].size() == values.If.size());
    Q_ASSERT(values.X.size() == values.If.size());
    Q_ASSERT(values.Xp[0].size() == values.If.size());
    Q_ASSERT(values.Xp[1].size() == values.If.size());
    Q_ASSERT(values.K.size() == values.If.size());

    double startTime = lastReportTime;
    double endTime = frameTime;
    lastReportTime = frameTime;

    double dT = values.timeBase;
    switch (config.first().timeMode) {
    case Configuration::PureReported:
        break;
    case Configuration::PureMeasured:
        if (!FP::defined(startTime))
            break;
        if (!FP::defined(endTime))
            break;
        if (endTime <= startTime)
            break;
        dT = endTime - startTime;
        break;
    case Configuration::RoundedMeasured:
        if (!FP::defined(startTime))
            break;
        if (!FP::defined(endTime))
            break;
        if (endTime <= startTime)
            break;
        dT = endTime - startTime;
        if (dT < (double) values.timeBase) {
            dT = values.timeBase;
        } else {
            dT = floor(dT / (double) values.timeBase + 0.5) * (double) values.timeBase;
        }
        break;
    }

    if (!haveEmittedLogMeta && loggingEgress != NULL && FP::defined(startTime)) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    if (!haveEmittedParameters && persistentEgress != NULL && dataParameters.read().exists()) {
        haveEmittedParameters = true;
        persistentEgress->emplaceData(SequenceName({}, "raw", "ZPARAMETERS"), dataParameters,
                                      frameTime, FP::undefined());
    }

    Variant::Flags flags;
    flags.insert("STP");

    Variant::Root Treport(values.Treport);
    remap("ZTReport", Treport);
    Variant::Root Preport(values.Preport);
    remap("ZPReport", Preport);

    bool realtimeDiagnostics = config.first().realtimeDiagnostics;

    Variant::Root ZLEAKAGE(firstValid(config.first().leakageFactor, parameters.leakageFactor, 0.0));
    remap("ZLEAKAGE", ZLEAKAGE);
    if (realtimeDiagnostics)
        realtimeValue(endTime, "ZLEAKAGE", ZLEAKAGE);

    Variant::Root Q1(values.Q1);
    remap("ZQ1", Q1);
    double Q1original =
            correctFlow(Q1.read().toDouble(), Treport.read().toDouble(), Preport.read().toDouble());
    Q1.write()
      .setDouble(applyLeakageFactor(config.first().flowCalibration[0].apply(Q1original),
                                    ZLEAKAGE.read().toDouble()));
    remap("Q1", Q1);
    logValue(startTime, endTime, "Q1", Q1);

    Variant::Root QC(values.QC);
    remap("ZQCRaw", QC);
    QC.write()
      .setDouble(applyLeakageFactor(config.first()
                                          .flowCalibrationTotal
                                          .apply(correctFlow(QC.read().toDouble(),
                                                             Treport.read().toDouble(),
                                                             Preport.read().toDouble())),
                                    ZLEAKAGE.read().toDouble()));
    remap("ZQC", QC);
    if (realtimeDiagnostics)
        realtimeValue(endTime, "ZQC", QC);

    Variant::Root Q2(config.first().flowCalibration[1].apply(
            calculateSpot2Flow(Q1.read().toDouble(), QC.read().toDouble())));
    remap("Q2", Q2);
    logValue(startTime, endTime, "Q2", Q2);

    Variant::Root T1(values.T1);
    remap("T1", T1);
    logValue(startTime, endTime, "T1", T1);

    Variant::Root T2(values.T2);
    remap("T2", T2);
    logValue(startTime, endTime, "T2", T2);

    Variant::Root T3(values.T3);
    remap("T3", T3);
    logValue(startTime, endTime, "T3", T3);

    Variant::Root status(values.status);
    remap("ZSTATUS", status);
    qint64 statusBits = status.read().toInt64();
    bool accumulatorReset = false;
    if (INTEGER::defined(statusBits)) {
        switch (statusBits & 0x3) {
        case 0:
            break;
        case 1:
            flags.insert("NotMeasuring");
            accumulatorReset = true;
            break;
        case 2:
            flags.insert("Calibrating");
            accumulatorReset = true;
            break;
        case 3:
            flags.insert("Stopped");
            accumulatorReset = true;
            break;
        default:
            Q_ASSERT(false);
            break;
        }

        switch ((statusBits >> 2) & 0x3) {
        case 0:
            break;
        case 1:
            flags.insert("FlowOutOfRange");
            break;
        case 2:
            flags.insert("FlowCheckHistory");
            break;
        case 3:
            flags.insert("FlowOutOfRange");
            flags.insert("FlowCheckHistory");
            break;
        default:
            Q_ASSERT(false);
            break;
        }

        switch ((statusBits >> 4) & 0x3) {
        case 0:
            break;
        case 1:
            flags.insert("LEDCalibration");
            break;
        case 2:
            flags.insert("LEDCalibrationError");
            break;
        case 3:
            flags.insert("LEDError");
            break;
        default:
            Q_ASSERT(false);
            break;
        }

        if ((statusBits >> 5) & 1) {
            flags.insert("ChamberError");
        }

        switch ((statusBits >> 8) & 0x3) {
        case 0:
            break;
        case 1:
            flags.insert("TapeLow");
            break;
        case 2:
            flags.insert("TapeLow");
            flags.insert("TapeCritical");
            break;
        case 3:
            flags.insert("TapeError");
            break;
        default:
            Q_ASSERT(false);
            break;
        }

        switch ((statusBits >> 10) & 0x3) {
        case 0:
            break;
        case 1:
            flags.insert("StabilityTest");
            break;
        case 2:
            flags.insert("CleanAirTest");
            break;
        case 3:
            flags.insert("ChangeTapeTest");
            break;
        default:
            Q_ASSERT(false);
            break;
        }
    }

    Variant::Root controlStatus(values.controlStatus);
    remap("ZCONTROL", controlStatus);
    statusBits = controlStatus.read().toInt64();
    if (INTEGER::defined(statusBits)) {
        switch (statusBits) {
        case 100:
            flags.insert("ControllerNotReady");
            break;
        case 255:
            flags.insert("ControllerBusy");
            break;
        default:
            break;
        }
    }

    Variant::Root detectStatus(values.detectStatus);
    remap("ZDETECT", detectStatus);
    statusBits = detectStatus.read().toInt64();
    if (INTEGER::defined(statusBits)) {
        switch (statusBits) {
        case 0:
            flags.insert("DetectorInitializationError");
            break;
        case 10:
            /* Normal measurement */
            break;
        case 20:
            flags.insert("DetectorStopped");
            break;
        case 30:
            flags.insert("DetectorLEDCalibration");
            break;
        case 40:
            flags.insert("DetectorFastLEDCalibration");
            break;
        case 55:
            flags.insert("DetectorReadNDF0");
            break;
        case 56:
            flags.insert("DetectorReadNDF1");
            break;
        case 57:
            flags.insert("DetectorReadNDF2");
            break;
        case 58:
            flags.insert("DetectorReadNDF3");
            break;
        case 59:
            flags.insert("DetectorReadNDFError");
            break;
        default:
            break;
        }
    }

    Variant::Root ledStatus(values.ledStatus);
    remap("ZLED", ledStatus);
    statusBits = detectStatus.read().toInt64();
    if (INTEGER::defined(statusBits)) {
        switch (statusBits) {
        case 0:
            flags.insert("LEDError");
            break;
        case 10:
            /* Normal measurement */
            break;
        default:
            break;
        }
    }

    Variant::Root valveStatus(values.valveStatus);
    remap("ZVALVE", valveStatus);

    Variant::Root updatedStatus;
    updatedStatus["Global"].set(status);
    updatedStatus["Controller"].set(controlStatus);
    updatedStatus["Detector"].set(detectStatus);
    updatedStatus["LED"].set(ledStatus);
    updatedStatus["Valve"].set(valveStatus);
    if (updatedStatus.read() != instrumentStatus.read()) {
        if (persistentEgress != NULL && FP::defined(instrumentStatus.getStart())) {
            instrumentStatus.setEnd(frameTime);
            persistentEgress->incomingData(instrumentStatus);
        }
        instrumentStatus.write().set(updatedStatus);

        instrumentStatus.setStart(frameTime);
        instrumentStatus.setEnd(FP::undefined());
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(instrumentStatus);
        }

        persistentValuesUpdated();
    }

    Variant::Root Fn(values.Fn);
    remap("Fn", Fn);

    if (INTEGER::defined(Fn.read().toInt64()) &&
            (!INTEGER::defined(normalizationLastSpotAdvance) ||
                    normalizationLastSpotAdvance != Fn.read().toInt64())) {
        if (persistentEgress != NULL && FP::defined(spotAdvance.getStart())) {
            spotAdvance.setEnd(frameTime);
            persistentEgress->incomingData(spotAdvance);
        }
        spotAdvance.write().set(Fn);

        spotAdvance.setStart(frameTime);
        spotAdvance.setEnd(FP::undefined());
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(spotAdvance);
        }

        persistentValuesUpdated();

        flags.insert("SpotAdvanced");
        accumulatorReset = true;
        normalizationLastSpotAdvance = Fn.read().toInt64();
        fvrfRecalculate = true;
    }

    if (!config.front().disableEBCZeroCheck) {
        /* Check for all BCs being zero and assume this means we're in a
         * transition state (so don't log them) */
        bool allBCZero = true;
        for (int color = 0, max = values.X.size(); color < max; color++) {
            if (values.X.at(color) != 0.0) {
                allBCZero = false;
                break;
            }
            if (values.Xp[0].at(color) != 0.0) {
                allBCZero = false;
                break;
            }
            if (values.Xp[1].at(color) != 0.0) {
                allBCZero = false;
                break;
            }
        }
        if (allBCZero) {
            for (int color = 0, max = values.X.size(); color < max; color++) {
                values.X.fill(FP::undefined());
                values.Xp[0].fill(FP::undefined());
                values.Xp[1].fill(FP::undefined());
            }
            accumulatorReset = true;
        }
    }

    double priorVolume[2];
    double currentVolume[2];

    if (accumulatorReset || !FP::defined(normalization.startTime)) {
        if (!normalizationUpdating &&
                FP::defined(normalization.startTime) &&
                persistentEgress != NULL) {
            qCDebug(log) << "Spot end detected at" << Logging::time(frameTime);

            SequenceValue result(SequenceName({}, "raw", "ZSPOT"), buildNormalizationValue(),
                                 normalization.startTime, endTime);
            persistentEgress->incomingData(result);

            {
                Variant::Write info = Variant::Write::empty();
                insertDiagnosticValues(info, values);
                info.hash("F1").setFlags(flags);
                info.hash("PriorStart").setReal(normalization.startTime);
                event(endTime, QObject::tr("Spot ended."), false, info);
            }

            persistentValuesUpdated();
        } else if (!normalizationUpdating) {
            qCDebug(log) << "Entering normalization update at" << Logging::time(frameTime);

            {
                Variant::Write info = Variant::Write::empty();
                insertDiagnosticValues(info, values);
                info.hash("F1").setFlags(flags);
                event(endTime, QObject::tr("Starting spot normalization."), false, info);
            }
        }

        normalization = IntensityData();
        normalizationUpdating = true;
    } else if (normalizationUpdating) {
        qCDebug(log) << "Spot begin detected at" << Logging::time(frameTime);

        if (FP::defined(startTime))
            normalization.startTime = startTime;
        else
            normalization.startTime = endTime;

        normalizationLastSpotAdvance = values.Fn;
        normalizationUpdating = false;

        {
            Variant::Write info = Variant::Write::empty();
            insertDiagnosticValues(info, values);
            info.hash("F1").setFlags(flags);
            event(endTime, QObject::tr("Ending spot normalization."), false, info);
        }

        if (state != NULL)
            state->requestStateSave();
        persistentValuesUpdated();
    }

    if (normalizationUpdating) {
        flags.insert("SpotAdvanced");

        priorVolume[0] = FP::undefined();
        priorVolume[1] = FP::undefined();
        currentVolume[0] = FP::undefined();
        currentVolume[1] = FP::undefined();

        normalizationLastVolume[0] = 0.0;
        normalizationLastVolume[1] = 0.0;
        normalizationResumeTime = FP::undefined();
        normalizationLatestTime = FP::undefined();

        normalization.startTime = endTime;
        normalizationLastSpotAdvance = values.Fn;

        if (!currentFilterAttenuation.isEmpty() && FP::defined(currentFilterAttenuation.at(0))) {
            priorFilterAttenuation = currentFilterAttenuation;
            priorFilterCorrection = currentFilterCorrection;
        }
        currentFilterAttenuation.clear();
        currentFilterCorrection.clear();

        fvrfRatioPoints.clear();
        fvrfAttenuationPoints.clear();
        fvrfRecalculate = true;

        for (int spot = 0; spot < 2; spot++) {
            realtimeValue(endTime, "L" + std::to_string(spot + 1), Variant::Root(FP::undefined()));

            priorIr[spot].clear();
        }
    } else {
        double extraTime = 0.0;
        if (FP::defined(normalizationResumeTime)) {
            if (FP::defined(startTime)) {
                if (normalizationResumeTime < startTime) {
                    extraTime += startTime - normalizationResumeTime;
                }
            } else if (FP::defined(endTime)) {
                double t = endTime - (double) values.timeBase;
                if (normalizationResumeTime < t) {
                    extraTime += t - normalizationResumeTime;
                }
            }
            normalizationResumeTime = FP::undefined();
        }
        for (int spot = 0; spot < 2; spot++) {
            Variant::Root Q;
            switch (spot) {
            case 0:
                Q = Q1;
                break;
            case 1:
                Q = Q2;
                break;
            default:
                Q_ASSERT(false);
                break;
            }

            priorVolume[spot] = normalizationLastVolume[spot];
            Variant::Root
                    Qt(integrateVolume(priorVolume[spot], Q.read().toDouble(), dT + extraTime));
            {
                auto name = "Qt" + std::to_string(spot + 1);
                remap(name, Qt);
                logValue(startTime, endTime, std::move(name), Qt);
            }
            currentVolume[spot] = Qt.read().toDouble();
            normalizationLastVolume[spot] = currentVolume[spot];

            Variant::Root L(calculateL(Qt.read().toDouble(),
                                       firstDefaulting(config.first().area[spot].read().toDouble(),
                                                       parameters.area, 78.5)));
            {
                auto name = "L" + std::to_string(spot + 1);
                remap(name, L);
                logValue(startTime, endTime, std::move(name), L);
            }
        }

        normalizationLatestTime = endTime;
    }

    double weingartnerConstant =
            firstValid(config.first().weingartnerConstant, parameters.weingartnerConstant, 1.57);
    double ATNf1 = firstValid(config.first().ATNf1, parameters.ATNf1, 10.0);
    double ATNf2 = firstValid(config.first().ATNf2, parameters.ATNf2, 30.0);
    double kMin = firstValid(config.first().kMin, parameters.kMin, -0.005);
    double kMax = firstValid(config.first().kMax, parameters.kMax, 0.015);

    int highestLoading = 0;
    double highestLoadingATN = 0;
    int lowestLoading = 0;
    double lowestLoadingATN = 0;
    std::vector<Variant::Root> ATN1;
    std::vector<Variant::Root> ATN2;
    std::vector<Variant::Root> Ba1;
    std::vector<Variant::Root> Ba2;
    std::vector<Variant::Root> K;
    ATN1.resize(values.X.size());
    ATN2.resize(values.X.size());
    Ba1.resize(values.X.size());
    Ba2.resize(values.X.size());
    K.resize(values.X.size());
    for (int color = 0, max = values.X.size(); color < max; color++) {
        Variant::Root If(values.If.at(color));
        {
            auto name = "If" + std::to_string(color + 1);
            remap(name, If);
            logValue(startTime, endTime, std::move(name), If);
        }

        Variant::Root Ip1(values.Ip[0].at(color));
        {
            auto name = "Ip" + std::to_string(color + 1);
            remap(name, Ip1);
            logValue(startTime, endTime, std::move(name), Ip1);
        }

        Variant::Root Ip2(values.Ip[1].at(color));
        {
            auto name = "Ips" + std::to_string(color + 1);
            remap(name, Ip2);
            logValue(startTime, endTime, std::move(name), Ip2);
        }

        Variant::Root X(correctEBC(values.X.at(color), Treport.read().toDouble(),
                                   Preport.read().toDouble()));
        {
            auto name = "X" + std::to_string(color + 1);
            remap(name, X);
            logValue(startTime, endTime, std::move(name), X);
        }

        Variant::Root Xf1(correctEBC(values.Xp[0].at(color), Treport.read().toDouble(),
                                     Preport.read().toDouble()));
        {
            auto name = "Xf" + std::to_string(color + 1);
            remap(name, Xf1);
            logValue(startTime, endTime, std::move(name), Xf1);
        }

        Variant::Root Xf2(correctEBC(values.Xp[1].at(color), Treport.read().toDouble(),
                                     Preport.read().toDouble()));
        {
            auto name = "Xfs" + std::to_string(color + 1);
            remap(name, Xf2);
            logValue(startTime, endTime, std::move(name), Xf2);
        }

        K[color].write().setDouble(values.K.at(color));
        {
            auto name = "ZFACTOR" + std::to_string(color + 1);
            remap(name, K[color]);
            logValue(startTime, endTime, std::move(name), K[color]);
        }

        Variant::Root ATN1c(recoverATN(X.read().toDouble(), Xf1.read().toDouble(),
                                       K[color].read().toDouble()));
        {
            auto name = "ZATNc" + std::to_string(color + 1);
            remap(name, ATN1c);
            if (realtimeDiagnostics)
                realtimeValue(endTime, std::move(name), ATN1c);
        }

        Variant::Root In1(calculateIn(If.read().toDouble(), Ip1.read().toDouble()));
        {
            auto name = "In" + std::to_string(color + 1);
            remap(name, In1);
            if (realtimeDiagnostics)
                realtimeValue(endTime, std::move(name), In1);
        }

        while (color >= latestIn[0].size()) {
            latestIn[0].append(FP::undefined());
        }
        latestIn[0][color] = In1.read().toDouble();

        Variant::Root In2(calculateIn(If.read().toDouble(), Ip2.read().toDouble()));
        {
            auto name = "Ins" + std::to_string(color + 1);
            remap(name, In2);
            if (realtimeDiagnostics)
                realtimeValue(endTime, std::move(name), In2);
        }

        while (color >= latestIn[1].size()) {
            latestIn[1].append(FP::undefined());
        }
        latestIn[1][color] = In2.read().toDouble();


        if (normalizationUpdating) {
            while (color >= normalization.If.size()) {
                normalization.If.append(FP::undefined());
            }
            normalization.If[color] = If.read().toDouble();

            while (color >= normalization.Ip[0].size()) {
                normalization.Ip[0].append(FP::undefined());
            }
            normalization.Ip[0][color] = Ip1.read().toDouble();

            while (color >= normalization.Ip[1].size()) {
                normalization.Ip[1].append(FP::undefined());
            }
            normalization.Ip[1][color] = Ip2.read().toDouble();

            while (color >= normalization.In[0].size()) {
                normalization.In[0].append(FP::undefined());
            }
            normalization.In[0][color] = In1.read().toDouble();

            while (color >= normalization.In[1].size()) {
                normalization.In[1].append(FP::undefined());
            }
            normalization.In[1][color] = In2.read().toDouble();

            realtimeValue(endTime, "Ir" + std::to_string(color + 1),
                          Variant::Root(FP::undefined()));
            realtimeValue(endTime, "Irs" + std::to_string(color + 1),
                          Variant::Root(FP::undefined()));
            continue;
        }

        double wavelength;
        if (color >= config.first().wavelengths.size())
            wavelength = FP::undefined();
        else
            wavelength = config.first().wavelengths.at(color);

        if (color < normalization.In[0].size()) {
            Variant::Root Ir(calculateIr(normalization.In[0][color], In1.read().toDouble()));
            {
                auto name = "Ir" + std::to_string(color + 1);
                remap(name, Ir);
                logValue(startTime, endTime, name, Ir);
            }

            while (color >= priorIr[0].size()) {
                priorIr[0].append(FP::undefined());
            }

            Ba1[color].write()
                      .setDouble(calculateBa(priorIr[0].at(color), priorVolume[0],
                                             Ir.read().toDouble(), currentVolume[0],
                                             firstDefaulting(
                                                     config.first().area[0].read().toDouble(),
                                                     parameters.area, 78.5)));
            {
                auto name = "Ba" + std::to_string(color + 1);
                remap(name, Ba1[color]);
                logValue(startTime, endTime, name, Ba1[color]);
            }

            if (realtimeDiagnostics) {
                Variant::Root Xc(applyWeingartner(calculateEBC(Ba1[color].read().toDouble(),
                                                               calculateEfficiency(wavelength,
                                                                                   config.first()
                                                                                         .absorptionEfficiency,
                                                                                   parameters.efficiency,
                                                                                   color)),
                                                  weingartnerConstant));
                {
                    auto name = "ZX" + std::to_string(color + 1);
                    remap(name, Xc);
                    realtimeValue(endTime, name, Xc);
                }
            }

            priorIr[0][color] = Ir.read().toDouble();

            ATN1[color].write().setDouble(calculateATN(Ir.read().toDouble()));
            {
                auto name = "ZATN" + std::to_string(color + 1);
                remap(name, ATN1[color]);
                if (realtimeDiagnostics)
                    realtimeValue(endTime, std::move(name), ATN1[color]);
            }

            if (FP::defined(ATN1[color].read().toDouble()) &&
                    ATN1[color].read().toDouble() > highestLoadingATN) {
                highestLoadingATN = ATN1[color].read().toDouble();
                highestLoading = color;
            }
            if (FP::defined(ATN1[color].read().toDouble()) &&
                    (lowestLoadingATN <= 0.0 || ATN1[color].read().toDouble() < lowestLoadingATN)) {
                lowestLoadingATN = ATN1[color].read().toDouble();
                lowestLoading = color;
            }
        }

        if (color < normalization.In[1].size()) {
            Variant::Root Ir(calculateIr(normalization.In[1][color], In2.read().toDouble()));
            {
                auto name = "Irs" + std::to_string(color + 1);
                remap(name, Ir);
                logValue(startTime, endTime, std::move(name), Ir);
            }

            while (color >= priorIr[1].size()) {
                priorIr[1].append(FP::undefined());
            }

            Ba2[color].write()
                      .setDouble(calculateBa(priorIr[1].at(color), priorVolume[1],
                                             Ir.read().toDouble(), currentVolume[1],
                                             firstDefaulting(
                                                     config.first().area[1].read().toDouble(),
                                                     parameters.area, 78.5)));
            {
                auto name = "Bas" + std::to_string(color + 1);
                remap(name, Ba2[color]);
                logValue(startTime, endTime, std::move(name), Ba2[color]);
            }

            if (realtimeDiagnostics) {
                Variant::Root Xc(applyWeingartner(calculateEBC(Ba2[color].read().toDouble(),
                                                               calculateEfficiency(wavelength,
                                                                                   config.first()
                                                                                         .absorptionEfficiency,
                                                                                   parameters.efficiency,
                                                                                   color)),
                                                  weingartnerConstant));
                {
                    auto name = "ZXs" + std::to_string(color + 1);
                    remap(name, Xc);
                    realtimeValue(endTime, std::move(name), Xc);
                }
            }

            priorIr[1][color] = Ir.read().toDouble();

            ATN2[color].write().setDouble(calculateATN(Ir.read().toDouble()));
            {
                auto name = "ZATNs" + std::to_string(color + 1);
                remap(name, ATN2[color]);
                if (realtimeDiagnostics)
                    realtimeValue(endTime, std::move(name), ATN2[color]);
            }
        }
    }

    bool isCalculatingFVRF =
            shouldCalculateFVRF(ATN1[highestLoading].read().toDouble(), ATNf1, ATNf2);
    if (!isCalculatingFVRF && fvrfRecalculate && !fvrfRatioPoints.isEmpty()) {
        double sum = 0.0;
        int count = 0;
        double totalSum = 0.0;
        int totalCount = 0;
        Variant::Write info = Variant::Write::empty();
        for (int i = 0, imax = qMin(fvrfRatioPoints.size(), fvrfAttenuationPoints.size());
                i < imax;
                i++) {
            BasicOrdinaryLeastSquares ols(fvrfAttenuationPoints.at(i), fvrfRatioPoints.at(i));
            double f = ols.intercept();

            if (!FP::defined(f) || f <= 0.01 || f >= 2.0)
                continue;

            totalSum += f;
            totalCount++;

            if (i == highestLoading)
                continue;
            if (i == lowestLoading)
                continue;
            sum += f;
            count++;

            info.hash("Intercepts").toArray().after_back().setReal(f);
            info.hash("TotalPoints").toArray().after_back().setInteger(fvrfRatioPoints.size());
        }
        double result = FP::undefined();
        if (count > 0) {
            result = sum / (double) count;
        } else if (totalCount > 0) {
            result = totalSum / (double) totalCount;
        }

        fvrfFinal = calculateFVRF(result, latestVolumeRatio);

        qCDebug(log) << "FVRF calculated at" << Logging::time(frameTime) << "is" << fvrfFinal;

        info.hash("VolumeRatio").setDouble(latestVolumeRatio);
        info.hash("FVRF").setDouble(fvrfFinal);
        insertDiagnosticValues(info, values);
        event(endTime, QObject::tr("FVRF updated."), false, info);
    }
    if (!isCalculatingFVRF) {
        fvrfRatioPoints.clear();
        fvrfAttenuationPoints.clear();
    }

    for (int color = 0, max = Ba1.size(); color < max; color++) {
        double wavelength;
        if (color >= config.first().wavelengths.size())
            wavelength = FP::undefined();
        else
            wavelength = config.first().wavelengths.at(color);

        Variant::Root ATNRatio
                (calculateATNRatio(ATN1[color].read().toDouble(), ATN2[color].read().toDouble()));
        {
            auto name = "ZATNRatio" + std::to_string(color + 1);
            remap(name, ATNRatio);
        }

        if (isCalculatingFVRF) {
            double ratio = ATNRatio.read().toDouble();
            if (FP::defined(ratio)) {
                while (color >= fvrfRatioPoints.size()) {
                    fvrfRatioPoints.append(QVector<double>());
                }
                while (color >= fvrfAttenuationPoints.size()) {
                    fvrfAttenuationPoints.append(QVector<double>());
                }
                fvrfRatioPoints[color].append(ratio);
                fvrfAttenuationPoints[color].append(ATN1[color].read().toDouble());

                limitFVRFBuffers(fvrfAttenuationPoints[color], fvrfRatioPoints[color]);

                latestVolumeRatio = currentVolume[1] / currentVolume[0];
            }
        }

        Variant::Root FVRF(fvrfFinal);
        if (!FP::defined(FVRF.read().toDouble()) || FVRF.read().toDouble() <= 0.01)
            FVRF.write().setDouble(1.0);
        {
            auto name = "ZFVRF" + std::to_string(color + 1);
            remap(name, FVRF);
            if (realtimeDiagnostics)
                realtimeValue(endTime, std::move(name), FVRF);
        }


        Variant::Root Kcalc(calculateFactor(ATN1[color].read().toDouble(), currentVolume[0],
                                            ATN2[color].read().toDouble(), currentVolume[1],
                                            FVRF.read().toDouble(), K[color].read().toDouble()));
        {
            auto name = "ZFACTORp" + std::to_string(color + 1);
            remap(name, Kcalc);
            if (realtimeDiagnostics)
                realtimeValue(endTime, std::move(name), Kcalc);
        }

        if (FP::defined(ATN1[color].read().toDouble()) && FP::defined(Kcalc.read().toDouble())) {
            while (color >= currentFilterAttenuation.size()) {
                currentFilterAttenuation.append(FP::undefined());
            }
            currentFilterAttenuation[color] = ATN1[color].read().toDouble();

            while (color >= currentFilterCorrection.size()) {
                currentFilterCorrection.append(FP::undefined());
            }
            currentFilterCorrection[color] = Kcalc.read().toDouble();
        }

        if (highestLoading < priorFilterAttenuation.size() &&
                color < priorFilterCorrection.size()) {
            Kcalc.write()
                 .setDouble(smoothFactor(priorFilterCorrection.at(color),
                                         priorFilterAttenuation.at(highestLoading),
                                         Kcalc.read().toDouble(),
                                         ATN1[highestLoading].read().toDouble(), ATNf2, kMin,
                                         kMax));
        }
        if (!FP::defined(Kcalc.read().toReal())) {
            Kcalc = K[color];
            flags.insert("FallbackCorrectionFactor");
        }
        {
            auto name = "ZFACTORc" + std::to_string(color + 1);
            remap(name, Kcalc);
            if (realtimeDiagnostics)
                realtimeValue(endTime, std::move(name), Kcalc);
        }

        Variant::Root Bac(applyWeingartner(
                applyFactor(Ba1[color].read().toDouble(), ATN1[color].read().toDouble(),
                            Kcalc.read().toDouble()), weingartnerConstant));
        {
            auto name = "Bac" + std::to_string(color + 1);
            remap(name, Bac);
            logValue(startTime, endTime, std::move(name), Bac);
        }

        Variant::Root Xcalc(calculateEBC(Bac.read().toDouble(), calculateEfficiency(wavelength,
                                                                                    config.first()
                                                                                          .absorptionEfficiency,
                                                                                    parameters.efficiency,
                                                                                    color)));
        {
            auto name = "Xp" + std::to_string(color + 1);
            remap(name, Xcalc);
            logValue(startTime, endTime, std::move(name), Xcalc);
        }
    }

    {
        Variant::Root volumeRatio(calculateVolumeRatio(currentVolume[0], currentVolume[1]));
        remap("ZVolumeRatio", volumeRatio);
        double v = volumeRatio.read().toDouble();
        if (FP::defined(v))
            latestVolumeRatio = v;
    }

    logValue(startTime, endTime, "F1", Variant::Root(flags));

    return 0;
}

Variant::Root AcquireMageeAethalometer33::buildNormalizationValue() const
{
    Q_ASSERT(!config.isEmpty());

    Variant::Root result;

    for (std::size_t idx = 0, max = config.front().wavelengths.size(); idx < max; ++idx) {
        if (idx < normalization.In[0].size()) {
            result["In1"].array(idx).setDouble(normalization.In[0][idx]);
        }
        if (idx < normalization.In[1].size()) {
            result["In2"].array(idx).setDouble(normalization.In[1][idx]);
        }

        if (idx < latestIn[0].size()) {
            result["In1Latest"].array(idx).setDouble(latestIn[0][idx]);
        }
        if (idx < latestIn[1].size()) {
            result["In2Latest"].array(idx).setDouble(latestIn[1][idx]);
        }

        if (idx < priorIr[0].size()) {
            result["Ir1"].array(idx).setDouble(priorIr[0][idx]);
        }
        if (idx < priorIr[1].size()) {
            result["Ir2"].array(idx).setDouble(priorIr[1][idx]);
        }
    }

    double area1 = firstDefaulting(config.front().area[0].read().toDouble(), parameters.area, 78.5);
    result["Area1"].setDouble(area1);
    double area2 = firstDefaulting(config.front().area[1].read().toDouble(), parameters.area, 78.5);
    result["Area2"].setDouble(area2);

    result["L1"].setDouble(calculateL(area1, normalizationLastVolume[0]));
    result["L2"].setDouble(calculateL(area2, normalizationLastVolume[1]));

    result["Fn"].set(spotAdvance.getValue());
    result["Qt1"].setDouble(normalizationLastVolume[0]);
    result["Qt2"].setDouble(normalizationLastVolume[1]);

    return result;
}

void AcquireMageeAethalometer33::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
}

void AcquireMageeAethalometer33::configurationAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    int oldNWavelengths = config.first().wavelengths.size();
    bool oldRealtimeDiagnostics = config.first().realtimeDiagnostics;
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();

    int newNWavelengths = config.first().wavelengths.size();
    bool newRealtimeDiagnostics = config.first().realtimeDiagnostics;
    if (newNWavelengths < oldNWavelengths || newRealtimeDiagnostics != oldRealtimeDiagnostics) {
        if (loggingEgress != NULL) {
            loggingEgress->incomingData(
                    invalidateLogMetadata(frameTime, oldNWavelengths, newNWavelengths));
        }
        if (realtimeEgress != NULL) {
            SequenceValue::Transfer
                    meta = invalidateLogMetadata(frameTime, oldNWavelengths, newNWavelengths);
            Util::append(invalidateRealtimeMetadata(frameTime, oldNWavelengths, newNWavelengths,
                                                    oldRealtimeDiagnostics, newRealtimeDiagnostics),
                         meta);
            realtimeEgress->incomingData(std::move(meta));
        }
    }
}

void AcquireMageeAethalometer33::invalidateLogValues(double frameTime)
{
    loggingLost(frameTime);
    priorValues = ReportedValues();
    lastReportTime = FP::undefined();
    parameters.invalidate();
}


double AcquireMageeAethalometer33::effectiveTimeout() const
{
    if (!INTEGER::defined(priorValues.timeBase) || priorValues.timeBase <= 0)
        return 120.0;
    return (double) (priorValues.timeBase * 2 + 1);
}

void AcquireMageeAethalometer33::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (FP::defined(frameTime))
        configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

            responseState = RESP_PASSIVE_RUN;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            timeoutAt(frameTime + effectiveTimeout());

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveAutoprobeWait");
            event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                      FP::undefined()));
            }
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }
    case RESP_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);
            responseState = RESP_PASSIVE_RUN;
            generalStatusUpdated();

            timeoutAt(frameTime + effectiveTimeout());

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                      FP::undefined()));
            }
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }


    case RESP_INTERACTIVE_START_PARAMETERS_READ:
    case RESP_INTERACTIVE_START_PARAMETERS_DATARETRY: {
        Util::ByteView temp(frame);
        dataParameters.write().setEmpty();
        int code = parseParameters(temp);
        if (code > 0) {
            /* Try parsing it as just a data response, in case we tried
             * this against an older firmware that just ignored it */
            ReportedValues values;
            double reportTime = FP::undefined();
            int checkCode = parseRecord(frame, reportTime, values, false);
            if (checkCode == 0) {
                qCDebug(log) << "Instrument ignored parameter request at"
                             << Logging::time(frameTime);

                parameters.invalidate();
                if (FP::defined(reportTime) &&
                        FP::defined(frameTime) &&
                        ::fabs(reportTime - frameTime) > 30.0 &&
                        config.first().setInstrumentTime) {
                    parameters.timeIncorrect = true;
                    responseState = RESP_INTERACTIVE_START_PARAMETERS_STOP;
                    discardData(frameTime + 0.5);
                    timeoutAt(frameTime + 10.0);

                    if (realtimeEgress != NULL) {
                        realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                                   Variant::Root(
                                                                           "StartInteractiveInstrumentStop"),
                                                                   frameTime, FP::undefined()));
                    }
                } else if (parameters.needToChange(config.first())) {
                    discardData(frameTime + 0.5);
                    timeoutAt(frameTime + 10.0);
                    parameters.timeIncorrect = false;
                    responseState = RESP_INTERACTIVE_START_PARAMETERS_STOP;

                    if (realtimeEgress != NULL) {
                        realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                                   Variant::Root(
                                                                           "StartInteractiveInstrumentStop"),
                                                                   frameTime, FP::undefined()));
                    }
                } else {
                    if (controlStream != NULL) {
                        controlStream->writeControl("$AE33:D1\r");
                    }
                    parameters.timeIncorrect = false;
                    responseState = RESP_INTERACTIVE_START_FIRSTVALID;
                    timeoutAt(frameTime + 5.0);

                    if (realtimeEgress != NULL) {
                        realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                                   Variant::Root(
                                                                           "StartInteractiveFirstValid"),
                                                                   frameTime, FP::undefined()));
                    }
                }
                break;
            } else if (checkCode < 0) {
                break;
            }

            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("ReadParameters");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the parameters read (code %1).").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
            break;
        } else if (code < 0) {
            break;
        }

        while (!temp.empty() && isDelimiter(temp[0])) {
            temp = temp.mid(1);
        }

        if (temp.empty()) {
            /* Keep waiting if we've already asked for a data record,
             * otherwise ask for one now. */

            if (responseState == RESP_INTERACTIVE_START_PARAMETERS_READ) {
                if (controlStream != NULL) {
                    controlStream->writeControl("$AE33:D1\r");
                }
                timeoutAt(frameTime + 2.0);
            } else {
                timeoutAt(frameTime + 1.0);
            }

            responseState = RESP_INTERACTIVE_START_PARAMETERS_RECORD;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveTimeRead"), frameTime, FP::undefined()));
            }
            break;
        }

        responseState = RESP_INTERACTIVE_START_PARAMETERS_RECORD;

        /* Now try to parse the time */
        ReportedValues values;
        double reportTime = FP::undefined();
        code = parseRecord(temp, reportTime, values, false);
        if (code == 0) {
            if (FP::defined(reportTime) &&
                    FP::defined(frameTime) &&
                    fabs(reportTime - frameTime) > 30.0 &&
                    config.first().setInstrumentTime) {
                discardData(frameTime + 0.5);
                timeoutAt(frameTime + 10.0);
                parameters.timeIncorrect = true;
                responseState = RESP_INTERACTIVE_START_PARAMETERS_STOP;

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveInstrumentStop"), frameTime, FP::undefined()));
                }
            } else if (parameters.needToChange(config.first())) {
                discardData(frameTime + 0.5);
                timeoutAt(frameTime + 10.0);
                parameters.timeIncorrect = false;
                responseState = RESP_INTERACTIVE_START_PARAMETERS_STOP;

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveInstrumentStop"), frameTime, FP::undefined()));
                }
            } else {
                if (controlStream != NULL) {
                    controlStream->writeControl("$AE33:D1\r");
                }
                responseState = RESP_INTERACTIVE_START_FIRSTVALID;
                timeoutAt(frameTime + 5.0);

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveFirstValid"), frameTime, FP::undefined()));
                }
            }
        } else if (code > 0) {
            /* If the parse failed, then just try issuing the read
             * normally in case we just got some garbage on the end */

            if (controlStream != NULL) {
                controlStream->writeControl("$AE33:D1\r");
            }
            timeoutAt(frameTime + 2.0);

        } /* Ignored line */
        break;
    }

    case RESP_INTERACTIVE_START_PARAMETERS_RECORD: {
        ReportedValues values;
        double reportTime = FP::undefined();
        int code = parseRecord(frame, reportTime, values, false);
        if (code == 0) {
            if (FP::defined(reportTime) &&
                    FP::defined(frameTime) &&
                    ::fabs(reportTime - frameTime) > 30.0 &&
                    config.first().setInstrumentTime) {
                parameters.timeIncorrect = true;
                responseState = RESP_INTERACTIVE_START_PARAMETERS_STOP;
                discardData(frameTime + 0.5);
                timeoutAt(frameTime + 10.0);

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveInstrumentStop"), frameTime, FP::undefined()));
                }
            } else if (parameters.needToChange(config.first())) {
                discardData(frameTime + 0.5);
                timeoutAt(frameTime + 10.0);
                parameters.timeIncorrect = false;
                responseState = RESP_INTERACTIVE_START_PARAMETERS_STOP;

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveInstrumentStop"), frameTime, FP::undefined()));
                }
            } else {
                if (controlStream != NULL) {
                    controlStream->writeControl("$AE33:D1\r");
                }
                parameters.timeIncorrect = false;
                responseState = RESP_INTERACTIVE_START_FIRSTVALID;
                timeoutAt(frameTime + 5.0);

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveFirstValid"), frameTime, FP::undefined()));
                }
            }
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("StartRecord");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the time line (code %1).").arg(code),
                  true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case RESP_INTERACTIVE_START_FIRSTVALID: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            responseState = RESP_INTERACTIVE_RUN;
            if (controlStream != NULL) {
                timeoutAt(frameTime + 2.0);
                controlStream->writeControl("$AE33:D1\r");
            } else {
                timeoutAt(frameTime + effectiveTimeout());
            }

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            event(frameTime, QObject::tr("Communications established."), false, info);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                      FP::undefined()));
            }
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (responseState == RESP_INTERACTIVE_RUN && controlStream != NULL) {
                if (FP::defined(config.first().pollInterval) && config.first().pollInterval > 0.0) {
                    timeoutAt(frameTime + config.first().pollInterval * 2.0 + 2.0);
                    discardData(frameTime + config.first().pollInterval);
                } else {
                    timeoutAt(frameTime + 2.0);
                    controlStream->writeControl("$AE33:D1\r");
                }
            } else {
                timeoutAt(frameTime + effectiveTimeout());
            }
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();

            if (responseState == RESP_PASSIVE_RUN) {
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
                info.hash("ResponseState").setString("PassiveRun");
            } else {
                responseState = RESP_INTERACTIVE_START_CLEAR;
                timeoutAt(frameTime + 30.0);
                discardData(frameTime + 0.5);
                info.hash("ResponseState").setString("Run");
            }
            generalStatusUpdated();

            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        } else if (responseState == RESP_INTERACTIVE_RUN && controlStream != NULL) {
            if (FP::defined(config.first().pollInterval) && config.first().pollInterval > 0.0) {
                timeoutAt(frameTime + config.first().pollInterval * 2.0 + 2.0);
                discardData(frameTime + config.first().pollInterval);
            } else {
                timeoutAt(frameTime + 2.0);
                controlStream->writeControl("$AE33:D1\r");
            }
        }
        break;
    }

    case RESP_INTERACTIVE_ADVANCE_HALT:
        if (controlStream != NULL) {
            controlStream->writeControl("$AE33:X0\r");
        }
        responseState = RESP_INTERACTIVE_ADVANCE_RESUME;
        discardData(frameTime + 5.0);
        timeoutAt(frameTime + 30.0);
        break;

    case RESP_INTERACTIVE_ADVANCE_RESUME:
        if (controlStream != NULL) {
            controlStream->writeControl("$AE33:X1\r");
        }
        responseState = RESP_INTERACTIVE_RUN;
        discardData(frameTime + 5.0);
        timeoutAt(frameTime + 30.0);
        break;

    default:
        break;
    }
}

void AcquireMageeAethalometer33::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_ADVANCE_HALT:
    case RESP_INTERACTIVE_ADVANCE_RESUME:
        qCDebug(log) << "Timeout in interactive state" << responseState << "at"
                     << Logging::time(frameTime);
        timeoutAt(frameTime + 30.0);
        responseState = RESP_INTERACTIVE_START_CLEAR;
        discardData(frameTime + 0.5);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("InteractiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_START_PARAMETERS_READ:
        /* If we timeout during parameters read, try issuing a data read
         * and see if we get anything */
        if (controlStream != NULL) {
            controlStream->writeControl("$AE33:D1\r");
        }

        responseState = RESP_INTERACTIVE_START_PARAMETERS_DATARETRY;
        timeoutAt(frameTime + 2.0);
        break;

    case RESP_INTERACTIVE_START_CLEAR:
    case RESP_INTERACTIVE_START_PARAMETERS_DATARETRY:
    case RESP_INTERACTIVE_START_PARAMETERS_RECORD:
    case RESP_INTERACTIVE_START_PARAMETERS_STOP:
    case RESP_INTERACTIVE_START_PARAMETERS_SET:
    case RESP_INTERACTIVE_START_PARAMETERS_START:
    case RESP_INTERACTIVE_START_FIRSTVALID:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        timeoutAt(frameTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("$AE33:D1\r");
        }
        responseState = RESP_INTERACTIVE_START_CLEAR;
        discardData(frameTime + 1.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        invalidateLogValues(frameTime);
        break;

    default:
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireMageeAethalometer33::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    switch (responseState) {
    case RESP_INTERACTIVE_START_CLEAR:
        if (controlStream != NULL) {
            controlStream->writeControl("$AE33:SG\r");

            responseState = RESP_INTERACTIVE_START_PARAMETERS_READ;
            timeoutAt(frameTime + 2.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveParametersRead"), frameTime, FP::undefined()));
            }
        } else {
            responseState = RESP_INTERACTIVE_START_FIRSTVALID;
            timeoutAt(frameTime + 5.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveFirstValid"), frameTime, FP::undefined()));
            }
        }
        break;

    case RESP_INTERACTIVE_START_PARAMETERS_STOP:
        if (controlStream != NULL) {
            controlStream->writeControl("$AE33:X0\r");
        }
        responseState = RESP_INTERACTIVE_START_PARAMETERS_SET;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 3.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetParameters"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_PARAMETERS_SET:
        if (parameters.needToChange(config.first()) && controlStream != NULL) {
            controlStream->writeControl(parameters.changeCommand(config.first(), frameTime));

            discardData(frameTime + 0.5);
            timeoutAt(frameTime + 5.0);
        } else {
            if (controlStream != NULL) {
                controlStream->writeControl("$AE33:X1\r");
            }

            responseState = RESP_INTERACTIVE_START_PARAMETERS_START;
            discardData(frameTime + 3.0);
            timeoutAt(frameTime + 10.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveInstrumentStart"), frameTime, FP::undefined()));
            }
        }
        break;

    case RESP_INTERACTIVE_START_PARAMETERS_START:
        if (controlStream != NULL) {
            controlStream->writeControl("$AE33:D1\r");
        }
        responseState = RESP_INTERACTIVE_START_FIRSTVALID;
        timeoutAt(frameTime + 5.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveFirstValid"),
                                                       frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        timeoutAt(frameTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("$AE33:D1\r");
        }
        responseState = RESP_INTERACTIVE_START_CLEAR;
        discardData(frameTime + 0.5);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_RUN:
        if (controlStream != NULL) {
            controlStream->writeControl("$AE33:D1\r");
        }
        timeoutAt(frameTime + 2.0);
        break;

    case RESP_INTERACTIVE_ADVANCE_HALT:
        if (controlStream != NULL) {
            controlStream->writeControl("$AE33:X0\r");
        }
        responseState = RESP_INTERACTIVE_ADVANCE_RESUME;
        discardData(frameTime + 5.0);
        timeoutAt(frameTime + 30.0);
        break;

    case RESP_INTERACTIVE_ADVANCE_RESUME:
        if (controlStream != NULL) {
            controlStream->writeControl("$AE33:X1\r");
        }
        responseState = RESP_INTERACTIVE_RUN;
        discardData(frameTime + 5.0);
        timeoutAt(frameTime + 30.0);
        break;

    default:
        break;
    }
}

void AcquireMageeAethalometer33::incomingControlFrame(const Util::ByteArray &frame,
                                                      double frameTime)
{
    Q_UNUSED(frameTime);
    Q_UNUSED(frame);
}


void AcquireMageeAethalometer33::command(const Variant::Read &command)
{
    if (command.hash("AdvanceSpot").exists()) {
        switch (responseState) {
        case RESP_INTERACTIVE_RUN:
            qCDebug(log) << "Advancing spot on external request in state" << responseState;
            responseState = RESP_INTERACTIVE_ADVANCE_HALT;
            break;
        default:
            qCDebug(log) << "Discarding spot advance in state" << responseState;
            break;
        }
    }
}

Variant::Root AcquireMageeAethalometer33::getCommands()
{
    Variant::Root result;

    result["AdvanceSpot"].hash("DisplayName").setString("&Advance Sampling Spot");
    result["AdvanceSpot"].hash("ToolTip")
                         .setString("Advance to the next spot.  This advances the tape.");
    result["AdvanceSpot"].hash("Exclude").array(0).hash("Type").setString("Flags");
    result["AdvanceSpot"].hash("Exclude").array(0).hash("Flags").setFlags({"SpotAdvanced"});
    result["AdvanceSpot"].hash("Exclude").array(0).hash("Variable").hash("F1");

    return result;
}

AcquisitionInterface::AutoprobeStatus AcquireMageeAethalometer33::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

SequenceValue::Transfer AcquireMageeAethalometer33::getPersistentValues()
{
    PauseLock paused(*this);
    SequenceValue::Transfer result;

    if (spotAdvance.read().exists() && FP::defined(spotAdvance.getStart())) {
        result.emplace_back(spotAdvance);
    }
    if (instrumentStatus.read().exists() && FP::defined(instrumentStatus.getStart())) {
        result.emplace_back(instrumentStatus);
    }
    if (!normalizationUpdating && FP::defined(normalization.startTime) && !config.isEmpty()) {
        result.emplace_back(SequenceName({}, "raw", "ZSPOT"), buildNormalizationValue(),
                            normalization.startTime, FP::undefined());
    }

    return result;
}

Variant::Root AcquireMageeAethalometer33::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireMageeAethalometer33::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_ADVANCE_HALT:
    case RESP_INTERACTIVE_ADVANCE_RESUME:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

void AcquireMageeAethalometer33::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_ADVANCE_HALT:
    case RESP_INTERACTIVE_ADVANCE_RESUME:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_CLEAR:
    case RESP_INTERACTIVE_START_PARAMETERS_READ:
    case RESP_INTERACTIVE_START_PARAMETERS_DATARETRY:
    case RESP_INTERACTIVE_START_PARAMETERS_RECORD:
    case RESP_INTERACTIVE_START_PARAMETERS_STOP:
    case RESP_INTERACTIVE_START_PARAMETERS_SET:
    case RESP_INTERACTIVE_START_PARAMETERS_START:
    case RESP_INTERACTIVE_START_FIRSTVALID:
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("$AE33:D1\r");
        }
        responseState = RESP_INTERACTIVE_START_CLEAR;
        discardData(time + 0.5);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireMageeAethalometer33::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    Q_ASSERT(!config.isEmpty());

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    discardData(time + 0.5, 1);
    timeoutAt(time + effectiveTimeout() * 2.0 + 1.0);
    generalStatusUpdated();
}

void AcquireMageeAethalometer33::autoprobePromote(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive run to interactive acquisition at"
                     << Logging::time(time);

        timeoutAt(time + 2.0);
        break;

    case RESP_INTERACTIVE_ADVANCE_HALT:
    case RESP_INTERACTIVE_ADVANCE_RESUME:
    case RESP_INTERACTIVE_START_CLEAR:
    case RESP_INTERACTIVE_START_PARAMETERS_READ:
    case RESP_INTERACTIVE_START_PARAMETERS_DATARETRY:
    case RESP_INTERACTIVE_START_PARAMETERS_RECORD:
    case RESP_INTERACTIVE_START_PARAMETERS_STOP:
    case RESP_INTERACTIVE_START_PARAMETERS_SET:
    case RESP_INTERACTIVE_START_PARAMETERS_START:
    case RESP_INTERACTIVE_START_FIRSTVALID:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 30.0);
        break;

    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("$AE33:D1\r");
        }
        responseState = RESP_INTERACTIVE_START_CLEAR;
        discardData(time + 0.5);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireMageeAethalometer33::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    Q_ASSERT(!config.isEmpty());

    timeoutAt(time + effectiveTimeout());

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_RUN:
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from interactive to passive acquisition at"
                     << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        generalStatusUpdated();
        break;
    }
}

static const quint16 serializedStateVersion = 6;

void AcquireMageeAethalometer33::serializeState(QDataStream &stream)
{
    PauseLock paused(*this);

    stream << serializedStateVersion;

    stream << normalization << normalizationLastVolume[0] << normalizationLastVolume[1]
           << normalizationLastSpotAdvance << normalizationLatestTime << priorFilterAttenuation
           << priorFilterCorrection << fvrfRatioPoints << fvrfAttenuationPoints << latestVolumeRatio
           << fvrfFinal << fvrfRecalculate;
}

void AcquireMageeAethalometer33::deserializeState(QDataStream &stream)
{
    quint16 version = 0;
    stream >> version;
    if (version != serializedStateVersion)
        return;

    PauseLock paused(*this);

    stream >> normalization >> normalizationLastVolume[0] >> normalizationLastVolume[1]
           >> normalizationLastSpotAdvance >> normalizationResumeTime >> priorFilterAttenuation
           >> priorFilterCorrection >> fvrfRatioPoints >> fvrfAttenuationPoints >> latestVolumeRatio
           >> fvrfFinal >> fvrfRecalculate;
}

QDataStream &operator<<(QDataStream &stream, const AcquireMageeAethalometer33::IntensityData &data)
{
    stream << data.startTime << data.If << data.Ip[0] << data.Ip[1] << data.In[0] << data.In[1];
    return stream;
}

QDataStream &operator>>(QDataStream &stream, AcquireMageeAethalometer33::IntensityData &data)
{
    stream >> data.startTime >> data.If >> data.Ip[0] >> data.Ip[1] >> data.In[0] >> data.In[1];
    return stream;
}

AcquireMageeAethalometer33::IntensityData::IntensityData() : startTime(FP::undefined()),
                                                             If(),
                                                             Ip(),
                                                             In()
{ }

AcquisitionInterface::AutomaticDefaults AcquireMageeAethalometer33::getDefaults()
{
    AutomaticDefaults result;
    result.name = "A$1$2";
    result.setSerialN81(115200);
    return result;
}


ComponentOptions AcquireMageeAethalometer33Component::getOptions()
{
    ComponentOptions options(getBaseOptions());

    LineIngressWrapper::addOptions(options, true);
    return options;
}

ComponentOptions AcquireMageeAethalometer33Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    ComponentOptionSingleCalibration *cal =
            new ComponentOptionSingleCalibration(tr("cal-q1", "name"),
                                                 tr("The calibration to apply to the flow through spot one"),
                                                 tr("This is the calibration applied to the reported flow rate "
                                                    "through spot one."), "");
    cal->setAllowInvalid(false);
    cal->setAllowConstant(true);
    options.add("cal-q1", cal);

    cal = new ComponentOptionSingleCalibration(tr("cal-q2", "name"),
                                               tr("The calibration to apply to the flow through spot two"),
                                               tr("This is the calibration applied to the reported flow rate "
                                                  "through spot two."), "");
    cal->setAllowInvalid(false);
    cal->setAllowConstant(true);
    options.add("cal-q2", cal);

    cal = new ComponentOptionSingleCalibration(tr("cal-qc", "name"),
                                               tr("The calibration to apply to the total flow"),
                                               tr("This is the calibration applied to the reported flow rate "
                                                  "through the total optics block.  This is used with the flow "
                                                  "through spot one to calculate the flow through spot two."),
                                               "");
    cal->setAllowInvalid(false);
    cal->setAllowConstant(true);
    options.add("cal-qc", cal);

    return options;
}

QList<ComponentExample> AcquireMageeAethalometer33Component::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples(true));
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireMageeAethalometer33Component::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireMageeAethalometer33Component::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireMageeAethalometer33Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data using the default spot sizes.")));

    options = getBaseOptions();
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("area1")))->set(42.12);
    examples.append(ComponentExample(options, tr("Explicitly set the sample spot size.")));

    return examples;
}

std::unique_ptr<AcquisitionInterface> AcquireMageeAethalometer33Component::createAcquisitionPassive(
        const ComponentOptions &options,
        const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(
            new AcquireMageeAethalometer33(options, loggingContext));
}

std::unique_ptr<AcquisitionInterface> AcquireMageeAethalometer33Component::createAcquisitionPassive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(
            new AcquireMageeAethalometer33(config, loggingContext));
}

std::unique_ptr<
        AcquisitionInterface> AcquireMageeAethalometer33Component::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                              const std::string &loggingContext)
{
    std::unique_ptr<AcquireMageeAethalometer33>
            i(new AcquireMageeAethalometer33(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireMageeAethalometer33Component::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireMageeAethalometer33>
            i(new AcquireMageeAethalometer33(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
