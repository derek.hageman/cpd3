/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREMAGEEAETHALOMETER33_H
#define ACQUIREMAGEEAETHALOMETER33_H

#include "core/first.hxx"

#include <array>
#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "acquisition/spancheck.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"
#include "core/csv.hxx"

class FieldIterator;

class AcquireMageeAethalometer33 : public CPD3::Acquisition::FramedInstrument {
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Passive autoprobe initialization, ignoring the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,
        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,

        /* Acquiring data in interactive mode, polling the report */
        RESP_INTERACTIVE_RUN,

        /* Acquiring data in interactive mode, waiting for the instrument halt to complete,
         * so the spot advances*/
                RESP_INTERACTIVE_ADVANCE_HALT,

        /* Acquiring data in interactive mode, waiting for the instrument resume to complete,
         * so the spot advances*/
                RESP_INTERACTIVE_ADVANCE_RESUME,

        /* Starting communications, clearing anything being transmitted out  */
                RESP_INTERACTIVE_START_CLEAR,
        /* Starting communications, reading parameters from the instrument */
                RESP_INTERACTIVE_START_PARAMETERS_READ,
        /* Starting communications, reading parameters and having
         * sent a $AE33:D1 record to attempt to get the response */
                RESP_INTERACTIVE_START_PARAMETERS_DATARETRY,
        /* Starting communications, finishing the record read from the
         * parameters */
                RESP_INTERACTIVE_START_PARAMETERS_RECORD,
        /* Starting communications, stoping the instrument to prepare for 
         * parameter change */
                RESP_INTERACTIVE_START_PARAMETERS_STOP,
        /* Starting communications, setting parameters */
                RESP_INTERACTIVE_START_PARAMETERS_SET,
        /* Starting communications, resuming the instrument */
                RESP_INTERACTIVE_START_PARAMETERS_START,
        /* Starting communications, reading the first line of data */
                RESP_INTERACTIVE_START_FIRSTVALID,

        /* Waiting before attempting a communications restart */
                RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
                RESP_INTERACTIVE_INITIALIZE,
    };
    ResponseState responseState;

    class Configuration;

    class InstrumentParameters {
    public:
        int timeBase;
        QVector<double> efficiency;
        double area;

        bool isUTC;
        bool isDST;

        double leakageFactor;
        double weingartnerConstant;
        double ATNf1;
        double ATNf2;
        double kMax;
        double kMin;

        bool timeIncorrect;

        InstrumentParameters();

        void invalidate();

        bool needToChange(const Configuration &target);

        CPD3::Util::ByteArray changeCommand(const Configuration &target, double currentTime);
    };

    friend class Configuration;

    class Configuration {
        double start;
        double end;
    public:
        enum {
            PureMeasured, RoundedMeasured, PureReported,
        } timeMode;
        bool setInstrumentTime;

        QHash<double, double> absorptionEfficiency;
        std::array<CPD3::Data::Variant::Root, 2> area;
        CPD3::Data::Variant::Root weingartnerConstant;
        CPD3::Data::Variant::Root ATNf1;
        CPD3::Data::Variant::Root ATNf2;
        CPD3::Data::Variant::Root kMin;
        CPD3::Data::Variant::Root kMax;
        CPD3::Data::Variant::Root leakageFactor;
        CPD3::Data::Variant::Root timeBase;

        QVector<double> wavelengths;

        CPD3::Calibration flowCalibrationTotal;
        std::array<CPD3::Calibration, 2> flowCalibration;

        double pollInterval;
        bool strictMode;

        bool disableEBCZeroCheck;

        bool realtimeDiagnostics;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    struct ReportedValues {
        qint64 timeBase;
        QVector<double> If;
        QVector<double> Ip[2];
        double Q1;
        double QC;
        double Treport;
        double Preport;
        double T1;
        double T2;
        double T3;
        qint64 status;
        qint64 controlStatus;
        qint64 detectStatus;
        qint64 ledStatus;
        qint64 valveStatus;
        QVector<double> X;
        QVector<double> Xp[2];
        QVector<double> K;
        qint64 Fn;

        ReportedValues();

        bool operator==(const ReportedValues &other) const;
    };

    ReportedValues priorValues;
    double lastReportTime;

    void insertDiagnosticValues(CPD3::Data::Variant::Write &target,
                                const ReportedValues &values) const;

    class IntensityData {
    public:
        double startTime;
        QVector<double> If;
        QVector<double> Ip[2];
        QVector<double> In[2];

        IntensityData();
    };

    friend QDataStream &operator<<(QDataStream &stream, const IntensityData &data);

    friend QDataStream &operator>>(QDataStream &stream, IntensityData &data);

    /* State that needs to be saved */
    IntensityData normalization;
    double normalizationLastVolume[2];
    qint64 normalizationLastSpotAdvance;
    double normalizationLatestTime;
    double normalizationResumeTime;
    QVector<double> priorFilterAttenuation;
    QVector<double> priorFilterCorrection;
    QVector<QVector<double> > fvrfRatioPoints;
    QVector<QVector<double> > fvrfAttenuationPoints;
    double latestVolumeRatio;
    double fvrfFinal;
    bool fvrfRecalculate;

    QVector<double> priorIr[2];
    QVector<double> latestIn[2];
    QVector<double> currentFilterAttenuation;
    QVector<double> currentFilterCorrection;

    CPD3::Data::Variant::Root instrumentMeta;

    CPD3::Data::SequenceValue instrumentStatus;
    CPD3::Data::SequenceValue spotAdvance;

    CPD3::Data::Variant::Root dataParameters;

    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool haveEmittedParameters;
    bool normalizationUpdating;

    InstrumentParameters parameters;

    void setDefaultInvalid();

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root value);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer invalidateLogMetadata(double time,
                                                              int prior,
                                                              int next) const;

    CPD3::Data::SequenceValue::Transfer invalidateRealtimeMetadata
            (double time, int prior, int next, bool priorDiag, bool nextDiag) const;

    int parseRecord(const CPD3::Util::ByteView &line,
                    double &frameTime,
                    ReportedValues &output,
                    bool authoritativeTime = true);

    int processRecord(const CPD3::Util::ByteView &line, double &frameTime);

    int parseParametersV1(CPD3::Util::ByteView &line,
                          FieldIterator &it,
                          std::deque<CPD3::Util::ByteView> &fields,
                          int idx_SerialNumber);

    int parseParametersV2(CPD3::Util::ByteView &line,
                          FieldIterator &it,
                          std::deque<CPD3::Util::ByteView> &fields,
                          int idx_SerialNumber);

    int parseParameters(CPD3::Util::ByteView &line);

    CPD3::Data::Variant::Root buildNormalizationValue() const;

    void configurationChanged();

    void configurationAdvance(double frameTime);

    void invalidateLogValues(double frameTime);

    double effectiveTimeout() const;

public:
    AcquireMageeAethalometer33(const CPD3::Data::ValueSegment::Transfer &config,
                               const std::string &loggingContext);

    AcquireMageeAethalometer33(const CPD3::ComponentOptions &options,
                               const std::string &loggingContext);

    virtual ~AcquireMageeAethalometer33();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Data::SequenceValue::Transfer getPersistentValues();

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    CPD3::Data::Variant::Root getCommands() override;

    virtual CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    virtual void serializeState(QDataStream &stream);

    virtual void deserializeState(QDataStream &stream);

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    virtual void command(const CPD3::Data::Variant::Read &value);

    virtual void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingInstrumentTimeout(double time);

    virtual void discardDataCompleted(double time);
};

class AcquireMageeAethalometer33Component
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_magee_aethalometer33"
                              FILE
                              "acquire_magee_aethalometer33.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
