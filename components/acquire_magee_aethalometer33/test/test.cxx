/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <cstdint>
#include <cmath>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>
#include <QDateTime>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "algorithms/numericsolve.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Algorithms;

namespace {
struct FactorSolver : public NewtonRaphson<FactorSolver> {
    double ATN1;
    double ATN2;
    double guess;

    FactorSolver(double inATN1, double inATN2, double g = FP::undefined()) : ATN1(inATN1),
                                                                             ATN2(inATN2),
                                                                             guess(g)
    {
        Q_ASSERT(FP::defined(ATN1) && ATN1 > 0.0);
        Q_ASSERT(FP::defined(ATN2) && ATN2 > 0.0);
        if (!FP::defined(guess) || guess < -0.01 || guess > 0.01 || guess == 0.0)
            guess = 0.001;
    }

    double evaluate(double k) const
    {
        Q_ASSERT(FP::defined(k));
        if (k <= 0.0)
            return FP::undefined();
        double r2 = 1.0 - k * ATN2;
        if (r2 <= 0.0)
            return FP::undefined();
        double r1 = 1.0 - k * ATN1;
        if (r1 <= 0.0 || r1 == 1.0)
            return FP::undefined();
        return log(r2) / log(r1);
    }

    double initial(double yTarget) const
    {
        Q_UNUSED(yTarget);
        return guess;
    }

    bool completed(double dX) const
    {
        return fabs(dX) < 1E-10;
    }
};
}
class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    QDateTime currentTime;
    QDateTime lastUpdateTime;

    bool outputUnpolled;
    enum {
        State_Normal, State_Advancing, State_Normalizing,
    } state;
    int stateCount;

    double timeBase;
    int tapeAdvanceCount;
    int offsetCounter;
    int absoluteCounter;

    double Q1;
    double QC;

    double Tcontrol;
    double Tsupply;
    double Tled;
    std::uint_fast16_t status;
    std::uint_fast16_t contStatus;
    std::uint_fast16_t detectStatus;
    std::uint_fast16_t ledStatus;
    std::uint_fast16_t valveStatus;

    double If[7];
    double Ip[2][7];

    double dIf[7];
    double dIp[2][7];
    double In0[2][7];

    double outputT;
    double outputP;
    double area[2];
    double wavelengths[7];
    double efficiency[7];
    double zeta;
    double weingartnerConstant;
    double ATNf1;
    double ATNf2;
    double kMin;
    double kMax;

    bool networkParameters;
    QSet<QByteArray> setParameters;
    enum {
        Normal, Alternate1,
    } parameterMode;

    ModelInstrument()
            : incoming(), outgoing(), currentTime(QDate(2014, 01, 02), QTime(10, 0, 0), Qt::UTC)
    {
        lastUpdateTime = currentTime;
        outputUnpolled = false;

        state = State_Advancing;
        stateCount = 2;

        timeBase = 60.0;
        tapeAdvanceCount = 7;
        offsetCounter = 0;
        absoluteCounter = 0;

        outputT = 0;
        outputP = 1013.25;
        area[0] = 78.5;
        area[1] = 78.5;
        wavelengths[0] = 370.0;
        wavelengths[1] = 470.0;
        wavelengths[2] = 520.0;
        wavelengths[3] = 590.0;
        wavelengths[4] = 660.0;
        wavelengths[5] = 880.0;
        wavelengths[6] = 950.0;
        weingartnerConstant = 1.57;
        zeta = 0.00;
        ATNf1 = 10.0;
        ATNf2 = 30.0;
        kMin = -0.1;
        kMax = 0.5;
        networkParameters = false;
        parameterMode = Normal;

        Q1 = 2.0;
        QC = 3.0;

        Tcontrol = 28.0;
        Tsupply = 29.0;
        Tled = 24.0;

        status = 0;
        contStatus = 0;
        detectStatus = 10;
        ledStatus = 0;
        valveStatus = 0;

        for (int color = 0; color < 7; color++) {
            If[color] = 500000 + color * 1000;
            dIf[color] = (color + 1) * -1;

            efficiency[color] = 6833.0 / wavelengths[color];

            Ip[0][color] = 700000 + (color * 2) * 1000;
            dIp[0][color] = -floor(Ip[0][color] * 0.001);

            Ip[1][color] = 700000 + (color * 2 + 1) * 1000;
            dIp[1][color] = -floor(Ip[1][color] * 0.00052);
        }

        normalize();
    }

    void normalize()
    {
        offsetCounter = 0;
        for (int color = 0; color < 7; color++) {
            for (int spot = 0; spot < 2; spot++) {
                In0[spot][color] = Ip[spot][color] / If[color];
            }
        }
    }

    double spotFlow(int spot, bool ignoreLoss = false) const
    {
        double factor = 1.0 - zeta;
        if (ignoreLoss)
            factor = 1.0;
        if (spot == 1) {
            return (QC - Q1) * factor;
        } else {
            return Q1 * factor;
        }
    }

    double sampleIntensity(int spot, int color, int offset = 0) const
    {
        return Ip[spot][color] - offset * dIp[spot][color];
    }

    double referenceIntensity(int color, int offset = 0) const
    {
        return If[color] - offset * dIf[color];
    }

    double normalizedIntensity(int spot, int color, int offset = 0) const
    {
        return sampleIntensity(spot, color, offset) / referenceIntensity(color, offset);
    }

    double absorption(int spot, int color, int offset = 0) const
    {
        double In0 = normalizedIntensity(spot, color, offset + 1);
        double In1 = normalizedIntensity(spot, color, offset);
        double dQt = (spotFlow(spot) * timeBase) / 60000.0;
        return log(In0 / In1) * (area[spot] / dQt);
    }

    double EBC(int spot, int color, int offset = 0) const
    {
        return absorption(spot, color, offset) / efficiency[color] / weingartnerConstant;
    }

    double totalEBC(int spot, int color, int offset = 0) const
    {
        double In1 = normalizedIntensity(spot, color, offset);
        double dT = timeBase * (offsetCounter - offset);
        if (dT <= 0.0)
            return 0.0;
        double dQt = (spotFlow(spot) * dT) / 60000.0;

        double Ba = log(In0[spot][color] / In1) * (area[spot] / dQt);
        return Ba / efficiency[color] / weingartnerConstant;
    }

    double transmittance(int spot, int color, int offset = 0) const
    {
        return normalizedIntensity(spot, color, offset) / In0[spot][color];
    }

    double attenuation(int spot, int color, int offset = 0) const
    {
        double Ir = transmittance(spot, color, offset);
        return -100.0 * log(Ir);
    }

    double correctionFactor(int color, int offset = 0) const
    {
        double ATN1 = attenuation(0, color, offset);
        double ATN2 = attenuation(1, color, offset);
        double y = spotFlow(1) / spotFlow(0);
        if (!FP::defined(ATN1) || ATN1 <= 0.0)
            return 0.0;
        if (!FP::defined(ATN2) || ATN2 <= 0.0)
            return 0.0;
        double r = FactorSolver(ATN1, ATN2).solve(y);
        return r;
    }

    static double applyCorrectionFactor(double input, double ATN, double k)
    {
        return input / (1.0 - k * ATN);
    }

    double correctedAbsorption(int color, int offset = 0) const
    {
        double k = correctionFactor(color, offset);
        double ATN = attenuation(0, color, offset);
        double Ba = absorption(0, color, offset);
        return applyCorrectionFactor(Ba, ATN, k) / weingartnerConstant;
    }

    double correctedEBC(int color, int offset = 0) const
    {
        double k = correctionFactor(color, offset);
        double ATN = attenuation(0, color, offset);
        double BC = EBC(0, color, offset);
        return applyCorrectionFactor(BC, ATN, k);
    }

    double outputDensity() const
    {
        return (outputP / 1013.25) * (273.15 / (outputT + 273.15));
    }

    double flowToOutput(double Q) const
    {
        return Q / outputDensity();
    }

    double ebcToOutput(double ebc) const
    {
        return ebc * outputDensity();
    }

    double outputFlowReversed(double Q) const
    {
        double output = (double) qRound(flowToOutput(Q) * 1000.0);
        output /= 1000.0;
        return output * outputDensity();
    }

    double outputEBCReversed(double ebc) const
    {
        double output = (double) qRound(ebcToOutput(ebc) * 1000.0);
        output /= 1000.0;
        return output / outputDensity();
    }

    void outputRecord()
    {
        outgoing.append(QByteArray::number(lastUpdateTime.date().year()).rightJustified(4, '0'));
        outgoing.append('/');
        outgoing.append(QByteArray::number(lastUpdateTime.date().month()).rightJustified(2, '0'));
        outgoing.append('/');
        outgoing.append(QByteArray::number(lastUpdateTime.date().day()).rightJustified(2, '0'));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(lastUpdateTime.time().hour()).rightJustified(2, '0'));
        outgoing.append(':');
        outgoing.append(QByteArray::number(lastUpdateTime.time().minute()).rightJustified(2, '0'));
        outgoing.append(':');
        outgoing.append(QByteArray::number(lastUpdateTime.time().second()).rightJustified(2, '0'));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(timeBase, 'f', 0));

        for (int color = 0; color < 7; color++) {
            outgoing.append(' ');
            outgoing.append(QByteArray::number(qRound(If[color])));
            outgoing.append(' ');
            outgoing.append(QByteArray::number(qRound(Ip[0][color])));
            outgoing.append(' ');
            outgoing.append(QByteArray::number(qRound(Ip[1][color])));
        }

        outgoing.append(' ');
        outgoing.append(QByteArray::number(qRound(flowToOutput(spotFlow(0, true)) * 1000.0)));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(qRound(flowToOutput(spotFlow(1, true)) * 1000.0)));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(qRound(flowToOutput(QC) * 1000.0)));

        outgoing.append(' ');
        outgoing.append(QByteArray::number(qRound(outputP * 100.0)));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(outputT, 'f', 2));
        outgoing.append(" 0 ");
        outgoing.append(QByteArray::number(qRound(Tcontrol)));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(qRound(Tsupply)));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(static_cast<int>(status)));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(static_cast<int>(contStatus)));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(static_cast<int>(detectStatus)));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(static_cast<int>(ledStatus)));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(static_cast<int>(valveStatus)).rightJustified(5, '0'));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(qRound(Tled)));

        for (int color = 0; color < 7; color++) {
            if (state != State_Normal) {
                outgoing.append(" 0 0 0");
            } else {
                outgoing.append(' ');
                outgoing.append(
                        QByteArray::number(static_cast<int>(std::round(EBC(0, color) * 1000.0))));
                outgoing.append(' ');
                outgoing.append(
                        QByteArray::number(static_cast<int>(std::round((EBC(1, color) * 1000.0)))));
                outgoing.append(' ');
                outgoing.append(QByteArray::number(
                        static_cast<int>(std::round((correctedEBC(color) * 1000.0)))));
            }
        }

        for (int color = 0; color < 7; color++) {
            outgoing.append(' ');
            outgoing.append(
                    QByteArray::number(std::round(correctionFactor(color) * 1E6) * 1E-6, 'f', 6));
        }

        outgoing.append(' ');
        outgoing.append(QByteArray::number(tapeAdvanceCount));

        outgoing.append(" 0 0 0\r");
    }

    void outputParameters()
    {
#if 0
        outgoing.append(
            "Aethalometer Magee Scientific AE33-S04-00370 AE33 EN 7 0 "
            "2015/12/14 18:45:35  1 1 590 5000 1 18.47 14.54 13.14 11.58 "
            "10.35 7.77 7.19 0.785 120 1 12 2015/01/01 00:00:00 50 3 "
            "-2520.578 -2987.951 -3000 13.86336 14.85225 16 -0.001809938 "
            "-0.002444611 -0.003 187.8744 0.07633822 8.370175E-07 1 "
            "1.156667 -38.98 1.144695 -37.3119 10 0.07 1.57 10 30 0.015 "
            "-0.005 1 2 0.005731522 0.006161526 0.006401527 0.006561526 "
            "0.006811527 0.007101523 0.007271525 3 101325 21.11 5 0 0  0 "
            "0 0 1  1 Coordinated Universal Time 0 0 1 ");
        return;
#endif
#if 0
        outgoing.append(
                "Aethalometer Magee Scientific AE33-S02-00163 AE33 EN 7 0 "
                "2014/10/07 14:04:05  1 1 631 5000 1 18.47 14.54 13.14 11.58 "
                "9.89 7.77 7.19 0.785 1 0 1 120 1 2 4504 3 -2343.41701368233 "
                "-2787.5645909162 -3185.46406359851 13.1896192742415 "
                "13.9582028345209 16.4108232833991 -0.00156157049375372 "
                "-0.00191657770019215 -0.00390099688136571 182.902307469299 "
                "0.0788617395480259 7.90604335322852E-07 1 1.05151515151515 "
                "-18.7909090909091 1.08206686930091 -29.3860182370821 0.07 1.57 "
                "10 30 0.015 -0.005 0.004313069 0.004848231 0.004719596 0.005077236 "
                "0.005216399 0.006834409 0.006861092 5 101325 0 5 0 0  0 True 0 3 "
                "14:20:00 1 2 0 1 Coordinated Universal Time 0 0001/01/01 00:00:00 "
                "0 1 1 ");
        return;
#endif
#if 0
        outgoing.append(
                "18 AE33-S02-00152 7/5/2017 7:24:41 AM 518 1.4.2.0 127.0.0.1:8001 "
                "0 0 1 18.47 14.54 13.14 11.58 10.35 7.77 7.19 1.57 0.785 0.025 1 2 "
                "101325 0 10 30 0.015 -0.005 4000 3 353 -2540.69165039062 -3006.47973632812 "
                "14.1106204986572 14.8359775543213 -0.00155629671644419 -0.00190677109640092 "
                "184.542098999023 0.0740144401788712 7.44768556160125E-07 1 120 4 5/22/2017 "
                "12:27:38 PM 1.0776397515528 -35.2298136645963 1.10903426791277 "
                "-41.7943925233645 3 0 0 1 5/22/2017 12:00:00 AM 1 0 1 0 0 "
                "Coordinated Universal Time -15 1 1 0 "
        );
        return;
#endif
#if 0
        outgoing.append(
                "19 AE33-S10-01257 4/26/2022 8:21:19 AM 540 1.7.0.0 172.16.0.2:8001 "
                "1 0 1 18.47 14.54 13.14 11.58 10.35 7.77 7.19 1.39 0.785 0.01 1 2 "
                "101325 0.00 10 30 0.015 -0.005 5000 3 585 -2071.90478515625 -2414.56079101563 "
                "11.8877620697021 12.4278383255005 0.00019356407574378 -0.000190374892554246 "
                "173.804489135742 0.0836383700370789 -1.10416010556946E-07 1 120 12 1/1/2003 "
                "12:02:47 AM 1.08437502384186 -12.0875015258789 1.10903429985046 "
                "-11.8504695892334 3 1 1 1 1/1/2014 12:00:00 AM 0 0 1 0 0 "
                "Coordinated Universal Time 5 1 1 0 1 192.168.0.2 255.255.255.0 192.168.0.1 "
        );
        return;
#endif

        outgoing.append("Aethalometer Magee Scientific AE33-S02-00225 AE33 EN 7 0 "
                        "2015/07/21 18:15:04  0 0 425 3000 ");
        outgoing.append(QByteArray::number(timeBase, 'f', 0));
        for (int color = 0; color < 7; color++) {
            outgoing.append(' ');
            outgoing.append(QByteArray::number(efficiency[color], 'f', 12));
        }
        outgoing.append(' ');
        outgoing.append(QByteArray::number(area[0] / 100.0, 'f', 3));
        outgoing.append(" 1 0 1 120 3 24 363 3 -2499.62258587646 -2750.53984881004 "
                        "-3000 13.7005564493786 13.3615963741002 16 -0.00160318386189971 "
                        "-0.00112893882125017 -0.003 188.312236847721 0.0764477696735761 "
                        "8.53172896183649E-07 1 1.05792682926829 -13.0396341463415 "
                        "1.08206686930091 -35.8784194528875 ");
        outgoing.append(QByteArray::number(zeta, 'f', 2));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(weingartnerConstant, 'f', 2));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(ATNf1, 'f', 2));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(ATNf2, 'f', 2));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(kMax, 'f', 3));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(kMin, 'f', 3));
        outgoing.append(" 0.001587678 0.0007993676 -0.0001351024 -0.0005201196 "
                        "-0.001508745 -0.002660246 -0.002699461 3 101325 21.11 5 0 0 ");
        if (parameterMode == Alternate1) {
            if (networkParameters)
                outgoing.append("192.168.100.160");
            outgoing.append(" 0 True 0 3 14:20:00 1 2 0 1 Coordinated Universal Time 0 "
                            "0001/01/01 00:00:00 0 1 1 ");
            return;
        }

        if (networkParameters)
            outgoing.append("192.168.100.160 False");
        outgoing.append(" 0 0 1 1  1 Coordinated Universal Time 0 1 2 0 ");

        /* Seems inconsistent, sometimes we see this (which looks like the
         * start of a data line), sometimes we won't get a CR until the next 
         * data request, sometimes we get neither trailer */
        if (1) {
            outgoing.append("2015/07/29 20:36:33 0 1");
        }
        if (0) {
            outgoing.append("\r");
        }
    }

    static QDateTime parseDateTime(const QByteArray &input)
    {
        bool ok = false;
        int year = input.mid(0, 4).toInt(&ok);
        if (!ok || year < 1970 || year > 2100) return QDateTime();
        int month = input.mid(4, 2).toInt(&ok);
        if (!ok || month < 1 || month > 12) return QDateTime();
        int day = input.mid(6, 2).toInt(&ok);
        if (!ok || day < 1 || day > 31) return QDateTime();
        int hour = input.mid(8, 2).toInt(&ok);
        if (!ok || hour < 0 || hour > 23) return QDateTime();
        int minute = input.mid(10, 2).toInt(&ok);
        if (!ok || minute < 0 || minute > 59) return QDateTime();
        int second = input.mid(12, 2).toInt(&ok);
        if (!ok || second < 0 || second > 60) return QDateTime();
        return QDateTime(QDate(year, month, day), QTime(hour, minute, second), Qt::UTC);
    }

    void advance(double seconds)
    {
        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR));

            if (line.startsWith("$AE33:D") && line.length() <= 10) {
                bool ok = false;
                int n = line.mid(7).toInt(&ok);
                if (ok && n == 1) {
                    outputRecord();
                } else {
                    outgoing.append("ERROR INVALID READ\r\n");
                }
            } else if (line.startsWith("$AE33:T") && line.length() == 21) {
                QDateTime dt(parseDateTime(line.mid(7)));
                if (dt.isValid()) {
                    currentTime = dt;
                } else {
                    outgoing.append("ERROR INVALID TIME\r\n");
                }
            } else if (line.startsWith("$AE33:SG")) {
                outputParameters();
            } else if (line.startsWith("$AE33:SS")) {
                QByteArray parameter(line.mid(8));
                QByteArray value;
                int idx = parameter.indexOf(' ');
                if (idx > 0) {
                    value = parameter.mid(idx + 1);
                    parameter = parameter.mid(0, idx);
                }
                parameter = parameter.toLower();
                setParameters |= parameter;
                if (parameter == "timebase") {
                    bool ok = false;
                    timeBase = value.toDouble(&ok);
                    if (!ok) {
                        outgoing.append("INVALID TIMEBASE\r\n");
                    }
                } else if (parameter == "zeta") {
                    bool ok = false;
                    zeta = value.toDouble(&ok);
                    if (!ok) {
                        outgoing.append("INVALID ZETA\r\n");
                    }
                } else if (parameter == "c") {
                    bool ok = false;
                    weingartnerConstant = value.toDouble(&ok);
                    if (!ok) {
                        outgoing.append("INVALID WEINGARTNERCONSTANT\r\n");
                    }
                } else if (parameter == "atnf1") {
                    bool ok = false;
                    ATNf1 = value.toDouble(&ok);
                    if (!ok) {
                        outgoing.append("INVALID ATNF1\r\n");
                    } else {
                        if (ATNf1 > ATNf2)
                            ATNf2 = ATNf1 + 1;
                    }
                } else if (parameter == "atnf2") {
                    bool ok = false;
                    ATNf2 = value.toDouble(&ok);
                    if (!ok) {
                        outgoing.append("INVALID ATNF2\r\n");
                    } else {
                        if (ATNf1 > ATNf2)
                            ATNf2 = ATNf1 + 1;
                    }
                } else {
                    outgoing.append("UNRECOGNIZED PARAMETER\r\n");
                }
            } else if (line.startsWith("$AE33:X0")) {
                /* Stop */
            } else if (line.startsWith("$AE33:X1")) {
                if (state == State_Normal) {
                    stateCount = 0;
                    tapeAdvanceCount++;
                }
            } else if (line.startsWith("$AE33:A")) {
                /*if (state == State_Normal)
                    stateCount = 0;*/
                /* "Tape advance left", does nothing apparently */
            } else {
                outgoing.append("ERROR INVALID COMMAND\r\n");
            }

            if (idxCR >= incoming.size() - 1) {
                incoming.clear();
                break;
            }
            incoming = incoming.mid(idxCR + 1);
        }

        currentTime = currentTime.addMSecs((qint64) (seconds * 1000.0));
        int delta = lastUpdateTime.secsTo(currentTime);
        if (delta < 0) {
            lastUpdateTime =
                    Time::toDateTime(floor(Time::fromDateTime(currentTime) / timeBase) * timeBase);
        } else {
            while (delta >= (int) timeBase) {
                delta -= (int) timeBase;

                lastUpdateTime = lastUpdateTime.addMSecs((qint64) (timeBase * 1000.0));
                if (outputUnpolled) {
                    outputRecord();
                }

                for (int color = 0; color < 7; color++) {
                    If[color] += dIf[color];
                    for (int spot = 0; spot < 2; spot++) {
                        Ip[spot][color] += dIp[spot][color];
                    }
                }

                offsetCounter++;
                absoluteCounter++;

                switch (state) {
                case State_Normal:
                    if (stateCount > 0) {
                        --stateCount;
                        if (stateCount <= 0) {
                            state = State_Advancing;
                            stateCount = 3;
                            status |= 0x01;
                        }
                    }
                    break;

                case State_Advancing:
                    status |= 0x01;
                    --stateCount;
                    if (stateCount <= 0) {
                        state = State_Normalizing;
                        normalize();
                    }
                    break;
                case State_Normalizing:
                    state = State_Normal;
                    status &= ~0x01;
                    break;
                }
            }
        }
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream,
                   const ModelInstrument &model,
                   double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("F2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZSPOT", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Qt1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Qt2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("L1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("L2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZPARAMETERS", Variant::Root(), QString(), time))
            return false;

        for (int color = 0; color < 7; color++) {
            if (!stream.hasMeta("Ir" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Irs" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;

            if (!stream.hasMeta("Ba" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Bas" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Bac" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;

            if (!stream.hasMeta("If" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Ip" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Ips" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;

            if (!stream.hasMeta("X" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Xp" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Xf" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Xfs" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;

            if (!stream.hasMeta("ZFACTOR" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;
        }

        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream,
                           const ModelInstrument &model,
                           double time = FP::undefined())
    {
        if (!stream.hasMeta("ZSTATE", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZLEAKAGE", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZQC", Variant::Root(), QString(), time))
            return false;

        for (int color = 0; color < 7; color++) {
            if (!stream.hasMeta("ZFACTORc" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;
            if (!stream.hasMeta("ZFACTORp" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;

            if (!stream.hasMeta("ZX" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;
            if (!stream.hasMeta("ZXs" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;

            if (!stream.hasMeta("ZATN" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;
            if (!stream.hasMeta("ZATNs" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;
            if (!stream.hasMeta("ZATNc" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;

            if (!stream.hasMeta("In" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Ins" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;

            if (!stream.hasMeta("ZFVRF" + std::to_string(color + 1),
                                Variant::Root(model.wavelengths[color]), "^Wavelength", time))
                return false;
        }
        return true;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     bool checkFlags = true,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("Q1",
                                        Variant::Root(model.outputFlowReversed(model.spotFlow(0))),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("Q2",
                                        Variant::Root(model.outputFlowReversed(model.spotFlow(1))),
                                        time))
            return false;

        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.Tcontrol), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.Tsupply), time))
            return false;
        if (!stream.hasAnyMatchingValue("T3", Variant::Root(model.Tled), time))
            return false;

        if (checkFlags) {
            if (!stream.hasAnyMatchingValue("F1", Variant::Root(Variant::Flags{"STP"}), time))
                return false;
        }
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             double time = FP::undefined())
    {
        Q_UNUSED(stream);
        Q_UNUSED(model);
        Q_UNUSED(time);
        return true;
    }

    bool checkAccumulators(StreamCapture &stream,
                           const ModelInstrument &model,
                           double baseTime,
                           int baseOffset,
                           int totalCheck,
                           double accumulationOffset,
                           bool isRealtime = false)
    {

        QList<double> times;
        QList<double> Qt1;
        QList<double> Qt2;
        QList<double> L1;
        QList<double> L2;
        QVector<QList<double> > Ir1(7);
        QVector<QList<double> > Ir2(7);
        QVector<QList<double> > Ba1(7);
        QVector<QList<double> > Ba2(7);
        QVector<QList<double> > Ba3(7);
        QVector<QList<double> > If(7);
        QVector<QList<double> > Ip1(7);
        QVector<QList<double> > Ip2(7);
        QVector<QList<double> > X(7);
        QVector<QList<double> > Xp1(7);
        QVector<QList<double> > Xp2(7);
        QVector<QList<double> > Xp3(7);
        QVector<QList<double> > ZFACTOR1(7);
        QVector<QList<double> > ZFACTOR2(7);
        QVector<QList<double> > ZATN1(7);
        QVector<QList<double> > ZATN2(7);
        QVector<QList<double> > In1(7);
        QVector<QList<double> > In2(7);
        QVector<QList<double> > ZXc1(7);
        QVector<QList<double> > ZXc2(7);
        for (int i = 0; i < totalCheck; i++) {
            int reverseIndex = totalCheck - (i + 1);

            times.append(baseTime - reverseIndex * model.timeBase);

            double accumulatedTime = times.last() - accumulationOffset;
            Qt1.append(accumulatedTime * model.spotFlow(0) / 60000.0);
            Qt2.append(accumulatedTime * model.spotFlow(1) / 60000.0);
            L1.append(Qt1.last() / (model.area[0] * 1E-6));
            L2.append(Qt2.last() / (model.area[1] * 1E-6));

            int offset = baseOffset + reverseIndex;
            for (int color = 0; color < 7; color++) {
                Ir1[color].append(model.transmittance(0, color, offset));
                Ir2[color].append(model.transmittance(1, color, offset));

                Ba1[color].append(model.absorption(0, color, offset));
                Ba2[color].append(model.absorption(1, color, offset));
                Ba3[color].append(model.correctedAbsorption(color, offset));

                If[color].append(model.referenceIntensity(color, offset));
                Ip1[color].append(model.sampleIntensity(0, color, offset));
                Ip2[color].append(model.sampleIntensity(1, color, offset));

                X[color].append(model.outputEBCReversed(model.correctedEBC(color, offset)));
                Xp1[color].append(model.correctedEBC(color, offset));
                Xp2[color].append(model.outputEBCReversed(model.EBC(0, color, offset)));
                Xp3[color].append(model.outputEBCReversed(model.EBC(1, color, offset)));

                ZFACTOR1[color].append(
                        (double) qRound(model.correctionFactor(color, offset) * 1E6) * 1E-6);


                if (!isRealtime)
                    continue;

                ZFACTOR2[color].append(model.correctionFactor(color, offset));

                ZXc1[color].append(model.EBC(0, color, offset));
                ZXc2[color].append(model.EBC(1, color, offset));

                ZATN1[color].append(model.attenuation(0, color, offset));
                ZATN2[color].append(model.attenuation(1, color, offset));

                In1[color].append(model.normalizedIntensity(0, color, offset));
                In2[color].append(model.normalizedIntensity(1, color, offset));
            }
        }

        if (!stream.verifyValues("Qt1", times, Qt1, !isRealtime, 1E-8, 0.1))
            return false;
        if (!stream.verifyValues("Qt2", times, Qt2, !isRealtime, 1E-8, 0.1))
            return false;
        if (!stream.verifyValues("L1", times, L1, !isRealtime, 1E-8, 0.1))
            return false;
        if (!stream.verifyValues("L2", times, L2, !isRealtime, 1E-8, 0.1))
            return false;

        for (int color = 0; color < 7; color++) {
            if (!stream.verifyValues("If" + std::to_string(color + 1), times, If[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;

            if (!stream.verifyValues("Ip" + std::to_string(color + 1), times, Ip1[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;
            if (!stream.verifyValues("Ips" + std::to_string(color + 1), times, Ip2[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;

            if (!stream.verifyValues("Xf" + std::to_string(color + 1), times, Xp2[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;
            if (!stream.verifyValues("Xfs" + std::to_string(color + 1), times, Xp3[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;

            if (!stream.verifyValues("ZFACTOR" + std::to_string(color + 1), times, ZFACTOR1[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;


            if (!stream.verifyValues("Ir" + std::to_string(color + 1), times, Ir1[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;
            if (!stream.verifyValues("Irs" + std::to_string(color + 1), times, Ir2[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;

            if (!stream.verifyValues("Ba" + std::to_string(color + 1), times, Ba1[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;
            if (!stream.verifyValues("Bas" + std::to_string(color + 1), times, Ba2[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;

            if (!stream.verifyValues("X" + std::to_string(color + 1), times, X[color], !isRealtime,
                                     1E-8, 0.1))
                return false;

            if (!stream.verifyValues("Bac" + std::to_string(color + 1), times, Ba3[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;
            if (!stream.verifyValues("Xp" + std::to_string(color + 1), times, Xp1[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;

            if (!isRealtime)
                continue;

            if (!stream.verifyValues("ZFACTORc" + std::to_string(color + 1), times, ZFACTOR2[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;
            if (!stream.verifyValues("ZFACTORp" + std::to_string(color + 1), times, ZFACTOR2[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;

            if (!stream.verifyValues("ZX" + std::to_string(color + 1), times, ZXc1[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;
            if (!stream.verifyValues("ZXs" + std::to_string(color + 1), times, ZXc2[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;

            if (!stream.verifyValues("ZATN" + std::to_string(color + 1), times, ZATN1[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;
            if (!stream.verifyValues("ZATNs" + std::to_string(color + 1), times, ZATN2[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;

            if (!stream.verifyValues("In" + std::to_string(color + 1), times, In1[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;
            if (!stream.verifyValues("Ins" + std::to_string(color + 1), times, In2[color],
                                     !isRealtime, 1E-8, 0.1))
                return false;
        }

        return true;
    }

    bool checkPersistent(StreamCapture &stream,
                         const ModelInstrument &model,
                         double time = FP::undefined())
    {
        if (!stream.hasValue("ZPARAMETERS", Variant::Root(model.weingartnerConstant),
                             "WeingartnerConstant", time))
            return false;
        if (!stream.hasValue("ZPARAMETERS", Variant::Root(model.area[0]), "Area", time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_magee_aethalometer33"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_magee_aethalometer33"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("area1")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("area2")));
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["TimeBase"] = 60;
        cv["AbsorptionEfficiency"] = true;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.outputUnpolled = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(30.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(30.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        double baseTime = (instrument.absoluteCounter - 5) * 60;
        QVERIFY(checkAccumulators(realtime, instrument, baseTime + 60.0, 5, 10, 3 * 60, true));
        QVERIFY(checkAccumulators(logging, instrument, baseTime, 5, 10, 2 * 60, false));

    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"] = 0.0;
        cv["ATNf1"] = 999.9;
        cv["ATNf2"] = FP::undefined();
        cv["TimeBase"] = 60;
        cv["AbsorptionEfficiency"] = true;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.2);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 1800 * 2; i++) {
            control.advance(0.5);
            QTest::qSleep(10);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();
        persistent.incomingData(persistentValues);

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(!persistentValues.empty());

        double unalignedOffset = logging.values().back().getStart();
        unalignedOffset -= floor(unalignedOffset / 60.0) * 60.0;
        double accumulationOffset = 1 * 60 + unalignedOffset;
        double baseTime = (instrument.absoluteCounter - 6) * 60 + unalignedOffset;
        QVERIFY(checkAccumulators(logging, instrument, baseTime, 5, 10, accumulationOffset, false));
        QVERIFY(checkAccumulators(realtime, instrument, baseTime + 60.0, 5, 10,
                                  accumulationOffset + 60.0, true));

        QVERIFY(checkPersistent(persistent, instrument));

    }

    void interactiveAutoprobeAlternate1()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"] = 0.0;
        cv["ATNf1"] = 999.9;
        cv["ATNf2"] = FP::undefined();
        cv["TimeBase"] = 60;
        cv["AbsorptionEfficiency"] = true;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.parameterMode = ModelInstrument::Alternate1;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.2);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 1800 * 2; i++) {
            control.advance(0.5);
            QTest::qSleep(10);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();
        persistent.incomingData(persistentValues);

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(!persistentValues.empty());

        double unalignedOffset = logging.values().back().getStart();
        unalignedOffset -= floor(unalignedOffset / 60.0) * 60.0;
        double accumulationOffset = 1 * 60 + unalignedOffset;
        double baseTime = (instrument.absoluteCounter - 6) * 60 + unalignedOffset;
        QVERIFY(checkAccumulators(logging, instrument, baseTime, 5, 10, accumulationOffset, false));
        QVERIFY(checkAccumulators(realtime, instrument, baseTime + 60.0, 5, 10,
                                  accumulationOffset + 60.0, true));

        QVERIFY(checkPersistent(persistent, instrument));

    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["ATNf1"] = 999.9;
        cv["ATNf2"] = FP::undefined();
        cv["TimeBase"] = 60;
        cv["AbsorptionEfficiency"] = true;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.outputUnpolled = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 600.0) {
            control.advance(30.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 600.0);
        control.advance(30.0);

        for (int i = 0; i < 50; i++) {
            control.advance(30.0);
            QTest::qSleep(50);
        }

        QVERIFY(logging.hasAnyMatchingValue("F1"));

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        double baseTime = (instrument.absoluteCounter - 5) * 60;
        QVERIFY(checkAccumulators(logging, instrument, baseTime, 5, 10, 2 * 60, false));
        QVERIFY(checkAccumulators(realtime, instrument, baseTime + 60.0, 5, 10, 3 * 60, true));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"] = 0.0;
        cv["ATNf1"] = 999.9;
        cv["ATNf2"] = FP::undefined();
        cv["TimeBase"] = 60;
        cv["AbsorptionEfficiency"] = true;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.networkParameters = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        double checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }

        for (int i = 0; i < 1800 * 2; i++) {
            control.advance(0.5);
            QTest::qSleep(10);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();
        persistent.incomingData(persistentValues);

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(!persistentValues.empty());

        double unalignedOffset = logging.values().back().getStart();
        unalignedOffset -= floor(unalignedOffset / 60.0) * 60.0;
        double accumulationOffset = 1 * 60 + unalignedOffset;
        double baseTime = (instrument.absoluteCounter - 6) * 60 + unalignedOffset;
        QVERIFY(checkAccumulators(logging, instrument, baseTime, 5, 10, accumulationOffset, false));
        QVERIFY(checkAccumulators(realtime, instrument, baseTime + 60.0, 5, 10,
                                  accumulationOffset + 60.0, true));

        QVERIFY(checkPersistent(persistent, instrument));

        QCOMPARE(instrument.setParameters.size(), 1);
        QVERIFY(instrument.setParameters.contains("atnf1"));

    }

    void spotAdvance()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"] = 0.0;
        cv["ATNf1"] = 999.9;
        cv["ATNf2"] = FP::undefined();
        cv["kMin"] = -0.1;
        cv["kMax"] = 0.5;
        cv["TimeBase"] = 60;
        cv["AbsorptionEfficiency"] = true;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.outputUnpolled = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 600.0) {
            control.advance(30.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 600.0);
        control.advance(30.0);

        for (int i = 0; i < 20; i++) {
            control.advance(30.0);
            QTest::qSleep(50);
        }

        instrument.stateCount = 2;
        int resetOffset = instrument.absoluteCounter + 3;

        for (int i = 0; i < 50; i++) {
            control.advance(30.0);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        double baseTime = (instrument.absoluteCounter - 5) * 60;
        QVERIFY(checkAccumulators(logging, instrument, baseTime, 5, 10, (2 + resetOffset) * 60,
                                  false));
        QVERIFY(checkAccumulators(realtime, instrument, baseTime + 60.0, 5, 10,
                                  (3 + resetOffset) * 60, true));

    }

    void stateSaveRestore()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["TimeBase"] = 60;
        cv["AbsorptionEfficiency"] = true;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.outputUnpolled = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 600.0) {
            control.advance(30.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 600.0);
        control.advance(30.0);

        for (int i = 0; i < 50; i++) {
            control.advance(30.0);
            QTest::qSleep(50);
        }

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface = component->createAcquisitionPassive(config);
            control.attach(interface.get());
            interface->start();
            interface->setLoggingEgress(&logging);
            interface->setRealtimeEgress(&realtime);
            interface->setPersistentEgress(&persistent);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }
        }

        for (int i = 0; i < 50; i++) {
            control.advance(30.0);
            QTest::qSleep(50);
        }


        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        double baseTime = (instrument.absoluteCounter - 5) * 60;
        QVERIFY(checkAccumulators(logging, instrument, baseTime, 5, 10, 2 * 60, false));
        QVERIFY(checkAccumulators(realtime, instrument, baseTime + 60.0, 5, 10, 3 * 60, true));

    }

    void interactiveAdvance()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"] = 0.0;
        cv["TimeBase"] = 60;
        cv["AbsorptionEfficiency"] = true;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.networkParameters = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        instrument.currentTime = Time::toDateTime(control.time());
        instrument.state = ModelInstrument::State_Normal;
        instrument.stateCount = -1;

        while (control.time() < 60.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        double checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }

        int originalAdvance = instrument.tapeAdvanceCount;

        Variant::Write cmd = Variant::Write::empty();
        cmd["AdvanceSpot"].setBool(true);
        interface->incomingCommand(cmd);

        for (int i = 0; i < 1800 * 2; i++) {
            control.advance(0.5);
            QTest::qSleep(10);
        }

        QVERIFY(instrument.tapeAdvanceCount > originalAdvance);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();
        persistent.incomingData(persistentValues);

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(!persistentValues.empty());

    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
