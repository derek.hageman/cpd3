/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QByteArray>
#include <QList>

#include "acquire_magee_aethalometer33.hxx"
#include "core/csv.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class FieldIterator {
    Util::ByteView data;
    std::size_t offset;

    static Util::ByteView advance(const Util::ByteView &data, std::size_t &offset)
    {
        static std::array<char, 6> delimiters{' ', '\n', '\r', '\t', '\v', ','};

        Util::ByteView remaining;
        for (;;) {
            if (offset >= data.size())
                return {};
            remaining = data.mid(offset);
            auto check = std::find(delimiters.begin(), delimiters.end(), remaining.front());
            if (check == delimiters.end())
                break;
            ++offset;
        }

        for (auto check : delimiters) {
            auto idx = remaining.indexOf(check);
            if (idx == remaining.npos)
                continue;
            Q_ASSERT(idx != 0);
            remaining = remaining.mid(0, idx);
        }

        offset += remaining.size();
        return remaining;
    }

public:
    explicit FieldIterator(const Util::ByteView &data) : data(data), offset(0)
    { }

    Util::ByteView next()
    { return advance(data, offset); }

    Util::ByteView peek(std::size_t count = 1) const
    {
        Q_ASSERT(count > 0);

        std::size_t tmp = offset;
        for (; count > 1; --count) {
            advance(data, tmp);
        }
        return advance(data, tmp);
    }

    Util::ByteView remaining() const
    {
        if (offset >= data.size())
            return {};
        return data.mid(offset);
    }
};

static bool isDigit(char c)
{
    switch (c) {
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
        return true;
    default:
        return false;
    }
    return false;
}

static bool isAllDigits(const Util::ByteView &test)
{
    for (auto check : test) {
        if (!isDigit(check))
            return false;
    }
    return true;
}

static bool hasLetter(const Util::ByteView &test)
{
    for (auto check: test) {
        if (QChar(check).isLetter())
            return true;
    }
    return false;
}

static bool isDate(const Util::ByteView &test, bool allowYearOne = false)
{
    bool haveYear = false;
    bool haveMonth = false;
    bool haveDay = false;
    const char *check = test.data<const char *>(), *end = check + test.size();

    for (int i = 0; i < 3; i++) {
        int digits = 0;
        int value = 0;
        for (; check != end; ++check, ++digits) {
            if (*check == '/' || *check == '\\' || *check == '-') {
                ++check;
                break;
            }
            if (!isDigit(*check))
                return false;
            value *= 10;
            value += (int) ((*check) - '0');
        }
        switch (digits) {
        case 4:
            if (haveYear)
                return false;
            if (value == 1 && allowYearOne) {
            } else if (value < 1900 || value > 2100) {
                return false;
            }
            haveYear = true;
            break;
        case 1:
        case 2:
            if (value < 1 || value > 31)
                return false;
            if (value <= 12) {
                if (haveMonth)
                    haveDay = true;
                else
                    haveMonth = true;
            } else {
                if (haveDay)
                    return false;
                haveDay = true;
            }
            break;
        default:
            return false;
        }
    }
    if (check != end)
        return false;
    return haveYear && haveMonth && haveDay;
}

static bool isTime(const Util::ByteView &test)
{
    bool haveHour = false;
    bool haveMinute = false;
    bool haveSecond = false;
    const char *check = test.data<const char *>(), *end = check + test.size();

    for (int i = 0; i < 3; i++) {
        int digits = 0;
        int value = 0;
        for (; check != end; ++check, ++digits) {
            if (*check == ':') {
                ++check;
                break;
            }
            if (!isDigit(*check))
                return false;
            value *= 10;
            value += (int) ((*check) - '0');
        }
        switch (digits) {
        case 1:
        case 2:
            if (value < 0 || value > 60)
                return false;
            if (value <= 23) {
                if (haveHour) {
                    if (haveMinute)
                        haveSecond = true;
                    else
                        haveMinute = true;
                } else {
                    haveHour = true;
                }
            } else {
                if (haveMinute)
                    haveSecond = true;
                else
                    haveMinute = true;
            }
            break;
        default:
            return false;
        }
    }
    if (check != end)
        return false;
    return haveHour && haveMinute && haveSecond;
}

static bool isAMorPM(const Util::ByteView &test)
{
    return test.string_equal_insensitive("am") || test.string_equal_insensitive("pm");
}

template<typename AnchorTest>
static int anchorFieldIndex(FieldIterator &it, std::deque<Util::ByteView> &fields, AnchorTest test)
{
    while (auto add = it.next()) {
        fields.emplace_back(add);
        if (test(fields))
            return fields.size() - 1;
    }
    return -1;
}

static bool anchorAdvanceAtLeast(FieldIterator &it, std::deque<Util::ByteView> &fields, int n)
{
    for (; n > 0; --n) {
        auto add = it.next();
        if (!add)
            return false;
        fields.emplace_back(add);
    }
    return true;
}

static bool anchor_isSerialNumber(const std::deque<Util::ByteView> &fields)
{
    Q_ASSERT(!fields.empty());
    if (fields.back().size() < 2)
        return false;
    if (fields.back().string_start("AE33"))
        return true;
    int idx = fields.back().indexOf('-');
    if (idx < 1)
        return false;
    return true;
}

static bool anchor_isInteger(const std::deque<Util::ByteView> &fields)
{
    Q_ASSERT(!fields.empty());
    if (fields.back().size() < 1)
        return false;
    return isAllDigits(fields.back());
}

static bool anchor_isZeroOrOne(const std::deque<Util::ByteView> &fields)
{
    Q_ASSERT(!fields.empty());
    if (fields.back().size() != 1)
        return false;
    switch (fields.back()[0]) {
    case '0':
    case '1':
        break;
    default:
        return false;
    }
    return true;
}

static bool anchor_kMin(const std::deque<Util::ByteView> &fields)
{
    Q_ASSERT(fields.size() >= 6);
    int idx_kMin = fields.size() - 1;
    bool ok = false;
    double v = fields[idx_kMin].toQByteArray().trimmed().toDouble(&ok);
    if (!ok || !FP::defined(v) || v > 0.0 || v <= -1.0) return false;

    int idx_kMax = idx_kMin - 1;
    v = fields[idx_kMax].toQByteArray().trimmed().toDouble(&ok);
    if (!ok || !FP::defined(v) || v < 0.0 || v >= 1.0) return false;

    int idx_ATNf2 = idx_kMax - 1;
    v = fields[idx_ATNf2].toQByteArray().trimmed().toDouble(&ok);
    if (!ok || !FP::defined(v) || v < 0.0 || v > 400.0) return false;

    int idx_ATNf1 = idx_ATNf2 - 1;
    double v2 = fields[idx_ATNf1].toQByteArray().trimmed().toDouble(&ok);
    if (!ok || !FP::defined(v2) || v2 < 0.0 || v2 >= v) return false;

    int idx_C = idx_ATNf1 - 1;
    v = fields[idx_C].toQByteArray().trimmed().toDouble(&ok);
    if (!ok || !FP::defined(v) || v <= 0.0 || v >= 10.0) return false;

    int idx_zeta = idx_C - 1;
    v = fields[idx_zeta].toQByteArray().trimmed().toDouble(&ok);
    if (!ok || !FP::defined(v) || v <= -1.0 || v >= 1.0) return false;

    return true;
}

static bool anchor_Temperature(const std::deque<Util::ByteView> &fields)
{
    Q_ASSERT(fields.size() >= 2);
    if (fields.back().size() < 1)
        return false;
    bool ok = false;
    double d = fields.back().toQByteArray().trimmed().toDouble(&ok);
    if (!ok || d < -100 || d > 100) return false;
    int i = fields[fields.size() - 2].parse_i32(&ok);
    if (!ok || i < 100 || i > 999999) return false;
    return true;
}

static bool isTrueOrFalseWord(const Util::ByteView &test)
{
    return test.string_equal_insensitive("false") || test.string_equal_insensitive("true");
}

static bool isIPv4Address(const Util::ByteView &test, bool acceptPort = false)
{
    int idx = test.indexOf('.');
    if (idx <= 0) return false;
    if (!isAllDigits(test.mid(0, idx))) return false;

    int begin = idx + 1;
    idx = test.indexOf('.', begin);
    if (idx <= 0) return false;
    if (!isAllDigits(test.mid(begin, idx))) return false;

    begin = idx + 1;
    idx = test.indexOf('.', begin);
    if (idx <= 0) return false;
    if (!isAllDigits(test.mid(begin, idx))) return false;

    begin = idx + 1;
    idx = test.indexOf(':', begin);
    if (!acceptPort || idx <= 0) {
        if (!isAllDigits(test.mid(begin))) return false;
        return true;
    }
    if (!isAllDigits(test.mid(begin, idx))) return false;

    begin = idx + 1;
    if (!isAllDigits(test.mid(begin))) return false;

    return true;
}

static bool isTimezoneNameWord(const Util::ByteView &test)
{
    if (test.size() < 3)
        return false;
    if (isDigit(test[0]))
        return false;
    if (!hasLetter(test))
        return false;
    if (isTrueOrFalseWord(test))
        return false;
    if (isIPv4Address(test))
        return false;
    return true;
}

static int locate_Display(const std::deque<Util::ByteView> &fields, int idx_TimeZone_Last)
{
    int idx_Display = idx_TimeZone_Last - 1;
    int total = 1;
    while (idx_Display >= 0 &&
            (isTimezoneNameWord(fields[idx_Display]) || !isAllDigits(fields[idx_Display]))) {
        if (isTrueOrFalseWord(fields[idx_Display]))
            return -1;
        if (isIPv4Address(fields[idx_Display]))
            return -1;
        --idx_Display;
        ++total;
    }
    if (total > 10)
        return -1;
    return idx_Display;
}

static int locate_DateTimeEnd(const std::deque<Util::ByteView> &fields,
                              int idx_Begin,
                              int idx_End = -1)
{
    if (idx_End < 0)
        idx_End = fields.size() - 1;
    if (idx_Begin > idx_End)
        return -1;
    if (!isDate(fields[idx_Begin]))
        return -1;

    ++idx_Begin;
    if (idx_Begin > idx_End)
        return -1;
    if (!isTime(fields[idx_Begin]))
        return -1;

    if (idx_Begin >= idx_End)
        return idx_Begin;
    if (!isAMorPM(fields[idx_Begin + 1]))
        return idx_Begin;
    ++idx_Begin;
    return idx_Begin;
}

static bool anchor_HomeInfo(const std::deque<Util::ByteView> &fields)
{
    Q_ASSERT(fields.size() >= 6);
    int idx_HomeInfo = fields.size() - 1;
    if (!isAllDigits(fields[idx_HomeInfo]))
        return false;

    /* Alternate mode omitting Abb/Aff */
    int idx_TimeZone_Last = idx_HomeInfo - 2;
    if (isTimezoneNameWord(fields[idx_TimeZone_Last])) {
        int idx_DST = idx_HomeInfo - 1;
        switch (fields[idx_DST][0]) {
        case '0':
        case '1':
            break;
        default:
            return false;
        }

        int idx_Display = locate_Display(fields, idx_TimeZone_Last);
        if (idx_Display < 0)
            return false;

        return true;
    }

    int idx_Abb = idx_HomeInfo - 1;
    if (!isAllDigits(fields[idx_Abb]))
        return false;

    int idx_Aff = idx_Abb - 1;
    if (!isAllDigits(fields[idx_Aff]))
        return false;

    int idx_DST = idx_Aff - 1;
    switch (fields[idx_DST][0]) {
    case '0':
    case '1':
        break;
    default:
        return false;
    }

    idx_TimeZone_Last = idx_DST - 1;
    if (!isTimezoneNameWord(fields[idx_TimeZone_Last]))
        return false;

    int idx_Display = locate_Display(fields, idx_TimeZone_Last);
    if (idx_Display < 0)
        return false;

    return true;
}

static bool anchor_BHParamID(const std::deque<Util::ByteView> &fields)
{
    Q_ASSERT(fields.size() >= 7);
    int idx_BHParamID = fields.size() - 1;
    if (!isAllDigits(fields[idx_BHParamID]))
        return false;

    int idx_ExternalID = idx_BHParamID - 1;
    if (!isAllDigits(fields[idx_ExternalID]))
        return false;

    int idx_TapeAdvanceAdjust = idx_ExternalID - 1;
    if (!isAllDigits(fields[idx_TapeAdvanceAdjust]))
        return false;

    int idx_TapeAdvanceTime = idx_TapeAdvanceAdjust - 1;
    if (!isTime(fields[idx_TapeAdvanceTime]))
        return false;

    int idx_TapeAdvanceDate = idx_TapeAdvanceAdjust - 2;
    if (!isDate(fields[idx_TapeAdvanceDate], true))
        return false;

    int idx_DST = idx_TapeAdvanceDate - 1;
    switch (fields[idx_DST][0]) {
    case '0':
    case '1':
        break;
    default:
        return false;
    }

    int idx_TimeZone_Last = idx_DST - 1;
    if (!isTimezoneNameWord(fields[idx_TimeZone_Last]))
        return false;

    return true;
}

static bool anchor_HomeInfoOrBHParamID(const std::deque<Util::ByteView> &fields)
{
    if (anchor_HomeInfo(fields))
        return true;
    if (fields.size() >= 7) {
        if (anchor_BHParamID(fields))
            return true;
    }
    return false;
}

static bool anchor_TimeBase(const std::deque<Util::ByteView> &fields)
{
    Q_ASSERT(fields.size() >= 3);
    int idx_TimeBase = fields.size() - 1;
    if (fields[idx_TimeBase].size() < 1) return false;
    if (!isAllDigits(fields[idx_TimeBase])) return false;

    int idx_InletFilter = idx_TimeBase - 1;
    if (fields[idx_InletFilter].size() < 1) return false;
    if (!isAllDigits(fields[idx_InletFilter])) return false;

    int idx_AutoConnect = idx_InletFilter - 1;
    if (fields[idx_AutoConnect].size() < 1) return false;
    if (!isAllDigits(fields[idx_AutoConnect])) return false;

    return true;
}

static bool anchor_AutoTestDay(const std::deque<Util::ByteView> &fields)
{
    Q_ASSERT(fields.size() >= 5);
    int idx_AutoTestDay = fields.size() - 1;
    if (!isAllDigits(fields[idx_AutoTestDay]))
        return false;

    int idx_AutoTestType = idx_AutoTestDay - 1;
    if (!isAllDigits(fields[idx_AutoTestType]))
        return false;

    int idx_AutoTestEnabled = idx_AutoTestType - 1;
    if (fields[idx_AutoTestEnabled].size() != 1)
        return false;
    switch (fields[idx_AutoTestEnabled][0]) {
    case '0':
    case '1':
        break;
    default:
        return false;
    }

    int idx_WarmUpInterval = idx_AutoTestEnabled - 1;
    if (fields[idx_WarmUpInterval].size() != 1) return false;

    int idx_TapeFormula = idx_WarmUpInterval - 1;
    bool ok = false;
    double v = fields[idx_TapeFormula].toQByteArray().trimmed().toDouble(&ok);
    if (!ok || !FP::defined(v)) return false;

    return true;
}

static bool anchor_DST(const std::deque<Util::ByteView> &fields)
{
    Q_ASSERT(fields.size() >= 5);
    int idx_DST = fields.size() - 1;
    if (fields[idx_DST].size() != 1) return false;
    switch (fields[idx_DST][0]) {
    case '0':
    case '1':
        break;
    default:
        return false;
    }

    int idx_About = idx_DST - 1;
    if (!isAllDigits(fields[idx_About]))
        return false;

    int idx_Display = idx_About - 1;
    if (!isAllDigits(fields[idx_Display]))
        return false;

    int idx_HomeInfo = idx_Display - 1;
    if (!isAllDigits(fields[idx_HomeInfo]))
        return false;

    int idx_MeasureTimeStamp = idx_HomeInfo - 1;
    if (!isAllDigits(fields[idx_MeasureTimeStamp]))
        return false;

    return true;
}

static bool anchor_TimeSync(const std::deque<Util::ByteView> &fields)
{
    Q_ASSERT(fields.size() >= 5);
    int idx_TimeSync = fields.size() - 1;
    if (!isAllDigits(fields[idx_TimeSync]))
        return false;

    int idx_BHparamID = idx_TimeSync - 1;
    if (!isAllDigits(fields[idx_BHparamID]))
        return false;

    int idx_ExternalID = idx_BHparamID - 1;
    if (!isAllDigits(fields[idx_ExternalID]))
        return false;

    int idx_TapeAdvanceAdjust = idx_ExternalID - 1;
    bool ok = false;
    fields[idx_TapeAdvanceAdjust].parse_i32(&ok, 10);
    if (!ok)
        return false;

    int idx_TimeZone_Last = idx_TapeAdvanceAdjust - 1;
    if (!isTimezoneNameWord(fields[idx_TimeZone_Last]))
        return false;

    return true;
}

static bool anchor_NetworkGateway(const std::deque<Util::ByteView> &fields)
{
    Q_ASSERT(fields.size() >= 4);
    int idx_Gateway = fields.size() - 1;
    if (!isIPv4Address(fields[idx_Gateway]))
        return false;

    int idx_Netmask = idx_Gateway - 1;
    if (!isIPv4Address(fields[idx_Netmask]))
        return false;

    int idx_IP = idx_Netmask - 1;
    if (!isIPv4Address(fields[idx_IP]))
        return false;

    int idx_EnableDHCP = idx_IP - 1;
    bool ok = false;
    fields[idx_EnableDHCP].parse_i32(&ok, 10);
    if (!ok)
        return false;

    return true;
}

static void setParameterInteger(Variant::Write target, const Util::ByteView &input)
{
    bool ok = false;
    qint64 i = input.parse_i64(&ok);
    if (ok) {
        target.setInt64(i);
    } else {
        target.setString(input.toString());
    }
}

static void setParameterDouble(Variant::Write target,
                               const Util::ByteView &input,
                               double scalar = 1.0)
{
    bool ok = false;
    double v = input.toQByteArray().trimmed().toDouble(&ok);
    if (ok && FP::defined(v)) {
        target.setDouble(v * scalar);
    } else {
        target.setString(input.toString());
    }
}

int AcquireMageeAethalometer33::parseParametersV1(Util::ByteView &line,
                                                  FieldIterator &it,
                                                  std::deque<Util::ByteView> &fields,
                                                  int idx_SerialNumber)
{
    if (!anchorAdvanceAtLeast(it, fields, 4)) return 2;
    int idx_DateFormat = anchorFieldIndex(it, fields, anchor_isInteger);
    if (idx_DateFormat < 0) return 3;
    if (!anchorAdvanceAtLeast(it, fields, 4)) return 4;
    if (!anchorAdvanceAtLeast(it, fields, 7)) return 5;
    if (!anchorAdvanceAtLeast(it, fields, 5)) return 6;
    int idx_TapeAdvanceCount = -1;
    int idx_TapeAdvanceTime = -1;
    if (isDate(fields.back()) || isTime(fields.back())) {
        idx_TapeAdvanceTime = fields.size();
        idx_TapeAdvanceCount = anchorFieldIndex(it, fields, anchor_isInteger);
        if (idx_TapeAdvanceCount < 0) return 7;
    } else {
        if (!anchorAdvanceAtLeast(it, fields, 2)) return 8;
        idx_TapeAdvanceCount = fields.size();
        if (!anchorAdvanceAtLeast(it, fields, 1)) return 9;
    }
    if (!anchorAdvanceAtLeast(it, fields, 1)) return 10;
    if (!anchorAdvanceAtLeast(it, fields, 12)) return 11;
    int idx_TapeOffsetValid = anchorFieldIndex(it, fields, anchor_isZeroOrOne);
    if (idx_TapeOffsetValid < 0) return 12;
    /* Reserve space for the algorithm parameters */
    if (!anchorAdvanceAtLeast(it, fields, 5)) return 13;
    int idx_kMin = anchorFieldIndex(it, fields, anchor_kMin);
    if (idx_kMin < 0) return 14;
    int idx_leakageFactor = idx_kMin - 5;
    if (!anchorAdvanceAtLeast(it, fields, 7)) return 15;
    if (!anchorAdvanceAtLeast(it, fields, 2)) return 16;
    int idx_T = anchorFieldIndex(it, fields, anchor_Temperature);
    if (idx_T < 0) return 17;
    int idx_flowReportingStandard = idx_T - 2;
    if (!anchorAdvanceAtLeast(it, fields, 6)) return 18;
    int idx_HomeInfoOrBHParamID = anchorFieldIndex(it, fields, anchor_HomeInfoOrBHParamID);
    if (idx_HomeInfoOrBHParamID < 0) return 19;
    int idx_Display;
    int idx_DST;
    int idx_Aff;
    int idx_HomeInfo;
    int idx_BHparamID = -1;
    int idx_TapeAdvanceAdjust = -1;
    if (anchor_HomeInfo(fields)) {
        idx_HomeInfo = idx_HomeInfoOrBHParamID;
        idx_Display = locate_Display(fields, idx_HomeInfo - 4);
        if (idx_Display < 0) return 20;
        idx_DST = idx_HomeInfo - 3;
        if (isTimezoneNameWord(fields[idx_HomeInfo - 2]))
            idx_DST = idx_HomeInfo - 1;
    } else {
        idx_BHparamID = idx_HomeInfoOrBHParamID;
        idx_Display = locate_Display(fields, idx_BHparamID - 6);
        if (idx_Display < 0) return 21;
        idx_DST = idx_BHparamID - 5;
        idx_HomeInfo = idx_Display - 1;
        idx_TapeAdvanceAdjust = idx_BHparamID - 2;
    }
    idx_Aff = idx_HomeInfo - 2;

    for (const auto &add : fields) {
        dataParameters["Raw"].toArray().after_back().setString(add.toString());
    }

    if (idx_SerialNumber > 0) {
        QStringList data;
        for (auto add = fields.begin(), endAdd = add + idx_SerialNumber; add != endAdd; ++add) {
            data.append(QString::fromUtf8(add->toQByteArrayRef()));
        }
        dataParameters["Description"] = data.join(" ");
    }

    int idx = idx_SerialNumber;
    if (idx < static_cast<int>(fields.size())) {
        QString str(fields[idx].toQString());
        dataParameters["SerialNumber"] = str;

        Variant::Root oldValue(instrumentMeta["SerialNumber"]);

        QRegExp re("[^-]+-[^-]+-(\\d+)");
        int sn = -1;
        bool ok = false;
        if (re.exactMatch(str) && (sn = re.cap(1).toInt(&ok, 10)) > 0 && ok) {
            instrumentMeta["SerialNumber"].setInt64(sn);
        } else if ((sn = str.toInt(&ok, 10)) > 0 && ok) {
            instrumentMeta["SerialNumber"].setInt64(sn);
        } else {
            instrumentMeta["SerialNumber"].setString(str.trimmed());
        }

        if (oldValue.read() != instrumentMeta["SerialNumber"]) {
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            emit sourceMetadataUpdated();
        }
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        QString str(fields[idx].toQString());
        dataParameters["Model"] = str;
        if (str.startsWith("AE")) {
            Variant::Root oldValue(instrumentMeta["Model"]);
            instrumentMeta["Model"].setString(str.trimmed());

            if (oldValue.read() != instrumentMeta["Model"]) {
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                emit sourceMetadataUpdated();
            }
        }
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        dataParameters["Language"].setString(fields[idx].toString());
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["NumberOfChannels"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["About"], fields[idx]);
    }
    ++idx;

    for (; idx < idx_DateFormat && idx < static_cast<int>(fields.size()); idx++) {
        dataParameters["SetupTimes"].toArray().after_back().setString(fields[idx].toString());
    }

    idx = idx_DateFormat;
    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["DateFormat"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["MeasureTimeStamp"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["PumpPresetValue"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterDouble(dataParameters["FlowSetpoint"], fields[idx], 0.001);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        int i = fields[idx].parse_i32(&ok);
        if (ok && i > 0) {
            parameters.timeBase = i;
            dataParameters["TimeBase"] = (qint64) i;
        } else {
            return 100;
        }
    } else {
        return 101;
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        QVector<double> oldEfficiency(parameters.efficiency);
        parameters.efficiency.clear();
        for (int i = 0; i < 7 && idx < static_cast<int>(fields.size()); i++, idx++) {
            bool ok = false;
            double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
            if (!ok || !FP::defined(v) || v <= 0.0)
                return 100 * (i + 2) + 1000;
            parameters.efficiency.append(v);
            dataParameters["Sigma"].toArray().after_back().setReal(v);
        }
        if (parameters.efficiency != oldEfficiency) {
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
    } else {
        return 102;
    }

    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v) && v > 0.0) {
            v *= 100.0;
            parameters.area = v;
            dataParameters["Area"] = v;
        } else {
            return 103;
        }
    } else {
        return 104;
    }
    ++idx;

    if (idx_TapeAdvanceTime == -1) {
        if (idx < static_cast<int>(fields.size())) {
            setParameterInteger(dataParameters["SpotsPerAdvance"], fields[idx]);
        }
        ++idx;

        if (idx < static_cast<int>(fields.size())) {
            setParameterInteger(dataParameters["TemperatureRHControl"], fields[idx]);
        }
        ++idx;

        if (idx < static_cast<int>(fields.size())) {
            setParameterInteger(dataParameters["FlowUnitsStandard"], fields[idx]);
        }
        ++idx;
    }

    if (idx < static_cast<int>(fields.size())) {
        setParameterDouble(dataParameters["Advance/Attenuation"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["Advance/Mode"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["Advance/Interval"], fields[idx]);
    }
    ++idx;

    if (idx_TapeAdvanceTime != -1) {
        idx = idx_TapeAdvanceTime;
        for (; idx < idx_TapeAdvanceCount && idx < static_cast<int>(fields.size()); idx++) {
            dataParameters["Advance/Time"].toArray().after_back().setString(fields[idx].toString());
        }
    }

    idx = idx_TapeAdvanceCount;
    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["TotalAdvances"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["Advance/Flush"], fields[idx]);
    }
    ++idx;

    for (; idx < static_cast<int>(fields.size()) && idx < idx_TapeOffsetValid; ++idx) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v)) {
            dataParameters["FlowFormula"].toArray().after_back().setReal(v);
        } else {
            dataParameters["FlowFormula"].toArray().after_back().setString(fields[idx].toString());
        }
    }

    idx = idx_TapeOffsetValid;
    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["TapeOffsetValid"], fields[idx]);
    }
    ++idx;

    for (; idx < static_cast<int>(fields.size()) && idx < idx_leakageFactor; ++idx) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v)) {
            dataParameters["TapeFormula"].toArray().after_back().setReal(v);
        } else {
            dataParameters["TapeFormula"].toArray().after_back().setString(fields[idx].toString());
        }
    }

    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v) && v < 1.0 && v > -1.0) {
            parameters.leakageFactor = v;
            dataParameters["LeakageFactor"] = v;
        } else {
            return 105;
        }
    } else {
        return 106;
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v) && v != 0.0) {
            parameters.weingartnerConstant = v;
            dataParameters["WeingartnerConstant"] = v;
        } else {
            return 107;
        }
    } else {
        return 108;
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v) && v >= 0.0) {
            parameters.ATNf1 = v;
            dataParameters["ATNf1"] = v;
        } else {
            return 108;
        }
    } else {
        return 109;
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v) && v >= 0.0) {
            if (FP::defined(parameters.ATNf1) && v <= parameters.ATNf1)
                return 108;
            parameters.ATNf2 = v;
            dataParameters["ATNf2"] = v;
        } else {
            return 109;
        }
    } else {
        return 110;
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v)) {
            parameters.kMax = v;
            dataParameters["kMax"] = v;
        } else {
            return 110;
        }
    } else {
        return 111;
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v)) {
            parameters.kMin = v;
            dataParameters["kMin"] = v;
        } else {
            return 112;
        }
    } else {
        return 113;
    }
    ++idx;

    if (FP::defined(parameters.kMax) &&
            FP::defined(parameters.kMin) &&
            parameters.kMin >= parameters.kMax)
        return 114;

    for (; idx < static_cast<int>(fields.size()) && idx < idx_flowReportingStandard; ++idx) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v)) {
            dataParameters["k"].toArray().after_back().setReal(v);
        } else {
            dataParameters["k"].toArray().after_back().setString(fields[idx].toString());
        }
    }

    idx = idx_flowReportingStandard;
    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["FlowReportingStandard"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterDouble(dataParameters["ExternalP"], fields[idx], 0.01);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterDouble(dataParameters["ExternalT"], fields[idx]);
    }
    ++idx;

    for (int i = 0; idx < static_cast<int>(fields.size()) && i < 3; ++idx, ++i) {
        setParameterInteger(dataParameters["COM"].array(i), fields[idx]);
    }

    for (; idx < static_cast<int>(fields.size()) && idx < idx_Display; ++idx) {
        dataParameters["NetworkingAndAutotest"].toArray()
                                               .after_back()
                                               .setString(fields[idx].toString());
    }

    idx = idx_Display;
    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["Display"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size()) && idx < idx_DST) {
        QString timezoneName;
        for (; idx < static_cast<int>(fields.size()) && idx < idx_DST; ++idx) {
            QString add(fields[idx].toQString().trimmed());
            if (add.isEmpty())
                continue;
            if (!timezoneName.isEmpty())
                timezoneName.append(' ');
            timezoneName.append(add);
        }
        dataParameters["TimeZone"] = timezoneName;
        if (timezoneName.contains("Coordinated Universal Time", Qt::CaseInsensitive))
            parameters.isUTC = true;
        else
            parameters.isUTC = false;
    } else {
        return 115;
    }

    idx = idx_DST;
    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        int i = fields[idx].parse_i32(&ok);
        if (ok) {
            bool isDST = (i != 0);
            parameters.isDST = isDST;
            dataParameters["DaylightSavingTime"] = isDST;
        } else {
            return 116;
        }
    } else {
        return 117;
    }
    ++idx;

    idx = idx_Aff;
    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["Aff"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["Abb"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["HomeInfo"], fields[idx]);
    }
    ++idx;

    if (idx_BHparamID > 0) {
        idx = idx_DST + 1;
        for (; idx < idx_TapeAdvanceAdjust && idx < static_cast<int>(fields.size()); idx++) {
            dataParameters["Advance/Time"].toArray().after_back().setString(fields[idx].toString());
        }

        if (idx < static_cast<int>(fields.size())) {
            setParameterInteger(dataParameters["Advance/Adjust"], fields[idx]);
        }
        ++idx;

        if (idx < static_cast<int>(fields.size())) {
            setParameterInteger(dataParameters["ExternalID"], fields[idx]);
        }
        ++idx;

        if (idx < static_cast<int>(fields.size())) {
            setParameterInteger(dataParameters["BHparamID"], fields[idx]);
        }
        ++idx;
    }

    line = it.remaining();
    haveEmittedParameters = false;

    return 0;
}

int AcquireMageeAethalometer33::parseParametersV2(Util::ByteView &line,
                                                  FieldIterator &it,
                                                  std::deque<Util::ByteView> &fields,
                                                  int idx_SerialNumber)
{
    if (!anchorAdvanceAtLeast(it, fields, 2)) return 200;
    int idx_TimeBase = anchorFieldIndex(it, fields, anchor_TimeBase);
    if (idx_TimeBase < 0) return 201;
    int idx_AutoConnect = idx_TimeBase - 2;
    int idx_TimeStamp_First = idx_SerialNumber + 1;
    int idx_TimeStamp_Last = locate_DateTimeEnd(fields, idx_TimeStamp_First, idx_TimeBase);
    if (idx_TimeStamp_Last < 0) idx_TimeStamp_First = -1;
    int idx_FirmwareVersion = -1;
    int idx_SoftwareVersion = -1;
    if (idx_TimeStamp_Last > 0 && idx_TimeStamp_Last + 1 < idx_TimeBase) {
        idx_FirmwareVersion = idx_TimeStamp_Last + 1;
        if (idx_FirmwareVersion + 1 < idx_TimeBase) {
            idx_SoftwareVersion = idx_FirmwareVersion + 1;
        }
    }
    if (!anchorAdvanceAtLeast(it, fields, 7)) return 202;
    if (!anchorAdvanceAtLeast(it, fields, 3)) return 203;
    if (!anchorAdvanceAtLeast(it, fields, 2)) return 204;
    if (!anchorAdvanceAtLeast(it, fields, 2)) return 205;
    if (!anchorAdvanceAtLeast(it, fields, 4)) return 206;
    int idx_Flow = anchorFieldIndex(it, fields, anchor_isInteger);
    if (idx_Flow < 0) return 207;
    int idx_FlowRepStd = anchorFieldIndex(it, fields, anchor_isInteger);
    if (idx_FlowRepStd < 0) return 208;
    int idx_PumpPresetValue = anchorFieldIndex(it, fields, anchor_isInteger);
    if (idx_PumpPresetValue < 0) return 209;
    if (!anchorAdvanceAtLeast(it, fields, 1)) return 210;
    int idx_TAType = anchorFieldIndex(it, fields, anchor_isInteger);
    if (idx_TAType < 0) return 211;
    int idx_TAatnMax = anchorFieldIndex(it, fields, anchor_isInteger);
    if (idx_TAatnMax < 0) return 212;
    int idx_TAinterval = anchorFieldIndex(it, fields, anchor_isInteger);
    if (idx_TAinterval < 0) return 213;
    if (!anchorAdvanceAtLeast(it, fields, 3)) return 214;
    int idx_TATime_First = idx_TAinterval + 1;
    int idx_TATime_Last = locate_DateTimeEnd(fields, idx_TATime_First);
    if (idx_TATime_Last < 0) idx_TATime_First = -1;
    if (idx_TATime_First < 0) {
        if (!anchorAdvanceAtLeast(it, fields, 1)) return 215;
    } else {
        if (!anchorAdvanceAtLeast(it, fields, 4)) return 216;
    }
    int idx_AutoTestDay = anchorFieldIndex(it, fields, anchor_AutoTestDay);
    if (idx_AutoTestDay < 0) return 217;
    int idx_WarmUpInterval = idx_AutoTestDay - 3;
    if (!anchorAdvanceAtLeast(it, fields, 4)) return 218;
    int idx_DST = anchorFieldIndex(it, fields, anchor_DST);
    if (idx_DST < 0) return 219;
    int idx_MeasureTimeStamp = idx_DST - 4;
    int idx_AutoTestTime_First = idx_AutoTestDay + 1;
    int idx_AutoTestTime_Last = locate_DateTimeEnd(fields, idx_AutoTestTime_First, idx_DST);
    if (idx_AutoTestTime_Last < 0) idx_AutoTestTime_First = -1;
    if (!anchorAdvanceAtLeast(it, fields, 4)) return 220;
    int idx_TimeSync = anchorFieldIndex(it, fields, anchor_TimeSync);
    if (idx_TimeSync < 0) return 221;
    int idx_TapeAdvanceAdjust = idx_TimeSync - 3;
    if (isIPv4Address(it.peek(2))) {
        if (!anchorAdvanceAtLeast(it, fields, 3)) return 222;
        int idx_NetworkGateway = anchorFieldIndex(it, fields, anchor_NetworkGateway);
        if (idx_NetworkGateway < 0) return 223;
    }

    for (const auto &add: fields) {
        dataParameters["Raw"].toArray().after_back().setString(add.toString());
    }

    if (idx_SerialNumber > 0) {
        QStringList data;
        for (auto add = fields.begin(), endAdd = add + idx_SerialNumber; add != endAdd; ++add) {
            data.append(QString::fromUtf8(add->toQByteArrayRef()));
        }
        dataParameters["InstrumentID"] = data.join(" ");
    }

    if (idx_TimeStamp_First > 0 && idx_TimeStamp_Last > 0) {
        for (int idx = idx_TimeStamp_First;
                idx <= idx_TimeStamp_Last && idx < static_cast<int>(fields.size());
                idx++) {
            dataParameters["TimeStamp"].toArray().after_back().setString(fields[idx].toString());
        }
    }

    int idx = idx_SerialNumber;
    if (idx < static_cast<int>(fields.size())) {
        QString str(fields[idx].toQString());
        dataParameters["SerialNumber"] = str;

        Variant::Root oldValue(instrumentMeta["SerialNumber"]);

        QRegExp re("[^-]+-[^-]+-(\\d+)");
        int sn = -1;
        bool ok = false;
        if (re.exactMatch(str) && (sn = re.cap(1).toInt(&ok, 10)) > 0 && ok) {
            instrumentMeta["SerialNumber"].setInt64(sn);
        } else if ((sn = str.toInt(&ok, 10)) > 0 && ok) {
            instrumentMeta["SerialNumber"].setInt64(sn);
        } else {
            instrumentMeta["SerialNumber"].setString(str.trimmed());
        }

        if (oldValue.read() != instrumentMeta["SerialNumber"]) {
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            emit sourceMetadataUpdated();
        }
    }

    if (idx_FirmwareVersion > 0) {
        idx = idx_FirmwareVersion;

        QString str(fields[idx].toQString());
        dataParameters["HardwareVersion"] = str;

        Variant::Root oldValue(instrumentMeta["HardwareVersion"]);

        bool ok = false;
        int v = fields[idx].parse_i32(&ok, 10);
        if (ok && v > 0) {
            instrumentMeta["HardwareVersion"].setInt64(v);
        } else {
            instrumentMeta["HardwareVersion"].setString(str.trimmed());
        }

        if (oldValue.read() != instrumentMeta["HardwareVersion"]) {
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            emit sourceMetadataUpdated();
        }
    }

    if (idx_SoftwareVersion > 0) {
        idx = idx_SoftwareVersion;

        QString str(fields[idx].toQString());
        dataParameters["FirmwareVersion"] = str;

        Variant::Root oldValue(instrumentMeta["FirmwareVersion"]);

        bool ok = false;
        int v = fields[idx].parse_i32(&ok, 10);
        if (ok && v > 0) {
            instrumentMeta["FirmwareVersion"].setInt64(v);
        } else {
            instrumentMeta["FirmwareVersion"].setString(str.trimmed());
        }

        if (oldValue.read() != instrumentMeta["FirmwareVersion"]) {
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            emit sourceMetadataUpdated();
        }
    }

    ++idx;
    if (idx < idx_AutoConnect) {
        for (; idx < idx_AutoConnect && idx < static_cast<int>(fields.size()); idx++) {
            dataParameters["DataCenterIP"].toArray().after_back().setString(fields[idx].toString());
        }
    }

    idx = idx_AutoConnect;
    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["AutoConnect"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["InletFilter"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        int i = fields[idx].parse_i32(&ok);
        if (ok && i > 0) {
            parameters.timeBase = i;
            dataParameters["TimeBase"] = (qint64) i;
        } else {
            return 300;
        }
    } else {
        return 301;
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        QVector<double> oldEfficiency(parameters.efficiency);
        parameters.efficiency.clear();
        for (int i = 0; i < 7 && idx < static_cast<int>(fields.size()); i++, idx++) {
            bool ok = false;
            double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
            if (!ok || !FP::defined(v) || v <= 0.0)
                return 100 * (i + 2) + 2000;
            parameters.efficiency.append(v);
            dataParameters["Sigma"].toArray().after_back().setReal(v);
        }
        if (parameters.efficiency != oldEfficiency) {
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
    } else {
        return 302;
    }

    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v) && v != 0.0) {
            parameters.weingartnerConstant = v;
            dataParameters["WeingartnerConstant"] = v;
        } else {
            return 303;
        }
    } else {
        return 304;
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v) && v > 0.0) {
            v *= 100.0;
            parameters.area = v;
            dataParameters["Area"] = v;
        } else {
            return 305;
        }
    } else {
        return 306;
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v) && v < 1.0 && v > -1.0) {
            parameters.leakageFactor = v;
            dataParameters["LeakageFactor"] = v;
        } else {
            return 307;
        }
    } else {
        return 308;
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["Aff"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["Abb"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterDouble(dataParameters["ExternalP"], fields[idx], 0.01);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterDouble(dataParameters["ExternalT"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v) && v >= 0.0) {
            parameters.ATNf1 = v;
            dataParameters["ATNf1"] = v;
        } else {
            return 309;
        }
    } else {
        return 310;
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v) && v >= 0.0) {
            if (FP::defined(parameters.ATNf1) && v <= parameters.ATNf1)
                return 311;
            parameters.ATNf2 = v;
            dataParameters["ATNf2"] = v;
        } else {
            return 312;
        }
    } else {
        return 313;
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v)) {
            parameters.kMax = v;
            dataParameters["kMax"] = v;
        } else {
            return 314;
        }
    } else {
        return 315;
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v)) {
            parameters.kMin = v;
            dataParameters["kMin"] = v;
        } else {
            return 316;
        }
    } else {
        return 317;
    }
    ++idx;

    idx = idx_Flow;
    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["FlowOutput"], fields[idx]);
    }
    ++idx;

    idx = idx_FlowRepStd;
    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["FlowReportingStandard"], fields[idx]);
    }
    ++idx;

    idx = idx_PumpPresetValue;
    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["PumpPresetValue"], fields[idx]);
    }
    ++idx;

    for (; idx < static_cast<int>(fields.size()) && idx < idx_TAType; ++idx) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v)) {
            dataParameters["FlowFormula"].toArray().after_back().setReal(v);
        } else {
            dataParameters["FlowFormula"].toArray().after_back().setString(fields[idx].toString());
        }
    }

    idx = idx_TAType;
    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["Advance/Mode"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterDouble(dataParameters["Advance/Attenuation"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["Advance/Interval"], fields[idx]);
    }
    ++idx;

    if (idx_TATime_First > 0 && idx_TATime_Last > 0) {
        for (idx = idx_TATime_First;
                idx <= idx_TATime_Last && idx < static_cast<int>(fields.size());
                idx++) {
            dataParameters["Advance/Time"].toArray().after_back().setString(fields[idx].toString());
        }
    }

    for (; idx < static_cast<int>(fields.size()) && idx < idx_WarmUpInterval; ++idx) {
        bool ok = false;
        double v = fields[idx].toQByteArray().trimmed().toDouble(&ok);
        if (ok && FP::defined(v)) {
            dataParameters["TapeFormula"].toArray().after_back().setReal(v);
        } else {
            dataParameters["TapeFormula"].toArray().after_back().setString(fields[idx].toString());
        }
    }

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["WarmUpInterval"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        int v = fields[idx].parse_i32(&ok);
        if (ok && (v == 0 || v == 1)) {
            dataParameters["AutoTest/Enable"].setBool(v != 0);
        } else {
            setParameterInteger(dataParameters["AutoTest/Enable"], fields[idx]);
        }
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["AutoTest/Mode"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["AutoTest/Day"], fields[idx]);
    }
    ++idx;

    if (idx_AutoTestTime_First > 0 && idx_AutoTestTime_Last > 0) {
        for (idx = idx_AutoTestTime_First;
                idx <= idx_AutoTestTime_Last && idx < static_cast<int>(fields.size());
                idx++) {
            dataParameters["AutoTest/Time"].toArray()
                                           .after_back()
                                           .setString(fields[idx].toString());
        }
    }

    idx = idx_MeasureTimeStamp;
    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["MeasureTimeStamp"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["HomeInfo"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["Display"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["About"], fields[idx]);
    }
    ++idx;

    idx = idx_DST;
    if (idx < static_cast<int>(fields.size())) {
        bool ok = false;
        int i = fields[idx].parse_i32(&ok);
        if (ok) {
            bool isDST = (i != 0);
            parameters.isDST = isDST;
            dataParameters["DaylightSavingTime"] = isDST;
        } else {
            return 318;
        }
    } else {
        return 319;
    }
    ++idx;

    if (idx < static_cast<int>(fields.size()) && idx < idx_TapeAdvanceAdjust) {
        QString timezoneName;
        for (; idx < static_cast<int>(fields.size()) && idx < idx_TapeAdvanceAdjust; ++idx) {
            QString add(fields[idx].toQString().trimmed());
            if (add.isEmpty())
                continue;
            if (!timezoneName.isEmpty())
                timezoneName.append(' ');
            timezoneName.append(add);
        }
        dataParameters["TimeZone"] = timezoneName;
        if (timezoneName.contains("Coordinated Universal Time", Qt::CaseInsensitive))
            parameters.isUTC = true;
        else
            parameters.isUTC = false;
    } else {
        return 320;
    }

    idx = idx_TapeAdvanceAdjust;
    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["Advance/Adjust"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["ExternalID"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["BHparamID"], fields[idx]);
    }
    ++idx;

    if (idx < static_cast<int>(fields.size())) {
        setParameterInteger(dataParameters["TimeSync"], fields[idx]);
    }
    ++idx;

    line = it.remaining();
    haveEmittedParameters = false;

    return 0;
}

int AcquireMageeAethalometer33::parseParameters(Util::ByteView &line)
{
    FieldIterator it(line);

    std::deque<Util::ByteView> fields;
    int idx_SerialNumber = anchorFieldIndex(it, fields, anchor_isSerialNumber);
    if (idx_SerialNumber < 0) return 1;

    if (idx_SerialNumber == 1 && isAllDigits(fields.front())) {
        int code = parseParametersV2(line, it, fields, idx_SerialNumber);
        if (code <= 0) {
            dataParameters["Frame"].setString(line.toString());
            return code;
        }
        return code;
    }

    int code = parseParametersV1(line, it, fields, idx_SerialNumber);
    if (code <= 0) {
        dataParameters["Frame"].setString(line.toString());
        return code;
    }

    return code;
}