/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;
    QDateTime logTime;
    int unpolledMode;
    double unpolledInterval;

    quint16 flags;
    double countRate;
    double P;
    double Q;
    double Tsaturator;
    double Tgrowth;
    double Toptics;
    double Alaser;
    double PCTnozzle;

    ModelInstrument()
            : incoming(),
              outgoing(),
              unpolledRemaining(0),
              logTime(QDate(2013, 10, 01), QTime(0, 0, 0)),
              unpolledMode(4),
              unpolledInterval(1.0)
    {

        flags = 0;
        countRate = 6000;
        P = 984.0;
        Q = 0.12;
        Tsaturator = 40.0;
        Tgrowth = 25.0;
        Toptics = 28.0;
        Alaser = 100.0;
        PCTnozzle = 100.0;
    }

    double concentration() const
    {
        return countRate / (Q * (1000.0 / 60.0));
    }

    void outputD()
    {
        outgoing.append("D,0,");
        outgoing.append(QByteArray::number(flags, 16));
        outgoing.append(',');
        outgoing.append(QByteArray::number(concentration(), 'e', 2));
        outgoing.append(",1,1.000,");
        outgoing.append(QByteArray::number(countRate, 'f', 0));
        outgoing.append(",0,0\r");
    }

    void outputS()
    {
        outgoing.append("S,");
        outgoing.append(QByteArray::number(Q * 1000.0, 'f', 0));
        outgoing.append(',');
        outgoing.append(QByteArray::number(P, 'f', 0));
        outgoing.append(',');
        outgoing.append(QByteArray::number(Tsaturator, 'f', 1));
        outgoing.append(',');
        outgoing.append(QByteArray::number(Tgrowth, 'f', 1));
        outgoing.append(',');
        outgoing.append(QByteArray::number(Toptics, 'f', 1));
        outgoing.append('\r');
    }

    void outputL(bool epochTime = false)
    {
        if (epochTime) {
            outgoing.append(QByteArray::number(logTime.toTime_t()));
        } else {
            outgoing.append(logTime.date().year());
            outgoing.append('/');
            outgoing.append(logTime.date().month());
            outgoing.append('/');
            outgoing.append(logTime.date().day());
            outgoing.append(',');
            outgoing.append(logTime.time().hour());
            outgoing.append(':');
            outgoing.append(logTime.time().minute());
            outgoing.append(':');
            outgoing.append(logTime.time().second());
        }

        outgoing.append(',');
        outgoing.append(QByteArray::number(flags, 16));
        outgoing.append(',');
        outgoing.append(QByteArray::number(concentration(), 'e', 2));
        outgoing.append(",1,1.000,");
        outgoing.append(QByteArray::number(countRate, 'f', 0));
        outgoing.append(',');
        outgoing.append(QByteArray::number(P, 'f', 0));
        outgoing.append(',');
        outgoing.append(QByteArray::number(Q * 1000.0, 'f', 0));
        outgoing.append('\r');
    }

    void outputA()
    {
        outgoing.append("A,0,0,0,0,0,0\r");
    }

    void outputC()
    {
        outgoing.append("C,0,0,0,0,0,0\r");
    }

    void advance(double seconds)
    {

        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR));

            if (line == "LE" ||
                    line.startsWith("SAS,") ||
                    line.startsWith("SDC,") ||
                    line.startsWith("SDS,") ||
                    line.startsWith("SDT,") ||
                    line.startsWith("SDZ,") ||
                    line.startsWith("SFC,") ||
                    line.startsWith("SFM,") ||
                    line.startsWith("SFS,") ||
                    line.startsWith("SFZ,") ||
                    line.startsWith("SG,") ||
                    line.startsWith("SGS,") ||
                    line.startsWith("SL,") ||
                    line.startsWith("SLS,") ||
                    line.startsWith("SO,") ||
                    line.startsWith("SOS,") ||
                    line.startsWith("SP,") ||
                    line.startsWith("SS,") ||
                    line.startsWith("SSS,") ||
                    line.startsWith("SZ,")) {
                outgoing.append("OK\r");
            } else if (line == "LC") {
                outgoing.append(QByteArray::number(logTime.date().year()));
                outgoing.append('/');
                outgoing.append(QByteArray::number(logTime.date().month()));
                outgoing.append('/');
                outgoing.append(QByteArray::number(logTime.date().day()));
                outgoing.append(',');
                outgoing.append(QByteArray::number(logTime.time().hour()));
                outgoing.append(':');
                outgoing.append(QByteArray::number(logTime.time().minute()));
                outgoing.append(':');
                outgoing.append(QByteArray::number(logTime.time().second()));
                outgoing.append('\r');
            } else if (line.startsWith("LC,")) {
                QList<QByteArray> fields(line.split(','));
                if (fields.size() != 7) {
                    outgoing.append("ERROR\r");
                } else {
                    logTime.setDate(QDate(fields.at(1).toInt(), fields.at(2).toInt(),
                                          fields.at(3).toInt()));
                    logTime.setTime(QTime(fields.at(4).toInt(), fields.at(5).toInt(),
                                          fields.at(6).toInt()));
                    if (logTime.isValid()) {
                        outgoing.append("OK\r");
                    } else {
                        outgoing.append("ERROR\r");
                    }
                }
            } else if (line == "LI") {
                outgoing.append("10\r");
            } else if (line == "LM") {
                outgoing.append("0\r");
            } else if (line.startsWith("LM,")) {
                QList<QByteArray> fields(line.split(','));
                if (fields.size() != 2 || fields.at(1) != "0") {
                    outgoing.append("ERROR\r");
                } else {
                    outgoing.append("OK\r");
                }
            } else if (line == "RD") {
                outgoing.append(QByteArray::number(concentration(), 'f', 0));
                outgoing.append('\r');
            } else if (line == "RN") {
                outgoing.append(QByteArray::number(PCTnozzle, 'f', 0));
                outgoing.append('\r');
            } else if (line == "RL") {
                outgoing.append(QByteArray::number(Alaser, 'f', 0));
                outgoing.append('\r');
            } else if (line == "RRA") {
                outputA();
            } else if (line == "RRC") {
                outputC();
            } else if (line == "RRD") {
                outputD();
            } else if (line == "RRS") {
                outputS();
            } else if (line == "RTO") {
                outgoing.append("456:40\r");
            } else if (line == "RV") {
                outgoing.append("Model 3781 Ver 0.20 S/N 101\r");
            } else if (line.startsWith("SM,")) {
                QList<QByteArray> fields(line.split(','));
                if (fields.size() != 3) {
                    outgoing.append("ERROR\r");
                } else {
                    unpolledMode = fields.at(1).toInt();
                    if (unpolledMode >= 0 && unpolledMode <= 10) {
                        unpolledInterval = fields.at(2).trimmed().toDouble() / 10.0;
                        unpolledRemaining = unpolledInterval;
                        outgoing.append("OK\r");
                        if (unpolledMode == 0)
                            unpolledRemaining = FP::undefined();
                    } else {
                        outgoing.append("ERROR\r");
                    }
                }
            } else {
                outgoing.append("ERROR\r");
            }

            if (idxCR >= incoming.size() - 1) {
                incoming.clear();
                break;
            }
            incoming = incoming.mid(idxCR + 1);
        }

        logTime = logTime.addMSecs((qint64) ceil(seconds * 1000.0));
        if (FP::defined(unpolledRemaining)) {
            unpolledRemaining -= seconds;
            while (unpolledRemaining <= 0.0) {
                unpolledRemaining += unpolledInterval;
                switch (unpolledMode) {
                case 0:
                    break;
                case 1:
                    outputD();
                    unpolledMode = 0;
                    break;
                case 2:
                    outputD();
                    break;
                case 3:
                    outputD();
                    outputS();
                    unpolledMode = 0;
                    break;
                case 4:
                    outputD();
                    outputS();
                    break;
                case 7:
                    outputD();
                    outputS();
                    outputA();
                    outputC();
                    unpolledMode = 0;
                    break;
                case 8:
                    outputD();
                    outputS();
                    outputA();
                    outputC();
                    break;
                case -1:
                case 10:
                    outputL();
                    break;
                default:
                    break;
                }
            }
        }

    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("N", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("P", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("A", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCT", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("C", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZND", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZQ", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("N"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("T3"))
            return false;
        if (!stream.checkContiguous("Q"))
            return false;
        if (!stream.checkContiguous("P"))
            return false;
        if (!stream.checkContiguous("A"))
            return false;
        if (!stream.checkContiguous("PCT"))
            return false;
        if (!stream.checkContiguous("F1"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream, const ModelInstrument &model,
                     double time = FP::undefined(),
                     bool enablePolled = true)
    {
        if (!stream.hasAnyMatchingValue("N", Variant::Root(model.concentration()), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.Tsaturator), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.Tgrowth), time))
            return false;
        if (!stream.hasAnyMatchingValue("T3", Variant::Root(model.Toptics), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q", Variant::Root(model.Q), time))
            return false;
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.P), time))
            return false;
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (enablePolled) {
            if (!stream.hasAnyMatchingValue("A", Variant::Root(model.Alaser), time))
                return false;
            if (!stream.hasAnyMatchingValue("PCT", Variant::Root(model.PCTnozzle), time))
                return false;
        }
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("C", Variant::Root(model.countRate), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZND", Variant::Root(model.concentration()), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZQ", Variant::Root(model.Q), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_tsi_cpc3781"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_tsi_cpc3781"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(0.2);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, FP::undefined(), false));
        QVERIFY(checkValues(realtime, instrument, FP::undefined(), false));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(0.1);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("N", Variant::Root(instrument.concentration())))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        for (int i = 0; i < 20; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, FP::undefined(), false));
        QVERIFY(checkValues(realtime, instrument, FP::undefined(), false));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("N", Variant::Root(instrument.concentration())))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        double checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        control.advance(0.125);
        checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        for (int i = 0; i < 20; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
