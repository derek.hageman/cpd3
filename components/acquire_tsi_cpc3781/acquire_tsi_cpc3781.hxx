/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIRETSICPC3781_H
#define ACQUIRETSICPC3781_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "acquisition/spancheck.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class AcquireTSICPC3781 : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;

    int autoprobePassiveValidRecords;
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Passive autoprobe initialization, ignorging the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,

        /* Acquiring interactive data, waiting for the D record read */
                RESP_INTERACTIVE_RUN_READ_D,
        /* Acquiring interactive data, waiting for the S record read */
                RESP_INTERACTIVE_RUN_READ_S,
        /* Acquiring interactive data, waiting for the laser current */
                RESP_INTERACTIVE_RUN_READ_LASER,
        /* Acquiring interactive data, waiting for the nozzle pressure */
                RESP_INTERACTIVE_RUN_READ_NOZZLE,
        /* Interactive mode, waiting before reading again */
                RESP_INTERACTIVE_RUN_WAIT,

        /* Waiting for the completion of the "SM,0,0" command during start
         * communications as well as flushing out all remaining data reports */
                RESP_INTERACTIVE_START_STOPREPORTS,

        /* Waiting for completion of the "LM,0" command to disable internal
         * data storage */
                RESP_INTERACTIVE_START_DISABLELOGGING,
        /* Waiting for the version response to the "RV" command */
                RESP_INTERACTIVE_START_READVERSION,
        /* Setting the logging RTC with the "LC" command */
                RESP_INTERACTIVE_START_SETTIME,
        /* Waiting for the OK response to the "SM,0,X" command to set the
         * sample time */
                RESP_INTERACTIVE_START_SETINTERVAL,
        /* Waiting for a D record */
                RESP_INTERACTIVE_START_READ_D,
        /* Waiting for a S record */
                RESP_INTERACTIVE_START_READ_S,

        /* Waiting before attempting a communications restart */
                RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
                RESP_INTERACTIVE_INITIALIZE,
    };
    ResponseState responseState;

    enum CommandType {
        /* Unknown command, so all responses until another command is
         * received are discarded. */
                COMMAND_IGNORED,

        /* RD, Read display concentration */
                COMMAND_RD, /* RL, Read laser current */
                COMMAND_RL, /* RN, Read nozzle pressure drop */
                COMMAND_RN, /* RRD, Read D record */
                COMMAND_RRD, /* RRS, Read S record */
                COMMAND_RRS, /* RV, Read version */
                COMMAND_RV,

        /* A generic command responding with a single "OK" */
                COMMAND_OK, /* A command we expect an error from (disambiguation) */
                COMMAND_ERROR,

        /* A specifically invalid command */
                COMMAND_INVALID,
    };

    class Command {
        CommandType type;
    public:
        Command();

        Command(CommandType t);

        Command(const Command &other);

        Command &operator=(const Command &other);

        inline CommandType getType() const
        { return type; }
    };

    QList<Command> commandQueue;

    enum LogStreamID {
        LogStream_Metadata = 0, LogStream_State,

        LogStream_D, LogStream_S, LogStream_RL, LogStream_RN,

        LogStream_TOTAL, LogStream_RecordBaseStart = LogStream_D
    };
    CPD3::Data::StaticMultiplexer loggingMux;
    quint32 streamAge[LogStream_TOTAL];
    double streamTime[LogStream_TOTAL];

    friend class Configuration;

    class Configuration {
        double start;
        double end;
    public:
        double flow;
        double pollInterval;
        double sampleTime;
        bool strictMode;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    CPD3::Data::Variant::Root instrumentMeta;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;

    CPD3::Data::Variant::Flags reportedFlags;
    double lastReportedFlow;

    void setDefaultInvalid();

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void logValue(double startTime,
                  double endTime,
                  int streamID,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    void invalidateLogValues(double frameTime);

    void emitMetadata(double frameTime);

    void streamAdvance(int streamID, double time);

    void configAdvance(double frameTime);

    void configurationChanged();

    void describeState(CPD3::Data::Variant::Write &info) const;

    int processDRecord(const CPD3::Util::ByteView &line, double &frameTime);

    int processSRecord(const CPD3::Util::ByteView &line, double &frameTime);

    int processLRecord(const CPD3::Util::ByteView &line, double &frameTime);

    int processResponse(const CPD3::Util::ByteView &line, double &frameTime);

public:
    AcquireTSICPC3781(const CPD3::Data::ValueSegment::Transfer &config,
                      const std::string &loggingContext);

    AcquireTSICPC3781(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireTSICPC3781();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    virtual void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingInstrumentTimeout(double time);

    virtual void discardDataCompleted(double time);
};

class AcquireTSICPC3781Component
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_tsi_cpc3781"
                              FILE
                              "acquire_tsi_cpc3781.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
