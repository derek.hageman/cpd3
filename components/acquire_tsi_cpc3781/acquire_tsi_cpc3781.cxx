/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_tsi_cpc3781.hxx"


using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

static const char *instrumentFlagTranslation[16] = {"ConcentrationOutOfRange",  /* 0x0001 */
                                                    "SampleFlowError",          /* 0x0002 */
                                                    "NozzleFlowError",          /* 0x0004 */
                                                    "PressureError",            /* 0x0008 */
                                                    "TemperatureError",         /* 0x0010 */
                                                    "WarmUpInProgress",         /* 0x0020 */
                                                    "TiltError",                /* 0x0040 */
                                                    "LaserCurrentError",        /* 0x0080 */
                                                    "LiquidValveOpen",          /* 0x0100 */
                                                    "LiquidLow",                /* 0x0200 */
                                                    NULL,                       /* 0x0400 */
                                                    NULL,                       /* 0x0800 */
                                                    NULL,                       /* 0x1000 */
                                                    NULL,                       /* 0x2000 */
                                                    NULL,                       /* 0x4000 */
                                                    NULL,                       /* 0x8000 */
};

AcquireTSICPC3781::Configuration::Configuration() : start(FP::undefined()),
                                                    end(FP::undefined()),
                                                    flow(FP::undefined()),
                                                    pollInterval(0.5),
                                                    sampleTime(1.0),
                                                    strictMode(true)
{
}

AcquireTSICPC3781::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          flow(other.flow),
          pollInterval(other.pollInterval),
          sampleTime(other.sampleTime),
          strictMode(other.strictMode)
{
}

AcquireTSICPC3781::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          flow(FP::undefined()),
          pollInterval(0.5),
          sampleTime(1.0),
          strictMode(true)
{
    setFromSegment(other);
}

AcquireTSICPC3781::Configuration::Configuration(const Configuration &under,
                                                const ValueSegment &over,
                                                double s,
                                                double e) : start(s),
                                                            end(e),
                                                            flow(under.flow),
                                                            pollInterval(under.pollInterval),
                                                            sampleTime(under.sampleTime),
                                                            strictMode(under.strictMode)
{
    setFromSegment(over);
}

void AcquireTSICPC3781::Configuration::setFromSegment(const ValueSegment &config)
{
    {
        double v = config["PollInterval"].toDouble();
        if (FP::defined(v) && v > 0.0)
            pollInterval = v;
    }

    {
        double v = config["SampleTime"].toDouble();
        if (FP::defined(v) && v > 0.0)
            sampleTime = v;
    }

    {
        double v = config["Flow"].toDouble();
        if (FP::defined(v) && v > 0.0)
            flow = v;
    }

    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();
}


void AcquireTSICPC3781::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("TSI");
    instrumentMeta["Model"].setString("3781");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    lastReportedFlow = FP::undefined();
    reportedFlags.clear();

    memset(streamAge, 0, sizeof(streamAge));
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
    }
}


AcquireTSICPC3781::AcquireTSICPC3781(const ValueSegment::Transfer &configData,
                                     const std::string &loggingContext) : FramedInstrument(
        "tsi3781", loggingContext),
                                                                          lastRecordTime(
                                                                                  FP::undefined()),
                                                                          autoprobePassiveValidRecords(
                                                                                  0),
                                                                          autoprobeStatus(
                                                                                  AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                          responseState(
                                                                                  RESP_PASSIVE_WAIT),
                                                                          commandQueue(),
                                                                          loggingMux(
                                                                                  LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
    configurationChanged();
}

void AcquireTSICPC3781::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE;
    autoprobePassiveValidRecords = 0;
}

void AcquireTSICPC3781::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireTSICPC3781Component::getBaseOptions()
{
    ComponentOptions options;

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(tr("q", "name"), tr("Flow rate"),
                                            tr("This is the sample flow rate of the CPC at lpm measured at ambient "
                                               "conditions."), tr("0.12 lpm", "default flow rate"),
                                            1);
    d->setMinimum(0.0, false);
    options.add("q", d);

    return options;
}

AcquireTSICPC3781::AcquireTSICPC3781(const ComponentOptions &options,
                                     const std::string &loggingContext) : FramedInstrument(
        "tsi3781", loggingContext),
                                                                          lastRecordTime(
                                                                                  FP::undefined()),
                                                                          autoprobePassiveValidRecords(
                                                                                  0),
                                                                          autoprobeStatus(
                                                                                  AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                          responseState(
                                                                                  RESP_PASSIVE_WAIT),
                                                                          commandQueue(),
                                                                          loggingMux(
                                                                                  LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());

    if (options.isSet("q")) {
        double value = qobject_cast<ComponentOptionSingleDouble *>(options.get("q"))->get();
        if (FP::defined(value) && value > 0.0)
            config.last().flow = value;
    }

    config.last().strictMode = false;

    configurationChanged();
}

AcquireTSICPC3781::~AcquireTSICPC3781()
{
}

AcquireTSICPC3781::Command::Command() : type(COMMAND_INVALID)
{ }

AcquireTSICPC3781::Command::Command(CommandType t) : type(t)
{ }

AcquireTSICPC3781::Command::Command(const Command &other) : type(other.type)
{ }

AcquireTSICPC3781::Command &AcquireTSICPC3781::Command::operator=(const Command &other)
{
    if (&other == this)
        return *this;
    type = other.type;
    return *this;
}

SequenceValue::Transfer AcquireTSICPC3781::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_tsi_cpc3781");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);

    result.emplace_back(SequenceName({}, "raw_meta", "N"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Condensation nuclei concentration");
    if (FP::defined(config.first().flow)) {
        result.back().write().metadataReal("SampleFlow").setDouble(config.first().flow);
    }
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Saturator temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Saturator"));

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Growth tube temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Growth"));

    result.emplace_back(SequenceName({}, "raw_meta", "T3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Optics temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Optics"));

    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Absolute pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Pressure"));

    result.emplace_back(SequenceName({}, "raw_meta", "Q"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));

    result.emplace_back(SequenceName({}, "raw_meta", "A"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000");
    result.back().write().metadataReal("Units").setString("mA");
    result.back().write().metadataReal("Description").setString("Laser current");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Laser"));

    result.emplace_back(SequenceName({}, "raw_meta", "PCT"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Nozzle pressure drop");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Nozzle pressure"));


    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("ConcentrationOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc3781");
    result.back()
          .write()
          .metadataSingleFlag("ConcentrationOutOfRange")
          .hash("Bits")
          .setInt64(0x00010000);
    result.back()
          .write()
          .metadataSingleFlag("ConcentrationOutOfRange")
          .hash("Description")
          .setString("Concentration out of range");

    result.back()
          .write()
          .metadataSingleFlag("SampleFlowError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc3781");
    result.back().write().metadataSingleFlag("SampleFlowError").hash("Bits").setInt64(0x00020000);
    result.back()
          .write()
          .metadataSingleFlag("SampleFlowError")
          .hash("Description")
          .setString("Sample flow out of range");

    result.back()
          .write()
          .metadataSingleFlag("NozzleFlowError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc3781");
    result.back().write().metadataSingleFlag("NozzleFlowError").hash("Bits").setInt64(0x00040000);
    result.back()
          .write()
          .metadataSingleFlag("NozzleFlowError")
          .hash("Description")
          .setString("Nozzle flow error present");

    result.back()
          .write()
          .metadataSingleFlag("PressureError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc3781");
    result.back().write().metadataSingleFlag("PressureError").hash("Bits").setInt64(0x00080000);
    result.back()
          .write()
          .metadataSingleFlag("PressureError")
          .hash("Description")
          .setString("Absolute pressure out of range");

    result.back()
          .write()
          .metadataSingleFlag("TemperatureError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc3781");
    result.back().write().metadataSingleFlag("TemperatureError").hash("Bits").setInt64(0x00100000);
    result.back()
          .write()
          .metadataSingleFlag("TemperatureError")
          .hash("Description")
          .setString("Saturator, growth tube, or optics temperature out of range");

    result.back()
          .write()
          .metadataSingleFlag("WarmUpInProgress")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc3781");
    result.back().write().metadataSingleFlag("WarmUpInProgress").hash("Bits").setInt64(0x00200000);
    result.back()
          .write()
          .metadataSingleFlag("WarmUpInProgress")
          .hash("Description")
          .setString("Warm-up period in progress");

    result.back()
          .write()
          .metadataSingleFlag("TiltError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc3781");
    result.back().write().metadataSingleFlag("TiltError").hash("Bits").setInt64(0x00400000);
    result.back()
          .write()
          .metadataSingleFlag("TiltError")
          .hash("Description")
          .setString("Instrument tilted beyond 45 degrees");

    result.back()
          .write()
          .metadataSingleFlag("LaserCurrentError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc3781");
    result.back().write().metadataSingleFlag("LaserCurrentError").hash("Bits").setInt64(0x00800000);
    result.back()
          .write()
          .metadataSingleFlag("LaserCurrentError")
          .hash("Description")
          .setString("Laser current error");

    result.back()
          .write()
          .metadataSingleFlag("LiquidValveOpen")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc3781");
    result.back().write().metadataSingleFlag("LiquidValveOpen").hash("Bits").setInt64(0x01000000);
    result.back()
          .write()
          .metadataSingleFlag("LiquidValveOpen")
          .hash("Description")
          .setString("Water fill valve open");

    result.back()
          .write()
          .metadataSingleFlag("LiquidLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc3781");
    result.back().write().metadataSingleFlag("LiquidLow").hash("Bits").setInt64(0x02000000);
    result.back()
          .write()
          .metadataSingleFlag("LiquidLow")
          .hash("Description")
          .setString("Out of water condition present");

    result.back()
          .write()
          .metadataSingleFlag("CoincidenceCorrected")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc3781");
    result.back()
          .write()
          .metadataSingleFlag("CoincidenceCorrected")
          .hash("Description")
          .setString(
                  "Particle concentration has a coincidence correction applied based on beam dead time");

    return result;
}

SequenceValue::Transfer AcquireTSICPC3781::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_tsi_cpc3781");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);

    result.emplace_back(SequenceName({}, "raw_meta", "C"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000000.0");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Count rate"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZND"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Displayed condensation nuclei concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Display concentration"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZQ"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Reported flow rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Display flow"));

    return result;
}

void AcquireTSICPC3781::logValue(double startTime,
                                 double endTime,
                                 int streamID,
                                 SequenceName::Component name,
                                 Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    if (!FP::defined(startTime) || !FP::defined(endTime)) {
        if (realtimeEgress && FP::defined(endTime)) {
            realtimeEgress->emplaceData(SequenceName({}, "raw", std::move(name)), std::move(value),
                                        endTime, endTime + 1.0);
        }
        return;
    }
    SequenceValue
            dv(SequenceName({}, "raw", std::move(name)), std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        loggingMux.incoming(streamID, std::move(dv), loggingEgress);
        return;
    }
    if (loggingEgress) {
        loggingMux.incoming(streamID, dv, loggingEgress);
    }
    dv.setStart(endTime);
    dv.setEnd(endTime + 1.0);
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireTSICPC3781::realtimeValue(double time,
                                      SequenceName::Component name,
                                      Variant::Root &&value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

void AcquireTSICPC3781::invalidateLogValues(double frameTime)
{
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;

        if (loggingEgress != NULL)
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    loggingMux.clear();
    loggingLost(frameTime);
    reportedFlags.clear();
}

void AcquireTSICPC3781::emitMetadata(double frameTime)
{
    if (loggingEgress != NULL) {
        if (!haveEmittedLogMeta) {
            haveEmittedLogMeta = true;
            loggingMux.incoming(LogStream_Metadata, buildLogMeta(frameTime), loggingEgress);
        }
        loggingMux.advance(LogStream_Metadata, frameTime, loggingEgress);
    } else {
        invalidateLogValues(frameTime);
    }

    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;

        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }
}

void AcquireTSICPC3781::streamAdvance(int streamID, double time)
{
    quint32 bits = 1 << streamID;
    if (loggingEgress != NULL) {
        for (int i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
            if (i == streamID)
                continue;
            if (!(streamAge[i] & bits)) {
                streamAge[i] |= bits;
                continue;
            }
            streamAge[i] = bits;
            streamTime[i] = FP::undefined();
            loggingMux.advance(i, time, loggingEgress);
        }

        streamAge[streamID] = bits;
        streamTime[streamID] = time;
        loggingMux.advance(streamID, time, loggingEgress);
    } else {
        streamAge[streamID] = 0;
        streamTime[streamID] = FP::undefined();
    }

    if (!(streamAge[LogStream_State] & bits)) {
        streamAge[LogStream_State] |= bits;
    } else {
        double startTime = streamTime[LogStream_State];
        double endTime = time;

        if (loggingEgress != NULL) {
            streamAge[LogStream_State] = bits;
            streamTime[LogStream_State] = endTime;
        } else {
            streamAge[LogStream_State] = 0;
            streamTime[LogStream_State] = FP::undefined();
        }

        Variant::Flags flags = reportedFlags;
        flags.insert("CoincidenceCorrected");

        logValue(startTime, endTime, LogStream_State, "F1", Variant::Root(std::move(flags)));
        loggingMux.advance(LogStream_State, endTime, loggingEgress);
    }
}

void AcquireTSICPC3781::configAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

void AcquireTSICPC3781::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
}

void AcquireTSICPC3781::describeState(Variant::Write &info) const
{
    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        info.hash("ResponseState").setString("AutoprobePassiveInitialize");
        break;
    case RESP_AUTOPROBE_PASSIVE_WAIT:
        info.hash("ResponseState").setString("AutoprobePassiveWait");
        break;
    case RESP_PASSIVE_WAIT:
        info.hash("ResponseState").setString("PassiveWait");
        break;
    case RESP_PASSIVE_RUN:
        info.hash("ResponseState").setString("PassiveRun");
        break;
    case RESP_INTERACTIVE_INITIALIZE:
        info.hash("ResponseState").setString("InteractiveInitialize");
        break;
    case RESP_INTERACTIVE_RESTART_WAIT:
        info.hash("ResponseState").setString("InteractiveRestartWait");
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS:
        info.hash("ResponseState").setString("InteractiveStartStopReports");
        break;
    case RESP_INTERACTIVE_START_DISABLELOGGING:
        info.hash("ResponseState").setString("InteractiveStartDisableLogging");
        break;
    case RESP_INTERACTIVE_START_READVERSION:
        info.hash("ResponseState").setString("InteractiveStartReadVersion");
        break;
    case RESP_INTERACTIVE_START_SETTIME:
        info.hash("ResponseState").setString("InteractiveStartSetTime");
        break;
    case RESP_INTERACTIVE_START_SETINTERVAL:
        info.hash("ResponseState").setString("InteractiveStartSetInterval");
        break;
    case RESP_INTERACTIVE_START_READ_D:
        info.hash("ResponseState").setString("InteractiveStartReadD");
        break;
    case RESP_INTERACTIVE_START_READ_S:
        info.hash("ResponseState").setString("InteractiveStartReadS");
        break;

    case RESP_INTERACTIVE_RUN_READ_D:
        info.hash("ResponseState").setString("InteractiveRunD");
        break;
    case RESP_INTERACTIVE_RUN_READ_S:
        info.hash("ResponseState").setString("InteractiveRunS");
        break;
    case RESP_INTERACTIVE_RUN_READ_LASER:
        info.hash("ResponseState").setString("InteractiveRunLaser");
        break;
    case RESP_INTERACTIVE_RUN_READ_NOZZLE:
        info.hash("ResponseState").setString("InteractiveRunNozzle");
        break;
    case RESP_INTERACTIVE_RUN_WAIT:
        info.hash("ResponseState").setString("InteractiveRunWait");
        break;
    }
}

static double calculateRate(double CNT, double LT)
{
    if (!FP::defined(CNT) || !FP::defined(LT))
        return FP::undefined();
    if (CNT < 0.0 || LT <= 0.0)
        return FP::undefined();
    return CNT / LT;
}

static double calculateConcentration(double C, double Q)
{
    if (!FP::defined(C) || !FP::defined(Q))
        return FP::undefined();
    if (Q <= 0.0)
        return FP::undefined();
    return C / (Q * (1000.0 / 60.0));
}

int AcquireTSICPC3781::processDRecord(const Util::ByteView &line, double &frameTime)
{
    if (line[0] != 'D') return 1;
    auto fields = Util::as_deque(line.split(','));
    if (fields.empty()) return 2;
    fields.pop_front();

    bool ok = false;

    /* Mode unused */
    if (fields.empty()) return 3;
    fields.pop_front();

    if (fields.empty()) return 4;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root flags((qint64) field.parse_u16(&ok, 16));
    if (!ok) return 5;
    remap("FRAW", flags);

    if (fields.empty()) return 6;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ND(field.parse_real(&ok));
    if (!ok) return 7;
    if (!FP::defined(ND.read().toReal())) return 8;
    remap("ZND", ND);

    /* Sample time unused */
    if (fields.empty()) return 9;
    fields.pop_front();

    if (fields.empty()) return 10;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root LT(field.parse_real(&ok));
    if (!ok) return 11;
    if (!FP::defined(LT.read().toReal())) return 12;
    remap("LTIME", LT);

    if (fields.empty()) return 13;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root CNT(field.parse_real(&ok));
    if (!ok) return 14;
    if (!FP::defined(CNT.read().toReal())) return 15;
    remap("ZCNT", CNT);

    /* PM, reserved */
    if (fields.empty()) return 16;
    fields.pop_front();

    /* RP, reserved */
    if (fields.empty()) return 17;
    fields.pop_front();

    if (config.first().strictMode) {
        if (!fields.empty())
            return 100;
    }

    if (!FP::defined(frameTime))
        frameTime = lastRecordTime;

    if (INTEGER::defined(flags.read().toInt64())) {
        reportedFlags.clear();
        qint64 flagsBits(flags.read().toInt64());
        for (int i = 0;
                i <
                        (int) (sizeof(instrumentFlagTranslation) /
                                sizeof(instrumentFlagTranslation[0]));
                i++) {
            if (flagsBits & (Q_INT64_C(1) << i)) {
                if (instrumentFlagTranslation[i] != NULL) {
                    reportedFlags.insert(instrumentFlagTranslation[i]);
                } else {
                    qCDebug(log) << "Unrecognized bit" << hex << ((1 << i))
                                 << "set in instrument flags";
                }
            }
        }
    }

    if (FP::defined(frameTime)) {
        configAdvance(frameTime);
        emitMetadata(frameTime);

        double startTime = streamTime[LogStream_D];
        double endTime = frameTime;

        double Q = lastReportedFlow;
        if (!FP::defined(Q)) {
            Q = 0.12;
            Variant::Root v(Q);
            remap("Q", v);
            Q = v.read().toDouble();
        }

        Variant::Root C(calculateRate(CNT.read().toDouble(), LT.read().toDouble()));
        remap("C", C);

        Variant::Root N(calculateConcentration(C.read().toDouble(), Q));
        remap("N", N);

        logValue(startTime, endTime, LogStream_D, "N", std::move(N));
        realtimeValue(frameTime, "C", std::move(C));
        realtimeValue(frameTime, "ZND", std::move(ND));

        streamAdvance(LogStream_D, frameTime);
    }

    return 0;
}

int AcquireTSICPC3781::processSRecord(const Util::ByteView &line, double &frameTime)
{
    if (line[0] != 'S') return 1;
    auto fields = Util::as_deque(line.split(','));
    if (fields.empty()) return 2;
    fields.pop_front();

    bool ok = false;

    if (fields.empty()) return 3;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    double value = field.parse_real(&ok);
    if (!ok) return 4;
    if (!FP::defined(value)) return 5;
    Variant::Root Q(value / 1000.0);
    Variant::Root ZQ = Q;
    remap("ZQ", ZQ);
    if (FP::defined(config.first().flow))
        Q.write().setDouble(config.first().flow);
    remap("Q", Q);
    lastReportedFlow = Q.read().toDouble();

    if (fields.empty()) return 6;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root P(field.parse_real(&ok));
    if (!ok) return 7;
    if (!FP::defined(P.read().toReal())) return 8;
    remap("P", P);

    if (fields.empty()) return 9;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T1(field.parse_real(&ok));
    if (!ok) return 10;
    if (!FP::defined(T1.read().toReal())) return 11;
    remap("T1", T1);

    if (fields.empty()) return 12;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T2(field.parse_real(&ok));
    if (!ok) return 13;
    if (!FP::defined(T2.read().toReal())) return 14;
    remap("T2", T2);

    if (fields.empty()) return 15;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T3(field.parse_real(&ok));
    if (!ok) return 16;
    if (!FP::defined(T3.read().toReal())) return 17;
    remap("T3", T3);

    if (config.first().strictMode) {
        if (!fields.empty())
            return 100;
    }

    if (!FP::defined(frameTime))
        frameTime = lastRecordTime;

    if (FP::defined(frameTime)) {
        configAdvance(frameTime);
        emitMetadata(frameTime);

        double startTime = streamTime[LogStream_S];
        double endTime = frameTime;

        logValue(startTime, endTime, LogStream_S, "Q", std::move(Q));
        logValue(startTime, endTime, LogStream_S, "P", std::move(P));
        logValue(startTime, endTime, LogStream_S, "T1", std::move(T1));
        logValue(startTime, endTime, LogStream_S, "T2", std::move(T2));
        logValue(startTime, endTime, LogStream_S, "T3", std::move(T3));
        realtimeValue(frameTime, "ZQ", std::move(ZQ));

        streamAdvance(LogStream_S, frameTime);
    }

    return 0;
}

int AcquireTSICPC3781::processLRecord(const Util::ByteView &line, double &frameTime)
{
    if (line.size() < 5) return 1;
    auto fields = Util::as_deque(line.split(','));
    if (fields.empty()) return 2;
    Util::ByteView field;
    bool ok = false;

    auto subfields = Util::as_deque(field.split('/'));
    if (subfields.size() == 3) {
        Q_ASSERT(!subfields.empty());
        field = subfields.front().string_trimmed();
        subfields.pop_front();
        qint64 iyear = field.parse_i32(&ok);
        if (!ok) return 3;
        if (iyear < 1900) return 4;
        if (iyear > 2999) return 5;
        Variant::Root year(iyear);
        remap("YEAR", year);
        iyear = year.read().toInt64();

        Q_ASSERT(!subfields.empty());
        field = subfields.front().string_trimmed();
        subfields.pop_front();
        qint64 imonth = field.parse_i32(&ok);
        if (!ok) return 6;
        if (imonth < 1) return 7;
        if (imonth > 12) return 8;
        Variant::Root month(imonth);
        remap("MONTH", month);
        imonth = month.read().toInt64();

        Q_ASSERT(!subfields.empty());
        field = subfields.front().string_trimmed();
        subfields.pop_front();
        qint64 iday = field.parse_i32(&ok);
        if (!ok) return 9;
        if (iday < 1) return 10;
        if (iday > 31) return 11;
        Variant::Root day(iday);
        remap("DAY", day);
        iday = day.read().toInt64();

        if (fields.empty()) return 12;
        subfields = Util::as_deque(fields.front().split(':'));
        fields.pop_front();
        if (subfields.size() != 3) return 13;

        Q_ASSERT(!subfields.empty());
        field = subfields.front().string_trimmed();
        subfields.pop_front();
        qint64 ihour = field.parse_i32(&ok);
        if (!ok) return 14;
        if (ihour < 0) return 15;
        if (ihour > 23) return 16;
        Variant::Root hour(ihour);
        remap("HOUR", hour);
        ihour = hour.read().toInt64();

        Q_ASSERT(!subfields.empty());
        field = subfields.front().string_trimmed();
        subfields.pop_front();
        qint64 iminute = field.parse_i32(&ok);
        if (!ok) return 17;
        if (iminute < 0) return 18;
        if (iminute > 59) return 19;
        Variant::Root minute(iminute);
        remap("MINUTE", minute);
        iminute = minute.read().toInt64();

        Q_ASSERT(!subfields.empty());
        field = subfields.front().string_trimmed();
        subfields.pop_front();
        qint64 isecond = field.parse_i32(&ok);
        if (!ok) return 20;
        if (isecond < 0) return 21;
        if (isecond > 60) return 22;
        Variant::Root second(isecond);
        remap("SECOND", second);
        isecond = second.read().toInt64();

        if (!FP::defined(frameTime) && INTEGER::defined(iyear) && iyear >= 1900 &&
                iyear <= 2999 &&
                INTEGER::defined(imonth) &&
                imonth >= 1 &&
                imonth <= 12 &&
                INTEGER::defined(iday) &&
                iday >= 1 &&
                iday <= 31 &&
                INTEGER::defined(ihour) &&
                ihour >= 0 &&
                ihour <= 23 &&
                INTEGER::defined(iminute) &&
                iminute >= 0 &&
                iminute <= 59 &&
                INTEGER::defined(isecond) &&
                isecond >= 0 &&
                isecond <= 60) {
            frameTime = Time::fromDateTime(QDateTime(QDate((int) iyear, (int) imonth, (int) iday),
                                                     QTime((int) ihour, (int) iminute,
                                                           (int) isecond), Qt::UTC));
        }
        if (FP::defined(lastRecordTime) && FP::defined(frameTime) && frameTime < lastRecordTime)
            return -1;
        lastRecordTime = frameTime;
    } else {
        qint64 iepoch = field.parse_i64(&ok, 10);
        if (!ok) return 23;
        if (iepoch < 10000) return 24;
        Variant::Root epoch(iepoch);
        remap("EPOCH", epoch);
        iepoch = epoch.read().toInt64();

        if (!FP::defined(frameTime) && INTEGER::defined(iepoch) && iepoch >= 10000) {
            frameTime = (double) iepoch;
        }
        if (FP::defined(lastRecordTime) && FP::defined(frameTime) && frameTime < lastRecordTime)
            return -1;
        lastRecordTime = frameTime;
    }

    if (fields.empty()) return 25;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root flags((qint64) field.parse_u16(&ok, 16));
    if (!ok) return 26;
    remap("FRAW", flags);

    if (fields.empty()) return 27;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ND(field.parse_real(&ok));
    if (!ok) return 28;
    if (!FP::defined(ND.read().toReal())) return 29;
    remap("ZND", ND);

    /* Sample time unused */
    if (fields.empty()) return 30;
    fields.pop_front();

    if (fields.empty()) return 31;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root LT(field.parse_real(&ok));
    if (!ok) return 32;
    if (!FP::defined(LT.read().toReal())) return 33;
    remap("ZLT", LT);

    if (fields.empty()) return 34;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root CNT(field.parse_real(&ok));
    if (!ok) return 35;
    if (!FP::defined(CNT.read().toReal())) return 36;
    remap("ZCNT", CNT);

    if (fields.empty()) return 37;
    field = fields.front().string_trimmed();
    fields.pop_front();
    double value = field.parse_real(&ok);
    if (!ok) return 38;
    if (!FP::defined(value)) return 39;
    Variant::Root Q(value / 1000.0);
    Variant::Root ZQ = Q;
    remap("ZQ", ZQ);
    if (FP::defined(config.first().flow))
        Q.write().setDouble(config.first().flow);
    remap("Q", Q);
    lastReportedFlow = Q.read().toDouble();

    if (fields.empty()) return 40;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root P(field.parse_real(&ok));
    if (!ok) return 41;
    if (!FP::defined(P.read().toReal())) return 42;
    remap("P", P);

    if (config.first().strictMode) {
        if (!fields.empty())
            return 100;
    }

    if (INTEGER::defined(flags.read().toInt64())) {
        reportedFlags.clear();
        qint64 flagsBits(flags.read().toInt64());
        for (int i = 0;
                i <
                        (int) (sizeof(instrumentFlagTranslation) /
                                sizeof(instrumentFlagTranslation[0]));
                i++) {
            if (flagsBits & (Q_INT64_C(1) << i)) {
                if (instrumentFlagTranslation[i] != NULL) {
                    reportedFlags.insert(instrumentFlagTranslation[i]);
                } else {
                    qCDebug(log) << "Unrecognized bit" << hex << ((1 << i))
                                 << "set in instrument flags";
                }
            }
        }
    }

    if (FP::defined(frameTime)) {
        configAdvance(frameTime);
        emitMetadata(frameTime);

        double startTime = streamTime[LogStream_D];
        double endTime = frameTime;

        Variant::Root C(calculateRate(CNT.read().toDouble(), LT.read().toDouble()));
        remap("C", C);

        Variant::Root N(calculateConcentration(C.read().toDouble(), lastReportedFlow));
        remap("N", N);

        logValue(startTime, endTime, LogStream_D, "N", std::move(N));
        realtimeValue(frameTime, "C", std::move(C));
        realtimeValue(frameTime, "ZND", std::move(ND));

        startTime = streamTime[LogStream_S];
        endTime = frameTime;

        logValue(startTime, endTime, LogStream_S, "Q", std::move(Q));
        logValue(startTime, endTime, LogStream_S, "P", std::move(P));
        realtimeValue(frameTime, "ZQ", std::move(ZQ));

        streamAdvance(LogStream_D, frameTime);
        streamAdvance(LogStream_S, frameTime);
    }

    return 0;
}

int AcquireTSICPC3781::processResponse(const Util::ByteView &line, double &frameTime)
{
    Q_ASSERT(!config.isEmpty());

    if (!commandQueue.isEmpty()) {
        bool ok = false;

        Command command(commandQueue.takeFirst());
        switch (command.getType()) {
        case COMMAND_INVALID:
            return -1;

        case COMMAND_ERROR:
            if (line != "ERROR")
                return 1;
            return -1;

        case COMMAND_OK:
            if (line != "OK")
                return 2;

            switch (responseState) {
            case RESP_INTERACTIVE_START_DISABLELOGGING:
                if (controlStream != NULL) {
                    controlStream->writeControl("RV\r");
                }
                responseState = RESP_INTERACTIVE_START_READVERSION;
                commandQueue.append(Command(COMMAND_RV));
                timeoutAt(frameTime + 2.0);
                return 0;

            case RESP_INTERACTIVE_START_SETTIME:
                if (controlStream != NULL) {
                    int interval = (int) floor(config.first().sampleTime * 10.0 + 0.5);
                    if (interval < 1) interval = 1; else if (interval > 36000) interval = 36000;
                    Util::ByteArray send("SM,0,");
                    send += QByteArray::number(interval);
                    send.push_back('\r');
                    controlStream->writeControl(std::move(send));
                }
                responseState = RESP_INTERACTIVE_START_SETINTERVAL;
                commandQueue.append(Command(COMMAND_OK));
                timeoutAt(frameTime + 2.0);
                return 0;

            case RESP_INTERACTIVE_START_SETINTERVAL:
                if (controlStream != NULL) {
                    controlStream->writeControl("RRD\r");
                }
                responseState = RESP_INTERACTIVE_START_READ_D;
                commandQueue.append(Command(COMMAND_RRD));
                timeoutAt(frameTime + 2.0);
                return 0;

            case RESP_PASSIVE_RUN:
                break;

            default:
                return -1;
            }

            return -1;

        case COMMAND_IGNORED:
            return -1;

        case COMMAND_RD: {
            Variant::Root ND(Util::ByteView(line).string_trimmed().parse_real(&ok));
            if (!ok) return 3;
            if (!FP::defined(ND.read().toReal())) return 4;
            remap("ZND", ND);

            realtimeValue(frameTime, "ZND", std::move(ND));

            return -1;
        }

        case COMMAND_RL: {
            Variant::Root A(Util::ByteView(line).string_trimmed().parse_real(&ok));
            if (!ok) return 5;
            if (!FP::defined(A.read().toReal())) return 6;
            remap("A", A);

            if (!FP::defined(frameTime))
                frameTime = lastRecordTime;

            if (FP::defined(frameTime)) {
                configAdvance(frameTime);
                emitMetadata(frameTime);

                double startTime = streamTime[LogStream_RL];
                double endTime = frameTime;
                logValue(startTime, endTime, LogStream_RL, "A", std::move(A));
                streamAdvance(LogStream_RL, frameTime);
            }

            switch (responseState) {
            case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
            case RESP_AUTOPROBE_PASSIVE_WAIT:
            case RESP_PASSIVE_WAIT:
                break;

            case RESP_INTERACTIVE_RUN_WAIT:
            case RESP_INTERACTIVE_RUN_READ_LASER:
                if (controlStream != NULL) {
                    controlStream->writeControl("RN\r");
                }
                responseState = RESP_INTERACTIVE_RUN_READ_NOZZLE;
                commandQueue.append(Command(COMMAND_RN));
                timeoutAt(frameTime + 2.0);
                return 0;

            case RESP_PASSIVE_RUN:
                break;

            default:
                return -1;
            }

            return -1;
        }

        case COMMAND_RN: {
            Variant::Root PCT(Util::ByteView(line).string_trimmed().parse_real(&ok));
            if (!ok) return 5;
            if (!FP::defined(PCT.read().toReal())) return 6;
            remap("PCT", PCT);

            if (!FP::defined(frameTime))
                frameTime = lastRecordTime;

            if (FP::defined(frameTime)) {
                configAdvance(frameTime);
                emitMetadata(frameTime);

                double startTime = streamTime[LogStream_RN];
                double endTime = frameTime;
                logValue(startTime, endTime, LogStream_RN, "PCT", std::move(PCT));
                streamAdvance(LogStream_RN, frameTime);
            }

            switch (responseState) {
            case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
            case RESP_AUTOPROBE_PASSIVE_WAIT:
            case RESP_PASSIVE_WAIT:
                break;

            case RESP_INTERACTIVE_RUN_WAIT:
            case RESP_INTERACTIVE_RUN_READ_NOZZLE:
                if (!FP::defined(config.first().pollInterval) ||
                        config.first().pollInterval <= 0.0) {
                    responseState = RESP_INTERACTIVE_RUN_WAIT;
                    timeoutAt(frameTime + config.first().pollInterval);
                } else {
                    if (controlStream != NULL) {
                        controlStream->writeControl("RRD\r");
                    }
                    responseState = RESP_INTERACTIVE_RUN_READ_D;
                    commandQueue.append(Command(COMMAND_RRD));
                    timeoutAt(frameTime + 2.0);
                }
                return 0;

            case RESP_PASSIVE_RUN:
                break;

            default:
                return -1;
            }

            return -1;
        }

        case COMMAND_RV: {
            QRegExp reVersion
                    ("Model\\s+(\\S+)\\s+Ver(?:sion)?\\s+(\\S+)\\s+(?:(?:S/N)|(?:Ser(?:ial)?\\s*Num(?:ber)?))\\s+(\\d+)\\s*",
                     Qt::CaseInsensitive);
            if (line == "ERROR" || line.empty() || !reVersion.exactMatch(line.toQString()))
                return 7;
            QString model(reVersion.cap(1));
            QString firmware(reVersion.cap(2));
            QString serial(reVersion.cap(3));
            if (model != "3781") return 7;

            if (instrumentMeta["FirmwareVersion"].toQString() != firmware) {
                instrumentMeta["FirmwareVersion"].setString(firmware);
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            if (instrumentMeta["SerialNumber"].toQString() != serial) {
                instrumentMeta["SerialNumber"].setString(serial);
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            switch (responseState) {
            case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
            case RESP_AUTOPROBE_PASSIVE_WAIT:
            case RESP_PASSIVE_WAIT:
                break;

            case RESP_INTERACTIVE_START_READVERSION:
                if (controlStream != NULL) {
                    QDateTime dt(Time::toDateTime(frameTime + 0.5));
                    Util::ByteArray send("LC,");
                    send += QByteArray::number(dt.date().year());
                    send.push_back(',');
                    send += QByteArray::number(dt.date().month());
                    send.push_back(',');
                    send += QByteArray::number(dt.date().day());
                    send.push_back(',');
                    send += QByteArray::number(dt.time().hour());
                    send.push_back(',');
                    send += QByteArray::number(dt.time().minute());
                    send.push_back(',');
                    send += QByteArray::number(dt.time().second());
                    send.push_back('\r');
                    controlStream->writeControl(std::move(send));
                }
                responseState = RESP_INTERACTIVE_START_SETTIME;
                commandQueue.append(Command(COMMAND_OK));
                timeoutAt(frameTime + 2.0);
                return 0;

            case RESP_PASSIVE_RUN:
                break;

            default:
                return -1;
            }

            return 0;
        }

        case COMMAND_RRD: {
            int code = processDRecord(line, frameTime);
            if (code < 0) return code; else if (code > 0) return code + 1000;

            switch (responseState) {
            case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
            case RESP_AUTOPROBE_PASSIVE_WAIT:
            case RESP_PASSIVE_WAIT:
                break;

            case RESP_INTERACTIVE_RUN_WAIT:
            case RESP_INTERACTIVE_RUN_READ_D:
                if (controlStream != NULL) {
                    controlStream->writeControl("RRS\r");
                }
                responseState = RESP_INTERACTIVE_RUN_READ_S;
                commandQueue.append(Command(COMMAND_RRS));
                timeoutAt(frameTime + 2.0);
                return 0;

            case RESP_INTERACTIVE_START_READ_D:
                if (controlStream != NULL) {
                    controlStream->writeControl("RRS\r");
                }
                responseState = RESP_INTERACTIVE_START_READ_S;
                commandQueue.append(Command(COMMAND_RRS));
                timeoutAt(frameTime + 2.0);
                return 0;

            case RESP_PASSIVE_RUN:
                break;

            default:
                return -1;
            }

            if (FP::defined(frameTime))
                timeoutAt(frameTime + config.first().sampleTime * 2.0);

            return 0;
        }

        case COMMAND_RRS: {
            int code = processSRecord(line, frameTime);
            if (code < 0) return code; else if (code > 0) return code + 2000;

            switch (responseState) {
            case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
            case RESP_AUTOPROBE_PASSIVE_WAIT:
            case RESP_PASSIVE_WAIT:
                break;

            case RESP_INTERACTIVE_RUN_WAIT:
            case RESP_INTERACTIVE_RUN_READ_S:
                if (controlStream != NULL) {
                    controlStream->writeControl("RL\r");
                }
                responseState = RESP_INTERACTIVE_RUN_READ_LASER;
                commandQueue.append(Command(COMMAND_RL));
                timeoutAt(frameTime + 2.0);
                return 0;

            case RESP_INTERACTIVE_START_READ_S: {
                Variant::Write info = Variant::Write::empty();
                describeState(info);
                event(frameTime, QObject::tr("Communications established."), false, info);
            }

                if (controlStream != NULL) {
                    controlStream->writeControl("RRD\r");
                }
                responseState = RESP_INTERACTIVE_RUN_READ_D;
                commandQueue.append(Command(COMMAND_RRD));
                timeoutAt(frameTime + 2.0);

                qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();
                return 0;

            case RESP_PASSIVE_RUN:
                break;

            default:
                return -1;
            }

            if (FP::defined(frameTime))
                timeoutAt(frameTime + config.first().sampleTime * 2.0);

            return 0;
        }

        }
    }

    if (line.size() < 3) return 100;
    char code = line[0];

    if (code == 'D') {
        int rcode = processDRecord(line, frameTime);
        if (rcode < 0) return rcode; else if (rcode > 0) return rcode + 3000;
    } else if (code == 'S') {
        int rcode = processSRecord(line, frameTime);
        if (rcode < 0) return rcode; else if (rcode > 0) return rcode + 4000;
    } else if (code == 'A' || code == 'C') {
        return -1;
    } else {
        int rcode = processLRecord(line, frameTime);
        if (rcode < 0) return rcode; else if (rcode > 0) return rcode + 5000;
    }

    if (FP::defined(frameTime))
        timeoutAt(frameTime + config.first().sampleTime * 2.0);

    return 0;
}


void AcquireTSICPC3781::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (FP::defined(frameTime))
        configAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        int code = processResponse(frame, frameTime);
        if (code == 0) {
            if (++autoprobePassiveValidRecords > 10) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                Variant::Write info = Variant::Write::empty();
                describeState(info);
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);

                responseState = RESP_PASSIVE_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                timeoutAt(frameTime + config.first().sampleTime + 1.0);
            }
        } else if (code > 0) {
            qCDebug(log) << "Passive autoprobe failed at" << Logging::time(frameTime) << ":"
                        << frame << "rejected with code" << code;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            responseState = RESP_PASSIVE_WAIT;
            autoprobePassiveValidRecords = 0;
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_PASSIVE_WAIT:
        if (processResponse(frame, frameTime) == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);

            Variant::Write info = Variant::Write::empty();
            describeState(info);
            event(frameTime, QObject::tr("Passive communications established."), false, info);

            responseState = RESP_PASSIVE_RUN;
            generalStatusUpdated();

            timeoutAt(frameTime + config.first().sampleTime + 1.0);
        } else {
            invalidateLogValues(frameTime);
        }
        break;


    case RESP_PASSIVE_RUN: {
        int code = processResponse(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + config.first().sampleTime + 1.0);
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();
            describeState(info);
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            timeoutAt(FP::undefined());
            responseState = RESP_PASSIVE_WAIT;
            generalStatusUpdated();

            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_RUN_READ_D:
    case RESP_INTERACTIVE_RUN_READ_S:
    case RESP_INTERACTIVE_RUN_READ_LASER:
    case RESP_INTERACTIVE_RUN_READ_NOZZLE:
    case RESP_INTERACTIVE_RUN_WAIT: {
        int code = processResponse(frame, frameTime);
        if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();
            describeState(info);
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            if (controlStream != NULL) {
                controlStream->writeControl("SM,0,0\r");
            }
            timeoutAt(frameTime + 30.0);
            discardData(frameTime + 0.5);
            responseState = RESP_INTERACTIVE_START_STOPREPORTS;
            generalStatusUpdated();

            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_STOPREPORTS:
        break;

    case RESP_INTERACTIVE_START_DISABLELOGGING:
    case RESP_INTERACTIVE_START_READVERSION:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETINTERVAL:
    case RESP_INTERACTIVE_START_READ_D:
    case RESP_INTERACTIVE_START_READ_S: {
        int code = processResponse(frame, frameTime);
        if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected during start communications state" << responseState
                         << "with code" << code;

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            discardData(frameTime + 10.0);
            timeoutAt(frameTime + 30.0);
            generalStatusUpdated();

            invalidateLogValues(frameTime);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            invalidateLogValues(frameTime);
        }
        break;
    }

    default:
        break;
    }
}

void AcquireTSICPC3781::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        /* Fall through */
    case RESP_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
        commandQueue.clear();
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            event(frameTime, QObject::tr("Timeout waiting for response.  Communications Dropped."),
                  true, info);
        }

        commandQueue.clear();
        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();
        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_RUN_READ_D:
    case RESP_INTERACTIVE_RUN_READ_S:
    case RESP_INTERACTIVE_RUN_READ_LASER:
    case RESP_INTERACTIVE_RUN_READ_NOZZLE:
        qCDebug(log) << "Timeout in interactive state" << responseState << "at"
                     << Logging::time(frameTime);

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            event(frameTime, QObject::tr("Timeout waiting for response.  Communications Dropped."),
                  true, info);
        }

        commandQueue.clear();
        if (controlStream != NULL) {
            controlStream->writeControl("SM,0,0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(frameTime);
        timeoutAt(frameTime + 30.0);
        generalStatusUpdated();

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_RUN_WAIT:
        if (controlStream != NULL) {
            controlStream->writeControl("RRD\r");
        }
        responseState = RESP_INTERACTIVE_RUN_READ_D;
        commandQueue.append(Command(COMMAND_RRD));
        timeoutAt(frameTime + 2.0);
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_DISABLELOGGING:
    case RESP_INTERACTIVE_START_READVERSION:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETINTERVAL:
    case RESP_INTERACTIVE_START_READ_D:
    case RESP_INTERACTIVE_START_READ_S:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);
        invalidateLogValues(frameTime);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        timeoutAt(frameTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("SM,0,0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(frameTime + 0.5);
        break;

    default:
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireTSICPC3781::discardDataCompleted(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configAdvance(frameTime);

    Q_ASSERT(!config.isEmpty());

    switch (responseState) {
    case RESP_INTERACTIVE_START_STOPREPORTS:
        if (controlStream != NULL) {
            controlStream->writeControl("LM,0\r");
        }
        commandQueue.append(Command(COMMAND_OK));

        responseState = RESP_INTERACTIVE_START_DISABLELOGGING;
        timeoutAt(frameTime + 2.0);
        discardData(FP::undefined());
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        timeoutAt(frameTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("SM,0,0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(frameTime + 0.5);
        break;

    default:
        break;
    }
}

void AcquireTSICPC3781::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    if (frame == "LC") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("LC,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "LE") {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "LI") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame == "LM") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("LM,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "RD") {
        commandQueue.append(Command(COMMAND_RD));
    } else if (frame == "RL") {
        commandQueue.append(Command(COMMAND_RL));
    } else if (frame == "RN") {
        commandQueue.append(Command(COMMAND_RN));
    } else if (frame == "RRA") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame == "RRC") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame == "RRD") {
        commandQueue.append(Command(COMMAND_RRD));
    } else if (frame == "RRS") {
        commandQueue.append(Command(COMMAND_RRS));
    } else if (frame == "RTO") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame == "RV") {
        commandQueue.append(Command(COMMAND_RV));
    } else if (frame == "SAS") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SAS,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SDC") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SDC,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SDS") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SDS,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SDT") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SDT,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SDZ") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SDZ,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SFC") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SFC,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SFM") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SFM,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SFS") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SFS,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SFZ") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SFZ,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SG") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SG,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SGS") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SGS,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SL") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SL,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SLS") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SLS,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SM") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SM,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SO") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SO,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SOS") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SOS,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SP") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SP,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SS") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SS,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SSS") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SSS,")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "SZ") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame.string_start("SZ,")) {
        commandQueue.append(Command(COMMAND_OK));
    }
}

Variant::Root AcquireTSICPC3781::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireTSICPC3781::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN_READ_D:
    case RESP_INTERACTIVE_RUN_READ_S:
    case RESP_INTERACTIVE_RUN_READ_LASER:
    case RESP_INTERACTIVE_RUN_READ_NOZZLE:
    case RESP_INTERACTIVE_RUN_WAIT:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

AcquisitionInterface::AutoprobeStatus AcquireTSICPC3781::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireTSICPC3781::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_READ_D:
    case RESP_INTERACTIVE_RUN_READ_S:
    case RESP_INTERACTIVE_RUN_READ_LASER:
    case RESP_INTERACTIVE_RUN_READ_NOZZLE:
    case RESP_INTERACTIVE_RUN_WAIT:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_DISABLELOGGING:
    case RESP_INTERACTIVE_START_READVERSION:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETINTERVAL:
    case RESP_INTERACTIVE_START_READ_D:
    case RESP_INTERACTIVE_START_READ_S:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("SM,0,0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(time + 0.5);

        generalStatusUpdated();
        break;
    }
}

void AcquireTSICPC3781::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobePassiveValidRecords = 0;
    commandQueue.clear();
    discardData(time + 0.5, 1);
    timeoutAt(time + 15.0);
    generalStatusUpdated();
}

void AcquireTSICPC3781::autoprobePromote(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    switch (responseState) {
    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Promoted from interactive wait at" << Logging::time(time);
        timeoutAt(time);
        break;
    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_DISABLELOGGING:
    case RESP_INTERACTIVE_START_READVERSION:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETINTERVAL:
    case RESP_INTERACTIVE_START_READ_D:
    case RESP_INTERACTIVE_START_READ_S:
    case RESP_INTERACTIVE_RUN_READ_D:
    case RESP_INTERACTIVE_RUN_READ_S:
    case RESP_INTERACTIVE_RUN_READ_LASER:
    case RESP_INTERACTIVE_RUN_READ_NOZZLE:
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);
        timeoutAt(time + 2.0);
        break;

    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("SM,0,0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(time + 0.5);

        generalStatusUpdated();

        break;
    }
}

void AcquireTSICPC3781::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);
    timeoutAt(time + config.first().sampleTime * 2.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_RUN_READ_D:
    case RESP_INTERACTIVE_RUN_READ_S:
    case RESP_INTERACTIVE_RUN_READ_LASER:
    case RESP_INTERACTIVE_RUN_READ_NOZZLE:
    case RESP_INTERACTIVE_RUN_WAIT:
        /* Already in unpolled, so don't reset timeout */
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from unpolled interactive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        memset(streamAge, 0, sizeof(streamAge));

        discardData(FP::undefined());
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireTSICPC3781::getDefaults()
{
    AutomaticDefaults result;
    result.name = "N$1$2";
    result.setSerialN81(115200);
    return result;
}


ComponentOptions AcquireTSICPC3781Component::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options);
    return options;
}

ComponentOptions AcquireTSICPC3781Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireTSICPC3781Component::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireTSICPC3781Component::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireTSICPC3781Component::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireTSICPC3781Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireTSICPC3781Component::createAcquisitionPassive(const ComponentOptions &options,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireTSICPC3781(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireTSICPC3781Component::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireTSICPC3781(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireTSICPC3781Component::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{
    std::unique_ptr<AcquireTSICPC3781> i(new AcquireTSICPC3781(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireTSICPC3781Component::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{
    std::unique_ptr<AcquireTSICPC3781> i(new AcquireTSICPC3781(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
