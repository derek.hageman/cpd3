/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef OUTPUTNETCDF_H
#define OUTPUTNETCDF_H

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>
#include <QHash>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "fileoutput/engine.hxx"
#include "datacore/segment.hxx"

class OutputNetCDF : public CPD3::CPD3Action {
Q_OBJECT

    std::string profile;
    QString directory;
    std::vector<CPD3::Data::SequenceName::Component> stations;
    double start;
    double end;

    bool terminated;
    std::unique_ptr<CPD3::Output::Engine> engine;
    std::mutex mutex;

    OutputNetCDF() = delete;

    bool testTerminated();

public:
    OutputNetCDF(const CPD3::ComponentOptions &options,
                 double start,
                 double end,
                 const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~OutputNetCDF();

    virtual void signalTerminate();

protected:
    virtual void run();
};

class OutputNetCDFComponent : public QObject, virtual public CPD3::ActionComponentTime {
Q_OBJECT
    Q_INTERFACES(CPD3::ActionComponentTime)

    Q_PLUGIN_METADATA(IID
                              "CPD3.output_netcdf"
                              FILE
                              "output_netcdf.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int actionRequireStations();

    virtual int actionAllowStations();

    virtual bool actionRequiresTime();

    virtual CPD3::CPD3Action *createTimeAction(const CPD3::ComponentOptions &options,
                                               double start,
                                               double end,
                                               const std::vector<std::string> &stations = {});
};

#endif
