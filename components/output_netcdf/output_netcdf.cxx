/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "datacore/archive/access.hxx"
#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "core/memory.hxx"
#include "core/threading.hxx"
#include "datacore/stream.hxx"
#include "fileoutput/netcdf.hxx"

#include "output_netcdf.hxx"


Q_LOGGING_CATEGORY(log_component_output_netcdf, "cpd3.component.output.netcdf", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Output;

OutputNetCDF::OutputNetCDF(const ComponentOptions &options,
                           double setStart,
                           double setEnd,
                           const std::vector<SequenceName::Component> &setStations) : profile(
        SequenceName::impliedProfile()),
                                                                                      directory(),
                                                                                      stations(
                                                                                              setStations),
                                                                                      start(setStart),
                                                                                      end(setEnd),
                                                                                      terminated(
                                                                                              false)
{
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }
    if (options.isSet("directory")) {
        directory = qobject_cast<ComponentOptionDirectory *>(options.get("directory"))->get();
    }
}

OutputNetCDF::~OutputNetCDF()
{
    if (engine) {
        engine->signalTerminate();
        engine->wait();
        engine.reset();
    }
}

void OutputNetCDF::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
        if (engine)
            engine->signalTerminate();
    }
}

bool OutputNetCDF::testTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}

namespace {
class NetCDFEngine : public Engine {
    std::unique_ptr<QThread> pipelineThread;
    QObject *context;
    std::unique_ptr<StreamPipeline> pipeline;

    void endPipelineThread()
    {
        pipeline.reset();
        context->deleteLater();
        context = nullptr;
        pipelineThread->quit();
    }
public:
    explicit NetCDFEngine(std::unique_ptr<Stage> &&stage) : Engine(std::move(stage)),
                                                            context(nullptr)
    { }

    virtual ~NetCDFEngine()
    {
        if (pipelineThread) {
            Q_ASSERT(context);

            Threading::runQueuedFunctor(context, [this] {
                if (pipeline) {
                    pipeline->signalTerminate();
                    pipeline->waitInEventLoop();
                }

                return endPipelineThread();
            });

            pipelineThread->wait();
            pipelineThread.reset();
        }
    }

protected:
    bool startFirstStage(Stage *stage) override
    {
        pipelineThread.reset(new QThread);
        pipelineThread->start();

        context = new QObject;
        context->moveToThread(pipelineThread.get());

        bool ok = false;
        Threading::runBlockingFunctor(context, [this, stage, &ok] {
            std::unique_ptr<StreamPipeline> result(new StreamPipeline(true, true, true));

            if (!result->setOutputIngress(getDataSink())) {
                qCDebug(log_component_output_netcdf) << "Failed to set pipeline output:"
                                                     << result->getOutputError();
                return;
            }

            if (!NetCDF::configurePipeline(stage, result.get()))
                return;

            if (!result->start()) {
                qCDebug(log_component_output_netcdf) << "Pipeline start failed";
                return;
            }

            pipeline = std::move(result);
            ok = true;
        });

        return ok;
    }

    void firstStageComplete(Stage *) override
    {
        Q_ASSERT(pipelineThread);
        Q_ASSERT(context);
        Q_ASSERT(pipeline);

        Threading::runQueuedFunctor(context, [this] {
            pipeline->waitInEventLoop();

            return endPipelineThread();
        });

        pipelineThread->wait();
        pipelineThread.reset();
    }

    void firstStageAborted(Stage *) override
    {
        if (!pipelineThread)
            return;

        Q_ASSERT(context);
        Q_ASSERT(pipeline);
        Threading::runQueuedFunctor(context, [this] {
            pipeline->signalTerminate();
            pipeline->waitInEventLoop();

            return endPipelineThread();
        });

        pipelineThread->wait();
        pipelineThread.reset();
    }
};
}

void OutputNetCDF::run()
{
    if (testTerminated())
        return;

    profile = Util::to_lower(std::move(profile));
    if (profile.empty()) {
        qCDebug(log_component_output_netcdf) << "Invalid profile";
        return;
    }

    double startProcessing = Time::time();

    std::unordered_map<SequenceName::Component, ValueSegment::Transfer> stationConfig;
    {
        Archive::Access access;
        Archive::Access::ReadLock lock(access);

        auto allStations = access.availableStations(stations);
        if (allStations.empty()) {
            qCDebug(log_component_output_netcdf) << "No stations available";
            return;
        }
        stations.clear();
        std::copy(allStations.begin(), allStations.end(), Util::back_emplacer(stations));

        for (const auto &station : stations) {
            auto config = ValueSegment::Stream::read(
                    Archive::Selection(start, end, {station}, {"configuration"}, {"netcdf"}),
                    &access);

            bool exists = false;
            for (auto &seg : config) {
                seg.setRoot(Variant::Root(seg.read().hash("Profiles").hash(profile)));
                if (seg.read().exists())
                    exists = true;
            }
            if (!exists)
                continue;

            stationConfig.emplace(station, std::move(config));
        }

        stations.clear();
        for (const auto &cfg : stationConfig) {
            stations.emplace_back(cfg.first);
        }
    }

    if (testTerminated())
        return;

    std::sort(stations.begin(), stations.end());

    qCDebug(log_component_output_netcdf) << "Starting output for" << stations.size()
                                         << "station(s)";

    for (const auto &station : stations) {
        qCDebug(log_component_output_netcdf) << "Generating output for" << station;

        Memory::release_unused();

        feedback.emitStage(
                tr("Output NetCDF for %1").arg(QString::fromStdString(station).toUpper()),
                tr("NetCDF data output is starting up for %1.").arg(
                        QString::fromStdString(station).toUpper()));

        Engine *outputEngine;
        {
            NetCDF config;
            config.start = start;
            config.end = end;
            config.station = station;
            config.config = std::move(stationConfig[station]);

            config.createOutput = [this](const std::string &fileName) {
                if (directory.isEmpty()) {
                    return std::unique_ptr<QFile>(new QFile(QString::fromStdString(fileName)));
                }

                return std::unique_ptr<QFile>(
                        new QFile(QDir(directory).filePath(QString::fromStdString(fileName))));
            };

            outputEngine = new NetCDFEngine(NetCDF::stage(std::move(config)));
        }

        outputEngine->feedback.forward(feedback);
        outputEngine->start();

        {
            std::unique_lock<std::mutex> lock(mutex);
            engine.reset(outputEngine);
            if (terminated) {
                engine.reset();
                lock.unlock();
                outputEngine->signalTerminate();
                outputEngine->wait();
                return;
            }
        }

        outputEngine->wait();

        {
            std::lock_guard<std::mutex> lock(mutex);
            engine.reset();
            if (terminated)
                return;
        }
    }

    double endProcessing = Time::time();
    qCDebug(log_component_output_netcdf) << "Finished output after"
                                         << Logging::elapsed(endProcessing - startProcessing);

    QCoreApplication::processEvents();
}


ComponentOptions OutputNetCDFComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Execution profile"),
                                                tr("This is the profile name to output.  Multiple profiles can be "
                                                   "defined to specify different sets of data that can be output "
                                                   "independently.  Consult the station configuration for available profiles."),
                                                tr("aerosol")));

    options.add("directory",
                new ComponentOptionDirectory(tr("directory", "name"), tr("Output directory"),
                                             tr("This is the directory files will be placed in."),
                                             tr("Current directory")));

    return options;
}

QList<ComponentExample> OutputNetCDFComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Output NetCDF4 data", "default example name"),
                                     tr("This will output data for the default \"aerosol\" profile.")));

    return examples;
}

int OutputNetCDFComponent::actionRequireStations()
{ return 1; }

int OutputNetCDFComponent::actionAllowStations()
{ return INT_MAX; }

bool OutputNetCDFComponent::actionRequiresTime()
{ return true; }

CPD3Action *OutputNetCDFComponent::createTimeAction(const ComponentOptions &options,
                                                    double start,
                                                    double end,
                                                    const std::vector<std::string> &stations)
{ return new OutputNetCDF(options, start, end, stations); }
