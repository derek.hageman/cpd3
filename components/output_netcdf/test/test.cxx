/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <netcdf.h>

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QDir>
#include <QFile>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/actioncomponent.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    ActionComponentTime *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ActionComponentTime *>(ComponentLoader::create("output_netcdf"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("profile")));
        QVERIFY(qobject_cast<ComponentOptionDirectory *>(options.get("directory")));
    }

    void basic()
    {
        QTemporaryFile nc;
        QVERIFY(nc.open());

        Variant::Root cfg;

        cfg["Profiles/aerosol/Filename/Constant"] = nc.fileName();

        cfg["Profiles/aerosol/Dimensions/Time/DataTime/Name/Constant"] = "time";
        cfg["Profiles/aerosol/Dimensions/Time/DataTime/Coordinates/StartTime/Name/Constant"] =
                "time";
        cfg["Profiles/aerosol/Dimensions/Real/CutSize/Name/Constant"] = "cut";
        cfg["Profiles/aerosol/Dimensions/Real/CutSize/ScalarRemove"] = "Missing";
        cfg["Profiles/aerosol/Dimensions/Real/CutSize/Coordinates/Cut/Name/Constant"] = "cut";
        cfg["Profiles/aerosol/Dimensions/Real/Wavelength/Name/Constant"] = "wavelength";
        cfg["Profiles/aerosol/Dimensions/Real/Wavelength/Coordinates/Cut/Name/Constant"] =
                "wavelength";

        cfg["Profiles/aerosol/Variables/Scattering/Data/Input/#0/Variable"] = "Bs[BGR]_S11";
        cfg["Profiles/aerosol/Variables/Scattering/Name/Constant"] = "scattering";
        cfg["Profiles/aerosol/Variables/Scattering/Time/Dimension"] = "DataTime";
        cfg["Profiles/aerosol/Variables/Scattering/Dimensions/#0/Dimension"] = "CutSize";
        cfg["Profiles/aerosol/Variables/Scattering/Dimensions/#0/Values/#0/Match/HasFlavors"] =
                "pm10";
        cfg["Profiles/aerosol/Variables/Scattering/Dimensions/#0/Values/#0/Value"] = 10.0;
        cfg["Profiles/aerosol/Variables/Scattering/Dimensions/#0/Values/#1/Match/HasFlavors"] =
                "pm1";
        cfg["Profiles/aerosol/Variables/Scattering/Dimensions/#0/Values/#1/Value"] = 1.0;
        cfg["Profiles/aerosol/Variables/Scattering/Dimensions/#0/Values/#2/Value"] =
                FP::undefined();
        cfg["Profiles/aerosol/Variables/Scattering/Dimensions/#1/Dimension"] = "Wavelength";
        cfg["Profiles/aerosol/Variables/Scattering/Dimensions/#1/Values/#0/Metadata/Path"] =
                "^Wavelength";

        Variant::Root meta;
        meta.write().metadataReal("Format").setString("0000.00");

        auto metaB = meta;
        metaB.write().metadataReal("Wavelength").setDouble(450);
        auto metaG = meta;
        metaG.write().metadataReal("Wavelength").setDouble(550);
        auto metaR = meta;
        metaR.write().metadataReal("Wavelength").setDouble(700);

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "configuration", "netcdf"}, cfg, FP::undefined(),
                              FP::undefined()),
                SequenceValue({"brw", "raw_meta", "BsB_S11"}, metaB, FP::undefined(),
                              FP::undefined()),
                SequenceValue({"brw", "raw_meta", "BsG_S11"}, metaG, FP::undefined(),
                              FP::undefined()),
                SequenceValue({"brw", "raw_meta", "BsR_S11"}, metaR, FP::undefined(),
                              FP::undefined()),
                SequenceValue({"brw", "raw", "T_S11"}, Variant::Root(25.0), 1388534400, 1388534520),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(1.0), 1388534400,
                              1388534460),
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.1), 1388534400,
                              1388534460),
                SequenceValue({"brw", "raw", "BsR_S11"}, Variant::Root(1.2), 1388534400,
                              1388534460),
                SequenceValue({"alt", "raw", "BsB_S11"}, Variant::Root(99.0), 1388534401,
                              1388534460),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(2.0), 1388534460,
                              1388534520),
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(2.1), 1388534460,
                              1388534520),
                SequenceValue({"brw", "raw", "BsR_S11"}, Variant::Root(2.2), 1388534460,
                              1388534520),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(3.0), 1388534520,
                              1388534580),
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(3.1), 1388534520,
                              1388534580)});


        ComponentOptions options(component->getOptions());
        CPD3Action *action =
                component->createTimeAction(options, 1388534400, 1388620800, {"brw"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait(30000));
        delete action;


        int ncid = -1;
        QVERIFY(::nc_open(nc.fileName().toUtf8(), NC_NOWRITE, &ncid) == NC_NOERR);


        int dimid = -1;
        QVERIFY(::nc_inq_dimid(ncid, "time", &dimid) == NC_NOERR);
        size_t dimlen = 0;
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, (int) 3);

        int varid = -1;
        QVERIFY(::nc_inq_varid(ncid, "time", &varid) == NC_NOERR);
        int ndims = 0;
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);

        std::vector<::size_t> index;
        double value = 0;

        index.resize(1);
        index[0] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, index.data(), &value) == NC_NOERR);
        QCOMPARE(value, 1388534400.0);
        index[0] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, index.data(), &value) == NC_NOERR);
        QCOMPARE(value, 1388534460.0);
        index[0] = 2;
        QVERIFY(::nc_get_var1_double(ncid, varid, index.data(), &value) == NC_NOERR);
        QCOMPARE(value, 1388534520.0);


        QVERIFY(::nc_inq_dimid(ncid, "wavelength", &dimid) == NC_NOERR);
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, (int) 3);

        QVERIFY(::nc_inq_varid(ncid, "wavelength", &varid) == NC_NOERR);
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);

        index.resize(1);
        index[0] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, index.data(), &value) == NC_NOERR);
        QCOMPARE(value, 450.0);
        index[0] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, index.data(), &value) == NC_NOERR);
        QCOMPARE(value, 550.0);
        index[0] = 2;
        QVERIFY(::nc_get_var1_double(ncid, varid, index.data(), &value) == NC_NOERR);
        QCOMPARE(value, 700.0);


        QVERIFY(::nc_inq_dimid(ncid, "cut_size", &dimid) != NC_NOERR);
        QVERIFY(::nc_inq_varid(ncid, "cut_size", &varid) != NC_NOERR);

        QVERIFY(::nc_inq_varid(ncid, "scattering", &varid) == NC_NOERR);
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 2);

        index.resize(2);
        index[1] = 0;
        index[0] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, index.data(), &value) == NC_NOERR);
        QCOMPARE(value, 1.0);
        index[0] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, index.data(), &value) == NC_NOERR);
        QCOMPARE(value, 1.1);
        index[0] = 2;
        QVERIFY(::nc_get_var1_double(ncid, varid, index.data(), &value) == NC_NOERR);
        QCOMPARE(value, 1.2);
        index[1] = 1;
        index[0] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, index.data(), &value) == NC_NOERR);
        QCOMPARE(value, 2.0);
        index[0] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, index.data(), &value) == NC_NOERR);
        QCOMPARE(value, 2.1);
        index[0] = 2;
        QVERIFY(::nc_get_var1_double(ncid, varid, index.data(), &value) == NC_NOERR);
        QCOMPARE(value, 2.2);
        index[1] = 2;
        index[0] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, index.data(), &value) == NC_NOERR);
        QCOMPARE(value, 3.0);
        index[0] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, index.data(), &value) == NC_NOERR);
        QCOMPARE(value, 3.1);
        index[0] = 2;
        QVERIFY(::nc_get_var1_double(ncid, varid, index.data(), &value) == NC_NOERR);
        QCOMPARE(value, NC_FILL_DOUBLE);


        QVERIFY(::nc_close(ncid) == NC_NOERR);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
