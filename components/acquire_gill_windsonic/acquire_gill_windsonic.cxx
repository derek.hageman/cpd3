/*
 * Copyright (c) 2021 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cctype>
#include <QRegularExpression>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "core/csv.hxx"
#include "core/timeutils.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_gill_windsonic.hxx"


using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

AcquireGillWindsonic::Configuration::Configuration() : start(FP::undefined()),
                                                       end(FP::undefined()),
                                                       strictMode(true),
                                                       address("Q")
{ }

AcquireGillWindsonic::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s), end(e), strictMode(other.strictMode), address(other.address)
{ }

void AcquireGillWindsonic::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Gill");
    instrumentMeta["Model"].setString("Windsonic");

    lastRecordTime = FP::undefined();
    autoprobeValidRecords = 0;

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    realtimeStateUpdated = false;

    lastReportedFlags.clear();
}

AcquireGillWindsonic::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s), end(e), strictMode(true), address("Q")
{
    setFromSegment(other);
}

AcquireGillWindsonic::Configuration::Configuration(const Configuration &under,
                                                   const ValueSegment &over,
                                                   double s,
                                                   double e) : start(s),
                                                               end(e),
                                                               strictMode(under.strictMode),
                                                               address(under.address)
{
    setFromSegment(over);
}

void AcquireGillWindsonic::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    if (config["Address"].exists())
        address = config["Address"].toString();
}

AcquireGillWindsonic::AcquireGillWindsonic(const ValueSegment::Transfer &configData,
                                           const std::string &loggingContext) : FramedInstrument(
        "windsonic", loggingContext),
                                                                                lastRecordTime(
                                                                                        FP::undefined()),
                                                                                autoprobeStatus(
                                                                                        AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                autoprobeValidRecords(
                                                                                        0),
                                                                                responseState(
                                                                                        ResponseState::Passive_Initialize)
{
    setDefaultInvalid();
    config.emplace_back();

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireGillWindsonic::setToAutoprobe()
{
    responseState = ResponseState::Autoprobe_Passive_Initialize;
}

void AcquireGillWindsonic::setToInteractive()
{ responseState = ResponseState::Interactive_Initialize; }


ComponentOptions AcquireGillWindsonicComponent::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireGillWindsonic::AcquireGillWindsonic(const ComponentOptions &,
                                           const std::string &loggingContext) : FramedInstrument(
        "windsonic", loggingContext),
                                                                                lastRecordTime(
                                                                                        FP::undefined()),
                                                                                autoprobeStatus(
                                                                                        AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                autoprobeValidRecords(
                                                                                        0),
                                                                                responseState(
                                                                                        ResponseState::Passive_Initialize)
{
    setDefaultInvalid();
    config.emplace_back();
}

AcquireGillWindsonic::~AcquireGillWindsonic() = default;

void AcquireGillWindsonic::invalidateLogValues(double frameTime)
{
    autoprobeValidRecords = 0;
    lastRecordTime = FP::undefined();
    loggingLost(frameTime);
}


SequenceValue::Transfer AcquireGillWindsonic::buildLogMeta(double time) const
{
    Q_ASSERT(!config.empty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_gill_windsonic");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    if (instrumentMeta["SerialNumber"].exists())
        processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    if (instrumentMeta["UnitConfiguration"].exists())
        processing["UnitConfiguration"].set(instrumentMeta["UnitConfiguration"]);

    result.emplace_back(SequenceName({}, "raw_meta", "WS"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("m/s");
    result.back().write().metadataReal("Description").setString("Wind speed");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("Vector2D");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Direction")
          .setString("WD");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Magnitude")
          .setString("WS");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Wind Speed"));

    result.emplace_back(SequenceName({}, "raw_meta", "WD"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Units").setString("degrees");
    result.back().write().metadataReal("Description").setString("Wind direction from true north");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("Vector2D");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Direction")
          .setString("WD");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Magnitude")
          .setString("WS");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Wind Direction"));

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("InsufficientUAxisSamples")
          .hash("Description")
          .setString("Insufficient samples in average period on U axis");
    result.back()
          .write()
          .metadataSingleFlag("InsufficientUAxisSamples")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gill_windsonic");

    result.back()
          .write()
          .metadataSingleFlag("InsufficientVAxisSamples")
          .hash("Description")
          .setString("Insufficient samples in average period on V axis");
    result.back()
          .write()
          .metadataSingleFlag("InsufficientVAxisSamples")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gill_windsonic");

    result.back()
          .write()
          .metadataSingleFlag("NVMChecksumFailed")
          .hash("Description")
          .setString("NVM checksum failed");
    result.back()
          .write()
          .metadataSingleFlag("NVMChecksumFailed")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gill_windsonic");

    result.back()
          .write()
          .metadataSingleFlag("ROMChecksumFailed")
          .hash("Description")
          .setString("ROM checksum failed");
    result.back()
          .write()
          .metadataSingleFlag("ROMChecksumFailed")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gill_windsonic");

    return result;
}

SequenceValue::Transfer AcquireGillWindsonic::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_gill_windsonic");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    if (instrumentMeta["SerialNumber"].exists())
        processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    if (instrumentMeta["UnitConfiguration"].exists())
        processing["UnitConfiguration"].set(instrumentMeta["UnitConfiguration"]);


    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Name").setString(std::string());
    result.back().write().metadataString("Realtime").hash("Units").setString(std::string());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidRecord")
          .setString(QObject::tr("NO COMMS: Invalid record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadSerialNumber")
          .setString(QObject::tr("STARTING COMMS: Reading serial number"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadFirmwareVersion")
          .setString(QObject::tr("STARTING COMMS: Reading firmware version"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetUnits")
          .setString(QObject::tr("STARTING COMMS: Setting wind speed units"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetOutputRate")
          .setString(QObject::tr("STARTING COMMS: Setting output rate"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetCSV")
          .setString(QObject::tr("STARTING COMMS: Setting CSV mode"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetRecordFormat")
          .setString(QObject::tr("STARTING COMMS: Setting record format"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadUnitConfiguration")
          .setString(QObject::tr("STARTING COMMS: Reading unit configuration"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveFirstValid")
          .setString(QObject::tr("STARTING COMMS: Waiting for a valid record"));

    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InsufficientUAxisSamples")
          .setString(QObject::tr("U axis fault"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InsufficientVAxisSamples")
          .setString(QObject::tr("V axis fault"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InsufficientBothAxisSamples")
          .setString(QObject::tr("Both axis fault"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("NVMChecksumFailed")
          .setString(QObject::tr("NVM checksum failed"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("ROMChecksumFailed")
          .setString(QObject::tr("ROM checksum failed"));


    return result;
}

SequenceMatch::Composite AcquireGillWindsonic::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    return sel;
}

void AcquireGillWindsonic::logValue(double startTime,
                                    double endTime,
                                    SequenceName::Component name,
                                    Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireGillWindsonic::realtimeValue(double time,
                                         SequenceName::Component name,
                                         Variant::Root &&value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

static double convertSpeed(double WD, const Util::ByteView &units)
{
    if (!FP::defined(WD))
        return WD;
    if (units.empty())
        return WD;

    switch (units.front()) {
    case 'M':
    case 'm':
        return WD;
    case 'N':
    case 'n':
        return WD * 0.514444;
    case 'P':
    case 'p':
        return WD * 0.44704;
    case 'K':
    case 'k':
        return WD * 0.277778;
    case 'F':
    case 'f':
        return WD * 0.3048;

    default:
        break;
    }

    return WD;
}

int AcquireGillWindsonic::processRecord(const Util::ByteArray &line, double &frameTime)
{
    Q_ASSERT(!config.empty());

    if (line.size() < 3)
        return 1;
    if (line.front() != 0x02)
        return 2;

    auto idx_etx = line.indexOf(0x03);
    if (idx_etx == line.npos)
        return 3;
    if (idx_etx + 1 >= line.size())
        return 4;

    bool ok = false;
    auto reportedChecksum = line.mid(idx_etx + 1).string_trimmed().parse_u32(&ok, 16);
    if (!ok)
        return 5;
    if (reportedChecksum > 0xFFU)
        return 6;

    auto dataContents = line.mid(1, idx_etx - 1);
    if (dataContents.empty())
        return 7;
    std::uint32_t calcChecksum = 0;
    for (auto c : dataContents) {
        calcChecksum ^= static_cast<std::uint32_t>(c);
    }
    if ((calcChecksum & 0xFFU) != reportedChecksum)
        return 8;

    auto fields = Util::as_deque(dataContents.split(','));

    if (fields.empty()) return 10;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (!config.front().address.empty()) {
        if (field.empty())
            return 11;
        if (field != config.front().address)
            return -1;
    }

    if (fields.empty()) return 12;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root WD;
    if (!field.empty()) {
        WD.write().setReal(field.parse_real(&ok));
        if (!ok) return 13;
        if (!FP::defined(WD.read().toReal())) return 14;
    }
    remap("WD", WD);

    if (fields.empty()) return 15;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root WS(field.parse_real(&ok));
    if (!ok) return 16;
    if (!FP::defined(WS.read().toReal())) return 17;
    remap("WSRAW", WS);

    if (fields.empty()) return 18;
    field = fields.front().string_trimmed();
    fields.pop_front();
    WS.write().setReal(convertSpeed(WS.read().toReal(), field));
    remap("WS", WS);

    if (fields.empty()) return 19;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root flags(static_cast<std::int_fast64_t>(field.parse_u16(&ok, 16)));
    if (!ok) return 20;
    if (!INTEGER::defined(flags.read().toInteger())) return 21;
    remap("FRAW", flags);
    {
        auto prior = std::move(lastReportedFlags);
        lastReportedFlags.clear();
        switch (flags.read().toInteger()) {
        case 0x01:
            lastReportedFlags.insert("InsufficientUAxisSamples");
            break;
        case 0x02:
            lastReportedFlags.insert("InsufficientVAxisSamples");
            break;
        case 0x04:
            lastReportedFlags.insert("InsufficientUAxisSamples");
            lastReportedFlags.insert("InsufficientVAxisSamples");
            break;
        case 0x08:
            lastReportedFlags.insert("NVMChecksumFailed");
            break;
        case 0x09:
            lastReportedFlags.insert("ROMChecksumFailed");
            break;
        default:
            break;
        }
        if (prior != lastReportedFlags)
            realtimeStateUpdated = true;
    }

    if (!fields.empty()) {
        if (fields.front().empty()) {
            fields.pop_front();
        }
    }
    if (!fields.empty()) {
        if (config.front().strictMode)
            return 999;
    }

    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;

    if (!FP::defined(startTime) || !FP::defined(endTime))
        return 0;

    if (!haveEmittedLogMeta && loggingEgress) {
        haveEmittedLogMeta = true;

        loggingEgress->incomingData(buildLogMeta(startTime));
    }

    if (!haveEmittedRealtimeMeta && realtimeEgress) {
        haveEmittedRealtimeMeta = true;

        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    logValue(startTime, endTime, "F1", Variant::Root(lastReportedFlags));
    logValue(startTime, endTime, "WS", std::move(WS));
    if (WD.read().exists()) {
        logValue(startTime, endTime, "WD", std::move(WD));
    }

    if (realtimeStateUpdated && realtimeEgress && FP::defined(frameTime)) {
        realtimeStateUpdated = false;

        auto reportFlags = lastReportedFlags;

        if (!reportFlags.empty()) {
            if (lastReportedFlags.size() == 1) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(*reportFlags.begin()),
                                      frameTime, FP::undefined()));
            } else {
                if (reportFlags.size() == 2 &&
                        reportFlags.count("InsufficientUAxisSamples") &&
                        reportFlags.count("InsufficientVAxisSamples")) {
                    realtimeEgress->incomingData(
                            SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(""), frameTime,
                                          FP::undefined()));
                } else {
                    QStringList sorted;
                    for (const auto &add : reportFlags) {
                        sorted.append(QString::fromStdString(add));
                    }
                    std::sort(sorted.begin(), sorted.end());
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            QString("Status: %1").arg(sorted.join(','))), frameTime,
                                                               FP::undefined()));
                }
            }
        } else {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                  FP::undefined()));
        }
    }

    return 0;
}

void AcquireGillWindsonic::configurationAdvance(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.empty());
}

void AcquireGillWindsonic::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    configurationAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Passive_Run: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + 5.0);
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            timeoutAt(FP::undefined());
            responseState = ResponseState::Passive_Wait;
            generalStatusUpdated();

            info.hash("Code").setInteger(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case ResponseState::Unpolled_Run: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + 5.0);
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("UnpolledRun");
            timeoutAt(FP::undefined());
            if (controlStream) {
                controlStream->writeControl("\r\n*" + config.front().address + "\r");
            }
            responseState = ResponseState::Interactive_Start_Flush;
            discardData(frameTime + 5.0);
            info.hash("ResponseState").setString("Run");
            generalStatusUpdated();

            info.hash("Code").setInteger(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }


    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 5) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                timeoutAt(frameTime + 5.0);

                responseState = ResponseState::Passive_Run;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                realtimeStateUpdated = true;

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case ResponseState::Passive_Initialize:
    case ResponseState::Passive_Wait: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);
            responseState = ResponseState::Passive_Run;
            generalStatusUpdated();

            ++autoprobeValidRecords;
            timeoutAt(frameTime + 5.0);

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case ResponseState::Interactive_Start_ReadSerialNumber: {
        auto line = Util::ByteView(frame).string_trimmed();
        if (line == "D1") {
            break;
        }

        if (line.size() <= 3) {
            qCDebug(log) << "Invalid serial number response" << frame << "during startup at"
                         << Logging::time(frameTime);

            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            break;
        }

        Variant::Root oldValue(instrumentMeta["SerialNumber"]);
        bool ok = false;
        std::uint_fast64_t i64;
        if (line.size() > 1 && !std::isdigit(line.front())) {
            i64 = line.mid(1).parse_i64(&ok, 10);
        } else {
            i64 = line.parse_i64(&ok, 10);
        }
        if (ok && INTEGER::defined(i64) && i64 > 0) {
            instrumentMeta["SerialNumber"].setInteger(i64);
        } else {
            instrumentMeta["SerialNumber"].setString(line.toString());
        }
        if (oldValue.read() != instrumentMeta["SerialNumber"]) {
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 2.0);
        break;
    }
    case ResponseState::Interactive_Start_ReadFirmwareVersion: {
        auto line = Util::ByteView(frame).string_trimmed();
        if (line == "D2") {
            break;
        }

        if (line.size() < 4) {
            qCDebug(log) << "Invalid firmware version response" << frame << "during startup at"
                         << Logging::time(frameTime);

            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            break;
        }

        Variant::Root oldValue(instrumentMeta["FirmwareVersion"]);
        instrumentMeta["SerialNumber"].setString(line.toString());
        if (oldValue.read() != instrumentMeta["FirmwareVersion"]) {
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 2.0);
        break;
    }
    case ResponseState::Interactive_Start_SetUnits: {
        if (Util::ByteView(frame).string_trimmed() != "U1") {
            qCDebug(log) << "Invalid set units response" << frame << "during startup at"
                         << Logging::time(frameTime);

            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            break;
        }

        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 2.0);
        break;
    }
    case ResponseState::Interactive_Start_SetOutputRate: {
        if (Util::ByteView(frame).string_trimmed() != "P1") {
            qCDebug(log) << "Invalid set output rate response" << frame << "during startup at"
                         << Logging::time(frameTime);

            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            break;
        }

        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 2.0);
        break;
    }
    case ResponseState::Interactive_Start_SetCSV: {
        if (Util::ByteView(frame).string_trimmed() != "O1") {
            qCDebug(log) << "Invalid set CSV response" << frame << "during startup at"
                         << Logging::time(frameTime);

            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            break;
        }

        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 2.0);
        break;
    }
    case ResponseState::Interactive_Start_RecordFormat: {
        if (Util::ByteView(frame).string_trimmed() != "M2") {
            qCDebug(log) << "Invalid set CSV response" << frame << "during startup at"
                         << Logging::time(frameTime);

            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            break;
        }

        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 2.0);
        break;
    }
    case ResponseState::Interactive_Start_ReadUnitConfiguration: {
        auto line = Util::ByteView(frame).string_trimmed();
        if (line == "D3") {
            break;
        }

        if (line.size() <= 5 || line.indexOf(',') == line.npos) {
            qCDebug(log) << "Invalid unit configuration" << frame << "during startup at"
                         << Logging::time(frameTime);

            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            break;
        }

        Variant::Root oldValue(instrumentMeta["UnitConfiguration"]);
        instrumentMeta["UnitConfiguration"].setString(line.toString());
        if (oldValue.read() != instrumentMeta["UnitConfiguration"]) {
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 2.0);
        break;
    }

    case ResponseState::Interactive_Start_Unpolled_FirstValid: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + 5.0);

            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            responseState = ResponseState::Unpolled_Run;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            responseState = ResponseState::Interactive_Restart_Wait;
            if (controlStream)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            info.hash("Code").setInteger(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }
    default:
        break;
    }
}

void AcquireGillWindsonic::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_DisableReports:
    case ResponseState::Interactive_Start_ReadSerialNumber:
    case ResponseState::Interactive_Start_ReadFirmwareVersion:
    case ResponseState::Interactive_Start_SetUnits:
    case ResponseState::Interactive_Start_SetOutputRate:
    case ResponseState::Interactive_Start_SetCSV:
    case ResponseState::Interactive_Start_RecordFormat:
    case ResponseState::Interactive_Start_ReadUnitConfiguration:
    case ResponseState::Interactive_Start_Unpolled_FirstValid:
        qCDebug(log) << "Timeout in interactive start state" << static_cast<int>(responseState)
                     << "at" << Logging::time(frameTime);

        invalidateLogValues(frameTime);

        responseState = ResponseState::Interactive_Restart_Wait;
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);
        autoprobeValidRecords = 0;
        generalStatusUpdated();

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        break;

    case ResponseState::Passive_Run: {
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = ResponseState::Passive_Wait;
        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;
    }

    case ResponseState::Unpolled_Run: {
        qCDebug(log) << "Timeout in unpolled mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 5.0);
        if (controlStream) {
            controlStream->writeControl("\r\n*" + config.front().address + "\r");
        }

        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("UnpolledRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;
    }

    case ResponseState::Passive_Initialize:
    case ResponseState::Passive_Wait:
        responseState = ResponseState::Passive_Wait;
        autoprobeValidRecords = 0;
        invalidateLogValues(frameTime);
        break;

    case ResponseState::Autoprobe_Passive_Wait: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case ResponseState::Autoprobe_Passive_Initialize:
        responseState = ResponseState::Autoprobe_Passive_Wait;
        autoprobeValidRecords = 0;
        invalidateLogValues(frameTime);
        break;

    case ResponseState::Interactive_Restart_Wait:
    case ResponseState::Interactive_Initialize:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 5.0);
        if (controlStream) {
            controlStream->writeControl("\r\n*" + config.front().address + "\r");
        }

        generalStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;
    }
}

void AcquireGillWindsonic::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Interactive_Start_Flush:
        timeoutAt(frameTime + 15.0);
        if (controlStream) {
            controlStream->writeControl("*\r");
        }
        responseState = ResponseState::Interactive_Start_DisableReports;
        discardData(frameTime + 5.0);
        generalStatusUpdated();
        break;

    case ResponseState::Interactive_Start_DisableReports:
        responseState = ResponseState::Interactive_Start_ReadSerialNumber;
        timeoutAt(frameTime + 5.0);

        if (controlStream) {
            controlStream->writeControl("D1\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadSerialNumber"), frameTime, FP::undefined()));
        }
        break;

    case ResponseState::Interactive_Start_ReadSerialNumber:
        responseState = ResponseState::Interactive_Start_ReadFirmwareVersion;
        timeoutAt(frameTime + 5.0);
        if (controlStream) {
            controlStream->writeControl("D2\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadFirmwareVersion"), frameTime, FP::undefined()));
        }
        break;
    case ResponseState::Interactive_Start_ReadFirmwareVersion:
        responseState = ResponseState::Interactive_Start_SetUnits;
        timeoutAt(frameTime + 5.0);
        if (controlStream) {
            controlStream->writeControl("U1\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetUnits"),
                                  frameTime, FP::undefined()));
        }
        break;
    case ResponseState::Interactive_Start_SetUnits:
        responseState = ResponseState::Interactive_Start_SetOutputRate;
        timeoutAt(frameTime + 5.0);
        if (controlStream) {
            controlStream->writeControl("P1\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetOutputRate"), frameTime, FP::undefined()));
        }
        break;
    case ResponseState::Interactive_Start_SetOutputRate:
        responseState = ResponseState::Interactive_Start_SetCSV;
        timeoutAt(frameTime + 5.0);
        if (controlStream) {
            controlStream->writeControl("O1\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetCSV"),
                                  frameTime, FP::undefined()));
        }
        break;
    case ResponseState::Interactive_Start_SetCSV:
        responseState = ResponseState::Interactive_Start_RecordFormat;
        timeoutAt(frameTime + 5.0);
        if (controlStream) {
            controlStream->writeControl("M2\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetRecordFormat"), frameTime, FP::undefined()));
        }
        break;
    case ResponseState::Interactive_Start_RecordFormat:
        responseState = ResponseState::Interactive_Start_ReadUnitConfiguration;
        timeoutAt(frameTime + 5.0);
        if (controlStream) {
            controlStream->writeControl("D3\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadUnitConfiguration"), frameTime, FP::undefined()));
        }
        break;
    case ResponseState::Interactive_Start_ReadUnitConfiguration:
        responseState = ResponseState::Interactive_Start_Unpolled_FirstValid;
        discardData(frameTime + 5.0, 1);
        timeoutAt(frameTime + 15.0);

        if (controlStream) {
            controlStream->writeControl("Q\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveFirstValid"),
                                                       frameTime, FP::undefined()));
        }
        break;

    case ResponseState::Interactive_Restart_Wait:
    case ResponseState::Interactive_Initialize:
        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 5.0);
        if (controlStream) {
            controlStream->writeControl("\r\n*" + config.front().address + "\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    default:
        break;
    }
}

void AcquireGillWindsonic::incomingControlFrame(const Util::ByteArray &, double)
{ }


Variant::Root AcquireGillWindsonic::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireGillWindsonic::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case ResponseState::Passive_Run:
    case ResponseState::Unpolled_Run:
        return AcquisitionInterface::GeneralStatus::Normal;
        break;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
        break;
    }
}

AcquisitionInterface::AutoprobeStatus AcquireGillWindsonic::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireGillWindsonic::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case ResponseState::Unpolled_Run:
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << static_cast<int>(responseState) << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_DisableReports:
    case ResponseState::Interactive_Start_ReadSerialNumber:
    case ResponseState::Interactive_Start_ReadFirmwareVersion:
    case ResponseState::Interactive_Start_SetUnits:
    case ResponseState::Interactive_Start_SetOutputRate:
    case ResponseState::Interactive_Start_SetCSV:
    case ResponseState::Interactive_Start_RecordFormat:
    case ResponseState::Interactive_Start_ReadUnitConfiguration:
    case ResponseState::Interactive_Start_Unpolled_FirstValid:
    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        qCDebug(log) << "Interactive autoprobe started from state"
                     << static_cast<int>(responseState) << "at" << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 10.0);
        discardData(time + 5.0);
        if (controlStream) {
            controlStream->writeControl("\r\n*" + config.front().address + "\r");
        }

        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireGillWindsonic::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    Q_ASSERT(!config.empty());
    discardData(time + 0.5, 1);
    timeoutAt(time + 4.0 * 6 + 3.0);

    qCDebug(log) << "Reset to passive autoprobe from state" << static_cast<int>(responseState)
                 << "at" << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = ResponseState::Autoprobe_Passive_Wait;
    autoprobeValidRecords = 0;
    generalStatusUpdated();
}

void AcquireGillWindsonic::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 2.0);

    switch (responseState) {
    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_DisableReports:
    case ResponseState::Interactive_Start_ReadSerialNumber:
    case ResponseState::Interactive_Start_ReadFirmwareVersion:
    case ResponseState::Interactive_Start_SetUnits:
    case ResponseState::Interactive_Start_SetOutputRate:
    case ResponseState::Interactive_Start_SetCSV:
    case ResponseState::Interactive_Start_RecordFormat:
    case ResponseState::Interactive_Start_ReadUnitConfiguration:
    case ResponseState::Interactive_Start_Unpolled_FirstValid:
    case ResponseState::Unpolled_Run:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << static_cast<int>(responseState)
                     << "to interactive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        qCDebug(log) << "Promoted from passive state" << static_cast<int>(responseState)
                     << "to interactive acquisition at" << Logging::time(time);

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 10.0);
        discardData(time + 5.0);
        if (controlStream) {
            controlStream->writeControl("\r\n*" + config.front().address + "\r");
        }

        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireGillWindsonic::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    Q_ASSERT(!config.empty());
    timeoutAt(time + 10.0);

    switch (responseState) {
    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from autoprobe passive state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        responseState = ResponseState::Passive_Initialize;
        break;

    case ResponseState::Unpolled_Run:
    case ResponseState::Interactive_Start_Unpolled_FirstValid:
        discardData(FP::undefined());

        responseState = ResponseState::Passive_Run;
        generalStatusUpdated();

        /* Already in unpolled, so do nothing */
        qCDebug(log) << "Promoted from unpolled state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_DisableReports:
    case ResponseState::Interactive_Start_ReadSerialNumber:
    case ResponseState::Interactive_Start_ReadFirmwareVersion:
    case ResponseState::Interactive_Start_SetUnits:
    case ResponseState::Interactive_Start_SetOutputRate:
    case ResponseState::Interactive_Start_SetCSV:
    case ResponseState::Interactive_Start_RecordFormat:
    case ResponseState::Interactive_Start_ReadUnitConfiguration:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        /* We didn't have full communications, so just drop it and start
         * over. */
        qCDebug(log) << "Promoted from state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);

        discardData(FP::undefined());

        responseState = ResponseState::Passive_Initialize;
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireGillWindsonic::getDefaults()
{
    AutomaticDefaults result;
    result.name = "XM$2";
    result.setSerialN81(9600);
    return result;
}


ComponentOptions AcquireGillWindsonicComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);
    return options;
}

ComponentOptions AcquireGillWindsonicComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireGillWindsonicComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples(true));
    examples.append(getPassiveExamples());
    return examples;
}

QList<ComponentExample> AcquireGillWindsonicComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data")));

    return examples;
}

bool AcquireGillWindsonicComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireGillWindsonicComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

std::unique_ptr<
        AcquisitionInterface> AcquireGillWindsonicComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                      const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireGillWindsonic(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireGillWindsonicComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                      const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireGillWindsonic(config, loggingContext)); }

std::unique_ptr<AcquisitionInterface> AcquireGillWindsonicComponent::createAcquisitionAutoprobe(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireGillWindsonic> i(new AcquireGillWindsonic(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireGillWindsonicComponent::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireGillWindsonic> i(new AcquireGillWindsonic(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
