/*
 * Copyright (c) 2021 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <array>
#include <cstdint>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;

    char address;
    double WS;
    double WD;

    ModelInstrument() : incoming(), outgoing(), unpolledRemaining(0.0), address('Q')
    {
        WS = 1.5;
        WD = 120.0;
    }

    static std::uint8_t checksum(const QByteArray &data)
    {
        std::uint8_t cs = 0;
        for (auto c : data) {
            cs ^= static_cast<std::uint8_t>(c);
        }
        return cs;
    }

    void unpolledResponse()
    {
        QByteArray data;

        data += address;
        data += ",";
        data += QByteArray::number(WD, 'f', 0);
        data += ",";
        data += QByteArray::number(WS, 'f', 2).rightJustified(5, '0');
        data += ",M,00,";

        auto cs = checksum(data);

        outgoing += static_cast<char>(0x02);
        outgoing += data;
        outgoing += static_cast<char>(0x03);
        outgoing += QByteArray::number(static_cast<int>(cs), 16).rightJustified(2, '0');
        outgoing += "\r\n";
    }

    void advance(double seconds)
    {
        if (FP::defined(unpolledRemaining)) {
            unpolledRemaining -= seconds;
            while (unpolledRemaining < 0.0) {
                unpolledRemaining += 1.0;
                unpolledResponse();
            }
        }

        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR).trimmed());

            incoming = incoming.mid(idxCR + 1);

            if (line.startsWith('*')) {
                unpolledRemaining = FP::undefined();
            } else if (line == "D1") {
                outgoing += "D1\r\nY16120001\r\n";
            } else if (line == "D2") {
                outgoing += "D2\r\n2368-110-01\r\n";
            } else if (line == "D3") {
                outgoing += "D3\r\nM2,U1,O1,L1,P1,B3,H1,NQ,F1,E2,T1,S4,C2,G0,K50,\r\n";
            } else if (line == "Q") {
                outgoing += "WINDSONIC (Gill Instruments Ltd)\n"
                            "2368-110-01\n"
                            "RS232 (CFG)\n"
                            "CHECKSUM ROM:E6D1 E6D1 *PASS*\n"
                            "CHECKSUM FAC:09EA 09EA *PASS*\n"
                            "CHECKSUM ENG:17FB 17FB *PASS*\n"
                            "CHECKSUM CAL:CC55 CC55 *PASS*";
                unpolledRemaining = 0.0;
            } else if (line.startsWith('U') ||
                    line.startsWith('P') ||
                    line.startsWith('O') ||
                    line.startsWith('M')) {
                outgoing += line + "\r\n";
            } else {
                outgoing += "ERROR\r\n";
            }
        }
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("WS", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("WD", Variant::Root(), {}, time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZSTATE", Variant::Root(), {}, time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("WS"))
            return false;
        if (!stream.checkContiguous("WD"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("WS", Variant::Root(model.WS), time))
            return false;
        if (!stream.hasAnyMatchingValue("WD", Variant::Root(model.WD), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_gill_windsonic"));
        QVERIFY(component);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.25);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(0.5);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));
        control.advance(0.1);

        for (int i = 0; i < 50; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));

        for (int i = 0; i < 50; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
    }

};

QTEST_MAIN(TestComponent)

#include "test.moc"
