/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREABROOKSPID0254_H
#define ACQUIREABROOKSPID0254_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QSet>
#include <QString>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class AcquireBrooksPID0254 : public CPD3::Acquisition::FramedInstrument {
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum {
        /* Passive autoprobe initialization, ignoring the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,
        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,

        /* Acquiring data in interactive mode, reading process values */
        RESP_INTERACTIVE_RUN_READ_PROCESS,
        /* Acquiring data in interactive mode, reading setpoint values */
                RESP_INTERACTIVE_RUN_READ_SETPOINTS,
        /* Acquiring data in interactive mode, sleeping until the next
         * time to query. */
                RESP_INTERACTIVE_RUN_WAIT,

        /* Starting communications, waiting for the reset to complete */
                RESP_INTERACTIVE_START_RESET,
        /* Starting communications, waiting for the AZS (pseudo-XON) to 
         * complete */
                RESP_INTERACTIVE_START_ENABLE_SEND,
        /* Starting communications, reading the identification */
                RESP_INTERACTIVE_START_IDENTIFY,
        /* Starting communications, reading out the channel parameters */
                RESP_INTERACTIVE_START_READ_PARAMETERS,
        /* Starting communications, reading all setpoints */
                RESP_INTERACTIVE_START_READ_SETPOINTS,

        /* Waiting before attempting a communications restart */
                RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
                RESP_INTERACTIVE_INITIALIZE,
    } responseState;

    enum CommandType {
        /* A specifically invalid command */
                COMMAND_INVALID = 0,

        /* AZM - Show menu, we handle this by discarding until we get a
         * timeout */
                COMMAND_MENU,

        /* AZV - View parameters, inspect lines but expect a timeout */
                COMMAND_VIEW,

        /* AZI - Read instrument identification */
                COMMAND_IDENTIFY,

        /* AZK - Read measured input parameters */
                COMMAND_READ_MEASURED,

        /* AZPzz? - Read a single parameter */
                COMMAND_READ_PARAMETER,

        /* AZPzz= - Set a single parameter */
                COMMAND_SET_PARAMETER,
    };

    enum {
        TotalChannels = 4,
    };

    friend class Command;

    class Command {
        CommandType type;
        CPD3::Util::ByteArray packet;
        int port;
        int state;
    public:
        enum {
            MultiPort_Begin = 1, MultiPort_End = MultiPort_Begin + TotalChannels,

            ExpectedTimeout_Begin, ExpectedTimeout_Wait,
        };

        Command();

        Command(CommandType t, CPD3::Util::ByteArray p = {}, int prt = -1, int st = 0);

        Command(CommandType t, const char *str, int prt = -1, int st = 0);

        Command(const Command &other);

        Command &operator=(const Command &other);

        inline CommandType getType() const
        { return type; }

        inline CPD3::Util::ByteArray getPacket() const
        { return packet; }

        inline int getPort() const
        { return port; }

        inline int getState() const
        { return state; }

        inline void setState(int s)
        { state = s; }

        inline void invalidate()
        { type = COMMAND_INVALID; };

        CPD3::Data::Variant::Root stateDescription() const;
    };

    Command issuedCommand;
    QList<Command> commandBacklog;
    int commandAttempt;

    enum LogStreamID {
        LogStream_Metadata = 0, LogStream_State,

        LogStream_PV_Begin, LogStream_PV_End = LogStream_PV_Begin + TotalChannels - 1,

        LogStream_TOTAL, LogStream_RecordBaseStart = LogStream_PV_Begin,
    };
    CPD3::Data::StaticMultiplexer loggingMux;
    quint32 streamAge[LogStream_TOTAL];
    double streamTime[LogStream_TOTAL];
    CPD3::Data::Variant::Root lastInputs;

    std::unordered_map<CPD3::Util::ByteArray, Command> incomingCommandLookup;

    struct OutputVariable {
        CPD3::Data::SequenceName::Component name;
        CPD3::Data::Variant::Root metadata;

        int channel;
        CPD3::Calibration calibration;
    };

    class Configuration {
        double start;
        double end;

        void addVariable(const CPD3::Data::Variant::Read &config,
                         qint64 defaultChannel = CPD3::INTEGER::undefined(),
                         const CPD3::Data::SequenceName::Component &defaultName = {});

        void setSetpointValues(const CPD3::Data::Variant::Read &config, QMap<int, double> &target);

    public:
        int address;
        double pollInterval;
        bool strictMode;

        bool logOutputs;

        std::unordered_map<CPD3::Data::SequenceName::Component, int> setpointNames;

        QMap<int, double> initializeSetpoint;
        QMap<int, double> exitSetpoint;
        QMap<int, double> bypassSetpoint;
        QMap<int, double> unbypassSetpoint;

        QHash<int, QList<OutputVariable> > variables;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    QMap<int, double> setpointUpdate;

    QMap<int, double> setpointTarget;

    CPD3::Data::Variant::Root channelMetadata;

    CPD3::Data::SequenceValue setpointOutputValues;

    CPD3::Data::Variant::Root instrumentMeta;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;

    void setDefaultInvalid();

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void invalidateLogValues(double frameTime);

    void logValues(double frameTime,
                   int streamID, CPD3::Data::SequenceValue::Transfer &&values);

    void updateLogMeta(double frameTime);

    void describeState(CPD3::Data::Variant::Write &info) const;

    void initiateInteractiveStart(double frameTime);

    void frameRejected(const CPD3::Util::ByteView &frame, double frameTime, int code);

    bool verifyChecksum(const CPD3::Util::ByteView &frame, double frameTime);

    void configurationChanged();

    void configurationAdvance(double frameTime);

    void packageAndSend(const CPD3::Util::ByteView &data, int port = -1);

    void sendCommand(const Command &command, double frameTime = CPD3::FP::undefined());

    bool advanceSetpointChange(double frameTime, int channel = -1);

    bool advanceSetpointRead(double frameTime, int channel = -1);

    bool advanceProcessRead(double frameTime, int channel = -1);

    bool handleExpectedTimeout(double frameTime, bool interactionPossible);

    int convertSetpointChannel(const CPD3::Data::Variant::Read &value);

    QHash<int, double> convertSetpointMultiple(const CPD3::Data::Variant::Read &value);

    void genericOutputCommand(const CPD3::Data::Variant::Read &command);

public:
    AcquireBrooksPID0254(const CPD3::Data::ValueSegment::Transfer &config,
                         const std::string &loggingContext);

    AcquireBrooksPID0254(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireBrooksPID0254();

    CPD3::Data::SequenceValue::Transfer getPersistentValues() override;

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus() override;

    CPD3::Data::Variant::Root getCommands() override;

    CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables() override;

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    void command(const CPD3::Data::Variant::Read &value) override;

    std::size_t dataFrameStart(std::size_t offset,
                               const CPD3::Util::ByteArray &input) const override;

    std::size_t dataFrameEnd(std::size_t start, const CPD3::Util::ByteArray &input) const override;

    std::size_t controlFrameStart(std::size_t offset,
                                  const CPD3::Util::ByteArray &input) const override;

    std::size_t controlFrameEnd(std::size_t start,
                                const CPD3::Util::ByteArray &input) const override;

    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;

    void shutdownInProgress(double time) override;
};

class AcquireBrooksPID0254Component
        : public QObject, virtual public CPD3::Acquisition::AcquisitionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_brooks_pid0254"
                              FILE
                              "acquire_brooks_pid0254.json")

    CPD3::ComponentOptions getBaseOptions();

public:

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
