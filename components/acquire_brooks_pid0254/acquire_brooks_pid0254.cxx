/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_brooks_pid0254.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

AcquireBrooksPID0254::Configuration::Configuration() : start(FP::undefined()),
                                                       end(FP::undefined()),
                                                       address(-1),
                                                       pollInterval(1.0),
                                                       strictMode(true),
                                                       logOutputs(false),
                                                       setpointNames(),
                                                       initializeSetpoint(),
                                                       exitSetpoint(),
                                                       bypassSetpoint(),
                                                       unbypassSetpoint(),
                                                       variables()
{ }

AcquireBrooksPID0254::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          address(other.address),
          pollInterval(other.pollInterval),
          strictMode(other.strictMode),
          logOutputs(other.logOutputs),
          setpointNames(other.setpointNames),
          initializeSetpoint(other.initializeSetpoint),
          exitSetpoint(other.exitSetpoint),
          bypassSetpoint(other.bypassSetpoint),
          unbypassSetpoint(other.unbypassSetpoint),
          variables(other.variables)
{ }

void AcquireBrooksPID0254::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Brooks");
    instrumentMeta["Model"].setString("0254");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    setpointOutputValues.setUnit(SequenceName({}, "raw", "ZOUTPUTS"));

    memset(streamAge, 0, sizeof(streamAge));
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
    }
    lastInputs.write().setEmpty();
}

AcquireBrooksPID0254::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          address(-1),
          pollInterval(1.0),
          strictMode(true),
          logOutputs(false),
          setpointNames(),
          initializeSetpoint(),
          exitSetpoint(),
          bypassSetpoint(),
          unbypassSetpoint(),
          variables()
{
    setFromSegment(other);
}

AcquireBrooksPID0254::Configuration::Configuration(const Configuration &under,
                                                   const ValueSegment &over,
                                                   double s,
                                                   double e) : start(s),
                                                               end(e),
                                                               address(under.address),
                                                               pollInterval(under.pollInterval),
                                                               strictMode(under.strictMode),
                                                               logOutputs(under.logOutputs),
                                                               setpointNames(under.setpointNames),
                                                               initializeSetpoint(
                                                                       under.initializeSetpoint),
                                                               exitSetpoint(under.exitSetpoint),
                                                               bypassSetpoint(under.bypassSetpoint),
                                                               unbypassSetpoint(
                                                                       under.unbypassSetpoint),
                                                               variables(under.variables)
{
    setFromSegment(over);
}

void AcquireBrooksPID0254::Configuration::addVariable(const Variant::Read &config,
                                                      qint64 defaultChannel,
                                                      const SequenceName::Component &defaultName)
{
    qint64 channel = config["Channel"].toInt64();
    if (!INTEGER::defined(channel)) {
        channel = defaultChannel;
    }
    if (channel < 1 || channel > TotalChannels)
        return;
    channel--;

    auto name = config["Name"].toString();
    if (name.empty())
        name = defaultName;
    if (name.empty())
        return;

    OutputVariable variable;
    variable.channel = (int) channel;
    variable.name = std::move(name);
    variable.metadata.write().set(config["Metadata"]);
    variable.calibration = Variant::Composite::toCalibration(config["Calibration"]);

    variables[variable.channel].append(std::move(variable));
}

void AcquireBrooksPID0254::Configuration::setSetpointValues(const Variant::Read &config,
                                                            QMap<int, double> &target)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        QStringList channels(config.toQString().split(QRegExp("[:;,]+")));
        for (int idx = 0, max = std::min<int>(channels.size(), TotalChannels); idx < max; ++idx) {
            bool ok = false;
            double value = channels.at(idx).toDouble(&ok);
            if (!ok)
                continue;
            if (!FP::defined(value))
                target.remove(idx);
            else
                target.insert(idx, value);
        }
        break;
    }
    default: {
        auto children = config.toChildren();
        for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
            int index = add.integerKey();
            auto check = add.stringKey();
            if (!check.empty()) {
                index = -1;
                auto lookup = setpointNames.find(check);
                if (lookup != setpointNames.end())
                    index = lookup->second;
                if (index == -1) {
                    bool ok = false;
                    index = QString::fromStdString(check).toInt(&ok);
                    if (!ok)
                        index = -1;
                    else
                        index = index - 1;
                }
                if (index < 1 || index > TotalChannels)
                    continue;
                --index;
            } else {
                if (index < 0 || index >= TotalChannels)
                    continue;
            }

            double value = Variant::Composite::toNumber(add.value());
            if (!FP::defined(value))
                target.remove(index);
            else
                target.insert(index, value);
        }
        break;
    }
    }
}

void AcquireBrooksPID0254::Configuration::setFromSegment(const ValueSegment &config)
{
    if (INTEGER::defined(config["Address"].toInt64())) {
        address = (int) config["Address"].toInt64();
        if (address < 0 || address > 65535)
            address = -1;
    }
    if (config["PollInterval"].exists()) {
        pollInterval = config["PollInterval"].toDouble();
        if (FP::defined(pollInterval) && pollInterval <= 0.0)
            pollInterval = FP::undefined();
    }
    if (config["StrictMode"].exists()) {
        strictMode = config["StrictMode"].toBool();
    }
    if (config["LogOutputs"].exists()) {
        logOutputs = config["LogOutputs"].toBool();
    }

    if (config["Variables"].exists()) {
        variables.clear();
        switch (config["Variables"].getType()) {
        case Variant::Type::String: {
            QStringList names(config["Variables"].toQString()
                                                 .split(QRegExp("[\\s+;:,]"),
                                                        QString::SkipEmptyParts));
            for (int idx = 0, max = names.size(); idx < max; ++idx) {
                if (idx < TotalChannels) {
                    addVariable(Variant::Read::empty(), idx + 1, names.at(idx).toStdString());
                } else {
                    addVariable(Variant::Read::empty(), INTEGER::undefined(),
                                names.at(idx).toStdString());
                }
            }
            break;
        }
        default: {
            auto children = config["Variables"].toChildren();
            for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
                bool ok = false;
                qint64 channel = add.integerKey() + 1;
                {
                    auto check = add.stringKey();
                    if (!check.empty()) {
                        channel = QString::fromStdString(add.stringKey()).toLongLong(&ok);
                        if (!ok)
                            channel = INTEGER::undefined();
                    }
                }
                addVariable(add.value(), channel, add.stringKey());
            }
            break;
        }
        }
    }

    if (config["SetpointOutputs"].exists()) {
        setpointNames.clear();
        for (auto add : config["SetpointOutputs"].toHash()) {
            if (add.first.empty())
                continue;
            qint64 channel = INTEGER::undefined();
            switch (add.second.getType()) {
            case Variant::Type::String: {
                bool ok = false;
                channel = add.second.toQString().toLongLong(&ok);
                if (!ok)
                    channel = INTEGER::undefined();
                break;
            }
            default:
                channel = add.second.toInt64();
                break;
            }
            if (channel < 1 || channel > TotalChannels)
                continue;
            --channel;

            setpointNames[add.first] = (int) channel;
        }
    }

    setSetpointValues(config["Initialize/Setpoint"], initializeSetpoint);
    setSetpointValues(config["Exit/Setpoint"], exitSetpoint);
    setSetpointValues(config["Bypass/Setpoint"], bypassSetpoint);
    setSetpointValues(config["UnBypass/Setpoint"], exitSetpoint);
}

AcquireBrooksPID0254::AcquireBrooksPID0254(const ValueSegment::Transfer &configData,
                                           const std::string &loggingContext) : FramedInstrument(
        "brooks0254", loggingContext),
                                                                                autoprobeStatus(
                                                                                        AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                responseState(
                                                                                        RESP_PASSIVE_WAIT),
                                                                                issuedCommand(),
                                                                                commandBacklog(),
                                                                                commandAttempt(0),
                                                                                loggingMux(
                                                                                        LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }

    configurationChanged();
}

void AcquireBrooksPID0254::setToAutoprobe()
{ responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE; }

void AcquireBrooksPID0254::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireBrooksPID0254Component::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireBrooksPID0254::AcquireBrooksPID0254(const ComponentOptions &options,
                                           const std::string &loggingContext) : FramedInstrument(
        "brooks0254", loggingContext),
                                                                                autoprobeStatus(
                                                                                        AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                responseState(
                                                                                        RESP_PASSIVE_WAIT),
                                                                                issuedCommand(),
                                                                                commandBacklog(),
                                                                                commandAttempt(0),
                                                                                loggingMux(
                                                                                        LogStream_TOTAL)
{
    Q_UNUSED(options);
    setDefaultInvalid();
    config.append(Configuration());
    configurationChanged();
}

AcquireBrooksPID0254::~AcquireBrooksPID0254()
{
}

SequenceValue::Transfer AcquireBrooksPID0254::buildLogMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_brooks_pid0254");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["Address"].set(instrumentMeta["Address"]);

    for (QHash<int, QList<OutputVariable> >::const_iterator
            channelVariables = config.first().variables.constBegin(),
            endCV = config.first().variables.constEnd();
            channelVariables != endCV;
            ++channelVariables) {
        for (QList<OutputVariable>::const_iterator variable = channelVariables.value().constBegin(),
                endV = channelVariables.value().constEnd(); variable != endV; ++variable) {
            result.emplace_back(SequenceName({}, "raw_meta", variable->name),
                                Variant::Root(variable->metadata), time, FP::undefined());
            if (result.back().write().metadataReal("Format").toString().empty()) {
                result.back().write().metadataReal("Format").setString("00.000");
            }
            result.back().write().metadataReal("Source").set(instrumentMeta);
            result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
            Variant::Composite::fromCalibration(result.back().write().metadataReal("Calibration"),
                                                variable->calibration);
            result.back().write().metadataReal("Channel").setInt64(variable->channel);
            result.back()
                  .write()
                  .metadataReal("ChannelInformation")
                  .set(channelMetadata.read().array(variable->channel));
            result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
            result.back()
                  .write()
                  .metadataReal("Realtime")
                  .hash("GroupName")
                  .setString(variable->name);
            result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(0);
            result.back()
                  .write()
                  .metadataReal("Realtime")
                  .hash("GroupColumn")
                  .setString(QObject::tr("Value"));
        }
    }

    result.emplace_back(SequenceName({}, "raw_meta", "ZINPUTS"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back()
          .write()
          .metadataArray("Description")
          .setString("Raw input values from all setpoint channels");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Count").setInt64(TotalChannels);
    for (int channel = 0; channel < TotalChannels; channel++) {
        result.back()
              .write()
              .metadataArray("ChannelInformation")
              .array(channel)
              .set(channelMetadata.read().array(channel));
    }
    result.back().write().metadataArray("Children").metadataReal("Format").setString("0.000");

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    /* Always output this metadata, even if we're not logging the
     * actual values, so it gets updated as needed */

    result.emplace_back(SequenceName({}, "raw_meta", "ZOUTPUTS"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Description").setString("Setpoint output channel values");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("0.000");

    return result;
}

SequenceValue::Transfer AcquireBrooksPID0254::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_brooks_pid0254");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["Address"].set(instrumentMeta["Address"]);

    for (QHash<int, QList<OutputVariable> >::const_iterator
            channelVariables = config.first().variables.constBegin(),
            endCV = config.first().variables.constEnd();
            channelVariables != endCV;
            ++channelVariables) {
        for (QList<OutputVariable>::const_iterator variable = channelVariables.value().constBegin(),
                endV = channelVariables.value().constEnd(); variable != endV; ++variable) {
            Variant::Root metaRoot = variable->metadata;
            auto baseMeta = metaRoot.write();
            if (baseMeta.metadataReal("Format").toString().empty()) {
                baseMeta.metadataReal("Format").setString("00.000");
            }
            baseMeta.metadataReal("Source").set(instrumentMeta);
            baseMeta.metadataReal("Processing").toArray().after_back().set(processing);
            baseMeta.metadataReal("Channel").setInt64(variable->channel);
            baseMeta.metadataReal("ChannelInformation")
                    .set(channelMetadata.read().array(variable->channel));
            baseMeta.metadataReal("Realtime").hash("Page").setInt64(0);
            baseMeta.metadataReal("Realtime").hash("GroupName").setString(variable->name);

            result.emplace_back(SequenceName({}, "raw_meta", "ZIN" + variable->name), metaRoot,
                                time, FP::undefined());
            result.back()
                  .write()
                  .metadataReal("Description")
                  .setString("Raw input value from the controller");
            result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(1);
            result.back()
                  .write()
                  .metadataReal("Realtime")
                  .hash("GroupColumn")
                  .setString(QObject::tr("Input"));

            result.emplace_back(SequenceName({}, "raw_meta", "ZSP" + variable->name), metaRoot,
                                time, FP::undefined());
            result.back()
                  .write()
                  .metadataReal("Description")
                  .setString("Current controller setpoint");
            result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(2);
            result.back()
                  .write()
                  .metadataReal("Realtime")
                  .hash("GroupColumn")
                  .setString(QObject::tr("Setpoint"));
        }
    }

    for (int channel = 0; channel < TotalChannels; channel++) {
        auto idx = std::to_string(channel + 1);
        auto group = idx;

        Variant::Root metaRoot;
        auto baseMeta = metaRoot.write();
        baseMeta.metadataReal("Format").setString("00.000");
        baseMeta.metadataReal("Source").set(instrumentMeta);
        baseMeta.metadataReal("Processing").toArray().after_back().set(processing);
        baseMeta.metadataReal("Channel").setInt64(channel);
        baseMeta.metadataReal("ChannelInformation").set(channelMetadata.read().array(channel));
        baseMeta.metadataReal("Realtime").hash("Page").setInt64(1);
        baseMeta.metadataReal("Realtime").hash("GroupName").setString(group);

        result.emplace_back(SequenceName({}, "raw_meta", "ZIN" + idx), metaRoot, time,
                            FP::undefined());
        result.back().write().metadataReal("Description").setString("Raw input value");
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(0);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Input"));
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "ZSP" + idx), metaRoot, time,
                            FP::undefined());
        result.back().write().metadataReal("Description").setString("Current setpoint");
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(1);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Setpoint"));
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);
    }

    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataString("Realtime").hash("Hide").setBool(true);
    /*result.back().write().metadataString("Realtime").hash("Name").
        setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").
        setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").
        setBool(true);        
    result.back().write().metadataString("Realtime").hash("Page").
        setInt64(0);
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("Run").setString(QObject::tr("", "run state string"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("MalformedCommand").setString(QObject::tr("NO COMMS: Malformed command"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("CommandSequencingError").setString(QObject::tr("NO COMMS: Command sequencing error"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("InvalidResponse").setString(QObject::tr("NO COMMS: Invalid response"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("Timeout").setString(QObject::tr("NO COMMS: Timeout"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractive").setString(QObject::tr("STARTING COMMS"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveResetWait").setString(QObject::tr("STARTING COMMS: Waiting for reset completion"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveReadRevision").setString(QObject::tr("STARTING COMMS: Reading firmware version"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveReadSetpoint").setString(QObject::tr("STARTING COMMS: Reading setpoint values"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveLoadConfiguration").setString(QObject::tr("STARTING COMMS: Loading configuration"));*/


    return result;
}

SequenceMatch::Composite AcquireBrooksPID0254::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    return sel;
}

void AcquireBrooksPID0254::invalidateLogValues(double frameTime)
{
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;
        if (loggingEgress != NULL)
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    loggingMux.clear();
    lastInputs.write().setEmpty();
    loggingLost(frameTime);
}

void AcquireBrooksPID0254::logValues(double frameTime,
                                     int streamID,
                                     SequenceValue::Transfer &&values)
{
    double startTime = streamTime[streamID];
    double endTime = frameTime;
    Q_ASSERT(FP::defined(endTime));
    if (!FP::defined(startTime) || startTime == endTime || !loggingEgress) {
        if (loggingEgress)
            streamTime[streamID] = endTime;
        if (!realtimeEgress)
            return;
        realtimeEgress->incomingData(std::move(values));
        return;
    }

    if (!realtimeEgress) {
        for (auto &output : values) {
            output.setStart(startTime);
            output.setEnd(endTime);
        }
        loggingMux.incoming(streamID, std::move(values), loggingEgress);
        if (values.empty()) {
            loggingMux.advance(streamID, startTime, loggingEgress);
        }

        streamAge[streamID]++;
        streamTime[streamID] = endTime;
        return;
    }

    {
        auto copy = values;
        for (auto &output : copy) {
            output.setStart(startTime);
            output.setEnd(endTime);
        }
        loggingMux.incoming(streamID, std::move(copy), loggingEgress);
        if (values.empty()) {
            loggingMux.advance(streamID, startTime, loggingEgress);
        }
    }
    streamAge[streamID]++;
    streamTime[streamID] = endTime;

    Q_ASSERT(realtimeEgress);
    realtimeEgress->incomingData(std::move(values));
}

std::size_t AcquireBrooksPID0254::dataFrameStart(std::size_t offset,
                                                 const Util::ByteArray &input) const
{
    auto max = input.size();
    while (offset < max) {
        auto ch = input[offset];
        if (ch == '\r' || ch == '\n' || ch == 0x10 || ch == 0x02) {
            offset++;
            continue;
        }
        return offset;
    }
    return input.npos;
}

std::size_t AcquireBrooksPID0254::dataFrameEnd(std::size_t start,
                                               const Util::ByteArray &input) const
{
    for (auto max = input.size(); start < max; start++) {
        auto ch = input[start];
        if (ch == '\r' || ch == '\n' || ch == 0x10 || ch == 0x02)
            return start;
    }
    return input.npos;
}

std::size_t AcquireBrooksPID0254::controlFrameStart(std::size_t offset,
                                                    const Util::ByteArray &input) const
{
    auto max = input.size();
    while (offset < max) {
        auto ch = input[offset];
        if (ch == '\r' || ch == '\n' || ch == 0x1B) {
            offset++;
            continue;
        }
        return offset;
    }
    return input.npos;
}

std::size_t AcquireBrooksPID0254::controlFrameEnd(std::size_t start,
                                                  const Util::ByteArray &input) const
{
    for (auto max = input.size(); start < max; start++) {
        auto ch = input[start];
        if (ch == '\r' || ch == '\n' || ch == 0x1B)
            return start;
    }
    return input.npos;
}

void AcquireBrooksPID0254::describeState(Variant::Write &info) const
{
    if (issuedCommand.getType() != COMMAND_INVALID) {
        info.hash("Command").set(issuedCommand.stateDescription());
    }

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        info.hash("ResponseState").setString("AutoprobePassiveInitialize");
        break;
    case RESP_AUTOPROBE_PASSIVE_WAIT:
        info.hash("ResponseState").setString("AutoprobePassiveWait");
        break;
    case RESP_PASSIVE_WAIT:
        info.hash("ResponseState").setString("PassiveWait");
        break;
    case RESP_PASSIVE_RUN:
        info.hash("ResponseState").setString("PassiveRun");
        break;
    case RESP_INTERACTIVE_RUN_READ_PROCESS:
        info.hash("ResponseState").setString("InteractiveRunReadProcess");
        break;
    case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
        info.hash("ResponseState").setString("InteractiveRunReadSetpoints");
        break;
    case RESP_INTERACTIVE_RUN_WAIT:
        info.hash("ResponseState").setString("InteractiveRunWait");
        break;
    case RESP_INTERACTIVE_INITIALIZE:
        info.hash("ResponseState").setString("InteractiveInitialize");
        break;
    case RESP_INTERACTIVE_START_RESET:
        info.hash("ResponseState").setString("InteractiveStartReset");
        break;
    case RESP_INTERACTIVE_START_ENABLE_SEND:
        info.hash("ResponseState").setString("InteractiveStartEnableSend");
        break;
    case RESP_INTERACTIVE_START_IDENTIFY:
        info.hash("ResponseState").setString("InteractiveStartIdentify");
        break;
    case RESP_INTERACTIVE_START_READ_PARAMETERS:
        info.hash("ResponseState").setString("InteractiveStartReadParameters");
        break;
    case RESP_INTERACTIVE_START_READ_SETPOINTS:
        info.hash("ResponseState").setString("InteractiveStartReadSetpoints");
        break;
    case RESP_INTERACTIVE_RESTART_WAIT:
        info.hash("ResponseState").setString("InteractiveRestartWait");
        break;
    }
}

AcquireBrooksPID0254::Command::Command() : type(COMMAND_INVALID), port(-1), state(0)
{ }

AcquireBrooksPID0254::Command::Command(CommandType t, Util::ByteArray p, int prt, int st) : type(t),
                                                                                            packet(std::move(
                                                                                                    p)),
                                                                                            port(prt),
                                                                                            state(st)
{ }

AcquireBrooksPID0254::Command::Command(CommandType t, const char *str, int prt, int st) : Command(t,
                                                                                                  Util::ByteArray(
                                                                                                          str),
                                                                                                  prt,
                                                                                                  st)
{ }

AcquireBrooksPID0254::Command::Command(const Command &other) = default;

AcquireBrooksPID0254::Command &AcquireBrooksPID0254::Command::operator=(const Command &other) = default;

Variant::Root AcquireBrooksPID0254::Command::stateDescription() const
{
    Variant::Root result;
    result["Data"].setString(packet.toString());
    if (port >= 0)
        result["Port"].setInt64(port);
    if (state > 0)
        result["State"].setInt64(state);

    switch (type) {
    default:
        result["Type"].setString("Unknown");
        break;
    case COMMAND_MENU:
        result["Type"].setString("Menu");
        break;
    case COMMAND_VIEW:
        result["Type"].setString("View");
        break;
    case COMMAND_IDENTIFY:
        result["Type"].setString("Identify");
        break;
    case COMMAND_READ_MEASURED:
        result["Type"].setString("ReadMeasured");
        break;
    case COMMAND_READ_PARAMETER:
        result["Type"].setString("ReadParameter");
        break;
    case COMMAND_SET_PARAMETER:
        result["Type"].setString("SetParameter");
        break;
    }
    return result;
}

void AcquireBrooksPID0254::initiateInteractiveStart(double frameTime)
{
    commandBacklog.clear();
    issuedCommand.invalidate();
    if (controlStream != NULL) {
        Util::ByteArray data;
        data.push_back(0x1B);
        data += "AZ\r";
        controlStream->writeControl(data);
    }
    responseState = RESP_INTERACTIVE_START_RESET;
    timeoutAt(frameTime + 10.0);
    discardData(frameTime + 1.0);
}

void AcquireBrooksPID0254::frameRejected(const Util::ByteView &frame, double frameTime, int code)
{
    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
        if (commandBacklog.isEmpty()) {
            issuedCommand.invalidate();
        } else {
            issuedCommand = commandBacklog.takeFirst();
        }
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                     << "rejected with code" << code << "in passive state" << responseState;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                  frameTime, FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            info.hash("Line").setString(frame.toString());
            info.hash("Code").setInt64(code);
            event(frameTime,
                  QObject::tr("Invalid response (code %1).  Communications dropped.").arg(code),
                  true, info);
        }

        if (commandBacklog.isEmpty()) {
            issuedCommand.invalidate();
        } else {
            issuedCommand = commandBacklog.takeFirst();
        }
        break;

    case RESP_INTERACTIVE_RUN_READ_PROCESS:
    case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                     << "rejected with code" << code << "in interactive run state" << responseState;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                  frameTime, FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            info.hash("Line").setString(frame.toString());
            info.hash("Code").setInt64(code);
            event(frameTime,
                  QObject::tr("Invalid response (code %1).  Communications dropped.").arg(code),
                  true, info);
        }

        initiateInteractiveStart(frameTime);
        generalStatusUpdated();
        break;

    case RESP_INTERACTIVE_INITIALIZE:
    case RESP_INTERACTIVE_START_RESET:
    case RESP_INTERACTIVE_START_ENABLE_SEND:
    case RESP_INTERACTIVE_START_IDENTIFY:
    case RESP_INTERACTIVE_START_READ_PARAMETERS:
    case RESP_INTERACTIVE_START_READ_SETPOINTS:
        qCDebug(log) << "Line at " << Logging::time(frameTime) << ":" << frame
                     << "rejected with code" << code << "in state interactive start state"
                     << responseState;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                  frameTime, FP::undefined()));
        }

        commandBacklog.clear();
        issuedCommand.invalidate();
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        break;
    }

    invalidateLogValues(frameTime);
}

static quint8 calculateChecksum(const uchar *data, int length)
{
    quint32 result = 0;
    for (const uchar *end = data + length + 1; data != end; data++) {
        result += *data;
        result &= 0xFF;
    }
    /* "Negation" from the manual means the lowest order byte of a > 1 byte
     * two's complement negation */
    result = (~result + 1) & 0xFF;
    return (quint8) result;
}

bool AcquireBrooksPID0254::verifyChecksum(const Util::ByteView &frame, double frameTime)
{
    quint32 sum = 0xFFFFFFFF;
    quint32 calculated = 0xFFFFFFFF;
    if (frame.size() > 4 && frame[0] == 'A' && frame[1] == 'Z') {
        bool ok = false;
        sum = frame.mid(frame.size() - 2).parse_u32(&ok, 16);
        if (ok && sum < 0xFF) {
            calculated =
                    (quint32) calculateChecksum(frame.data<const uchar *>(2), frame.size() - 5);
            if (sum == calculated) {
                /* If the checksums match, then send the ACK, the
                 * parser will bail out in a more complete fashion
                 * if something else is amiss */
                packageAndSend(Util::ByteArray("A"));
                return true;
            }
        }
    }

    ++commandAttempt;
    if (controlStream != NULL && commandAttempt < 5) {
        if (sum != 0xFFFFFFFF && calculated != 0xFFFFFFFF && sum != calculated) {
            packageAndSend(Util::ByteArray("N"));
            return false;
        } else if (issuedCommand.getType() != COMMAND_INVALID) {
            packageAndSend(issuedCommand.getPacket(), issuedCommand.getPort());
            return false;
        }
    }

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        break;

    case RESP_PASSIVE_RUN:
        break;

    case RESP_INTERACTIVE_RUN_READ_PROCESS:
    case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Command retry limit exceeded in interactive run state" << responseState
                     << "at" << Logging::time(frameTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("RetryLimitExceeded"),
                                  frameTime, FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            info.hash("Line").setString(frame.toString());
            if (sum != 0xFFFFFFFF && calculated != 0xFFFFFFFF && sum != calculated) {
                info.hash("ReportedChecksum").setInt64(sum);
                info.hash("CalculatedChecksum").setInt64(calculated);
                event(frameTime, QObject::tr("Checksum mismatch.  Communications dropped."), true,
                      info);
            } else {
                event(frameTime, QObject::tr("Invalid line framing.  Communications dropped."),
                      true, info);
            }
        }

        initiateInteractiveStart(frameTime);
        generalStatusUpdated();
        break;

    case RESP_INTERACTIVE_INITIALIZE:
    case RESP_INTERACTIVE_START_RESET:
    case RESP_INTERACTIVE_START_ENABLE_SEND:
    case RESP_INTERACTIVE_START_IDENTIFY:
    case RESP_INTERACTIVE_START_READ_PARAMETERS:
    case RESP_INTERACTIVE_START_READ_SETPOINTS:
        qCDebug(log) << "Command retry limit exceeded in interactive start state" << responseState
                     << "at" << Logging::time(frameTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("RetryLimitExceeded"),
                                  frameTime, FP::undefined()));
        }

        commandBacklog.clear();
        issuedCommand.invalidate();
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        break;
    }

    invalidateLogValues(frameTime);
    return false;
}


void AcquireBrooksPID0254::configurationChanged()
{
    for (QMap<int, double>::const_iterator add = config.first().initializeSetpoint.constBegin(),
            endAdd = config.first().initializeSetpoint.constEnd(); add != endAdd; ++add) {
        if (!FP::defined(add.value()))
            continue;
        QMap<int, double>::const_iterator check = setpointUpdate.constFind(add.key());
        if (check == setpointUpdate.constEnd() || !FP::defined(check.value())) {
            setpointUpdate.insert(add.key(), add.value());
        }

        check = setpointTarget.constFind(add.key());
        if (check == setpointTarget.constEnd() || !FP::defined(check.value())) {
            setpointTarget.insert(add.key(), add.value());
        }
    }

    if (config.first().address > 0) {
        instrumentMeta["Address"].setInt64(config.first().address);
    }

    auto variables = instrumentMeta["VariableIndex"];
    variables.remove();
    for (auto channelVariables = config.first().variables.constBegin(),
            endCV = config.first().variables.constEnd();
            channelVariables != endCV;
            ++channelVariables) {
        if (channelVariables.value().empty())
            continue;

        auto index = channelVariables.key();
        if (index < 0 || index >= TotalChannels)
            continue;

        const auto &variable = channelVariables.value().front();
        variables.array(static_cast<std::size_t>(index)).setString(variable.name);
    }

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    sourceMetadataUpdated();
}

void AcquireBrooksPID0254::configurationAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

void AcquireBrooksPID0254::updateLogMeta(double frameTime)
{
    if (loggingEgress == NULL) {
        haveEmittedLogMeta = false;
        loggingMux.clear();
        memset(streamAge, 0, sizeof(streamAge));
        for (int i = 0; i < LogStream_TOTAL; i++) {
            streamTime[i] = FP::undefined();
        }
        lastInputs.write().setEmpty();
    } else {
        if (!haveEmittedLogMeta) {
            haveEmittedLogMeta = true;
            loggingMux.incoming(LogStream_Metadata, buildLogMeta(frameTime), loggingEgress);
        }
        loggingMux.advance(LogStream_Metadata, frameTime, loggingEgress);
    }
}

static bool viewParameterExtract(const QString &input,
                                 const QString &match,
                                 Variant::Write &target,
                                 bool asNumber = false)
{
    int idx = input.indexOf(match, 0, Qt::CaseInsensitive);
    if (idx < 0)
        return false;
    idx += match.length() + 1;

    QString result(input.mid(idx).trimmed());
    if (result.isEmpty())
        return false;

    Variant::Root original(target);

    if (asNumber) {
        bool ok = false;
        double v = result.toDouble(&ok);
        if (!ok || !FP::defined(v))
            return false;
        target.setDouble(v);
    } else {
        target.setString(result);
    }

    return target != original.read();
}

static bool viewParameterExtract(const QString &input,
                                 const QString &match,
                                 Variant::Write &&target,
                                 bool asNumber = false)
{ return viewParameterExtract(input, match, target, asNumber); }

void AcquireBrooksPID0254::packageAndSend(const Util::ByteView &data, int port)
{
    if (controlStream == NULL)
        return;

    Util::ByteArray output("AZ");
    if (config.first().address >= 0) {
        output += QByteArray::number(config.first().address).rightJustified(5, '0');
    }
    if (port > 0) {
        output.push_back('.');
        output += QByteArray::number(port).rightJustified(2, '0');
    }
    output += data;
    output.push_back('\r');

    controlStream->writeControl(std::move(output));
}

void AcquireBrooksPID0254::sendCommand(const Command &command, double frameTime)
{
    commandBacklog.clear();
    issuedCommand = command;

    if (FP::defined(frameTime)) {
        discardData(FP::undefined());
        timeoutAt(frameTime + 2.0);
    }

    commandAttempt = 0;
    packageAndSend(command.getPacket(), command.getPort());
}


static bool isInput(int port)
{
    return (port % 2) == 1;
}

static int toInputPort(int channel)
{
    return (channel * 2) + 1;
}

static int toOutputPort(int channel)
{
    return (channel + 1) * 2;
}

static int fromInputPort(int port)
{
    return port / 2;
}

static int fromOutputPort(int port)
{
    return (port / 2) - 1;
}

bool AcquireBrooksPID0254::advanceSetpointChange(double frameTime, int channel)
{
    if (channel < 0 || channel >= TotalChannels)
        channel = 0;
    else
        channel = channel + 1;
    QMap<int, double>::const_iterator next = setpointUpdate.lowerBound(channel);
    for (; next != setpointUpdate.constEnd() && !FP::defined(next.value()); ++next) { }
    if (next == setpointUpdate.constEnd())
        return false;
    channel = next.key();
    if (channel < 0 || channel >= TotalChannels)
        return false;

    Util::ByteArray packet("P01=");
    packet += QByteArray::number(next.value(), 'f', 3);

    responseState = RESP_INTERACTIVE_RUN_READ_PROCESS;
    sendCommand(Command(COMMAND_SET_PARAMETER, std::move(packet), toOutputPort(channel)),
                frameTime);
    return true;
}

bool AcquireBrooksPID0254::advanceSetpointRead(double frameTime, int channel)
{
    if (channel < 0 || channel >= TotalChannels) {
        channel = 0;
    } else if (channel + 1 >= TotalChannels) {
        return false;
    } else {
        channel = channel + 1;
    }

    responseState = RESP_INTERACTIVE_RUN_READ_SETPOINTS;
    sendCommand(Command(COMMAND_READ_PARAMETER, "P01?", toOutputPort(channel)), frameTime);
    return true;
}

bool AcquireBrooksPID0254::advanceProcessRead(double frameTime, int channel)
{
    if (channel < 0 || channel >= TotalChannels) {
        channel = 0;
    } else if (channel + 1 >= TotalChannels) {
        if (FP::defined(config.first().pollInterval) && config.first().pollInterval > 0.0) {
            double beginTime = streamTime[LogStream_PV_Begin];
            if (!FP::defined(beginTime) || beginTime > frameTime)
                beginTime = frameTime;
            beginTime += config.first().pollInterval;
            if (beginTime > frameTime) {
                timeoutAt(beginTime + config.first().pollInterval);
                discardData(FP::undefined());
                responseState = RESP_INTERACTIVE_RUN_WAIT;
                commandBacklog.clear();
                issuedCommand.invalidate();
                return false;
            }
        }
        channel = 0;
    } else {
        channel = channel + 1;
    }

    responseState = RESP_INTERACTIVE_RUN_READ_PROCESS;
    sendCommand(Command(COMMAND_READ_MEASURED, "K", toInputPort(channel)), frameTime);
    return true;
}

void AcquireBrooksPID0254::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configurationAdvance(frameTime);

    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    std::deque<Util::ByteView> fields;
    Util::ByteView field;
    bool ok = false;
    int channel = -1;

    /* First stage: process the actual response to the command and update
     * the associated values and intermediate state.  During interactive
     * start comms, this also issues the next command for some of the initial
     * sequence (to prevent value output before we know the serial number,
     * etc). */
    switch (issuedCommand.getType()) {
    case COMMAND_INVALID:
        /* Maybe do something with "unpolled" style packets? */
        break;

    case COMMAND_MENU:
        switch (issuedCommand.getState()) {
        case Command::ExpectedTimeout_Begin:
            issuedCommand.setState(Command::ExpectedTimeout_Wait);

            switch (responseState) {
            default:
                timeoutAt(frameTime + 1.0);
                break;

            case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
                responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
                /* Fall through */
            case RESP_AUTOPROBE_PASSIVE_WAIT:
            case RESP_PASSIVE_WAIT:
            case RESP_PASSIVE_RUN:
                /* Reduced timeout in passive mode so we hit it first */
                timeoutAt(frameTime + 0.9);
                break;
            }
            break;
        default:
            break;
        }

        /* State transitions handled in timeout */
        return;

    case COMMAND_VIEW: {
        switch (issuedCommand.getState()) {
        case Command::ExpectedTimeout_Begin:
            issuedCommand.setState(Command::ExpectedTimeout_Wait);

            switch (responseState) {
            default:
                timeoutAt(frameTime + 1.0);
                break;

            case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
                responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
                /* Fall through */
            case RESP_AUTOPROBE_PASSIVE_WAIT:
            case RESP_PASSIVE_WAIT:
            case RESP_PASSIVE_RUN:
                /* Reduced timeout in passive mode so we hit it first */
                timeoutAt(frameTime + 0.9);
                break;
            }
            break;
        default:
            break;
        }

        int port = issuedCommand.getPort();
        if (port >= 1 && port <= 7 && isInput(port)) {
            channel = fromInputPort(port);
            Q_ASSERT(channel >= 0 && channel < TotalChannels);

            auto metaTarget = channelMetadata.write().array(channel);

            QString line(frame.toQString(false));
            if (viewParameterExtract(line, "Measure Units", metaTarget.hash("Units")) ||
                    viewParameterExtract(line, "Gas Factor", metaTarget.hash("GasFactor"), true) ||
                    viewParameterExtract(line, "PV Signal Type", metaTarget.hash("SignalType")) ||
                    viewParameterExtract(line, "PV Full Scale", metaTarget.hash("FullScale"))) {
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
            }
        }

        /* State transitions handled in timeout */
        return;
    }

    case COMMAND_IDENTIFY: {
        if (!verifyChecksum(frame, frameTime))
            return;

        fields = Util::as_deque(frame.split(','));
        if (fields.empty()) {
            frameRejected(frame, frameTime, 1000);
            return;
        }
        if (fields.front() != "AZ") {
            frameRejected(frame, frameTime, 1001);
            return;
        }
        fields.pop_front();

        if (fields.empty()) {
            frameRejected(frame, frameTime, 1002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field.size() != 5) {
                frameRejected(frame, frameTime, 1100);
                return;
            }
        }
        int address = field.parse_u16(&ok);
        if (!ok) {
            frameRejected(frame, frameTime, 1003);
            return;
        }
        /* Ignore anything not targeted at us */
        if (config.first().address >= 0 && address != config.first().address)
            return;

        /* Response type code */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 1004);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field != "4") {
                frameRejected(frame, frameTime, 1101);
                return;
            }
        }

        /* Manufacturer */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 1005);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (!Util::contains(Util::to_lower(field.toString()), "brooks")) {
                frameRejected(frame, frameTime, 1102);
                return;
            }
        }

        /* Model */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 1006);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (!Util::contains(field.toString(), "0254")) {
                frameRejected(frame, frameTime, 1103);
                return;
            }
        }

        /* Port count */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 1006);
            return;
        }
        fields.pop_front();

        /* Firmware version */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 1007);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        QString firmwareVersion(field.toQString(false));
        if (firmwareVersion.isEmpty()) {
            frameRejected(frame, frameTime, 1008);
            return;
        }

        /* Start vector */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 1009);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field.size() != 4) {
                frameRejected(frame, frameTime, 1104);
                return;
            }
        }

        /* Checksum */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 1010);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field.size() != 2) {
                frameRejected(frame, frameTime, 1105);
                return;
            }
        }

        if (config.first().strictMode) {
            if (!fields.empty()) {
                frameRejected(frame, frameTime, 1199);
                return;
            }
        }

        Variant::Root oldMeta = instrumentMeta;
        instrumentMeta["FirmwareVersion"].setString(firmwareVersion);
        instrumentMeta["Address"].setInt64(address);
        if (instrumentMeta.read() != oldMeta.read()) {
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
        case RESP_INTERACTIVE_RUN_READ_PROCESS:
        case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
        case RESP_INTERACTIVE_RUN_WAIT:
            break;

        case RESP_INTERACTIVE_START_IDENTIFY:
            sendCommand(Command(COMMAND_VIEW, "V", 1, Command::ExpectedTimeout_Begin), frameTime);
            responseState = RESP_INTERACTIVE_START_READ_PARAMETERS;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadParameters"), frameTime, FP::undefined()));
            }
            return;

        default:
            frameRejected(frame, frameTime, 1999);
            return;
        }

        break;
    }

    case COMMAND_READ_MEASURED: {
        if (!verifyChecksum(frame, frameTime))
            return;

        fields = Util::as_deque(frame.split(','));
        if (fields.empty()) {
            frameRejected(frame, frameTime, 2000);
            return;
        }
        if (fields.front() != "AZ") {
            frameRejected(frame, frameTime, 2001);
            return;
        }
        fields.pop_front();

        if (fields.empty()) {
            frameRejected(frame, frameTime, 2002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field.size() != 8) {
                frameRejected(frame, frameTime, 2100);
                return;
            }
        }

        int address = 0;
        int port = 0;
        {
            int idxDecimal = field.indexOf('.');
            if (idxDecimal <= 0 || idxDecimal >= field.size() - 1) {
                frameRejected(frame, frameTime, 2003);
                return;
            }
            if (config.first().strictMode) {
                if (idxDecimal != 5) {
                    frameRejected(frame, frameTime, 2101);
                    return;
                }
            }
            address = field.mid(0, idxDecimal).parse_u16(&ok);
            if (!ok) {
                frameRejected(frame, frameTime, 2004);
                return;
            }
            /* Ignore anything not targeted at us */
            if (config.first().address >= 0 && address != config.first().address)
                return;

            port = field.mid(idxDecimal + 1).parse_i32(&ok);
            if (!ok) {
                frameRejected(frame, frameTime, 2005);
                return;
            }
        }

        if (port <= 0 || port >= 8 || (port % 2) != 1) {
            frameRejected(frame, frameTime, 2006);
            return;
        }
        channel = fromInputPort(port);
        Q_ASSERT(channel >= 0 && channel < TotalChannels);

        /* Response type code */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 2007);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field != "2" && field != "4") {
                frameRejected(frame, frameTime, 2102);
                return;
            }
        }

        /* Non-resettable totalizer value */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 2008);
            return;
        }
        fields.pop_front();

        /* Totalizer value */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 2009);
            return;
        }
        fields.pop_front();

        if (fields.empty()) {
            frameRejected(frame, frameTime, 2010);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root pv(field.parse_real(&ok));
        if (!ok) {
            frameRejected(frame, frameTime, 2011);
            return;
        }
        if (!FP::defined(pv.read().toReal())) {
            frameRejected(frame, frameTime, 2012);
            return;
        }
        remap("ZIN" + std::to_string(channel + 1), pv);

        /* Reserved fields */
        for (int i = 0; i < 7; i++) {
            if (fields.empty()) {
                frameRejected(frame, frameTime, 2020 + i);
                return;
            }
            fields.pop_front();
        }

        /* Checksum */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 2013);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field.size() != 2) {
                frameRejected(frame, frameTime, 2102);
                return;
            }
        }

        if (config.first().strictMode) {
            if (!fields.empty()) {
                frameRejected(frame, frameTime, 2199);
                return;
            }
        }

        lastInputs.write().array(channel).set(pv);

        if (realtimeEgress != NULL) {
            SequenceValue dv(SequenceName({}, "raw", "ZIN" + std::to_string(channel + 1)),
                             Variant::Root(pv), frameTime, frameTime + 1.0);
            realtimeEgress->incomingData(dv);
        }

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
            responseState = RESP_PASSIVE_RUN;
            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            qCDebug(log) << "Autoprobe succeeded at" << Logging::time(frameTime);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                      FP::undefined()));
            }

            {
                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("AutoprobePassiveWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
            break;

        case RESP_PASSIVE_WAIT:
            responseState = RESP_PASSIVE_RUN;
            generalStatusUpdated();

            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                      FP::undefined()));
            }

            {
                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveWait");
                event(frameTime, QObject::tr("Passive communications established."), false, info);
            }
            break;

        case RESP_PASSIVE_RUN:
        case RESP_INTERACTIVE_RUN_READ_PROCESS:
        case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
        case RESP_INTERACTIVE_RUN_WAIT:
            break;

        default:
            frameRejected(frame, frameTime, 2999);
            return;
        }

        break;
    }

    case COMMAND_READ_PARAMETER: {
        if (!verifyChecksum(frame, frameTime))
            return;

        fields = Util::as_deque(frame.split(','));
        if (fields.empty()) {
            frameRejected(frame, frameTime, 3000);
            return;
        }
        if (fields.front() != "AZ") {
            frameRejected(frame, frameTime, 3001);
            return;
        }
        fields.pop_front();

        if (fields.empty()) {
            frameRejected(frame, frameTime, 3002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field.size() != 8) {
                frameRejected(frame, frameTime, 3100);
                return;
            }
        }
        int address = 0;
        int port = 0;
        {
            int idxDecimal = field.indexOf('.');
            if (idxDecimal <= 0 || idxDecimal >= field.size() - 1) {
                frameRejected(frame, frameTime, 3003);
                return;
            }
            if (config.first().strictMode) {
                if (idxDecimal != 5) {
                    frameRejected(frame, frameTime, 3101);
                    return;
                }
            }
            address = field.mid(0, idxDecimal).parse_u16(&ok);
            if (!ok) {
                frameRejected(frame, frameTime, 3004);
                return;
            }
            /* Ignore anything not targeted at us */
            if (config.first().address >= 0 && address != config.first().address)
                return;

            port = field.mid(idxDecimal + 1).parse_i32(&ok);
            if (!ok) {
                frameRejected(frame, frameTime, 3005);
                return;
            }
            if (port <= 0 || port > TotalChannels * 2) {
                frameRejected(frame, frameTime, 3006);
                return;
            }
            if (config.first().strictMode) {
                if (port != issuedCommand.getPort() && issuedCommand.getPort() > 0) {
                    frameRejected(frame, frameTime, 3102);
                    return;
                }
            }
        }

        /* Response type code */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 3007);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field != "4") {
                frameRejected(frame, frameTime, 3103);
                return;
            }
        }

        /* Parameter type */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 3008);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field.size() != 3) {
                frameRejected(frame, frameTime, 3104);
                return;
            }
            if (field != issuedCommand.getPacket().mid(0, 3)) {
                frameRejected(frame, frameTime, 3109);
                return;
            }
        }
        if (field.string_start("P"))
            field = field.mid(1);
        int parameter = field.string_trimmed().parse_i32(&ok);
        if (!ok)
            parameter = -1;

        /* Value */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 3010);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        double value = field.parse_real(&ok);
        if (!ok) {
            frameRejected(frame, frameTime, 3011);
            return;
        }
        if (!FP::defined(value)) {
            frameRejected(frame, frameTime, 3012);
            return;
        }
        int decimals = field.indexOf('.');
        if (decimals < 0)
            decimals = 0;
        else
            decimals = field.size() - (decimals + 1);

        /* Checksum */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 3013);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field.size() != 2) {
                frameRejected(frame, frameTime, 3106);
                return;
            }
        }

        if (config.first().strictMode) {
            if (!fields.empty()) {
                frameRejected(frame, frameTime, 3199);
                return;
            }
        }

        if (isInput(port)) {
            /* Input parameter */
            channel = fromInputPort(port);
            Q_ASSERT(channel >= 0 && channel < TotalChannels);

            switch (responseState) {
            case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
            case RESP_AUTOPROBE_PASSIVE_WAIT:
            case RESP_PASSIVE_WAIT:
            case RESP_PASSIVE_RUN:
            case RESP_INTERACTIVE_RUN_READ_PROCESS:
            case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
            case RESP_INTERACTIVE_RUN_WAIT:
                break;

            default:
                frameRejected(frame, frameTime, 3990);
                return;
            }
        } else {
            /* Output parameter */
            channel = fromOutputPort(port);
            Q_ASSERT(channel >= 0 && channel < TotalChannels);

            switch (parameter) {
            default:
                switch (responseState) {
                case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
                case RESP_AUTOPROBE_PASSIVE_WAIT:
                case RESP_PASSIVE_WAIT:
                case RESP_PASSIVE_RUN:
                case RESP_INTERACTIVE_RUN_READ_PROCESS:
                case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
                case RESP_INTERACTIVE_RUN_WAIT:
                    break;

                default:
                    frameRejected(frame, frameTime, 3991);
                    return;
                }

                /* Setpoint */
            case 1: {
                double check = setpointOutputValues.read().array(channel).toReal();
                double divisor = pow(10.0, (double) decimals);

                {
                    QMap<int, double>::iterator update = setpointUpdate.find(channel);
                    if (update != setpointUpdate.end()) {
                        double rounded = floor(update.value() * divisor) / divisor;
                        if (qFuzzyCompare(rounded, value))
                            setpointUpdate.erase(update);
                    }
                }

                if (!FP::defined(check) ||
                        !qFuzzyCompare(floor(check * divisor) / divisor, value)) {
                    if (persistentEgress != NULL &&
                            config.first().logOutputs &&
                            FP::defined(setpointOutputValues.getStart())) {
                        setpointOutputValues.setEnd(frameTime);
                        persistentEgress->incomingData(setpointOutputValues);
                    }
                    setpointOutputValues.write().array(channel).setDouble(value);

                    setpointOutputValues.setStart(frameTime);
                    setpointOutputValues.setEnd(FP::undefined());
                    if (realtimeEgress != NULL) {
                        realtimeEgress->incomingData(setpointOutputValues);
                    }

                    if (config.first().logOutputs) {
                        persistentValuesUpdated();
                    }
                }

                if (realtimeEgress != NULL) {
                    QList<OutputVariable> channelVariables(config.first().variables[channel]);
                    for (QList<OutputVariable>::const_iterator var = channelVariables.constBegin(),
                            endVar = channelVariables.constEnd(); var != endVar; ++var) {

                        SequenceValue dv
                                (SequenceName({}, "raw", "ZSP" + var->name), Variant::Root(value),
                                 frameTime, frameTime + 1.0);
                        realtimeEgress->incomingData(dv);
                    }
                    SequenceValue dv(SequenceName({}, "raw", "ZSP" + std::to_string(channel + 1)),
                                     Variant::Root(value), frameTime, frameTime + 1.0);
                    realtimeEgress->incomingData(dv);
                }

                switch (responseState) {
                case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
                case RESP_AUTOPROBE_PASSIVE_WAIT:
                case RESP_PASSIVE_WAIT:
                case RESP_PASSIVE_RUN:
                case RESP_INTERACTIVE_RUN_READ_PROCESS:
                case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
                case RESP_INTERACTIVE_RUN_WAIT:
                    break;

                case RESP_INTERACTIVE_START_READ_SETPOINTS:
                    ++channel;
                    if (channel < TotalChannels) {
                        sendCommand(Command(COMMAND_READ_PARAMETER, "P01?", toOutputPort(channel)),
                                    frameTime);
                    } else {
                        sendCommand(Command(COMMAND_READ_MEASURED, "K", 1), frameTime);
                        responseState = RESP_INTERACTIVE_RUN_READ_PROCESS;

                        qCDebug(log) << "Interactive start comms succeeded at"
                                     << Logging::time(frameTime);

                        {
                            std::lock_guard<std::mutex> lock(mutex);
                            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                        }
                        autoprobeStatusUpdated();
                        generalStatusUpdated();

                        if (realtimeEgress != NULL) {
                            realtimeEgress->incomingData(
                                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"),
                                                  frameTime, FP::undefined()));
                        }

                        {
                            Variant::Write info = Variant::Write::empty();
                            info.hash("ResponseState").setString("InteractiveStartReadSetpoints");
                            event(frameTime, QObject::tr("Communications established."), false,
                                  info);
                        }

                        setpointUpdate = setpointTarget;
                    }
                    return;

                default:
                    frameRejected(frame, frameTime, 3992);
                    return;
                }
            }
            }
        }

        break;

    }

    case COMMAND_SET_PARAMETER: {
        if (!verifyChecksum(frame, frameTime))
            return;

        fields = Util::as_deque(frame.split(','));
        if (fields.empty()) {
            frameRejected(frame, frameTime, 4000);
            return;
        }
        if (fields.front() != "AZ") {
            frameRejected(frame, frameTime, 4001);
            return;
        }
        fields.pop_front();

        if (fields.empty()) {
            frameRejected(frame, frameTime, 4002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field.size() != 8) {
                frameRejected(frame, frameTime, 4100);
                return;
            }
        }
        int address = 0;
        int port = 0;
        {
            int idxDecimal = field.indexOf('.');
            if (idxDecimal <= 0 || idxDecimal >= field.size() - 1) {
                frameRejected(frame, frameTime, 4003);
                return;
            }
            if (config.first().strictMode) {
                if (idxDecimal != 5) {
                    frameRejected(frame, frameTime, 4101);
                    return;
                }
            }
            address = field.mid(0, idxDecimal).parse_u16(&ok);
            if (!ok) {
                frameRejected(frame, frameTime, 4004);
                return;
            }
            /* Ignore anything not targeted at us */
            if (config.first().address >= 0 && address != config.first().address)
                return;

            port = field.mid(idxDecimal + 1).parse_i32(&ok);
            if (!ok) {
                frameRejected(frame, frameTime, 4005);
                return;
            }
            if (port <= 0 || port > TotalChannels * 2) {
                frameRejected(frame, frameTime, 4006);
                return;
            }
            if (config.first().strictMode) {
                if (port != issuedCommand.getPort() && issuedCommand.getPort() > 0) {
                    frameRejected(frame, frameTime, 4102);
                    return;
                }
            }
        }

        /* Response type code */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 4007);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field != "4") {
                frameRejected(frame, frameTime, 4103);
                return;
            }
        }

        /* Parameter type */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 4008);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field.size() != 3) {
                frameRejected(frame, frameTime, 4104);
                return;
            }
            if (field != issuedCommand.getPacket().mid(0, 3)) {
                frameRejected(frame, frameTime, 4109);
                return;
            }
        }
        if (field.string_start("P"))
            field = field.mid(1);
        int parameter = field.parse_i32(&ok);
        if (!ok)
            parameter = -1;

        /* Value */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 4010);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        double value = field.parse_real(&ok);
        if (!ok) {
            frameRejected(frame, frameTime, 4011);
            return;
        }
        if (!FP::defined(value)) {
            frameRejected(frame, frameTime, 4012);
            return;
        }
        int decimals = field.indexOf('.');
        if (decimals < 0)
            decimals = 0;
        else
            decimals = field.size() - (decimals + 1);

        /* Checksum */
        if (fields.empty()) {
            frameRejected(frame, frameTime, 4013);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field.size() != 2) {
                frameRejected(frame, frameTime, 4106);
                return;
            }
        }

        if (config.first().strictMode) {
            if (!fields.empty()) {
                frameRejected(frame, frameTime, 4199);
                return;
            }
        }

        if (isInput(port)) {
            /* Input parameter */
            channel = fromInputPort(port);
            Q_ASSERT(channel >= 0 && channel < TotalChannels);

            switch (responseState) {
            case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
            case RESP_AUTOPROBE_PASSIVE_WAIT:
            case RESP_PASSIVE_WAIT:
            case RESP_PASSIVE_RUN:
            case RESP_INTERACTIVE_RUN_READ_PROCESS:
            case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
            case RESP_INTERACTIVE_RUN_WAIT:
                break;

            default:
                frameRejected(frame, frameTime, 4990);
                return;
            }
        } else {
            /* Output parameter */
            channel = fromOutputPort(port);
            Q_ASSERT(channel >= 0 && channel < TotalChannels);

            switch (parameter) {
            default:
                switch (responseState) {
                case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
                case RESP_AUTOPROBE_PASSIVE_WAIT:
                case RESP_PASSIVE_WAIT:
                case RESP_PASSIVE_RUN:
                case RESP_INTERACTIVE_RUN_READ_PROCESS:
                case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
                case RESP_INTERACTIVE_RUN_WAIT:
                    break;

                default:
                    frameRejected(frame, frameTime, 4991);
                    return;
                }

                /* Setpoint */
            case 1: {
                double check = setpointOutputValues.read().array(channel).toReal();
                double divisor = pow(10.0, (double) decimals);

                {
                    QMap<int, double>::iterator update = setpointUpdate.find(channel);
                    if (update != setpointUpdate.end()) {
                        double rounded = floor(update.value() * divisor) / divisor;
                        if (qFuzzyCompare(rounded, value))
                            setpointUpdate.erase(update);
                    }
                }

                if (!FP::defined(check) ||
                        !qFuzzyCompare(floor(check * divisor) / divisor, value)) {
                    updateLogMeta(frameTime);

                    if (persistentEgress != NULL &&
                            config.first().logOutputs &&
                            FP::defined(setpointOutputValues.getStart())) {
                        setpointOutputValues.setEnd(frameTime);
                        persistentEgress->incomingData(setpointOutputValues);
                    }
                    setpointOutputValues.write().array(channel).setDouble(value);

                    setpointOutputValues.setStart(frameTime);
                    setpointOutputValues.setEnd(FP::undefined());
                    if (realtimeEgress != NULL) {
                        realtimeEgress->incomingData(setpointOutputValues);
                    }

                    if (config.first().logOutputs) {
                        persistentValuesUpdated();
                    }
                }

                if (realtimeEgress != NULL) {
                    QList<OutputVariable> channelVariables(config.first().variables[channel]);
                    for (QList<OutputVariable>::const_iterator var = channelVariables.constBegin(),
                            endVar = channelVariables.constEnd(); var != endVar; ++var) {

                        SequenceValue dv
                                (SequenceName({}, "raw", "ZSP" + var->name), Variant::Root(value),
                                 frameTime, frameTime + 1.0);
                        realtimeEgress->incomingData(dv);
                    }
                    SequenceValue dv(SequenceName({}, "raw", "ZSP" + std::to_string(channel + 1)),
                                     Variant::Root(value), frameTime, frameTime + 1.0);
                    realtimeEgress->incomingData(dv);
                }

                switch (responseState) {
                case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
                case RESP_AUTOPROBE_PASSIVE_WAIT:
                case RESP_PASSIVE_WAIT:
                case RESP_PASSIVE_RUN:
                case RESP_INTERACTIVE_RUN_READ_PROCESS:
                case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
                case RESP_INTERACTIVE_RUN_WAIT:
                    break;

                default:
                    frameRejected(frame, frameTime, 4992);
                    return;
                }
            }
            }
        }

        break;
    }
    }


    /* Second stage: handle the response state/operating mode and transitions
     * based on the response. */
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RUN_READ_PROCESS:
    case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
    case RESP_INTERACTIVE_RUN_WAIT:
        /* Handling continues below */
        break;

    case RESP_INTERACTIVE_START_RESET:
    case RESP_INTERACTIVE_START_ENABLE_SEND:
    case RESP_INTERACTIVE_START_IDENTIFY:
    case RESP_INTERACTIVE_START_READ_PARAMETERS:
    case RESP_INTERACTIVE_START_READ_SETPOINTS:
        /* These should all be caught above. */
        frameRejected(frame, frameTime, 1);
        return;

    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        /* If we get here it's because something else overrode our control,
         * so just retry the startup in a moment. */
        issuedCommand.invalidate();
        commandBacklog.clear();
        timeoutAt(frameTime + 1.0);
        return;
    }


    /* Now we know we're in a normal operating mode. */

    /* Third stage: handle values output */
    updateLogMeta(frameTime);
    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }
    switch (issuedCommand.getType()) {
    case COMMAND_READ_MEASURED: {
        Q_ASSERT(channel >= 0 && channel < TotalChannels);

        remap("ZINPUTS", lastInputs);

        auto inputValue = lastInputs.read().array(channel);
        QList<OutputVariable> channelVariables(config.first().variables[channel]);
        SequenceValue::Transfer outputData;
        for (QList<OutputVariable>::const_iterator var = channelVariables.constBegin(),
                endVar = channelVariables.constEnd(); var != endVar; ++var) {

            auto name = "ZIN" + var->name;
            Variant::Root outputValue(inputValue);
            remap(name, outputValue);

            if (realtimeEgress != NULL) {
                realtimeEgress->emplaceData(SequenceName({}, "raw", name), outputValue, frameTime,
                                            frameTime + 1.0);
            }

            outputValue.write().setDouble(var->calibration.apply(outputValue.read().toDouble()));
            remap(var->name, outputValue);

            outputData.emplace_back(
                    SequenceIdentity({{}, "raw", var->name}, frameTime, frameTime + 1.0),
                    std::move(outputValue));
        }

        logValues(frameTime, LogStream_PV_Begin + channel, std::move(outputData));

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZINPUTS"}, lastInputs, frameTime, frameTime + 1.0));
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "F1"}, Variant::Root(Variant::Type::Flags), frameTime,
                                  frameTime + 1.0));
        }
        break;
    }

    default:
        break;
    }
    for (int i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
        if (streamAge[i] < 2)
            continue;
        if (loggingEgress == NULL)
            continue;

        updateLogMeta(frameTime);

        if (FP::defined(streamTime[LogStream_State])) {
            double startTime = streamTime[LogStream_State];
            double endTime = frameTime;

            loggingMux.incoming(LogStream_State, SequenceValue({{}, "raw", "F1"},
                                                               Variant::Root(Variant::Type::Flags),
                                                               startTime, endTime), loggingEgress);
            loggingMux.incoming(LogStream_State,
                                SequenceValue({{}, "raw", "ZINPUTS"}, lastInputs, startTime,
                                              endTime), loggingEgress);
        } else {
            loggingMux.advance(LogStream_State, frameTime, loggingEgress);
        }
        streamTime[LogStream_State] = frameTime;

        for (i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
            if (streamAge[i] == 0) {
                loggingMux.advance(i, frameTime, loggingEgress);
                streamTime[i] = FP::undefined();
            }
            streamAge[i] = 0;
        }
        break;
    }


    /* Fourth stage: handle interactive control */
    if ((responseState != RESP_INTERACTIVE_RUN_READ_PROCESS &&
            responseState != RESP_INTERACTIVE_RUN_READ_SETPOINTS &&
            responseState != RESP_INTERACTIVE_RUN_WAIT) || controlStream == NULL) {
        if (issuedCommand.getState() >= Command::MultiPort_Begin) {
            issuedCommand.setState(issuedCommand.getState() + 1);
            if (issuedCommand.getState() < Command::MultiPort_End)
                return;
        }

        if (!commandBacklog.isEmpty()) {
            issuedCommand = commandBacklog.takeFirst();
        } else {
            issuedCommand.invalidate();
        }
        return;
    }

    switch (issuedCommand.getType()) {
    case COMMAND_INVALID:
    case COMMAND_MENU:
    case COMMAND_VIEW:
    case COMMAND_IDENTIFY:
        advanceProcessRead(frameTime);
        break;

    case COMMAND_READ_MEASURED:
        if (channel + 1 >= TotalChannels) {
            if (advanceSetpointChange(frameTime))
                break;
            if (advanceSetpointRead(frameTime))
                break;
        }
        advanceProcessRead(frameTime, channel);
        break;

    case COMMAND_READ_PARAMETER:
        if (advanceSetpointRead(frameTime, channel))
            break;
        advanceProcessRead(frameTime, TotalChannels);
        break;

    case COMMAND_SET_PARAMETER:
        if (advanceSetpointChange(frameTime, channel))
            break;
        advanceProcessRead(frameTime, TotalChannels);
        break;
    }
}

bool AcquireBrooksPID0254::handleExpectedTimeout(double frameTime, bool interactionPossible)
{
    if (issuedCommand.getType() == COMMAND_INVALID)
        return false;
    if (issuedCommand.getState() != Command::ExpectedTimeout_Wait)
        return false;

    if (interactionPossible) {
        switch (responseState) {
        case RESP_INTERACTIVE_RUN_READ_PROCESS:
        case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
            advanceProcessRead(frameTime);
            return true;

        case RESP_INTERACTIVE_START_READ_PARAMETERS: {
            int port = issuedCommand.getPort();
            if (isInput(port)) {
                /* Input parameter */
                int channel = fromInputPort(port);
                ++channel;
                if (channel >= TotalChannels) {
                    sendCommand(Command(COMMAND_READ_PARAMETER, "P01?", toOutputPort(0)),
                                frameTime);
                    responseState = RESP_INTERACTIVE_START_READ_SETPOINTS;
                } else {
                    sendCommand(Command(COMMAND_VIEW, "V", toInputPort(channel),
                                        Command::ExpectedTimeout_Begin), frameTime);
                }
                return true;
            } else {
                /* Output parameter */
            }
            break;
        }

        default:
            break;
        }
    }

    if (!commandBacklog.isEmpty()) {
        issuedCommand = commandBacklog.takeFirst();
    } else {
        issuedCommand.invalidate();
    }
    return false;
}

void AcquireBrooksPID0254::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    if (handleExpectedTimeout(frameTime, true))
        return;

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        commandBacklog.clear();
        issuedCommand.invalidate();
        break;
    case RESP_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
        commandBacklog.clear();
        issuedCommand.invalidate();
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            event(frameTime, QObject::tr("Timeout waiting for response.  Communications Dropped."),
                  true, info);
        }

        commandBacklog.clear();
        issuedCommand.invalidate();
        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_RUN_READ_PROCESS:
    case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
        qCDebug(log) << "Timeout in interactive state" << responseState << "at"
                     << Logging::time(frameTime);

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            event(frameTime, QObject::tr("Timeout waiting for response.  Communications Dropped."),
                  true, info);
        }

        initiateInteractiveStart(frameTime);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_RUN_WAIT:
        advanceProcessRead(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        initiateInteractiveStart(frameTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_RESET:
    case RESP_INTERACTIVE_START_ENABLE_SEND:
    case RESP_INTERACTIVE_START_IDENTIFY:
    case RESP_INTERACTIVE_START_READ_PARAMETERS:
    case RESP_INTERACTIVE_START_READ_SETPOINTS:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        commandBacklog.clear();
        issuedCommand.invalidate();
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireBrooksPID0254::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_INTERACTIVE_START_RESET:
        packageAndSend(Util::ByteView("S"));
        responseState = RESP_INTERACTIVE_START_ENABLE_SEND;
        discardData(frameTime + 1.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveEnableSend"),
                                                       frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_ENABLE_SEND:
        sendCommand(Command(COMMAND_IDENTIFY, "I"), frameTime);
        responseState = RESP_INTERACTIVE_START_IDENTIFY;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveIdentify"),
                                  frameTime, FP::undefined()));
        }
        break;

    default:
        break;
    }
}

void AcquireBrooksPID0254::shutdownInProgress(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_READ_PROCESS:
    case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
    case RESP_INTERACTIVE_RUN_WAIT:
        /* Just issue all the commands needed, without waiting for
         * acknowledgments */

        setpointUpdate = config.first().exitSetpoint;
        for (QMap<int, double>::const_iterator set = setpointUpdate.constBegin(),
                endSet = setpointUpdate.constEnd(); set != endSet; ++set) {
            Util::ByteArray packet("P01=");
            packet += QByteArray::number(set.value(), 'f', 3);
            packageAndSend(packet, toOutputPort(set.key()));
        }
        break;

    default:
        /* No interactive communications, so no action needed */
        break;
    }
}


static bool isValidDigit(char d)
{
    return (d >= '0' && d <= '9');
}

static bool controlFraming(const char *&data, int &addr, int &port)
{
    if (*data != 'A') return false;
    ++data;
    if (*data != 'Z') return false;
    ++data;
    if (isValidDigit(*data)) {
        addr = 0;
        while (isValidDigit(*data)) {
            addr *= 10;
            addr += *data - '0';
            ++data;
        }
    }
    if (*data == '.') {
        ++data;
        if (!isValidDigit(*data))
            return false;
        port = 0;
        while (isValidDigit(*data)) {
            port *= 10;
            port += *data - '0';
            ++data;
        }
    }
    return true;
}

void AcquireBrooksPID0254::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    handleExpectedTimeout(frameTime, false);

    int addr = -1;
    int port = -1;
    const char *data = frame.data<const char *>();
    if (!controlFraming(data, addr, port))
        return;

    /* Reset request */
    if (!(*data)) {
        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
            responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
            /* Fall through */
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
            commandBacklog.clear();
            issuedCommand.invalidate();
            discardData(frameTime + 0.9);
            break;

        default:
            break;
        }
        return;
    }

    CommandType type = COMMAND_INVALID;
    int state = 0;
    if (*data == 'M') {
        type = COMMAND_MENU;
        state = Command::ExpectedTimeout_Begin;
    } else if (*data == 'M') {
        type = COMMAND_VIEW;
        state = Command::ExpectedTimeout_Begin;
    } else if (*data == 'I') {
        type = COMMAND_IDENTIFY;
    } else if (*data == 'K') {
        type = COMMAND_READ_MEASURED;
        if (port < 0)
            state = Command::MultiPort_Begin;
    } else if (*data == 'P') {
        if (port < 0)
            state = Command::MultiPort_Begin;
        if (isValidDigit(*(data + 1)) && isValidDigit(*(data + 2))) {
            if (*(data + 3) == '?') {
                type = COMMAND_READ_PARAMETER;
            } else if (*(data + 3) == '=') {
                type = COMMAND_SET_PARAMETER;
            }
        }
    } else if (*data == 'H') {
        /* XOF, no response */
        return;
    } else if (*data == 'S') {
        /* XON, no response */
        return;
    } else if (*data == 'A') {
        /* ACK, no response */
        return;
    } else if (*data == 'N') {
        /* NACK, no response */
        return;
    } else if (*data == 'Z') {
        /* Totalizer reset, no response */
        return;
    } else if (*data == 'F') {
        /* Batch mode handling, which we don't deal with, so just
         * ignore it */
    } else if (*data == 'B') {
        /* Blend mode handling, which we don't deal with, so just
         * ignore it */
    }

    if (type == COMMAND_INVALID)
        return;

    /* Not destined for us, so ignore it if the response includes an address
     * that we could filter on */
    if (addr >= 0 && config.first().address >= 0 && addr != config.first().address) {
        switch (type) {
        case COMMAND_MENU:
        case COMMAND_VIEW:
            /* These are text responses so we can't filter on them */
            break;

        default:
            return;
        }
    }

    if (issuedCommand.getType() == COMMAND_INVALID) {
        issuedCommand = Command(type, Util::ByteArray(data), port, state);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
            responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
            /* Fall through */
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
            timeoutAt(frameTime + 2.0);
            break;

        default:
            break;
        }
    } else {
        commandBacklog.append(Command(type, Util::ByteArray(data), port, state));
    }
}

Variant::Root AcquireBrooksPID0254::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireBrooksPID0254::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN_READ_PROCESS:
    case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
    case RESP_INTERACTIVE_RUN_WAIT:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

int AcquireBrooksPID0254::convertSetpointChannel(const Variant::Read &value)
{
    {
        auto i = Variant::Composite::toInteger(value);
        if (INTEGER::defined(i) && i > 0 && i <= TotalChannels)
            return (int) (i - 1);
    }

    Q_ASSERT(!config.isEmpty());

    const auto &check = value.toString();
    if (!check.empty()) {
        auto i = config.front().setpointNames.find(check);
        if (i != config.front().setpointNames.end() && i->second >= 0 && i->second <= 9)
            return i->second;
    }

    return -1;
}

QHash<int, double> AcquireBrooksPID0254::convertSetpointMultiple(const Variant::Read &value)
{
    switch (value.getType()) {
    case Variant::Type::Hash: {
        QHash<int, double> result;
        for (auto add : value.toHash()) {
            double v = Variant::Composite::toNumber(add.second);
            if (!FP::defined(v))
                v = Variant::Composite::toNumber(add.second.hash("Value"));
            if (!FP::defined(v))
                continue;

            int index = -1;
            auto lookup = config.front().setpointNames.find(add.first);
            if (lookup != config.front().setpointNames.end())
                index = lookup->second;
            if (index == -1) {
                bool ok = false;
                index = QString::fromStdString(add.first).toInt(&ok);
                if (!ok) {
                    index = -1;
                } else {
                    --index;
                }
            }
            if (index < 0 || index >= TotalChannels)
                continue;

            result.insert(index, v);
        }
        return result;
    }
    case Variant::Type::Array: {
        auto children = value.toArray();
        QHash<int, double> result;
        for (int idx = 0, max = std::min<int>(children.size(), TotalChannels); idx < max; ++idx) {
            double v = Variant::Composite::toNumber(children[idx]);
            if (!FP::defined(v))
                v = Variant::Composite::toNumber(children[idx].hash("Value"));
            if (!FP::defined(v))
                continue;
            result.insert(idx, v);
        }
        return result;
    }
    case Variant::Type::String: {
        QStringList channels(value.toQString().split(QRegExp("[:;,]+")));
        QHash<int, double> result;
        for (int idx = 0, max = std::min<int>(channels.size(), TotalChannels); idx < max; ++idx) {
            bool ok = false;
            double v = channels.at(idx).toDouble(&ok);
            if (!ok || FP::defined(v))
                continue;
            result.insert(idx, v);
        }
        return result;
    }

    default:
        break;
    }

    return QHash<int, double>();
}


void AcquireBrooksPID0254::genericOutputCommand(const Variant::Read &command)
{
    if (command.getType() != Variant::Type::Hash) {
        switch (command.getType()) {
        case Variant::Type::Real: {
            double v = command.toDouble();
            auto index = config.front().setpointNames.find(SequenceName::Component());
            if (FP::defined(v) &&
                    index != config.front().setpointNames.end() &&
                    index->second >= 0 &&
                    index->second < TotalChannels) {
                setpointUpdate.insert(index->second, v);
                setpointTarget.insert(index->second, v);
            }
            break;
        }

        default:
            break;
        }
        return;
    }
    int index = convertSetpointChannel(command.hash("Parameters").hash("Channel").hash("Value"));
    if (index == -1) {
        index = convertSetpointChannel(command.hash("Parameters").hash("Channel"));
    }
    if (index == -1) {
        index = convertSetpointChannel(command.hash("Channel"));
    }
    if (index == -1) {
        index = convertSetpointChannel(command.hash("Parameters").hash("Index").hash("Value"));
    }
    if (index == -1) {
        index = convertSetpointChannel(command.hash("Parameters").hash("Index"));
    }
    if (index == -1) {
        index = convertSetpointChannel(command.hash("Index"));
    }
    if (index != -1) {
        double value = FP::undefined();
        if (command.hash("Parameters").hash("Value").hash("Value").getType() ==
                Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").hash("Value").toDouble();
        }
        if (!FP::defined(value) &&
                command.hash("Parameters").hash("Value").getType() == Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").toDouble();
        }
        if (!FP::defined(value) && command.hash("Value").getType() == Variant::Type::Real) {
            value = command.hash("Value").toDouble();
        }
        if (FP::defined(value)) {
            setpointUpdate.insert(index, value);
            setpointTarget.insert(index, value);
            return;
        }
    }

    auto name = command.hash("Parameters").hash("Name").hash("Value").toString();
    if (name.empty()) {
        name = command.hash("Parameters").hash("Name").toString();
    }
    if (name.empty()) {
        name = command.hash("Name").toString();
    }

    {
        double value = FP::undefined();
        if (command.hash("Parameters").hash("Value").hash("Value").getType() ==
                Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").hash("Value").toDouble();
        }
        if (!FP::defined(value) &&
                command.hash("Parameters").hash("Value").getType() == Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").toDouble();
        }
        if (!FP::defined(value) && command.hash("Value").getType() == Variant::Type::Real) {
            value = command.hash("Value").toDouble();
        }
        if (!FP::defined(value) &&
                command.hash("Parameters").hash("Output").hash("Value").getType() ==
                        Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").hash("Value").toDouble();
        }
        if (!FP::defined(value) &&
                command.hash("Parameters").hash("Output").getType() == Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").toDouble();
        }
        if (!FP::defined(value) && command.hash("Value").getType() == Variant::Type::Real) {
            value = command.hash("Output").toDouble();
        }
        if (FP::defined(value)) {
            auto i = config.front().setpointNames.find(name);
            if (i != config.front().setpointNames.end() && i->second >= 0 && i->second < TotalChannels) {
                setpointUpdate.insert(i->second, value);
                setpointTarget.insert(i->second, value);
                return;
            }
        }
    }

    for (auto check : command.toHash()) {
        index = -1;
        auto lookup = config.front().setpointNames.find(check.first);
        if (lookup != config.front().setpointNames.end())
            index = lookup->second;
        if (index != -1) {
            double value = Variant::Composite::toNumber(check.second);
            if (!FP::defined(value))
                value = Variant::Composite::toNumber(check.second.hash("Value"));
            if (FP::defined(value)) {
                setpointUpdate.insert(index, value);
                setpointTarget.insert(index, value);
                continue;
            }
        }

        bool ok = false;
        index = QString::fromStdString(check.first).toInt(&ok);
        if (!ok)
            continue;
        --index;

        auto base = check.second;
        if (base.getType() == Variant::Type::Hash)
            base = base.hash("Value");

        switch (base.getType()) {
        case Variant::Type::Real: {
            if (index < 0 || index >= TotalChannels)
                break;
            double value = base.toDouble();
            if (FP::defined(value)) {
                setpointUpdate.insert(index, value);
                setpointTarget.insert(index, value);
            }
            break;
        }
        default:
            break;
        }
    }
}

void AcquireBrooksPID0254::command(const Variant::Read &command)
{
    if (command.hash("Setpoint").exists()) {
        int index = convertSetpointChannel(
                command.hash("Setpoint").hash("Parameters").hash("Channel").hash("Value"));
        if (index == -1) {
            index = convertSetpointChannel(
                    command.hash("Setpoint").hash("Parameters").hash("Channel"));
        }
        if (index == -1) {
            index = convertSetpointChannel(command.hash("Setpoint").hash("Channel"));
        }
        if (index != -1) {
            double value = Variant::Composite::toNumber(
                    command.hash("Setpoint").hash("Parameters").hash("Value").hash("Value"));
            if (!FP::defined(value)) {
                value = Variant::Composite::toNumber(
                        command.hash("Setpoint").hash("Parameters").hash("Value"));
            }
            if (!FP::defined(value)) {
                value = Variant::Composite::toNumber(command.hash("Setpoint").hash("Value"));
            }

            if (FP::defined(value)) {
                setpointUpdate.insert(index, value);
                setpointTarget.insert(index, value);
            }
        } else {
            QHash<int, double>
                    values(convertSetpointMultiple(command.hash("Setpoint").hash("Parameters")));
            if (values.isEmpty()) {
                values = convertSetpointMultiple(command.hash("Setpoint"));
            }
            if (!values.isEmpty()) {
                for (QHash<int, double>::const_iterator add = values.constBegin(),
                        endAdd = values.constEnd(); add != endAdd; ++add) {
                    setpointUpdate.insert(add.key(), add.value());
                    setpointTarget.insert(add.key(), add.value());
                }
            }
        }
    }

    if (command.hash("Output").exists()) {
        genericOutputCommand(command.hash("Output"));
    }

    if (command.hash("Bypass").toBool()) {
        Q_ASSERT(!config.isEmpty());
        for (QMap<int, double>::const_iterator add = config.first().bypassSetpoint.constBegin(),
                endAdd = config.first().bypassSetpoint.constEnd(); add != endAdd; ++add) {
            setpointUpdate.insert(add.key(), add.value());
            setpointTarget.insert(add.key(), add.value());
        }
    }
    if (command.hash("UnBypass").toBool()) {
        Q_ASSERT(!config.isEmpty());
        for (QMap<int, double>::const_iterator add = config.first().unbypassSetpoint.constBegin(),
                endAdd = config.first().unbypassSetpoint.constEnd(); add != endAdd; ++add) {
            setpointUpdate.insert(add.key(), add.value());
            setpointTarget.insert(add.key(), add.value());
        }
    }
}

Variant::Root AcquireBrooksPID0254::getCommands()
{
    Variant::Root result;

    result["Setpoint"].hash("DisplayName").setString("Set an &Setpoint");
    result["Setpoint"].hash("ToolTip").setString("Change the current value of a rate setpoint.");
    result["Setpoint"].hash("Parameters").hash("Channel").hash("Name").setString("Channel");
    result["Setpoint"].hash("Parameters").hash("Channel").hash("Format").setString("0");
    result["Setpoint"].hash("Parameters").hash("Channel").hash("Minimum").setInt64(1);
    result["Setpoint"].hash("Parameters").hash("Channel").hash("Maximum").setInt64(TotalChannels);
    result["Setpoint"].hash("Parameters").hash("Channel").hash("Default").setInt64(1);
    result["Setpoint"].hash("Parameters").hash("Value").hash("Name").setString("Output");
    result["Setpoint"].hash("Parameters").hash("Value").hash("Format").setString("0.000");
    result["Setpoint"].hash("Parameters").hash("Value").hash("Default").setReal(0.0);

    return result;
}

AcquisitionInterface::AutoprobeStatus AcquireBrooksPID0254::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

SequenceValue::Transfer AcquireBrooksPID0254::getPersistentValues()
{
    PauseLock paused(*this);
    SequenceValue::Transfer result;

    Q_ASSERT(!config.isEmpty());

    if (config.first().logOutputs) {
        if (FP::defined(setpointOutputValues.getStart())) {
            result.emplace_back(setpointOutputValues);
        }
    }

    return result;
}

void AcquireBrooksPID0254::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_WAIT:
    case RESP_INTERACTIVE_RUN_READ_PROCESS:
    case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_RESET:
    case RESP_INTERACTIVE_START_ENABLE_SEND:
    case RESP_INTERACTIVE_START_IDENTIFY:
    case RESP_INTERACTIVE_START_READ_PARAMETERS:
    case RESP_INTERACTIVE_START_READ_SETPOINTS:
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        initiateInteractiveStart(time);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireBrooksPID0254::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    issuedCommand.invalidate();
    commandBacklog.clear();
    discardData(time + 0.5, 1);
    timeoutAt(time + 8.0);
    generalStatusUpdated();
}

void AcquireBrooksPID0254::autoprobePromote(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Promoted from interactive wait to interactive acquisition at"
                     << Logging::time(time);
        timeoutAt(time);
        break;

    case RESP_INTERACTIVE_RUN_READ_PROCESS:
    case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
    case RESP_INTERACTIVE_START_RESET:
    case RESP_INTERACTIVE_START_ENABLE_SEND:
    case RESP_INTERACTIVE_START_IDENTIFY:
    case RESP_INTERACTIVE_START_READ_PARAMETERS:
    case RESP_INTERACTIVE_START_READ_SETPOINTS:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 2.0);
        break;

    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        initiateInteractiveStart(time);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireBrooksPID0254::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 2.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_RUN_READ_PROCESS:
    case RESP_INTERACTIVE_RUN_READ_SETPOINTS:
    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_WAIT;
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireBrooksPID0254::getDefaults()
{
    AutomaticDefaults result;
    result.name = "X$2";
    result.setSerialN81(9600);
    return result;
}


ComponentOptions AcquireBrooksPID0254Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireBrooksPID0254Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data using all defaults.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireBrooksPID0254Component::createAcquisitionPassive(const ComponentOptions &options,
                                                                                      const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireBrooksPID0254(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireBrooksPID0254Component::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                      const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireBrooksPID0254(config, loggingContext)); }

std::unique_ptr<AcquisitionInterface> AcquireBrooksPID0254Component::createAcquisitionAutoprobe(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireBrooksPID0254> i(new AcquireBrooksPID0254(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireBrooksPID0254Component::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireBrooksPID0254> i(new AcquireBrooksPID0254(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
