/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <map>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    int address;

    double resendTime;
    int resendTry;
    QByteArray lastLineSent;

    double pv[4];
    double sp[4];

    ModelInstrument()
            : incoming(),
              outgoing(),
              address(-1),
              resendTime(FP::undefined()),
              resendTry(-1),
              lastLineSent()
    {
        for (int i = 0; i < 4; i++) {
            pv[i] = 10.0 + i * 2.0;
            sp[i] = 11.0 + i * 2.0;
        }
    }

    static bool isValidDigit(char d)
    {
        return (d >= '0' && d <= '9');
    }

    static bool incomingFraming(QByteArray &line, int &addr, int &port)
    {
        if (line.at(0) != 'A') return false;
        if (line.at(1) != 'Z') return false;
        line = line.mid(2);

        if (isValidDigit(line.at(0))) {
            addr = 0;
            while (isValidDigit(line.at(0))) {
                addr *= 10;
                addr += line.at(0) - '0';
                line = line.mid(1);
            }
        }
        if (line.at(0) == '.') {
            line = line.mid(1);
            if (!isValidDigit(line.at(0)))
                return false;
            port = 0;
            while (isValidDigit(line.at(0))) {
                port *= 10;
                port += line.at(0) - '0';
                line = line.mid(1);
            }
        }
        return true;
    }

    void outputView(int port)
    {
        resendTry = -1;
        lastLineSent.clear();
        resendTime = FP::undefined();

        switch (port) {
        case 1:
        case 3:
        case 5:
        case 7:
            outgoing.append("PROGRAM VALUES - Channel ");
            outgoing.append(QByteArray::number(port / 2));
            outgoing.append(" - Port ");
            outgoing.append(QByteArray::number(port).rightJustified(2, '0'));
            outgoing.append("\r\n\n"
                            "<04> Measure Units                 l\r\n"
                            "<10> Time Base                   min\r\n"
                            "<03> Decimal Point              x.xx\r\n"
                            "<27> Gas Factor                1.000\r\n"
                            "<28> Log Type                    Off\r\n"
                            "<00> PV Signal Type           0-20mA\r\n"
                            "<00> PV Full Scale         20.00 l/m\r\n");
            break;

        case 2:
        case 4:
        case 6:
        case 8:
            outgoing.append("PROGRAM VALUES - Channel ");
            outgoing.append(QByteArray::number(port / 2 - 1));
            outgoing.append(" - Port ");
            outgoing.append(QByteArray::number(port).rightJustified(2, '0'));
            outgoing.append("\r\n\n"
                            "<00> SP Signal Type          0-20mA\r\n"
                            "<09> SP Full Scale        20.00 l/m\r\n"
                            "<02> SP Function               Rate\r\n"
                            "<01> SP Rate               0.00 l/m\r\n"
                            "<29> SP VOR                  Normal\r\n"
                            "<44> SP Batch                0.00 l\r\n"
                            "<45> SP Blend               0.000 %\r\n"
                            "<46> SP Source               Keypad\r\n");
            break;

        case 9:
            outgoing.append("PROGRAM VALUES - Channel Global\r\n");
            outgoing.append("\r\n\n"
                            "<39> Audio Beep                 On\r\n"
                            "<32> Zero Supress               On\r\n"
                            "<33> Pwr SP Clear              Off\r\n"
                            "<43> Record Count          0000000\r\n"
                            "<25> Sample Rate           535 sec\r\n"
                            "<22> Date-Time    00Jan00 00:00:00\r\n"
                            "<17> Network Addr           ");
            outgoing.append(QByteArray::number(address >= 0 ? address : 0).rightJustified(5, '0'));
            outgoing.append("\r\n");
            break;
        default:
            break;
        }
    }

    void outputChecksumed(QByteArray line, int port = -1, bool portPeriod = false)
    {
        {
            QByteArray
                    prefix(QByteArray::number(address >= 0 ? address : 0).rightJustified(5, '0'));
            if (port >= 0) {
                if (portPeriod) {
                    prefix.append('.');
                } else {
                    prefix.append(',');
                }
                prefix.append(QByteArray::number(port).rightJustified(2, '0'));
            }

            line.prepend(',');
            line.prepend(prefix);
            line.prepend(',');
        }
        line.append(',');

        quint32 sum = 0;
        for (const uchar *add = (const uchar *) line.constBegin(),
                *endAdd = (const uchar *) line.constEnd(); add != endAdd; ++add) {
            sum += *add;
            sum &= 0xFF;
        }
        sum = (~sum + 1) & 0xFF;

        line.prepend("AZ");
        line.append(QByteArray::number((quint8) sum, 16).toUpper().rightJustified(2, '0'));
        line.append("\r\n");
        outgoing.append(line);

        resendTime = 4.0;
        resendTry = 0;
        lastLineSent = line;
    }

    void advance(double seconds)
    {
        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR));
            incoming = incoming.mid(idxCR + 1);

            /* qmake freaks out if you put an literal escape character
             * in a string, so do this the hard way */
            if (line.startsWith((char) 0x1B) && line.mid(1) == "AZ") {
                resendTime = FP::undefined();
                resendTry = -1;
                lastLineSent.clear();
                continue;
            }

            int addr = -1;
            int port = -1;
            if (!incomingFraming(line, addr, port))
                continue;
            if (address != addr)
                continue;

            if (line == "M") {
                if (port != -1)
                    continue;
                outgoing.append("\r\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
                                "Brooks Instruments\r\n"
                                "COMMANDS - Model 0254 V01.03.21 FD00\r\n"
                                "\r\n\n"
                                "[M] Command Select Menu\r\n"
                                "[V] Display Port Values\r\n"
                                "[K] Send Measured\r\n"
                                "[I] Product Identify\r\n"
                                "[P] Program New Values\r\n"
                                "[Z] Clear Measured Values\r\n"
                                "[F] Batch Control Service\r\n"
                                "[B] Blend Control Service\r\n"
                                "[G] Log Record Service\r\n");
                resendTry = -1;
                lastLineSent.clear();
                resendTime = FP::undefined();
            } else if (line == "V") {
                if (port >= 0) {
                    outputView(port);
                } else {
                    for (int i = 1; i <= 9; i++) {
                        outputView(i);
                    }
                }
                resendTry = -1;
            } else if (line == "I") {
                if (port != -1)
                    continue;
                outputChecksumed("4,BROOKS,0254,08,01.01.13,FE00");
            } else if (line == "A") {
                resendTry = -1;
                lastLineSent.clear();
                resendTime = FP::undefined();
            } else if (line == "N") {
                if (!lastLineSent.isEmpty()) {
                    resendTry = 0;
                    resendTime = 4.0;
                    outgoing.append(lastLineSent);
                }
            } else if (line == "K") {
                if ((port % 2) == 1 && port >= 1 && port <= 7) {
                    int channel = port / 2;
                    QByteArray toSend("4,xxxxxxxx.xx,00162871.43,");
                    toSend.append(QByteArray::number(pv[channel], 'f', 2).rightJustified(11, ' '));
                    toSend.append(",xxxxxxx.xx,xxxxx,X,X,X,X,X");
                    outputChecksumed(toSend, port, true);
                } else if (port == -1) {
                    for (int channel = 0; channel < 4; ++channel) {
                        QByteArray toSend("4,xxxxxxxx.xx,00162871.43,");
                        toSend.append(
                                QByteArray::number(pv[channel], 'f', 2).rightJustified(11, ' '));
                        toSend.append(",xxxxxxx.xx,xxxxx,X,X,X,X,X");
                        outputChecksumed(toSend, port, true);
                    }
                }
            } else if (line.startsWith("Z")) {
                /* Reset totalizer, ignored */
            } else if (line.startsWith("F")) {
                /* Batching, ignored */
            } else if (line.startsWith("B")) {
                /* Blending, ignored */
            } else if (line.startsWith("P") && line.length() >= 4) {
                bool ok = false;
                int parameter = line.mid(1, 2).toInt(&ok);
                if (ok && parameter >= 0 && parameter <= 99) {
                    double value = FP::undefined();

                    if (line.at(3) == '?') {
                        switch (parameter) {
                        case 1:
                            if ((port % 2) == 0 && port >= 2 && port <= 8) {
                                value = sp[port / 2 - 1];
                            }
                            break;

                        default:
                            break;
                        }
                    } else if (line.at(3) == '=' && line.length() >= 5) {
                        value = line.mid(4).toDouble(&ok);
                        if (ok && FP::defined(value)) {
                            switch (parameter) {
                            case 1:
                                if ((port % 2) == 0 && port >= 2 && port <= 8) {
                                    sp[port / 2 - 1] = floor(value * 100.0) / 100.0;
                                }
                                break;

                            default:
                                break;
                            }
                        }
                    } else {
                        continue;
                    }

                    switch (parameter) {
                    case 1:
                        if ((port % 2) == 0 && port >= 2 && port <= 8) {
                            QByteArray toSend("4,P01,");
                            toSend.append(
                                    QByteArray::number(value, 'f', 2).rightJustified(11, ' '));
                            outputChecksumed(toSend, port, true);
                        }
                        break;

                    default:
                        break;
                    }
                }
            }
        }

        if (resendTry >= 0 && FP::defined(resendTime) && !lastLineSent.isEmpty()) {
            resendTime -= seconds;
            if (resendTime <= 0.0) {
                ++resendTry;
                if (resendTry > 4) {
                    resendTry = -1;
                    resendTime = FP::undefined();
                    lastLineSent.clear();
                } else {
                    resendTime = 4.0;
                    outgoing.append(lastLineSent);
                }
            }
        }
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;

    bool checkMeta(StreamCapture &stream,
                   const ModelInstrument &model,
                   double time = FP::undefined())
    {
        Q_UNUSED(model);
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZINPUTS", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZOUTPUTS", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkVariableMeta(StreamCapture &stream,
                           const SequenceName::Component &variable,
                           int channel,
                           double time = FP::undefined())
    {
        if (!stream.hasMeta(variable, Variant::Root(channel), "^Channel", time))
            return false;
        return true;
    }

    bool checkVariableMeta(StreamCapture &stream,
                           const std::map<SequenceName::Component, int> &variables,
                           double time = FP::undefined())
    {
        for (const auto &check : variables) {
            if (!checkVariableMeta(stream, check.first, check.second, time))
                return false;
        }
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream,
                           const ModelInstrument &model,
                           double time = FP::undefined())
    {
        Q_UNUSED(model);
        if (!stream.hasMeta("ZSTATE", Variant::Root(), QString(), time))
            return false;
        for (int i = 0; i < 4; i++) {
            auto idx = std::to_string(i + 1);
            if (!stream.hasMeta("ZIN" + idx, Variant::Root(), QString(), time))
                return false;
            if (!stream.hasMeta("ZSP" + idx, Variant::Root(), QString(), time))
                return false;
        }
        return true;
    }

    bool checkRealtimeVariableMeta(StreamCapture &stream,
                                   const SequenceName::Component &variable,
                                   int channel,
                                   double time = FP::undefined())
    {
        if (!stream.hasMeta("ZIN" + variable, Variant::Root(channel), "^Channel", time))
            return false;
        if (!stream.hasMeta("ZSP" + variable, Variant::Root(channel), "^Channel", time))
            return false;
        return true;
    }

    bool checkRealtimeVariableMeta(StreamCapture &stream,
                                   const std::map<SequenceName::Component, int> &variables,
                                   double time = FP::undefined())
    {
        for (const auto &check : variables) {
            if (!checkRealtimeVariableMeta(stream, check.first, check.second, time))
                return false;
        }
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("ZINPUTS"))
            return false;
        return true;
    }

    bool checkVariableContiguous(StreamCapture &stream, const SequenceName::Component &variable)
    {
        if (!stream.checkContiguous(variable))
            return false;
        return true;
    }

    bool checkVariableContiguous(StreamCapture &stream,
                                 const std::map<SequenceName::Component, int> &variables)
    {
        for (const auto &check : variables) {
            if (!checkVariableContiguous(stream, check.first))
                return false;
        }
        return true;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        {
            Variant::Write inputs = Variant::Write::empty();
            for (int i = 0; i < 4; i++) {
                inputs.array(i).setDouble(model.pv[i]);
            }
            if (!stream.hasAnyMatchingValue("ZINPUTS", inputs, time))
                return false;
        }
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             bool checkSetpoints = true,
                             double time = FP::undefined())
    {
        for (int i = 0; i < 4; i++) {
            auto idx = std::to_string(i + 1);
            if (!stream.hasAnyMatchingValue("ZIN" + idx, Variant::Root(model.pv[i]), time))
                return false;
            if (checkSetpoints &&
                    !stream.hasAnyMatchingValue("ZSP" + idx, Variant::Root(model.sp[i]), time))
                return false;
        }
        return true;
    }

    bool checkOutputValues(StreamCapture &stream,
                           const ModelInstrument &model,
                           double time = FP::undefined())
    {
        {
            Variant::Write outputs = Variant::Write::empty();
            for (int i = 0; i < 0; i++) {
                outputs.array(i).setDouble(model.sp[i]);
            }
            if (!stream.hasAnyMatchingValue("ZOUTPUTS", outputs, time))
                return false;
        }
        return true;
    }

    bool checkVariableValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             const std::map<SequenceName::Component, int> &variables,
                             double time = FP::undefined())
    {
        for (const auto &check : variables) {
            if (!stream.hasAnyMatchingValue(check.first, Variant::Root(model.pv[check.second]),
                                            time))
                return false;
        }
        return true;
    }

    bool checkRealtimeVariableValues(StreamCapture &stream,
                                     const ModelInstrument &model,
                                     const std::map<SequenceName::Component, int> &variables,
                                     bool checkSetpoints = true,
                                     double time = FP::undefined())
    {
        for (const auto &check : variables) {
            if (!stream.hasAnyMatchingValue("ZIN" + check.first,
                                            Variant::Root(model.pv[check.second]), time))
                return false;
            if (checkSetpoints &&
                    !stream.hasAnyMatchingValue("ZSP" + check.first,
                                                Variant::Root(model.sp[check.second]), time))
                return false;
        }
        return true;
    }

    void configureVariables(Variant::Root &cv,
                            const std::map<SequenceName::Component, int> &variables)
    {
        for (const auto &check : variables) {
            cv["Variables"].hash(check.first).hash("Channel").setInt64(check.second + 1);
        }
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_brooks_pid0254"));
        QVERIFY(component);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        std::map<SequenceName::Component, int> variables{{"T_V11", 0},
                                                         {"U_V11", 1},};
        configureVariables(cv, variables);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 10; i++) {
            control.externalControl("AZ.02P01?\r");
            control.externalControl("AZ.04P01?\r");
            control.externalControl("AZ.06P01?\r");
            control.externalControl("AZ.08P01?\r");
            control.externalControl("AZ.01K\r");
            control.externalControl("AZ.03K\r");
            control.externalControl("AZ.05K\r");
            control.externalControl("AZ.07K\r");
            control.externalControl("AZA\r");
        }
        while (control.time() < 10.0) {
            control.advance(0.125);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 10.0);

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture persistent;
        StreamCapture realtime;

        interface->setRealtimeEgress(&realtime);
        interface->setLoggingEgress(&logging);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        for (int i = 0; i < 10; i++) {
            control.externalControl("AZ.02P01?\r");
            control.advance(0.125);
            control.externalControl("AZ.04P01?\r");
            control.advance(0.125);
            control.externalControl("AZ.06P01?\r");
            control.advance(0.125);
            control.externalControl("AZ.08P01?\r");
            control.advance(0.125);
            control.externalControl("AZ.01K\r");
            control.advance(0.125);
            control.externalControl("AZ.03K\r");
            control.advance(0.125);
            control.externalControl("AZ.05K\r");
            control.advance(0.125);
            control.externalControl("AZ.07K\r");
            control.advance(0.125);
            control.externalControl("AZA\r");
        }
        while (control.time() < 60.0) {
            control.advance(0.125);
            if (logging.hasAnyMatchingValue("F1", Variant::Root(), FP::undefined()))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        for (int i = 0; i < 30; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkVariableMeta(logging, variables));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));
        QVERIFY(checkVariableMeta(realtime, variables));
        QVERIFY(checkRealtimeVariableMeta(realtime, variables));

        QVERIFY(checkContiguous(logging));
        QVERIFY(checkVariableContiguous(logging, variables));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));

        QVERIFY(checkVariableValues(logging, instrument, variables));
        QVERIFY(checkVariableValues(realtime, instrument, variables));
        QVERIFY(checkRealtimeVariableValues(realtime, instrument, variables));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        std::map<SequenceName::Component, int> variables{{"T_V11", 1},
                                                         {"U_V11", 2},};
        configureVariables(cv, variables);
        cv["Address"].setInt64(123);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.address = 123;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (logging.hasAnyMatchingValue("F1", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.5);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("F1", Variant::Root(), FP::undefined()));

        for (int i = 0; i < 30; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkVariableMeta(logging, variables));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));
        QVERIFY(checkVariableMeta(realtime, variables));
        QVERIFY(checkRealtimeVariableMeta(realtime, variables));

        QVERIFY(checkContiguous(logging));
        QVERIFY(checkVariableContiguous(logging, variables));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));

        QVERIFY(checkVariableValues(logging, instrument, variables));
        QVERIFY(checkVariableValues(realtime, instrument, variables));
        QVERIFY(checkRealtimeVariableValues(realtime, instrument, variables));
    }

    void interactiveRun()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        std::map<SequenceName::Component, int> variables{{"T_V11", 1},
                                         {"U_V11", 2}};
        configureVariables(cv, variables);
        cv["SetpointOutputs/AOut1"].setInt64(1);
        cv["Initialize/Setpoint"].setString("15,16,17,18");
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));

        instrument.pv[1] = 2.25;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("T_V11", Variant::Root(2.25)))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("T_V11", Variant::Root(2.25)));

        Variant::Write aout = Variant::Write::empty();
        aout.array(0).setDouble(15);
        aout.array(1).setDouble(16);
        aout.array(2).setDouble(17);
        aout.array(3).setDouble(18);
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZOUTPUTS", aout))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZOUTPUTS", aout));

        Variant::Write cmd = Variant::Write::empty();
        cmd["Setpoint/2"].setDouble(4.0);
        cmd["Setpoint/AOut1"].setDouble(4.5);
        aout.array(0).setDouble(4.5);
        aout.array(1).setDouble(4.0);
        interface->incomingCommand(cmd);
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZOUTPUTS", aout))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZOUTPUTS", aout));
        QCOMPARE(instrument.sp[0], 4.5);
        QCOMPARE(instrument.sp[1], 4.0);

        cmd.setEmpty();
        cmd["Setpoint/Parameters/Channel/Value"].setInt64(3);
        cmd["Setpoint/Parameters/Value/Value"].setDouble(0.5);
        aout.array(2).setDouble(0.5);
        interface->incomingCommand(cmd);
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZOUTPUTS", aout))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZOUTPUTS", aout));
        QCOMPARE(instrument.sp[2], 0.5);

        cmd.setEmpty();
        cmd["Output/AOut1"].setDouble(1.5);
        aout.array(0).setDouble(1.5);
        interface->incomingCommand(cmd);
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZOUTPUTS", aout))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZOUTPUTS", aout));
        QCOMPARE(instrument.sp[0], 1.5);

        for (int i = 0; i < 30; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkVariableMeta(logging, variables));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));
        QVERIFY(checkVariableMeta(realtime, variables));
        QVERIFY(checkRealtimeVariableMeta(realtime, variables));

        QVERIFY(checkContiguous(logging));
        QVERIFY(checkVariableContiguous(logging, variables));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkOutputValues(realtime, instrument));

        QVERIFY(checkVariableValues(logging, instrument, variables));
        QVERIFY(checkVariableValues(realtime, instrument, variables));
        QVERIFY(checkRealtimeVariableValues(realtime, instrument, variables));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
