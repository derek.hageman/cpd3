/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtEndian>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_eigenbrodt_nmo191.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

AcquireEigenbrodtNMO191::Configuration::Configuration() : start(FP::undefined()),
                                                          end(FP::undefined()),
                                                          address("SN"),
                                                          pollInterval(1.0),
                                                          strictMode(true)
{ }

AcquireEigenbrodtNMO191::Configuration::Configuration(const Configuration &other,
                                                      double s,
                                                      double e) : start(s),
                                                                  end(e),
                                                                  address(other.address),
                                                                  pollInterval(other.pollInterval),
                                                                  strictMode(other.strictMode)
{ }

void AcquireEigenbrodtNMO191::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Eigenbrodt");
    instrumentMeta["Model"].setString("NMO191");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    forceRealtimeStateEmit = true;

    priorBucketTips = FP::undefined();
    priorFunnelError = false;
    priorPrecipitationState = false;

    persistentFn.setUnit(SequenceName({}, "raw", "Fn"));
}

AcquireEigenbrodtNMO191::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s), end(e), address("SN"), pollInterval(1.0), strictMode(true)
{
    setFromSegment(other);
}

AcquireEigenbrodtNMO191::Configuration::Configuration(const Configuration &under,
                                                      const ValueSegment &over,
                                                      double s,
                                                      double e) : start(s),
                                                                  end(e),
                                                                  address(under.address),
                                                                  pollInterval(under.pollInterval),
                                                                  strictMode(under.strictMode)
{
    setFromSegment(over);
}

void AcquireEigenbrodtNMO191::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();
    if (FP::defined(config["PollInterval"].toDouble()))
        pollInterval = config["PollInterval"].toDouble();

    switch (config["Address"].getType()) {
    default:
        break;
    case Variant::Type::String:
        address = config["Address"].toQString().toUtf8();
        break;
    case Variant::Type::Bytes:
        address = config["Address"].toBytes();
        break;
    case Variant::Type::Integer:
        address = QByteArray::number(config["Address"].toInt64());
        break;
    }
}

AcquireEigenbrodtNMO191::AcquireEigenbrodtNMO191(const ValueSegment::Transfer &configData,
                                                 const std::string &loggingContext)
        : FramedInstrument("nmo191", loggingContext),
          lastRecordTime(FP::undefined()),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          responseState(RESP_PASSIVE_WAIT),
          autoprobeValidRecords(0)
{
    setDefaultInvalid();
    config.append(Configuration());

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireEigenbrodtNMO191::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void AcquireEigenbrodtNMO191::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireEigenbrodtNMO191Component::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireEigenbrodtNMO191::AcquireEigenbrodtNMO191(const ComponentOptions &options,
                                                 const std::string &loggingContext)
        : FramedInstrument("nmo191", loggingContext),
          lastRecordTime(FP::undefined()),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          responseState(RESP_PASSIVE_WAIT),
          autoprobeValidRecords(0)
{
    setDefaultInvalid();
    config.append(Configuration());
    config.first().strictMode = false;
}

AcquireEigenbrodtNMO191::~AcquireEigenbrodtNMO191() = default;

void AcquireEigenbrodtNMO191::logValue(double startTime,
                                       double endTime,
                                       SequenceName::Component name,
                                       Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress) return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireEigenbrodtNMO191::realtimeValue(double time,
                                            SequenceName::Component name,
                                            Variant::Root &&value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

void AcquireEigenbrodtNMO191::invalidateLogValues(double frameTime)
{
    autoprobeValidRecords = 0;
    lastRecordTime = FP::undefined();
    priorBucketTips = FP::undefined();
    loggingLost(frameTime);
}

SequenceValue::Transfer AcquireEigenbrodtNMO191::buildLogMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_eigenbrodt_nmo191");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["SerialNumber"].exists())
        processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);

    result.emplace_back(SequenceName({}, "raw_meta", "WI"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.000");
    result.back().write().metadataReal("Units").setString("mm/h");
    result.back().write().metadataReal("Description").setString("Precipitation rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Precipitation"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);

    result.emplace_back(SequenceName({}, "raw_meta", "ZPH"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("GroupUnits").setString("pH");
    result.back().write().metadataReal("Description").setString("Precipitation pH");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("pH"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);

    result.emplace_back(SequenceName({}, "raw_meta", "ZConductivity"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("\xCE\xBCS/cm");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Precipitation electrical conductivity");
    result.back().write().metadataReal("ReportT").setDouble(25.0);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Conductivity"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(3);

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Internal temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Internal"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(4);

    result.emplace_back(SequenceName({}, "raw_meta", "Fn"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataInteger("Format").setString("00");
    result.back().write().metadataInteger("GroupUnits").setString("BottleNumber");
    result.back()
          .write()
          .metadataInteger("Description")
          .setString("Active collection bottle number");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataInteger("Realtime").hash("Persistent").setBool(true);
    result.back()
          .write()
          .metadataInteger("Realtime")
          .hash("Name")
          .setString(QObject::tr("Collection bottle"));
    result.back().write().metadataInteger("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("ContaminateSensorDry")
          .hash("Description")
          .setString("Data contaminated because there is no precipitation to measure");
    result.back().write().metadataSingleFlag("ContaminateSensorDry").hash("Bits").setInt64(0x000F);
    result.back()
          .write()
          .metadataSingleFlag("ContaminateSensorDry")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_eigenbrodt_nmo191");

    result.back()
          .write()
          .metadataSingleFlag("FunnelError")
          .hash("Description")
          .setString("Funnel door error detected");
    result.back()
          .write()
          .metadataSingleFlag("FunnelError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_eigenbrodt_nmo191");

    return result;
}

SequenceValue::Transfer AcquireEigenbrodtNMO191::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_eigenbrodt_nmo191");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["SerialNumber"].exists())
        processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(8);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("FunnelError")
          .setString(QObject::tr("FUNNEL DOOR ERROR"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("PrecipitationDetected")
          .setString(QObject::tr("Sampling precipitation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetDate")
          .setString(QObject::tr("STARTING COMMS: Setting instrument date"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetTime")
          .setString(QObject::tr("STARTING COMMS: Setting instrument time"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveStartReadFirstRecord")
          .setString(QObject::tr("STARTING COMMS: Waiting for first record"));

    return result;
}

SequenceMatch::Composite AcquireEigenbrodtNMO191::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    sel.append({}, {}, "Fn");
    return sel;
}

static double calculateRainIntensity(double tips, double startTime, double endTime)
{
    if (!FP::defined(tips) || !FP::defined(startTime) || !FP::defined(endTime))
        return FP::undefined();
    if (tips <= 0.0)
        return 0.0;
    double dT = endTime - startTime;
    if (dT <= 0.0)
        return FP::undefined();
    return (tips * 0.05) / (dT / 3600.0);
}

int AcquireEigenbrodtNMO191::processRecord(const Util::ByteView &frame, double &frameTime)
{
    if (frame.size() < 3)
        return 1;

    auto fields = Util::as_deque(frame.split(';'));
    bool ok = false;

    if (fields.empty()) return 2;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (!field.empty() && field != "SN") {
        Variant::Root oldValue(instrumentMeta["SerialNumber"]);
        auto sn = field.parse_i64(&ok);
        if (ok && INTEGER::defined(sn)) {
            instrumentMeta["SerialNumber"].setInteger(sn);
        } else {
            instrumentMeta["SerialNumber"].setString(field.toString());
        }
        if (oldValue.read() != instrumentMeta["SerialNumber"]) {
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }
    }

    if (fields.empty()) return 3;
    field = fields.front().string_trimmed();
    fields.pop_front();
    auto subfields = Util::as_deque(field.split('.'));
    if (subfields.size() != 3) return 4;

    Q_ASSERT(!subfields.empty());
    qint64 iday = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 5;
    if (iday < 1) return 6;
    if (iday > 31) return 7;
    Variant::Root day(iday);
    remap("DAY", day);
    iday = day.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 imonth = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 8;
    if (imonth < 1) return 9;
    if (imonth > 12) return 10;
    Variant::Root month(imonth);
    remap("MONTH", month);
    imonth = month.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 iyear = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 11;
    if (iyear < 1900) {
        if (iyear > 99) return 12;
        if (iyear < 0) return 13;
        iyear += 2000;
    }
    if (iyear > 2999) return 14;
    Variant::Root year(iyear);
    remap("YEAR", year);
    iyear = year.read().toInt64();

    if (fields.empty()) return 15;
    field = fields.front().string_trimmed();
    fields.pop_front();
    subfields = Util::as_deque(field.split(':'));
    if (subfields.size() != 3) return 16;

    Q_ASSERT(!subfields.empty());
    qint64 ihour = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 17;
    if (ihour < 0) return 18;
    if (ihour > 23) return 19;
    Variant::Root hour(ihour);
    remap("HOUR", hour);
    ihour = hour.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 iminute = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 20;
    if (iminute < 0) return 21;
    if (iminute > 59) return 22;
    Variant::Root minute(iminute);
    remap("MINUTE", minute);
    iminute = minute.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 isecond = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 23;
    if (isecond < 0) return 24;
    if (isecond > 60) return 25;
    Variant::Root second(isecond);
    remap("SECOND", second);
    isecond = second.read().toInt64();

    if (!FP::defined(frameTime) && INTEGER::defined(iyear) && iyear >= 1900 && iyear <= 2999 &&
            INTEGER::defined(imonth) &&
            imonth >= 1 &&
            imonth <= 12 &&
            INTEGER::defined(iday) &&
            iday >= 1 &&
            iday <= 31 &&
            INTEGER::defined(ihour) &&
            ihour >= 0 &&
            ihour <= 23 &&
            INTEGER::defined(iminute) &&
            iminute >= 0 &&
            iminute <= 59 &&
            INTEGER::defined(isecond) &&
            isecond >= 0 &&
            isecond <= 60) {
        frameTime = Time::fromDateTime(QDateTime(QDate((int) iyear, (int) imonth, (int) iday),
                                                 QTime((int) ihour, (int) iminute, (int) isecond),
                                                 Qt::UTC));
    }
    if (FP::defined(lastRecordTime) && FP::defined(frameTime) && frameTime < lastRecordTime)
        return -1;
    double endTime = frameTime;
    double startTime;
    if (!FP::defined(lastRecordTime)) {
        startTime = FP::undefined();
        lastRecordTime = frameTime;
    } else {
        startTime = lastRecordTime;
        if (lastRecordTime != frameTime)
            lastRecordTime = frameTime;
    }

    if (fields.empty()) return 26;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.size() != 1) return 27;
    bool isStatus = false;
    switch (field[0]) {
    case 'S':
    case 's':
        isStatus = true;
        break;
    case 'A':
    case 'a':
    case 'I':
    case 'i':
        break;
    default:
        return 28;
    }

    if (fields.empty()) return 29;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZTips(field.parse_real(&ok));
    if (!ok) return 30;
    if (!FP::defined(ZTips.read().toReal())) return 31;
    remap("ZTips", ZTips);
    double bucketTips = ZTips.read().toReal();
    Variant::Root WI;
    if (FP::defined(bucketTips) && FP::defined(priorBucketTips)) {
        if (bucketTips < priorBucketTips) {
            WI.write().setReal(calculateRainIntensity(bucketTips, startTime, endTime));
        } else {
            WI.write()
              .setReal(calculateRainIntensity(bucketTips - priorBucketTips, startTime, endTime));
        }
    }
    remap("WI", WI);

    if (fields.empty()) return 32;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZPH(field.parse_real(&ok));
    if (!ok) return 33;
    if (!FP::defined(ZPH.read().toReal())) return 34;
    remap("ZPH", ZPH);

    if (fields.empty()) return 35;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZConductivity(field.parse_real(&ok));
    if (!ok) return 36;
    if (!FP::defined(ZConductivity.read().toReal())) return 37;
    remap("ZConductivity", ZConductivity);

    if (fields.empty()) return 38;
    field = fields.front().string_trimmed();
    fields.pop_front();
    bool hasPrecipitation = (field.string_trimmed().parse_i32(&ok) != 0);
    if (!ok) return 39;

    if (fields.empty()) return 40;
    field = fields.front().string_trimmed();
    fields.pop_front();
    bool funnelError = (field.string_trimmed().parse_i32(&ok) != 0);
    if (!ok) return 41;

    if (fields.empty()) return 42;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Fn(field.parse_i64(&ok));
    if (!ok) return 43;
    remap("Fn", Fn);

    if (fields.empty()) return 44;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T2(field.parse_real(&ok));
    if (!ok) return 45;
    if (!FP::defined(T2.read().toReal())) return 46;
    remap("T2", T2);

    if (fields.empty()) return 47;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T1(field.parse_real(&ok));
    if (!ok) return 48;
    if (!FP::defined(T1.read().toReal())) return 49;
    remap("T1", T1);

    if (config.first().strictMode) {
        if (!fields.empty())
            return 100;
    }


    priorBucketTips = bucketTips;

    if (!haveEmittedLogMeta && loggingEgress && FP::defined(startTime)) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(endTime);
        Util::append(buildRealtimeMeta(endTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    if (persistentFn.read() != Fn.read()) {
        if (persistentEgress && FP::defined(persistentFn.getStart())) {
            persistentFn.setEnd(endTime);
            persistentEgress->incomingData(persistentFn);
            persistentFn.setEnd(FP::undefined());
        }
        persistentFn.setRoot(Fn);
        persistentFn.setStart(endTime);

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "Fn"}, Fn, endTime, FP::undefined()));
        }

        persistentValuesUpdated();
    } else if (forceRealtimeStateEmit && realtimeEgress) {
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "Fn"}, Fn, endTime, FP::undefined()));
    }

    if (realtimeEgress &&
            (forceRealtimeStateEmit ||
                    priorPrecipitationState != hasPrecipitation ||
                    priorFunnelError != funnelError)) {
        priorPrecipitationState = hasPrecipitation;
        priorFunnelError = funnelError;
        Variant::Root state("Run");
        if (funnelError) {
            state.write().setString("FunnelError");
        } else if (hasPrecipitation) {
            state.write().setString("PrecipitationDetected");
        }
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZSTATE"}, state, endTime, FP::undefined()));
    }

    if (realtimeEgress)
        forceRealtimeStateEmit = false;

    Variant::Flags flags;
    if (!hasPrecipitation)
        flags.insert("ContaminateSensorDry");
    if (funnelError)
        flags.insert("FunnelError");

    logValue(startTime, endTime, "F1", Variant::Root(std::move(flags)));
    logValue(startTime, endTime, "WI", std::move(WI));
    logValue(startTime, endTime, "ZPH", std::move(ZPH));
    logValue(startTime, endTime, "ZConductivity", std::move(ZConductivity));
    logValue(startTime, endTime, "T1", std::move(T1));
    logValue(startTime, endTime, "T2", std::move(T2));

    if (isStatus)
        return -1;

    return 0;
}

void AcquireEigenbrodtNMO191::sendCommand(const Util::ByteView &command)
{
    if (!controlStream)
        return;

    Util::ByteArray data("#");
    data += config.front().address;
    data.push_back('#');
    data += command;
    data += "\r\n";

    controlStream->writeControl(std::move(data));
}

bool AcquireEigenbrodtNMO191::isOkResponse(const Util::ByteView &frame) const
{
    if (frame == "+SN+")
        return true;
    Util::ByteArray check("+");
    check += config.front().address;
    check.push_back('+');
    return frame == check;
}

void AcquireEigenbrodtNMO191::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    {
        Q_ASSERT(!config.isEmpty());
        if (!Range::intersectShift(config, frameTime)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.isEmpty());
    }

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        if (isOkResponse(frame))
            break;
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 10) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_PASSIVE_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();
                forceRealtimeStateEmit = true;

                if (FP::defined(frameTime))
                    timeoutAt(frameTime + config.first().pollInterval + 10.0);

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }
    case RESP_PASSIVE_WAIT: {
        if (isOkResponse(frame))
            break;
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);
            responseState = RESP_PASSIVE_RUN;
            generalStatusUpdated();
            forceRealtimeStateEmit = true;
            ++autoprobeValidRecords;

            if (FP::defined(frameTime))
                timeoutAt(frameTime + config.first().pollInterval + 10.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_RUN_RESPONSE: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            responseState = RESP_INTERACTIVE_RUN_ACK;
            if (FP::defined(frameTime)) {
                timeoutAt(frameTime + 2.0);
            }
            ++autoprobeValidRecords;
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;


            if (FP::defined(frameTime)) {
                timeoutAt(frameTime + 10.0);
                discardData(frameTime + 2.0);
            }
            sendCommand("PM_0");
            responseState = RESP_INTERACTIVE_START_STOPREPORTS;

            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("RunResponse");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case RESP_INTERACTIVE_RUN_ACK:
        if (frame.empty())
            break;
        if (!isOkResponse(frame)) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "is not an acknowledgment";

            if (FP::defined(frameTime)) {
                timeoutAt(frameTime + 10.0);
                discardData(frameTime + 2.0);
            }
            sendCommand("PM_0");
            responseState = RESP_INTERACTIVE_START_STOPREPORTS;

            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("RunACK");
            info.hash("Line").setString(frame.toString());
            event(frameTime, QObject::tr("Non-acknowledgment received.  Communications dropped."),
                  true, info);

            invalidateLogValues(frameTime);
        } else {
            responseState = RESP_INTERACTIVE_RUN_WAIT;
            if (FP::defined(frameTime)) {
                timeoutAt(frameTime + config.front().pollInterval);
            }
        }
        break;

    case RESP_PASSIVE_RUN: {
        /* Ignored */
        if (isOkResponse(frame))
            break;
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + config.first().pollInterval + 5.0);
            ++autoprobeValidRecords;
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            responseState = RESP_PASSIVE_WAIT;
            timeoutAt(FP::undefined());
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case RESP_INTERACTIVE_START_SETDATE: {
        if (!isOkResponse(frame)) {
            qCDebug(log) << "Interactive start comms date set failed at" << Logging::time(frameTime)
                         << "with response" << frame;

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            if (FP::defined(frameTime))
                discardData(frameTime + 30.0);
            invalidateLogValues(frameTime);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            break;
        }

        responseState = RESP_INTERACTIVE_START_SETTIME;
        if (FP::defined(frameTime))
            timeoutAt(frameTime + 5.0);
        {
            Util::ByteArray d("clk_");
            d += QByteArray::number(setInstrumentTime.time().hour()).rightJustified(2, '0');
            d += QByteArray::number(setInstrumentTime.time().minute()).rightJustified(2, '0');
            d += QByteArray::number(setInstrumentTime.time().second()).rightJustified(2, '0');
            sendCommand(d);
        }

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetTime"),
                                  frameTime, FP::undefined()));
        }

        break;
    }

    case RESP_INTERACTIVE_START_SETTIME: {
        if (!isOkResponse(frame)) {
            qCDebug(log) << "Interactive start comms time set failed at" << Logging::time(frameTime)
                         << "with response" << frame;

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            if (FP::defined(frameTime))
                discardData(frameTime + 30.0);
            invalidateLogValues(frameTime);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            break;
        }

        responseState = RESP_INTERACTIVE_START_FIRSTVALID;
        if (FP::defined(frameTime))
            timeoutAt(frameTime + 5.0);
        sendCommand("ST1");

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveStartReadFirstRecord"), frameTime, FP::undefined()));
        }

        break;
    }

    case RESP_INTERACTIVE_START_FIRSTVALID: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            responseState = RESP_INTERACTIVE_RUN_ACK;
            if (FP::defined(frameTime))
                timeoutAt(frameTime + 2.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();
            forceRealtimeStateEmit = true;
            ++autoprobeValidRecords;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            if (FP::defined(frameTime))
                discardData(frameTime + 30.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    default:
        break;
    }
}

void AcquireEigenbrodtNMO191::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_RESPONSE:
    case RESP_INTERACTIVE_RUN_ACK:
        qCDebug(log) << "Timeout in interactive state" << responseState << "at"
                     << Logging::time(frameTime);

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        {
            Variant::Write info = Variant::Write::empty();
            if (responseState == RESP_INTERACTIVE_RUN_RESPONSE) {
                info.hash("ResponseState").setString("InteractiveRun");
            } else {
                info.hash("ResponseState").setString("InteractiveRunACK");
            }
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 2.0);
        sendCommand("PM_0");
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;

        generalStatusUpdated();

        invalidateLogValues(frameTime);
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_SETDATE:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_FIRSTVALID:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 30.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 2.0);
        sendCommand("PM_0");
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_RUN_WAIT:
        responseState = RESP_INTERACTIVE_RUN_RESPONSE;
        timeoutAt(frameTime + 5.0);
        sendCommand("ST1");
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        invalidateLogValues(frameTime);
        break;

    default:
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireEigenbrodtNMO191::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    switch (responseState) {
    case RESP_INTERACTIVE_START_STOPREPORTS:
        setInstrumentTime = Time::toDateTime(frameTime + 0.5);
        timeoutAt(frameTime + 5.0);
        responseState = RESP_INTERACTIVE_START_SETDATE;
        {
            Util::ByteArray d("dte_");
            d += QByteArray::number(setInstrumentTime.date().day()).rightJustified(2, '0');
            d += QByteArray::number(setInstrumentTime.date().month()).rightJustified(2, '0');
            d += QByteArray::number(setInstrumentTime.date().year() % 100).rightJustified(2, '0');
            sendCommand(d);
        }

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetDate"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 2.0);
        sendCommand("PM_0");
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    default:
        break;
    }
}

void AcquireEigenbrodtNMO191::incomingControlFrame(const Util::ByteArray &, double)
{ }

SequenceValue::Transfer AcquireEigenbrodtNMO191::getPersistentValues()
{
    PauseLock paused(*this);
    SequenceValue::Transfer result;

    if (persistentFn.read().exists()) {
        result.emplace_back(persistentFn);
    }

    return result;
}

Variant::Root AcquireEigenbrodtNMO191::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireEigenbrodtNMO191::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN_RESPONSE:
    case RESP_INTERACTIVE_RUN_ACK:
    case RESP_INTERACTIVE_RUN_WAIT:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

AcquisitionInterface::AutoprobeStatus AcquireEigenbrodtNMO191::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireEigenbrodtNMO191::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_RESPONSE:
    case RESP_INTERACTIVE_RUN_ACK:
    case RESP_INTERACTIVE_RUN_WAIT:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_SETDATE:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_FIRSTVALID:
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + 10.0);
        discardData(time + 2.0);
        sendCommand("PM_0");
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireEigenbrodtNMO191::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobeValidRecords = 0;
    discardData(time + 0.5, 1);
    timeoutAt(time + 15.0);
    generalStatusUpdated();
}

void AcquireEigenbrodtNMO191::autoprobePromote(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_RESPONSE:
    case RESP_INTERACTIVE_RUN_ACK:
    case RESP_INTERACTIVE_RUN_WAIT:
    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_SETDATE:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_FIRSTVALID:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 5.0);
        break;

    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 10.0);
        discardData(time + 2.0);
        sendCommand("PM_0");
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireEigenbrodtNMO191::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    Q_ASSERT(!config.isEmpty());

    timeoutAt(time + 5.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_RUN_RESPONSE:
    case RESP_INTERACTIVE_RUN_ACK:
    case RESP_INTERACTIVE_RUN_WAIT:
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireEigenbrodtNMO191::getDefaults()
{
    AutomaticDefaults result;
    result.name = "XM$2";
    result.setSerialN81(38400);
    return result;
}


ComponentOptions AcquireEigenbrodtNMO191Component::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);
    return options;
}

ComponentOptions AcquireEigenbrodtNMO191Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireEigenbrodtNMO191Component::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples(true));
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireEigenbrodtNMO191Component::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireEigenbrodtNMO191Component::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireEigenbrodtNMO191Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data from the instrument")));

    return examples;
}

std::unique_ptr<AcquisitionInterface> AcquireEigenbrodtNMO191Component::createAcquisitionPassive(
        const ComponentOptions &options,
        const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(
            new AcquireEigenbrodtNMO191(options, loggingContext));
}

std::unique_ptr<AcquisitionInterface> AcquireEigenbrodtNMO191Component::createAcquisitionPassive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(
            new AcquireEigenbrodtNMO191(config, loggingContext));
}

std::unique_ptr<AcquisitionInterface> AcquireEigenbrodtNMO191Component::createAcquisitionAutoprobe(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireEigenbrodtNMO191> i(new AcquireEigenbrodtNMO191(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireEigenbrodtNMO191Component::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                             const std::string &loggingContext)
{
    std::unique_ptr<AcquireEigenbrodtNMO191> i(new AcquireEigenbrodtNMO191(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
