/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <cstdint>
#include <cmath>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>
#include <QtEndian>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;

    QDateTime time;
    QByteArray serialNumber;
    int bucketTips;
    double pH;
    double cond;
    bool funnelError;
    bool precipitation;
    int bottle;
    double internalTemperature;
    double measurementTemperature;

    ModelInstrument()
            : incoming(),
              outgoing(),
              unpolledRemaining(10.0),
              time(QDate(2013, 2, 3), QTime(1, 2, 0))
    {
        serialNumber = "4023";
        bucketTips = 321;
        pH = 5.43;
        cond = 567.3;
        funnelError = false;
        precipitation = true;
        bottle = 1;
        internalTemperature = 6.2;
        measurementTemperature = 25.1;
    }

    void outputRecord(char code = 'A')
    {
        outgoing.append(serialNumber);
        outgoing.append(';');
        outgoing.append(QByteArray::number(time.date().day()).rightJustified(2, '0'));
        outgoing.append('.');
        outgoing.append(QByteArray::number(time.date().month()).rightJustified(2, '0'));
        outgoing.append('.');
        outgoing.append(QByteArray::number(time.date().year() % 100).rightJustified(2, '0'));
        outgoing.append(';');
        outgoing.append(QByteArray::number(time.time().hour()).rightJustified(2, '0'));
        outgoing.append(':');
        outgoing.append(QByteArray::number(time.time().minute()).rightJustified(2, '0'));
        outgoing.append(':');
        outgoing.append(QByteArray::number(time.time().second()).rightJustified(2, '0'));
        outgoing.append(';');
        outgoing.append(code);
        outgoing.append(';');
        outgoing.append(QByteArray::number(bucketTips));
        outgoing.append(';');
        outgoing.append(QByteArray::number(pH, 'f', 2));
        outgoing.append(';');
        outgoing.append(QByteArray::number(cond, 'f', 1));
        outgoing.append(';');
        outgoing.append(precipitation ? '1' : '0');
        outgoing.append(';');
        outgoing.append(funnelError ? '1' : '0');
        outgoing.append(';');
        outgoing.append(QByteArray::number(bottle));
        outgoing.append(';');
        outgoing.append(internalTemperature >= 0.0 ? '+' : '-');
        outgoing.append(QByteArray::number(internalTemperature, 'f', 1));
        outgoing.append(';');
        outgoing.append(measurementTemperature >= 0.0 ? '+' : '-');
        outgoing.append(QByteArray::number(measurementTemperature, 'f', 1));
        outgoing.append("\r\n");
    }

    void outputACK()
    { outgoing.append("+SN+\r\n"); }

    bool consumeAddress(QByteArray &line) const
    {
        if (line.startsWith("#SN#")) {
            line = line.mid(4);
            return !line.isEmpty();
        }
        QByteArray check;
        check.append('#');
        check.append(serialNumber);
        check.append('#');
        if (line.startsWith(check)) {
            line = line.mid(check.length());
            return !line.isEmpty();
        }

        return false;
    }

    void advance(double seconds)
    {
        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR).trimmed());

            if (consumeAddress(line)) {
                if (line.startsWith("clk_") && line.length() == 10) {
                    time.setTime(QTime(line.mid(4, 2).toInt(), line.mid(6, 2).toInt(),
                                       line.mid(8, 2).toInt()));
                    outputACK();
                } else if (line.startsWith("dte_") && line.length() == 10) {
                    time.setDate(QDate(line.mid(6, 2).toInt(), line.mid(4, 2).toInt(),
                                       line.mid(8, 2).toInt()));
                    outputACK();
                } else if (line == "PM_0") {
                    unpolledRemaining = FP::undefined();
                    outputACK();
                } else if (line == "PM_1" || line == "PM_2") {
                    unpolledRemaining = 1.0;
                    outputACK();
                } else if (line == "ST1") {
                    outputRecord('I');
                    outputACK();
                }
            }

            incoming = incoming.mid(idxCR + 1);
        }

        if (FP::defined(unpolledRemaining)) {
            unpolledRemaining -= seconds;
            while (unpolledRemaining <= 0.0) {
                unpolledRemaining += 10.0;
                outputRecord();
            }
        }
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("WI", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZPH", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZConductivity", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Fn", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZSTATE", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("WI"))
            return false;
        if (!stream.checkContiguous("T"))
            return false;
        if (!stream.checkContiguous("ZPH"))
            return false;
        if (!stream.checkContiguous("ZConductivity"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("WI", Variant::Root(0.0), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZPH", Variant::Root(model.pH), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZConductivity", Variant::Root(model.cond), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.measurementTemperature), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.internalTemperature), time))
            return false;
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &, const ModelInstrument &, double = FP::undefined())
    { return true; }

    bool checkPersistent(const SequenceValue::Transfer &values, ModelInstrument &instrument)
    {
        if (!StreamCapture::findValue(values, "raw", "Fn", Variant::Root(instrument.bottle)))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_eigenbrodt_nmo191"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_eigenbrodt_nmo191"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"] = 10.0;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(5.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(5.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkPersistent(realtime.values(), instrument));
        QVERIFY(checkPersistent(persistentValues, instrument));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get(), 1519862400);
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(control.time());

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 20; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkPersistent(realtime.values(), instrument));
        QVERIFY(checkPersistent(persistentValues, instrument));
    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"] = 10.0;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 600.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")) ||
                    realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("PrecipitationDetected")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 600.0);
        control.advance(0.1);

        for (int i = 0; i < 100; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        QVERIFY(logging.hasAnyMatchingValue("F1"));

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkPersistent(realtime.values(), instrument));
        QVERIFY(checkPersistent(persistentValues, instrument));
    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get(), 1519862400);
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(control.time());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 100; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkPersistent(realtime.values(), instrument));
        QVERIFY(checkPersistent(persistentValues, instrument));
    }

};

QTEST_MAIN(TestComponent)

#include "test.moc"
