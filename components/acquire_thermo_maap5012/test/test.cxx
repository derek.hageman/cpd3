/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>
#include <QDateTime>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    QByteArray buffer;
    QDateTime currentTime;

    enum {
        Measure, FlagsCalculateHold, Calculate, Changing, Zeroing,
    } state;
    double stateRemaining;
    int unpolledFormat;

    double Iref;
    double Isam;
    double Iscat135;
    double Iscat165;
    double dIref;
    double dIsam;
    double dIscat135;
    double dIscat165;

    double I0ref;
    double I0sam;
    double I0scat135;
    double I0scat165;
    double accumulatedVolume;

    double IDref;
    double IDsam;
    double IDscat135;
    double IDscat165;

    double Porifice;
    double Ppump;
    double Pambient;
    double Tambient;
    double Tmeasure;
    double Tsystem;
    double ssa;

    double Q;
    double flowRegulatorPercent;

    double inversionEffect;
    double sigmaBC;
    double area;

    int errorCounter;

    int offsetCounter;
    int absoluteCounter;
    enum {
        Normal, Fast, WaitForFlags
    } fastResponseMode;

    ModelInstrument()
            : incoming(),
              outgoing(),
              buffer(),
              currentTime(QDate(2014, 01, 02), QTime(10, 0, 0), Qt::UTC),
              state(Measure),
              stateRemaining(2.0),
              unpolledFormat(12)
    {
        offsetCounter = 0;
        absoluteCounter = 0;
        fastResponseMode = Normal;

        Iref = 2100.0;
        Isam = 2300.0;
        Iscat135 = 1950.0;
        Iscat165 = 1970.0;
        dIref = -0.1;
        dIsam = -1.5;
        dIscat135 = -0.2;
        dIscat165 = -0.3;

        IDref = 10.0;
        IDsam = 20.0;
        IDscat135 = 30.0;
        IDscat165 = 40.0;

        normalize();

        Porifice = 56.0;
        Ppump = 57.0;
        Pambient = 890.0;
        Tambient = 23.0;
        Tmeasure = 24.0;
        Tsystem = 25.0;
        ssa = 0.954;

        Q = 12.0;
        flowRegulatorPercent = 12.5;

        inversionEffect = 0.9;
        sigmaBC = 6.6;
        area = 200.0;

        errorCounter = 0;
    }

    void normalize()
    {
        offsetCounter = 0;

        I0ref = Iref;
        I0sam = Isam;
        I0scat135 = Iscat135;
        I0scat165 = Iscat165;
        accumulatedVolume = 0.0;
    }

    double referenceIntensity(int offset = 0) const
    {
        return Iref - dIref * offset;
    }

    double sampleIntensity(int offset = 0) const
    {
        return Isam - dIsam * offset;
    }

    double scat135Intensity(int offset = 0) const
    {
        return Iscat135 - dIscat135 * offset;
    }

    double scat165Intensity(int offset = 0) const
    {
        return Iscat165 - dIscat165 * offset;
    }

    double normalizedIntensity(int offset) const
    {
        return sampleIntensity(offset) / referenceIntensity(offset);
    }

    double absorption(int offset = 0) const
    {
        double In0 = normalizedIntensity(offset + 1);
        double In1 = normalizedIntensity(offset);
        double dQt = (Q * 22.0) / 60000.0;
        return log(In0 / In1) * (area / dQt);
    }

    double EBC(int offset = 0) const
    {
        return absorption(offset) / sigmaBC;
    }

    double transmittance(int offset = 0) const
    {
        return normalizedIntensity(offset) / (I0sam / I0ref);
    }

    double LOD(int offset = 0) const
    {
        double Ir = transmittance(offset);
        return (double) qRound(100.0 * log(Ir) * 1E3) / 1E3;
    }

    double correctedEBC(int offset = 0) const
    {
        double e = EBC(offset) * inversionEffect;
        e = (double) qRound(e * 1E2) / 1E2;
        return e;
    }

    double correctedAbsorption(int offset = 0) const
    {
        return correctedEBC(offset) * sigmaBC;
    }

    double flowAsDeltaV() const
    {
        return (double) qRound(((Q / 60000.0) * 22.0) * 1E3) / 1E3;
    }

    double absorptionDeltaV(int offset = 0) const
    {
        double In0 = normalizedIntensity(offset + 1);
        double In1 = normalizedIntensity(offset);
        double dQt = flowAsDeltaV();
        return log(In0 / In1) * (area / dQt);
    }

    void flushBuffer()
    {
        outgoing.append(buffer);
        outgoing.append("\r\n");
        buffer.clear();
    }

    void bufferDateTime()
    {
        buffer.append(QByteArray::number(currentTime.date().year() % 100).rightJustified(2, '0'));
        buffer.append('-');
        buffer.append(QByteArray::number(currentTime.date().month()).rightJustified(2, '0'));
        buffer.append('-');
        buffer.append(QByteArray::number(currentTime.date().day()).rightJustified(2, '0'));
        buffer.append(' ');
        buffer.append(QByteArray::number(currentTime.time().hour()).rightJustified(2, '0'));
        buffer.append(':');
        buffer.append(QByteArray::number(currentTime.time().minute()).rightJustified(2, '0'));
        buffer.append(':');
        buffer.append(QByteArray::number(currentTime.time().second()).rightJustified(2, '0'));
    }

    void bufferGlobalStatus()
    {
        quint32 flags = 0;
        switch (state) {
        case FlagsCalculateHold:
        case Measure:
        case Calculate:
            break;
        case Changing:
            flags |= 0x000008;
            flags |= 0x000001;
            break;
        case Zeroing:
            flags |= 0x000002;
            break;
        }
        buffer.append(QByteArray::number(flags, 16).rightJustified(6, '0'));
    }

    void outputPF1()
    {
        bufferDateTime();
        buffer.append(' ');
        bufferGlobalStatus();
        buffer.append(' ');
        buffer.append(QByteArray::number(correctedEBC(), 'f', 2));
        flushBuffer();
    }

    void outputPF2()
    {
        bufferDateTime();
        buffer.append(' ');
        bufferGlobalStatus();
        buffer.append(' ');
        buffer.append(QByteArray::number(correctedEBC(), 'f', 2));
        buffer.append(" 0.93");
        flushBuffer();
    }

    void outputPF3()
    {
        bufferDateTime();
        buffer.append(' ');
        bufferGlobalStatus();
        buffer.append(' ');
        buffer.append(QByteArray::number(correctedEBC(), 'f', 2));
        buffer.append(" 0.93 ");
        buffer.append(QByteArray::number(Q * 60, 'f', 0));
        flushBuffer();
    }

    void outputPF5()
    {
        bufferDateTime();
        buffer.append(' ');
        bufferGlobalStatus();
        buffer.append(' ');
        buffer.append(QByteArray::number(correctedEBC(), 'f', 2));
        buffer.append(" 0.93 ");
        buffer.append(QByteArray::number(Q * 60, 'f', 0));
        buffer.append(' ');
        buffer.append(QByteArray::number(correctedEBC(), 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(correctedEBC(), 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(correctedEBC(), 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(correctedEBC(), 'f', 2));
        flushBuffer();
    }

    void outputPF6()
    {
        buffer.append((char) 0x02);
        buffer.append("MD01 001 ");

        double b = correctedEBC();
        int exponent = (int) log10(b);
        int nbc = (int) qRound((b * 100.0) / pow(10.0, (double) exponent));

        if (nbc < 0) {
            buffer.append('-');
            buffer.append(QByteArray::number(-nbc).rightJustified(4, '0'));
        } else {
            buffer.append('+');
            buffer.append(QByteArray::number(nbc).rightJustified(4, '0'));
        }
        if (exponent < 0) {
            buffer.append('-');
            buffer.append(QByteArray::number(-exponent).rightJustified(2, '0'));
        } else {
            buffer.append('+');
            buffer.append(QByteArray::number(exponent).rightJustified(2, '0'));
        }

        buffer.append(" 00 00 003 ");
        bufferGlobalStatus();
        buffer.append((char) 0x03);
        buffer.append("3B");
        flushBuffer();
    }

    void outputPF99()
    {
        bufferDateTime();
        buffer.append(' ');
        buffer.append(QByteArray::number(Q * 60, 'f', 0));
        buffer.append(' ');
        buffer.append(QByteArray::number(referenceIntensity(), 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(sampleIntensity(), 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(scat135Intensity(), 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(scat165Intensity(), 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(I0ref, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(I0sam, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(I0scat135, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(I0scat165, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(IDref, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(IDsam, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(IDscat135, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(IDscat165, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(correctedEBC(), 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(correctedEBC(), 'f', 2));
        buffer.append(" 0.01666");
        buffer.append(' ');
        buffer.append(QByteArray::number(Tambient, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(Tmeasure, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(Tsystem, 'f', 2));
        buffer.append(" 28.9 78.7 ");
        buffer.append(QByteArray::number(accumulatedVolume, 'f', 2));

        flushBuffer();
    }

    void outputPF98()
    {
        bufferDateTime();
        buffer.append(' ');
        buffer.append(QByteArray::number(Q * 60, 'f', 0));
        buffer.append(' ');
        buffer.append(QByteArray::number(referenceIntensity(), 'f', 0));
        buffer.append(' ');
        buffer.append(QByteArray::number(referenceIntensity(), 'f', 0));
        buffer.append(' ');
        buffer.append(QByteArray::number(sampleIntensity(), 'f', 0));
        buffer.append(' ');
        buffer.append(QByteArray::number(sampleIntensity(), 'f', 0));
        buffer.append(' ');
        buffer.append(QByteArray::number(scat135Intensity(), 'f', 0));
        buffer.append(' ');
        buffer.append(QByteArray::number(scat135Intensity(), 'f', 0));
        buffer.append(' ');
        buffer.append(QByteArray::number(scat165Intensity(), 'f', 0));
        buffer.append(' ');
        buffer.append(QByteArray::number(scat165Intensity(), 'f', 0));
        buffer.append(' ');
        buffer.append(QByteArray::number(I0ref, 'f', 4));
        buffer.append(' ');
        buffer.append(QByteArray::number(I0sam, 'f', 4));
        buffer.append(' ');
        buffer.append(QByteArray::number(I0scat135, 'f', 4));
        buffer.append(' ');
        buffer.append(QByteArray::number(I0scat165, 'f', 4));
        buffer.append(' ');
        buffer.append(QByteArray::number(correctedEBC(), 'f', 4));
        buffer.append(" 0.53258   0.00899");
        buffer.append(' ');
        buffer.append(QByteArray::number(Tambient, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(Tmeasure, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(Tsystem, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(Porifice, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(Ppump, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(Pambient, 'f', 2));

        flushBuffer();
    }

    void bufferPF12Start()
    {
        bufferDateTime();
        buffer.append(' ');
        buffer.append(QByteArray::number(sampleIntensity(), 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(scat135Intensity(), 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(scat165Intensity(), 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(referenceIntensity(), 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(I0sam, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(I0scat135, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(I0scat165, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(I0ref, 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(flowAsDeltaV(), 'f', 3));
    }

    void outputPF12End()
    {
        buffer.append("  17 ");
        buffer.append(QByteArray::number(ssa, 'f', 6));
        buffer.append(' ');
        buffer.append(QByteArray::number(LOD(), 'f', 3));
        buffer.append(" 0.93 ");
        buffer.append(QByteArray::number(correctedEBC(), 'f', 2));
        buffer.append(' ');
        buffer.append(QByteArray::number(EBC(), 'f', 2));
        flushBuffer();
    }

    void outputParametersList()
    {
        buffer.append("  THERMO SCIENTIFIC  MAAP v1.32         SERIAL NUMBER  89    14-02-19\r\n"
                              "--------------------------------------------------------------------------------\r\n"
                              "SIGMA BC:              ");
        buffer.append(QByteArray::number(sigmaBC, 'f', 1));
        buffer.append(" m2/g\r\n"
                              "AIR FLOW:             1000\r\n"
                              "STORE AVERAGES:          0 min \r\n"
                              "VOLUME REFERENCE    STANDARD TEMPERATURE\r\n"
                              "STANDARD TEMPERATURE     0 _C  \r\n"
                              "PRINTFORMAT:        COM2     0\r\n"
                              "PRINTCYCLE:              0 s   \r\n"
                              "BAUDRATE:        Bd COM1  9600\r\n"
                              "BAUDRATE:        Bd COM2  9600\r\n"
                              "DEVICE-ADDRESS:          1\r\n"
                              "FILTER CHANGE       \r\n"
                              "TRANSM. <       %       70\r\n"
                              "CYCLE           h      100\r\n"
                              "HOUR:                    0\r\n"
                              "CALIBRATION OF SENS.\r\n"
                              "    T1    T2    T3    T4    P1    P2    P3\r\n"
                              "   -33   -21   -41    65     6    76    44\r\n"
                              "AIR FLOW             100.1\r\n"
                              "HEATER PARAMETERS   \r\n"
                              "Diff. T2-T1 nominal      0 _C  \r\n"
                              "Max. Heating Temp.      45 _C  \r\n"
                              "Min. Heating Power      10 %   \r\n"
                              "ANALOG OUTPUTS      \r\n"
                              "OUTPUT ZERO:         4mA \r\n"
                              "CBC      0    10\r\n"
                              "MBC      0  2400\r\n"
                              "GESYTEC-PROTOKOL    \r\n"
                              "STATUS VERSION      STANDARD \r\n"
                              "NUMBER OF VARIABLES      1\r\n"
                              "CBC \r\n"
                              "END");
        flushBuffer();
    }

    void outputSingleValue(double value, int digits = 2)
    {
        buffer.append(' ');
        buffer.append(QByteArray::number(value, 'f', digits));
        flushBuffer();
    }

    static bool setParameter(const QByteArray &line,
                             const QByteArray &prefix,
                             int min = -9999,
                             int max = 9999,
                             int *result = NULL)
    {
        if (!line.startsWith(prefix + " "))
            return false;
        bool ok = false;
        int v = line.mid(prefix.length() + 1).toInt(&ok);
        if (!ok)
            return false;
        if (v < min || v > max)
            return false;
        if (result != NULL)
            *result = v;
        return true;
    }

    static QDateTime parseDateTime(const QByteArray &input)
    {
        bool ok = false;
        int year = input.mid(0, 2).toInt(&ok);
        if (!ok || year < 0 || year > 99) return QDateTime();
        if (year < 70) year += 1900; else year += 2000;
        int month = input.mid(2, 2).toInt(&ok);
        if (!ok || month < 1 || month > 12) return QDateTime();
        int day = input.mid(4, 2).toInt(&ok);
        if (!ok || day < 1 || day > 31) return QDateTime();
        int hour = input.mid(6, 2).toInt(&ok);
        if (!ok || hour < 0 || hour > 23) return QDateTime();
        int minute = input.mid(8, 2).toInt(&ok);
        if (!ok || minute < 0 || minute > 59) return QDateTime();
        int second = input.mid(10, 2).toInt(&ok);
        if (!ok || second < 0 || second > 60) return QDateTime();
        return QDateTime(QDate(year, month, day), QTime(hour, minute, second), Qt::UTC);
    }

    void advance(double seconds)
    {
        int idxCR = -1;
        bool flagsResponsePending = false;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR).simplified());
            incoming = incoming.mid(idxCR + 1);

            if (line == "C") {
                outputSingleValue(correctedEBC());
            } else if (line == "m1") {
                outputSingleValue(0.93);
            } else if (line == "H") {
                outputSingleValue(correctedEBC());
            } else if (line == "H1") {
                outputSingleValue(correctedEBC());
            } else if (line == "H3") {
                outputSingleValue(correctedEBC());
            } else if (line == "HT") {
                outputSingleValue(correctedEBC());
            } else if (line == "J0") {
                outputSingleValue(Tambient);
            } else if (line == "J1") {
                outputSingleValue(Tmeasure);
            } else if (line == "J2") {
                outputSingleValue(Tsystem);
            } else if (line == "J3") {
                outputSingleValue(Porifice);
            } else if (line == "J4") {
                outputSingleValue(Ppump);
            } else if (line == "J5") {
                outputSingleValue(Pambient);
            } else if (line == "JA") {
                outputSingleValue(Iref);
            } else if (line == "JB") {
                outputSingleValue(Iscat135);
            } else if (line == "JC") {
                outputSingleValue(Iscat165);
            } else if (line == "JD") {
                outputSingleValue(Isam);
            } else if (line == "JJ") {
                outputSingleValue(Q * 60);
            } else if (line == "JK") {
                outputSingleValue(Q * 60);
            } else if (line == "JL") {
                outputSingleValue(Q * 60);
            } else if (line == "JM") {
                outputSingleValue(flowRegulatorPercent * 4096.0 / 100.0, 0);
            } else if (line == "JN") {
                outputSingleValue(accumulatedVolume);
            } else if (line == "N") {
                outputSingleValue(errorCounter, 0);
                errorCounter = 0;
            } else if (line == "v") {
                buffer.append("SERIAL NUMBER    89\r\n   THERMO SCIENTIFIC  MAAP v1.32");
                flushBuffer();
            } else if (line == "?") {
                outputSingleValue(1, 0);
            } else if (line == "#") {
                if (fastResponseMode != WaitForFlags) {
                    buffer.append(" 0000 0000 0000 0000 ");
                    bufferGlobalStatus();
                    flushBuffer();
                } else {
                    flagsResponsePending = true;
                }
            } else if (setParameter(line, "K0", 0, 3)) {
            } else if (setParameter(line, "K1", 0, 99)) {
            } else if (setParameter(line, "K2", 1, 100)) {
            } else if (setParameter(line, "K3", 0, 24)) {
            } else if (setParameter(line, "K4", 0, 3000)) {
            } else if (setParameter(line, "KK", 0, 0)) {
            } else if (setParameter(line, "KM", 1, 1)) {
            } else if (setParameter(line, "KN", 0, 0)) {
            } else if (setParameter(line, "d1", 0, 12, &unpolledFormat)) {
            } else if (setParameter(line, "D", 0, 12, &unpolledFormat)) {
            } else if (setParameter(line, "d2", 0, 0)) {
            } else if (setParameter(line, "d3", 0, 0)) {
            } else if (line.startsWith("Z")) {
                QDateTime dt(parseDateTime(line.mid(1)));
                if (dt.isValid()) {
                    currentTime = dt;
                } else {
                    errorCounter++;
                }
            } else if (line == "F") {
                state = Changing;
                stateRemaining = 20.0;
            } else if (line == "P") {
                switch (unpolledFormat) {
                case 1:
                    outputPF1();
                    break;
                case 2:
                    outputPF2();
                    break;
                case 3:
                    outputPF3();
                    break;
                case 5:
                    outputPF5();
                    break;
                case 6:
                    outputPF6();
                    break;
                case 8:
                    outputParametersList();
                    break;
                case 98:
                    outputPF98();
                    break;
                case 99:
                    outputPF99();
                    break;
                default:
                    break;
                }
            }
        }

        currentTime = currentTime.addMSecs((qint64) (seconds * 1000.0));

        stateRemaining -= seconds;
        while (stateRemaining <= 0.0 || flagsResponsePending) {
            switch (state) {
            case FlagsCalculateHold:
                if (unpolledFormat == 12) {
                    if (flagsResponsePending) {
                        flagsResponsePending = false;
                        bufferPF12Start();
                        buffer.append("0000 0000 0000 0000 ");
                        bufferGlobalStatus();
                        flushBuffer();
                    } else {
                        bufferPF12Start();
                    }

                    state = Calculate;
                    stateRemaining = 20.0;
                    break;
                }
                /* Fall through */
            case Measure:
                Iref += dIref;
                Isam += dIsam;
                Iscat135 += dIscat135;
                Iscat165 += dIscat165;

                accumulatedVolume += (Q * 22.0) / 60000.0;
                offsetCounter++;
                absoluteCounter++;

                if (unpolledFormat == 12) {
                    if (fastResponseMode == WaitForFlags) {
                        state = FlagsCalculateHold;
                        stateRemaining = 20.0;
                        break;
                    }
                    bufferPF12Start();
                }

                state = Calculate;
                if (fastResponseMode == Fast)
                    stateRemaining += 1.0;
                else
                    stateRemaining += 20.0;
                break;

            case Calculate:
                switch (unpolledFormat) {
                case 1:
                    outputPF1();
                    break;
                case 2:
                    outputPF2();
                    break;
                case 3:
                    outputPF3();
                    break;
                case 5:
                    outputPF5();
                    break;
                case 6:
                    outputPF6();
                    break;
                case 12:
                    outputPF12End();
                    break;
                case 98:
                    outputPF98();
                    break;
                case 99:
                    outputPF99();
                    break;
                default:
                    break;
                }

                state = Measure;
                stateRemaining += 2.0;
                break;

            case Changing:
                switch (unpolledFormat) {
                case 1:
                    outputPF1();
                    break;
                case 2:
                    outputPF2();
                    break;
                case 3:
                    outputPF3();
                    break;
                case 5:
                    outputPF5();
                    break;
                case 6:
                    outputPF6();
                    break;
                case 12:
                    bufferPF12Start();
                    outputPF12End();
                    break;
                case 98:
                    outputPF98();
                    break;
                case 99:
                    outputPF99();
                    break;
                default:
                    break;
                }

                state = Zeroing;
                stateRemaining += 20.0;
                break;

            case Zeroing:
                normalize();

                switch (unpolledFormat) {
                case 1:
                    outputPF1();
                    break;
                case 2:
                    outputPF2();
                    break;
                case 3:
                    outputPF3();
                    break;
                case 5:
                    outputPF5();
                    break;
                case 6:
                    outputPF6();
                    break;
                case 12:
                    bufferPF12Start();
                    outputPF12End();
                    break;
                case 98:
                    outputPF98();
                    break;
                case 99:
                    outputPF99();
                    break;
                default:
                    break;
                }

                state = Measure;
                stateRemaining += 2.0;
                break;
            }
            flagsResponsePending = false;
        }
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, const ModelInstrument &model,
                   double time = FP::undefined())
    {
        Q_UNUSED(model);
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZSPOT", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Ff", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Qt", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("L", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Pd1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Pd2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("P", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZPARAMETERS", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("IrR", Variant::Root(670.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BaR", Variant::Root(670.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BacR", Variant::Root(670.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("XR", Variant::Root(670.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IfR", Variant::Root(670.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IpR", Variant::Root(670.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("Is1", Variant::Root(670.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("Is2", Variant::Root(670.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("ZSSAR", Variant::Root(670.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("ZIrR", Variant::Root(670.0), "^Wavelength", time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, const ModelInstrument &model,
                           double time = FP::undefined())
    {
        Q_UNUSED(model);
        if (!stream.hasMeta("ZSTATE", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZF2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZF3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCT", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("InR", Variant::Root(670.0), "^Wavelength", time))
            return false;
        return true;
    }

    bool checkValuesPF1(StreamCapture &stream, const ModelInstrument &model,
                        double time = FP::undefined())
    {
        Q_UNUSED(model);
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkRealtimeValuesPF1(StreamCapture &stream, const ModelInstrument &model,
                                double time = FP::undefined())
    {
        Q_UNUSED(model);
        if (!stream.hasAnyMatchingValue("ZF2", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkValuesPF2(StreamCapture &stream, const ModelInstrument &model,
                        double time = FP::undefined())
    {
        Q_UNUSED(model);
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkRealtimeValuesPF2(StreamCapture &stream, const ModelInstrument &model,
                                double time = FP::undefined())
    {
        Q_UNUSED(model);
        if (!stream.hasAnyMatchingValue("ZF2", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkValuesPF3(StreamCapture &stream, const ModelInstrument &model,
                        double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("Q", Variant::Root(model.Q), time))
            return false;
        return true;
    }

    bool checkRealtimeValuesPF3(StreamCapture &stream, const ModelInstrument &model,
                                double time = FP::undefined())
    {
        Q_UNUSED(model);
        if (!stream.hasAnyMatchingValue("ZF2", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkValuesPF5(StreamCapture &stream, const ModelInstrument &model,
                        double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("Q", Variant::Root(model.Q), time))
            return false;
        return true;
    }

    bool checkRealtimeValuesPF5(StreamCapture &stream, const ModelInstrument &model,
                                double time = FP::undefined())
    {
        Q_UNUSED(model);
        if (!stream.hasAnyMatchingValue("ZF2", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkValuesPF6(StreamCapture &stream, const ModelInstrument &model,
                        double time = FP::undefined())
    {
        Q_UNUSED(model);
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkRealtimeValuesPF6(StreamCapture &stream, const ModelInstrument &model,
                                double time = FP::undefined())
    {
        Q_UNUSED(model);
        if (!stream.hasAnyMatchingValue("ZF2", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkValuesPF12(StreamCapture &stream, const ModelInstrument &model,
                         bool checkFlow = false,
                         double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZSSAR", Variant::Root(model.ssa), time))
            return false;
        if (checkFlow) {
            if (!stream.hasAnyMatchingValue("Q",
                                            Variant::Root(model.flowAsDeltaV() * 60000.0 / 22.0),
                                            time))
                return false;
        }
        return true;
    }

    bool checkRealtimeValuesPF12(StreamCapture &stream, const ModelInstrument &model,
                                 double time = FP::undefined())
    {
        Q_UNUSED(stream);
        Q_UNUSED(model);
        Q_UNUSED(time);
        return true;
    }

    bool checkValuesPF99(StreamCapture &stream, const ModelInstrument &model,
                         double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.Tambient), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.Tmeasure), time))
            return false;
        if (!stream.hasAnyMatchingValue("T3", Variant::Root(model.Tsystem), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q", Variant::Root(model.Q), time))
            return false;
        return true;
    }

    bool checkValuesPF99Alternate(StreamCapture &stream,
                                  const ModelInstrument &model,
                                  double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.Tambient), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.Tmeasure), time))
            return false;
        if (!stream.hasAnyMatchingValue("T3", Variant::Root(model.Tsystem), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q", Variant::Root(model.Q), time))
            return false;
        if (!stream.hasAnyMatchingValue("Pd1", Variant::Root(model.Porifice), time))
            return false;
        if (!stream.hasAnyMatchingValue("Pd2", Variant::Root(model.Ppump), time))
            return false;
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.Pambient - model.Porifice), time))
            return false;
        return true;
    }

    bool checkRealtimeValuesPF99(StreamCapture &stream, const ModelInstrument &model,
                                 double time = FP::undefined())
    {
        Q_UNUSED(stream);
        Q_UNUSED(model);
        Q_UNUSED(time);
        return true;
    }

    bool checkValuesInteractive(StreamCapture &stream, const ModelInstrument &model,
                                double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("Pd1", Variant::Root(model.Porifice), time))
            return false;
        if (!stream.hasAnyMatchingValue("Pd2", Variant::Root(model.Ppump), time))
            return false;
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.Pambient - model.Porifice), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.Tambient), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.Tmeasure), time))
            return false;
        if (!stream.hasAnyMatchingValue("T3", Variant::Root(model.Tsystem), time))
            return false;
        return true;
    }

    bool checkRealtimeValuesInteractive(StreamCapture &stream, const ModelInstrument &model,
                                        double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("ZF2", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZF3", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("PCT", Variant::Root(model.flowRegulatorPercent), time))
            return false;
        return true;
    }

    bool checkAccumulatorsEBC(StreamCapture &stream, const ModelInstrument &model,
                              double baseTime,
                              int baseOffset,
                              int totalCheck,
                              double accumulationOffset,
                              bool isRealtime = false)
    {
        Q_UNUSED(accumulationOffset);

        QList<double> times;
        QList<double> X;
        QList<double> Bac;
        for (int i = 0; i < totalCheck; i++) {
            int reverseIndex = totalCheck - (i + 1);

            times.append(baseTime - reverseIndex * 22.0);

            /*double accumulatedTime = times.last() - accumulationOffset;*/

            int offset = baseOffset + reverseIndex;
            X.append(model.correctedEBC(offset));
            Bac.append(model.correctedAbsorption(offset));
        }

        if (!stream.verifyValues("XR", times, X, !isRealtime))
            return false;
        if (!stream.verifyValues("BacR", times, Bac, !isRealtime))
            return false;
        return true;
    }

    bool checkAccumulatorsPF1(StreamCapture &stream, const ModelInstrument &model,
                              double baseTime,
                              int baseOffset,
                              int totalCheck,
                              double accumulationOffset,
                              bool isRealtime = false)
    {
        return checkAccumulatorsEBC(stream, model, baseTime, baseOffset, totalCheck,
                                    accumulationOffset, isRealtime);
    }

    bool checkAccumulatorsPF2(StreamCapture &stream, const ModelInstrument &model,
                              double baseTime,
                              int baseOffset,
                              int totalCheck,
                              double accumulationOffset,
                              bool isRealtime = false)
    {
        return checkAccumulatorsEBC(stream, model, baseTime, baseOffset, totalCheck,
                                    accumulationOffset, isRealtime);
    }

    bool checkAccumulatorsPF3(StreamCapture &stream, const ModelInstrument &model,
                              double baseTime,
                              int baseOffset,
                              int totalCheck,
                              double accumulationOffset,
                              bool isRealtime = false)
    {
        return checkAccumulatorsEBC(stream, model, baseTime, baseOffset, totalCheck,
                                    accumulationOffset, isRealtime);
    }

    bool checkAccumulatorsPF5(StreamCapture &stream, const ModelInstrument &model,
                              double baseTime,
                              int baseOffset,
                              int totalCheck,
                              double accumulationOffset,
                              bool isRealtime = false)
    {
        return checkAccumulatorsEBC(stream, model, baseTime, baseOffset, totalCheck,
                                    accumulationOffset, isRealtime);
    }

    bool checkAccumulatorsPF6(StreamCapture &stream, const ModelInstrument &model,
                              double baseTime,
                              int baseOffset,
                              int totalCheck,
                              double accumulationOffset,
                              bool isRealtime = false)
    {
        return checkAccumulatorsEBC(stream, model, baseTime, baseOffset, totalCheck,
                                    accumulationOffset, isRealtime);
    }

    bool checkAccumulatorsPF12(StreamCapture &stream, const ModelInstrument &model,
                               double baseTime,
                               int baseOffset,
                               int totalCheck,
                               double accumulationOffset,
                               bool isRealtime = false,
                               bool usePF12Flow = false)
    {
        QList<double> times;
        QList<double> L;
        QList<double> Qt;
        QList<double> Ba;
        QList<double> Ir;
        QList<double> In;
        QList<double> If;
        QList<double> Ip;
        QList<double> Is1;
        QList<double> Is2;
        QList<double> ZIr;
        for (int i = 0; i < totalCheck; i++) {
            int reverseIndex = totalCheck - (i + 1);

            times.append(baseTime - reverseIndex * 22.0);

            double accumulatedTime = times.last() - accumulationOffset;

            double Q;
            if (usePF12Flow) {
                Q = model.flowAsDeltaV() * 60000.0 / 22.0;
            } else {
                Q = model.Q;
            }
            Qt.append(accumulatedTime * Q / 60000.0);
            L.append(Qt.last() / (200.0 * 1E-6));

            int offset = baseOffset + reverseIndex;

            if (usePF12Flow) {
                Ba.append(model.absorptionDeltaV(offset));
            } else {
                Ba.append(model.absorption(offset));
            }
            Ir.append(model.transmittance(offset));
            In.append(model.normalizedIntensity(offset));
            If.append(model.referenceIntensity(offset));
            Ip.append(model.sampleIntensity(offset));
            Is1.append(model.scat135Intensity(offset));
            Is2.append(model.scat165Intensity(offset));
            ZIr.append(model.LOD(offset));
        }

        if (!stream.verifyValues("Qt", times, Qt, !isRealtime))
            return false;
        if (!stream.verifyValues("L", times, L, !isRealtime))
            return false;

        if (!stream.verifyValues("IpR", times, Ip, !isRealtime))
            return false;
        if (!stream.verifyValues("IfR", times, If, !isRealtime))
            return false;
        if (!stream.verifyValues("Is1", times, Is1, !isRealtime))
            return false;
        if (!stream.verifyValues("Is2", times, Is2, !isRealtime))
            return false;
        if (!stream.verifyValues("IrR", times, Ir, !isRealtime))
            return false;
        if (!stream.verifyValues("BaR", times, Ba, !isRealtime))
            return false;
        if (!stream.verifyValues("ZIrR", times, ZIr, !isRealtime))
            return false;

        if (!isRealtime)
            return true;

        if (!stream.verifyValues("InR", times, In, !isRealtime))
            return false;

        return true;
    }

    bool checkAccumulatorsPF99(StreamCapture &stream, const ModelInstrument &model,
                               double baseTime,
                               int baseOffset,
                               int totalCheck,
                               double accumulationOffset,
                               bool isRealtime = false)
    {
        QList<double> times;
        QList<double> L;
        QList<double> Qt;
        QList<double> Ba;
        QList<double> Ir;
        QList<double> In;
        QList<double> If;
        QList<double> Ip;
        QList<double> Is1;
        QList<double> Is2;
        for (int i = 0; i < totalCheck; i++) {
            int reverseIndex = totalCheck - (i + 1);

            times.append(baseTime - reverseIndex * 22.0);

            double accumulatedTime = times.last() - accumulationOffset;

            double Q = model.Q;
            Qt.append(accumulatedTime * Q / 60000.0);
            L.append(Qt.last() / (200.0 * 1E-6));

            int offset = baseOffset + reverseIndex;

            Ba.append(model.absorption(offset));
            Ir.append(model.transmittance(offset));
            In.append(model.normalizedIntensity(offset));
            If.append(model.referenceIntensity(offset));
            Ip.append(model.sampleIntensity(offset));
            Is1.append(model.scat135Intensity(offset));
            Is2.append(model.scat165Intensity(offset));
        }

        if (!stream.verifyValues("Qt", times, Qt, !isRealtime))
            return false;
        if (!stream.verifyValues("L", times, L, !isRealtime))
            return false;

        if (!stream.verifyValues("IpR", times, Ip, !isRealtime))
            return false;
        if (!stream.verifyValues("IfR", times, If, !isRealtime))
            return false;
        if (!stream.verifyValues("Is1", times, Is1, !isRealtime))
            return false;
        if (!stream.verifyValues("Is2", times, Is2, !isRealtime))
            return false;
        if (!stream.verifyValues("IrR", times, Ir, !isRealtime))
            return false;
        if (!stream.verifyValues("BaR", times, Ba, !isRealtime))
            return false;

        if (!isRealtime)
            return true;

        if (!stream.verifyValues("InR", times, In, !isRealtime))
            return false;

        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_thermo_maap5012"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_thermo_maap5012"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("area")));
        QVERIFY(qobject_cast<ComponentOptionSingleCalibration *>(options.get("cal-q")));
    }

    void passiveAutoprobePF1()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledFormat = 1;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(11.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(11.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValuesPF1(logging, instrument));
        QVERIFY(checkValuesPF1(realtime, instrument));
        QVERIFY(checkRealtimeValuesPF1(realtime, instrument));

        double baseTime = (instrument.absoluteCounter - 6) * 22.0;
        QVERIFY(checkAccumulatorsPF1(logging, instrument, baseTime, 5, 10, 1 * 22 - 1, false));
        QVERIFY(checkAccumulatorsPF1(realtime, instrument, baseTime + 22.0, 5, 10, 2 * 22 - 1,
                                     true));

    }

    void passiveAutoprobePF2()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledFormat = 2;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(11.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(11.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValuesPF2(logging, instrument));
        QVERIFY(checkValuesPF2(realtime, instrument));
        QVERIFY(checkRealtimeValuesPF2(realtime, instrument));

        double baseTime = (instrument.absoluteCounter - 6) * 22.0;
        QVERIFY(checkAccumulatorsPF2(logging, instrument, baseTime, 5, 10, 1 * 22 - 1, false));
        QVERIFY(checkAccumulatorsPF2(realtime, instrument, baseTime + 22.0, 5, 10, 2 * 22 - 1,
                                     true));

    }

    void passiveAutoprobePF3()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledFormat = 3;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(11.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(11.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValuesPF3(logging, instrument));
        QVERIFY(checkValuesPF3(realtime, instrument));
        QVERIFY(checkRealtimeValuesPF1(realtime, instrument));

        double baseTime = (instrument.absoluteCounter - 6) * 22.0;
        QVERIFY(checkAccumulatorsPF3(logging, instrument, baseTime, 5, 10, 1 * 22 - 1, false));
        QVERIFY(checkAccumulatorsPF3(realtime, instrument, baseTime + 22.0, 5, 10, 2 * 22 - 1,
                                     true));

    }

    void passiveAutoprobePF5()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledFormat = 5;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(11.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(11.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValuesPF5(logging, instrument));
        QVERIFY(checkValuesPF5(realtime, instrument));
        QVERIFY(checkRealtimeValuesPF5(realtime, instrument));

        double baseTime = (instrument.absoluteCounter - 6) * 22.0;
        QVERIFY(checkAccumulatorsPF5(logging, instrument, baseTime, 5, 10, 1 * 22 - 1, false));
        QVERIFY(checkAccumulatorsPF5(realtime, instrument, baseTime + 22.0, 5, 10, 2 * 22 - 1,
                                     true));

    }

    void passiveAutoprobePF6()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledFormat = 6;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(11.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(11.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValuesPF6(logging, instrument));
        QVERIFY(checkValuesPF6(realtime, instrument));
        QVERIFY(checkRealtimeValuesPF1(realtime, instrument));

        double baseTime = (instrument.absoluteCounter - 6) * 22.0;
        QVERIFY(checkAccumulatorsPF6(logging, instrument, baseTime, 5, 10, 1 * 22 - 1, false));
        QVERIFY(checkAccumulatorsPF6(realtime, instrument, baseTime + 22.0, 5, 10, 2 * 22 - 1,
                                     true));

    }

    void passiveAutoprobePF12()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledFormat = 12;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(11.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(11.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValuesPF12(logging, instrument, true));
        QVERIFY(checkValuesPF12(realtime, instrument, true));
        QVERIFY(checkRealtimeValuesPF12(realtime, instrument));

        double baseTime = (instrument.absoluteCounter - 6) * 22.0;
        QVERIFY(checkAccumulatorsPF12(logging, instrument, baseTime, 5, 10, 1 * 22 - 1, false,
                                      true));
        QVERIFY(checkAccumulatorsPF12(realtime, instrument, baseTime + 22.0, 5, 10, 2 * 22 - 1,
                                      true, true));

    }

    void passiveAutoprobePF99()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledFormat = 99;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(11.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(11.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValuesPF99(logging, instrument));
        QVERIFY(checkValuesPF99(realtime, instrument));
        QVERIFY(checkRealtimeValuesPF99(realtime, instrument));

        double baseTime = (instrument.absoluteCounter - 6) * 22.0;
        QVERIFY(checkAccumulatorsPF99(logging, instrument, baseTime, 5, 10, 1 * 22 - 1));
        QVERIFY(checkAccumulatorsPF99(realtime, instrument, baseTime + 22.0, 5, 10, 2 * 22 - 1,
                                      true));

    }

    void passiveAutoprobePF99Alternate()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledFormat = 98;
        instrument.dIref = 0;
        instrument.dIsam = -3.0;
        instrument.dIscat135 = -1.0;
        instrument.dIscat165 = -2.0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(11.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        auto persistentValues = interface->getPersistentValues();

        for (int i = 0; i < 50; i++) {
            control.advance(11.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValuesPF99Alternate(logging, instrument));
        QVERIFY(checkValuesPF99Alternate(realtime, instrument));
        QVERIFY(checkRealtimeValuesPF99(realtime, instrument));

        double baseTime = (instrument.absoluteCounter - 6) * 22.0;
        QVERIFY(checkAccumulatorsPF99(logging, instrument, baseTime, 5, 10, 1 * 22 - 1));
        QVERIFY(checkAccumulatorsPF99(realtime, instrument, baseTime + 22.0, 5, 10, 2 * 22 - 1, true));

    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.2);
            QTest::qSleep(25);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(0.5);
            QTest::qSleep(25);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValuesPF12(logging, instrument));
        QVERIFY(checkValuesPF12(realtime, instrument));
        QVERIFY(checkRealtimeValuesPF12(realtime, instrument));

        QVERIFY(checkValuesInteractive(logging, instrument));
        QVERIFY(checkValuesInteractive(realtime, instrument));
        QVERIFY(checkRealtimeValuesInteractive(realtime, instrument));

        QVERIFY(!persistentValues.empty());

        /* Timing (intensities vs global) is a mess, so just rely on the
         * PF12 general checking to assume it's correct */

    }

    void interactiveAutoprobeFragment()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.fastResponseMode = ModelInstrument::Fast;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.2);
            QTest::qSleep(25);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(0.5);
            QTest::qSleep(25);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValuesPF12(logging, instrument));
        QVERIFY(checkValuesPF12(realtime, instrument));
        QVERIFY(checkRealtimeValuesPF12(realtime, instrument));

        QVERIFY(checkValuesInteractive(logging, instrument));
        QVERIFY(checkValuesInteractive(realtime, instrument));
        QVERIFY(checkRealtimeValuesInteractive(realtime, instrument));

        QVERIFY(!persistentValues.empty());

        /* Unpredictable timing due to fragmentation, so don't
         * check accumulators */

    }

    void interactiveAutoprobeFragmentFlags()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.fastResponseMode = ModelInstrument::Fast;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.2);
            QTest::qSleep(25);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        instrument.fastResponseMode = ModelInstrument::WaitForFlags;

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.5);
            QTest::qSleep(25);
        }
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValuesPF12(logging, instrument));
        QVERIFY(checkValuesPF12(realtime, instrument));
        QVERIFY(checkRealtimeValuesPF12(realtime, instrument));

        QVERIFY(checkValuesInteractive(logging, instrument));
        QVERIFY(checkValuesInteractive(realtime, instrument));
        QVERIFY(checkRealtimeValuesInteractive(realtime, instrument));

        QVERIFY(!persistentValues.empty());

        /* Unpredictable timing due to fragmentation, so don't
         * check accumulators */

    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledFormat = 12;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 600.0) {
            control.advance(11.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 600.0);
        control.advance(11.0);

        for (int i = 0; i < 50; i++) {
            control.advance(11.0);
            QTest::qSleep(50);
        }

        QVERIFY(logging.hasAnyMatchingValue("F1"));

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValuesPF12(logging, instrument, true));
        QVERIFY(checkValuesPF12(realtime, instrument, true));
        QVERIFY(checkRealtimeValuesPF12(realtime, instrument));

        double baseTime = (instrument.absoluteCounter - 6) * 22.0;
        QVERIFY(checkAccumulatorsPF12(logging, instrument, baseTime, 5, 10, 1 * 22 - 1, false,
                                      true));
        QVERIFY(checkAccumulatorsPF12(realtime, instrument, baseTime + 22.0, 5, 10, 2 * 22 - 1,
                                      true, true));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 120.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(25);
        }
        QVERIFY(control.time() < 120.0);

        double checkTime = control.time();
        while (control.time() < 240.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(25);
        }

        for (int i = 0; i < 100; i++) {
            control.advance(0.5);
            QTest::qSleep(25);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValuesPF12(logging, instrument));
        QVERIFY(checkValuesPF12(realtime, instrument));
        QVERIFY(checkRealtimeValuesPF12(realtime, instrument));

        QVERIFY(checkValuesInteractive(logging, instrument));
        QVERIFY(checkValuesInteractive(realtime, instrument));
        QVERIFY(checkRealtimeValuesInteractive(realtime, instrument));

        QVERIFY(!persistentValues.empty());

        /* Timing (intensities vs global) is a mess, so just rely on the
         * PF12 general checking to assume it's correct */

    }

    void spotAdvance()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 120.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(25);
        }
        QVERIFY(control.time() < 120.0);

        double checkTime = control.time();
        while (control.time() < 240.0) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(25);
        }
        checkTime = control.time();
        control.advance(0.2);

        Variant::Write cmd = Variant::Write::empty();
        cmd["AdvanceSpot"].setBool(true);
        interface->incomingCommand(cmd);

        while (control.time() < 300.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange"), checkTime))
                break;
            QTest::qSleep(25);
        }
        QVERIFY(control.time() < 300.0);
        checkTime = control.time();
        control.advance(0.2);

        while (control.time() < 360.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(25);
        }
        QVERIFY(control.time() < 360.0);
        checkTime = control.time() + 0.1;
        control.advance(0.2);

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface = component->createAcquisitionInteractive(config);
            control.attach(interface.get());
            interface->setState(&control);
            interface->setControlStream(&control);
            control.requestTimeout(0.0);
            interface->start();
            interface->setLoggingEgress(&logging);
            interface->setRealtimeEgress(&realtime);
            interface->setPersistentEgress(&persistent);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }
        }

        while (control.time() < 600.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(25);
        }
        QVERIFY(control.time() < 600.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(!persistentValues.empty());

        /* Timing (intensities vs global) is a mess, so just rely on the
         * PF12 general checking to assume it's correct */

    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
