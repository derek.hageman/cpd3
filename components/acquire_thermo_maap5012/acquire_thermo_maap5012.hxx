/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIRETHERMOMAAP5012_H
#define ACQUIRETHERMOMAAP5012_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "acquisition/spancheck.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "core/number.hxx"
#include "smoothing/baseline.hxx"

enum BCUnits {
    BC_unknown, BC_ugm3, BC_ngm3
};

class AcquireThermoMAAP5012 : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;
    double lastEmbeddedTime;
    double forwardOffset;
    int autoprobePassiveValidRecords;
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Passive autoprobe initialization, ignoring the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,

        /* Acquiring interactive data, in active state querying the
         * instrument */
                RESP_INTERACTIVE_RUN_QUERYING,
        /* Acquiring interactive data, with a partial record received */
                RESP_INTERACTIVE_RUN_QUERYING_PARTIAL,
        /* Acquiring interactive data, with a complete record received */
                RESP_INTERACTIVE_RUN_QUERYING_COMPLETE,
        /* Acquiring interactive data, waiting for the completion of the
         * record before querying again */
                RESP_INTERACTIVE_RUN_WAITING,

        /* Waiting for the completion of a "D 0" command to stop record
         * output */
                RESP_INTERACTIVE_START_STOPREPORTS_INITIAL,
        /* Waiting for the completion of a "N" command to flush any remaining
         * contents, this also clears the error counter */
                RESP_INTERACTIVE_START_STOPREPORTS_FLUSH,
        /* Waiting for the completion of the "KK 1" command */
                RESP_INTERACTIVE_START_SETENGLISH,
        /* Waiting for a valid response to the "v" command */
                RESP_INTERACTIVE_START_READVERSION_BEGIN,
        /* Waiting for a timeout signaling the end of the "v" response */
                RESP_INTERACTIVE_START_READVERSION_END,
        /* Waiting for the response to a "?" command */
                RESP_INTERACTIVE_START_READADDRESS,
        /* Waiting for the completion of the "Z" command to set the time */
                RESP_INTERACTIVE_START_SETTIME,
        /* Waiting for the completion of the "d2 0" command */
                RESP_INTERACTIVE_START_SETRECORDMINUTES,
        /* Waiting for the completion of the "d3 0" command */
                RESP_INTERACTIVE_START_SETRECORDSECONDS,
        /* Waiting for the completion of the "K0 0" command */
                RESP_INTERACTIVE_START_SETAVERAGING,
        /* Waiting for the completion of the "K1 x" command */
                RESP_INTERACTIVE_START_SETADVANCETR,
        /* Waiting for the completion of the "K2 x" command */
                RESP_INTERACTIVE_START_SETADVANCEHOUR,
        /* Waiting for the completion of the "K3 x" command */
                RESP_INTERACTIVE_START_SETADVANCEATHOUR,
        /* Waiting for the completion of the "K4 x" command */
                RESP_INTERACTIVE_START_SETTARGETFLOW,
        /* Waiting for the completion of the "KN 0" command */
                RESP_INTERACTIVE_START_SETSTANDARDTEMP,
        /* Waiting for the completion of the "KM 1" command */
                RESP_INTERACTIVE_START_SETSTP, /* Waiting for the completion of the "D 8" command */
                RESP_INTERACTIVE_START_SETPARAMETERLIST,
        /* Issued a "P" command, waiting for the first line of parameters */
                RESP_INTERACTIVE_START_READPARAMETERS_BEGIN, /* Resent the P command again */
                RESP_INTERACTIVE_START_READPARAMETERS_BEGIN_RETRY,
        /* Waiting for a timeout signaling the end of parameters (or an
         * "END" line ending it immediately) */
                RESP_INTERACTIVE_START_READPARAMETERS_END,
        /* Read the error counter, to make sure it's still zero (all commands
         * received ok) */
                RESP_INTERACTIVE_START_READERRORCOUNTER,
        /* Waiting for a complete line (will discard a partial output) */
                RESP_INTERACTIVE_START_FIRSTVALID,

        /* Waiting before attempting a communications restart */
                RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
                RESP_INTERACTIVE_INITIALIZE,
    };
    ResponseState responseState;

    enum CommandType {
        COMMAND_READ_CONCENTRATION,         /* C */

        /* Note that the manual is wrong for what these actually are, so
         * this is based on what firmware 1.33 was outputting */
                COMMAND_READ_TEMPERATURE_AMBIENT, /* J0 */
                COMMAND_READ_TEMPERATURE_MEASURE, /* J1 */
                COMMAND_READ_TEMPERATURE_SYSTEM, /* J2 */
                COMMAND_READ_PRESSURE_ORIFICE, /* J3 */
                COMMAND_READ_PRESSURE_PUMP, /* J4 */
                COMMAND_READ_PRESSURE_AMBIENT, /* J5 */

                COMMAND_READ_FLOW, /* JK */
                COMMAND_READ_VOLUME, /* JN */
                COMMAND_READ_PUMP_SIGNAL, /* JM */
                COMMAND_READ_STATUS, /* # */

                COMMAND_READ_INTENSITY_REFERENCE, /* JA */
                COMMAND_READ_INTENSITY_BS135, /* JB */
                COMMAND_READ_INTENSITY_BS165, /* JC */
                COMMAND_READ_INTENSITY_TR,          /* JD */

        /* A command that has a single response, but that we don't
         * handle it */
                COMMAND_UNHANDLED,

        /* A specifically invalid command */
                COMMAND_INVALID,
    };

    class Command {
        CommandType ty;
        CPD3::Util::ByteArray dt;
    public:
        Command();

        Command(CommandType t, CPD3::Util::ByteArray d = {});

        inline Command(CommandType t, const char *d) : Command(t, CPD3::Util::ByteArray(d))
        { }

        Command(const Command &other);

        Command &operator=(const Command &other);

        inline bool valid() const
        { return ty != COMMAND_INVALID; }

        inline CommandType type() const
        { return ty; }

        inline const CPD3::Util::ByteArray &data() const
        { return dt; }

        void invalidate();

        CPD3::Data::Variant::Root stateDescription() const;
    };

    Command issuedCommand;
    QList<Command> commandBacklog;

    enum LogStreamID {
        LogStream_Metadata = 0,

        /* Includes flags, volume integration, and calculated absorption and
         * transmittance */
                LogStream_Common,

        /* All in PF12 records */
                LogStream_BC,
        LogStream_IntensityTr,
        LogStream_IntensityBS135,
        LogStream_IntensityBS165,
        LogStream_IntensityRef,
        LogStream_SSA,
        LogStream_LOD,

        /* From interactive querying */
                LogStream_TemperatureAmbient,
        LogStream_PressurePump, LogStream_PressureOrifice,
        LogStream_TemperatureMeasure,
        LogStream_TemperatureSystem,

        LogStream_TOTAL,
        LogStream_ReadBegin = LogStream_BC
    };
    CPD3::Data::StaticMultiplexer loggingMux;
    quint32 streamHit[LogStream_TOTAL];
    quint32 streamOutdated[LogStream_TOTAL];
    double streamTime[LogStream_TOTAL];

    friend class Configuration;

    class Configuration {
        double start;
        double end;
    public:
        CPD3::Calibration flowCalibration;
        double area;
        double wavelength;
        BCUnits bcUnits;

        double reportInterval;

        double advanceTransmittance;
        int advanceHours;
        int advanceAtHour;

        double targetFlow;

        int address;
        bool checkNormalizationInstrumentID;

        bool enableAutodetect;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    class IntensityData {
    public:
        double startTime;
        double Ip;
        double If;
        double In;
        double Is1;
        double Is2;

        IntensityData();

        bool operator==(const IntensityData &other) const;

        inline bool operator!=(const IntensityData &other) const
        { return !(*this == other); }
    };

    friend QDataStream &operator<<(QDataStream &stream, const IntensityData &data);

    friend QDataStream &operator>>(QDataStream &stream, IntensityData &data);

    /* State that needs to be saved */
    double restoreInstrumentVolume;
    double restoreTime;
    double reportedInstrumentVolume;
    double accumulateVolume;
    IntensityData filterBegin;
    IntensityData filterLatest;
    CPD3::Data::Variant::Root filterSerialNumber;
    CPD3::Data::Variant::Root filterFirmwareVersion;
    CPD3::Data::Variant::Root filterAddress;

    IntensityData filterBeginPending;

    CPD3::Data::SequenceValue persistentParameters;
    bool persistentParametersUpdated;
    bool realtimeParametersUpdated;

    bool haveAcceptedNormalization;
    double accumulateTime;
    double priorIr;
    double flowHighPrecision;
    double flowLowPrecision;
    double orificePressureDrop;
    double ambientPressure;
    double lastLowPrecisionFlow;
    double lastKnownFlow;
    double lastKnownRawFlow;
    double lastKnownAbsorption;
    BCUnits lastBCUnits;
    bool priorFilterChanging;
    bool filterChangeWasAutodetected;

    CPD3::Data::Variant::Root instrumentMeta;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool requireFilterBreak;
    bool forceFilterBreak;
    bool forceRealtimeStateEmit;

    bool reportingAtSTP;
    double reportingT;
    double absorptionEfficiency;

    CPD3::Data::Variant::Flags reportedFlags;
    enum {
        ReportFlag_Normal, ReportFlag_Warning, ReportFlag_Error,
    } reportFlagStatus;

    CPD3::Data::DynamicTimeInterval *acceptNormalizationTime;
    CPD3::Smoothing::BaselineSmoother *filterChangeIp;

    void setDefaultInvalid();

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void logValue(double time,
                  int streamID,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    void invalidateLogValues(double frameTime);

    void globalAdvance(double time);

    void streamAdvance(int streamID, double time);

    CPD3::Data::Variant::Root buildNormalizationValue() const;

    bool reportedNormalizationMatchesEffective() const;

    bool isSameNormalization(double time) const;

    bool instrumentIDMatchesNormalization() const;

    void instrumentIDUpdated();

    bool inInteractiveMode() const;

    void configurationAdvance(double frameTime);

    void configurationChanged();

    void describeState(CPD3::Data::Variant::Write &info) const;

    void emitNormalizationEnd(double endTime);

    void writeCommand(const CPD3::Util::ByteView &command);

    inline void writeCommand(const char *command)
    { return writeCommand(CPD3::Util::ByteView(command)); }

    void issueCommand(const Command &command, double frameTime);

    void interactiveStartBegin(double time);

    void invalidStartResponse(double time);

    void invalidRunResponse(const CPD3::Util::ByteView &frame, double frameTime, int code);

    void processEBC(CPD3::Data::Variant::Write &X, const CPD3::Util::ByteView &field);

    inline void processEBC(CPD3::Data::Variant::Root &X, const CPD3::Util::ByteView &field)
    {
        auto w = X.write();
        return processEBC(w, field);
    }

    int processPF1(std::deque<CPD3::Util::ByteView> &fields, double frameTime);

    int processPF2(std::deque<CPD3::Util::ByteView> &fields, double frameTime);

    int processPF3(std::deque<CPD3::Util::ByteView> &fields, double frameTime);

    int processPF5(std::deque<CPD3::Util::ByteView> &fields, double frameTime);

    int processPF99Alternate(std::deque<CPD3::Util::ByteView> &fields, double frameTime);

    int processPF99(std::deque<CPD3::Util::ByteView> &fields, double frameTime);

    int processPF12Start(std::deque<CPD3::Util::ByteView> &fields,
                         double &frameTime,
                         bool skipDateTime = false,
                         bool alterState = true);

    int processPF12End(std::deque<CPD3::Util::ByteView> &fields,
                       double frameTime,
                       bool alterState = true);

    int processNetworkReport(std::deque<CPD3::Util::ByteView> &fields, double frameTime);

    void processGlobalStatus(const CPD3::Data::Variant::Read &ZF2);

    int processDateTime(std::deque<CPD3::Util::ByteView> &fields,
                        double &frameTime,
                        bool timeIsStart = false,
                        bool alterState = true);

    void parseParameters(const CPD3::Util::ByteView &frame);

    int processIncomingRecord(std::deque<CPD3::Util::ByteView> &fields,
                              const CPD3::Util::ByteView &frame,
                              double &frameTime,
                              int suffixFields = 0);

public:
    AcquireThermoMAAP5012(const CPD3::Data::ValueSegment::Transfer &config,
                          const std::string &loggingContext);

    AcquireThermoMAAP5012(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireThermoMAAP5012();

    CPD3::Data::SequenceValue::Transfer getPersistentValues() override;

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    CPD3::Data::Variant::Root getCommands() override;

    CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables() override;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus() override;

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    void serializeState(QDataStream &stream) override;

    void deserializeState(QDataStream &stream) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    void command(const CPD3::Data::Variant::Read &value) override;

    std::size_t dataFrameStart(std::size_t offset,
                               const CPD3::Util::ByteArray &input) const override;

    std::size_t dataFrameEnd(std::size_t start, const CPD3::Util::ByteArray &input) const override;

    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;
};

class AcquireThermoMAAP5012Component
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_thermo_maap5012"
                              FILE
                              "acquire_thermo_maap5012.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
