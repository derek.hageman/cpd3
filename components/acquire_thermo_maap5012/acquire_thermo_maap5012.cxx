/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QDateTime>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "datacore/wavelength.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_thermo_maap5012.hxx"


using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Smoothing;

/* When the model isn't converging well (e.x. during zero) it can take
 * almost a minute to run, so double that to allow for partial lines */
static const double pf12Timeout = 120.0;

static const char *detailedStatusBitsTranslation[64] =
        {"PROMError",                /* 0x0000 0000 0000 0001 */
         "RAMERROR",                 /* 0x0000 0000 0000 0002 */
         "NVRAMError",               /* 0x0000 0000 0000 0004 */
         "EEPROMError",              /* 0x0000 0000 0000 0008 */
         NULL,                       /* 0x0000 0000 0000 0010 */
         NULL,                       /* 0x0000 0000 0000 0020 */
         NULL,                       /* 0x0000 0000 0000 0040 */
         NULL,                       /* 0x0000 0000 0000 0080 */
         NULL,                       /* 0x0000 0000 0000 0100 */
         NULL,                       /* 0x0000 0000 0000 0200 */
         NULL,                       /* 0x0000 0000 0000 0400 */
         "NegativePressureError",    /* 0x0000 0000 0000 0800 */
         "TapeGapOpenError",         /* 0x0000 0000 0000 1000 */
         "TapeAdvanceError",         /* 0x0000 0000 0000 2000 */
         NULL,                       /* 0x0000 0000 0000 4000 */
         "ImmediateChangeError",     /* 0x0000 0000 0000 8000 */

         "OrificePressureOutOfRange",/* 0x0000 0000 0001 0000 */
         "PumpPressureOutOfRange",   /* 0x0000 0000 0002 0000 */
         "AmbientPressureOutOfRange",/* 0x0000 0000 0004 0000 */
         NULL,                       /* 0x0000 0000 0008 0000 */
         NULL,                       /* 0x0000 0000 0010 0000 */
         "OrificePressureLow",       /* 0x0000 0000 0020 0000 */
         NULL,                       /* 0x0000 0000 0040 0000 */
         NULL,                       /* 0x0000 0000 0080 0000 */
         "FlowRegulationUnstable",   /* 0x0000 0000 0100 0000 */
         "FlowRegulatorFullyOpen",   /* 0x0000 0000 0200 0000 */
         "FlowRegulatorFullyClosed", /* 0x0000 0000 0400 0000 */
         NULL,                       /* 0x0000 0000 0800 0000 */
         NULL,                       /* 0x0000 0000 1000 0000 */
         NULL,                       /* 0x0000 0000 2000 0000 */
         NULL,                       /* 0x0000 0000 4000 0000 */
         NULL,                       /* 0x0000 0000 8000 0000 */

         "ReferenceOutOfRange",      /* 0x0000 0001 0000 0000 */
         "TransmitanceOutOfRange",   /* 0x0000 0002 0000 0000 */
         "BS165OutOfRange",          /* 0x0000 0004 0000 0000 */
         "BS135OutOfRange",          /* 0x0000 0008 0000 0000 */
         "IlluminatedSignalLow",     /* 0x0000 0010 0000 0000 */
         "DarkSignalHigh",           /* 0x0000 0020 0000 0000 */
         NULL,                       /* 0x0000 0040 0000 0000 */
         NULL,                       /* 0x0000 0080 0000 0000 */
         "AmbientTemperatureShortCircuit",/* 0x0000 0100 0000 0000 */
         "AmbientTemperatureShortInterruption",/* 0x0000 0200 0000 0000 */
         "MeasurementTemperatureShortCircuit",/* 0x0000 0400 0000 0000 */
         "MeasurementTemperatureShortInterruption",/* 0x0000 0800 0000 0000 */
         "SystemTemperatureShortCircuit",/* 0x0000 1000 0000 0000 */
         "SystemTemperatureShortInterruption",/* 0x0000 2000 0000 0000 */
         NULL,                       /* 0x0000 4000 0000 0000 */
         NULL,                       /* 0x0000 8000 0000 0000 */

         NULL,                       /* 0x0001 0000 0000 0000 */
         NULL,                       /* 0x0002 0000 0000 0000 */
         NULL,                       /* 0x0004 0000 0000 0000 */
         NULL,                       /* 0x0008 0000 0000 0000 */
         NULL,                       /* 0x0010 0000 0000 0000 */
         NULL,                       /* 0x0020 0000 0000 0000 */
         NULL,                       /* 0x0040 0000 0000 0000 */
         NULL,                       /* 0x0080 0000 0000 0000 */
         NULL,                       /* 0x0100 0000 0000 0000 */
         NULL,                       /* 0x0200 0000 0000 0000 */
         NULL,                       /* 0x0400 0000 0000 0000 */
         NULL,                       /* 0x0800 0000 0000 0000 */
         NULL,                       /* 0x1000 0000 0000 0000 */
         NULL,                       /* 0x2000 0000 0000 0000 */
         NULL,                       /* 0x4000 0000 0000 0000 */
         NULL,                       /* 0x8000 0000 0000 0000 */
        };

static const qint64 globalStatusErrorMask = 0xFF0000;
static const qint64 globalStatusWarningMask = 0x000F00;
static const char *globalStatusBitsTranslation[24] = {"FilterChanging",           /* 0x000001 */
                                                      "Zero",                     /* 0x000002 */
                                                      NULL,                       /* 0x000004 */
                                                      "PumpOff",                  /* 0x000008 */
                                                      "ManualOperation",          /* 0x000010 */
                                                      "CalibrationEnabled",       /* 0x000020 */
                                                      NULL,                       /* 0x000040 */
                                                      "MainsOn",                  /* 0x000080 */

                                                      "LEDWeak",                  /* 0x000100 */
                                                      NULL,                       /* 0x000200 */
                                                      NULL,                       /* 0x000400 */
                                                      NULL,                       /* 0x000800 */
                                                      NULL,                       /* 0x001000 */
                                                      NULL,                       /* 0x002000 */
                                                      NULL,                       /* 0x004000 */
                                                      NULL,                       /* 0x008000 */

                                                      "MemoryError",              /* 0x010000 */
                                                      "MechanicalError",          /* 0x020000 */
                                                      "PressureError",            /* 0x040000 */
                                                      "FlowError",                /* 0x080000 */
                                                      "DetectorError",            /* 0x100000 */
                                                      "TemperatureError",         /* 0x200000 */
                                                      NULL,                       /* 0x400000 */
                                                      NULL,                       /* 0x800000 */
};

AcquireThermoMAAP5012::Configuration::Configuration() : start(FP::undefined()),
                                                        end(FP::undefined()),
                                                        flowCalibration(),
                                                        area(200.0),
                                                        wavelength(670.0),
                                                        bcUnits(BC_unknown),
                                                        reportInterval(pf12Timeout),
                                                        advanceTransmittance(0.7),
                                                        advanceHours(100),
                                                        advanceAtHour(-1),
                                                        targetFlow(FP::undefined()),
                                                        address(-1),
                                                        checkNormalizationInstrumentID(true),
                                                        enableAutodetect(false)
{ }

AcquireThermoMAAP5012::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          flowCalibration(other.flowCalibration),
          area(other.area),
          wavelength(other.wavelength),
          bcUnits(other.bcUnits),
          reportInterval(other.reportInterval),
          advanceTransmittance(other.advanceTransmittance),
          advanceHours(other.advanceHours),
          advanceAtHour(other.advanceAtHour),
          targetFlow(other.targetFlow),
          address(other.address),
          checkNormalizationInstrumentID(other.checkNormalizationInstrumentID),
          enableAutodetect(other.enableAutodetect)
{ }

AcquireThermoMAAP5012::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          flowCalibration(),
          area(200.0),
          wavelength(670.0),
          bcUnits(BC_unknown),
          reportInterval(pf12Timeout),
          advanceTransmittance(0.7),
          advanceHours(100),
          advanceAtHour(-1),
          targetFlow(FP::undefined()),
          address(-1),
          checkNormalizationInstrumentID(true),
          enableAutodetect(false)
{
    setFromSegment(other);
}

AcquireThermoMAAP5012::Configuration::Configuration(const Configuration &under,
                                                    const ValueSegment &over,
                                                    double s,
                                                    double e) : start(s),
                                                                end(e),
                                                                flowCalibration(
                                                                        under.flowCalibration),
                                                                area(under.area),
                                                                wavelength(under.wavelength),
                                                                bcUnits(under.bcUnits),
                                                                reportInterval(
                                                                        under.reportInterval),
                                                                advanceTransmittance(
                                                                        under.advanceTransmittance),
                                                                advanceHours(under.advanceHours),
                                                                advanceAtHour(under.advanceAtHour),
                                                                targetFlow(under.targetFlow),
                                                                address(under.address),
                                                                checkNormalizationInstrumentID(
                                                                        under.checkNormalizationInstrumentID),
                                                                enableAutodetect(
                                                                        under.enableAutodetect)
{
    setFromSegment(over);
}

void AcquireThermoMAAP5012::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["FlowCalibration"].exists())
        flowCalibration = Variant::Composite::toCalibration(config["FlowCalibration"]);

    if (FP::defined(config["Area/#0"].toDouble())) {
        area = config["Area/#0"].toDouble();
        if (FP::defined(area) && area < 0.1)
            area *= 1E6;
    }
    if (FP::defined(config["Area"].toDouble())) {
        area = config["Area"].toDouble();
        if (FP::defined(area) && area < 0.1)
            area *= 1E6;
    }

    if (FP::defined(config["Wavelength/R"].toDouble()))
        wavelength = config["Wavelength/R"].toDouble();
    if (FP::defined(config["Wavelength"].toDouble()))
        wavelength = config["Wavelength"].toDouble();

    const auto &s = config["BCUnits"].toString();
    if (!s.empty()) {
        if (Util::equal_insensitive(s, "u", "ug", "ugm3", "ug/m3")) {
            bcUnits = BC_ugm3;
        } else if (Util::equal_insensitive(s, "n", "ng", "ngm3", "ng/m3")) {
            bcUnits = BC_ngm3;
        } else {
            bcUnits = BC_unknown;
        }
    }

    {
        double v = config["ReportInterval"].toDouble();
        if (FP::defined(v) && v > 0.0)
            reportInterval = v;
    }

    if (config["Spot/AdvanceTransmittance"].exists()) {
        advanceTransmittance = config["Spot/AdvanceTransmittance"].toDouble();
    }
    if (INTEGER::defined(config["Spot/AdvanceHours"].toInt64())) {
        advanceHours = (int) config["Spot/AdvanceHours"].toInt64();
    }
    if (INTEGER::defined(config["Spot/AdvanceAtHour"].toInt64())) {
        advanceAtHour = (int) config["Spot/AdvanceAtHour"].toInt64() - 1;
    }

    if (config["Filter/CheckInstrumentID"].exists()) {
        checkNormalizationInstrumentID = config["Filter/CheckInstrumentID"].toBool();
    }

    if (config["TargetFlow"].exists()) {
        targetFlow = config["TargetFlow"].toDouble();
    }

    if (config["Autodetect/Enable"].exists())
        enableAutodetect = config["Autodetect/Enable"].toBool();

    if (config["Address"].exists()) {
        qint64 i = config["Address"].toInt64();
        if (INTEGER::defined(i) && i >= 0)
            address = (int) i;
        else
            address = -1;
    }
}


AcquireThermoMAAP5012::IntensityData::IntensityData() : startTime(FP::undefined()),
                                                        Ip(FP::undefined()),
                                                        If(FP::undefined()),
                                                        In(FP::undefined()),
                                                        Is1(FP::undefined()),
                                                        Is2(FP::undefined())
{ }

bool AcquireThermoMAAP5012::IntensityData::operator==(const IntensityData &other) const
{
    if (!FP::defined(If) || !FP::defined(other.If))
        return false;
    if (If != other.If)
        return false;

    if (!FP::defined(Ip) || !FP::defined(other.Ip))
        return false;
    if (Ip != other.Ip)
        return false;

    if (!FP::defined(Is1) || !FP::defined(other.Is1))
        return false;
    if (Is1 != other.Is1)
        return false;

    if (!FP::defined(Is2) || !FP::defined(other.Is2))
        return false;
    if (Is2 != other.Is2)
        return false;

    return true;
}

QDataStream &operator<<(QDataStream &stream, const AcquireThermoMAAP5012::IntensityData &data)
{
    stream << data.startTime << data.Ip << data.If << data.In << data.Is1 << data.Is2;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, AcquireThermoMAAP5012::IntensityData &data)
{
    stream >> data.startTime >> data.Ip >> data.If >> data.In >> data.Is1 >> data.Is2;
    return stream;
}


void AcquireThermoMAAP5012::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Thermo");
    instrumentMeta["Model"].setString("5012");

    persistentParameters.setUnit(SequenceName({}, "raw", "ZPARAMETERS"));
    persistentParametersUpdated = false;
    realtimeParametersUpdated = false;

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    requireFilterBreak = false;
    forceFilterBreak = false;
    forceRealtimeStateEmit = true;

    accumulateVolume = FP::undefined();
    haveAcceptedNormalization = true;

    restoreInstrumentVolume = FP::undefined();
    restoreTime = FP::undefined();
    reportedInstrumentVolume = FP::undefined();

    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamHit[i] = 0;
        streamOutdated[i] = 0;
    }

    reportingAtSTP = true;
    reportingT = 0.0;
    absorptionEfficiency = 6.6;

    priorIr = FP::undefined();
    accumulateVolume = FP::undefined();
    orificePressureDrop = FP::undefined();
    ambientPressure = FP::undefined();
    flowHighPrecision = FP::undefined();
    flowLowPrecision = FP::undefined();
    lastLowPrecisionFlow = FP::undefined();
    lastKnownFlow = FP::undefined();
    lastKnownRawFlow = FP::undefined();
    lastKnownAbsorption = FP::undefined();
    lastBCUnits = BC_unknown;
    accumulateTime = FP::undefined();
    priorFilterChanging = false;
    filterChangeWasAutodetected = false;

    reportFlagStatus = ReportFlag_Normal;
}


AcquireThermoMAAP5012::AcquireThermoMAAP5012(const ValueSegment::Transfer &configData,
                                             const std::string &loggingContext) : FramedInstrument(
        "maap", loggingContext),
                                                                                  lastRecordTime(
                                                                                          FP::undefined()),
                                                                                  lastEmbeddedTime(
                                                                                          FP::undefined()),
                                                                                  forwardOffset(
                                                                                          FP::undefined()),
                                                                                  autoprobePassiveValidRecords(
                                                                                          0),
                                                                                  autoprobeStatus(
                                                                                          AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                  responseState(
                                                                                          RESP_PASSIVE_WAIT),
                                                                                  issuedCommand(),
                                                                                  commandBacklog(),
                                                                                  loggingMux(
                                                                                          LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }

    DynamicTimeInterval::Variable *defaultTime = new DynamicTimeInterval::Variable;
    defaultTime->set(Time::Minute, 30, false);
    acceptNormalizationTime =
            DynamicTimeInterval::fromConfiguration(configData, "Spot/ResumeTimeout",
                                                   FP::undefined(), FP::undefined(), true,
                                                   defaultTime);

    delete defaultTime;

    Variant::Write defaultSmoother = Variant::Write::empty();
    defaultSmoother["Type"].setString("FixedTime");
    defaultSmoother["RSD"].setDouble(0.05);
    defaultSmoother["Time"].setDouble(600.0);
    defaultSmoother["MinimumTime"].setDouble(120.0);
    defaultSmoother["DiscardTime"].setDouble(120.0);
    defaultSmoother["Band"].setDouble(1.5);
    filterChangeIp =
            BaselineSmoother::fromConfiguration(defaultSmoother, configData, "Filter/Smoother");

    configurationChanged();
}

void AcquireThermoMAAP5012::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE;
    autoprobePassiveValidRecords = 0;
}

void AcquireThermoMAAP5012::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireThermoMAAP5012Component::getBaseOptions()
{
    ComponentOptions options;

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(tr("spot", "name"), tr("Area of the sample spot"),
                                            tr("This is the area of the sample spot.  If it is greater "
                                               "than 0.1 it is assumed to be in mm\xC2\xB2, otherwise it is "
                                               "treated as being in m\xC2\xB2."),
                                            tr("200 mm\xC2\xB2"), 1);
    d->setMinimum(0.0, false);
    options.add("area", d);

    ComponentOptionSingleCalibration *cal =
            new ComponentOptionSingleCalibration(tr("cal-q", "name"), tr("Flow rate calibration"),
                                                 tr("This is the calibration applied to the flow reported by "
                                                    "the instrument."), "");
    cal->setAllowInvalid(false);
    cal->setAllowConstant(true);
    options.add("cal-q", cal);

    BaselineSmootherOption *smoother =
            new BaselineSmootherOption(tr("filter-change", "name"), tr("Filter change smoother"),
                                       tr("This defines the smoothing used to detect the start of "
                                          "filter changes.  When not set only the flags "
                                          "are used in detection."), QString());
    smoother->setDefault(new BaselineFixedTime(120, 600, 0.05, 1.5, 120.0));
    smoother->setSpikeDetection(true);
    smoother->setStabilityDetection(true);
    options.add("filter-change", smoother);

    return options;
}

AcquireThermoMAAP5012::AcquireThermoMAAP5012(const ComponentOptions &options,
                                             const std::string &loggingContext) : FramedInstrument(
        "maap", loggingContext),
                                                                                  lastRecordTime(
                                                                                          FP::undefined()),
                                                                                  lastEmbeddedTime(
                                                                                          FP::undefined()),
                                                                                  forwardOffset(
                                                                                          FP::undefined()),
                                                                                  autoprobePassiveValidRecords(
                                                                                          0),
                                                                                  autoprobeStatus(
                                                                                          AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                  responseState(
                                                                                          RESP_PASSIVE_WAIT),
                                                                                  issuedCommand(),
                                                                                  commandBacklog(),
                                                                                  loggingMux(
                                                                                          LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());

    if (options.isSet("area")) {
        double value = qobject_cast<ComponentOptionSingleDouble *>(options.get("area"))->get();
        Q_ASSERT(FP::defined(value) && value > 0.0);
        if (value < 0.1)
            value *= 1E6;
        config.first().area = value;
    }

    if (options.isSet("cal-q")) {
        config.first().flowCalibration =
                qobject_cast<ComponentOptionSingleCalibration *>(options.get("cal-q"))->get();
    }

    acceptNormalizationTime = new DynamicTimeInterval::Undefined;

    filterChangeIp =
            qobject_cast<BaselineSmootherOption *>(options.get("filter-change"))->getSmoother();
    if (options.isSet("filter-change")) {
        config.first().enableAutodetect = true;
    }

    configurationChanged();
}

AcquireThermoMAAP5012::~AcquireThermoMAAP5012()
{
    delete acceptNormalizationTime;
    delete filterChangeIp;
}

AcquireThermoMAAP5012::Command::Command() : ty(COMMAND_INVALID), dt()
{ }

AcquireThermoMAAP5012::Command::Command(CommandType t, Util::ByteArray d) : ty(t), dt(std::move(d))
{ }

AcquireThermoMAAP5012::Command::Command(const Command &other) : ty(other.ty), dt(other.dt)
{ }

AcquireThermoMAAP5012::Command &AcquireThermoMAAP5012::Command::operator=(const Command &other)
{
    if (&other == this)
        return *this;
    ty = other.ty;
    dt = other.dt;
    return *this;
}

void AcquireThermoMAAP5012::Command::invalidate()
{
    ty = COMMAND_INVALID;
    dt = QByteArray();
}

Variant::Root AcquireThermoMAAP5012::Command::stateDescription() const
{
    Variant::Root result;
    if (!dt.empty())
        result["Payload"].setString(dt.toString());

    switch (ty) {
    case COMMAND_UNHANDLED:
        result["Type"].setString("Unhandled");
        break;
    case COMMAND_INVALID:
        result["Type"].setString("Invalid");
        break;
    case COMMAND_READ_CONCENTRATION:
        result["Type"].setString("ReadConcentration");
        break;
    case COMMAND_READ_TEMPERATURE_AMBIENT:
        result["Type"].setString("ReadAmbientTemperature");
        break;
    case COMMAND_READ_TEMPERATURE_MEASURE:
        result["Type"].setString("ReadMeasurementHeadTemperature");
        break;
    case COMMAND_READ_TEMPERATURE_SYSTEM:
        result["Type"].setString("ReadSystemTemperature");
        break;
    case COMMAND_READ_PRESSURE_ORIFICE:
        result["Type"].setString("ReadOrificePressure");
        break;
    case COMMAND_READ_PRESSURE_PUMP:
        result["Type"].setString("ReadPumpPressure");
        break;
    case COMMAND_READ_PRESSURE_AMBIENT:
        result["Type"].setString("ReadAmbientPressure");
        break;
    case COMMAND_READ_FLOW:
        result["Type"].setString("ReadFlow");
        break;
    case COMMAND_READ_VOLUME:
        result["Type"].setString("ReadAccumulatedVolume");
        break;
    case COMMAND_READ_PUMP_SIGNAL:
        result["Type"].setString("ReadPumpRegulatorSignal");
        break;
    case COMMAND_READ_STATUS:
        result["Type"].setString("ReadStatusFlags");
        break;
    case COMMAND_READ_INTENSITY_REFERENCE:
        result["Type"].setString("ReadReferenceIntensity");
        break;
    case COMMAND_READ_INTENSITY_BS135:
        result["Type"].setString("Read135DegreeScatteringIntensity");
        break;
    case COMMAND_READ_INTENSITY_BS165:
        result["Type"].setString("Read165DegreeScatteringIntensity");
        break;
    case COMMAND_READ_INTENSITY_TR:
        result["Type"].setString("ReadTransmissionIntensity");
        break;
    }

    return result;
}

SequenceValue::Transfer AcquireThermoMAAP5012::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_thermo_maap5012");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    processing["Address"].set(instrumentMeta["Address"]);


    result.emplace_back(SequenceName({}, "raw_meta", "Q"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    if (reportingAtSTP) {
        result.back().write().metadataReal("ReportT").setDouble(reportingT);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Flow"));

    result.emplace_back(SequenceName({}, "raw_meta", "Qt"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.00000");
    result.back().write().metadataReal("Units").setString("m³");
    result.back().write().metadataReal("Description").setString("Accumulated sample volume");
    if (reportingAtSTP) {
        result.back().write().metadataReal("ReportT").setDouble(reportingT);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");

    result.emplace_back(SequenceName({}, "raw_meta", "L"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0000");
    result.back().write().metadataReal("Units").setString("m");
    result.back().write().metadataReal("Description").setString("Integrated sample length, Qt/A");
    if (reportingAtSTP) {
        result.back().write().metadataReal("ReportT").setDouble(reportingT);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");

    result.emplace_back(SequenceName({}, "raw_meta", "Ir" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0.0000000");
    result.back().write().metadataReal("GroupUnits").setString("Transmittance");
    result.back().write().metadataReal("Description").setString("Transmittance");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Transmittance"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);

    result.emplace_back(SequenceName({}, "raw_meta", "Ba" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light absorption coefficient");
    if (reportingAtSTP) {
        result.back().write().metadataReal("ReportT").setDouble(reportingT);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Mode")
          .setString("BeersLawAbsorptionInitial");
    result.back().write().metadataReal("Smoothing").hash("Parameters").hash("L").setString("L");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Transmittance")
          .setString("Ir" + wlCode);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Bap"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "Bac" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light absorption coefficient calculated from the inversion model");
    if (reportingAtSTP) {
        result.back().write().metadataReal("ReportT").setDouble(reportingT);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }
    result.back().write().metadataReal("Source").set(instrumentMeta);
    if (FP::defined(absorptionEfficiency)) {
        auto mp = processing;
        mp["Parameters"].hash("Efficiency").setDouble(absorptionEfficiency);
        result.back().write().metadataReal("Processing").toArray().after_back().set(mp);
    }
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Bap Model"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);


    result.emplace_back(SequenceName({}, "raw_meta", "If" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Reference detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("I reference"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "Ip" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Sample forward detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("I sample 0"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "Is1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Sample 135 degree backscatter detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("I sample 165"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "Is2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Sample 165 degree backscatter detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("I sample 165"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSSA" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0.000000");
    result.back().write().metadataReal("GroupUnits").setString("Albedo");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Inversion calculated single scattering albedo");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("SSA"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "ZIr" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("000.0000");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Inversion calculated aerosol optical depth of the filter");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");

    result.emplace_back(SequenceName({}, "raw_meta", "X" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
    result.back().write().metadataReal("Format").setString("0000.000");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Inversion calculated equivalent black carbon concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("EBC"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
    if (reportingAtSTP) {
        result.back().write().metadataReal("ReportT").setDouble(reportingT);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
    }


    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Ambient temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Ambient"));

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Measuring head temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Measure"));

    result.emplace_back(SequenceName({}, "raw_meta", "T3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("System temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("System"));

    result.emplace_back(SequenceName({}, "raw_meta", "Pd1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("GroupUnits").setString("dhPa");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Pressure drop from ambient to orifice face");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Orifice"));

    result.emplace_back(SequenceName({}, "raw_meta", "Pd2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("GroupUnits").setString("dhPa");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Vacuum pressure pump drop across orifice");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Pump"));

    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));


    result.emplace_back(SequenceName({}, "raw_meta", "ZPARAMETERS"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataArray("Description").setString("List of all system parameters");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataArray("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataArray("Realtime").hash("Hide").setBool(true);
    result.back()
          .write()
          .metadataArray("Children")
          .metadataString("Description")
          .setString("System parameter line");


    result.emplace_back(SequenceName({}, "raw_meta", "Ff"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataInteger("Format").setString("0000000000");
    result.back().write().metadataInteger("GroupUnits").setString("FilterID");
    result.back().write().metadataInteger("Description").setString("Filter ID");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataInteger("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataInteger("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSPOT"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataHash("Description").setString("Spot sampling parameters");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataHashChild("In" + wlCode)
          .metadataReal("Description")
          .setString("Spot start normalized intensity");
    result.back()
          .write()
          .metadataHashChild("In" + wlCode)
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelength);
    result.back()
          .write()
          .metadataHashChild("In" + wlCode)
          .metadataReal("Format")
          .setString("00.0000000");
    result.back()
          .write()
          .metadataHashChild("In" + wlCode + "Latest")
          .metadataReal("Description")
          .setString("Spot end normalized intensity");
    result.back()
          .write()
          .metadataHashChild("In" + wlCode + "Latest")
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelength);
    result.back()
          .write()
          .metadataHashChild("In" + wlCode + "Latest")
          .metadataReal("Format")
          .setString("00.0000000");
    result.back()
          .write()
          .metadataHashChild("Ir" + wlCode)
          .metadataReal("Description")
          .setString("Spot end transmittance");
    result.back()
          .write()
          .metadataHashChild("Ir" + wlCode)
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelength);
    result.back()
          .write()
          .metadataHashChild("Ir" + wlCode)
          .metadataReal("Format")
          .setString("0.0000000");
    result.back()
          .write()
          .metadataHashChild("Qt")
          .metadataReal("Description")
          .setString("Total spot sample volume");
    result.back().write().metadataHashChild("Qt").metadataReal("Format").setString("000.00");
    result.back().write().metadataHashChild("Qt").metadataReal("Units").setString("m³");
    if (reportingAtSTP) {
        result.back().write().metadataHashChild("Qt").metadataReal("ReportT").setDouble(reportingT);
        result.back().write().metadataHashChild("Qt").metadataReal("ReportP").setDouble(1013.25);
    }
    result.back()
          .write()
          .metadataHashChild("L")
          .metadataReal("Description")
          .setString("Total integrated spot sample length, Qt/A");
    result.back().write().metadataHashChild("L").metadataReal("Format").setString("00000.0000");
    result.back().write().metadataHashChild("L").metadataReal("Units").setString("m");
    if (reportingAtSTP) {
        result.back().write().metadataHashChild("L").metadataReal("ReportT").setDouble(reportingT);
        result.back().write().metadataHashChild("L").metadataReal("ReportP").setDouble(1013.25);
    }
    result.back()
          .write()
          .metadataHashChild("Area")
          .metadataReal("Description")
          .setString("Spot area");
    result.back().write().metadataHashChild("Area").metadataReal("Format").setString("000.000");
    result.back().write().metadataHashChild("Area").metadataReal("Units").setString("mm²");


    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Flags"));

    result.back()
          .write()
          .metadataSingleFlag("STP")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");
    result.back().write().metadataSingleFlag("STP").hash("Bits").setInt64(0x0200);
    result.back()
          .write()
          .metadataSingleFlag("STP")
          .hash("Description")
          .setString("Data reported at STP");

    result.back()
          .write()
          .metadataSingleFlag("FilterChanging")
          .hash("Description")
          .setString("Changing filter");
    result.back()
          .write()
          .metadataSingleFlag("FilterChanging")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x0000010000));
    result.back()
          .write()
          .metadataSingleFlag("FilterChanging")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("MemoryError")
          .hash("Description")
          .setString("Memory system error");
    result.back()
          .write()
          .metadataSingleFlag("MemoryError")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x0100000000));
    result.back()
          .write()
          .metadataSingleFlag("MemoryError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("MechanicalError")
          .hash("Description")
          .setString("Mechanical system error");
    result.back()
          .write()
          .metadataSingleFlag("MechanicalError")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x0200000000));
    result.back()
          .write()
          .metadataSingleFlag("MechanicalError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("PressureError")
          .hash("Description")
          .setString("Pressure measurement error");
    result.back()
          .write()
          .metadataSingleFlag("PressureError")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x0400000000));
    result.back()
          .write()
          .metadataSingleFlag("PressureError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("FlowError")
          .hash("Description")
          .setString("Flow measurement error");
    result.back()
          .write()
          .metadataSingleFlag("FlowError")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x0800000000));
    result.back()
          .write()
          .metadataSingleFlag("FlowError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("DetectorError")
          .hash("Description")
          .setString("Photodetector error");
    result.back()
          .write()
          .metadataSingleFlag("DetectorError")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x1000000000));
    result.back()
          .write()
          .metadataSingleFlag("DetectorError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("TemperatureError")
          .hash("Description")
          .setString("Temperature measurement error");
    result.back()
          .write()
          .metadataSingleFlag("TemperatureError")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x2000000000));
    result.back()
          .write()
          .metadataSingleFlag("TemperatureError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("Zero")
          .hash("Description")
          .setString("Zero in progress");
    result.back()
          .write()
          .metadataSingleFlag("FlowError")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x0000020000));
    result.back()
          .write()
          .metadataSingleFlag("Zero")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("PumpOff")
          .hash("Description")
          .setString("Pump turned off");
    result.back()
          .write()
          .metadataSingleFlag("PumpOff")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x0000080000));
    result.back()
          .write()
          .metadataSingleFlag("PumpOff")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("ManualOperation")
          .hash("Description")
          .setString("Manual operation, offline with keyboard enabled");
    result.back()
          .write()
          .metadataSingleFlag("ManualOperation")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x0000100000));
    result.back()
          .write()
          .metadataSingleFlag("ManualOperation")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("CalibrationEnabled")
          .hash("Description")
          .setString("Calibration enabled");
    result.back()
          .write()
          .metadataSingleFlag("CalibrationEnabled")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x0000200000));
    result.back()
          .write()
          .metadataSingleFlag("CalibrationEnabled")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back().write().metadataSingleFlag("MainsOn").hash("Description").setString("Mains on");
    result.back()
          .write()
          .metadataSingleFlag("MainsOn")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x0000800000));
    result.back()
          .write()
          .metadataSingleFlag("MainsOn")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("LEDWeak")
          .hash("Description")
          .setString("LED signal too weak warning");
    result.back()
          .write()
          .metadataSingleFlag("LEDWeak")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("PROMError")
          .hash("Description")
          .setString("PROM error");
    result.back()
          .write()
          .metadataSingleFlag("PROMError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back().write().metadataSingleFlag("RAMERROR").hash("Description").setString("RAM error");
    result.back()
          .write()
          .metadataSingleFlag("RAMERROR")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("NVRAMError")
          .hash("Description")
          .setString("SaveRAM error, battery backup empty");
    result.back()
          .write()
          .metadataSingleFlag("NVRAMError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("EEPROMError")
          .hash("Description")
          .setString("EEPROM read or write error");
    result.back()
          .write()
          .metadataSingleFlag("EEPROMError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("NegativePressureError")
          .hash("Description")
          .setString("Suction chamber negative pressure less than 10 hPa");
    result.back()
          .write()
          .metadataSingleFlag("NegativePressureError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("TapeGapOpenError")
          .hash("Description")
          .setString("Unable to detect opening of the filter tape gap");
    result.back()
          .write()
          .metadataSingleFlag("TapeGapOpenError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("TapeAdvanceError")
          .hash("Description")
          .setString("Filter tape advance not detected or filter tape torn");
    result.back()
          .write()
          .metadataSingleFlag("TapeAdvanceError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("ImmediateChangeError")
          .hash("Description")
          .setString("Filter change condition met immediately after a filter change");
    result.back()
          .write()
          .metadataSingleFlag("ImmediateChangeError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("OrificePressureOutOfRange")
          .hash("Description")
          .setString("Orifice pressure sensor negative or over range");
    result.back()
          .write()
          .metadataSingleFlag("OrificePressureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("PumpPressureOutOfRange")
          .hash("Description")
          .setString("Pump pressure sensor negative or over range");
    result.back()
          .write()
          .metadataSingleFlag("PumpPressureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("AmbientPressureOutOfRange")
          .hash("Description")
          .setString("Ambient pressure sensor negative or over range");
    result.back()
          .write()
          .metadataSingleFlag("AmbientPressureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("OrificePressureLow")
          .hash("Description")
          .setString("Orifice pressure less than 1 hPa");
    result.back()
          .write()
          .metadataSingleFlag("OrificePressureLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("FlowRegulationUnstable")
          .hash("Description")
          .setString("Flow deviation greater than five percent");
    result.back()
          .write()
          .metadataSingleFlag("FlowRegulationUnstable")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("FlowRegulatorFullyOpen")
          .hash("Description")
          .setString("Air flow regulator completely open");
    result.back()
          .write()
          .metadataSingleFlag("FlowRegulatorFullyOpen")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("FlowRegulatorFullyClosed")
          .hash("Description")
          .setString("Air flow regulator completely closed");
    result.back()
          .write()
          .metadataSingleFlag("FlowRegulatorFullyClosed")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("ReferenceOutOfRange")
          .hash("Description")
          .setString("Reference signal negative or over range");
    result.back()
          .write()
          .metadataSingleFlag("ReferenceOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("TransmitanceOutOfRange")
          .hash("Description")
          .setString("Zero degree transmittance signal negative or over range");
    result.back()
          .write()
          .metadataSingleFlag("TransmitanceOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("BS165OutOfRange")
          .hash("Description")
          .setString("Backscatter signal at 165 degrees negative or over range");
    result.back()
          .write()
          .metadataSingleFlag("BS165OutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("BS135OutOfRange")
          .hash("Description")
          .setString("Backscatter signal at 135 degrees negative or over range");
    result.back()
          .write()
          .metadataSingleFlag("BS135OutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("IlluminatedSignalLow")
          .hash("Description")
          .setString("Signal too low while the LED is illuminated");
    result.back()
          .write()
          .metadataSingleFlag("IlluminatedSignalLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("DarkSignalHigh")
          .hash("Description")
          .setString("Dark signal too high");
    result.back()
          .write()
          .metadataSingleFlag("DarkSignalHigh")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("AmbientTemperatureShortCircuit")
          .hash("Description")
          .setString("Ambient temperature sensor short circuit");
    result.back()
          .write()
          .metadataSingleFlag("AmbientTemperatureShortCircuit")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("AmbientTemperatureShortInterruption")
          .hash("Description")
          .setString("Ambient temperature sensor interruption");
    result.back()
          .write()
          .metadataSingleFlag("AmbientTemperatureShortInterruption")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("MeasurementTemperatureShortCircuit")
          .hash("Description")
          .setString("Measurement temperature sensor short circuit");
    result.back()
          .write()
          .metadataSingleFlag("MeasurementTemperatureShortCircuit")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("MeasurementTemperatureShortInterruption")
          .hash("Description")
          .setString("Measurement temperature sensor interruption");
    result.back()
          .write()
          .metadataSingleFlag("MeasurementTemperatureShortInterruption")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("SystemTemperatureShortCircuit")
          .hash("Description")
          .setString("System temperature sensor short circuit");
    result.back()
          .write()
          .metadataSingleFlag("SystemTemperatureShortCircuit")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    result.back()
          .write()
          .metadataSingleFlag("SystemTemperatureShortInterruption")
          .hash("Description")
          .setString("System temperature sensor interruption");
    result.back()
          .write()
          .metadataSingleFlag("SystemTemperatureShortInterruption")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_maap5012");

    return result;
}

SequenceValue::Transfer AcquireThermoMAAP5012::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    Variant::Root processing;
    processing["By"].setString("acquire_thermo_maap5012");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    processing["Address"].set(instrumentMeta["Address"]);

    result.emplace_back(SequenceName({}, "raw_meta", "In" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("00.0000000");
    result.back().write().metadataReal("GroupUnits").setString("NormalizedIntensity");
    result.back().write().metadataReal("Description").setString("Normalized transmitted intensity");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("I normalized"));


    result.emplace_back(SequenceName({}, "raw_meta", "ZF2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataInteger("Format").setString("FFFFFF");
    result.back().write().metadataInteger("Description").setString("Global status");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataInteger("Realtime").hash("Name").setString("Status");
    result.back().write().metadataInteger("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataInteger("Realtime").hash("RowOrder").setInt64(6);

    result.emplace_back(SequenceName({}, "raw_meta", "ZF3"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataInteger("Format").setString("FFFFFFFFFFFF");
    result.back().write().metadataInteger("Description").setString("Detailed error code");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataInteger("Realtime").hash("Name").setString("Error");
    result.back().write().metadataInteger("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataInteger("Realtime").hash("RowOrder").setInt64(6);

    result.emplace_back(SequenceName({}, "raw_meta", "PCT"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Pump regulation signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Pump"));


    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("FilterChange")
          .setString(QObject::tr("Changing filter..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Error")
          .setString(QObject::tr("ERROR CONDITION PRESENT"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Warning")
          .setString(QObject::tr("Warning condition present"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));

    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetTime")
          .setString(QObject::tr("STARTING COMMS: Setting time"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadErrorCounter")
          .setString(QObject::tr("STARTING COMMS: Reading error counter"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveFirstValid")
          .setString(QObject::tr("STARTING COMMS: Waiting for initial response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InteractiveStartStopReportsInitial")
          .setString(QObject::tr("STARTING COMMS: Stopping reports"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InteractiveStartStopReportsFlush")
          .setString(QObject::tr("STARTING COMMS: Flushing reports"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadAddress")
          .setString(QObject::tr("STARTING COMMS: Reading address"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetEnglish")
          .setString(QObject::tr("STARTING COMMS: Setting English mode"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadVersion")
          .setString(QObject::tr("STARTING COMMS: Reading version"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetRecordMinutes")
          .setString(QObject::tr("STARTING COMMS: Setting reporting interval (minutes)"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetRecordSeconds")
          .setString(QObject::tr("STARTING COMMS: Setting reporting interval (seconds)"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetAveraging")
          .setString(QObject::tr("STARTING COMMS: Setting averaging time"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetAdvanceTr")
          .setString(QObject::tr("STARTING COMMS: Setting spot advance transmittance"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetAdvanceHour")
          .setString(QObject::tr("STARTING COMMS: Setting spot advance maximum hours"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetAdvanceAtHour")
          .setString(QObject::tr("STARTING COMMS: Setting spot advance hour of day"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetTargetFlow")
          .setString(QObject::tr("STARTING COMMS: Setting target flow rate"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InteractiveStartSetStandardTemperature")
          .setString(QObject::tr("STARTING COMMS: Setting standard temperature"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InteractiveStartSetSTP")
          .setString(QObject::tr("STARTING COMMS: Setting STP reporting mode"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InteractiveStartSetParameterList")
          .setString(QObject::tr("STARTING COMMS: Changing to parameter list output"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadParameters")
          .setString(QObject::tr("STARTING COMMS: Reading parameter list"));

    return result;
}

SequenceMatch::Composite AcquireThermoMAAP5012::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "Ff");
    sel.append({}, {}, "ZF2");
    sel.append({}, {}, "ZF3");
    sel.append({}, {}, "ZSPOT");
    sel.append({}, {}, "ZPARAMETERS");
    sel.append({}, {}, "ZSTATE");
    return sel;
}

void AcquireThermoMAAP5012::logValue(double time,
                                     int streamID,
                                     SequenceName::Component name,
                                     Variant::Root &&value)
{
    SequenceValue dv
            (SequenceName({}, "raw", std::move(name)), std::move(value), streamTime[streamID],
             time);

    if (loggingEgress && FP::defined(streamTime[streamID])) {
        if (!realtimeEgress || !FP::defined(time)) {
            loggingMux.incoming(streamID, std::move(dv), loggingEgress);
            return;
        }
        loggingMux.incoming(streamID, dv, loggingEgress);
    }
    if (!realtimeEgress || !FP::defined(time))
        return;
    dv.setStart(time);
    dv.setEnd(time + 1.0);
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireThermoMAAP5012::realtimeValue(double time,
                                          SequenceName::Component name,
                                          Variant::Root &&value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

void AcquireThermoMAAP5012::invalidateLogValues(double frameTime)
{
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamHit[i] = 0;
        streamOutdated[i] = 0;

        if (loggingEgress != NULL && FP::defined(frameTime))
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    loggingMux.clear();
    reportedFlags.clear();
    reportFlagStatus = ReportFlag_Normal;
    autoprobePassiveValidRecords = 0;
    lastLowPrecisionFlow = FP::undefined();
    lastBCUnits = BC_unknown;

    if (FP::defined(frameTime))
        loggingLost(frameTime);
}

void AcquireThermoMAAP5012::globalAdvance(double time)
{
    if (!FP::defined(time))
        return;
    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;

        SequenceValue::Transfer meta = buildLogMeta(time);
        Util::append(buildRealtimeMeta(time), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    if (loggingEgress == NULL) {
        for (int i = 0; i < LogStream_TOTAL; i++) {
            streamTime[i] = FP::undefined();
        }
        loggingMux.clear();
        return;
    }

    if (!haveEmittedLogMeta) {
        haveEmittedLogMeta = true;
        loggingMux.incoming(LogStream_Metadata, buildLogMeta(time), loggingEgress);
    }
    loggingMux.advance(LogStream_Metadata, time, loggingEgress);
}

static double calculateL(double Qt, double area)
{
    if (!FP::defined(Qt) || !FP::defined(area) || area <= 0.0 || Qt <= 0.0)
        return FP::undefined();
    return Qt / (area * 1E-6);
}

static double calculateIn(double Iref, double Isam)
{
    if (!FP::defined(Iref) || !FP::defined(Isam) || Isam == 0.0)
        return FP::undefined();
    return Isam / Iref;
}

static double calculateIr(double In0, double In)
{
    if (!FP::defined(In0) || !FP::defined(In))
        return FP::undefined();
    if (In0 == 0.0)
        return FP::undefined();
    return In / In0;
}

static double calculateBa(double Ir0, double Qt0, double Ir1, double Qt1, double area)
{
    if (!FP::defined(Ir0) ||
            !FP::defined(Qt0) ||
            !FP::defined(Ir1) ||
            !FP::defined(Qt1) ||
            !FP::defined(area))
        return FP::undefined();
    if (Ir0 <= 0.0 || Ir1 <= 0.0)
        return FP::undefined();
    double dQt = Qt1 - Qt0;
    if (dQt <= 0.0)
        return FP::undefined();
    return log(Ir0 / Ir1) * (area / dQt);
}

static double calculateVolume(double startTime, double endTime, double Q)
{
    if (!FP::defined(Q) || !FP::defined(endTime))
        return FP::undefined();
    if (!FP::defined(startTime))
        startTime = endTime - 1.0;
    if (Q < 0.0)
        return FP::undefined();
    if (endTime < startTime)
        return FP::undefined();
    return (Q * (endTime - startTime)) / 60000.0;
}

static double sampleVolumeToFlow(double startTime, double endTime, double dQt)
{
    if (!FP::defined(startTime) || !FP::defined(endTime) || !FP::defined(dQt))
        return FP::undefined();
    double dT = endTime - startTime;
    if (dT <= 0.0)
        return FP::undefined();
    if (dQt < 0.0)
        return FP::undefined();
    return dQt * 60000.0 / dT;
}

static double recalibrateFlow(double Ba, double Qraw, double Qcal)
{
    if (!FP::defined(Ba) || !FP::defined(Qraw) || !FP::defined(Qcal))
        return FP::undefined();
    if (Qraw <= 0.0 || Qcal <= 0.0)
        return FP::undefined();
    return Ba * (Qraw / Qcal);
}

static double recalibrateArea(double Ba, double areaRaw, double areaTrue)
{
    if (!FP::defined(Ba) || !FP::defined(areaRaw) || !FP::defined(areaTrue))
        return FP::undefined();
    if (areaRaw <= 0.0 || areaTrue <= 0.0)
        return FP::undefined();
    return Ba * (areaTrue / areaRaw);
}

static double calculateBaEBC(double ebc, double e)
{
    if (!FP::defined(ebc) || !FP::defined(e))
        return FP::undefined();
    return (ebc * e);
}

static double calculateEBC(double Ba, double e)
{
    if (!FP::defined(Ba))
        return FP::undefined();
    Q_ASSERT(FP::defined(e));
    if (e <= 0.0)
        return FP::undefined();
    return (Ba / e);
}

void AcquireThermoMAAP5012::streamAdvance(int streamID, double time)
{
    if (!FP::defined(time))
        return;

    quint32 bits = 1 << streamID;
    if (loggingEgress != NULL) {
        for (int i = LogStream_ReadBegin; i < LogStream_TOTAL; i++) {
            if (i == streamID)
                continue;
            if (!(streamHit[i] & bits)) {
                streamHit[i] |= bits;
                continue;
            }
            if (!(streamOutdated[i] & bits)) {
                streamOutdated[i] |= bits;
                continue;
            }
            streamHit[i] = 0xFFFFFFFF;
            streamOutdated[i] = 0xFFFFFFFF;
            streamTime[i] = FP::undefined();
            loggingMux.advance(i, time, loggingEgress);
        }

        streamHit[streamID] = bits;
        streamOutdated[streamID] = 0;
        streamTime[streamID] = time;
        loggingMux.advance(streamID, time, loggingEgress);
    } else {
        streamHit[streamID] = 0;
        streamTime[streamID] = FP::undefined();
    }

    if (!(streamHit[LogStream_Common] & bits)) {
        streamHit[LogStream_Common] |= bits;
        return;
    }

    const auto &wlCode = Wavelength::code(config.front().wavelength);
    if (loggingEgress == NULL) {
        priorIr = FP::undefined();
    }

    Variant::Flags flags = reportedFlags;
    if (reportingAtSTP) {
        flags.insert("STP");
    }
    logValue(time, LogStream_Common, "F1", Variant::Root(flags));

    double effectiveFlow = lastKnownFlow;
    if (FP::defined(flowLowPrecision))
        effectiveFlow = flowLowPrecision;
    if (FP::defined(flowHighPrecision))
        effectiveFlow = flowHighPrecision;
    if (!FP::defined(effectiveFlow)) {
        if (config.first().flowCalibration.size() == 1)
            effectiveFlow = config.first().flowCalibration.apply(0.0);
    }
    lastKnownRawFlow = effectiveFlow;
    Variant::Root Q(config.first().flowCalibration.apply(effectiveFlow));
    remap("Q", Q);
    lastKnownFlow = Q.read().toDouble();
    if (FP::defined(effectiveFlow) || FP::defined(Q.read().toReal())) {
        logValue(time, LogStream_Common, "Q", std::move(Q));
    }

    double effectivePressure = ambientPressure;
    if (FP::defined(orificePressureDrop))
        effectivePressure -= orificePressureDrop;
    Variant::Root P(effectivePressure);
    remap("P", P);
    if (FP::defined(effectivePressure) || FP::defined(P.read().toReal())) {
        logValue(time, LogStream_Common, "P", std::move(P));
    }

    double addVolume = FP::undefined();

    if (!haveAcceptedNormalization) {
        haveAcceptedNormalization = true;

        if (!FP::defined(filterBegin.startTime) && FP::defined(filterBeginPending.startTime)) {
            qCDebug(log) << "Using reported normalization at" << Logging::time(time);
            filterBegin = filterBeginPending;
            filterBegin.startTime = time;

            restoreInstrumentVolume = FP::undefined();
            filterSerialNumber.write().set(instrumentMeta["SerialNumber"]);
            filterFirmwareVersion.write().set(instrumentMeta["FirmwareVersion"]);
            filterAddress.write().set(instrumentMeta["Address"]);
            priorIr = FP::undefined();
            accumulateVolume = 0.0;
            forceFilterBreak = true;

            filterBeginPending.startTime = FP::undefined();

            Variant::Write info = Variant::Write::empty();
            describeState(info);
            info.hash("IssuedCommand").set(issuedCommand.stateDescription());
            info.hash("Filter").set(buildNormalizationValue());
            info.hash("InstrumentIntensities").setBool(true);
            event(time, QObject::tr("Instrument reported normalization selected."), false, info);

            persistentValuesUpdated();
        } else if (FP::defined(filterBegin.startTime) && isSameNormalization(time)) {
            Variant::Write info = Variant::Write::empty();
            info.hash("Existing").set(buildNormalizationValue());

            filterSerialNumber.write().set(instrumentMeta["SerialNumber"]);
            filterFirmwareVersion.write().set(instrumentMeta["FirmwareVersion"]);
            filterAddress.write().set(instrumentMeta["Address"]);
            priorIr = FP::undefined();
            accumulateVolume = FP::undefined();
            forceFilterBreak = true;

            if (FP::defined(accumulateVolume) &&
                    FP::defined(time) &&
                    FP::defined(restoreTime) &&
                    restoreTime < time) {
                addVolume = calculateVolume(restoreTime, time, lastKnownFlow);
            }

            describeState(info);
            info.hash("IssuedCommand").set(issuedCommand.stateDescription());
            info.hash("Filter").set(buildNormalizationValue());
            event(time, QObject::tr("Saved normalization accepted."), false, info);

            persistentValuesUpdated();
        } else {
            qCDebug(log) << "No available valid normalization at" << Logging::time(time);

            Variant::Write info = Variant::Write::empty();
            describeState(info);
            info.hash("IssuedCommand").set(issuedCommand.stateDescription());
            info.hash("Filter").set(buildNormalizationValue());
            event(time, QObject::tr("No available normalization."), false, info);

            forceFilterBreak = true;

            filterBegin = IntensityData();
            persistentValuesUpdated();
        }
    } else {
        addVolume = calculateVolume(accumulateTime, time, lastKnownFlow);
    }

    accumulateTime = time;
    double latestVolume;
    if (!FP::defined(accumulateVolume)) {
        latestVolume = addVolume;
    } else if (FP::defined(addVolume)) {
        latestVolume = accumulateVolume + addVolume;
    } else {
        latestVolume = FP::undefined();
    }

    bool filterChanging = reportedFlags.count("FilterChanging") || reportedFlags.count("Zero");
    if (config.first().enableAutodetect) {
        if (!filterChanging) {
            if (filterChangeIp->ready() && filterChangeIp->spike()) {
                filterChanging = true;
                filterChangeWasAutodetected = true;
            }
        } else {
            if (filterChangeWasAutodetected && filterChangeIp->ready() && !filterChangeIp->stable())
                filterChanging = true;
        }
    } else {
        filterChangeWasAutodetected = false;
    }
    if (priorFilterChanging && !filterChanging) {
        qCDebug(log) << "Filter change end detected at" << Logging::time(time);

        emitNormalizationEnd(time);

        bool usedReported;
        if (!FP::defined(filterBeginPending.startTime)) {
            filterBegin = filterLatest;
            usedReported = false;
        } else {
            filterBegin = filterBeginPending;
            usedReported = true;
        }
        filterBegin.startTime = time;
        latestVolume = 0.0;
        restoreInstrumentVolume = FP::undefined();
        filterSerialNumber.write().set(instrumentMeta["SerialNumber"]);
        filterFirmwareVersion.write().set(instrumentMeta["FirmwareVersion"]);
        filterAddress.write().set(instrumentMeta["Address"]);
        priorIr = 1.0;
        accumulateVolume = 0.0;
        forceFilterBreak = true;

        forceRealtimeStateEmit = true;

        filterBeginPending.startTime = FP::undefined();

        Variant::Write info = Variant::Write::empty();
        describeState(info);
        info.hash("IssuedCommand").set(issuedCommand.stateDescription());
        info.hash("Filter").set(buildNormalizationValue());
        info.hash("Filter").hash("Change").hash("Ip").set(filterChangeIp->describeState());
        info.hash("InstrumentIntensities").setBool(usedReported);
        if (!filterChangeWasAutodetected) {
            event(time, QObject::tr("Filter normalization updated due to instrument indicator."),
                  false, info);
        } else {
            event(time, QObject::tr("Filter normalization updated due to detected stability."),
                  false, info);
        }

        if (state != NULL)
            state->requestStateSave();
        persistentValuesUpdated();

        filterChangeWasAutodetected = false;
    } else if (!priorFilterChanging && filterChanging) {
        qCDebug(log) << "Filter change start detected at" << Logging::time(time);

        priorIr = FP::undefined();
        accumulateVolume = FP::undefined();
        forceRealtimeStateEmit = true;
        filterChangeIp->reset();
    }
    priorFilterChanging = filterChanging;

    Variant::Root Qt;
    if (filterChanging) {
        Qt.write().setDouble(FP::undefined());
    } else {
        Qt.write().setDouble(latestVolume);
    }
    remap("Qt", Qt);
    latestVolume = Qt.read().toDouble();

    Variant::Root L(calculateL(latestVolume, config.first().area));
    remap("L", L);


    Variant::Root Ir;
    if (filterChanging || !haveAcceptedNormalization) {
        Ir.write().setDouble(FP::undefined());
    } else {
        Ir.write().setDouble(calculateIr(filterBegin.In, filterLatest.In));
    }
    remap("Ir" + wlCode, Ir);

    double latestIr = Ir.read().toDouble();
    Variant::Root Ba;
    if (filterChanging) {
        Ba.write().setDouble(FP::undefined());
    } else {
        Ba.write()
          .setDouble(calculateBa(priorIr, accumulateVolume, latestIr, latestVolume,
                                 config.first().area));
    }
    remap("Ba" + wlCode, Ba);
    lastKnownAbsorption = Ba.read().toDouble();

    /* Don't emit these when we don't have enough information to output
     * them ever (passive formats other than PF12) */
    if (!FP::defined(Ir.read().toReal()) &&
            !FP::defined(Ba.read().toReal()) &&
            !FP::defined(filterBegin.In) &&
            !FP::defined(filterLatest.In)) {
        requireFilterBreak = true;
        forceFilterBreak = true;
    }

    if (forceFilterBreak && requireFilterBreak) {
        realtimeValue(time, "L", Variant::Root(FP::undefined()));
        realtimeValue(time, "Qt", Variant::Root(FP::undefined()));
        realtimeValue(time, "Ir" + wlCode, Variant::Root(FP::undefined()));
        realtimeValue(time, "Ba" + wlCode, Variant::Root(FP::undefined()));
        requireFilterBreak = false;
    } else {
        logValue(time, LogStream_Common, "L", std::move(L));
        logValue(time, LogStream_Common, "Qt", std::move(Qt));
        logValue(time, LogStream_Common, "Ir" + wlCode, std::move(Ir));
        logValue(time, LogStream_Common, "Ba" + wlCode, std::move(Ba));
        requireFilterBreak = true;
    }
    forceFilterBreak = false;

    orificePressureDrop = FP::undefined();
    ambientPressure = FP::undefined();
    flowLowPrecision = FP::undefined();
    flowHighPrecision = FP::undefined();
    accumulateVolume = latestVolume;

    streamHit[LogStream_Common] = bits;
    streamTime[LogStream_Common] = time;
    accumulateVolume = latestVolume;
    priorIr = latestIr;
    loggingMux.advance(LogStream_Common, time, loggingEgress);

    if (forceRealtimeStateEmit && realtimeEgress != NULL) {
        forceRealtimeStateEmit = false;
        if (filterChanging) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("FilterChange"), time,
                                  FP::undefined()));
        } else {
            switch (reportFlagStatus) {
            case ReportFlag_Error:
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Error"), time,
                                      FP::undefined()));
                break;
            case ReportFlag_Warning:
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Warning"), time,
                                      FP::undefined()));
                break;
            case ReportFlag_Normal:
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), time,
                                      FP::undefined()));
                break;
            }
        }

    }
}


Variant::Root AcquireThermoMAAP5012::buildNormalizationValue() const
{
    Variant::Root result;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    result.write().hash("In" + wlCode).setDouble(filterBegin.In);
    result.write().hash("In" + wlCode + "Latest").setDouble(filterLatest.In);
    result.write().hash("Ir" + wlCode).setDouble(priorIr);
    result.write().hash("Qt").setDouble(accumulateVolume);
    result.write().hash("SerialNumber").set(filterSerialNumber);
    result.write().hash("FirmwareVersion").set(filterFirmwareVersion);
    result.write().hash("Address").set(filterAddress);
    if (FP::defined(filterBegin.startTime))
        result.write().hash("Ff").setInt64((qint64) floor(filterBegin.startTime));
    else
        result.write().hash("Ff").setInt64(INTEGER::undefined());
    if (!config.isEmpty()) {
        result.write().hash("L").setDouble(calculateL(accumulateVolume, config.first().area));
        result.write().hash("Area").setDouble(config.first().area);
    }

    return result;
}

bool AcquireThermoMAAP5012::reportedNormalizationMatchesEffective() const
{
    if (!haveAcceptedNormalization)
        return true;
    if (priorFilterChanging)
        return true;
    if (FP::defined(filterBegin.startTime)) {
        if (filterBeginPending == filterBegin)
            return true;
    }
    if (reportedFlags.count("FilterChanging") || reportedFlags.count("Zero"))
        return true;
    return false;
}

bool AcquireThermoMAAP5012::isSameNormalization(double time) const
{
    if (FP::defined(filterBeginPending.startTime)) {
        if (filterBeginPending != filterBegin)
            return false;
    }

    if (FP::defined(time) && FP::defined(restoreTime)) {
        double checkTime = acceptNormalizationTime->applyConst(restoreTime, restoreTime, true);
        if (FP::defined(checkTime) && checkTime <= time)
            return false;
    }

    if (FP::defined(reportedInstrumentVolume) && FP::defined(restoreInstrumentVolume)) {
        if (reportedInstrumentVolume < restoreInstrumentVolume)
            return false;
    }

    if (!instrumentIDMatchesNormalization())
        return false;

    return true;
}

bool AcquireThermoMAAP5012::instrumentIDMatchesNormalization() const
{
    if (!config.first().checkNormalizationInstrumentID)
        return true;
    if (filterSerialNumber.read().exists() &&
            instrumentMeta["SerialNumber"].exists() &&
            instrumentMeta["SerialNumber"] != filterSerialNumber.read())
        return false;
    if (filterFirmwareVersion.read().exists() &&
            instrumentMeta["FirmwareVersion"].exists() &&
            instrumentMeta["FirmwareVersion"] != filterFirmwareVersion.read())
        return false;
    if (filterAddress.read().exists() &&
            instrumentMeta["Address"].exists() &&
            instrumentMeta["Address"] != filterAddress.read())
        return false;
    return true;
}

void AcquireThermoMAAP5012::instrumentIDUpdated()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    sourceMetadataUpdated();

    if (!instrumentIDMatchesNormalization()) {
        qCDebug(log) << "Instrument identifier (" << instrumentMeta["SerialNumber"] << ":"
                     << instrumentMeta["FirmwareVersion"] << ":" << instrumentMeta["Address"]
                     << ") does not match the normalization (" << filterSerialNumber << ":"
                     << filterFirmwareVersion << ":" << filterAddress
                     << ") normalization invalidated";

        filterBegin = IntensityData();
        haveAcceptedNormalization = false;
    }
}

bool AcquireThermoMAAP5012::inInteractiveMode() const
{
    switch (responseState) {
    case RESP_INTERACTIVE_RUN_WAITING:
    case RESP_INTERACTIVE_RUN_QUERYING:
    case RESP_INTERACTIVE_RUN_QUERYING_PARTIAL:
    case RESP_INTERACTIVE_RUN_QUERYING_COMPLETE:
        return true;

    default:
        return false;
    }
    return false;
}


void AcquireThermoMAAP5012::configurationAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

void AcquireThermoMAAP5012::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
}

void AcquireThermoMAAP5012::describeState(Variant::Write &info) const
{
    info.hash("Flags").setFlags(reportedFlags);

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        info.hash("ResponseState").setString("AutoprobePassiveInitialize");
        break;
    case RESP_AUTOPROBE_PASSIVE_WAIT:
        info.hash("ResponseState").setString("AutoprobePassiveWait");
        break;
    case RESP_PASSIVE_WAIT:
        info.hash("ResponseState").setString("PassiveWait");
        break;
    case RESP_PASSIVE_RUN:
        info.hash("ResponseState").setString("PassiveRun");
        break;
    case RESP_INTERACTIVE_INITIALIZE:
        info.hash("ResponseState").setString("InteractiveInitialize");
        break;
    case RESP_INTERACTIVE_RESTART_WAIT:
        info.hash("ResponseState").setString("InteractiveRestartWait");
        break;

    case RESP_INTERACTIVE_RUN_QUERYING:
        info.hash("ResponseState").setString("InteractiveRunQuerying");
        break;
    case RESP_INTERACTIVE_RUN_QUERYING_PARTIAL:
        info.hash("ResponseState").setString("InteractiveRunQueryingPartial");
        break;
    case RESP_INTERACTIVE_RUN_QUERYING_COMPLETE:
        info.hash("ResponseState").setString("InteractiveRunQueryingComplete");
        break;
    case RESP_INTERACTIVE_RUN_WAITING:
        info.hash("ResponseState").setString("InteractiveRunWait");
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS_INITIAL:
        info.hash("ResponseState").setString("InteractiveStartStopReportsInitial");
        break;
    case RESP_INTERACTIVE_START_STOPREPORTS_FLUSH:
        info.hash("ResponseState").setString("InteractiveStartStopReportsFlush");
        break;
    case RESP_INTERACTIVE_START_SETENGLISH:
        info.hash("ResponseState").setString("InteractiveStartSetEnglish");
        break;
    case RESP_INTERACTIVE_START_READVERSION_BEGIN:
        info.hash("ResponseState").setString("InteractiveStartReadVersionBegin");
        break;
    case RESP_INTERACTIVE_START_READVERSION_END:
        info.hash("ResponseState").setString("InteractiveStartReadVersionEnd");
        break;
    case RESP_INTERACTIVE_START_READADDRESS:
        info.hash("ResponseState").setString("InteractiveStartReadAddress");
        break;
    case RESP_INTERACTIVE_START_SETTIME:
        info.hash("ResponseState").setString("InteractiveStartSetTime");
        break;
    case RESP_INTERACTIVE_START_SETRECORDMINUTES:
        info.hash("ResponseState").setString("InteractiveStartSetRecordMinutes");
        break;
    case RESP_INTERACTIVE_START_SETRECORDSECONDS:
        info.hash("ResponseState").setString("InteractiveStartSetRecordSeconds");
        break;
    case RESP_INTERACTIVE_START_SETAVERAGING:
        info.hash("ResponseState").setString("InteractiveStartSetAveraging");
        break;
    case RESP_INTERACTIVE_START_SETADVANCETR:
        info.hash("ResponseState").setString("InteractiveStartSetAdvanceTr");
        break;
    case RESP_INTERACTIVE_START_SETADVANCEHOUR:
        info.hash("ResponseState").setString("InteractiveStartSetAdvanceHour");
        break;
    case RESP_INTERACTIVE_START_SETADVANCEATHOUR:
        info.hash("ResponseState").setString("InteractiveStartSetAdvanceAtHour");
        break;
    case RESP_INTERACTIVE_START_SETTARGETFLOW:
        info.hash("ResponseState").setString("InteractiveStartSetTargetFlow");
        break;
    case RESP_INTERACTIVE_START_SETSTANDARDTEMP:
        info.hash("ResponseState").setString("InteractiveStartSetStandardTemperature");
        break;
    case RESP_INTERACTIVE_START_SETSTP:
        info.hash("ResponseState").setString("InteractiveStartSetSTP");
        break;
    case RESP_INTERACTIVE_START_SETPARAMETERLIST:
        info.hash("ResponseState").setString("InteractiveStartSetParameterList");
        break;
    case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN:
        info.hash("ResponseState").setString("InteractiveStartReadParametersBegin");
        break;
    case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN_RETRY:
        info.hash("ResponseState").setString("InteractiveStartReadParametersBeginRetry");
        break;
    case RESP_INTERACTIVE_START_READPARAMETERS_END:
        info.hash("ResponseState").setString("InteractiveStartReadParametersEnd");
        break;
    case RESP_INTERACTIVE_START_READERRORCOUNTER:
        info.hash("ResponseState").setString("InteractiveStartReadErrorCounter");
        break;
    case RESP_INTERACTIVE_START_FIRSTVALID:
        info.hash("ResponseState").setString("InteractiveStartFirstValid");
        break;
    }
}

void AcquireThermoMAAP5012::writeCommand(const Util::ByteView &command)
{
    if (controlStream == NULL)
        return;

    Q_ASSERT(!config.isEmpty());

    Util::ByteArray send;
    if (config.first().address >= 0) {
        send += QByteArray::number(config.first().address);
        send.push_back(':');
    }
    send += command;
    send += "\r\n";

    controlStream->writeControl(std::move(send));
}

void AcquireThermoMAAP5012::issueCommand(const Command &command, double frameTime)
{
    commandBacklog.clear();
    issuedCommand = command;

    if (FP::defined(frameTime)) {
        discardData(FP::undefined());
        timeoutAt(frameTime + 2.0);
    }

    writeCommand(command.data());
}

void AcquireThermoMAAP5012::interactiveStartBegin(double time)
{
    if (realtimeEgress != NULL) {
        realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                "InteractiveStartStopReportsInitial"), time, FP::undefined()));
    }

    writeCommand("D 0");
    responseState = RESP_INTERACTIVE_START_STOPREPORTS_INITIAL;

    if (FP::defined(time)) {
        discardData(time + 0.5);
        timeoutAt(time + 30.0);
    }

    invalidateLogValues(time);
}

void AcquireThermoMAAP5012::invalidStartResponse(double time)
{
    responseState = RESP_INTERACTIVE_RESTART_WAIT;
    if (controlStream != NULL)
        controlStream->resetControl();
    timeoutAt(time + 30.0);
    discardData(time + 10.0);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
    autoprobeStatusUpdated();
    if (realtimeEgress != NULL) {
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"), time,
                              FP::undefined()));
    }
}

void AcquireThermoMAAP5012::invalidRunResponse(const Util::ByteView &frame,
                                               double frameTime,
                                               int code)
{
    Variant::Write info = Variant::Write::empty();

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
        break;
    default:
        if (issuedCommand.valid()) {
            qCDebug(log) << "Response at" << Logging::time(frameTime) << "in state" << responseState
                         << frame << "rejected in response to" << issuedCommand.data()
                         << "with code" << code;
        } else {
            qCDebug(log) << "Response at" << Logging::time(frameTime) << "in state" << responseState
                         << frame << "rejected with code" << code;
        }
        break;
    }

    info.hash("Response").setString(frame.toString());
    info.hash("Code").setInt64(code);
    describeState(info);
    info.hash("IssuedCommand").set(issuedCommand.stateDescription());

    invalidateLogValues(frameTime);

    switch (responseState) {
    case RESP_PASSIVE_RUN:
        timeoutAt(FP::undefined());
        responseState = RESP_PASSIVE_WAIT;
        autoprobePassiveValidRecords = 0;

        event(frameTime, QObject::tr(
                "Invalid command response received (code %1).  Communications dropped.").arg(code),
              true, info);
        break;

    case RESP_INTERACTIVE_RUN_QUERYING:
    case RESP_INTERACTIVE_RUN_QUERYING_PARTIAL:
    case RESP_INTERACTIVE_RUN_QUERYING_COMPLETE:
    case RESP_INTERACTIVE_RUN_WAITING:
        event(frameTime, QObject::tr(
                "Invalid command response received (code %1).  Communications dropped.").arg(code),
              true, info);

        autoprobePassiveValidRecords = 0;
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        timeoutAt(frameTime + 2.0);
        discardData(frameTime + 0.5);
        generalStatusUpdated();
        break;

    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS_INITIAL:
    case RESP_INTERACTIVE_START_STOPREPORTS_FLUSH:
    case RESP_INTERACTIVE_START_SETENGLISH:
    case RESP_INTERACTIVE_START_READVERSION_BEGIN:
    case RESP_INTERACTIVE_START_READVERSION_END:
    case RESP_INTERACTIVE_START_READADDRESS:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETRECORDMINUTES:
    case RESP_INTERACTIVE_START_SETRECORDSECONDS:
    case RESP_INTERACTIVE_START_SETAVERAGING:
    case RESP_INTERACTIVE_START_SETADVANCETR:
    case RESP_INTERACTIVE_START_SETADVANCEHOUR:
    case RESP_INTERACTIVE_START_SETADVANCEATHOUR:
    case RESP_INTERACTIVE_START_SETTARGETFLOW:
    case RESP_INTERACTIVE_START_SETSTANDARDTEMP:
    case RESP_INTERACTIVE_START_SETSTP:
    case RESP_INTERACTIVE_START_SETPARAMETERLIST:
    case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN:
    case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN_RETRY:
    case RESP_INTERACTIVE_START_READPARAMETERS_END:
    case RESP_INTERACTIVE_START_READERRORCOUNTER:
    case RESP_INTERACTIVE_START_FIRSTVALID:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(frameTime + 30.0);
        discardData(frameTime + 10.0);
        autoprobePassiveValidRecords = 0;

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                  frameTime, FP::undefined()));
        }
        break;
    }

    if (!commandBacklog.isEmpty()) {
        issuedCommand = commandBacklog.takeFirst();
    } else {
        issuedCommand.invalidate();
    }
}

static bool isValidDigit(char c)
{
    return c >= '0' && c <= '9';
}

static bool isAllDigits(const Util::ByteView &check)
{
    for (auto c : check) {
        if (!isValidDigit(c))
            return false;
    }
    return true;
}

void AcquireThermoMAAP5012::processEBC(Variant::Write &X, const Util::ByteView &field)
{
    Q_ASSERT(!config.isEmpty());

    double ebc = X.toDouble();
    if (!config.first().flowCalibration.isIdentity()) {
        ebc = recalibrateFlow(ebc, lastKnownRawFlow, lastKnownFlow);
    }
    /* I don't see any way to change this, so assume it's fixed */
    ebc = recalibrateArea(ebc, 200.0, config.first().area);
    if (!FP::defined(ebc)) {
        X.setDouble(ebc);
        return;
    }

    BCUnits bcUnits = config.first().bcUnits;
    if (bcUnits == BC_unknown) {
        do {
            /* If it has decimal places, it's in ug */
            if (field.indexOf('.') > 0) {
                bcUnits = BC_ugm3;
                break;
            }

            /* If it's sufficiently large, then assume it's in ng */
            if (ebc > 1000.0) {
                bcUnits = BC_ngm3;
                break;
            }

            /* An integer that could be either, so try looking at the
             * calculated absorption and pick the closer match */
            double ugEBC = calculateEBC(lastKnownAbsorption, absorptionEfficiency);
            if (!FP::defined(ugEBC) || fabs(ugEBC) < 10.0 || fabs(ebc) < 10.0) {
                /* Can't decide, so assume an integer is in ng, unless
                 * we have prior knowledge */
                if (lastBCUnits != BC_ugm3)
                    bcUnits = BC_ngm3;
                break;
            }

            double ngEBC = ugEBC * 1E3;

            if (fabs(ebc - ugEBC) < fabs(ebc - ngEBC)) {
                bcUnits = BC_ugm3;
            } else {
                bcUnits = BC_ngm3;
            }
        } while (false);
    }
    if (bcUnits == BC_unknown)
        bcUnits = lastBCUnits;

    lastBCUnits = bcUnits;

    if (bcUnits == BC_ngm3) {
        ebc = ebc / 1E3;
    }

    X.setDouble(ebc);
}

int AcquireThermoMAAP5012::processPF1(std::deque<Util::ByteView> &fields, double frameTime)
{
    bool ok = false;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    if (fields.empty()) return 1;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.size() < 6) return 2;
    Variant::Root ZF2((qint64) field.parse_u64(&ok, 16));
    if (!ok) return 3;
    remap("ZF2", ZF2);

    if (fields.empty()) return 4;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root X(field.parse_real(&ok));
    if (!ok) return 5;
    if (!FP::defined(X.read().toReal())) return 6;
    remap("ZX" + wlCode, X);
    processEBC(X, field);
    remap("X" + wlCode, X);

    Variant::Root Ba(calculateBaEBC(X.read().toDouble(), absorptionEfficiency));
    remap("Bac" + wlCode, Ba);

    processGlobalStatus(ZF2);

    globalAdvance(frameTime);
    logValue(frameTime, LogStream_BC, "X" + wlCode, std::move(X));
    logValue(frameTime, LogStream_BC, "Bac" + wlCode, std::move(Ba));
    realtimeValue(frameTime, "ZF2", std::move(ZF2));
    streamAdvance(LogStream_BC, frameTime);
    return 0;
}

int AcquireThermoMAAP5012::processPF2(std::deque<Util::ByteView> &fields, double frameTime)
{
    bool ok = false;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    if (fields.empty()) return 1;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.size() < 6) return 2;
    Variant::Root ZF2((qint64) field.parse_u64(&ok, 16));
    if (!ok) return 3;
    remap("ZF2", ZF2);

    if (fields.empty()) return 4;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root X(field.parse_real(&ok));
    if (!ok) return 5;
    if (!FP::defined(X.read().toReal())) return 6;
    remap("ZX" + wlCode, X);
    processEBC(X, field);
    remap("X" + wlCode, X);

    Variant::Root Ba(calculateBaEBC(X.read().toDouble(), absorptionEfficiency));
    remap("Bac" + wlCode, Ba);

    /* MBC, ignored */
    if (fields.empty()) return 7;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 8;

    processGlobalStatus(ZF2);

    globalAdvance(frameTime);
    logValue(frameTime, LogStream_BC, "X" + wlCode, std::move(X));
    logValue(frameTime, LogStream_BC, "Bac" + wlCode, std::move(Ba));
    realtimeValue(frameTime, "ZF2", std::move(ZF2));
    streamAdvance(LogStream_BC, frameTime);
    return 0;
}

int AcquireThermoMAAP5012::processPF3(std::deque<Util::ByteView> &fields, double frameTime)
{
    bool ok = false;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    if (fields.empty()) return 1;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.size() < 6) return 2;
    Variant::Root ZF2((qint64) field.parse_u64(&ok, 16));
    if (!ok) return 3;
    remap("ZF2", ZF2);

    if (fields.empty()) return 4;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root X(field.parse_real(&ok));
    if (!ok) return 5;
    if (!FP::defined(X.read().toReal())) return 6;
    remap("ZX" + wlCode, X);
    processEBC(X, field);
    remap("X" + wlCode, X);

    Variant::Root Ba(calculateBaEBC(X.read().toDouble(), absorptionEfficiency));
    remap("Bac" + wlCode, Ba);

    /* MBC, ignored */
    if (fields.empty()) return 7;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 8;

    if (fields.empty()) return 9;
    field = fields.front().string_trimmed();
    fields.pop_front();
    {
        double v = field.parse_real(&ok);
        if (!ok) return 10;
        if (!FP::defined(v)) return 11;
        flowHighPrecision = v / 60.0;
    }

    processGlobalStatus(ZF2);

    globalAdvance(frameTime);
    logValue(frameTime, LogStream_BC, "X" + wlCode, std::move(X));
    logValue(frameTime, LogStream_BC, "Bac" + wlCode, std::move(Ba));
    realtimeValue(frameTime, "ZF2", std::move(ZF2));
    streamAdvance(LogStream_BC, frameTime);
    return 0;
}

int AcquireThermoMAAP5012::processPF5(std::deque<Util::ByteView> &fields, double frameTime)
{
    bool ok = false;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    if (fields.empty()) return 1;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.size() < 6) return 2;
    Variant::Root ZF2((qint64) field.parse_u64(&ok, 16));
    if (!ok) return 3;
    remap("ZF2", ZF2);

    if (fields.empty()) return 4;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root X(field.parse_real(&ok));
    if (!ok) return 5;
    if (!FP::defined(X.read().toReal())) return 6;
    remap("ZX" + wlCode, X);
    processEBC(X, field);
    remap("X" + wlCode, X);

    Variant::Root Ba(calculateBaEBC(X.read().toDouble(), absorptionEfficiency));
    remap("Bac" + wlCode, Ba);

    /* MBC, ignored */
    if (fields.empty()) return 7;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 8;

    if (fields.empty()) return 9;
    field = fields.front().string_trimmed();
    fields.pop_front();
    {
        double v = field.parse_real(&ok);
        if (!ok) return 10;
        if (!FP::defined(v)) return 11;
        flowHighPrecision = v / 60.0;
    }

    /* Last EBC, ignored */
    if (fields.empty()) return 12;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 13;

    /* 1-h average EBC, ignored */
    if (fields.empty()) return 14;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 15;

    /* 3-h average EBC, ignored */
    if (fields.empty()) return 16;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 17;

    /* 24-h average EBC, ignored */
    if (fields.empty()) return 18;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 19;

    processGlobalStatus(ZF2);

    globalAdvance(frameTime);
    logValue(frameTime, LogStream_BC, "X" + wlCode, std::move(X));
    logValue(frameTime, LogStream_BC, "Bac" + wlCode, std::move(Ba));
    realtimeValue(frameTime, "ZF2", std::move(ZF2));
    streamAdvance(LogStream_BC, frameTime);
    return 0;
}

int AcquireThermoMAAP5012::processPF99(std::deque<Util::ByteView> &fields, double frameTime)
{
    bool ok = false;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    /* Are the units on this correct? */
    if (fields.empty()) return 1;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.size() > 4) return 2;
    if (field.size() < 2) return 3;
    if (field.indexOf('.') != field.npos) return 4;
    double v = field.parse_real(&ok);
    if (!ok) return 5;
    if (!FP::defined(v)) return 6;
    flowHighPrecision = v / 60.0;

    if (fields.empty()) return 7;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root If(field.parse_real(&ok));
    if (!ok) return 8;
    if (!FP::defined(If.read().toReal())) return 9;
    remap("If" + wlCode, If);

    if (fields.empty()) return 10;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Ip(field.parse_real(&ok));
    if (!ok) return 11;
    if (!FP::defined(Ip.read().toReal())) return 12;
    remap("Ip" + wlCode, Ip);

    if (fields.empty()) return 13;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Is1(field.parse_real(&ok));
    if (!ok) return 14;
    if (!FP::defined(Is1.read().toReal())) return 15;
    remap("Is1", Is1);

    if (fields.empty()) return 16;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Is2(field.parse_real(&ok));
    if (!ok) return 17;
    if (!FP::defined(Is2.read().toReal())) return 18;
    remap("Is2", Is2);

    if (fields.empty()) return 19;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Ifz(field.parse_real(&ok));
    if (!ok) return 20;
    if (!FP::defined(Ifz.read().toReal())) return 21;
    remap("Ifz" + wlCode, Ifz);

    if (fields.empty()) return 22;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Ipz(field.parse_real(&ok));
    if (!ok) return 23;
    if (!FP::defined(Ipz.read().toReal())) return 24;
    remap("Ipz" + wlCode, Ipz);

    if (fields.empty()) return 25;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Isz1(field.parse_real(&ok));
    if (!ok) return 26;
    if (!FP::defined(Isz1.read().toReal())) return 27;
    remap("Isz1", Isz1);

    if (fields.empty()) return 28;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Isz2(field.parse_real(&ok));
    if (!ok) return 29;
    if (!FP::defined(Isz2.read().toReal())) return 30;
    remap("Isz2", Isz2);

    if (fields.empty()) return 31;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root X(field.parse_real(&ok));
    if (!ok) return 32;
    if (!FP::defined(X.read().toReal())) return 33;
    remap("ZX" + wlCode, X);
    processEBC(X, field);
    remap("X" + wlCode, X);

    /* Reference dark, ignored */
    if (fields.empty()) return 34;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 35;

    /* Sample dark, ignored */
    if (fields.empty()) return 36;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 37;

    /* 135 dark, ignored */
    if (fields.empty()) return 38;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 39;

    /* 165 dark, ignored */
    if (fields.empty()) return 40;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 41;

    /* MBC, ignored */
    if (fields.empty()) return 42;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 43;

    /* Unknown, ignored */
    if (fields.empty()) return 44;
    fields.pop_front();

    /* Are these in the right order? needs verification */
    if (fields.empty()) return 45;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T1(field.parse_real(&ok));
    if (!ok) return 46;
    if (!FP::defined(T1.read().toReal())) return 47;
    remap("T1", T1);

    if (fields.empty()) return 48;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T2(field.parse_real(&ok));
    if (!ok) return 49;
    if (!FP::defined(T2.read().toReal())) return 50;
    remap("T2", T2);

    if (fields.empty()) return 51;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T3(field.parse_real(&ok));
    if (!ok) return 52;
    if (!FP::defined(T3.read().toReal())) return 53;
    remap("T3", T3);

    /* Unknown temperature (?), ignored */
    if (fields.empty()) return 54;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 55;

    /* Heater power (%), ignored */
    if (fields.empty()) return 56;
    field = fields.front().string_trimmed();
    fields.pop_front();
    v = field.parse_real(&ok);
    if (!ok) return 57;
    if (!FP::defined(v)) return 58;
    if (v < 0.0 || v > 100.0) return 59;

    if (fields.empty()) return 60;
    field = fields.front().string_trimmed();
    fields.pop_front();
    v = field.parse_real(&ok);
    if (!ok) return 61;
    if (!FP::defined(v)) return 62;
    reportedInstrumentVolume = v;

    Variant::Root Ba(calculateBaEBC(X.read().toDouble(), absorptionEfficiency));
    remap("Bac" + wlCode, Ba);


    if (FP::defined(frameTime))
        filterLatest.startTime = frameTime;
    filterLatest.Ip = Ip.read().toDouble();
    filterLatest.If = If.read().toDouble();
    filterLatest.Is1 = Is1.read().toDouble();
    filterLatest.Is2 = Is2.read().toDouble();
    Variant::Root In(calculateIn(filterLatest.If, filterLatest.Ip));
    remap("In" + wlCode, In);
    filterLatest.In = In.read().toDouble();

    filterBeginPending.startTime = frameTime;
    filterBeginPending.Ip = Ipz.read().toDouble();
    filterBeginPending.If = Ifz.read().toDouble();
    filterBeginPending.Is1 = Isz1.read().toDouble();
    filterBeginPending.Is2 = Isz2.read().toDouble();
    Variant::Root Inz(calculateIn(filterBeginPending.If, filterBeginPending.Ip));
    remap("Inz" + wlCode, Inz);
    filterBeginPending.In = Inz.read().toDouble();

    filterChangeIp->add(Ip.read().toDouble(), frameTime);

    if (!reportedNormalizationMatchesEffective()) {
        qCDebug(log)
            << "Reported normalization intensities do not match instrument reported values, filter change assumed at"
            << Logging::time(frameTime);

        emitNormalizationEnd(frameTime);

        filterBegin = filterBeginPending;
        filterBegin.startTime = frameTime;
        restoreInstrumentVolume = FP::undefined();
        filterSerialNumber.write().set(instrumentMeta["SerialNumber"]);
        filterFirmwareVersion.write().set(instrumentMeta["FirmwareVersion"]);
        filterAddress.write().set(instrumentMeta["Address"]);
        priorIr = FP::undefined();
        accumulateVolume = 0.0;
        forceFilterBreak = true;

        Variant::Write info = Variant::Write::empty();
        describeState(info);
        info.hash("IssuedCommand").set(issuedCommand.stateDescription());
        info.hash("Filter").set(buildNormalizationValue());
        info.hash("Flags").setFlags(reportedFlags);
        info.hash("InstrumentIntensities").setBool(true);
        event(frameTime,
              QObject::tr("Filter normalization updated due reported intensity differences."),
              false, info);

        if (state != NULL)
            state->requestStateSave();
        persistentValuesUpdated();
    }

    globalAdvance(frameTime);

    logValue(frameTime, LogStream_IntensityTr, "Ip" + wlCode, std::move(Ip));
    logValue(frameTime, LogStream_IntensityRef, "If" + wlCode, std::move(If));
    logValue(frameTime, LogStream_IntensityBS135, "Is1", std::move(Is1));
    logValue(frameTime, LogStream_IntensityBS165, "Is2", std::move(Is2));
    logValue(frameTime, LogStream_BC, "X" + wlCode, std::move(X));
    logValue(frameTime, LogStream_BC, "Bac" + wlCode, std::move(Ba));
    logValue(frameTime, LogStream_TemperatureAmbient, "T1", std::move(T1));
    logValue(frameTime, LogStream_TemperatureMeasure, "T2", std::move(T2));
    logValue(frameTime, LogStream_TemperatureSystem, "T3", std::move(T3));

    realtimeValue(frameTime, "In" + wlCode, std::move(In));

    streamAdvance(LogStream_IntensityTr, frameTime);
    streamAdvance(LogStream_IntensityRef, frameTime);
    streamAdvance(LogStream_IntensityBS135, frameTime);
    streamAdvance(LogStream_IntensityBS165, frameTime);
    streamAdvance(LogStream_BC, frameTime);
    streamAdvance(LogStream_TemperatureAmbient, frameTime);
    streamAdvance(LogStream_TemperatureMeasure, frameTime);
    streamAdvance(LogStream_TemperatureSystem, frameTime);

    return 0;
}

int AcquireThermoMAAP5012::processPF99Alternate(std::deque<Util::ByteView> &fields,
                                                double frameTime)
{
    bool ok = false;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    /* Are the units on this correct? */
    if (fields.empty()) return 1;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.size() > 4) return 2;
    if (field.size() < 2) return 3;
    if (field.indexOf('.') != field.npos) return 4;
    double v = field.parse_real(&ok);
    if (!ok) return 5;
    if (!FP::defined(v)) return 6;
    flowHighPrecision = v / 60.0;

    if (fields.empty()) return 7;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.indexOf('.') != field.npos) return 8;
    Variant::Root If(field.parse_real(&ok));
    if (!ok) return 9;
    if (!FP::defined(If.read().toReal())) return 10;
    remap("If" + wlCode, If);

    /* Reference 2, ignored */
    if (fields.empty()) return 10;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.indexOf('.') != field.npos) return 11;
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 12;

    if (fields.empty()) return 13;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.indexOf('.') != field.npos) return 14;
    Variant::Root Ip(field.parse_real(&ok));
    if (!ok) return 15;
    if (!FP::defined(Ip.read().toReal())) return 16;
    remap("Ip" + wlCode, Ip);

    /* Sample 2, ignored */
    if (fields.empty()) return 17;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.indexOf('.') != field.npos) return 18;
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 19;

    if (fields.empty()) return 20;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.indexOf('.') != field.npos) return 21;
    Variant::Root Is1(field.parse_real(&ok));
    if (!ok) return 22;
    if (!FP::defined(Is1.read().toReal())) return 23;
    remap("Is1", Is1);

    /* Scattering 135 2, ignored */
    if (fields.empty()) return 24;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.indexOf('.') != field.npos) return 25;
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 26;

    if (fields.empty()) return 27;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.indexOf('.') != field.npos) return 28;
    Variant::Root Is2(field.parse_real(&ok));
    if (!ok) return 29;
    if (!FP::defined(Is2.read().toReal())) return 30;
    remap("Is2", Is2);

    /* Scattering 165 2, ignored */
    if (fields.empty()) return 31;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.indexOf('.') != field.npos) return 32;
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 33;

    if (fields.empty()) return 34;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Ifz(field.parse_real(&ok));
    if (!ok) return 35;
    if (!FP::defined(Ifz.read().toReal())) return 36;
    remap("Ifz" + wlCode, Ifz);

    if (fields.empty()) return 37;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Ipz(field.parse_real(&ok));
    if (!ok) return 38;
    if (!FP::defined(Ipz.read().toReal())) return 39;
    remap("Ipz" + wlCode, Ipz);

    if (fields.empty()) return 40;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Isz1(field.parse_real(&ok));
    if (!ok) return 41;
    if (!FP::defined(Isz1.read().toReal())) return 42;
    remap("Isz1", Isz1);

    if (fields.empty()) return 43;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Isz2(field.parse_real(&ok));
    if (!ok) return 44;
    if (!FP::defined(Isz2.read().toReal())) return 45;
    remap("Isz2", Isz2);

    if (fields.empty()) return 47;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root X(field.parse_real(&ok));
    if (!ok) return 48;
    if (!FP::defined(X.read().toReal())) return 49;
    remap("ZX" + wlCode, X);
    processEBC(X, field);
    remap("X" + wlCode, X);

    /* Unknown, ignored */
    if (fields.empty()) return 50;
    fields.pop_front();

    /* Unknown, ignored */
    if (fields.empty()) return 51;
    fields.pop_front();

    if (fields.empty()) return 52;
    field = fields.front().string_trimmed();
    fields.pop_front();
    v = field.parse_real(&ok);
    if (!ok) return 53;
    if (!FP::defined(v)) return 54;
    if (v < -40.0 || v > 100.0) return 55;
    Variant::Root T1(v);
    remap("T1", T1);

    if (fields.empty()) return 56;
    field = fields.front().string_trimmed();
    fields.pop_front();
    v = field.parse_real(&ok);
    if (!ok) return 57;
    if (!FP::defined(v)) return 58;
    if (v < -40.0 || v > 100.0) return 59;
    Variant::Root T2(v);
    remap("T2", T2);

    if (fields.empty()) return 60;
    field = fields.front().string_trimmed();
    fields.pop_front();
    v = field.parse_real(&ok);
    if (!ok) return 61;
    if (!FP::defined(v)) return 62;
    if (v < -40.0 || v > 100.0) return 63;
    Variant::Root T3(v);
    remap("T3", T3);

    if (fields.empty()) return 64;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Pd1(field.parse_real(&ok));
    if (!ok) return 65;
    if (!FP::defined(Pd1.read().toReal())) return 66;
    remap("Pd1", Pd1);

    if (fields.empty()) return 67;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Pd2(field.parse_real(&ok));
    if (!ok) return 68;
    if (!FP::defined(Pd2.read().toReal())) return 69;
    remap("Pd2", Pd2);

    if (fields.empty()) return 70;
    field = fields.front().string_trimmed();
    fields.pop_front();
    v = field.parse_real(&ok);
    if (!ok) return 71;
    if (!FP::defined(v)) return 72;
    if (v < 10.0 || v > 1200.0) return 73;

    Variant::Root Ba(calculateBaEBC(X.read().toDouble(), absorptionEfficiency));
    remap("Bac" + wlCode, Ba);


    if (FP::defined(frameTime))
        filterLatest.startTime = frameTime;
    filterLatest.Ip = Ip.read().toDouble();
    filterLatest.If = If.read().toDouble();
    filterLatest.Is1 = Is1.read().toDouble();
    filterLatest.Is2 = Is2.read().toDouble();
    Variant::Root In(calculateIn(filterLatest.If, filterLatest.Ip));
    remap("In" + wlCode, In);
    filterLatest.In = In.read().toDouble();

    filterBeginPending.startTime = frameTime;
    filterBeginPending.Ip = Ipz.read().toDouble();
    filterBeginPending.If = Ifz.read().toDouble();
    filterBeginPending.Is1 = Isz1.read().toDouble();
    filterBeginPending.Is2 = Isz2.read().toDouble();
    Variant::Root Inz(calculateIn(filterBeginPending.If, filterBeginPending.Ip));
    remap("Inz" + wlCode, Inz);
    filterBeginPending.In = Inz.read().toDouble();

    filterChangeIp->add(Ip.read().toDouble(), frameTime);

    ambientPressure = v;
    orificePressureDrop = Pd1.read().toDouble();

    if (!reportedNormalizationMatchesEffective()) {
        qCDebug(log)
            << "Reported normalization intensities do not match instrument reported values, filter change assumed at"
            << Logging::time(frameTime);

        emitNormalizationEnd(frameTime);

        filterBegin = filterBeginPending;
        filterBegin.startTime = frameTime;
        restoreInstrumentVolume = FP::undefined();
        filterSerialNumber.write().set(instrumentMeta["SerialNumber"]);
        filterFirmwareVersion.write().set(instrumentMeta["FirmwareVersion"]);
        filterAddress.write().set(instrumentMeta["Address"]);
        priorIr = FP::undefined();
        accumulateVolume = 0.0;
        forceFilterBreak = true;

        Variant::Write info = Variant::Write::empty();
        describeState(info);
        info.hash("IssuedCommand").set(issuedCommand.stateDescription());
        info.hash("Filter").set(buildNormalizationValue());
        info.hash("Flags").setFlags(reportedFlags);
        info.hash("InstrumentIntensities").setBool(true);
        event(frameTime,
              QObject::tr("Filter normalization updated due reported intensity differences."),
              false, info);

        if (state != NULL)
            state->requestStateSave();
        persistentValuesUpdated();
    }

    globalAdvance(frameTime);

    logValue(frameTime, LogStream_IntensityTr, "Ip" + wlCode, std::move(Ip));
    logValue(frameTime, LogStream_IntensityRef, "If" + wlCode, std::move(If));
    logValue(frameTime, LogStream_IntensityBS135, "Is1", std::move(Is1));
    logValue(frameTime, LogStream_IntensityBS165, "Is2", std::move(Is2));
    logValue(frameTime, LogStream_BC, "X" + wlCode, std::move(X));
    logValue(frameTime, LogStream_BC, "Bac" + wlCode, std::move(Ba));
    logValue(frameTime, LogStream_TemperatureAmbient, "T1", std::move(T1));
    logValue(frameTime, LogStream_TemperatureMeasure, "T2", std::move(T2));
    logValue(frameTime, LogStream_TemperatureSystem, "T3", std::move(T3));
    logValue(frameTime, LogStream_PressureOrifice, "Pd1", std::move(Pd1));
    logValue(frameTime, LogStream_PressurePump, "Pd2", std::move(Pd2));

    realtimeValue(frameTime, "In" + wlCode, std::move(In));

    streamAdvance(LogStream_IntensityTr, frameTime);
    streamAdvance(LogStream_IntensityRef, frameTime);
    streamAdvance(LogStream_IntensityBS135, frameTime);
    streamAdvance(LogStream_IntensityBS165, frameTime);
    streamAdvance(LogStream_BC, frameTime);
    streamAdvance(LogStream_TemperatureAmbient, frameTime);
    streamAdvance(LogStream_TemperatureMeasure, frameTime);
    streamAdvance(LogStream_TemperatureSystem, frameTime);
    streamAdvance(LogStream_PressureOrifice, frameTime);
    streamAdvance(LogStream_PressurePump, frameTime);

    return 0;
}

int AcquireThermoMAAP5012::processPF12Start(std::deque<Util::ByteView> &fields,
                                            double &frameTime,
                                            bool skipDateTime,
                                            bool alterState)
{
    if (!skipDateTime) {
        int code = processDateTime(fields, frameTime, true, alterState);
        if (code > 0) return 100 + code; else if (code > 0) return -3;
    }

    bool ok = false;
    const auto &wlCode = Wavelength::code(config.front().wavelength);

    if (fields.empty()) return 1;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Ip(field.parse_real(&ok));
    if (!ok) return 2;
    if (!FP::defined(Ip.read().toReal())) return 3;
    remap("Ip" + wlCode, Ip);

    if (fields.empty()) return 4;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Is1(field.parse_real(&ok));
    if (!ok) return 5;
    if (!FP::defined(Is1.read().toReal())) return 6;
    remap("Is1", Is1);

    if (fields.empty()) return 7;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Is2(field.parse_real(&ok));
    if (!ok) return 8;
    if (!FP::defined(Is2.read().toReal())) return 9;
    remap("Is2", Is2);

    if (fields.empty()) return 10;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root If(field.parse_real(&ok));
    if (!ok) return 11;
    if (!FP::defined(If.read().toReal())) return 12;
    remap("If" + wlCode, If);

    if (fields.empty()) return 13;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Ipz(field.parse_real(&ok));
    if (!ok) return 14;
    if (!FP::defined(Ipz.read().toReal())) return 15;
    remap("Ipz" + wlCode, Ipz);

    if (fields.empty()) return 16;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Isz1(field.parse_real(&ok));
    if (!ok) return 17;
    if (!FP::defined(Isz1.read().toReal())) return 18;
    remap("Isz1", Isz1);

    if (fields.empty()) return 19;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Isz2(field.parse_real(&ok));
    if (!ok) return 20;
    if (!FP::defined(Isz2.read().toReal())) return 21;
    remap("Isz2", Isz2);

    if (fields.empty()) return 22;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Ifz(field.parse_real(&ok));
    if (!ok) return 23;
    if (!FP::defined(Ifz.read().toReal())) return 24;
    remap("Ifz" + wlCode, Ifz);

    if (fields.empty()) return 22;

    /* Complicated handling to allow for concatenation.  Normally this isn't
     * a problem because leading spaces cause a break, but when reading the
     * system flags, they don't have leading spaces so you can end up
     * with the last field looking like "0.0060000". */
    field = Util::ByteView(fields.front()).string_trimmed();
    if (field.size() < 7) {
        fields.pop_front();
    } else {
        int idx = field.indexOf('.');
        if (idx == -1) return 25;
        fields[0] = field.mid(idx + 4);
        field = field.mid(idx + 4);
    }
    double v = field.parse_real(&ok);
    if (!ok) return 26;
    if (!FP::defined(v)) return 27;

    /* Abort before we change anything, if we don't know that we're actually'
     * doing this */
    if (!alterState) return 0;

    /* This is supposedly m3/h, but it looks more like m3 per sample,
     * so we'll use that instead */
    flowLowPrecision = sampleVolumeToFlow(lastLowPrecisionFlow, frameTime, v);
    lastLowPrecisionFlow = frameTime;


    if (FP::defined(frameTime))
        filterLatest.startTime = frameTime;
    filterLatest.Ip = Ip.read().toDouble();
    filterLatest.If = If.read().toDouble();
    filterLatest.Is1 = Is1.read().toDouble();
    filterLatest.Is2 = Is2.read().toDouble();
    Variant::Root In(calculateIn(filterLatest.If, filterLatest.Ip));
    remap("In" + wlCode, In);
    filterLatest.In = In.read().toDouble();

    filterBeginPending.startTime = frameTime;
    filterBeginPending.Ip = Ipz.read().toDouble();
    filterBeginPending.If = Ifz.read().toDouble();
    filterBeginPending.Is1 = Isz1.read().toDouble();
    filterBeginPending.Is2 = Isz2.read().toDouble();
    Variant::Root Inz(calculateIn(filterBeginPending.If, filterBeginPending.Ip));
    remap("Inz" + wlCode, Inz);
    filterBeginPending.In = Inz.read().toDouble();

    filterChangeIp->add(Ip.read().toDouble(), frameTime);

    if (!reportedNormalizationMatchesEffective()) {
        qCDebug(log)
            << "Reported normalization intensities do not match instrument reported values, filter change assumed at"
            << Logging::time(frameTime);

        emitNormalizationEnd(frameTime);

        filterBegin = filterBeginPending;
        filterBegin.startTime = frameTime;
        restoreInstrumentVolume = FP::undefined();
        filterSerialNumber.write().set(instrumentMeta["SerialNumber"]);
        filterFirmwareVersion.write().set(instrumentMeta["FirmwareVersion"]);
        filterAddress.write().set(instrumentMeta["Address"]);
        priorIr = FP::undefined();
        accumulateVolume = 0.0;
        forceFilterBreak = true;

        Variant::Write info = Variant::Write::empty();
        describeState(info);
        info.hash("IssuedCommand").set(issuedCommand.stateDescription());
        info.hash("Filter").set(buildNormalizationValue());
        info.hash("Flags").setFlags(reportedFlags);
        info.hash("InstrumentIntensities").setBool(true);
        event(frameTime,
              QObject::tr("Filter normalization updated due reported intensity differences."),
              false, info);

        if (state != NULL)
            state->requestStateSave();
        persistentValuesUpdated();
    }

    globalAdvance(frameTime);

    logValue(frameTime, LogStream_IntensityTr, "Ip" + wlCode, std::move(Ip));
    logValue(frameTime, LogStream_IntensityRef, "If" + wlCode, std::move(If));
    logValue(frameTime, LogStream_IntensityBS135, "Is1", std::move(Is1));
    logValue(frameTime, LogStream_IntensityBS165, "Is2", std::move(Is2));

    realtimeValue(frameTime, "In" + wlCode, std::move(In));

    streamAdvance(LogStream_IntensityTr, frameTime);
    streamAdvance(LogStream_IntensityRef, frameTime);
    streamAdvance(LogStream_IntensityBS135, frameTime);
    streamAdvance(LogStream_IntensityBS165, frameTime);

    return 0;
}

int AcquireThermoMAAP5012::processPF12End(std::deque<Util::ByteView> &fields,
                                          double frameTime,
                                          bool alterState)
{
    bool ok = false;
    const auto &wlCode = Wavelength::code(config.front().wavelength);

    /* Iterations to converge, ignored */
    if (fields.empty()) return 1;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (!isAllDigits(field)) return 2;
    if (field.size() > 4) return 3;

    if (fields.empty()) return 4;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZSSA(field.parse_real(&ok));
    if (!ok) return 5;
    if (!FP::defined(ZSSA.read().toReal())) return 7;
    remap("ZSSA" + wlCode, ZSSA);

    if (fields.empty()) return 8;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZIr(field.parse_real(&ok));
    if (!ok) return 9;
    if (!FP::defined(ZIr.read().toReal())) return 10;
    remap("ZIr" + wlCode, ZIr);

    /* MBC, ignored */
    if (fields.empty()) return 11;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 12;

    if (fields.empty()) return 13;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root X(field.parse_real(&ok));
    if (!ok) return 14;
    if (!FP::defined(X.read().toReal())) return 15;
    remap("ZX" + wlCode, X);
    processEBC(X, field);
    remap("X" + wlCode, X);

    Variant::Root Ba(calculateBaEBC(X.read().toDouble(), absorptionEfficiency));
    remap("Bac" + wlCode, Ba);

    /* EBC from transmittance only, ignored */
    if (fields.empty()) return 16;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!FP::defined(field.parse_real(&ok)) || !ok) return 15;

    if (!alterState)
        return 0;

    globalAdvance(frameTime);

    logValue(frameTime, LogStream_SSA, "ZSSA" + wlCode, std::move(ZSSA));
    logValue(frameTime, LogStream_LOD, "ZIr" + wlCode, std::move(ZIr));
    logValue(frameTime, LogStream_BC, "X" + wlCode, std::move(X));
    logValue(frameTime, LogStream_BC, "Bac" + wlCode, std::move(Ba));

    streamAdvance(LogStream_SSA, frameTime);
    streamAdvance(LogStream_LOD, frameTime);
    streamAdvance(LogStream_BC, frameTime);

    return 0;
}

int AcquireThermoMAAP5012::processNetworkReport(std::deque<Util::ByteView> &fields,
                                                double frameTime)
{
    bool ok = false;
    const auto &wlCode = Wavelength::code(config.front().wavelength);

    if (fields.empty()) return 1;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    {
        int addr = field.parse_i32(&ok);
        if (!ok) return 2;
        if (addr < 0) return 3;

        if (config.first().address >= 0 && addr != config.first().address) {
            if (fields.size() >= 5)
                fields.erase(fields.begin(), fields.begin() + 5);
            return -1;
        }

        Variant::Root oldValue(instrumentMeta["Address"]);
        instrumentMeta["Address"].setInt64(addr);
        if (oldValue.read() != instrumentMeta["Address"]) {
            instrumentIDUpdated();
        }
    }

    if (fields.empty()) return 4;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.size() < 8) return 5;
    Variant::Root X;
    {
        double v = field.mid(0, 5).parse_real(&ok);
        if (!ok) return 6;
        if (!FP::defined(v)) return 7;
        v /= 100.0;

        double e = field.mid(5).parse_real(&ok);
        if (!ok) return 8;
        if (!FP::defined(v)) return 9;

        X.write().setDouble(v * pow(10.0, e));
    }
    remap("ZX" + wlCode, X);
    /* This field is always in ug/m3 so don't do the normal processing */
    if (!config.first().flowCalibration.isIdentity()) {
        X.write().setDouble(recalibrateFlow(X.read().toDouble(), lastKnownRawFlow, lastKnownFlow));
    }
    /* I don't see any way to change this, so assume it's fixed */
    X.write().setDouble(recalibrateArea(X.read().toDouble(), 200.0, config.first().area));
    remap("X" + wlCode, X);

    Variant::Root Ba(calculateBaEBC(X.read().toDouble(), absorptionEfficiency));
    remap("Bac" + wlCode, Ba);

    /* Unknown field, ignored */
    if (fields.empty()) return 10;
    fields.pop_front();

    /* Unknown field, ignored */
    if (fields.empty()) return 11;
    fields.pop_front();

    /* Serial number */
    if (fields.empty()) return 12;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 sn = field.parse_i64(&ok);
    if (!ok) return 13;
    if (!INTEGER::defined(sn) || sn <= 0) return 14;
    Variant::Root oldValue(instrumentMeta["SerialNumber"]);
    instrumentMeta["SerialNumber"].setInt64(sn);
    if (oldValue.read() != instrumentMeta["SerialNumber"]) {
        instrumentIDUpdated();
    }

    if (fields.empty()) return 15;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.size() < 6) return 16;
    Variant::Root ZF2((qint64) field.parse_u64(&ok, 16));
    if (!ok) return 17;
    remap("ZF2", ZF2);

    processGlobalStatus(ZF2);

    globalAdvance(frameTime);
    logValue(frameTime, LogStream_BC, "X" + wlCode, std::move(X));
    logValue(frameTime, LogStream_BC, "Bac" + wlCode, std::move(Ba));
    realtimeValue(frameTime, "ZF2", std::move(ZF2));
    streamAdvance(LogStream_BC, frameTime);
    return 0;
}

void AcquireThermoMAAP5012::processGlobalStatus(const Variant::Read &ZF2)
{
    qint64 bits = ZF2.toInt64();
    if (!INTEGER::defined(bits))
        return;

    for (int i = 0; i < 24; i++) {
        if (globalStatusBitsTranslation[i] == NULL)
            continue;
        if (!(bits & ((qint64) 1 << (qint64) i))) {
            reportedFlags.erase(globalStatusBitsTranslation[i]);
            continue;
        }
        reportedFlags.insert(globalStatusBitsTranslation[i]);
    }

    if (bits & globalStatusErrorMask) {
        if (reportFlagStatus != ReportFlag_Error) {
            forceRealtimeStateEmit = true;
            reportFlagStatus = ReportFlag_Error;
        }
    } else if (bits & globalStatusWarningMask) {
        if (reportFlagStatus != ReportFlag_Warning) {
            forceRealtimeStateEmit = true;
            reportFlagStatus = ReportFlag_Warning;
        }
    } else {
        if (reportFlagStatus != ReportFlag_Normal) {
            forceRealtimeStateEmit = true;
            reportFlagStatus = ReportFlag_Normal;
        }
    }
}


static Util::ByteView extractParameterField(const Util::ByteView &frame, std::string search)
{
    auto str = Util::to_lower(frame.toString());
    auto idx = str.find(Util::to_lower(std::move(search)));
    if (idx == str.npos)
        return {};
    auto result = frame.mid(idx + search.size());
    result.string_trimmed();
    std::size_t end = 0;
    while (end < result.size() && result[end] != ' ' && result[end] != '\t') {
        ++end;
    }
    result = result.mid(0, end);
    return result;
}

void AcquireThermoMAAP5012::parseParameters(const Util::ByteView &frame)
{
    auto field = extractParameterField(frame, "SIGMA BC:");
    if (!field.empty()) {
        bool ok = false;
        double v = field.parse_real(&ok);
        if (ok && v > 0.0 && v != absorptionEfficiency) {
            absorptionEfficiency = v;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
    }

    field = extractParameterField(frame, "VOLUME REFERENCE");
    if (!field.empty()) {
        auto lower = Util::to_lower(field.toString());
        if (Util::contains(lower, "standard") && !reportingAtSTP) {
            reportingAtSTP = true;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        } else if (Util::contains(lower, "operating") && reportingAtSTP) {
            reportingAtSTP = false;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
    }

    field = extractParameterField(frame, "STANDARD TEMPERATURE");
    if (!field.empty()) {
        bool ok = false;
        double v = field.parse_real(&ok);
        if (ok && v > 0.0 && v != reportingT) {
            reportingT = v;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
    }

    field = extractParameterField(frame, "DEVICE-ADDRESS:");
    if (!field.empty()) {
        bool ok = false;
        int addr = field.parse_i32(&ok);
        if (ok) {
            Variant::Root oldValue(instrumentMeta["Address"]);
            instrumentMeta["Address"].setInt64(addr);
            if (oldValue.read() != instrumentMeta["Address"]) {
                instrumentIDUpdated();
            }
        }
    }
}

int AcquireThermoMAAP5012::processDateTime(std::deque<Util::ByteView> &fields,
                                           double &frameTime,
                                           bool timeIsStart,
                                           bool alterState)
{
    bool ok = false;

    if (fields.empty()) return 1;
    auto subfields = Util::as_deque(fields.front().string_trimmed().split('-'));
    fields.pop_front();

    if (subfields.empty()) return 2;
    auto field = subfields.front().string_trimmed();
    subfields.pop_front();
    Variant::Root year;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0) return 3;
        if (field.size() == 2) {
            raw += 2000;
            year.write().setInt64(raw);
        } else if (field.size() == 4) {
            if (raw < 1900 || raw > 2100) return 4;
            year.write().setInt64(raw);
        } else {
            return 5;
        }
    }
    remap("YEAR", year);

    if (subfields.empty()) return 5;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    Variant::Root month;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 1 || raw > 12) return 6;
        month.write().setInt64(raw);
    }
    remap("MONTH", month);

    if (subfields.empty()) return 7;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    if (field.empty()) return 8;
    Variant::Root day;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 1 || raw > 31) return 9;
        day.write().setInt64(raw);
    }
    remap("DAY", day);

    if (!subfields.empty()) return 10;

    if (fields.empty()) return 11;
    field = fields.front().string_trimmed();
    fields.pop_front();
    subfields = Util::as_deque(field.split(':'));

    if (subfields.empty()) return 12;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    if (field.empty()) return 13;
    Variant::Root hour;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0 || raw > 23) return 14;
        hour.write().setInt64(raw);
    }
    remap("HOUR", hour);

    if (subfields.empty()) return 15;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    if (field.empty()) return 16;
    Variant::Root minute;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0 || raw > 59) return 17;
        minute.write().setInt64(raw);
    }
    remap("MINUTE", minute);

    if (subfields.empty()) return 18;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    if (field.empty()) return 19;
    Variant::Root second;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0 || raw > 60) return 20;
        second.write().setInt64(raw);
    }
    remap("SECOND", second);

    if (!alterState)
        return 0;

    if (!FP::defined(frameTime)) {
        qint64 y = year.read().toInt64();
        qint64 mo = month.read().toInt64();
        qint64 d = day.read().toInt64();
        qint64 h = hour.read().toInt64();
        qint64 m = minute.read().toInt64();
        qint64 s = second.read().toInt64();

        if (INTEGER::defined(y) &&
                y > 1900 &&
                y < 2100 &&
                INTEGER::defined(mo) &&
                mo >= 1 &&
                mo <= 12 &&
                INTEGER::defined(d) &&
                d >= 1 &&
                d <= 31 &&
                INTEGER::defined(h) &&
                h >= 0 &&
                h <= 23 &&
                INTEGER::defined(m) &&
                h >= 0 &&
                h <= 59 &&
                INTEGER::defined(s) &&
                h >= 0 &&
                h <= 60) {
            QDate date((int) y, (int) mo, (int) d);
            QTime time((int) h, (int) m, (int) s);
            if (date.isValid() && time.isValid()) {
                frameTime = Time::fromDateTime(QDateTime(date, time, Qt::UTC));

                /* Most records have the time at the end, but PF12 does it
                 * at the start, so we have to correct for that */
                if (timeIsStart) {
                    if (FP::defined(forwardOffset))
                        frameTime += forwardOffset;
                }
                if (FP::defined(lastEmbeddedTime) && frameTime >= lastEmbeddedTime) {
                    forwardOffset = frameTime - lastEmbeddedTime;
                }
                lastEmbeddedTime = frameTime;

                if (FP::defined(lastRecordTime) && frameTime < lastRecordTime)
                    return -3;
                lastRecordTime = frameTime;

                configurationAdvance(frameTime);
            }
        }
    }

    return 0;
}

int AcquireThermoMAAP5012::processIncomingRecord(std::deque<Util::ByteView> &fields,
                                                 const Util::ByteView &frame,
                                                 double &frameTime,
                                                 int suffixFields)
{
    Q_UNUSED(frame);

    int available = fields.size();
    if (available <= suffixFields)
        return -1;
    int availableDirect = available - suffixFields;

    if (fields.empty())
        return -1;

    if (fields[0].string_start("MD")) {
        fields.pop_front();
        int code = processNetworkReport(fields, frameTime);
        if (code > 0) return code + 1000;
        return code;
    }

    /* Handle the buffer fragmentation */
    if (availableDirect == 11 || availableDirect == 10) {
        auto tmp = fields;
        int code = processPF12Start(tmp, frameTime);
        if (code == 0) {
            fields = std::move(tmp);
            return -2;
        }
    } else if (availableDirect >= 17) {
        auto tmp = fields;
        int code = processPF12Start(tmp, frameTime, false, false);
        if (code == 0) {
            code = processPF12End(tmp, frameTime, false);
            if (code == 0) {
                processPF12Start(fields, frameTime);
                processPF12End(fields, frameTime);
                return 0;
            }
        }
    }
    if (availableDirect >= 6) {
        auto tmp = fields;
        int code = processPF12End(tmp, frameTime);
        if (code == 0) {
            fields = std::move(tmp);
            return 0;
        }
    }

    /* Unsolicited tail of a PF12, so we won't have the date */
    if (available == 6) {
        auto tmp = fields;
        int code = processPF12End(tmp, frameTime);
        if (code == 0) {
            fields = std::move(tmp);
            return -3;
        }
    }

    bool hadFrameTime = FP::defined(frameTime);
    int code = processDateTime(fields, frameTime);
    if (code > 0) return code + 2000; else if (code < 0) return code;
    availableDirect -= 2;
    available -= 2;

    if (availableDirect >= 22) {
        auto tmp = fields;
        code = processPF99Alternate(fields, frameTime);
        if (code != 0) {
            fields = std::move(tmp);
            code = processPF99(fields, frameTime);
            if (code > 0) return code + 4000;
        }
        return 0;
    }
    if (availableDirect >= 8) {
        code = processPF5(fields, frameTime);
        if (code > 0) return code + 5000;
        return 0;
    }
    if (availableDirect >= 4) {
        code = processPF3(fields, frameTime);
        if (code > 0) return code + 6000;
        return 0;
    }
    if (availableDirect >= 3) {
        code = processPF2(fields, frameTime);
        if (code > 0) return code + 7000;
        return 0;
    }
    if (availableDirect >= 2) {
        code = processPF1(fields, frameTime);
        if (code > 0) return code + 8000;
        return 0;
    }

    if (available == 22) {
        auto tmp = fields;
        code = processPF99Alternate(tmp, frameTime);
        if (code == 0) {
            fields = std::move(tmp);
            return -3;
        }
        tmp = fields;
        code = processPF99(tmp, frameTime);
        if (code == 0) {
            fields = std::move(tmp);
            return -3;
        }
    }
    if (available >= 15) {
        auto tmp = fields;
        double effectiveTime = frameTime;
        if (hadFrameTime || !FP::defined(effectiveTime) || !FP::defined(forwardOffset)) {
            code = processPF12Start(tmp, effectiveTime, true);
        } else {
            effectiveTime += forwardOffset;
            code = processPF12Start(tmp, effectiveTime, true);
        }
        if (code == 0) {
            code = processPF12End(tmp, effectiveTime);
            if (code == 0) {
                fields = std::move(tmp);
                return -3;
            }
        }
    }
    if (available == 8) {
        auto tmp = fields;
        code = processPF5(tmp, frameTime);
        if (code == 0) {
            fields = std::move(tmp);
            return -3;
        }
    }
    if (available == 4) {
        auto tmp = fields;
        code = processPF3(tmp, frameTime);
        if (code == 0) {
            fields = std::move(tmp);
            return -3;
        }
    }
    if (available == 3) {
        auto tmp = fields;
        code = processPF2(tmp, frameTime);
        if (code == 0) {
            fields = std::move(tmp);
            return -3;
        }
    }
    if (available == 2) {
        auto tmp = fields;
        code = processPF1(tmp, frameTime);
        if (code == 0) {
            fields = std::move(tmp);
            return -3;
        }
    }

    return -1;
}

void AcquireThermoMAAP5012::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (FP::defined(frameTime)) {
        configurationAdvance(frameTime);
        lastRecordTime = frameTime;
    }

    /* First stage: startup control.  This is separate because it's more
     * strict than the normal operational handling. */
    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        break;

    case RESP_INTERACTIVE_RUN_QUERYING:
    case RESP_INTERACTIVE_RUN_QUERYING_PARTIAL:
    case RESP_INTERACTIVE_RUN_QUERYING_COMPLETE:
    case RESP_INTERACTIVE_RUN_WAITING:
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        return;

    case RESP_INTERACTIVE_START_STOPREPORTS_INITIAL:
    case RESP_INTERACTIVE_START_STOPREPORTS_FLUSH:
        return;

    case RESP_INTERACTIVE_START_SETENGLISH:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETRECORDMINUTES:
    case RESP_INTERACTIVE_START_SETRECORDSECONDS:
    case RESP_INTERACTIVE_START_SETAVERAGING:
    case RESP_INTERACTIVE_START_SETADVANCETR:
    case RESP_INTERACTIVE_START_SETADVANCEHOUR:
    case RESP_INTERACTIVE_START_SETADVANCEATHOUR:
    case RESP_INTERACTIVE_START_SETTARGETFLOW:
    case RESP_INTERACTIVE_START_SETSTANDARDTEMP:
    case RESP_INTERACTIVE_START_SETSTP:
    case RESP_INTERACTIVE_START_SETPARAMETERLIST:
        /* No response to any of these */
        return;

    case RESP_INTERACTIVE_START_READVERSION_BEGIN:
    case RESP_INTERACTIVE_START_READVERSION_END: {
        QRegExp reFW("THERMO.*v(\\d[^\\s]*)", Qt::CaseInsensitive);
        QRegExp reSN("SERIAL\\s*NUMBER\\s*(?:-\\s*)?(\\d+)", Qt::CaseInsensitive);
        QString line(frame.toQString(false));
        if (reFW.indexIn(line) != -1) {
            Variant::Root oldValue(instrumentMeta["FirmwareVersion"]);
            instrumentMeta["FirmwareVersion"].setString(reFW.cap(1));
            if (oldValue.read() != instrumentMeta["FirmwareVersion"]) {
                instrumentIDUpdated();
            }

            if (responseState == RESP_INTERACTIVE_START_READVERSION_BEGIN) {
                responseState = RESP_INTERACTIVE_START_READVERSION_END;
                if (FP::defined(frameTime))
                    timeoutAt(frameTime + 2.0);
            }
        }
        if (reSN.indexIn(line) != -1) {
            Variant::Root oldValue(instrumentMeta["SerialNumber"]);

            bool ok = false;
            qint64 n = reSN.cap(1).toLongLong(&ok);
            if (ok && n > 0) {
                /* Unset value */
                if (n != 32767 && n != 32768) {
                    instrumentMeta["SerialNumber"].setInt64(n);
                }
            } else {
                instrumentMeta["SerialNumber"].setString(reSN.cap(1));
            }
            if (oldValue.read() != instrumentMeta["SerialNumber"]) {
                instrumentIDUpdated();
            }

            if (responseState == RESP_INTERACTIVE_START_READVERSION_BEGIN) {
                responseState = RESP_INTERACTIVE_START_READVERSION_END;
                if (FP::defined(frameTime))
                    timeoutAt(frameTime + 2.0);
            }
        }
        return;
    }

    case RESP_INTERACTIVE_START_READADDRESS: {
        bool ok = false;
        int addr = Util::ByteView(frame).string_trimmed().parse_i32(&ok);
        if (!ok) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "is not a valid address";
            invalidStartResponse(frameTime);
            return;
        }

        Variant::Root oldValue(instrumentMeta["Address"]);
        instrumentMeta["Address"].setInt64(addr);
        if (oldValue.read() != instrumentMeta["Address"]) {
            instrumentIDUpdated();
        }

        if (controlStream != NULL && FP::defined(frameTime)) {
            QDateTime st(Time::toDateTime(frameTime + 0.5));
            Util::ByteArray command("Z");
            command += QByteArray::number(st.date().year() % 100).rightJustified(2, '0');
            command += QByteArray::number(st.date().month()).rightJustified(2, '0');
            command += QByteArray::number(st.date().day()).rightJustified(2, '0');
            command += QByteArray::number(st.time().hour()).rightJustified(2, '0');
            command += QByteArray::number(st.time().minute()).rightJustified(2, '0');
            command += QByteArray::number(st.time().second()).rightJustified(2, '0');
            writeCommand(std::move(command));
        }
        responseState = RESP_INTERACTIVE_START_SETTIME;
        if (FP::defined(frameTime)) {
            discardData(frameTime + 0.5);
            timeoutAt(frameTime + 40.0);
        }

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetTime"),
                                  frameTime, FP::undefined()));
        }

        return;
    }

    case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN:
    case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN_RETRY: {
        persistentParameters.write().setEmpty();
        persistentParametersUpdated = false;
        realtimeParametersUpdated = false;

        QRegExp re("THERMO.*v([\\d\\.\\,]+).*SERIAL\\s*NUMBER\\s*(?:-\\s*)?(\\d+)",
                   Qt::CaseInsensitive);
        QString line(frame.toQString(false));
        if (re.indexIn(line) == -1)
            return;

        Variant::Root oldValue(instrumentMeta["FirmwareVersion"]);
        instrumentMeta["FirmwareVersion"].setString(re.cap(1));
        if (oldValue.read() != instrumentMeta["FirmwareVersion"]) {
            instrumentIDUpdated();
        }

        oldValue.write().set(instrumentMeta["SerialNumber"]);
        bool ok = false;
        qint64 n = re.cap(2).toLongLong(&ok);
        if (ok && n > 0) {
            /* Unset value */
            if (n != 32767 && n != 32768) {
                instrumentMeta["SerialNumber"].setInt64(n);
            }
        } else {
            instrumentMeta["SerialNumber"].setString(re.cap(2));
        }
        if (oldValue.read() != instrumentMeta["SerialNumber"]) {
            instrumentIDUpdated();
        }

        responseState = RESP_INTERACTIVE_START_READPARAMETERS_END;
        if (FP::defined(frameTime))
            timeoutAt(frameTime + 30.0);

        /* Fall through */
    }
    case RESP_INTERACTIVE_START_READPARAMETERS_END: {
        if (Util::ByteView(frame).string_trimmed().string_equal_insensitive("end")) {
            writeCommand("N");

            responseState = RESP_INTERACTIVE_START_READERRORCOUNTER;
            if (FP::defined(frameTime))
                timeoutAt(frameTime + 3.0);
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadErrorCounter"), frameTime, FP::undefined()));
            }
            return;
        }

        parseParameters(frame);

        persistentParametersUpdated = true;
        realtimeParametersUpdated = true;

        persistentParameters.write()
                            .toArray()
                            .after_back()
                            .setString(Util::trimmed(frame.toString()));
        return;
    }

    case RESP_INTERACTIVE_START_READERRORCOUNTER: {
        bool ok = false;
        if (Util::ByteView(frame).string_trimmed().parse_i32(&ok) != 0 || !ok) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "is not a valid error counter";
            invalidStartResponse(frameTime);
            return;
        }

        writeCommand("D 12");
        responseState = RESP_INTERACTIVE_START_FIRSTVALID;
        if (FP::defined(frameTime)) {
            discardData(frameTime + 2.0, 2);
            timeoutAt(frameTime + config.first().reportInterval * 2 + 2.0);
        }
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveFirstValid"),
                                                       frameTime, FP::undefined()));
        }
        return;
    }


    case RESP_INTERACTIVE_START_FIRSTVALID: {
        auto fields = CSV::acquisitionSplit(frame);
        if (fields.size() < 17)
            return;

        int code = processPF12Start(fields, frameTime);
        if (code < 0)
            return;
        if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;
            invalidStartResponse(frameTime);
            return;
        }

        code = processPF12End(fields, frameTime);
        if (code < 0)
            return;
        if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;
            invalidStartResponse(frameTime);
            return;
        }

        invalidateLogValues(frameTime);

        qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

        Variant::Write info = Variant::Write::empty();
        describeState(info);
        responseState = RESP_INTERACTIVE_RUN_QUERYING;

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        generalStatusUpdated();

        event(frameTime, QObject::tr("Communications established."), false, info);

        forceRealtimeStateEmit = true;
        reportFlagStatus = ReportFlag_Normal;
        issueCommand(Command(COMMAND_READ_TEMPERATURE_AMBIENT, "J0"), frameTime);
        return;
    }
    }

    /* Second stage: handle record prepending.  The MAAP appears to have
     * an internal output buffer that it writes records to as it gathers
     * the information, then finally flushes it when all the information
     * is complete (e.x. once the inversion finishes running).  However a
     * command response causes a buffer flush, so we can get a partial
     * record followed by the actual response to the command.  Eventually
     * the second half of the record will come in on its own.  So here
     * we handle the partial record being prepended to an actual
     * response, this consumes the record associated fields. */
    auto fields = CSV::acquisitionSplit(frame);
    int suffixFields = 0;
    switch (issuedCommand.type()) {
    case COMMAND_INVALID:
        break;

    case COMMAND_UNHANDLED:
    case COMMAND_READ_CONCENTRATION:
    case COMMAND_READ_PRESSURE_ORIFICE:
    case COMMAND_READ_TEMPERATURE_MEASURE:
    case COMMAND_READ_TEMPERATURE_SYSTEM:
    case COMMAND_READ_TEMPERATURE_AMBIENT:
    case COMMAND_READ_PRESSURE_PUMP:
    case COMMAND_READ_PRESSURE_AMBIENT:
    case COMMAND_READ_FLOW:
    case COMMAND_READ_VOLUME:
    case COMMAND_READ_PUMP_SIGNAL:
    case COMMAND_READ_INTENSITY_REFERENCE:
    case COMMAND_READ_INTENSITY_BS135:
    case COMMAND_READ_INTENSITY_BS165:
    case COMMAND_READ_INTENSITY_TR:
        suffixFields = 1;
        break;

    case COMMAND_READ_STATUS:
        suffixFields = 5;
        break;
    }
    int recordCode = processIncomingRecord(fields, frame, frameTime, suffixFields);
    if (recordCode > 0) {
        invalidRunResponse(frame, frameTime, recordCode);
        return;
    }
    if (recordCode == -3) {
        /* Line completely consumed without the command response, so
         * we normally just return and wait for the actual command response.
         * However, interactive control may need to issue another command,
         * so fall through in that event. */
        switch (responseState) {
        case RESP_INTERACTIVE_RUN_WAITING:
        case RESP_INTERACTIVE_RUN_QUERYING_COMPLETE:
            if (issuedCommand.type() != COMMAND_INVALID)
                return;
            break;

        case RESP_INTERACTIVE_RUN_QUERYING:
        case RESP_INTERACTIVE_RUN_QUERYING_PARTIAL:
            responseState = RESP_INTERACTIVE_RUN_QUERYING_COMPLETE;
            if (issuedCommand.type() != COMMAND_INVALID)
                return;
            break;

        default:
            return;
        }
    }

    bool ok = false;
    Util::ByteView field;

    /* Third stage handle command responses */
    switch (issuedCommand.type()) {
    case COMMAND_INVALID:
        break;

    case COMMAND_UNHANDLED:
        if (fields.empty()) {
            invalidRunResponse(frame, frameTime, 100);
            return;
        }
        break;

    case COMMAND_READ_CONCENTRATION: {
        const auto &wlCode = Wavelength::code(config.front().wavelength);

        if (fields.empty()) {
            invalidRunResponse(frame, frameTime, 101);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root X(field.parse_real(&ok));
        if (!ok) {
            invalidRunResponse(frame, frameTime, 102);
            return;
        }
        if (!FP::defined(X.read().toReal())) {
            invalidRunResponse(frame, frameTime, 103);
            return;
        }
        remap("ZX" + wlCode, X);
        processEBC(X, field);
        remap("X" + wlCode, X);

        Variant::Root Ba(calculateBaEBC(X.read().toDouble(), absorptionEfficiency));
        remap("Bac" + wlCode, Ba);

        globalAdvance(frameTime);
        logValue(frameTime, LogStream_BC, "X" + wlCode, std::move(X));
        logValue(frameTime, LogStream_BC, "Bac" + wlCode, std::move(Ba));
        streamAdvance(LogStream_BC, frameTime);
        break;
    }

    case COMMAND_READ_PRESSURE_ORIFICE: {
        if (fields.empty()) {
            invalidRunResponse(frame, frameTime, 104);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root Pd1(field.parse_real(&ok));
        if (!ok) {
            invalidRunResponse(frame, frameTime, 105);
            return;
        }
        if (!FP::defined(Pd1.read().toReal())) {
            invalidRunResponse(frame, frameTime, 106);
            return;
        }
        remap("Pd1", Pd1);
        orificePressureDrop = Pd1.read().toDouble();

        globalAdvance(frameTime);
        logValue(frameTime, LogStream_PressureOrifice, "Pd1", std::move(Pd1));
        streamAdvance(LogStream_PressureOrifice, frameTime);
        break;
    }

    case COMMAND_READ_TEMPERATURE_MEASURE: {
        if (fields.empty()) {
            invalidRunResponse(frame, frameTime, 107);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root T2(field.parse_real(&ok));
        if (!ok) {
            invalidRunResponse(frame, frameTime, 108);
            return;
        }
        if (!FP::defined(T2.read().toReal())) {
            invalidRunResponse(frame, frameTime, 109);
            return;
        }
        remap("T2", T2);

        globalAdvance(frameTime);
        logValue(frameTime, LogStream_TemperatureMeasure, "T2", std::move(T2));
        streamAdvance(LogStream_TemperatureMeasure, frameTime);
        break;
    }

    case COMMAND_READ_TEMPERATURE_SYSTEM: {
        if (fields.empty()) {
            invalidRunResponse(frame, frameTime, 110);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root T3(field.parse_real(&ok));
        if (!ok) {
            invalidRunResponse(frame, frameTime, 111);
            return;
        }
        if (!FP::defined(T3.read().toReal())) {
            invalidRunResponse(frame, frameTime, 112);
            return;
        }
        remap("T3", T3);

        globalAdvance(frameTime);
        logValue(frameTime, LogStream_TemperatureSystem, "T3", std::move(T3));
        streamAdvance(LogStream_TemperatureSystem, frameTime);
        break;
    }

    case COMMAND_READ_TEMPERATURE_AMBIENT: {
        if (fields.empty()) {
            invalidRunResponse(frame, frameTime, 110);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root T1(field.parse_real(&ok));
        if (!ok) {
            invalidRunResponse(frame, frameTime, 111);
            return;
        }
        if (!FP::defined(T1.read().toReal())) {
            invalidRunResponse(frame, frameTime, 112);
            return;
        }
        remap("T1", T1);

        globalAdvance(frameTime);
        logValue(frameTime, LogStream_TemperatureAmbient, "T1", std::move(T1));
        streamAdvance(LogStream_TemperatureAmbient, frameTime);
        break;
    }

    case COMMAND_READ_PRESSURE_PUMP: {
        if (fields.empty()) {
            invalidRunResponse(frame, frameTime, 113);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root Pd2(field.parse_real(&ok));
        if (!ok) {
            invalidRunResponse(frame, frameTime, 114);
            return;
        }
        if (!FP::defined(Pd2.read().toReal())) {
            invalidRunResponse(frame, frameTime, 115);
            return;
        }
        remap("Pd2", Pd2);

        globalAdvance(frameTime);
        logValue(frameTime, LogStream_PressurePump, "Pd2", std::move(Pd2));
        streamAdvance(LogStream_PressurePump, frameTime);
        break;
    }

    case COMMAND_READ_PRESSURE_AMBIENT: {
        if (fields.empty()) {
            invalidRunResponse(frame, frameTime, 113);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        double v = field.parse_real(&ok);
        if (!ok) {
            invalidRunResponse(frame, frameTime, 114);
            return;
        }
        if (!FP::defined(v)) {
            invalidRunResponse(frame, frameTime, 115);
            return;
        }
        ambientPressure = v;
        break;
    }

    case COMMAND_READ_FLOW: {
        if (fields.empty()) {
            invalidRunResponse(frame, frameTime, 116);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        double v = field.parse_real(&ok);
        if (!ok) {
            invalidRunResponse(frame, frameTime, 117);
            return;
        }
        if (!FP::defined(v)) {
            invalidRunResponse(frame, frameTime, 118);
            return;
        }
        flowHighPrecision = v / 60.0;
        break;
    }

    case COMMAND_READ_VOLUME: {
        if (fields.empty()) {
            invalidRunResponse(frame, frameTime, 119);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        double v = field.parse_real(&ok);
        if (!ok) {
            invalidRunResponse(frame, frameTime, 120);
            return;
        }
        if (!FP::defined(v)) {
            invalidRunResponse(frame, frameTime, 121);
            return;
        }

        reportedInstrumentVolume = v;
        break;
    }

    case COMMAND_READ_STATUS: {
        if (fields.empty()) {
            invalidRunResponse(frame, frameTime, 122);
            return;
        }
        Util::ByteArray assemble;
        if (fields.size() > 4) {
            assemble = fields.front().string_trimmed();
            fields.pop_front();
            assemble += fields.front().string_trimmed();
            fields.pop_front();
            assemble += fields.front().string_trimmed();
            fields.pop_front();
            assemble += fields.front().string_trimmed();
            fields.pop_front();
            field = assemble;
        } else {
            field = fields.front().string_trimmed();
            fields.pop_front();
        }

        reportedFlags.clear();

        Variant::Root ZF3((qint64) field.parse_u64(&ok, 16));
        if (!ok) {
            invalidRunResponse(frame, frameTime, 123);
            return;
        }
        remap("ZF3", ZF3);
        qint64 bits = ZF3.read().toInt64();
        if (INTEGER::defined(bits)) {
            for (int i = 0; i < 64; i++) {
                if (!(bits & ((qint64) 1 << (qint64) i)))
                    continue;
                if (detailedStatusBitsTranslation[i] == NULL)
                    continue;
                reportedFlags.insert(detailedStatusBitsTranslation[i]);
            }
        }

        if (fields.empty()) {
            invalidRunResponse(frame, frameTime, 124);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root ZF2((qint64) field.parse_u64(&ok, 16));
        if (!ok) {
            invalidRunResponse(frame, frameTime, 125);
            return;
        }
        remap("ZF2", ZF2);

        processGlobalStatus(ZF2);
        globalAdvance(frameTime);
        realtimeValue(frameTime, "ZF2", std::move(ZF2));
        realtimeValue(frameTime, "ZF3", std::move(ZF3));
        break;
    }

    case COMMAND_READ_INTENSITY_REFERENCE: {
        const auto &wlCode = Wavelength::code(config.front().wavelength);

        if (fields.empty()) {
            invalidRunResponse(frame, frameTime, 126);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root I(field.parse_real(&ok));
        if (!ok) {
            invalidRunResponse(frame, frameTime, 127);
            return;
        }
        if (!FP::defined(I.read().toReal())) {
            invalidRunResponse(frame, frameTime, 128);
            return;
        }
        remap("If" + wlCode, I);

        filterLatest.If = I.read().toDouble();
        if (FP::defined(frameTime))
            filterLatest.startTime = frameTime;

        Variant::Root In(calculateIn(filterLatest.If, filterLatest.Ip));
        remap("In" + wlCode, In);
        filterLatest.In = In.read().toDouble();

        globalAdvance(frameTime);
        logValue(frameTime, LogStream_IntensityRef, "If" + wlCode, std::move(I));
        realtimeValue(frameTime, "In" + wlCode, std::move(In));
        streamAdvance(LogStream_IntensityRef, frameTime);
        break;
    }

    case COMMAND_READ_INTENSITY_BS135: {
        if (fields.empty()) {
            invalidRunResponse(frame, frameTime, 129);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root I(field.parse_real(&ok));
        if (!ok) {
            invalidRunResponse(frame, frameTime, 130);
            return;
        }
        if (!FP::defined(I.read().toReal())) {
            invalidRunResponse(frame, frameTime, 131);
            return;
        }
        remap("Is1", I);

        filterLatest.Is1 = I.read().toDouble();
        if (FP::defined(frameTime))
            filterLatest.startTime = frameTime;

        globalAdvance(frameTime);
        logValue(frameTime, LogStream_IntensityBS135, "Is1", std::move(I));
        streamAdvance(LogStream_IntensityBS135, frameTime);
        break;
    }

    case COMMAND_READ_INTENSITY_BS165: {
        if (fields.empty()) {
            invalidRunResponse(frame, frameTime, 132);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root I(field.parse_real(&ok));
        if (!ok) {
            invalidRunResponse(frame, frameTime, 133);
            return;
        }
        if (!FP::defined(I.read().toReal())) {
            invalidRunResponse(frame, frameTime, 134);
            return;
        }
        remap("Is2", I);

        filterLatest.Is2 = I.read().toDouble();
        if (FP::defined(frameTime))
            filterLatest.startTime = frameTime;

        globalAdvance(frameTime);
        logValue(frameTime, LogStream_IntensityBS165, "Is2", std::move(I));
        streamAdvance(LogStream_IntensityBS165, frameTime);
        break;
    }

    case COMMAND_READ_INTENSITY_TR: {
        const auto &wlCode = Wavelength::code(config.front().wavelength);

        if (fields.empty()) {
            invalidRunResponse(frame, frameTime, 126);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root I(field.parse_real(&ok));
        if (!ok) {
            invalidRunResponse(frame, frameTime, 127);
            return;
        }
        if (!FP::defined(I.read().toReal())) {
            invalidRunResponse(frame, frameTime, 128);
            return;
        }
        remap("Ip" + wlCode, I);

        filterLatest.Ip = I.read().toDouble();
        if (FP::defined(frameTime))
            filterLatest.startTime = frameTime;

        Variant::Root In(calculateIn(filterLatest.If, filterLatest.Ip));
        remap("In" + wlCode, In);
        filterLatest.In = In.read().toDouble();

        filterChangeIp->add(I.read().toDouble(), frameTime);

        globalAdvance(frameTime);
        logValue(frameTime, LogStream_IntensityTr, "Ip" + wlCode, std::move(I));
        realtimeValue(frameTime, "In" + wlCode, std::move(In));
        streamAdvance(LogStream_IntensityTr, frameTime);
        break;
    }

    case COMMAND_READ_PUMP_SIGNAL: {
        if (fields.empty()) {
            invalidRunResponse(frame, frameTime, 129);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        double v = field.parse_real(&ok);
        if (!ok) {
            invalidRunResponse(frame, frameTime, 130);
            return;
        }
        if (!FP::defined(v)) {
            invalidRunResponse(frame, frameTime, 131);
            return;
        }
        Variant::Root PCT((v / 4096.0) * 100.0);
        remap("PCT", PCT);

        globalAdvance(frameTime);
        realtimeValue(frameTime, "PCT", std::move(PCT));
        break;
    }
    }

    /* Fourth stage: handle interactive control */
    if (!inInteractiveMode() || controlStream == NULL) {
        if (recordCode == 0 && fields.empty()) {
            switch (responseState) {
            case RESP_PASSIVE_WAIT: {
                qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);

                Variant::Write info = Variant::Write::empty();
                describeState(info);
                info.hash("IssuedCommand").set(issuedCommand.stateDescription());
                info.hash("ResponseState").setString("PassiveWait");
                event(frameTime, QObject::tr("Passive communications established."), false, info);

                responseState = RESP_PASSIVE_RUN;
                if (FP::defined(frameTime))
                    timeoutAt(frameTime + config.first().reportInterval);
                generalStatusUpdated();

                forceRealtimeStateEmit = true;
                break;
            }

            case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
            case RESP_AUTOPROBE_PASSIVE_WAIT:
                if (++autoprobePassiveValidRecords > 5) {
                    qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                    Variant::Write info = Variant::Write::empty();
                    describeState(info);
                    info.hash("IssuedCommand").set(issuedCommand.stateDescription());
                    event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);

                    responseState = RESP_PASSIVE_RUN;
                    {
                        std::lock_guard<std::mutex> lock(mutex);
                        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                    }
                    autoprobeStatusUpdated();
                    generalStatusUpdated();

                    if (FP::defined(frameTime))
                        timeoutAt(frameTime + config.first().reportInterval);
                }
                break;

            case RESP_PASSIVE_RUN:
                if (FP::defined(frameTime))
                    timeoutAt(frameTime + config.first().reportInterval);
                break;

            default:
                break;
            }
        } else if (recordCode > 0) {
            invalidateLogValues(frameTime);
        }


        if (!commandBacklog.isEmpty()) {
            issuedCommand = commandBacklog.takeFirst();
        } else {
            issuedCommand.invalidate();
        }
        return;
    }

    /* Change states for partial record reception */
    switch (responseState) {
    case RESP_INTERACTIVE_RUN_WAITING:
        /* When we're waiting, we require a complete record response.  Once we
         * have that, then we start the full query sequence again. */
        if (recordCode != 0) {
            issuedCommand.invalidate();
            commandBacklog.clear();
            return;
        }

        responseState = RESP_INTERACTIVE_RUN_QUERYING;
        issueCommand(Command(COMMAND_READ_TEMPERATURE_AMBIENT, "J0"), frameTime);
        return;

    case RESP_INTERACTIVE_RUN_QUERYING:
        if (recordCode == -2)
            responseState = RESP_INTERACTIVE_RUN_QUERYING_PARTIAL;
        else if (recordCode == 0)
            responseState = RESP_INTERACTIVE_RUN_QUERYING_COMPLETE;
        break;

    case RESP_INTERACTIVE_RUN_QUERYING_PARTIAL:
        if (recordCode == 0)
            responseState = RESP_INTERACTIVE_RUN_QUERYING_COMPLETE;
        break;

    case RESP_INTERACTIVE_RUN_QUERYING_COMPLETE:
        break;

    default:
        Q_ASSERT(false);
        break;
    }

    /* Issue the next command in the sequence */
    switch (issuedCommand.type()) {
    case COMMAND_INVALID:
    case COMMAND_UNHANDLED:
    case COMMAND_READ_CONCENTRATION:
    case COMMAND_READ_INTENSITY_REFERENCE:
    case COMMAND_READ_INTENSITY_BS135:
    case COMMAND_READ_INTENSITY_BS165:
    case COMMAND_READ_INTENSITY_TR:
        issueCommand(Command(COMMAND_READ_TEMPERATURE_AMBIENT, "J0"), frameTime);
        break;

    case COMMAND_READ_TEMPERATURE_AMBIENT:
        issueCommand(Command(COMMAND_READ_TEMPERATURE_MEASURE, "J1"), frameTime);
        break;
    case COMMAND_READ_TEMPERATURE_MEASURE:
        issueCommand(Command(COMMAND_READ_TEMPERATURE_SYSTEM, "J2"), frameTime);
        break;
    case COMMAND_READ_TEMPERATURE_SYSTEM:
        issueCommand(Command(COMMAND_READ_PRESSURE_ORIFICE, "J3"), frameTime);
        break;
    case COMMAND_READ_PRESSURE_ORIFICE:
        issueCommand(Command(COMMAND_READ_PRESSURE_PUMP, "J4"), frameTime);
        break;
    case COMMAND_READ_PRESSURE_PUMP:
        issueCommand(Command(COMMAND_READ_PRESSURE_AMBIENT, "J5"), frameTime);
        break;
    case COMMAND_READ_PRESSURE_AMBIENT:
        issueCommand(Command(COMMAND_READ_FLOW, "JK"), frameTime);
        break;
    case COMMAND_READ_FLOW:
        issueCommand(Command(COMMAND_READ_VOLUME, "JN"), frameTime);
        break;
    case COMMAND_READ_VOLUME:
        issueCommand(Command(COMMAND_READ_PUMP_SIGNAL, "JM"), frameTime);
        break;
    case COMMAND_READ_PUMP_SIGNAL:
        issueCommand(Command(COMMAND_READ_STATUS, "#"), frameTime);
        break;

    case COMMAND_READ_STATUS:
        switch (responseState) {
        case RESP_INTERACTIVE_RUN_QUERYING:
        case RESP_INTERACTIVE_RUN_WAITING:
        case RESP_INTERACTIVE_RUN_QUERYING_PARTIAL:
            /* A partial record would already have kicked us out of this
             * state above, so we know we haven't seen anything */
            responseState = RESP_INTERACTIVE_RUN_WAITING;

            issuedCommand.invalidate();
            commandBacklog.clear();

            if (FP::defined(frameTime))
                timeoutAt(frameTime + config.first().reportInterval);
            return;

        case RESP_INTERACTIVE_RUN_QUERYING_COMPLETE:
            responseState = RESP_INTERACTIVE_RUN_QUERYING;
            issueCommand(Command(COMMAND_READ_TEMPERATURE_AMBIENT, "J0"), frameTime);
            break;

        default:
            Q_ASSERT(false);
            break;
        }
        break;
    }
}

void AcquireThermoMAAP5012::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_INTERACTIVE_INITIALIZE:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        interactiveStartBegin(frameTime);
        generalStatusUpdated();
        break;

    case RESP_INTERACTIVE_RUN_WAITING:
        /* During a filter change or zero, the model may or may not run, so
         * allow for timeouts during those to just restart the query cycle */
        if (reportedFlags.count("FilterChanging") || reportedFlags.count("Zero")) {
            responseState = RESP_INTERACTIVE_RUN_QUERYING;
            issueCommand(Command(COMMAND_READ_TEMPERATURE_AMBIENT, "J0"), frameTime);
            return;
        }
    case RESP_INTERACTIVE_RUN_QUERYING:
    case RESP_INTERACTIVE_RUN_QUERYING_PARTIAL:
    case RESP_INTERACTIVE_RUN_QUERYING_COMPLETE:
        qCDebug(log) << "Timeout in interactive state" << responseState << "at"
                     << Logging::time(frameTime) << " waiting for command "
                     << (int) issuedCommand.type();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            info.hash("IssuedCommand").set(issuedCommand.stateDescription());
            event(frameTime, QObject::tr("Timeout waiting for response.  Communications Dropped."),
                  true, info);
        }

        interactiveStartBegin(frameTime);
        generalStatusUpdated();
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime)
                     << "waiting for command" << issuedCommand.type();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            info.hash("IssuedCommand").set(issuedCommand.stateDescription());
            event(frameTime, QObject::tr("Timeout waiting for response.  Communications Dropped."),
                  true, info);
        }

        timeoutAt(FP::undefined());
        responseState = RESP_PASSIVE_WAIT;
        issuedCommand.invalidate();
        commandBacklog.clear();
        generalStatusUpdated();

        invalidateLogValues(frameTime);
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        /* Fall through */
    case RESP_PASSIVE_WAIT:
        timeoutAt(FP::undefined());
        issuedCommand.invalidate();
        commandBacklog.clear();
        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_START_READVERSION_END:
        writeCommand("?");
        responseState = RESP_INTERACTIVE_START_READADDRESS;

        discardData(FP::undefined());
        timeoutAt(frameTime + 3.0);
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveReadAddress"),
                                                       frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_READPARAMETERS_END:
        writeCommand("N");
        responseState = RESP_INTERACTIVE_START_READERRORCOUNTER;

        discardData(FP::undefined());
        timeoutAt(frameTime + 3.0);
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadErrorCounter"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN:
        responseState = RESP_INTERACTIVE_START_READPARAMETERS_BEGIN_RETRY;
        discardData(frameTime + 5.0);
        timeoutAt(frameTime + 30.0);
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS_INITIAL:
    case RESP_INTERACTIVE_START_STOPREPORTS_FLUSH:
    case RESP_INTERACTIVE_START_SETENGLISH:
    case RESP_INTERACTIVE_START_READVERSION_BEGIN:
    case RESP_INTERACTIVE_START_READADDRESS:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETRECORDMINUTES:
    case RESP_INTERACTIVE_START_SETRECORDSECONDS:
    case RESP_INTERACTIVE_START_SETAVERAGING:
    case RESP_INTERACTIVE_START_SETADVANCETR:
    case RESP_INTERACTIVE_START_SETADVANCEHOUR:
    case RESP_INTERACTIVE_START_SETADVANCEATHOUR:
    case RESP_INTERACTIVE_START_SETTARGETFLOW:
    case RESP_INTERACTIVE_START_SETSTANDARDTEMP:
    case RESP_INTERACTIVE_START_SETSTP:
    case RESP_INTERACTIVE_START_SETPARAMETERLIST:
    case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN_RETRY:
    case RESP_INTERACTIVE_START_READERRORCOUNTER:
    case RESP_INTERACTIVE_START_FIRSTVALID:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);
        issuedCommand.invalidate();
        commandBacklog.clear();
        autoprobePassiveValidRecords = 0;

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireThermoMAAP5012::discardDataCompleted(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configurationAdvance(frameTime);

    Q_ASSERT(!config.isEmpty());

    switch (responseState) {
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        interactiveStartBegin(frameTime);
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS_INITIAL:
        writeCommand("N");
        responseState = RESP_INTERACTIVE_START_STOPREPORTS_FLUSH;

        discardData(frameTime + 0.5);
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveStopReportsFlush"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS_FLUSH:
        writeCommand("KK 1");
        responseState = RESP_INTERACTIVE_START_SETENGLISH;

        discardData(frameTime + 0.5);
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveSetEnglish"),
                                                       frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETENGLISH:
        writeCommand("v");
        responseState = RESP_INTERACTIVE_START_READVERSION_BEGIN;

        timeoutAt(frameTime + 5.0);
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveReadVersion"),
                                                       frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETTIME:
        writeCommand("d2 0");
        responseState = RESP_INTERACTIVE_START_SETRECORDMINUTES;

        discardData(frameTime + 0.5);
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetRecordMinutes"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETRECORDMINUTES:
        writeCommand("d3 0");
        responseState = RESP_INTERACTIVE_START_SETRECORDSECONDS;

        discardData(frameTime + 0.5);
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetRecordSeconds"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETRECORDSECONDS:
        writeCommand("K0 0");
        responseState = RESP_INTERACTIVE_START_SETAVERAGING;

        discardData(frameTime + 0.5);
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetAveraging"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETAVERAGING:
        if (FP::defined(config.first().advanceTransmittance)) {
            Util::ByteArray cmd("K1 ");
            int tr = (int) qRound(config.first().advanceTransmittance * 100.0);
            if (tr < 0)
                tr = 0;
            else if (tr > 99)
                tr = 99;
            cmd += QByteArray::number(tr);
            writeCommand(std::move(cmd));
            responseState = RESP_INTERACTIVE_START_SETADVANCETR;

            discardData(frameTime + 0.5);
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveSetAdvanceTr"), frameTime, FP::undefined()));
            }
        } else {
            Util::ByteArray cmd("K2 ");
            int hours = config.first().advanceHours;
            if (hours < 1)
                hours = 1;
            else if (hours > 100)
                hours = 100;
            cmd += QByteArray::number(hours);
            writeCommand(std::move(cmd));
            responseState = RESP_INTERACTIVE_START_SETADVANCEHOUR;

            discardData(frameTime + 0.5);
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveSetAdvanceHours"), frameTime, FP::undefined()));
            }
        }
        break;

    case RESP_INTERACTIVE_START_SETADVANCETR: {
        Util::ByteArray cmd("K2 ");
        int hours = config.first().advanceHours;
        if (hours < 1)
            hours = 1;
        else if (hours > 100)
            hours = 100;
        cmd += QByteArray::number(hours);
        writeCommand(std::move(cmd));
        responseState = RESP_INTERACTIVE_START_SETADVANCEHOUR;

        discardData(frameTime + 0.5);
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetAdvanceHours"), frameTime, FP::undefined()));
        }
        break;
    }

    case RESP_INTERACTIVE_START_SETADVANCEHOUR: {
        Util::ByteArray cmd("K3 ");
        int hour = config.first().advanceAtHour;
        if (hour < 0 || hour > 23)
            hour = 0;
        else
            --hour;
        cmd += QByteArray::number(hour);
        writeCommand(std::move(cmd));
        responseState = RESP_INTERACTIVE_START_SETADVANCEATHOUR;

        discardData(frameTime + 0.5);
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetAdvanceAtHour"), frameTime, FP::undefined()));
        }
        break;
    }

    case RESP_INTERACTIVE_START_SETADVANCEATHOUR:
        if (FP::defined(config.first().targetFlow)) {
            Util::ByteArray cmd("K4 ");
            int flow = (int) qRound(config.first().targetFlow * 60.0);
            if (flow < 0)
                flow = 0;
            else if (flow > 3000)
                flow = 3000;
            cmd += QByteArray::number(flow);
            writeCommand(std::move(cmd));
            responseState = RESP_INTERACTIVE_START_SETTARGETFLOW;

            discardData(frameTime + 0.5);
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveSetTargetFlow"), frameTime, FP::undefined()));
            }
        } else {
            writeCommand("KN 0");
            responseState = RESP_INTERACTIVE_START_SETSTANDARDTEMP;

            discardData(frameTime + 0.5);
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveSetStandardTemperature"), frameTime, FP::undefined()));
            }
        }
        break;

    case RESP_INTERACTIVE_START_SETTARGETFLOW:
        writeCommand("KN 0");
        responseState = RESP_INTERACTIVE_START_SETSTANDARDTEMP;

        discardData(frameTime + 0.5);
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetStandardTemperature"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETSTANDARDTEMP:
        writeCommand("KM 1");
        responseState = RESP_INTERACTIVE_START_SETSTP;

        discardData(frameTime + 0.5);
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetSTP"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETSTP:
        writeCommand("D 8");
        responseState = RESP_INTERACTIVE_START_SETPARAMETERLIST;

        discardData(frameTime + 0.5);
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetParameterList"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETPARAMETERLIST:
        writeCommand("P");
        responseState = RESP_INTERACTIVE_START_READPARAMETERS_BEGIN;

        timeoutAt(frameTime + 10.0);
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadParameters"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN_RETRY:
        writeCommand("P");
        timeoutAt(frameTime + 15.0);
        break;

    case RESP_INTERACTIVE_START_FIRSTVALID:
        timeoutAt(frameTime + config.first().reportInterval);
        break;

    default:
        break;
    }
}

void AcquireThermoMAAP5012::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configurationAdvance(frameTime);

    Command command(COMMAND_INVALID, frame);
    if (frame.string_equal_insensitive("c")) {
        command = Command(COMMAND_READ_CONCENTRATION, frame);
    } else if (frame.string_equal_insensitive("j0")) {
        command = Command(COMMAND_READ_TEMPERATURE_AMBIENT, frame);
    } else if (frame.string_equal_insensitive("j1")) {
        command = Command(COMMAND_READ_TEMPERATURE_MEASURE, frame);
    } else if (frame.string_equal_insensitive("j2")) {
        command = Command(COMMAND_READ_TEMPERATURE_SYSTEM, frame);
    } else if (frame.string_equal_insensitive("j3")) {
        command = Command(COMMAND_READ_PRESSURE_ORIFICE, frame);
    } else if (frame.string_equal_insensitive("j4")) {
        command = Command(COMMAND_READ_PRESSURE_PUMP, frame);
    } else if (frame.string_equal_insensitive("j5")) {
        command = Command(COMMAND_READ_PRESSURE_AMBIENT, frame);
    } else if (frame.string_equal_insensitive("jk")) {
        command = Command(COMMAND_READ_FLOW, frame);
    } else if (frame.string_equal_insensitive("jn")) {
        command = Command(COMMAND_READ_VOLUME, frame);
    } else if (frame.string_equal_insensitive("#")) {
        command = Command(COMMAND_READ_STATUS, frame);
    } else if (frame.string_equal_insensitive("ja")) {
        command = Command(COMMAND_READ_INTENSITY_REFERENCE, frame);
    } else if (frame.string_equal_insensitive("jb")) {
        command = Command(COMMAND_READ_INTENSITY_BS135, frame);
    } else if (frame.string_equal_insensitive("jc")) {
        command = Command(COMMAND_READ_INTENSITY_BS165, frame);
    } else if (frame.string_equal_insensitive("jd")) {
        command = Command(COMMAND_READ_INTENSITY_TR, frame);
    } else if (frame.string_equal_insensitive("jn")) {
        command = Command(COMMAND_READ_VOLUME, frame);
    } else if (frame.string_equal_insensitive("jm")) {
        command = Command(COMMAND_READ_PUMP_SIGNAL, frame);
    } else if (frame.string_equal_insensitive("m1")) {
        command = Command(COMMAND_UNHANDLED, frame);
    } else if (frame.string_equal_insensitive("h")) {
        command = Command(COMMAND_READ_CONCENTRATION, frame);
    } else if (frame.string_equal_insensitive("h1")) {
        command = Command(COMMAND_READ_CONCENTRATION, frame);
    } else if (frame.string_equal_insensitive("h3")) {
        command = Command(COMMAND_READ_CONCENTRATION, frame);
    } else if (frame.string_equal_insensitive("ht")) {
        command = Command(COMMAND_READ_CONCENTRATION, frame);
    } else if (frame.string_equal_insensitive("i0") ||
            frame.string_equal_insensitive("i1") ||
            frame.string_equal_insensitive("i2") ||
            frame.string_equal_insensitive("i3") ||
            frame.string_equal_insensitive("i4") ||
            frame.string_equal_insensitive("i5") ||
            frame.string_equal_insensitive("i6") ||
            frame.string_equal_insensitive("i7") ||
            frame.string_equal_insensitive("ia") ||
            frame.string_equal_insensitive("ib") ||
            frame.string_equal_insensitive("ic") ||
            frame.string_equal_insensitive("id")) {
        command = Command(COMMAND_UNHANDLED, frame);
    } else if (frame.string_equal_insensitive("jj")) {
        command = Command(COMMAND_UNHANDLED, frame);
    } else if (frame.string_equal_insensitive("jl")) {
        command = Command(COMMAND_UNHANDLED, frame);
    } else if (frame.string_equal_insensitive("n")) {
        command = Command(COMMAND_UNHANDLED, frame);
    } else if (frame.string_equal_insensitive("v")) {
        command = Command(COMMAND_UNHANDLED, frame);
    } else if (frame.string_equal_insensitive("z")) {
        command = Command(COMMAND_UNHANDLED, frame);
    } else if (frame.string_equal_insensitive("zz")) {
        command = Command(COMMAND_UNHANDLED, frame);
    } else if (frame.string_equal_insensitive("?")) {
        command = Command(COMMAND_UNHANDLED, frame);
    } else if (frame.string_equal_insensitive("f")) {
        reportedFlags.insert("FilterChanging");
    }
    if (!command.valid())
        return;

    if (!issuedCommand.valid()) {
        issuedCommand = command;
    } else {
        commandBacklog.append(command);
    }
}

std::size_t AcquireThermoMAAP5012::dataFrameStart(std::size_t offset,
                                                  const Util::ByteArray &input) const
{
    auto max = input.size();
    while (offset < max) {
        auto ch = input[offset];
        if (ch == 0x02) {
            if (offset + 1 > max)
                return input.npos;
            return offset + 1;
        }
        if (ch == 0x03) {
            offset += 3;
            continue;
        }
        if (ch == '\r' || ch == '\n') {
            offset++;
            continue;
        }
        return offset;
    }
    return input.npos;
}

std::size_t AcquireThermoMAAP5012::dataFrameEnd(std::size_t start,
                                                const Util::ByteArray &input) const
{
    for (auto max = input.size(); start < max; start++) {
        auto ch = input[start];
        if (ch == '\r' || ch == '\n' || ch == 0x03)
            return start;
    }
    return input.npos;
}

void AcquireThermoMAAP5012::emitNormalizationEnd(double endTime)
{
    if (persistentEgress == NULL)
        return;
    if (!FP::defined(filterLatest.startTime))
        return;

    SequenceValue::Transfer result;
    result.emplace_back(SequenceName({}, "raw", "ZSPOT"), buildNormalizationValue(),
                        filterBegin.startTime, endTime);
    result.emplace_back(SequenceName({}, "raw", "Ff"),
                        Variant::Root(static_cast<qint64>(std::floor(filterBegin.startTime))),
                        filterBegin.startTime, endTime);

    persistentEgress->incomingData(result);
}

SequenceValue::Transfer AcquireThermoMAAP5012::getPersistentValues()
{
    PauseLock paused(*this);
    SequenceValue::Transfer result;

    if (haveAcceptedNormalization && FP::defined(filterBegin.startTime)) {
        result.emplace_back(SequenceName({}, "raw", "ZSPOT"), buildNormalizationValue(),
                            filterBegin.startTime, FP::undefined());

        result.emplace_back(SequenceName({}, "raw", "Ff"),
                            Variant::Root(static_cast<qint64>(std::floor(filterBegin.startTime))),
                            filterBegin.startTime, FP::undefined());
    }

    return result;
}


Variant::Root AcquireThermoMAAP5012::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireThermoMAAP5012::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN_QUERYING:
    case RESP_INTERACTIVE_RUN_QUERYING_PARTIAL:
    case RESP_INTERACTIVE_RUN_QUERYING_COMPLETE:
    case RESP_INTERACTIVE_RUN_WAITING:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}


void AcquireThermoMAAP5012::command(const Variant::Read &command)
{

    if (command.hash("AdvanceSpot").exists()) {
        switch (responseState) {
        case RESP_INTERACTIVE_RUN_QUERYING:
        case RESP_INTERACTIVE_RUN_QUERYING_PARTIAL:
        case RESP_INTERACTIVE_RUN_QUERYING_COMPLETE:
        case RESP_INTERACTIVE_RUN_WAITING: {
            qCDebug(log) << "Issuing spot advance in state" << responseState;

            Variant::Write info = Variant::Write::empty();
            describeState(info);
            info.hash("IssuedCommand").set(issuedCommand.stateDescription());
            event(Time::time(), QObject::tr("Advancing spot on external request."), false, info);

            reportedFlags.insert("FilterChanging");
            writeCommand("F");
            forceRealtimeStateEmit = true;
            forceFilterBreak = true;
            break;
        }

        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
        case RESP_INTERACTIVE_START_STOPREPORTS_INITIAL:
        case RESP_INTERACTIVE_START_STOPREPORTS_FLUSH:
        case RESP_INTERACTIVE_START_SETENGLISH:
        case RESP_INTERACTIVE_START_READVERSION_BEGIN:
        case RESP_INTERACTIVE_START_READVERSION_END:
        case RESP_INTERACTIVE_START_READADDRESS:
        case RESP_INTERACTIVE_START_SETTIME:
        case RESP_INTERACTIVE_START_SETRECORDMINUTES:
        case RESP_INTERACTIVE_START_SETRECORDSECONDS:
        case RESP_INTERACTIVE_START_SETAVERAGING:
        case RESP_INTERACTIVE_START_SETADVANCETR:
        case RESP_INTERACTIVE_START_SETADVANCEHOUR:
        case RESP_INTERACTIVE_START_SETADVANCEATHOUR:
        case RESP_INTERACTIVE_START_SETTARGETFLOW:
        case RESP_INTERACTIVE_START_SETSTANDARDTEMP:
        case RESP_INTERACTIVE_START_SETSTP:
        case RESP_INTERACTIVE_START_SETPARAMETERLIST:
        case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN:
        case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN_RETRY:
        case RESP_INTERACTIVE_START_READPARAMETERS_END:
        case RESP_INTERACTIVE_START_READERRORCOUNTER:
        case RESP_INTERACTIVE_START_FIRSTVALID:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            qCDebug(log) << "Discarding spot advance in state" << responseState;
            break;
        }
    }

}

Variant::Root AcquireThermoMAAP5012::getCommands()
{
    Variant::Root result;

    result["AdvanceSpot"].hash("DisplayName").setString("&Advance Sampling Spot");
    result["AdvanceSpot"].hash("ToolTip")
                         .setString(
                                 "Advance to the next spot.  This advances the filter tape by one spot.");
    result["AdvanceSpot"].hash("Exclude").array(0).hash("Type").setString("Flags");
    result["AdvanceSpot"].hash("Exclude").array(0).hash("Flags").setFlags({"FilterChanging"});
    result["AdvanceSpot"].hash("Exclude").array(0).hash("Variable").hash("F1");

    return result;
}


AcquisitionInterface::AutoprobeStatus AcquireThermoMAAP5012::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireThermoMAAP5012::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_QUERYING:
    case RESP_INTERACTIVE_RUN_QUERYING_PARTIAL:
    case RESP_INTERACTIVE_RUN_QUERYING_COMPLETE:
    case RESP_INTERACTIVE_RUN_WAITING:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS_INITIAL:
    case RESP_INTERACTIVE_START_STOPREPORTS_FLUSH:
    case RESP_INTERACTIVE_START_SETENGLISH:
    case RESP_INTERACTIVE_START_READVERSION_BEGIN:
    case RESP_INTERACTIVE_START_READVERSION_END:
    case RESP_INTERACTIVE_START_READADDRESS:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETRECORDMINUTES:
    case RESP_INTERACTIVE_START_SETRECORDSECONDS:
    case RESP_INTERACTIVE_START_SETAVERAGING:
    case RESP_INTERACTIVE_START_SETADVANCETR:
    case RESP_INTERACTIVE_START_SETADVANCEHOUR:
    case RESP_INTERACTIVE_START_SETADVANCEATHOUR:
    case RESP_INTERACTIVE_START_SETTARGETFLOW:
    case RESP_INTERACTIVE_START_SETSTANDARDTEMP:
    case RESP_INTERACTIVE_START_SETSTP:
    case RESP_INTERACTIVE_START_SETPARAMETERLIST:
    case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN:
    case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN_RETRY:
    case RESP_INTERACTIVE_START_READPARAMETERS_END:
    case RESP_INTERACTIVE_START_READERRORCOUNTER:
    case RESP_INTERACTIVE_START_FIRSTVALID:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        interactiveStartBegin(time);
        generalStatusUpdated();
        break;
    }
}

void AcquireThermoMAAP5012::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    issuedCommand.invalidate();
    commandBacklog.clear();
    discardData(time + 0.5, 1);
    timeoutAt(time + config.first().reportInterval * 2.0 + 5.0);
    generalStatusUpdated();

    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamHit[i] = 0;
        streamOutdated[i] = 0;
    }
}

void AcquireThermoMAAP5012::autoprobePromote(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    Q_ASSERT(!config.isEmpty());
    switch (responseState) {
    case RESP_INTERACTIVE_RUN_QUERYING:
    case RESP_INTERACTIVE_RUN_QUERYING_PARTIAL:
    case RESP_INTERACTIVE_RUN_QUERYING_COMPLETE:
        qCDebug(log) << "Promoted from interactive query state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + config.first().reportInterval);
        break;

    case RESP_INTERACTIVE_RUN_WAITING:
        qCDebug(log) << "Promoted from interactive record wait to interactive acquisition at"
                     << Logging::time(time);

        timeoutAt(time + config.first().reportInterval);
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS_INITIAL:
    case RESP_INTERACTIVE_START_STOPREPORTS_FLUSH:
    case RESP_INTERACTIVE_START_SETENGLISH:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETRECORDMINUTES:
    case RESP_INTERACTIVE_START_SETRECORDSECONDS:
    case RESP_INTERACTIVE_START_SETAVERAGING:
    case RESP_INTERACTIVE_START_SETADVANCETR:
    case RESP_INTERACTIVE_START_SETADVANCEHOUR:
    case RESP_INTERACTIVE_START_SETADVANCEATHOUR:
    case RESP_INTERACTIVE_START_SETTARGETFLOW:
    case RESP_INTERACTIVE_START_SETSTANDARDTEMP:
    case RESP_INTERACTIVE_START_SETSTP:
    case RESP_INTERACTIVE_START_SETPARAMETERLIST:
        qCDebug(log) << "Promoted from interactive startup state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 30.0);
        break;

    case RESP_INTERACTIVE_START_READVERSION_BEGIN:
    case RESP_INTERACTIVE_START_READVERSION_END:
        qCDebug(log) << "Promoted from interactive startup state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 5.0);
        break;

    case RESP_INTERACTIVE_START_READADDRESS:
    case RESP_INTERACTIVE_START_READERRORCOUNTER:
        qCDebug(log) << "Promoted from interactive startup state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 3.0);
        break;

    case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN:
    case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN_RETRY:
    case RESP_INTERACTIVE_START_READPARAMETERS_END:
        qCDebug(log) << "Promoted from interactive startup state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 30.0);
        break;

    case RESP_INTERACTIVE_START_FIRSTVALID:
        qCDebug(log) << "Promoted from interactive startup state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + config.first().reportInterval);
        break;

    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        interactiveStartBegin(time);
        generalStatusUpdated();
        break;
    }
}

void AcquireThermoMAAP5012::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    timeoutAt(time + config.first().reportInterval);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_RUN_QUERYING:
    case RESP_INTERACTIVE_RUN_QUERYING_PARTIAL:
    case RESP_INTERACTIVE_RUN_QUERYING_COMPLETE:
    case RESP_INTERACTIVE_RUN_WAITING:
        /* Already in unpolled, so don't reset timeout */
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from unpolled interactive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        for (int i = 0; i < LogStream_TOTAL; i++) {
            streamHit[i] = 0;
            streamOutdated[i] = 0;
        }

        discardData(FP::undefined());
        generalStatusUpdated();
        break;
    }
}

static const quint16 serializedStateVersion = 1;

void AcquireThermoMAAP5012::serializeState(QDataStream &stream)
{
    PauseLock paused(*this);

    stream << serializedStateVersion;

    stream << reportedInstrumentVolume << filterLatest.startTime << filterSerialNumber
           << filterFirmwareVersion << filterAddress << accumulateVolume << filterBegin;
}

void AcquireThermoMAAP5012::deserializeState(QDataStream &stream)
{
    quint16 version = 0;
    stream >> version;
    if (version != serializedStateVersion)
        return;

    PauseLock paused(*this);

    stream >> restoreInstrumentVolume >> restoreTime >> filterSerialNumber >> filterFirmwareVersion
           >> filterAddress >> accumulateVolume >> filterBegin;
    haveAcceptedNormalization = false;
}

AcquisitionInterface::AutomaticDefaults AcquireThermoMAAP5012::getDefaults()
{
    AutomaticDefaults result;
    result.name = "A$1$2";
    result.interface["Baud"].setInt64(9600);
    result.interface["Parity"].setString("Even");
    result.interface["DataBits"].setInt64(7);
    result.interface["StopBits"].setInt64(2);
    return result;
}


ComponentOptions AcquireThermoMAAP5012Component::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);
    return options;
}

ComponentOptions AcquireThermoMAAP5012Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireThermoMAAP5012Component::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples(true));
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireThermoMAAP5012Component::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireThermoMAAP5012Component::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireThermoMAAP5012Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireThermoMAAP5012Component::createAcquisitionPassive(const ComponentOptions &options,
                                                                                       const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(
            new AcquireThermoMAAP5012(options, loggingContext));
}

std::unique_ptr<
        AcquisitionInterface> AcquireThermoMAAP5012Component::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireThermoMAAP5012(config, loggingContext)); }

std::unique_ptr<AcquisitionInterface> AcquireThermoMAAP5012Component::createAcquisitionAutoprobe(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireThermoMAAP5012> i(new AcquireThermoMAAP5012(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireThermoMAAP5012Component::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireThermoMAAP5012> i(new AcquireThermoMAAP5012(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
