/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QDir>
#include <QFile>
#include <QHostInfo>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/actioncomponent.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;
    QString tmpPath;
    QDir tmpDir;

    ActionComponentTime *component;

    void recursiveRemove(const QDir &base)
    {
        if (base.path().isEmpty() || base == QDir::current())
            return;
        QFileInfoList list(base.entryInfoList(QDir::Files | QDir::NoDotAndDotDot));
        for (QFileInfoList::const_iterator rm = list.constBegin(), endRm = list.constEnd();
                rm != endRm;
                ++rm) {
            if (!QFile::remove(rm->absoluteFilePath())) {
                qDebug() << "Failed to remove file" << rm->absoluteFilePath();
                continue;
            }
        }
        list = base.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);
        for (QFileInfoList::const_iterator rm = list.constBegin(), endRm = list.constEnd();
                rm != endRm;
                ++rm) {
            QDir subDir(base);
            if (!subDir.cd(rm->fileName())) {
                qDebug() << "Failed to change directory" << rm->absoluteFilePath();
                continue;
            }
            recursiveRemove(subDir);
            base.rmdir(rm->fileName());
        }
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ActionComponentTime *>(ComponentLoader::create("output_upload"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());

        tmpDir = QDir(QDir::tempPath());
        tmpPath = "CPD3UploadTest-";
        tmpPath.append(Random::string());
        tmpDir.mkdir(tmpPath);
        QVERIFY(tmpDir.cd(tmpPath));
    }

    void cleanupTestCase()
    {
        recursiveRemove(tmpDir);
        tmpDir.cdUp();
        tmpDir.rmdir(tmpPath);
    }

    void init()
    {
        recursiveRemove(tmpDir);
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("profile")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("timeout")));
        QVERIFY(qobject_cast<ComponentOptionSingleTime *>(options.get("after")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("single")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("discard")));
    }

#ifdef Q_OS_UNIX

    void basic()
    {
        tmpDir.mkdir("retry");
        tmpDir.mkdir("archive");
        tmpDir.mkdir("transferred");

        Variant::Root cfg;

        cfg["Profiles/aerosol/Transfer/Batch"] = true;
        cfg["Profiles/aerosol/Archive/RetryDirectory"] =
                QString("%1/%2").arg(tmpDir.absolutePath(), "retry");
        cfg["Profiles/aerosol/Archive/Completed"] =
                QString("%1/%2/${FILE}").arg(tmpDir.absolutePath(), "archive");
        cfg["Profiles/aerosol/Transfer/Type"] = "Copy";
        cfg["Profiles/aerosol/Transfer/Local/Target"] =
                QString("%1/%2/${FILE}").arg(tmpDir.absolutePath(), "transferred");


        cfg["Profiles/aerosol/Generate/#0/Command/Program"] = "sh";
        cfg["Profiles/aerosol/Generate/#0/Command/Arguments/#0"] = "-c";
        cfg["Profiles/aerosol/Generate/#0/Command/Arguments/#1"] =
                "/bin/echo -n ${START|ISO} > ${PROFILE}.1";

        cfg["Profiles/aerosol/Generate/#1/Command/Program"] = "sh";
        cfg["Profiles/aerosol/Generate/#1/Command/Arguments/#0"] = "-c";
        cfg["Profiles/aerosol/Generate/#1/Command/Arguments/#1"] =
                "/bin/echo -n ${END|ISO} > ${PROFILE}.2";

        Archive::Access(databaseFile).writeSynchronous(
                SequenceValue::Transfer{SequenceValue({"brw", "configuration", "upload"}, cfg)});

        ComponentOptions options(component->getOptions());
        CPD3Action *action =
                component->createTimeAction(options, 1388534400, 1388620800, {"brw"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait(30000));
        delete action;

        QFile f1(tmpDir.filePath("transferred/aerosol.1"));
        QVERIFY(f1.open(QIODevice::ReadOnly));
        QCOMPARE(f1.readAll(), QByteArray("2014-01-01T00:00:00Z"));

        QFile f2(tmpDir.filePath("transferred/aerosol.2"));
        QVERIFY(f2.open(QIODevice::ReadOnly));
        QCOMPARE(f2.readAll(), QByteArray("2014-01-02T00:00:00Z"));

        QFile f3(tmpDir.filePath("archive/aerosol.1"));
        QVERIFY(f3.open(QIODevice::ReadOnly));
        QCOMPARE(f3.readAll(), QByteArray("2014-01-01T00:00:00Z"));

        QFile f4(tmpDir.filePath("archive/aerosol.2"));
        QVERIFY(f4.open(QIODevice::ReadOnly));
        QCOMPARE(f4.readAll(), QByteArray("2014-01-02T00:00:00Z"));

        QVERIFY(QDir(tmpDir.filePath("retry")).entryInfoList(QDir::Files | QDir::NoDotAndDotDot)
                                              .isEmpty());
    }

    void retryFailed()
    {
        tmpDir.mkdir("retry");
        tmpDir.mkdir("archive");
        QVERIFY(!tmpDir.exists("transferred"));

        Variant::Root cfg;

        cfg["Profiles/aerosol/Transfer/Batch"] = true;
        cfg["Profiles/aerosol/Archive/RetryDirectory"] =
                QString("%1/%2").arg(tmpDir.absolutePath(), "retry");
        cfg["Profiles/aerosol/Archive/Completed"] =
                QString("%1/%2/${FILE}").arg(tmpDir.absolutePath(), "archive");
        cfg["Profiles/aerosol/Transfer/Type"] = "Copy";
        cfg["Profiles/aerosol/Transfer/Local/CreateDirectory"] = false;
        cfg["Profiles/aerosol/Transfer/Local/Target"] =
                QString("%1/%2/${FILE}").arg(tmpDir.absolutePath(), "transferred");


        cfg["Profiles/aerosol/Generate/#0/Command/Program"] = "sh";
        cfg["Profiles/aerosol/Generate/#0/Command/Arguments/#0"] = "-c";
        cfg["Profiles/aerosol/Generate/#0/Command/Arguments/#1"] =
                "/bin/echo -n ${START|ISO} > ${PROFILE}.1";

        cfg["Profiles/aerosol/Generate/#1/Command/Program"] = "sh";
        cfg["Profiles/aerosol/Generate/#1/Command/Arguments/#0"] = "-c";
        cfg["Profiles/aerosol/Generate/#1/Command/Arguments/#1"] =
                "/bin/echo -n ${END|ISO} > ${PROFILE}.2";

        Archive::Access(databaseFile).writeSynchronous(
                SequenceValue::Transfer{SequenceValue({"brw", "configuration", "upload"}, cfg)});


        ComponentOptions options(component->getOptions());
        CPD3Action *action =
                component->createTimeAction(options, 1388534400, 1388620800, {"brw"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait(30000));
        delete action;

        QVERIFY(!QDir(tmpDir.filePath("retry")).entryInfoList(QDir::Files | QDir::NoDotAndDotDot)
                                               .isEmpty());
        QVERIFY(QDir(tmpDir.filePath("archive")).entryInfoList(QDir::Files | QDir::NoDotAndDotDot)
                                                .isEmpty());


        tmpDir.mkdir("transferred");

        cfg["Profiles/aerosol/Generate"].setEmpty();
        Archive::Access(databaseFile).writeSynchronous(
                SequenceValue::Transfer{SequenceValue({"brw", "configuration", "upload"}, cfg)});

        action = component->createTimeAction(options, 1388534401, 1388620801, {"brw"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait(30000));
        delete action;

        QFile f1(tmpDir.filePath("transferred/aerosol.1"));
        QVERIFY(f1.open(QIODevice::ReadOnly));
        QCOMPARE(f1.readAll(), QByteArray("2014-01-01T00:00:00Z"));

        QFile f2(tmpDir.filePath("transferred/aerosol.2"));
        QVERIFY(f2.open(QIODevice::ReadOnly));
        QCOMPARE(f2.readAll(), QByteArray("2014-01-02T00:00:00Z"));

        QFile f3(tmpDir.filePath("archive/aerosol.1"));
        QVERIFY(f3.open(QIODevice::ReadOnly));
        QCOMPARE(f3.readAll(), QByteArray("2014-01-01T00:00:00Z"));

        QFile f4(tmpDir.filePath("archive/aerosol.2"));
        QVERIFY(f4.open(QIODevice::ReadOnly));
        QCOMPARE(f4.readAll(), QByteArray("2014-01-02T00:00:00Z"));


        QVERIFY(QDir(tmpDir.filePath("retry")).entryInfoList(QDir::Files | QDir::NoDotAndDotDot)
                                              .isEmpty());

    }

    void latest()
    {
        tmpDir.mkdir("transferred");

        Variant::Root cfg;

        cfg["Profiles/aerosol/Transfer/Batch"] = true;
        cfg["Profiles/aerosol/Transfer/Type"] = "Copy";
        cfg["Profiles/aerosol/Transfer/Local/Target"] =
                QString("%1/%2/${FILE}").arg(tmpDir.absolutePath(), "transferred");

        cfg["Profiles/aerosol/Merge/Units"] = "Minute";
        cfg["Profiles/aerosol/Merge/Count"] = 5;
        cfg["Profiles/aerosol/LatestOnly"] = true;
        cfg["Profiles/aerosol/DisableFill"] = true;
        cfg["Profiles/aerosol/Trigger/Variable"] = "passed";
        cfg["Profiles/aerosol/Interval/Units"] = "Minute";
        cfg["Profiles/aerosol/Interval/Count"] = 1;

        cfg["Profiles/aerosol/Generate/#0/Command/Program"] = "sh";
        cfg["Profiles/aerosol/Generate/#0/Command/Arguments/#0"] = "-c";
        cfg["Profiles/aerosol/Generate/#0/Command/Arguments/#1"] =
                "/bin/echo -n ${START|ISO} >> ${PROFILE}.1";

        Archive::Access(databaseFile).writeSynchronous(
                SequenceValue::Transfer{SequenceValue({"brw", "configuration", "upload"}, cfg)});

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createTimeAction(options, {"brw"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait(30000));
        delete action;

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 0);

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "passed"}, Variant::Root(true), 1420106400,
                              1420107000)});

        options = component->getOptions();
        action = component->createTimeAction(options, {"brw"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait(30000));
        delete action;


        {
            QFile f(tmpDir.filePath("transferred/aerosol.1"));
            QVERIFY(f.open(QIODevice::ReadOnly));
            QCOMPARE(f.readAll(), QByteArray("2015-01-01T10:00:00Z"));
            f.remove();
        }


        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "passed"}, Variant::Root(true), 1420106400,
                              1420107001),
                SequenceValue({"brw", "raw", "passed"}, Variant::Root(true), 1420124400,
                              1420125000)});

        options = component->getOptions();
        action = component->createTimeAction(options, {"brw"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait(30000));
        delete action;

        {
            QFile f(tmpDir.filePath("transferred/aerosol.1"));
            QVERIFY(f.open(QIODevice::ReadOnly));
            QCOMPARE(f.readAll(), QByteArray("2015-01-01T15:00:00Z"));
            f.remove();
        }
    }

    void manualSplit()
    {
        tmpDir.mkdir("transferred");

        Variant::Root cfg;

        cfg["Profiles/aerosol/Transfer/Batch"] = true;
        cfg["Profiles/aerosol/Transfer/Type"] = "Copy";
        cfg["Profiles/aerosol/Transfer/Local/Target"] =
                QString("%1/%2/${FILE}").arg(tmpDir.absolutePath(), "transferred");

        cfg["Profiles/aerosol/Interval/Unit"] = "Year";
        cfg["Profiles/aerosol/Interval/Count"] = 1;
        cfg["Profiles/aerosol/Interval/Align"] = true;

        cfg["Profiles/aerosol/Generate/#0/Command/Program"] = "sh";
        cfg["Profiles/aerosol/Generate/#0/Command/Arguments/#0"] = "-c";
        cfg["Profiles/aerosol/Generate/#0/Command/Arguments/#1"] =
                "/bin/echo -n '${START|ISO} ${END|ISO}' > ${PROFILE}.${START|YEAR}";

        Archive::Access(databaseFile).writeSynchronous(
                SequenceValue::Transfer{SequenceValue({"brw", "configuration", "upload"}, cfg)});

        ComponentOptions options(component->getOptions());
        CPD3Action *action =
                component->createTimeAction(options, 1388534401, 1451606400, {"brw"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait(30000));
        delete action;

        QFile f1(tmpDir.filePath("transferred/aerosol.2014"));
        QVERIFY(f1.open(QIODevice::ReadOnly));
        QCOMPARE(f1.readAll(), QByteArray("2014-01-01T00:00:00Z 2015-01-01T00:00:00Z"));

        QFile f2(tmpDir.filePath("transferred/aerosol.2015"));
        QVERIFY(f2.open(QIODevice::ReadOnly));
        QCOMPARE(f2.readAll(), QByteArray("2015-01-01T00:00:00Z 2016-01-01T00:00:00Z"));
    }

    void manualSingle()
    {
        tmpDir.mkdir("transferred");

        Variant::Root cfg;

        cfg["Profiles/aerosol/Transfer/Batch"] = true;
        cfg["Profiles/aerosol/Transfer/Type"] = "Copy";
        cfg["Profiles/aerosol/Transfer/Local/Target"] =
                QString("%1/%2/${FILE}").arg(tmpDir.absolutePath(), "transferred");

        cfg["Profiles/aerosol/Interval/Unit"] = "Year";
        cfg["Profiles/aerosol/Interval/Count"] = 1;
        cfg["Profiles/aerosol/Interval/Align"] = true;

        cfg["Profiles/aerosol/Generate/#0/Command/Program"] = "sh";
        cfg["Profiles/aerosol/Generate/#0/Command/Arguments/#0"] = "-c";
        cfg["Profiles/aerosol/Generate/#0/Command/Arguments/#1"] =
                "/bin/echo -n '${START|ISO} ${END|ISO}' > ${PROFILE}.${START|YEAR}";

        Archive::Access(databaseFile).writeSynchronous(
                SequenceValue::Transfer{SequenceValue({"brw", "configuration", "upload"}, cfg)});

        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionBoolean *>(options.get("single"))->set(true);
        CPD3Action *action =
                component->createTimeAction(options, 1388534401, 1451606400, {"brw"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait(30000));
        delete action;

        QFile f1(tmpDir.filePath("transferred/aerosol.2014"));
        QVERIFY(f1.open(QIODevice::ReadOnly));
        QCOMPARE(f1.readAll(), QByteArray("2014-01-01T00:00:01Z 2016-01-01T00:00:00Z"));
    }

    void manualIntervalSplit()
    {
        tmpDir.mkdir("transferred");

        Variant::Root cfg;

        cfg["Profiles/aerosol/Transfer/Batch"] = true;
        cfg["Profiles/aerosol/Transfer/Type"] = "Copy";
        cfg["Profiles/aerosol/Transfer/Local/Target"] =
                QString("%1/%2/${FILE}").arg(tmpDir.absolutePath(), "transferred");

        cfg["Profiles/aerosol/Interval/Unit"] = "Year";
        cfg["Profiles/aerosol/Interval/Count"] = 1;
        cfg["Profiles/aerosol/Interval/Align"] = true;
        cfg["Profiles/aerosol/Split/Unit"] = "Quarter";
        cfg["Profiles/aerosol/Split/Count"] = 1;
        cfg["Profiles/aerosol/Split/Align"] = true;

        cfg["Profiles/aerosol/Generate/#0/Command/Program"] = "sh";
        cfg["Profiles/aerosol/Generate/#0/Command/Arguments/#0"] = "-c";
        cfg["Profiles/aerosol/Generate/#0/Command/Arguments/#1"] =
                "/bin/echo -n '${START|ISO} ${END|ISO}' > ${PROFILE}.${START|YEAR}.${START|MONTH}";

        Archive::Access(databaseFile).writeSynchronous(
                SequenceValue::Transfer{SequenceValue({"brw", "configuration", "upload"}, cfg)});

        ComponentOptions options(component->getOptions());
        std::unique_ptr<CPD3Action> action
                (component->createTimeAction(options, 1388534401, 1398902400, {"brw"}));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        {
            QFile f(tmpDir.filePath("transferred/aerosol.2014.01"));
            QVERIFY(f.open(QIODevice::ReadOnly));
            QCOMPARE(f.readAll(), QByteArray("2014-01-01T00:00:00Z 2014-04-01T00:00:00Z"));
        }

        {
            QFile f(tmpDir.filePath("transferred/aerosol.2014.04"));
            QVERIFY(f.open(QIODevice::ReadOnly));
            QCOMPARE(f.readAll(), QByteArray("2014-04-01T00:00:00Z 2014-07-01T00:00:00Z"));
        }

        {
            QFile f(tmpDir.filePath("transferred/aerosol.2014.07"));
            QVERIFY(f.open(QIODevice::ReadOnly));
            QCOMPARE(f.readAll(), QByteArray("2014-07-01T00:00:00Z 2014-10-01T00:00:00Z"));
        }

        {
            QFile f(tmpDir.filePath("transferred/aerosol.2014.10"));
            QVERIFY(f.open(QIODevice::ReadOnly));
            QCOMPARE(f.readAll(), QByteArray("2014-10-01T00:00:00Z 2015-01-01T00:00:00Z"));
        }
    }

    void intervalSplit()
    {
        tmpDir.mkdir("transferred");

        Variant::Root cfg;

        cfg["Profiles/aerosol/Transfer/Batch"] = true;
        cfg["Profiles/aerosol/Transfer/Type"] = "Copy";
        cfg["Profiles/aerosol/Transfer/Local/Target"] =
                QString("%1/%2/${FILE}").arg(tmpDir.absolutePath(), "transferred");

        cfg["Profiles/aerosol/Trigger/Variable"] = "passed";
        cfg["Profiles/aerosol/AllowIncomplete"] = true;
        cfg["Profiles/aerosol/Interval/Unit"] = "Year";
        cfg["Profiles/aerosol/Interval/Count"] = 1;
        cfg["Profiles/aerosol/Interval/Align"] = true;
        cfg["Profiles/aerosol/Split/Unit"] = "Quarter";
        cfg["Profiles/aerosol/Split/Count"] = 1;
        cfg["Profiles/aerosol/Split/Align"] = true;

        cfg["Profiles/aerosol/Generate/#0/Command/Program"] = "sh";
        cfg["Profiles/aerosol/Generate/#0/Command/Arguments/#0"] = "-c";
        cfg["Profiles/aerosol/Generate/#0/Command/Arguments/#1"] =
                "/bin/echo -n '${START|ISO} ${END|ISO}' > ${PROFILE}.${START|YEAR}.${START|MONTH}";

        Archive::Access(databaseFile).writeSynchronous(
                SequenceValue::Transfer{SequenceValue({"brw", "configuration", "upload"}, cfg),
                                        SequenceValue({"brw", "raw", "passed"}, Variant::Root(true),
                                                      1388534401, 1398902400),});

        ComponentOptions options(component->getOptions());
        std::unique_ptr<CPD3Action>
                action(component->createTimeAction(options, {"brw"}));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        {
            QFile f(tmpDir.filePath("transferred/aerosol.2014.01"));
            QVERIFY(f.open(QIODevice::ReadOnly));
            QCOMPARE(f.readAll(), QByteArray("2014-01-01T00:00:00Z 2014-04-01T00:00:00Z"));
        }

        {
            QFile f(tmpDir.filePath("transferred/aerosol.2014.04"));
            QVERIFY(f.open(QIODevice::ReadOnly));
            QCOMPARE(f.readAll(), QByteArray("2014-04-01T00:00:00Z 2014-07-01T00:00:00Z"));
        }

        {
            QFile f(tmpDir.filePath("transferred/aerosol.2014.07"));
            QVERIFY(f.open(QIODevice::ReadOnly));
            QCOMPARE(f.readAll(), QByteArray("2014-07-01T00:00:00Z 2014-10-01T00:00:00Z"));
        }

        {
            QFile f(tmpDir.filePath("transferred/aerosol.2014.10"));
            QVERIFY(f.open(QIODevice::ReadOnly));
            QCOMPARE(f.readAll(), QByteArray("2014-10-01T00:00:00Z 2015-01-01T00:00:00Z"));
        }
    }

#endif

    void updated()
    {
        tmpDir.mkdir("transferred");

        Variant::Root cfg;

        cfg["Profiles/aerosol/Transfer/Batch"] = false;
        cfg["Profiles/aerosol/Transfer/Type"] = "Copy";
        cfg["Profiles/aerosol/Transfer/Local/Target"] =
                QString("%1/%2/${START}.${END}").arg(tmpDir.absolutePath(), "transferred");
        cfg["Profiles/aerosol/Transfer/AbortOnFailure"] = true;

        cfg["Profiles/aerosol/Trigger/Variable"] = "T_S11";
        cfg["Profiles/aerosol/Interval/Unit"] = "Day";
        cfg["Profiles/aerosol/Interval/Count"] = 1;
        cfg["Profiles/aerosol/Interval/Align"] = true;

        cfg["Profiles/aerosol/Generate/#0/Component/Name"] = "output_ebas";

        Variant::Root ebas;

        ebas["Profiles/aerosol/Test_L0/Types/TL0/Variables/T/Data/Input/Variable"] = "T_S11";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Variables/T/Data/Input/Archive"] = "raw";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Variables/T/Format"] = "000.00";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Variables/T/MVC"] = "999.99";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Variables/T/Header/Title/Full"] = "Temperature";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Variables/T/Header/Column"] = "T_int";

        ebas["Profiles/aerosol/Test_L0/Types/TL0/Filename"] = "EBASOutput";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Originator"] = "Ogren, John";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Organization"] = "NOAA";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Submitter"] = "Ogren, John";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Projects"] = "GAW-WDCA";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Interval"] = "0";

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "configuration", "upload"}, cfg, FP::undefined(),
                              FP::undefined()),
                SequenceValue({"brw", "configuration", "ebas"}, ebas, FP::undefined(),
                              FP::undefined()),
                SequenceValue({"brw", "raw", "T_S11"}, Variant::Root(1.0), 1388534400, 1388538000),
                SequenceValue({"brw", "raw", "T_S11"}, Variant::Root(1.1), 1388538000, 1388620800),
                SequenceValue({"brw", "raw", "T_S11"}, Variant::Root(2.0), 1388620800,
                              1388624400)});


        ComponentOptions options(component->getOptions());
        std::unique_ptr<CPD3Action>
                action(component->createTimeAction(options, {"brw"}));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 1);
        QVERIFY(QFile::exists(
                QString("%1/%2/20140101T000000Z.20140102T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));
        QVERIFY(QFile::remove(
                QString("%1/%2/20140101T000000Z.20140102T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));


        action.reset(component->createTimeAction(options, {"brw"}));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 0);


        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "T_S11"}, Variant::Root(1.3), 1388534400,
                              1388538000)});

        action.reset(component->createTimeAction(options, {"brw"}));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 1);
        QVERIFY(QFile::exists(
                QString("%1/%2/20140101T000000Z.20140102T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));
        QVERIFY(QFile::remove(
                QString("%1/%2/20140101T000000Z.20140102T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));


        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "T_S11"}, Variant::Root(2.1), 1388620800,
                              1388624400)});

        action.reset(component->createTimeAction(options, {"brw"}));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 0);


        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "T_S11"}, Variant::Root(1.4), 1388534400, 1388538000),
                SequenceValue({"brw", "raw", "T_S11"}, Variant::Root(2.2), 1388624400, 1388707200),
                SequenceValue({"brw", "raw", "T_S11"}, Variant::Root(3.0), 1388707200,
                              1388707201)});

        action.reset(component->createTimeAction(options, {"brw"}));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 2);
        QVERIFY(QFile::exists(
                QString("%1/%2/20140101T000000Z.20140102T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));
        QVERIFY(QFile::exists(
                QString("%1/%2/20140102T000000Z.20140103T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));
        QVERIFY(QFile::remove(
                QString("%1/%2/20140101T000000Z.20140102T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));
        QVERIFY(QFile::remove(
                QString("%1/%2/20140102T000000Z.20140103T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));

        action.reset(component->createTimeAction(options, {"brw"}));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 0);


        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "T_S11"}, Variant::Root(2.3), 1388624400,
                              1388707200)});

        action.reset(component->createTimeAction(options, {"brw"}));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 1);
        QVERIFY(QFile::exists(
                QString("%1/%2/20140102T000000Z.20140103T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));
        QVERIFY(QFile::remove(
                QString("%1/%2/20140102T000000Z.20140103T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));


        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "T_S11"}, Variant::Root(9.0), 1388534400,
                              1388707200)});

        action.reset(component->createTimeAction(options, {"brw"}));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 2);
        QVERIFY(QFile::exists(
                QString("%1/%2/20140101T000000Z.20140102T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));
        QVERIFY(QFile::exists(
                QString("%1/%2/20140102T000000Z.20140103T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));
        QVERIFY(QFile::remove(
                QString("%1/%2/20140101T000000Z.20140102T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));
        QVERIFY(QFile::remove(
                QString("%1/%2/20140102T000000Z.20140103T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));


        qobject_cast<ComponentOptionSingleTime *>(options.get("after"))->set(FP::undefined());
        action.reset(component->createTimeAction(options));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait());
        action.reset();

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 2);
        QVERIFY(QFile::exists(
                QString("%1/%2/20140101T000000Z.20140102T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));
        QVERIFY(QFile::exists(
                QString("%1/%2/20140102T000000Z.20140103T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));
        QVERIFY(QFile::remove(
                QString("%1/%2/20140101T000000Z.20140102T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));
        QVERIFY(QFile::remove(
                QString("%1/%2/20140102T000000Z.20140103T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));

        options = component->getOptions();
        action.reset(component->createTimeAction(options));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 0);


        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "T_S11"}, Variant::Root(10.0), 1388707201, 1388707202),
                SequenceValue({"brw", "raw", "T_S11"}, Variant::Root(10.0), 1388793601,
                              1388793602)});

        options = component->getOptions();
        action.reset(component->createTimeAction(options));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 1);
        QVERIFY(QFile::exists(
                QString("%1/%2/20140103T000000Z.20140104T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));
        QVERIFY(QFile::remove(
                QString("%1/%2/20140103T000000Z.20140104T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));


        options = component->getOptions();
        action.reset(component->createTimeAction(options));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 0);
    }

    void backUpdate()
    {
        tmpDir.mkdir("transferred");

        Variant::Root cfg;

        cfg["Profiles/aerosol/Transfer/Batch"] = false;
        cfg["Profiles/aerosol/Transfer/Type"] = "Copy";
        cfg["Profiles/aerosol/Transfer/Local/Target"] =
                QString("%1/%2/${START}.${END}").arg(tmpDir.absolutePath(), "transferred");
        cfg["Profiles/aerosol/Transfer/AbortOnFailure"] = true;

        cfg["Profiles/aerosol/Trigger/Variable"] = "T_S11";
        cfg["Profiles/aerosol/Interval/Unit"] = "Day";
        cfg["Profiles/aerosol/Interval/Count"] = 1;
        cfg["Profiles/aerosol/Interval/Align"] = true;
        cfg["Profiles/aerosol/AllowIncomplete"] = true;

        cfg["Profiles/aerosol/Generate/#0/Component/Name"] = "output_ebas";

        Variant::Root ebas;

        ebas["Profiles/aerosol/Test_L0/Types/TL0/Variables/T/Data/Input/Variable"] = "T_S11";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Variables/T/Data/Input/Archive"] = "raw";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Variables/T/Format"] = "000.00";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Variables/T/MVC"] = "999.99";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Variables/T/Header/Title/Full"] = "Temperature";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Variables/T/Header/Column"] = "T_int";

        ebas["Profiles/aerosol/Test_L0/Types/TL0/Filename"] = "EBASOutput";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Originator"] = "Ogren, John";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Organization"] = "NOAA";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Submitter"] = "Ogren, John";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Projects"] = "GAW-WDCA";
        ebas["Profiles/aerosol/Test_L0/Types/TL0/Interval"] = "0";

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "configuration", "upload"}, cfg, FP::undefined(),
                              FP::undefined()),
                SequenceValue({"brw", "configuration", "ebas"}, ebas, FP::undefined(),
                              FP::undefined())});

        auto options = component->getOptions();
        std::unique_ptr<CPD3Action> action(component->createTimeAction(options));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 0);


        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "T_S11"}, Variant::Root(1.0), 1388534400,
                              1388534460)});

        QTest::qSleep(100);

        options = component->getOptions();
        action.reset(component->createTimeAction(options));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 1);
        QVERIFY(QFile::exists(
                QString("%1/%2/20140101T000000Z.20140102T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));
        QVERIFY(QFile::remove(
                QString("%1/%2/20140101T000000Z.20140102T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));

        auto now = Time::time();

        QTest::qSleep(100);

        options = component->getOptions();
        action.reset(component->createTimeAction(options));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 0);

        Archive::Access(databaseFile).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue({"brw", "raw", "T_S11"}, Variant::Root(2.0), 1388620800, 1388620860, 0,
                             now, false)});

        options = component->getOptions();
        action.reset(component->createTimeAction(options));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 1);
        QVERIFY(QFile::exists(
                QString("%1/%2/20140102T000000Z.20140103T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));
        QVERIFY(QFile::remove(
                QString("%1/%2/20140102T000000Z.20140103T000000Z").arg(tmpDir.absolutePath(),
                                                                       "transferred")));

        options = component->getOptions();
        action.reset(component->createTimeAction(options));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait(30000));
        action.reset();

        QCOMPARE(QDir(tmpDir.filePath("transferred")).entryInfoList(
                QDir::Files | QDir::NoDotAndDotDot).size(), 0);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
