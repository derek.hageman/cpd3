/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef OUTPUTUpload_H
#define OUTPUTUpload_H

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>
#include <QHash>
#include <QDir>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/timeutils.hxx"
#include "core/textsubstitution.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/segment.hxx"
#include "transfer/upload.hxx"
#include "core/threading.hxx"

class OutputUpload;

class Uploader : public CPD3::Transfer::FileUploader {
    OutputUpload *parent;
public:
    Uploader(const CPD3::Data::Variant::Read &config, OutputUpload *parent);

    virtual ~Uploader();

protected:
    virtual QString applySubstitutions(const QString &input) const;

    virtual void pushTemporaryOutput(const QFileInfo &fileName);

    virtual void popTemporaryOutput();

    virtual void pushInputFile(const QFileInfo &file);

    virtual void popInputFile();
};

class OutputUpload : public CPD3::CPD3Action {
Q_OBJECT

    friend class Uploader;

    bool lockingMode;
    bool discardOutput;
    bool singleUpload;

    std::string profile;
    double lockTimeout;
    std::vector<CPD3::Data::SequenceName::Component> stations;
    double start;
    double end;

    bool haveAfter;
    double afterTime;

    bool terminated;
    std::mutex mutex;

    CPD3::Threading::Signal<> terminateRequested;

    CPD3::TextSubstitutionStack substitutions;

    OutputUpload();

    bool testTerminated();

    bool invokeCommand(const QString &program, const QStringList &arguments);

    bool executeAction(CPD3::CPD3Action *action);

    void setOptions(const CPD3::Data::Variant::Read &generator,
                    const QDir &targetDirectory,
                    CPD3::ComponentOptions &options);

    QDir createTemporaryDirectory();

    bool generateFiles(const CPD3::Data::SequenceName::Component &station,
                       const CPD3::Data::Variant::Read &config,
                       double start,
                       double end,
                       const QDir &targetDirectory);

    bool uploadFiles(const CPD3::Data::Variant::Read &config, const QList<QFile *> &files);

    bool uploadFile(const CPD3::Data::Variant::Read &config, QFile *file);

    bool completeFiles(const CPD3::Data::Variant::Read &config, const QList<QFile *> &files);

    bool completeFile(const CPD3::Data::Variant::Read &config, QFile *file);

    bool failFiles(const CPD3::Data::Variant::Read &config,
                   const QList<QFile *> &files,
                   const QString &retryLocation);

    bool failFile(const CPD3::Data::Variant::Read &config,
                  QFile *file,
                  const QString &retryLocation);

    bool completeUpload(const CPD3::Data::Variant::Read &config,
                        QList<QFile *> filesToUpload = QList<QFile *>());

    bool uploadFiles(const CPD3::Data::Variant::Read &config, const QDir &sourceDirectory);

    bool performUpload(const CPD3::Data::SequenceName::Component &station,
                       const CPD3::Data::Variant::Read &config,
                       double start,
                       double end);

    void splitRanges(QList<CPD3::Time::Bounds> &result,
                     const CPD3::Data::ValueSegment::Transfer &config);

    QList<CPD3::Time::Bounds> buildUploadRanges(const CPD3::Data::SequenceName::Component &station,
                                                const CPD3::Data::ValueSegment::Transfer &config,
                                                double lastRunTime,
                                                double &dataModified,
                                                const QList<CPD3::Time::Bounds> &completed);

    QList<CPD3::Time::Bounds> buildManualRanges(const CPD3::Data::ValueSegment::Transfer &config);

public:
    OutputUpload(const CPD3::ComponentOptions &options,
                 double start,
                 double end,
                 const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    OutputUpload(const CPD3::ComponentOptions &options,
                 const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~OutputUpload();

    virtual void signalTerminate();

protected:
    virtual void run();
};

class OutputUploadComponent : public QObject, virtual public CPD3::ActionComponentTime {
Q_OBJECT
    Q_INTERFACES(CPD3::ActionComponentTime)

    Q_PLUGIN_METADATA(IID
                              "CPD3.output_upload"
                              FILE
                              "output_upload.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int actionRequireStations();

    virtual int actionAllowStations();

    virtual bool actionRequiresTime();

    virtual CPD3::CPD3Action *createTimeAction(const CPD3::ComponentOptions &options,
                                               double start,
                                               double end,
                                               const std::vector<std::string> &stations = {});

    virtual CPD3::CPD3Action *createTimeAction
            (const CPD3::ComponentOptions &options = CPD3::ComponentOptions(),
             const std::vector<std::string> &stations = {});
};

#endif
