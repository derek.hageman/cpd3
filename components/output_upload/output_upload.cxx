/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <memory>
#include <QTemporaryDir>
#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/range.hxx"
#include "core/environment.hxx"
#include "core/qtcompat.hxx"
#include "core/memory.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/sequencefilter.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/variant/composite.hxx"
#include "database/runlock.hxx"
#include "io/process.hxx"

#include "output_upload.hxx"


Q_LOGGING_CATEGORY(log_component_output_upload, "cpd3.component.output.upload", QtWarningMsg)


using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Transfer;

OutputUpload::OutputUpload()
{ Q_ASSERT(false); }

OutputUpload::OutputUpload(const ComponentOptions &options,
                           double start,
                           double end,
                           const std::vector<SequenceName::Component> &setStations) : lockingMode(
        false),
                                                                                      discardOutput(
                                                                                              false),
                                                                                      singleUpload(
                                                                                              false),
                                                                                      profile(SequenceName::impliedProfile()),
                                                                                      lockTimeout(
                                                                                              30.0),
                                                                                      stations(
                                                                                              setStations),
                                                                                      start(start),
                                                                                      end(end),
                                                                                      haveAfter(
                                                                                              false),
                                                                                      afterTime(
                                                                                              FP::undefined()),
                                                                                      terminated(
                                                                                              false)
{
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }
    if (options.isSet("discard")) {
        discardOutput = qobject_cast<ComponentOptionBoolean *>(options.get("discard"))->get();
    }
    if (options.isSet("single")) {
        singleUpload = qobject_cast<ComponentOptionBoolean *>(options.get("single"))->get();
    }
    if (options.isSet("after")) {
        haveAfter = true;
        afterTime = qobject_cast<ComponentOptionSingleTime *>(options.get("after"))->get();
    }
}

OutputUpload::OutputUpload(const ComponentOptions &options,
                           const std::vector<SequenceName::Component> &setStations) : lockingMode(
        true),
                                                                                      discardOutput(
                                                                                              false),
                                                                                      singleUpload(
                                                                                              false),
                                                                                      profile(SequenceName::impliedProfile()),
                                                                                      lockTimeout(
                                                                                              FP::undefined()),
                                                                                      stations(
                                                                                              setStations),
                                                                                      start(FP::undefined()),
                                                                                      end(FP::undefined()),
                                                                                      haveAfter(
                                                                                              false),
                                                                                      afterTime(
                                                                                              FP::undefined()),
                                                                                      terminated(
                                                                                              false)
{
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }
    if (options.isSet("discard")) {
        discardOutput = qobject_cast<ComponentOptionBoolean *>(options.get("discard"))->get();
    }
    if (options.isSet("single")) {
        singleUpload = qobject_cast<ComponentOptionBoolean *>(options.get("single"))->get();
    }
    if (options.isSet("timeout")) {
        lockTimeout = qobject_cast<ComponentOptionSingleDouble *>(options.get("timeout"))->get();
    }
    if (options.isSet("after")) {
        haveAfter = true;
        afterTime = qobject_cast<ComponentOptionSingleTime *>(options.get("after"))->get();
    }
}

OutputUpload::~OutputUpload()
{
}

void OutputUpload::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    terminateRequested();
}

bool OutputUpload::testTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}

bool OutputUpload::invokeCommand(const QString &program, const QStringList &arguments)
{
    qCDebug(log_component_output_upload) << "Invoking: " << program << arguments;

    auto run = IO::Process::Spawn(program, arguments).forward().create();
    if (!run)
        return false;
    Threading::Receiver receiver;

    terminateRequested.connect(receiver, std::bind(&IO::Process::Instance::terminate, run.get()));
    if (testTerminated())
        return false;

    bool normalExit = true;
    int exitCode = 0;
    run->exited.connect(receiver, [&normalExit, &exitCode](int code, bool normal) {
        exitCode = code;
        normalExit = normal;
    });

    if (!run->start())
        return false;
    if (!run->wait())
        return false;

    run.reset();
    receiver.disconnect();

    if (!normalExit) {
        qCDebug(log_component_output_upload) << "Process did not exit normally";
        return false;
    }
    if (exitCode != 0) {
        qCDebug(log_component_output_upload) << "Process exited with code" << exitCode;
        return false;
    }
    return true;
}

bool OutputUpload::executeAction(CPD3Action *action)
{
    Q_ASSERT(action != NULL);

    action->feedback.forward(feedback);
    terminateRequested.connect(action, "signalTerminate", Qt::DirectConnection);
    if (testTerminated())
        return false;

    action->start();

    /* Polling loop, but not really worth setting up a wait conditional
     * for this. */
    while (!action->wait(500)) {
        if (testTerminated()) {
            qCDebug(log_component_output_upload) << "Terminating action";
            action->signalTerminate();
            action->wait();
            return false;
        }
    }

    action->wait();

    return true;
}

void OutputUpload::setOptions(const Variant::Read &generator,
                              const QDir &targetDirectory,
                              ComponentOptions &options)
{
    if (!generator["Component/IgnoreProfile"].toBool()) {
        ComponentOptionSingleString
                *p = qobject_cast<ComponentOptionSingleString *>(options.get("profile"));
        if (p)
            p->set(QString::fromStdString(profile));
    }

    if (!generator["Component/IgnoreDirectory"].toBool()) {
        ComponentOptionDirectory
                *d = qobject_cast<ComponentOptionDirectory *>(options.get("directory"));
        if (d)
            d->set(targetDirectory.absolutePath());
    }

    ValueOptionParse::parse(options, generator["Component/Options"]);
}

bool OutputUpload::generateFiles(const SequenceName::Component &station,
                                 const Variant::Read &config,
                                 double start,
                                 double end,
                                 const QDir &targetDirectory)
{
    for (auto generator : config["Generate"].toArray()) {
        Memory::release_unused();

        if (generator.getPath("Component").exists()) {
            QString componentName(generator.getPath("Component/Name").toQString());
            QObject *component = ComponentLoader::create(componentName);
            if (component == NULL) {
                qCWarning(log_component_output_upload) << "Can't load component" << componentName;
                return false;
            }

            ActionComponent *actionComponent;
            if ((actionComponent = qobject_cast<ActionComponent *>(component))) {
                if (actionComponent->actionRequireStations() > 1) {
                    qCWarning(log_component_output_upload) << "Component" << componentName
                                                           << "requires multiple stations";
                    return false;
                }

                feedback.emitStage(tr("Start Output Creator"),
                                   tr("An action is being executed to generate files."), false);
                feedback.emitState(componentName);

                ComponentOptions o = actionComponent->getOptions();
                setOptions(generator, targetDirectory, o);
                CPD3Action *action;
                if (actionComponent->actionAllowStations() > 0) {
                    action = actionComponent->createAction(o, {station});
                } else {
                    action = actionComponent->createAction(o);
                }
                if (action != NULL) {
                    if (!executeAction(action)) {
                        delete action;
                        return false;
                    }
                    delete action;
                }
                continue;
            }

            ActionComponentTime *actionComponentTime;
            if ((actionComponentTime = qobject_cast<ActionComponentTime *>(component))) {
                feedback.emitStage(tr("Start Output Creator"),
                                   tr("An action is being executed to generate files."), false);
                feedback.emitState(componentName);

                ComponentOptions o = actionComponentTime->getOptions();
                setOptions(generator, targetDirectory, o);
                CPD3Action *action;
                if (actionComponentTime->actionAllowStations() > 0) {
                    action = actionComponentTime->createTimeAction(o, start, end, {station});
                } else {
                    action = actionComponentTime->createTimeAction(o, start, end);
                }
                if (action != NULL) {
                    if (!executeAction(action)) {
                        delete action;
                        return false;
                    }
                    delete action;
                }
                continue;
            }

            qCWarning(log_component_output_upload) << "Component" << componentName
                                                   << "is not an executable action";
            return false;
        } else if (generator.getPath("Command").exists()) {
            if (generator.getPath("Command").getType() == Variant::Type::String) {
                QStringList arguments(substitutions.apply(generator.getPath("Command").toQString())
                                                   .split(QRegExp("\\s+"),
                                                          QString::SkipEmptyParts));
                if (arguments.isEmpty()) {
                    qCWarning(log_component_output_upload) << "Empty command as generator";
                    continue;
                }
                QString program(arguments.takeFirst());

                feedback.emitStage(tr("Execute External Generator"),
                                   tr("An external program is currently being executed to generate files."),
                                   false);
                feedback.emitState(program);
                if (!invokeCommand(program, arguments))
                    return false;
                continue;
            } else {
                QString program
                        (substitutions.apply(generator.getPath("Command/Program").toQString()));
                if (program.isEmpty()) {
                    qCWarning(log_component_output_upload) << "Empty command as generator";
                    continue;
                }

                feedback.emitStage(tr("Execute External Generator"),
                                   tr("An external program is currently being executed to generate files."),
                                   false);

                QStringList arguments;
                for (auto add : generator.getPath("Command/Arguments").toArray()) {
                    arguments.append(substitutions.apply(add.toQString()));
                }

                feedback.emitState(program);
                bool status = invokeCommand(program, arguments);
                if (!status && !generator.getPath("Command/AllowFailure").toBool()) {
                    qCDebug(log_component_output_upload) << "Execution of" << program << "failed";
                    return false;
                }
                continue;
            }
        }

        qCDebug(log_component_output_upload) << "Invalid generator" << generator;
    }

    Memory::release_unused();

    return true;
}

Uploader::Uploader(const Variant::Read &config, OutputUpload *p) : FileUploader(config),
                                                                                parent(p)
{ }

Uploader::~Uploader()
{ }

QString Uploader::applySubstitutions(const QString &input) const
{ return parent->substitutions.apply(input); }

void Uploader::pushTemporaryOutput(const QFileInfo &info)
{
    parent->substitutions.push();
    parent->substitutions.setFile("upload", info);
    parent->substitutions.setFile("source", info);
    parent->substitutions.setFile("input", info);
}

void Uploader::popTemporaryOutput()
{
    parent->substitutions.pop();
}

void Uploader::pushInputFile(const QFileInfo &info)
{
    parent->substitutions.push();
    parent->substitutions.setFile("input", info);
    parent->substitutions.setFile("upload", info);
    parent->substitutions.setFile("source", info);
    parent->substitutions.setString("file", info.fileName());
}

void Uploader::popInputFile()
{
    parent->substitutions.pop();
}

bool OutputUpload::uploadFiles(const Variant::Read &config, const QList<QFile *> &files)
{
    Uploader upload(config["Transfer"], this);
    for (const auto &add : files) {
        upload.addUpload(IO::Access::file(add->fileName(), IO::File::Mode::readOnly()));
    }
    Threading::Receiver rx;
    terminateRequested.connect(rx, std::bind(&Uploader::signalTerminate, &upload));
    if (testTerminated())
        return false;
    if (!upload.exec())
        return false;
    return true;
}

bool OutputUpload::uploadFile(const Variant::Read &config, QFile *file)
{
    Uploader upload(config["Transfer"], this);
    upload.addUpload(IO::Access::file(file->fileName(), IO::File::Mode::readOnly()));
    Threading::Receiver rx;
    terminateRequested.connect(rx, std::bind(&Uploader::signalTerminate, &upload));
    if (testTerminated())
        return false;
    if (!upload.exec())
        return false;
    return true;
}

bool OutputUpload::completeFiles(const Variant::Read &config, const QList<QFile *> &files)
{
    for (QList<QFile *>::const_iterator file = files.constBegin(), end = files.constEnd();
            file != end;
            ++file) {
        TextSubstitutionStack::Context subctx(substitutions);
        QFileInfo info(*(*file));
        substitutions.setFile("input", info);
        substitutions.setFile("upload", info);
        substitutions.setFile("source", info);
        substitutions.setString("file", info.fileName());

        QString moveTarget(substitutions.apply(config["Archive/Completed"].toQString()));

        if (!moveTarget.isEmpty()) {
            if (!FileUploader::moveLocalFile(info, QFileInfo(moveTarget)))
                return false;
            continue;
        }

        (*file)->remove();
    }

    return true;
}

bool OutputUpload::completeFile(const Variant::Read &config, QFile *file)
{ return completeFiles(config, QList<QFile *>() << file); }

bool OutputUpload::failFiles(const Variant::Read &config,
                             const QList<QFile *> &files,
                             const QString &retryLocation)
{
    Q_UNUSED(config);

    if (!retryLocation.isEmpty()) {
        QDir dir(retryLocation);
        for (QList<QFile *>::const_iterator file = files.constBegin(), end = files.constEnd();
                file != end;
                ++file) {
            QFileInfo info(**file);
            QString moveTarget(dir.filePath(info.fileName()));
            if (moveTarget.isEmpty())
                return false;

            QFileInfo targetInfo(moveTarget);
            if (targetInfo.absoluteFilePath() == info.absoluteFilePath())
                continue;

            qCDebug(log_component_output_upload) << "Moving" << info.fileName() << "to"
                                                 << retryLocation << "for a future retry";
            if (!FileUploader::moveLocalFile(info, QFileInfo(moveTarget)))
                return false;
        }
        return true;
    }

    for (QList<QFile *>::const_iterator file = files.constBegin(), end = files.constEnd();
            file != end;
            ++file) {
        (*file)->remove();
    }
    /* We're in a failure mode with no retry, so abort the upload entirely */
    return false;
}

bool OutputUpload::failFile(const Variant::Read &config, QFile *file, const QString &retryLocation)
{ return failFiles(config, QList<QFile *>() << file, retryLocation); }

bool OutputUpload::completeUpload(const Variant::Read &config, QList<QFile *> filesToUpload)
{
    QString retryLocation(substitutions.apply(config["Archive/RetryDirectory"].toQString()));
    if (!retryLocation.isEmpty()) {
        qCDebug(log_component_output_upload) << "Checking" << retryLocation << "for files to retry";
        QFileInfoList fileList
                (QDir(retryLocation).entryInfoList(QDir::Files | QDir::Readable | QDir::Writable,
                                                   QDir::Name));
        for (QFileInfoList::const_iterator operateFileInfo = fileList.constBegin(),
                endFileInfo = fileList.constEnd();
                operateFileInfo != endFileInfo;
                ++operateFileInfo) {
            filesToUpload.append(new QFile(operateFileInfo->absoluteFilePath()));
            qCDebug(log_component_output_upload) << "Retrying" << operateFileInfo->fileName();
        }
    }

    if (filesToUpload.isEmpty()) {
        qCDebug(log_component_output_upload) << "No files to upload";
        return true;
    }

    if (discardOutput) {
        qCDebug(log_component_output_upload) << "Discarding" << filesToUpload.size() << "files";
        for (QList<QFile *>::const_iterator file = filesToUpload.constBegin(),
                end = filesToUpload.constEnd(); file != end; ++file) {
            (*file)->remove();
        }
        qDeleteAll(filesToUpload);
        return true;
    }

    bool result = true;
    if (config["Transfer/Batch"].toBool()) {
        if (!uploadFiles(config, filesToUpload)) {
            if (!failFiles(config, filesToUpload, retryLocation))
                result = false;
        } else {
            if (!completeFiles(config, filesToUpload)) {
                if (config["Transfer/AbortOnFailure"].toBool())
                    result = false;
            }
        }
    } else {
        for (QList<QFile *>::const_iterator file = filesToUpload.constBegin(),
                end = filesToUpload.constEnd(); file != end; ++file) {
            if (!uploadFile(config, *file)) {
                if (!failFile(config, *file, retryLocation))
                    result = false;
                if (result && config["Transfer/AbortOnFailure"].toBool()) {
                    result = false;
                    qCDebug(log_component_output_upload) << "Aborting after upload failure for"
                                                         << (*file)->fileName();
                    break;
                }
                if (config["Transfer/HaltOnFailure"].toBool()) {
                    qCDebug(log_component_output_upload) << "Halting after upload failure for"
                                                         << (*file)->fileName();
                    break;
                }

                qCDebug(log_component_output_upload) << "Upload failed for" << (*file)->fileName()
                                                     << "but continuing anyway";
                continue;
            }

            if (!completeFiles(config, QList<QFile *>() << *file)) {
                if (config["Transfer/AbortOnFailure"].toBool()) {
                    result = false;
                    qCDebug(log_component_output_upload) << "Aborting after completion failure for"
                                                         << (*file)->fileName();
                    break;
                }
                if (config["Transfer/HaltOnFailure"].toBool()) {
                    qCDebug(log_component_output_upload) << "Halting after completion failure for"
                                                         << (*file)->fileName();
                    break;
                }
                qCDebug(log_component_output_upload) << "Completion failed for"
                                                     << (*file)->fileName()
                                                     << "but continuing anyway";
            }
        }
    }

    qDeleteAll(filesToUpload);
    return result;
}

bool OutputUpload::uploadFiles(const Variant::Read &config, const QDir &sourceDirectory)
{
    QFileInfoList fileList
            (sourceDirectory.entryInfoList(QDir::Files | QDir::Readable | QDir::Writable,
                                           QDir::Name));

    QList<QFile *> filesToUpload;
    for (QFileInfoList::const_iterator operateFileInfo = fileList.constBegin(),
            endFileInfo = fileList.constEnd(); operateFileInfo != endFileInfo; ++operateFileInfo) {
        filesToUpload.append(new QFile(operateFileInfo->absoluteFilePath()));
    }
    if (filesToUpload.isEmpty()) {
        qCDebug(log_component_output_upload) << "No new files generated";
    }

    return completeUpload(config, filesToUpload);
}


QDir OutputUpload::createTemporaryDirectory()
{
    {
        QTemporaryDir tempDir;
        tempDir.setAutoRemove(false);
        QString path(tempDir.path());
        if (!path.isEmpty()) {
            QDir dir(path);
            if (dir.exists())
                return dir;
        }
    }
#ifdef Q_OS_UNIX
    if (auto run = IO::Process::Spawn("mktemp", {"-d"}).create()) {
        auto contents = run->outputStream();
        if (contents) {
            IO::Generic::Stream::Iterator data(*contents);
            if (run->start() && run->wait()) {
                auto path = QString::fromUtf8(data.all().toQByteArrayRef()).trimmed();
                if (!path.isEmpty()) {
                    QDir dir(path);
                    if (dir.exists())
                        return dir;
                }
            }
        }
    }
#endif

    QDir dir(QDir::temp());
    for (int t = 0; t < 1000; t++) {
        if (testTerminated())
            return QDir();
        QString name(QString("CPD3Upload-%1").arg(Random::string()));
        if (!dir.mkdir(name))
            continue;
        QDir result(dir);
        if (!result.cd(name)) {
            dir.rmdir(name);
            continue;
        }
        return result;
    }

    return QDir();
}

bool OutputUpload::performUpload(const SequenceName::Component &station,
                                 const Variant::Read &config,
                                 double start,
                                 double end)
{
    QDir temp(createTemporaryDirectory());
    if (temp.path().isEmpty() || temp.path() == ".") {
        qCDebug(log_component_output_upload) << "Unable to create temporary directory";
        return false;
    }

    TextSubstitutionStack::Context subctx(substitutions);
    substitutions.setTime("start", start, false);
    substitutions.setTime("end", end, false);
    substitutions.setFile("directory", temp.absolutePath());
    substitutions.setFile("pwd", temp.absolutePath());

    bool result;
    {
        IO::Process::WorkingDirectoryLock lock(temp.absolutePath());
        result = generateFiles(station, config, start, end, temp);
    }
    if (!result) {
        qCDebug(log_component_output_upload) << "File generation failed, uploading will be skipped";
    } else {
        result = uploadFiles(config, temp);
    }

    QFileInfoList files(temp.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot));
    for (QFileInfoList::const_iterator file = files.constBegin(), endFiles = files.constEnd();
            file != endFiles;
            ++file) {
        if (file->isDir()) {
            temp.rmdir(file->fileName());
        } else {
            temp.remove(file->fileName());
        }
    }

    QString removeName(temp.dirName());
    temp.cdUp();
    temp.rmdir(removeName);
    return result;
}

static void setFailsafeTimes(Archive::Selection::List &selections,
                             const ValueSegment::Transfer &config)
{
    double now = Time::time();
    auto active = Range::findIntersecting(config, now);
    if (active == config.end())
        return;
    auto setting = active->getValue()["MaximumAge"];
    if (!setting.exists())
        return;
    double age = Variant::Composite::offsetTimeInterval(setting, now, false);
    if (!FP::defined(age) || age >= now)
        return;
    for (auto &sel: selections) {
        sel.start = age;
    }
}

static bool shouldUpdateSegment(const Variant::Read &config,
                                double segmentStart,
                                double segmentEnd,
                                const QList<Time::Bounds> &completed)
{
    if (config["DisableReupload"].toBool())
        return false;
    if (Range::findIntersecting(completed, segmentStart, segmentEnd) == completed.constEnd())
        return false;
    return true;
}

static bool segmentPassesFilter(const Variant::Read &config,
                                double segmentStart,
                                double segmentEnd,
                                double valueStart,
                                double valueEnd,
                                const QList<Time::Bounds> &completed)
{
    Q_UNUSED(valueStart);

    if (!config["AllowIncomplete"].toBool()) {
        /* The end of the value must be at least the end of the segment,
         * otherwise it's not yet complete */
        if (Range::compareEnd(valueEnd, segmentEnd) < 0) {
            /* Unless we've already uploaded this segment, in which case
             * it's still ok if we're doing incremental updates */
            if (!shouldUpdateSegment(config, segmentStart, segmentEnd, completed))
                return false;
        }
    }

    return true;
}

QList<Time::Bounds> OutputUpload::buildUploadRanges(const SequenceName::Component &station,
                                                    const CPD3::Data::ValueSegment::Transfer &config,
                                                    double lastRunTime,
                                                    double &dataModified,
                                                    const QList<Time::Bounds> &completed)
{

    SequenceFilter filter;
    filter.configure(ValueSegment::withPath(config, "Trigger"), {QString::fromStdString(station)});

    auto selections = filter.toArchiveSelection({station});
    for (auto &sel : selections) {
        sel.modifiedAfter = lastRunTime;
        sel.includeMetaArchive = false;
        sel.includeDefaultStation = false;
    }
    setFailsafeTimes(selections, config);

    qCDebug(log_component_output_upload) << "Issuing archive read for" << station << ':'
                                         << selections;

    std::unique_ptr<DynamicTimeInterval> interval
            (DynamicTimeInterval::fromConfiguration(config, "Interval", config.front().getStart(),
                                                    config.back().getEnd()));
    std::unique_ptr<DynamicTimeInterval> rounder(interval->clone());

    QList<Time::Bounds> result;
    double lastStart = FP::undefined();
    double lastEnd = FP::undefined();

    ArchiveSink::Iterator data;
    Archive::Access access;
    Archive::Access::ReadLock lock(access, true);
    auto reader = access.readArchive(selections, &data);

    double segmentStart = FP::undefined();
    double segmentEnd = FP::undefined();
    if (!completed.empty() && FP::defined(completed.back().getEnd())) {
        segmentStart =
                interval->roundConst(completed.back().getStart(), completed.back().getEnd(), true);
        if (FP::defined(segmentStart)) {
            segmentEnd = interval->applyConst(completed.back().getStart(), segmentStart, true);
        }
    }

    auto configStart = config.begin();
    while (data.hasNext()) {
        auto value = data.next();
        if (!filter.acceptAdvancing(value))
            continue;

        Q_ASSERT(FP::defined(value.getModified()));
        if (!FP::defined(dataModified) || dataModified < value.getModified())
            dataModified = value.getModified();

        double start = value.getStart();
        double end = value.getEnd();

        /* Do nothing if this value is entirely within the last
         * segment */
        if (FP::defined(lastStart) && FP::defined(lastEnd)) {
            if (FP::defined(start) && start >= lastStart && FP::defined(end) && end <= lastEnd)
                continue;
        }

        auto active = Range::findIntersecting(configStart, config.end(), start, end);
        if (active == config.end())
            continue;
        if (active != configStart && Range::compareStartEnd(start, (active - 1)->getEnd()) >= 0) {
            configStart = active;
        }

        double checkStart = interval->round(start, start);
        if (!FP::defined(segmentStart)) {
            segmentStart = checkStart;
        } else if (FP::defined(checkStart)) {
            if (checkStart < segmentStart) {
                segmentStart = checkStart;
            } else if (active->value()["DisableFill"].toBoolean()) {
                /* If not filling, then check if this is a gap and skip if it is, but
                 * still keep the fill if it's contiguous (end equal to incoming start) so
                 * we still emit the completion on values equal to the start of a segment */
                if (!FP::defined(segmentEnd) || segmentEnd + 1 < checkStart)
                    segmentStart = checkStart;
            }
        }
        if (!FP::defined(segmentStart))
            continue;

        for (int i = 0; i < 10000; i++) {
            segmentEnd = interval->apply(start, segmentStart, true);
            if (!FP::defined(segmentEnd))
                break;
            /* Don't allow zero (or tiny) segments, so just stop */
            if (segmentStart + 1 >= segmentEnd)
                break;

            if (segmentPassesFilter(active->getValue(), segmentStart, segmentEnd, start, end,
                                    completed)) {
                Range::overlayFragmenting(result, Time::Bounds(segmentStart, segmentEnd));

                lastStart = segmentStart;
                lastEnd = segmentEnd;
            }

            if (!FP::defined(segmentEnd) || !FP::defined(end) || segmentEnd + 1 >= end)
                break;
            segmentStart = segmentEnd;
        }
    }
    reader->signalTerminate();
    reader->wait();
    reader.reset();
    interval.reset();

    /* Do a rounding pass on the segments and eliminate any that become
     * the same.  This takes care of fragmenting segments producing tiny
     * slivers if the interval changes (sort of a degenerate case to begin
     * with, however). */
    if (result.size() > 1) {
        for (auto check = result.begin(); check != result.end();) {
            double time = check->start;
            check->start = rounder->round(time, check->start, false);
            check->end = rounder->round(time, check->end, true);

            if (check != result.begin()) {
                if (FP::equal((check - 1)->start, check->start) &&
                        FP::equal((check - 1)->end, check->end)) {
                    check = result.erase(check);
                    continue;
                }
            }

            ++check;
        }
    }

    rounder.reset();

    /* If we've got multiple segments, try to merge them. */
    if (result.size() > 1) {
        if (singleUpload) {
            result = QList<Time::Bounds>()
                    << Time::Bounds(result.first().getStart(), result.last().getEnd());
        } else {
            interval.reset(DynamicTimeInterval::fromConfiguration(config, "Merge",
                                                                  config.front().getStart(),
                                                                  config.back().getEnd()));
            for (auto check = result.begin(); check != result.end() - 1;) {
                double mergeEnd = interval->apply(check->getEnd(), check->getEnd(), true);
                if (!FP::defined(mergeEnd)) {
                    ++check;
                    continue;
                }
                if (Range::compareStartEnd((check + 1)->getStart(), mergeEnd) > 0) {
                    ++check;
                    continue;
                }

                (check + 1)->start = check->start;
                check = result.erase(check);
            }

            interval.reset();
        }
    }

    splitRanges(result, config);

    if (!result.isEmpty() && config.back().getValue()["LatestOnly"].toBool()) {
        if (!completed.isEmpty() &&
                Range::compareEnd(result.last().getEnd(), completed.last().getEnd()) < 0) {
            result.clear();
        } else {
            result = result.mid(result.length() - 1);
        }
    }

    return result;
}

QList<Time::Bounds> OutputUpload::buildManualRanges(const CPD3::Data::ValueSegment::Transfer &config)
{
    if (singleUpload || !FP::defined(start) || !FP::defined(end))
        return QList<Time::Bounds>() << Time::Bounds(start, end);

    std::unique_ptr<DynamicTimeInterval> interval
            (DynamicTimeInterval::fromConfiguration(config, "Interval", config.front().getStart(),
                                                    config.back().getEnd()));

    QList<Time::Bounds> result;
    double currentStart = interval->round(start, start, false);
    if (!FP::defined(currentStart)) {
        result.append(Time::Bounds(start, end));
    } else {
        for (;;) {
            double currentEnd = interval->apply(currentStart, currentStart, true);
            if (!FP::defined(currentEnd) || currentEnd <= currentStart) {
                result.append(Time::Bounds(currentStart, end));
                break;
            }

            result.append(Time::Bounds(currentStart, currentEnd));

            if (currentEnd >= end)
                break;
            currentStart = currentEnd;
        }
    }
    interval.reset();

    splitRanges(result, config);

    return result;
}

void OutputUpload::splitRanges(QList<Time::Bounds> &result, const ValueSegment::Transfer &config)
{
    if (singleUpload)
        return;
    if (result.empty())
        return;

    std::unique_ptr<DynamicTimeInterval> interval
            (DynamicTimeInterval::fromConfiguration(config, "Split", config.front().getStart(),
                                                    config.back().getEnd()));
    for (auto check = result.begin(); check != result.end();) {
        if (!FP::defined(check->getEnd())) {
            ++check;
            continue;
        }

        double splitEnd = interval->apply(check->getStart(), check->getStart(), true);
        if (!FP::defined(splitEnd)) {
            ++check;
            continue;
        }
        if (Range::compareEnd(check->getEnd(), splitEnd) <= 0) {
            ++check;
            continue;
        }

        auto next = *check;
        check->end = splitEnd;

        next.start = splitEnd;
        ++check;
        check = result.insert(check, std::move(next));
    }
}

void OutputUpload::run()
{
    if (testTerminated())
        return;

    profile = Util::to_lower(std::move(profile));
    if (profile.empty()) {
        qCDebug(log_component_output_upload) << "Invalid profile";
        return;
    }

    double startProcessing = Time::time();

    std::unordered_map<SequenceName::Component, ValueSegment::Transfer> stationConfig;
    {
        Archive::Access access;
        Archive::Access::ReadLock lock(access);

        auto allStations = access.availableStations(stations);
        if (allStations.empty()) {
            qCDebug(log_component_output_upload) << "No stations available";
            return;
        }
        stations.clear();
        std::copy(allStations.begin(), allStations.end(), Util::back_emplacer(stations));

        for (const auto &station : stations) {
            auto config = ValueSegment::Stream::read(
                    Archive::Selection(start, end, {station}, {"configuration"}, {"upload"}),
                    &access);
            config = ValueSegment::withPath(config, QString("Profiles/%1").arg(
                    QString::fromStdString(profile)));
            bool anyValid = false;
            for (const auto &check : config) {
                if (!check.getValue().exists())
                    continue;
                anyValid = true;
                break;
            }
            if (!anyValid)
                continue;
            stationConfig.emplace(station, std::move(config));
        }
    }

    if (stationConfig.empty()) {
        qCDebug(log_component_output_upload) << "No upload defined for" << stations;
        return;
    }

    if (testTerminated())
        return;

    std::sort(stations.begin(), stations.end());

    qCDebug(log_component_output_upload) << "Starting upload for" << stations.size()
                                         << "station(s)";

    for (const auto &station : stations) {
        if (testTerminated())
            break;
        auto &config = stationConfig[station];
        if (config.empty())
            continue;

        substitutions.clear();
        substitutions.setString("station", QString::fromStdString(station).toLower());
        substitutions.setString("profile", QString::fromStdString(profile));

        QList<Time::Bounds> uploadRanges;
        QList<Time::Bounds> completedRanges;
        double lastRunTime = FP::undefined();
        Database::RunLock rc;
        QString rcKey(QString("outputupload %1 %2").arg(QString::fromStdString(profile),
                                                        QString::fromStdString(station)));

        if (lockingMode) {
            feedback.emitStage(tr("Lock %1").arg(QString::fromStdString(station).toUpper()),
                               tr("The system is acquiring an exclusive lock for %1.").arg(
                                       QString::fromStdString(station).toUpper()), false);

            qCDebug(log_component_output_upload) << "Locking" << rcKey;

            bool ok = false;
            lastRunTime = rc.acquire(rcKey, lockTimeout, &ok);
            if (!ok) {
                feedback.emitFailure();
                qCDebug(log_component_output_upload) << "Upload skipped for" << station;
                continue;
            }

            qCDebug(log_component_output_upload) << "Locked" << station << "after"
                                                 << Logging::time(lastRunTime);

            if (haveAfter) {
                lastRunTime = afterTime;
            } else {
                QByteArray data(rc.get(rcKey));
                if (!data.isEmpty()) {
                    QDataStream stream(&data, QIODevice::ReadOnly);
                    stream >> completedRanges;
                }
            }

            qCDebug(log_component_output_upload) << "Prepare" << station
                                                 << Logging::time(lastRunTime) << "with"
                                                 << completedRanges.size() << "segments";

            feedback.emitStage(tr("Prepare %1").arg(QString::fromStdString(station).toUpper()),
                               tr("The system is determining the affected time ranges for %1.").arg(
                                       QString::fromStdString(station).toUpper()), false);

            double dataModified = FP::undefined();
            uploadRanges =
                    buildUploadRanges(station, config, lastRunTime, dataModified, completedRanges);
            if (uploadRanges.empty()) {
                if (haveAfter) {
                    rc.seenTime(afterTime);
                } else {
                    /* If there's no data to upload, then abort, so we don't move the time
                     * forwards.  This is to allow for delays on the trigger back past the
                     * current time on the leading edge (e.x. incoming FTP sync delays) */
                    qCDebug(log_component_output_upload) << "No segments to upload for" << station;
                    completeUpload(config.back().getValue());
                    rc.fail();
                    continue;
                }
            } else if (FP::defined(dataModified)) {
                rc.seenTime(dataModified);
            } else if (haveAfter) {
                rc.seenTime(afterTime);
            }
        } else {
            uploadRanges = buildManualRanges(config);
        }

        qCDebug(log_component_output_upload) << "Uploading" << uploadRanges.size()
                                             << "segment(s) for" << station;

        bool ok = true;
        if (uploadRanges.isEmpty()) {
            ok = completeUpload(config.back().getValue());
        } else {
            auto configBegin = config.cbegin();
            for (const auto &bounds : uploadRanges) {
                auto active = Range::findIntersecting(configBegin, config.cend(), bounds.getStart(),
                                                      bounds.getEnd());
                if (active == config.cend())
                    continue;
                if (!active->getValue().exists())
                    continue;

                qCDebug(log_component_output_upload) << "Uploading" << station << bounds;

                if (!performUpload(station, active->getValue(), bounds.getStart(),
                                   bounds.getEnd())) {
                    ok = false;
                    break;
                }

                Range::overlayFragmenting(completedRanges, bounds);
            }

            if (config.back().getValue()["RecordEvent"].toBool() && !discardOutput) {
                Variant::Root event;

                double tNow = Time::time();
                if (!ok) {
                    event["Text"].setString(
                            tr("Failed upload of %n segment(s)", "upload event text",
                               uploadRanges.size()));
                } else {
                    event["Text"].setString(
                            tr("Completed upload of %n segment(s)", "upload event text",
                               uploadRanges.size()));
                }
                event["Information"].hash("By").setString("output_upload");
                event["Information"].hash("At").setDouble(tNow);
                event["Information"].hash("Environment").setString(Environment::describe());
                event["Information"].hash("Revision").setString(Environment::revision());
                event["Information"].hash("TotalCompleted").setInt64(completedRanges.size());
                event["Information"].hash("Status").setBool(ok);
                event["Information"].hash("Profile").setString(profile);
                if (lockingMode) {
                    event["Information"].hash("LastRunTime").setDouble(lastRunTime);
                }

                for (const auto &bounds : uploadRanges) {
                    auto range = event["Segments"].toArray().after_back();
                    range.hash("Start").setDouble(bounds.start);
                    range.hash("End").setDouble(bounds.end);
                }
                Archive::Access().writeSynchronous(SequenceValue::Transfer{
                        SequenceValue({station, "events", "upload"}, std::move(event), tNow,
                                      tNow)});
            }
        }

        /* Merge completed ranges */
        if (completedRanges.size() > 1) {
            for (auto check = completedRanges.begin() + 1; check != completedRanges.end();) {
                if (Range::compareStartEnd(check->getStart(), (check - 1)->getEnd() + 1.0) <= 0) {
                    (check - 1)->setEnd(check->getEnd());
                    check = completedRanges.erase(check);
                    continue;
                }
                ++check;
            }
        }

        if (ok && !discardOutput) {
            if (lockingMode) {
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    stream << completedRanges;
                }
                rc.set(rcKey, data);
                rc.release();
            }
            qCDebug(log_component_output_upload) << "Upload completed for" << station;
        } else {
            if (lockingMode)
                rc.fail();
        }
    }

    double endProcessing = Time::time();
    qCDebug(log_component_output_upload) << "Finished upload after "
                                         << Logging::elapsed(endProcessing - startProcessing);

    QCoreApplication::processEvents();
}


ComponentOptions OutputUploadComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Execution profile"),
                                                tr("This is the profile name to perform.  Multiple profiles can be "
                                                   "defined to perform different types of uploading for a station.  "
                                                   "Consult the station configuration for available profiles."),
                                                tr("aerosol")));

    options.add("discard", new ComponentOptionBoolean(tr("discard", "name"), tr("Discard output"),
                                                      tr("When enabled this causes the generated files to be discarded "
                                                         "instead of actually uploaded."),
                                                      QString()));

    options.add("single", new ComponentOptionBoolean(tr("single", "name"), tr("Single file upload"),
                                                     tr("When enabled this results in only a single upload of all "
                                                        "affected data, instead of the normal multiple segments used."),
                                                     QString()));

    ComponentOptionSingleTime *after =
            new ComponentOptionSingleTime(tr("after", "name"), tr("Override time"),
                                          tr("This sets the time to re-upload after.  When set, this will upload "
                                             "all data after the given time.  This can be used to "
                                             "ignore the record of the last update and all prior uploaded ranges, "
                                             "but doing so can cause there to be updated data that does not exist "
                                             "in the final upload.  No effect when used with an explicit time range."),
                                          tr("Last run time"));
    after->setAllowUndefined(true);
    options.add("after", after);

    ComponentOptionSingleDouble *timeout =
            new ComponentOptionSingleDouble(tr("wait", "name"), tr("Maximum wait time"),
                                            tr("This is the maximum time to wait for another upload process to "
                                               "complete, in seconds.  If there are two concurrent processes for "
                                               "the same station and profile, the new one will wait at most this "
                                               "many seconds before giving up without uploading new data.  If "
                                               "this is set to undefined, it will wait forever for the other "
                                               "process to complete.  This is only valid when no times are "
                                               "specified."), tr("30 seconds"));
    timeout->setMinimum(0.0);
    timeout->setAllowUndefined(true);
    options.add("timeout", timeout);

    return options;
}

QList<ComponentExample> OutputUploadComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Upload data", "default example name"),
                                     tr("This will upload data for the default \"aerosol\" profile.")));

    return examples;
}

int OutputUploadComponent::actionRequireStations()
{ return 0; }

int OutputUploadComponent::actionAllowStations()
{ return INT_MAX; }

bool OutputUploadComponent::actionRequiresTime()
{ return false; }

CPD3Action *OutputUploadComponent::createTimeAction(const ComponentOptions &options,
                                                    double start,
                                                    double end,
                                                    const std::vector<std::string> &stations)
{ return new OutputUpload(options, start, end, stations); }

CPD3Action *OutputUploadComponent::createTimeAction(const ComponentOptions &options,
                                                    const std::vector<std::string> &stations)
{ return new OutputUpload(options, stations); }
