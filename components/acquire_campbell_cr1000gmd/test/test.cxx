/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <map>
#include <cstdint>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray outgoing;
    QByteArray incoming;
    bool enableAOT;

    double ain[32];
    double T;
    double V;

    double aot[8];
    std::uint_fast32_t dot;

    ModelInstrument() : outgoing(), incoming(), enableAOT(false)
    {
        for (int i = 0; i < 32; i++) {
            ain[i] = (double) (i + 1) * 0.25;
        }
        T = 25.1;
        V = 5.12;

        dot = 0;
        for (int i = 0; i < 8; i++) {
            aot[i] = 0;
        }
    }

    void advance(double seconds)
    {
        Q_UNUSED(seconds);

        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR).trimmed());
            incoming = incoming.mid(idxCR + 1);

            if (line == "RST") {
                outgoing.append("STA,");
                outgoing.append(
                        QByteArray::number(static_cast<quint32>(dot), 16).rightJustified(8, '0'));
                for (int i = 0; i < 32; i++) {
                    outgoing.append(',');
                    if (!FP::defined(ain[i])) {
                        outgoing.append("NAN");
                        continue;
                    }
                    outgoing.append(QByteArray::number(ain[i], 'f', 7));
                }
                outgoing.append(',');
                outgoing.append(QByteArray::number(V, 'f', 7));
                outgoing.append(',');
                outgoing.append(QByteArray::number(T, 'f', 7));
                outgoing.append('\r');
            } else if (line.startsWith("SDO,")) {
                line = line.mid(4);
                if (line.length() <= 4) {
                    outgoing.append("ERR,Invalid digital state\r");
                    continue;
                }
                bool ok = false;
                quint32 v = line.mid(4).toUInt(&ok, 16);
                if (!ok) {
                    outgoing.append("ERR,Error parsing SDO\r");
                    continue;
                }
                dot = v;

                outgoing.append("SDA,");
                outgoing.append(
                        QByteArray::number(static_cast<quint32>(dot), 16).rightJustified(8, '0'));
                outgoing.append('\r');
            } else if (line.startsWith("SAO,")) {
                if (enableAOT) {
                    QList<QByteArray> fields(line.split(','));
                    if (fields.length() != 3) {
                        outgoing.append("ERR,Invalid analog output\r");
                        continue;
                    }

                    bool ok = false;
                    int index = fields.at(1).toInt(&ok);
                    if (!ok || index < 0 || index >= 8) {
                        outgoing.append("ERR,Error parsing SAO index\r");
                        continue;
                    }

                    double value = fields.at(2).toDouble(&ok);
                    if (!ok || !FP::defined(value)) {
                        outgoing.append("ERR,Error parsing SAO value\r");
                        continue;
                    }

                    aot[index] = value;

                    outgoing.append("SAA,");
                    outgoing.append(QByteArray::number(index));
                    outgoing.append(',');
                    outgoing.append(QByteArray::number(value, 'f', 7));
                    outgoing.append('\r');
                }
                /* Otherwise just ignore it */
            } else {
                outgoing.append("ERR,Invalid command\r");
            }
        }
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZINPUTS", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZOUTPUTS", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("F2", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZINDEX", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZDIGITAL", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZSTATE", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("T"))
            return false;
        if (!stream.checkContiguous("V"))
            return false;
        if (!stream.checkContiguous("ZINPUTS"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("T", Variant::Root(model.T), time))
            return false;
        if (!stream.hasAnyMatchingValue("V", Variant::Root(model.V), time))
            return false;
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;

        Variant::Write check = Variant::Write::empty();
        for (int i = 0; i < 32; i++) {
            check.array(i).setDouble(model.ain[i]);
        }
        if (!stream.hasAnyMatchingValue("ZINPUTS", check, time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("ZDIGITAL", Variant::Root((qint64) model.dot), time))
            return false;
        return true;
    }

    bool checkVariableValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             const std::map<SequenceName::Component, int> &variables,
                             double time = FP::undefined())
    {
        for (const auto &check : variables) {
            if (!stream.hasAnyMatchingValue(check.first, Variant::Root(model.ain[check.second]),
                                            time))
                return false;
        }
        return true;
    }

    void configureVariables(Variant::Root &cv,
                            const std::map<SequenceName::Component, int> &variables)
    {
        for (const auto &check : variables) {
            cv["Variables"].hash(check.first).hash("Channel").setInt64(check.second + 1);
        }
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_campbell_cr1000gmd"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_campbell_cr1000gmd"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        std::map<SequenceName::Component, int> variables{{"T_V11", 0},
                                                         {"U_V11", 1},};
        configureVariables(cv, variables);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 20; i++) {
            control.externalControl("RST\r");
            control.advance(1.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 20; i++) {
            control.externalControl("RST\r");
            for (int j = 0; j < 4; j++) {
                control.advance(0.25);
                QTest::qSleep(50);
            }
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        logging.checkAscending();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkContiguous(logging));

        QVERIFY(checkVariableValues(logging, instrument, variables));
        QVERIFY(checkVariableValues(realtime, instrument, variables));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        std::map<SequenceName::Component, int> variables{{"T_V11", 1},
                                                         {"U_V11", 2},
                                                         {"P_V11", 3},};
        configureVariables(cv, variables);
        cv["PollInterval"].setDouble(0.0);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.dot = 0x010;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.2);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 40; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        logging.checkAscending();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkContiguous(logging));

        QVERIFY(checkVariableValues(logging, instrument, variables));
        QVERIFY(checkVariableValues(realtime, instrument, variables));
    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 30; i++) {
            control.externalControl("RST\r");
            for (int j = 0; j < 4; j++) {
                control.advance(0.25);
                QTest::qSleep(50);
            }
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        logging.checkAscending();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkContiguous(logging));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        logging.checkAscending();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkContiguous(logging));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveControl()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        std::map<SequenceName::Component, int> variables{{"T_V11", 0},
                                                         {"U_V11", 1},};
        configureVariables(cv, variables);
        cv["AnalogOutputs/AOut1"].setInt64(0);
        cv["DigitalOutputs/DOut1"].setInt64(1);
        cv["Initialize/Analog"].setString("0,1,2,3");
        cv["Initialize/Digital"].setInt64(0x0123);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.enableAOT = true;
        instrument.ain[5] = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));

        instrument.ain[0] = 2.25;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("T_V11", Variant::Root(2.25)))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("T_V11", Variant::Root(2.25)));

        Variant::Write aout = Variant::Write::empty();
        aout.array(0).setDouble(0);
        aout.array(1).setDouble(1);
        aout.array(2).setDouble(2);
        aout.array(3).setDouble(3);
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZOUTPUTS", aout) &&
                    realtime.hasAnyMatchingValue("ZDIGITAL", Variant::Root(0x0123)))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZOUTPUTS", aout));
        QVERIFY(realtime.hasAnyMatchingValue("ZDIGITAL", Variant::Root(0x0123)));

        Variant::Write cmd = Variant::Write::empty();
        cmd["SetAnalog/1"].setDouble(4.0);
        cmd["SetAnalog/AOut1"].setDouble(4.5);
        aout.array(0).setDouble(4.5);
        aout.array(1).setDouble(4.0);
        interface->incomingCommand(cmd);
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZOUTPUTS", aout))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZOUTPUTS", aout));
        QCOMPARE(instrument.aot[0], 4.5);
        QCOMPARE(instrument.aot[1], 4.0);

        cmd.setEmpty();
        cmd["SetAnalog/Parameters/Channel/Value"].setInt64(2);
        cmd["SetAnalog/Parameters/Value/Value"].setDouble(0.5);
        aout.array(2).setDouble(0.5);
        interface->incomingCommand(cmd);
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZOUTPUTS", aout))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZOUTPUTS", aout));
        QCOMPARE(instrument.aot[2], 0.5);

        cmd.setEmpty();
        cmd["Output/AOut1"].setDouble(1.5);
        aout.array(0).setDouble(1.5);
        interface->incomingCommand(cmd);
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZOUTPUTS", aout))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZOUTPUTS", aout));
        QCOMPARE(instrument.aot[0], 1.5);

        cmd.setEmpty();
        cmd["SetDigital/Parameters/Channel/Value"].setInt64(15);
        cmd["SetDigital/Parameters/Value/Value"].setBool(true);
        interface->incomingCommand(cmd);
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZDIGITAL", Variant::Root(0x8123)))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZDIGITAL", Variant::Root(0x8123)));
        QCOMPARE(instrument.dot, (std::uint_fast32_t) 0x8123);

        cmd.setEmpty();
        cmd["Output/Parameters/Name"].setString("DOut1");
        cmd["Output/Parameters/Value"].setBool(false);
        interface->incomingCommand(cmd);
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZDIGITAL", Variant::Root(0x8121)))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZDIGITAL", Variant::Root(0x8121)));
        QCOMPARE(instrument.dot, (std::uint_fast32_t) 0x8121);

        for (int i = 0; i < 30; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkVariableValues(logging, instrument, variables));
        QVERIFY(checkVariableValues(realtime, instrument, variables));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
