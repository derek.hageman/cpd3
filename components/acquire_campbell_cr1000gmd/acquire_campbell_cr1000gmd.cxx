/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_campbell_cr1000gmd.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

enum {
    TotalDigitalOutputs = 20, TotalAnalogOutputs = 8, TotalAnalogInputs = 32,
};

AcquireCampbellCR1000GMD::Configuration::Configuration() : start(FP::undefined()),
                                                           end(FP::undefined()),
                                                           pollInterval(1.0),
                                                           strictMode(true),
                                                           logOutputs(false),
                                                           analogNames(),
                                                           digitalNames(),
                                                           initializeAnalog(),
                                                           exitAnalog(),
                                                           initializeDigital(),
                                                           exitDigital(),
                                                           bypassAnalog(),
                                                           unbypassAnalog(),
                                                           bypassDigital(),
                                                           unbypassDigital(),
                                                           variables()
{ }

AcquireCampbellCR1000GMD::Configuration::Configuration(const Configuration &other,
                                                       double s,
                                                       double e) : start(s),
                                                                   end(e),
                                                                   pollInterval(other.pollInterval),
                                                                   strictMode(other.strictMode),
                                                                   logOutputs(other.logOutputs),
                                                                   analogNames(other.analogNames),
                                                                   digitalNames(other.digitalNames),
                                                                   initializeAnalog(
                                                                           other.initializeAnalog),
                                                                   exitAnalog(other.exitAnalog),
                                                                   initializeDigital(
                                                                           other.initializeDigital),
                                                                   exitDigital(other.exitDigital),
                                                                   bypassAnalog(other.bypassAnalog),
                                                                   unbypassAnalog(
                                                                           other.unbypassAnalog),
                                                                   bypassDigital(
                                                                           other.bypassDigital),
                                                                   unbypassDigital(
                                                                           other.unbypassDigital),
                                                                   variables(other.variables)
{ }

void AcquireCampbellCR1000GMD::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Campbell");
    instrumentMeta["Model"].setString("CR1000");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    realtimeDigitalStateUpdated = true;

    analogOutputValues.setUnit(SequenceName({}, "raw", "ZOUTPUTS"));
    digitalOutputValues.setUnit(SequenceName({}, "raw", "F2"));
    digitalOutputBits.setUnit(SequenceName({}, "raw", "ZDIGITAL"));

    lastInputs.write().setEmpty();

    updateChannel = 0;
}

AcquireCampbellCR1000GMD::Configuration::Configuration(const ValueSegment &other,
                                                       double s,
                                                       double e) : start(s),
                                                                   end(e),
                                                                   pollInterval(1.0),
                                                                   strictMode(true),
                                                                   logOutputs(false),
                                                                   analogNames(),
                                                                   digitalNames(),
                                                                   initializeAnalog(),
                                                                   exitAnalog(),
                                                                   initializeDigital(),
                                                                   exitDigital(),
                                                                   bypassAnalog(),
                                                                   unbypassAnalog(),
                                                                   bypassDigital(),
                                                                   unbypassDigital(),
                                                                   variables()
{
    setFromSegment(other);
}

AcquireCampbellCR1000GMD::Configuration::Configuration(const Configuration &under,
                                                       const ValueSegment &over,
                                                       double s,
                                                       double e) : start(s),
                                                                   end(e),
                                                                   pollInterval(under.pollInterval),
                                                                   strictMode(under.strictMode),
                                                                   logOutputs(under.logOutputs),
                                                                   analogNames(under.analogNames),
                                                                   digitalNames(under.digitalNames),
                                                                   initializeAnalog(
                                                                           under.initializeAnalog),
                                                                   exitAnalog(under.exitAnalog),
                                                                   initializeDigital(
                                                                           under.initializeDigital),
                                                                   exitDigital(under.exitDigital),
                                                                   bypassAnalog(under.bypassAnalog),
                                                                   unbypassAnalog(
                                                                           under.unbypassAnalog),
                                                                   bypassDigital(
                                                                           under.bypassDigital),
                                                                   unbypassDigital(
                                                                           under.unbypassDigital),
                                                                   variables(under.variables)
{
    setFromSegment(over);
}

void AcquireCampbellCR1000GMD::Configuration::addVariable(const Variant::Read &config,
                                                          qint64 defaultChannel,
                                                          const SequenceName::Component &defaultName)
{
    qint64 channel = config["Channel"].toInt64();
    if (!INTEGER::defined(channel)) {
        const auto &check = config["Channel"].toString();
        if (Util::equal_insensitive(check, "t")) {
            channel = -1;
        } else if (Util::equal_insensitive(check, "v")) {
            channel = -2;
        } else {
            channel = defaultChannel;
        }
    } else if (channel <= 0 || channel > TotalAnalogInputs) {
        return;
    } else {
        --channel;
    }
    if (channel < -2 || channel >= TotalAnalogInputs)
        return;

    auto name = config["Name"].toString();
    if (name.empty())
        name = defaultName;
    if (name.empty())
        return;

    OutputVariable variable;
    variable.channel = (int) channel;
    variable.name = std::move(name);
    variable.metadata.write().set(config["Metadata"]);
    variable.calibration = Variant::Composite::toCalibration(config["Calibration"]);

    variables.append(std::move(variable));
}

void AcquireCampbellCR1000GMD::Configuration::setAnalogValues(const Variant::Read &config,
                                                              QMap<int, double> &target)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        QStringList channels(config.toQString().split(QRegExp("[:;,]+")));
        for (int idx = 0, max = std::min<int>(channels.size(), TotalAnalogOutputs);
                idx < max;
                ++idx) {
            bool ok = false;
            double value = channels.at(idx).toDouble(&ok);
            if (!ok)
                continue;
            if (!FP::defined(value))
                target.remove(idx);
            else
                target.insert(idx, value);
        }
        break;
    }
    default: {
        auto children = config.toChildren();
        for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
            int index = add.integerKey();
            auto check = add.stringKey();
            if (!check.empty()) {
                index = -1;
                auto lookup = analogNames.find(check);
                if (lookup != analogNames.end())
                    index = lookup->second;
                if (index == -1) {
                    bool ok = false;
                    index = QString::fromStdString(check).toInt(&ok);
                    if (!ok)
                        index = -1;
                }
            }
            if (index < 0 || index >= TotalAnalogOutputs)
                continue;

            double value = add.value().toDouble();
            if (!FP::defined(value))
                target.remove(index);
            else
                target.insert(index, value);
        }
        break;
    }
    }
}

void AcquireCampbellCR1000GMD::Configuration::setDigitalValues(const Variant::Read &config,
                                                               QMap<int, bool> &target)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        QStringList channels(config.toQString().split(QRegExp("[:;,]+")));
        if (channels.size() == 1) {
            bool ok = false;
            quint64 outputs = channels.at(0).toULongLong(&ok, 16);
            for (int idx = 0; idx < 64; idx++) {
                target.insert(idx, (outputs & ((quint64) 1 << idx)) != 0);
            }
        } else {
            for (int idx = 0, max = std::min<int>(channels.size(), TotalDigitalOutputs);
                    idx < max;
                    ++idx) {
                bool value = false;
                if (channels.at(idx).toLower() == "on" || channels.at(idx).toLower() == "enabled") {
                    value = true;
                } else {
                    bool ok = false;
                    if (channels.at(idx).toInt(&ok) && ok)
                        value = true;
                    else if (!ok)
                        continue;
                }
                target.insert(idx, value);
            }
        }
        break;
    }
    case Variant::Type::Integer: {
        qint64 outputs = config.toInt64();
        if (INTEGER::defined(outputs)) {
            for (int idx = 0; idx < 63; idx++) {
                target.insert(idx, (outputs & ((qint64) 1 << idx)) != 0);
            }
        }
        break;
    }
    default: {
        auto children = config.toChildren();
        for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
            int index = add.integerKey();
            auto check = add.stringKey();
            if (!check.empty()) {
                index = -1;
                auto lookup = digitalNames.find(check);
                if (lookup != digitalNames.end())
                    index = lookup->second;
                if (index == -1) {
                    bool ok = false;
                    index = QString::fromStdString(check).toInt(&ok);
                    if (!ok)
                        index = -1;
                }
            }
            if (index < 0 || index >= TotalDigitalOutputs)
                continue;

            if (add.value().getType() != Variant::Type::Boolean)
                target.remove(index);
            else
                target.insert(index, add.value().toBool());
        }
        break;
    }
    }
}

static double sanitizeAnalogOutput(double in)
{
    return ::floor(in * 1E7 + 0.5) / 1E7;
}

void AcquireCampbellCR1000GMD::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["PollInterval"].exists()) {
        pollInterval = config["PollInterval"].toDouble();
        if (FP::defined(pollInterval) && pollInterval <= 0.0)
            pollInterval = FP::undefined();
    }
    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    if (config["LogOutputs"].exists()) {
        logOutputs = config["LogOutputs"].toBool();
    }


    if (config["Variables"].exists()) {
        variables.clear();
        switch (config["Variables"].getType()) {
        case Variant::Type::String: {
            QStringList names(config["Variables"].toQString()
                                                 .split(QRegExp("[\\s+;:,]"),
                                                        QString::SkipEmptyParts));
            for (int idx = 0, max = names.size(); idx < max; ++idx) {
                if (idx < 10) {
                    addVariable(Variant::Read::empty(), idx + 1, names.at(idx).toStdString());
                } else {
                    addVariable(Variant::Read::empty(), INTEGER::undefined(),
                                names.at(idx).toStdString());
                }
            }
            break;
        }
        default: {
            auto children = config["Variables"].toChildren();
            for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
                bool ok = false;
                qint64 channel = add.integerKey();
                {
                    auto check = add.stringKey();
                    if (!check.empty()) {
                        channel = QString::fromStdString(add.stringKey()).toLongLong(&ok);
                        if (!ok)
                            channel = INTEGER::undefined();
                    }
                }
                addVariable(add.value(), channel, add.stringKey());
            }
            break;
        }
        }
    }

    if (config["AnalogOutputs"].exists()) {
        analogNames.clear();
        for (auto add : config["AnalogOutputs"].toHash()) {
            if (add.first.empty())
                continue;
            qint64 channel = INTEGER::undefined();
            switch (add.second.getType()) {
            case Variant::Type::String: {
                bool ok = false;
                channel = add.second.toQString().toLongLong(&ok);
                if (!ok)
                    channel = INTEGER::undefined();
                break;
            }
            default:
                channel = add.second.toInt64();
                break;
            }
            if (channel < 0 || channel >= TotalAnalogOutputs)
                continue;

            analogNames[add.first] = (int) channel;
        }
    }

    if (config["DigitalOutputs"].exists()) {
        digitalNames.clear();
        for (auto add : config["DigitalOutputs"].toHash()) {
            if (add.first.empty())
                continue;
            qint64 channel = INTEGER::undefined();
            switch (add.second.getType()) {
            case Variant::Type::String: {
                bool ok = false;
                channel = add.second.toQString().toLongLong(&ok);
                if (!ok)
                    channel = INTEGER::undefined();
                break;
            }
            default:
                channel = add.second.toInt64();
                break;
            }
            if (channel < 0 || channel >= TotalDigitalOutputs)
                continue;

            digitalNames[add.first] = (int) channel;
        }
    }

    setAnalogValues(config["Initialize/Analog"], initializeAnalog);
    setAnalogValues(config["Exit/Analog"], exitAnalog);
    setDigitalValues(config["Initialize/Digital"], initializeDigital);
    setDigitalValues(config["Exit/Digital"], exitDigital);
    setAnalogValues(config["Bypass/Analog"], bypassAnalog);
    setAnalogValues(config["UnBypass/Analog"], unbypassAnalog);
    setDigitalValues(config["Bypass/Digital"], bypassDigital);
    setDigitalValues(config["UnBypass/Digital"], unbypassDigital);

    for (QMap<int, double>::iterator mod = initializeAnalog.begin(),
            endMod = initializeAnalog.end(); mod != endMod; ++mod) {
        mod.value() = sanitizeAnalogOutput(mod.value());
    }
    for (QMap<int, double>::iterator mod = exitAnalog.begin(), endMod = exitAnalog.end();
            mod != endMod;
            ++mod) {
        mod.value() = sanitizeAnalogOutput(mod.value());
    }
    for (QMap<int, double>::iterator mod = bypassAnalog.begin(), endMod = bypassAnalog.end();
            mod != endMod;
            ++mod) {
        mod.value() = sanitizeAnalogOutput(mod.value());
    }
    for (QMap<int, double>::iterator mod = unbypassAnalog.begin(), endMod = unbypassAnalog.end();
            mod != endMod;
            ++mod) {
        mod.value() = sanitizeAnalogOutput(mod.value());
    }
}


AcquireCampbellCR1000GMD::AcquireCampbellCR1000GMD(const ValueSegment::Transfer &configData,
                                                   const std::string &loggingContext)
        : FramedInstrument("cr1000gmd", loggingContext),
          lastRecordTime(FP::undefined()),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          responseState(RESP_PASSIVE_WAIT),
          autoprobeValidRecords(0)
{
    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
    configurationChanged();
}

void AcquireCampbellCR1000GMD::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void AcquireCampbellCR1000GMD::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireCampbellCR1000GMDComponent::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireCampbellCR1000GMD::AcquireCampbellCR1000GMD(const ComponentOptions &options,
                                                   const std::string &loggingContext)
        : FramedInstrument("cr1000gmd", loggingContext),
          lastRecordTime(FP::undefined()),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          responseState(RESP_PASSIVE_WAIT),
          autoprobeValidRecords(0)
{
    Q_UNUSED(options);

    setDefaultInvalid();

    config.append(Configuration());

    configurationChanged();
}

AcquireCampbellCR1000GMD::~AcquireCampbellCR1000GMD()
{
}


void AcquireCampbellCR1000GMD::logValue(double startTime,
                                        double endTime,
                                        SequenceName::Component name,
                                        Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireCampbellCR1000GMD::realtimeValue(double time,
                                             SequenceName::Component name,
                                             Variant::Root &&value)
{
    if (realtimeEgress == NULL)
        return;
    realtimeEgress->emplaceData(SequenceName({}, "raw", std::move(name)), std::move(value), time,
                                time + 1.0);
}

SequenceValue::Transfer AcquireCampbellCR1000GMD::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_campbell_cr1000gmd");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    for (QList<OutputVariable>::const_iterator variable = config.first().variables.constBegin(),
            end = config.first().variables.constEnd(); variable != end; ++variable) {
        result.emplace_back(SequenceName({}, "raw_meta", variable->name),
                            Variant::Root(variable->metadata), time, FP::undefined());
        if (result.back().write().metadataReal("Format").toString().empty()) {
            result.back().write().metadataReal("Format").setString("00.000");
        }
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        Variant::Composite::fromCalibration(result.back().write().metadataReal("Calibration"),
                                            variable->calibration);
        result.back().write().metadataReal("Channel").setInt64(variable->channel);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
        result.back().write().metadataReal("Realtime").hash("Name").setString(variable->name);
    }

    result.emplace_back(SequenceName({}, "raw_meta", "T"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Internal board temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Board"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "V"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Supply voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Supply"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "ZINPUTS"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back()
          .write()
          .metadataArray("Description")
          .setString("Raw input values from all analog channels");
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Count").setInt64(TotalAnalogInputs);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("0.000");
    result.back().write().metadataArray("Children").metadataReal("Units").setString("V");
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back().write().metadataArray("Realtime").hash("ColumnOrder").setInt64(1);
    result.back().write().metadataArray("Realtime").hash("Name").setString("Ain");

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    /* Always output this metadata, even if we're not logging the
     * actual values, so it gets updated as needed */

    result.emplace_back(SequenceName({}, "raw_meta", "ZOUTPUTS"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Description").setString("Analog output channel values");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("0.000");
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back().write().metadataArray("Realtime").hash("ColumnOrder").setInt64(3);
    result.back().write().metadataArray("Realtime").hash("Name").setString("Aout");
    result.back().write().metadataArray("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "F2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Description").setString("Digital output channel values");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Children").setType(Variant::Type::MetadataBoolean);
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back().write().metadataArray("Realtime").hash("ColumnOrder").setInt64(2);
    result.back().write().metadataArray("Realtime").hash("Name").setString("Dout");
    result.back()
          .write()
          .metadataArray("Realtime")
          .hash("Translation")
          .hash("TRUE")
          .setString("ON");
    result.back()
          .write()
          .metadataArray("Realtime")
          .hash("Translation")
          .hash("FALSE")
          .setString("OFF");

    return result;
}

SequenceValue::Transfer AcquireCampbellCR1000GMD::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_campbell_cr1000gmd");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());


    result.emplace_back(SequenceName({}, "raw_meta", "ZDIGITAL"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataInteger("Format").setString("FFFFFFFF");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataInteger("Realtime").hash("Name").setString("Digital");
    result.back().write().metadataInteger("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "ZINDEX"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Description").setString("Output index display");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Children").metadataInteger("Format").setString("000");
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back().write().metadataArray("Realtime").hash("ColumnOrder").setInt64(0);
    result.back().write().metadataArray("Realtime").hash("Name").setString("Channel");
    result.back().write().metadataArray("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataString("Realtime").hash("Hide").setBool(true);
    /*result.back().write().metadataString("Realtime").hash("Name").
        setString(std::string());
    result.back().write().metadataString("Realtime").hash("Units").
        setString(std::string());
    result.back().write().metadataString("Realtime").hash("FullLine").
        setBool(true);        
    result.back().write().metadataString("Realtime").hash("Page").
        setInt64(0);
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("Run").setString(QObject::tr("", "run state string"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("InvalidRecord").setString(QObject::tr("NO COMMS: Invalid response"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("Timeout").setString(QObject::tr("NO COMMS: Timeout"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractive").setString(QObject::tr("STARTING COMMS"));*/


    /* Not really metadata, but it's fixed so output it here anyway */
    Variant::Root output;
    for (std::size_t i = 0; i < TotalAnalogInputs; i++) {
        output.write().array(i).setInteger(i + 1);
    }
    result.emplace_back(SequenceName({}, "raw", "ZINDEX"), std::move(output), time,
                        FP::undefined());

    return result;
}

SequenceMatch::Composite AcquireCampbellCR1000GMD::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "F2");
    sel.append({}, {}, "ZDIGITAL");
    sel.append({}, {}, "ZINDEX");
    sel.append({}, {}, "ZSTATE");
    return sel;
}

bool AcquireCampbellCR1000GMD::issueNextAnalogUpdate(double time)
{
    if (analogUpdate.isEmpty())
        return false;

    Q_ASSERT(responseState == RESP_INTERACTIVE_RUN_READ_STATE ||
                     responseState == RESP_INTERACTIVE_RUN_WRITE_ANALOG ||
                     responseState == RESP_INTERACTIVE_RUN_WRITE_DIGITAL ||
                     responseState == RESP_INTERACTIVE_RUN_WAIT);

    QMap<int, double>::const_iterator next = analogUpdate.lowerBound(updateChannel);
    for (; next != analogUpdate.constEnd() && !FP::defined(next.value()); ++next) { }
    if (next == analogUpdate.constEnd())
        return false;
    updateChannel = next.key() + 1;

    int index = next.key();
    double value = next.value();
    Q_ASSERT(index >= 0 && FP::defined(value));
    analogUpdate.erase(analogUpdate.begin());

    if (controlStream != NULL) {
        Util::ByteArray data("SAO,");
        data += QByteArray::number(index);
        data.push_back(',');
        data += QByteArray::number(value, 'f', 6);
        data.push_back('\r');
        controlStream->writeControl(data);
    }

    responseState = RESP_INTERACTIVE_RUN_WRITE_ANALOG;
    if (FP::defined(time))
        timeoutAt(time + 1.0);
    return true;
}

bool AcquireCampbellCR1000GMD::issueNextDigitalUpdate(double time)
{
    if (digitalUpdate.isEmpty())
        return false;

    Q_ASSERT(responseState == RESP_INTERACTIVE_RUN_READ_STATE ||
                     responseState == RESP_INTERACTIVE_RUN_WRITE_ANALOG ||
                     responseState == RESP_INTERACTIVE_RUN_WRITE_DIGITAL ||
                     responseState == RESP_INTERACTIVE_RUN_WAIT);

    qint64 bits = digitalOutputBits.read().toInt64();
    if (!INTEGER::defined(bits))
        bits = 0;
    for (QMap<int, bool>::const_iterator add = digitalUpdate.constBegin(),
            endAdd = digitalUpdate.constEnd(); add != endAdd; ++add) {
        qint64 mask = (Q_INT64_C(1) << (qint64) add.key());

        if (add.value()) {
            bits |= mask;
        } else {
            bits &= ~mask;
        }
    }
    digitalUpdate.clear();

    if (controlStream) {
        Util::ByteArray data("SDO,");
        data += QByteArray::number(bits, 16).rightJustified(8, '0');
        data.push_back('\r');
        controlStream->writeControl(std::move(data));
    }

    responseState = RESP_INTERACTIVE_RUN_WRITE_DIGITAL;
    if (FP::defined(time))
        timeoutAt(time + 1.0);
    return true;

}

bool AcquireCampbellCR1000GMD::advanceReadUpdate(double time)
{
    Q_ASSERT(responseState == RESP_INTERACTIVE_RUN_READ_STATE ||
                     responseState == RESP_INTERACTIVE_RUN_WRITE_ANALOG ||
                     responseState == RESP_INTERACTIVE_RUN_WRITE_DIGITAL ||
                     responseState == RESP_INTERACTIVE_RUN_WAIT);

    updateChannel = 0;

    double interval = config.first().pollInterval;
    if (FP::defined(interval) && interval > 0.0) {
        double nextTime;
        if (FP::defined(lastRecordTime) && lastRecordTime < time)
            nextTime = lastRecordTime + interval;
        else
            nextTime = time + interval;
        if (nextTime > time) {
            responseState = RESP_INTERACTIVE_RUN_WAIT;
            timeoutAt(nextTime);
            return false;
        }
    }

    if (controlStream != NULL) {
        controlStream->writeControl("RST\r");
    }

    responseState = RESP_INTERACTIVE_RUN_READ_STATE;
    timeoutAt(time + 2.0);
    return true;
}


void AcquireCampbellCR1000GMD::outputMetadata(double time)
{
    if (!haveEmittedLogMeta && loggingEgress != NULL) {
        haveEmittedLogMeta = true;

        loggingEgress->incomingData(buildLogMeta(time));
    }

    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;

        SequenceValue::Transfer meta = buildLogMeta(time);
        Util::append(buildRealtimeMeta(time), meta);
        realtimeEgress->incomingData(std::move(meta));
    }
}

void AcquireCampbellCR1000GMD::processDigitalState(double frameTime, qint64 bits)
{
    for (int b = 0; b < TotalDigitalOutputs; b++) {
        bool current = (bits & (Q_INT64_C(1) << (qint64) b)) != 0;

        QMap<int, bool>::const_iterator check = digitalTarget.find(b);
        if (check != digitalTarget.constEnd() && check.value() != current) {
            digitalUpdate.insert(b, check.value());
        }
    }

    if (digitalOutputBits.read().toInt64() == bits) {
        if (realtimeDigitalStateUpdated && realtimeEgress != NULL) {
            realtimeEgress->incomingData(digitalOutputValues);
            realtimeEgress->incomingData(digitalOutputBits);

            realtimeDigitalStateUpdated = false;
        }
        return;
    }

    realtimeDigitalStateUpdated = true;

    if (persistentEgress != NULL &&
            config.first().logOutputs &&
            FP::defined(digitalOutputValues.getStart())) {
        digitalOutputValues.setEnd(frameTime);
        persistentEgress->incomingData(digitalOutputValues);
    }

    for (int b = 0; b < 64; b++) {
        bool current = (bits & (Q_INT64_C(1) << (qint64) b)) != 0;

        digitalOutputValues.write().array(b).setBool(current);
    }
    digitalOutputBits.write().setInt64(bits);

    digitalOutputValues.setStart(frameTime);
    digitalOutputValues.setEnd(FP::undefined());
    digitalOutputBits.setStart(frameTime);
    digitalOutputBits.setEnd(FP::undefined());
    if (realtimeEgress != NULL) {
        realtimeEgress->incomingData(digitalOutputValues);
        realtimeEgress->incomingData(digitalOutputBits);

        realtimeDigitalStateUpdated = false;
    }

    if (config.first().logOutputs) {
        persistentValuesUpdated();
    }
}

int AcquireCampbellCR1000GMD::processRecord(const Util::ByteArray &line, double frameTime)
{
    if (line.size() < 3)
        return 100;

    auto fields = Util::as_deque(line.split(','));
    if (fields.empty())
        return 101;

    auto field = fields.front();
    fields.pop_front();
    bool ok = false;

    if (field == "STA") {
        if (fields.empty()) return 1000;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (field.size() != 8) return 1001;
        qint64 bits = field.parse_u64(&ok, 16);
        if (!ok) return 1002;

        lastInputs.write().setEmpty();
        for (int i = 0; i < TotalAnalogInputs; i++) {
            if (fields.empty()) return 1100 + (10 * (i + 1));
            field = fields.front().string_trimmed();
            fields.pop_front();
            if (!field.string_equal_insensitive("nan")) {
                Variant::Root channel(field.parse_real(&ok));
                if (!ok) return 1101 + (10 * (i + 1));
                if (!FP::defined(channel.read().toReal())) return 1102 + (10 * (i + 1));

                remap("ZIN" + std::to_string(i + 1), channel);
                lastInputs.write().array(i).set(channel);
            } else {
                Variant::Root channel(FP::undefined());
                remap("ZIN" + std::to_string(i + 1), channel);
                lastInputs.write().array(i).set(channel);
            }
        }
        remap("ZINPUTS", lastInputs);

        if (fields.empty()) return 1003;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root V(field.parse_real(&ok));
        if (!ok) return 1004;
        if (!FP::defined(V.read().toReal())) return 1005;
        remap("V", V);

        if (fields.empty()) return 1006;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root T(field.parse_real(&ok));
        if (!ok) return 1007;
        if (!FP::defined(T.read().toReal())) return 1008;
        remap("T", T);

        if (config.first().strictMode) {
            if (!fields.empty())
                return 1009;
        }

        double startTime = lastRecordTime;
        double endTime = frameTime;
        lastRecordTime = frameTime;

        outputMetadata(frameTime);
        processDigitalState(frameTime, bits);
        logValue(startTime, endTime, "ZINPUTS", Variant::Root(lastInputs));

        for (QList<OutputVariable>::const_iterator var = config.first().variables.constBegin(),
                endVar = config.first().variables.constEnd(); var != endVar; ++var) {
            Variant::Read input;
            if (var->channel == -1) {
                input = T;
            } else if (var->channel == -2) {
                input = V;
            } else {
                Q_ASSERT(var->channel >= 0);
                input = lastInputs.read().array(var->channel);
            }

            Variant::Root outputValue(var->calibration.apply(input.toDouble()));
            remap(var->name, outputValue);
            logValue(startTime, endTime, var->name, std::move(outputValue));
        }
        logValue(startTime, endTime, "T", std::move(T));
        logValue(startTime, endTime, "V", std::move(V));
        logValue(startTime, endTime, "F1", Variant::Root(Variant::Type::Flags));

        switch (responseState) {
        case RESP_INTERACTIVE_RUN_READ_STATE:
            if (!analogUpdate.empty() || !digitalUpdate.empty()) {
                updateChannel = 0;
                discardData(frameTime + 1.0);
                timeoutAt(frameTime + 10.0);
            } else {
                advanceReadUpdate(frameTime);
            }
            break;
        case RESP_INTERACTIVE_RUN_WAIT:
            if (issueNextAnalogUpdate(frameTime))
                break;
            updateChannel = 0;
            if (issueNextDigitalUpdate(frameTime))
                break;
            updateChannel = 0;
            advanceReadUpdate(frameTime);
            break;

        default:
            break;
        }
        return 0;
    } else if (field == "SAA") {
        if (fields.empty()) return 2000;
        field = fields.front().string_trimmed();
        fields.pop_front();
        int index = field.string_trimmed().parse_i32(&ok);
        if (!ok) return 2001;
        if (index < 0 || index >= TotalAnalogOutputs) return 2002;

        if (fields.empty()) return 2003;
        field = fields.front().string_trimmed();
        fields.pop_front();
        double value = field.parse_real(&ok);
        if (!ok) return 2004;
        if (!FP::defined(value)) return 2005;

        if (config.first().strictMode) {
            if (!fields.empty())
                return 2006;
        }

        {
            QMap<int, double>::iterator check = analogUpdate.find(index);
            if (check != analogUpdate.end() && qFuzzyCompare(check.value(), value)) {
                analogUpdate.erase(check);
            }
        }
        {
            QMap<int, double>::const_iterator check = analogTarget.find(index);
            if (check != analogTarget.constEnd() && !qFuzzyCompare(check.value(), value)) {
                analogUpdate.insert(index, check.value());
            }
        }

        outputMetadata(frameTime);

        if (persistentEgress != NULL &&
                config.first().logOutputs &&
                FP::defined(analogOutputValues.getStart())) {
            analogOutputValues.setEnd(frameTime);
            persistentEgress->incomingData(analogOutputValues);
        }

        analogOutputValues.write().array(index).setDouble(value);
        analogOutputValues.setStart(frameTime);
        analogOutputValues.setEnd(FP::undefined());
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(analogOutputValues);
        }

        if (config.first().logOutputs) {
            persistentValuesUpdated();
        }

        switch (responseState) {
        case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
            discardData(frameTime + 1.0);
            timeoutAt(frameTime + 10.0);
            break;
        case RESP_INTERACTIVE_RUN_WAIT:
            if (issueNextAnalogUpdate(frameTime))
                break;
            updateChannel = 0;
            if (issueNextDigitalUpdate(frameTime))
                break;
            advanceReadUpdate(frameTime);
            break;

        default:
            break;
        }
        return 0;
    } else if (field == "SDA") {
        if (fields.empty()) return 3000;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (field.size() != 8) return 3001;
        qint64 bits = field.parse_u64(&ok, 16);
        if (!ok) return 3002;

        if (config.first().strictMode) {
            if (!fields.empty())
                return 3003;
        }

        outputMetadata(frameTime);
        processDigitalState(frameTime, bits);

        switch (responseState) {
        case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
            discardData(frameTime + 1.0);
            timeoutAt(frameTime + 10.0);
            break;
        case RESP_INTERACTIVE_RUN_WAIT:
            advanceReadUpdate(frameTime);
            break;

        default:
            break;
        }
        return 0;
    } else if (field == "ERR") {
        if (fields.empty())
            return 4000;
        lastError = QString::fromLatin1(fields.front().toQByteArrayRef());
        fields.pop_front();
        return 1;
    } else {
        return 102;
    }

    Q_ASSERT(false);
    return 99;
}

void AcquireCampbellCR1000GMD::invalidateLogValues(double frameTime)
{
    autoprobeValidRecords = 0;
    lastInputs.write().setEmpty();
    loggingLost(frameTime);
}

void AcquireCampbellCR1000GMD::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    for (QMap<int, double>::const_iterator add = config.first().initializeAnalog.constBegin(),
            endAdd = config.first().initializeAnalog.constEnd(); add != endAdd; ++add) {
        if (!FP::defined(add.value()))
            continue;
        QMap<int, double>::const_iterator check = analogUpdate.constFind(add.key());
        if (check == analogUpdate.constEnd() || !FP::defined(check.value())) {
            analogUpdate.insert(add.key(), add.value());
        }

        check = analogTarget.constFind(add.key());
        if (check == analogTarget.constEnd() || !FP::defined(check.value())) {
            analogTarget.insert(add.key(), add.value());
        }
    }

    for (QMap<int, bool>::const_iterator add = config.first().initializeDigital.constBegin(),
            endAdd = config.first().initializeDigital.constEnd(); add != endAdd; ++add) {
        QMap<int, bool>::const_iterator check = digitalUpdate.constFind(add.key());
        if (check == digitalUpdate.constEnd()) {
            digitalUpdate.insert(add.key(), add.value());
        }

        check = digitalTarget.constFind(add.key());
        if (check == digitalTarget.constEnd()) {
            digitalTarget.insert(add.key(), add.value());
        }
    }

    auto variables = instrumentMeta["VariableIndex"];
    variables.remove();
    for (const auto &variable: config.first().variables) {
        variables.array(static_cast<std::size_t>(variable.channel)).setString(variable.name);
    }

    auto output = instrumentMeta["OutputIndex"];
    output.remove();
    for (const auto &channel: config.first().analogNames) {
        output.array(static_cast<std::size_t>(channel.second)).setString(channel.first);
    }

    auto digital = instrumentMeta["DigitalIndex"];
    digital.remove();
    for (const auto &channel: config.first().digitalNames) {
        digital.array(static_cast<std::size_t>(channel.second)).setString(channel.first);
    }

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    sourceMetadataUpdated();
}

void AcquireCampbellCR1000GMD::configurationAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

void AcquireCampbellCR1000GMD::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 5) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_PASSIVE_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                timeoutAt(frameTime + config.first().pollInterval + 2.0);

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(
                            SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                          FP::undefined()));
                }
            }
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }
    case RESP_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);
            responseState = RESP_PASSIVE_RUN;
            generalStatusUpdated();

            timeoutAt(frameTime + config.first().pollInterval + 2.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                      FP::undefined()));
            }
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN_READ_STATE:
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
    case RESP_INTERACTIVE_RUN_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code << "in state" << responseState;

            Variant::Write info = Variant::Write::empty();

            switch (responseState) {
            case RESP_PASSIVE_WAIT:
                info.hash("ResponseState").setString("PassiveRun");
                break;
            case RESP_INTERACTIVE_RUN_READ_STATE:
                info.hash("ResponseState").setString("InteractiveRunReadState");
                break;
            case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
                info.hash("ResponseState").setString("InteractiveRunWriteAnalog");
                break;
            case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
                info.hash("ResponseState").setString("InteractiveRunWriteDigital");
                break;
            case RESP_INTERACTIVE_RUN_WAIT:
                info.hash("ResponseState").setString("InteractiveRunWait");
                break;
            default:
                Q_ASSERT(false);
                break;
            }

            if (responseState == RESP_PASSIVE_RUN) {
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
                info.hash("ResponseState").setString("PassiveRun");
            } else {
                responseState = RESP_INTERACTIVE_START_READ_STATE;
                timeoutAt(frameTime + 2.0);
                discardData(frameTime + 0.5);
                info.hash("ResponseState").setString("Run");
            }
            generalStatusUpdated();

            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            if (code == 1) {
                info.hash("ErrorString").setString(lastError);
                event(frameTime,
                      QObject::tr("Instrument error: %1.  Communications dropped.").arg(lastError),
                      true, info);
            } else {
                event(frameTime,
                      QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                              code), true, info);
            }

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case RESP_INTERACTIVE_START_READ_STATE: {
        if (!frame.string_start("STA,"))
            break;

        int code = processRecord(frame, frameTime);
        if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("StartReadState");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the initial state (code %1).").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
            break;
        }

        qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("InteractiveStartReadState");
            event(frameTime, QObject::tr("Communications established."), false, info);
        }

        analogUpdate = analogTarget;
        digitalUpdate = digitalTarget;

        responseState = RESP_INTERACTIVE_RUN_WAIT;
        timeoutAt(frameTime + 0.5);
        discardData(FP::undefined());
        break;
    }

    default:
        break;
    }
}

void AcquireCampbellCR1000GMD::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_READ_STATE:
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
        qCDebug(log) << "Timeout in interactive state" << responseState << "at"
                     << Logging::time(frameTime);

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        {
            Variant::Write info = Variant::Write::empty();
            switch (responseState) {
            case RESP_INTERACTIVE_RUN_READ_STATE:
                info.hash("ResponseState").setString("InteractiveRunReadState");
                break;
            case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
                info.hash("ResponseState").setString("InteractiveRunWriteAnalog");
                break;
            case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
                info.hash("ResponseState").setString("InteractiveRunWriteDigital");
                break;
            default:
                Q_ASSERT(false);
                break;
            }
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        timeoutAt(FP::undefined());
        discardData(frameTime + 0.5);
        generalStatusUpdated();

        invalidateLogValues(frameTime);
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_RUN_WAIT:
        timeoutAt(frameTime + 2.0);
        responseState = RESP_INTERACTIVE_RUN_READ_STATE;

        if (controlStream != NULL) {
            controlStream->writeControl("RST\r");
        }
        break;

    case RESP_INTERACTIVE_START_READ_STATE:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        timeoutAt(frameTime + 30.0);
        responseState = RESP_INTERACTIVE_START_READ_STATE;
        discardData(frameTime + 0.5);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        invalidateLogValues(frameTime);
        break;

    default:
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireCampbellCR1000GMD::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    switch (responseState) {
    case RESP_INTERACTIVE_START_READ_STATE:
        timeoutAt(frameTime + 2.0);
        responseState = RESP_INTERACTIVE_START_READ_STATE;

        if (controlStream != NULL) {
            controlStream->writeControl("RST\r");
        }
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        if (controlStream != NULL) {
            controlStream->writeControl("\r\r\r\r\r\r\rRST\rRST\rRST\rRST\rRST\rRST\r");
        }
        responseState = RESP_INTERACTIVE_START_READ_STATE;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_RUN_READ_STATE:
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
        if (issueNextAnalogUpdate(frameTime))
            break;
        updateChannel = 0;
        if (issueNextDigitalUpdate(frameTime))
            break;
        advanceReadUpdate(frameTime);
        break;
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
        advanceReadUpdate(frameTime);
        break;

    default:
        break;
    }
}

void AcquireCampbellCR1000GMD::shutdownInProgress(double frameTime)
{
    Q_UNUSED(frameTime);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_READ_STATE:
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
    case RESP_INTERACTIVE_RUN_WAIT:
        /* Just issue all the commands needed, without waiting for
         * acknowledgments */

        analogUpdate = config.first().exitAnalog;
        digitalUpdate = config.first().exitDigital;

        updateChannel = 0;
        while (issueNextAnalogUpdate(FP::undefined())) { }
        updateChannel = 0;
        while (issueNextDigitalUpdate(FP::undefined())) { }
        break;

    default:
        /* No interactive communications, so no action needed */
        break;
    }
}

void AcquireCampbellCR1000GMD::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frameTime);
    Q_UNUSED(frame);
}

int AcquireCampbellCR1000GMD::convertAnalogChannel(const Variant::Read &value)
{
    {
        auto i = Variant::Composite::toInteger(value);
        if (INTEGER::defined(i) && i >= 0 && i < TotalAnalogOutputs)
            return (int) i;
    }

    Q_ASSERT(!config.isEmpty());

    const auto &check = value.toString();
    if (!check.empty()) {
        auto i = config.front().analogNames.find(check);
        if (i != config.front().analogNames.end() &&
                i->second >= 0 &&
                i->second < TotalAnalogOutputs)
            return i->second;
    }

    return -1;
}

int AcquireCampbellCR1000GMD::convertDigitalChannel(const Variant::Read &value)
{
    {
        auto i = Variant::Composite::toInteger(value);
        if (INTEGER::defined(i) && i >= 0 && i < TotalDigitalOutputs)
            return (int) i;
    }

    Q_ASSERT(!config.isEmpty());

    const auto &check = value.toString();
    if (!check.empty()) {
        auto i = config.front().digitalNames.find(check);
        if (i != config.front().analogNames.end() &&
                i->second >= 0 &&
                i->second < TotalDigitalOutputs)
            return i->second;
    }

    return -1;
}

QHash<int, double> AcquireCampbellCR1000GMD::convertAnalogMultiple(const Variant::Read &value)
{
    switch (value.getType()) {
    case Variant::Type::Hash: {
        QHash<int, double> result;
        for (auto add : value.toHash()) {
            double v = Variant::Composite::toNumber(add.second);
            if (!FP::defined(v))
                v = Variant::Composite::toNumber(add.second.hash("Value"));
            if (!FP::defined(v))
                continue;

            int index = -1;
            auto lookup = config.front().analogNames.find(add.first);
            if (lookup != config.front().analogNames.end())
                index = lookup->second;
            if (index == -1) {
                bool ok = false;
                index = QString::fromStdString(add.first).toInt(&ok);
                if (!ok)
                    index = -1;
            }
            if (index < 0 || index >= TotalAnalogOutputs)
                continue;

            result.insert(index, v);
        }
        return result;
    }
    case Variant::Type::Array: {
        auto children = value.toArray();
        QHash<int, double> result;
        for (int idx = 0, max = std::min<int>(children.size(), TotalAnalogOutputs);
                idx < max;
                ++idx) {
            double v = Variant::Composite::toNumber(children[idx]);
            if (!FP::defined(v))
                v = Variant::Composite::toNumber(children[idx].hash("Value"));
            if (!FP::defined(v))
                continue;
            result.insert(idx, v);
        }
        return result;
    }
    case Variant::Type::String: {
        QStringList channels(value.toQString().split(QRegExp("[:;,]+")));
        QHash<int, double> result;
        for (int idx = 0, max = std::min<int>(channels.size(), TotalAnalogOutputs);
                idx < max;
                ++idx) {
            bool ok = false;
            double v = channels.at(idx).toDouble(&ok);
            if (!ok || FP::defined(v))
                continue;
            result.insert(idx, v);
        }
        return result;
    }

    default:
        break;
    }

    return QHash<int, double>();
}

static bool getValidBoolean(const Variant::Read &v, bool &ok)
{
    if (v.getType() == Variant::Type::Boolean) {
        ok = true;
        return v.toBool();
    } else if (v.getType() == Variant::Type::String) {
        const auto &check = v.toString();
        if (!check.empty()) {
            ok = true;
            if (Util::equal_insensitive(check, "on", "true") ||
                    (QString::fromStdString(check).toInt(&ok) && ok))
                return true;
            return false;
        }
    }

    {
        qint64 i = v.toInt64();
        if (INTEGER::defined(i)) {
            ok = true;
            return i != 0;
        }
    }

    {
        double d = v.toDouble();
        if (FP::defined(d)) {
            ok = true;
            return d != 0.0;
        }
    }

    return false;
}

static qint64 getValidMask(const Variant::Read &v)
{
    if (v.getType() != Variant::Type::Integer)
        return INTEGER::undefined();
    return v.toInt64();
}

QHash<int, bool> AcquireCampbellCR1000GMD::convertDigitalMultiple(const Variant::Read &value)
{
    switch (value.getType()) {
    case Variant::Type::Hash: {
        QHash<int, bool> result;
        for (auto add : value.toHash()) {
            bool ok = false;
            bool v = getValidBoolean(add.second, ok);
            if (!ok)
                v = getValidBoolean(add.second.hash("Value"), ok);
            if (!ok)
                continue;

            int index = -1;
            auto lookup = config.front().digitalNames.find(add.first);
            if (lookup != config.front().digitalNames.end())
                index = lookup->second;
            if (index == -1) {
                index = QString::fromStdString(add.first).toInt(&ok);
                if (!ok)
                    index = -1;
            }
            if (index < 0 || index >= TotalDigitalOutputs)
                continue;

            result.insert(index, v);
        }
        return result;
    }
    case Variant::Type::Array: {
        auto children = value.toArray();
        QHash<int, bool> result;
        for (int idx = 0, max = std::min<int>(children.size(), TotalDigitalOutputs);
                idx < max;
                ++idx) {
            bool ok = false;
            bool v = getValidBoolean(children[idx], ok);
            if (!ok)
                v = getValidBoolean(children[idx].hash("Value"), ok);
            if (!ok)
                continue;
            result.insert(idx, v);
        }
        return result;
    }
    case Variant::Type::String: {
        QStringList channels(value.toQString().split(QRegExp("[:;,]+")));
        QHash<int, bool> result;
        for (int idx = 0, max = std::min<int>(channels.size(), TotalDigitalOutputs);
                idx < max;
                ++idx) {
            QString check(channels.at(idx).toLower());
            if (check.isEmpty())
                continue;
            bool v = false;
            bool ok = false;
            if (check == "on" || check == "true" || (check.toInt(&ok) && ok))
                v = true;
            result.insert(idx, v);
        }
        return result;
    }

    default:
        break;
    }

    return QHash<int, bool>();
}

void AcquireCampbellCR1000GMD::genericOutputCommand(const Variant::Read &command)
{
    if (command.getType() != Variant::Type::Hash) {
        switch (command.getType()) {
        case Variant::Type::Real: {
            double v = command.toDouble();
            auto index = config.front().analogNames.find(SequenceName::Component());
            if (FP::defined(v) &&
                    index != config.front().analogNames.end() &&
                    index->second >= 0 &&
                    index->second < TotalAnalogOutputs) {
                v = sanitizeAnalogOutput(v);
                analogUpdate.insert(index->second, v);
                analogTarget.insert(index->second, v);
            }
            break;
        }

        case Variant::Type::Integer: {
            qint64 bits = command.toInt64();
            if (INTEGER::defined(bits)) {
                for (int i = 0; i < 63; i++) {
                    qint64 c = (qint64) 1 << i;
                    bool value = (bits & c) != 0;
                    digitalUpdate.insert(i, value);
                    digitalTarget.insert(i, value);
                }
            }
            break;
        }

        default: {
            bool v = command.toBool();
            auto index = config.front().digitalNames.find(SequenceName::Component());
            if (index != config.front().digitalNames.end() &&
                    index->second >= 0 &&
                    index->second < TotalDigitalOutputs) {
                digitalUpdate.insert(index->second, v);
                digitalTarget.insert(index->second, v);
            }
            break;
        }
        }
        return;
    }
    int index = convertAnalogChannel(command.hash("Parameters").hash("Channel").hash("Value"));
    if (index == -1) {
        index = convertAnalogChannel(command.hash("Parameters").hash("Channel"));
    }
    if (index == -1) {
        index = convertAnalogChannel(command.hash("Channel"));
    }
    if (index == -1) {
        index = convertAnalogChannel(command.hash("Parameters").hash("Index").hash("Value"));
    }
    if (index == -1) {
        index = convertAnalogChannel(command.hash("Parameters").hash("Index"));
    }
    if (index == -1) {
        index = convertAnalogChannel(command.hash("Index"));
    }
    if (index != -1) {
        double value = FP::undefined();
        if (command.hash("Parameters").hash("Value").hash("Value").getType() ==
                Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").hash("Value").toDouble();
        }
        if (!FP::defined(value) &&
                command.hash("Parameters").hash("Value").getType() == Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").toDouble();
        }
        if (!FP::defined(value) && command.hash("Value").getType() == Variant::Type::Real) {
            value = command.hash("Value").toDouble();
        }
        if (FP::defined(value)) {
            value = sanitizeAnalogOutput(value);
            analogUpdate.insert(index, value);
            analogTarget.insert(index, value);
            return;
        }
    }

    index = convertDigitalChannel(command.hash("Parameters").hash("Channel").hash("Value"));
    if (index == -1) {
        index = convertDigitalChannel(command.hash("Parameters").hash("Channel"));
    }
    if (index == -1) {
        index = convertDigitalChannel(command.hash("Channel"));
    }
    if (index == -1) {
        index = convertDigitalChannel(command.hash("Parameters").hash("Index").hash("Value"));
    }
    if (index == -1) {
        index = convertDigitalChannel(command.hash("Parameters").hash("Index"));
    }
    if (index == -1) {
        index = convertDigitalChannel(command.hash("Index"));
    }
    if (index != -1) {
        if (command.hash("Parameters").hash("Value").hash("Value").getType() ==
                Variant::Type::Boolean) {
            bool value = command.hash("Parameters").hash("Value").hash("Value").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
        if (command.hash("Parameters").hash("Value").getType() == Variant::Type::Boolean) {
            bool value = command.hash("Parameters").hash("Value").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
        if (command.hash("Value").getType() == Variant::Type::Boolean) {
            bool value = command.hash("Value").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
    }

    auto name = command.hash("Parameters").hash("Name").hash("Value").toString();
    if (name.empty()) {
        name = command.hash("Parameters").hash("Name").toString();
    }
    if (name.empty()) {
        name = command.hash("Name").toString();
    }

    {
        double value = FP::undefined();
        if (command.hash("Parameters").hash("Value").hash("Value").getType() ==
                Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").hash("Value").toDouble();
        }
        if (!FP::defined(value) &&
                command.hash("Parameters").hash("Value").getType() == Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").toDouble();
        }
        if (!FP::defined(value) && command.hash("Value").getType() == Variant::Type::Real) {
            value = command.hash("Value").toDouble();
        }
        if (!FP::defined(value) &&
                command.hash("Parameters").hash("Output").hash("Value").getType() ==
                        Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").hash("Value").toDouble();
        }
        if (!FP::defined(value) &&
                command.hash("Parameters").hash("Output").getType() == Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").toDouble();
        }
        if (!FP::defined(value) && command.hash("Value").getType() == Variant::Type::Real) {
            value = command.hash("Output").toDouble();
        }
        if (FP::defined(value)) {
            auto i = config.front().analogNames.find(name);
            if (i != config.front().analogNames.end() &&
                    i->second >= 0 &&
                    i->second < TotalAnalogOutputs) {
                analogUpdate.insert(i->second, value);
                analogTarget.insert(i->second, value);
                return;
            }
        }
    }

    index = -1;
    auto lookup = config.front().digitalNames.find(name);
    if (lookup != config.front().digitalNames.end())
        index = lookup->second;
    if (index >= 0 && index < TotalDigitalOutputs) {
        if (command.hash("Parameters").hash("Value").hash("Value").getType() ==
                Variant::Type::Boolean) {
            bool value = command.hash("Parameters").hash("Value").hash("Value").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
        if (command.hash("Parameters").hash("Value").getType() == Variant::Type::Boolean) {
            bool value = command.hash("Parameters").hash("Value").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
        if (command.hash("Value").getType() == Variant::Type::Boolean) {
            bool value = command.hash("Value").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
        if (command.hash("Parameters").hash("Output").hash("Value").getType() ==
                Variant::Type::Boolean) {
            bool value = command.hash("Parameters").hash("Output").hash("Value").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
        if (command.hash("Parameters").hash("Output").getType() == Variant::Type::Boolean) {
            bool value = command.hash("Parameters").hash("Output").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
        if (command.hash("Output").getType() == Variant::Type::Boolean) {
            bool value = command.hash("Output").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
    }

    for (auto check : command.toHash()) {
        index = -1;
        lookup = config.front().analogNames.find(check.first);
        if (lookup != config.front().analogNames.end())
            index = lookup->second;
        if (index != -1) {
            double value = Variant::Composite::toNumber(check.second);
            if (!FP::defined(value))
                value = Variant::Composite::toNumber(check.second.hash("Value"));
            if (FP::defined(value)) {
                value = sanitizeAnalogOutput(value);
                analogUpdate.insert(index, value);
                analogTarget.insert(index, value);
                continue;
            }
        }

        index = -1;
        lookup = config.front().digitalNames.find(check.first);
        if (lookup != config.front().digitalNames.end())
            index = lookup->second;
        if (index != -1) {
            bool value = check.second.toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            continue;
        }

        bool ok = false;
        index = QString::fromStdString(check.first).toInt(&ok);
        if (!ok)
            continue;

        auto base = check.second;
        if (base.getType() == Variant::Type::Hash)
            base = base.hash("Value");

        switch (base.getType()) {
        case Variant::Type::Real: {
            if (index < 0 || index > TotalAnalogOutputs)
                break;
            double value = base.toDouble();
            if (FP::defined(value)) {
                analogUpdate.insert(index, value);
                analogTarget.insert(index, value);
            }
            break;
        }
        case Variant::Type::Boolean: {
            if (index < 0 || index >= TotalDigitalOutputs)
                break;
            bool value = base.toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            break;
        }
        default:
            break;
        }
    }
}

void AcquireCampbellCR1000GMD::command(const Variant::Read &command)
{
    if (command.hash("SetAnalog").exists()) {
        int index = convertAnalogChannel(
                command.hash("SetAnalog").hash("Parameters").hash("Channel").hash("Value"));
        if (index == -1) {
            index = convertAnalogChannel(
                    command.hash("SetAnalog").hash("Parameters").hash("Channel"));
        }
        if (index == -1) {
            index = convertAnalogChannel(command.hash("SetAnalog").hash("Channel"));
        }
        if (index != -1) {
            double value = Variant::Composite::toNumber(
                    command.hash("SetAnalog").hash("Parameters").hash("Value").hash("Value"));
            if (!FP::defined(value)) {
                value = Variant::Composite::toNumber(
                        command.hash("SetAnalog").hash("Parameters").hash("Value"));
            }
            if (!FP::defined(value)) {
                value = Variant::Composite::toNumber(command.hash("SetAnalog").hash("Value"));
            }

            if (FP::defined(value)) {
                value = sanitizeAnalogOutput(value);
                analogUpdate.insert(index, value);
                analogTarget.insert(index, value);
            }
        } else {
            QHash<int, double>
                    values(convertAnalogMultiple(command.hash("SetAnalog").hash("Parameters")));
            if (values.isEmpty()) {
                values = convertAnalogMultiple(command.hash("SetAnalog"));
            }
            if (!values.isEmpty()) {
                for (QHash<int, double>::const_iterator add = values.constBegin(),
                        endAdd = values.constEnd(); add != endAdd; ++add) {
                    analogUpdate.insert(add.key(), add.value());
                    analogTarget.insert(add.key(), add.value());
                }
            }
        }
    }

    if (command.hash("SetDigital").exists()) {
        int index = convertDigitalChannel(
                command.hash("SetDigital").hash("Parameters").hash("Channel").hash("Value"));
        if (index == -1) {
            index = convertDigitalChannel(
                    command.hash("SetDigital").hash("Parameters").hash("Channel"));
        }
        if (index == -1) {
            index = convertDigitalChannel(command.hash("SetDigital").hash("Channel"));
        }
        if (index != -1) {
            bool ok = false;
            bool value = getValidBoolean(
                    command.hash("SetDigital").hash("Parameters").hash("Value").hash("Value"), ok);
            if (!ok) {
                value = getValidBoolean(command.hash("SetDigital").hash("Parameters").hash("Value"),
                                        ok);
            }
            if (!ok) {
                value = getValidBoolean(command.hash("SetDigital").hash("Value"), ok);
            }

            if (ok) {
                digitalUpdate.insert(index, value);
                digitalTarget.insert(index, value);
            }
        } else {
            qint64 mask = command.hash("SetDigital")
                                 .hash("Parameters")
                                 .hash("Mask")
                                 .hash("Value")
                                 .toInt64();
            if (!INTEGER::defined(mask)) {
                mask = command.hash("SetDigital").hash("Parameters").hash("Mask").toInt64();
            }
            if (!INTEGER::defined(mask)) {
                mask = command.hash("SetDigital").hash("Mask").toInt64();
            }
            if (INTEGER::defined(mask)) {
                qint64 bits = getValidMask(
                        command.hash("SetDigital").hash("Parameters").hash("Value").hash("Value"));
                if (!INTEGER::defined(bits)) {
                    bool ok = false;
                    bool value = getValidBoolean(command.hash("SetDigital")
                                                        .hash("Parameters")
                                                        .hash("Value")
                                                        .hash("Value"), ok);
                    if (ok) {
                        if (value)
                            bits = mask;
                        else
                            bits = 0;
                    }
                }
                if (!INTEGER::defined(bits)) {
                    bits = getValidMask(
                            command.hash("SetDigital").hash("Parameters").hash("Value"));
                }
                if (!INTEGER::defined(bits)) {
                    bool ok = false;
                    bool value = getValidBoolean(
                            command.hash("SetDigital").hash("Parameters").hash("Value"), ok);
                    if (ok) {
                        if (value)
                            bits = mask;
                        else
                            bits = 0;
                    }
                }
                if (!INTEGER::defined(bits)) {
                    bits = getValidMask(command.hash("SetDigital").hash("Value"));
                }
                if (!INTEGER::defined(bits)) {
                    bool ok = false;
                    bool value = getValidBoolean(command.hash("SetDigital").hash("Value"), ok);
                    if (ok) {
                        if (value)
                            bits = mask;
                        else
                            bits = 0;
                    }
                }

                if (INTEGER::defined(bits)) {
                    for (int i = 0; i < 63; i++) {
                        qint64 c = (qint64) 1 << i;
                        if (!(mask & c))
                            continue;
                        bool value = (bits & c) != 0;
                        digitalUpdate.insert(i, value);
                        digitalTarget.insert(i, value);
                    }
                }
            } else {
                QHash<int, bool> values
                        (convertDigitalMultiple(command.hash("SetDigital").hash("Parameters")));
                if (values.isEmpty()) {
                    values = convertDigitalMultiple(command.hash("SetDigital"));
                }
                if (!values.isEmpty()) {
                    for (QHash<int, bool>::const_iterator add = values.constBegin(),
                            endAdd = values.constEnd(); add != endAdd; ++add) {
                        digitalUpdate.insert(add.key(), add.value());
                        digitalTarget.insert(add.key(), add.value());
                    }
                }
            }
        }
    }

    if (command.hash("Output").exists()) {
        genericOutputCommand(command.hash("Output"));
    }

    if (command.hash("Bypass").toBool()) {
        Q_ASSERT(!config.isEmpty());
        for (QMap<int, double>::const_iterator add = config.first().bypassAnalog.constBegin(),
                endAdd = config.first().bypassAnalog.constEnd(); add != endAdd; ++add) {
            analogUpdate.insert(add.key(), add.value());
            analogTarget.insert(add.key(), add.value());
        }
        for (QMap<int, bool>::const_iterator add = config.first().bypassDigital.constBegin(),
                endAdd = config.first().bypassDigital.constEnd(); add != endAdd; ++add) {
            digitalUpdate.insert(add.key(), add.value());
            digitalTarget.insert(add.key(), add.value());
        }
    }
    if (command.hash("UnBypass").toBool()) {
        Q_ASSERT(!config.isEmpty());
        for (QMap<int, double>::const_iterator add = config.first().unbypassAnalog.constBegin(),
                endAdd = config.first().unbypassAnalog.constEnd(); add != endAdd; ++add) {
            analogUpdate.insert(add.key(), add.value());
            analogTarget.insert(add.key(), add.value());
        }
        for (QMap<int, bool>::const_iterator add = config.first().unbypassDigital.constBegin(),
                endAdd = config.first().unbypassDigital.constEnd(); add != endAdd; ++add) {
            digitalUpdate.insert(add.key(), add.value());
            digitalTarget.insert(add.key(), add.value());
        }
    }
}

Variant::Root AcquireCampbellCR1000GMD::getCommands()
{
    Variant::Root result;

    /* Disabled showing this because the current firmware just ignores it */
    /*result["SetAnalog"].hash("DisplayName").setString("Set an &Analog Output");
    result["SetAnalog"].hash("ToolTip").setString("Change the current output of an analog output.");
    result["SetAnalog"].hash("Parameters").hash("Channel").hash("Name").setString("Channel");
    result["SetAnalog"].hash("Parameters").hash("Channel").hash("Format").setString("0");
    result["SetAnalog"].hash("Parameters").hash("Channel").hash("Minimum").setInt64(1);
    result["SetAnalog"].hash("Parameters")
                       .hash("Channel")
                       .hash("Maximum")
                       .setInt64(TotalAnalogOutputs);
    result["SetAnalog"].hash("Parameters").hash("Channel").hash("Default").setInt64(1);
    result["SetAnalog"].hash("Parameters").hash("Value").hash("Name").setString("Output");
    result["SetAnalog"].hash("Parameters").hash("Value").hash("Format").setString("0.000");
    result["SetAnalog"].hash("Parameters").hash("Value").hash("Default").setReal(0.0);*/

    result["SetDigital"].hash("DisplayName").setString("Set a &Digital Output");
    result["SetDigital"].hash("ToolTip").setString("Change the current state of a digital output.");
    result["SetDigital"].hash("Parameters").hash("Channel").hash("Name").setString("Channel");
    result["SetDigital"].hash("Parameters").hash("Channel").hash("Format").setString("0");
    result["SetDigital"].hash("Parameters").hash("Channel").hash("Minimum").setInt64(0);
    result["SetDigital"].hash("Parameters")
                        .hash("Channel")
                        .hash("Maximum")
                        .setInt64(TotalDigitalOutputs - 1);
    result["SetDigital"].hash("Parameters").hash("Channel").hash("Default").setInt64(0);
    result["SetDigital"].hash("Parameters").hash("Value").hash("Name").setString("Output high");
    result["SetDigital"].hash("Parameters").hash("Value").hash("Type").setString("Boolean");
    result["SetDigital"].hash("Parameters").hash("Value").hash("Default").setBoolean(false);

    return result;
}


AcquisitionInterface::AutoprobeStatus AcquireCampbellCR1000GMD::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

SequenceValue::Transfer AcquireCampbellCR1000GMD::getPersistentValues()
{
    PauseLock paused(*this);
    SequenceValue::Transfer result;

    Q_ASSERT(!config.isEmpty());

    if (config.first().logOutputs) {
        if (FP::defined(analogOutputValues.getStart())) {
            result.emplace_back(analogOutputValues);
        }
        if (FP::defined(digitalOutputValues.getStart())) {
            result.emplace_back(digitalOutputValues);
        }
    }

    return result;
}

Variant::Root AcquireCampbellCR1000GMD::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireCampbellCR1000GMD::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN_READ_STATE:
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
    case RESP_INTERACTIVE_RUN_WAIT:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

void AcquireCampbellCR1000GMD::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_READ_STATE:
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
    case RESP_INTERACTIVE_RUN_WAIT:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_READ_STATE:
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + 30.0);
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        discardData(time + 0.5);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireCampbellCR1000GMD::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobeValidRecords = 0;
    discardData(time + 0.5, 1);
    timeoutAt(time + config.first().pollInterval * 7.0 + 3.0);
    generalStatusUpdated();
}

void AcquireCampbellCR1000GMD::autoprobePromote(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_WAIT:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive wait to interactive acquisition at"
                     << Logging::time(time);
        timeoutAt(time);
        break;

    case RESP_INTERACTIVE_RUN_READ_STATE:
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
    case RESP_INTERACTIVE_START_READ_STATE:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 5.0);
        break;

    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 30.0);
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        discardData(time + 0.5);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireCampbellCR1000GMD::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    Q_ASSERT(!config.isEmpty());

    timeoutAt(time + config.first().pollInterval + 2.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_RUN_READ_STATE:
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
    case RESP_INTERACTIVE_START_READ_STATE:
    case RESP_INTERACTIVE_RUN_WAIT:
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireCampbellCR1000GMD::getDefaults()
{
    AutomaticDefaults result;
    result.name = "X$2";
    result.setSerialN81(115200);
    return result;
}


ComponentOptions AcquireCampbellCR1000GMDComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options);
    return options;
}

ComponentOptions AcquireCampbellCR1000GMDComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireCampbellCR1000GMDComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireCampbellCR1000GMDComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireCampbellCR1000GMDComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options);
}

QList<ComponentExample> AcquireCampbellCR1000GMDComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data with the default settings.")));

    return examples;
}

std::unique_ptr<AcquisitionInterface> AcquireCampbellCR1000GMDComponent::createAcquisitionPassive(
        const ComponentOptions &options,
        const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(
            new AcquireCampbellCR1000GMD(options, loggingContext));
}

std::unique_ptr<AcquisitionInterface> AcquireCampbellCR1000GMDComponent::createAcquisitionPassive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(
            new AcquireCampbellCR1000GMD(config, loggingContext));
}

std::unique_ptr<AcquisitionInterface> AcquireCampbellCR1000GMDComponent::createAcquisitionAutoprobe(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireCampbellCR1000GMD>
            i(new AcquireCampbellCR1000GMD(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireCampbellCR1000GMDComponent::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                              const std::string &loggingContext)
{
    std::unique_ptr<AcquireCampbellCR1000GMD>
            i(new AcquireCampbellCR1000GMD(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}