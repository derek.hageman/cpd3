/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIRECAMPBELLCR1000GMD_H
#define ACQUIRECAMPBELLCR1000GMD_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "acquisition/spancheck.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class AcquireCampbellCR1000GMD : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Passive autoprobe initializing, ignoring the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,

        /* Acquiring data in interactive mode, waiting for the analog read
         * response */
                RESP_INTERACTIVE_RUN_READ_STATE,

        /* Acquiring data in interactive mode, waiting for the analog output write response */
                RESP_INTERACTIVE_RUN_WRITE_ANALOG,

        /* Acquiring data in interactive mode, waiting for the digital output write response */
                RESP_INTERACTIVE_RUN_WRITE_DIGITAL,

        /* Acquiring data in interactive mode, sleeping until the next time to query. */
                RESP_INTERACTIVE_RUN_WAIT,

        /* Starting communications, reading the initial state */
                RESP_INTERACTIVE_START_READ_STATE,

        /* Waiting before attempting a communications restart */
                RESP_INTERACTIVE_RESTART_WAIT,

        /* Initial state for interactive start */
                RESP_INTERACTIVE_INITIALIZE,
    };
    ResponseState responseState;
    int autoprobeValidRecords;

    struct OutputVariable {
        CPD3::Data::SequenceName::Component name;
        CPD3::Data::Variant::Root metadata;

        int channel;
        CPD3::Calibration calibration;
    };

    friend class Configuration;

    class Configuration {
        double start;
        double end;

        void addVariable(const CPD3::Data::Variant::Read &config,
                         qint64 defaultChannel = CPD3::INTEGER::undefined(),
                         const CPD3::Data::SequenceName::Component &defaultName = {});

        void setAnalogValues(const CPD3::Data::Variant::Read &config, QMap<int, double> &target);

        void setDigitalValues(const CPD3::Data::Variant::Read &config, QMap<int, bool> &target);

    public:
        double pollInterval;
        bool strictMode;

        bool logOutputs;

        std::unordered_map<CPD3::Data::SequenceName::Component, int> analogNames;
        std::unordered_map<CPD3::Data::SequenceName::Component, int> digitalNames;

        QMap<int, double> initializeAnalog;
        QMap<int, double> exitAnalog;

        QMap<int, bool> initializeDigital;
        QMap<int, bool> exitDigital;

        QMap<int, double> bypassAnalog;
        QMap<int, double> unbypassAnalog;

        QMap<int, bool> bypassDigital;
        QMap<int, bool> unbypassDigital;

        QList<OutputVariable> variables;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    void setDefaultInvalid();

    CPD3::Data::Variant::Root instrumentMeta;

    QMap<int, double> analogUpdate;
    QMap<int, bool> digitalUpdate;
    int updateChannel;

    QMap<int, double> analogTarget;
    QMap<int, bool> digitalTarget;

    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    QString lastError;

    bool realtimeDigitalStateUpdated;

    CPD3::Data::Variant::Root lastInputs;
    CPD3::Data::SequenceValue analogOutputValues;
    CPD3::Data::SequenceValue digitalOutputValues;
    CPD3::Data::SequenceValue digitalOutputBits;

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void invalidateLogValues(double frameTime);

    bool issueNextAnalogUpdate(double time);

    bool issueNextDigitalUpdate(double time);

    bool advanceReadUpdate(double time);

    void outputMetadata(double time);

    void processDigitalState(double frameTime, qint64 bits);

    int processRecord(const CPD3::Util::ByteArray &line, double frameTime);

    void configurationChanged();

    void configurationAdvance(double frameTime);

    int convertAnalogChannel(const CPD3::Data::Variant::Read &value);

    int convertDigitalChannel(const CPD3::Data::Variant::Read &value);

    QHash<int, double> convertAnalogMultiple(const CPD3::Data::Variant::Read &value);

    QHash<int, bool> convertDigitalMultiple(const CPD3::Data::Variant::Read &value);

    void genericOutputCommand(const CPD3::Data::Variant::Read &command);

public:
    AcquireCampbellCR1000GMD(const CPD3::Data::ValueSegment::Transfer &config,
                             const std::string &loggingContext);

    AcquireCampbellCR1000GMD(const CPD3::ComponentOptions &options,
                             const std::string &loggingContext);

    virtual ~AcquireCampbellCR1000GMD();

    virtual CPD3::Data::SequenceValue::Transfer getPersistentValues();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    CPD3::Data::Variant::Root getCommands() override;

    virtual CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    virtual void command(const CPD3::Data::Variant::Read &value);

    virtual void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingInstrumentTimeout(double time);

    virtual void discardDataCompleted(double time);

    virtual void shutdownInProgress(double time);
};

class AcquireCampbellCR1000GMDComponent
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_campbell_cr1000gmd"
                              FILE
                              "acquire_campbell_cr1000gmd.json")

    CPD3::ComponentOptions getBaseOptions();

public:

    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
