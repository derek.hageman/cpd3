/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef TIMELIMIT_H
#define TIMELIMIT_H

#include "core/first.hxx"

#include <deque>
#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/dynamicprimitive.hxx"

class TimeLimit : public CPD3::Data::AsyncProcessingStage {
    CPD3::Data::DynamicTimeInterval *includeStart;
    CPD3::Data::DynamicTimeInterval *includeEnd;
    CPD3::Data::DynamicTimeInterval *excludeStart;
    CPD3::Data::DynamicTimeInterval *excludeEnd;
    double absoluteStart;
    double absoluteEnd;

    double firstDataStart;
    double lastDataEnd;
    std::deque<CPD3::Data::SequenceValue> endExcludePending;
    std::deque<CPD3::Data::SequenceValue> endIncludePending;

    void processEndLimiting(double time);

public:
    TimeLimit(const CPD3::ComponentOptions &options);

    TimeLimit(double start,
              double end,
              const CPD3::Data::SequenceName::Component &station,
              const CPD3::Data::SequenceName::Component &archive,
              const CPD3::Data::ValueSegment::Transfer &config);

    TimeLimit(QDataStream &stream);

    ~TimeLimit();

    virtual void serialize(QDataStream &stream);

protected:
    virtual void process(CPD3::Data::SequenceValue::Transfer &&incoming);

    virtual void finish();
};

class TimeLimitComponent : public QObject, public CPD3::Data::ProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.time_limit"
                              FILE
                              "time_limit.json")

public:
    virtual QString getGeneralSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual CPD3::Data::ProcessingStage *createGeneralFilterDynamic
            (const CPD3::ComponentOptions &options = CPD3::ComponentOptions());

    virtual CPD3::Data::ProcessingStage *createGeneralFilterEditing(double start,
                                                                    double end,
                                                                    const CPD3::Data::SequenceName::Component &station,
                                                                    const CPD3::Data::SequenceName::Component &archive,
                                                                    const CPD3::Data::ValueSegment::Transfer &config);

    virtual CPD3::Data::ProcessingStage *deserializeGeneralFilter(QDataStream &stream);

};

#endif
