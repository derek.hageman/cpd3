/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/processingstage.hxx"
#include "core/component.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestComponent : public QObject {
Q_OBJECT

    ProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<ProcessingStageComponent *>(ComponentLoader::create("time_limit"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<TimeIntervalSelectionOption *>(options.get("include-start")));
        QVERIFY(qobject_cast<TimeIntervalSelectionOption *>(options.get("include-end")));
        QVERIFY(qobject_cast<ComponentOptionSingleTime *>(options.get("start")));
        QVERIFY(qobject_cast<ComponentOptionSingleTime *>(options.get("end")));
    }

    void includeStart()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<TimeIntervalSelectionOption *>(options.get("include-start"))->set(Time::Second,
                                                                                       10);

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName u1("brw", "raw", "T_S11");
        filter->incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(2.0), 5.0, 12.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(3.0), 12.0, 13.0));
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 2);
        QCOMPARE(e.values()[0], SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        QCOMPARE(e.values()[1], SequenceValue(u1, Variant::Root(2.0), 5.0, 11.0));
    }

    void includeEnd()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<TimeIntervalSelectionOption *>(options.get("include-end"))->set(Time::Second,
                                                                                     10);

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName u1("brw", "raw", "T_S11");
        filter->incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(2.0), 5.0, 16.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(3.0), 12.0, 13.0));
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 2);
        QCOMPARE(e.values()[0], SequenceValue(u1, Variant::Root(2.0), 6.0, 16.0));
        QCOMPARE(e.values()[1], SequenceValue(u1, Variant::Root(3.0), 12.0, 13.0));
    }

    void absoluteLimits()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<ComponentOptionSingleTime *>(options.get("start"))->set(3.0);
        qobject_cast<ComponentOptionSingleTime *>(options.get("end"))->set(9.0);

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName u1("brw", "raw", "T_S11");
        filter->incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(2.0), 2.0, 16.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(3.0), 12.0, 13.0));
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 1);
        QCOMPARE(e.values()[0], SequenceValue(u1, Variant::Root(2.0), 3.0, 9.0));;
    }

    void excludeLimits()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<TimeIntervalSelectionOption *>(options.get("exclude-start"))->set(Time::Second,
                                                                                       10);
        qobject_cast<TimeIntervalSelectionOption *>(options.get("exclude-end"))->set(Time::Second,
                                                                                     5);

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName u1("brw", "raw", "T_S11");
        filter->incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(2.0), 5.0, 12.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(3.0), 17.0, 20.0));
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 1);
        QCOMPARE(e.values()[0], SequenceValue(u1, Variant::Root(2.0), 11.0, 12.0));
    }

    void serialize()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<ComponentOptionSingleTime *>(options.get("start"))->set(10.5);

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName u1("brw", "raw", "T_S11");
        for (int i = 0; i < 10000; i++) {
            filter->incomingData(SequenceValue(u1, Variant::Root(1.0 + i), 1.0 + i, 1.1 + i));
        }
        filter->setEgress(NULL);

        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            filter->serialize(stream);
        }
        filter->signalTerminate();
        QVERIFY(filter->wait());
        delete filter;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            filter = component->deserializeGeneralFilter(stream);
            QVERIFY(stream.atEnd());
        }
        QVERIFY(filter != NULL);
        filter->start();
        filter->setEgress(&e);

        for (int i = 10000; i < 20000; i++) {
            filter->incomingData(SequenceValue(u1, Variant::Root(1.0 + i), 1.0 + i, 1.1 + i));
        }
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 20000 - 10);
        for (int i = 0; i < 20000 - 10; i++) {
            int j = i + 10;
            QCOMPARE(e.values()[i], SequenceValue(u1, Variant::Root(1.0 + j), 1.0 + j, 1.1 + j));
        }
    }

    void combined()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<TimeIntervalSelectionOption *>(options.get("include-start"))->set(Time::Second,
                                                                                       20);
        qobject_cast<TimeIntervalSelectionOption *>(options.get("include-end"))->set(Time::Second,
                                                                                     15);
        qobject_cast<ComponentOptionSingleTime *>(options.get("start"))->set(4.0);
        qobject_cast<ComponentOptionSingleTime *>(options.get("end"))->set(12.0);

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName u1("brw", "raw", "T_S11");
        filter->incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(2.0), 5.0, 25.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(3.0), 21.0, 22.0));
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 1);
        QCOMPARE(e.values()[0], SequenceValue(u1, Variant::Root(2.0), 10.0, 12.0));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
