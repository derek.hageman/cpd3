/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "core/environment.hxx"

#include "time_limit.hxx"

using namespace CPD3;
using namespace CPD3::Data;


TimeLimit::TimeLimit(const ComponentOptions &options)
{
    firstDataStart = FP::undefined();
    lastDataEnd = FP::undefined();

    if (options.isSet("include-start")) {
        includeStart = qobject_cast<TimeIntervalSelectionOption *>(
                options.get("include-start"))->getInterval();
    } else {
        includeStart = new DynamicTimeInterval::Undefined;
    }

    if (options.isSet("include-end")) {
        includeEnd = qobject_cast<TimeIntervalSelectionOption *>(
                options.get("include-end"))->getInterval();
    } else {
        includeEnd = new DynamicTimeInterval::Undefined;
    }

    if (options.isSet("exclude-start")) {
        excludeStart = qobject_cast<TimeIntervalSelectionOption *>(
                options.get("exclude-start"))->getInterval();
    } else {
        excludeStart = new DynamicTimeInterval::Undefined;
    }

    if (options.isSet("exclude-end")) {
        excludeEnd = qobject_cast<TimeIntervalSelectionOption *>(
                options.get("exclude-end"))->getInterval();
    } else {
        excludeEnd = new DynamicTimeInterval::Undefined;
    }

    if (options.isSet("start")) {
        absoluteStart = qobject_cast<ComponentOptionSingleTime *>(options.get("start"))->get();
    } else {
        absoluteStart = FP::undefined();
    }
    if (options.isSet("end")) {
        absoluteEnd = qobject_cast<ComponentOptionSingleTime *>(options.get("end"))->get();
    } else {
        absoluteEnd = FP::undefined();
    }
}

TimeLimit::TimeLimit(double start,
                     double end,
                     const SequenceName::Component &station,
                     const SequenceName::Component &archive,
                     const ValueSegment::Transfer &config)
{
    Q_UNUSED(station);
    Q_UNUSED(archive);

    firstDataStart = FP::undefined();
    lastDataEnd = FP::undefined();

    includeStart = DynamicTimeInterval::fromConfiguration(config, "Include/Start", start, end);
    includeEnd = DynamicTimeInterval::fromConfiguration(config, "Include/End", start, end);

    excludeStart = DynamicTimeInterval::fromConfiguration(config, "Exclude/Start", start, end);
    excludeEnd = DynamicTimeInterval::fromConfiguration(config, "Exclude/End", start, end);

    absoluteStart = FP::undefined();
    absoluteEnd = FP::undefined();
}

TimeLimit::~TimeLimit()
{
    delete includeStart;
    delete includeEnd;
    delete excludeStart;
    delete excludeEnd;
}

TimeLimit::TimeLimit(QDataStream &stream) : AsyncProcessingStage(stream)
{
    stream >> includeStart;
    stream >> includeEnd;
    stream >> excludeStart;
    stream >> excludeEnd;
    stream >> absoluteStart;
    stream >> absoluteEnd;
    stream >> firstDataStart;
    stream >> lastDataEnd;
    stream >> endExcludePending;
    stream >> endIncludePending;
}

void TimeLimit::serialize(QDataStream &stream)
{
    AsyncProcessingStage::serialize(stream);
    stream << includeStart;
    stream << includeEnd;
    stream << excludeStart;
    stream << excludeEnd;
    stream << absoluteStart;
    stream << absoluteEnd;
    stream << firstDataStart;
    stream << lastDataEnd;
    stream << endExcludePending;
    stream << endIncludePending;
}

void TimeLimit::processEndLimiting(double time)
{
    if (!endExcludePending.empty()) {
        double limit = excludeEnd->apply(time, lastDataEnd, false, -1);
        if (!FP::defined(limit)) {
            Util::append(std::move(endExcludePending), endIncludePending);
            endExcludePending.clear();
        } else {
            auto begin = endExcludePending.begin();

            /* If the first value is inside the buffer range, then everything
             * else is as well, so we can't do anything else */
            if (FP::defined(begin->getStart()) && begin->getStart() > limit)
                return;

            auto end = endExcludePending.end();
            auto purgeEnd = Range::heuristicUpperBound(begin, end, limit);
            /* Note that unlike all other modes, we don't adjust the bounds
             * here, since doing so would end up requiring us to buffer hugely
             * when we deal with values that span the boundary (e.x. metadata).
             * That is, spanning values would have to hold up everything inside
             * them until the very end when their end was actually known. */
            if (purgeEnd == end) {
                Util::append(std::move(endExcludePending), endIncludePending);
                endExcludePending.clear();
            } else {
                std::move(begin, purgeEnd, Util::back_emplacer(endIncludePending));
                endExcludePending.erase(begin, purgeEnd);
            }
        }
    }

    if (endIncludePending.empty())
        return;

    double limit = includeEnd->apply(time, lastDataEnd, false, -1);
    if (!FP::defined(limit)) {
        egress->incomingData(
                SequenceValue::Transfer(std::make_move_iterator(endIncludePending.begin()),
                                        std::make_move_iterator(endIncludePending.end())));
        endIncludePending.clear();
    } else {
        auto begin = endIncludePending.begin();

        /* If it's already inside the limit, skip the complicated logic
         * (catches duplicate times as well) */
        if (FP::defined(begin->getStart()) && begin->getStart() >= limit)
            return;

        /* Chances are this is near the start of the list, so don't
         * use the heuristic version */
        auto purgeEnd = Range::lowerBound(begin, endIncludePending.end(), limit);
        for (; begin != purgeEnd;) {
            if (FP::defined(begin->getEnd()) && begin->getEnd() <= limit) {
                int remaining = purgeEnd - begin;
                begin = endIncludePending.erase(begin);
                purgeEnd = begin + (remaining - 1);
                continue;
            }

            Q_ASSERT(!FP::defined(begin->getStart()) || begin->getStart() < limit);

            begin->setStart(limit);

            Q_ASSERT(begin == endIncludePending.begin() ||
                             Range::compareStart((begin - 1)->getStart(), begin->getStart()) <= 0);

            ++begin;
        }
    }
}

void TimeLimit::process(SequenceValue::Transfer &&incoming)
{
    if (incoming.empty())
        return;

    int checkCounter = 0;
    double originalStart = FP::undefined();
    for (auto &add : incoming) {
        originalStart = add.getStart();

        if (!FP::defined(firstDataStart) && FP::defined(add.getStart()))
            firstDataStart = add.getStart();
        if (FP::defined(add.getEnd()) && (!FP::defined(lastDataEnd) || add.getEnd() > lastDataEnd))
            lastDataEnd = add.getEnd();

        if (FP::defined(absoluteStart)) {
            if (FP::defined(add.getEnd()) && add.getEnd() <= absoluteStart)
                continue;
            if (!FP::defined(add.getStart()) || add.getStart() < absoluteStart)
                add.setStart(absoluteStart);
        }
        if (FP::defined(absoluteEnd)) {
            if (FP::defined(add.getStart()) && add.getStart() > absoluteEnd)
                continue;
            if (!FP::defined(add.getEnd()) || add.getEnd() > absoluteEnd)
                add.setEnd(absoluteEnd);
        }

        double limitStart = includeStart->apply(originalStart, firstDataStart, true);
        if (FP::defined(limitStart)) {
            if (FP::defined(add.getStart()) && add.getStart() > limitStart)
                continue;
            if (!FP::defined(add.getEnd()) || add.getEnd() > limitStart)
                add.setEnd(limitStart);
        }

        double limitEnd = excludeStart->apply(originalStart, firstDataStart, true);
        if (FP::defined(limitEnd)) {
            if (FP::defined(add.getEnd()) && add.getEnd() <= limitEnd)
                continue;
            if (!FP::defined(add.getStart()) || add.getStart() < limitEnd)
                add.setStart(limitEnd);
        }


        if (add.getName().isMeta() &&
                add.read().isMetadata() &&
                add.read().metadata("Processing").exists()) {

            auto meta = add.write().metadata("Processing").toArray().after_back();
            meta["By"].setString("time_limit");
            meta["At"].setDouble(Time::time());
            meta["Environment"].setString(Environment::describe());
            meta["Revision"].setString(Environment::revision());
            meta["Parameters"].hash("IncludeStart")
                              .setString(includeStart->describe(originalStart));
            meta["Parameters"].hash("IncludeEnd").setString(includeStart->describe(originalStart));
            meta["Parameters"].hash("ExcludeStart")
                              .setString(excludeStart->describe(originalStart));
            meta["Parameters"].hash("ExcludeEnd").setString(excludeStart->describe(originalStart));
            meta["Parameters"].hash("AbsoluteStart").setDouble(absoluteStart);
            meta["Parameters"].hash("AbsoluteEnd").setDouble(absoluteEnd);
        }

        endExcludePending.emplace_back(std::move(add));

        if (++checkCounter % 10000 == 0)
            processEndLimiting(originalStart);
    }
    processEndLimiting(originalStart);
}

void TimeLimit::finish()
{
    if (!endIncludePending.empty()) {
        /* At the end now, so all this is included */
        egress->incomingData(SequenceValue::Transfer(std::make_move_iterator(endIncludePending.begin()),
                                                     std::make_move_iterator(endIncludePending.end())));
        endIncludePending.clear();
    }
    AsyncProcessingStage::finish();
}


QString TimeLimitComponent::getGeneralSerializationName() const
{ return QString::fromLatin1("time_limit"); }

ComponentOptions TimeLimitComponent::getOptions()
{
    ComponentOptions options;

    TimeIntervalSelectionOption *limit =
            new TimeIntervalSelectionOption(tr("include-start", "name"),
                                            tr("The maximum time after the start of data"),
                                            tr("This if the maximum time accepted after the start of the first "
                                               "defined value time.  That is, data that falls outside this "
                                               "interval from the start are rejected."),
                                            tr("Unlimited", "default limit"));
    limit->setAllowZero(true);
    limit->setAllowNegative(false);
    options.add("include-start", limit);

    limit = new TimeIntervalSelectionOption(tr("include-end", "name"),
                                            tr("The maximum time before the end of data"),
                                            tr("This if the minimum time accepted before the end of the last "
                                               "defined value time.  That is, data that falls outside this "
                                               "interval from the end are rejected."),
                                            tr("Unlimited", "default limit"));
    limit->setAllowZero(true);
    limit->setAllowNegative(false);
    options.add("include-end", limit);

    limit = new TimeIntervalSelectionOption(tr("exclude-start", "name"),
                                            tr("The first time after the start of data"),
                                            tr("This if the minimum time accepted after the start of the first "
                                               "defined value time.  That is, data that falls inside this "
                                               "interval from the start are rejected."),
                                            tr("Disabled", "default limit"));
    limit->setAllowZero(false);
    limit->setAllowNegative(false);
    options.add("exclude-start", limit);

    limit = new TimeIntervalSelectionOption(tr("exclude-end", "name"),
                                            tr("The last time before the end of data"),
                                            tr("This if the maximum time accepted before the end of the last "
                                               "defined value time.  That is, data that falls inside this "
                                               "interval from the end are rejected."),
                                            tr("Unlimited", "default limit"));
    limit->setAllowZero(false);
    limit->setAllowNegative(false);
    options.add("exclude-end", limit);


    options.add("start",
                new ComponentOptionSingleTime(tr("start", "name"), tr("The start time to accept"),
                                              tr("This is the start time to output data.  If this is set then no "
                                                 "data before this time is output."), QString()));

    options.add("end",
                new ComponentOptionSingleTime(tr("end", "name"), tr("The end time to accept"),
                                              tr("This is the end time to output data.  If this is set then no "
                                                 "data after this time is output."), QString()));

    return options;
}

ProcessingStage *TimeLimitComponent::createGeneralFilterDynamic(const ComponentOptions &options)
{ return new TimeLimit(options); }

ProcessingStage *TimeLimitComponent::createGeneralFilterEditing(double start,
                                                                double end,
                                                                const SequenceName::Component &station,
                                                                const SequenceName::Component &archive,
                                                                const ValueSegment::Transfer &config)
{ return new TimeLimit(start, end, station, archive, config); }

ProcessingStage *TimeLimitComponent::deserializeGeneralFilter(QDataStream &stream)
{ return new TimeLimit(stream); }
