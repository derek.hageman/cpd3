/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CALCPITOTFLOW_H
#define CALCPITOTFLOW_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QList>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

class CalcPitotFlow : public CPD3::Data::SegmentProcessingStage {
    class Processing {
    public:
        CPD3::Data::DynamicSequenceSelection *operateFlow;

        CPD3::Data::DynamicInput *inputPitot;
        CPD3::Data::DynamicInput *inputArea;
        CPD3::Data::DynamicInput *inputTemperature;
        CPD3::Data::DynamicInput *inputPressure;

        Processing() : operateFlow(NULL),
                       inputPitot(NULL),
                       inputArea(NULL),
                       inputTemperature(NULL),
                       inputPressure(NULL)
        { }
    };

    CPD3::Data::DynamicInput *defaultArea;
    CPD3::Data::DynamicInput *defaultTemperature;
    CPD3::Data::DynamicInput *defaultPressure;
    CPD3::Data::SequenceName::ComponentSet filterSuffixes;

    bool restrictedInputs;

    void handleOptions(const CPD3::ComponentOptions &options);

    std::vector<Processing> processing;

    void handleNewProcessing(const CPD3::Data::SequenceName &unit,
                             int id,
                             CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control);

    void registerPossibleInput(const CPD3::Data::SequenceName &unit,
                               CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                               int filterID = -1);

public:
    CalcPitotFlow();

    CalcPitotFlow(const CPD3::ComponentOptions &options);

    CalcPitotFlow(const CPD3::ComponentOptions &options,
                  double start, double end, const QList<CPD3::Data::SequenceName> &inputs);

    CalcPitotFlow(double start,
                  double end,
                  const CPD3::Data::SequenceName::Component &station,
                  const CPD3::Data::SequenceName::Component &archive,
                  const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CalcPitotFlow();

    void unhandled(const CPD3::Data::SequenceName &unit,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;

    QSet<double> metadataBreaks(int id) override;

    CalcPitotFlow(QDataStream &stream);

    void serialize(QDataStream &stream) override;
};

class CalcPitotFlowComponent
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.calc_pitotflow"
                              FILE
                              "calc_pitotflow.json")
public:
    virtual QString getBasicSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Data::SegmentProcessingStage
            *createBasicFilterDynamic(const CPD3::ComponentOptions &options);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined
            (const CPD3::ComponentOptions &options,
             double start,
             double end, const QList<CPD3::Data::SequenceName> &inputs);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                         double end,
                                                                         const CPD3::Data::SequenceName::Component &station,
                                                                         const CPD3::Data::SequenceName::Component &archive,
                                                                         const CPD3::Data::ValueSegment::Transfer &config);

    virtual CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream);
};

#endif
