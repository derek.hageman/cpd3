/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "core/environment.hxx"

#include "calc_pitotflow.hxx"

using namespace CPD3;
using namespace CPD3::Data;

#ifndef M_PI
#define M_PI    3.14159265358979
#endif


void CalcPitotFlow::handleOptions(const ComponentOptions &options)
{
    if (options.isSet("input-area")) {
        defaultArea = qobject_cast<DynamicInputOption *>(options.get("input-area"))->getInput();
    } else {
        defaultArea = NULL;
    }

    if (options.isSet("diameter")) {
        if (defaultArea != NULL)
            delete defaultArea;
        double d = qobject_cast<ComponentOptionSingleDouble *>(options.get("diameter"))->get();
        d /= 1000.0;
        d /= 2.0;
        defaultArea = new DynamicInput::Constant(M_PI * d * d);
    }

    if (defaultArea == NULL) {
        double d = 1.75 * 25.4;
        d /= 1000.0;
        d /= 2.0;
        defaultArea = new DynamicInput::Constant(M_PI * d * d);
    }

    if (options.isSet("input-t")) {
        defaultTemperature = qobject_cast<DynamicInputOption *>(options.get("input-t"))->getInput();
    } else {
        defaultTemperature = NULL;
    }

    if (options.isSet("input-p")) {
        defaultPressure = qobject_cast<DynamicInputOption *>(options.get("input-p"))->getInput();
    } else {
        defaultPressure = NULL;
    }

    restrictedInputs = false;

    if (options.isSet("input-dp") || options.isSet("output-q")) {
        restrictedInputs = true;

        Processing p;

        SequenceName::Component suffix;
        SequenceName::Component station;
        SequenceName::Component archive;
        SequenceName::Flavors flavors;
        if (options.isSet("output-q")) {
            p.operateFlow = qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-q"))
                    ->getOperator();
            for (const auto &name : p.operateFlow->getAllUnits()) {
                suffix = Util::suffix(name.getVariable(), '_');
                station = name.getStation();
                archive = name.getArchive();
                flavors = name.getFlavors();
                if (suffix.length() != 0)
                    break;
            }
        }
        if (options.isSet("input-dp")) {
            p.inputPitot = qobject_cast<DynamicInputOption *>(options.get("input-dp"))->getInput();
            if (suffix.length() == 0) {
                for (const auto &name : p.inputPitot->getUsedInputs()) {
                    suffix = Util::suffix(name.getVariable(), '_');
                    station = name.getStation();
                    archive = name.getArchive();
                    flavors = name.getFlavors();
                    if (suffix.length() != 0)
                        break;
                }
            }
        }

        if (p.operateFlow == NULL) {
            if (suffix.length() == 0) {
                p.operateFlow =
                        new DynamicSequenceSelection::Match(
                                QString::fromStdString(station),
                                QString::fromStdString(archive), "Q\\d*_.*", flavors);
            } else {
                p.operateFlow = new DynamicSequenceSelection::Basic(
                        SequenceName(station, archive, "Q_" + suffix, flavors));
            }
        }
        if (p.inputPitot == NULL) {
            DynamicInput::Variable *sel = new DynamicInput::Variable;
            p.inputPitot = sel;
            if (suffix.length() == 0) {
                sel->set(SequenceMatch::OrderedLookup(
                        SequenceMatch::Element(station, archive, "Pd\\d*_.*", flavors)),
                         Calibration());
            } else {
                sel->set(SequenceMatch::OrderedLookup(
                        SequenceMatch::Element(station, archive, "Pd_" + suffix,
                                               flavors)), Calibration());
            }
        }

        if (defaultArea == NULL) {
            p.inputArea = new DynamicInput::Constant(FP::undefined());
        } else {
            p.inputArea = defaultArea->clone();
        }

        if (defaultTemperature == NULL) {
            p.inputTemperature = new DynamicInput::Constant(0.0);
        } else {
            p.inputTemperature = defaultTemperature->clone();
        }

        if (defaultPressure == NULL) {
            p.inputPressure = new DynamicInput::Constant(1013.25);
        } else {
            p.inputPressure = defaultPressure->clone();
        }

        processing.push_back(p);
    }

    if (options.isSet("suffix")) {
        filterSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->get());
    }
}


CalcPitotFlow::CalcPitotFlow()
{ Q_ASSERT(false); }

CalcPitotFlow::CalcPitotFlow(const ComponentOptions &options)
{
    handleOptions(options);
}

CalcPitotFlow::CalcPitotFlow(const ComponentOptions &options,
                             double start, double end, const QList<SequenceName> &inputs)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    handleOptions(options);

    for (QList<SequenceName>::const_iterator unit = inputs.constBegin(), endU = inputs.constEnd();
            unit != endU;
            ++unit) {
        CalcPitotFlow::unhandled(*unit, NULL);
    }
}

CalcPitotFlow::CalcPitotFlow(double start,
                             double end,
                             const SequenceName::Component &station,
                             const SequenceName::Component &archive,
                             const ValueSegment::Transfer &config)
{
    defaultArea = NULL;
    defaultTemperature = NULL;
    defaultPressure = NULL;
    restrictedInputs = true;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    for (const auto &child : children) {
        Processing p;

        p.operateFlow = DynamicSequenceSelection::fromConfiguration(config,
                                                                    QString("%1/CalculateFlow").arg(
                                                                            QString::fromStdString(
                                                                                    child)), start,
                                                                    end);

        p.inputPitot = DynamicInput::fromConfiguration(config, QString("%1/PitotPressure").arg(
                QString::fromStdString(child)),
                                                       start, end);
        p.inputArea = DynamicInput::fromConfiguration(config, QString("%1/Area").arg(
                QString::fromStdString(child)), start, end);
        p.inputTemperature = DynamicInput::fromConfiguration(config, QString("%1/Temperature").arg(
                QString::fromStdString(child)),
                                                             start, end);
        p.inputPressure = DynamicInput::fromConfiguration(config, QString("%1/Pressure").arg(
                QString::fromStdString(child)), start,
                                                          end);

        p.operateFlow->registerExpected(station, archive);
        p.inputPitot->registerExpected(station, archive);
        p.inputArea->registerExpected(station, archive);
        p.inputTemperature->registerExpected(station, archive);
        p.inputPressure->registerExpected(station, archive);

        processing.push_back(p);
    }
}


CalcPitotFlow::~CalcPitotFlow()
{
    if (defaultArea != NULL)
        delete defaultArea;
    if (defaultTemperature != NULL)
        delete defaultTemperature;
    if (defaultPressure != NULL)
        delete defaultPressure;

    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        delete p->operateFlow;
        delete p->inputPitot;
        delete p->inputArea;
        delete p->inputTemperature;
        delete p->inputPressure;
    }
}

void CalcPitotFlow::handleNewProcessing(const SequenceName &unit,
                                        int id,
                                        SegmentProcessingStage::SequenceHandlerControl *control)
{
    Processing *p = &processing[id];

    SequenceName::Set reg;

    p->operateFlow
     ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->operateFlow->getAllUnits(), reg);

    if (!control)
        return;

    for (const auto &name : reg) {
        bool isNew = !control->isHandled(name);
        control->filterUnit(name, id);
        if (isNew)
            registerPossibleInput(name, control, id);
    }
}

void CalcPitotFlow::registerPossibleInput(const SequenceName &unit,
                                          SegmentProcessingStage::SequenceHandlerControl *control,
                                          int filterID)
{
    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        bool usedAsInput = false;
        if (processing[id].inputArea->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }
        if (processing[id].inputTemperature->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }
        if (processing[id].inputPressure->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }
        if (processing[id].inputPitot->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }
        if (usedAsInput && id != filterID)
            handleNewProcessing(unit, id, control);
    }
    if (defaultArea != NULL && defaultArea->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultTemperature != NULL && defaultTemperature->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultPressure != NULL && defaultPressure->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
}

void CalcPitotFlow::unhandled(const SequenceName &unit,
                              SegmentProcessingStage::SequenceHandlerControl *control)
{
    registerPossibleInput(unit, control);

    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].operateFlow->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            handleNewProcessing(unit, id, control);
            return;
        }
    }

    if (restrictedInputs)
        return;

    if (unit.hasFlavor(SequenceName::flavor_cover) || unit.hasFlavor(SequenceName::flavor_stats))
        return;

    QRegExp reCheck("Pd(\\d*)_(.+)");
    if (!reCheck.exactMatch(unit.getVariableQString()))
        return;
    auto suffix = reCheck.cap(2).toStdString();
    if (suffix.empty())
        return;
    if (!filterSuffixes.empty() && !filterSuffixes.count(suffix))
        return;

    auto prefix = reCheck.cap(1).toStdString();

    SequenceName flowUnit
            (unit.getStation(), unit.getArchive(), "Q" + prefix + "_" + suffix,
             unit.getFlavors());

    Processing p;

    p.operateFlow = new DynamicSequenceSelection::Single(flowUnit);
    p.operateFlow->registerInput(unit);
    p.operateFlow->registerInput(flowUnit);

    p.inputPitot = new DynamicInput::Basic(unit);
    p.inputPitot->registerInput(unit);
    p.inputPitot->registerInput(flowUnit);

    if (defaultArea == NULL) {
        p.inputArea = new DynamicInput::Constant(FP::undefined());
    } else {
        p.inputArea = defaultArea->clone();
    }
    p.inputArea->registerInput(unit);
    p.inputArea->registerInput(flowUnit);

    if (defaultTemperature == NULL) {
        p.inputTemperature = new DynamicInput::Constant(0.0);
    } else {
        p.inputTemperature = defaultTemperature->clone();
    }
    p.inputTemperature->registerInput(unit);
    p.inputTemperature->registerInput(flowUnit);

    if (defaultPressure == NULL) {
        p.inputPressure = new DynamicInput::Constant(1013.25);
    } else {
        p.inputPressure = defaultTemperature->clone();
    }
    p.inputPressure->registerInput(unit);
    p.inputPressure->registerInput(flowUnit);

    int id = (int) processing.size();
    processing.push_back(p);

    if (control) {
        for (const auto &n : p.inputArea->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : p.inputTemperature->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : p.inputPressure->getUsedInputs()) {
            control->inputUnit(n, id);
        }

        control->inputUnit(unit, id);

        control->filterUnit(flowUnit, id);
    }
}

static double calculateFlow(double dP, double A, double t, double p)
{
    if (!FP::defined(dP) || dP < 0.0)
        return FP::undefined();
    if (!FP::defined(A) || A <= 0.0)
        return FP::undefined();
    if (dP == 0.0)
        return 0.0;
    if (!FP::defined(t) || t <= 0.0)
        return FP::undefined();
    if (!FP::defined(p) || p <= 0.0)
        return FP::undefined();

    double density = 1.2922;    /* kg/m^3 at 0C */
    density *= (273.15 / t) * (p / 1013.25);

    double v = sqrt(2.0 * (dP * 100.0) / density);   /* m/s */
    double Q = v * A;   /* m^3/s */

    Q *= 60000.0;   /* lpm */
    Q *= (p / 1013.25) * (273.15 / t);  /* To mass flow */

    return Q;
}

void CalcPitotFlow::process(int id, SequenceSegment &data)
{
    Processing *proc = &processing[id];

    double dP = proc->inputPitot->get(data);
    double A = proc->inputArea->get(data);
    double t = proc->inputTemperature->get(data);
    double p = proc->inputPressure->get(data);

    if (FP::defined(t) && t < 150.0) t += 273.15;

    double Q = calculateFlow(dP, A, t, p);

    for (const auto &i : proc->operateFlow->get(data)) {
        data[i].setDouble(Q);
    }
}


void CalcPitotFlow::processMeta(int id, SequenceSegment &data)
{
    Processing *p = &processing[id];

    Variant::Root meta;

    meta["By"].setString("calc_pitotflow");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    meta["Parameters"].hash("dP") = p->inputPitot->describe(data);
    meta["Parameters"].hash("Area") = p->inputArea->describe(data);
    meta["Parameters"].hash("T") = p->inputTemperature->describe(data);
    meta["Parameters"].hash("P") = p->inputPressure->describe(data);

    for (auto i : p->operateFlow->get(data)) {
        i.setMeta();
        if (!data[i].exists()) {
            data[i].metadataReal("Description").setString("Calculated flow from a pitot tube");
            data[i].metadataReal("Format").setString("00.000");
            data[i].metadataReal("Units").setString("lpm");
            data[i].metadataReal("ReportT").setDouble(0);
            data[i].metadataReal("ReportP").setDouble(1013.25);
        }
        data[i].metadata("Processing").toArray().after_back().set(meta);
    }
}


SequenceName::Set CalcPitotFlow::requestedInputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        Util::merge(p->inputPitot->getUsedInputs(), out);
        Util::merge(p->inputArea->getUsedInputs(), out);
        Util::merge(p->inputTemperature->getUsedInputs(), out);
        Util::merge(p->inputPressure->getUsedInputs(), out);
    }
    return out;
}

SequenceName::Set CalcPitotFlow::predictedOutputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        Util::merge(p->operateFlow->getAllUnits(), out);
    }
    return out;
}

QSet<double> CalcPitotFlow::metadataBreaks(int id)
{
    Processing *p = &processing[id];
    QSet<double> result;
    Util::merge(p->operateFlow->getChangedPoints(), result);
    Util::merge(p->inputPitot->getChangedPoints(), result);
    Util::merge(p->inputArea->getChangedPoints(), result);
    Util::merge(p->inputTemperature->getChangedPoints(), result);
    Util::merge(p->inputPressure->getChangedPoints(), result);
    return result;
}

CalcPitotFlow::CalcPitotFlow(QDataStream &stream)
{
    stream >> defaultArea;
    stream >> defaultTemperature;
    stream >> defaultPressure;
    stream >> filterSuffixes;
    stream >> restrictedInputs;

    quint32 n;
    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        Processing p;
        stream >> p.operateFlow;
        stream >> p.inputPitot;
        stream >> p.inputArea;
        stream >> p.inputTemperature;
        stream >> p.inputPressure;
        processing.push_back(p);
    }
}

void CalcPitotFlow::serialize(QDataStream &stream)
{
    stream << defaultArea;
    stream << defaultTemperature;
    stream << defaultPressure;
    stream << filterSuffixes;
    stream << restrictedInputs;

    quint32 n = (quint32) processing.size();
    stream << n;
    for (int i = 0; i < (int) n; i++) {
        stream << processing[i].operateFlow;
        stream << processing[i].inputPitot;
        stream << processing[i].inputArea;
        stream << processing[i].inputTemperature;
        stream << processing[i].inputPressure;
    }
}


QString CalcPitotFlowComponent::getBasicSerializationName() const
{ return QString::fromLatin1("calc_pitotflow"); }

ComponentOptions CalcPitotFlowComponent::getOptions()
{
    ComponentOptions options;

    options.add("input-t", new DynamicInputOption(tr("input-t", "name"), tr("Input temperature"),
                                                  tr("This is the temperature to use in calculations.  This is the "
                                                             "temperature of the air at the pitot tube."),
                                                  tr("273.15 K")));

    options.add("input-p", new DynamicInputOption(tr("input-p", "name"), tr("Input pressure"),
                                                  tr("This is the temperature to use in calculations.  This is the "
                                                             "pressure of the air at the pitot tube."),
                                                  tr("1013.25 hPa")));

    options.add("input-area",
                new DynamicInputOption(tr("input-area", "name"), tr("Cross sectional area"),
                                       tr("This is cross sectional area of the duct at the pitot tube in m².  "
                                                  "This option is mutually exclusive with the diameter."),
                                       QString()));

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(tr("diameter", "name"), tr("Tube diameter"),
                                            tr("This is the diameter of the flow tube in mm.  This is used to set "
                                                       "the cross sectional duct area for calculating the flow."),
                                            tr("44.45 mm = 1.75 in"));
    d->setMinimum(0, false);
    options.add("diameter", d);

    options.exclude("input-area", "diameter");
    options.exclude("diameter", "input-area");


    options.add("input-dp",
                new DynamicInputOption(tr("input-dp", "name"), tr("Pitot tube delta pressure"),
                                       tr("This is the delta pressure at the pitot tube.  This option is "
                                                  "mutually exclusive with with instrument specification."),
                                       tr("All delta pressures")));

    options.add("output-q",
                new DynamicSequenceSelectionOption(tr("output-q", "name"), tr("Output flow"),
                                                   tr("This is the output flow that is calculated.  This option is "
                                                              "mutually exclusive with with instrument specification."),
                                                   tr("Calculated from input pressures")));


    options.add("suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments", "name"),
                                                                 tr("Instrument suffixes to correct"),
                                                                 tr("These are the instrument suffixes to calculate flows for.  "
                                                                            "For example P01 would usually specifies the stack flow pitot "
                                                                            "sensor.  This option is mutually exclusive with manual "
                                                                            "variable specification."),
                                                                 tr("All instrument suffixes")));
    options.exclude("input-dp", "suffix");
    options.exclude("output-q", "suffix");
    options.exclude("suffix", "input-dp");
    options.exclude("suffix", "output-q");

    return options;
}

QList<ComponentExample> CalcPitotFlowComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will calculate flows for all delta pressures in the input "
                                                "assuming they are at STP with 1.75 in ID tubing.  For example, "
                                                "it will calculate Q_P01 from Pd_P01.")));

    options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")))->add("P01");
    (qobject_cast<DynamicInputOption *>(options.get("input-t")))->set(
            SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("T_V01")), Calibration());
    (qobject_cast<DynamicInputOption *>(options.get("input-p")))->set(
            SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("P_XM")), Calibration());
    (qobject_cast<DynamicInputOption *>(options.get("input-area")))->set(0.002);
    examples.append(ComponentExample(options,
                                     tr("Single instrument with measured temperature and pressure"),
                                     tr("This will calculate the flow for the P01 sensor using the measured "
                                                "temperature T_V01 and the pressure P_XM assuming a duct area of "
                                                "0.002 m².")));

    options = getOptions();
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-q")))->set("", "", "Q_X1");
    (qobject_cast<DynamicInputOption *>(options.get("input-dp")))->set(
            SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("Pd_XM")), Calibration());
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("diameter")))->set(30.0);
    examples.append(ComponentExample(options, tr("Single variable"),
                                     tr("This will calculate Q_X1 from Pd_XM for a flow tube with a 30 mm "
                                                "inner diameter.")));

    return examples;
}

SegmentProcessingStage *CalcPitotFlowComponent::createBasicFilterDynamic(const ComponentOptions &options)
{ return new CalcPitotFlow(options); }

SegmentProcessingStage *CalcPitotFlowComponent::createBasicFilterPredefined(const ComponentOptions &options,
                                                                            double start,
                                                                            double end,
                                                                            const QList<
                                                                                    SequenceName> &inputs)
{ return new CalcPitotFlow(options, start, end, inputs); }

SegmentProcessingStage *CalcPitotFlowComponent::createBasicFilterEditing(double start,
                                                                         double end,
                                                                         const SequenceName::Component &station,
                                                                         const SequenceName::Component &archive,
                                                                         const ValueSegment::Transfer &config)
{ return new CalcPitotFlow(start, end, station, archive, config); }

SegmentProcessingStage *CalcPitotFlowComponent::deserializeBasicFilter(QDataStream &stream)
{ return new CalcPitotFlow(stream); }
