/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"
#include "algorithms/dewpoint.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Algorithms;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    class ID {
    public:
        ID() : filter(), input()
        { }

        ID(const QSet<SequenceName> &setFilter, const QSet<SequenceName> &setInput) : filter(
                setFilter), input(setInput)
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }

        QSet<SequenceName> filter;
        QSet<SequenceName> input;
    };

    QHash<int, ID> contents;

    virtual void filterUnit(const SequenceName &unit, int targetID)
    {
        contents[targetID].input.remove(unit);
        contents[targetID].filter.insert(unit);
    }

    virtual void inputUnit(const SequenceName &unit, int targetID)
    {
        if (contents[targetID].filter.contains(unit))
            return;
        contents[targetID].input.insert(unit);
    }

    virtual bool isHandled(const SequenceName &unit)
    {
        for (QHash<int, ID>::const_iterator it = contents.constBegin();
                it != contents.constEnd();
                ++it) {
            if (it.value().filter.contains(unit))
                return true;
            if (it.value().input.contains(unit))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("calc_pitotflow"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("input-dp")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("input-area")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("input-t")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("input-p")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-q")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("diameter")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")));

        QVERIFY(options.excluded().value("diameter").contains("input-area"));
        QVERIFY(options.excluded().value("input-area").contains("diameter"));

        QVERIFY(options.excluded().value("output-q").contains("suffix"));
        QVERIFY(options.excluded().value("input-dp").contains("suffix"));
        QVERIFY(options.excluded().value("suffix").contains("output-q"));
        QVERIFY(options.excluded().value("suffix").contains("input-dp"));
    }

    void dynamicDefaults()
    {
        ComponentOptions options(component->getOptions());
        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        SegmentProcessingStage *filter2;
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "P_A11"), &controller);
        QVERIFY(controller.contents.isEmpty());
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2 != NULL);
            filter2->unhandled(SequenceName("brw", "raw", "P_A11"), &controller);
            QVERIFY(controller.contents.isEmpty());
            delete filter2;
        }

        SequenceName Pd_P01_1("brw", "raw", "Pd_P01");
        SequenceName Q_P01_1("brw", "raw", "Q_P01");
        SequenceName Pd_P01_2("sgp", "raw", "Pd_P01");
        SequenceName Q_P01_2("sgp", "raw", "Q_P01");
        SequenceName Pd_P11_1("brw", "raw", "Pd_P11");
        SequenceName Q_P11_1("brw", "raw", "Q_P11");

        filter->unhandled(Pd_P01_1, &controller);
        filter->unhandled(Q_P01_1, &controller);
        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << Q_P01_1,
                                                 QSet<SequenceName>() << Pd_P01_1));
        QCOMPARE(idL.size(), 1);
        int P01_1 = idL.at(0);

        filter->unhandled(SequenceName("brw", "raw", "X_P01"), &controller);
        QCOMPARE(controller.contents.size(), 1);

        filter->unhandled(Pd_P01_2, &controller);
        QCOMPARE(controller.contents.size(), 2);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << Q_P01_2,
                                                 QSet<SequenceName>() << Pd_P01_2));
        QCOMPARE(idL.size(), 1);
        int P01_2 = idL.at(0);

        filter->unhandled(Q_P11_1, &controller);
        filter->unhandled(Pd_P11_1, &controller);
        QCOMPARE(controller.contents.size(), 3);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << Q_P11_1,
                                                 QSet<SequenceName>() << Pd_P11_1));
        QCOMPARE(idL.size(), 1);
        int P11_1 = idL.at(0);

        data.setStart(15.0);
        data.setEnd(16.0);

        data.setValue(Pd_P01_1, Variant::Root(0.3));
        filter->process(P01_1, data);
        QCOMPARE(data.value(Q_P01_1).toDouble(), 634.447103797217);

        {
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
            }
            QVERIFY(filter2 != NULL);
            data.setValue(Q_P01_1, Variant::Root());
            data.setValue(Pd_P01_1, Variant::Root(0.3));
            filter->process(P01_1, data);
            QCOMPARE(data.value(Q_P01_1).toDouble(), 634.447103797217);
            delete filter2;
        }

        data.setValue(Pd_P01_2, Variant::Root(0.4));
        filter->process(P01_2, data);
        QCOMPARE(data.value(Q_P01_2).toDouble(), 732.5964123278033);

        data.setValue(Pd_P11_1, Variant::Root(0.35));
        filter->process(P11_1, data);
        QCOMPARE(data.value(Q_P11_1).toDouble(), 685.2811944276032);

        data.setValue(Pd_P11_1, Variant::Root(FP::undefined()));
        filter->process(P11_1, data);
        QVERIFY(!FP::defined(data.value(Q_P11_1).toDouble()));

        filter->processMeta(P01_1, data);
        QCOMPARE(data.value(Q_P01_1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);

        delete filter;
    }

    void dynamicOptions()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<DynamicInputOption *>(options.get("input-t"))->set(20.0);
        qobject_cast<DynamicInputOption *>(options.get("input-p"))->set(950.0);
        qobject_cast<DynamicInputOption *>(options.get("input-dp"))->set(
                SequenceMatch::OrderedLookup(
                        SequenceMatch::Element(QString("brw"), QString("raw"), "P_V11",
                                               SequenceName::Flavors())), Calibration());
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-q"))->set("brw", "raw",
                                                                                     "Q_V11",
                                                                                     SequenceName::Flavors());

        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "Pd_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "Pd_P01"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "Q_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "Q_P01"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "X_V11"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName P_V11("brw", "raw", "P_V11");
        SequenceName Q_V11("brw", "raw", "Q_V11");
        filter->unhandled(Q_V11, &controller);
        filter->unhandled(P_V11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << Q_V11,
                                                 QSet<SequenceName>() << P_V11));
        QCOMPARE(idL.size(), 1);
        int id = idL.at(0);

        data.setValue(P_V11, Variant::Root(0.35));
        filter->process(id, data);
        QCOMPARE(data.value(Q_V11).toDouble(), 640.5130219873953);

        delete filter;


        options = component->getOptions();
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->add("V11");
        qobject_cast<ComponentOptionSingleDouble *>(options.get("diameter"))->set(35.0);

        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        SequenceName Pd_V11("brw", "raw", "Pd_V11");

        filter->unhandled(SequenceName("brw", "raw", "Pd_P01"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "Q_P01"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "Q_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "Pd_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "T_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "P_V11"), &controller);
        QVERIFY(controller.contents.isEmpty());

        filter->unhandled(Pd_V11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << Q_V11,
                                                 QSet<SequenceName>() << Pd_V11));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        data.setValue(Pd_V11, Variant::Root(0.35));
        data.setValue(Q_V11, Variant::Root());
        filter->process(id, data);
        QCOMPARE(data.value(Q_V11).toDouble(), 424.8751902954947);

        delete filter;


        options = component->getOptions();
        qobject_cast<DynamicInputOption *>(options.get("input-area"))->set(0.0005);

        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        filter->unhandled(SequenceName("brw", "raw", "T_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "P_V11"), &controller);
        QVERIFY(controller.contents.isEmpty());

        filter->unhandled(Q_V11, &controller);
        filter->unhandled(Pd_V11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << Q_V11,
                                                 QSet<SequenceName>() << Pd_V11));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        data.setValue(Pd_V11, Variant::Root(0.41));
        data.setValue(Q_V11, Variant::Root());
        filter->process(id, data);
        QCOMPARE(data.value(Q_V11).toDouble(), 238.9809661507056);

        delete filter;
    }

    void predefined()
    {
        ComponentOptions options(component->getOptions());

        SequenceName Pd_A11("brw", "raw", "Pd_A11");
        SequenceName Q_A11("brw", "raw", "Q_A11");
        QList<SequenceName> input;
        input << Pd_A11;

        SegmentProcessingStage *filter =
                component->createBasicFilterPredefined(options, FP::undefined(), FP::undefined(),
                                                       input);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{Pd_A11}));
        QCOMPARE(filter->predictedOutputs(), (SequenceName::Set{Q_A11}));

        delete filter;
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["P01/CalculateFlow"] = "::Q_P01:=";
        cv["P01/PitotPressure"] = "::Pd_P01:=";
        cv["P01/Area"] = 0.0005;
        cv["P01/Temperature"] = "::T_P01:=";
        cv["P01/Pressure"] = "::P_P01:=";
        config.emplace_back(FP::undefined(), 13.0, cv);
        cv.write().setEmpty();
        cv["P01/CalculateFlow"] = "::Q_P01:=";
        cv["P01/PitotPressure"] = "::Pd_P01:=";
        cv["P01/Area"] = 0.0004;
        cv["P01/Temperature"] = "::T_V11:=";
        cv["P01/Pressure"] = 950.0;
        config.emplace_back(13.0, FP::undefined(), cv);

        SegmentProcessingStage
                *filter = component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config);
        QVERIFY(filter != NULL);
        TestController controller;

        SequenceName Q_P01("brw", "raw", "Q_P01");
        SequenceName Pd_P01("brw", "raw", "Pd_P01");
        SequenceName T_P01("brw", "raw", "T_P01");
        SequenceName P_P01("brw", "raw", "P_P01");
        SequenceName T_V11("brw", "raw", "T_V11");

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{T_P01, P_P01, T_V11, Pd_P01}));
        QCOMPARE(filter->predictedOutputs(), (SequenceName::Set{Q_P01}));

        filter->unhandled(T_P01, &controller);
        filter->unhandled(P_P01, &controller);
        filter->unhandled(Pd_P01, &controller);
        filter->unhandled(T_V11, &controller);

        QList<int> idL = controller.contents
                                   .keys(TestController::ID(QSet<SequenceName>() << Q_P01,
                                                            QSet<SequenceName>() << T_P01 << P_P01
                                                                                 << T_V11
                                                                                 << Pd_P01));
        QCOMPARE(idL.size(), 1);
        int P01 = idL.at(0);

        QCOMPARE(filter->metadataBreaks(P01), QSet<double>() << 13.0);

        SequenceSegment data1;
        data1.setStart(12.0);
        data1.setEnd(13.0);

        data1.setValue(T_P01, Variant::Root(10.0));
        data1.setValue(P_P01, Variant::Root(920.0));
        data1.setValue(Pd_P01, Variant::Root(0.25));
        filter->process(P01, data1);
        QCOMPARE(data1.value(Q_P01).toDouble(), 174.6502917462073);

        data1.setValue(T_P01, Variant::Root());
        data1.setValue(T_V11, Variant::Root(25.0));
        data1.setValue(P_P01, Variant::Root(920.0));
        data1.setValue(Pd_P01, Variant::Root(0.25));
        filter->process(P01, data1);
        QVERIFY(!FP::defined(data1.value(Q_P01).toDouble()));

        data1.setValue(T_P01, Variant::Root(10.0));
        data1.setValue(P_P01, Variant::Root());
        data1.setValue(Pd_P01, Variant::Root(0.25));
        filter->process(P01, data1);
        QVERIFY(!FP::defined(data1.value(Q_P01).toDouble()));

        data1.setValue(T_P01, Variant::Root(10.0));
        data1.setValue(P_P01, Variant::Root(920.0));
        data1.setValue(Pd_P01, Variant::Root());
        filter->process(P01, data1);
        QVERIFY(!FP::defined(data1.value(Q_P01).toDouble()));

        filter->processMeta(P01, data1);
        QCOMPARE(data1.value(Q_P01.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);


        SequenceSegment data2;
        data2.setStart(13.0);
        data2.setEnd(15.0);

        data2.setValue(Pd_P01, Variant::Root(0.5));
        data2.setValue(T_V11, Variant::Root(25.0));
        filter->process(P01, data2);
        QCOMPARE(data2.value(Q_P01).toDouble(), 195.6739740316079);

        data2.setValue(Pd_P01, Variant::Root());
        data2.setValue(T_V11, Variant::Root(25.0));
        filter->process(P01, data2);
        QVERIFY(!FP::defined(data1.value(Q_P01).toDouble()));

        data2.setValue(Pd_P01, Variant::Root(0.5));
        data2.setValue(T_V11, Variant::Root());
        filter->process(P01, data2);
        QVERIFY(!FP::defined(data1.value(Q_P01).toDouble()));

        filter->processMeta(P01, data2);
        QCOMPARE(data2.value(Q_P01.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);

        delete filter;
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
