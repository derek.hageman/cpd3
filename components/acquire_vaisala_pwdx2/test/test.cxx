/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>
#include <QtAlgorithms>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;
    double unpolledInterval;
    int unpolledReport;
    QByteArray id;
    bool lineOpen;
    bool pwdx0Mode;

    int visibilityAlarm;
    QSet<QByteArray> hardwareErrors;
    QSet<QByteArray> hardwareWarnings;

    double vis1Min;
    double vis10Min;

    int pwInstant;
    int pw15Min;
    int pw1Hour;
    QString nwsCode;

    double waterIntensity;
    double cumulativeWater;
    double cumulativeSnow;

    double ambientTemperature;

    double luminance;

    double signalValue;
    double signalOffset;
    double signalDrift;
    double receiverBSValue;
    double receiverBSChange;
    double transmitterBSValue;
    double transmitterBSChange;
    double ledControlV;
    double ambientControlV;
    double supplyV;
    double positiveV;
    double negativeV;
    double boardTemperature;
    double drdTemperature;
    double drdSignal;
    double drdDry;

    ModelInstrument()
            : incoming(),
              outgoing(),
              unpolledRemaining(0),
              unpolledInterval(15.0),
              unpolledReport(2),
              id(), lineOpen(false), pwdx0Mode(false)
    {

        visibilityAlarm = 0;

        vis1Min = 5.000;
        vis10Min = 6.000;

        pwInstant = 61;
        pw15Min = 62;
        pw1Hour = 63;
        nwsCode = "R-";

        waterIntensity = 0.3;
        cumulativeWater = 12.0;
        cumulativeSnow = 3.0;

        ambientTemperature = 23.4;

        luminance = 1234;


        signalValue = 3.43;
        signalOffset = 146.11;
        signalDrift = 0.0;
        receiverBSValue = 2802.0;
        receiverBSChange = 2;
        transmitterBSValue = -2.3;
        transmitterBSChange = 0.1;
        ledControlV = 2.3;
        ambientControlV = -1.0;
        supplyV = 12.6;
        positiveV = 11.4;
        negativeV = -11.3;
        boardTemperature = 29.0;
        drdTemperature = 24.0;
        drdSignal = 858.0;
        drdDry = 857.5;
    }

    void outputReportFlags()
    {
        outgoing.append(QByteArray::number(visibilityAlarm));
        if (hardwareErrors.contains("Backscatter High"))
            outgoing.append('3');
        else if (hardwareWarnings.contains("Backscatter Increased"))
            outgoing.append('4');
        else if (!hardwareErrors.isEmpty())
            outgoing.append('1');
        else if (!hardwareWarnings.isEmpty())
            outgoing.append('2');
        else
            outgoing.append('0');
    }

    void outputNumberOrSlashes(double n, int size, int decimals = 0)
    {
        if (!FP::defined(n)) {
            outgoing.append(' ');
            outgoing.append(QByteArray(size - 1, '/'));
            return;
        }
        outgoing.append(QByteArray::number(n, 'f', decimals).rightJustified(size, ' '));
    }

    void outputNumberOrSlashes(int n, int size)
    {
        if (n < 0) {
            outgoing.append(' ');
            outgoing.append(QByteArray(size - 1, '/'));
            return;
        }
        outgoing.append(QByteArray::number(n).rightJustified(size, ' '));
    }

    void outputReport0()
    {
        outputReportFlags();
        outputNumberOrSlashes(vis1Min * 1E3, 7);
        outputNumberOrSlashes(vis10Min * 1E3, 7);
    }

    void outputReport1()
    {
        outputReportFlags();
        outputNumberOrSlashes(vis1Min * 1E3, 7);
        outputNumberOrSlashes(pwInstant, 3);
        outputNumberOrSlashes(waterIntensity, 6, 1);
    }

    void outputReport2()
    {
        outputReportFlags();
        outputNumberOrSlashes(vis1Min * 1E3, 6);
        outputNumberOrSlashes(vis10Min * 1E3, 6);
        if (pwdx0Mode) {
            outgoing.append(" /// // // // ////// ////// ////");
            return;
        }
        outgoing.append(nwsCode.toLatin1().rightJustified(4, ' '));
        outputNumberOrSlashes(pwInstant, 3);
        outputNumberOrSlashes(pw15Min, 3);
        outputNumberOrSlashes(pw1Hour, 3);
        outputNumberOrSlashes(waterIntensity, 7, 2);
        outputNumberOrSlashes(cumulativeWater, 7, 2);
        outputNumberOrSlashes(cumulativeSnow, 5, 0);
    }

    void outputReport7()
    {
        outputReportFlags();
        outputNumberOrSlashes(vis1Min * 1E3, 6);
        outputNumberOrSlashes(vis10Min * 1E3, 6);
        outgoing.append(nwsCode.toLatin1().rightJustified(4, ' '));
        outputNumberOrSlashes(pwInstant, 3);
        outputNumberOrSlashes(pw15Min, 3);
        outputNumberOrSlashes(pw1Hour, 3);
        outputNumberOrSlashes(waterIntensity, 7, 2);
        outputNumberOrSlashes(cumulativeWater, 7, 2);
        outputNumberOrSlashes(cumulativeSnow, 5, 0);
        outputNumberOrSlashes(ambientTemperature, 6, 2);
        outputNumberOrSlashes(luminance, 6, 0);
        outgoing.append("\r\n-RA\r\nRERA\r\n");
    }

    void outputLabeledField(const QByteArray &name, double value, int totalLength, int digits)
    {
        outgoing.append(name);
        outgoing.append(' ');
        outgoing.append(QByteArray::number(value, 'f', digits).rightJustified(
                totalLength - name.length() - 1, ' '));
    }

    void outputStatus()
    {
        outgoing.append("VAISALA PWD22 V 1.00 2003-12-15 SN:Y46101\r\n\r\n");

        outputLabeledField("SIGNAL", signalValue, 16, 2);
        outgoing.append(' ');
        outputLabeledField("OFFSET", signalOffset, 16, 2);
        outgoing.append(' ');
        outputLabeledField("DRIFT", signalDrift, 16, 2);
        outgoing.append("\r\n");

        outputLabeledField("REC. BACKSCATTER", receiverBSValue, 25, 1);
        outgoing.append("  ");
        outputLabeledField("CHANGE", receiverBSChange, 13, 1);
        outgoing.append("\r\n");
        outputLabeledField("TR. BACKSCATTER", transmitterBSValue, 25, 1);
        outgoing.append("  ");
        outputLabeledField("CHANGE", transmitterBSChange, 13, 1);
        outgoing.append("\r\n");

        outputLabeledField("LEDI", ledControlV, 11, 1);
        outgoing.append("  ");
        outputLabeledField("AMBL", ambientControlV, 12, 1);
        outgoing.append("\r\n");

        outputLabeledField("VBB", supplyV, 11, 1);
        outgoing.append("  ");
        outputLabeledField("P12", positiveV, 12, 1);
        outgoing.append("  ");
        outputLabeledField("M12", negativeV, 12, 1);
        outgoing.append("\r\n");

        outputLabeledField("TS", ambientTemperature, 11, 1);
        outgoing.append("  ");
        outputLabeledField("TB", boardTemperature, 11, 0);
        outgoing.append("\r\n");

        outputLabeledField("TDRD", drdTemperature, 11, 0);
        outgoing.append("    25  ");
        outputLabeledField("DRD", drdSignal, 12, 0);
        outgoing.append("  843  ");
        outputLabeledField("DRY", drdDry, 12, 1);
        outgoing.append("\r\n");

        if (FP::defined(luminance)) {
            outputLabeledField("BL", luminance, 11, 0);
            outgoing.append("\r\n");
        }

        outgoing.append("RELAYS  OFF OFF OFF\r\n\r\n");
        outgoing.append("HOOD HEATERS OFF\r\n");
        outgoing.append("HARDWARE :\r\n");
        if (hardwareErrors.isEmpty() && hardwareWarnings.isEmpty()) {
            outgoing.append(" OK\r\n");
        } else {
            QList<QByteArray> output(hardwareErrors.values());
            output.append(hardwareWarnings.values());
            std::sort(output.begin(), output.end());
            for (QList<QByteArray>::const_iterator add = output.constBegin(),
                    endAdd = output.constEnd(); add != endAdd; ++add) {
                outgoing.append(*add);
                outgoing.append("\r\n");
            }
        }
    }

    void outputReport(int type)
    {
        outgoing.append((char) 1);
        outgoing.append("PW ");
        if (!id.isEmpty())
            outgoing.append(id);
        else
            outgoing.append(" 1");
        outgoing.append((char) 2);

        switch (type) {
        case 0:
            outputReport0();
            break;
        case 1:
            outputReport1();
            break;
        case 2:
            outputReport2();
            break;
        case 7:
            outputReport7();
            break;
        case 3:
            outputStatus();
            break;
        default:
            outgoing.append("ERROR");
            break;
        }

        outgoing.append((char) 3);
        outgoing.append("\r\n");
    }

    void outputParameters()
    {
        outgoing.append("SYSTEM PARAMETERS\r\n"
                                "VAISALA PWD22 v 1.00 2003-04-09 SN:X1234567 ID STRING:\r\n"
                                "AUTOMATIC MESSAGE 0 INTERVAL 0\r\n"
                                "BAUD RATE: 9600 N81\r\n"
                                "ALARM LIMIT 1 0\r\n"
                                "ALARM LIMIT 2 0\r\n"
                                "ALARM LIMIT 3 0\r\n"
                                "RELAY ON DELAY 10 OFF DELAY 11\r\n"
                                "OFFSET REF 152.38\r\n"
                                "CLEAN REFERENCES\r\n"
                                "TRANSMITTER 5.0 RECEIVER 1200\r\n"
                                "CONTAMINATION WARNING LIMITS\r\n"
                                "TRANSMITTER 0.5 RECEIVER 300\r\n"
                                "CONTAMINATION ALARM LIMITS\r\n"
                                "TRANSMITTER 3.0 RECEIVER 600\r\n"
                                "SIGN SIGNAL 1 1.000\r\n"
                                "DAC MODE: EXT1\r\n"
                                "MAX VIS 20000, 20.0 mA\r\n"
                                "MIN VIS 180, 4.5 mA\r\n"
                                "20 mA SCALE_1 184.6, SC_0 -2.8\r\n"
                                "1 mA SCALE_1 184.8, SC_0 -1.4\r\n");
    }

    void outputWeatherParameters()
    {
        outgoing.append("WEATHER PARAMETERS\r\n\r\n"
                                "PRECIPITATION LIMIT     40\r\n"
                                "WEATHER UPDATE DELAY     6\r\n"
                                "RAIN INTENSITY SCALE  1.00\r\n"
                                "HEAVY RAIN LIMIT       8.0\r\n"
                                "LIGHT RAIN LIMIT       2.0\r\n"
                                "SNOW LIMIT             5.0\r\n"
                                "HEAVY SNOW LIMIT       600\r\n"
                                "LIGHT SNOW LIMIT      1200\r\n"
                                "DRD SCALE              1.0\r\n"
                                "DRD DRY OFFSET       809.5\r\n"
                                "DRD WET SCALE       0.0017\r\n");
    }

    void advance(double seconds)
    {
        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR));
            incoming = incoming.mid(idxCR + 1);
            if (line.isEmpty()) {
                continue;
            }

            if (!lineOpen) {
                if (line.startsWith("OPEN")) {
                    if (pwdx0Mode) {
                        outgoing.append("PWD OPENED FOR OPERATOR COMMANDS\r\n");
                    } else {
                        outgoing.append("LINE OPENED FOR OPERATOR COMMANDS\r\n");
                    }
                    lineOpen = true;
                } else if (line.startsWith(QByteArray(1, (char) 0x05) +
                                                   "PW " +
                                                   (id.isEmpty() ? QByteArray(" 1")
                                                                 : id.rightJustified(2, ' ')) +
                                                   QByteArray(" "))) {
                    bool ok = false;
                    int record = line.mid(6).toInt(&ok);
                    if (ok) {
                        outputReport(record);
                    }
                }
                continue;
            }

            if (line == "CLOSE") {
                outgoing.append("LINE CLOSED\r\n");
                if (FP::defined(unpolledInterval) && unpolledInterval > 0 && unpolledReport >= 0) {
                    unpolledRemaining = unpolledInterval;
                } else {
                    unpolledRemaining = FP::undefined();
                }
                lineOpen = false;
            } else if (line.startsWith("AMES ")) {
                QList<QByteArray> fields(line.simplified().split(' '));
                if (fields.size() == 3) {
                    bool recordOK = false;
                    int record = fields.at(1).toInt(&recordOK);
                    bool intervalOK = false;
                    int interval = fields.at(1).toInt(&intervalOK);
                    if (recordOK &&
                            intervalOK &&
                            record >= 0 &&
                            record <= 7 &&
                            interval > 1 &&
                            interval <= 255) {
                        interval = interval / 15;
                        interval *= 15;
                        if (interval < 15)
                            interval = 15;
                        unpolledInterval = (double) interval;
                        unpolledReport = record;
                        outgoing.append("UNPOLLED SET\r\n");
                    } else {
                        outgoing.append("ERROR\r\n");
                    }
                } else {
                    outgoing.append("ERROR\r\n");
                }
            } else if (line == "STA") {
                outgoing.append("PWD STATUS\r\n");
                outputStatus();
            } else if (line == "PAR") {
                outputParameters();
            } else if (line == "WPAR") {
                if (!pwdx0Mode) {
                    outputWeatherParameters();
                }
            } else if (line.startsWith("TIME ")) {
                QList<QByteArray> fields(line.simplified().split(' '));
                if (fields.length() == 4) {
                    bool hourOk = false;
                    int hour = fields.at(1).toInt(&hourOk);
                    bool minuteOk = false;
                    int minute = fields.at(2).toInt(&minuteOk);
                    bool secondOk = false;
                    int second = fields.at(3).toInt(&secondOk);
                    if (hourOk &&
                            minuteOk &&
                            secondOk &&
                            hour >= 0 &&
                            hour <= 23 &&
                            minute >= 0 &&
                            minute <= 59 &&
                            second >= 0 &&
                            seconds <= 60) {
                        outgoing.append("TIME SET\r\n");
                    } else {
                        outgoing.append("ERROR\r\n");
                    }
                } else {
                    outgoing.append("ERROR\r\n");
                }
            } else if (line.startsWith("DATE ")) {
                QList<QByteArray> fields(line.simplified().split(' '));
                if (fields.length() == 4) {
                    bool yearOk = false;
                    int year = fields.at(1).toInt(&yearOk);
                    bool monthOk = false;
                    int month = fields.at(2).toInt(&monthOk);
                    bool dayOk = false;
                    int day = fields.at(3).toInt(&dayOk);
                    if (yearOk &&
                            monthOk &&
                            dayOk &&
                            year >= 1970 &&
                            year <= 2999 &&
                            month >= 1 &&
                            month <= 12 &&
                            day >= 1 &&
                            day <= 31) {
                        outgoing.append("DATE SET\r\n");
                    } else {
                        outgoing.append("ERROR\r\n");
                    }
                } else {
                    outgoing.append("ERROR\r\n");
                }
            } else if (line.simplified().isEmpty()) {
            } else {
                outgoing.append("ERROR\r\n");
            }
        }

        if (FP::defined(unpolledRemaining) && unpolledReport >= 0) {
            unpolledRemaining -= seconds;
            while (unpolledRemaining <= 0.0) {
                unpolledRemaining += unpolledInterval;
                outputReport(unpolledReport);
            }
        }
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("WZ", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("WI", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("WX1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("WX2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("I", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("C1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("C2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZBsp", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZBsx", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZPARAMETERS", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("C3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("C4", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZBspd", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZBsxd", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V4", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V5", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZWXState", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZWXNWS", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZWZ1Min", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZWZ10Min", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZWXStateInstant", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZWXInstant1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZWXInstant2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZWXState15Min", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZWX15Min1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZWX15Min2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZWXState1Hour", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZWX1Hour1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZWX1Hour2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZSTATE", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    double visibilityValue(const ModelInstrument &model, double interval = FP::undefined())
    {
        if (!FP::defined(interval))
            interval = model.unpolledInterval;
        if (!FP::defined(interval))
            interval = 15.0;

        if (interval < 5.0 * 60.0)
            return model.vis1Min;
        return model.vis10Min;
    }

    int weatherValue(const ModelInstrument &model, double interval = FP::undefined())
    {
        if (!FP::defined(interval))
            interval = model.unpolledInterval;
        if (!FP::defined(interval))
            interval = 15.0;

        if (interval < 7.5 * 60.0)
            return model.pwInstant;
        if (interval < 30.0 * 60.0)
            return model.pw15Min;
        return model.pw1Hour;
    }

    bool checkContiguousRecord0(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("WZ"))
            return false;
        return true;
    }

    bool checkRecord0(StreamCapture &stream, const ModelInstrument &model,
                      double interval = FP::undefined(),
                      double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("WZ", Variant::Root(visibilityValue(model, interval)),
                                        time))
            return false;
        return true;
    }

    bool checkRealtimeRecord0(StreamCapture &stream, const ModelInstrument &model,
                              double interval = FP::undefined(),
                              double time = FP::undefined())
    {
        Q_UNUSED(interval);
        if (!stream.hasAnyMatchingValue("ZWZ1Min", Variant::Root(model.vis1Min), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWZ10Min", Variant::Root(model.vis10Min), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZSTATE", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkPersistentRecord0(const SequenceValue::Transfer &values, const ModelInstrument &model,
                                double interval = FP::undefined())
    {
        Q_UNUSED(values);
        Q_UNUSED(model);
        Q_UNUSED(interval);
        return true;
    }

    bool checkContiguousRecord1(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("WZ"))
            return false;
        if (!stream.checkContiguous("WX1"))
            return false;
        if (!stream.checkContiguous("WI"))
            return false;
        return true;
    }

    bool checkRecord1(StreamCapture &stream, const ModelInstrument &model,
                      double interval = FP::undefined(),
                      double time = FP::undefined())
    {
        Q_UNUSED(interval);
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("WZ", Variant::Root(model.vis1Min), time))
            return false;
        if (!stream.hasAnyMatchingValue("WX1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("WI", Variant::Root(model.waterIntensity), time))
            return false;
        return true;
    }

    bool checkRealtimeRecord1(StreamCapture &stream, const ModelInstrument &model,
                              double interval = FP::undefined(),
                              double time = FP::undefined())
    {
        Q_UNUSED(interval);
        if (!stream.hasAnyMatchingValue("ZWZ1Min", Variant::Root(model.vis1Min), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXState", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXStateInstant", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXInstant1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXInstant2", Variant::Root(model.pwInstant), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZSTATE", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkPersistentRecord1(const SequenceValue::Transfer &values, const ModelInstrument &model,
                                double interval = FP::undefined())
    {
        Q_UNUSED(interval);
        if (!StreamCapture::findValue(values, "raw", "WX2", Variant::Root(model.pwInstant)))
            return false;
        return true;
    }

    bool checkContiguousRecord2(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("WZ"))
            return false;
        if (!stream.checkContiguous("WX1"))
            return false;
        if (!stream.checkContiguous("WI"))
            return false;
        return true;
    }

    bool checkRecord2(StreamCapture &stream, const ModelInstrument &model,
                      double interval = FP::undefined(),
                      double time = FP::undefined())
    {
        Q_UNUSED(interval);
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("WZ", Variant::Root(visibilityValue(model, interval)),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("WX1", Variant::Root(), time))
            return false;
        if (!model.pwdx0Mode) {
            if (!stream.hasAnyMatchingValue("WI", Variant::Root(model.waterIntensity), time))
                return false;
        }
        return true;
    }

    bool checkRealtimeRecord2(StreamCapture &stream, const ModelInstrument &model,
                              double interval = FP::undefined(),
                              double time = FP::undefined())
    {
        Q_UNUSED(interval);
        if (!stream.hasAnyMatchingValue("ZWZ1Min", Variant::Root(model.vis1Min), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWZ10Min", Variant::Root(model.vis10Min), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXState", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXStateInstant", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXInstant1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXInstant2", Variant::Root(model.pwInstant), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXState15Min", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWX15Min1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWX15Min2", Variant::Root(model.pw15Min), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXState1Hour", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWX1Hour1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWX1Hour2", Variant::Root(model.pw1Hour), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXNWS", Variant::Root(model.nwsCode), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZSTATE", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkPersistentRecord2(const SequenceValue::Transfer &values, const ModelInstrument &model,
                                double interval = FP::undefined())
    {
        Q_UNUSED(interval);
        if (!StreamCapture::findValue(values, "raw", "WX2",
                                      Variant::Root(weatherValue(model, interval))))
            return false;
        return true;
    }

    bool checkContiguousRecord7(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("WZ"))
            return false;
        if (!stream.checkContiguous("WX1"))
            return false;
        if (!stream.checkContiguous("WI"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("I"))
            return false;
        return true;
    }

    bool checkRecord7(StreamCapture &stream, const ModelInstrument &model,
                      double interval = FP::undefined(),
                      double time = FP::undefined())
    {
        Q_UNUSED(interval);
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("WZ", Variant::Root(visibilityValue(model, interval)),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("WX1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("WI", Variant::Root(model.waterIntensity), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.ambientTemperature), time))
            return false;
        if (!stream.hasAnyMatchingValue("I", Variant::Root(model.luminance), time))
            return false;
        return true;
    }

    bool checkRealtimeRecord7(StreamCapture &stream, const ModelInstrument &model,
                              double interval = FP::undefined(),
                              double time = FP::undefined())
    {
        Q_UNUSED(interval);
        if (!stream.hasAnyMatchingValue("ZWZ1Min", Variant::Root(model.vis1Min), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWZ10Min", Variant::Root(model.vis10Min), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXState", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXStateInstant", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXInstant1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXInstant2", Variant::Root(model.pwInstant), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXState15Min", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWX15Min1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWX15Min2", Variant::Root(model.pw15Min), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXState1Hour", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWX1Hour1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWX1Hour2", Variant::Root(model.pw1Hour), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWXNWS", Variant::Root(model.nwsCode), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZSTATE", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkPersistentRecord7(const SequenceValue::Transfer &values, const ModelInstrument &model,
                                double interval = FP::undefined())
    {
        Q_UNUSED(interval);
        if (!StreamCapture::findValue(values, "raw", "WX2",
                                      Variant::Root(weatherValue(model, interval))))
            return false;
        return true;
    }

    bool checkContiguousStatus(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("C1"))
            return false;
        if (!stream.checkContiguous("C2"))
            return false;
        if (!stream.checkContiguous("ZBsp"))
            return false;
        if (!stream.checkContiguous("ZBsx"))
            return false;
        if (!stream.checkContiguous("V1"))
            return false;
        if (!stream.checkContiguous("V2"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("T3"))
            return false;
        return true;
    }

    bool checkStatus(StreamCapture &stream, const ModelInstrument &model,
                     double interval = FP::undefined(),
                     double time = FP::undefined())
    {
        Q_UNUSED(interval);
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("C1", Variant::Root(model.signalValue), time))
            return false;
        if (!stream.hasAnyMatchingValue("C2", Variant::Root(model.signalOffset), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBsp", Variant::Root(model.receiverBSValue), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBsx", Variant::Root(model.transmitterBSValue), time))
            return false;
        if (!stream.hasAnyMatchingValue("V1", Variant::Root(model.ledControlV), time))
            return false;
        if (!stream.hasAnyMatchingValue("V2", Variant::Root(model.ambientControlV), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.ambientTemperature), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.boardTemperature), time))
            return false;
        if (!stream.hasAnyMatchingValue("T3", Variant::Root(model.drdTemperature), time))
            return false;
        if (FP::defined(model.luminance)) {
            if (!stream.hasAnyMatchingValue("I", Variant::Root(model.luminance), time))
                return false;
        }
        return true;
    }

    bool checkRealtimeStatus(StreamCapture &stream, const ModelInstrument &model,
                             double interval = FP::undefined(),
                             double time = FP::undefined())
    {
        Q_UNUSED(interval);
        if (!stream.hasAnyMatchingValue("C3", Variant::Root(model.signalDrift), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBspd", Variant::Root(model.receiverBSChange), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBsxd", Variant::Root(model.transmitterBSChange), time))
            return false;
        if (!stream.hasAnyMatchingValue("V3", Variant::Root(model.supplyV), time))
            return false;
        if (!stream.hasAnyMatchingValue("V4", Variant::Root(model.positiveV), time))
            return false;
        if (!stream.hasAnyMatchingValue("V5", Variant::Root(model.negativeV), time))
            return false;
        if (!stream.hasAnyMatchingValue("C4", Variant::Root(model.drdSignal), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZSTATE", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkPersistentStatus(const SequenceValue::Transfer &values, const ModelInstrument &model,
                               double interval = FP::undefined())
    {
        Q_UNUSED(values);
        Q_UNUSED(model);
        Q_UNUSED(interval);
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_vaisala_pwdx2"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_vaisala_pwdx2"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe0()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledReport = 0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(3.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(3.0);
            QTest::qSleep(50);
        }
        Util::append(interface->getPersistentValues(), persistentValues);
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguousRecord0(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkRecord0(logging, instrument));
        QVERIFY(checkRecord0(realtime, instrument));
        QVERIFY(checkRealtimeRecord0(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkPersistentRecord0(persistentValues, instrument));
        QVERIFY(checkPersistentRecord0(realtime.values(), instrument));

    }

    void passiveAutoprobe1()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledReport = 1;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(3.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(3.0);
            QTest::qSleep(50);
        }
        Util::append(interface->getPersistentValues(), persistentValues);
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguousRecord1(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkRecord1(logging, instrument));
        QVERIFY(checkRecord1(realtime, instrument));
        QVERIFY(checkRealtimeRecord1(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkPersistentRecord1(persistentValues, instrument));
        QVERIFY(checkPersistentRecord1(realtime.values(), instrument));

    }

    void passiveAutoprobe2()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"].setDouble(8.0 * 60.0);
        cv["ID"].setString("AB");
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledReport = 2;
        instrument.unpolledInterval = 8.0 * 60.0;
        instrument.hardwareErrors |= "DRD Error";
        instrument.id = "AB";
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(60.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(60.0);
            QTest::qSleep(50);
        }
        Util::append(interface->getPersistentValues(), persistentValues);
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguousRecord2(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkRecord2(logging, instrument, 8 * 60));
        QVERIFY(checkRecord2(realtime, instrument, 8 * 60));
        QVERIFY(checkRealtimeRecord2(realtime, instrument, 8 * 60));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkPersistentRecord2(persistentValues, instrument, 8 * 60));
        QVERIFY(checkPersistentRecord2(realtime.values(), instrument, 8 * 60));

        QVERIFY(logging.hasAnyMatchingValue("F1", Variant::Root(Variant::Flags{"HardwareError"})));
        QVERIFY(realtime.hasAnyMatchingValue("F1", Variant::Root(Variant::Flags{"HardwareError"})));

    }

    void passiveAutoprobe3()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledReport = 3;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(3.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(3.0);
            QTest::qSleep(50);
        }
        Util::append(interface->getPersistentValues(), persistentValues);
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguousRecord1(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkStatus(logging, instrument));
        QVERIFY(checkStatus(realtime, instrument));
        QVERIFY(checkRealtimeStatus(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkPersistentStatus(persistentValues, instrument));
        QVERIFY(checkPersistentStatus(realtime.values(), instrument));

    }

    void passiveAutoprobe7()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledReport = 7;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(3.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(3.0);
            QTest::qSleep(50);
        }
        Util::append(interface->getPersistentValues(), persistentValues);
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguousRecord7(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkRecord7(logging, instrument));
        QVERIFY(checkRecord7(realtime, instrument));
        QVERIFY(checkRealtimeRecord7(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkPersistentRecord7(persistentValues, instrument));
        QVERIFY(checkPersistentRecord7(realtime.values(), instrument));

    }

    void interactiveAutoprobePWDx0()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.pwdx0Mode = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.2);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 2000; i++) {
            control.advance(0.2);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        Util::append(interface->getPersistentValues(), persistentValues);
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguousRecord2(logging));
        QVERIFY(checkContiguousStatus(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkRecord2(logging, instrument));
        QVERIFY(checkRecord2(realtime, instrument));
        QVERIFY(checkStatus(logging, instrument));
        QVERIFY(checkStatus(realtime, instrument));
        QVERIFY(checkRealtimeStatus(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkPersistentStatus(persistentValues, instrument));
        QVERIFY(checkPersistentStatus(realtime.values(), instrument));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.2);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 2000; i++) {
            control.advance(0.2);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        Util::append(interface->getPersistentValues(), persistentValues);
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguousRecord2(logging));
        QVERIFY(checkContiguousStatus(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkRecord2(logging, instrument));
        QVERIFY(checkRecord2(realtime, instrument));
        QVERIFY(checkRealtimeRecord2(realtime, instrument));
        QVERIFY(checkStatus(logging, instrument));
        QVERIFY(checkStatus(realtime, instrument));
        QVERIFY(checkRealtimeStatus(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkPersistentRecord2(persistentValues, instrument));
        QVERIFY(checkPersistentRecord2(realtime.values(), instrument));
        QVERIFY(checkPersistentStatus(persistentValues, instrument));
        QVERIFY(checkPersistentStatus(realtime.values(), instrument));

        QVERIFY(StreamCapture::findValue(persistentValues, "raw", "ZPARAMETERS",
                                         Variant::Root(40.0), "Weather/PRECIPITATION LIMIT"));

    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledReport = 2;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(5.0);
            QTest::qSleep(50);
        }

        Util::append(interface->getPersistentValues(), persistentValues);
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguousRecord2(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkRecord2(logging, instrument));
        QVERIFY(checkRecord2(realtime, instrument));
        QVERIFY(checkRealtimeRecord2(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkPersistentRecord2(persistentValues, instrument));
        QVERIFY(checkPersistentRecord2(realtime.values(), instrument));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        while (control.time() < 60.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        instrument.hardwareErrors |= "Signal Error";
        for (int i = 0; i < 100; i++) {
            control.advance(5.0);
            QTest::qSleep(50);
        }

        Util::append(interface->getPersistentValues(), persistentValues);
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguousRecord2(logging));
        QVERIFY(checkContiguousStatus(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkRecord2(logging, instrument));
        QVERIFY(checkRecord2(realtime, instrument));
        QVERIFY(checkRealtimeRecord2(realtime, instrument));
        QVERIFY(checkStatus(logging, instrument));
        QVERIFY(checkStatus(realtime, instrument));
        QVERIFY(checkRealtimeStatus(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkPersistentRecord2(persistentValues, instrument));
        QVERIFY(checkPersistentRecord2(realtime.values(), instrument));
        QVERIFY(checkPersistentStatus(persistentValues, instrument));
        QVERIFY(checkPersistentStatus(realtime.values(), instrument));

        QVERIFY(StreamCapture::findValue(persistentValues, "raw", "ZPARAMETERS",
                                         Variant::Root(40.0),
                                         "Weather/PRECIPITATION LIMIT"));

        QVERIFY(logging.hasAnyMatchingValue("F1", Variant::Root(
                Variant::Flags{"HardwareError", "SignalError"})));
        QVERIFY(realtime.hasAnyMatchingValue("F1", Variant::Root(
                Variant::Flags{"HardwareError", "SignalError"})));

    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
