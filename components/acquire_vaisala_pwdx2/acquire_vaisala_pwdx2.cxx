/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/wavelength.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_vaisala_pwdx2.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;


AcquireVaisalaPWDx2::Configuration::Configuration() : start(FP::undefined()),
                                                      end(FP::undefined()),
                                                      strictMode(true),
                                                      pollInterval(15.0),
                                                      id(),
                                                      visibilitySource(VisibilitySource_Auto),
                                                      presentWeatherSource(
                                                              PresentWeatherSource_Auto)
{ }

AcquireVaisalaPWDx2::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          strictMode(other.strictMode),
          pollInterval(other.pollInterval),
          id(other.id),
          visibilitySource(other.visibilitySource),
          presentWeatherSource(other.presentWeatherSource)
{ }

AcquireVaisalaPWDx2::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          strictMode(true),
          pollInterval(15.0),
          id(),
          visibilitySource(VisibilitySource_Auto),
          presentWeatherSource(PresentWeatherSource_Auto)
{
    setFromSegment(other);
}

AcquireVaisalaPWDx2::Configuration::Configuration(const Configuration &under,
                                                  const ValueSegment &over,
                                                  double s,
                                                  double e) : start(s),
                                                              end(e),
                                                              strictMode(under.strictMode),
                                                              pollInterval(under.pollInterval),
                                                              id(under.id),
                                                              visibilitySource(
                                                                      under.visibilitySource),
                                                              presentWeatherSource(
                                                                      under.presentWeatherSource)
{
    setFromSegment(over);
}

void AcquireVaisalaPWDx2::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();
    if (FP::defined(config["PollInterval"].toDouble()))
        pollInterval = config["PollInterval"].toDouble();

    if (config["ID"].exists())
        id = config["ID"].toString();

    switch (config["Visibility"].getType()) {
    case Variant::Type::String: {
        const auto &value = config["Visibility"].toString();
        if (Util::equal_insensitive(value, "minute", "1min", "1minute"))
            visibilitySource = VisibilitySource_1Min;
        else if (Util::equal_insensitive(value, "10min", "10minute"))
            visibilitySource = VisibilitySource_10Min;
        else
            visibilitySource = VisibilitySource_Auto;
        break;
    }
    case Variant::Type::Real: {
        double value = config["Visibility"].toDouble();
        if (!FP::defined(value) || value <= 0.0)
            visibilitySource = VisibilitySource_Auto;
        else if (value <= 60.0)
            visibilitySource = VisibilitySource_1Min;
        else
            visibilitySource = VisibilitySource_10Min;
        break;
    }
    default:
        break;
    }

    switch (config["PresentWeather"].getType()) {
    case Variant::Type::String: {
        const auto &value = config["PresentWeather"].toString();
        if (Util::equal_insensitive(value, "instant", "instantaneous"))
            presentWeatherSource = PresentWeatherSource_Instant;
        else if (Util::equal_insensitive(value, "15min", "15minute"))
            presentWeatherSource = PresentWeatherSource_15Min;
        else if (Util::equal_insensitive(value, "hour", "1hour"))
            presentWeatherSource = PresentWeatherSource_1Hour;
        else
            presentWeatherSource = PresentWeatherSource_Auto;
        break;
    }
    case Variant::Type::Real: {
        double value = config["PresentWeather"].toDouble();
        if (!FP::defined(value) || value < 0.0)
            presentWeatherSource = PresentWeatherSource_Auto;
        else if (value < 15.0 * 60.0)
            presentWeatherSource = PresentWeatherSource_Instant;
        else if (value < 3600.0)
            presentWeatherSource = PresentWeatherSource_15Min;
        else
            presentWeatherSource = PresentWeatherSource_1Hour;
        break;
    }
    default:
        break;
    }
}


void AcquireVaisalaPWDx2::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Vaisala");
    instrumentMeta["Model"].setString("PWDx2");

    lastReportInterval = FP::undefined();
    lastRecordTime = FP::undefined();
    loggingMux.clear();
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;
    }

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    haveEmittedParameters = false;
    realtimeStateUpdated = true;
    pollID.clear();
    selectedVisibilitySource = Visibility_1Min;
    selectedPresentWeatherSource = Weather_Instant;

    priorDisplayFlags.clear();
    alarmFlags.clear();
    statusHardwareFlags.clear();

    currentWX2.setUnit(SequenceName({}, "raw", "WX2"));
}

AcquireVaisalaPWDx2::AcquireVaisalaPWDx2(const ValueSegment::Transfer &configData,
                                         const std::string &loggingContext) : FramedInstrument(
        "pwdx2", loggingContext),
                                                                              lastRecordTime(
                                                                                      FP::undefined()),
                                                                              autoprobeStatus(
                                                                                      AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                              responseState(
                                                                                      RESP_PASSIVE_WAIT),
                                                                              autoprobeValidRecords(
                                                                                      0),
                                                                              loggingMux(
                                                                                      LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireVaisalaPWDx2::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void AcquireVaisalaPWDx2::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireVaisalaPWDx2Component::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireVaisalaPWDx2::AcquireVaisalaPWDx2(const ComponentOptions &options,
                                         const std::string &loggingContext) : FramedInstrument(
        "pwdx2", loggingContext),
                                                                              lastRecordTime(
                                                                                      FP::undefined()),
                                                                              autoprobeStatus(
                                                                                      AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                              responseState(
                                                                                      RESP_PASSIVE_WAIT),
                                                                              autoprobeValidRecords(
                                                                                      0),
                                                                              loggingMux(
                                                                                      LogStream_TOTAL)
{
    Q_UNUSED(options);

    setDefaultInvalid();

    config.append(Configuration());
}

AcquireVaisalaPWDx2::~AcquireVaisalaPWDx2()
{
}


void AcquireVaisalaPWDx2::logValue(double frameTime,
                                   int streamID,
                                   SequenceName::Component name,
                                   CPD3::Data::Variant::Root &&value)
{
    double startTime = streamTime[streamID];
    double endTime = frameTime;
    Q_ASSERT(FP::defined(endTime));
    if (!FP::defined(startTime) || startTime == endTime || !loggingEgress) {
        if (loggingEgress)
            streamTime[streamID] = endTime;
        if (!realtimeEgress)
            return;
        realtimeEgress->emplaceData(SequenceName({}, "raw", std::move(name)), std::move(value),
                                    frameTime, frameTime + 0.5);
        return;
    }

    SequenceValue
            dv(SequenceName({}, "raw", std::move(name)), std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        loggingMux.incoming(streamID, std::move(dv), loggingEgress);
        return;
    }

    loggingMux.incoming(streamID, dv, loggingEgress);
    dv.setStart(endTime);
    dv.setEnd(endTime + 1.0);
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireVaisalaPWDx2::realtimeValue(double time,
                                        SequenceName::Component name,
                                        Variant::Root &&value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

void AcquireVaisalaPWDx2::advanceStream(double frameTime, int streamID)
{
    streamAge[streamID]++;
    streamTime[streamID] = frameTime;
}

void AcquireVaisalaPWDx2::realtimeWeatherCodeTranslation(Variant::Write &target) const
{
    target.hash("Clear").setString(QObject::tr("Clear"));
    target.hash("HazeHighVis").setString(QObject::tr("Haze with visibility greater than 1km"));
    target.hash("HazeLowVis").setString(QObject::tr("Haze with visibility less than 1km"));
    target.hash("Mist").setString(QObject::tr("Mist"));
    target.hash("FogPartial").setString(QObject::tr("Fog observed in the last hour"));
    target.hash("PrecipitationPartial")
          .setString(QObject::tr("Precipitation observed in the last hour"));
    target.hash("DrizzlePartial").setString(QObject::tr("Drizzle observed in the last hour"));
    target.hash("RainPartial").setString(QObject::tr("Rain observed in the last hour"));
    target.hash("SnowPartial").setString(QObject::tr("Snow observed in the last hour"));
    target.hash("FreezingRainPartial")
          .setString(QObject::tr("Freezing rain observed in the last hour"));
    target.hash("Fog").setString(QObject::tr("Fog or ice fog"));
    target.hash("FogPatches").setString(QObject::tr("Fog or ice fog in patches"));
    target.hash("FogThinning").setString(QObject::tr("Fog or ice fog thinning in the last hour"));
    target.hash("FogThickening")
          .setString(QObject::tr("Fog or ice fog thickening in the last hour"));
    target.hash("UnknownPrecipitation").setString(QObject::tr("Light or moderate precipitation"));
    target.hash("HeavyUnknownPrecipitation").setString(QObject::tr("Heavy precipitation"));
    target.hash("Drizzle").setString(QObject::tr("Drizzle"));
    target.hash("DrizzleLight").setString(QObject::tr("Slight drizzle"));
    target.hash("DrizzleHeavy").setString(QObject::tr("Heavy drizzle"));
    target.hash("FreezingDrizzle").setString(QObject::tr("Freezing drizzle"));
    target.hash("FreezingDrizzleLight").setString(QObject::tr("Slight freezing drizzle"));
    target.hash("FreezingDrizzleHeavy").setString(QObject::tr("Heavy freezing drizzle"));
    target.hash("Rain").setString(QObject::tr("Rain"));
    target.hash("RainLight").setString(QObject::tr("Slight rain"));
    target.hash("RainHeavy").setString(QObject::tr("Heavy rain"));
    target.hash("FreezingRain").setString(QObject::tr("Freezing rain"));
    target.hash("FreezingRainLight").setString(QObject::tr("Slight freezing rain"));
    target.hash("FreezingRainHeavy").setString(QObject::tr("Heavy freezing rain"));
    target.hash("RainAndSnowLight").setString(QObject::tr("Light rain or drizzle and snow"));
    target.hash("RainAndSnow").setString(QObject::tr("Rain or drizzle and snow"));
    target.hash("Snow").setString(QObject::tr("Snow"));
    target.hash("SnowLight").setString(QObject::tr("Light snow"));
    target.hash("SnowHeavy").setString(QObject::tr("Heavy snow"));
    target.hash("IcePellets").setString(QObject::tr("Ice pellets"));
    target.hash("IcePelletsLight").setString(QObject::tr("Light ice pellets"));
    target.hash("IcePelletsHeavy").setString(QObject::tr("Heavy ice pellets"));
    target.hash("RainShowers").setString(QObject::tr("Rain showers"));
    target.hash("RainShowersLight").setString(QObject::tr("Light rain showers"));
    target.hash("RainShowersHeavy").setString(QObject::tr("Heavy rain showers"));
    target.hash("RainShowersViolent").setString(QObject::tr("Violent rain showers"));
    target.hash("SnowShowers").setString(QObject::tr("Snow showers"));
    target.hash("SnowShowersLight").setString(QObject::tr("Light snow showers"));
    target.hash("SnowShowersHeavy").setString(QObject::tr("Heavy snow showers"));
}

void AcquireVaisalaPWDx2::loggingWeatherFlagsMeta(Variant::Write &target) const
{
    target.metadataSingleFlag("Rain").hash("Description").setString("Rain");
    target.metadataSingleFlag("Rain").hash("Bits").setInt64(0x00000001);
    target.metadataSingleFlag("Rain")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    target.metadataSingleFlag("Snow").hash("Description").setString("Snow");
    target.metadataSingleFlag("Snow").hash("Bits").setInt64(0x00000002);
    target.metadataSingleFlag("Snow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    target.metadataSingleFlag("Drizzle").hash("Description").setString("Drizzle");
    target.metadataSingleFlag("Drizzle").hash("Bits").setInt64(0x00000004);
    target.metadataSingleFlag("Drizzle")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    target.metadataSingleFlag("IcePellets").hash("Description").setString("Ice pellets");
    target.metadataSingleFlag("IcePellets").hash("Bits").setInt64(0x00000008);
    target.metadataSingleFlag("IcePellets")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    target.metadataSingleFlag("UnknownPrecipitation")
          .hash("Description")
          .setString("Unknown or un-categorized precipitation");
    target.metadataSingleFlag("UnknownPrecipitation").hash("Bits").setInt64(0x00000100);
    target.metadataSingleFlag("UnknownPrecipitation")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    target.metadataSingleFlag("Heavy")
          .hash("Description")
          .setString("Heavy intensity or low visibility");
    target.metadataSingleFlag("Heavy").hash("Bits").setInt64(0x00001000);
    target.metadataSingleFlag("Heavy")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    target.metadataSingleFlag("Light").hash("Description").setString("Slight intensity");
    target.metadataSingleFlag("Light").hash("Bits").setInt64(0x00002000);
    target.metadataSingleFlag("Light")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    target.metadataSingleFlag("Violent").hash("Description").setString("Violent intensity");
    target.metadataSingleFlag("Violent").hash("Bits").setInt64(0x00004000);
    target.metadataSingleFlag("Violent")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    target.metadataSingleFlag("Partial")
          .hash("Description")
          .setString("Partial effect or phenomenon observed only for part of the hour");
    target.metadataSingleFlag("Partial").hash("Bits").setInt64(0x00008000);
    target.metadataSingleFlag("Partial")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    target.metadataSingleFlag("Shower")
          .hash("Description")
          .setString("Effect or phenomenon observed in showers");
    target.metadataSingleFlag("Shower").hash("Bits").setInt64(0x00010000);
    target.metadataSingleFlag("Shower")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    target.metadataSingleFlag("Patches")
          .hash("Description")
          .setString("Effect or phenomenon observed in patches during the last hour");
    target.metadataSingleFlag("Patches").hash("Bits").setInt64(0x00020000);
    target.metadataSingleFlag("Patches")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    target.metadataSingleFlag("Freezing")
          .hash("Description")
          .setString("Precipitation is freezing");
    target.metadataSingleFlag("Freezing").hash("Bits").setInt64(0x00400000);
    target.metadataSingleFlag("Freezing")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    target.metadataSingleFlag("Thinning")
          .hash("Description")
          .setString("Effect has become thinner in the last hour");
    target.metadataSingleFlag("Thinning").hash("Bits").setInt64(0x00800000);
    target.metadataSingleFlag("Thinning")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    target.metadataSingleFlag("Thickening")
          .hash("Description")
          .setString("Effect has become thicker in the last hour");
    target.metadataSingleFlag("Thickening").hash("Bits").setInt64(0x01000000);
    target.metadataSingleFlag("Thickening")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    target.metadataSingleFlag("Mist").hash("Description").setString("Mist");
    target.metadataSingleFlag("Mist").hash("Bits").setInt64(0x02000000);
    target.metadataSingleFlag("Mist")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    target.metadataSingleFlag("Fog").hash("Description").setString("Fog or ice fog");
    target.metadataSingleFlag("Fog").hash("Bits").setInt64(0x04000000);
    target.metadataSingleFlag("Fog")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    target.metadataSingleFlag("Haze")
          .hash("Description")
          .setString("Haze, smoke, or dust suspended in the air");
    target.metadataSingleFlag("Haze").hash("Bits").setInt64(0x08000000);
    target.metadataSingleFlag("Haze")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");
}

Variant::Root AcquireVaisalaPWDx2::describeVisibilitySource() const
{
    switch (selectedVisibilitySource) {
    case Visibility_1Min:
        return Variant::Root("OneMinuteAverage");
    case Visibility_10Min:
        return Variant::Root("TenMinuteAverage");
    }
    Q_ASSERT(false);
    return Variant::Root();
}

Variant::Root AcquireVaisalaPWDx2::describePresentWeatherSource() const
{
    switch (selectedPresentWeatherSource) {
    case Weather_Instant:
        return Variant::Root("Instantaneous");
    case Weather_15Min:
        return Variant::Root("FifteenMinute");
    case Weather_1Hour:
        return Variant::Root("OneHour");
    }
    Q_ASSERT(false);
    return Variant::Root();
}

SequenceValue::Transfer AcquireVaisalaPWDx2::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_vaisala_pwdx2");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    processing["Model"].set(instrumentMeta["Model"]);
    processing["ID"].set(instrumentMeta["ID"]);

    result.emplace_back(SequenceName({}, "raw_meta", "WZ"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.000");
    result.back().write().metadataReal("Units").setString("km");
    result.back().write().metadataReal("Description").setString("Visibility distance");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("MeasurementType").set(describeVisibilitySource());
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Visibility"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "WI"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.00");
    result.back().write().metadataReal("Units").setString("mm/h");
    result.back().write().metadataReal("Description").setString("Precipitation rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Precipitation"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "WX1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Present weather flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);
    result.back().write().metadataFlags("MeasurementType").set(describePresentWeatherSource());
    loggingWeatherFlagsMeta(result.back().write());

    result.emplace_back(SequenceName({}, "raw_meta", "WX2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataInteger("Format").setString("00");
    result.back().write().metadataInteger("Description").setString("Present weather code");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("MeasurementType").set(describePresentWeatherSource());
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Ambient temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Temperature"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Circuit board temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Board"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "T3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("DRD precipitation sensor temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("DRD"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "I"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("cd/m²");
    result.back().write().metadataReal("Description").setString("Background luminance");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Luminance"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "V1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("LED control voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("LED"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "V2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Ambient light receiver voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Ambient Receiver"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "C1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.00");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Frequency of the transmission signal between the transducer and processor");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Signal"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "C2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.00");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Measurement signal offset and the lowest possible frequency measurement");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Offset"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "ZBsp"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Receiver contamination control backscatter signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Receiver Backscatter"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "ZBsx"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Transmitter contamination control backscatter signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Transmitter Backscatter"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("HardwareError")
          .hash("Description")
          .setString("Hardware error condition present");
    result.back().write().metadataSingleFlag("HardwareError").hash("Bits").setInt64(0x00010000);
    result.back()
          .write()
          .metadataSingleFlag("HardwareError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("HardwareWarning")
          .hash("Description")
          .setString("Hardware warning condition present");
    result.back().write().metadataSingleFlag("HardwareWarning").hash("Bits").setInt64(0x00020000);
    result.back()
          .write()
          .metadataSingleFlag("HardwareWarning")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("BackscatterAlarm")
          .hash("Description")
          .setString("Receiver or transmitter contamination signal exceeded alarm threshold");
    result.back().write().metadataSingleFlag("BackscatterAlarm").hash("Bits").setInt64(0x00040000);
    result.back()
          .write()
          .metadataSingleFlag("BackscatterAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("TransmitterError")
          .hash("Description")
          .setString("LED control signal it outside the range of -8V to 7V");
    result.back().write().metadataSingleFlag("TransmitterError").hash("Bits").setInt64(0x00080000);
    result.back()
          .write()
          .metadataSingleFlag("TransmitterError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("PowerError")
          .hash("Description")
          .setString("Receiver or transmitter power supply is outside of 10V to 14V");
    result.back().write().metadataSingleFlag("PowerError").hash("Bits").setInt64(0x00100000);
    result.back()
          .write()
          .metadataSingleFlag("PowerError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("OffsetError")
          .hash("Description")
          .setString("Offset frequency is outside of 80Hz to 170Hz");
    result.back().write().metadataSingleFlag("OffsetError").hash("Bits").setInt64(0x00200000);
    result.back()
          .write()
          .metadataSingleFlag("OffsetError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("SignalError")
          .hash("Description")
          .setString("Signal and offset combine to an invalid value");
    result.back().write().metadataSingleFlag("SignalError").hash("Bits").setInt64(0x00400000);
    result.back()
          .write()
          .metadataSingleFlag("SignalError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("ReceiverError")
          .hash("Description")
          .setString("Receiver backscatter signal too low");
    result.back().write().metadataSingleFlag("ReceiverError").hash("Bits").setInt64(0x00800000);
    result.back()
          .write()
          .metadataSingleFlag("ReceiverError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("DataRAMError")
          .hash("Description")
          .setString("Error in RAM read/write check");
    result.back().write().metadataSingleFlag("DataRAMError").hash("Bits").setInt64(0x01000000);
    result.back()
          .write()
          .metadataSingleFlag("DataRAMError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("EEPROMError")
          .hash("Description")
          .setString("EEPROM checksum error");
    result.back().write().metadataSingleFlag("EEPROMError").hash("Bits").setInt64(0x02000000);
    result.back()
          .write()
          .metadataSingleFlag("EEPROMError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("TemperatureError")
          .hash("Description")
          .setString("Temperature out of range");
    result.back().write().metadataSingleFlag("TemperatureError").hash("Bits").setInt64(0x04000000);
    result.back()
          .write()
          .metadataSingleFlag("TemperatureError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("RainError")
          .hash("Description")
          .setString("Rain sensor too close to zero");
    result.back().write().metadataSingleFlag("RainError").hash("Bits").setInt64(0x08000000);
    result.back()
          .write()
          .metadataSingleFlag("RainError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("LuminanceError")
          .hash("Description")
          .setString("PWL111 luminance signal out of range");
    result.back().write().metadataSingleFlag("LuminanceError").hash("Bits").setInt64(0x10000000);
    result.back()
          .write()
          .metadataSingleFlag("LuminanceError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("BackscatterWarning")
          .hash("Description")
          .setString("Receiver or transmitter contamination signal exceeded warning threshold");
    result.back()
          .write()
          .metadataSingleFlag("BackscatterWarning")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("TransmitterLow")
          .hash("Description")
          .setString("LED control signal less than -6V");
    result.back()
          .write()
          .metadataSingleFlag("TransmitterLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("ReceiverSaturated")
          .hash("Description")
          .setString("Ambient light receiver control voltage less than -9V");
    result.back().write().metadataSingleFlag("ReceiverSaturated").hash("Bits").setInt64(0x20000000);
    result.back()
          .write()
          .metadataSingleFlag("ReceiverSaturated")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("OffsetDrifted")
          .hash("Description")
          .setString("Offset drifted");
    result.back()
          .write()
          .metadataSingleFlag("OffsetDrifted")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("VisiblityNotCalibrated")
          .hash("Description")
          .setString(
                  "Visibility calibration coefficient has not been changed from the factory default");
    result.back()
          .write()
          .metadataSingleFlag("VisiblityNotCalibrated")
          .hash("Bits")
          .setInt64(0x80000000);
    result.back()
          .write()
          .metadataSingleFlag("VisiblityNotCalibrated")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("VisibilityAlarm1")
          .hash("Description")
          .setString("Visibility below first alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("VisibilityAlarm1")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("VisibilityAlarm2")
          .hash("Description")
          .setString("Visibility below second alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("VisibilityAlarm2")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");

    result.back()
          .write()
          .metadataSingleFlag("VisibilityAlarm3")
          .hash("Description")
          .setString("Visibility below third alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("VisibilityAlarm3")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_pwdx2");


    result.emplace_back(SequenceName({}, "raw_meta", "ZPARAMETERS"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("Parameter settings");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataHashChild("System")
          .metadataArray("Description")
          .setString("System parameters");
    result.back()
          .write()
          .metadataHashChild("System")
          .metadataArray("Children")
          .metadataString("Description")
          .setString("Parameter line");
    result.back()
          .write()
          .metadataHashChild("Weather")
          .metadataHash("Description")
          .setString("Weather parameters");
    result.back()
          .write()
          .metadataHashChild("Weather")
          .metadataHashChild("")
          .metadataReal("Description")
          .setString("Parameter value");

    return result;
}

SequenceValue::Transfer AcquireVaisalaPWDx2::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_vaisala_pwdx2");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    processing["Model"].set(instrumentMeta["Model"]);
    processing["ID"].set(instrumentMeta["ID"]);

    result.emplace_back(SequenceName({}, "raw_meta", "C3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.00");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Measurement signal frequency drift");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Drift"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(3);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "C4"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.00");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("DRD rain sensor measurement signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("DRD Signal"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(6);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "ZBspd"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Receiver contamination control backscatter signal change");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Change"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "ZBsxd"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Transmitter contamination control backscatter signal change");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Change"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "V3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Raw supply voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Raw supply"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "V4"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Regulated positive voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Positive supply"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "V5"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Regulated positive voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Negative supply"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "ZWXState"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current weather type enumation");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("MeasurementType").set(describePresentWeatherSource());
    result.back().write().metadataString("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("Name").setString(QObject::tr("Weather"));
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(0);
    realtimeWeatherCodeTranslation(
            result.back().write().metadataString("Realtime").hash("Translation"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZWXNWS"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Instantaneous NWS weather code");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("MeasurementType").setString("Instantaneous");
    result.back().write().metadataString("Smoothing").hash("Mode").setString("None");


    result.emplace_back(SequenceName({}, "raw_meta", "ZWZ1Min"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.000");
    result.back().write().metadataReal("Units").setString("km");
    result.back().write().metadataReal("Description").setString("One minute averaged visibility");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("MeasurementType").setString("OneMinuteAverage");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Visibility 1 Min"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(7);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "ZWZ10Min"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.000");
    result.back().write().metadataReal("Units").setString("km");
    result.back().write().metadataReal("Description").setString("Ten minute averaged visibility");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("MeasurementType").setString("TenMinuteAverage");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Visibility 10 Min"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(8);
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);


    result.emplace_back(SequenceName({}, "raw_meta", "ZWXStateInstant"), Variant::Root(), time,
                        FP::undefined());
    result.back()
          .write()
          .metadataString("Description")
          .setString("Instantaneous weather type enumation");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataString("MeasurementType").setString("Instantaneous");
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Name")
          .setString(QObject::tr("Instantaneous weather"));
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(9);
    result.back().write().metadataString("Realtime").hash("Page").setInt64(1);
    realtimeWeatherCodeTranslation(
            result.back().write().metadataString("Realtime").hash("Translation"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZWXInstant1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instantaneous weather flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);
    result.back().write().metadataFlags("MeasurementType").setString("Instantaneous");
    loggingWeatherFlagsMeta(result.back().write());

    result.emplace_back(SequenceName({}, "raw_meta", "ZWXInstant2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataInteger("Format").setString("00");
    result.back().write().metadataInteger("Description").setString("Instantaneous weather code");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("MeasurementType").setString("Instantaneous");
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");


    result.emplace_back(SequenceName({}, "raw_meta", "ZWXState15Min"), Variant::Root(), time,
                        FP::undefined());
    result.back()
          .write()
          .metadataString("Description")
          .setString("Fifteen minute weather type enumation");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataString("MeasurementType").setString(QObject::tr("FifteenMinute"));
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Name")
          .setString(QObject::tr("Fifteen minute weather"));
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(10);
    result.back().write().metadataString("Realtime").hash("Page").setInt64(1);
    realtimeWeatherCodeTranslation(
            result.back().write().metadataString("Realtime").hash("Translation"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZWX15Min1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Fifteen minute weather flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);
    result.back().write().metadataFlags("MeasurementType").setString("FifteenMinute");
    loggingWeatherFlagsMeta(result.back().write());

    result.emplace_back(SequenceName({}, "raw_meta", "ZWX15Min2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataInteger("Format").setString("00");
    result.back().write().metadataInteger("Description").setString("Fifteen minute weather code");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("MeasurementType").setString("FifteenMinute");
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");


    result.emplace_back(SequenceName({}, "raw_meta", "ZWXState1Hour"), Variant::Root(), time,
                        FP::undefined());
    result.back()
          .write()
          .metadataString("Description")
          .setString("One hour weather type enumation");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataString("MeasurementType").setString(QObject::tr("OneHour"));
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Name")
          .setString(QObject::tr("One hour weather"));
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(11);
    result.back().write().metadataString("Realtime").hash("Page").setInt64(1);
    realtimeWeatherCodeTranslation(
            result.back().write().metadataString("Realtime").hash("Translation"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZWX1Hour1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("One hour weather flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);
    result.back().write().metadataFlags("MeasurementType").setString("OneHour");
    loggingWeatherFlagsMeta(result.back().write());

    result.emplace_back(SequenceName({}, "raw_meta", "ZWX1Hour2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataInteger("Format").setString("00");
    result.back().write().metadataInteger("Description").setString("One hour weather code");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("MeasurementType").setString("OneHour");
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");


    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(99);
    result.back().write().metadataString("Realtime").hash("PageMask").setInt64(0x03);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidRecord")
          .setString(QObject::tr("NO COMMS: Invalid record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveOpenNoID")
          .setString(QObject::tr("STARTING COMMS: Opening no ID line"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveOpenID")
          .setString(QObject::tr("STARTING COMMS: Opening specific ID"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveOpenAnyID")
          .setString(QObject::tr("STARTING COMMS: Opening specific wildcard ID"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetDate")
          .setString(QObject::tr("STARTING COMMS: Setting time"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveDisableUnpolled")
          .setString(QObject::tr("STARTING COMMS: Disabling unpolled records"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadParameters")
          .setString(QObject::tr("STARTING COMMS: Reading system parameters"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadWeatherParameters")
          .setString(QObject::tr("STARTING COMMS: Reading weather parameters"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadStatus")
          .setString(QObject::tr("STARTING COMMS: Reading system status"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveClose")
          .setString(QObject::tr("STARTING COMMS: Closing line"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("HardwareError")
          .setString(QObject::tr("HARDWARE ERROR"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("HardwareWarning")
          .setString(QObject::tr("HARDWARE WARNING"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BackscatterAlarm")
          .setString(QObject::tr("ERROR: Backscatter signal too high"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("TransmitterError")
          .setString(QObject::tr("ERROR: LED control signal out of range"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("PowerError")
          .setString(QObject::tr("ERROR: Power supply voltage out of range"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("OffsetError")
          .setString(QObject::tr("ERROR: Offset frequency out of range"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SignalError")
          .setString(QObject::tr("ERROR: Invalid signal frequency"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("ReceiverError")
          .setString(QObject::tr("ERROR: Receiver signal too low"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("DataRAMError")
          .setString(QObject::tr("ERROR: RAM failed read/write check"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("EEPROMError")
          .setString(QObject::tr("ERROR: EEPROM checksum mismatch"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("TemperatureError")
          .setString(QObject::tr("ERROR: Temperature out of range"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("RainError")
          .setString(QObject::tr("ERROR: Rain sensor signal too low"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("LuminanceError")
          .setString(QObject::tr("ERROR: Luminance sensor signal out of range"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BackscatterWarning")
          .setString(QObject::tr("WARNING: Backscatter signal high"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("TransmitterLow")
          .setString(QObject::tr("WARNING: Transmitter control signal low"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("ReceiverSaturated")
          .setString(QObject::tr("WARNING: Receiver saturated"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("OffsetDrifted")
          .setString(QObject::tr("WARNING: Sample signal offset drifted"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("VisiblityNotCalibrated")
          .setString(QObject::tr("VISIBILITY NOT CALIBRATED"));

    return result;
}

SequenceMatch::Composite AcquireVaisalaPWDx2::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZPARAMETERS");
    sel.append({}, {}, "ZSTATE");
    return sel;
}


void AcquireVaisalaPWDx2::updateMetadata(double frameTime)
{
    if (loggingEgress == NULL) {
        haveEmittedLogMeta = false;
        loggingMux.clear();
        memset(streamAge, 0, sizeof(streamAge));
        for (int i = 0; i < LogStream_TOTAL; i++) {
            streamTime[i] = FP::undefined();
        }
    } else {
        if (!haveEmittedLogMeta) {
            haveEmittedLogMeta = true;
            loggingMux.incoming(LogStream_Metadata, buildLogMeta(frameTime), loggingEgress);
        }
        loggingMux.advance(LogStream_Metadata, frameTime, loggingEgress);
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }
}

void AcquireVaisalaPWDx2::advanceState(double frameTime)
{
    Variant::Flags flags = alarmFlags;
    Util::merge(statusHardwareFlags, flags);

    for (int i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
        if (streamAge[i] < 2)
            continue;
        if (loggingEgress == NULL)
            continue;

        if (FP::defined(streamTime[LogStream_State])) {
            double startTime = streamTime[LogStream_State];
            double endTime = frameTime;

            loggingMux.incoming(LogStream_State,
                                SequenceValue({{}, "raw", "F1"}, Variant::Root(flags), startTime,
                                              endTime), loggingEgress);
        } else {
            loggingMux.advance(LogStream_State, frameTime, loggingEgress);
        }
        streamTime[LogStream_State] = frameTime;

        for (i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
            if (streamAge[i] == 0) {
                loggingMux.advance(i, frameTime, loggingEgress);
                streamTime[i] = FP::undefined();
            }
            streamAge[i] = 0;
        }
        break;
    }

    if (realtimeEgress != NULL) {
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "F1"}, Variant::Root(flags), frameTime, FP::undefined()));
    }

    auto displayFlags = flags;
    if (displayFlags.size() > 1) {
        displayFlags.erase("HardwareWarning");
    }
    if (displayFlags.size() > 1) {
        displayFlags.erase("HardwareError");
    }
    if (priorDisplayFlags != displayFlags) {
        priorDisplayFlags = std::move(displayFlags);
        realtimeStateUpdated = true;
    }

    if (realtimeEgress != NULL && realtimeStateUpdated) {
        realtimeStateUpdated = false;

        if (priorDisplayFlags.empty()) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                  FP::undefined()));
        } else {
            QStringList sorted;
            for (const auto &add : priorDisplayFlags) {
                sorted.push_back(QString::fromStdString(add));
            }
            std::sort(sorted.begin(), sorted.end());
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(sorted.join(",")), frameTime,
                                  FP::undefined()));
        }
    }

    if (!haveEmittedParameters && persistentEgress != NULL && parameterData.read().exists()) {
        haveEmittedParameters = true;
        persistentEgress->emplaceData(SequenceName({}, "raw", "ZPARAMETERS"), parameterData,
                                      frameTime, FP::undefined());
    }
}

/* If we find an 0x01 character then the frame is until at 0x03 character.
 * If we find a non CR/LF character then the frame is until a CR/LF.  That is
 * we frame anything between 0x01 and 0x03 but if we don't find a start of
 * frame then we do conventional line framing. */
std::size_t AcquireVaisalaPWDx2::dataFrameStart(std::size_t offset,
                                                const Util::ByteArray &input) const
{
    for (auto max = input.size(); offset < max; ++offset) {
        auto ch = input[offset];
        if (ch == '\r' || ch == '\n')
            continue;
        if (ch == 0x03)
            continue;
        return offset;
    }
    return input.npos;
}

std::size_t AcquireVaisalaPWDx2::dataFrameEnd(std::size_t start, const Util::ByteArray &input) const
{
    auto max = input.size();
    if (start >= max)
        return input.npos;

    /* Handle a strict frame */
    if (input[start] == 0x01)
        return input.indexOf(0x03, start);

    for (; start < max; start++) {
        auto ch = input[start];
        if (ch == '\r' || ch == '\n')
            return start;
    }
    return input.npos;
}

void AcquireVaisalaPWDx2::issuePollingCommand(int code)
{
    if (controlStream == NULL)
        return;

    Util::ByteArray command;
    command.push_back('\r');
    command.push_back(0x05);
    command += "PW";
    if (pollID.empty()) {
        command += "  1";
    } else {
        command.push_back(' ');
        command += pollID;
    }
    command.push_back(' ');
    command += QByteArray::number(code);
    command.push_back('\r');
    controlStream->writeControl(std::move(command));
}

#define RETURN_IF_ERROR(_code, _offset) do {\
    int _rc = (_code); \
    if (_rc < 0) return _rc; \
    if (_rc > 0) return _rc + (_offset); \
} while (0)

static bool isSpaceDelimiter(char c)
{
    return (c == ' ' || c == '\t' || c == '\r' || c == '\n' || c == '\v');
}

static bool matchStatusLine(const Util::ByteView &data, const char *trigger, Util::ByteView &line)
{
    auto str = data.toString();
    std::size_t max = str.size();
    auto begin = max;
    for (std::size_t offset = 0; offset < max;) {
        auto index = str.find(trigger, offset);
        if (index == str.npos)
            return false;
        if (index != 0 && !isSpaceDelimiter(str[index - 1])) {
            offset = index + 1;
            continue;
        }
        begin = index + strlen(trigger);
        break;
    }
    if (begin >= max)
        return false;

    auto end = begin;
    for (; end < max; end++) {
        char c = str[end];
        if (c == '\r' || c == '\n')
            break;
    }
    if (begin == end)
        return false;
    line = data.mid(begin, end - begin);
    return true;
}

static bool matchStatusField(const Util::ByteView &line, const char *name, Util::ByteView &field)
{
    std::size_t begin;
    if (!name) {
        begin = 0;
    } else {
        begin = line.toString().find(name);
        if (begin == std::string::npos)
            return false;
        begin += strlen(name);
    }
    std::size_t max = line.size();
    for (; begin < max; ++begin) {
        if (!isSpaceDelimiter(line[begin]))
            break;
    }
    if (begin >= max)
        return false;
    std::size_t end = begin + 1;
    for (; end < max; ++end) {
        if (isSpaceDelimiter(line[end]))
            break;
    }
    field = line.mid(begin, end - begin);
    return true;
}

int AcquireVaisalaPWDx2::processStatus(const Util::ByteView &data, double frameTime)
{
    bool anyFound = false;

    Util::ByteView line;
    bool ok = false;

    if (matchStatusLine(data, "VAISALA PWD", line)) {
        std::size_t end = 1;
        for (std::size_t max = line.size(); end < max; end++) {
            if (isSpaceDelimiter(line[end]))
                break;
        }

        std::string model("PWD");
        model += line.mid(0, end).toString();
        if (instrumentMeta["Model"].toString() != model) {
            instrumentMeta["Model"].setString(std::move(model));
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        QString remaining(QString::fromLatin1(line.mid(end).toQByteArrayRef()));
        QRegExp reFirmware("V\\s*(\\S+(?:\\s+\\d{4}-\\d{2}-\\d{2})?)", Qt::CaseInsensitive);
        if (reFirmware.indexIn(remaining) != -1) {
            anyFound = true;

            QString firmwareVersion(reFirmware.cap(1));
            if (instrumentMeta["FirmwareVersion"].toQString() != firmwareVersion) {
                instrumentMeta["FirmwareVersion"].setString(firmwareVersion);
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }
        }

        QRegExp reSerial("SN\\s*:\\s*(\\S+)", Qt::CaseInsensitive);
        if (reSerial.indexIn(remaining) != -1) {
            anyFound = true;

            QString serialNumber(reSerial.cap(1));
            if (instrumentMeta["SerialNumber"].toQString() != serialNumber) {
                instrumentMeta["SerialNumber"].setString(serialNumber);
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }
        }
    }

    if (matchStatusLine(data, "SIGNAL", line)) {
        Util::ByteView fieldValue;
        Util::ByteView fieldOffset;
        Util::ByteView fieldDrift;
        if (matchStatusField(line, nullptr, fieldValue) &&
                matchStatusField(line, "OFFSET", fieldOffset) &&
                matchStatusField(line, "DRIFT", fieldDrift)) {
            anyFound = true;

            Variant::Root C1(fieldValue.parse_real(&ok));
            if (!ok) return 10;
            if (!FP::defined(C1.read().toReal())) return 11;
            remap("C1", C1);

            Variant::Root C2(fieldOffset.parse_real(&ok));
            if (!ok) return 12;
            if (!FP::defined(C2.read().toReal())) return 13;
            remap("C2", C2);

            Variant::Root C3(fieldDrift.parse_real(&ok));
            if (!ok) return 14;
            if (!FP::defined(C3.read().toReal())) return 15;
            remap("C3", C3);

            updateMetadata(frameTime);
            realtimeValue(frameTime, "C3", std::move(C3));

            logValue(frameTime, LogStream_SignalFrequency, "C1", std::move(C1));
            logValue(frameTime, LogStream_SignalOffset, "C2", std::move(C2));

            advanceStream(frameTime, LogStream_SignalFrequency);
            advanceStream(frameTime, LogStream_SignalOffset);
        }
    }

    if (matchStatusLine(data, "REC. BACKSCATTER", line)) {
        Util::ByteView fieldValue;
        Util::ByteView fieldChange;
        if (matchStatusField(line, nullptr, fieldValue) &&
                (matchStatusField(line, "CHANGE*", fieldChange) ||
                        matchStatusField(line, "CHANGE", fieldChange))) {
            anyFound = true;

            Variant::Root ZBsp(fieldValue.parse_real(&ok));
            if (!ok) return 20;
            if (!FP::defined(ZBsp.read().toReal())) return 21;
            remap("ZBsp", ZBsp);

            Variant::Root ZBspd(fieldChange.parse_real(&ok));
            if (!ok) return 22;
            if (!FP::defined(ZBspd.read().toReal())) return 23;
            remap("ZBspd", ZBspd);

            updateMetadata(frameTime);
            logValue(frameTime, LogStream_ReceiverBackscatter, "ZBsp", std::move(ZBsp));
            realtimeValue(frameTime, "ZBspd", std::move(ZBspd));

            advanceStream(frameTime, LogStream_ReceiverBackscatter);
        }
    }

    if (matchStatusLine(data, "TR. BACKSCATTER", line) || matchStatusLine(data, "TR. B", line)) {
        Util::ByteView fieldValue;
        Util::ByteView fieldChange;
        if (matchStatusField(line, nullptr, fieldValue) &&
                (matchStatusField(line, "CHANGE*", fieldChange) ||
                        matchStatusField(line, "CHANGE", fieldChange))) {
            anyFound = true;

            Variant::Root ZBsx(fieldValue.parse_real(&ok));
            if (!ok) return 30;
            if (!FP::defined(ZBsx.read().toReal())) return 31;
            remap("ZBsx", ZBsx);

            Variant::Root ZBsxd(fieldChange.parse_real(&ok));
            if (!ok) return 32;
            if (!FP::defined(ZBsxd.read().toReal())) return 33;
            remap("ZBsxd", ZBsxd);

            updateMetadata(frameTime);
            logValue(frameTime, LogStream_TransmitterBackscatter, "ZBsx", std::move(ZBsx));
            realtimeValue(frameTime, "ZBsxd", std::move(ZBsxd));

            advanceStream(frameTime, LogStream_TransmitterBackscatter);
        }
    }

    if (matchStatusLine(data, "LEDI", line)) {
        Util::ByteView fieldValue;
        if (matchStatusField(line, nullptr, fieldValue)) {
            Variant::Root V1(fieldValue.parse_real(&ok));
            if (!ok) return 40;
            if (!FP::defined(V1.read().toReal())) return 41;
            remap("V1", V1);

            updateMetadata(frameTime);
            logValue(frameTime, LogStream_LEDVoltage, "V1", std::move(V1));
            advanceStream(frameTime, LogStream_LEDVoltage);
        }
    }

    if (matchStatusLine(data, "AMBL", line)) {
        Util::ByteView fieldValue;
        if (matchStatusField(line, nullptr, fieldValue)) {
            Variant::Root V2(fieldValue.parse_real(&ok));
            if (!ok) return 50;
            if (!FP::defined(V2.read().toReal())) return 51;
            remap("V2", V2);

            updateMetadata(frameTime);
            logValue(frameTime, LogStream_AmbientLightVoltage, "V2", std::move(V2));
            advanceStream(frameTime, LogStream_AmbientLightVoltage);
        }
    }

    if (matchStatusLine(data, "VBB", line)) {
        Util::ByteView fieldValue;
        if (matchStatusField(line, nullptr, fieldValue)) {
            Variant::Root V3(fieldValue.parse_real(&ok));
            if (!ok) return 60;
            if (!FP::defined(V3.read().toReal())) return 61;
            remap("V3", V3);

            updateMetadata(frameTime);
            realtimeValue(frameTime, "V3", std::move(V3));
        }
    }

    if (matchStatusLine(data, "P12", line)) {
        Util::ByteView fieldValue;
        if (matchStatusField(line, nullptr, fieldValue)) {
            Variant::Root V4(fieldValue.parse_real(&ok));
            if (!ok) return 70;
            if (!FP::defined(V4.read().toReal())) return 71;
            remap("V4", V4);

            updateMetadata(frameTime);
            realtimeValue(frameTime, "V4", std::move(V4));
        }
    }

    if (matchStatusLine(data, "M12", line)) {
        Util::ByteView fieldValue;
        if (matchStatusField(line, nullptr, fieldValue)) {
            Variant::Root V5(fieldValue.parse_real(&ok));
            if (!ok) return 80;
            if (!FP::defined(V5.read().toReal())) return 81;
            remap("V5", V5);

            updateMetadata(frameTime);
            realtimeValue(frameTime, "V5", std::move(V5));
        }
    }

    if (matchStatusLine(data, "TS", line)) {
        Util::ByteView fieldValue;
        Util::ByteView fieldBoard;
        if (matchStatusField(line, nullptr, fieldValue) &&
                matchStatusField(line, "TB", fieldBoard)) {
            anyFound = true;

            Variant::Root T1(fieldValue.parse_real(&ok));
            if (!ok) return 90;
            if (!FP::defined(T1.read().toReal())) return 91;
            remap("T1", T1);

            Variant::Root T2(fieldBoard.parse_real(&ok));
            if (!ok) return 92;
            if (!FP::defined(T2.read().toReal())) return 93;
            remap("T2", T2);

            updateMetadata(frameTime);

            logValue(frameTime, LogStream_Temperature, "T1", std::move(T1));
            logValue(frameTime, LogStream_BoardTemperature, "T2", std::move(T2));

            advanceStream(frameTime, LogStream_Temperature);
            advanceStream(frameTime, LogStream_BoardTemperature);
        }
    }

    if (matchStatusLine(data, "TDRD", line)) {
        Util::ByteView fieldTemperature;
        Util::ByteView fieldDRD;
        Util::ByteView fieldDry;
        if (matchStatusField(line, nullptr, fieldTemperature) &&
                matchStatusField(line, "DRD", fieldDRD) &&
                matchStatusField(line, "DRY", fieldDry)) {
            anyFound = true;

            Variant::Root T3(fieldTemperature.parse_real(&ok));
            if (!ok) return 100;
            if (!FP::defined(T3.read().toReal())) return 101;
            remap("T3", T3);

            Variant::Root C4(fieldDRD.parse_real(&ok));
            if (!ok) return 102;
            if (!FP::defined(C4.read().toReal())) return 103;
            remap("C4", C4);

            updateMetadata(frameTime);
            logValue(frameTime, LogStream_DRDTemperature, "T3", std::move(T3));
            realtimeValue(frameTime, "C4", std::move(C4));

            advanceStream(frameTime, LogStream_DRDTemperature);
        }
    }

    if (matchStatusLine(data, "BL", line)) {
        Util::ByteView fieldValue;
        if (matchStatusField(line, nullptr, fieldValue)) {
            Variant::Root I(fieldValue.parse_real(&ok));
            if (!ok) return 110;
            if (!FP::defined(I.read().toReal())) return 111;
            remap("I", I);

            logValue(frameTime, LogStream_BackgroundLuminance, "I", std::move(I));
            advanceStream(frameTime, LogStream_BackgroundLuminance);
        }
    }

    /* Clear these out if we get any match, but if we're reading one line
     * at a time, don't clear them out again */
    if (anyFound) {
        statusHardwareFlags.clear();
    }

    auto lc = Util::to_lower(data.toString());
    if (Util::contains(lc, "backscatter high")) {
        statusHardwareFlags.insert("HardwareError");
        statusHardwareFlags.insert("BackscatterAlarm");
    }
    if (Util::contains(lc, "transmitter error")) {
        statusHardwareFlags.insert("TransmitterError");
        statusHardwareFlags.insert("HardwareError");
    }
    if (Util::contains(lc, "power error")) {
        statusHardwareFlags.insert("PowerError");
        statusHardwareFlags.insert("HardwareError");
    }
    if (Util::contains(lc, "offset error")) {
        statusHardwareFlags.insert("OffsetError");
        statusHardwareFlags.insert("HardwareError");
    }
    if (Util::contains(lc, "signal error")) {
        statusHardwareFlags.insert("SignalError");
        statusHardwareFlags.insert("HardwareError");
    }
    if (Util::contains(lc, "receiver error")) {
        statusHardwareFlags.insert("ReceiverError");
        statusHardwareFlags.insert("HardwareError");
    }
    if (Util::contains(lc, "data ram error")) {
        statusHardwareFlags.insert("DataRAMError");
        statusHardwareFlags.insert("HardwareError");
    }
    if (Util::contains(lc, "eeprom error")) {
        statusHardwareFlags.insert("EEPROMError");
        statusHardwareFlags.insert("HardwareError");
    }
    if (Util::contains(lc, "ts sensor error")) {
        statusHardwareFlags.insert("TemperatureError");
        statusHardwareFlags.insert("HardwareError");
    }
    if (Util::contains(lc, "drd error")) {
        statusHardwareFlags.insert("RainError");
        statusHardwareFlags.insert("HardwareError");
    }
    if (Util::contains(lc, "luminance sensor error")) {
        statusHardwareFlags.insert("LuminanceError");
        statusHardwareFlags.insert("HardwareError");
    }
    if (Util::contains(lc, "backscatter increased")) {
        statusHardwareFlags.insert("BackscatterWarning");
        statusHardwareFlags.insert("HardwareWarning");
    }
    if (Util::contains(lc, "transmitter intensity low")) {
        statusHardwareFlags.insert("TransmitterLow");
        statusHardwareFlags.insert("HardwareWarning");
    }
    if (Util::contains(lc, "receiver saturated")) {
        statusHardwareFlags.insert("ReceiverSaturated");
        statusHardwareFlags.insert("HardwareWarning");
    }
    if (Util::contains(lc, "offset drifted")) {
        statusHardwareFlags.insert("OffsetDrifted");
        statusHardwareFlags.insert("HardwareWarning");
    }
    if (Util::contains(lc, "visibility not calibrated")) {
        statusHardwareFlags.insert("VisiblityNotCalibrated");
        statusHardwareFlags.insert("HardwareWarning");
    }

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_RESPONSE_2:
        return 999;

    case RESP_INTERACTIVE_RUN_RESPONSE_3:
    case RESP_INTERACTIVE_RUN_WAIT:
        if (config.first().pollInterval <= 0.0) {
            issuePollingCommand(2);
            responseState = RESP_INTERACTIVE_RUN_RESPONSE_2;
            timeoutAt(frameTime + 5.0);
        } else {
            responseState = RESP_INTERACTIVE_RUN_WAIT;
            timeoutAt(frameTime + config.first().pollInterval);
        }
        break;

    default:
        break;
    }

    advanceState(frameTime);

    if (!anyFound)
        return -1;
    return 0;
}

void AcquireVaisalaPWDx2::reselectVisibilitySource()
{
    switch (config.first().visibilitySource) {
    case Configuration::VisibilitySource_Auto:
        break;
    case Configuration::VisibilitySource_1Min:
        if (selectedVisibilitySource != Visibility_1Min) {
            selectedVisibilitySource = Visibility_1Min;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
        return;
    case Configuration::VisibilitySource_10Min:
        if (selectedVisibilitySource != Visibility_10Min) {
            selectedVisibilitySource = Visibility_10Min;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
        return;
    }

    if (!FP::defined(lastReportInterval))
        return;

    if (lastReportInterval <= 5.0 * 60.0) {
        if (selectedVisibilitySource != Visibility_1Min) {
            selectedVisibilitySource = Visibility_1Min;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
    } else {
        if (selectedVisibilitySource != Visibility_10Min) {
            selectedVisibilitySource = Visibility_10Min;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
    }
}

void AcquireVaisalaPWDx2::reselectPresentWeatherSource()
{
    switch (config.first().presentWeatherSource) {
    case Configuration::PresentWeatherSource_Auto:
        break;
    case Configuration::PresentWeatherSource_Instant:
        if (selectedPresentWeatherSource != Weather_Instant) {
            selectedPresentWeatherSource = Weather_Instant;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
        return;
    case Configuration::PresentWeatherSource_15Min:
        if (selectedPresentWeatherSource != Weather_15Min) {
            selectedPresentWeatherSource = Weather_15Min;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
        return;
    case Configuration::PresentWeatherSource_1Hour:
        if (selectedPresentWeatherSource != Weather_1Hour) {
            selectedPresentWeatherSource = Weather_1Hour;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
        return;
    }

    if (!FP::defined(lastReportInterval))
        return;

    if (lastReportInterval <= 7.5 * 60) {
        if (selectedPresentWeatherSource != Weather_Instant) {
            selectedPresentWeatherSource = Weather_Instant;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
    } else if (lastReportInterval <= 30.0 * 60.0) {
        if (selectedPresentWeatherSource != Weather_15Min) {
            selectedPresentWeatherSource = Weather_15Min;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
    } else {
        if (selectedPresentWeatherSource != Weather_1Hour) {
            selectedPresentWeatherSource = Weather_1Hour;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
    }
}

void AcquireVaisalaPWDx2::updateWX2(Variant::Root &&WX2, double frameTime)
{
    if (realtimeEgress) {
        realtimeEgress->emplaceData(SequenceName({}, "raw", "WX2"), WX2, frameTime,
                                    frameTime + 1.0);
    }

    if (WX2.read() == currentWX2.read())
        return;

    if (persistentEgress && FP::defined(currentWX2.getStart())) {
        currentWX2.setEnd(frameTime);
        persistentEgress->incomingData(currentWX2);
    }

    currentWX2.root() = std::move(WX2);
    currentWX2.setStart(frameTime);
    currentWX2.setEnd(FP::undefined());

    persistentValuesUpdated();
}

int AcquireVaisalaPWDx2::processAlarmField(const Util::ByteView &field)
{
    if (field.size() < 2) return 1;
    if (config.first().strictMode && field.size() != 2) return 2;

    alarmFlags.clear();

    switch (field[0]) {
    case '0':
        break;
    case '1':
        alarmFlags.insert("VisibilityAlarm1");
        break;
    case '2':
        alarmFlags.insert("VisibilityAlarm2");
        break;
    case '3':
        alarmFlags.insert("VisibilityAlarm3");
        break;
    default:
        return 4;
    }

    switch (field[1]) {
    case '0':
        break;
    case '1':
        alarmFlags.insert("HardwareError");
        break;
    case '2':
        alarmFlags.insert("HardwareWarning");
        break;
    case '3':
        alarmFlags.insert("BackscatterAlarm");
        break;
    case '4':
        alarmFlags.insert("BackscatterWarning");
        break;
    default:
        return 3;
    }

    return 0;
}

static bool isMissing(const Util::ByteView &field)
{
    if (field.empty())
        return false;
    const char *begin = field.data<const char *>();
    if (*begin == ' ')
        ++begin;
    for (const char *end = field.data<const char *>() + field.size(); begin != end; ++begin) {
        if (*begin != '/')
            return false;
    }
    return true;
}

int AcquireVaisalaPWDx2::processSYNOPCode(const Util::ByteView &field,
                                          Variant::Root &WX1,
                                          Variant::Root &WX2,
                                          Variant::Root &ZWXState)
{
    Variant::Flags output;

    if (isMissing(field))
        return 0;

    if (config.first().strictMode && (field.size() < 1 || field.size() > 2))
        return 1;

    bool ok = false;
    int code = field.parse_i32(&ok);
    if (!ok) return 2;
    switch (code) {
    default:
        return 3;

    case 0:
        ZWXState.write().setString("Clear");
        break;

    case 4:
        ZWXState.write().setString("HazeHighVis");
        output.insert("Haze");
        break;
    case 5:
        ZWXState.write().setString("HazeLowVis");
        output.insert("Haze");
        output.insert("Heavy");
        break;
    case 10:
        ZWXState.write().setString("Mist");
        output.insert("Mist");
        break;

    case 20:
        ZWXState.write().setString("FogPartial");
        output.insert("Fog");
        output.insert("Partial");
        break;
    case 21:
        ZWXState.write().setString("PrecipitationPartial");
        output.insert("UnknownPrecipitation");
        output.insert("Partial");
        break;
    case 22:
        ZWXState.write().setString("DrizzlePartial");
        output.insert("Drizzle");
        output.insert("Partial");
        break;
    case 23:
        ZWXState.write().setString("RainPartial");
        output.insert("Rain");
        output.insert("Partial");
        break;
    case 24:
        ZWXState.write().setString("SnowPartial");
        output.insert("Snow");
        output.insert("Partial");
        break;
    case 25:
        ZWXState.write().setString("FreezingRainPartial");
        output.insert("Rain");
        output.insert("Freezing");
        output.insert("Partial");
        break;

    case 30:
    case 33:
        ZWXState.write().setString("Fog");
        output.insert("Fog");
        break;
    case 31:
        ZWXState.write().setString("FogPatches");
        output.insert("Fog");
        output.insert("Patches");
        break;
    case 32:
        ZWXState.write().setString("FogThinning");
        output.insert("Fog");
        output.insert("Thinning");
        break;
    case 34:
        ZWXState.write().setString("FogThickening");
        output.insert("Fog");
        output.insert("Thickening");
        break;

    case 40:
    case 41:
        ZWXState.write().setString("UnknownPrecipitation");
        output.insert("UnknownPrecipitation");
        break;
    case 42:
        ZWXState.write().setString("HeavyPrecipitation");
        output.insert("UnknownPrecipitation");
        output.insert("Heavy");
        break;

    case 50:
    case 52:
        ZWXState.write().setString("Drizzle");
        output.insert("Drizzle");
        break;
    case 51:
        ZWXState.write().setString("DrizzleLight");
        output.insert("Drizzle");
        output.insert("Light");
        break;
    case 53:
        ZWXState.write().setString("DrizzleHeavy");
        output.insert("Drizzle");
        output.insert("Heavy");
        break;
    case 54:
        ZWXState.write().setString("FreezingDrizzleLight");
        output.insert("Drizzle");
        output.insert("Freezing");
        output.insert("Light");
        break;
    case 55:
        ZWXState.write().setString("FreezingDrizzle");
        output.insert("Drizzle");
        output.insert("Freezing");
        break;
    case 56:
        ZWXState.write().setString("FreezingDrizzleHeavy");
        output.insert("Drizzle");
        output.insert("Freezing");
        output.insert("Heavy");
        break;

    case 60:
    case 62:
        ZWXState.write().setString("Rain");
        output.insert("Rain");
        break;
    case 61:
        ZWXState.write().setString("RainLight");
        output.insert("Rain");
        output.insert("Light");
        break;
    case 63:
        ZWXState.write().setString("RainHeavy");
        output.insert("Rain");
        output.insert("Heavy");
        break;
    case 64:
        ZWXState.write().setString("FreezingRainLight");
        output.insert("Rain");
        output.insert("Freezing");
        output.insert("Light");
        break;
    case 65:
        ZWXState.write().setString("FreezingRain");
        output.insert("Rain");
        output.insert("Freezing");
        break;
    case 66:
        ZWXState.write().setString("FreezingRainHeavy");
        output.insert("Rain");
        output.insert("Freezing");
        output.insert("Heavy");
        break;
    case 67:
        ZWXState.write().setString("RainAndSnowLight");
        output.insert("Rain");
        output.insert("Snow");
        output.insert("Light");
        break;
    case 68:
        ZWXState.write().setString("RainAndSnow");
        output.insert("Rain");
        output.insert("Snow");
        break;

    case 70:
    case 72:
        ZWXState.write().setString("Snow");
        output.insert("Snow");
        break;
    case 71:
        ZWXState.write().setString("SnowLight");
        output.insert("Snow");
        output.insert("Light");
        break;
    case 73:
        ZWXState.write().setString("SnowHeavy");
        output.insert("Snow");
        output.insert("Heavy");
        break;
    case 74:
        ZWXState.write().setString("IcePelletsLight");
        output.insert("IcePellets");
        output.insert("Light");
        break;
    case 75:
        ZWXState.write().setString("IcePellets");
        output.insert("IcePellets");
        break;
    case 76:
        ZWXState.write().setString("IcePelletsHeavy");
        output.insert("IcePellets");
        output.insert("Heavy");
        break;

    case 80:
    case 82:
        ZWXState.write().setString("RainShowers");
        output.insert("Rain");
        output.insert("Shower");
        break;
    case 81:
        ZWXState.write().setString("RainShowersLight");
        output.insert("Rain");
        output.insert("Shower");
        output.insert("Light");
        break;
    case 83:
        ZWXState.write().setString("RainShowersHeavy");
        output.insert("Rain");
        output.insert("Shower");
        output.insert("Heavy");
        break;
    case 84:
        ZWXState.write().setString("RainShowersViolent");
        output.insert("Rain");
        output.insert("Shower");
        output.insert("Heavy");
        output.insert("Violent");
        break;
    case 85:
        ZWXState.write().setString("SnowShowersLight");
        output.insert("Snow");
        output.insert("Shower");
        output.insert("Light");
        break;
    case 86:
        ZWXState.write().setString("SnowShowers");
        output.insert("Snow");
        output.insert("Shower");
        break;
    case 87:
        ZWXState.write().setString("SnowShowersHeavy");
        output.insert("Snow");
        output.insert("Shower");
        output.insert("Heavy");
        break;
    }

    WX1.write().setFlags(std::move(output));
    WX2.write().setInt64(code);

    return 0;
}

static double convertVisiblity(double d)
{
    if (!FP::defined(d))
        return FP::undefined();
    if (d < 0.0)
        return FP::undefined();
    return d * 1E-3;
}

int AcquireVaisalaPWDx2::processVisiblity(std::deque<Util::ByteView> &fields,
                                          Variant::Root &ZWZ1Min,
                                          Variant::Root &ZWZ10Min,
                                          Variant::Root &WZ)
{
    reselectVisibilitySource();

    Util::ByteView field;
    bool ok = false;

    if (fields.empty()) return 2;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!isMissing(field)) {
        ZWZ1Min.write().setDouble(convertVisiblity(field.parse_real(&ok)));
        if (!ok) return 3;
        if (!FP::defined(ZWZ1Min.read().toReal())) return 4;
    }
    remap("ZWZ1Min", ZWZ1Min);

    if (fields.empty()) return 5;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!isMissing(field)) {
        ZWZ10Min.write().setDouble(convertVisiblity(field.parse_real(&ok)));
        if (!ok) return 6;
        if (!FP::defined(ZWZ10Min.read().toReal())) return 7;
    }
    remap("ZWZ10Min", ZWZ10Min);

    switch (selectedVisibilitySource) {
    case Visibility_1Min:
        WZ.write().set(ZWZ1Min);
        break;
    case Visibility_10Min:
        WZ.write().set(ZWZ10Min);
        break;
    }
    remap("WZ", WZ);

    return 0;
}

int AcquireVaisalaPWDx2::processResponse0(std::deque<Util::ByteView> &fields, double frameTime)
{
    if (fields.empty()) return 1;
    RETURN_IF_ERROR(processAlarmField(fields.front()), 100);
    fields.pop_front();

    Variant::Root ZWZ1Min;
    Variant::Root ZWZ10Min;
    Variant::Root WZ;
    RETURN_IF_ERROR(processVisiblity(fields, ZWZ1Min, ZWZ10Min, WZ), 200);

    if (config.first().strictMode) {
        if (!fields.empty())
            return 99;
    }


    updateMetadata(frameTime);

    logValue(frameTime, LogStream_Visibility, "WZ", std::move(WZ));
    realtimeValue(frameTime, "ZWZ1Min", std::move(ZWZ1Min));
    realtimeValue(frameTime, "ZWZ10Min", std::move(ZWZ10Min));

    advanceStream(frameTime, LogStream_Visibility);
    advanceState(frameTime);
    return 0;
}

int AcquireVaisalaPWDx2::processResponse1(std::deque<Util::ByteView> &fields, double frameTime)
{
    if (fields.empty()) return 1;
    RETURN_IF_ERROR(processAlarmField(fields.front()), 100);
    fields.pop_front();

    if (selectedVisibilitySource != Visibility_1Min) {
        selectedVisibilitySource = Visibility_1Min;
        haveEmittedLogMeta = false;
        haveEmittedRealtimeMeta = false;
    }
    if (selectedPresentWeatherSource != Weather_Instant) {
        selectedPresentWeatherSource = Weather_Instant;
        haveEmittedLogMeta = false;
        haveEmittedRealtimeMeta = false;
    }

    Util::ByteView field;
    bool ok = false;

    if (fields.empty()) return 2;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZWZ1Min;
    if (!isMissing(field)) {
        ZWZ1Min.write().setDouble(convertVisiblity(field.parse_real(&ok)));
        if (!ok) return 3;
        if (!FP::defined(ZWZ1Min.read().toReal())) return 4;
    }
    remap("ZWZ1Min", ZWZ1Min);
    Variant::Root WZ = ZWZ1Min;
    remap("WZ", WZ);

    Variant::Root ZWXInstant1;
    Variant::Root ZWXInstant2;
    Variant::Root ZWXStateInstant;
    RETURN_IF_ERROR(processSYNOPCode(fields.front().string_trimmed(), ZWXInstant1, ZWXInstant2,
                                     ZWXStateInstant), 200);
    fields.pop_front();
    remap("ZWXInstant1", ZWXInstant1);
    remap("ZWXInstant2", ZWXInstant2);
    remap("ZWXStateInstant", ZWXStateInstant);

    Variant::Root WX1 = ZWXInstant1;
    remap("WX1", WX1);

    Variant::Root WX2 = ZWXInstant2;
    remap("WX2", WX2);

    Variant::Root ZWXState = ZWXStateInstant;
    remap("ZWXState", ZWXState);


    if (fields.empty()) return 5;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root WI;
    if (!isMissing(field)) {
        WI.write().setDouble(field.parse_real(&ok));
        if (!ok) return 6;
        if (!FP::defined(WI.read().toReal())) return 7;
    }
    remap("WI", WI);

    if (config.first().strictMode) {
        if (!fields.empty())
            return 99;
    }


    updateMetadata(frameTime);

    logValue(frameTime, LogStream_Visibility, "WZ", std::move(WZ));
    realtimeValue(frameTime, "ZWZ1Min", std::move(ZWZ1Min));

    logValue(frameTime, LogStream_PresentWeather, "WX1", std::move(WX1));
    realtimeValue(frameTime, "ZWXState", std::move(ZWXState));
    realtimeValue(frameTime, "ZWXInstant1", std::move(ZWXInstant1));
    realtimeValue(frameTime, "ZWXInstant2", std::move(ZWXInstant2));
    realtimeValue(frameTime, "ZWXStateInstant", std::move(ZWXStateInstant));

    logValue(frameTime, LogStream_PrecipitationRate, "WI", std::move(WI));

    advanceStream(frameTime, LogStream_Visibility);
    advanceStream(frameTime, LogStream_PresentWeather);
    advanceStream(frameTime, LogStream_PrecipitationRate);
    advanceState(frameTime);

    updateWX2(std::move(WX2), frameTime);
    return 0;
}

int AcquireVaisalaPWDx2::processResponse2(std::deque<Util::ByteView> &fields, double frameTime)
{
    if (fields.empty()) return 1;
    RETURN_IF_ERROR(processAlarmField(fields.front()), 100);
    fields.pop_front();

    reselectPresentWeatherSource();

    Util::ByteView field;
    bool ok = false;

    Variant::Root ZWZ1Min;
    Variant::Root ZWZ10Min;
    Variant::Root WZ;
    RETURN_IF_ERROR(processVisiblity(fields, ZWZ1Min, ZWZ10Min, WZ), 200);

    if (fields.empty()) return 8;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZWXNWS;
    if (!isMissing(field))
        ZWXNWS.write().setString(field.toString());
    remap("ZWXNWS", ZWXNWS);


    Variant::Root ZWXInstant1;
    Variant::Root ZWXInstant2;
    Variant::Root ZWXStateInstant;
    RETURN_IF_ERROR(processSYNOPCode(fields.front().string_trimmed(), ZWXInstant1, ZWXInstant2,
                                     ZWXStateInstant), 300);
    fields.pop_front();
    remap("ZWXInstant1", ZWXInstant1);
    remap("ZWXInstant2", ZWXInstant2);
    remap("ZWXStateInstant", ZWXStateInstant);

    Variant::Root ZWX15Min1;
    Variant::Root ZWX15Min2;
    Variant::Root ZWXState15Min;
    RETURN_IF_ERROR(
            processSYNOPCode(fields.front().string_trimmed(), ZWX15Min1, ZWX15Min2, ZWXState15Min),
            400);
    fields.pop_front();
    remap("ZWX15Min1", ZWX15Min1);
    remap("ZWX15Min2", ZWX15Min2);
    remap("ZWXState15Min", ZWXState15Min);

    Variant::Root ZWX1Hour1;
    Variant::Root ZWX1Hour2;
    Variant::Root ZWXState1Hour;
    RETURN_IF_ERROR(
            processSYNOPCode(fields.front().string_trimmed(), ZWX1Hour1, ZWX1Hour2, ZWXState1Hour),
            500);
    fields.pop_front();
    remap("ZWX1Hour1", ZWX1Hour1);
    remap("ZWX1Hour2", ZWX1Hour2);
    remap("ZWXState1Hour", ZWXState1Hour);

    Variant::Root WX1;
    Variant::Root WX2;
    Variant::Root ZWXState;
    switch (selectedPresentWeatherSource) {
    case Weather_Instant:
        WX1 = ZWXInstant1;
        WX2 = ZWXInstant2;
        ZWXState = ZWXStateInstant;
        break;
    case Weather_15Min:
        WX1 = ZWX15Min1;
        WX2 = ZWX15Min2;
        ZWXState = ZWXState15Min;
        break;
    case Weather_1Hour:
        WX1 = ZWX1Hour1;
        WX2 = ZWX1Hour2;
        ZWXState = ZWXState1Hour;
        break;
    }
    remap("WX1", WX1);
    remap("WX2", WX2);
    remap("ZWXState", ZWXState);


    if (fields.empty()) return 9;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root WI;
    if (!isMissing(field)) {
        WI.write().setReal(field.parse_real(&ok));
        if (!ok) return 10;
        if (!FP::defined(WI.read().toReal())) return 11;
    }
    remap("WI", WI);

    /* Cumulative water, ignored */
    if (fields.empty()) return 12;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode && !isMissing(field)) {
        double v = field.parse_real(&ok);
        if (!ok) return 13;
        if (!FP::defined(v)) return 14;
    }

    /* Cumulative snow, ignored */
    if (fields.empty()) return 15;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode && !isMissing(field)) {
        double v = field.parse_real(&ok);
        if (!ok) return 16;
        if (!FP::defined(v)) return 17;
    }

    if (config.first().strictMode) {
        if (!fields.empty())
            return 99;
    }


    switch (responseState) {
    case RESP_INTERACTIVE_RUN_RESPONSE_3:
        return 999;

    case RESP_INTERACTIVE_RUN_RESPONSE_2:
    case RESP_INTERACTIVE_RUN_WAIT:
        issuePollingCommand(3);
        responseState = RESP_INTERACTIVE_RUN_RESPONSE_3;
        timeoutAt(frameTime + 5.0);
        break;

    default:
        break;
    }

    updateMetadata(frameTime);

    logValue(frameTime, LogStream_Visibility, "WZ", std::move(WZ));
    realtimeValue(frameTime, "ZWZ1Min", std::move(ZWZ1Min));
    realtimeValue(frameTime, "ZWZ10Min", std::move(ZWZ10Min));

    logValue(frameTime, LogStream_PresentWeather, "WX1", std::move(WX1));
    realtimeValue(frameTime, "ZWXState", std::move(ZWXState));
    realtimeValue(frameTime, "ZWXNWS", std::move(ZWXNWS));
    realtimeValue(frameTime, "ZWXInstant1", std::move(ZWXInstant1));
    realtimeValue(frameTime, "ZWXInstant2", std::move(ZWXInstant2));
    realtimeValue(frameTime, "ZWXStateInstant", std::move(ZWXStateInstant));
    realtimeValue(frameTime, "ZWX15Min1", std::move(ZWX15Min1));
    realtimeValue(frameTime, "ZWX15Min2", std::move(ZWX15Min2));
    realtimeValue(frameTime, "ZWXState15Min", std::move(ZWXState15Min));
    realtimeValue(frameTime, "ZWX1Hour1", std::move(ZWX1Hour1));
    realtimeValue(frameTime, "ZWX1Hour2", std::move(ZWX1Hour2));
    realtimeValue(frameTime, "ZWXState1Hour", std::move(ZWXState1Hour));

    logValue(frameTime, LogStream_PrecipitationRate, "WI", std::move(WI));

    advanceStream(frameTime, LogStream_Visibility);
    advanceStream(frameTime, LogStream_PresentWeather);
    advanceStream(frameTime, LogStream_PrecipitationRate);
    advanceState(frameTime);

    updateWX2(std::move(WX2), frameTime);
    return 0;
}

int AcquireVaisalaPWDx2::processResponse7(std::deque<Util::ByteView> &fields,
                                          double frameTime,
                                          bool requireMETARCodes)
{
    if (fields.empty()) return 1;
    RETURN_IF_ERROR(processAlarmField(fields.front()), 100);
    fields.pop_front();

    reselectPresentWeatherSource();

    Util::ByteView field;
    bool ok = false;

    Variant::Root ZWZ1Min;
    Variant::Root ZWZ10Min;
    Variant::Root WZ;
    RETURN_IF_ERROR(processVisiblity(fields, ZWZ1Min, ZWZ10Min, WZ), 200);

    Variant::Root ZWXNWS;
    if (fields.empty()) return 8;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (!isMissing(field)) {
        ZWXNWS.write().setString(field.toString());
    }
    remap("ZWXNWS", ZWXNWS);


    Variant::Root ZWXInstant1;
    Variant::Root ZWXInstant2;
    Variant::Root ZWXStateInstant;
    RETURN_IF_ERROR(processSYNOPCode(fields.front().string_trimmed(), ZWXInstant1, ZWXInstant2,
                                     ZWXStateInstant), 300);
    fields.pop_front();
    remap("ZWXInstant1", ZWXInstant1);
    remap("ZWXInstant2", ZWXInstant2);
    remap("ZWXStateInstant", ZWXStateInstant);

    Variant::Root ZWX15Min1;
    Variant::Root ZWX15Min2;
    Variant::Root ZWXState15Min;
    RETURN_IF_ERROR(
            processSYNOPCode(fields.front().string_trimmed(), ZWX15Min1, ZWX15Min2, ZWXState15Min),
            400);
    fields.pop_front();
    remap("ZWX15Min1", ZWX15Min1);
    remap("ZWX15Min2", ZWX15Min2);
    remap("ZWXState15Min", ZWXState15Min);

    Variant::Root ZWX1Hour1;
    Variant::Root ZWX1Hour2;
    Variant::Root ZWXState1Hour;
    RETURN_IF_ERROR(
            processSYNOPCode(fields.front().string_trimmed(), ZWX1Hour1, ZWX1Hour2, ZWXState1Hour),
            500);
    fields.pop_front();
    remap("ZWX1Hour1", ZWX1Hour1);
    remap("ZWX1Hour2", ZWX1Hour2);
    remap("ZWXState1Hour", ZWXState1Hour);

    Variant::Root WX1;
    Variant::Root WX2;
    Variant::Root ZWXState;
    switch (selectedPresentWeatherSource) {
    case Weather_Instant:
        WX1 = ZWXInstant1;
        WX2 = ZWXInstant2;
        ZWXState = ZWXStateInstant;
        break;
    case Weather_15Min:
        WX1 = ZWX15Min1;
        WX2 = ZWX15Min2;
        ZWXState = ZWXState15Min;
        break;
    case Weather_1Hour:
        WX1 = ZWX1Hour1;
        WX2 = ZWX1Hour2;
        ZWXState = ZWXState1Hour;
        break;
    }
    remap("WX1", WX1);
    remap("WX2", WX2);
    remap("ZWXState", ZWXState);


    if (fields.empty()) return 9;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root WI;
    if (!isMissing(field)) {
        WI.write().setReal(field.parse_real(&ok));
        if (!ok) return 10;
        if (!FP::defined(WI.read().toReal())) return 11;
    }
    remap("WI", WI);

    /* Cumulative water, ignored */
    if (fields.empty()) return 12;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode) {
        double v = field.parse_real(&ok);
        if (!ok) return 13;
        if (!FP::defined(v)) return 14;
    }

    /* Cumulative snow, ignored */
    if (fields.empty()) return 15;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode) {
        double v = field.parse_real(&ok);
        if (!ok) return 16;
        if (!FP::defined(v)) return 17;
    }

    if (fields.empty()) return 18;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T1;
    if (!isMissing(field)) {
        T1.write().setReal(field.parse_real(&ok));
        if (!ok) return 19;
        if (!FP::defined(T1.read().toReal())) return 20;
    }
    remap("T1", T1);

    if (fields.empty()) return 21;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root I;
    if (!isMissing(field) && field != "0") {
        I.write().setReal(field.parse_real(&ok));
        if (!ok) return 22;
        if (!FP::defined(I.read().toReal())) return 23;
    }
    remap("I", I);

    /* Don't do anything with the METAR codes */
    if (requireMETARCodes || field.size() >= 1) {
        if (fields.empty()) return 24;
        fields.pop_front();
    }
    if (requireMETARCodes || field.size() >= 1) {
        if (fields.empty()) return 25;
        fields.pop_front();
    }

    if (config.first().strictMode) {
        if (!fields.empty())
            return 99;
    }


    updateMetadata(frameTime);

    logValue(frameTime, LogStream_Visibility, "WZ", std::move(WZ));
    realtimeValue(frameTime, "ZWZ1Min", std::move(ZWZ1Min));
    realtimeValue(frameTime, "ZWZ10Min", std::move(ZWZ10Min));

    logValue(frameTime, LogStream_PresentWeather, "WX1", std::move(WX1));
    realtimeValue(frameTime, "ZWXState", std::move(ZWXState));
    realtimeValue(frameTime, "ZWXNWS", std::move(ZWXNWS));
    realtimeValue(frameTime, "ZWXInstant1", std::move(ZWXInstant1));
    realtimeValue(frameTime, "ZWXInstant2", std::move(ZWXInstant2));
    realtimeValue(frameTime, "ZWXStateInstant", std::move(ZWXStateInstant));
    realtimeValue(frameTime, "ZWX15Min1", std::move(ZWX15Min1));
    realtimeValue(frameTime, "ZWX15Min2", std::move(ZWX15Min2));
    realtimeValue(frameTime, "ZWXState15Min", std::move(ZWXState15Min));
    realtimeValue(frameTime, "ZWX1Hour1", std::move(ZWX1Hour1));
    realtimeValue(frameTime, "ZWX1Hour2", std::move(ZWX1Hour2));
    realtimeValue(frameTime, "ZWXState1Hour", std::move(ZWXState1Hour));

    logValue(frameTime, LogStream_PrecipitationRate, "WI", std::move(WI));

    logValue(frameTime, LogStream_Temperature, "T1", std::move(T1));
    logValue(frameTime, LogStream_BackgroundLuminance, "I", std::move(I));

    advanceStream(frameTime, LogStream_Visibility);
    advanceStream(frameTime, LogStream_PresentWeather);
    advanceStream(frameTime, LogStream_PrecipitationRate);
    advanceStream(frameTime, LogStream_Temperature);
    advanceStream(frameTime, LogStream_BackgroundLuminance);
    advanceState(frameTime);

    updateWX2(std::move(WX2), frameTime);
    return 0;
}

static int offsetErrorCode(int code, int offset)
{
    if (code > 0)
        return code + offset;
    return code;
}

int AcquireVaisalaPWDx2::processRecord(Util::ByteView line, double frameTime)
{
    Q_ASSERT(!config.isEmpty());

    if (line.size() < 3)
        return 1;
    bool expectFrameHeader = false;
    if (line.front() == 0x01) {
        if (line.size() < 8)
            return 2;
        line = line.mid(1);
        if (!line.string_start("PW ") && !line.string_start("FD "))
            return 3;
        expectFrameHeader = true;
    }
    if (expectFrameHeader || line.string_start("PW ") || line.string_start("FD ")) {
        line = line.mid(3);
        if (line.size() < 3)
            return 4;
        auto id = line.mid(0, 2);
        if (!config.first().id.empty()) {
            if (id != config.first().id)
                return -1;
        } else {
            pollID = id;
        }
        if (id != " 1" && !id.string_equal(instrumentMeta["ID"].toString())) {
            instrumentMeta["ID"].setString(id.toString());
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }
        line = line.mid(2);
    }
    if (line.size() < 2)
        return 5;
    if (expectFrameHeader) {
        if (line.front() != 0x02)
            return 6;
        line = line.mid(1);
    } else if (line.front() == 0x02) {
        line = line.mid(1);
    }
    while (line.string_end("\n") || line.string_end("\r")) {
        line = line.mid(0, line.size() - 1);
    }
    if (line.size() < 2)
        return 6;

    if (responseState == RESP_INTERACTIVE_RUN_RESPONSE_3 ||
            line.string_start("PWD STATUS") ||
            line.string_start("VAISALA PWD"))
        return offsetErrorCode(processStatus(line, frameTime), 1000);

    std::deque<Util::ByteView> fields;
    switch (line.size()) {
    case 15:
        fields.emplace_back(line.mid(0, 2));
        fields.emplace_back(line.mid(2, 7));
        fields.emplace_back(line.mid(9));
        break;
    case 18:
        fields.emplace_back(line.mid(0, 2));
        fields.emplace_back(line.mid(2, 7));
        fields.emplace_back(line.mid(9, 3));
        fields.emplace_back(line.mid(12));
        break;
    case 46:
        fields.emplace_back(line.mid(0, 2));
        fields.emplace_back(line.mid(2, 6));
        fields.emplace_back(line.mid(8, 6));
        fields.emplace_back(line.mid(14, 4));
        fields.emplace_back(line.mid(18, 3));
        fields.emplace_back(line.mid(21, 3));
        fields.emplace_back(line.mid(24, 3));
        fields.emplace_back(line.mid(27, 7));
        fields.emplace_back(line.mid(34, 7));
        fields.emplace_back(line.mid(41));
        break;
    case 60:
        fields.emplace_back(line.mid(0, 2));
        fields.emplace_back(line.mid(2, 6));
        fields.emplace_back(line.mid(8, 6));
        fields.emplace_back(line.mid(14, 4));
        fields.emplace_back(line.mid(18, 3));
        fields.emplace_back(line.mid(21, 3));
        fields.emplace_back(line.mid(24, 3));
        fields.emplace_back(line.mid(27, 7));
        fields.emplace_back(line.mid(34, 7));
        fields.emplace_back(line.mid(41, 5));
        fields.emplace_back(line.mid(46, 6));
        fields.emplace_back(line.mid(52));
        break;
    default: {
        /* METAR codes are variable length, so this is more complicated */
        int firstLine = line.indexOf('\r');
        if (firstLine == 60) {
            auto simple = line.toQByteArray().simplified();
            auto containedLines = Util::ByteView(simple).split('\r');
            if (containedLines.size() == 3) {
                fields.emplace_back(line.mid(0, 2));
                fields.emplace_back(line.mid(2, 6));
                fields.emplace_back(line.mid(8, 6));
                fields.emplace_back(line.mid(14, 4));
                fields.emplace_back(line.mid(18, 3));
                fields.emplace_back(line.mid(21, 3));
                fields.emplace_back(line.mid(24, 3));
                fields.emplace_back(line.mid(27, 7));
                fields.emplace_back(line.mid(34, 7));
                fields.emplace_back(line.mid(41, 5));
                fields.emplace_back(line.mid(46, 6));
                fields.emplace_back(line.mid(52, 6));
                fields.emplace_back(containedLines[1].string_trimmed());
                fields.emplace_back(containedLines[2].string_trimmed());
                break;
            }
        }
        /*if (config.first().strictMode)
            return 7;*/
        fields = CSV::acquisitionSplit(line);
        break;
    }
    }
    if (fields.empty())
        return 8;

    if (FP::defined(lastRecordTime)) {
        lastReportInterval = frameTime - lastRecordTime;
    }
    lastRecordTime = frameTime;

    if (responseState == RESP_INTERACTIVE_RUN_RESPONSE_2)
        return offsetErrorCode(processResponse2(fields, frameTime), 2000);

    switch (fields.size()) {
    case 3:
        return offsetErrorCode(processResponse0(fields, frameTime), 3000);
    case 4:
        return offsetErrorCode(processResponse1(fields, frameTime), 4000);
    case 10:
        return offsetErrorCode(processResponse2(fields, frameTime), 5000);
    case 14:
        return offsetErrorCode(processResponse7(fields, frameTime), 6000);
    case 11:
        /* Allow for missing background luminance reading (all spaces) */
        if (line.size() == 65)
            fields.emplace_back("0");
        else
            break;
    case 12:
    case 13:
        return offsetErrorCode(processResponse7(fields, frameTime, false), 7000);
    default:
        break;
    }

    return 100;
}

void AcquireVaisalaPWDx2::configAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

void AcquireVaisalaPWDx2::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    if (!config.first().id.empty())
        pollID = config.first().id;
}

void AcquireVaisalaPWDx2::invalidateLogValues(double frameTime)
{
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;

        if (loggingEgress != NULL)
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    loggingMux.clear();
    loggingLost(frameTime);

    lastRecordTime = FP::undefined();
    lastReportInterval = FP::undefined();

    autoprobeValidRecords = 0;
}

void AcquireVaisalaPWDx2::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (FP::defined(frameTime))
        configAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 3) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_PASSIVE_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                double interval = config.first().pollInterval;
                if (interval < 15.0)
                    interval = 15.0;
                timeoutAt(frameTime + interval * 2.0 + 1.0);

                realtimeStateUpdated = true;

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }
    case RESP_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);

            responseState = RESP_PASSIVE_RUN;
            ++autoprobeValidRecords;
            generalStatusUpdated();

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN_RESPONSE_2:
    case RESP_INTERACTIVE_RUN_RESPONSE_3:
    case RESP_INTERACTIVE_RUN_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (responseState == RESP_PASSIVE_RUN) {
                double interval = config.first().pollInterval;
                if (interval < 15.0)
                    interval = 15.0;
                timeoutAt(frameTime + interval * 2.0 + 1.0);
            }
            ++autoprobeValidRecords;
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();

            switch (responseState) {
            case RESP_PASSIVE_RUN:
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
                info.hash("ResponseState").setString("PassiveRun");
                break;
            case RESP_INTERACTIVE_RUN_RESPONSE_2:
                if (controlStream != NULL) {
                    controlStream->writeControl("CLOSE\r");
                }
                responseState = RESP_INTERACTIVE_START_CLOSE;
                discardData(frameTime + 1.0);
                timeoutAt(frameTime + 30.0);
                info.hash("ResponseState").setString("RunResponse2");
                break;
            case RESP_INTERACTIVE_RUN_RESPONSE_3:
                if (controlStream != NULL) {
                    controlStream->writeControl("CLOSE\r");
                }
                responseState = RESP_INTERACTIVE_START_CLOSE;
                discardData(frameTime + 1.0);
                timeoutAt(frameTime + 30.0);
                info.hash("ResponseState").setString("RunResponse3");
                break;
            case RESP_INTERACTIVE_RUN_WAIT:
                if (controlStream != NULL) {
                    controlStream->writeControl("CLOSE\r");
                }
                responseState = RESP_INTERACTIVE_START_CLOSE;
                discardData(frameTime + 1.0);
                timeoutAt(frameTime + 30.0);
                info.hash("ResponseState").setString("RunWait");
                break;
            default:
                Q_ASSERT(false);
                break;
            }

            generalStatusUpdated();

            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_OPEN_NOID:
    case RESP_INTERACTIVE_START_OPEN_ID:
        if (Util::contains(frame.toString(), "OPENED FOR OPERATOR COMMANDS")) {
            discardData(frameTime + 0.5);
        }
        break;

    case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN:
        if (!Util::contains(frame.toString(), "SYSTEM PARAMETERS"))
            break;
        responseState = RESP_INTERACTIVE_START_READPARAMETERS_WAIT;
        timeoutAt(frameTime + 2.0);

        parameterData.write().hash("System").remove(true);
        haveEmittedParameters = false;
        break;

    case RESP_INTERACTIVE_START_READPARAMETERS_WAIT: {
        QString line(frame.toQString(false).trimmed());
        if (line.isEmpty())
            break;
        parameterData.write().hash("System").toArray().after_back().setString(line);

        QRegExp reID("VAISALA\\s+"
                     "(PWD\\S+)\\s+"
                     "v\\s*(\\S+(?:\\s+\\d{4}-\\d{2}-\\d{2})?)\\s+"
                     "SN\\s*:\\s*(\\S+)"
                     "(?:\\s+ID\\s+STRING\\s*:\\s*(\\S+))?", Qt::CaseInsensitive);
        if (reID.indexIn(line) != -1) {
            QStringList caps(reID.capturedTexts());

            if (caps.length() > 1 && !caps.at(1).isEmpty()) {
                if (instrumentMeta["Model"].toQString() != caps.at(1)) {
                    instrumentMeta["Model"].setString(caps.at(1));
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    sourceMetadataUpdated();
                }
            }

            if (caps.length() > 2 && !caps.at(2).isEmpty()) {
                if (instrumentMeta["FirmwareVersion"].toQString() != caps.at(2)) {
                    instrumentMeta["FirmwareVersion"].setString(caps.at(2));
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    sourceMetadataUpdated();
                }
            }

            if (caps.length() > 3 && !caps.at(3).isEmpty()) {
                if (instrumentMeta["SerialNumber"].toQString() != caps.at(3)) {
                    instrumentMeta["SerialNumber"].setString(caps.at(3));
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    sourceMetadataUpdated();
                }
            }

            if (caps.length() > 4 && !caps.at(4).isEmpty()) {
                if (config.first().id.empty()) {
                    pollID = caps.at(4).toLatin1();
                }
                if (instrumentMeta["ID"].toQString() != caps.at(4)) {
                    instrumentMeta["ID"].setString(caps.at(4));
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    sourceMetadataUpdated();
                }
            }

            break;
        }

        break;
    }

    case RESP_INTERACTIVE_START_READWEATHERPARAMETERS_BEGIN:
        if (!Util::contains(frame.toString(), "WEATHER PARAMETERS"))
            break;
        responseState = RESP_INTERACTIVE_START_READWEATHERPARAMETERS_WAIT;
        timeoutAt(frameTime + 2.0);

        parameterData.write().hash("Weather").remove(true);
        haveEmittedParameters = false;
        break;

    case RESP_INTERACTIVE_START_READWEATHERPARAMETERS_WAIT: {
        QString line(frame.toQString(false).trimmed());
        if (line.isEmpty())
            break;

        int end = line.length() - 1;
        for (; end > 0 && !line.at(end).isSpace(); --end) { }

        QString name(line.mid(0, end).trimmed());

        bool ok = false;
        double v = line.mid(end).toDouble(&ok);
        if (!ok)
            v = FP::undefined();

        parameterData.write().hash("Weather").hash(name).setDouble(v);
        break;
    }

    case RESP_INTERACTIVE_START_READSTATUS_BEGIN:
        if (!Util::contains(frame.toString(), "PWD STATUS"))
            break;
        responseState = RESP_INTERACTIVE_START_READSTATUS_ANYVALID;
        timeoutAt(frameTime + 2.0);
        break;

    case RESP_INTERACTIVE_START_READSTATUS_ANYVALID: {
        int code = processStatus(frame, frameTime);
        if (code == 0) {
            responseState = RESP_INTERACTIVE_START_READSTATUS_WAIT;
        } else if (code > 0) {
            qCDebug(log) << "Invalid response" << frame << "code" << code
                         << "during initial status read";

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_READSTATUS_WAIT: {
        int code = processStatus(frame, frameTime);
        if (code > 0) {
            qCDebug(log) << "Invalid response" << frame << "code" << code
                         << "during initial status read tail";

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_CLOSE_FINAL:
        if (Util::contains(frame.toString(), "LINE CLOSED")) {
            discardData(frameTime + 0.5);
        }
        break;

    default:
        break;
    }
}

void AcquireVaisalaPWDx2::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configAdvance(frameTime);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_RESPONSE_2:
    case RESP_INTERACTIVE_RUN_RESPONSE_3:
        qCDebug(log) << "Timeout in interactive state" << responseState << "at"
                     << Logging::time(frameTime);

        if (controlStream != NULL) {
            controlStream->writeControl("CLOSE\r");
        }
        timeoutAt(frameTime + 30.0);
        discardData(frameTime + 1.0);
        responseState = RESP_INTERACTIVE_START_CLOSE;
        generalStatusUpdated();

        invalidateLogValues(frameTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("UnpolledRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_RUN_WAIT:
        issuePollingCommand(2);
        timeoutAt(frameTime + 5.0);
        responseState = RESP_INTERACTIVE_RUN_RESPONSE_2;
        break;

    case RESP_INTERACTIVE_START_CLOSE:
    case RESP_INTERACTIVE_START_OPEN_ID:
    case RESP_INTERACTIVE_START_SETDATE:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_DISABLEUNPOLLED:
    case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN:
    case RESP_INTERACTIVE_START_READSTATUS_BEGIN:
    case RESP_INTERACTIVE_START_READSTATUS_ANYVALID:
    case RESP_INTERACTIVE_START_CLOSE_FINAL:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        if (controlStream != NULL) {
            controlStream->writeControl("CLOSE\r");
        }
        timeoutAt(frameTime + 30.0);
        discardData(frameTime + 1.0);
        responseState = RESP_INTERACTIVE_START_CLOSE;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_OPEN_NOID:
        if (controlStream != NULL) {
            controlStream->writeControl("OPEN *\r");
        }
        timeoutAt(frameTime + 5.0);
        responseState = RESP_INTERACTIVE_START_OPEN_ID;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveOpenAnyID"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_READPARAMETERS_WAIT:
        if (controlStream != NULL) {
            controlStream->writeControl("WPAR\r");
        }
        timeoutAt(frameTime + 5.0);
        responseState = RESP_INTERACTIVE_START_READWEATHERPARAMETERS_BEGIN;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadWeatherParameters"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_READWEATHERPARAMETERS_BEGIN:
        /* No response on the PWDx0, so ignore failure */
    case RESP_INTERACTIVE_START_READWEATHERPARAMETERS_WAIT:
        if (controlStream != NULL) {
            controlStream->writeControl("STA\r");
        }
        timeoutAt(frameTime + 5.0);
        responseState = RESP_INTERACTIVE_START_READSTATUS_BEGIN;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveReadStatus"),
                                                       frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_READSTATUS_WAIT:
        if (controlStream != NULL) {
            controlStream->writeControl("CLOSE\r");
        }
        timeoutAt(frameTime + 5.0);
        responseState = RESP_INTERACTIVE_START_CLOSE_FINAL;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveClose"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        invalidateLogValues(frameTime);
        break;

    default:
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireVaisalaPWDx2::discardDataCompleted(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configAdvance(frameTime);

    switch (responseState) {
    case RESP_INTERACTIVE_START_CLOSE:
        if (config.first().id.empty()) {
            controlStream->writeControl("OPEN\r");
            responseState = RESP_INTERACTIVE_START_OPEN_NOID;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveOpenNoID"), frameTime, FP::undefined()));
            }
        } else {
            Util::ByteArray command("OPEN ");
            command += config.first().id;
            command.push_back('\r');
            controlStream->writeControl(std::move(command));
            responseState = RESP_INTERACTIVE_START_OPEN_ID;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                           Variant::Root("StartInteractiveOpenID"),
                                                           frameTime, FP::undefined()));
            }
        }
        timeoutAt(frameTime + 5.0);
        break;

    case RESP_INTERACTIVE_START_OPEN_NOID:
    case RESP_INTERACTIVE_START_OPEN_ID:
        setInstrumentTime = Time::toDateTime(frameTime + 0.5).toUTC();
        if (controlStream != NULL) {
            Util::ByteArray command("DATE ");
            command += QByteArray::number(setInstrumentTime.date().year()).rightJustified(4, '0');
            command.push_back(' ');
            command += QByteArray::number(setInstrumentTime.date().month()).rightJustified(2, '0');
            command.push_back(' ');
            command += QByteArray::number(setInstrumentTime.date().day()).rightJustified(2, '0');
            command.push_back('\r');
            controlStream->writeControl(std::move(command));
        }
        responseState = RESP_INTERACTIVE_START_SETDATE;
        discardData(frameTime + 1.0);
        timeoutAt(frameTime + 5.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetDate"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETDATE:
        if (controlStream != NULL) {
            Util::ByteArray command("TIME ");
            command += QByteArray::number(setInstrumentTime.time().hour()).rightJustified(2, '0');
            command.push_back(' ');
            command += QByteArray::number(setInstrumentTime.time().minute()).rightJustified(2, '0');
            command.push_back(' ');
            command += QByteArray::number(setInstrumentTime.time().second()).rightJustified(2, '0');
            command.push_back('\r');
            controlStream->writeControl(std::move(command));
        }
        responseState = RESP_INTERACTIVE_START_SETTIME;
        discardData(frameTime + 0.5);
        timeoutAt(frameTime + 5.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetTime"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETTIME:
        if (controlStream != NULL) {
            controlStream->writeControl("AMES 0 0\r");
        }
        responseState = RESP_INTERACTIVE_START_DISABLEUNPOLLED;
        discardData(frameTime + 0.5);
        timeoutAt(frameTime + 5.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveDisableUnpolled"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_DISABLEUNPOLLED:
        if (controlStream != NULL) {
            controlStream->writeControl("PAR\r");
        }
        responseState = RESP_INTERACTIVE_START_READPARAMETERS_BEGIN;
        timeoutAt(frameTime + 5.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadParameters"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        if (controlStream != NULL) {
            controlStream->writeControl("CLOSE\r");
        }
        timeoutAt(frameTime + 30.0);
        discardData(frameTime + 1.0);
        responseState = RESP_INTERACTIVE_START_CLOSE;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_CLOSE_FINAL: {
        qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

        issuePollingCommand(2);
        timeoutAt(frameTime + 5.0);
        responseState = RESP_INTERACTIVE_RUN_RESPONSE_2;
        realtimeStateUpdated = true;

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        generalStatusUpdated();

        Variant::Write info = Variant::Write::empty();
        info.hash("ResponseState").setString("StartClose");
        event(frameTime, QObject::tr("Communications established."), false, info);

        break;
    }

    default:
        break;
    }
}


void AcquireVaisalaPWDx2::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frame);
    Q_UNUSED(frameTime);
}

Variant::Root AcquireVaisalaPWDx2::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireVaisalaPWDx2::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN_RESPONSE_2:
    case RESP_INTERACTIVE_RUN_RESPONSE_3:
    case RESP_INTERACTIVE_RUN_WAIT:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

AcquisitionInterface::AutoprobeStatus AcquireVaisalaPWDx2::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

SequenceValue::Transfer AcquireVaisalaPWDx2::getPersistentValues()
{
    PauseLock paused(*this);
    SequenceValue::Transfer result;

    if (currentWX2.read().exists() && FP::defined(currentWX2.getStart())) {
        result.emplace_back(currentWX2);
    }

    return result;
}

void AcquireVaisalaPWDx2::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_RESPONSE_2:
    case RESP_INTERACTIVE_RUN_RESPONSE_3:
    case RESP_INTERACTIVE_RUN_WAIT:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_CLOSE:
    case RESP_INTERACTIVE_START_OPEN_ID:
    case RESP_INTERACTIVE_START_OPEN_NOID:
    case RESP_INTERACTIVE_START_SETDATE:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_DISABLEUNPOLLED:
    case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN:
    case RESP_INTERACTIVE_START_READPARAMETERS_WAIT:
    case RESP_INTERACTIVE_START_READWEATHERPARAMETERS_BEGIN:
    case RESP_INTERACTIVE_START_READWEATHERPARAMETERS_WAIT:
    case RESP_INTERACTIVE_START_READSTATUS_BEGIN:
    case RESP_INTERACTIVE_START_READSTATUS_ANYVALID:
    case RESP_INTERACTIVE_START_READSTATUS_WAIT:
    case RESP_INTERACTIVE_START_CLOSE_FINAL:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        if (controlStream != NULL) {
            controlStream->writeControl("CLOSE\r");
        }
        timeoutAt(time + 30.0);
        discardData(time + 1.0);
        responseState = RESP_INTERACTIVE_START_CLOSE;

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireVaisalaPWDx2::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    double interval = config.first().pollInterval;
    if (interval < 15.0)
        interval = 15.0;

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobeValidRecords = 0;
    discardData(time + 0.5, 1);
    timeoutAt(time + interval * 4.0 + 5.0);
    generalStatusUpdated();
}

void AcquireVaisalaPWDx2::autoprobePromote(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_RESPONSE_2:
    case RESP_INTERACTIVE_RUN_RESPONSE_3:
        qCDebug(log) << "Promoted from interactive run state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);
        /* Reset timeout in case we didn't have a control stream */
        timeoutAt(time + 5.0);
        break;

    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Promoted from interactive run waiting interactive acquisition at"
                     << Logging::time(time);
        issuePollingCommand(2);
        timeoutAt(time + 5.0);
        responseState = RESP_INTERACTIVE_RUN_RESPONSE_2;
        break;

    case RESP_INTERACTIVE_START_CLOSE:
    case RESP_INTERACTIVE_START_OPEN_ID:
    case RESP_INTERACTIVE_START_OPEN_NOID:
    case RESP_INTERACTIVE_START_SETDATE:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_DISABLEUNPOLLED:
    case RESP_INTERACTIVE_START_READPARAMETERS_BEGIN:
    case RESP_INTERACTIVE_START_READPARAMETERS_WAIT:
    case RESP_INTERACTIVE_START_READWEATHERPARAMETERS_BEGIN:
    case RESP_INTERACTIVE_START_READWEATHERPARAMETERS_WAIT:
    case RESP_INTERACTIVE_START_READSTATUS_BEGIN:
    case RESP_INTERACTIVE_START_READSTATUS_ANYVALID:
    case RESP_INTERACTIVE_START_READSTATUS_WAIT:
    case RESP_INTERACTIVE_START_CLOSE_FINAL:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);
        break;

    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        if (controlStream != NULL) {
            controlStream->writeControl("CLOSE\r");
        }
        timeoutAt(time + 30.0);
        discardData(time + 1.0);
        responseState = RESP_INTERACTIVE_START_CLOSE;
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireVaisalaPWDx2::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    /* Reset timeout in case we didn't have a control stream */
    double interval = config.first().pollInterval;
    if (interval < 15.0)
        interval = 15.0;
    timeoutAt(time + interval * 2.0 + 1.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        realtimeStateUpdated = true;

        discardData(FP::undefined());
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireVaisalaPWDx2::getDefaults()
{
    AutomaticDefaults result;
    result.name = "XM$2";
    result.interface["Baud"].setInt64(9600);
    result.interface["Parity"].setString("Even");
    result.interface["DataBits"].setInt64(7);
    result.interface["StopBits"].setInt64(1);
    return result;
}


ComponentOptions AcquireVaisalaPWDx2Component::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options);

    return options;
}

ComponentOptions AcquireVaisalaPWDx2Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireVaisalaPWDx2Component::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireVaisalaPWDx2Component::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireVaisalaPWDx2Component::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options);
}

QList<ComponentExample> AcquireVaisalaPWDx2Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireVaisalaPWDx2Component::createAcquisitionPassive(const ComponentOptions &options,
                                                                                     const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireVaisalaPWDx2(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireVaisalaPWDx2Component::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireVaisalaPWDx2(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireVaisalaPWDx2Component::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{
    std::unique_ptr<AcquireVaisalaPWDx2> i(new AcquireVaisalaPWDx2(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireVaisalaPWDx2Component::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireVaisalaPWDx2> i(new AcquireVaisalaPWDx2(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
