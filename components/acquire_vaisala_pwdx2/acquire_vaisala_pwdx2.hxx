/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREVAISALAPWD22_H
#define ACQUIREVAISALAPWD22_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QDateTime>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class AcquireVaisalaPWDx2 : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Passive autoprobe initialization, ignorging the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,

        /* Acquiring data in interactive mode, waiting for response type 2 */
                RESP_INTERACTIVE_RUN_RESPONSE_2,
        /* Acquiring data in interactive mode, waiting for response type 3
         * (status response) */
                RESP_INTERACTIVE_RUN_RESPONSE_3,
        /* Acquiring data in interactive mode, waiting for the next poll
         * interval */
                RESP_INTERACTIVE_RUN_WAIT,

        /* Sent a "CLOSE" command and discarding data for a second to
         * make sure we have control of the line */
                RESP_INTERACTIVE_START_CLOSE,
        /* Waiting for the acknowledge from an "OPEN" command */
                RESP_INTERACTIVE_START_OPEN_NOID,
        /* Waiting for the acknowledge from an "OPEN *" or "OPEN <ID>" command,
         * this can be entered either because the general open failed
         * or because the configuration specified an ID explicitly */
                RESP_INTERACTIVE_START_OPEN_ID,

        /* Waiting for the "DATE" command to set the date */
                RESP_INTERACTIVE_START_SETDATE,
        /* Waiting for the "TIME" command to set the time */
                RESP_INTERACTIVE_START_SETTIME,
        /* Waiting for the "AMES 0 0" command to disable unpolled */
                RESP_INTERACTIVE_START_DISABLEUNPOLLED,
        /* Reading the the response to the "PAR" command */
                RESP_INTERACTIVE_START_READPARAMETERS_BEGIN,
        /* Waiting for all lines to complete */
                RESP_INTERACTIVE_START_READPARAMETERS_WAIT,
        /* Reading the the response to the "WPAR" command */
                RESP_INTERACTIVE_START_READWEATHERPARAMETERS_BEGIN,
        /* Waiting for all lines to complete */
                RESP_INTERACTIVE_START_READWEATHERPARAMETERS_WAIT,
        /* Reading the the response to the "STA" command */
                RESP_INTERACTIVE_START_READSTATUS_BEGIN,
        /* Waiting for any recognized line in the status response */
                RESP_INTERACTIVE_START_READSTATUS_ANYVALID,
        /* Waiting for all lines to complete */
                RESP_INTERACTIVE_START_READSTATUS_WAIT,
        /* Waiting for the acknowledge from a "CLOSE" command */
                RESP_INTERACTIVE_START_CLOSE_FINAL,

        /* Waiting before attempting a communications restart */
                RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
                RESP_INTERACTIVE_INITIALIZE,
    };
    ResponseState responseState;
    int autoprobeValidRecords;

    enum LogStreamID {
        LogStream_Metadata = 0,
        LogStream_State,

        LogStream_Visibility,
        LogStream_PresentWeather,
        LogStream_PrecipitationRate,
        LogStream_Temperature,
        LogStream_BoardTemperature,
        LogStream_DRDTemperature,
        LogStream_BackgroundLuminance,

        LogStream_SignalFrequency,
        LogStream_SignalOffset,
        LogStream_AmbientLightVoltage,
        LogStream_LEDVoltage,

        LogStream_ReceiverBackscatter,
        LogStream_TransmitterBackscatter,

        LogStream_TOTAL,
        LogStream_RecordBaseStart = LogStream_Visibility,
    };
    CPD3::Data::StaticMultiplexer loggingMux;
    quint32 streamAge[LogStream_TOTAL];
    double streamTime[LogStream_TOTAL];

    friend class Configuration;

    class Configuration {
        double start;
        double end;
    public:
        bool strictMode;
        double pollInterval;

        CPD3::Util::ByteArray id;

        enum {
            VisibilitySource_Auto, VisibilitySource_1Min, VisibilitySource_10Min,
        } visibilitySource;
        enum {
            PresentWeatherSource_Auto,
            PresentWeatherSource_Instant,
            PresentWeatherSource_15Min,
            PresentWeatherSource_1Hour,
        } presentWeatherSource;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    QDateTime setInstrumentTime;
    double lastReportInterval;
    CPD3::Util::ByteArray pollID;

    enum {
        Visibility_1Min, Visibility_10Min,
    } selectedVisibilitySource;
    enum {
        Weather_Instant, Weather_15Min, Weather_1Hour,
    } selectedPresentWeatherSource;

    void setDefaultInvalid();

    CPD3::Data::Variant::Root instrumentMeta;

    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool haveEmittedParameters;
    bool realtimeStateUpdated;
    CPD3::Data::Variant::Flags priorDisplayFlags;

    CPD3::Data::SequenceValue currentWX2;
    CPD3::Data::Variant::Root parameterData;

    CPD3::Data::Variant::Flags alarmFlags;
    CPD3::Data::Variant::Flags statusHardwareFlags;

    void logValue(double frameTime,
                  int streamID,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    void advanceStream(double frameTime, int streamID);

    void realtimeWeatherCodeTranslation(CPD3::Data::Variant::Write &target) const;

    inline void realtimeWeatherCodeTranslation(CPD3::Data::Variant::Write &&target) const
    { return realtimeWeatherCodeTranslation(target); }

    void loggingWeatherFlagsMeta(CPD3::Data::Variant::Write &target) const;

    inline void loggingWeatherFlagsMeta(CPD3::Data::Variant::Write &&target) const
    { return loggingWeatherFlagsMeta(target); }

    CPD3::Data::Variant::Root describeVisibilitySource() const;

    CPD3::Data::Variant::Root describePresentWeatherSource() const;

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void reselectVisibilitySource();

    void reselectPresentWeatherSource();

    void updateMetadata(double frameTime);

    void advanceState(double frameTime);

    void issuePollingCommand(int code);

    int processStatus(const CPD3::Util::ByteView &data, double frameTime);

    void updateWX2(CPD3::Data::Variant::Root &&WX2, double frameTime);

    int processAlarmField(const CPD3::Util::ByteView &field);

    int processSYNOPCode(const CPD3::Util::ByteView &field,
                         CPD3::Data::Variant::Root &WX1,
                         CPD3::Data::Variant::Root &WX2,
                         CPD3::Data::Variant::Root &ZWXState);

    int processVisiblity(std::deque<CPD3::Util::ByteView> &fields,
                         CPD3::Data::Variant::Root &ZWZ1Min,
                         CPD3::Data::Variant::Root &ZWZ10Min,
                         CPD3::Data::Variant::Root &WZ);

    int processResponse0(std::deque<CPD3::Util::ByteView> &fields, double frameTime);

    int processResponse1(std::deque<CPD3::Util::ByteView> &fields, double frameTime);

    int processResponse2(std::deque<CPD3::Util::ByteView> &fields, double frameTime);

    int processResponse7(std::deque<CPD3::Util::ByteView> &fields,
                         double frameTime,
                         bool requireMETARCodes = true);

    int processRecord(CPD3::Util::ByteView line, double frameTime);

    void configAdvance(double frameTime);

    void configurationChanged();

    void invalidateLogValues(double frameTime);

public:
    AcquireVaisalaPWDx2(const CPD3::Data::ValueSegment::Transfer &config,
                        const std::string &loggingContext);

    AcquireVaisalaPWDx2(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireVaisalaPWDx2();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Data::SequenceValue::Transfer getPersistentValues() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables() override;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus() override;

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    std::size_t dataFrameStart(std::size_t offset,
                               const CPD3::Util::ByteArray &input) const override;

    std::size_t dataFrameEnd(std::size_t start, const CPD3::Util::ByteArray &input) const override;

    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;
};

class AcquireVaisalaPWDx2Component
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_vaisala_pwdx2"
                              FILE
                              "acquire_vaisala_pwdx2.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
