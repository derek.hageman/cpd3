/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "core/environment.hxx"
#include "algorithms/dewpoint.hxx"

#include "calc_dewpoint.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Algorithms;

void CalcDewpoint::buildFixedProcessing(Processing *p)
{
    SequenceName::Component station;
    SequenceName::Component archive;
    SequenceName::Component suffix;
    SequenceName::Flavors flavors;

    SequenceName::Set names;
    Util::merge(p->operateRH->getAllUnits(), names);
    Util::merge(p->operateTemperature->getAllUnits(), names);
    Util::merge(p->operateDewpoint->getAllUnits(), names);
    QRegExp reCheck("(?:U|T|TD)([uc]?\\d*_.+)");
    for (const auto &name : names) {
        station = name.getStation();
        archive = name.getArchive();
        flavors = name.getFlavors();

        if (reCheck.exactMatch(name.getVariableQString())) {
            if (suffix.empty())
                suffix = reCheck.cap(1).toStdString();
        }
    }

    if (defaultRH == NULL) {
        p->inputRH = new DynamicInput::Basic(SequenceName(station, archive, "U" + suffix, flavors));
    } else {
        p->inputRH = defaultRH->clone();
    }

    if (defaultTemperature == NULL) {
        p->inputTemperature =
                new DynamicInput::Basic(SequenceName(station, archive, "T" + suffix, flavors));
    } else {
        p->inputTemperature = defaultTemperature->clone();
    }

    if (defaultDewpoint == NULL) {
        p->inputDewpoint =
                new DynamicInput::Basic(SequenceName(station, archive, "TD" + suffix, flavors));
    } else {
        p->inputDewpoint = defaultDewpoint->clone();
    }

    p->forceWater = new DynamicPrimitive<bool>::Constant(false);

    p->alwaysCalculateRH = new DynamicPrimitive<bool>::Constant(defaultAlawysCalculate);
    p->alwaysCalculateTemperature = new DynamicPrimitive<bool>::Constant(defaultAlawysCalculate);
    p->alwaysCalculateDewpoint = new DynamicPrimitive<bool>::Constant(defaultAlawysCalculate);
}

void CalcDewpoint::handleOptions(const ComponentOptions &options)
{
    if (options.isSet("input-rh")) {
        defaultRH = qobject_cast<DynamicInputOption *>(options.get("input-rh"))->getInput();
    } else {
        defaultRH = NULL;
    }

    if (options.isSet("input-t")) {
        defaultTemperature = qobject_cast<DynamicInputOption *>(options.get("input-t"))->getInput();
    } else {
        defaultTemperature = NULL;
    }

    if (options.isSet("input-td")) {
        defaultDewpoint = qobject_cast<DynamicInputOption *>(options.get("input-td"))->getInput();
    } else {
        defaultDewpoint = NULL;
    }

    if (options.isSet("always")) {
        defaultAlawysCalculate =
                qobject_cast<ComponentOptionBoolean *>(options.get("always"))->get();
    } else {
        defaultAlawysCalculate = false;
    }

    restrictedInputs = false;

    if (options.isSet("output-rh")) {
        restrictedInputs = true;

        Processing p;

        p.operateRH = qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("output-rh"))->getOperator();
        p.operateTemperature = new DynamicSequenceSelection::None;
        p.operateDewpoint = new DynamicSequenceSelection::None;

        buildFixedProcessing(&p);
        processing.push_back(p);
    }

    if (options.isSet("output-t")) {
        restrictedInputs = true;

        Processing p;

        p.operateTemperature = qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("output-t"))->getOperator();
        p.operateRH = new DynamicSequenceSelection::None;
        p.operateDewpoint = new DynamicSequenceSelection::None;

        buildFixedProcessing(&p);
        processing.push_back(p);
    }

    if (options.isSet("output-td")) {
        restrictedInputs = true;

        Processing p;

        p.operateDewpoint = qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-td"))
                ->getOperator();
        p.operateTemperature = new DynamicSequenceSelection::None;
        p.operateRH = new DynamicSequenceSelection::None;

        buildFixedProcessing(&p);
        processing.push_back(p);
    }

    if (options.isSet("rh-suffix")) {
        filterSuffixesRH = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(
                        options.get("rh-suffix"))->get());
    }
    if (options.isSet("t-suffix")) {
        filterSuffixesTemperature = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("t-suffix"))->get());
    }
    if (options.isSet("td-suffix")) {
        filterSuffixesDewpoint = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(
                        options.get("td-suffix"))->get());
    }
}


CalcDewpoint::CalcDewpoint()
{ Q_ASSERT(false); }

CalcDewpoint::CalcDewpoint(const ComponentOptions &options)
{
    handleOptions(options);
}

CalcDewpoint::CalcDewpoint(const ComponentOptions &options,
                           double start,
                           double end,
                           const QList<SequenceName> &inputs)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    handleOptions(options);

    for (QList<SequenceName>::const_iterator unit = inputs.constBegin(), endU = inputs.constEnd();
            unit != endU;
            ++unit) {
        CalcDewpoint::unhandled(*unit, NULL);
    }
}

CalcDewpoint::CalcDewpoint(double start,
                           double end,
                           const SequenceName::Component &station,
                           const SequenceName::Component &archive,
                           const ValueSegment::Transfer &config)
{
    defaultRH = NULL;
    defaultTemperature = NULL;
    defaultDewpoint = NULL;
    defaultAlawysCalculate = false;
    restrictedInputs = true;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    for (const auto &child : children) {
        Processing p;

        p.operateRH = DynamicSequenceSelection::fromConfiguration(config,
                                                                  QString("%1/CalculateRH").arg(
                                                                          QString::fromStdString(
                                                                                  child)), start,
                                                                  end);
        p.operateTemperature = DynamicSequenceSelection::fromConfiguration(config,
                                                                           QString("%1/CalculateTemperature")
                                                                                   .arg(QString::fromStdString(
                                                                                           child)),
                                                                           start, end);
        p.operateDewpoint = DynamicSequenceSelection::fromConfiguration(config,
                                                                        QString("%1/CalculateDewpoint")
                                                                                .arg(QString::fromStdString(
                                                                                        child)),
                                                                        start, end);

        p.inputRH = DynamicInput::fromConfiguration(config, QString("%1/InputRH").arg(
                QString::fromStdString(child)), start, end);
        p.inputTemperature = DynamicInput::fromConfiguration(config,
                                                             QString("%1/InputTemperature").arg(
                                                                     QString::fromStdString(child)),
                                                             start, end);
        p.inputDewpoint = DynamicInput::fromConfiguration(config, QString("%1/InputDewpoint").arg(
                QString::fromStdString(child)), start, end);

        p.operateRH->registerExpected(station, archive);
        p.operateTemperature->registerExpected(station, archive);
        p.operateDewpoint->registerExpected(station, archive);
        p.inputRH->registerExpected(station, archive);
        p.inputTemperature->registerExpected(station, archive);
        p.inputDewpoint->registerExpected(station, archive);

        p.alwaysCalculateRH = DynamicBoolOption::fromConfiguration(config,
                                                                   QString("%1/AlwaysRH").arg(
                                                                           QString::fromStdString(
                                                                                   child)), start,
                                                                   end);
        p.alwaysCalculateTemperature = DynamicBoolOption::fromConfiguration(config,
                                                                            QString("%1/AlwaysTemperature")
                                                                                    .arg(QString::fromStdString(
                                                                                            child)),
                                                                            start, end);
        p.alwaysCalculateDewpoint = DynamicBoolOption::fromConfiguration(config,
                                                                         QString("%1/AlwaysDewpoint")
                                                                                 .arg(QString::fromStdString(
                                                                                         child)),
                                                                         start, end);

        p.forceWater = DynamicBoolOption::fromConfiguration(config, QString("%1/ForceWater").arg(
                QString::fromStdString(child)), start, end);

        processing.push_back(p);
    }
}


CalcDewpoint::~CalcDewpoint()
{
    if (defaultRH != NULL)
        delete defaultRH;
    if (defaultTemperature != NULL)
        delete defaultTemperature;
    if (defaultDewpoint != NULL)
        delete defaultDewpoint;

    for (std::vector<Processing>::const_iterator p = processing.begin(), end = processing.end();
            p != end;
            ++p) {
        delete p->operateRH;
        delete p->operateTemperature;
        delete p->operateDewpoint;
        delete p->inputRH;
        delete p->inputTemperature;
        delete p->inputDewpoint;
        delete p->alwaysCalculateRH;
        delete p->alwaysCalculateTemperature;
        delete p->alwaysCalculateDewpoint;
        delete p->forceWater;
    }
}

void CalcDewpoint::handleNewProcessing(const SequenceName &unit,
                                       int id,
                                       SegmentProcessingStage::SequenceHandlerControl *control)
{
    Processing *p = &processing[id];

    SequenceName::Set reg;

    p->operateRH->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->operateRH->getAllUnits(), reg);

    p->operateTemperature
     ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->operateTemperature->getAllUnits(), reg);

    p->operateDewpoint
     ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->operateDewpoint->getAllUnits(), reg);

    if (!control)
        return;

    for (const auto &n : reg) {
        bool isNew = !control->isHandled(n);
        control->filterUnit(n, id);
        if (isNew)
            registerPossibleInput(n, control, id);
    }
}

void CalcDewpoint::registerPossibleInput(const SequenceName &unit,
                                         SegmentProcessingStage::SequenceHandlerControl *control,
                                         int filterID)
{
    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        bool usedAsInput = false;

        if (processing[id].inputRH->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }
        if (processing[id].inputTemperature->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }
        if (processing[id].inputDewpoint->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }

        if (usedAsInput && id != filterID)
            handleNewProcessing(unit, id, control);
    }

    if (defaultRH != NULL && defaultRH->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultTemperature != NULL && defaultTemperature->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultDewpoint != NULL && defaultDewpoint->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
}

void CalcDewpoint::unhandled(const SequenceName &unit,
                             SegmentProcessingStage::SequenceHandlerControl *control)
{
    registerPossibleInput(unit, control);

    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].operateRH->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            handleNewProcessing(unit, id, control);
            return;
        }
        if (processing[id].operateTemperature->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            handleNewProcessing(unit, id, control);
            return;
        }
        if (processing[id].operateDewpoint->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            handleNewProcessing(unit, id, control);
            return;
        }
    }

    if (restrictedInputs)
        return;

    if (unit.hasFlavor(SequenceName::flavor_cover) || unit.hasFlavor(SequenceName::flavor_stats))
        return;

    QRegExp reCheck("(?:U|T|TD)([uc]?\\d*)_(.+)");
    if (!reCheck.exactMatch(unit.getVariableQString()))
        return;
    auto suffix = reCheck.cap(2).toStdString();
    if (suffix.empty())
        return;

    auto prefix = reCheck.cap(1).toStdString();

    SequenceName rhUnit
            (unit.getStation(), unit.getArchive(), "U" + prefix + "_" + suffix, unit.getFlavors());
    SequenceName temperatureUnit
            (unit.getStation(), unit.getArchive(), "T" + prefix + "_" + suffix, unit.getFlavors());
    SequenceName dewpointUnit
            (unit.getStation(), unit.getArchive(), "TD" + prefix + "_" + suffix, unit.getFlavors());

    Processing p;
    if (!filterSuffixesRH.empty() ||
            !filterSuffixesTemperature.empty() ||
            !filterSuffixesDewpoint.empty()) {
        for (int id = 0, max = (int) processing.size(); id < max; id++) {
            if (processing[id].suffix == suffix)
                return;
        }

        if (!filterSuffixesRH.count(suffix) &&
                !filterSuffixesTemperature.count(suffix) &&
                !filterSuffixesDewpoint.count(suffix))
            return;

        if (filterSuffixesRH.count(suffix)) {
            p.operateRH = new DynamicSequenceSelection::Single(rhUnit);
        } else {
            p.operateRH = new DynamicSequenceSelection::None;
        }

        if (filterSuffixesTemperature.count(suffix)) {
            p.operateTemperature = new DynamicSequenceSelection::Single(temperatureUnit);
        } else {
            p.operateTemperature = new DynamicSequenceSelection::None;
        }

        if (filterSuffixesDewpoint.count(suffix)) {
            p.operateDewpoint = new DynamicSequenceSelection::Single(dewpointUnit);
        } else {
            p.operateDewpoint = new DynamicSequenceSelection::None;
        }

        p.suffix = suffix;
    } else {
        p.operateRH = new DynamicSequenceSelection::Single(rhUnit);
        p.operateTemperature = new DynamicSequenceSelection::Single(temperatureUnit);
        p.operateDewpoint = new DynamicSequenceSelection::Single(dewpointUnit);
    }


    if (defaultRH == NULL) {
        p.inputRH = new DynamicInput::Basic(rhUnit);
    } else {
        p.inputRH = defaultRH->clone();
    }
    if (defaultTemperature == NULL) {
        p.inputTemperature = new DynamicInput::Basic(temperatureUnit);
    } else {
        p.inputTemperature = defaultTemperature->clone();
    }
    if (defaultDewpoint == NULL) {
        p.inputDewpoint = new DynamicInput::Basic(dewpointUnit);
    } else {
        p.inputDewpoint = defaultDewpoint->clone();
    }

    p.forceWater = new DynamicPrimitive<bool>::Constant(false);

    p.alwaysCalculateRH = new DynamicPrimitive<bool>::Constant(defaultAlawysCalculate);
    p.alwaysCalculateTemperature = new DynamicPrimitive<bool>::Constant(defaultAlawysCalculate);
    p.alwaysCalculateDewpoint = new DynamicPrimitive<bool>::Constant(defaultAlawysCalculate);

    int id = (int) processing.size();
    processing.push_back(p);

    SequenceName::Set toRegister;
    toRegister.insert(std::move(rhUnit));
    toRegister.insert(std::move(temperatureUnit));
    toRegister.insert(std::move(dewpointUnit));
    for (const auto &u : toRegister) {
        if (processing[id].inputRH->registerInput(u)) {
            if (control != NULL)
                control->inputUnit(u, id);
        }
        if (processing[id].inputTemperature->registerInput(u)) {
            if (control != NULL)
                control->inputUnit(u, id);
        }
        if (processing[id].inputDewpoint->registerInput(u)) {
            if (control != NULL)
                control->inputUnit(u, id);
        }

        if (processing[id].operateRH->registerInput(u)) {
            if (control != NULL)
                control->filterUnit(u, id);
            continue;
        }
        if (processing[id].operateTemperature->registerInput(u)) {
            if (control != NULL)
                control->filterUnit(u, id);
            continue;
        }
        if (processing[id].operateDewpoint->registerInput(u)) {
            if (control != NULL)
                control->filterUnit(u, id);
            continue;
        }
    }
}

template<typename Operation>
static void calculate(const SequenceName &unit,
                      SequenceSegment &data,
                      bool always,
                      bool forceWater,
                      double a,
                      double b,
                      Operation op)
{
    auto d = data[unit];
    Variant::Composite::applyInplace(data[unit], [&](Variant::Write &d) {
        double v = d.toReal();
        if (FP::defined(v) && !always)
            return;
        v = op(a, b, forceWater);
        if (!FP::defined(v)) {
            Variant::Composite::invalidate(d);
            return;
        }
        d.setReal(v);
    });
}


void CalcDewpoint::process(int id, SequenceSegment &data)
{
    Processing *proc = &processing[id];

    double rh = proc->inputRH->get(data);
    double temperature = proc->inputTemperature->get(data);
    double dewpoint = proc->inputDewpoint->get(data);
    bool forceWater = proc->forceWater->get(data);

    bool always = proc->alwaysCalculateRH->get(data);
    for (const auto &i : proc->operateRH->get(data)) {
        calculate(i, data, always, forceWater, temperature, dewpoint, Dewpoint::rh);
    }

    always = proc->alwaysCalculateTemperature->get(data);
    for (const auto &i : proc->operateTemperature->get(data)) {
        calculate(i, data, always, forceWater, rh, dewpoint, Dewpoint::t);
    }

    always = proc->alwaysCalculateDewpoint->get(data);
    for (const auto &i : proc->operateDewpoint->get(data)) {
        calculate(i, data, always, forceWater, temperature, rh, Dewpoint::dewpoint);
    }
}


void CalcDewpoint::processMeta(int id, SequenceSegment &data)
{
    Processing *p = &processing[id];

    Variant::Root meta;

    meta["By"].setString("calc_dewpoint");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    meta["Parameters"].hash("U").setString(p->inputRH->describe(data));
    meta["Parameters"].hash("T").setString(p->inputTemperature->describe(data));
    meta["Parameters"].hash("TD").setString(p->inputDewpoint->describe(data));
    meta["Parameters"].hash("ForceWater").setBool(p->forceWater->get(data));

    /* Note: we don't set the smoothing parameters because there's not a good
     * way to guess them from the input specifications.  It would be possible
     * to extend those to support that, but it's probably not worth it. */

    meta["Parameters"].hash("AlwaysCalculate").setBool(p->alwaysCalculateRH->get(data));
    for (auto i : p->operateRH->get(data)) {
        i.setMeta();
        if (!data[i].exists()) {
            data[i].metadataReal("Description")
                   .setString("Calculated relative humidity from temperature and dewpoint");
            data[i].metadataReal("Format").setString("00.0");
            data[i].metadataReal("Units").setString("%");
        }
        data[i].metadata("Processing").toArray().after_back().set(meta);
    }

    meta["Parameters"].hash("AlwaysCalculate").setBool(p->alwaysCalculateTemperature->get(data));
    for (auto i : p->operateTemperature->get(data)) {
        i.setMeta();
        if (!data[i].exists()) {
            data[i].metadataReal("Description")
                   .setString("Calculated temperature from relative humidity and dewpoint");
            data[i].metadataReal("Format").setString("00.0");
            data[i].metadataReal("Units").setString("\xC2\xB0\x43");
        }
        data[i].metadata("Processing").toArray().after_back().set(meta);
    }

    meta["Parameters"].hash("AlwaysCalculate").setBool(p->alwaysCalculateDewpoint->get(data));
    for (auto i : p->operateDewpoint->get(data)) {
        i.setMeta();
        if (!data[i].exists()) {
            data[i].metadataReal("Description")
                   .setString("Calculated dewpoint from temperature and relative humidity");
            data[i].metadataReal("Format").setString("00.0");
            data[i].metadataReal("Units").setString("\xC2\xB0\x43");
        }
        data[i].metadata("Processing").toArray().after_back().set(meta);
    }
}


SequenceName::Set CalcDewpoint::requestedInputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::const_iterator p = processing.begin(), end = processing.end();
            p != end;
            ++p) {
        Util::merge(p->inputRH->getUsedInputs(), out);
        Util::merge(p->inputTemperature->getUsedInputs(), out);
        Util::merge(p->inputDewpoint->getUsedInputs(), out);
    }
    return out;
}

SequenceName::Set CalcDewpoint::predictedOutputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::const_iterator p = processing.begin(), end = processing.end();
            p != end;
            ++p) {
        Util::merge(p->operateRH->getAllUnits(), out);
        Util::merge(p->operateTemperature->getAllUnits(), out);
        Util::merge(p->operateDewpoint->getAllUnits(), out);
    }
    return out;
}

QSet<double> CalcDewpoint::metadataBreaks(int id)
{
    Processing *p = &processing[id];
    QSet<double> result;
    Util::merge(p->operateRH->getChangedPoints(), result);
    Util::merge(p->operateTemperature->getChangedPoints(), result);
    Util::merge(p->operateDewpoint->getChangedPoints(), result);
    Util::merge(p->inputRH->getChangedPoints(), result);
    Util::merge(p->inputTemperature->getChangedPoints(), result);
    Util::merge(p->inputDewpoint->getChangedPoints(), result);
    Util::merge(p->alwaysCalculateRH->getChangedPoints(), result);
    Util::merge(p->alwaysCalculateTemperature->getChangedPoints(), result);
    Util::merge(p->alwaysCalculateDewpoint->getChangedPoints(), result);
    Util::merge(p->forceWater->getChangedPoints(), result);
    return result;
}

CalcDewpoint::CalcDewpoint(QDataStream &stream)
{
    stream >> defaultRH;
    stream >> defaultTemperature;
    stream >> defaultDewpoint;
    stream >> defaultAlawysCalculate;
    stream >> filterSuffixesRH;
    stream >> filterSuffixesTemperature;
    stream >> filterSuffixesDewpoint;
    stream >> restrictedInputs;

    quint32 n;
    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        Processing p;
        stream >> p.operateRH;
        stream >> p.operateTemperature;
        stream >> p.operateDewpoint;
        stream >> p.inputRH;
        stream >> p.inputTemperature;
        stream >> p.inputDewpoint;
        stream >> p.alwaysCalculateRH;
        stream >> p.alwaysCalculateTemperature;
        stream >> p.alwaysCalculateDewpoint;
        stream >> p.forceWater;
        processing.push_back(p);
    }
}

void CalcDewpoint::serialize(QDataStream &stream)
{
    stream << defaultRH;
    stream << defaultTemperature;
    stream << defaultDewpoint;
    stream << defaultAlawysCalculate;
    stream << filterSuffixesRH;
    stream << filterSuffixesTemperature;
    stream << filterSuffixesDewpoint;
    stream << restrictedInputs;

    quint32 n = (quint32) processing.size();
    stream << n;
    for (int i = 0; i < (int) n; i++) {
        stream << processing[i].operateRH;
        stream << processing[i].operateTemperature;
        stream << processing[i].operateDewpoint;
        stream << processing[i].inputRH;
        stream << processing[i].inputTemperature;
        stream << processing[i].inputDewpoint;
        stream << processing[i].alwaysCalculateRH;
        stream << processing[i].alwaysCalculateTemperature;
        stream << processing[i].alwaysCalculateDewpoint;
        stream << processing[i].forceWater;
    }
}


QString CalcDewpointComponent::getBasicSerializationName() const
{ return QString::fromLatin1("calc_dewpoint"); }

ComponentOptions CalcDewpointComponent::getOptions()
{
    ComponentOptions options;

    options.add("input-rh",
                new DynamicInputOption(tr("input-rh", "name"), tr("Input relative humidity"),
                                       tr("This is the relative humidity to use in calculations.  This is "
                                          "used when calculating a dewpoint or a temperature."),
                                       tr("Resolved automatically from other variables")));

    options.add("input-t", new DynamicInputOption(tr("input-t", "name"), tr("Input temperature"),
                                                  tr("This is the temperature to use in calculations.  This is "
                                                     "used when calculating a relative humidity or dewpoint."),
                                                  tr("Resolved automatically from other variables")));

    options.add("input-td", new DynamicInputOption(tr("input-td", "name"), tr("Input dewpoint"),
                                                   tr("This is the dewpoint to use in calculations.  This is "
                                                      "used when calculating a relative humidity or temperature."),
                                                   tr("Resolved automatically from other variables")));

    options.add("output-rh", new DynamicSequenceSelectionOption(tr("output-rh", "name"),
                                                                tr("Relative humidity variables to calculate"),
                                                                tr("These are the relative humidity variables that are generated.  "
                                                                   "When only these variables are calculated from a single set of "
                                                                   "inputs."),
                                                                tr("All relative humidities")));

    options.add("output-t", new DynamicSequenceSelectionOption(tr("output-t", "name"),
                                                               tr("Temperature variables to calculate"),
                                                               tr("These are the temperature variables that are generated.  "
                                                                  "When only these variables are calculated from a single set of "
                                                                  "inputs."),
                                                               tr("All temperatures")));

    options.add("output-td", new DynamicSequenceSelectionOption(tr("output-td", "name"),
                                                                tr("Dewpoint variables to calculate"),
                                                                tr("These are the dewpoint variables that are generated.  "
                                                                   "When only these variables are calculated from a single set of "
                                                                   "inputs."),
                                                                tr("All temperatures")));

    options.add("always",
                new ComponentOptionBoolean(tr("always", "name"), tr("Always calculate outputs"),
                                           tr("When set, this enables calculation of the outputs regardless of "
                                              "if they already have values.  Normally the outputs are only "
                                              "calculated when they do not already exist in the data, but this "
                                              "will cause them to always be (re-)calculated."),
                                           QString()));


    options.add("rh-suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments-rh", "name"),
                                                                    tr("Instrument suffixes to calculate relative humidity for"),
                                                                    tr("These are the instrument suffixes to perform relative humidity "
                                                                       "calculation for.  When set, only relative humidities with these "
                                                                       "suffixes are generated.  This option is mutually exclusive with "
                                                                       "manual variable specification."),
                                                                    tr("All instrument suffixes")));
    options.exclude("output-rh", "rh-suffix");
    options.exclude("output-t", "rh-suffix");
    options.exclude("output-td", "rh-suffix");
    options.exclude("rh-suffix", "output-rh");
    options.exclude("rh-suffix", "output-t");
    options.exclude("rh-suffix", "output-td");

    options.add("t-suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments-t", "name"),
                                                                   tr("Instrument suffixes to calculate temperature for"),
                                                                   tr("These are the instrument suffixes to perform temperature "
                                                                      "calculation for.  When set, only temperatures with these "
                                                                      "suffixes are generated.  This option is mutually exclusive with "
                                                                      "manual variable specification."),
                                                                   tr("All instrument suffixes")));
    options.exclude("output-rh", "t-suffix");
    options.exclude("output-t", "t-suffix");
    options.exclude("output-td", "t-suffix");
    options.exclude("t-suffix", "output-rh");
    options.exclude("t-suffix", "output-t");
    options.exclude("t-suffix", "output-td");

    options.add("td-suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments-td", "name"),
                                                                    tr("Instrument suffixes to calculate dewpoint for"),
                                                                    tr("These are the instrument suffixes to perform dewpoint "
                                                                       "calculation for.  When set, only dewpoints with these "
                                                                       "suffixes are generated.  This option is mutually exclusive with "
                                                                       "manual variable specification."),
                                                                    tr("All instrument suffixes")));
    options.exclude("output-rh", "td-suffix");
    options.exclude("output-t", "td-suffix");
    options.exclude("output-td", "td-suffix");
    options.exclude("td-suffix", "output-rh");
    options.exclude("td-suffix", "output-t");
    options.exclude("td-suffix", "output-td");

    return options;
}

QList<ComponentExample> CalcDewpointComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will calculate temperatures, dewpoints, and relative "
                                        "humidities for all sensors present in the input data stream.  "
                                        "If two are present the missing third one will be calculated.  "
                                        "For example, if the input contains T_V11 and U_V11 the output "
                                        "will have TD_V11 generated.")));

    options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("td-suffix")))->add("S11");
    (qobject_cast<DynamicInputOption *>(options.get("input-rh")))->set(
            SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("U_V11")), Calibration());
    examples.append(ComponentExample(options, tr("Single instrument with alternate humidity"),
                                     tr("This will calculate the dewpoints for all sensors belonging to "
                                        "the S11 instrument but using the value of U_V11 as the relative "
                                        "humidity.")));

    options = getOptions();
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-rh")))->set("", "",
                                                                                    "U_XM1");
    (qobject_cast<DynamicInputOption *>(options.get("input-t")))->set(
            SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("T_XM1")), Calibration());
    (qobject_cast<DynamicInputOption *>(options.get("input-td")))->set(
            SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("TD_XM1")),
            Calibration());
    examples.append(ComponentExample(options, tr("Single variable"),
                                     tr("This will calculate U_XM1 from TD_XM1 and T_XM1.")));

    return examples;
}

SegmentProcessingStage *CalcDewpointComponent::createBasicFilterDynamic(const ComponentOptions &options)
{ return new CalcDewpoint(options); }

SegmentProcessingStage *CalcDewpointComponent::createBasicFilterPredefined(const ComponentOptions &options,
                                                                           double start,
                                                                           double end,
                                                                           const QList<
                                                                                   SequenceName> &inputs)
{ return new CalcDewpoint(options, start, end, inputs); }

SegmentProcessingStage *CalcDewpointComponent::createBasicFilterEditing(double start,
                                                                        double end,
                                                                        const SequenceName::Component &station,
                                                                        const SequenceName::Component &archive,
                                                                        const ValueSegment::Transfer &config)
{ return new CalcDewpoint(start, end, station, archive, config); }

SegmentProcessingStage *CalcDewpointComponent::deserializeBasicFilter(QDataStream &stream)
{ return new CalcDewpoint(stream); }