/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"
#include "algorithms/dewpoint.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Algorithms;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    class ID {
    public:
        ID() : filter(), input()
        { }

        ID(const QSet<SequenceName> &setFilter, const QSet<SequenceName> &setInput) : filter(
                setFilter), input(setInput)
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }

        QSet<SequenceName> filter;
        QSet<SequenceName> input;
    };

    QHash<int, ID> contents;

    virtual void filterUnit(const SequenceName &unit, int targetID)
    {
        contents[targetID].input.remove(unit);
        contents[targetID].filter.insert(unit);
    }

    virtual void inputUnit(const SequenceName &unit, int targetID)
    {
        if (contents[targetID].filter.contains(unit))
            return;
        contents[targetID].input.insert(unit);
    }

    virtual bool isHandled(const SequenceName &unit)
    {
        for (QHash<int, ID>::const_iterator it = contents.constBegin();
                it != contents.constEnd();
                ++it) {
            if (it.value().filter.contains(unit))
                return true;
            if (it.value().input.contains(unit))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("calc_dewpoint"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("input-rh")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("input-t")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("input-td")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-rh")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-t")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-td")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("always")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("rh-suffix")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("t-suffix")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("td-suffix")));

        QVERIFY(options.excluded().value("output-rh").contains("rh-suffix"));
        QVERIFY(options.excluded().value("output-rh").contains("t-suffix"));
        QVERIFY(options.excluded().value("output-rh").contains("td-suffix"));
        QVERIFY(options.excluded().value("rh-suffix").contains("output-rh"));
        QVERIFY(options.excluded().value("rh-suffix").contains("output-t"));
        QVERIFY(options.excluded().value("rh-suffix").contains("output-td"));

        QVERIFY(options.excluded().value("output-t").contains("rh-suffix"));
        QVERIFY(options.excluded().value("output-t").contains("t-suffix"));
        QVERIFY(options.excluded().value("output-t").contains("td-suffix"));
        QVERIFY(options.excluded().value("t-suffix").contains("output-rh"));
        QVERIFY(options.excluded().value("t-suffix").contains("output-t"));
        QVERIFY(options.excluded().value("t-suffix").contains("output-td"));

        QVERIFY(options.excluded().value("output-td").contains("rh-suffix"));
        QVERIFY(options.excluded().value("output-td").contains("t-suffix"));
        QVERIFY(options.excluded().value("output-td").contains("td-suffix"));
        QVERIFY(options.excluded().value("td-suffix").contains("output-rh"));
        QVERIFY(options.excluded().value("td-suffix").contains("output-t"));
        QVERIFY(options.excluded().value("td-suffix").contains("output-td"));
    }

    void dynamicDefaults()
    {
        ComponentOptions options(component->getOptions());
        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        SegmentProcessingStage *filter2;
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "Q_A11"), &controller);
        QVERIFY(controller.contents.isEmpty());
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2 != NULL);
            filter2->unhandled(SequenceName("brw", "raw", "Q_A11"), &controller);
            QVERIFY(controller.contents.isEmpty());
            delete filter2;
        }

        SequenceName T_V11_1("brw", "raw", "T_V11");
        SequenceName U_V11_1("brw", "raw", "U_V11");
        SequenceName TD_V11_1("brw", "raw", "TD_V11");
        SequenceName T_V11_2("mlo", "raw", "T_V11");
        SequenceName U_V11_2("mlo", "raw", "U_V11");
        SequenceName TD_V11_2("mlo", "raw", "TD_V11");
        SequenceName T_V12_1("brw", "raw", "T_V12");
        SequenceName U_V12_1("brw", "raw", "U_V12");
        SequenceName TD_V12_1("brw", "raw", "TD_V12");
        SequenceName T_V13_1("brw", "raw", "T_V13");
        SequenceName U_V13_1("brw", "raw", "U_V13");
        SequenceName TD_V13_1("brw", "raw", "TD_V13");

        filter->unhandled(T_V11_1, &controller);
        filter->unhandled(U_V11_1, &controller);
        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << T_V11_1 << U_V11_1 << TD_V11_1,
                                QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int V11_1 = idL.at(0);

        filter->unhandled(SequenceName("brw", "raw", "Q_V11"), &controller);
        QCOMPARE(controller.contents.size(), 1);

        filter->unhandled(TD_V11_2, &controller);
        QCOMPARE(controller.contents.size(), 2);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << T_V11_2 << U_V11_2 << TD_V11_2,
                                QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int V11_2 = idL.at(0);

        filter->unhandled(U_V12_1, &controller);
        filter->unhandled(TD_V12_1, &controller);
        QCOMPARE(controller.contents.size(), 3);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << T_V12_1 << U_V12_1 << TD_V12_1,
                                QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int V12_1 = idL.at(0);

        filter->unhandled(T_V13_1, &controller);
        QCOMPARE(controller.contents.size(), 4);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << T_V13_1 << U_V13_1 << TD_V13_1,
                                QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int V13_1 = idL.at(0);

        data.setStart(15.0);
        data.setEnd(16.0);

        data.setValue(T_V11_1, Variant::Root(25.0));
        data.setValue(U_V11_1, Variant::Root(50.0));
        filter->process(V11_1, data);
        QCOMPARE(data.value(TD_V11_1).toDouble(), Dewpoint::dewpoint(25.0, 50.0));

        {
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
            }
            QVERIFY(filter2 != NULL);
            data.setValue(TD_V11_1, Variant::Root());
            data.setValue(T_V11_1, Variant::Root(25.0));
            data.setValue(U_V11_1, Variant::Root(50.0));
            filter->process(V11_1, data);
            QCOMPARE(data.value(TD_V11_1).toDouble(), Dewpoint::dewpoint(25.0, 50.0));
            delete filter2;
        }

        data.setValue(TD_V11_1, Variant::Root());
        data.setValue(T_V11_1, Variant::Root(20.0));
        data.setValue(U_V11_1, Variant::Root(40.0));
        filter->process(V11_1, data);
        QCOMPARE(data.value(TD_V11_1).toDouble(), Dewpoint::dewpoint(20.0, 40.0));

        data.setValue(T_V11_1, Variant::Root());
        data.setValue(TD_V11_1, Variant::Root(20.0));
        data.setValue(U_V11_1, Variant::Root(40.0));
        filter->process(V11_1, data);
        QCOMPARE(data.value(T_V11_1).toDouble(), Dewpoint::t(40.0, 20.0));

        data.setValue(TD_V11_2, Variant::Root());
        data.setValue(T_V11_2, Variant::Root(25.0));
        data.setValue(U_V11_2, Variant::Root(45.0));
        filter->process(V11_2, data);
        QCOMPARE(data.value(TD_V11_2).toDouble(), Dewpoint::dewpoint(25.0, 45.0));

        data.setValue(T_V12_1, Variant::Root());
        data.setValue(TD_V12_1, Variant::Root(20.0));
        data.setValue(U_V12_1, Variant::Root(40.0));
        filter->process(V12_1, data);
        QCOMPARE(data.value(T_V12_1).toDouble(), Dewpoint::t(40.0, 20.0));

        data.setValue(U_V13_1, Variant::Root());
        data.setValue(T_V13_1, Variant::Root(25.0));
        data.setValue(TD_V13_1, Variant::Root(20.0));
        filter->process(V13_1, data);
        QCOMPARE(data.value(U_V13_1).toDouble(), Dewpoint::rh(25.0, 20.0));

        filter->processMeta(V11_1, data);
        QCOMPARE(data.value(U_V11_1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data.value(T_V11_1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data.value(TD_V11_1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);

        delete filter;
    }

    void dynamicOptions()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<DynamicInputOption *>(options.get("input-rh"))->set(45.0);
        qobject_cast<DynamicInputOption *>(options.get("input-t"))->set(
                SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("T_V.1")),
                Calibration());
        qobject_cast<ComponentOptionBoolean *>(options.get("always"))->set(true);
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-td"))->set("", "",
                                                                                      "TD_V.1");

        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "T_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "TD_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "U_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "U_V11"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName U_V11("brw", "raw", "U_V11");
        SequenceName T_V11("brw", "raw", "T_V11");
        SequenceName TD_V11("brw", "raw", "TD_V11");
        filter->unhandled(T_V11, &controller);
        filter->unhandled(TD_V11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << TD_V11,
                                                 QSet<SequenceName>() << T_V11));
        QCOMPARE(idL.size(), 1);
        int id = idL.at(0);

        data.setValue(T_V11, Variant::Root(5.0));
        data.setValue(TD_V11, Variant::Root(2.0));
        filter->process(id, data);
        QCOMPARE(data.value(TD_V11).toDouble(), Dewpoint::dewpoint(5.0, 45.0));

        delete filter;


        options = component->getOptions();
        qobject_cast<DynamicInputOption *>(options.get("input-rh"))->set(45.0);
        qobject_cast<DynamicInputOption *>(options.get("input-td"))->set(30.0);
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-t"))->set("", "",
                                                                                     "T_V.1");

        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        filter->unhandled(SequenceName("brw", "raw", "TD_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "U_V12"), &controller);
        QVERIFY(controller.contents.isEmpty());

        filter->unhandled(T_V11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << T_V11,
                                                 QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        data.setValue(TD_V11, Variant::Root(35.0));
        data.setValue(U_V11, Variant::Root());
        data.setValue(T_V11, Variant::Root());
        filter->process(id, data);
        QCOMPARE(data.value(T_V11).toDouble(), Dewpoint::t(45.0, 30.0));

        delete filter;


        options = component->getOptions();
        qobject_cast<DynamicInputOption *>(options.get("input-t"))->set(45.0);
        qobject_cast<DynamicInputOption *>(options.get("input-td"))->set(
                SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("TD_V.1")),
                Calibration());
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("output-rh"))->set("", "",
                                                                                      "U_V.1");

        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        filter->unhandled(SequenceName("brw", "raw", "U_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "TD_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "T_V12"), &controller);
        QVERIFY(controller.contents.isEmpty());

        filter->unhandled(U_V11, &controller);
        filter->unhandled(TD_V11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << U_V11,
                                                 QSet<SequenceName>() << TD_V11));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        data.setValue(TD_V11, Variant::Root(21.0));
        data.setValue(U_V11, Variant::Root());
        data.setValue(T_V11, Variant::Root());
        filter->process(id, data);
        QCOMPARE(data.value(U_V11).toDouble(), Dewpoint::rh(45.0, 21.0));

        delete filter;


        options = component->getOptions();
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("t-suffix"))->add("V11");

        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        filter->unhandled(SequenceName("brw", "raw", "U_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "TD_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "T_V12"), &controller);
        QVERIFY(controller.contents.isEmpty());

        filter->unhandled(U_V11, &controller);
        filter->unhandled(TD_V11, &controller);
        filter->unhandled(T_V11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << T_V11,
                                                 QSet<SequenceName>() << TD_V11 << U_V11));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        data.setValue(TD_V11, Variant::Root(21.0));
        data.setValue(U_V11, Variant::Root(50.0));
        data.setValue(T_V11, Variant::Root());
        filter->process(id, data);
        QCOMPARE(data.value(T_V11).toDouble(), Dewpoint::t(50.0, 21.0));

        delete filter;


        options = component->getOptions();
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("td-suffix"))->add("V11");

        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        filter->unhandled(SequenceName("brw", "raw", "U_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "TD_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "T_V12"), &controller);
        QVERIFY(controller.contents.isEmpty());

        filter->unhandled(U_V11, &controller);
        filter->unhandled(TD_V11, &controller);
        filter->unhandled(T_V11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << TD_V11,
                                                 QSet<SequenceName>() << T_V11 << U_V11));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        data.setValue(TD_V11, Variant::Root());
        data.setValue(U_V11, Variant::Root(50.0));
        data.setValue(T_V11, Variant::Root(25.0));
        filter->process(id, data);
        QCOMPARE(data.value(TD_V11).toDouble(), Dewpoint::dewpoint(25.0, 50.0));

        delete filter;


        options = component->getOptions();
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("rh-suffix"))->add("V11");

        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        filter->unhandled(SequenceName("brw", "raw", "U_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "TD_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "T_V12"), &controller);
        QVERIFY(controller.contents.isEmpty());

        filter->unhandled(U_V11, &controller);
        filter->unhandled(TD_V11, &controller);
        filter->unhandled(T_V11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << U_V11,
                                                 QSet<SequenceName>() << T_V11 << TD_V11));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        data.setValue(TD_V11, Variant::Root(35.0));
        data.setValue(U_V11, Variant::Root());
        data.setValue(T_V11, Variant::Root(45.0));
        filter->process(id, data);
        QCOMPARE(data.value(U_V11).toDouble(), Dewpoint::rh(45.0, 35.0));

        delete filter;
    }

    void predefined()
    {
        ComponentOptions options(component->getOptions());

        SequenceName T_A11("brw", "raw", "T_A11");
        SequenceName U_A11("brw", "raw", "U_A11");
        SequenceName TD_A11("brw", "raw", "TD_A11");
        QList<SequenceName> input;
        input << T_A11;

        SegmentProcessingStage *filter =
                component->createBasicFilterPredefined(options, FP::undefined(), FP::undefined(),
                                                       input);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{T_A11, U_A11, TD_A11}));
        QCOMPARE(filter->predictedOutputs(), (SequenceName::Set{T_A11, U_A11, TD_A11}));

        delete filter;
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["V11/CalculateRH"] = "::U_V11:=";
        cv["V11/CalculateTemperature"] = "::T_V11:=";
        cv["V11/CalculateDewpoint"] = "::TD_V11:=";
        cv["V11/InputRH"] = "::U_V11:=";
        cv["V11/InputTemperature"] = "::T_V11:=";
        cv["V11/InputDewpoint"] = "::TD_V11:=";
        config.emplace_back(FP::undefined(), 13.0, cv);
        cv.write().setEmpty();
        cv["V11/CalculateDewpoint"] = "::TD_V11:=";
        cv["V11/InputRH"] = "::U_V11:=";
        cv["V11/InputTemperature"] = "::T_V11:=";
        cv["V11/InputDewpoint"] = "::TD_V11:=";
        cv["V11/AlwaysDewpoint"] = true;
        config.emplace_back(13.0, FP::undefined(), cv);

        SegmentProcessingStage
                *filter = component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config);
        QVERIFY(filter != NULL);
        TestController controller;

        SequenceName U_V11("brw", "raw", "U_V11");
        SequenceName T_V11("brw", "raw", "T_V11");
        SequenceName TD_V11("brw", "raw", "TD_V11");

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{T_V11, U_V11, TD_V11}));

        filter->unhandled(T_V11, &controller);
        filter->unhandled(U_V11, &controller);
        filter->unhandled(TD_V11, &controller);

        QList<int> idL = controller.contents
                                   .keys(TestController::ID(
                                           QSet<SequenceName>() << T_V11 << U_V11 << TD_V11,
                                           QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int V11 = idL.at(0);

        QCOMPARE(filter->metadataBreaks(V11), QSet<double>() << 13.0);

        SequenceSegment data1;
        data1.setStart(12.0);
        data1.setEnd(13.0);

        data1.setValue(T_V11, Variant::Root(10.0));
        data1.setValue(U_V11, Variant::Root(20.0));
        filter->process(V11, data1);
        QCOMPARE(data1.value(TD_V11).toDouble(), Dewpoint::dewpoint(10.0, 20.0));

        data1.setValue(TD_V11, Variant::Root(5.0));
        data1.setValue(T_V11, Variant::Root(10.0));
        data1.setValue(U_V11, Variant::Root(20.0));
        filter->process(V11, data1);
        QCOMPARE(data1.value(TD_V11).toDouble(), 5.0);

        data1.setValue(TD_V11, Variant::Root(5.0));
        data1.setValue(T_V11, Variant::Root());
        data1.setValue(U_V11, Variant::Root(20.0));
        filter->process(V11, data1);
        QCOMPARE(data1.value(T_V11).toDouble(), Dewpoint::t(20.0, 5.0));

        data1.setValue(TD_V11, Variant::Root(5.0));
        data1.setValue(T_V11, Variant::Root(10.0));
        data1.setValue(U_V11, Variant::Root(20.0));
        filter->process(V11, data1);
        QCOMPARE(data1.value(T_V11).toDouble(), 10.0);

        data1.setValue(TD_V11, Variant::Root(5.0));
        data1.setValue(T_V11, Variant::Root(10.0));
        data1.setValue(U_V11, Variant::Root());
        filter->process(V11, data1);
        QCOMPARE(data1.value(U_V11).toDouble(), Dewpoint::rh(10.0, 5.0));

        data1.setValue(TD_V11, Variant::Root(5.0));
        data1.setValue(T_V11, Variant::Root(10.0));
        data1.setValue(U_V11, Variant::Root(50.0));
        filter->process(V11, data1);
        QCOMPARE(data1.value(U_V11).toDouble(), 50.0);


        filter->processMeta(V11, data1);
        QCOMPARE(data1.value(T_V11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data1.value(U_V11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data1.value(TD_V11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);


        SequenceSegment data2;
        data2.setStart(13.0);
        data2.setEnd(15.0);

        data2.setValue(T_V11, Variant::Root(10.0));
        data2.setValue(U_V11, Variant::Root(20.0));
        filter->process(V11, data2);
        QCOMPARE(data2.value(TD_V11).toDouble(), Dewpoint::dewpoint(10.0, 20.0));

        data2.setValue(TD_V11, Variant::Root(5.0));
        data2.setValue(T_V11, Variant::Root(10.0));
        data2.setValue(U_V11, Variant::Root(20.0));
        filter->process(V11, data2);
        QCOMPARE(data2.value(TD_V11).toDouble(), Dewpoint::dewpoint(10.0, 20.0));

        data2.setValue(TD_V11, Variant::Root(5.0));
        data2.setValue(T_V11, Variant::Root());
        data2.setValue(U_V11, Variant::Root(20.0));
        filter->process(V11, data2);
        QVERIFY(!data2.value(T_V11).exists());

        data2.setValue(TD_V11, Variant::Root(5.0));
        data2.setValue(T_V11, Variant::Root(10.0));
        data2.setValue(U_V11, Variant::Root(20.0));
        filter->process(V11, data2);
        QCOMPARE(data2.value(T_V11).toDouble(), 10.0);

        data2.setValue(TD_V11, Variant::Root(5.0));
        data2.setValue(T_V11, Variant::Root(10.0));
        data2.setValue(U_V11, Variant::Root());
        filter->process(V11, data2);
        QVERIFY(!data2.value(U_V11).exists());

        data2.setValue(TD_V11, Variant::Root(5.0));
        data2.setValue(T_V11, Variant::Root(10.0));
        data2.setValue(U_V11, Variant::Root(50.0));
        filter->process(V11, data2);
        QCOMPARE(data2.value(U_V11).toDouble(), 50.0);


        filter->processMeta(V11, data2);
        QVERIFY(!data2.value(T_V11.toMeta()).exists());
        QVERIFY(!data2.value(U_V11.toMeta()).exists());
        QCOMPARE(data2.value(TD_V11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);

        delete filter;
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
