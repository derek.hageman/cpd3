/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/processingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestComponent : public QObject {
Q_OBJECT

    ProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<ProcessingStageComponent *>(
                ComponentLoader::create("corr_removeundefined"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("require")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("remove")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("undefine")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")));
    }

    void basic()
    {
        ComponentOptions options;
        options = component->getOptions();

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName Ba("brw", "raw", "Ba_A11");
        SequenceName Ir("brw", "raw", "Ir_A11");
        SequenceName Qt("brw", "raw", "Qt_A11");
        filter->incomingData(SequenceValue(Qt, Variant::Root(1.0), 1000.0, 5000.0));
        filter->incomingData(SequenceValue(Ba, Variant::Root(1.0), 1100.0, 1200.0));
        filter->incomingData(SequenceValue(Ir, Variant::Root(1.0), 1100.0, 1200.0));
        filter->incomingData(SequenceValue(Ba, Variant::Root(FP::undefined()), 1200.0, 1300.0));
        filter->incomingData(SequenceValue(Ir, Variant::Root(1.0), 1200.0, 1300.0));
        filter->incomingData(SequenceValue(Ir, Variant::Root(1.0), 1300.0, 1400.0));
        filter->incomingData(SequenceValue(Ba, Variant::Root(FP::undefined()), 1300.0, 1400.0));
        filter->incomingData(SequenceValue(Ir, Variant::Root(1.0), 1400.0, 1500.0));
        filter->incomingData(SequenceValue(Ba, Variant::Root(1.0), 1400.0, 1500.0));
        filter->incomingData(SequenceValue(Ir, Variant::Root(1.0), 1500.0, 1600.0));
        filter->incomingData(SequenceValue(Ba, Variant::Root(FP::undefined()), 2000.0, 3000.0));
        filter->incomingData(SequenceValue(Qt, Variant::Root(1.0), 2000.0, 3000.0));
        Variant::Root meta;
        meta.write().metadataReal("Foo") = "Bar";
        meta.write().metadataReal("Processing").setType(Variant::Type::Array);
        filter->incomingData(SequenceValue(Qt.toMeta(), meta, 5000.0, 6000.0));
        filter->incomingData(SequenceValue(Ba.toMeta(), meta, 5000.0, 6000.0));
        filter->incomingData(SequenceValue(Ir.toMeta(), meta, 5000.0, 6000.0));
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 13);
        QCOMPARE(e.values()[0], SequenceValue(Qt, Variant::Root(1.0), 1000.0, 5000.0));
        QCOMPARE(e.values()[1], SequenceValue(Ba, Variant::Root(1.0), 1100.0, 1200.0));
        QCOMPARE(e.values()[2], SequenceValue(Ir, Variant::Root(1.0), 1100.0, 1200.0));
        QCOMPARE(e.values()[3], SequenceValue(Ba, Variant::Root(FP::undefined()), 1200.0, 1300.0));
        QCOMPARE(e.values()[4], SequenceValue(Ba, Variant::Root(FP::undefined()), 1300.0, 1400.0));
        QCOMPARE(e.values()[5], SequenceValue(Ir, Variant::Root(1.0), 1400.0, 1500.0));
        QCOMPARE(e.values()[6], SequenceValue(Ba, Variant::Root(1.0), 1400.0, 1500.0));
        QCOMPARE(e.values()[7], SequenceValue(Ir, Variant::Root(1.0), 1500.0, 1600.0));
        QCOMPARE(e.values()[8], SequenceValue(Ba, Variant::Root(FP::undefined()), 2000.0, 3000.0));
        QCOMPARE(e.values()[9], SequenceValue(Qt, Variant::Root(1.0), 2000.0, 3000.0));

        QCOMPARE(e.values()[10], SequenceValue(Qt.toMeta(), meta, 5000.0, 6000.0));
        QCOMPARE(e.values()[11], SequenceValue(Ba.toMeta(), meta, 5000.0, 6000.0));
        QCOMPARE(e.values()[12].getIdentity(), SequenceIdentity(Ir.toMeta(), 5000.0, 6000.0));
        QCOMPARE(e.values()[12].getValue().metadata("Processing").getType(), Variant::Type::Array);
        QCOMPARE((int) e.values()[12].getValue().metadata("Processing").toArray().size(), 1);
    }

    void editing()
    {
        Variant::Root config;
        config["A11/Require"] = "::Ba_A11:=";
        config["A11/Remove"] = "::Ir_A11:=";
        config["A11/Undefine"] = "::L_A11:=";

        ProcessingStage *filter =
                component->createGeneralFilterEditing(FP::undefined(), FP::undefined(), "brw",
                                                      "raw", ValueSegment::Transfer{
                                ValueSegment(FP::undefined(), FP::undefined(), config)});
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName Ba("brw", "raw", "Ba_A11");
        SequenceName Ir("brw", "raw", "Ir_A11");
        SequenceName Qt("brw", "raw", "Qt_A11");
        SequenceName L("brw", "raw", "L_A11");

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{Ba}));

        filter->incomingData(SequenceValue(Qt, Variant::Root(1.0), 1000.0, 5000.0));
        filter->incomingData(SequenceValue(Ba, Variant::Root(1.0), 1100.0, 1200.0));
        filter->incomingData(SequenceValue(Ir, Variant::Root(1.0), 1100.0, 1200.0));
        filter->incomingData(SequenceValue(L, Variant::Root(1.0), 1100.0, 1200.0));
        filter->incomingData(SequenceValue(Ba, Variant::Root(FP::undefined()), 1200.0, 1300.0));
        filter->incomingData(SequenceValue(Ir, Variant::Root(1.0), 1200.0, 1300.0));
        filter->incomingData(SequenceValue(L, Variant::Root(1.0), 1200.0, 1300.0));
        filter->incomingData(SequenceValue(Ir, Variant::Root(1.0), 1300.0, 1400.0));
        filter->incomingData(SequenceValue(Ba, Variant::Root(FP::undefined()), 1300.0, 1400.0));
        filter->incomingData(SequenceValue(Ir, Variant::Root(1.0), 1400.0, 1500.0));
        filter->incomingData(SequenceValue(Ba, Variant::Root(1.0), 1400.0, 1500.0));
        filter->incomingData(SequenceValue(Ir, Variant::Root(1.0), 1500.0, 1600.0));
        filter->incomingData(SequenceValue(Ba, Variant::Root(FP::undefined()), 2000.0, 3000.0));
        filter->incomingData(SequenceValue(Qt, Variant::Root(1.0), 2000.0, 3000.0));
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 12);
        QCOMPARE(e.values()[0], SequenceValue(Qt, Variant::Root(1.0), 1000.0, 5000.0));
        QCOMPARE(e.values()[1], SequenceValue(Ba, Variant::Root(1.0), 1100.0, 1200.0));
        QCOMPARE(e.values()[2], SequenceValue(Ir, Variant::Root(1.0), 1100.0, 1200.0));
        QCOMPARE(e.values()[3], SequenceValue(L, Variant::Root(1.0), 1100.0, 1200.0));
        QVERIFY(FP::defined(e.values()[3].getValue().toReal()));
        QCOMPARE(e.values()[4], SequenceValue(Ba, Variant::Root(FP::undefined()), 1200.0, 1300.0));
        QCOMPARE(e.values()[5], SequenceValue(L, Variant::Root(FP::undefined()), 1200.0, 1300.0));
        QVERIFY(!FP::defined(e.values()[5].getValue().toReal()));
        QCOMPARE(e.values()[6], SequenceValue(Ba, Variant::Root(FP::undefined()), 1300.0, 1400.0));
        QCOMPARE(e.values()[7], SequenceValue(Ir, Variant::Root(1.0), 1400.0, 1500.0));
        QCOMPARE(e.values()[8], SequenceValue(Ba, Variant::Root(1.0), 1400.0, 1500.0));
        QCOMPARE(e.values()[9], SequenceValue(Ir, Variant::Root(1.0), 1500.0, 1600.0));
        QCOMPARE(e.values()[10], SequenceValue(Ba, Variant::Root(FP::undefined()), 2000.0, 3000.0));
        QCOMPARE(e.values()[11], SequenceValue(Qt, Variant::Root(1.0), 2000.0, 3000.0));
    }


    void serialize()
    {
        ComponentOptions options;
        options = component->getOptions();

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName Ba("brw", "raw", "Ba_A11");
        SequenceName Ir("brw", "raw", "Ir_A11");
        SequenceName Qt("brw", "raw", "Qt_A11");
        for (int i = 0; i < 10000; i++) {
            filter->incomingData(SequenceValue(Qt, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i));
            filter->incomingData(SequenceValue(Ba, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i));
            filter->incomingData(SequenceValue(Ir, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i));
        }
        filter->setEgress(NULL);

        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            filter->serialize(stream);
        }
        filter->signalTerminate();
        QVERIFY(filter->wait());
        delete filter;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            filter = component->deserializeGeneralFilter(stream);
            QVERIFY(stream.atEnd());
        }
        QVERIFY(filter != NULL);
        filter->start();
        filter->setEgress(&e);

        for (int i = 10000; i < 20000; i++) {
            filter->incomingData(SequenceValue(Qt, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i));
            filter->incomingData(SequenceValue(Ba, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i));
            filter->incomingData(SequenceValue(Ir, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i));
        }
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 60000);
        for (int i = 0; i < 20000; i++) {
            QCOMPARE(e.values()[i * 3],
                     SequenceValue(Qt, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i));
            QCOMPARE(e.values()[i * 3 + 1],
                     SequenceValue(Ba, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i));
            QCOMPARE(e.values()[i * 3 + 2],
                     SequenceValue(Ir, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i));
        }
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
