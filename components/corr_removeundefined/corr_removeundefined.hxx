/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CORRREMOVEUNDEFINED_H
#define CORRREMOVEUNDEFINED_H

#include "core/first.hxx"

#include <vector>
#include <deque>
#include <unordered_map>
#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamicsequenceselection.hxx"

class CorrRemoveUndefined : public CPD3::Data::AsyncProcessingStage {
    class Processing {
    public:
        CPD3::Data::DynamicSequenceSelection *require;
        CPD3::Data::DynamicSequenceSelection *remove;
        CPD3::Data::DynamicSequenceSelection *undefine;

        std::unordered_map<CPD3::Data::SequenceName, std::deque<CPD3::Time::Bounds>> seenUndefined;

        Processing() : require(NULL), remove(NULL), undefine(NULL)
        { }

        bool haveUndefined(double start, double end) const;
    };

    std::deque<CPD3::Data::SequenceValue> pending;

    std::unordered_map<CPD3::Data::SequenceName, std::vector<std::reference_wrapper<Processing>>>
            inputDispatch;
    std::unordered_map<CPD3::Data::SequenceName, std::vector<std::reference_wrapper<Processing>>>
            outputDispatch;

    std::vector<Processing> processing;

    std::vector<std::reference_wrapper<Processing>> &getInput(const CPD3::Data::SequenceName &unit);

    std::vector<
            std::reference_wrapper<Processing>> &getOutput(const CPD3::Data::SequenceName &unit);

    void flushPending(double advanceTime = CPD3::FP::undefined());

public:
    CorrRemoveUndefined(const CPD3::ComponentOptions &options);

    CorrRemoveUndefined(double start,
                        double end,
                        const CPD3::Data::SequenceName::Component &station,
                        const CPD3::Data::SequenceName::Component &archive,
                        const CPD3::Data::ValueSegment::Transfer &config);

    CorrRemoveUndefined(QDataStream &stream);

    ~CorrRemoveUndefined();

    CPD3::Data::SequenceName::Set requestedInputs() override;

    void serialize(QDataStream &stream) override;

protected:
    void process(CPD3::Data::SequenceValue::Transfer &&incoming) override;

    void finish() override;
};

class CorrRemoveUndefinedComponent : public QObject, public CPD3::Data::ProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.corr_removeundefined"
                              FILE
                              "corr_removeundefined.json")

public:
    virtual QString getGeneralSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();


    virtual CPD3::Data::ProcessingStage *createGeneralFilterDynamic(const CPD3::ComponentOptions &options = CPD3::ComponentOptions());

    virtual CPD3::Data::ProcessingStage *createGeneralFilterEditing(double start,
                                                                    double end,
                                                                    const CPD3::Data::SequenceName::Component &station,
                                                                    const CPD3::Data::SequenceName::Component &archive,
                                                                    const CPD3::Data::ValueSegment::Transfer &config);

    virtual CPD3::Data::ProcessingStage *deserializeGeneralFilter(QDataStream &stream);

};

#endif
