/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/variant/composite.hxx"
#include "core/environment.hxx"

#include "corr_removeundefined.hxx"

using namespace CPD3;
using namespace CPD3::Data;


CorrRemoveUndefined::CorrRemoveUndefined(const ComponentOptions &options)
{
    if (options.isSet("suffix")) {
        QSet<QString> suffixes
                (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->get());
        for (QSet<QString>::const_iterator suffix = suffixes.constBegin(),
                endSuffixes = suffixes.constEnd(); suffix != endSuffixes; ++suffix) {
            Processing p;

            p.require = new DynamicSequenceSelection::Match(QString(), QString(),
                                                            "Ba[A-Z0-9]*_" + *suffix);
            p.remove = new DynamicSequenceSelection::Match(QString(), QString(),
                                                           "(?:(?:Ir[A-Z0-9]*)|(?:L))_" + *suffix);
            p.undefine = new DynamicSequenceSelection::None;

            processing.push_back(p);
        }
    } else if (options.isSet("require") || options.isSet("remove") || options.isSet("undefine")) {
        Processing p;

        if (options.isSet("require")) {
            p.require = qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("require"))->getOperator();
        } else {
            p.require = new DynamicSequenceSelection::Match(QString(), QString(), "Ba[A-Z0-9]*_.+");
        }

        if (options.isSet("remove")) {
            p.remove = qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("remove"))->getOperator();
        } else {
            p.remove = new DynamicSequenceSelection::Match(QString(), QString(),
                                                           "(?:(?:Ir[A-Z0-9]*)|(?:L))_.+");
        }

        if (options.isSet("undefine")) {
            p.undefine = qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("undefine"))->getOperator();
        } else {
            p.undefine = new DynamicSequenceSelection::None;
        }

        processing.push_back(p);
    } else {
        Processing p;

        p.require = new DynamicSequenceSelection::Match(QString(), QString(), "Ba[A-Z0-9]*_.+");
        p.remove = new DynamicSequenceSelection::Match(QString(), QString(),
                                                       "(?:(?:Ir[A-Z0-9]*)|(?:L))_.+");
        p.undefine = new DynamicSequenceSelection::None;

        processing.push_back(p);
    }
}

CorrRemoveUndefined::CorrRemoveUndefined(double start,
                                         double end,
                                         const SequenceName::Component &station,
                                         const SequenceName::Component &archive,
                                         const ValueSegment::Transfer &config)
{
    Q_UNUSED(station);
    Q_UNUSED(archive);

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    for (const auto &child : children) {
        Processing p;

        p.require = DynamicSequenceSelection::fromConfiguration(config, QString("%1/Require").arg(
                QString::fromStdString(child)), start, end);
        p.remove = DynamicSequenceSelection::fromConfiguration(config, QString("%1/Remove").arg(
                QString::fromStdString(child)), start, end);
        p.undefine = DynamicSequenceSelection::fromConfiguration(config, QString("%1/Undefine").arg(
                QString::fromStdString(child)), start, end);

        p.require->registerExpected(station, archive);
        p.remove->registerExpected(station, archive);
        p.undefine->registerExpected(station, archive);

        processing.push_back(p);
    }
}

CorrRemoveUndefined::~CorrRemoveUndefined()
{
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        delete p->require;
        delete p->remove;
        delete p->undefine;
    }
}

CorrRemoveUndefined::CorrRemoveUndefined(QDataStream &stream) : AsyncProcessingStage(stream)
{
    stream >> pending;

    quint32 n = 0;
    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        Processing p;
        stream >> p.require;
        stream >> p.remove;
        stream >> p.undefine;
        stream >> p.seenUndefined;

        processing.push_back(p);
    }
}

void CorrRemoveUndefined::serialize(QDataStream &stream)
{
    AsyncProcessingStage::serialize(stream);

    stream << pending;

    quint32 n = processing.size();
    stream << n;
    for (int i = 0; i < (int) n; i++) {
        stream << processing[i].require;
        stream << processing[i].remove;
        stream << processing[i].undefine;
        stream << processing[i].seenUndefined;
    }
}

std::vector<std::reference_wrapper<CorrRemoveUndefined::Processing>> &CorrRemoveUndefined::getInput(
        const SequenceName &unit)
{
    auto check = inputDispatch.find(unit);
    if (check == inputDispatch.end()) {
        std::vector<std::reference_wrapper<Processing>> targets;
        if (!unit.isMeta()) {
            for (auto &p : processing) {
                bool hit = false;
                if (p.require->registerInput(unit))
                    hit = true;

                if (hit)
                    targets.emplace_back(p);
            }
        }

        check = inputDispatch.emplace(unit, std::move(targets)).first;
    }

    return check->second;
}

std::vector<
        std::reference_wrapper<CorrRemoveUndefined::Processing>> &CorrRemoveUndefined::getOutput(
        const SequenceName &unit)
{
    auto check = outputDispatch.find(unit);
    if (check == outputDispatch.end()) {
        std::vector<std::reference_wrapper<Processing>> targets;
        if (!unit.isMeta()) {
            for (auto &p : processing) {
                bool hit = false;
                if (p.undefine->registerInput(unit))
                    hit = true;
                if (p.remove->registerInput(unit))
                    hit = true;

                if (hit)
                    targets.emplace_back(p);
            }
        }

        check = outputDispatch.emplace(unit, std::move(targets)).first;
    }

    return check->second;
}

bool CorrRemoveUndefined::Processing::haveUndefined(double start, double end) const
{
    for (const auto &type : seenUndefined) {
        for (const auto &seg : type.second) {
            if (Range::intersects(seg.getStart(), seg.getEnd(), start, end))
                return true;
        }
    }
    return false;
}

void CorrRemoveUndefined::flushPending(double advanceTime)
{
    bool doLimit = FP::defined(advanceTime);
    while (!pending.empty()) {
        SequenceValue &value = pending.front();

        auto &d = getOutput(value.getUnit());
        /* Not our concern, so pass it through */
        if (d.empty()) {
            egress->incomingData(std::move(value));
            pending.pop_front();
            continue;
        }

        if (doLimit) {
            /* Wait for some time advance, regardless */
            if (FP::equal(pending.front().getStart(), advanceTime))
                break;

            /* Otherwise, wait until we've seen enough to complete the value, unless it has
             * no end time (would stall forever, so we work with what we have once we've
             * got any advance time at all). */
            if (FP::defined(pending.front().getEnd()) && pending.front().getEnd() > advanceTime) {
                /* However, just give up and work with it as is, if the pending gets huge */
                if (pending.size() < StreamSink::stallThreshold * 16) {
                    break;
                }
            }
        }

        bool doOutput = true;
        for (const auto &p : d) {
            if (!p.get().haveUndefined(value.getStart(), value.getEnd()))
                continue;

            if (p.get().remove->get(value).count(value.getUnit()) != 0) {
                doOutput = false;
                break;
            }

            if (p.get().undefine->get(value).count(value.getUnit()) != 0) {
                Variant::Composite::invalidate(value.write());
            }
        }

        if (doOutput)
            egress->incomingData(std::move(value));

        pending.pop_front();
    }

    /* Trim the undefined end trackers, since they do not need to retain anything with end
     * times before our possible emit.  If we have no more pending emits, then we
     * can trim them to how far we've advanced(all values will start after that time). */
    double limitTime = advanceTime;
    if (!pending.empty())
        limitTime = pending.front().getStart();
    if (FP::defined(limitTime)) {
        for (auto &p : processing) {
            for (auto type = p.seenUndefined.begin(); type != p.seenUndefined.end();) {
                auto &segs = type->second;
                while (!segs.empty()) {
                    auto &s = segs.front();
                    if (!FP::defined(s.getEnd()))
                        break;
                    if (s.getEnd() > limitTime)
                        break;
                    segs.pop_front();
                }

                if (type->second.empty()) {
                    type = p.seenUndefined.erase(type);
                    continue;
                }
                ++type;
            }
        }
    }
}

void CorrRemoveUndefined::finish()
{
    flushPending();
    egress->endData();
}

void CorrRemoveUndefined::process(SequenceValue::Transfer &&incoming)
{
    if (incoming.empty())
        return;

    double advanceLimit = incoming.back().getStart();

    for (auto &value : incoming) {
        if (value.getUnit().isMeta() && value.read().isMetadata()) {
            auto noMeta = value.getUnit().fromMeta();
            auto &d = getOutput(noMeta);

            if (d.empty() || !value.read().metadata("Processing").exists()) {
                pending.emplace_back(std::move(value));
                continue;
            }

            Variant::Root meta(Variant::Type::Hash);

            bool hit = false;
            for (const auto &p : d) {
                if (p.get().remove->get(value).count(noMeta) != 0) {
                    meta["Parameters"].hash("Remove").setBool(true);
                    hit = true;
                } else if (p.get().undefine->get(value).count(noMeta) != 0) {
                    meta["Parameters"].hash("Undefine").setBool(true);
                    hit = true;
                }
            }
            if (!hit) {
                pending.emplace_back(std::move(value));
                continue;
            }

            meta["By"].setString("corr_removeundefined");
            meta["At"].setDouble(Time::time());
            meta["Environment"].setString(Environment::describe());
            meta["Revision"].setString(Environment::revision());

            SequenceValue mod = value;
            mod.write().metadata("Processing").toArray().after_back().set(meta);
            pending.emplace_back(std::move(mod));
            continue;
        }

        for (const auto &p : getInput(value.getUnit())) {
            if (!p.get().require->get(value).count(value.getUnit()))
                continue;

            auto &target = p.get().seenUndefined[value.getUnit()];
            if (Variant::Composite::isDefined(value.read())) {
                /* Terminate the existing, if any */
                if (!FP::defined(value.getStart())) {
                    target.clear();
                } else if (!target.empty()) {
                    if (!FP::defined(target.back().getEnd()) ||
                            target.back().getEnd() > value.getStart()) {
                        target.back().setEnd(value.getStart());
                    }
                }
                continue;
            }

            if (!target.empty()) {
                /* Already covered */
                if (Range::compareEnd(value.getEnd(), target.back().getEnd()) <= 0)
                    continue;
                /* Extend the existing */
                if (Range::compareStartEnd(value.getStart(), target.back().getEnd()) <= 0) {
                    target.back().setEnd(value.getEnd());
                    continue;
                }
            }

            target.emplace_back(value.getStart(), value.getEnd());
        }
        pending.emplace_back(std::move(value));
    }

    if (FP::defined(advanceLimit)) {
        flushPending(advanceLimit);
    }
}

SequenceName::Set CorrRemoveUndefined::requestedInputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        Util::merge(p->require->getAllUnits(), out);
    }
    return out;
}

QString CorrRemoveUndefinedComponent::getGeneralSerializationName() const
{ return QString::fromLatin1("corr_removeundefined"); }

ComponentOptions CorrRemoveUndefinedComponent::getOptions()
{
    ComponentOptions options;

    options.add("require", new DynamicSequenceSelectionOption(tr("require", "name"),
                                                              tr("Variables required to be defined"),
                                                              tr("These are the variables to that must be defined (or absent "
                                                                         "entirely) for the remove variables to pass through."),
                                                              tr("Absorptions")));

    options.add("remove",
                new DynamicSequenceSelectionOption(tr("remove", "name"), tr("Variables to remove"),
                                                   tr("These are the variables that are removed when the required "
                                                              "variables are not defined."),
                                                   tr("Transmittance and length")));

    options.add("undefine", new DynamicSequenceSelectionOption(tr("undefine", "name"),
                                                               tr("Variables to undefine"),
                                                               tr("These are the variables that set to undefined when the required "
                                                                          "variables are not defined."),
                                                               QString()));

    options.add("suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments", "name"),
                                                                 tr("Instrument suffixes to correct"),
                                                                 tr("These are the instrument suffixes to correct.  "
                                                                            "For example S11 would usually specifies the reference nephelometer.  "
                                                                            "This option is mutually exclusive with manual variable specification."),
                                                                 tr("All instrument suffixes")));

    options.exclude("require", "suffix");
    options.exclude("remove", "suffix");
    options.exclude("undefine", "suffix");
    options.exclude("suffix", "require");
    options.exclude("suffix", "remove");
    options.exclude("suffix", "undefine");

    return options;
}

QList<ComponentExample> CorrRemoveUndefinedComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will remove transmittance and length whenever the absorption "
                                                "is not defined.")));

    options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")))->add("A11");
    examples.append(ComponentExample(options, tr("Single instrument"),
                                     tr("This will only remove transmittance and lengths from the A11 "
                                                "instrument whenever any absorption from it is not defined.")));

    return examples;
}

ProcessingStage *CorrRemoveUndefinedComponent::createGeneralFilterDynamic(const ComponentOptions &options)
{ return new CorrRemoveUndefined(options); }

ProcessingStage *CorrRemoveUndefinedComponent::createGeneralFilterEditing(double start,
                                                                          double end,
                                                                          const SequenceName::Component &station,
                                                                          const SequenceName::Component &archive,
                                                                          const ValueSegment::Transfer &config)
{ return new CorrRemoveUndefined(start, end, station, archive, config); }

ProcessingStage *CorrRemoveUndefinedComponent::deserializeGeneralFilter(QDataStream &stream)
{ return new CorrRemoveUndefined(stream); }
