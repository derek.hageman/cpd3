/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    class ID {
    public:
        ID() : filter(), input()
        { }

        ID(const QSet<SequenceName> &setFilter, const QSet<SequenceName> &setInput) : filter(
                setFilter), input(setInput)
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }

        QSet<SequenceName> filter;
        QSet<SequenceName> input;
    };

    QHash<int, ID> contents;

    virtual void filterUnit(const SequenceName &unit, int targetID)
    {
        contents[targetID].input.remove(unit);
        contents[targetID].filter.insert(unit);
    }

    virtual void inputUnit(const SequenceName &unit, int targetID)
    {
        if (contents[targetID].filter.contains(unit))
            return;
        contents[targetID].input.insert(unit);
    }

    virtual bool isHandled(const SequenceName &unit)
    {
        for (QHash<int, ID>::const_iterator it = contents.constBegin();
                it != contents.constEnd();
                ++it) {
            if (it.value().filter.contains(unit))
                return true;
            if (it.value().input.contains(unit))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("calc_integratesize"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionDoubleSet *>(options.get("bs")));
        QVERIFY(qobject_cast<ComponentOptionDoubleSet *>(options.get("bbs")));
        QVERIFY(qobject_cast<ComponentOptionDoubleSet *>(options.get("ba")));
        QVERIFY(qobject_cast<ComponentOptionDoubleSet *>(options.get("be")));
        QVERIFY(qobject_cast<ComponentOptionDoubleSet *>(options.get("ds")));
        QVERIFY(qobject_cast<ComponentOptionDoubleSet *>(options.get("g")));
        QVERIFY(qobject_cast<ComponentOptionDoubleSet *>(options.get("bsfm")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("n")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("nv")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("nm")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("nmv")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("dnn")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("dnb")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("dv")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("ri-r")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("ri-i")));
        QVERIFY(qobject_cast<ComponentOptionSingleInteger *>(options.get("n-angles")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("fine-diameter")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")));
    }

    void dynamicDefaults()
    {
        ComponentOptions options(component->getOptions());
        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        SegmentProcessingStage *filter2;
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "T1_N11"), &controller);
        QVERIFY(controller.contents.isEmpty());
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2 != NULL);
            filter2->unhandled(SequenceName("brw", "raw", "T1_N11"), &controller);
            QVERIFY(controller.contents.isEmpty());
            delete filter2;
        }

        SequenceName Ns_N11("brw", "raw", "Ns_N11");
        SequenceName Nn_N11("brw", "raw", "Nn_N11");
        SequenceName Nb_N11("brw", "raw", "Nb_N11");
        SequenceName BsB_N11("brw", "raw", "BsB_N11");
        SequenceName BsG_N11("brw", "raw", "BsG_N11");
        SequenceName BsR_N11("brw", "raw", "BsR_N11");
        SequenceName BbsB_N11("brw", "raw", "BbsB_N11");
        SequenceName BbsG_N11("brw", "raw", "BbsG_N11");
        SequenceName BbsR_N11("brw", "raw", "BbsR_N11");
        SequenceName NnsG_N11("brw", "raw", "NnsG_N11");
        SequenceName N_N11("brw", "raw", "N_N11");
        SequenceName Nv_N11("brw", "raw", "Nv_N11");
        SequenceName Nm_N11("brw", "raw", "Nm_N11");
        SequenceName Ngm_N11("brw", "raw", "Ngm_N11");
        SequenceName Nmv_N11("brw", "raw", "Nmv_N11");
        SequenceName Nnv_N11("brw", "raw", "Nnv_N11");

        SequenceName Ns_N12("brw", "raw", "Ns_N12");
        SequenceName Nn_N12("brw", "raw", "Nn_N12");
        SequenceName Nb_N12("brw", "raw", "Nb_N12");
        SequenceName BsB_N12("brw", "raw", "BsB_N12");
        SequenceName BsG_N12("brw", "raw", "BsG_N12");
        SequenceName BsR_N12("brw", "raw", "BsR_N12");
        SequenceName BbsB_N12("brw", "raw", "BbsB_N12");
        SequenceName BbsG_N12("brw", "raw", "BbsG_N12");
        SequenceName BbsR_N12("brw", "raw", "BbsR_N12");
        SequenceName NnsG_N12("brw", "raw", "NnsG_N12");
        SequenceName N_N12("brw", "raw", "N_N12");
        SequenceName Nv_N12("brw", "raw", "Nv_N12");
        SequenceName Nm_N12("brw", "raw", "Nm_N12");
        SequenceName Ngm_N12("brw", "raw", "Ngm_N12");
        SequenceName Nmv_N12("brw", "raw", "Nmv_N12");
        SequenceName Nnv_N12("brw", "raw", "Nnv_N12");

        filter->unhandled(Ns_N11, &controller);
        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                               BsB_N11 <<
                                                                               BsG_N11 <<
                                                                               BsR_N11 <<
                                                                               BbsB_N11 <<
                                                                               BbsG_N11 <<
                                                                               BbsR_N11 <<
                                                                               NnsG_N11 <<
                                                                               N_N11 <<
                                                                               Nv_N11 <<
                                                                               Nm_N11 <<
                                                                               Ngm_N11 <<
                                                                               Nmv_N11 << Nnv_N11,
                                                          QSet<SequenceName>() <<
                                                                               Ns_N11 <<
                                                                               Nn_N11 <<
                                                                               Nb_N11));
        QCOMPARE(idL.size(), 1);
        int N11 = idL.at(0);

        filter->unhandled(Nb_N12, &controller);
        QCOMPARE(controller.contents.size(), 2);
        idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                               BsB_N12 <<
                                                                               BsG_N12 <<
                                                                               BsR_N12 <<
                                                                               BbsB_N12 <<
                                                                               BbsG_N12 <<
                                                                               BbsR_N12 <<
                                                                               NnsG_N12 <<
                                                                               N_N12 <<
                                                                               Nv_N12 <<
                                                                               Nm_N12 <<
                                                                               Ngm_N12 <<
                                                                               Nmv_N12 << Nnv_N12,
                                                          QSet<SequenceName>() <<
                                                                               Ns_N12 <<
                                                                               Nn_N12 <<
                                                                               Nb_N12));
        QCOMPARE(idL.size(), 1);
        int N12 = idL.at(0);

        data.setStart(15.0);
        data.setEnd(16.0);

        {
            Variant::Root d;
            d.write().array(0).setDouble(0.1);
            d.write().array(1).setDouble(0.2);
            d.write().array(2).setDouble(0.3);
            data.setValue(Ns_N11, d);
        }
        {
            Variant::Root d;
            d.write().array(0).setDouble(100);
            d.write().array(1).setDouble(50);
            d.write().array(2).setDouble(150);
            data.setValue(Nn_N11, d);
        }
        filter->process(N11, data);
        double t = sqrt(0.3 * 0.2) - sqrt(0.2 * 0.1);
        t *= 0.43429448190325182765 / 0.2;
        QCOMPARE(data.value(N_N11).toDouble(),
                 100.0 * log10(0.2 / 0.1) + 50.0 * t + 150.0 * log10(0.3 / 0.2));

        {
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
            }
            QVERIFY(filter2 != NULL);
            {
                Variant::Root d;
                d.write().array(0).setDouble(0.1);
                d.write().array(1).setDouble(0.2);
                d.write().array(2).setDouble(0.3);
                data.setValue(Ns_N11, d);
            }
            {
                Variant::Root d;
                d.write().array(0).setDouble(100);
                d.write().array(1).setDouble(50);
                d.write().array(2).setDouble(150);
                data.setValue(Nn_N11, d);
            }
            filter2->process(N11, data);
            QCOMPARE(data.value(N_N11).toDouble(),
                     100.0 * log10(0.2 / 0.1) + 50.0 * t + 150.0 * log10(0.3 / 0.2));
            delete filter2;
        }


        {
            Variant::Root d;
            d.write().array(0).setDouble(0.1);
            d.write().array(1).setDouble(0.2);
            d.write().array(2).setDouble(0.3);
            data.setValue(Ns_N12, d);
        }
        {
            Variant::Root d;
            d.write().array(0).setDouble(100);
            d.write().array(1).setDouble(50);
            d.write().array(2).setDouble(150);
            data.setValue(Nb_N12, d);
        }

        filter->process(N12, data);
        QCOMPARE(data.value(N_N12).toDouble(), 100.0 + 50.0 + 150.0);

        filter->processMeta(N11, data);
        QCOMPARE(data.value(BsB_N11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data.value(BsG_N11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data.value(BsR_N11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data.value(BbsB_N11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data.value(BbsG_N11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data.value(BbsR_N11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);

        delete filter;
    }

    void dynamicOptions()
    {
        ComponentOptions options(component->getOptions());

        qobject_cast<ComponentOptionDoubleSet *>(options.get("bs"))->clear();
        qobject_cast<ComponentOptionDoubleSet *>(options.get("bbs"))->clear();
        qobject_cast<ComponentOptionDoubleSet *>(options.get("ba"))->clear();
        qobject_cast<ComponentOptionDoubleSet *>(options.get("be"))->clear();
        qobject_cast<ComponentOptionDoubleSet *>(options.get("ds"))->clear();
        qobject_cast<ComponentOptionDoubleSet *>(options.get("g"))->clear();
        qobject_cast<ComponentOptionDoubleSet *>(options.get("bsfm"))->clear();
        qobject_cast<ComponentOptionBoolean *>(options.get("n"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("nv"))->set(false);
        qobject_cast<ComponentOptionBoolean *>(options.get("nm"))->set(false);
        qobject_cast<ComponentOptionBoolean *>(options.get("nmv"))->set(false);
        qobject_cast<ComponentOptionBoolean *>(options.get("dv"))->set(false);

        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "P_S11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "T_S11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BsG_S11"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName Nn_N11("brw", "raw", "Nn_N11");
        SequenceName Ns_N11("brw", "raw", "Ns_N11");
        SequenceName Nb_N11("brw", "raw", "Nb_N11");
        SequenceName N_N11("brw", "raw", "N_N11");
        filter->unhandled(Nn_N11, &controller);
        filter->unhandled(Ns_N11, &controller);
        filter->unhandled(N_N11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << N_N11,
                                                 QSet<SequenceName>() <<
                                                         Nn_N11 <<
                                                         Ns_N11 <<
                                                         Nb_N11));
        QCOMPARE(idL.size(), 1);
        int id = idL.at(0);

        {
            Variant::Root d;
            d.write().array(0).setDouble(0.1);
            d.write().array(1).setDouble(0.2);
            d.write().array(2).setDouble(0.3);
            data.setValue(Ns_N11, d);
        }
        {
            Variant::Root d;
            d.write().array(0).setDouble(100);
            d.write().array(1).setDouble(50);
            d.write().array(2).setDouble(150);
            data.setValue(Nb_N11, d);
        }

        filter->process(id, data);
        QCOMPARE(data.value(N_N11).toDouble(), 100.0 + 50.0 + 150.0);

        delete filter;
    }

    void predefined()
    {
        ComponentOptions options(component->getOptions());

        SequenceName Ns_N11("brw", "raw", "Ns_N11");
        SequenceName Nn_N11("brw", "raw", "Nn_N11");
        SequenceName Nb_N11("brw", "raw", "Nb_N11");
        QList<SequenceName> input;
        input << Ns_N11;

        SegmentProcessingStage *filter =
                component->createBasicFilterPredefined(options, FP::undefined(), FP::undefined(),
                                                       input);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{Ns_N11, Nn_N11, Nb_N11}));

        delete filter;
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["N11/Input/Concentration"] = "::Nb_N11:=";
        cv["N11/Input/Diameter"] = "::Ns_N11:=";
        cv["N11/NumberConcentration"] = "::N_N11:=";

        config.emplace_back(FP::undefined(), 13.0, cv);
        cv.write().setEmpty();
        cv["N11/Input/Diameter"] = "::ZNq_N11:=";
        config.emplace_back(13.0, FP::undefined(), cv);

        SegmentProcessingStage
                *filter = component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config);
        QVERIFY(filter != NULL);
        TestController controller;

        SequenceName Nb_N11("brw", "raw", "Nb_N11");
        SequenceName Ns_N11("brw", "raw", "Ns_N11");
        SequenceName N_N11("brw", "raw", "N_N11");
        SequenceName ZNq_N11("brw", "raw", "ZNq_N11");

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{Nb_N11, Ns_N11, ZNq_N11}));

        filter->unhandled(Nb_N11, &controller);
        filter->unhandled(Ns_N11, &controller);
        filter->unhandled(N_N11, &controller);
        filter->unhandled(ZNq_N11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        QList<int> idL = controller.contents
                                   .keys(TestController::ID(QSet<SequenceName>() << N_N11,
                                                            QSet<SequenceName>() <<
                                                                    Nb_N11 <<
                                                                    Ns_N11 <<
                                                                    ZNq_N11));
        QCOMPARE(idL.size(), 1);
        int N11 = idL.at(0);

        QCOMPARE(filter->metadataBreaks(N11), QSet<double>() << 13.0);

        delete filter;
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
