/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CALCINTEGRATESIZE_H
#define CALCINTEGRATESIZE_H

#include "core/first.hxx"

#include <vector>
#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QList>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "algorithms/mie.hxx"

class CalcIntegrateSize : public CPD3::Data::SegmentProcessingStage {
    struct Processing {
        struct OpticalWavelength {
            CPD3::Data::DynamicDouble *inputWavelength;
            CPD3::Data::DynamicInput *inputRefractiveIndexR;
            CPD3::Data::DynamicInput *inputRefractiveIndexI;
            CPD3::Data::DynamicInteger *inputNAngles;
            CPD3::Data::DynamicInput *inputFineDiameter;
            CPD3::Data::DynamicSequenceSelection *targetScattering;
            CPD3::Data::DynamicSequenceSelection *targetBackScattering;
            CPD3::Data::DynamicSequenceSelection *targetAbsorption;
            CPD3::Data::DynamicSequenceSelection *targetExtiniction;
            CPD3::Data::DynamicSequenceSelection *targetAsymmetry;
            CPD3::Data::DynamicSequenceSelection *targetFineScattering;
            CPD3::Data::DynamicSequenceSelection *targetScatteringDistribution;

            double previousRIR;
            double previousRII;
            int previousNAngles;
            std::shared_ptr<CPD3::Algorithms::MieTable> mie;

            OpticalWavelength() : inputWavelength(NULL),
                                  inputRefractiveIndexR(NULL),
                                  inputRefractiveIndexI(NULL),
                                  inputNAngles(NULL),
                                  inputFineDiameter(NULL),
                                  targetScattering(NULL),
                                  targetBackScattering(NULL),
                                  targetAbsorption(NULL),
                                  targetExtiniction(NULL),
                                  targetAsymmetry(NULL),
                                  targetFineScattering(NULL),
                                  targetScatteringDistribution(NULL),
                                  previousRIR(CPD3::FP::undefined()),
                                  previousRII(CPD3::FP::undefined()),
                                  previousNAngles(-1)
            { }

            OpticalWavelength(const OpticalWavelength &) = default;

            OpticalWavelength &operator=(const OpticalWavelength &) = default;

            OpticalWavelength(OpticalWavelength &&) = default;

            OpticalWavelength &operator=(OpticalWavelength &&) = default;
        };

        std::vector<OpticalWavelength> optical;
        CPD3::Data::DynamicSequenceSelection *targetNumberConcentration;
        CPD3::Data::DynamicSequenceSelection *targetVolumeConcentration;
        CPD3::Data::DynamicSequenceSelection *targetNumberMeanDiameter;
        CPD3::Data::DynamicSequenceSelection *targetNumberDiameterStandardDeviation;
        CPD3::Data::DynamicSequenceSelection *targetVolumeMeanDiameter;
        CPD3::Data::DynamicSequenceSelection *targetConcentrationDistribution;
        CPD3::Data::DynamicSequenceSelection *targetNormalizedDistribution;
        CPD3::Data::DynamicSequenceSelection *targetVolumeDistribution;

        CPD3::Data::DynamicInput *inputDiameter;
        CPD3::Data::DynamicInput *inputN;
        CPD3::Data::DynamicInput *inputUnnormalized;
        CPD3::Data::DynamicInput *inputMinimumDiameter;
        CPD3::Data::DynamicInput *inputMaximumDiameter;

        Processing() : targetNumberConcentration(NULL),
                       targetVolumeConcentration(NULL),
                       targetNumberMeanDiameter(NULL),
                       targetNumberDiameterStandardDeviation(NULL),
                       targetVolumeMeanDiameter(NULL),
                       targetConcentrationDistribution(NULL),
                       targetNormalizedDistribution(NULL),
                       targetVolumeDistribution(NULL),
                       inputDiameter(NULL),
                       inputN(NULL),
                       inputUnnormalized(NULL),
                       inputMinimumDiameter(NULL),
                       inputMaximumDiameter(NULL)
        { }

        Processing(const Processing &) = default;

        Processing &operator=(const Processing &) = default;

        Processing(Processing &&) = default;

        Processing &operator=(Processing &&) = default;
    };

    QSet<double> defaultScattering;
    QSet<double> defaultBackScattering;
    QSet<double> defaultAbsorption;
    QSet<double> defaultExtiniction;
    QSet<double> defaultAsymmetry;
    QSet<double> defaultFineScattering;
    QSet<double> defaultScatteringDistribution;
    bool defaultNumberConcentration;
    bool defaultVolumeConcentration;
    bool defaultNumberMeanDiameter;
    bool defaultVolumeMeanDiameter;
    bool defaultConcentrationDistribution;
    bool defaultNormalizedDistribution;
    bool defaultVolumeDistribution;

    double defaultRefractiveIndexR;
    double defaultRefractiveIndexI;
    int defaultNAngles;
    double defaultFineDiameter;
    double defaultMinimumDiameter;
    double defaultMaximumDiameter;

    CPD3::Data::SequenceName::ComponentSet filterSuffixes;

    bool restrictedInputs;

    std::vector<Processing> processing;

    void handleOptions(const CPD3::ComponentOptions &options);

    void handleNewProcessing(const CPD3::Data::SequenceName &unit,
                             int id,
                             CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control);

    bool registerPossibleInput(const CPD3::Data::SequenceName &unit,
                               CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                               int filterID = -1);

    bool initialHandler(const CPD3::Data::SequenceName &unit,
                        CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control);

public:
    CalcIntegrateSize();

    CalcIntegrateSize(const CPD3::ComponentOptions &options);

    CalcIntegrateSize(const CPD3::ComponentOptions &options,
                      double start,
                      double end, const QList<CPD3::Data::SequenceName> &inputs);

    CalcIntegrateSize(double start,
                      double end,
                      const CPD3::Data::SequenceName::Component &station,
                      const CPD3::Data::SequenceName::Component &archive,
                      const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CalcIntegrateSize();

    void unhandled(const CPD3::Data::SequenceName &unit,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;

    QSet<double> metadataBreaks(int id) override;

    CalcIntegrateSize(QDataStream &stream);

    void serialize(QDataStream &stream) override;
};

class CalcIntegrateSizeComponent
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.calc_integratesize"
                              FILE
                              "calc_integratesize.json")
public:
    virtual QString getBasicSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Data::SegmentProcessingStage
            *createBasicFilterDynamic(const CPD3::ComponentOptions &options);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined
            (const CPD3::ComponentOptions &options,
             double start,
             double end, const QList<CPD3::Data::SequenceName> &inputs);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                         double end,
                                                                         const CPD3::Data::SequenceName::Component &station,
                                                                         const CPD3::Data::SequenceName::Component &archive,
                                                                         const CPD3::Data::ValueSegment::Transfer &config);

    virtual CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream);
};

#endif
