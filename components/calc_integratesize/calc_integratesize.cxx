/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cmath>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/wavelength.hxx"
#include "core/environment.hxx"
#include "algorithms/mie.hxx"

#include "calc_integratesize.hxx"

#ifndef M_PI
#define M_PI        3.14159265358979323846
#endif
#ifndef M_E
#define M_E         2.71828182845904523536
#endif
#ifndef M_LOG10E
#define M_LOG10E    0.43429448190325182765
#endif

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Algorithms;

void CalcIntegrateSize::handleOptions(const ComponentOptions &options)
{
    restrictedInputs = false;

    if (options.isSet("bs")) {
        defaultScattering = qobject_cast<ComponentOptionDoubleSet *>(options.get("bs"))->get();
    } else {
        defaultScattering |= 450.0;
        defaultScattering |= 550.0;
        defaultScattering |= 700.0;
    }

    if (options.isSet("bbs")) {
        defaultBackScattering = qobject_cast<ComponentOptionDoubleSet *>(options.get("bbs"))->get();
    } else {
        defaultBackScattering |= 450.0;
        defaultBackScattering |= 550.0;
        defaultBackScattering |= 700.0;
    }

    if (options.isSet("ba")) {
        defaultAbsorption = qobject_cast<ComponentOptionDoubleSet *>(options.get("ba"))->get();
    }

    if (options.isSet("be")) {
        defaultExtiniction = qobject_cast<ComponentOptionDoubleSet *>(options.get("be"))->get();
    }

    if (options.isSet("g")) {
        defaultAsymmetry = qobject_cast<ComponentOptionDoubleSet *>(options.get("g"))->get();
    }

    if (options.isSet("bsfm")) {
        defaultFineScattering =
                qobject_cast<ComponentOptionDoubleSet *>(options.get("bsfm"))->get();
    }

    if (options.isSet("ds")) {
        defaultScatteringDistribution =
                qobject_cast<ComponentOptionDoubleSet *>(options.get("ds"))->get();
    } else {
        defaultScatteringDistribution |= 550.0;
    }

    if (options.isSet("n")) {
        defaultNumberConcentration =
                qobject_cast<ComponentOptionBoolean *>(options.get("n"))->get();
    } else {
        defaultNumberConcentration = true;
    }

    if (options.isSet("nv")) {
        defaultVolumeConcentration =
                qobject_cast<ComponentOptionBoolean *>(options.get("nv"))->get();
    } else {
        defaultVolumeConcentration = true;
    }

    if (options.isSet("nm")) {
        defaultNumberMeanDiameter =
                qobject_cast<ComponentOptionBoolean *>(options.get("nm"))->get();
    } else {
        defaultNumberMeanDiameter = true;
    }

    if (options.isSet("nmv")) {
        defaultVolumeMeanDiameter =
                qobject_cast<ComponentOptionBoolean *>(options.get("nmv"))->get();
    } else {
        defaultVolumeMeanDiameter = true;
    }

    if (options.isSet("dnb")) {
        defaultConcentrationDistribution =
                qobject_cast<ComponentOptionBoolean *>(options.get("dnb"))->get();
    } else {
        defaultConcentrationDistribution = false;
    }

    if (options.isSet("dnn")) {
        defaultNormalizedDistribution =
                qobject_cast<ComponentOptionBoolean *>(options.get("dnn"))->get();
    } else {
        defaultNormalizedDistribution = false;
    }

    if (options.isSet("dv")) {
        defaultVolumeDistribution =
                qobject_cast<ComponentOptionBoolean *>(options.get("dv"))->get();
    } else {
        defaultVolumeDistribution = true;
    }

    if (options.isSet("ri-r")) {
        defaultRefractiveIndexR =
                qobject_cast<ComponentOptionSingleDouble *>(options.get("ri-r"))->get();
    } else {
        defaultRefractiveIndexR = 1.53;
    }

    if (options.isSet("ri-i")) {
        defaultRefractiveIndexI =
                qobject_cast<ComponentOptionSingleDouble *>(options.get("ri-i"))->get();
    } else {
        defaultRefractiveIndexI = 0.001;
    }

    if (options.isSet("n-angles")) {
        defaultNAngles =
                (int) qobject_cast<ComponentOptionSingleInteger *>(options.get("n-angles"))->get();
        if (defaultNAngles < 2)
            defaultNAngles = 2;
    } else {
        defaultNAngles = 2;
    }

    if (options.isSet("fine-diameter")) {
        defaultFineDiameter =
                qobject_cast<ComponentOptionSingleDouble *>(options.get("fine-diameter"))->get();
    } else {
        defaultFineDiameter = 0.7;
    }

    if (options.isSet("minimum-diameter")) {
        defaultMinimumDiameter =
                qobject_cast<ComponentOptionSingleDouble *>(options.get("minimum-diameter"))->get();
    } else {
        defaultMinimumDiameter = FP::undefined();
    }

    if (options.isSet("maximum-diameter")) {
        defaultMaximumDiameter =
                qobject_cast<ComponentOptionSingleDouble *>(options.get("maximum-diameter"))->get();
    } else {
        defaultMaximumDiameter = FP::undefined();
    }

    if (options.isSet("suffix")) {
        filterSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->get());
    }
}


CalcIntegrateSize::CalcIntegrateSize()
{ Q_ASSERT(false); }

CalcIntegrateSize::CalcIntegrateSize(const ComponentOptions &options)
{
    handleOptions(options);
}

CalcIntegrateSize::CalcIntegrateSize(const ComponentOptions &options,
                                     double start,
                                     double end,
                                     const QList<SequenceName> &inputs)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    handleOptions(options);

    for (QList<SequenceName>::const_iterator unit = inputs.constBegin(), endU = inputs.constEnd();
            unit != endU;
            ++unit) {
        unhandled(*unit, NULL);
    }
}

CalcIntegrateSize::CalcIntegrateSize(double start,
                                     double end,
                                     const SequenceName::Component &station,
                                     const SequenceName::Component &archive,
                                     const ValueSegment::Transfer &config)
{
    defaultNumberConcentration = false;
    defaultVolumeConcentration = false;
    defaultNumberMeanDiameter = false;
    defaultVolumeMeanDiameter = false;
    defaultConcentrationDistribution = false;
    defaultNormalizedDistribution = false;
    defaultVolumeDistribution = false;
    defaultRefractiveIndexR = 1.53;
    defaultRefractiveIndexI = 0.001;
    defaultNAngles = 2;
    restrictedInputs = true;
    defaultFineDiameter = 0.7;
    defaultMinimumDiameter = FP::undefined();
    defaultMaximumDiameter = FP::undefined();

    struct ChildData {
        std::unordered_set<Variant::PathElement::HashIndex> optical;
    };
    std::unordered_map<Variant::PathElement::HashIndex, ChildData> children;
    for (const auto &add : config) {
        auto h = add.read().toHash();
        for (auto child : h) {
            auto &target = children[child.first];
            Util::merge(child.second["Optical"].toHash().keys(), target.optical);
        }
    }
    children.erase(Variant::PathElement::HashIndex());

    for (auto &child : children) {
        child.second.optical.erase(Variant::PathElement::HashIndex());

        Processing p;

        p.inputDiameter = DynamicInput::fromConfiguration(config, QString("%1/Input/Diameter").arg(
                QString::fromStdString(child.first)), start, end);
        p.inputN = DynamicInput::fromConfiguration(config, QString("%1/Input/Normalized").arg(
                QString::fromStdString(child.first)), start, end);
        p.inputUnnormalized = DynamicInput::fromConfiguration(config,
                                                              QString("%1/Input/Concentration").arg(
                                                                      QString::fromStdString(
                                                                              child.first)), start,
                                                              end);
        p.inputMinimumDiameter = DynamicInput::fromConfiguration(config,
                                                                 QString("%1/MinimumDiameter").arg(
                                                                         QString::fromStdString(
                                                                                 child.first)),
                                                                 start, end);
        p.inputMaximumDiameter = DynamicInput::fromConfiguration(config,
                                                                 QString("%1/MaximumDiameter").arg(
                                                                         QString::fromStdString(
                                                                                 child.first)),
                                                                 start, end);
        p.inputDiameter->registerExpected(station, archive);
        p.inputN->registerExpected(station, archive);
        p.inputUnnormalized->registerExpected(station, archive);
        p.inputMinimumDiameter->registerExpected(station, archive);
        p.inputMaximumDiameter->registerExpected(station, archive);

        p.targetNumberConcentration = DynamicSequenceSelection::fromConfiguration(config,
                                                                                  QString("%1/NumberConcentration")
                                                                                          .arg(QString::fromStdString(
                                                                                                  child.first)),
                                                                                  start, end);
        p.targetVolumeConcentration = DynamicSequenceSelection::fromConfiguration(config,
                                                                                  QString("%1/VolumeConcentration")
                                                                                          .arg(QString::fromStdString(
                                                                                                  child.first)),
                                                                                  start, end);
        p.targetNumberMeanDiameter = DynamicSequenceSelection::fromConfiguration(config,
                                                                                 QString("%1/NumberMeanDiameter")
                                                                                         .arg(QString::fromStdString(
                                                                                                 child.first)),
                                                                                 start, end);
        p.targetNumberDiameterStandardDeviation =
                DynamicSequenceSelection::fromConfiguration(config,
                                                            QString("%1/NumberDiameterStandardDeviation")
                                                                    .arg(QString::fromStdString(
                                                                            child.first)), start,
                                                            end);
        p.targetVolumeMeanDiameter = DynamicSequenceSelection::fromConfiguration(config,
                                                                                 QString("%1/VolumeMeanDiameter")
                                                                                         .arg(QString::fromStdString(
                                                                                                 child.first)),
                                                                                 start, end);
        p.targetConcentrationDistribution = DynamicSequenceSelection::fromConfiguration(config,
                                                                                        QString("%1/Distribution/Concentration")
                                                                                                .arg(QString::fromStdString(
                                                                                                        child.first)),
                                                                                        start, end);
        p.targetNormalizedDistribution = DynamicSequenceSelection::fromConfiguration(config,
                                                                                     QString("%1/Distribution/Normalized")
                                                                                             .arg(QString::fromStdString(
                                                                                                     child.first)),
                                                                                     start, end);
        p.targetVolumeDistribution = DynamicSequenceSelection::fromConfiguration(config,
                                                                                 QString("%1/Distribution/Volume")
                                                                                         .arg(QString::fromStdString(
                                                                                                 child.first)),
                                                                                 start, end);
        p.targetNumberConcentration->registerExpected(station, archive);
        p.targetVolumeConcentration->registerExpected(station, archive);
        p.targetNumberMeanDiameter->registerExpected(station, archive);
        p.targetNumberDiameterStandardDeviation->registerExpected(station, archive);
        p.targetVolumeMeanDiameter->registerExpected(station, archive);
        p.targetConcentrationDistribution->registerExpected(station, archive);
        p.targetNormalizedDistribution->registerExpected(station, archive);
        p.targetVolumeDistribution->registerExpected(station, archive);

        for (const auto &wl : child.second.optical) {
            Processing::OpticalWavelength o;

            o.inputWavelength = DynamicDoubleOption::fromConfiguration(config,
                                                                       QString("%1/Optical/%2/Wavelength")
                                                                               .arg(QString::fromStdString(
                                                                                       child.first),
                                                                                    QString::fromStdString(
                                                                                            wl)),
                                                                       start, end);
            o.inputRefractiveIndexR = DynamicInput::fromConfiguration(config,
                                                                      QString("%1/Optical/%2/RefractiveIndex/R")
                                                                              .arg(QString::fromStdString(
                                                                                      child.first),
                                                                                   QString::fromStdString(
                                                                                           wl)),
                                                                      start, end);
            o.inputRefractiveIndexI = DynamicInput::fromConfiguration(config,
                                                                      QString("%1/Optical/%2/RefractiveIndex/I")
                                                                              .arg(QString::fromStdString(
                                                                                      child.first),
                                                                                   QString::fromStdString(
                                                                                           wl)),
                                                                      start, end);
            o.inputNAngles = DynamicIntegerOption::fromConfiguration(config,
                                                                     QString("%1/Optical/%2/TotalAngles")
                                                                             .arg(QString::fromStdString(
                                                                                     child.first),
                                                                                  QString::fromStdString(
                                                                                          wl)),
                                                                     start, end);
            o.inputFineDiameter = DynamicInput::fromConfiguration(config,
                                                                  QString("%1/Optical/%2/FineDiameter")
                                                                          .arg(QString::fromStdString(
                                                                                  child.first),
                                                                               QString::fromStdString(
                                                                                       wl)), start,
                                                                  end);
            o.inputRefractiveIndexR->registerExpected(station, archive);
            o.inputRefractiveIndexI->registerExpected(station, archive);
            o.inputFineDiameter->registerExpected(station, archive);

            o.targetScattering = DynamicSequenceSelection::fromConfiguration(config,
                                                                             QString("%1/Optical/%2/Scattering")
                                                                                     .arg(QString::fromStdString(
                                                                                             child.first),
                                                                                          QString::fromStdString(
                                                                                                  wl)),
                                                                             start, end);
            o.targetBackScattering = DynamicSequenceSelection::fromConfiguration(config,
                                                                                 QString("%1/Optical/%2/BackScattering")
                                                                                         .arg(QString::fromStdString(
                                                                                                 child.first),
                                                                                              QString::fromStdString(
                                                                                                      wl)),
                                                                                 start, end);
            o.targetAbsorption = DynamicSequenceSelection::fromConfiguration(config,
                                                                             QString("%1/Optical/%2/Absorption")
                                                                                     .arg(QString::fromStdString(
                                                                                             child.first),
                                                                                          QString::fromStdString(
                                                                                                  wl)),
                                                                             start, end);
            o.targetExtiniction = DynamicSequenceSelection::fromConfiguration(config,
                                                                              QString("%1/Optical/%2/Extinction")
                                                                                      .arg(QString::fromStdString(
                                                                                              child.first),
                                                                                           QString::fromStdString(
                                                                                                   wl)),
                                                                              start, end);
            o.targetFineScattering = DynamicSequenceSelection::fromConfiguration(config,
                                                                                 QString("%1/Optical/%2/FineScattering")
                                                                                         .arg(QString::fromStdString(
                                                                                                 child.first),
                                                                                              QString::fromStdString(
                                                                                                      wl)),
                                                                                 start, end);
            o.targetAsymmetry = DynamicSequenceSelection::fromConfiguration(config,
                                                                            QString("%1/Optical/%2/AsymmetryParameter")
                                                                                    .arg(QString::fromStdString(
                                                                                            child.first),
                                                                                         QString::fromStdString(
                                                                                                 wl)),
                                                                            start, end);
            o.targetScatteringDistribution = DynamicSequenceSelection::fromConfiguration(config,
                                                                                         QString("%1/Optical/%2/ScatteringDistribution")
                                                                                                 .arg(QString::fromStdString(
                                                                                                         child.first),
                                                                                                      QString::fromStdString(
                                                                                                              wl)),
                                                                                         start,
                                                                                         end);
            o.targetScattering->registerExpected(station, archive);
            o.targetBackScattering->registerExpected(station, archive);
            o.targetAbsorption->registerExpected(station, archive);
            o.targetExtiniction->registerExpected(station, archive);
            o.targetFineScattering->registerExpected(station, archive);
            o.targetAsymmetry->registerExpected(station, archive);
            o.targetScatteringDistribution->registerExpected(station, archive);

            o.previousRIR = -1.0;
            o.previousRII = -1.0;
            o.previousNAngles = -1;
            o.mie.reset();

            p.optical.push_back(o);
        }

        processing.push_back(p);
    }
}


CalcIntegrateSize::~CalcIntegrateSize()
{
    for (std::vector<Processing>::iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        delete p->targetNumberConcentration;
        delete p->targetVolumeConcentration;
        delete p->targetNumberMeanDiameter;
        delete p->targetNumberDiameterStandardDeviation;
        delete p->targetVolumeMeanDiameter;
        delete p->targetConcentrationDistribution;
        delete p->targetNormalizedDistribution;
        delete p->targetVolumeDistribution;
        delete p->inputDiameter;
        delete p->inputN;
        delete p->inputUnnormalized;
        delete p->inputMinimumDiameter;
        delete p->inputMaximumDiameter;
        for (std::vector<Processing::OpticalWavelength>::iterator o = p->optical.begin(),
                endO = p->optical.end(); o != endO; ++o) {
            delete o->inputWavelength;
            delete o->inputRefractiveIndexI;
            delete o->inputRefractiveIndexR;
            delete o->inputNAngles;
            delete o->inputFineDiameter;
            delete o->targetScattering;
            delete o->targetBackScattering;
            delete o->targetAbsorption;
            delete o->targetExtiniction;
            delete o->targetAsymmetry;
            delete o->targetFineScattering;
            delete o->targetScatteringDistribution;
            o->mie.reset();
        }
    }
}

void CalcIntegrateSize::handleNewProcessing(const SequenceName &unit,
                                            int id,
                                            SegmentProcessingStage::SequenceHandlerControl *control)
{
    Processing *p = &processing[id];
    SequenceName::Set reg;

    p->targetNumberConcentration
     ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->targetNumberConcentration->getAllUnits(), reg);

    p->targetVolumeConcentration
     ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->targetVolumeConcentration->getAllUnits(), reg);

    p->targetNumberMeanDiameter
     ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->targetNumberMeanDiameter->getAllUnits(), reg);

    p->targetNumberDiameterStandardDeviation
     ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->targetNumberDiameterStandardDeviation->getAllUnits(), reg);

    p->targetVolumeMeanDiameter
     ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->targetVolumeMeanDiameter->getAllUnits(), reg);

    p->targetConcentrationDistribution
     ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->targetConcentrationDistribution->getAllUnits(), reg);

    p->targetNormalizedDistribution
     ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->targetNormalizedDistribution->getAllUnits(), reg);

    p->targetVolumeDistribution
     ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->targetVolumeDistribution->getAllUnits(), reg);

    for (std::vector<Processing::OpticalWavelength>::iterator it = p->optical.begin(),
            end = p->optical.end(); it != end; ++it) {
        it->targetScattering
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->targetScattering->getAllUnits(), reg);

        it->targetBackScattering
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->targetBackScattering->getAllUnits(), reg);

        it->targetAbsorption
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->targetAbsorption->getAllUnits(), reg);

        it->targetExtiniction
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->targetExtiniction->getAllUnits(), reg);

        it->targetAsymmetry
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->targetAsymmetry->getAllUnits(), reg);

        it->targetScatteringDistribution
          ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        Util::merge(it->targetScatteringDistribution->getAllUnits(), reg);
    }

    if (!control)
        return;

    for (const auto &n : reg) {
        bool isNew = !control->isHandled(n);
        control->filterUnit(n, id);
        if (isNew)
            registerPossibleInput(n, control, id);
    }
}

bool CalcIntegrateSize::registerPossibleInput(const SequenceName &unit,
                                              SegmentProcessingStage::SequenceHandlerControl *control,
                                              int filterID)
{
    bool handledAsDistributionInput = false;

    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        bool usedAsInput = false;
        if (processing[id].inputDiameter->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
            handledAsDistributionInput = true;
        }
        if (processing[id].inputN->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
            handledAsDistributionInput = true;
        }
        if (processing[id].inputUnnormalized->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
            handledAsDistributionInput = true;
        }
        if (processing[id].inputMinimumDiameter->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }
        if (processing[id].inputMaximumDiameter->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }
        for (std::vector<Processing::OpticalWavelength>::iterator
                it = processing[id].optical.begin(), end = processing[id].optical.end();
                it != end;
                ++it) {
            if (it->inputRefractiveIndexR->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
                usedAsInput = true;
            }
            if (it->inputRefractiveIndexI->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
                usedAsInput = true;
            }
            if (it->inputFineDiameter->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
                usedAsInput = true;
            }
        }

        if (usedAsInput && id != filterID)
            handleNewProcessing(unit, id, control);
    }

    return handledAsDistributionInput;
}

bool CalcIntegrateSize::initialHandler(const SequenceName &unit,
                                       SegmentProcessingStage::SequenceHandlerControl *control)
{
    bool handledAsDistributionInput = registerPossibleInput(unit, control);

    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].targetNumberConcentration->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            handleNewProcessing(unit, id, control);
            return true;
        }
        if (processing[id].targetVolumeConcentration->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            handleNewProcessing(unit, id, control);
            return true;
        }
        if (processing[id].targetNumberMeanDiameter->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            handleNewProcessing(unit, id, control);
            return true;
        }
        if (processing[id].targetNumberDiameterStandardDeviation->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            handleNewProcessing(unit, id, control);
            return true;
        }
        if (processing[id].targetVolumeMeanDiameter->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            handleNewProcessing(unit, id, control);
            return true;
        }
        if (processing[id].targetConcentrationDistribution->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            handleNewProcessing(unit, id, control);
            return true;
        }
        if (processing[id].targetNormalizedDistribution->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            handleNewProcessing(unit, id, control);
            return true;
        }
        if (processing[id].targetVolumeDistribution->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            handleNewProcessing(unit, id, control);
            return true;
        }
        for (std::vector<Processing::OpticalWavelength>::iterator
                it = processing[id].optical.begin(), end = processing[id].optical.end();
                it != end;
                ++it) {
            if (it->targetScattering->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return true;
            }
            if (it->targetBackScattering->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return true;
            }
            if (it->targetAbsorption->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return true;
            }
            if (it->targetExtiniction->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return true;
            }
            if (it->targetAsymmetry->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return true;
            }
            if (it->targetFineScattering->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return true;
            }
            if (it->targetScatteringDistribution->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                handleNewProcessing(unit, id, control);
                return true;
            }
        }
    }

    return handledAsDistributionInput;
}

void CalcIntegrateSize::unhandled(const SequenceName &unit,
                                  SegmentProcessingStage::SequenceHandlerControl *control)
{
    if (initialHandler(unit, control))
        return;
    if (restrictedInputs)
        return;
    if (unit.hasFlavor(SequenceName::flavor_cover) || unit.hasFlavor(SequenceName::flavor_stats))
        return;
    if (!Util::starts_with(unit.getVariable(), "Nn_") &&
            !Util::starts_with(unit.getVariable(), "Ns_") &&
            !Util::starts_with(unit.getVariable(), "Nb_"))
        return;

    auto suffix = Util::suffix(unit.getVariable(), '_');
    if (!filterSuffixes.empty() && !filterSuffixes.count(suffix))
        return;

    Processing p;

    p.inputDiameter = new DynamicInput::Basic(
            SequenceName(unit.getStation(), unit.getArchive(), "Ns_" + suffix, unit.getFlavors()));
    p.inputDiameter->registerInput(unit);

    p.inputN = new DynamicInput::Basic(
            SequenceName(unit.getStation(), unit.getArchive(), "Nn_" + suffix, unit.getFlavors()));
    p.inputN->registerInput(unit);

    p.inputUnnormalized = new DynamicInput::Basic(
            SequenceName(unit.getStation(), unit.getArchive(), "Nb_" + suffix, unit.getFlavors()));
    p.inputUnnormalized->registerInput(unit);

    p.inputMinimumDiameter = new DynamicInput::Constant(defaultMinimumDiameter);
    p.inputMaximumDiameter = new DynamicInput::Constant(defaultMaximumDiameter);

    if (defaultNumberConcentration) {
        p.targetNumberConcentration = new DynamicSequenceSelection::Single(
                SequenceName(unit.getStation(), unit.getArchive(), "N_" + suffix,
                             unit.getFlavors()));
    } else {
        p.targetNumberConcentration = new DynamicSequenceSelection::None;
    }
    p.targetNumberConcentration->registerInput(unit);

    if (defaultVolumeConcentration) {
        p.targetVolumeConcentration = new DynamicSequenceSelection::Single(
                SequenceName(unit.getStation(), unit.getArchive(), "Nv_" + suffix,
                             unit.getFlavors()));
    } else {
        p.targetVolumeConcentration = new DynamicSequenceSelection::None;
    }
    p.targetVolumeConcentration->registerInput(unit);

    if (defaultNumberMeanDiameter) {
        p.targetNumberMeanDiameter = new DynamicSequenceSelection::Single(
                SequenceName(unit.getStation(), unit.getArchive(), "Nm_" + suffix,
                             unit.getFlavors()));
    } else {
        p.targetNumberMeanDiameter = new DynamicSequenceSelection::None;
    }
    p.targetNumberMeanDiameter->registerInput(unit);

    if (defaultNumberMeanDiameter) {
        p.targetNumberDiameterStandardDeviation = new DynamicSequenceSelection::Single(
                SequenceName(unit.getStation(), unit.getArchive(), "Ngm_" + suffix,
                             unit.getFlavors()));
    } else {
        p.targetNumberDiameterStandardDeviation = new DynamicSequenceSelection::None;
    }
    p.targetNumberDiameterStandardDeviation->registerInput(unit);

    if (defaultVolumeMeanDiameter) {
        p.targetVolumeMeanDiameter = new DynamicSequenceSelection::Single(
                SequenceName(unit.getStation(), unit.getArchive(), "Nmv_" + suffix,
                             unit.getFlavors()));
    } else {
        p.targetVolumeMeanDiameter = new DynamicSequenceSelection::None;
    }
    p.targetVolumeMeanDiameter->registerInput(unit);

    if (defaultConcentrationDistribution) {
        p.targetConcentrationDistribution = new DynamicSequenceSelection::Single(
                SequenceName(unit.getStation(), unit.getArchive(), "Nb_" + suffix,
                             unit.getFlavors()));
    } else {
        p.targetConcentrationDistribution = new DynamicSequenceSelection::None;
    }
    p.targetConcentrationDistribution->registerInput(unit);

    if (defaultNormalizedDistribution) {
        p.targetNormalizedDistribution = new DynamicSequenceSelection::Single(
                SequenceName(unit.getStation(), unit.getArchive(), "Nn_" + suffix,
                             unit.getFlavors()));
    } else {
        p.targetNormalizedDistribution = new DynamicSequenceSelection::None;
    }
    p.targetNormalizedDistribution->registerInput(unit);

    if (defaultVolumeDistribution) {
        p.targetVolumeDistribution = new DynamicSequenceSelection::Single(
                SequenceName(unit.getStation(), unit.getArchive(), "Nnv_" + suffix,
                             unit.getFlavors()));
    } else {
        p.targetVolumeDistribution = new DynamicSequenceSelection::None;
    }
    p.targetVolumeDistribution->registerInput(unit);

    QSet<double> allWavelengths(defaultScattering);
    allWavelengths |= defaultBackScattering;
    allWavelengths |= defaultAbsorption;
    allWavelengths |= defaultExtiniction;
    allWavelengths |= defaultAsymmetry;
    allWavelengths |= defaultFineScattering;
    allWavelengths |= defaultScatteringDistribution;

    bool canUseColorCodes = true;
    {
        std::unordered_set<std::string> usedCodes;
        for (auto wl : allWavelengths) {
            const auto &check = Wavelength::code(wl);
            if (check.empty()) {
                canUseColorCodes = false;
                break;
            }
            if (usedCodes.count(check)) {
                canUseColorCodes = false;
                break;
            }
            usedCodes.insert(check);
        }
    }

    QList<double> sortedWavelengths(allWavelengths.values());
    std::sort(sortedWavelengths.begin(), sortedWavelengths.end());
    for (int i = 0; i < sortedWavelengths.size(); i++) {
        SequenceName::Component color;
        if (canUseColorCodes) {
            color = Wavelength::code(sortedWavelengths.at(i));
            Q_ASSERT(!color.empty());
        } else if (sortedWavelengths.size() > 1) {
            color = std::to_string(i + 1);
        }

        Processing::OpticalWavelength o;
        o.inputWavelength = new DynamicPrimitive<double>::Constant(sortedWavelengths.at(i));
        o.inputRefractiveIndexR = new DynamicInput::Constant(defaultRefractiveIndexR);
        o.inputRefractiveIndexI = new DynamicInput::Constant(defaultRefractiveIndexI);
        o.inputNAngles = new DynamicPrimitive<qint64>::Constant(defaultNAngles);
        o.inputFineDiameter = new DynamicInput::Constant(defaultFineDiameter);

        if (defaultScattering.contains(sortedWavelengths.at(i))) {
            o.targetScattering = new DynamicSequenceSelection::Single(
                    SequenceName(unit.getStation(), unit.getArchive(), "Bs" + color + "_" + suffix,
                                 unit.getFlavors()));
        } else {
            o.targetScattering = new DynamicSequenceSelection::None;
        }

        if (defaultBackScattering.contains(sortedWavelengths.at(i))) {
            o.targetBackScattering = new DynamicSequenceSelection::Single(
                    SequenceName(unit.getStation(), unit.getArchive(), "Bbs" + color + "_" + suffix,
                                 unit.getFlavors()));
        } else {
            o.targetBackScattering = new DynamicSequenceSelection::None;
        }

        if (defaultAbsorption.contains(sortedWavelengths.at(i))) {
            o.targetAbsorption = new DynamicSequenceSelection::Single(
                    SequenceName(unit.getStation(), unit.getArchive(), "Ba" + color + "_" + suffix,
                                 unit.getFlavors()));
        } else {
            o.targetAbsorption = new DynamicSequenceSelection::None;
        }

        if (defaultExtiniction.contains(sortedWavelengths.at(i))) {
            o.targetExtiniction = new DynamicSequenceSelection::Single(
                    SequenceName(unit.getStation(), unit.getArchive(), "Be" + color + "_" + suffix,
                                 unit.getFlavors()));
        } else {
            o.targetExtiniction = new DynamicSequenceSelection::None;
        }

        if (defaultAsymmetry.contains(sortedWavelengths.at(i))) {
            o.targetAsymmetry = new DynamicSequenceSelection::Single(
                    SequenceName(unit.getStation(), unit.getArchive(), "ZG" + color + "_" + suffix,
                                 unit.getFlavors()));
        } else {
            o.targetAsymmetry = new DynamicSequenceSelection::None;
        }

        if (defaultFineScattering.contains(sortedWavelengths.at(i))) {
            o.targetFineScattering = new DynamicSequenceSelection::Single(
                    SequenceName(unit.getStation(), unit.getArchive(),
                                 "ZBsFine" + color + "_" + suffix, unit.getFlavors()));
        } else {
            o.targetFineScattering = new DynamicSequenceSelection::None;
        }

        if (defaultScatteringDistribution.contains(sortedWavelengths.at(i))) {
            o.targetScatteringDistribution = new DynamicSequenceSelection::Single(
                    SequenceName(unit.getStation(), unit.getArchive(), "Nns" + color + "_" + suffix,
                                 unit.getFlavors()));
        } else {
            o.targetScatteringDistribution = new DynamicSequenceSelection::None;
        }

        o.previousRIR = -1.0;
        o.previousRII = -1.0;
        o.previousNAngles = -1;
        o.mie.reset();

        p.optical.push_back(o);
    }

    int id = processing.size();
    processing.push_back(p);

    initialHandler(unit, control);

    if (!control)
        return;

    SequenceName::Set reg;
    Util::merge(processing[id].inputDiameter->getUsedInputs(), reg);
    Util::merge(processing[id].inputN->getUsedInputs(), reg);
    Util::merge(processing[id].inputUnnormalized->getUsedInputs(), reg);
    Util::merge(processing[id].inputMinimumDiameter->getUsedInputs(), reg);
    Util::merge(processing[id].inputMaximumDiameter->getUsedInputs(), reg);
    for (std::vector<Processing::OpticalWavelength>::iterator it = processing[id].optical.begin(),
            endO = processing[id].optical.end(); it != endO; ++it) {
        Util::merge(it->inputRefractiveIndexR->getUsedInputs(), reg);
        Util::merge(it->inputRefractiveIndexI->getUsedInputs(), reg);
        Util::merge(it->inputFineDiameter->getUsedInputs(), reg);;

        Util::merge(it->targetScattering->getAllUnits(), reg);
        Util::merge(it->targetBackScattering->getAllUnits(), reg);
        Util::merge(it->targetAbsorption->getAllUnits(), reg);
        Util::merge(it->targetExtiniction->getAllUnits(), reg);
        Util::merge(it->targetAsymmetry->getAllUnits(), reg);
        Util::merge(it->targetFineScattering->getAllUnits(), reg);
        Util::merge(it->targetScatteringDistribution->getAllUnits(), reg);
    }
    reg.insert(unit);

    /* Since we have static names for things, do input requests for all of them */
    for (const auto &n : reg) {
        initialHandler(n, control);
    }
}

static void undefineAll(SequenceSegment &data, const SequenceName::Set &s)
{
    for (const auto &i : s) {
        data[i].setEmpty();
    }
}

static void setAll(SequenceSegment &data, const SequenceName::Set &s, double value)
{
    for (const auto &i : s) {
        data[i].setDouble(value);
    }
}

static void setAll(SequenceSegment &data, const SequenceName::Set &s, const Variant::Read &value)
{
    for (const auto &i : s) {
        data[i].set(value);
    }
}

namespace {

class MieCallback : public MieTable::CalculateHandler {
public:
    double binScalar;
    double scatteringDistributionScalar;
    double *sumScattering;
    double *sumBackScattering;
    double *sumAbsorption;
    double *sumExtiniction;
    double *sumAsymmetry;
    double *sumFineScattering;
    Variant::Write *scatteringDistribution;
    int scatteringDistributionIndex;

    MieCallback(double bs, double sds) : binScalar(bs),
                                         scatteringDistributionScalar(sds),
                                         sumScattering(NULL),
                                         sumBackScattering(NULL),
                                         sumAbsorption(NULL),
                                         sumExtiniction(NULL),
                                         sumAsymmetry(NULL),
                                         sumFineScattering(NULL),
                                         scatteringDistribution(NULL),
                                         scatteringDistributionIndex(-1)
    { }

    virtual ~MieCallback()
    { }

    virtual void mieResult(double qsca, double qbsc, double qabs, double qext, double gsca)
    {
        double t = 0;
        if (FP::defined(qsca)) {
            t = binScalar * qsca;
            if (FP::defined(*sumScattering))
                *sumScattering += t;
            else
                *sumScattering = t;

            if (sumFineScattering != NULL) {
                if (FP::defined(*sumFineScattering))
                    *sumFineScattering += t;
                else
                    *sumFineScattering = t;
            }

            if (FP::defined(gsca)) {
                t *= gsca;
                if (FP::defined(*sumAsymmetry))
                    *sumAsymmetry += t;
                else
                    *sumAsymmetry = t;
            }

            Q_ASSERT(scatteringDistributionIndex >= 0);
            scatteringDistribution->array(scatteringDistributionIndex)
                                  .setDouble(qsca * scatteringDistributionScalar);
        }

        if (FP::defined(qbsc)) {
            t = binScalar * qbsc;
            if (FP::defined(*sumBackScattering))
                *sumBackScattering += t;
            else
                *sumBackScattering = t;
        }

        if (FP::defined(qabs)) {
            t = binScalar * qabs;
            if (FP::defined(*sumAbsorption))
                *sumAbsorption += t;
            else
                *sumAbsorption = t;
        }

        if (FP::defined(qext)) {
            t = binScalar * qext;
            if (FP::defined(*sumExtiniction))
                *sumExtiniction += t;
            else
                *sumExtiniction = t;
        }
    }
};

}

static double denormalizationFactor(double diameter, std::size_t i, const Variant::Read &D)
{
    Q_ASSERT(FP::defined(diameter) && diameter > 0.0);
    if (i == 0) {
        double dp1 = D.array(1).toReal();
        if (!FP::defined(dp1) || dp1 <= 0.0)
            return FP::undefined();
        return std::fabs(std::log10(dp1 / diameter));
    }
    double dm1 = D.array(i - 1).toReal();
    double dp1 = D.array(i + 1).toReal();
    if (!FP::defined(dm1) || dm1 <= 0.0) {
        if (!FP::defined(dp1) || dp1 <= 0.0)
            return FP::undefined();
        return std::fabs(std::log10(dp1 / diameter));
    }
    if (!FP::defined(dp1) || dp1 <= 0.0) {
        return std::fabs(std::log10(diameter / dm1));
    }

    double deltaDp = std::sqrt(diameter * dp1) - std::sqrt(diameter * dm1);
    deltaDp = std::fabs(deltaDp);
    return M_LOG10E * deltaDp / diameter;
}

void CalcIntegrateSize::process(int id, SequenceSegment &data)
{
    Processing *p = &processing[id];

    auto D = p->inputDiameter->getValue(data);
    auto Nb = p->inputUnnormalized->getValue(data);
    auto Nn = p->inputN->getValue(data);
    int nBins = std::min<int>(D.toArray().size(),
                              std::max<int>(Nn.toArray().size(), Nb.toArray().size()));

    double minimumDiameter = p->inputMinimumDiameter->get(data);
    double maximumDiameter = p->inputMaximumDiameter->get(data);

    double sumNumberConcentration = FP::undefined();
    double sumVolumeConcentration = FP::undefined();
    double sumNumberMeanDiameter = FP::undefined();
    double sumNumberDiameterStandardDeviation = FP::undefined();
    std::vector<double> sumScattering(p->optical.size(), FP::undefined());
    std::vector<double> sumBackScattering(p->optical.size(), FP::undefined());
    std::vector<double> sumAbsorption(p->optical.size(), FP::undefined());
    std::vector<double> sumExtiniction(p->optical.size(), FP::undefined());
    std::vector<double> sumAsymmetry(p->optical.size(), FP::undefined());
    std::vector<double> sumFineScattering(p->optical.size(), FP::undefined());
    std::vector<Variant::Write> outputScatteringDistribution;
    Variant::Write outputConcentrationDistribution = Variant::Write::empty();
    Variant::Write outputNormalizedDistribution = Variant::Write::empty();
    Variant::Write outputVolumeDistribution = Variant::Write::empty();
    {
        Variant::Write sdMVC = Variant::Write::empty();
        for (int binID = 0; binID < nBins; ++binID) {
            sdMVC.array(binID).setDouble(FP::undefined());
        }
        outputVolumeDistribution.set(sdMVC);
        outputConcentrationDistribution.set(sdMVC);
        outputVolumeDistribution.set(sdMVC);

        outputScatteringDistribution.reserve(p->optical.size());
        for (int i = 0, max = p->optical.size(); i < max; i++) {
            outputScatteringDistribution.emplace_back(sdMVC);
            outputScatteringDistribution.back().detachFromRoot();
        }
    }

    std::vector<double> wavelengthAdjust;
    wavelengthAdjust.reserve(p->optical.size());
    std::vector<double> fineCutoff;
    fineCutoff.reserve(p->optical.size());
    for (auto &o : p->optical) {
        double wl = o.inputWavelength->get(data);
        if (!FP::defined(wl) || wl == 0.0)
            wavelengthAdjust.push_back(FP::undefined());
        else
            wavelengthAdjust.push_back((M_PI / wl) * 1E3);
        fineCutoff.push_back(o.inputFineDiameter->get(data));

        double riR = o.inputRefractiveIndexR->get(data);
        double riI = o.inputRefractiveIndexI->get(data);
        auto nAngles = o.inputNAngles->get(data);
        if (!FP::defined(riR) || !FP::defined(riI)) {
            *(wavelengthAdjust.end() - 1) = FP::undefined();
            o.mie.reset();
            o.previousRIR = -1.0;
            o.previousRII = -1.0;
            o.previousNAngles = -1;
            continue;
        }
        if (!INTEGER::defined(nAngles) || static_cast<int>(nAngles) < 2)
            nAngles = 2;

        if (riR != o.previousRIR || riI != o.previousRII || nAngles != o.previousNAngles) {
            o.mie.reset();
            o.previousRIR = riR;
            o.previousRII = riI;
            o.previousNAngles = static_cast<int>(nAngles);
        }
    }

    double t = 0;
    double diameter = 0;
    double diameter2 = 0;
    double diameter3 = 0;
    double denormalized = 0;
    double normalized = 0;
    double binScalar = 0;
    double qsca = 0;
    double qbsc = 0;
    double qabs = 0;
    double qext = 0;
    double gsca = 0;
    for (int binID = 0; binID < nBins; ++binID) {
        diameter = D.array(binID).toDouble();
        denormalized = Nb.array(binID).toDouble();
        normalized = Nn.array(binID).toDouble();
        if (!FP::defined(denormalized) || !FP::defined(normalized)) {
            if (!FP::defined(diameter) || diameter <= 0.0)
                continue;

            t = denormalizationFactor(diameter, binID, D);
            if (!FP::defined(t) || t <= 0.0)
                continue;

            if (!FP::defined(denormalized)) {
                if (!FP::defined(normalized))
                    continue;
                denormalized = normalized * t;
            } else {
                if (!FP::defined(denormalized))
                    continue;
                normalized = denormalized / t;
            }
        }

        Q_ASSERT(FP::defined(denormalized));
        Q_ASSERT(FP::defined(normalized));

        if (FP::defined(minimumDiameter) && (!FP::defined(diameter) || diameter < minimumDiameter))
            continue;
        if (FP::defined(maximumDiameter) && (!FP::defined(diameter) || diameter > maximumDiameter))
            continue;

        if (FP::defined(sumNumberConcentration))
            sumNumberConcentration += denormalized;
        else
            sumNumberConcentration = denormalized;

        outputConcentrationDistribution.array(binID).setDouble(denormalized);
        outputNormalizedDistribution.array(binID).setDouble(normalized);

        if (!FP::defined(diameter))
            continue;

        diameter2 = diameter * diameter;
        diameter3 = diameter2 * diameter;

        t = diameter3 * denormalized;
        if (FP::defined(sumVolumeConcentration))
            sumVolumeConcentration += t;
        else
            sumVolumeConcentration = t;

        if (diameter > 0.0) {
            t = std::log(diameter) * denormalized;
            if (FP::defined(sumNumberMeanDiameter))
                sumNumberMeanDiameter += t;
            else
                sumNumberMeanDiameter = t;
        }

        outputVolumeDistribution.array(binID).setDouble(diameter3 * normalized * (M_PI / 6.0));

        for (std::size_t o = 0, maxO = p->optical.size(); o < maxO; o++) {
            t = wavelengthAdjust[o];
            if (!FP::defined(t))
                continue;
            t *= diameter;
            if (t < 0.0)
                continue;

            Processing::OpticalWavelength &working = p->optical[o];

            if (!working.mie) {
                Q_ASSERT(FP::defined(working.previousRIR) &&
                                 FP::defined(working.previousRII) &&
                                 working.previousNAngles >= 2);

                for (const auto &checkProcessing : processing) {
                    for (const auto &checkOptical : checkProcessing.optical) {
                        if (!checkOptical.mie)
                            continue;
                        if (checkOptical.previousNAngles != working.previousNAngles)
                            continue;
                        if (checkOptical.previousRIR != working.previousRIR)
                            continue;
                        if (checkOptical.previousRII != working.previousRII)
                            continue;

                        working.mie = checkOptical.mie;
                        break;
                    }
                }

                if (!working.mie) {
                    working.mie = std::make_shared<MieTable>(
                            new Mie(working.previousRIR, working.previousRII,
                                    working.previousNAngles), true);
                }
            }

            binScalar = diameter2 * denormalized * (M_PI / 4.0);

            if (working.mie->beginManualDeferCalculate(t, &qsca, &qbsc, &qabs, &qext, &gsca)) {
                MieCallback *cb = new MieCallback(binScalar, diameter2 * normalized * (M_PI / 4.0));
                cb->sumScattering = &sumScattering[o];
                if (FP::defined(fineCutoff[o]) && diameter <= fineCutoff[o])
                    cb->sumFineScattering = &sumFineScattering[o];
                cb->scatteringDistributionIndex = binID;
                cb->scatteringDistribution = &outputScatteringDistribution[o];
                cb->sumBackScattering = &sumBackScattering[o];
                cb->sumAbsorption = &sumAbsorption[o];
                cb->sumExtiniction = &sumExtiniction[o];
                cb->sumAsymmetry = &sumAsymmetry[o];
                working.mie->issueManualDeferCalculate(cb);
            } else {
                if (FP::defined(qsca)) {
                    t = binScalar * qsca;
                    if (FP::defined(sumScattering[o]))
                        sumScattering[o] += t;
                    else
                        sumScattering[o] = t;

                    if (FP::defined(fineCutoff[o]) && diameter <= fineCutoff[o]) {
                        if (FP::defined(sumFineScattering[o]))
                            sumFineScattering[o] += t;
                        else
                            sumFineScattering[o] = t;
                    }

                    if (FP::defined(gsca)) {
                        t *= gsca;
                        if (FP::defined(sumAsymmetry[o]))
                            sumAsymmetry[o] += t;
                        else
                            sumAsymmetry[o] = t;
                    }

                    outputScatteringDistribution[o].array(binID)
                                                   .setDouble(qsca *
                                                                      diameter2 *
                                                                      normalized *
                                                                      (M_PI / 4.0));
                }

                if (FP::defined(qbsc)) {
                    t = binScalar * qbsc;
                    if (FP::defined(sumBackScattering[o]))
                        sumBackScattering[o] += t;
                    else
                        sumBackScattering[o] = t;
                }

                if (FP::defined(qabs)) {
                    t = binScalar * qabs;
                    if (FP::defined(sumAbsorption[o]))
                        sumAbsorption[o] += t;
                    else
                        sumAbsorption[o] = t;
                }

                if (FP::defined(qext)) {
                    t = binScalar * qext;
                    if (FP::defined(sumExtiniction[o]))
                        sumExtiniction[o] += t;
                    else
                        sumExtiniction[o] = t;
                }
            }
            working.mie->endManualDeferCalculate();
        }
    }

    double numberMeanDiameter = FP::undefined();
    if (FP::defined(sumNumberConcentration) &&
            FP::defined(sumNumberMeanDiameter) &&
            sumNumberConcentration != 0.0) {
        numberMeanDiameter = sumNumberMeanDiameter / sumNumberConcentration;
    }

    /* Second pass for standard deviation */
    for (int binID = 0; binID < nBins; ++binID) {
        diameter = D.array(binID).toDouble();
        if (!FP::defined(diameter) || diameter <= 0.0)
            continue;

        denormalized = Nb.array(binID).toDouble();
        if (!FP::defined(denormalized)) {
            normalized = Nn.array(binID).toDouble();
            if (!FP::defined(normalized))
                continue;
            t = denormalizationFactor(diameter, binID, D);
            if (!FP::defined(t) || t <= 0.0)
                continue;
            denormalized = normalized * t;
        }

        Q_ASSERT(FP::defined(denormalized));
        Q_ASSERT(FP::defined(diameter) && diameter > 0.0);

        if (FP::defined(numberMeanDiameter)) {
            t = (std::log(diameter) - numberMeanDiameter);
            t = denormalized * t * t;

            if (FP::defined(sumNumberDiameterStandardDeviation))
                sumNumberDiameterStandardDeviation += t;
            else
                sumNumberDiameterStandardDeviation = t;
        }
    }

    for (std::size_t o = 0, maxO = p->optical.size(); o < maxO; o++) {
        /* Also waits for all differed to finish, so the sums are accessible
         * without a mutex. */
        if (p->optical[o].mie) {
            /* We run on the thread pool, so we have to release it so we're
             * not taking the only available thread waiting for it */
            p->optical[o].mie->scanDone();
        }

        setAll(data, p->optical[o].targetScattering->get(data), sumScattering[o]);
        setAll(data, p->optical[o].targetBackScattering->get(data), sumBackScattering[o]);
        setAll(data, p->optical[o].targetAbsorption->get(data), sumAbsorption[o]);
        setAll(data, p->optical[o].targetExtiniction->get(data), sumExtiniction[o]);
        setAll(data, p->optical[o].targetFineScattering->get(data), sumFineScattering[o]);
        setAll(data, p->optical[o].targetScatteringDistribution->get(data),
               outputScatteringDistribution[o]);

        if (FP::defined(sumScattering[o]) &&
                FP::defined(sumAsymmetry[o]) &&
                sumScattering[o] != 0.0) {
            setAll(data, p->optical[o].targetAsymmetry->get(data),
                   sumAsymmetry[o] / sumScattering[o]);
        } else {
            undefineAll(data, p->optical[o].targetAsymmetry->get(data));
        }
    }

    if (FP::defined(sumVolumeConcentration)) {
        setAll(data, p->targetVolumeConcentration->get(data),
               sumVolumeConcentration * (M_PI / 6.0));
    } else {
        undefineAll(data, p->targetVolumeConcentration->get(data));
    }

    setAll(data, p->targetConcentrationDistribution->get(data), outputConcentrationDistribution);
    setAll(data, p->targetNormalizedDistribution->get(data), outputNormalizedDistribution);
    setAll(data, p->targetVolumeDistribution->get(data), outputVolumeDistribution);

    if (FP::defined(numberMeanDiameter)) {
        if (FP::defined(sumNumberConcentration) &&
                FP::defined(sumNumberDiameterStandardDeviation) &&
                sumNumberConcentration != 1.0) {
            double v = sumNumberDiameterStandardDeviation / (sumNumberConcentration - 1.0);
            if (v >= 0.0) {
                setAll(data, p->targetNumberDiameterStandardDeviation->get(data), exp(sqrt(v)));
            } else {
                undefineAll(data, p->targetNumberDiameterStandardDeviation->get(data));
            }
        } else {
            undefineAll(data, p->targetNumberDiameterStandardDeviation->get(data));
        }

        setAll(data, p->targetNumberMeanDiameter->get(data), exp(numberMeanDiameter));
    } else {
        undefineAll(data, p->targetNumberDiameterStandardDeviation->get(data));
        undefineAll(data, p->targetNumberMeanDiameter->get(data));
    }

    if (FP::defined(sumNumberConcentration)) {
        setAll(data, p->targetNumberConcentration->get(data), sumNumberConcentration);

        if (sumNumberConcentration != 0.0 && FP::defined(sumVolumeConcentration)) {
            setAll(data, p->targetVolumeMeanDiameter->get(data),
                   pow(sumVolumeConcentration / sumNumberConcentration, 1.0 / 3.0));
        } else {
            undefineAll(data, p->targetVolumeMeanDiameter->get(data));
        }
    } else {
        undefineAll(data, p->targetNumberConcentration->get(data));
        undefineAll(data, p->targetVolumeMeanDiameter->get(data));
    }
}

void CalcIntegrateSize::processMeta(int id, SequenceSegment &data)
{
    Processing *p = &processing[id];

    Variant::Root meta;

    meta["By"].setString("calc_integratesize");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    meta["Parameters"].hash("Diameter") = p->inputDiameter->describe(data);
    meta["Parameters"].hash("Concentration") = p->inputN->describe(data);
    meta["Parameters"].hash("UnnormalizedConcentration") = p->inputUnnormalized->describe(data);
    meta["Parameters"].hash("MinimumDiameter") = p->inputMinimumDiameter->describe(data);
    meta["Parameters"].hash("MaximumDiameter") = p->inputMaximumDiameter->describe(data);

    Variant::Read reportT = Variant::Read::empty();
    Variant::Read reportP = Variant::Read::empty();
    qint64 totalCount = INTEGER::undefined();
    for (auto i : p->inputN->getUsedInputs()) {
        i.setMeta();
        if (!reportT.exists())
            reportT = data[i].metadata("ReportT");
        if (!reportP.exists())
            reportP = data[i].metadata("ReportP");
        qint64 n = data[i].metadata("Count").toInt64();
        if (INTEGER::defined(n) && n > 0) {
            if (!INTEGER::defined(totalCount) || n < totalCount)
                totalCount = n;
        }
    }
    for (auto i : p->inputUnnormalized->getUsedInputs()) {
        i.setMeta();
        if (!reportT.exists())
            reportT = data[i].metadata("ReportT");
        if (!reportP.exists())
            reportP = data[i].metadata("ReportP");
        qint64 n = data[i].metadata("Count").toInt64();
        if (INTEGER::defined(n) && n > 0) {
            if (!INTEGER::defined(totalCount) || n < totalCount)
                totalCount = n;
        }
    }
    for (auto i : p->inputDiameter->getUsedInputs()) {
        i.setMeta();
        qint64 n = data[i].metadata("Count").toInt64();
        if (INTEGER::defined(n) && n > 0) {
            if (!INTEGER::defined(totalCount) || n < totalCount)
                totalCount = n;
        }
    }

    for (auto i : p->targetNumberConcentration->get(data)) {
        i.setMeta();
        data[i].metadataReal("Processing").toArray().after_back().set(meta);
        data[i].metadataReal("ReportT").set(reportT);
        data[i].metadataReal("ReportP").set(reportP);
        data[i].metadataReal("Units").setString("cm\xE2\x81\xBB³");
        data[i].metadataReal("Format").setString("00000.0");
        data[i].metadataReal("Description")
               .setString(
                       "Total number concentration calculated by integrating the size distribution");
    }

    for (auto i : p->targetVolumeConcentration->get(data)) {
        i.setMeta();
        data[i].metadataReal("Processing").toArray().after_back().set(meta);
        data[i].metadataReal("ReportT").set(reportT);
        data[i].metadataReal("ReportP").set(reportP);
        data[i].metadataReal("Units").setString("\xCE\xBCm³ cm\xE2\x81\xBB³");
        data[i].metadataReal("Format").setString("00000.0");
        data[i].metadataReal("Description")
               .setString(
                       "Total volume concentration calculated by integrating the size distribution");
    }

    for (auto i : p->targetNumberMeanDiameter->get(data)) {
        i.setMeta();
        data[i].metadataReal("Processing").toArray().after_back().set(meta);
        data[i].metadataReal("ReportT").set(reportT);
        data[i].metadataReal("ReportP").set(reportP);
        data[i].metadataReal("Units").setString("\xCE\xBCm");
        data[i].metadataReal("Format").setString("00.000");
        data[i].metadataReal("Description")
               .setString(
                       "Geometric mean number diameter calculated by integrating the diameter distribution");
    }

    for (auto i : p->targetNumberDiameterStandardDeviation->get(data)) {
        i.setMeta();
        data[i].metadataReal("Processing").toArray().after_back().set(meta);
        data[i].metadataReal("ReportT").set(reportT);
        data[i].metadataReal("ReportP").set(reportP);
        data[i].metadataReal("Units").setString("\xCE\xBCm");
        data[i].metadataReal("GroupUnits").setString("GeometricStdDev");
        data[i].metadataReal("Format").setString("00.000");
        data[i].metadataReal("Description")
               .setString("Geometric standard deviation of the number diameter distribution");
    }

    for (auto i : p->targetVolumeMeanDiameter->get(data)) {
        i.setMeta();
        data[i].metadataReal("Processing").toArray().after_back().set(meta);
        data[i].metadataReal("ReportT").set(reportT);
        data[i].metadataReal("ReportP").set(reportP);
        data[i].metadataReal("Units").setString("\xCE\xBCm");
        data[i].metadataReal("Format").setString("00.000");
        data[i].metadataReal("Description")
               .setString("Mean volume diameter calculated by integrating the size distribution");
    }

    for (auto i : p->targetConcentrationDistribution->get(data)) {
        i.setMeta();
        data[i].metadataArray("Processing").toArray().after_back().set(meta);
        data[i].metadataArray("ReportT").set(reportT);
        data[i].metadataArray("ReportP").set(reportP);
        data[i].metadataArray("Count").setInteger(totalCount);
        data[i].metadataArray("Children").metadataReal("Units").setString("cm\xE2\x81\xBB³");
        data[i].metadataArray("Children").metadataReal("Format").setString("00000.00");
        data[i].metadataArray("Description").setString("Particle concentration distribution");
    }

    for (auto i : p->targetNormalizedDistribution->get(data)) {
        i.setMeta();
        data[i].metadataArray("Processing").toArray().after_back().set(meta);
        data[i].metadataArray("ReportT").set(reportT);
        data[i].metadataArray("ReportP").set(reportP);
        data[i].metadataArray("Count").setInteger(totalCount);
        data[i].metadataArray("Children").metadataReal("Units").setString("cm\xE2\x81\xBB³");
        data[i].metadataArray("Children").metadataReal("Format").setString("00000.00");
        data[i].metadataArray("Description")
               .setString("Particle concentration distribution normalized by dlog(Dp)");
    }

    for (auto i : p->targetVolumeDistribution->get(data)) {
        i.setMeta();
        data[i].metadataArray("Processing").toArray().after_back().set(meta);
        data[i].metadataArray("ReportT").set(reportT);
        data[i].metadataArray("ReportP").set(reportP);
        data[i].metadataArray("Count").setInteger(totalCount);
        data[i].metadataArray("Children")
               .metadataReal("Units")
               .setString("\xCE\xBCm³ cm\xE2\x81\xBB³");
        data[i].metadataArray("Children").metadataReal("Format").setString("00000.00");
        data[i].metadataArray("Description").setString("Particle volume distribution");
    }


    for (std::vector<Processing::OpticalWavelength>::iterator o = p->optical.begin(),
            end = p->optical.end(); o != end; ++o) {
        double wavelength = o->inputWavelength->get(data);

        meta["Parameters"].hash("Mie").hash("RefractiveIndex").hash("R") =
                o->inputRefractiveIndexR->describe(data);
        meta["Parameters"].hash("Mie").hash("RefractiveIndex").hash("I") =
                o->inputRefractiveIndexI->describe(data);
        meta["Parameters"].hash("Mie").hash("TotalAngles") = o->inputNAngles->get(data);

        for (auto i : o->targetScattering->get(data)) {
            i.setMeta();
            data[i].metadataReal("Processing").toArray().after_back().set(meta);
            data[i].metadataReal("ReportT").set(reportT);
            data[i].metadataReal("ReportP").set(reportP);
            data[i].metadataReal("Wavelength").setDouble(wavelength);
            data[i].metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
            data[i].metadataReal("Format").setString("0000.00");
            data[i].metadataReal("Description").setString("Aerosol light scattering coefficient");
        }

        for (auto i : o->targetFineScattering->get(data)) {
            i.setMeta();
            data[i].metadataReal("Processing").toArray().after_back().set(meta);
            data[i].metadataReal("Processing").toArray().back()["Parameters/Cutoff"] =
                    o->inputFineDiameter->describe(data);
            data[i].metadataReal("ReportT").set(reportT);
            data[i].metadataReal("ReportP").set(reportP);
            data[i].metadataReal("Wavelength").setDouble(wavelength);
            data[i].metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
            data[i].metadataReal("Format").setString("0000.00");
            data[i].metadataReal("Description")
                   .setString("Aerosol light scattering coefficient with fine size cutoff applied");
        }

        for (auto i : o->targetBackScattering->get(data)) {
            i.setMeta();
            data[i].metadataReal("Processing").toArray().after_back().set(meta);
            data[i].metadataReal("ReportT").set(reportT);
            data[i].metadataReal("ReportP").set(reportP);
            data[i].metadataReal("Wavelength").setDouble(wavelength);
            data[i].metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
            data[i].metadataReal("Format").setString("0000.00");
            data[i].metadataReal("Description")
                   .setString("Aerosol light backwards hemispheric scattering coefficient");
        }

        for (auto i : o->targetAbsorption->get(data)) {
            i.setMeta();
            data[i].metadataReal("Processing").toArray().after_back().set(meta);
            data[i].metadataReal("ReportT").set(reportT);
            data[i].metadataReal("ReportP").set(reportP);
            data[i].metadataReal("Wavelength").setDouble(wavelength);
            data[i].metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
            data[i].metadataReal("Format").setString("0000.00");
            data[i].metadataReal("Description").setString("Aerosol light absorption coefficient");
        }

        for (auto i : o->targetExtiniction->get(data)) {
            i.setMeta();
            data[i].metadataReal("Processing").toArray().after_back().set(meta);
            data[i].metadataReal("ReportT").set(reportT);
            data[i].metadataReal("ReportP").set(reportP);
            data[i].metadataReal("Wavelength").setDouble(wavelength);
            data[i].metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
            data[i].metadataReal("Format").setString("0000.00");
            data[i].metadataReal("Description").setString("Aerosol light extinction coefficient");
        }

        for (auto i : o->targetAsymmetry->get(data)) {
            i.setMeta();
            data[i].metadataReal("Processing").toArray().after_back().set(meta);
            data[i].metadataReal("ReportT").set(reportT);
            data[i].metadataReal("ReportP").set(reportP);
            data[i].metadataReal("Wavelength").setDouble(wavelength);
            data[i].metadataReal("GroupUnits").setString("AsymmetryParameter");
            data[i].metadataReal("Format").setString("00.000");
            data[i].metadataReal("Description").setString("Light scattering asymmetry parameter");
        }

        for (auto i : o->targetScatteringDistribution->get(data)) {
            i.setMeta();
            data[i].metadataArray("Processing").toArray().after_back().set(meta);
            data[i].metadataArray("ReportT").set(reportT);
            data[i].metadataArray("ReportP").set(reportP);
            data[i].metadataArray("Count").setInteger(totalCount);
            data[i].metadataArray("Wavelength").setDouble(wavelength);
            data[i].metadataArray("Units").setString("\xCE\xBCm² cm\xE2\x81\xBB³");
            data[i].metadataArray("Children").metadataReal("Format").setString("00000.0");
            data[i].metadataArray("Description").setString("Aerosol light scattering distribution");
        }
    }
}


SequenceName::Set CalcIntegrateSize::requestedInputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::iterator p = processing.begin(); p != processing.end(); ++p) {
        Util::merge(p->inputDiameter->getUsedInputs(), out);
        Util::merge(p->inputN->getUsedInputs(), out);
        Util::merge(p->inputUnnormalized->getUsedInputs(), out);
        Util::merge(p->inputMinimumDiameter->getUsedInputs(), out);
        Util::merge(p->inputMaximumDiameter->getUsedInputs(), out);

        for (std::vector<Processing::OpticalWavelength>::iterator o = p->optical.begin();
                o != p->optical.end();
                ++o) {
            Util::merge(o->inputRefractiveIndexR->getUsedInputs(), out);
            Util::merge(o->inputRefractiveIndexI->getUsedInputs(), out);
            Util::merge(o->inputFineDiameter->getUsedInputs(), out);
        }
    }
    return out;
}

SequenceName::Set CalcIntegrateSize::predictedOutputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::iterator p = processing.begin(); p != processing.end(); ++p) {
        Util::merge(p->targetNumberConcentration->getAllUnits(), out);
        Util::merge(p->targetVolumeConcentration->getAllUnits(), out);
        Util::merge(p->targetNumberMeanDiameter->getAllUnits(), out);
        Util::merge(p->targetNumberDiameterStandardDeviation->getAllUnits(), out);
        Util::merge(p->targetVolumeMeanDiameter->getAllUnits(), out);
        Util::merge(p->targetConcentrationDistribution->getAllUnits(), out);
        Util::merge(p->targetNormalizedDistribution->getAllUnits(), out);
        Util::merge(p->targetVolumeDistribution->getAllUnits(), out);

        for (std::vector<Processing::OpticalWavelength>::iterator o = p->optical.begin();
                o != p->optical.end();
                ++o) {
            Util::merge(o->targetScattering->getAllUnits(), out);
            Util::merge(o->targetBackScattering->getAllUnits(), out);
            Util::merge(o->targetAbsorption->getAllUnits(), out);
            Util::merge(o->targetExtiniction->getAllUnits(), out);
            Util::merge(o->targetAsymmetry->getAllUnits(), out);
            Util::merge(o->targetFineScattering->getAllUnits(), out);
            Util::merge(o->targetScatteringDistribution->getAllUnits(), out);
        }
    }
    return out;
}

QSet<double> CalcIntegrateSize::metadataBreaks(int id)
{
    QSet<double> breaks;
    Util::merge(processing[id].targetNumberConcentration->getChangedPoints(), breaks);
    Util::merge(processing[id].targetVolumeConcentration->getChangedPoints(), breaks);
    Util::merge(processing[id].targetNumberMeanDiameter->getChangedPoints(), breaks);
    Util::merge(processing[id].targetNumberDiameterStandardDeviation->getChangedPoints(), breaks);
    Util::merge(processing[id].targetVolumeMeanDiameter->getChangedPoints(), breaks);
    Util::merge(processing[id].targetConcentrationDistribution->getChangedPoints(), breaks);
    Util::merge(processing[id].targetNormalizedDistribution->getChangedPoints(), breaks);
    Util::merge(processing[id].targetVolumeDistribution->getChangedPoints(), breaks);
    for (std::vector<Processing::OpticalWavelength>::const_iterator
            o = processing[id].optical.begin(); o != processing[id].optical.end(); ++o) {
        Util::merge(o->inputWavelength->getChangedPoints(), breaks);
        Util::merge(o->inputRefractiveIndexR->getChangedPoints(), breaks);
        Util::merge(o->inputRefractiveIndexI->getChangedPoints(), breaks);
        Util::merge(o->inputNAngles->getChangedPoints(), breaks);
        Util::merge(o->inputFineDiameter->getChangedPoints(), breaks);

        Util::merge(o->targetScattering->getChangedPoints(), breaks);
        Util::merge(o->targetBackScattering->getChangedPoints(), breaks);
        Util::merge(o->targetAbsorption->getChangedPoints(), breaks);
        Util::merge(o->targetExtiniction->getChangedPoints(), breaks);
        Util::merge(o->targetAsymmetry->getChangedPoints(), breaks);
        Util::merge(o->targetFineScattering->getChangedPoints(), breaks);
        Util::merge(o->targetScatteringDistribution->getChangedPoints(), breaks);
    }
    return breaks;
}

namespace {

struct MieSerializeLookup {
    double riR;
    double riI;
    qint32 nAng;

    MieSerializeLookup()
    { }

    MieSerializeLookup(double r, double i, qint32 a) : riR(r), riI(i), nAng(a)
    { }

    bool operator==(const MieSerializeLookup &other) const
    {
        return nAng == other.nAng && riR == other.riR && riI == other.riI;
    }
};

}

namespace std {

template<>
struct hash<MieSerializeLookup> {
    inline std::size_t operator()(const MieSerializeLookup &s) const
    {
        std::size_t h = std::hash<double>()(s.riR);
        h = INTEGER::mix(h, std::hash<double>()(s.riI));
        return INTEGER::mix(h, std::hash<double>()(s.nAng));
    }
};

}

QDataStream &operator>>(QDataStream &stream, MieSerializeLookup &output)
{
    stream >> output.riR;
    stream >> output.riI;
    stream >> output.nAng;
    return stream;
}

QDataStream &operator<<(QDataStream &stream, const MieSerializeLookup &output)
{
    stream << output.riR;
    stream << output.riI;
    stream << output.nAng;
    return stream;
}

CalcIntegrateSize::CalcIntegrateSize(QDataStream &stream)
{
    stream >> defaultScattering;
    stream >> defaultBackScattering;
    stream >> defaultAbsorption;
    stream >> defaultExtiniction;
    stream >> defaultAsymmetry;
    stream >> defaultFineScattering;
    stream >> defaultScatteringDistribution;
    stream >> defaultNumberConcentration;
    stream >> defaultVolumeConcentration;
    stream >> defaultNumberMeanDiameter;
    stream >> defaultVolumeMeanDiameter;
    stream >> defaultConcentrationDistribution;
    stream >> defaultNormalizedDistribution;
    stream >> defaultVolumeDistribution;
    stream >> defaultRefractiveIndexR;
    stream >> defaultRefractiveIndexI;
    quint32 n;
    stream >> n;
    defaultNAngles = (int) n;
    stream >> defaultFineDiameter;
    stream >> defaultMinimumDiameter;
    stream >> defaultMaximumDiameter;
    stream >> filterSuffixes;
    stream >> restrictedInputs;

    stream >> n;
    for (quint32 i = 0; i < n; i++) {
        Processing p;
        stream >> p.targetNumberConcentration;
        stream >> p.targetVolumeConcentration;
        stream >> p.targetNumberMeanDiameter;
        stream >> p.targetNumberDiameterStandardDeviation;
        stream >> p.targetVolumeMeanDiameter;
        stream >> p.targetConcentrationDistribution;
        stream >> p.targetNormalizedDistribution;
        stream >> p.targetVolumeDistribution;
        stream >> p.inputDiameter;
        stream >> p.inputN;
        stream >> p.inputUnnormalized;
        stream >> p.inputMinimumDiameter;
        stream >> p.inputMaximumDiameter;

        quint32 m;
        stream >> m;
        for (quint32 j = 0; j < m; j++) {
            Processing::OpticalWavelength o;
            o.previousRIR = -1.0;
            o.previousRII = -1.0;
            o.previousNAngles = -1;
            o.mie.reset();

            stream >> o.inputWavelength;
            stream >> o.inputRefractiveIndexR;
            stream >> o.inputRefractiveIndexI;
            stream >> o.inputNAngles;
            stream >> o.inputFineDiameter;
            stream >> o.targetScattering;
            stream >> o.targetBackScattering;
            stream >> o.targetAbsorption;
            stream >> o.targetExtiniction;
            stream >> o.targetAsymmetry;
            stream >> o.targetFineScattering;
            stream >> o.targetScatteringDistribution;

            stream >> o.previousRIR >> o.previousRII;
            qint32 nAng;
            stream >> nAng;
            o.previousNAngles = (int) nAng;

            p.optical.push_back(o);
        }

        processing.push_back(p);
    }

    std::unordered_map<MieSerializeLookup, std::shared_ptr<MieTable> > tableSerialize;
    stream >> n;
    for (quint32 i = 0; i < n; i++) {
        MieSerializeLookup k;
        stream >> k;
        tableSerialize.emplace(std::move(k), std::make_shared<MieTable>(stream));
    }

    for (auto &p : processing) {
        for (auto &o : p.optical) {
            auto check = tableSerialize.find(
                    MieSerializeLookup(o.previousRIR, o.previousRII, (qint32) o.previousNAngles));
            if (check == tableSerialize.end())
                continue;
            o.mie = check->second;
        }
    }
}

void CalcIntegrateSize::serialize(QDataStream &stream)
{
    stream << defaultScattering;
    stream << defaultBackScattering;
    stream << defaultAbsorption;
    stream << defaultExtiniction;
    stream << defaultAsymmetry;
    stream << defaultFineScattering;
    stream << defaultScatteringDistribution;
    stream << defaultNumberConcentration;
    stream << defaultVolumeConcentration;
    stream << defaultNumberMeanDiameter;
    stream << defaultVolumeMeanDiameter;
    stream << defaultConcentrationDistribution;
    stream << defaultNormalizedDistribution;
    stream << defaultVolumeDistribution;
    stream << defaultRefractiveIndexR;
    stream << defaultRefractiveIndexI;
    stream << (quint32) defaultNAngles;
    stream << defaultFineDiameter;
    stream << defaultMinimumDiameter;
    stream << defaultMaximumDiameter;
    stream << filterSuffixes;
    stream << restrictedInputs;

    std::unordered_map<MieSerializeLookup, std::shared_ptr<MieTable> > tableSerialize;

    stream << (quint32) processing.size();
    for (std::vector<Processing>::iterator p = processing.begin(); p != processing.end(); ++p) {
        stream << p->targetNumberConcentration;
        stream << p->targetVolumeConcentration;
        stream << p->targetNumberMeanDiameter;
        stream << p->targetNumberDiameterStandardDeviation;
        stream << p->targetVolumeMeanDiameter;
        stream << p->targetConcentrationDistribution;
        stream << p->targetNormalizedDistribution;
        stream << p->targetVolumeDistribution;
        stream << p->inputDiameter;
        stream << p->inputN;
        stream << p->inputUnnormalized;
        stream << p->inputMinimumDiameter;
        stream << p->inputMaximumDiameter;

        stream << (quint32) p->optical.size();
        for (std::vector<Processing::OpticalWavelength>::iterator o = p->optical.begin();
                o != p->optical.end();
                ++o) {
            stream << o->inputWavelength;
            stream << o->inputRefractiveIndexR;
            stream << o->inputRefractiveIndexI;
            stream << o->inputNAngles;
            stream << o->inputFineDiameter;
            stream << o->targetScattering;
            stream << o->targetBackScattering;
            stream << o->targetAbsorption;
            stream << o->targetExtiniction;
            stream << o->targetAsymmetry;
            stream << o->targetFineScattering;
            stream << o->targetScatteringDistribution;

            stream << o->previousRIR;
            stream << o->previousRII;
            stream << (qint32) o->previousNAngles;
            if (o->mie) {
                tableSerialize.emplace(MieSerializeLookup(o->previousRIR, o->previousRII,
                                                          (qint32) o->previousNAngles), o->mie);
            }
        }
    }

    stream << (quint32) tableSerialize.size();
    for (const auto &tsI : tableSerialize) {
        stream << tsI.first;
        tsI.second->serialize(stream);
    }
}


QString CalcIntegrateSizeComponent::getBasicSerializationName() const
{ return QString::fromLatin1("calc_integratesize"); }

ComponentOptions CalcIntegrateSizeComponent::getOptions()
{
    ComponentOptions options;

    ComponentOptionDoubleSet *wl =
            new ComponentOptionDoubleSet(tr("bs", "name"), tr("Scattering wavelengths to generate"),
                                         tr("These are the wavelengths to calculate scattering at.  When set "
                                            "to empty, no scatterings are generated."),
                                         tr("450,550,700"));
    wl->setMinimum(0.0, false);
    options.add("bs", wl);

    wl = new ComponentOptionDoubleSet(tr("bbs", "name"),
                                      tr("Back scattering wavelengths to generate"),
                                      tr("These are the wavelengths to calculate back scattering at.  "
                                         "When set to empty, no back scatterings are generated."),
                                      tr("450,550,700"));
    wl->setMinimum(0.0, false);
    options.add("bbs", wl);

    wl = new ComponentOptionDoubleSet(tr("ba", "name"), tr("Absorption wavelengths to generate"),
                                      tr("These are the wavelengths to calculate absorption at.  "
                                         "When set to empty, no absorptions are generated."),
                                      tr("Disabled"));
    wl->setMinimum(0.0, false);
    options.add("ba", wl);

    wl = new ComponentOptionDoubleSet(tr("be", "name"), tr("Extinction wavelengths to generate"),
                                      tr("These are the wavelengths to calculate extinction at.  "
                                         "When set to empty, no back extinctions are generated."),
                                      tr("Disabled"));
    wl->setMinimum(0.0, false);
    options.add("be", wl);

    wl = new ComponentOptionDoubleSet(tr("ds", "name"),
                                      tr("Scattering distribution wavelengths to generate"),
                                      tr("These are the wavelengths to calculate generate the scattering "
                                             "distribution (d\xCF\x83/dLogDp) at."), tr("550"));
    wl->setMinimum(0.0, false);
    options.add("ds", wl);

    wl = new ComponentOptionDoubleSet(tr("g", "name"), tr("Asymmetry wavelengths to generate"),
                                      tr("These are the wavelengths to calculate the asymmetry parameter "
                                         "at.  When set to empty, no asymmetry parameters are generated."),
                                      tr("Disabled"));
    wl->setMinimum(0.0, false);
    options.add("g", wl);

    wl = new ComponentOptionDoubleSet(tr("bsfm", "name"),
                                      tr("Fine mode scattering wavelengths to generate"),
                                      tr("These are the wavelengths to calculate the fine mode scattering "
                                         "at.  The diameter cut off used to determine what aerosol is fine "
                                         "mode is set with --fine-diameter.  When set to empty, no fine "
                                         "mode scatterings are generated."), tr("Disabled"));
    wl->setMinimum(0.0, false);
    options.add("bsfm", wl);

    options.add("n",
                new ComponentOptionBoolean(tr("n", "name"), tr("Calculate number concentration"),
                                           tr("When enabled the total number concentration is calculated."),
                                           tr("Enabled")));

    options.add("nv",
                new ComponentOptionBoolean(tr("nv", "name"), tr("Calculate volume concentration"),
                                           tr("When enabled the total volume concentration is calculated."),
                                           tr("Enabled")));

    options.add("nm",
                new ComponentOptionBoolean(tr("nm", "name"), tr("Calculate number mean diameter"),
                                           tr("When enabled the geometric number mean diameter and standard "
                                              "deviation are calculated."), tr("Enabled")));

    options.add("nmv",
                new ComponentOptionBoolean(tr("nmv", "name"), tr("Calculate volume mean diameter"),
                                           tr("When enabled the volume mean diameter is calculated."),
                                           tr("Enabled")));

    options.add("dnb", new ComponentOptionBoolean(tr("dnb", "name"),
                                                  tr("Calculate the un-normalized distribution"),
                                                  tr("When enabled the un-normalized distribution (dN) is calculated."),
                                                  tr("Disabled")));

    options.add("dnn", new ComponentOptionBoolean(tr("dnn", "name"),
                                                  tr("Calculate the normalized distribution"),
                                                  tr("When enabled the normalized distribution (dN/dlogDp) is calculated."),
                                                  tr("Disabled")));

    options.add("dv", new ComponentOptionBoolean(tr("dv", "name"),
                                                 tr("Calculate the volume distribution"),
                                                 tr("When enabled the volume distribution (dV/dlogDp) is calculated."),
                                                 tr("Enabled")));

    ComponentOptionSingleDouble *d = new ComponentOptionSingleDouble(tr("ri-r", "name"),
                                                                     tr("Refractive index real component"),
                                                                     tr("The real component of the refractive index used in Mie "
                                                                        "calculations."),
                                                                     tr("1.53"));
    d->setMinimum(0.0, false);
    options.add("ri-r", d);

    d = new ComponentOptionSingleDouble(tr("ri-i", "name"),
                                        tr("Refractive index imaginary component"),
                                        tr("The imaginary component of the refractive index used in Mie "
                                           "calculations."), tr("0.001"));
    d->setMinimum(0.0, false);
    options.add("ri-i", d);

    ComponentOptionSingleInteger *i = new ComponentOptionSingleInteger(tr("angles", "name"),
                                                                       tr("Number of angles between 0 and 90"),
                                                                       tr("The number of angles between 0 and 90 degrees used in Mie "
                                                                          "calculations."),
                                                                       tr("2"));
    i->setMinimum(2);
    options.add("n-angles", i);

    d = new ComponentOptionSingleDouble(tr("fine-diameter", "name"),
                                        tr("The maximum diameter of fine mode in \xCE\xBCm"),
                                        tr("The maximum diameter bin in \xCE\xBCm to include in calculations "
                                               "of fine mode only properties.  All bins greater than this "
                                               "diameter are excluded from the integration."),
                                        tr("0.7 \xCE\xBCm"));
    d->setMinimum(0.0, false);
    options.add("fine-diameter", d);

    d = new ComponentOptionSingleDouble(tr("minimum-diameter", "name"),
                                        tr("The minimum diameter to analyze in \xCE\xBCm"),
                                        tr("The minimum diameter bin in \xCE\xBCm to include in any calculations.  "
                                               "All bins less than this are excluded from all calculations."),
                                        {});
    d->setMinimum(0.0, false);
    d->setAllowUndefined(true);
    options.add("minimum-diameter", d);

    d = new ComponentOptionSingleDouble(tr("maximum-diameter", "name"),
                                        tr("The maximum diameter to analyze in \xCE\xBCm"),
                                        tr("The maximum diameter bin in \xCE\xBCm to include in any calculations.  "
                                               "All bins greater than this are excluded from all calculations."),
                                        {});
    d->setMinimum(0.0, false);
    d->setAllowUndefined(true);
    options.add("maximum-diameter", d);

    options.add("suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments", "name"),
                                                                 tr("Instrument suffixes to correct"),
                                                                 tr("These are the instrument suffixes to run the calculation on.  "
                                                                    "For example N11 would usually specifies the first size distribution instrument.  "
                                                                    "This option is mutually exclusive with manual variable specification."),
                                                                 tr("All instrument suffixes")));

    return options;
}

QList<ComponentExample> CalcIntegrateSizeComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will generate all default size distribution integrations from "
                                        "any size distributions in the input data stream.")));

    options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")))->add("N11");
    examples.append(ComponentExample(options, tr("Single instrument with manual input list"),
                                     tr("This will calculate the size distributions from N11 only.")));

    return examples;
}

SegmentProcessingStage *CalcIntegrateSizeComponent::createBasicFilterDynamic(const ComponentOptions &options)
{
    return new CalcIntegrateSize(options);
}

SegmentProcessingStage *CalcIntegrateSizeComponent::createBasicFilterPredefined(const ComponentOptions &options,
                                                                                double start,
                                                                                double end,
                                                                                const QList<
                                                                                        SequenceName> &inputs)
{
    return new CalcIntegrateSize(options, start, end, inputs);
}

SegmentProcessingStage *CalcIntegrateSizeComponent::createBasicFilterEditing(double start,
                                                                             double end,
                                                                             const SequenceName::Component &station,
                                                                             const SequenceName::Component &archive,
                                                                             const ValueSegment::Transfer &config)
{
    return new CalcIntegrateSize(start, end, station, archive, config);
}

SegmentProcessingStage *CalcIntegrateSizeComponent::deserializeBasicFilter(QDataStream &stream)
{
    return new CalcIntegrateSize(stream);
}
