/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_gmd_cpcpulse.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;


AcquireGMDCPCPulse::Configuration::Configuration() : start(FP::undefined()),
                                                     end(FP::undefined()),
                                                     flow(1.4210),
                                                     channel(0),
                                                     useMeasuredTime(false),
                                                     requireChannelCounting(false)
{ }

AcquireGMDCPCPulse::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          flow(other.flow),
          channel(other.channel),
          useMeasuredTime(other.useMeasuredTime),
          requireChannelCounting(other.requireChannelCounting)
{ }

AcquireGMDCPCPulse::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          flow(1.4210),
          channel(0),
          useMeasuredTime(false),
          requireChannelCounting(false)
{
    setFromSegment(other);
}

AcquireGMDCPCPulse::Configuration::Configuration(const Configuration &under,
                                                 const ValueSegment &over,
                                                 double s,
                                                 double e) : start(s),
                                                             end(e),
                                                             flow(under.flow),
                                                             channel(under.channel),
                                                             useMeasuredTime(under.useMeasuredTime),
                                                             requireChannelCounting(
                                                                     under.requireChannelCounting)
{
    setFromSegment(over);
}

void AcquireGMDCPCPulse::Configuration::setFromSegment(const ValueSegment &config)
{
    double d = config["Flow"].toDouble();
    if (FP::defined(d) && d > 0.0)
        flow = d;

    qint64 i = config["Channel"].toInt64();
    if (INTEGER::defined(i) && i >= 1 && i <= 2)
        channel = i - 1;

    if (config["UseMeasuredTime"].exists())
        useMeasuredTime = config["UseMeasuredTime"].toBool();

    if (config["RequireChannelCounting"].exists())
        requireChannelCounting = config["RequireChannelCounting"].toBool();
}


void AcquireGMDCPCPulse::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("GMD");
    instrumentMeta["Model"].setString("Pulse");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
}


AcquireGMDCPCPulse::AcquireGMDCPCPulse(const ValueSegment::Transfer &configData,
                                       const std::string &loggingContext) : FramedInstrument(
        "gmdpulse", loggingContext),
                                                                            lastRecordTime(
                                                                                    FP::undefined()),
                                                                            autoprobeStatus(
                                                                                    AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                            responseState(
                                                                                    RESP_WAIT),
                                                                            autoprobeValidRecords(0)
{
    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireGMDCPCPulse::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void AcquireGMDCPCPulse::setToInteractive()
{ responseState = RESP_INITIALIZE; }


ComponentOptions AcquireGMDCPCPulseComponent::getBaseOptions()
{
    ComponentOptions options;

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(tr("q", "name"), tr("Flow rate"),
                                            tr("This is the flow rate of the CPC at lpm measured at ambient "
                                               "conditions."),
                                            tr("1.4210 lpm", "default flow rate"), 1);
    d->setMinimum(0.0, false);
    options.add("q", d);

    ComponentOptionSingleInteger *i =
            new ComponentOptionSingleInteger(tr("channel", "name"), tr("Input channel"),
                                             tr("This is the input channel number on the acquisition box."),
                                             tr("The first channel", "default channel"));
    i->setMinimum(1);
    i->setMaximum(2);
    options.add("channel", i);

    options.add("measuredtime", new ComponentOptionBoolean(tr("measuredtime", "name"),
                                                           tr("Use measured time interval"),
                                                           tr("If set then the measured time interval between reports is used "
                                                              "instead of assuming one second.  This is generally not "
                                                              "recommended because it is affected by processing delays and "
                                                              "jitter in the system scheduling."),
                                                           QString()));

    return options;
}

AcquireGMDCPCPulse::AcquireGMDCPCPulse(const ComponentOptions &options,
                                       const std::string &loggingContext) : FramedInstrument(
        "gmdpulse", loggingContext),
                                                                            lastRecordTime(
                                                                                    FP::undefined()),
                                                                            autoprobeStatus(
                                                                                    AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                            responseState(
                                                                                    RESP_WAIT),
                                                                            autoprobeValidRecords(0)
{
    setDefaultInvalid();

    config.append(Configuration());

    if (options.isSet("q")) {
        double value = qobject_cast<ComponentOptionSingleDouble *>(options.get("q"))->get();
        if (FP::defined(value) && value > 0.0)
            config.last().flow = value;
    }
    if (options.isSet("channel")) {
        qint64 value = qobject_cast<ComponentOptionSingleInteger *>(options.get("channel"))->get();
        if (INTEGER::defined(value) && value >= 1 && value <= 2)
            config.last().channel = value - 1;
    }
    if (options.isSet("measuredtime")) {
        config.last().useMeasuredTime =
                qobject_cast<ComponentOptionBoolean *>(options.get("measuredtime"))->get();
    }
}

AcquireGMDCPCPulse::~AcquireGMDCPCPulse()
{
}


void AcquireGMDCPCPulse::logValue(double startTime,
                                  double endTime,
                                  SequenceName::Component name,
                                  Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress) return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireGMDCPCPulse::realtimeValue(double time,
                                       SequenceName::Component name,
                                       Variant::Root &&value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

SequenceValue::Transfer AcquireGMDCPCPulse::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_gmd_cpcpulse");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    result.emplace_back(SequenceName({}, "raw_meta", "N"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Condensation nuclei concentration");
    if (FP::defined(config.first().flow)) {
        result.back().write().metadataReal("SampleFlow").setDouble(config.first().flow);
    }
    result.back().write().metadataReal("Channel").setInt64(config.first().channel);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    return result;
}

SequenceValue::Transfer AcquireGMDCPCPulse::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_gmd_cpcpulse");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());


    result.emplace_back(SequenceName({}, "raw_meta", "C"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000000.0");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Count rate"));

    return result;
}

static bool validDigit(char c)
{ return (c >= '0' && c <= '9'); }

static bool validFrameByte(char c)
{
    if (validDigit(c))
        return true;
    if (c == ' ')
        return true;
    return false;
}

/* Data frames look like:
 * 
 *     \r\X  1234   1\r\X
 *     \r\X\n  1234   1\r\X\n
 * 
 * Where \X is apparently an uninitialized (random) byte.
 */
std::size_t AcquireGMDCPCPulse::dataFrameStart(std::size_t offset,
                                               const Util::ByteArray &input) const
{
    if (input.size() < 6)
        return input.npos;
    auto max = input.size() - 6;
    while (offset < max) {
        if (input[offset] != '\r') {
            offset++;
            continue;
        }

        auto result = offset + 2;

        bool hit = false;
        for (auto check = result, cmax = std::min<std::size_t>(input.size(), result + 32);
                check < cmax;
                ++check) {
            if (input[check] != '\r')
                continue;
            hit = true;
            break;
        }
        if (!hit) {
            offset++;
            continue;
        }

        if (validFrameByte(input[result])) {
            return result;
        }

        if (input[result] != '\n') {
            offset++;
            continue;
        }

        result++;
        if (!validFrameByte(input[result])) {
            offset++;
            continue;
        }

        return result;
    }
    return input.npos;
}

std::size_t AcquireGMDCPCPulse::dataFrameEnd(std::size_t start, const Util::ByteArray &input) const
{
    if (start < 2)
        return input.npos;

    auto delimiter = input[start - 1];
    bool expectLF = false;
    if (delimiter == '\n') {
        expectLF = true;
        delimiter = input[start - 2];
    }

    for (auto result = start, max = input.size() - 1; result < max; result++) {
        if (input[result] != '\r')
            continue;
        auto next = result + 1;
        if (input[next] != delimiter)
            continue;
        ++next;
        if (expectLF) {
            if (next >= input.size())
                continue;
            if (input[next] != '\n')
                continue;
        }

        return result;
    }
    return input.npos;
}

static double calculateCountRate(double C, double dT)
{
    if (!FP::defined(C) || !FP::defined(dT))
        return FP::undefined();
    if (dT <= 0.0)
        return 0.0;
    return C / dT;
}

static double calculateConcentration(double C, double Q)
{
    if (!FP::defined(C) || !FP::defined(Q))
        return FP::undefined();
    if (C < 0.0)
        return FP::undefined();
    if (Q <= 0.0)
        return FP::undefined();
    return C / (Q * (1000.0 / 60.0));
}

int AcquireGMDCPCPulse::processRecord(const Util::ByteView &line, double startTime, double endTime)
{
    Q_ASSERT(!config.isEmpty());

    if (line.size() < 3)
        return 1;

    Util::ByteView channel1;
    Util::ByteView channel2;

    /* Extra paranoid parsing because the records are so simple */
    {
        const char *begin = line.data<const char *>();
        const char *end = begin + line.size();
        const char *f1Start = begin;
        for (;; ++f1Start) {
            if (f1Start == end)
                return 2;
            if (*f1Start != ' ')
                break;
        }
        if (*f1Start == '0') {
            if (f1Start + 1 == end)
                return 3;
            if (*(f1Start + 1) != ' ')
                return 4;
        } else {
            if (!validDigit(*f1Start))
                return 5;
        }

        const char *f1End = f1Start + 1;
        for (;; ++f1End) {
            if (f1End == end)
                return 6;
            if (*f1End == ' ')
                break;
            if (!validDigit(*f1End))
                return 7;
        }

        const char *f2Start = f1End;
        for (;; ++f2Start) {
            if (f2Start == end)
                return 8;
            if (*f2Start != ' ')
                break;
        }
        if (*f2Start == '0') {
            if (f2Start + 1 != end)
                return 9;
        } else {
            const char *f2End = f2Start;
            for (;; ++f2End) {
                if (f2End == end)
                    break;
                if (!validDigit(*f2End))
                    return 10;
            }
        }

        Q_ASSERT(f1Start < f1End);
        Q_ASSERT(f2Start < end);

        int lenF1 = (int) (f1End - f1Start);
        int lenF2 = (int) (end - f2Start);
        if (lenF1 > 12)
            return 11;
        if (lenF2 > 12)
            return 12;

        channel1 = Util::ByteView(f1Start, lenF1);
        channel2 = Util::ByteView(f2Start, lenF2);
    }

    bool ok = false;
    bool haveNonzero = false;

    if (channel1.empty()) return 13;
    Variant::Root C1(Variant::Type::Real);
    {
        auto i = channel1.parse_i64(&ok);
        if (!ok) return 14;
        if (!INTEGER::defined(i)) return 15;
        if (i < 0) return 16;
        if (i > 1) {
            if (!config.front().requireChannelCounting || config.front().channel == 0)
                haveNonzero = true;
        }
        C1.write().setDouble(static_cast<double>(i));
    }
    remap("C1", C1);

    if (channel2.empty()) return 17;
    Variant::Root C2(Variant::Type::Real);
    {
        auto i = channel2.parse_i64(&ok);
        if (!ok) return 18;
        if (!INTEGER::defined(i)) return 19;
        if (i < 0) return 20;
        if (i > 1) {
            if (!config.front().requireChannelCounting || config.front().channel == 1)
                haveNonzero = true;
        }
        C2.write().setDouble(static_cast<double>(i));
    }
    remap("C2", C2);

    if (!haveEmittedLogMeta && loggingEgress && FP::defined(startTime)) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(endTime);
        Util::append(buildRealtimeMeta(endTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    Variant::Root Q(config.first().flow);
    remap("Q", Q);

    Variant::Root dT(1.0);
    if (config.first().useMeasuredTime) {
        if (FP::defined(startTime) && FP::defined(endTime))
            dT.write().setDouble(endTime - startTime);
    }
    remap("Sd", dT);

    Variant::Root C(calculateCountRate(
            (config.first().channel == 0 ? C1.read().toDouble() : C2.read().toDouble()),
            dT.read().toDouble()));
    remap("C", C);

    Variant::Root N(calculateConcentration(C.read().toDouble(), Q.read().toDouble()));
    remap("N", N);

    logValue(startTime, endTime, "F1", Variant::Root(Variant::Type::Flags));
    logValue(startTime, endTime, "N", std::move(N));
    realtimeValue(endTime, "C", std::move(C));

    if (!haveNonzero)
        return -1;

    return 0;
}


void AcquireGMDCPCPulse::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    double startTime;
    if (!FP::defined(lastRecordTime)) {
        startTime = FP::undefined();
        lastRecordTime = frameTime;
    } else {
        startTime = lastRecordTime;
        if (lastRecordTime != frameTime)
            lastRecordTime = frameTime;
    }

    {
        int oldSize = config.size();
        Q_ASSERT(!config.isEmpty());
        double oldFlow = config.first().flow;
        int oldChannel = config.first().channel;
        if (!Range::intersectShift(config, frameTime)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.isEmpty());
        if (oldSize != config.size()) {
            if (!FP::equal(oldFlow, config.first().flow) || oldChannel != config.first().channel) {
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
            }
        }
    }

    switch (responseState) {
    case RESP_AUTOPROBE_INITIALIZE:
    case RESP_AUTOPROBE_FIRST:
    case RESP_AUTOPROBE_WAIT: {
        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 10) {
                qCDebug(log) << "Autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                timeoutAt(frameTime + 3.0);

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("AutoprobeWait");
                event(frameTime, QObject::tr("Autoprobe succeeded."), false, info);
            } else if (responseState == RESP_AUTOPROBE_FIRST) {
                responseState = RESP_AUTOPROBE_WAIT;
                timeoutAt(frameTime + 30.0);
            }
        } else if (code > 0) {
            qCDebug(log) << "Autoprobe failed at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            responseState = RESP_WAIT;
            autoprobeValidRecords = 0;

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        } else {
            if (responseState == RESP_AUTOPROBE_FIRST) {
                responseState = RESP_AUTOPROBE_WAIT;
                timeoutAt(frameTime + 30.0);
            }
        }
        break;
    }

    case RESP_WAIT:
    case RESP_INITIALIZE: {
        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            qCDebug(log) << "Comms established at" << Logging::time(frameTime);

            responseState = RESP_RUN;
            generalStatusUpdated();

            timeoutAt(frameTime + 3.0);
            ++autoprobeValidRecords;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("Wait");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            autoprobeValidRecords = 0;
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    case RESP_RUN: {
        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + 3.0);
            ++autoprobeValidRecords;
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            responseState = RESP_WAIT;
            discardData(frameTime + 0.5, 1);
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            info.hash("ResponseState").setString("Run");
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            autoprobeValidRecords = 0;
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        } else {
            timeoutAt(frameTime + 3.0);
            autoprobeValidRecords = 0;
        }
        break;
    }

    default:
        break;
    }
}

void AcquireGMDCPCPulse::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    switch (responseState) {
    case RESP_RUN: {
        qCDebug(log) << "Timeout in unpolled mode at" << Logging::time(frameTime);

        responseState = RESP_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        discardData(frameTime + 2.0, 1);
        generalStatusUpdated();

        Variant::Write info = Variant::Write::empty();
        info.hash("ResponseState").setString("Run");
        event(frameTime, QObject::tr("Timeout waiting for report.  Communications dropped."), true,
              info);

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;
    }

    case RESP_AUTOPROBE_FIRST:
    case RESP_AUTOPROBE_WAIT:
        qCDebug(log) << "Timeout waiting for autoprobe records at" << Logging::time(frameTime);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (controlStream != NULL)
            controlStream->resetControl();
        discardData(frameTime + 2.0, 1);
        responseState = RESP_WAIT;
        autoprobeValidRecords = 0;

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_INITIALIZE:
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + 30.0);
        responseState = RESP_WAIT;
        autoprobeValidRecords = 0;
        break;

    case RESP_AUTOPROBE_INITIALIZE:
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + 10.0);
        responseState = RESP_AUTOPROBE_FIRST;
        autoprobeValidRecords = 0;
        break;

    default:
        discardData(frameTime + 0.5, 1);
        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;
    }
}

void AcquireGMDCPCPulse::discardDataCompleted(double frameTime)
{
    Q_UNUSED(frameTime);
}

void AcquireGMDCPCPulse::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frameTime);
    Q_UNUSED(frame);
}

AcquisitionInterface::AutoprobeStatus AcquireGMDCPCPulse::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

Variant::Root AcquireGMDCPCPulse::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireGMDCPCPulse::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

void AcquireGMDCPCPulse::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_RUN:
        if (autoprobeValidRecords > 10) {
            /* Already running, just notify that we succeeded. */
            qCDebug(log) << "Interactive autoprobe requested with communications established at"
                         << Logging::time(time);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            break;
        }

        /* Fall through */
    default:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        autoprobeValidRecords = 0;

        timeoutAt(time + 10.0);
        discardData(time + 0.5, 1);
        responseState = RESP_AUTOPROBE_FIRST;
        generalStatusUpdated();
        break;
    }
}

void AcquireGMDCPCPulse::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    timeoutAt(time + 10.0);
    discardData(time + 0.5, 1);
    responseState = RESP_AUTOPROBE_FIRST;
    generalStatusUpdated();
}

void AcquireGMDCPCPulse::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 3.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from state " << responseState << "to interactive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_FIRST:
    case RESP_AUTOPROBE_WAIT:
        qCDebug(log) << "Promoted from passive autoprobe state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        responseState = RESP_WAIT;
        break;
    }
}

void AcquireGMDCPCPulse::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 3.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_FIRST:
    case RESP_AUTOPROBE_WAIT:
        qCDebug(log) << "Promoted from passive autoprobe state" << responseState
                     << " to passive acquisition at" << Logging::time(time);

        responseState = RESP_WAIT;
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireGMDCPCPulse::getDefaults()
{
    AutomaticDefaults result;
    result.name = "N$1$2";
    result.setSerialN81(9600);
    return result;
}


ComponentOptions AcquireGMDCPCPulseComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options);
    return options;
}

ComponentOptions AcquireGMDCPCPulseComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireGMDCPCPulseComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireGMDCPCPulseComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireGMDCPCPulseComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options);
}

QList<ComponentExample> AcquireGMDCPCPulseComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options,
                                     tr("Convert data from the first channel with the default flow rate.")));

    options = getBaseOptions();
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("q")))->set(25.78);
    (qobject_cast<ComponentOptionSingleInteger *>(options.get("channel")))->set(2);
    examples.append(
            ComponentExample(options, tr("Explicitly set flow rate acquired from the second input "
                                         "channel.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireGMDCPCPulseComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                    const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireGMDCPCPulse(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireGMDCPCPulseComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                    const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireGMDCPCPulse(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireGMDCPCPulseComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                      const std::string &loggingContext)
{
    std::unique_ptr<AcquireGMDCPCPulse> i(new AcquireGMDCPCPulse(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireGMDCPCPulseComponent::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireGMDCPCPulse> i(new AcquireGMDCPCPulse(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
