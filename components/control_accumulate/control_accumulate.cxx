/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cmath>
#include <QTemporaryFile>
#include <QDateTime>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/segment.hxx"
#include "acquisition/acquisitioncomponent.hxx"

#include "control_accumulate.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

static std::string assembleLoggingCategory(const std::string &loggingContext)
{
    std::string result = "cpd3.acquisition.control.accumulate";
    if (!loggingContext.empty()) {
        result += '.';
        result += loggingContext;
    }
    return result;
}

ControlAccumulate::ControlAccumulate(const ValueSegment::Transfer &configData,
                                     const std::string &loggingContext) : loggingName(
        assembleLoggingCategory(loggingContext)),
                                                                          log(loggingName.data()),
                                                                          terminated(false),
                                                                          state(nullptr),
                                                                          realtimeEgress(nullptr),
                                                                          loggingEgress(nullptr),
                                                                          persistentEgress(nullptr),
                                                                          haveEmittedLogMeta(false),
                                                                          haveEmittedRealtimeMeta(
                                                                                  false),
                                                                          sampleState(SAMPLE_RUN),
                                                                          stateEndTime(
                                                                                  FP::undefined()),
                                                                          realtimeStateUpdated(
                                                                                  true),
                                                                          realtimeNextUpdated(true),
                                                                          systemStateUpdated(true),
                                                                          setStateEndTime(true),
                                                                          resetPending(false),
                                                                          accumulatingIndex(
                                                                                  INTEGER::undefined()),
                                                                          priorActiveIndex(
                                                                                  INTEGER::undefined()),
                                                                          discardDataEnd(
                                                                                  FP::undefined()),
                                                                          priorUpdateTime(
                                                                                  FP::undefined()),
                                                                          instrumentMeta(),
                                                                          realtimeIngress(this),
                                                                          readIngress(this)
{
    instrumentMeta["InstrumentCode"].setString("controlaccumulate");

    config.emplace_back();
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }

    advanceDuration = DynamicTimeInterval::fromConfiguration(configData, "Advance");
    minimumDuration = DynamicTimeInterval::fromConfiguration(configData, "MinimumTime");
    blankDuration = DynamicTimeInterval::fromConfiguration(configData, "BlankTime");

    chain = new ProcessingTapChain;
    chain->setBackend(&realtimeIngress);

    Variant::Read processing = Variant::Read::empty();
    auto hit = Range::findIntersecting(configData, Time::time());
    if (hit != configData.end()) {
        processing = hit->value().hash("Processing");
    }

    chain->add(&readIngress, processing,
               SequenceMatch::Composite(SequenceMatch::Element::SpecialMatch::Data));
    chain->start();

    persistentFn = SequenceValue(SequenceName({}, "raw", "Fn"));
    persistentFf = SequenceValue(SequenceName({}, "raw", "Ff"));
    persistentTotals = SequenceValue(SequenceName({}, "raw", "ZTOTAL"));
}

ControlAccumulate::~ControlAccumulate()
{
    realtimeIngress.finish();
    chain->signalTerminate();
    chain->wait();
    delete chain;

    delete advanceDuration;
    delete minimumDuration;
    delete blankDuration;
}


ControlAccumulate::Configuration::Target::Target() : accumulateWhileBypassed(false)
{ }

ControlAccumulate::Configuration::Target::Target(const Target &other) : variable(other.variable),
                                                                        rate(other.rate->clone()),
                                                                        metadata(other.metadata),
                                                                        accumulateWhileBypassed(
                                                                                other.accumulateWhileBypassed),
                                                                        activateCommands(
                                                                                other.activateCommands),
                                                                        deactivateCommands(
                                                                                other.deactivateCommands)
{ }

ControlAccumulate::Configuration::Target &ControlAccumulate::Configuration::Target::operator=(const Target &other)
{
    variable = other.variable;
    rate.reset(other.rate->clone());
    metadata = other.metadata;
    accumulateWhileBypassed = other.accumulateWhileBypassed;
    activateCommands = other.activateCommands;
    deactivateCommands = other.deactivateCommands;
    return *this;
}

ControlAccumulate::Configuration::Target::Target(Target &&) = default;

ControlAccumulate::Configuration::Target &ControlAccumulate::Configuration::Target::operator=(Target &&) = default;

ControlAccumulate::Configuration::Configuration() : start(FP::undefined()),
                                                    end(FP::undefined()),
                                                    targets(),
                                                    bypassIndex(INTEGER::undefined()),
                                                    blankIndex(INTEGER::undefined()),
                                                    startIndex(0),
                                                    endIndex(INTEGER::undefined()),
                                                    bypassWhileChanging(false),
                                                    bypassOnEnd(false),
                                                    updateTimeout(120.0)
{ }

ControlAccumulate::Configuration::~Configuration() = default;

ControlAccumulate::Configuration::Configuration(const Configuration &) = default;

ControlAccumulate::Configuration &ControlAccumulate::Configuration::operator=(const Configuration &) = default;

ControlAccumulate::Configuration::Configuration(Configuration &&) = default;

ControlAccumulate::Configuration &ControlAccumulate::Configuration::operator=(Configuration &&) = default;

ControlAccumulate::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          targets(other.targets),
          bypassIndex(other.bypassIndex),
          blankIndex(other.blankIndex),
          startIndex(other.startIndex),
          endIndex(other.endIndex),
          bypassWhileChanging(other.bypassWhileChanging),
          bypassOnEnd(other.bypassOnEnd),
          updateTimeout(other.updateTimeout)
{ }

ControlAccumulate::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          targets(),
          bypassIndex(INTEGER::undefined()),
          blankIndex(INTEGER::undefined()),
          startIndex(0),
          endIndex(INTEGER::undefined()),
          bypassWhileChanging(false),
          bypassOnEnd(false),
          updateTimeout(120.0)
{
    setFromSegment(other);
}

ControlAccumulate::Configuration::Configuration(const Configuration &under,
                                                const ValueSegment &over,
                                                double s,
                                                double e) : start(s),
                                                            end(e),
                                                            targets(under.targets),
                                                            bypassIndex(under.bypassIndex),
                                                            blankIndex(under.blankIndex),
                                                            startIndex(under.startIndex),
                                                            endIndex(under.endIndex),
                                                            bypassWhileChanging(
                                                                    under.bypassWhileChanging),
                                                            bypassOnEnd(under.bypassOnEnd),
                                                            updateTimeout(under.updateTimeout)
{
    setFromSegment(over);
}

static DynamicInput *parseTimeInput(const Variant::Read &config)
{
    return DynamicInput::fromConfiguration(ValueSegment::Transfer{
                                                   ValueSegment(FP::undefined(), FP::undefined(), Variant::Root(config))}, QString(),
                                           FP::undefined(), FP::undefined(), {}, "rt_instant");
}

void ControlAccumulate::Configuration::parseAccumulator(const Variant::Read &config,
                                                        const SequenceName::Component &name)
{
    Target target;

    target.variable = config["Name"].toString();
    if (target.variable.empty())
        target.variable = name;
    if (target.variable.empty())
        return;

    if (config["Activate/InstrumentCommands"].exists()) {
        for (auto add : config["Activate/InstrumentCommands"].toHash()) {
            if (add.first.empty())
                continue;

            auto merge = target.activateCommands.find(add.first);
            if (merge == target.activateCommands.end()) {
                target.activateCommands.emplace(add.first, Variant::Root(add.second));
                continue;
            }

            merge->second = Variant::Root::overlay(merge->second, Variant::Root(add.second));
        }
    }
    if (config["Activate/Commands"].exists()) {
        auto merge = target.activateCommands.find(std::string());
        if (merge == target.activateCommands.end()) {
            target.activateCommands
                  .emplace(std::string(), Variant::Root(config["Activate/Commands"]));
        } else {
            merge->second = Variant::Root::overlay(merge->second,
                                                   Variant::Root(config["Activate/Commands"]));
        }
    }

    if (config["Deactivate/InstrumentCommands"].exists()) {
        for (auto add : config["Deactivate/InstrumentCommands"].toHash()) {
            if (add.first.empty())
                continue;

            auto merge = target.deactivateCommands.find(add.first);
            if (merge == target.deactivateCommands.end()) {
                target.deactivateCommands.emplace(add.first, Variant::Root(add.second));
                continue;
            }

            merge->second = Variant::Root::overlay(merge->second, Variant::Root(add.second));
        }
    }
    if (config["Deactivate/Commands"].exists()) {
        auto merge = target.deactivateCommands.find(std::string());
        if (merge == target.deactivateCommands.end()) {
            target.deactivateCommands
                  .emplace(std::string(), Variant::Root(config["Deactivate/Commands"]));
        } else {
            merge->second = Variant::Root::overlay(merge->second,
                                                   Variant::Root(config["Deactivate/Commands"]));
        }
    }

    target.rate.reset(parseTimeInput(config["Rate"]));
    target.metadata.write().set(config["Metadata"]);

    target.accumulateWhileBypassed = config["AccumulateWhileBypassed"].toBoolean();

    qint64 index = config["Index"].toInt64();
    if (!INTEGER::defined(index) || index < 0) {
        targets.emplace_back(std::vector<Target>{std::move(target)});
    } else {
        while (targets.size() <= static_cast<std::size_t>(index)) {
            targets.emplace_back(std::vector<Target>());
        }
        targets[static_cast<std::size_t>(index)].emplace_back(std::move(target));
    }
}

void ControlAccumulate::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["BypassIndex"].exists())
        bypassIndex = config["BypassIndex"].toInt64();
    if (config["BlankIndex"].exists())
        blankIndex = config["BlankIndex"].toInt64();
    if (config["StartIndex"].exists())
        startIndex = config["StartIndex"].toInt64();
    if (config["EndIndex"].exists())
        endIndex = config["EndIndex"].toInt64();
    if (config["BypassWhileChanging"].exists())
        bypassWhileChanging = config["BypassWhileChanging"].toBool();
    if (config["BypassOnEnd"].exists())
        bypassOnEnd = config["BypassOnEnd"].toBool();

    {
        double v = config["UpdateTimeout"].toDouble();
        if (FP::defined(v) && v > 0.0)
            updateTimeout = v;
    }


    if (config["Accumulators"].exists()) {
        targets.clear();
        auto children = config["Accumulators"].toChildren();
        for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
            parseAccumulator(add.value(), add.stringKey());
        }
    }
}

void ControlAccumulate::Configuration::Target::activate(AcquisitionState *st) const
{
    for (const auto &c : activateCommands) {
        st->sendCommand(c.first, c.second);
    }
}

void ControlAccumulate::Configuration::Target::deactivate(AcquisitionState *st) const
{
    for (const auto &c : deactivateCommands) {
        st->sendCommand(c.first, c.second);
    }
}

void ControlAccumulate::updateNextTime(double currentTime)
{
    if (!FP::defined(stateEndTime)) {
        stateEndTime = advanceDuration->round(currentTime, currentTime);
        if (!FP::defined(stateEndTime))
            stateEndTime = currentTime;
    }
    stateEndTime = advanceDuration->apply(currentTime, stateEndTime, true);
    if (FP::defined(stateEndTime)) {
        double check = minimumDuration->apply(currentTime, currentTime, true);
        if (FP::defined(check) && stateEndTime < check) {
            double next = advanceDuration->apply(currentTime, stateEndTime, true);
            if (FP::defined(next) && next > stateEndTime)
                stateEndTime = next;
        }
    }
    realtimeNextUpdated = true;
    systemStateUpdated = true;
    if (state)
        state->requestStateSave();
}

SequenceName::ComponentSet ControlAccumulate::getAllPossibleVariables() const
{
    SequenceName::ComponentSet result;
    for (const auto &i : config.front().targets) {
        for (const auto &t : i) {
            result.insert(t.variable);
        }
    }
    return result;
}

ControlAccumulate::Accumulator::Accumulator() : value(FP::undefined()), time(FP::undefined())
{ }

bool ControlAccumulate::isBypassed() const
{
    switch (sampleState) {
    case SAMPLE_CHANGING:
    case SAMPLE_BLANK:
    case SAMPLE_RUN:
    case SAMPLE_END:
        return false;
    case SAMPLE_BYPASSED_CHANGING:
    case SAMPLE_BYPASSED_BLANK:
    case SAMPLE_BYPASSED_RUN:
    case SAMPLE_BYPASSED_END:
        return true;
    }
    Q_ASSERT(false);
    return false;
}

void ControlAccumulate::advanceTime(double executeTime)
{
    if (!FP::defined(executeTime))
        return;

    {
        std::unique_lock<std::mutex> lock(readMutex);
        std::size_t oldSize = config.size();
        if (!Range::intersectShift(config, executeTime)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        if (config.empty())
            return;
        if (oldSize != config.size()) {
            haveEmittedRealtimeMeta = false;
            haveEmittedLogMeta = false;
            readIngress.reset();
        }
    }

    if (!haveEmittedRealtimeMeta && realtimeEgress) {
        haveEmittedRealtimeMeta = true;
        realtimeEgress->incomingData(buildLogMeta(executeTime));
        realtimeEgress->incomingData(buildRealtimeMeta(executeTime));

        if (persistentFf.read().exists() && !resetPending) {
            SequenceValue dv = persistentFf;
            dv.setStart(executeTime);
            dv.setEnd(FP::undefined());
            realtimeEgress->incomingData(std::move(dv));
        }
    }


    if (resetPending) {
        persistentFn.setEnd(executeTime);
        persistentTotals.setEnd(executeTime);

        if (persistentEgress) {
            if (persistentFn.read().exists() &&
                    FP::defined(persistentFn.getStart()) &&
                    !FP::equal(persistentFn.getStart(), persistentFn.getEnd()))
                persistentEgress->incomingData(persistentFn);
            if (persistentFf.read().exists() &&
                    FP::defined(persistentFf.getStart()) &&
                    !FP::equal(persistentFf.getStart(), persistentFf.getEnd()))
                persistentEgress->incomingData(persistentFf);
            if (persistentTotals.read().exists() &&
                    FP::defined(persistentTotals.getStart()) &&
                    !FP::equal(persistentTotals.getStart(), persistentTotals.getEnd()))
                persistentEgress->incomingData(persistentTotals);
        }

        persistentFn.write().setEmpty();
        persistentFn.setEnd(FP::undefined());
        persistentFn.setStart(executeTime);

        persistentFf.write().setInteger(static_cast<qint64>(std::floor(executeTime)));
        persistentFf.setEnd(FP::undefined());
        persistentFf.setStart(executeTime);

        persistentTotals.write().setEmpty();
        persistentTotals.setEnd(FP::undefined());
        persistentTotals.setStart(executeTime);

        persistentValuesUpdated();

        totals.clear();
        resetPending = false;

        if (realtimeEgress) {
            SequenceValue dv = persistentFf;
            dv.setStart(executeTime);
            dv.setEnd(FP::undefined());
            realtimeEgress->incomingData(std::move(dv));
        }

        if (state)
            state->requestStateSave();
    }

    qint64 targetIndex = 0;

    Variant::Flags flags;

    switch (sampleState) {
    case SAMPLE_BYPASSED_CHANGING:
        flags.insert("Bypass");
        /* Fall through */
    case SAMPLE_CHANGING:
        targetIndex = INTEGER::undefined();
        if (FP::defined(stateEndTime))
            realtimeNextUpdated = true;
        stateEndTime = FP::undefined();
        flags.insert("AccumulatorChanging");
        break;
    case SAMPLE_BLANK:
        systemStateUpdated = true;
        if (setStateEndTime) {
            setStateEndTime = false;
            stateEndTime = blankDuration->apply(executeTime, executeTime, true);
            realtimeNextUpdated = true;
            if (state)
                state->requestStateSave();
            qCDebug(log) << "Blank end time set to" << Logging::time(stateEndTime) << "at"
                         << Logging::time(executeTime);
        }
        if (FP::defined(stateEndTime) && stateEndTime > executeTime) {
            targetIndex = config.front().blankIndex;
            if (!INTEGER::defined(targetIndex))
                targetIndex = accumulatingIndex;
            flags.insert("Blank");
            break;
        }
        sampleState = SAMPLE_RUN;
        realtimeStateUpdated = true;
        setStateEndTime = true;
        stateEndTime = FP::undefined();
        accumulatingIndex = config.front().startIndex;
        /* Fall through */
    case SAMPLE_RUN:
        if (setStateEndTime) {
            setStateEndTime = false;
            updateNextTime(executeTime);
            qCDebug(log) << "Initial end time set to" << Logging::time(stateEndTime) << "at"
                         << Logging::time(executeTime);
        }
        if (FP::defined(stateEndTime) && executeTime >= stateEndTime) {
            if (INTEGER::defined(accumulatingIndex) &&
                    static_cast<std::size_t>(accumulatingIndex + 1) <
                            config.front().targets.size()) {
                ++accumulatingIndex;
                updateNextTime(executeTime);

                qCDebug(log) << "Accumulator advancing to index" << accumulatingIndex
                             << "with and end time of" << Logging::time(stateEndTime) << "at"
                             << Logging::time(executeTime);
            } else {
                stateEndTime = FP::undefined();
                sampleState = SAMPLE_END;
                realtimeStateUpdated = true;
                systemStateUpdated = true;
                realtimeNextUpdated = true;
                if (state)
                    state->requestStateSave();

                qCDebug(log) << "Accumulator advance ending at" << Logging::time(executeTime);
            }
        }

        if (sampleState == SAMPLE_RUN) {
            targetIndex = accumulatingIndex;
            break;
        }
        /* Fall through */
    case SAMPLE_END:
        targetIndex = config.front().endIndex;
        if (!INTEGER::defined(targetIndex))
            targetIndex = accumulatingIndex;
        break;
    case SAMPLE_BYPASSED_BLANK:
        if (setStateEndTime) {
            setStateEndTime = false;
            systemStateUpdated = true;
            stateEndTime = blankDuration->apply(executeTime, executeTime, true);
            realtimeNextUpdated = true;
            if (state)
                state->requestStateSave();

            qCDebug(log) << "Bypassed blank end time set to" << Logging::time(stateEndTime) << "at"
                         << Logging::time(executeTime);
        }
        if (FP::defined(stateEndTime) && stateEndTime > executeTime) {
            flags.insert("Blank");
            flags.insert("Bypass");
            targetIndex = config.front().bypassIndex;
            if (!INTEGER::defined(targetIndex)) {
                targetIndex = accumulatingIndex;
            }
            break;
        }
        sampleState = SAMPLE_BYPASSED_RUN;
        realtimeStateUpdated = true;
        systemStateUpdated = true;
        setStateEndTime = true;
        stateEndTime = FP::undefined();
        accumulatingIndex = config.front().startIndex;
        /* Fall through */
    case SAMPLE_BYPASSED_RUN:
        if (setStateEndTime) {
            setStateEndTime = false;
            updateNextTime(executeTime);

            qCDebug(log) << "Initial bypassed end time set to" << Logging::time(stateEndTime)
                         << "at" << Logging::time(executeTime);
        }
        if (FP::defined(stateEndTime) && executeTime >= stateEndTime) {
            if (INTEGER::defined(accumulatingIndex) &&
                    static_cast<std::size_t>(accumulatingIndex + 1) <
                            config.front().targets.size()) {
                ++accumulatingIndex;
                updateNextTime(executeTime);

                qCDebug(log) << "Bypassed accumulator advancing to index" << accumulatingIndex
                             << "with and end time of" << Logging::time(stateEndTime) << "at"
                             << Logging::time(executeTime);
            } else {
                stateEndTime = FP::undefined();
                sampleState = SAMPLE_BYPASSED_END;
                realtimeStateUpdated = true;
                systemStateUpdated = true;
                realtimeNextUpdated = true;
                if (state)
                    state->requestStateSave();

                qCDebug(log) << "Bypassed accumulator advance ending at"
                             << Logging::time(executeTime);
            }
        }

        if (sampleState == SAMPLE_BYPASSED_RUN) {
            flags.insert("Bypass");
            targetIndex = config.front().bypassIndex;
            if (!INTEGER::defined(targetIndex)) {
                targetIndex = accumulatingIndex;
            }
            break;
        }
        /* Fall through */
    case SAMPLE_BYPASSED_END:
        flags.insert("Bypass");
        targetIndex = config.front().bypassIndex;
        if (!INTEGER::defined(targetIndex)) {
            targetIndex = config.front().endIndex;
            if (!INTEGER::defined(targetIndex))
                targetIndex = accumulatingIndex;
        }
        break;
    }

    if (!INTEGER::defined(targetIndex) ||
            targetIndex < 0 ||
            static_cast<std::size_t>(targetIndex) >= config.front().targets.size()) {
        targetIndex = INTEGER::undefined();
    }

    if (!INTEGER::equal(priorActiveIndex, targetIndex)) {
        if (INTEGER::defined(priorActiveIndex) &&
                priorActiveIndex >= 0 &&
                static_cast<std::size_t>(priorActiveIndex) < config.front().targets.size()) {
            qCDebug(log) << "Executing exit from index" << priorActiveIndex;
            for (const auto &t : config.front().targets[priorActiveIndex]) {
                t.deactivate(state);
            }
        }
        if (INTEGER::defined(targetIndex)) {
            qCDebug(log) << "Executing entry to index" << targetIndex;

            Q_ASSERT(targetIndex >= 0 &&
                             static_cast<std::size_t>(targetIndex) < config.front().targets.size());
            for (const auto &t : config.front().targets[targetIndex]) {
                t.activate(state);
            }
        }

        priorActiveIndex = targetIndex;
        discardDataEnd = executeTime;

        persistentValuesUpdated();
    }

    if (!INTEGER::equal(targetIndex, persistentFn.read().toInt64())) {
        persistentFn.setEnd(executeTime);
        if (persistentEgress) {
            if (persistentFn.read().exists() &&
                    FP::defined(persistentFn.getStart()) &&
                    !FP::equal(persistentFn.getStart(), persistentFn.getEnd()))
                persistentEgress->incomingData(persistentFn);
        }

        persistentFn.setEnd(FP::undefined());
        persistentFn.setStart(executeTime);
        persistentFn.write().setInt64(targetIndex);
    }

    if (realtimeStateUpdated && realtimeEgress) {
        realtimeStateUpdated = false;
        switch (sampleState) {
        case SAMPLE_CHANGING:
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Changing"), executeTime,
                                  FP::undefined()));
            break;
        case SAMPLE_BLANK:
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Blank"), executeTime,
                                  FP::undefined()));
            break;
        case SAMPLE_RUN:
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), executeTime,
                                  FP::undefined()));
            break;
        case SAMPLE_END:
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("End"), executeTime,
                                  FP::undefined()));
            break;
        case SAMPLE_BYPASSED_CHANGING:
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("BypassedChanging"),
                                  executeTime, FP::undefined()));
            break;
        case SAMPLE_BYPASSED_BLANK:
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("BypassedBlank"),
                                  executeTime, FP::undefined()));
            break;
        case SAMPLE_BYPASSED_RUN:
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("BypassedRun"), executeTime,
                                  FP::undefined()));
            break;
        case SAMPLE_BYPASSED_END:
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("BypassedEnd"), executeTime,
                                  FP::undefined()));
            break;
        }
    }

    if (realtimeNextUpdated && realtimeEgress) {
        realtimeNextUpdated = false;

        Variant::Root next;
        if (FP::defined(stateEndTime)) {
            next.write().hash("Time").setDouble(stateEndTime);
        }
        realtimeEgress->incomingData(
                SequenceValue(SequenceName({}, "raw", "ZNEXT"), std::move(next), executeTime,
                              FP::undefined()));
    }

    if (systemStateUpdated && state) {
        systemStateUpdated = false;

        if (FP::defined(stateEndTime))
            state->requestDataWakeup(stateEndTime);

        switch (sampleState) {
        case SAMPLE_CHANGING:
            if (!config.empty() && config.front().bypassWhileChanging)
                state->setBypassFlag("AccumulatorChanging");
            state->clearBypassFlag("AccumulatorEnd");
            break;
        case SAMPLE_BLANK:
            state->clearBypassFlag("AccumulatorChanging");
            state->clearBypassFlag("AccumulatorEnd");
            break;
        case SAMPLE_RUN:
            state->clearBypassFlag("AccumulatorChanging");
            state->clearBypassFlag("AccumulatorEnd");
            break;
        case SAMPLE_END:
            if (!config.empty() && config.front().bypassOnEnd)
                state->setBypassFlag("AccumulatorEnd");
            state->clearBypassFlag("AccumulatorChanging");
            break;
        case SAMPLE_BYPASSED_CHANGING:
            if (!config.empty() && config.front().bypassWhileChanging)
                state->setBypassFlag("AccumulatorChanging");
            state->clearBypassFlag("AccumulatorEnd");
            break;
        case SAMPLE_BYPASSED_BLANK:
            state->clearBypassFlag("AccumulatorChanging");
            state->clearBypassFlag("AccumulatorEnd");
            break;
        case SAMPLE_BYPASSED_RUN:
            state->clearBypassFlag("AccumulatorChanging");
            state->clearBypassFlag("AccumulatorEnd");
            break;
        case SAMPLE_BYPASSED_END:
            if (!config.empty() && config.front().bypassOnEnd)
                state->setBypassFlag("AccumulatorEnd");
            state->clearBypassFlag("AccumulatorChanging");
            break;
        }
    }

    CPD3::Data::SequenceSegment::Transfer processSegments;
    {
        std::lock_guard<std::mutex> lock(readMutex);
        if (pendingSegments.empty()) {
            if (FP::defined(priorUpdateTime) &&
                    executeTime - priorUpdateTime > config.front().updateTimeout) {
                priorUpdateTime = FP::undefined();
            }
            return;
        }
        processSegments = std::move(pendingSegments);
        pendingSegments.clear();
    }

    double startTime = priorUpdateTime;
    double endTime = executeTime;
    priorUpdateTime = executeTime;

    if (realtimeEgress) {
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "F1"}, Variant::Root(flags), executeTime,
                              executeTime + 1.0));
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "Fn"}, Variant::Root(accumulatingIndex), executeTime,
                              executeTime + 1.0));
    }

    if (!haveEmittedLogMeta && loggingEgress && FP::defined(startTime)) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }

    if (loggingEgress && FP::defined(startTime) && FP::defined(endTime)) {
        loggingEgress->incomingData(
                SequenceValue({{}, "raw", "F1"}, Variant::Root(std::move(flags)), startTime,
                              endTime));
    }

    SequenceName::ComponentSet updated;
    if (INTEGER::defined(targetIndex) && FP::defined(priorUpdateTime) && FP::defined(executeTime)) {
        Q_ASSERT(targetIndex >= 0 &&
                         static_cast<std::size_t>(targetIndex) < config.front().targets.size());
        const auto &targets = config.front().targets[targetIndex];
        for (auto &segment: processSegments) {
            if (!FP::defined(segment.getEnd()) || !FP::defined(segment.getStart()))
                continue;
            double dT = segment.getEnd() - segment.getStart();
            if (dT <= 0.0)
                continue;
            if (FP::defined(discardDataEnd) && discardDataEnd >= segment.getEnd())
                continue;
            for (const auto &t: targets) {
                double rs = t.rate->get(segment);
                if (!FP::defined(rs))
                    continue;
                if (isBypassed() && !t.accumulateWhileBypassed)
                    continue;

                updated.insert(t.variable);

                auto &output = totals[t.variable];

                if (!FP::defined(output.time))
                    output.time = 0.0;
                output.time += dT;

                if (!FP::defined(output.value))
                    output.value = 0.0;
                output.value += dT * rs;
            }
        }
    }

    if (!updated.empty() && persistentFf.read().exists()) {
        persistentTotals.write().hash("Ff").set(persistentFf.read());
    }

    for (const auto &v : updated) {
        auto output = totals.find(v);
        Q_ASSERT(output != totals.end());

        persistentTotals.write().hash(v).hash("Time").setDouble(output->second.time);
        persistentTotals.write().hash(v).hash("Total").setDouble(output->second.value);

        if (!loggingEgress || !FP::defined(startTime) || !FP::defined(endTime))
            continue;

        loggingEgress->incomingData(
                SequenceValue({{}, "raw", v}, Variant::Root(output->second.value), startTime,
                              endTime));
    }

    if (realtimeEgress && FP::defined(endTime)) {
        auto allVariables = getAllPossibleVariables();
        for (const auto &v : allVariables) {
            previousRealtimeVariables.erase(v);

            auto output = totals.find(v);
            if (output == totals.end()) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", v}, Variant::Root(), endTime, endTime + 1.0));
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "Zt" + v}, Variant::Root(), endTime,
                                      endTime + 1.0));
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "Za" + v}, Variant::Root(false), endTime,
                                      endTime + 1.0));
                continue;
            }

            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", v}, Variant::Root(output->second.value), endTime,
                                  endTime + 1.0));
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "Zt" + v}, Variant::Root(output->second.time),
                                  endTime, endTime + 1.0));
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "Za" + v}, Variant::Root(updated.count(v) != 0),
                                  endTime, endTime + 1.0));
        }

        for (const auto &v: previousRealtimeVariables) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", v}, Variant::Root(), endTime, FP::undefined()));
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "Zt" + v}, Variant::Root(), endTime,
                                  FP::undefined()));
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "Za" + v}, Variant::Root(), endTime,
                                  FP::undefined()));
        }
        previousRealtimeVariables = std::move(allVariables);
    }

    if (loggingEgress && FP::defined(startTime)) {
        auto allVariables = getAllPossibleVariables();
        for (const auto &v : allVariables) {
            previousLoggingVariables.erase(v);
        }
        for (const auto &v : previousLoggingVariables) {
            loggingEgress->incomingData(
                    SequenceValue({{}, "raw", v}, Variant::Root(), endTime, FP::undefined()));
        }
        previousLoggingVariables = std::move(allVariables);
    }
}

SequenceValue::Transfer ControlAccumulate::getPersistentValues()
{
    SequenceValue::Transfer result;

    if (persistentFn.read().exists() && FP::defined(persistentFn.getStart())) {
        result.emplace_back(persistentFn);
    }
    if (persistentFf.read().exists() && FP::defined(persistentFf.getStart())) {
        result.emplace_back(persistentFf);
    }
    if (persistentTotals.read().exists() && FP::defined(persistentTotals.getStart())) {
        result.emplace_back(persistentTotals);
    }

    return result;
}

static const quint16 serializedStateVersion = 1;

namespace {
enum SerializedSampleState {
    STATE_CHANGING = 0, STATE_BLANK, STATE_RUN, STATE_END,
};
}

void ControlAccumulate::serializeState(QDataStream &stream)
{
    if (config.empty())
        return;

    stream << serializedStateVersion;

    stream << accumulatingIndex << stateEndTime;

    stream << persistentFn.getStart() << persistentFn.read();
    stream << persistentFf.getStart() << persistentFf.read();
    stream << persistentTotals.getStart() << persistentTotals.read();

    switch (sampleState) {
    case SAMPLE_CHANGING:
    case SAMPLE_BYPASSED_CHANGING:
        stream << (quint8) STATE_CHANGING;
        break;
    case SAMPLE_BLANK:
    case SAMPLE_BYPASSED_BLANK:
        stream << (quint8) STATE_BLANK;
        break;
    case SAMPLE_RUN:
    case SAMPLE_BYPASSED_RUN:
        stream << (quint8) STATE_RUN;
        break;
    case SAMPLE_END:
    case SAMPLE_BYPASSED_END:
        stream << (quint8) STATE_END;
        break;
    }

    auto allVariables = getAllPossibleVariables();
    stream << static_cast<quint32>(allVariables.size());
    for (const auto &v : allVariables) {
        stream << v;

        auto output = totals.find(v);
        if (output == totals.end()) {
            stream << FP::undefined() << FP::undefined();
        } else {
            stream << output->second.value << output->second.time;
        }
    }
}

void ControlAccumulate::deserializeState(QDataStream &stream)
{
    quint16 version = 0;
    stream >> version;
    if (version != serializedStateVersion) {
        initializeState();
        return;
    }

    stream >> accumulatingIndex >> stateEndTime;
    {
        double start = 0;
        stream >> start >> persistentFn.root();
        persistentFn.setStart(start);
    }
    {
        double start = 0;
        stream >> start >> persistentFf.root();
        persistentFf.setStart(start);
    }
    {
        double start = 0;
        stream >> start >> persistentTotals.root();
        persistentTotals.setStart(start);
    }

    setStateEndTime = false;
    resetPending = false;
    realtimeStateUpdated = true;
    realtimeNextUpdated = true;
    systemStateUpdated = true;
    quint8 stateId = 0;
    stream >> stateId;
    switch (stateId) {
    default:
    case STATE_CHANGING:
        stateEndTime = FP::undefined();
        sampleState = isBypassed() ? SAMPLE_BYPASSED_CHANGING : SAMPLE_CHANGING;
        break;
    case STATE_BLANK:
        sampleState = isBypassed() ? SAMPLE_BYPASSED_BLANK : SAMPLE_BLANK;
        break;
    case STATE_RUN:
        sampleState = isBypassed() ? SAMPLE_BYPASSED_RUN : SAMPLE_RUN;
        break;
    case STATE_END:
        sampleState = isBypassed() ? SAMPLE_BYPASSED_END : SAMPLE_END;
        break;
    }

    quint32 n = 0;
    stream >> n;
    for (quint32 i = 0; i < n; i++) {
        SequenceName::Component variable;
        stream >> variable;
        stream >> totals[variable].value >> totals[variable].time;
    }
}

void ControlAccumulate::initializeState()
{
    realtimeStateUpdated = true;
    systemStateUpdated = true;
    realtimeNextUpdated = true;
    sampleState = SAMPLE_CHANGING;
    accumulatingIndex = INTEGER::undefined();

    persistentFn.setStart(FP::undefined());
    persistentFn.write().setEmpty();
    persistentFf.setStart(FP::undefined());
    persistentFf.write().setEmpty();
    persistentTotals.setStart(FP::undefined());
    persistentTotals.write().setEmpty();

    totals.clear();
}

SequenceValue::Transfer ControlAccumulate::buildLogMeta(double time)
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("control_accumulate");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    result.emplace_back(SequenceName({}, "raw_meta", "ZTOTAL"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("Accumulator totals");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataHashChild("Ff")
          .metadataInteger("Description")
          .setString("Accumulator ID");
    result.back().write().metadataHashChild("Ff").metadataInteger("Format").setString("0000000000");


    {
        std::unordered_map<SequenceName::Component, Variant::Root> variableMetadata;
        int indexOrder = 0;
        for (const auto &t : config.front().targets) {
            for (const auto &v : t) {
                std::vector<Variant::Root> overlay;
                if (variableMetadata.count(v.variable)) {
                    overlay.emplace_back(variableMetadata[v.variable]);
                } else {
                    Variant::Root addMeta;
                    auto meta = addMeta.write();
                    meta.metadataReal("Format").setString("0000.00000");
                    meta.metadataReal("Source").set(instrumentMeta);
                    meta.metadataReal("Processing").toArray().after_back().set(processing);
                    meta.metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
                    meta.metadataReal("Realtime").hash("GroupName").setString(v.variable);
                    meta.metadataReal("Realtime").hash("GroupColumn").setString("Total");
                    meta.metadataReal("Realtime").hash("ColumnOrder").setInt64(0);
                    meta.metadataReal("Realtime").hash("RowOrder").setInt64(indexOrder);
                    overlay.emplace_back(std::move(addMeta));
                }
                if (v.metadata.read().exists())
                    overlay.emplace_back(v.metadata);

                auto added = Util::insert_or_assign(variableMetadata, v.variable,
                                                    Variant::Root::overlay(overlay.begin(),
                                                                           overlay.end()));
                added->second
                     .write()
                     .metadata("Indices")
                     .toArray()
                     .after_back()
                     .setInteger(indexOrder);

                result.back()
                      .write()
                      .metadataHashChild(v.variable)
                      .metadataHash("Description")
                      .setString("Single variable total");

                result.back()
                      .write()
                      .metadataHashChild(v.variable)
                      .metadataHashChild("Total")
                      .set(added->second);
                result.back()
                      .write()
                      .metadataHashChild(v.variable)
                      .metadataHashChild("Total")
                      .metadata("Processing")
                      .remove();
                result.back()
                      .write()
                      .metadataHashChild(v.variable)
                      .metadataHashChild("Total")
                      .metadata("Smoothing")
                      .remove();
                result.back()
                      .write()
                      .metadataHashChild(v.variable)
                      .metadataHashChild("Total")
                      .metadata("Source")
                      .remove();

                result.back()
                      .write()
                      .metadataHashChild(v.variable)
                      .metadataHashChild("Time")
                      .metadataReal("Format")
                      .setString("0000000");
                result.back()
                      .write()
                      .metadataHashChild(v.variable)
                      .metadataHashChild("Time")
                      .metadataReal("Units")
                      .setString("s");
                result.back()
                      .write()
                      .metadataHashChild(v.variable)
                      .metadataHashChild("Time")
                      .metadataReal("Description")
                      .setString("Total number of seconds accumulated");
            }

            ++indexOrder;
        }
        for (auto &add : variableMetadata) {
            result.emplace_back(SequenceName({}, "raw_meta", add.first), std::move(add.second),
                                time, FP::undefined());
        }
    }

    result.emplace_back(SequenceName({}, "raw_meta", "Fn"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataInteger("Format").setString("00");
    result.back().write().metadataInteger("Description").setString("Active accumulator index");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataInteger("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataInteger("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "Ff"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataInteger("Format").setString("0000000000");
    result.back().write().metadataInteger("Description").setString("Accumulator ID");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataInteger("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataInteger("Realtime").hash("Hide").setBool(true);


    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("AccumulatorChanging")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("control_accumulate");
    result.back()
          .write()
          .metadataSingleFlag("AccumulatorChanging")
          .hash("Description")
          .setString("Accumulator change in progress");

    result.back()
          .write()
          .metadataSingleFlag("Blank")
          .hash("Description")
          .setString("Accumulated quanties added to blank");
    result.back()
          .write()
          .metadataSingleFlag("Blank")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("control_accumulate");

    result.back()
          .write()
          .metadataSingleFlag("Bypass")
          .hash("Description")
          .setString("Accumulated quanties added to bypass");
    result.back()
          .write()
          .metadataSingleFlag("Bypass")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("control_accumulate");

    return result;
}

SequenceValue::Transfer ControlAccumulate::buildRealtimeMeta(double time)
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("control_accumulate");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    SequenceName::ComponentSet seen;
    int indexOrder = 0;
    for (const auto &t : config.front().targets) {
        for (const auto &v : t) {
            if (seen.count(v.variable))
                continue;

            result.emplace_back(SequenceName({}, "raw_meta", "Zt" + v.variable), Variant::Root(),
                                time, FP::undefined());
            result.back().write().metadataReal("Format").setString("0000000");
            result.back().write().metadataReal("Units").setString("s");
            result.back().write().metadataReal("Description").setString("Total accumulated time");
            result.back()
                  .write()
                  .metadataReal("Smoothing")
                  .hash("Mode")
                  .setString("DifferenceInitial");
            result.back().write().metadataReal("Source").set(instrumentMeta);
            result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
            result.back().write().metadataReal("Realtime").hash("GroupName").setString(v.variable);
            result.back().write().metadataReal("Realtime").hash("GroupColumn").setString("Time");
            result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(1);
            result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(indexOrder);

            result.emplace_back(SequenceName({}, "raw_meta", "Za" + v.variable), Variant::Root(),
                                time, FP::undefined());
            result.back().write().metadataBoolean("Description").setString("Active indicator");
            result.back().write().metadataBoolean("Smoothing").hash("Mode").setString("None");
            result.back().write().metadataBoolean("Source").set(instrumentMeta);
            result.back()
                  .write()
                  .metadataBoolean("Processing")
                  .toArray()
                  .after_back()
                  .set(processing);
            result.back()
                  .write()
                  .metadataBoolean("Realtime")
                  .hash("GroupName")
                  .setString(v.variable);
            result.back()
                  .write()
                  .metadataBoolean("Realtime")
                  .hash("GroupColumn")
                  .setString("Active");
            result.back()
                  .write()
                  .metadataBoolean("Translation")
                  .hash("TRUE")
                  .setString(QObject::tr("*", "active"));
            result.back()
                  .write()
                  .metadataBoolean("Translation")
                  .hash("FALSE")
                  .setString(QObject::tr("", "inactive"));
            result.back().write().metadataBoolean("Realtime").hash("ColumnOrder").setInt64(2);
            result.back().write().metadataBoolean("Realtime").hash("RowOrder").setInt64(indexOrder);

            seen.insert(v.variable);
        }
        ++indexOrder;
    }

    result.emplace_back(SequenceName({}, "raw_meta", "ZNEXT"), Variant::Root(), time,
                        FP::undefined());
    result.back()
          .write()
          .metadataHash("Description")
          .setString("The next accumulator advance state");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataHash("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataHash("Realtime").hash("Name").setString(QString());
    result.back().write().metadataHash("Realtime").hash("Units").setString(QString());
    result.back().write().metadataHash("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataHash("Realtime").hash("RowOrder").setInt64(indexOrder++);
    result.back()
          .write()
          .metadataHash("Realtime")
          .hash("HashFormat")
          .setString("Advance at ${TIME|Time}");

    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(indexOrder++);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Changing")
          .setString(QObject::tr("Changing...", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Blank")
          .setString(QObject::tr("Initial blanking", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("End")
          .setString(QObject::tr("STOPPED DUE TO COMPLETION", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BypassedRun")
          .setString(QObject::tr("BYPASSED", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BypassedChanging")
          .setString(QObject::tr("Changing and bypassed...", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BypassedBlank")
          .setString(QObject::tr("Initial blanking and bypassed", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BypassedEnd")
          .setString(QObject::tr("STOPPED DUE TO COMPLETION AND BYPASSED", "run state string"));

    return result;
}

SequenceMatch::Composite ControlAccumulate::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    sel.append({}, {}, "ZTOTAL");
    sel.append({}, {}, "ZNEXT");
    sel.append({}, {}, "Fn");
    sel.append({}, {}, "Ff");
    return sel;
}

void ControlAccumulate::setState(AcquisitionState *st)
{
    std::lock_guard<std::mutex> lock(readMutex);
    state = st;
}

void ControlAccumulate::setControlStream(AcquisitionControlStream *controlStream)
{ Q_UNUSED(controlStream); }

void ControlAccumulate::setRealtimeEgress(StreamSink *egress)
{
    realtimeEgress = egress;
    haveEmittedRealtimeMeta = false;
}

void ControlAccumulate::setLoggingEgress(StreamSink *egress)
{
    loggingEgress = egress;
    haveEmittedLogMeta = false;
}

void ControlAccumulate::setPersistentEgress(StreamSink *egress)
{
    persistentEgress = egress;
}

Variant::Root ControlAccumulate::getSourceMetadata()
{ return instrumentMeta; }

AcquisitionInterface::GeneralStatus ControlAccumulate::getGeneralStatus()
{
    switch (sampleState) {
    case SAMPLE_CHANGING:
    case SAMPLE_BLANK:
    case SAMPLE_RUN:
    case SAMPLE_BYPASSED_CHANGING:
    case SAMPLE_BYPASSED_BLANK:
    case SAMPLE_BYPASSED_RUN:
        break;
    case SAMPLE_END:
    case SAMPLE_BYPASSED_END:
        return AcquisitionInterface::GeneralStatus::Disabled;
    }
    return AcquisitionInterface::GeneralStatus::Normal;
}

void ControlAccumulate::incomingData(const Util::ByteView &,
                                     double time,
                                     AcquisitionInterface::IncomingDataType)
{
    advanceTime(time);
    if (state) {
        state->realtimeAdvanced(time);
        if (!FP::defined(priorUpdateTime)) {
            state->loggingHalted(time);
        } else if (!config.empty() && time - priorUpdateTime > config.front().updateTimeout) {
            priorUpdateTime = FP::undefined();
        }
    }
}

void ControlAccumulate::incomingControl(const Util::ByteView &,
                                        double time,
                                        AcquisitionInterface::IncomingDataType)
{
    advanceTime(time);
    if (state) {
        state->realtimeAdvanced(time);
        if (!FP::defined(priorUpdateTime)) {
            state->loggingHalted(time);
        } else if (!config.empty() && time - priorUpdateTime > config.front().updateTimeout) {
            priorUpdateTime = FP::undefined();
        }
    }
}

void ControlAccumulate::incomingTimeout(double time)
{
    advanceTime(time);
    if (state) {
        state->realtimeAdvanced(time);
        if (!FP::defined(priorUpdateTime)) {
            state->loggingHalted(time);
        } else if (!config.empty() && time - priorUpdateTime > config.front().updateTimeout) {
            priorUpdateTime = FP::undefined();
        }
    }
}

void ControlAccumulate::incomingAdvance(double time)
{
    advanceTime(time);
    if (state) {
        state->realtimeAdvanced(time);
        if (!FP::defined(priorUpdateTime)) {
            state->loggingHalted(time);
        } else if (!config.empty() && time - priorUpdateTime > config.front().updateTimeout) {
            priorUpdateTime = FP::undefined();
        }
    }
}

void ControlAccumulate::incomingCommand(const Variant::Read &command)
{
    if (command.hash("StartAccumulateChange").exists()) {
        switch (sampleState) {
        case SAMPLE_CHANGING:
        case SAMPLE_BYPASSED_CHANGING:
            qCDebug(log) << "Discarding change request in state:" << sampleState;
            break;
        case SAMPLE_BLANK:
        case SAMPLE_RUN:
        case SAMPLE_END: {
            realtimeStateUpdated = true;
            systemStateUpdated = true;
            resetPending = true;
            if (state)
                state->requestDataWakeup(0.0);
            qCDebug(log) << "Accumulator change started in state" << sampleState;

            Variant::Write info = Variant::Write::empty();
            if (sampleState == SAMPLE_RUN) {
                info.hash("State").setString("Run");
            } else if (sampleState == SAMPLE_END) {
                info.hash("State").setString("End");
            } else {
                info.hash("State").setString("Blank");
            }
            info.hash("Index").setInt64(accumulatingIndex);
            if (state) {
                state->event(Time::time(), QObject::tr("Accumulator change started."), false, info);
            }
            sampleState = SAMPLE_CHANGING;
            break;
        }
        case SAMPLE_BYPASSED_BLANK:
        case SAMPLE_BYPASSED_RUN:
        case SAMPLE_BYPASSED_END:
            realtimeStateUpdated = true;
            systemStateUpdated = true;
            systemStateUpdated = true;
            resetPending = true;
            if (state)
                state->requestDataWakeup(0.0);
            qCDebug(log) << "Bypassed accumulator change started in state" << sampleState;

            Variant::Write info = Variant::Write::empty();
            if (sampleState == SAMPLE_BYPASSED_RUN) {
                info.hash("State").setString("BypassedRun");
            } else if (sampleState == SAMPLE_BYPASSED_END) {
                info.hash("State").setString("BypassedEnd");
            } else {
                info.hash("State").setString("BypassedBlank");
            }
            info.hash("Index").setInt64(accumulatingIndex);
            if (state) {
                state->event(Time::time(), QObject::tr("Accumulator change started."), false, info);
            }
            sampleState = SAMPLE_BYPASSED_CHANGING;
            break;
        }
    }

    if (command.hash("EndAccumulateChange").exists()) {
        switch (sampleState) {
        case SAMPLE_CHANGING:
        case SAMPLE_BLANK:
        case SAMPLE_RUN:
        case SAMPLE_END: {
            realtimeStateUpdated = true;
            systemStateUpdated = true;
            resetPending = true;
            setStateEndTime = true;
            if (state)
                state->requestDataWakeup(0.0);
            qCDebug(log) << "Accumulator change ended in state" << sampleState;

            if (!config.empty() && INTEGER::defined(config.front().blankIndex))
                accumulatingIndex = config.front().blankIndex;

            Variant::Write info = Variant::Write::empty();
            if (sampleState == SAMPLE_CHANGING) {
                info.hash("State").setString("Changing");
            } else if (sampleState == SAMPLE_RUN) {
                info.hash("State").setString("Run");
            } else if (sampleState == SAMPLE_END) {
                info.hash("State").setString("End");
            } else {
                info.hash("State").setString("Blank");
            }
            if (state) {
                state->event(Time::time(), QObject::tr("Accumulator change ended."), false, info);
            }
            sampleState = SAMPLE_BLANK;
            break;
        }
        case SAMPLE_BYPASSED_CHANGING:
        case SAMPLE_BYPASSED_BLANK:
        case SAMPLE_BYPASSED_RUN:
        case SAMPLE_BYPASSED_END:
            realtimeStateUpdated = true;
            systemStateUpdated = true;
            resetPending = true;
            setStateEndTime = true;
            if (state)
                state->requestDataWakeup(0.0);
            qCDebug(log) << "Bypassed accumulator change ended in state" << sampleState;

            if (!config.empty() && INTEGER::defined(config.front().blankIndex))
                accumulatingIndex = config.front().blankIndex;

            Variant::Write info = Variant::Write::empty();
            if (sampleState == SAMPLE_BYPASSED_CHANGING) {
                info.hash("State").setString("BypassedChanging");
            } else if (sampleState == SAMPLE_BYPASSED_RUN) {
                info.hash("State").setString("BypassedRun");
            } else if (sampleState == SAMPLE_BYPASSED_END) {
                info.hash("State").setString("BypassedEnd");
            } else {
                info.hash("State").setString("BypassedBlank");
            }
            if (state) {
                state->event(Time::time(), QObject::tr("Accumulator change ended."), false, info);
            }
            sampleState = SAMPLE_BYPASSED_BLANK;
            break;
        }
    }

    if (command.hash("AdvanceAccumulator").exists()) {
        switch (sampleState) {
        case SAMPLE_CHANGING:
        case SAMPLE_BYPASSED_CHANGING:
        case SAMPLE_END:
        case SAMPLE_BYPASSED_END:
            qCDebug(log) << "Discarding advance request in state" << sampleState;
            break;
        case SAMPLE_BYPASSED_BLANK:
        case SAMPLE_BYPASSED_RUN: {
            stateEndTime = FP::undefined();
            setStateEndTime = true;
            if (!INTEGER::defined(accumulatingIndex) ||
                    (!config.empty() &&
                            static_cast<std::size_t>(accumulatingIndex + 1) >=
                                    config.front().targets.size())) {
                sampleState = SAMPLE_BYPASSED_END;
                qCDebug(log) << "Ending bypassed advance in state" << sampleState;
            } else {
                accumulatingIndex = accumulatingIndex + 1;
                qCDebug(log) << "Advancing bypassed to index " << accumulatingIndex << "in state"
                             << sampleState;
            }
            if (state)
                state->requestDataWakeup(0.0);
            break;
        }
        case SAMPLE_BLANK:
        case SAMPLE_RUN: {
            stateEndTime = FP::undefined();
            setStateEndTime = true;
            if (!INTEGER::defined(accumulatingIndex) ||
                    (!config.empty() &&
                            static_cast<std::size_t>(accumulatingIndex + 1) >=
                                    config.front().targets.size())) {
                sampleState = SAMPLE_END;
                qCDebug(log) << "Ending advance in state" << sampleState;
            } else {
                accumulatingIndex = accumulatingIndex + 1;
                qCDebug(log) << "Advancing to index" << accumulatingIndex << "in state"
                             << sampleState;
            }
            if (state)
                state->requestDataWakeup(0.0);
            break;
        }
        }
    }

    if (command.hash("Bypass").exists()) {
        switch (sampleState) {
        case SAMPLE_BYPASSED_CHANGING:
        case SAMPLE_BYPASSED_BLANK:
        case SAMPLE_BYPASSED_RUN:
        case SAMPLE_BYPASSED_END:
            qCDebug(log) << "Discarding bypass request in state: " << sampleState;
            break;
        case SAMPLE_CHANGING:
            qCDebug(log) << "Switching to bypassed changing state";
            sampleState = SAMPLE_BYPASSED_CHANGING;
            realtimeStateUpdated = true;
            systemStateUpdated = true;
            if (state)
                state->requestDataWakeup(0.0);
            break;
        case SAMPLE_BLANK:
            qCDebug(log) << "Switching to bypassed blank state";
            sampleState = SAMPLE_BYPASSED_BLANK;
            realtimeStateUpdated = true;
            systemStateUpdated = true;
            if (state)
                state->requestDataWakeup(0.0);
            break;
        case SAMPLE_RUN:
            qCDebug(log) << "Switching to bypassed run state";
            sampleState = SAMPLE_BYPASSED_RUN;
            realtimeStateUpdated = true;
            systemStateUpdated = true;
            if (state)
                state->requestDataWakeup(0.0);
            break;
        case SAMPLE_END:
            qCDebug(log) << "Switching to bypassed end state";
            sampleState = SAMPLE_BYPASSED_END;
            realtimeStateUpdated = true;
            systemStateUpdated = true;
            if (state)
                state->requestDataWakeup(0.0);
            break;
        }
    }

    if (command.hash("UnBypass").exists()) {
        switch (sampleState) {
        case SAMPLE_CHANGING:
        case SAMPLE_BLANK:
        case SAMPLE_RUN:
        case SAMPLE_END:
            qCDebug(log) << "Discarding unbypass request in state: " << sampleState;
            break;
        case SAMPLE_BYPASSED_CHANGING:
            qCDebug(log) << "Switching to unbypassed changing state";
            sampleState = SAMPLE_CHANGING;
            realtimeStateUpdated = true;
            systemStateUpdated = true;
            if (state)
                state->requestDataWakeup(0.0);
            break;
        case SAMPLE_BYPASSED_BLANK:
            qCDebug(log) << "Switching to unbypassed blank state";
            sampleState = SAMPLE_BLANK;
            realtimeStateUpdated = true;
            systemStateUpdated = true;
            if (state)
                state->requestDataWakeup(0.0);
            break;
        case SAMPLE_BYPASSED_RUN:
            qCDebug(log) << "Switching to unbypassed run state";
            sampleState = SAMPLE_RUN;
            realtimeStateUpdated = true;
            systemStateUpdated = true;
            if (state)
                state->requestDataWakeup(0.0);
            break;
        case SAMPLE_BYPASSED_END:
            qCDebug(log) << "Switching to unbypassed end state";
            sampleState = SAMPLE_END;
            realtimeStateUpdated = true;
            systemStateUpdated = true;
            if (state)
                state->requestDataWakeup(0.0);
            break;
        }
    }
}

Variant::Root ControlAccumulate::getCommands()
{
    Variant::Root result;

    result["StartAccumulateChange"].hash("DisplayName").setString("&Start change");
    result["StartAccumulateChange"].hash("DisplayPriority").setInt64(0);
    result["StartAccumulateChange"].hash("ToolTip").setString("Start the accumulator change.");
    result["StartAccumulateChange"].hash("Include").array(0).hash("Type").setString("Flags");
    result["StartAccumulateChange"].hash("Exclude")
                                   .array(0)
                                   .hash("Flags")
                                   .setFlags({"AccumulatorChanging"});
    result["StartAccumulateChange"].hash("Exclude").array(0).hash("Variable").setString("F1");

    result["EndAccumulateChange"].hash("DisplayName").setString("&End change");
    result["EndAccumulateChange"].hash("DisplayPriority").setInt64(0);
    result["EndAccumulateChange"].hash("ToolTip").setString("End the accumulator change.");
    result["EndAccumulateChange"].hash("Include").array(0).hash("Type").setString("Flags");
    result["EndAccumulateChange"].hash("Include")
                                 .array(0)
                                 .hash("Flags")
                                 .setFlags({"AccumulatorChanging"});
    result["EndAccumulateChange"].hash("Include").array(0).hash("Variable").setString("F1");

    result["AdvanceAccumulator"].hash("DisplayName").setString("&Advance");
    result["AdvanceAccumulator"].hash("DisplayPriority").setInt64(1);
    result["AdvanceAccumulator"].hash("ToolTip")
                                .setString(
                                        "Advance to the next accumulator target.  This increases the active index by one.");
    result["AdvanceAccumulator"].hash("Exclude").array(0).hash("Type").setString("Flags");
    result["AdvanceAccumulator"].hash("Exclude")
                                .array(0)
                                .hash("Flags")
                                .setFlags({"AccumulatorChanging"});
    result["AdvanceAccumulator"].hash("Exclude").array(0).hash("Variable").hash("F1");
    if (!config.empty() && !config.front().targets.empty()) {
        result["AdvanceAccumulator"].hash("Include").array(0).hash("Type").setString("LessThan");
        result["AdvanceAccumulator"].hash("Include")
                                    .array(0)
                                    .hash("Value")
                                    .setInt64(config.front().targets.size());
        result["AdvanceAccumulator"].hash("Include").array(0).hash("Variable").setString("Fn");
    }

    return result;
}

StreamSink *ControlAccumulate::getRealtimeIngress()
{ return &realtimeIngress; }

ControlAccumulate::RealtimeIngress::RealtimeIngress(ControlAccumulate *ce) : control(ce),
                                                                             realtimeMutex(),
                                                                             isFinished(false)
{ }

ControlAccumulate::RealtimeIngress::~RealtimeIngress() = default;

const std::vector<
        CPD3::Data::StreamSink *> &ControlAccumulate::RealtimeIngress::lookup(const CPD3::Data::SequenceName &name)
{
    auto d = dispatch.find(name);
    if (d != dispatch.end())
        return d->second;

    std::vector<StreamSink *> destination;
    for (const auto &check : targets) {
        if (!check.first->matches(name))
            continue;
        destination.emplace_back(check.second);
    }
    return dispatch.emplace(name, std::move(destination)).first->second;
}

void ControlAccumulate::RealtimeIngress::incomingData(const SequenceValue::Transfer &values)
{
    std::lock_guard<std::mutex> lock(realtimeMutex);
    for (const auto &v : values) {
        for (auto t : lookup(v.getName())) {
            t->incomingData(v);
        }
    }
}

void ControlAccumulate::RealtimeIngress::incomingData(SequenceValue::Transfer &&values)
{
    std::lock_guard<std::mutex> lock(realtimeMutex);
    for (auto &v : values) {
        const auto &targets = lookup(v.getName());
        if (targets.empty())
            continue;

        auto t = targets.begin();
        for (auto end = targets.end() - 1; t != end; ++t) {
            (*t)->incomingData(v);
        }
        (*t)->incomingData(std::move(v));
    }
}

void ControlAccumulate::RealtimeIngress::incomingData(const SequenceValue &v)
{
    std::lock_guard<std::mutex> lock(realtimeMutex);
    for (auto t : lookup(v.getName())) {
        t->incomingData(v);
    }
}

void ControlAccumulate::RealtimeIngress::incomingData(SequenceValue &&v)
{
    std::lock_guard<std::mutex> lock(realtimeMutex);
    const auto &targets = lookup(v.getName());
    if (targets.empty())
        return;

    auto t = targets.begin();
    for (auto end = targets.end() - 1; t != end; ++t) {
        (*t)->incomingData(v);
    }
    (*t)->incomingData(std::move(v));
}

void ControlAccumulate::RealtimeIngress::endData()
{ }

void ControlAccumulate::RealtimeIngress::finish()
{
    std::lock_guard<std::mutex> lock(realtimeMutex);
    isFinished = true;
    for (const auto &t : targets) {
        t.second->endData();
    }
}

void ControlAccumulate::RealtimeIngress::issueArchiveRead(const Archive::Selection::List &selections,
                                                          StreamSink *target)
{
    std::lock_guard<std::mutex> lock(realtimeMutex);
    if (isFinished) {
        target->endData();
        return;
    }
    targets.emplace_back(SequenceMatch::Basic::compile(selections), target);
    dispatch.clear();
}


ControlAccumulate::ReadIngress::ReadIngress(ControlAccumulate *ce) : control(ce),
                                                                     dispatch(),
                                                                     lastIncomingTime(
                                                                             FP::undefined())
{ }

ControlAccumulate::ReadIngress::~ReadIngress() = default;

bool ControlAccumulate::ReadIngress::lookup(const CPD3::Data::SequenceName &name)
{
    auto check = dispatch.find(name);
    if (check != dispatch.end())
        return check->second;

    bool accept = false;

    for (const auto &i : control->config.front().targets) {
        for (const auto &t : i) {
            if (t.rate->registerInput(name))
                accept = true;
        }
    }

    dispatch.emplace(name, accept);
    return accept;
}

void ControlAccumulate::ReadIngress::incomingData(const SequenceValue::Transfer &values)
{
    bool updated = false;

    std::lock_guard<std::mutex> lock(control->readMutex);
    for (const auto &v : values) {
        if (!lookup(v.getName()))
            continue;

        SequenceValue av = v;

        if (Range::compareStart(lastIncomingTime, av.getStart()) > 0) {
            av.setStart(lastIncomingTime);
        } else {
            lastIncomingTime = av.getStart();
        }

        auto add = control->reader.add(std::move(av));
        if (add.empty())
            continue;
        Util::append(std::move(add), control->pendingSegments);
        updated = true;
    }

    if (!updated)
        return;
    if (control->state)
        control->state->requestDataWakeup(0.0);
}

void ControlAccumulate::ReadIngress::incomingData(SequenceValue::Transfer &&values)
{
    bool updated = false;

    std::lock_guard<std::mutex> lock(control->readMutex);
    for (auto &v : values) {
        if (!lookup(v.getName()))
            continue;

        if (Range::compareStart(lastIncomingTime, v.getStart()) > 0) {
            v.setStart(lastIncomingTime);
        } else {
            lastIncomingTime = v.getStart();
        }

        auto add = control->reader.add(std::move(v));
        if (add.empty())
            continue;
        Util::append(std::move(add), control->pendingSegments);
        updated = true;
    }

    if (!updated)
        return;
    if (control->state)
        control->state->requestDataWakeup(0.0);
}

void ControlAccumulate::ReadIngress::incomingData(const SequenceValue &v)
{
    std::lock_guard<std::mutex> lock(control->readMutex);
    if (!lookup(v.getName()))
        return;

    SequenceValue av = v;

    if (Range::compareStart(lastIncomingTime, av.getStart()) > 0) {
        av.setStart(lastIncomingTime);
    } else {
        lastIncomingTime = av.getStart();
    }

    auto add = control->reader.add(std::move(av));
    if (add.empty())
        return;
    Util::append(std::move(add), control->pendingSegments);
    if (control->state)
        control->state->requestDataWakeup(0.0);
}

void ControlAccumulate::ReadIngress::incomingData(SequenceValue &&v)
{
    std::lock_guard<std::mutex> lock(control->readMutex);
    if (!lookup(v.getName()))
        return;

    if (Range::compareStart(lastIncomingTime, v.getStart()) > 0) {
        v.setStart(lastIncomingTime);
    } else {
        lastIncomingTime = v.getStart();
    }

    auto add = control->reader.add(std::move(v));
    if (add.empty())
        return;
    Util::append(std::move(add), control->pendingSegments);
    if (control->state)
        control->state->requestDataWakeup(0.0);
}

void ControlAccumulate::ReadIngress::endData()
{
    std::lock_guard<std::mutex> lock(control->readMutex);
    auto add = control->reader.finish();
    if (add.empty())
        return;
    Util::append(std::move(add), control->pendingSegments);
    if (control->state)
        control->state->requestDataWakeup(0.0);
}

void ControlAccumulate::ReadIngress::reset()
{
    dispatch.clear();
}

AcquisitionInterface::AutomaticDefaults ControlAccumulate::getDefaults()
{
    AutomaticDefaults result;
    result.name = "X$2";
    return result;
}

bool ControlAccumulate::isCompleted()
{ return terminated; }

void ControlAccumulate::signalTerminate()
{
    terminated = true;
    completed();
}


std::unique_ptr<
        AcquisitionInterface> ControlAccumulateComponent::createAcquisitionPassive(const ComponentOptions &,
                                                                                   const std::string &loggingContext)
{
    qWarning() << "Option based creation is not supported";
    return std::unique_ptr<AcquisitionInterface>(
            new ControlAccumulate(ValueSegment::Transfer(), loggingContext));
}

std::unique_ptr<
        AcquisitionInterface> ControlAccumulateComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                   const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(new ControlAccumulate(config, loggingContext));
}

std::unique_ptr<
        AcquisitionInterface> ControlAccumulateComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{ return {}; }

