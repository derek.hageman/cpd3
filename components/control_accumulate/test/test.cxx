/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/timeutils.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ControlTestState : public AcquisitionState {
public:
    Variant::Flags bypassFlags;
    Variant::Flags systemFlags;
    double flushTime;
    std::unordered_map<std::string, Variant::Read> commands;
    SequenceName::Flavors flavors;
    double wakeupTime;

    ControlTestState()
            : bypassFlags(),
              systemFlags(),
              flushTime(FP::undefined()),
              commands(),
              flavors(),
              wakeupTime(FP::undefined())
    { }

    virtual ~ControlTestState() = default;

    void setBypassFlag(const Variant::Flag &flag) override
    { bypassFlags.insert(flag); }

    void clearBypassFlag(const Variant::Flag &flag) override
    { bypassFlags.erase(flag); }

    void setSystemFlag(const Variant::Flag &flag) override
    { systemFlags.insert(flag); }

    void clearSystemFlag(const Variant::Flag &flag) override
    { systemFlags.erase(flag); }

    void requestFlush(double seconds) override
    { flushTime = seconds; }

    void sendCommand(const std::string &target, const Variant::Read &command) override
    { Util::insert_or_assign(commands, target, Variant::Root(command).read()); }

    void setSystemFlavors(const SequenceName::Flavors &flavors) override
    { this->flavors = flavors; }

    void setAveragingTime(Time::LogicalTimeUnit, int, bool setAligned) override
    { }

    void requestStateSave() override
    { }

    void requestGlobalStateSave() override
    { }

    void event(double, const QString &, bool, const Variant::Read &) override
    { }

    void realtimeAdvanced(double) override
    { }

    void requestDataWakeup(double time) override
    {
        wakeupTime = time;
    }

    void requestControlWakeup(double time) override
    {
        wakeupTime = time;
    }
};

class TestComponent : public QObject {
Q_OBJECT
    AcquisitionComponent *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component =
                qobject_cast<AcquisitionComponent *>(ComponentLoader::create("control_accumulate"));
        QVERIFY(component);
    }

    void basic()
    {
        Variant::Root config;

        config["BypassIndex"] = 0;
        config["BlankIndex"] = 1;
        config["StartIndex"] = 2;
        config["Advance/Unit"] = "Day";
        config["Advance/Count"] = 2;
        config["Advance/Align"] = true;
        config["BlankTime/Unit"] = "Second";
        config["BlankTime/Count"] = 10;
        config["UpdateTimeout"] = 7 * 86400.0;

        config["Processing/Additional/#0/Variable"] = "Q_Q11";

        config["Accumulators/#0/Name"] = "Qt0";
        config["Accumulators/#0/Activate/InstrumentCommands/S11"] = "Activate0";
        config["Accumulators/#0/Deactivate/InstrumentCommands/S11"] = "Deactivate0";
        config["Accumulators/#0/Rate"] = "Q_Q11";
        config["Accumulators/#0/AccumulateWhileBypassed"] = true;
        config["Accumulators/#0/Metadata"].metadataReal("Foo").setString("Bar0");

        config["Accumulators/#1/Name"] = "Qt1";
        config["Accumulators/#1/Activate/InstrumentCommands/S11"] = "Activate1";
        config["Accumulators/#1/Deactivate/InstrumentCommands/S11"] = "Deactivate1";
        config["Accumulators/#1/Rate"] = "Q_Q11";
        config["Accumulators/#1/Metadata"].metadataReal("Foo").setString("Bar1");

        config["Accumulators/#2/Name"] = "Qt2";
        config["Accumulators/#2/Activate/InstrumentCommands/S11"] = "Activate2";
        config["Accumulators/#2/Deactivate/InstrumentCommands/S11"] = "Deactivate2";
        config["Accumulators/#2/Rate"] = "Q_Q11";
        config["Accumulators/#2/Metadata"].metadataReal("Foo").setString("Bar2");

        config["Accumulators/#3/Name"] = "Qt3";
        config["Accumulators/#3/Activate/InstrumentCommands/S11"] = "Activate3";
        config["Accumulators/#3/Deactivate/InstrumentCommands/S11"] = "Deactivate3";
        config["Accumulators/#3/Rate"] = "Q_Q11";
        config["Accumulators/#3/Metadata"].metadataReal("Foo").setString("Bar3");

        config["Accumulators/#4/Name"] = "Qt4";
        config["Accumulators/#4/Activate/InstrumentCommands/S11"] = "Activate4";
        config["Accumulators/#4/Deactivate/InstrumentCommands/S11"] = "Deactivate4";
        config["Accumulators/#4/Rate"] = "Q_Q11";
        config["Accumulators/#4/Metadata"].metadataReal("Foo").setString("Bar4");

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        StreamCapture realtime;
        interface->setRealtimeEgress(&realtime);
        StreamCapture logging;
        interface->setLoggingEgress(&logging);
        StreamCapture persistent;
        interface->setPersistentEgress(&persistent);

        /* Make sure the chain is started before we start adding data */
        QTest::qSleep(250);

        {
            Variant::Write cmd = Variant::Write::empty();
            cmd["EndAccumulateChange"] = true;
            interface->incomingCommand(cmd);
        }

        interface->incomingAdvance(86400.0);
        QCOMPARE(state.wakeupTime, 86410.0);

        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(1.0),
                                              86400,
                                              86401));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(1.0),
                                              86401,
                                              86402));
        interface->incomingAdvance(86401.0);
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(1.0),
                                              86402,
                                              86403));
        interface->incomingAdvance(86402.0);

        QTest::qSleep(50);

        QVERIFY(!realtime.values().empty());
        QVERIFY(realtime.latestTime() >= 86400.0);
        QVERIFY(realtime.hasMeta("ZTOTAL", Variant::Root()));
        QVERIFY(realtime.hasMeta("ZSTATE", Variant::Root()));
        QVERIFY(realtime.hasMeta("ZNEXT", Variant::Root()));
        QVERIFY(realtime.hasMeta("F1", Variant::Root()));
        QVERIFY(realtime.hasMeta("Fn", Variant::Root()));
        QVERIFY(realtime.hasMeta("Ff", Variant::Root()));
        QVERIFY(realtime.hasMeta("Qt0", Variant::Root("Bar0"), "^Foo"));
        QVERIFY(realtime.hasMeta("Qt1", Variant::Root("Bar1"), "^Foo"));
        QVERIFY(realtime.hasMeta("Qt2", Variant::Root("Bar2"), "^Foo"));
        QVERIFY(realtime.hasMeta("Qt3", Variant::Root("Bar3"), "^Foo"));
        QVERIFY(realtime.hasMeta("Qt4", Variant::Root("Bar4"), "^Foo"));
        QVERIFY(realtime.hasMeta("ZaQt0", Variant::Root()));
        QVERIFY(realtime.hasMeta("ZaQt1", Variant::Root()));
        QVERIFY(realtime.hasMeta("ZaQt2", Variant::Root()));
        QVERIFY(realtime.hasMeta("ZaQt3", Variant::Root()));
        QVERIFY(realtime.hasMeta("ZaQt4", Variant::Root()));
        QVERIFY(realtime.hasMeta("ZtQt0", Variant::Root()));
        QVERIFY(realtime.hasMeta("ZtQt1", Variant::Root()));
        QVERIFY(realtime.hasMeta("ZtQt2", Variant::Root()));
        QVERIFY(realtime.hasMeta("ZtQt3", Variant::Root()));
        QVERIFY(realtime.hasMeta("ZtQt4", Variant::Root()));

        QVERIFY(realtime.hasValue("F1", Variant::Root()));
        QVERIFY(realtime.hasValue("Fn", Variant::Root()));
        QVERIFY(realtime.hasValue("Ff", Variant::Root((qint64) 86400)));
        QVERIFY(realtime.hasValue("Qt1", Variant::Root(2.0)));
        QVERIFY(realtime.hasValue("ZaQt1", Variant::Root(true)));
        QVERIFY(realtime.hasValue("ZtQt1", Variant::Root(1.0)));
        QVERIFY(realtime.hasValue("ZaQt2", Variant::Root(false)));
        QVERIFY(realtime.hasValue("ZNEXT", Variant::Root(86410.0), "Time"));
        QVERIFY(realtime.hasValue("ZSTATE", Variant::Root("Blank")));

        QVERIFY(logging.hasMeta("ZTOTAL", Variant::Root()));
        QVERIFY(logging.hasMeta("F1", Variant::Root()));
        QVERIFY(logging.hasMeta("Fn", Variant::Root()));
        QVERIFY(logging.hasMeta("Ff", Variant::Root()));
        QVERIFY(logging.hasMeta("Qt0", Variant::Root("Bar0"), "^Foo"));
        QVERIFY(logging.hasMeta("Qt1", Variant::Root("Bar1"), "^Foo"));
        QVERIFY(logging.hasMeta("Qt2", Variant::Root("Bar2"), "^Foo"));
        QVERIFY(logging.hasMeta("Qt3", Variant::Root("Bar3"), "^Foo"));

        QVERIFY(logging.hasValue("Qt1"));
        QVERIFY(!logging.hasValue("Qt2"));

        QCOMPARE((int) persistent.values().size(), 0);
        QCOMPARE((int) interface->getPersistentValues().size(), 3);

        realtime.reset();
        logging.reset();
        interface->incomingAdvance(86410.0);
        QCOMPARE(state.wakeupTime, 259200.0);
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(1.0),
                                              86410,
                                              86411));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(1.0),
                                              86411,
                                              86412));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(1.0),
                                              86412,
                                              86413));
        interface->incomingAdvance(86412.0);


        QVERIFY(realtime.hasValue("F1", Variant::Root()));
        QVERIFY(realtime.hasValue("Fn", Variant::Root(2)));
        QVERIFY(realtime.hasValue("Qt2", Variant::Root(2.0)));
        QVERIFY(realtime.hasValue("ZaQt1", Variant::Root(false)));
        QVERIFY(realtime.hasValue("ZaQt2", Variant::Root(true)));
        QVERIFY(realtime.hasValue("ZtQt1", Variant::Root(2.0)));
        QVERIFY(realtime.hasValue("ZtQt2", Variant::Root(2.0)));
        QVERIFY(realtime.hasValue("ZNEXT", Variant::Root(259200.0), "Time"));
        QVERIFY(realtime.hasValue("ZSTATE", Variant::Root("Run")));

        QVERIFY(!logging.hasValue("Qt1"));
        QVERIFY(logging.hasValue("Qt2"));
        QVERIFY(persistent.hasValue("Fn", Variant::Root(1)));

        realtime.reset();
        logging.reset();
        persistent.reset();
        interface->incomingAdvance(259200.0);
        QCOMPARE(state.wakeupTime, 432000.0);

        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(10.0),
                                              259200,
                                              259201));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(10.0),
                                              259201,
                                              259202));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(10.0),
                                              259203,
                                              259204));
        interface->incomingAdvance(259203.0);

        QVERIFY(realtime.hasValue("F1", Variant::Root()));
        QVERIFY(realtime.hasValue("Fn", Variant::Root(3)));
        QVERIFY(realtime.hasValue("Qt3", Variant::Root(20.0)));
        QVERIFY(realtime.hasValue("ZaQt2", Variant::Root(false)));
        QVERIFY(realtime.hasValue("ZaQt3", Variant::Root(true)));
        QVERIFY(realtime.hasValue("ZtQt1", Variant::Root(2.0)));
        QVERIFY(realtime.hasValue("ZtQt2", Variant::Root(2.0)));
        QVERIFY(realtime.hasValue("ZtQt3", Variant::Root(2.0)));
        QVERIFY(realtime.hasValue("ZNEXT", Variant::Root(432000.0), "Time"));

        QVERIFY(!logging.hasValue("Qt2"));
        QVERIFY(logging.hasValue("Qt3"));
        QVERIFY(persistent.hasValue("Fn", Variant::Root(2)));

        {
            Variant::Write cmd = Variant::Write::empty();
            cmd["AdvanceAccumulator"] = true;
            interface->incomingCommand(cmd);
        }

        realtime.reset();
        logging.reset();
        persistent.reset();
        interface->incomingAdvance(259204.0);
        QCOMPARE(state.wakeupTime, 432000.0);
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(10.0),
                                              259204,
                                              259205));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(10.0),
                                              259205,
                                              259206));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(10.0),
                                              259206,
                                              259207));
        interface->incomingAdvance(259206.0);

        QVERIFY(realtime.hasValue("F1", Variant::Root()));
        QVERIFY(realtime.hasValue("Fn", Variant::Root(4)));
        QVERIFY(realtime.hasValue("Qt4", Variant::Root(20.0)));
        QVERIFY(realtime.hasValue("ZaQt3", Variant::Root(false)));
        QVERIFY(realtime.hasValue("ZaQt4", Variant::Root(true)));
        QVERIFY(realtime.hasValue("ZtQt1", Variant::Root(2.0)));
        QVERIFY(realtime.hasValue("ZtQt2", Variant::Root(2.0)));
        QVERIFY(realtime.hasValue("ZtQt3", Variant::Root(2.0)));
        QVERIFY(realtime.hasValue("ZtQt4", Variant::Root(2.0)));
        QVERIFY(realtime.hasValue("ZNEXT", Variant::Root(432000.0), "Time"));

        QVERIFY(!logging.hasValue("Qt3"));
        QVERIFY(logging.hasValue("Qt4"));
        QVERIFY(persistent.hasValue("Fn", Variant::Root(3)));

        realtime.reset();
        logging.reset();
        persistent.reset();
        interface->incomingAdvance(432000.0);
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(10.0),
                                              432000,
                                              432001));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(10.0),
                                              432001,
                                              432002));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(10.0),
                                              432002,
                                              432003));
        interface->incomingAdvance(432002.0);

        QVERIFY(realtime.hasValue("F1", Variant::Root()));
        QVERIFY(realtime.hasValue("Fn", Variant::Root(4)));
        QVERIFY(realtime.hasValue("Qt4", Variant::Root(50.0)));
        QVERIFY(realtime.hasValue("ZaQt4", Variant::Root(true)));
        QVERIFY(realtime.hasValue("ZtQt4", Variant::Root(5.0)));
        QVERIFY(realtime.hasValue("ZSTATE", Variant::Root("End")));

        QVERIFY(logging.hasValue("Qt4"));


        realtime.reset();
        logging.reset();
        persistent.reset();

        {
            Variant::Write cmd = Variant::Write::empty();
            cmd["StartAccumulateChange"] = true;
            interface->incomingCommand(cmd);
        }
        interface->incomingAdvance(432003.0);
        QVERIFY(realtime.hasValue("ZSTATE", Variant::Root("Changing")));
        QVERIFY(persistent.hasValue("ZTOTAL", Variant::Root()));

        {
            Variant::Write cmd = Variant::Write::empty();
            cmd["EndAccumulateChange"] = true;
            cmd["Bypass"] = true;
            interface->incomingCommand(cmd);
        }
        interface->incomingAdvance(432004.0);
        QVERIFY(realtime.hasValue("ZSTATE", Variant::Root("BypassedBlank")));
        QCOMPARE(state.wakeupTime, 432014.0);

        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(1.0),
                                              432004,
                                              432005));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(1.0),
                                              432005,
                                              432006));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(1.0),
                                              432006,
                                              432007));
        interface->incomingAdvance(432006.0);

        QVERIFY(realtime.hasValue("Fn", Variant::Root(1)));
        QVERIFY(realtime.hasValue("Ff", Variant::Root((qint64) 432004)));
        QVERIFY(realtime.hasValue("Qt0", Variant::Root(2.0)));
        QVERIFY(realtime.hasValue("ZaQt0", Variant::Root(true)));
        QVERIFY(realtime.hasValue("ZtQt0", Variant::Root(2.0)));
        QVERIFY(realtime.hasValue("ZNEXT", Variant::Root(432014.0), "Time"));

        QVERIFY(persistent.hasValue("Ff", Variant::Root((qint64) 86400)));
        QVERIFY(persistent.hasValue("ZTOTAL", Variant::Root()));

        realtime.reset();
        logging.reset();
        persistent.reset();
        interface->incomingAdvance(432014.0);
        QVERIFY(realtime.hasValue("ZSTATE", Variant::Root("BypassedRun")));
        QCOMPARE(state.wakeupTime, 604800.0);

        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(1.0),
                                              432014,
                                              432015));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(1.0),
                                              432015,
                                              432016));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(1.0),
                                              432016,
                                              432017));
        interface->incomingAdvance(432016.0);

        QVERIFY(realtime.hasValue("Fn", Variant::Root(2)));
        QVERIFY(realtime.hasValue("Qt0", Variant::Root(5.0)));
        QVERIFY(realtime.hasValue("ZaQt0", Variant::Root(true)));
        QVERIFY(realtime.hasValue("ZtQt0", Variant::Root(5.0)));
        QVERIFY(realtime.hasValue("ZNEXT", Variant::Root(604800.0), "Time"));

        realtime.reset();
        logging.reset();
        persistent.reset();
        interface->incomingAdvance(604800.0);
        QCOMPARE(state.wakeupTime, 777600.0);

        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(1.0),
                                              604801,
                                              604802));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(1.0),
                                              604802,
                                              604803));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(1.0),
                                              604803,
                                              604804));
        interface->incomingAdvance(604805.0);

        QVERIFY(realtime.hasValue("Fn", Variant::Root(3)));
        QVERIFY(realtime.hasValue("Qt0", Variant::Root(8.0)));
        QVERIFY(realtime.hasValue("ZaQt0", Variant::Root(true)));
        QVERIFY(realtime.hasValue("ZtQt0", Variant::Root(8.0)));
        QVERIFY(realtime.hasValue("ZNEXT", Variant::Root(777600.0), "Time"));


        realtime.reset();
        logging.reset();
        persistent.reset();
        {
            Variant::Write cmd = Variant::Write::empty();
            cmd["UnBypass"] = true;
            interface->incomingCommand(cmd);
        }
        interface->incomingAdvance(604806.0);

        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(10.0),
                                              604806,
                                              604807));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(10.0),
                                              604808,
                                              604809));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(10.0),
                                              604809,
                                              604810));
        interface->incomingAdvance(604810.0);

        QVERIFY(realtime.hasValue("Fn", Variant::Root(3)));
        QVERIFY(realtime.hasValue("Qt3", Variant::Root(20.0)));
        QVERIFY(realtime.hasValue("ZaQt0", Variant::Root(false)));
        QVERIFY(realtime.hasValue("ZaQt3", Variant::Root(true)));

        QVERIFY(persistent.hasValue("Fn", Variant::Root(0)));
    }

    void bypassOnEnd()
    {
        Variant::Root config;

        config["BypassIndex"] = 0;
        config["StartIndex"] = 1;
        config["BypassOnEnd"] = true;
        config["Advance/Unit"] = "Day";
        config["Advance/Count"] = 1;
        config["Advance/Align"] = true;
        config["UpdateTimeout"] = 7 * 86400.0;

        config["Processing/Additional/#0/Variable"] = "Q_Q11";

        config["Accumulators/#0/Name"] = "Qt0";
        config["Accumulators/#0/Activate/InstrumentCommands/S11"] = "Activate0";
        config["Accumulators/#0/Deactivate/InstrumentCommands/S11"] = "Deactivate0";
        config["Accumulators/#0/AccumulateWhileBypassed"] = true;
        config["Accumulators/#0/Rate"] = "Q_Q11";
        config["Accumulators/#0/Metadata"].metadataReal("Foo").setString("Bar0");

        config["Accumulators/#1/Name"] = "Qt1";
        config["Accumulators/#1/Activate/InstrumentCommands/S11"] = "Activate1";
        config["Accumulators/#1/Deactivate/InstrumentCommands/S11"] = "Deactivate1";
        config["Accumulators/#1/Rate"] = "Q_Q11";
        config["Accumulators/#1/Metadata"].metadataReal("Foo").setString("Bar1");

        config["Accumulators/#2/Name"] = "Qt2";
        config["Accumulators/#2/Activate/InstrumentCommands/S11"] = "Activate2";
        config["Accumulators/#2/Deactivate/InstrumentCommands/S11"] = "Deactivate2";
        config["Accumulators/#2/Rate"] = "Q_Q11";
        config["Accumulators/#2/Metadata"].metadataReal("Foo").setString("Bar2");

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        StreamCapture realtime;
        interface->setRealtimeEgress(&realtime);
        StreamCapture logging;
        interface->setLoggingEgress(&logging);
        StreamCapture persistent;
        interface->setPersistentEgress(&persistent);

        /* Make sure the chain is started before we start adding data */
        QTest::qSleep(250);

        {
            Variant::Write cmd = Variant::Write::empty();
            cmd["EndAccumulateChange"] = true;
            interface->incomingCommand(cmd);
        }

        interface->incomingAdvance(86400.0);
        QCOMPARE(state.wakeupTime, 172800.0);

        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(1.0), 86400, 86401));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(1.0), 86401, 86402));

        interface->incomingAdvance(172800.0);
        QCOMPARE(state.wakeupTime, 259200.0);

        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(2.0), 172800, 172801));
        interface->getRealtimeIngress()
                 ->incomingData(SequenceValue(SequenceName("brw", "rt_instant", "Q_Q11"),
                                              Variant::Root(2.0), 172801, 172802));

        QVERIFY(state.bypassFlags.size() == 0);

        interface->incomingAdvance(259200.0);

        QVERIFY(state.bypassFlags.size() != 0);
    }

    void serialize()
    {
        Variant::Root config;

        config["StartIndex"] = 0;
        config["Advance/Unit"] = "Day";
        config["Advance/Count"] = 2;
        config["Advance/Align"] = true;
        config["UpdateTimeout"] = 7 * 86400.0;

        config["Processing/Additional/#0/Variable"] = "Q_Q11";

        config["Accumulators/#0/Name"] = "Qt0";
        config["Accumulators/#0/Rate"] = "Q_Q11";

        config["Accumulators/#1/Name"] = "Qt1";
        config["Accumulators/#1/Rate"] = "Q_Q11";

        config["Accumulators/#2/Name"] = "Qt1";
        config["Accumulators/#2/Rate"] = "Q_Q11";


        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        StreamCapture realtime;
        interface->setRealtimeEgress(&realtime);
        StreamCapture logging;
        interface->setLoggingEgress(&logging);
        StreamCapture persistent;
        interface->setPersistentEgress(&persistent);

        /* Make sure the chain is started before we start adding data */
        QTest::qSleep(250);

        {
            Variant::Write cmd = Variant::Write::empty();
            cmd["EndAccumulateChange"] = true;
            interface->incomingCommand(cmd);
        }


        interface->incomingAdvance(86400.0);
        QCOMPARE(state.wakeupTime, 259200.0);
        interface->incomingAdvance(86401.0);
        QVERIFY(realtime.hasValue("ZNEXT", Variant::Root(259200.0), "Time"));

        state.wakeupTime = 0;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface = component->createAcquisitionInteractive(
                    {ValueSegment(FP::undefined(), FP::undefined(), config)});
            interface->start();
            interface->setRealtimeEgress(&realtime);
            interface->setLoggingEgress(&logging);
            interface->setPersistentEgress(&persistent);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }
            interface->setState(&state);
        }

        interface->incomingAdvance(86402.0);
        QCOMPARE(state.wakeupTime, 259200.0);
        QVERIFY(realtime.hasValue("ZNEXT", Variant::Root(259200.0), "Time"));

        interface->incomingAdvance(259200.0);
        QCOMPARE(state.wakeupTime, 432000.0);
        interface->incomingAdvance(432001.0);
        QVERIFY(realtime.hasValue("ZNEXT", Variant::Root(432000.0), "Time"));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
