/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CONTROLACCUMULATE_H
#define CONTROLACCUMULATE_H

#include "core/first.hxx"

#include <vector>
#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QHash>
#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/timeutils.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/processingtapchain.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"

class ControlAccumulate : public CPD3::Acquisition::AcquisitionInterface {
    std::string loggingName;
    QLoggingCategory log;

    bool terminated;
    CPD3::Acquisition::AcquisitionState *state;
    CPD3::Data::StreamSink *realtimeEgress;
    CPD3::Data::StreamSink *loggingEgress;
    CPD3::Data::StreamSink *persistentEgress;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;

    enum {
        SAMPLE_CHANGING, SAMPLE_BLANK, SAMPLE_RUN, SAMPLE_END,

        SAMPLE_BYPASSED_CHANGING, SAMPLE_BYPASSED_BLANK, SAMPLE_BYPASSED_RUN, SAMPLE_BYPASSED_END,
    } sampleState;
    double stateEndTime;
    bool realtimeStateUpdated;
    bool realtimeNextUpdated;
    bool systemStateUpdated;
    bool setStateEndTime;
    bool resetPending;

    qint64 accumulatingIndex;
    qint64 priorActiveIndex;
    double discardDataEnd;

    double priorUpdateTime;

    CPD3::Data::Variant::Root instrumentMeta;

    class RealtimeIngress
            : public CPD3::Data::StreamSink, public CPD3::Data::ProcessingTapChain::ArchiveBackend {
        ControlAccumulate *control;
        std::mutex realtimeMutex;
        bool isFinished;

        std::vector<std::pair<std::unique_ptr<CPD3::Data::SequenceMatch::Basic>,
                              CPD3::Data::StreamSink *>> targets;
        CPD3::Data::SequenceName::Map<std::vector<CPD3::Data::StreamSink *>> dispatch;

        const std::vector<CPD3::Data::StreamSink *> &lookup(const CPD3::Data::SequenceName &name);

    public:
        RealtimeIngress(ControlAccumulate *ce);

        virtual ~RealtimeIngress();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        virtual void endData();

        void finish();

        virtual void issueArchiveRead(const CPD3::Data::Archive::Selection::List &selections,
                                      CPD3::Data::StreamSink *target);
    };

    friend class RealtimeIngress;

    RealtimeIngress realtimeIngress;

    class ReadIngress : public CPD3::Data::StreamSink {
        ControlAccumulate *control;
        CPD3::Data::SequenceName::Map<bool> dispatch;
        double lastIncomingTime;

        bool lookup(const CPD3::Data::SequenceName &name);

    public:
        ReadIngress(ControlAccumulate *ce);

        virtual ~ReadIngress();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        virtual void endData();

        void reset();
    };

    friend class ReadIngress;

    ReadIngress readIngress;

    std::mutex readMutex;
    CPD3::Data::SequenceSegment::Transfer pendingSegments;
    CPD3::Data::SequenceSegment::Stream reader;
    CPD3::Data::ProcessingTapChain *chain;

    struct Accumulator {
        double value;
        double time;

        Accumulator();
    };

    std::unordered_map<CPD3::Data::SequenceName::Component, Accumulator> totals;

    CPD3::Data::SequenceValue persistentFn;
    CPD3::Data::SequenceValue persistentFf;
    CPD3::Data::SequenceValue persistentTotals;

    std::unordered_set<CPD3::Data::SequenceName::Component> previousRealtimeVariables;
    std::unordered_set<CPD3::Data::SequenceName::Component> previousLoggingVariables;

    friend class Configuration;

    class Configuration {
        double start;
        double end;

    public:
        struct Target {
            CPD3::Data::SequenceName::Component variable;
            std::unique_ptr<CPD3::Data::DynamicInput> rate;
            CPD3::Data::Variant::Root metadata;

            bool accumulateWhileBypassed;
            std::unordered_map<std::string, CPD3::Data::Variant::Root> activateCommands;
            std::unordered_map<std::string, CPD3::Data::Variant::Root> deactivateCommands;

            void activate(CPD3::Acquisition::AcquisitionState *state) const;

            void deactivate(CPD3::Acquisition::AcquisitionState *state) const;

            Target();

            Target(const Target &other);

            Target &operator=(const Target &other);

            Target(Target &&);

            Target &operator=(Target &&);
        };

        std::vector<std::vector<Target>> targets;
        qint64 bypassIndex;
        qint64 blankIndex;
        qint64 startIndex;
        qint64 endIndex;
        bool bypassWhileChanging;
        bool bypassOnEnd;

        double updateTimeout;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        ~Configuration();

        Configuration(const Configuration &other);

        Configuration &operator=(const Configuration &other);

        Configuration(Configuration &&other);

        Configuration &operator=(Configuration &&other);

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under,
                      const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void parseAccumulator(const CPD3::Data::Variant::Read &config,
                              const CPD3::Data::SequenceName::Component &name = {});

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    std::deque<Configuration> config;
    QHash<CPD3::Data::SequenceName, bool> dispatch;

    CPD3::Data::DynamicTimeInterval *advanceDuration;
    CPD3::Data::DynamicTimeInterval *minimumDuration;
    CPD3::Data::DynamicTimeInterval *blankDuration;

    bool isBypassed() const;

    void advanceTime(double executeTime);

    CPD3::Data::SequenceName::ComponentSet getAllPossibleVariables() const;

    void updateNextTime(double currentTime);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time);

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time);

public:
    virtual ~ControlAccumulate();

    ControlAccumulate(const CPD3::Data::ValueSegment::Transfer &config,
                      const std::string &loggingContext);

    void setState(CPD3::Acquisition::AcquisitionState *state) override;

    void setControlStream(CPD3::Acquisition::AcquisitionControlStream *controlStream) override;

    void setRealtimeEgress(CPD3::Data::StreamSink *egress) override;

    void setLoggingEgress(CPD3::Data::StreamSink *egress) override;

    void setPersistentEgress(CPD3::Data::StreamSink *egress) override;

    CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables() override;

    void incomingData(const CPD3::Util::ByteView &data,
                      double time,
                      AcquisitionInterface::IncomingDataType type = IncomingDataType::Stream) override;

    void incomingControl(const CPD3::Util::ByteView &data,
                         double time,
                         AcquisitionInterface::IncomingDataType type = IncomingDataType::Stream) override;

    void incomingTimeout(double time) override;

    void incomingAdvance(double time) override;

    CPD3::Data::SequenceValue::Transfer getPersistentValues() override;

    void serializeState(QDataStream &stream) override;

    void deserializeState(QDataStream &stream) override;

    void initializeState() override;

    void signalTerminate() override;

    void incomingCommand(const CPD3::Data::Variant::Read &command) override;

    CPD3::Data::Variant::Root getCommands() override;

    CPD3::Data::StreamSink *getRealtimeIngress() override;

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    AutomaticDefaults getDefaults() override;

    bool isCompleted() override;
};

class ControlAccumulateComponent
        : public QObject, virtual public CPD3::Acquisition::AcquisitionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.control_accumulate"
                              FILE
                              "control_accumulate.json")
public:
    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config = {},
                                                                                const std::string &loggingContext = {}) override;
};

#endif
