/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "core/component.hxx"
#include "datacore/dynamicsequenceselection.hxx"

#include "corr_script.hxx"
#include "general.hxx"
#include "processor.hxx"
#include "fanout.hxx"


Q_LOGGING_CATEGORY(log_component_corr_script, "cpd3.component.corr.script", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;


enum Type {
    Type_Value, Type_Segment,
};

enum Mode {
    Mode_General, Mode_Processor, Mode_Fanout,
};

ComponentOptions CorrScriptComponent::getOptions()
{
    ComponentOptions options;

    options.add("code", new ComponentOptionScript(tr("code", "name"), tr("The processing code"),
                                                  tr("This option sets the script code that is evaluated.  "
                                                     "When in general mode, the incoming data stream is available on the global variable \"data\".  "
                                                     "In processor mode, the variable contains the current value and \"control\" is stream control.  "
                                                     "In fanout mode the global variable \"fanout\" contains the fanout controller, which calls the  "
                                                     "handler with a control stream and fanout key as the two arguments.  "
                                                     "It expects a return of the handler with an optional output transformer."),
                                                  QString()));

    ComponentOptionEnum *enumOption = new ComponentOptionEnum(tr("mode", "name"), tr("Invoke mode"),
                                                              tr("Set the mode that the processing code is invoked as."),
                                                              tr("Processor"));
    enumOption->add(Mode_General, "general", tr("general", "mode name"),
                    tr("Invoke the processing code as a general script, expecting it to loop over the incoming data.  "
                       "The global \"data\" contains the value stream."));
    enumOption->add(Mode_Processor, "processor", tr("processor", "mode name"),
                    tr("Invoke the processing code repeatedly as a handler.  "
                       "The global \"data\" contains the current value and \"control\" contains the stream controller."));
    enumOption->add(Mode_Fanout, "fanout", tr("fanout", "mode name"),
                    tr("Invoke the processing code as a fanout dispatcher.  "
                       "The fanout controller is available as the global \"fanout\" and should be called with "
                       "the control handler before the script finishes."));
    options.add("mode", enumOption);

    enumOption = new ComponentOptionEnum(tr("type", "name"), tr("Stream type"),
                                         tr("Set the type of data stream provided."),
                                         tr("Segment"));
    enumOption->add(Type_Segment, "segment", tr("segment", "mode name"),
                    tr("Provide data sequence segment values, containing multiple variables."));
    enumOption->add(Type_Value, "value", tr("value", "mode name"),
                    tr("Provide individual data values, each representing a single un-flattened variable value."));
    options.add("type", enumOption);

    options.add("input", new DynamicSequenceSelectionOption(tr("input", "name"),
                                                            tr("The values passed into the processor"),
                                                            tr("This option set the values that are passed into the processor.  "
                                                               "Only values that this option matches are available as inputs"),
                                                            tr("Everything", "default inputs")));

    options.add("output", new DynamicSequenceSelectionOption(tr("output", "name"),
                                                             tr("The values extracted from the processor"),
                                                             tr("This option set the values that are extracted from segments or not "
                                                                "ignored by value processors."),
                                                             tr("All segment inputs or all values produced",
                                                                "default outputs")));

    options.add("bypass", new DynamicSequenceSelectionOption(tr("bypass", "name"),
                                                             tr("The values that bypass the processor"),
                                                             tr("This option sets the values that bypass the processor.  "
                                                                "Bypass values may also be inputs, but should generally not be outputs."),
                                                             tr("Anything not input or output",
                                                                "default bypass")));

    return options;
}

QList<ComponentExample> CorrScriptComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    qobject_cast<ComponentOptionScript *>(options.get("code"))->set(
            "data.BsG_S11 = data.BsG_S11 - 0.25");
    examples.append(ComponentExample(options, tr("Simple operation", "simple operation"),
                                     tr("This subtracts 0.25 from the BsG_S11 variable.  "
                                        "This mode is only recommended for a single set of aligned "
                                        "data, since it forces all data into the same time segments.")));

    options = getOptions();
    qobject_cast<ComponentOptionEnum *>(options.get("type"))->set("value");
    qobject_cast<ComponentOptionScript *>(options.get("code"))->set(
            "control.autorelease = 60; data.START = math.floor(data.START / 60) * 60; data.END = math.ceil(data.END / 60) * 60;");
    examples.append(ComponentExample(options, tr("Time alteration", "time alteration name"),
                                     tr("This forces data into one minute alignment of starts and ends.  "
                                        "Note that this can disrupt some averaging types.")));

    options = getOptions();
    qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("general");
    qobject_cast<ComponentOptionScript *>(options.get("code"))->set(
            "for v in data do if (v.BsG_S11 < 0.25) then data.erase(v); end end");
    examples.append(ComponentExample(options, tr("Data removal", "data removal name"),
                                     tr("This removes all data where the green scattering is less than 0.25.")));

    options = getOptions();
    qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("fanout");
    qobject_cast<ComponentOptionScript *>(options.get("code"))->set(
            "fanout(function(control) return function(data) data.BsG_S11 = data.BsG_S11 - 0.25; end end)");
    examples.append(ComponentExample(options, tr("Fanout operation", "fanout operation name"),
                                     tr("This subtracts 0.25 from BsG_S11 while properly handling "
                                        "overlapping cut sizes, archives, and stations.")));

    return examples;
}

void handle_register(const SequenceName &name,
                     DynamicSequenceSelection *inputs,
                     DynamicSequenceSelection *outputs,
                     DynamicSequenceSelection *bypass)
{
    bool used = false;
    if (inputs->registerInput(name.fromMeta()))
        used = true;
    if (inputs->registerInput(name.toMeta()))
        used = true;
    if (outputs->registerInput(name.fromMeta()))
        used = true;
    if (outputs->registerInput(name.toMeta()))
        used = true;
    if (bypass) {
        if (bypass->registerInput(name.fromMeta()))
            used = true;
        if (bypass->registerInput(name.toMeta()))
            used = true;
    }

    if (used) {
        inputs->registerExpected(name.getStation(), name.getArchive(), {}, name.getFlavors());
        outputs->registerExpected(name.getStation(), name.getArchive(), {}, name.getFlavors());
        if (bypass) {
            bypass->registerExpected(name.getStation(), name.getArchive(), {}, name.getFlavors());
        }
    }
}

bool handle_input(CPD3::Data::SequenceValue &value,
                  DynamicSequenceSelection *inputs,
                  DynamicSequenceSelection *outputs,
                  DynamicSequenceSelection *bypass,
                  CPD3::StreamMultiplexer<CPD3::Data::SequenceValue>::Simple *bypassSink,
                  bool &sinkReady)
{
    const auto &name = value.getName();

    bool usedAsInput = false;
    if (inputs->get(value).count(name)) {
        usedAsInput = true;
    }

    if (bypass) {
        if (bypass->get(value).count(name)) {
            if (usedAsInput) {
                if (bypassSink->incomingValue(value))
                    sinkReady = true;
            } else {
                if (bypassSink->incomingValue(std::move(value)))
                    sinkReady = true;
            }
        } else {
            if (bypassSink->advance(value.getStart()))
                sinkReady = true;
        }
    } else if (!usedAsInput) {
        bool usedAsOutput = false;
        if (outputs->get(value).count(name)) {
            usedAsOutput = true;
        }
        if (!usedAsOutput) {
            if (bypassSink->incomingValue(std::move(value)))
                sinkReady = true;
        } else {
            if (bypassSink->advance(value.getStart()))
                sinkReady = true;
        }
    }

    return usedAsInput;
}

ProcessingStage *CorrScriptComponent::createGeneralFilterPredefined(const ComponentOptions &options,
                                                                    double start,
                                                                    double end,
                                                                    const QList<
                                                                            SequenceName> &names)
{
    std::string code;
    if (options.isSet("code")) {
        code = qobject_cast<ComponentOptionScript *>(options.get("code"))->get().toStdString();
    }
    if (code.empty()) {
        qCWarning(log_component_corr_script)
            << "No script code specified, this operation will fail";
    }

    std::unique_ptr<DynamicSequenceSelection> inputs;
    if (options.isSet("input")) {
        inputs.reset(qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("input"))->getOperator());
    }
    std::unique_ptr<DynamicSequenceSelection> outputs;
    if (options.isSet("output")) {
        outputs.reset(qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("output"))->getOperator());
    }
    std::unique_ptr<DynamicSequenceSelection> bypass;
    if (options.isSet("bypass")) {
        bypass.reset(qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("bypass"))->getOperator());
    }

    Mode mode = Mode_Processor;
    if (options.isSet("mode")) {
        mode = static_cast<Mode>(qobject_cast<ComponentOptionEnum *>(options.get("mode"))->get()
                                                                                         .getID());
    }

    Type type = Type_Segment;
    if (options.isSet("type")) {
        type = static_cast<Type>(qobject_cast<ComponentOptionEnum *>(options.get("type"))->get()
                                                                                         .getID());
    }

    if (!inputs) {
        inputs.reset(new DynamicSequenceSelection::Match(SequenceMatch::Element::Pattern(),
                                                         QString::fromStdString(Archive::Selection::excludeMetaArchiveMatcher()),
                                                         SequenceMatch::Element::Pattern(),
                                                         SequenceMatch::Element::PatternList(),
                                                         Util::to_qstringlist(SequenceName::defaultLacksFlavors())));
    }

    if (!outputs) {
        switch (type) {
        case Type_Value:
            outputs.reset(new DynamicSequenceSelection::Match(std::vector<
                    SequenceMatch::Element>{{SequenceMatch::Element::SpecialMatch::Data}}));
            break;
        case Type_Segment:
            outputs.reset(inputs->clone());
            break;
        }
    }

    for (const auto &name : names) {
        handle_register(name, inputs.get(), outputs.get(), bypass.get());
    }

    switch (mode) {
    case Mode_General:
        switch (type) {
        case Type_Value:
            return new GeneralValue(std::move(code), std::move(inputs), std::move(outputs),
                                    std::move(bypass));
        case Type_Segment:
            return new GeneralSegment(std::move(code), std::move(inputs), std::move(outputs),
                                      std::move(bypass));
        }
        break;
    case Mode_Processor:
        switch (type) {
        case Type_Value:
            return new ValueProcessor(std::move(code), std::move(inputs), std::move(outputs),
                                      std::move(bypass));
        case Type_Segment:
            return new SegmentProcessor(std::move(code), std::move(inputs), std::move(outputs),
                                        std::move(bypass));
        }
        break;
    case Mode_Fanout:
        switch (type) {
        case Type_Value:
            return new FanoutValue(std::move(code), std::move(inputs), std::move(outputs),
                                   std::move(bypass));
        case Type_Segment:
            return new FanoutSegment(std::move(code), std::move(inputs), std::move(outputs),
                                     std::move(bypass));
        }
        break;
    }

    Q_ASSERT(false);
    return nullptr;
}

ProcessingStage *CorrScriptComponent::createGeneralFilterDynamic(const ComponentOptions &options)
{ return createGeneralFilterPredefined(options, FP::undefined(), FP::undefined(), {}); }

ProcessingStage *CorrScriptComponent::createGeneralFilterEditing(double start,
                                                                 double end,
                                                                 const SequenceName::Component &station,
                                                                 const SequenceName::Component &archive,
                                                                 const ValueSegment::Transfer &config)
{
    std::unique_ptr<DynamicSequenceSelection>
            inputs(DynamicSequenceSelection::fromConfiguration(config, "Input", start, end));
    inputs->registerExpected(station, archive);
    std::unique_ptr<DynamicSequenceSelection>
            outputs(DynamicSequenceSelection::fromConfiguration(config, "Output", start, end));
    outputs->registerExpected(station, archive);
    std::unique_ptr<DynamicSequenceSelection>
            bypass(DynamicSequenceSelection::fromConfiguration(config, "Bypass", start, end));
    bypass->registerExpected(station, archive);

    Mode mode = Mode_Processor;
    Type type = Type_Segment;
    std::string code;
    for (const auto &segment : config) {
        if (!Range::intersects(start, end, segment.getStart(), segment.getEnd()))
            continue;

        code = segment["Code"].toString();

        {
            const auto &check = segment["Mode"].toString();
            if (Util::equal_insensitive(check, "general")) {
                mode = Mode_General;
            } else if (Util::equal_insensitive(check, "processor")) {
                mode = Mode_Processor;
            } else {
                mode = Mode_Fanout;
            }
        }
        {
            const auto &check = segment["Type"].toString();
            if (Util::equal_insensitive(check, "value", "values")) {
                type = Type_Value;
            } else {
                type = Type_Segment;
            }
        }
    }

    if (code.empty()) {
        qCWarning(log_component_corr_script)
            << "No script code specified, this operation will fail";
    }

    switch (mode) {
    case Mode_General:
        switch (type) {
        case Type_Value:
            return new GeneralValue(std::move(code), std::move(inputs), std::move(outputs),
                                    std::move(bypass));
        case Type_Segment:
            return new GeneralSegment(std::move(code), std::move(inputs), std::move(outputs),
                                      std::move(bypass));
        }
        break;
    case Mode_Processor:
        switch (type) {
        case Type_Value:
            return new ValueProcessor(std::move(code), std::move(inputs), std::move(outputs),
                                      std::move(bypass));
        case Type_Segment:
            return new SegmentProcessor(std::move(code), std::move(inputs), std::move(outputs),
                                        std::move(bypass));
        }
        break;
    case Mode_Fanout:
        switch (type) {
        case Type_Value:
            return new FanoutValue(std::move(code), std::move(inputs), std::move(outputs),
                                   std::move(bypass));
        case Type_Segment:
            return new FanoutSegment(std::move(code), std::move(inputs), std::move(outputs),
                                     std::move(bypass));
        }
        break;
    }

    Q_ASSERT(false);
    return nullptr;
}

