/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3CORRSCRIPT_FANOUT_HXX
#define CPD3CORRSCRIPT_FANOUT_HXX

#include "core/first.hxx"

#include "processor.hxx"
#include "luascript/fanoutcontroller.hxx"
#include "datacore/stream.hxx"
#include "datacore/segment.hxx"
#include "datacore/processingstage.hxx"

class FanoutSegment : public CPD3::Data::AsyncProcessingStage {
    typedef CPD3::StreamMultiplexer<CPD3::Data::SequenceValue> Multiplexer;

    std::string code;
    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> inputs;
    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> outputs;
    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> bypass;
    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> baselineOutputs;

    Multiplexer mux;
    Multiplexer::Simple *bypassSink;
    bool muxOutputReady;

    CPD3::Lua::Engine engine;
    CPD3::Lua::Engine::Frame root;
    CPD3::Lua::Engine::Reference controller;


    class BaseTarget : public CPD3::Lua::FanoutController::Target {
    public:
        BaseTarget();

        virtual ~BaseTarget();

        virtual void incomingSequenceValue(CPD3::Data::SequenceValue &value) = 0;

        virtual void incomingDataAdvance(double time);

        virtual void finalize();
    };

    class ForegroundTarget : public BaseTarget {
        std::unique_ptr<CPD3::Data::DynamicSequenceSelection> baseOutputs;

        class Buffer : public SegmentBuffer {
            ForegroundTarget &parent;
        public:
            Buffer(Multiplexer::Simple *sink, ForegroundTarget &parent);

            virtual ~Buffer();

        protected:
            virtual CPD3::Data::SequenceValue::Transfer extractFromSegment(CPD3::Data::SequenceSegment &segment);
        };

        friend class Buffer;

        FanoutSegment &parent;
        Buffer buffer;

        bool outputsFromScript;
        CPD3::Data::SequenceName::Set scriptOutputs;

        void process(bool ignoreEnd = true);

        inline bool &muxOutputReady() const
        { return parent.muxOutputReady; }

    public:
        ForegroundTarget(FanoutSegment &parent);

        virtual ~ForegroundTarget();

        void incomingSequenceValue(CPD3::Data::SequenceValue &value) override;

        void incomingDataAdvance(double time) override;

        void finalize() override;

        void addedDispatch(CPD3::Lua::Engine::Frame &frame,
                           const CPD3::Lua::Engine::Reference &controller,
                           const CPD3::Data::SequenceName &name);

    protected:
        bool initializeCall(CPD3::Lua::Engine::Frame &arguments,
                            const CPD3::Lua::Engine::Reference &controller,
                            const CPD3::Lua::Engine::Reference &key,
                            const std::vector<std::shared_ptr<Target>> &background,
                            CPD3::Lua::Engine::Table &context) override;

        void processSaved(CPD3::Lua::Engine::Frame &saved,
                          const CPD3::Lua::Engine::Reference &controller,
                          const CPD3::Lua::Engine::Reference &key,
                          const std::vector<std::shared_ptr<Target>> &background,
                          CPD3::Lua::Engine::Table &context) override;
    };

    friend class ForegroundTarget;

    class BackgroundTarget : public BaseTarget {
        CPD3::Data::SequenceSegment::Stream reader;

        friend class FanoutSegment::ForegroundTarget;

    public:
        BackgroundTarget();

        virtual ~BackgroundTarget();

        void incomingSequenceValue(CPD3::Data::SequenceValue &value) override;

    protected:
        bool initializeCall(CPD3::Lua::Engine::Frame &arguments,
                            const CPD3::Lua::Engine::Reference &controller,
                            const CPD3::Lua::Engine::Reference &key,
                            const std::vector<std::shared_ptr<Target>> &background,
                            CPD3::Lua::Engine::Table &context) override;
    };

    class BypassTarget : public BaseTarget {
        Multiplexer::Simple *sink;
    public:
        BypassTarget(Multiplexer::Simple *sink);

        virtual ~BypassTarget();

        void incomingSequenceValue(CPD3::Data::SequenceValue &value) override;
    };

    class OutputMetadataTarget : public BaseTarget {
        FanoutSegment &parent;
    public:
        OutputMetadataTarget(FanoutSegment &parent);

        virtual ~OutputMetadataTarget();

        void incomingSequenceValue(CPD3::Data::SequenceValue &value) override;
    };

    friend class OutputModifyTarget;

    class Fanout : public CPD3::Lua::FanoutController {
        FanoutSegment &parent;
    public:
        Fanout(FanoutSegment &parent);

        virtual ~Fanout();

    protected:
        std::shared_ptr<Target> createTarget(const CPD3::Lua::Engine::Output &type) override;

        void addedDispatch(CPD3::Lua::Engine::Frame &frame,
                           const CPD3::Lua::Engine::Reference &controller,
                           const CPD3::Data::SequenceName &name,
                           std::vector<std::shared_ptr<Target>> &targets) override;
    };

    Fanout fanout;

public:
    FanoutSegment(std::string code,
                  std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&inputs,
                  std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&outputs,
                  std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&bypass);

    virtual ~FanoutSegment();

protected:
    void process(CPD3::Data::SequenceValue::Transfer &&incoming) override;

    void finish() override;
};

class FanoutValue : public CPD3::Data::AsyncProcessingStage {
    typedef CPD3::StreamMultiplexer<CPD3::Data::SequenceValue> Multiplexer;

    std::string code;
    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> inputs;
    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> outputs;
    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> bypass;

    Multiplexer mux;
    Multiplexer::Simple *bypassSink;
    bool muxOutputReady;

    CPD3::Lua::Engine engine;
    CPD3::Lua::Engine::Frame root;
    CPD3::Lua::Engine::Reference controller;


    class BaseTarget : public CPD3::Lua::FanoutController::Target {
    public:
        BaseTarget();

        virtual ~BaseTarget();

        virtual void incomingSequenceValue(CPD3::Data::SequenceValue &value) = 0;

        virtual void incomingDataAdvance(double time);

        virtual void finalize();
    };

    class ForegroundTarget : public BaseTarget {
        friend class Buffer;

        FanoutValue &parent;
        ValueBuffer buffer;

        void process(bool ignoreEnd = true);

    public:
        ForegroundTarget(FanoutValue &parent);

        virtual ~ForegroundTarget();

        void incomingSequenceValue(CPD3::Data::SequenceValue &value) override;

        void incomingDataAdvance(double time) override;

        void finalize() override;

    protected:
        bool initializeCall(CPD3::Lua::Engine::Frame &arguments,
                            const CPD3::Lua::Engine::Reference &controller,
                            const CPD3::Lua::Engine::Reference &key,
                            const std::vector<std::shared_ptr<Target>> &background,
                            CPD3::Lua::Engine::Table &context) override;

        void processSaved(CPD3::Lua::Engine::Frame &saved,
                          const CPD3::Lua::Engine::Reference &controller,
                          const CPD3::Lua::Engine::Reference &key,
                          const std::vector<std::shared_ptr<Target>> &background,
                          CPD3::Lua::Engine::Table &context) override;
    };

    friend class ForegroundTarget;

    class BackgroundTarget : public BaseTarget {
        friend class FanoutValue::ForegroundTarget;

    public:
        BackgroundTarget();

        virtual ~BackgroundTarget();

        void incomingSequenceValue(CPD3::Data::SequenceValue &value) override;

    protected:
        bool initializeCall(CPD3::Lua::Engine::Frame &arguments,
                            const CPD3::Lua::Engine::Reference &controller,
                            const CPD3::Lua::Engine::Reference &key,
                            const std::vector<std::shared_ptr<Target>> &background,
                            CPD3::Lua::Engine::Table &context) override;
    };

    class BypassTarget : public BaseTarget {
        Multiplexer::Simple *sink;
    public:
        BypassTarget(Multiplexer::Simple *sink);

        virtual ~BypassTarget();

        void incomingSequenceValue(CPD3::Data::SequenceValue &value) override;
    };

    class OutputMetadataTarget : public BaseTarget {
        FanoutValue &parent;
    public:
        OutputMetadataTarget(FanoutValue &parent);

        virtual ~OutputMetadataTarget();

        void incomingSequenceValue(CPD3::Data::SequenceValue &value) override;
    };

    friend class OutputModifyTarget;

    class Fanout : public CPD3::Lua::FanoutController {
        FanoutValue &parent;
    public:
        Fanout(FanoutValue &parent);

        virtual ~Fanout();

    protected:
        std::shared_ptr<Target> createTarget(const CPD3::Lua::Engine::Output &type) override;

        void addedDispatch(CPD3::Lua::Engine::Frame &frame,
                           const CPD3::Lua::Engine::Reference &controller,
                           const CPD3::Data::SequenceName &name,
                           std::vector<std::shared_ptr<Target>> &targets) override;
    };

    Fanout fanout;

public:
    FanoutValue(std::string code,
                std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&inputs,
                std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&outputs,
                std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&bypass);

    virtual ~FanoutValue();

protected:
    void process(CPD3::Data::SequenceValue::Transfer &&incoming) override;

    void finish() override;
};

#endif //CPD3CORRSCRIPT_FANOUT_HXX
