/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3CORRSCRIPT_PROCESSOR_HXX
#define CPD3CORRSCRIPT_PROCESSOR_HXX

#include "core/first.hxx"

#include "luascript/streambuffer.hxx"
#include "core/merge.hxx"
#include "datacore/stream.hxx"
#include "datacore/segment.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/processingstage.hxx"

class ValueBuffer : public CPD3::Lua::StreamBuffer::Value {
    CPD3::StreamMultiplexer<CPD3::Data::SequenceValue>::Simple *target;
    bool &targetReady;

    double advance;
    std::deque<CPD3::Data::SequenceValue> pending;
public:
    ValueBuffer(CPD3::StreamMultiplexer<CPD3::Data::SequenceValue>::Simple *target,
                bool &targetReady);

    virtual ~ValueBuffer();

    void incomingData(const CPD3::Data::SequenceValue &value);

    void incomingData(CPD3::Data::SequenceValue &&value);

    void incomingAdvance(double time);

protected:
    bool pushNext(CPD3::Lua::Engine::Frame &target) override;

    void outputReady(CPD3::Lua::Engine::Frame &frame,
                     const CPD3::Lua::Engine::Reference &ref) override;

    void advanceReady(double time) override;

    void endReady() override;
};

class SegmentBuffer : public CPD3::Lua::StreamBuffer::Segment {
    CPD3::StreamMultiplexer<CPD3::Data::SequenceValue>::Simple *target;
    bool &targetReady;

    double advance;
    std::deque<CPD3::Data::SequenceSegment> pending;
    CPD3::Data::SequenceSegment::Stream reader;
public:
    SegmentBuffer(CPD3::StreamMultiplexer<CPD3::Data::SequenceValue>::Simple *target,
                  bool &targetReady);

    virtual ~SegmentBuffer();

    void incomingData(const CPD3::Data::SequenceValue &value);

    void incomingData(CPD3::Data::SequenceValue &&value);

    void incomingAdvance(double time);

    void incomingAdvanceDiscard(double time);

    void incomingEnd();

    void overlay(const CPD3::Data::SequenceSegment::Stream &under);

protected:
    bool pushNext(CPD3::Lua::Engine::Frame &target) override;

    void outputReady(CPD3::Lua::Engine::Frame &frame,
                     const CPD3::Lua::Engine::Reference &ref) override;

    void advanceReady(double time) override;

    void endReady() override;

    virtual CPD3::Data::SequenceValue::Transfer extractFromSegment(CPD3::Data::SequenceSegment &segment) = 0;
};


class ValueProcessor : public CPD3::Data::AsyncProcessingStage {
    typedef CPD3::StreamMultiplexer<CPD3::Data::SequenceValue> Multiplexer;

    std::string code;
    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> inputs;
    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> outputs;
    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> bypass;

    Multiplexer mux;
    Multiplexer::Simple *bypassSink;
    Multiplexer::Simple *outputSink;
    bool muxOutputReady;

    CPD3::Lua::Engine engine;
    CPD3::Lua::Engine::Frame root;
    ValueBuffer buffer;

    CPD3::Lua::Engine::Reference controller;
    CPD3::Lua::Engine::Table environment;
    CPD3::Lua::Engine::Reference invoke;

    CPD3::Data::SequenceName::Set seenNames;
public:
    ValueProcessor(std::string code,
                   std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&inputs,
                   std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&outputs,
                   std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&bypass);

    virtual ~ValueProcessor();

protected:
    void process(CPD3::Data::SequenceValue::Transfer &&incoming) override;

    void finish() override;
};

class SegmentProcessor : public CPD3::Data::AsyncProcessingStage {
    typedef CPD3::StreamMultiplexer<CPD3::Data::SequenceValue> Multiplexer;

    std::string code;
    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> inputs;
    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> outputs;
    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> bypass;

    Multiplexer mux;
    Multiplexer::Simple *bypassSink;
    Multiplexer::Simple *outputSink;
    bool muxOutputReady;

    CPD3::Lua::Engine engine;
    CPD3::Lua::Engine::Frame root;

    class Buffer : public SegmentBuffer {
        SegmentProcessor &parent;
    public:
        Buffer(SegmentProcessor &parent);

        virtual ~Buffer();

    public:
        CPD3::Data::SequenceValue::Transfer extractFromSegment(CPD3::Data::SequenceSegment &segment) override;
    };

    friend class Buffer;

    Buffer buffer;

    CPD3::Lua::Engine::Reference controller;
    CPD3::Lua::Engine::Table environment;
    CPD3::Lua::Engine::Reference invoke;

    CPD3::Data::SequenceName::Set seenNames;
public:
    SegmentProcessor(std::string code,
                     std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&inputs,
                     std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&outputs,
                     std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&bypass);

    virtual ~SegmentProcessor();

protected:
    void process(CPD3::Data::SequenceValue::Transfer &&incoming) override;

    void finish() override;
};

#endif //CPD3CORRSCRIPT_PROCESSOR_HXX
