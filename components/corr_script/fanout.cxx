/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QLoggingCategory>

#include "fanout.hxx"
#include "corr_script.hxx"
#include "core/environment.hxx"
#include "luascript/libs/sequencename.hxx"


Q_LOGGING_CATEGORY(log_component_corr_script_fanout, "cpd3.component.corr.script.fanout",
                   QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;

FanoutSegment::FanoutSegment(std::string code,
                             std::unique_ptr<DynamicSequenceSelection> &&inputs,
                             std::unique_ptr<DynamicSequenceSelection> &&outputs,
                             std::unique_ptr<DynamicSequenceSelection> &&bypass) : code(
        std::move(code)),
                                                                                   inputs(std::move(
                                                                                           inputs)),
                                                                                   outputs(std::move(
                                                                                           outputs)),
                                                                                   bypass(std::move(
                                                                                           bypass)),
                                                                                   mux(),
                                                                                   bypassSink(
                                                                                           mux.createSimple()),
                                                                                   muxOutputReady(
                                                                                           false),
                                                                                   engine(),
                                                                                   root(engine),
                                                                                   fanout(*this)
{
    baselineOutputs.reset(this->outputs->clone());

    fanout.pushController(root);
    controller = root.back();

    auto environment = root.pushSandboxEnvironment();
    {
        Lua::Engine::Assign assign(root, environment, "print");
        assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                qCInfo(log_component_corr_script_fanout) << entry[i].toOutputString();
            }
        }));
    }
    environment.set("fanout", controller);
    {
        Lua::Engine::Call call(root);
        if (!call.pushChunk(this->code, environment)) {
            qCDebug(log_component_corr_script_fanout) << "Error parsing fanout code:"
                                                      << call.errorDescription();
            call.clearError();
            return;
        }
        if (!call.execute()) {
            qCDebug(log_component_corr_script_fanout) << "Error initializing fanout code:"
                                                      << call.errorDescription();
            call.clearError();
            return;
        }
    }
}

FanoutSegment::~FanoutSegment() = default;

void FanoutSegment::process(CPD3::Data::SequenceValue::Transfer &&incoming)
{
    if (!incoming.empty()) {
        double advance = incoming.back().getStart();

        for (auto &value : incoming) {
            const auto &targets = fanout.dispatch(root, controller, value.getName());

            if (!handle_input(value, inputs.get(), outputs.get(), bypass.get(), bypassSink,
                              muxOutputReady))
                continue;

            for (const auto &base : targets) {
                static_cast<BaseTarget *>(base.get())->incomingSequenceValue(value);
            }
        }

        if (FP::defined(advance)) {
            for (const auto &base : fanout.allTargets()) {
                static_cast<BaseTarget *>(base.get())->incomingDataAdvance(advance);
            }
            if (bypassSink->advance(advance))
                muxOutputReady = true;
        }
    }

    if (muxOutputReady) {
        muxOutputReady = false;
        egress->incomingData(mux.output());
    }
}

void FanoutSegment::finish()
{
    for (const auto &base : fanout.allTargets()) {
        static_cast<BaseTarget *>(base.get())->finalize();
    }

    mux.creationComplete();
    bypassSink->end();
    bypassSink = nullptr;

    egress->incomingData(mux.output());
    egress->endData();
}

FanoutSegment::Fanout::Fanout(FanoutSegment &parent) : parent(parent)
{ }

FanoutSegment::Fanout::~Fanout() = default;

std::shared_ptr<
        Lua::FanoutController::Target> FanoutSegment::Fanout::createTarget(const Lua::Engine::Output &type)
{
    if (type.toBoolean())
        return std::make_shared<BackgroundTarget>();
    return std::make_shared<ForegroundTarget>(parent);
}

void FanoutSegment::Fanout::addedDispatch(Lua::Engine::Frame &frame,
                                          const Lua::Engine::Reference &controller,
                                          const Data::SequenceName &name,
                                          std::vector<std::shared_ptr<
                                                  Lua::FanoutController::Target>> &targets)
{
    handle_register(name, parent.inputs.get(), parent.outputs.get(), parent.bypass.get());

    bool haveBackground = false;
    for (const auto &check : targets) {
        if (dynamic_cast<BackgroundTarget *>(check.get())) {
            haveBackground = true;
            break;
        }
    }
    if (haveBackground) {
        targets.emplace_back(std::make_shared<BypassTarget>(parent.bypassSink));
        return;
    }

    if (name.isMeta() && parent.outputs->registerInput(name.fromMeta())) {
        targets.insert(targets.begin(), std::make_shared<OutputMetadataTarget>(parent));
    }

    bool haveForeground = false;
    for (const auto &check : targets) {
        if (auto fg = dynamic_cast<ForegroundTarget *>(check.get())) {
            fg->addedDispatch(frame, controller, name);
            haveForeground = true;
        }
    }
    if (!haveForeground) {
        targets.emplace_back(std::make_shared<BypassTarget>(parent.bypassSink));
    }
}

FanoutSegment::BaseTarget::BaseTarget() = default;

FanoutSegment::BaseTarget::~BaseTarget() = default;

void FanoutSegment::BaseTarget::incomingDataAdvance(double)
{ }

void FanoutSegment::BaseTarget::finalize()
{ }


FanoutSegment::BackgroundTarget::BackgroundTarget() = default;

FanoutSegment::BackgroundTarget::~BackgroundTarget() = default;

void FanoutSegment::BackgroundTarget::incomingSequenceValue(Data::SequenceValue &value)
{ reader.add(value); }

bool FanoutSegment::BackgroundTarget::initializeCall(Lua::Engine::Frame &arguments,
                                                     const Lua::Engine::Reference &,
                                                     const Lua::Engine::Reference &,
                                                     const std::vector<std::shared_ptr<
                                                             Lua::FanoutController::Target>> &,
                                                     Lua::Engine::Table &)
{
    arguments.clear();
    return false;
}

FanoutSegment::BypassTarget::BypassTarget(Multiplexer::Simple *sink) : sink(sink)
{ }

FanoutSegment::BypassTarget::~BypassTarget() = default;

void FanoutSegment::BypassTarget::incomingSequenceValue(Data::SequenceValue &value)
{ sink->incomingValue(value); }

FanoutSegment::OutputMetadataTarget::OutputMetadataTarget(FanoutSegment &parent) : parent(parent)
{ }

FanoutSegment::OutputMetadataTarget::~OutputMetadataTarget() = default;

void FanoutSegment::OutputMetadataTarget::incomingSequenceValue(CPD3::Data::SequenceValue &value)
{
    if (!parent.outputs->get(value).count(value.getName().fromMeta()))
        return;
    if (!value.read().isMetadata())
        return;
    if (!value.read().metadata("Processing").exists())
        return;

    auto meta = value.write().metadata("Processing").toArray().after_back();
    meta["By"].setString("corr_script");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());
    meta["Parameters"].hash("Type").setString("FanoutSegment");
    meta["Parameters"].hash("Code").setString(parent.code);
}

FanoutSegment::ForegroundTarget::Buffer::Buffer(Multiplexer::Simple *sink,
                                                FanoutSegment::ForegroundTarget &pr)
        : SegmentBuffer(sink, pr.muxOutputReady()), parent(pr)
{ }

FanoutSegment::ForegroundTarget::Buffer::~Buffer() = default;

CPD3::Data::SequenceValue::Transfer FanoutSegment::ForegroundTarget::Buffer::extractFromSegment(
        SequenceSegment &segment)
{
    if (parent.outputsFromScript) {
        SequenceValue::Transfer result;
        for (const auto &add : parent.scriptOutputs) {
            auto v = segment.getValue(add);
            if (!v.exists())
                continue;
            result.emplace_back(add, Variant::Root(v), segment.getStart(), segment.getEnd());
        }
        return result;
    }

    SequenceValue::Transfer result;
    for (const auto &add : parent.baseOutputs->get(segment)) {
        auto v = segment.getValue(add);
        if (!v.exists())
            continue;
        result.emplace_back(add, Variant::Root(v), segment.getStart(), segment.getEnd());
    }
    return result;
}

FanoutSegment::ForegroundTarget::ForegroundTarget(FanoutSegment &pr) : baseOutputs(
        pr.baselineOutputs->clone()),
                                                                       parent(pr),
                                                                       buffer(parent.mux
                                                                                    .createSimple(),
                                                                              *this),
                                                                       outputsFromScript(false)
{ }

FanoutSegment::ForegroundTarget::~ForegroundTarget() = default;

void FanoutSegment::ForegroundTarget::process(bool ignoreEnd)
{
    Lua::Engine::Frame local(parent.root);
    pushSaved(local, parent.controller, 2);
    auto bcontroller = local.back();
    pushSaved(local, parent.controller, 0);
    auto invoke = local.back();

    for (;;) {
        Lua::Engine::Call call(local);
        call.push(invoke);
        if (call.front().isNil())
            return;
        if (!buffer.pushExternal(call, bcontroller, ignoreEnd))
            return;
        if (!call.execute()) {
            call.clearError();
            continue;
        }
    }
}

void FanoutSegment::ForegroundTarget::incomingSequenceValue(SequenceValue &value)
{
    buffer.incomingData(value);
    process();
}

void FanoutSegment::ForegroundTarget::incomingDataAdvance(double time)
{
    buffer.incomingAdvance(time);
    process();
}

void FanoutSegment::ForegroundTarget::finalize()
{
    buffer.incomingEnd();
    process(false);

    Lua::Engine::Frame local(parent.root);
    pushSaved(local, parent.controller, 2);
    auto bcontroller = local.back();
    buffer.finish(local, bcontroller);
}

bool FanoutSegment::ForegroundTarget::initializeCall(Lua::Engine::Frame &arguments,
                                                     const Lua::Engine::Reference &controller,
                                                     const Lua::Engine::Reference &key,
                                                     const std::vector<std::shared_ptr<
                                                             Lua::FanoutController::Target>> &background,
                                                     Lua::Engine::Table &context)
{
    for (const auto &check : background) {
        auto btarget = dynamic_cast<BackgroundTarget *>(check.get());
        if (!btarget)
            continue;
        buffer.overlay(btarget->reader);
    }

    buffer.incomingAdvanceDiscard(parent.mux.getCurrentAdvance());

    buffer.pushExternalController(arguments);
    context.set("bcontrol", arguments.back());

    arguments.push(key);

    arguments.propagate(2);
    return true;
}

void FanoutSegment::ForegroundTarget::processSaved(Lua::Engine::Frame &saved,
                                                   const Lua::Engine::Reference &,
                                                   const Lua::Engine::Reference &,
                                                   const std::vector<std::shared_ptr<
                                                           Lua::FanoutController::Target>> &,
                                                   Lua::Engine::Table &context)
{
    saved.resize(2);
    if (saved[0].isNil())
        return;
    if (!saved[1].isNil()) {
        outputsFromScript = true;
    }
    saved.push(context, "bcontrol");
}

void FanoutSegment::ForegroundTarget::addedDispatch(Lua::Engine::Frame &frame,
                                                    const Lua::Engine::Reference &controller,
                                                    const Data::SequenceName &name)
{
    if (!outputsFromScript) {
        bool used = false;
        if (parent.inputs->registerInput(name.fromMeta()))
            used = true;
        if (parent.inputs->registerInput(name.toMeta()))
            used = true;
        if (baseOutputs->registerInput(name.fromMeta()))
            used = true;
        if (baseOutputs->registerInput(name.toMeta()))
            used = true;
        if (parent.bypass) {
            if (parent.bypass->registerInput(name.fromMeta()))
                used = true;
            if (parent.bypass->registerInput(name.toMeta()))
                used = true;
        }

        if (used) {
            baseOutputs->registerExpected(name.getStation(), name.getArchive(), {},
                                          name.getFlavors());
        }
        return;
    }

    Lua::Engine::Call call(frame);
    pushSaved(call, controller, 1);
    call.pushData<Lua::Libs::SequenceName>(name);
    if (!call.executeVariable()) {
        call.clearError();
        return;
    }
    for (std::size_t i = 0, max = call.size(); i < max; i++) {
        scriptOutputs.insert(Lua::Libs::SequenceName::extract(call, call[i]));
    }
}


FanoutValue::FanoutValue(std::string code,
                         std::unique_ptr<DynamicSequenceSelection> &&inputs,
                         std::unique_ptr<DynamicSequenceSelection> &&outputs,
                         std::unique_ptr<DynamicSequenceSelection> &&bypass) : code(
        std::move(code)),
                                                                               inputs(std::move(
                                                                                       inputs)),
                                                                               outputs(std::move(
                                                                                       outputs)),
                                                                               bypass(std::move(
                                                                                       bypass)),
                                                                               mux(),
                                                                               bypassSink(
                                                                                       mux.createSimple()),
                                                                               muxOutputReady(
                                                                                       false),
                                                                               engine(),
                                                                               root(engine),
                                                                               fanout(*this)
{
    fanout.pushController(root);
    controller = root.back();

    auto environment = root.pushSandboxEnvironment();
    {
        Lua::Engine::Assign assign(root, environment, "print");
        assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                qCInfo(log_component_corr_script_fanout) << entry[i].toOutputString();
            }
        }));
    }
    environment.set("fanout", controller);
    {
        Lua::Engine::Call call(root);
        if (!call.pushChunk(this->code, environment)) {
            qCDebug(log_component_corr_script_fanout) << "Error parsing fanout code:"
                                                      << call.errorDescription();
            call.clearError();
            return;
        }
        if (!call.execute()) {
            qCDebug(log_component_corr_script_fanout) << "Error initializing fanout code:"
                                                      << call.errorDescription();
            call.clearError();
            return;
        }
    }
}

FanoutValue::~FanoutValue() = default;

void FanoutValue::process(CPD3::Data::SequenceValue::Transfer &&incoming)
{
    if (!incoming.empty()) {
        double advance = incoming.back().getStart();

        for (auto &value : incoming) {
            const auto &targets = fanout.dispatch(root, controller, value.getName());

            if (!handle_input(value, inputs.get(), outputs.get(), bypass.get(), bypassSink,
                              muxOutputReady))
                continue;

            for (const auto &base : targets) {
                static_cast<BaseTarget *>(base.get())->incomingSequenceValue(value);
            }
        }

        if (FP::defined(advance)) {
            for (const auto &base : fanout.allTargets()) {
                static_cast<BaseTarget *>(base.get())->incomingDataAdvance(advance);
            }
            if (bypassSink->advance(advance))
                muxOutputReady = true;
        }
    }

    if (muxOutputReady) {
        muxOutputReady = false;
        egress->incomingData(mux.output());
    }
}

void FanoutValue::finish()
{
    for (const auto &base : fanout.allTargets()) {
        static_cast<BaseTarget *>(base.get())->finalize();
    }

    mux.creationComplete();
    bypassSink->end();
    bypassSink = nullptr;

    egress->incomingData(mux.output());
    egress->endData();
}

FanoutValue::Fanout::Fanout(FanoutValue &parent) : parent(parent)
{ }

FanoutValue::Fanout::~Fanout() = default;

std::shared_ptr<
        Lua::FanoutController::Target> FanoutValue::Fanout::createTarget(const Lua::Engine::Output &type)
{
    if (type.toBoolean())
        return std::make_shared<BackgroundTarget>();
    return std::make_shared<ForegroundTarget>(parent);
}

void FanoutValue::Fanout::addedDispatch(Lua::Engine::Frame &frame,
                                        const Lua::Engine::Reference &controller,
                                        const Data::SequenceName &name,
                                        std::vector<std::shared_ptr<
                                                Lua::FanoutController::Target>> &targets)
{
    handle_register(name, parent.inputs.get(), parent.outputs.get(), parent.bypass.get());

    bool haveBackground = false;
    for (const auto &check : targets) {
        if (dynamic_cast<BackgroundTarget *>(check.get())) {
            haveBackground = true;
            break;
        }
    }
    if (haveBackground) {
        targets.emplace_back(std::make_shared<BypassTarget>(parent.bypassSink));
        return;
    }

    if (name.isMeta() && parent.outputs->registerInput(name.fromMeta())) {
        targets.insert(targets.begin(), std::make_shared<OutputMetadataTarget>(parent));
    }

    bool haveForeground = false;
    for (const auto &check : targets) {
        if (dynamic_cast<ForegroundTarget *>(check.get())) {
            haveForeground = true;
        }
    }
    if (!haveForeground) {
        targets.emplace_back(std::make_shared<BypassTarget>(parent.bypassSink));
    }
}

FanoutValue::BaseTarget::BaseTarget() = default;

FanoutValue::BaseTarget::~BaseTarget() = default;

void FanoutValue::BaseTarget::incomingDataAdvance(double)
{ }

void FanoutValue::BaseTarget::finalize()
{ }


FanoutValue::BackgroundTarget::BackgroundTarget() = default;

FanoutValue::BackgroundTarget::~BackgroundTarget() = default;

void FanoutValue::BackgroundTarget::incomingSequenceValue(Data::SequenceValue &)
{ }

bool FanoutValue::BackgroundTarget::initializeCall(Lua::Engine::Frame &arguments,
                                                   const Lua::Engine::Reference &,
                                                   const Lua::Engine::Reference &,
                                                   const std::vector<std::shared_ptr<
                                                           Lua::FanoutController::Target>> &,
                                                   Lua::Engine::Table &)
{
    arguments.clear();
    return false;
}

FanoutValue::BypassTarget::BypassTarget(Multiplexer::Simple *sink) : sink(sink)
{ }

FanoutValue::BypassTarget::~BypassTarget() = default;

void FanoutValue::BypassTarget::incomingSequenceValue(Data::SequenceValue &value)
{ sink->incomingValue(value); }

FanoutValue::OutputMetadataTarget::OutputMetadataTarget(FanoutValue &parent) : parent(parent)
{ }

FanoutValue::OutputMetadataTarget::~OutputMetadataTarget() = default;

void FanoutValue::OutputMetadataTarget::incomingSequenceValue(CPD3::Data::SequenceValue &value)
{
    if (!parent.outputs->get(value).count(value.getName().fromMeta()))
        return;
    if (!value.read().isMetadata())
        return;
    if (!value.read().metadata("Processing").exists())
        return;

    auto meta = value.write().metadata("Processing").toArray().after_back();
    meta["By"].setString("corr_script");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());
    meta["Parameters"].hash("Type").setString("FanoutValue");
    meta["Parameters"].hash("Code").setString(parent.code);
}

FanoutValue::ForegroundTarget::ForegroundTarget(FanoutValue &parent) : parent(parent),
                                                                       buffer(parent.mux
                                                                                    .createSimple(),
                                                                              parent.muxOutputReady)
{ }

FanoutValue::ForegroundTarget::~ForegroundTarget() = default;

void FanoutValue::ForegroundTarget::process(bool ignoreEnd)
{
    Lua::Engine::Frame local(parent.root);
    pushSaved(local, parent.controller, 1);
    auto bcontroller = local.back();
    pushSaved(local, parent.controller, 0);
    auto invoke = local.back();

    for (;;) {
        Lua::Engine::Call call(local);
        call.push(invoke);
        if (call.front().isNil())
            return;
        if (!buffer.pushExternal(call, bcontroller, ignoreEnd))
            return;
        if (!call.execute()) {
            call.clearError();
            continue;
        }
    }
}

void FanoutValue::ForegroundTarget::incomingSequenceValue(SequenceValue &value)
{
    buffer.incomingData(value);
    process();
}

void FanoutValue::ForegroundTarget::incomingDataAdvance(double time)
{
    buffer.incomingAdvance(time);
    process();
}

void FanoutValue::ForegroundTarget::finalize()
{
    process(false);

    Lua::Engine::Frame local(parent.root);
    pushSaved(local, parent.controller, 1);
    auto bcontroller = local.back();
    buffer.finish(local, bcontroller);
}

bool FanoutValue::ForegroundTarget::initializeCall(Lua::Engine::Frame &arguments,
                                                   const Lua::Engine::Reference &controller,
                                                   const Lua::Engine::Reference &key,
                                                   const std::vector<std::shared_ptr<
                                                           Lua::FanoutController::Target>> &background,
                                                   Lua::Engine::Table &context)
{
    buffer.incomingAdvance(parent.mux.getCurrentAdvance());

    buffer.pushExternalController(arguments);
    context.set("bcontrol", arguments.back());

    arguments.push(key);

    arguments.propagate(2);
    return true;
}

void FanoutValue::ForegroundTarget::processSaved(Lua::Engine::Frame &saved,
                                                 const Lua::Engine::Reference &,
                                                 const Lua::Engine::Reference &,
                                                 const std::vector<std::shared_ptr<
                                                         Lua::FanoutController::Target>> &,
                                                 Lua::Engine::Table &context)
{
    saved.resize(1);
    if (saved[0].isNil())
        return;
    saved.push(context, "bcontrol");
}