/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QLoggingCategory>

#include "general.hxx"
#include "corr_script.hxx"
#include "core/environment.hxx"
#include "core/threadpool.hxx"
#include "luascript/libs/sequencesegment.hxx"
#include "luascript/libs/streamvalue.hxx"

Q_LOGGING_CATEGORY(log_component_corr_script_general, "cpd3.component.corr.script.general",
                   QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;

GeneralBuffer::GeneralBuffer(std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&inputs,
                             std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&outputs,
                             std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&bypass)
        : inputs(std::move(inputs)),
          outputs(std::move(outputs)),
          bypass(std::move(bypass)),
          mux(),
          bypassSink(mux.createSimple()),
          outputSink(mux.createSimple()),
          muxOutputReady(false),
          bufferAdvance(FP::undefined()),
          state(State::Active),
          incomingEnded(false),
          egress(nullptr)
{
    mux.creationComplete();
}

GeneralBuffer::~GeneralBuffer() = default;

bool GeneralBuffer::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    return state == State::Completed;
}

bool GeneralBuffer::wait(std::thread &thread, double timeout)
{
    if (!thread.joinable())
        return true;
    if (!Threading::waitForTimeout(timeout, mutex, notify,
                                   [this] { return state == State::Completed; }))
        return false;
    thread.join();
    return true;
}

void GeneralBuffer::signalTerminate()
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        if (state == State::Completed)
            return;
        state = State::Terminated;
    }
    notify.notify_all();
}

void GeneralBuffer::setEgress(StreamSink *set)
{
    std::lock_guard<std::mutex> lock(mutex);
    std::lock_guard<std::mutex> elock(egressLock);
    egress = set;
    notify.notify_all();
}

void GeneralBuffer::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Util::append(values, pending);
    }
    notify.notify_all();
}

void GeneralBuffer::incomingData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Util::append(std::move(values), pending);
    }
    notify.notify_all();
}

void GeneralBuffer::incomingData(const SequenceValue &value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        pending.emplace_back(value);
    }
    notify.notify_all();
}

void GeneralBuffer::incomingData(SequenceValue &&value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        pending.emplace_back(std::move(value));
    }
    notify.notify_all();
}

void GeneralBuffer::stall(std::unique_lock<std::mutex> &lock)
{
    while (pending.size() > ProcessingStage::stallThreshold) {
        switch (state) {
        case State::Active:
            break;
        case State::Terminated:
        case State::Completed:
            return;
        }
        notify.wait(lock);
    }
}

void GeneralBuffer::endData()
{
    std::lock_guard<std::mutex> lock(mutex);
    incomingEnded = true;
    notify.notify_all();
}

CPD3::Data::SequenceName::Set GeneralBuffer::requestedInputs()
{
    CPD3::Data::SequenceName::Set result;
    Util::merge(inputs->getAllUnits(), result);
    return result;
}

CPD3::Data::SequenceName::Set GeneralBuffer::predictedOutputs()
{
    CPD3::Data::SequenceName::Set result;
    Util::merge(outputs->getAllUnits(), result);
    return result;
}

void GeneralBuffer::complete(CPD3::Lua::Engine::Frame &frame,
                             const CPD3::Lua::Engine::Reference &controller)
{
    finish(frame, controller);

    std::unique_lock<std::mutex> lock(mutex);
    std::unique_lock<std::mutex> elock(egressLock);
    for (;;) {
        if (state == State::Terminated) {
            state = State::Completed;
            lock.unlock();

            if (egress) {
                egress->endData();
            }

            notify.notify_all();
            return;
        }

        if (egress)
            break;

        elock.unlock();
        notify.wait(lock);
        elock.lock();
    }
    lock.unlock();

    Q_ASSERT(egress);

    bypassSink->end();
    bypassSink = nullptr;

    if (outputSink) {
        outputSink->end();
        outputSink = nullptr;
    }

    egress->incomingData(mux.output());
    egress->endData();

    elock.unlock();


    lock.lock();
    state = State::Completed;
    lock.unlock();
    notify.notify_all();
}

bool GeneralBuffer::pushNext(CPD3::Lua::Engine::Frame &target)
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        switch (state) {
        case State::Active:
            break;
        case State::Terminated:
            return false;
        case State::Completed:
            Q_ASSERT(false);
            return false;
        }

        {
            std::unique_lock<std::mutex> elock(egressLock);
            /* Don't do any processing (stall the Lua code) while we have no output, so
             * we correctly stall anything inputting to us if we fill up the buffers */

            if (!egress) {
                elock.unlock();
                notify.wait(lock);
                continue;
            }

            if (muxOutputReady) {
                muxOutputReady = false;
                lock.unlock();

                egress->incomingData(mux.output());
                elock.unlock();

                lock.lock();
                continue;
            }
        }

        if (pending.empty()) {
            if (!incomingEnded) {
                double adv = bufferAdvance;
                bufferAdvance = FP::undefined();
                if (pushAdvanceLocked(target, lock, adv))
                    return true;
                if (!lock) {
                    lock.lock();
                    continue;
                }
            } else {
                return pushFinishLocked(target, lock);
            }
            notify.wait(lock);
            continue;
        }

        bool wasStalled = pending.size() > ProcessingStage::stallThreshold;

        if (pushNextLocked(target, pending, lock)) {
            if (lock)
                lock.unlock();
            if (wasStalled && pending.size() <= ProcessingStage::stallThreshold)
                notify.notify_all();
            if (pushNextUnlocked(target))
                return true;
            if (!lock)
                lock.lock();
            continue;
        }
        if (wasStalled && pending.size() <= ProcessingStage::stallThreshold)
            notify.notify_all();
        if (!lock)
            lock.lock();

        /* Should always have done some work here (either deplete the pending, or
         * some internal operation), so don't wait */
    }
}

bool GeneralBuffer::pushNextUnlocked(CPD3::Lua::Engine::Frame &)
{ return true; }

bool GeneralBuffer::processPending(CPD3::Data::SequenceValue &value)
{
    const auto &name = value.getName();
    if (!seenNames.count(name)) {
        seenNames.insert(name);

        handle_register(name, inputs.get(), outputs.get(), bypass.get());
    }

    if (name.isMeta() && outputs->get(value).count(name.fromMeta())) {
        if (value.read().isMetadata() && value.read().metadata("Processing").exists()) {
            auto meta = value.write().metadata("Processing").toArray().after_back();
            meta["By"].setString("corr_script");
            meta["At"].setDouble(Time::time());
            meta["Environment"].setString(Environment::describe());
            meta["Revision"].setString(Environment::revision());
            processMetadata(meta);
        }
    }

    return handle_input(value, inputs.get(), outputs.get(), bypass.get(), bypassSink,
                        muxOutputReady);
}

bool GeneralBuffer::pushAdvanceLocked(CPD3::Lua::Engine::Frame &target,
                                      std::unique_lock<std::mutex> &lock,
                                      double advance)
{
    if (!FP::defined(advance))
        return false;
    lock.unlock();
    target.push(advance);
    return true;
}

bool GeneralBuffer::pushFinishLocked(CPD3::Lua::Engine::Frame &, std::unique_lock<std::mutex> &)
{ return false; }

void GeneralBuffer::outputReady(CPD3::Lua::Engine::Frame &frame,
                                const CPD3::Lua::Engine::Reference &ref)
{
    Q_ASSERT(outputSink);

    if (outputSink->incoming(processOutput(frame, ref)))
        muxOutputReady = true;
}

void GeneralBuffer::advanceReady(double time)
{
    Q_ASSERT(outputSink);

    if (outputSink->advance(time))
        muxOutputReady = true;
}

void GeneralBuffer::endReady()
{
    Q_ASSERT(outputSink);

    if (outputSink->end())
        muxOutputReady = true;
    outputSink = nullptr;
}


GeneralSegment::Buffer::Buffer(GeneralSegment &parent,
                               std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&inputs,
                               std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&outputs,
                               std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&bypass)
        : GeneralBuffer(std::move(inputs), std::move(outputs), std::move(bypass)),
          parent(parent),
          readerEnded(false)
{ }

GeneralSegment::Buffer::~Buffer() = default;

bool GeneralSegment::Buffer::pushNextLocked(CPD3::Lua::Engine::Frame &target,
                                            std::deque<CPD3::Data::SequenceValue> &pending,
                                            std::unique_lock<std::mutex> &)
{
    if (!segments.empty())
        return true;
    Util::append(std::move(pending), incoming);
    pending.clear();
    return !incoming.empty();
}

bool GeneralSegment::Buffer::pushNextUnlocked(CPD3::Lua::Engine::Frame &target)
{
    if (!incoming.empty()) {
        for (auto &add : incoming) {
            if (!processPending(add))
                continue;
            Util::append(reader.add(std::move(add)), segments);
        }
        incoming.clear();
    }
    if (segments.empty())
        return false;

    target.pushData<Lua::Libs::SequenceSegment>(std::move(segments.front()));
    segments.pop_front();
    return true;
}

bool GeneralSegment::Buffer::pushFinishLocked(Lua::Engine::Frame &target,
                                              std::unique_lock<std::mutex> &lock)
{
    lock.unlock();

    if (!readerEnded) {
        readerEnded = true;
        Util::append(reader.finish(), segments);
    }
    if (segments.empty())
        return false;

    target.pushData<Lua::Libs::SequenceSegment>(std::move(segments.front()));
    segments.pop_front();
    return true;
}

bool GeneralSegment::Buffer::pushAdvanceLocked(CPD3::Lua::Engine::Frame &target,
                                               std::unique_lock<std::mutex> &lock,
                                               double advance)
{
    lock.unlock();

    if (FP::defined(advance)) {
        Util::append(reader.advance(advance), segments);
    }

    if (!segments.empty()) {
        target.pushData<Lua::Libs::SequenceSegment>(std::move(segments.front()));
        segments.pop_front();
        return true;
    }

    if (!FP::defined(advance))
        return false;

    target.push(advance);
    return true;
}

double GeneralSegment::Buffer::getStart(Lua::Engine::Frame &frame,
                                        const Lua::Engine::Reference &ref)
{
    auto value = ref.toData<Lua::Libs::SequenceSegment>();
    if (!value)
        return FP::undefined();
    return value->get().getStart();
}

void GeneralSegment::Buffer::convertFromLua(Lua::Engine::Frame &frame)
{
    if (frame.back().toData<Lua::Libs::SequenceSegment>())
        return;
    auto value = Lua::Libs::SequenceSegment::extract(frame, frame.back());
    frame.pop();
    frame.pushData<Lua::Libs::SequenceSegment>(std::move(value));
}

CPD3::Data::SequenceValue::Transfer GeneralSegment::Buffer::processOutput(CPD3::Lua::Engine::Frame &frame,
                                                                          const CPD3::Lua::Engine::Reference &ref)
{
    auto value = ref.toData<Lua::Libs::SequenceSegment>();
    if (!value)
        return {};
    auto &extract = value->get();

    SequenceValue::Transfer result;
    for (const auto &add : getOutputs()->get(extract)) {
        result.emplace_back(extract.getSequenceValue(add));
    }
    return result;
}

void GeneralSegment::Buffer::processMetadata(CPD3::Data::Variant::Write &processing)
{
    processing["Parameters"].hash("Type").setString("GeneralSegment");
    processing["Parameters"].hash("Code").setString(parent.code);
}


GeneralValue::Buffer::Buffer(GeneralValue &parent,
                             std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&inputs,
                             std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&outputs,
                             std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&bypass)
        : GeneralBuffer(std::move(inputs), std::move(outputs), std::move(bypass)), parent(parent)
{ }

GeneralValue::Buffer::~Buffer() = default;

bool GeneralValue::Buffer::pushNextLocked(CPD3::Lua::Engine::Frame &target,
                                          std::deque<CPD3::Data::SequenceValue> &pending,
                                          std::unique_lock<std::mutex> &lock)
{
    while (!pending.empty()) {
        auto value = std::move(pending.front());
        pending.pop_front();
        if (!processPending(value))
            continue;
        lock.unlock();

        target.pushData<Lua::Libs::SequenceValue>(std::move(value));
        return true;
    }
    return false;
}

double GeneralValue::Buffer::getStart(CPD3::Lua::Engine::Frame &frame,
                                      const CPD3::Lua::Engine::Reference &ref)
{
    auto value = ref.toData<Lua::Libs::SequenceValue>();
    if (!value)
        return FP::undefined();
    return value->getStart(frame, ref);
}

void GeneralValue::Buffer::convertFromLua(CPD3::Lua::Engine::Frame &frame)
{
    if (frame.back().toData<Lua::Libs::SequenceValue>())
        return;
    auto value = Lua::Libs::SequenceValue::extract(frame, frame.back());
    frame.pop();
    frame.pushData<Lua::Libs::SequenceValue>(std::move(value));
}

CPD3::Data::SequenceValue::Transfer GeneralValue::Buffer::processOutput(CPD3::Lua::Engine::Frame &frame,
                                                                        const CPD3::Lua::Engine::Reference &ref)
{ return CPD3::Data::SequenceValue::Transfer{Lua::Libs::SequenceValue::extract(frame, ref)}; }

void GeneralValue::Buffer::processMetadata(CPD3::Data::Variant::Write &processing)
{
    processing["Parameters"].hash("Type").setString("GeneralValue");
    processing["Parameters"].hash("Code").setString(parent.code);
}


GeneralSegment::GeneralSegment(std::string code,
                               std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&inputs,
                               std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&outputs,
                               std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&bypass)
        : code(std::move(code)),
          buffer(*this, std::move(inputs), std::move(outputs), std::move(bypass))
{ }

GeneralSegment::~GeneralSegment()
{
    buffer.signalTerminate();
    if (thread.joinable())
        thread.join();
}

bool GeneralSegment::isFinished()
{ return buffer.isFinished(); }

bool GeneralSegment::wait(double timeout)
{ return buffer.wait(thread, timeout); }

void GeneralSegment::signalTerminate()
{ return buffer.signalTerminate(); }

void GeneralSegment::start()
{ thread = std::thread(std::bind(&GeneralSegment::run, this)); }

void GeneralSegment::setEgress(CPD3::Data::StreamSink *egress)
{ return buffer.setEgress(egress); }

void GeneralSegment::incomingData(const CPD3::Data::SequenceValue::Transfer &values)
{ return buffer.incomingData(values); }

void GeneralSegment::incomingData(CPD3::Data::SequenceValue::Transfer &&values)
{ return buffer.incomingData(std::move(values)); }

void GeneralSegment::incomingData(const CPD3::Data::SequenceValue &value)
{ return buffer.incomingData(value); }

void GeneralSegment::incomingData(CPD3::Data::SequenceValue &&value)
{ return buffer.incomingData(std::move(value)); }

void GeneralSegment::endData()
{ return buffer.endData(); }

SequenceName::Set GeneralSegment::requestedInputs()
{ return buffer.requestedInputs(); }

SequenceName::Set GeneralSegment::predictedOutputs()
{ return buffer.predictedOutputs(); }

void GeneralSegment::run()
{
    Lua::Engine engine;

    Lua::Engine::Frame root(engine);
    buffer.pushLuaController(root);
    auto controller = root.back();
    auto environment = root.pushSandboxEnvironment();
    environment.set("data", controller);
    {
        Lua::Engine::Assign assign(root, environment, "print");
        assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                qCInfo(log_component_corr_script_general) << entry[i].toOutputString();
            }
        }));
    }

    {
        Lua::Engine::Call call(root);
        if (!call.pushChunk(code, environment)) {
            qCDebug(log_component_corr_script_general) << "Error general segment code:"
                                                       << call.errorDescription();
            call.clearError();
            buffer.signalTerminate();
        } else if (!call.execute()) {
            engine.clearError();
            buffer.signalTerminate();
        }
    }

    buffer.complete(root, controller);
    finished();
}

GeneralValue::GeneralValue(std::string code,
                           std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&inputs,
                           std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&outputs,
                           std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&bypass) : code(
        std::move(code)), buffer(*this, std::move(inputs), std::move(outputs), std::move(bypass))
{ }

GeneralValue::~GeneralValue()
{
    buffer.signalTerminate();
    if (thread.joinable())
        thread.join();
}

bool GeneralValue::isFinished()
{ return buffer.isFinished(); }

bool GeneralValue::wait(double timeout)
{ return buffer.wait(thread, timeout); }

void GeneralValue::signalTerminate()
{ return buffer.signalTerminate(); }

void GeneralValue::start()
{ thread = std::thread(std::bind(&GeneralValue::run, this)); }

void GeneralValue::setEgress(CPD3::Data::StreamSink *egress)
{ return buffer.setEgress(egress); }

void GeneralValue::incomingData(const CPD3::Data::SequenceValue::Transfer &values)
{ return buffer.incomingData(values); }

void GeneralValue::incomingData(CPD3::Data::SequenceValue::Transfer &&values)
{ return buffer.incomingData(std::move(values)); }

void GeneralValue::incomingData(const CPD3::Data::SequenceValue &value)
{ return buffer.incomingData(value); }

void GeneralValue::incomingData(CPD3::Data::SequenceValue &&value)
{ return buffer.incomingData(std::move(value)); }

void GeneralValue::endData()
{ return buffer.endData(); }

SequenceName::Set GeneralValue::requestedInputs()
{ return buffer.requestedInputs(); }

SequenceName::Set GeneralValue::predictedOutputs()
{ return buffer.predictedOutputs(); }

void GeneralValue::run()
{
    Lua::Engine engine;

    Lua::Engine::Frame root(engine);
    buffer.pushLuaController(root);
    auto controller = root.back();
    auto environment = root.pushSandboxEnvironment();
    environment.set("data", controller);
    {
        Lua::Engine::Assign assign(root, environment, "print");
        assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                qCInfo(log_component_corr_script_general) << entry[i].toOutputString();
            }
        }));
    }

    {
        Lua::Engine::Call call(root);
        if (!call.pushChunk(code, environment)) {
            qCDebug(log_component_corr_script_general) << "Error general segment code:"
                                                       << call.errorDescription();
            call.clearError();
            buffer.signalTerminate();
        } else if (!call.execute()) {
            engine.clearError();
            buffer.signalTerminate();
        }
    }

    buffer.complete(root, controller);
    finished();
}
