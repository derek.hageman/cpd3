/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/processingstage.hxx"
#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestComponent : public QObject {
Q_OBJECT

    ProcessingStageComponent *component;

    bool contains(const SequenceValue::Transfer &list, const SequenceValue &check)
    {
        for (const auto &v : list) {
            if (SequenceValue::ValueEqual()(v, check))
                return true;
        }
        return false;
    }

private slots:

    void initTestCase()
    {
        component =
                qobject_cast<ProcessingStageComponent *>(ComponentLoader::create("corr_script"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();

        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("input")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("output")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("bypass")));
        QVERIFY(qobject_cast<ComponentOptionScript *>(options.get("code")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("mode")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("type")));
    }

    void basicSegment()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<ComponentOptionScript *>(options.get("code"))->set(R"EOF(
data.BsG_S11 = data.BsG_S11 + data.BsB_S11 + 0.25;
)EOF");

        std::unique_ptr<ProcessingStage> filter(component->createGeneralFilterDynamic(options));
        QVERIFY(filter.get() != nullptr);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName a("brw", "raw", "BsG_S11");
        SequenceName b("brw", "raw", "BsB_S11");

        filter->incomingData(SequenceValue(a, Variant::Root(1.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(b, Variant::Root(2.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(a, Variant::Root(3.0), 2.0, 3.0));
        filter->incomingData(SequenceValue(b, Variant::Root(4.0), 2.0, 3.0));
        filter->endData();

        QVERIFY(filter->wait());
        filter.reset();

        QCOMPARE((int) e.values().size(), 4);
        QVERIFY(contains(e.values(), SequenceValue(a, Variant::Root(3.25), 1.0, 2.0)));
        QVERIFY(contains(e.values(), SequenceValue(b, Variant::Root(2.0), 1.0, 2.0)));
        QVERIFY(contains(e.values(), SequenceValue(a, Variant::Root(7.25), 2.0, 3.0)));
        QVERIFY(contains(e.values(), SequenceValue(b, Variant::Root(4.0), 2.0, 3.0)));
    }

    void basicValue()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<ComponentOptionEnum *>(options.get("type"))->set("value");
        qobject_cast<ComponentOptionScript *>(options.get("code"))->set(R"EOF(
control.autorelease = 60;
data.START = math.floor(data.START / 60) * 60;
data.value = data.value + 0.5;
)EOF");

        std::unique_ptr<ProcessingStage> filter(component->createGeneralFilterDynamic(options));
        QVERIFY(filter.get() != nullptr);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName a("brw", "raw", "BsG_S11");
        SequenceName b("sgp", "raw", "BsB_S11");

        filter->incomingData(SequenceValue(a, Variant::Root(1.0), 60.0, 61.0));
        filter->incomingData(SequenceValue(b, Variant::Root(2.0), 61.0, 62.0));
        filter->incomingData(SequenceValue(b, Variant::Root(3.0), 120.0, 121.0));
        filter->incomingData(SequenceValue(a, Variant::Root(4.0), 121.0, 122.0));
        filter->endData();

        QVERIFY(filter->wait());
        filter.reset();

        QCOMPARE((int) e.values().size(), 4);
        QVERIFY(contains(e.values(), SequenceValue(a, Variant::Root(1.5), 60.0, 61.0)));
        QVERIFY(contains(e.values(), SequenceValue(b, Variant::Root(2.5), 60.0, 62.0)));
        QVERIFY(contains(e.values(), SequenceValue(b, Variant::Root(3.5), 120.0, 121.0)));
        QVERIFY(contains(e.values(), SequenceValue(a, Variant::Root(4.5), 120.0, 122.0)));
    }

    void generalSegment()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("general");
        qobject_cast<ComponentOptionScript *>(options.get("code"))->set(R"EOF(
for v in data do
    if (v.BsG_S11 < 0.25) then
        data:erase(v);
    else
        v.BsB_S11 = v.BsB_S11 + 0.5;
    end
end
)EOF");

        std::unique_ptr<ProcessingStage> filter(component->createGeneralFilterDynamic(options));
        QVERIFY(filter.get() != nullptr);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName a("brw", "raw", "BsG_S11");
        SequenceName b("brw", "raw", "BsB_S11");

        filter->incomingData(SequenceValue(a, Variant::Root(1.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(b, Variant::Root(2.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(a, Variant::Root(0.1), 2.0, 3.0));
        filter->incomingData(SequenceValue(b, Variant::Root(99.0), 2.0, 3.0));
        filter->incomingData(SequenceValue(a, Variant::Root(3.0), 3.0, 4.0));
        filter->incomingData(SequenceValue(b, Variant::Root(4.0), 3.0, 4.0));
        filter->endData();

        QVERIFY(filter->wait());
        filter.reset();

        QCOMPARE((int) e.values().size(), 4);
        QVERIFY(contains(e.values(), SequenceValue(a, Variant::Root(1.0), 1.0, 2.0)));
        QVERIFY(contains(e.values(), SequenceValue(b, Variant::Root(2.5), 1.0, 2.0)));
        QVERIFY(contains(e.values(), SequenceValue(a, Variant::Root(3.0), 3.0, 4.0)));
        QVERIFY(contains(e.values(), SequenceValue(b, Variant::Root(4.5), 3.0, 4.0)));
    }

    void generalValue()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("general");
        qobject_cast<ComponentOptionEnum *>(options.get("type"))->set("value");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("input"))->set("brw", "raw",
                                                                                  "BsG_S11");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("bypass"))->set("brw", "raw",
                                                                                   "BsB_S11");
        qobject_cast<ComponentOptionScript *>(options.get("code"))->set(R"EOF(
for v in data do
    v.value = v.value + 0.5;
end
data:add(CPD3.SequenceValue(
    CPD3.SequenceIdentity(
        CPD3.SequenceName('brw', 'raw', 'BsR_S11'),
        5, 6),
    5.0)
);
)EOF");

        std::unique_ptr<ProcessingStage> filter(component->createGeneralFilterDynamic(options));
        QVERIFY(filter.get() != nullptr);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName a("brw", "raw", "BsG_S11");
        SequenceName b("brw", "raw", "BsB_S11");
        SequenceName c("brw", "raw", "BsR_S11");

        filter->incomingData(SequenceValue(a, Variant::Root(1.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(b, Variant::Root(2.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(a, Variant::Root(3.0), 2.0, 3.0));
        filter->incomingData(SequenceValue(b, Variant::Root(4.0), 3.0, 4.0));
        filter->endData();

        QVERIFY(filter->wait());
        filter.reset();

        QCOMPARE((int) e.values().size(), 5);
        QVERIFY(contains(e.values(), SequenceValue(a, Variant::Root(1.5), 1.0, 2.0)));
        QVERIFY(contains(e.values(), SequenceValue(b, Variant::Root(2.0), 1.0, 2.0)));
        QVERIFY(contains(e.values(), SequenceValue(a, Variant::Root(3.5), 2.0, 3.0)));
        QVERIFY(contains(e.values(), SequenceValue(b, Variant::Root(4.0), 3.0, 4.0)));
        QVERIFY(contains(e.values(), SequenceValue(c, Variant::Root(5.0), 5.0, 6.0)));
    }

    void fanoutSegment()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("bypass"))->set("", "raw",
                                                                                   "BsB_S11");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("output"))->set("", "raw",
                                                                                   "BsG_S11");
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("fanout");
        qobject_cast<ComponentOptionScript *>(options.get("code"))->set(R"EOF(
fanout(function(control)
    return function(data)
         data.BsG_S11 = data.BsG_S11 + 0.5 + (data.BsB_S11 or 0);
    end
end);
)EOF");

        std::unique_ptr<ProcessingStage> filter(component->createGeneralFilterDynamic(options));
        QVERIFY(filter.get() != nullptr);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName a("brw", "raw", "BsG_S11");
        SequenceName b("brw", "raw", "BsB_S11");
        SequenceName c("sgp", "raw", "BsG_S11");

        filter->incomingData(SequenceValue(a, Variant::Root(1.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(b, Variant::Root(2.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(c, Variant::Root(3.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(a, Variant::Root(4.0), 2.0, 3.0));
        filter->incomingData(SequenceValue(c, Variant::Root(5.0), 3.0, 4.0));
        filter->incomingData(SequenceValue(b, Variant::Root(6.0), 3.0, 4.0));
        filter->incomingData(SequenceValue(a, Variant::Root(7.0), 3.0, 4.0));
        filter->endData();

        QVERIFY(filter->wait());
        filter.reset();

        QCOMPARE((int) e.values().size(), 7);
        QVERIFY(contains(e.values(), SequenceValue(a, Variant::Root(3.5), 1.0, 2.0)));
        QVERIFY(contains(e.values(), SequenceValue(b, Variant::Root(2.0), 1.0, 2.0)));
        QVERIFY(contains(e.values(), SequenceValue(c, Variant::Root(3.5), 1.0, 2.0)));
        QVERIFY(contains(e.values(), SequenceValue(a, Variant::Root(4.5), 2.0, 3.0)));
        QVERIFY(contains(e.values(), SequenceValue(c, Variant::Root(5.5), 3.0, 4.0)));
        QVERIFY(contains(e.values(), SequenceValue(b, Variant::Root(6.0), 3.0, 4.0)));
        QVERIFY(contains(e.values(), SequenceValue(a, Variant::Root(13.5), 3.0, 4.0)));
    }

    void fanoutValue()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("input"))->set("", "raw",
                                                                                  "BsG_S11");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("bypass"))->set("", "raw",
                                                                                   "BsB_S11");
        qobject_cast<ComponentOptionEnum *>(options.get("type"))->set("value");
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("fanout");
        qobject_cast<ComponentOptionScript *>(options.get("code"))->set(R"EOF(
fanout(function(control, key)
    if key.station == "brw" then
        return function(data)
             data.value = data.value + 0.5;
        end
    else
        return function(data)
             data.value = data.value + 0.25;
        end
    end
end);
)EOF");

        std::unique_ptr<ProcessingStage> filter(component->createGeneralFilterDynamic(options));
        QVERIFY(filter.get() != nullptr);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName a("brw", "raw", "BsG_S11");
        SequenceName b("brw", "raw", "BsB_S11");
        SequenceName c("sgp", "raw", "BsG_S11");

        filter->incomingData(SequenceValue(a, Variant::Root(1.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(b, Variant::Root(2.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(c, Variant::Root(3.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(a, Variant::Root(4.0), 2.0, 3.0));
        filter->incomingData(SequenceValue(c, Variant::Root(5.0), 3.0, 4.0));
        filter->incomingData(SequenceValue(b, Variant::Root(6.0), 3.0, 4.0));
        filter->incomingData(SequenceValue(a, Variant::Root(7.0), 3.0, 4.0));
        filter->endData();

        QVERIFY(filter->wait());
        filter.reset();

        QCOMPARE((int) e.values().size(), 7);
        QVERIFY(contains(e.values(), SequenceValue(a, Variant::Root(1.5), 1.0, 2.0)));
        QVERIFY(contains(e.values(), SequenceValue(b, Variant::Root(2.0), 1.0, 2.0)));
        QVERIFY(contains(e.values(), SequenceValue(c, Variant::Root(3.25), 1.0, 2.0)));
        QVERIFY(contains(e.values(), SequenceValue(a, Variant::Root(4.5), 2.0, 3.0)));
        QVERIFY(contains(e.values(), SequenceValue(c, Variant::Root(5.25), 3.0, 4.0)));
        QVERIFY(contains(e.values(), SequenceValue(b, Variant::Root(6.0), 3.0, 4.0)));
        QVERIFY(contains(e.values(), SequenceValue(a, Variant::Root(7.5), 3.0, 4.0)));
    }


    void basicSegmentError()
    {
        CPD3::Logging::suppressForTesting();

        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<ComponentOptionScript *>(options.get("code"))->set(R"EOF(
error();
)EOF");

        std::unique_ptr<ProcessingStage> filter(component->createGeneralFilterDynamic(options));
        QVERIFY(filter.get() != nullptr);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName a("brw", "raw", "BsG_S11");

        filter->incomingData(SequenceValue(a, Variant::Root(1.0), 1.0, 2.0));
        filter->endData();

        QVERIFY(filter->wait());
    }

    void basicValueError()
    {
        CPD3::Logging::suppressForTesting();

        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<ComponentOptionEnum *>(options.get("type"))->set("value");
        qobject_cast<ComponentOptionScript *>(options.get("code"))->set(R"EOF(
error();
)EOF");

        std::unique_ptr<ProcessingStage> filter(component->createGeneralFilterDynamic(options));
        QVERIFY(filter.get() != nullptr);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName a("brw", "raw", "BsG_S11");

        filter->incomingData(SequenceValue(a, Variant::Root(1.0), 60.0, 61.0));
        filter->endData();

        QVERIFY(filter->wait());
    }

    void generalSegmentError()
    {
        CPD3::Logging::suppressForTesting();

        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("general");
        qobject_cast<ComponentOptionScript *>(options.get("code"))->set(R"EOF(
error();
)EOF");

        std::unique_ptr<ProcessingStage> filter(component->createGeneralFilterDynamic(options));
        QVERIFY(filter.get() != nullptr);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName a("brw", "raw", "BsG_S11");

        filter->incomingData(SequenceValue(a, Variant::Root(1.0), 1.0, 2.0));
        filter->endData();

        QVERIFY(filter->wait());
    }

    void generalValueError()
    {
        CPD3::Logging::suppressForTesting();

        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("general");
        qobject_cast<ComponentOptionEnum *>(options.get("type"))->set("value");
        qobject_cast<ComponentOptionScript *>(options.get("code"))->set(R"EOF(
error();
)EOF");

        std::unique_ptr<ProcessingStage> filter(component->createGeneralFilterDynamic(options));
        QVERIFY(filter.get() != nullptr);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName a("brw", "raw", "BsG_S11");

        filter->incomingData(SequenceValue(a, Variant::Root(1.0), 1.0, 2.0));
        filter->endData();

        QVERIFY(filter->wait());
    }

    void fanoutSegmentError()
    {
        CPD3::Logging::suppressForTesting();

        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("fanout");
        qobject_cast<ComponentOptionScript *>(options.get("code"))->set(R"EOF(
error();
)EOF");

        std::unique_ptr<ProcessingStage> filter(component->createGeneralFilterDynamic(options));
        QVERIFY(filter.get() != nullptr);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName a("brw", "raw", "BsG_S11");

        filter->incomingData(SequenceValue(a, Variant::Root(1.0), 1.0, 2.0));
        filter->endData();

        QVERIFY(filter->wait());
    }

    void fanoutValueError()
    {
        CPD3::Logging::suppressForTesting();

        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<ComponentOptionEnum *>(options.get("type"))->set("value");
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("fanout");
        qobject_cast<ComponentOptionScript *>(options.get("code"))->set(R"EOF(
fanout(function(control, key)
    error();
end);
)EOF");

        std::unique_ptr<ProcessingStage> filter(component->createGeneralFilterDynamic(options));
        QVERIFY(filter.get() != nullptr);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName a("brw", "raw", "BsG_S11");

        filter->incomingData(SequenceValue(a, Variant::Root(1.0), 1.0, 2.0));
        filter->endData();

        QVERIFY(filter->wait());
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
