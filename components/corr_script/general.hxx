/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3CORRSCRIPT_GENERAL_HXX
#define CPD3CORRSCRIPT_GENERAL_HXX

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>
#include <memory>
#include <thread>

#include "core/merge.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/segment.hxx"
#include "datacore/stream.hxx"
#include "luascript/engine.hxx"
#include "luascript/streambuffer.hxx"


class GeneralBuffer : public CPD3::Lua::StreamBuffer {
    typedef CPD3::StreamMultiplexer<CPD3::Data::SequenceValue> Multiplexer;

    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> inputs;
    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> outputs;
    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> bypass;

    Multiplexer mux;
    Multiplexer::Simple *bypassSink;
    Multiplexer::Simple *outputSink;
    bool muxOutputReady;

    double bufferAdvance;

    std::mutex mutex;
    std::mutex egressLock;
    std::condition_variable notify;

    enum class State {
        Active, Terminated, Completed,
    };
    State state;
    bool incomingEnded;
    CPD3::Data::StreamSink *egress;

    std::deque<CPD3::Data::SequenceValue> pending;

    CPD3::Data::SequenceName::Set seenNames;

    void stall(std::unique_lock<std::mutex> &lock);

public:
    GeneralBuffer(std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&inputs,
                  std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&outputs,
                  std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&bypass);

    virtual ~GeneralBuffer();

    bool isFinished();

    bool wait(std::thread &thread, double timeout = CPD3::FP::undefined());

    void signalTerminate();

    void setEgress(CPD3::Data::StreamSink *egress);

    void incomingData(const CPD3::Data::SequenceValue::Transfer &values);

    void incomingData(CPD3::Data::SequenceValue::Transfer &&values);

    void incomingData(const CPD3::Data::SequenceValue &value);

    void incomingData(CPD3::Data::SequenceValue &&value);

    void endData();

    CPD3::Data::SequenceName::Set requestedInputs();

    CPD3::Data::SequenceName::Set predictedOutputs();

    void complete(CPD3::Lua::Engine::Frame &frame, const CPD3::Lua::Engine::Reference &controller);

protected:
    bool pushNext(CPD3::Lua::Engine::Frame &target) override;

    void outputReady(CPD3::Lua::Engine::Frame &frame,
                     const CPD3::Lua::Engine::Reference &ref) override;

    void advanceReady(double time) override;

    void endReady() override;


    bool processPending(CPD3::Data::SequenceValue &value);


    virtual bool pushNextLocked(CPD3::Lua::Engine::Frame &target,
                                std::deque<CPD3::Data::SequenceValue> &pending,
                                std::unique_lock<std::mutex> &lock) = 0;

    virtual bool pushNextUnlocked(CPD3::Lua::Engine::Frame &target);

    virtual bool pushAdvanceLocked(CPD3::Lua::Engine::Frame &target,
                                   std::unique_lock<std::mutex> &lock,
                                   double advance);

    virtual bool pushFinishLocked(CPD3::Lua::Engine::Frame &target,
                                  std::unique_lock<std::mutex> &lock);

    virtual CPD3::Data::SequenceValue::Transfer processOutput(CPD3::Lua::Engine::Frame &frame,
                                                              const CPD3::Lua::Engine::Reference &ref) = 0;

    virtual void processMetadata(CPD3::Data::Variant::Write &processing) = 0;

    inline CPD3::Data::DynamicSequenceSelection *getOutputs() const
    { return outputs.get(); }
};


class GeneralSegment : public CPD3::Data::ProcessingStage {
    std::string code;

    class Buffer : public GeneralBuffer {
        GeneralSegment &parent;
        bool readerEnded;

        std::deque<CPD3::Data::SequenceValue> incoming;
        CPD3::Data::SequenceSegment::Stream reader;
        std::deque<CPD3::Data::SequenceSegment> segments;
    public:
        Buffer(GeneralSegment &parent,
               std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&inputs,
               std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&outputs,
               std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&bypass);

        virtual ~Buffer();

    protected:
        bool pushNextLocked(CPD3::Lua::Engine::Frame &target,
                            std::deque<CPD3::Data::SequenceValue> &pending,
                            std::unique_lock<std::mutex> &lock) override;

        bool pushNextUnlocked(CPD3::Lua::Engine::Frame &target) override;

        bool pushFinishLocked(CPD3::Lua::Engine::Frame &target,
                              std::unique_lock<std::mutex> &lock) override;

        bool pushAdvanceLocked(CPD3::Lua::Engine::Frame &target,
                               std::unique_lock<std::mutex> &lock,
                               double advance) override;

        double getStart(CPD3::Lua::Engine::Frame &frame,
                        const CPD3::Lua::Engine::Reference &ref) override;

        void convertFromLua(CPD3::Lua::Engine::Frame &frame) override;

        CPD3::Data::SequenceValue::Transfer processOutput(CPD3::Lua::Engine::Frame &frame,
                                                          const CPD3::Lua::Engine::Reference &ref) override;

        void processMetadata(CPD3::Data::Variant::Write &processing) override;
    };

    friend class Buffer;

    Buffer buffer;
    std::thread thread;

    void run();

public:
    explicit GeneralSegment(std::string code,
                            std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&inputs,
                            std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&outputs,
                            std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&bypass);

    virtual ~GeneralSegment();

    bool isFinished() override;

    bool wait(double timeout = CPD3::FP::undefined()) override;

    void signalTerminate() override;

    void start() override;

    void setEgress(CPD3::Data::StreamSink *egress) override;

    void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

    void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

    void incomingData(const CPD3::Data::SequenceValue &value) override;

    void incomingData(CPD3::Data::SequenceValue &&value) override;

    void endData() override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;
};

class GeneralValue : public CPD3::Data::ProcessingStage {
    std::string code;

    class Buffer : public GeneralBuffer {
        GeneralValue &parent;
    public:
        Buffer(GeneralValue &parent,
               std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&inputs,
               std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&outputs,
               std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&bypass);

        virtual ~Buffer();

    protected:
        bool pushNextLocked(CPD3::Lua::Engine::Frame &target,
                            std::deque<CPD3::Data::SequenceValue> &pending,
                            std::unique_lock<std::mutex> &lock) override;

        double getStart(CPD3::Lua::Engine::Frame &frame,
                        const CPD3::Lua::Engine::Reference &ref) override;

        void convertFromLua(CPD3::Lua::Engine::Frame &frame) override;

        CPD3::Data::SequenceValue::Transfer processOutput(CPD3::Lua::Engine::Frame &frame,
                                                          const CPD3::Lua::Engine::Reference &ref) override;

        void processMetadata(CPD3::Data::Variant::Write &processing) override;
    };

    friend class Buffer;

    Buffer buffer;
    std::thread thread;

    void run();

public:
    explicit GeneralValue(std::string code,
                          std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&inputs,
                          std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&outputs,
                          std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&bypass);

    virtual ~GeneralValue();

    bool isFinished() override;

    bool wait(double timeout = CPD3::FP::undefined()) override;

    void signalTerminate() override;

    void start() override;

    void setEgress(CPD3::Data::StreamSink *egress) override;

    void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

    void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

    void incomingData(const CPD3::Data::SequenceValue &value) override;

    void incomingData(CPD3::Data::SequenceValue &&value) override;

    void endData() override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;
};


#endif //CPD3CORRSCRIPT_GENERAL_HXX
