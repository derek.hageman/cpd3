/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CORRSCRIPT_H
#define CORRSCRIPT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "core/merge.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamicsequenceselection.hxx"

void handle_register(const CPD3::Data::SequenceName &name,
                     CPD3::Data::DynamicSequenceSelection *inputs,
                     CPD3::Data::DynamicSequenceSelection *outputs,
                     CPD3::Data::DynamicSequenceSelection *bypass);

bool handle_input(CPD3::Data::SequenceValue &value,
                  CPD3::Data::DynamicSequenceSelection *inputs,
                  CPD3::Data::DynamicSequenceSelection *outputs,
                  CPD3::Data::DynamicSequenceSelection *bypass,
                  CPD3::StreamMultiplexer<CPD3::Data::SequenceValue>::Simple *bypassSink,
                  bool &sinkReady);

class CorrScriptComponent : public QObject, public CPD3::Data::ProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.corr_script"
                              FILE
                              "corr_script.json")

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    CPD3::Data::ProcessingStage *createGeneralFilterDynamic(const CPD3::ComponentOptions &options = CPD3::ComponentOptions()) override;

    CPD3::Data::ProcessingStage *createGeneralFilterPredefined(const CPD3::ComponentOptions &options,
                                                               double start,
                                                               double end,
                                                               const QList<
                                                                       CPD3::Data::SequenceName> &inputs) override;

    CPD3::Data::ProcessingStage *createGeneralFilterEditing(double start,
                                                            double end,
                                                            const CPD3::Data::SequenceName::Component &station,
                                                            const CPD3::Data::SequenceName::Component &archive,
                                                            const CPD3::Data::ValueSegment::Transfer &config) override;

};

#endif
