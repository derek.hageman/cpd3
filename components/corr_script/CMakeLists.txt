cpd3_component(corr_script
               corr_script.cxx corr_script.hxx
               general.cxx general.hxx
               processor.cxx processor.hxx
               fanout.cxx fanout.hxx)
target_link_libraries(component_corr_script cpd3luascript)

add_subdirectory(test)
