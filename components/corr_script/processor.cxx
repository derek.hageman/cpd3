/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QLoggingCategory>

#include "processor.hxx"
#include "corr_script.hxx"
#include "luascript/libs/streamvalue.hxx"
#include "luascript/libs/sequencesegment.hxx"
#include "core/environment.hxx"


Q_LOGGING_CATEGORY(log_component_corr_script_processor, "cpd3.component.corr.script.processor",
                   QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;


ValueBuffer::ValueBuffer(StreamMultiplexer<CPD3::Data::SequenceValue>::Simple *target,
                         bool &targetReady) : target(target),
                                              targetReady(targetReady),
                                              advance(FP::undefined()),
                                              pending()
{ }

ValueBuffer::~ValueBuffer() = default;

void ValueBuffer::incomingData(const CPD3::Data::SequenceValue &value)
{
    pending.emplace_back(value);
    advance = FP::undefined();
}

void ValueBuffer::incomingData(CPD3::Data::SequenceValue &&value)
{
    pending.emplace_back(std::move(value));
    advance = FP::undefined();
}

void ValueBuffer::incomingAdvance(double time)
{ advance = time; }

bool ValueBuffer::pushNext(Lua::Engine::Frame &target)
{
    if (!pending.empty()) {
        target.pushData<Lua::Libs::SequenceValue>(std::move(pending.front()));
        pending.pop_front();
        return true;
    }
    if (FP::defined(advance)) {
        target.push(advance);
        advance = FP::undefined();
        return true;
    }
    return false;
}

void ValueBuffer::outputReady(Lua::Engine::Frame &frame, const Lua::Engine::Reference &ref)
{
    Q_ASSERT(target);
    if (target->incomingValue(extract(frame, ref)))
        targetReady = true;
}

void ValueBuffer::advanceReady(double time)
{
    Q_ASSERT(target);
    if (target->advance(time))
        targetReady = true;
}

void ValueBuffer::endReady()
{
    Q_ASSERT(target);
    if (target->end())
        targetReady = true;
    target = nullptr;
}

SegmentBuffer::SegmentBuffer(StreamMultiplexer<CPD3::Data::SequenceValue>::Simple *target,
                             bool &targetReady) : target(target),
                                                  targetReady(targetReady),
                                                  advance(FP::undefined()),
                                                  pending()
{ }

SegmentBuffer::~SegmentBuffer() = default;

void SegmentBuffer::incomingData(const CPD3::Data::SequenceValue &value)
{
    Util::append(reader.add(value), pending);
    advance = FP::undefined();
}

void SegmentBuffer::incomingData(CPD3::Data::SequenceValue &&value)
{
    Util::append(reader.add(std::move(value)), pending);
    advance = FP::undefined();
}

void SegmentBuffer::incomingAdvance(double time)
{
    Util::append(reader.advance(time), pending);
    advance = time;
}

void SegmentBuffer::incomingAdvanceDiscard(double time)
{
    reader.advance(time);
    advance = time;
}

void SegmentBuffer::incomingEnd()
{
    Util::append(reader.finish(), pending);
    advance = FP::undefined();
}

bool SegmentBuffer::pushNext(Lua::Engine::Frame &target)
{
    if (!pending.empty()) {
        target.pushData<Lua::Libs::SequenceSegment>(std::move(pending.front()));
        pending.pop_front();
        return true;
    }
    if (FP::defined(advance)) {
        target.push(advance);
        advance = FP::undefined();
        return true;
    }
    return false;
}

void SegmentBuffer::outputReady(Lua::Engine::Frame &frame, const Lua::Engine::Reference &ref)
{
    auto value = ref.toData<Lua::Libs::SequenceSegment>();
    if (!value)
        return;
    auto &extract = value->get();

    Q_ASSERT(target);

    if (target->incoming(extractFromSegment(extract)))
        targetReady = true;
}

void SegmentBuffer::advanceReady(double time)
{
    Q_ASSERT(target);
    if (target->advance(time))
        targetReady = true;
}

void SegmentBuffer::endReady()
{
    Q_ASSERT(target);
    if (target->end())
        targetReady = true;
    target = nullptr;
}

void SegmentBuffer::overlay(const CPD3::Data::SequenceSegment::Stream &under)
{
    reader.overlay(under);
}


ValueProcessor::ValueProcessor(std::string code,
                               std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&inputs,
                               std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&outputs,
                               std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&bypass)
        : code(std::move(code)),
          inputs(std::move(inputs)),
          outputs(std::move(outputs)),
          bypass(std::move(bypass)),
          mux(),
          bypassSink(mux.createSimple()),
          outputSink(mux.createSimple()),
          muxOutputReady(false),
          engine(),
          root(engine),
          buffer(outputSink, muxOutputReady)
{
    mux.creationComplete();

    buffer.pushExternalController(root);
    controller = root.back();
    environment = root.pushSandboxEnvironment();
    {
        Lua::Engine::Assign assign(root, environment, "print");
        assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                qCInfo(log_component_corr_script_processor) << entry[i].toOutputString();
            }
        }));
    }
    environment.set("control", controller);
    if (!root.pushChunk(this->code, environment)) {
        qCDebug(log_component_corr_script_processor)
            << "Error parsing sequence value processor code:" << root.errorDescription();
        root.clearError();
        outputSink->end();
        outputSink = nullptr;
        return;
    }
    invoke = root.back();
}

ValueProcessor::~ValueProcessor() = default;

void ValueProcessor::process(CPD3::Data::SequenceValue::Transfer &&incoming)
{
    for (auto &value : incoming) {
        const auto &name = value.getName();
        if (!seenNames.count(name)) {
            seenNames.insert(name);

            handle_register(name, inputs.get(), outputs.get(), bypass.get());
        }

        if (name.isMeta() && outputs->get(value).count(name.fromMeta())) {
            if (value.read().isMetadata() && value.read().metadata("Processing").exists()) {
                auto meta = value.write().metadata("Processing").toArray().after_back();
                meta["By"].setString("corr_script");
                meta["At"].setDouble(Time::time());
                meta["Environment"].setString(Environment::describe());
                meta["Revision"].setString(Environment::revision());
                meta["Parameters"].hash("Type").setString("ProcessorValue");
                meta["Parameters"].hash("Code").setString(code);
            }
        }

        double time = value.getStart();
        if (!handle_input(value, inputs.get(), outputs.get(), bypass.get(), bypassSink,
                          muxOutputReady)) {
            if (FP::defined(time))
                buffer.incomingAdvance(time);
            continue;
        }

        buffer.incomingData(std::move(value));
    }

    for (;;) {
        {
            Lua::Engine::Frame local(root);
            if (!buffer.pushExternal(local, controller, true))
                break;
            environment.set("data", local.back());
        }
        {
            Lua::Engine::Call call(root);
            call.push(invoke);
            if (!call.execute()) {
                call.clearError();
                continue;
            }
        }
    }

    if (muxOutputReady) {
        muxOutputReady = false;
        egress->incomingData(mux.output());
    }
}

void ValueProcessor::finish()
{
    if (outputSink) {
        for (;;) {
            {
                Lua::Engine::Frame local(root);
                if (!buffer.pushExternal(local, controller))
                    break;
                environment.set("data", local.back());
            }
            {
                Lua::Engine::Call call(root);
                call.push(invoke);
                if (!call.execute()) {
                    call.clearError();
                    continue;
                }
            }
        }

        buffer.finish(root, controller);
    }

    bypassSink->end();
    bypassSink = nullptr;

    egress->incomingData(mux.output());
    egress->endData();
}

SegmentProcessor::SegmentProcessor(std::string code,
                                   std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&inputs,
                                   std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&outputs,
                                   std::unique_ptr<CPD3::Data::DynamicSequenceSelection> &&bypass)
        : code(std::move(code)),
          inputs(std::move(inputs)),
          outputs(std::move(outputs)),
          bypass(std::move(bypass)),
          mux(),
          bypassSink(mux.createSimple()),
          outputSink(mux.createSimple()),
          muxOutputReady(false),
          engine(),
          root(engine),
          buffer(*this)
{
    mux.creationComplete();

    buffer.pushExternalController(root);
    controller = root.back();
    environment = root.pushSandboxEnvironment();
    {
        Lua::Engine::Assign assign(root, environment, "print");
        assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                qCInfo(log_component_corr_script_processor) << entry[i].toOutputString();
            }
        }));
    }
    environment.set("control", controller);
    if (!root.pushChunk(this->code, environment)) {
        qCDebug(log_component_corr_script_processor)
            << "Error parsing sequence value processor code:" << root.errorDescription();
        root.clearError();
        outputSink->end();
        outputSink = nullptr;
        return;
    }
    invoke = root.back();
}

SegmentProcessor::~SegmentProcessor() = default;

void SegmentProcessor::process(CPD3::Data::SequenceValue::Transfer &&incoming)
{
    if (!outputSink)
        return;

    for (auto &value : incoming) {
        const auto &name = value.getName();
        if (!seenNames.count(name)) {
            seenNames.insert(name);

            handle_register(name, inputs.get(), outputs.get(), bypass.get());
        }

        if (name.isMeta() && outputs->get(value).count(name.fromMeta())) {
            if (value.read().isMetadata() && value.read().metadata("Processing").exists()) {
                auto meta = value.write().metadata("Processing").toArray().after_back();
                meta["By"].setString("corr_script");
                meta["At"].setDouble(Time::time());
                meta["Environment"].setString(Environment::describe());
                meta["Revision"].setString(Environment::revision());
                meta["Parameters"].hash("Type").setString("ProcessorSegment");
                meta["Parameters"].hash("Code").setString(code);
            }
        }

        double time = value.getStart();
        if (!handle_input(value, inputs.get(), outputs.get(), bypass.get(), bypassSink,
                          muxOutputReady)) {
            if (FP::defined(time))
                buffer.incomingAdvance(time);
            continue;
        }

        buffer.incomingData(std::move(value));
    }

    for (;;) {
        {
            Lua::Engine::Frame local(root);
            if (!buffer.pushExternal(local, controller, true))
                break;
            environment.set("data", local.back());
        }
        {
            Lua::Engine::Call call(root);
            call.push(invoke);
            if (!call.execute()) {
                call.clearError();
                continue;
            }
        }
    }

    if (muxOutputReady) {
        muxOutputReady = false;
        egress->incomingData(mux.output());
    }
}

void SegmentProcessor::finish()
{
    if (outputSink) {
        buffer.incomingEnd();

        for (;;) {
            {
                Lua::Engine::Frame local(root);
                if (!buffer.pushExternal(local, controller))
                    break;
                environment.set("data", local.back());
            }
            {
                Lua::Engine::Call call(root);
                call.push(invoke);
                if (!call.execute()) {
                    call.clearError();
                    continue;
                }
            }
        }

        buffer.finish(root, controller);
    }

    bypassSink->end();
    bypassSink = nullptr;

    egress->incomingData(mux.output());
    egress->endData();
}

SegmentProcessor::Buffer::Buffer(SegmentProcessor &pr) : SegmentBuffer(pr.outputSink,
                                                                       pr.muxOutputReady),
                                                         parent(pr)
{ }

SegmentProcessor::Buffer::~Buffer() = default;

SequenceValue::Transfer SegmentProcessor::Buffer::extractFromSegment(SequenceSegment &segment)
{
    SequenceValue::Transfer result;
    for (const auto &add : parent.outputs->get(segment)) {
        auto v = segment.getValue(add);
        if (!v.exists())
            continue;
        result.emplace_back(add, Variant::Root(v), segment.getStart(), segment.getEnd());
    }
    return result;
}
