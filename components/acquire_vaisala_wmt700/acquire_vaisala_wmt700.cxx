/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cctype>
#include <QRegularExpression>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/wavelength.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_vaisala_wmt700.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

static const char *instrumentFlagTranslation[13] = {"TemperatureSensor1Failure",    /* 0x0001 */
                                                    "TemperatureSensor2Failure",    /* 0x0002 */
                                                    "TemperatureSensor3Failure",    /* 0x0004 */
                                                    "HeaterFailure",                /* 0x0008 */
                                                    "HighSupplyVoltage",            /* 0x0010 */
                                                    "LowSupplyVoltage",             /* 0x0020 */
                                                    "WindSpeedHigh",                /* 0x0040 */
                                                    "SonicTemperatureOutOfRange",   /* 0x0080 */
                                                    "WindMeasurementSuspect",       /* 0x0100 */
                                                    nullptr,                        /* 0x0200 */
                                                    "BlockedSensor",                /* 0x0400 */
                                                    nullptr,                        /* 0x0800 */
                                                    "HighNoise",                    /* 0x1000 */
};

AcquireVaisalaWMT700::Configuration::Configuration() : start(FP::undefined()),
                                                       end(FP::undefined()),
                                                       strictMode(true),
                                                       reportInterval(1.0),
                                                       address('0')
{ }

AcquireVaisalaWMT700::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          strictMode(other.strictMode),
          reportInterval(other.reportInterval),
          address(other.address)
{ }

AcquireVaisalaWMT700::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s), end(e), strictMode(true), reportInterval(1.0), address('0')
{
    setFromSegment(other);
}

AcquireVaisalaWMT700::Configuration::Configuration(const Configuration &under,
                                                   const ValueSegment &over,
                                                   double s,
                                                   double e) : start(s),
                                                               end(e),
                                                               strictMode(under.strictMode),
                                                               reportInterval(under.reportInterval),
                                                               address(under.address)
{
    setFromSegment(over);
}

void AcquireVaisalaWMT700::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    {
        double v = config["ReportInterval"].toDouble();
        if (FP::defined(v) && v > 0.0)
            reportInterval = v;
    }

    {
        const auto &addr = config["Address"].toString();
        if (!addr.empty()) {
            address = addr[0];
        }
    }
}


void AcquireVaisalaWMT700::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Vaisla");
    instrumentMeta["Model"].setString("WMT700");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    realtimeStateUpdated = true;
}

AcquireVaisalaWMT700::AcquireVaisalaWMT700(const ValueSegment::Transfer &configData,
                                           const std::string &loggingContext) : FramedInstrument(
        "wmt700", loggingContext),
                                                                                lastRecordTime(
                                                                                        FP::undefined()),
                                                                                autoprobeStatus(
                                                                                        AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                responseState(
                                                                                        RESP_PASSIVE_WAIT)
{
    setDefaultInvalid();
    config.emplace_back();
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireVaisalaWMT700::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void AcquireVaisalaWMT700::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireVaisalaWMT700Component::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireVaisalaWMT700::AcquireVaisalaWMT700(const ComponentOptions &options,
                                           const std::string &loggingContext) : FramedInstrument(
        "wmt700", loggingContext),
                                                                                lastRecordTime(
                                                                                        FP::undefined()),
                                                                                autoprobeStatus(
                                                                                        AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                responseState(
                                                                                        RESP_PASSIVE_WAIT)
{
    setDefaultInvalid();
    config.emplace_back();
}

AcquireVaisalaWMT700::~AcquireVaisalaWMT700() = default;

void AcquireVaisalaWMT700::logValue(double startTime,
                                    double endTime,
                                    SequenceName::Component name,
                                    Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress) return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireVaisalaWMT700::realtimeValue(double time,
                                         SequenceName::Component name,
                                         Variant::Root &&value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

SequenceValue::Transfer AcquireVaisalaWMT700::buildLogMeta(double time) const
{
    Q_ASSERT(!config.empty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_vaisla_wmt700");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    processing["PCBNumber"].set(instrumentMeta["PCBNumber"]);
    processing["CalibrationDate"].set(instrumentMeta["CalibrationDate"]);

    result.emplace_back(SequenceName({}, "raw_meta", "WS"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("m/s");
    result.back().write().metadataReal("Description").setString("Wind speed");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("Vector2D");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Direction")
          .setString("WD");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Magnitude")
          .setString("WS");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Wind Speed"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "WD"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Units").setString("degrees");
    result.back().write().metadataReal("Description").setString("Wind direction from true north");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("Vector2D");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Direction")
          .setString("WD");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Magnitude")
          .setString("WS");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Wind Direction"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sonic temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Sonic temperature"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Transducer temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Transducer temperature"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("AbnormalStatus")
          .hash("Description")
          .setString("None zero status code reported");

    return result;
}

SequenceValue::Transfer AcquireVaisalaWMT700::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_vaisla_wmt700");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    processing["PCBNumber"].set(instrumentMeta["PCBNumber"]);
    processing["CalibrationDate"].set(instrumentMeta["CalibrationDate"]);

    result.emplace_back(SequenceName({}, "raw_meta", "V1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Supply voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Supply voltage"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "V2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Heater voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Heater voltage"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(99);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidRecord")
          .setString(QObject::tr("NO COMMS: Invalid record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadVersion")
          .setString(QObject::tr("STARTING COMMS: Reading instrument version"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveGetSerialNumber")
          .setString(QObject::tr("STARTING COMMS: Reading instrument serial number"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveGetPCBSerial")
          .setString(QObject::tr("STARTING COMMS: Reading PCB serial number"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveGetCalibrationDate")
          .setString(QObject::tr("STARTING COMMS: Reading calibration date"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetUnits")
          .setString(QObject::tr("STARTING COMMS: Setting wind units"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetVectorMode")
          .setString(QObject::tr("STARTING COMMS: Setting wind averaging"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetReportRecord")
          .setString(QObject::tr("STARTING COMMS: Setting report record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetReportMode")
          .setString(QObject::tr("STARTING COMMS: Setting report mode"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveContinuous")
          .setString(QObject::tr("STARTING COMMS: Enable continuous sampling"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveEnableMessages")
          .setString(QObject::tr("STARTING COMMS: Enable error messages"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadFirstRecord")
          .setString(QObject::tr("STARTING COMMS: Waiting for first record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("TemperatureSensor1Failure")
          .setString(QObject::tr("Temperature sensor 1 failed", "flags translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("TemperatureSensor2Failure")
          .setString(QObject::tr("Temperature sensor 2 failed", "flags translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("TemperatureSensor3Failure")
          .setString(QObject::tr("Temperature sensor 2 failed", "flags translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("HeaterFailure")
          .setString(QObject::tr("Heater failure", "flags translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("HighSupplyVoltage")
          .setString(QObject::tr("Supply voltage too high", "flags translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("LowSupplyVoltage")
          .setString(QObject::tr("Supply voltage too low", "flags translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("WindSpeedHigh")
          .setString(QObject::tr("Wind speed too high", "flags translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SonicTemperatureOutOfRange")
          .setString(QObject::tr("Sonic temperature out of range", "flags translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("WindMeasurementSuspect")
          .setString(QObject::tr("Low wind measurement validity", "flags translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BlockedSensor")
          .setString(QObject::tr("Blocked sensor", "flags translation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("HighNoise")
          .setString(QObject::tr("High noise level", "flags translation"));

    return result;
}

SequenceMatch::Composite AcquireVaisalaWMT700::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    return sel;
}


int AcquireVaisalaWMT700::processRecord(CPD3::Util::ByteView line, double startTime, double endTime)
{
    if (line.size() < 2)
        return 1;

    if (line.front() != '$') return 2;

    {
        auto csum = line.mid(line.size() - 3);
        if (csum.front() != ',') return 3;
        csum = csum.mid(1);
        bool ok = false;
        std::uint8_t expected = static_cast<std::uint8_t>(csum.string_trimmed()
                                                              .toQByteArrayRef()
                                                              .toUShort(&ok, 16));
        if (!ok || expected > 0xFF) return 4;

        line = line.mid(0, line.size() - 2);

        std::uint8_t calculated = 0;
        for (auto add = line.begin(), end = add + line.size(); add != end; ++add) {
            calculated ^= static_cast<std::uint8_t>(*add);
        }
        if (calculated != expected) return 5;
    }
    line = line.mid(1, line.size() - 2);

    auto fields = CSV::acquisitionSplit(line);
    bool ok = false;

    if (fields.empty()) return 6;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root WS(field.parse_real(&ok));
    if (!ok) return 7;
    if (!FP::defined(WS.read().toReal())) return 8;
    remap("WS", WS);

    if (fields.empty()) return 9;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root WD(field.parse_real(&ok));
    if (!ok) return 10;
    if (!FP::defined(WD.read().toReal())) return 11;
    remap("WD", WD);

    /* Wind speed maximum */
    if (fields.empty()) return 12;
    fields.pop_front();

    /* Wind speed minimum */
    if (fields.empty()) return 13;
    fields.pop_front();

    if (fields.empty()) return 14;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T1(field.parse_real(&ok));
    if (!ok) return 15;
    if (!FP::defined(T1.read().toReal())) return 16;
    remap("T1", T1);

    if (fields.empty()) return 17;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root V2(field.parse_real(&ok));
    if (!ok) return 18;
    if (!FP::defined(V2.read().toReal())) return 19;
    remap("V2", V2);

    if (fields.empty()) return 20;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root V1(field.parse_real(&ok));
    if (!ok) return 21;
    if (!FP::defined(V1.read().toReal())) return 22;
    remap("V1", V1);

    if (fields.empty()) return 23;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T2(field.parse_real(&ok));
    if (!ok) return 24;
    if (!FP::defined(T2.read().toReal())) return 25;
    remap("T2", T2);

    if (fields.empty()) return 26;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root flags(field.parse_i64(&ok));
    if (!ok) return 27;
    if (!INTEGER::defined(flags.read().toInteger())) return 28;
    remap("FRAW", flags);

    if (config.front().strictMode) {
        if (!fields.empty())
            return 99;
    }

    Variant::Flags reportedFlags;
    if (INTEGER::defined(flags.read().toInteger())) {
        auto flagsBits = flags.read().toInteger();
        for (size_t i = 0;
                i < sizeof(instrumentFlagTranslation) / sizeof(instrumentFlagTranslation[0]);
                i++) {
            if (flagsBits & (static_cast<std::int_fast64_t>(1) << i)) {
                if (instrumentFlagTranslation[i]) {
                    reportedFlags.insert(instrumentFlagTranslation[i]);
                } else {
                    qCDebug(log) << "Unrecognized bit" << hex << ((1 << i))
                                 << "set in instrument flags";
                }
            }
        }
    }
    Variant::Root F1(reportedFlags);
    remap("F1", F1);
    if (priorRealtimeFlags != reportedFlags) {
        priorRealtimeFlags = reportedFlags;
        realtimeStateUpdated = true;
    }

    if (!haveEmittedLogMeta && loggingEgress && FP::defined(startTime)) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress && FP::defined(endTime)) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(endTime);
        Util::append(buildRealtimeMeta(endTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    logValue(startTime, endTime, "WS", std::move(WS));
    logValue(startTime, endTime, "WD", std::move(WD));
    logValue(startTime, endTime, "T1", std::move(T1));
    logValue(startTime, endTime, "T2", std::move(T2));
    realtimeValue(endTime, "V1", std::move(V1));
    realtimeValue(endTime, "V2", std::move(V2));

    logValue(startTime, endTime, "F1", std::move(F1));

    if (realtimeEgress && realtimeStateUpdated) {
        if (reportedFlags.empty()) {
            realtimeStateUpdated = false;
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), endTime,
                                  FP::undefined()));
        } else {
            realtimeStateUpdated = true;
            if (reportedFlags.size() == 1) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(*reportedFlags.begin()),
                                      endTime, FP::undefined()));
            } else {
                auto sorted = Util::to_qstringlist(reportedFlags);
                std::sort(sorted.begin(), sorted.end());
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        QObject::tr("Status: %1").arg(sorted.join(","))), endTime,
                                                           FP::undefined()));
            }
        }
    }

    return 0;
}

void AcquireVaisalaWMT700::invalidateLogValues(double frameTime)
{
    loggingLost(frameTime);
    lastRecordTime = FP::undefined();
    autoprobeValidRecords = 0;
    realtimeStateUpdated = true;
}

static bool validDigit(char c)
{ return c >= '0' && c <= '9'; }

static bool isOnlyDigits(const Util::ByteView &frame)
{
    if (frame.empty())
        return false;
    for (auto p : frame) {
        if (!validDigit(p))
            return false;
    }
    return true;
}

static bool isErrorResponse(const Util::ByteView &frame)
{
    auto str = frame.toString();
    return Util::contains(str, "ERROR") ||
            Util::contains(str, "Command does not exist") ||
            Util::contains(str, "unknown parameter");
}

void AcquireVaisalaWMT700::invalidStartupResponse(const Util::ByteView &frame, double frameTime)
{
    qCDebug(log) << "Invalid response" << frame << "in interactive start state" << responseState
                 << "at" << Logging::time(frameTime);

    if (realtimeEgress) {
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"), frameTime,
                              FP::undefined()));
    }

    responseState = RESP_INTERACTIVE_RESTART_WAIT;
    if (controlStream)
        controlStream->resetControl();
    timeoutAt(FP::undefined());
    discardData(frameTime + 10.0);
    invalidateLogValues(frameTime);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
    autoprobeStatusUpdated();
}

void AcquireVaisalaWMT700::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    {
        Q_ASSERT(!config.empty());
        if (!Range::intersectShift(config, frameTime)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.empty());
    }

    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        double startTime;
        if (!FP::defined(lastRecordTime)) {
            startTime = FP::undefined();
            lastRecordTime = frameTime;
        } else {
            startTime = lastRecordTime;
            if (lastRecordTime != frameTime)
                lastRecordTime = frameTime;
        }

        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 5) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_PASSIVE_RUN;
                realtimeStateUpdated = true;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                timeoutAt(frameTime + unpolledResponseTime + 1.0);

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
        } else if (code > 0) {
            autoprobeValidRecords = 0;
            invalidateLogValues(frameTime);
        } else {
            invalidateLogValues(frameTime);
        }
        break;
    }
    case RESP_PASSIVE_WAIT: {
        double startTime;
        if (!FP::defined(lastRecordTime)) {
            startTime = FP::undefined();
            lastRecordTime = frameTime;
        } else {
            startTime = lastRecordTime;
            if (lastRecordTime != frameTime)
                lastRecordTime = frameTime;
        }

        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);

            responseState = RESP_PASSIVE_RUN;
            realtimeStateUpdated = true;
            ++autoprobeValidRecords;
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else if (code > 0) {
            autoprobeValidRecords = 0;
            invalidateLogValues(frameTime);
        } else {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID: {
        double startTime;
        if (!FP::defined(lastRecordTime)) {
            startTime = FP::undefined();
            lastRecordTime = frameTime;
        } else {
            startTime = lastRecordTime;
            if (lastRecordTime != frameTime)
                lastRecordTime = frameTime;
        }

        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            timeoutAt(frameTime + unpolledResponseTime + 1.0);
            responseState = RESP_UNPOLLED_RUN;
            realtimeStateUpdated = true;
            ++autoprobeValidRecords;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            autoprobeValidRecords = 0;

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream)
                controlStream->resetControl();
            discardData(frameTime + 30.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

            invalidateLogValues(frameTime);
        } else {
            invalidateLogValues(frameTime);
        }
    }
        break;

    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN: {
        double startTime;
        if (!FP::defined(lastRecordTime)) {
            startTime = FP::undefined();
            lastRecordTime = frameTime;
        } else {
            startTime = lastRecordTime;
            if (lastRecordTime != frameTime)
                lastRecordTime = frameTime;
        }

        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + unpolledResponseTime + 1.0);
            ++autoprobeValidRecords;
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            Variant::Write info = Variant::Write::empty();

            if (responseState == RESP_PASSIVE_RUN) {
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
                info.hash("ResponseState").setString("PassiveRun");
            } else {
                responseState = RESP_INTERACTIVE_START_FLUSH;
                timeoutAt(frameTime + unpolledResponseTime * 2 + 10.0);
                discardData(frameTime + unpolledResponseTime + 2.0);
                if (controlStream) {
                    Util::ByteArray command("\r\r\n\r\r$");
                    command.push_back(config.front().address);
                    command += "OPEN\r";
                    controlStream->writeControl(std::move(command));
                }
                info.hash("ResponseState").setString("Run");
            }
            generalStatusUpdated();
            autoprobeValidRecords = 0;

            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        } else {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_VERSION: {
        if (isErrorResponse(frame))
            return invalidStartupResponse(frame, frameTime);

        Variant::Root value;
        auto trimmed = Util::ByteView(frame).string_trimmed();
        if (trimmed.string_start(">")) {
            trimmed = trimmed.mid(1);
            trimmed = trimmed.string_trimmed();
        }
        if (isOnlyDigits(trimmed))
            value.write().setInteger(trimmed.parse_i64());
        else
            value.write().setString(trimmed.toString());

        if (instrumentMeta["SerialNumber"] != value.read()) {
            instrumentMeta["SerialNumber"].set(value);
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveGetSerialNumber"), frameTime, FP::undefined()));
        }

        responseState = RESP_INTERACTIVE_START_GET_SERIAL_NUMBER;
        timeoutAt(frameTime + 2.0);
        if (controlStream) {
            controlStream->writeControl("G serial_n\r");
        }

        break;
    }
    case RESP_INTERACTIVE_START_GET_SERIAL_NUMBER: {
        if (isErrorResponse(frame))
            return invalidStartupResponse(frame, frameTime);

        auto trimmed = Util::ByteView(frame).string_trimmed();
        if (trimmed.string_start(">")) {
            trimmed = trimmed.mid(1);
            trimmed = trimmed.string_trimmed();
        }
        if (trimmed.string_start("s serial_n")) {
            trimmed = trimmed.mid(10);
            trimmed = trimmed.string_trimmed();
        }
        if (trimmed.string_start(",")) {
            trimmed = trimmed.mid(1);
            trimmed = trimmed.string_trimmed();
        }

        Variant::Root value;
        if (isOnlyDigits(trimmed))
            value.write().setInteger(trimmed.parse_i64());
        else
            value.write().setString(trimmed.toString());

        if (instrumentMeta["SerialNumber"] != value.read()) {
            instrumentMeta["SerialNumber"].set(value);
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveGetPCBSerial"), frameTime, FP::undefined()));
        }

        responseState = RESP_INTERACTIVE_START_GET_PCB_SERIAL;
        timeoutAt(frameTime + 2.0);
        if (controlStream) {
            controlStream->writeControl("G serial_pcb\r");
        }

        break;
    }
    case RESP_INTERACTIVE_START_GET_PCB_SERIAL: {
        if (isErrorResponse(frame))
            return invalidStartupResponse(frame, frameTime);

        auto trimmed = Util::ByteView(frame).string_trimmed();
        if (trimmed.string_start(">")) {
            trimmed = trimmed.mid(1);
            trimmed = trimmed.string_trimmed();
        }
        if (trimmed.string_start("s serial_pcb")) {
            trimmed = trimmed.mid(12);
            trimmed = trimmed.string_trimmed();
        }
        if (trimmed.string_start(",")) {
            trimmed = trimmed.mid(1);
            trimmed = trimmed.string_trimmed();
        }

        Variant::Root value;
        if (isOnlyDigits(trimmed))
            value.write().setInteger(trimmed.parse_i64());
        else
            value.write().setString(trimmed.toString());

        if (instrumentMeta["PCBNumber"] != value.read()) {
            instrumentMeta["PCBNumber"].set(value);
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveGetCalibrationDate"), frameTime, FP::undefined()));
        }

        responseState = RESP_INTERACTIVE_START_GET_CALIBRATION_DATE;
        timeoutAt(frameTime + 2.0);
        if (controlStream) {
            controlStream->writeControl("G cal_date\r");
        }

        break;
    }
    case RESP_INTERACTIVE_START_GET_CALIBRATION_DATE: {
        if (isErrorResponse(frame))
            return invalidStartupResponse(frame, frameTime);

        auto trimmed = Util::ByteView(frame).string_trimmed();
        if (trimmed.string_start(">")) {
            trimmed = trimmed.mid(1);
            trimmed = trimmed.string_trimmed();
        }
        if (trimmed.string_start("s cal_date")) {
            trimmed = trimmed.mid(10);
            trimmed = trimmed.string_trimmed();
        }
        if (trimmed.string_start(",")) {
            trimmed = trimmed.mid(1);
            trimmed = trimmed.string_trimmed();
        }

        Variant::Root value(trimmed.toString());
        if (instrumentMeta["CalibrationDate"] != value.read()) {
            instrumentMeta["CalibrationDate"].set(value);
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetUnits"),
                                  frameTime, FP::undefined()));
        }

        responseState = RESP_INTERACTIVE_START_SET_UNITS;
        timeoutAt(frameTime + 2.0);
        if (controlStream) {
            controlStream->writeControl("S wndUnit,0\r");
        }

        break;
    }

    case RESP_INTERACTIVE_START_SET_UNITS: {
        if (isErrorResponse(frame))
            return invalidStartupResponse(frame, frameTime);

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetVectorMode"), frameTime, FP::undefined()));
        }

        responseState = RESP_INTERACTIVE_START_SET_VECTOR;
        timeoutAt(frameTime + 2.0);
        if (controlStream) {
            controlStream->writeControl("S wndVector,1\r");
        }

        break;
    }
    case RESP_INTERACTIVE_START_SET_VECTOR: {
        if (isErrorResponse(frame))
            return invalidStartupResponse(frame, frameTime);

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetReportRecord"), frameTime, FP::undefined()));
        }

        responseState = RESP_INTERACTIVE_START_SET_REPORT;
        timeoutAt(frameTime + 2.0);
        if (controlStream) {
            controlStream->writeControl("S autoSend,24\r");
        }

        break;
    }
    case RESP_INTERACTIVE_START_SET_REPORT: {
        if (isErrorResponse(frame))
            return invalidStartupResponse(frame, frameTime);

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetReportMode"), frameTime, FP::undefined()));
        }

        responseState = RESP_INTERACTIVE_START_SET_INTERVAL;
        timeoutAt(frameTime + 2.0);
        if (controlStream) {
            Util::ByteArray command("S autoInt,");
            double interval = unpolledResponseTime;
            if (interval < 0.25)
                interval = 0.25;
            else if (interval > 1000)
                interval = 1000;
            interval = std::round(interval / 0.25) * 0.25;
            command += QByteArray::number(interval, 'f', 2);
            command.push_back('\r');
            controlStream->writeControl(std::move(command));
        }

        break;
    }
    case RESP_INTERACTIVE_START_SET_INTERVAL: {
        if (isErrorResponse(frame))
            return invalidStartupResponse(frame, frameTime);

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveContinuous"),
                                                       frameTime, FP::undefined()));
        }

        responseState = RESP_INTERACTIVE_START_ENABLE_CONTINUOUS;
        discardData(frameTime + 1.0);
        timeoutAt(frameTime + 3.0);
        if (controlStream) {
            controlStream->writeControl("START\r");
        }

        break;
    }

    default:
        break;
    }

}

void AcquireVaisalaWMT700::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    {
        Q_ASSERT(!config.empty());
        if (!Range::intersectShift(config, frameTime)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.empty());
    }

    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        qCDebug(log) << "Timeout in unpolled state" << responseState << "at"
                     << Logging::time(frameTime);
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        realtimeStateUpdated = true;

        timeoutAt(frameTime + unpolledResponseTime + 30.0);

        responseState = RESP_INTERACTIVE_START_FLUSH;
        timeoutAt(frameTime + unpolledResponseTime * 2 + 10.0);
        discardData(frameTime + unpolledResponseTime + 2.0);
        if (controlStream) {
            Util::ByteArray command("\r\r\n\r\r$");
            command.push_back(config.front().address);
            command += "OPEN\r";
            controlStream->writeControl(std::move(command));
        }
        generalStatusUpdated();

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("UnpolledRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        realtimeStateUpdated = true;

        timeoutAt(FP::undefined());

        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_START_FLUSH:
    case RESP_INTERACTIVE_START_ENABLE_MESSAGES:
    case RESP_INTERACTIVE_START_VERSION:
    case RESP_INTERACTIVE_START_GET_SERIAL_NUMBER:
    case RESP_INTERACTIVE_START_GET_PCB_SERIAL:
    case RESP_INTERACTIVE_START_GET_CALIBRATION_DATE:
    case RESP_INTERACTIVE_START_SET_UNITS:
    case RESP_INTERACTIVE_START_SET_VECTOR:
    case RESP_INTERACTIVE_START_SET_REPORT:
    case RESP_INTERACTIVE_START_SET_INTERVAL:
    case RESP_INTERACTIVE_START_ENABLE_CONTINUOUS:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        realtimeStateUpdated = true;

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        timeoutAt(FP::undefined());
        if (controlStream)
            controlStream->resetControl();
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        realtimeStateUpdated = true;

        responseState = RESP_INTERACTIVE_START_FLUSH;
        timeoutAt(frameTime + unpolledResponseTime * 2 + 10.0);
        discardData(frameTime + unpolledResponseTime + 2.0);
        if (controlStream) {
            Util::ByteArray command("\r\r\n\r\r$");
            command.push_back(config.front().address);
            command += "OPEN\r";
            controlStream->writeControl(std::move(command));
        }
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        invalidateLogValues(frameTime);
        break;

    default:
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireVaisalaWMT700::discardDataCompleted(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    {
        Q_ASSERT(!config.empty());
        if (!Range::intersectShift(config, frameTime)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.empty());
    }

    Q_ASSERT(!config.empty());
    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_INTERACTIVE_START_FLUSH:
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveEnableMessages"), frameTime, FP::undefined()));
        }
        realtimeStateUpdated = true;

        responseState = RESP_INTERACTIVE_START_ENABLE_MESSAGES;
        timeoutAt(frameTime + 5.0);
        discardData(frameTime + 2.0);
        if (controlStream) {
            controlStream->writeControl("S messages,1\r");
        }
        break;

    case RESP_INTERACTIVE_START_ENABLE_MESSAGES:
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveReadVersion"),
                                                       frameTime, FP::undefined()));
        }
        realtimeStateUpdated = true;

        responseState = RESP_INTERACTIVE_START_VERSION;
        timeoutAt(frameTime + 2.0);
        if (controlStream) {
            controlStream->writeControl("VERSION\r");
        }
        break;

    case RESP_INTERACTIVE_START_ENABLE_CONTINUOUS:
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadFirstRecord"), frameTime, FP::undefined()));
        }

        responseState = RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID;
        discardData(frameTime + unpolledResponseTime + 1.0);
        timeoutAt(frameTime + unpolledResponseTime * 2.0 + 10.0);
        if (controlStream) {
            controlStream->writeControl("CLOSE\r");
        }

        break;

    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        realtimeStateUpdated = true;

        responseState = RESP_INTERACTIVE_START_FLUSH;
        timeoutAt(frameTime + unpolledResponseTime * 2 + 10.0);
        discardData(frameTime + unpolledResponseTime + 2.0);
        if (controlStream) {
            Util::ByteArray command("\r\r\n\r\r$");
            command.push_back(config.front().address);
            command += "OPEN\r";
            controlStream->writeControl(std::move(command));
        }
        break;

    default:
        break;
    }
}


void AcquireVaisalaWMT700::incomingControlFrame(const Util::ByteArray &, double)
{ }

Variant::Root AcquireVaisalaWMT700::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireVaisalaWMT700::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}


AcquisitionInterface::AutoprobeStatus AcquireVaisalaWMT700::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireVaisalaWMT700::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    {
        Q_ASSERT(!config.empty());
        if (!Range::intersectShift(config, time)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.empty());
    }

    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << " at " << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_FLUSH:
    case RESP_INTERACTIVE_START_ENABLE_MESSAGES:
    case RESP_INTERACTIVE_START_VERSION:
    case RESP_INTERACTIVE_START_GET_SERIAL_NUMBER:
    case RESP_INTERACTIVE_START_GET_PCB_SERIAL:
    case RESP_INTERACTIVE_START_GET_CALIBRATION_DATE:
    case RESP_INTERACTIVE_START_SET_UNITS:
    case RESP_INTERACTIVE_START_SET_VECTOR:
    case RESP_INTERACTIVE_START_SET_REPORT:
    case RESP_INTERACTIVE_START_SET_INTERVAL:
    case RESP_INTERACTIVE_START_ENABLE_CONTINUOUS:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from passive" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        responseState = RESP_INTERACTIVE_START_FLUSH;
        timeoutAt(time + unpolledResponseTime * 2 + 10.0);
        discardData(time + unpolledResponseTime + 2.0);
        if (controlStream) {
            Util::ByteArray command("\r\r\n\r\r$");
            command.push_back(config.front().address);
            command += "OPEN\r";
            controlStream->writeControl(std::move(command));
        }

        generalStatusUpdated();
        break;
    }
}

void AcquireVaisalaWMT700::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    {
        Q_ASSERT(!config.empty());
        if (!Range::intersectShift(config, time)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.empty());
    }

    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobeValidRecords = 0;
    discardData(time + 0.5, 1);
    timeoutAt(time + unpolledResponseTime * 7.0 + 3.0);
    generalStatusUpdated();
}

void AcquireVaisalaWMT700::autoprobePromote(double time)
{
    PauseLock paused(*this);

    {
        Q_ASSERT(!config.empty());
        if (!Range::intersectShift(config, time)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.empty());
    }

    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        qCDebug(log) << "Promoted from interactive run to interactive acquisition at"
                     << Logging::time(time);
        /* Reset timeout in case we didn't have a control stream */
        timeoutAt(time + unpolledResponseTime + 2.0);
        break;

    case RESP_INTERACTIVE_START_FLUSH:
    case RESP_INTERACTIVE_START_ENABLE_MESSAGES:
    case RESP_INTERACTIVE_START_VERSION:
    case RESP_INTERACTIVE_START_GET_SERIAL_NUMBER:
    case RESP_INTERACTIVE_START_GET_PCB_SERIAL:
    case RESP_INTERACTIVE_START_GET_CALIBRATION_DATE:
    case RESP_INTERACTIVE_START_SET_UNITS:
    case RESP_INTERACTIVE_START_SET_VECTOR:
    case RESP_INTERACTIVE_START_SET_REPORT:
    case RESP_INTERACTIVE_START_SET_INTERVAL:
    case RESP_INTERACTIVE_START_ENABLE_CONTINUOUS:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);
        break;

    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        responseState = RESP_INTERACTIVE_START_FLUSH;
        timeoutAt(time + unpolledResponseTime * 2 + 10.0);
        discardData(time + unpolledResponseTime + 2.0);
        if (controlStream) {
            Util::ByteArray command("\r\r\n\r\r$");
            command.push_back(config.front().address);
            command += "OPEN\r";
            controlStream->writeControl(std::move(command));
        }

        generalStatusUpdated();
        break;
    }
}

void AcquireVaisalaWMT700::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    {
        Q_ASSERT(!config.empty());
        if (!Range::intersectShift(config, time)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.empty());
    }

    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    /* Reset timeout in case we didn't have a control stream */
    timeoutAt(time + unpolledResponseTime + 2.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);

        break;

    case RESP_UNPOLLED_RUN:
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from unpolled interactive state to passive acquisition at"
                     << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;

        discardData(FP::undefined());
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireVaisalaWMT700::getDefaults()
{
    AutomaticDefaults result;
    result.name = "XM$2";
    result.setSerialN81(9600);
    return result;
}


ComponentOptions AcquireVaisalaWMT700Component::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options);

    return options;
}

ComponentOptions AcquireVaisalaWMT700Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireVaisalaWMT700Component::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireVaisalaWMT700Component::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireVaisalaWMT700Component::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options);
}

QList<ComponentExample> AcquireVaisalaWMT700Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireVaisalaWMT700Component::createAcquisitionPassive(const ComponentOptions &options,
                                                                                      const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireVaisalaWMT700(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireVaisalaWMT700Component::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                      const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireVaisalaWMT700(config, loggingContext)); }

std::unique_ptr<AcquisitionInterface> AcquireVaisalaWMT700Component::createAcquisitionAutoprobe(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireVaisalaWMT700> i(new AcquireVaisalaWMT700(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireVaisalaWMT700Component::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireVaisalaWMT700> i(new AcquireVaisalaWMT700(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
