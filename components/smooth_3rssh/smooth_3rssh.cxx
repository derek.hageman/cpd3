/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "smoothing/smoothingengine.hxx"

#include "smooth_3rssh.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;


QString Smooth3RSSHComponent::getGeneralSerializationName() const
{ return QString::fromLatin1("smooth_3rssh"); }

ComponentOptions Smooth3RSSHComponent::getOptions()
{
    ComponentOptions options;

    TimeIntervalSelectionOption *tis =
            new TimeIntervalSelectionOption(tr("gap", "name"), tr("The maximum allowed gap"),
                                            tr("If two values are seperated by this much time then the smoother is "
                                                       "reset.  That is, data separated by this much will be smoothed as "
                                                       "independent runs of data."),
                                            tr("Infinite", "default gap"));
    tis->setAllowZero(true);
    tis->setAllowNegative(false);
    tis->setAllowUndefined(true);
    tis->setDefaultAligned(true);
    options.add("gap", tis);

    options.add("ignore-undefined", new ComponentOptionBoolean(tr("ignore-undefined", "name"),
                                                               tr("Ignore undefined values"),
                                                               tr("If this option is set undefined values are ignored instead of "
                                                                          "forcing a smoother reset."),
                                                               tr("Disabled, undefined values cause a reset",
                                                                  "default ignore undefined mode")));

    return options;
}

QList<ComponentExample> Smooth3RSSHComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Default", "default example name"),
                                     tr("This smooths all available data with a Tukey 3RSSH smoother.  "
                                                "Gaps of any size are smoothed over (taken as continuous "
                                                "sequences of data) but undefined values cause the smoother to "
                                                "reset.")));

    return examples;
}

ProcessingStage *Smooth3RSSHComponent::createGeneralFilterDynamic(const ComponentOptions &options)
{
    bool smoothUndefined = false;
    if (options.isSet("ignore-undefined")) {
        smoothUndefined =
                qobject_cast<ComponentOptionBoolean *>(options.get("ignore-undefined"))->get();
    }

    DynamicTimeInterval *gap;
    if (options.isSet("gap")) {
        gap = qobject_cast<TimeIntervalSelectionOption *>(options.get("gap"))->getInterval();
    } else {
        gap = NULL;
    }

    return SmoothingEngine::createTukey3RSSH(gap, smoothUndefined);
}

ProcessingStage *Smooth3RSSHComponent::createGeneralFilterEditing(double start,
                                                                  double end,
                                                                  const SequenceName::Component &station,
                                                                  const SequenceName::Component &archive,
                                                                  const ValueSegment::Transfer &config)
{
    Q_UNUSED(station);
    Q_UNUSED(archive);

    DynamicTimeInterval *gap = DynamicTimeInterval::fromConfiguration(config, "Gap", start, end);
    bool smoothUndefined = false;
    for (const auto &seg : config) {
        if (!Range::intersects(seg.getStart(), seg.getEnd(), start, end))
            continue;
        if (seg.getValue().getPath("IgnoreUndefined").exists()) {
            smoothUndefined = seg.getValue().getPath("IgnoreUndefined").toBool();
        }
    }

    return SmoothingEngine::createTukey3RSSH(gap, smoothUndefined);
}

ProcessingStage *Smooth3RSSHComponent::deserializeGeneralFilter(QDataStream &stream)
{ return SmoothingEngine::deserializeTukey3RSSH(stream); }
