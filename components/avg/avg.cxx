/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "smoothing/contamfilter.hxx"
#include "smoothing/smoothingengine.hxx"
#include "smoothing/fixedtime.hxx"

#include "avg.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;


QString AvgComponent::getGeneralSerializationName() const
{ return QString::fromLatin1("avg"); }

ComponentOptions AvgComponent::getOptions()
{
    ComponentOptions options;

    options.add("contamination", new ComponentOptionSingleString(tr("contamination", "name"),
                                                                 tr("The contamination mode"),
                                                                 tr("This defines the contamination mode name in effect.  The contamination "
                                                                            "mode determines which variables are removed when they are flagged "
                                                                            "as contaminated.  This often corisponds with various profiles for "
                                                                            " other station specific configuration.  For example the \"aerosol\" mode generally "
                                                                            "removes scattering, absorption, extinction and concentrations.  "
                                                                            "The special mode \"none\" or \"disable\" turns off all "
                                                                            "contamination removal and values are averaged regardless of their "
                                                                            "contaminated state."),
                                                                 tr("aerosol",
                                                                    "default contamination mode")));

    TimeIntervalSelectionOption *tis =
            new TimeIntervalSelectionOption(tr("interval", "name"), tr("The averaging interval"),
                                            tr("This is width of the bin to average on.  All data within each "
                                                       "interval is averaged together to produce the output.  An "
                                                       "undefined interval averages all available data together."),
                                            tr("One hour", "default interval"), 1);
    tis->setAllowZero(false);
    tis->setAllowNegative(false);
    tis->setAllowUndefined(true);
    tis->setDefaultAligned(true);
    options.add("interval", tis);

    tis = new TimeIntervalSelectionOption(tr("gap", "name"), tr("The maximum allowed gap"),
                                          tr("If two values are seperated by this much time then the average "
                                                     "is split.  Even if there is no gap the values may still be split "
                                                     "into separate bins if the interval demands it.  An undefined gap "
                                                     "allows for infinite separation."),
                                          tr("Infinite", "default gap"));
    tis->setAllowZero(true);
    tis->setAllowNegative(false);
    tis->setAllowUndefined(true);
    tis->setDefaultAligned(true);
    options.add("gap", tis);

    options.add("continuous", new ComponentOptionBoolean(tr("continuous", "name"),
                                                         tr("Produce only continuous averages"),
                                                         tr("If this option is set then the averages produced are the maximum "
                                                                    "continuous averages within the binning interval.  This allows "
                                                                    "difference measurements to be repeatable but may cause splitting "
                                                                    "within the interval."),
                                                         tr("Disabled",
                                                            "default continuous mode")));
    options.exclude("gap", "continuous");
    options.exclude("continuous", "gap");

    DynamicDoubleOption *fi = new DynamicDoubleOption(tr("cover", "name"),
                                                      tr("The fraction of data required to exist"),
                                                      tr("This is the fraction of data required to exist for an average "
                                                                     "bin to be produced at all.  For example if this is set to 0.95 "
                                                                     "then averages will only be produced when the fraction of "
                                                                     "missing data in a bin is greater than 95% of the total bin.  So, "
                                                                     "for a one hour bin that would require at least 57 minutes of "
                                                                     "valid data.  Setting this to undefined disables the requirement."),
                                                      tr("Disabled", "default cover fraction"));
    options.add("cover", fi);

    return options;
}

QList<ComponentExample> AvgComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will average input data to one hour intervals and remove "
                                                "contamination from the standard set of variables involved in an "
                                                "aerosol system (generally scattering, absorption, extinction and "
                                                "concentrations).")));

    options = getOptions();
    (qobject_cast<TimeIntervalSelectionOption *>(options.get("interval")))->set(Time::Day, 1, true);
    (qobject_cast<DynamicDoubleOption *>(options.get("cover")))->set(0.95);
    (qobject_cast<ComponentOptionSingleString *>(options.get("contamination")))->set(
            tr("none", "contamination none"));
    examples.append(ComponentExample(options, tr("Interval and coverage requirement"),
                                     tr("This will average data to one day interval requiring at least 95% "
                                                "of data within each day to exist (1368 minutes).  Contaminated "
                                                "data is not excluded from the averages.")));

    options = getOptions();
    (qobject_cast<TimeIntervalSelectionOption *>(options.get("interval")))->setUndefined();
    (qobject_cast<ComponentOptionBoolean *>(options.get("continuous")))->set(true);
    examples.append(ComponentExample(options, tr("Infinite continuous averages"),
                                     tr("This will average all input data to the maximum extent possible "
                                                "but will break up those averages whenever there is a "
                                                "discontinuity in the input data.  This also excludes "
                                                "contaminated values for all standard aerosol parameters.")));

    return examples;
}

enum SerializeMode {
    ContaminateFilter_Conventional = 0,
    ContaminateFilter_Continuous,
    Unfiltered_Conventional,
    Unfiltered_Continuous,
};

ProcessingStage *AvgComponent::createGeneralFilterDynamic(const ComponentOptions &options)
{
    return createGeneralFilterPredefined(options, FP::undefined(), FP::undefined(),
                                         QList<SequenceName>());
}

class AvgSmoothingEngine : public SmoothingEngineFixedTime {
    SerializeMode mode;
public:
    AvgSmoothingEngine(SerializeMode sm,
                       DynamicTimeInterval *setInterval,
                       DynamicTimeInterval *setGap = NULL,
                       DynamicDouble *setRequiredCover = NULL,
                       bool setGapPreserving = false) : SmoothingEngineFixedTime(setInterval,
                                                                                 setGap,
                                                                                 setRequiredCover,
                                                                                 setGapPreserving),
                                                        mode(sm)
    { }

    ~AvgSmoothingEngine()
    { }

    void serialize(QDataStream &stream) override
    {
        stream << (quint8) mode;
        SmoothingEngineFixedTime::serialize(stream);
    }
};

ProcessingStage *AvgComponent::createGeneralFilterPredefined(const ComponentOptions &options,
                                                             double start,
                                                             double end,
                                                             const QList<SequenceName> &inputs)
{
    Q_UNUSED(start);
    Q_UNUSED(end);

    DynamicTimeInterval *interval;
    if (options.isSet("interval")) {
        interval =
                qobject_cast<TimeIntervalSelectionOption *>(options.get("interval"))->getInterval();
    } else {
        interval = new DynamicTimeInterval::Constant(Time::Hour, 1, true);
    }

    DynamicDouble *requiredCover;
    if (options.isSet("cover")) {
        requiredCover = qobject_cast<DynamicDoubleOption *>(options.get("cover"))->getInput();
    } else {
        requiredCover = NULL;
    }

    QString contamMode("aerosol");
    if (options.isSet("contamination")) {
        contamMode =
                qobject_cast<ComponentOptionSingleString *>(options.get("contamination"))->get()
                                                                                         .toLower();
        if (contamMode == tr("none", "contamination none") ||
                contamMode == tr("disable", "contamination disable")) {
            contamMode.clear();
        }
    }

    bool cont = false;
    if (options.isSet("continuous")) {
        cont = qobject_cast<ComponentOptionBoolean *>(options.get("continuous"))->get();
    }

    SmoothingEngineFixedTime *engine;
    if (cont) {
        engine = new AvgSmoothingEngine(
                contamMode.isEmpty() ? Unfiltered_Continuous : ContaminateFilter_Continuous,
                interval, NULL, requiredCover, true);
    } else {
        DynamicTimeInterval *gap;
        if (options.isSet("gap")) {
            gap = qobject_cast<TimeIntervalSelectionOption *>(options.get("gap"))->getInterval();
        } else {
            gap = NULL;
        }

        engine = new AvgSmoothingEngine(
                contamMode.isEmpty() ? Unfiltered_Conventional : ContaminateFilter_Conventional,
                interval, gap, requiredCover, false);
    }

    if (!inputs.isEmpty()) {
        engine->registerExpectedInputs(QSet<SequenceName>::fromList(inputs));
    }

    if (contamMode.isEmpty())
        return engine;

    return new SmoothingContaminationFilter(engine, contamMode);
}

ProcessingStage *AvgComponent::createGeneralFilterEditing(double start,
                                                          double end,
                                                          const SequenceName::Component &station,
                                                          const SequenceName::Component &archive,
                                                          const ValueSegment::Transfer &config)
{
    Q_UNUSED(station);
    Q_UNUSED(archive);


    DynamicTimeInterval::Variable *defaultTime = new DynamicTimeInterval::Variable;
    defaultTime->set(Time::Hour, 1, true);
    DynamicTimeInterval
            *interval = DynamicTimeInterval::fromConfiguration(config, "Interval", start, end, true,
                                                     defaultTime);
    delete defaultTime;

    DynamicTimeInterval *gap = DynamicTimeInterval::fromConfiguration(config, "Gap", start, end);

    DynamicDouble *requiredCover =
            DynamicDoubleOption::fromConfiguration(config, "RequiredCoverage", start, end);

    return new AvgSmoothingEngine(Unfiltered_Conventional, interval, gap, requiredCover, false);
}

void AvgComponent::extendGeneralFilterEditing(double &start,
                                              double &end,
                                              const SequenceName::Component &,
                                              const SequenceName::Component &,
                                              const ValueSegment::Transfer &config,
                                              Archive::Access *)
{
    DynamicTimeInterval
            *interval = DynamicTimeInterval::fromConfiguration(config, "Interval", start, end);

    start = interval->roundConst(start, start, false);
    end = interval->roundConst(end, end, true);

    delete interval;
}

ProcessingStage *AvgComponent::deserializeGeneralFilter(QDataStream &stream)
{
    quint8 i8;
    stream >> i8;

    SmoothingEngine *engine = new AvgSmoothingEngine((SerializeMode) i8, NULL, NULL, NULL, true);
    engine->deserialize(stream);

    switch ((SerializeMode) i8) {
    case Unfiltered_Conventional:
    case Unfiltered_Continuous:
        return engine;
    case ContaminateFilter_Continuous:
    case ContaminateFilter_Conventional: {
        SmoothingContaminationFilter *filter = new SmoothingContaminationFilter(engine, QString());
        filter->deserialize(stream);
        return filter;
    }
    }
    Q_ASSERT(false);
    return NULL;
}
