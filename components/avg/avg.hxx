/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef AVG_H
#define AVG_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "datacore/processingstage.hxx"

class AvgComponent : public QObject, public CPD3::Data::ProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.avg"
                              FILE
                              "avg.json")

public:
    virtual QString getGeneralSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    QList<CPD3::ComponentExample> getExamples();


    virtual CPD3::Data::ProcessingStage *createGeneralFilterDynamic
            (const CPD3::ComponentOptions &options = CPD3::ComponentOptions());

    virtual CPD3::Data::ProcessingStage *createGeneralFilterPredefined
            (const CPD3::ComponentOptions &options,
             double start,
             double end, const QList<CPD3::Data::SequenceName> &inputs);

    virtual CPD3::Data::ProcessingStage *createGeneralFilterEditing(double start,
                                                                    double end,
                                                                    const CPD3::Data::SequenceName::Component &station,
                                                                    const CPD3::Data::SequenceName::Component &archive,
                                                                    const CPD3::Data::ValueSegment::Transfer &config);

    virtual void extendGeneralFilterEditing(double &start,
                                            double &end,
                                            const CPD3::Data::SequenceName::Component &station,
                                            const CPD3::Data::SequenceName::Component &archive,
                                            const CPD3::Data::ValueSegment::Transfer &config,
                                            CPD3::Data::Archive::Access *access);

    virtual CPD3::Data::ProcessingStage *deserializeGeneralFilter(QDataStream &stream);

};

#endif
