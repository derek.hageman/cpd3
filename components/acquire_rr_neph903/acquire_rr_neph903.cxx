/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/wavelength.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_rr_neph903.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;


AcquireRRNeph903::Configuration::Configuration() : start(FP::undefined()),
                                                   end(FP::undefined()),
                                                   wavelength(530.0),
                                                   strictMode(true),
                                                   calibratorFill(true),
                                                   reportInterval(1.0),
                                                   zeroRelay(-1),
                                                   spanRelay(-1)
{ }

AcquireRRNeph903::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          wavelength(other.wavelength),
          strictMode(other.strictMode),
          calibratorFill(other.calibratorFill),
          reportInterval(other.reportInterval),
          zeroRelay(other.zeroRelay),
          spanRelay(other.spanRelay)
{ }

AcquireRRNeph903::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          wavelength(530.0),
          strictMode(true),
          calibratorFill(true),
          reportInterval(1.0),
          zeroRelay(-1),
          spanRelay(-1)
{
    setFromSegment(other);
}

AcquireRRNeph903::Configuration::Configuration(const Configuration &under,
                                               const ValueSegment &over,
                                               double s,
                                               double e) : start(s),
                                                           end(e),
                                                           wavelength(under.wavelength),
                                                           strictMode(under.strictMode),
                                                           calibratorFill(under.calibratorFill),
                                                           reportInterval(under.reportInterval),
                                                           zeroRelay(under.zeroRelay),
                                                           spanRelay(under.spanRelay)
{
    setFromSegment(over);
}

void AcquireRRNeph903::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();
    if (config["CalibratorFillMissing"].exists())
        calibratorFill = config["CalibratorFillMissing"].toBool();

    {
        double v = config["ReportInterval"].toDouble();
        if (FP::defined(v) && v > 0.0)
            reportInterval = v;
    }

    if (config["Wavelength"].exists())
        wavelength = config["Wavelength"].toDouble();

    if (config["Relay/Zero"].exists()) {
        qint64 i = config["Relay/Zero"].toInt64();
        if (INTEGER::defined(i) && i >= 1 && i <= 2) {
            zeroRelay = (int) i - 1;
        } else {
            zeroRelay = -1;
        }
    }

    if (config["Relay/Spancheck"].exists()) {
        qint64 i = config["Relay/Spancheck"].toInt64();
        if (INTEGER::defined(i) && i >= 1 && i <= 2) {
            spanRelay = (int) i - 1;
        } else {
            spanRelay = -1;
        }
    }
}


static Variant::Root defaultSpancheckConfiguration()
{
    Variant::Root result;
    return result;
}

void AcquireRRNeph903::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("RadianceResearch");
    instrumentMeta["Model"].setString("M903");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    realtimeStateUpdated = true;

    priorScattering = FP::undefined();
    priorCalibrator = FP::undefined();
    lastScatteringTime = FP::undefined();
    lastCalibratorTime = FP::undefined();

    zeroEffectiveTime = FP::undefined();
    zeroPersistentUpdated = true;
    zeroRealtimeUpdated = true;
    zeroBsCount = 0;
    zeroBsSum = 0;
    zeroTCount = 0;
    zeroTSum = 0;
    zeroPCount = 0;
    zeroPSum = 0;

    /* Don't output these unless we receive a record that contains them */
    parametersPersistentUpdated = false;
    parametersRealtimeUpdated = false;

    spancheckDetails.setUnit(SequenceName({}, "raw", "ZSPANCHECK"));
    instrumentParameters.setUnit(SequenceName({}, "raw", "ZPARAMETERS"));
}

AcquireRRNeph903::AcquireRRNeph903(const ValueSegment::Transfer &configData,
                                   const std::string &loggingContext) : FramedInstrument("rr903",
                                                                                         loggingContext),
                                                                        lastRecordTime(
                                                                                FP::undefined()),
                                                                        autoprobeStatus(
                                                                                AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                        responseState(
                                                                                RESP_PASSIVE_WAIT),
                                                                        autoprobeValidRecords(0),
                                                                        loggingBaseTime(
                                                                                FP::undefined()),
                                                                        sampleState(SAMPLE_RUN),
                                                                        sampleStateEndTime(
                                                                                FP::undefined()),
                                                                        spancheckInterface(this),
                                                                        loggingMux(LogStream_TOTAL)
{
    spancheckController =
            new NephelometerSpancheckController(ValueSegment::withPath(configData, "Spancheck"),
                                                defaultSpancheckConfiguration());
    spancheckController->setInterface(&spancheckInterface);

    DynamicTimeInterval::Variable defaultFlush;
    defaultFlush.set(Time::Second, 62);
    zeroFlushTime =
            DynamicTimeInterval::fromConfiguration(configData, "Zero/Flush", FP::undefined(),
                                                   FP::undefined(), true, &defaultFlush);

    DynamicTimeInterval::Variable defaultSample;
    defaultSample.set(Time::Second, 300);
    zeroSampleTime =
            DynamicTimeInterval::fromConfiguration(configData, "Zero/Sample", FP::undefined(),
                                                   FP::undefined(), true, &defaultSample);

    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
    configurationChanged();
}

void AcquireRRNeph903::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void AcquireRRNeph903::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireRRNeph903Component::getBaseOptions()
{
    ComponentOptions options;

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(tr("wavelength", "name"), tr("Sample wavelength"),
                                            tr("This wavelength in nm of the optical filter in the nephelometer."),
                                            tr("530nm", "default wavelength"), 1);
    d->setMinimum(0.0, false);
    options.add("wavelength", d);

    return options;
}

AcquireRRNeph903::AcquireRRNeph903(const ComponentOptions &options,
                                   const std::string &loggingContext) : FramedInstrument("rr903",
                                                                                         loggingContext),
                                                                        lastRecordTime(
                                                                                FP::undefined()),
                                                                        autoprobeStatus(
                                                                                AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                        responseState(
                                                                                RESP_PASSIVE_WAIT),
                                                                        autoprobeValidRecords(0),
                                                                        loggingBaseTime(
                                                                                FP::undefined()),
                                                                        sampleState(SAMPLE_RUN),
                                                                        sampleStateEndTime(
                                                                                FP::undefined()),
                                                                        spancheckInterface(this),
                                                                        loggingMux(LogStream_TOTAL)
{
    spancheckController = new NephelometerSpancheckController(ValueSegment::Transfer(),
                                                              defaultSpancheckConfiguration());
    spancheckController->setInterface(&spancheckInterface);

    DynamicTimeInterval::Variable defaultFlush;
    defaultFlush.set(Time::Second, 62);
    zeroFlushTime = defaultFlush.clone();

    DynamicTimeInterval::Variable defaultSample;
    defaultSample.set(Time::Second, 300);
    zeroSampleTime = defaultSample.clone();

    setDefaultInvalid();

    config.append(Configuration());

    if (options.isSet("wavelength")) {
        double value =
                qobject_cast<ComponentOptionSingleDouble *>(options.get("wavelength"))->get();
        if (FP::defined(value) && value > 0.0)
            config.first().wavelength = value;
    }

    configurationChanged();
}

AcquireRRNeph903::~AcquireRRNeph903()
{
    delete spancheckController;
    delete zeroFlushTime;
    delete zeroSampleTime;
}


SequenceValue::Transfer AcquireRRNeph903::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_rr_neph903");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    const auto &wlCode = Wavelength::code(config.first().wavelength);

    result.emplace_back(SequenceName({}, "raw_meta", "Bs" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "Cs" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("00000");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Sample counts");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Scattering Counts"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);

    result.emplace_back(SequenceName({}, "raw_meta", "Bsf" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Calibrator light scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Calibrator"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "Cf" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("00000");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Calibrator counts");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Calibrator Counts"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "Cd"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Dark count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Dark"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(6);

    result.emplace_back(SequenceName({}, "raw_meta", "Cp" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("00000");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Sample raw PMT output with dark current removed");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "Cpf" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("00000");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Calibrator raw PMT output with dark current removed");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);


    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(9);

    result.emplace_back(SequenceName({}, "raw_meta", "T"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(9);

    result.emplace_back(SequenceName({}, "raw_meta", "U"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Sample RH");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(9);


    result.emplace_back(SequenceName({}, "raw_meta", "Tz"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature during zero");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "Pz"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure during zero");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "Bsz" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient during zero measurement");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Zero"));
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);


    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("Blank")
          .hash("Description")
          .setString("Data removed in blank period");
    result.back().write().metadataSingleFlag("Blank").hash("Bits").setInt64(0x40000000);
    result.back()
          .write()
          .metadataSingleFlag("Blank")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_rr_neph903");

    result.back()
          .write()
          .metadataSingleFlag("Zero")
          .hash("Description")
          .setString("Zero in progress");
    result.back().write().metadataSingleFlag("Zero").hash("Bits").setInt64(0x20000000);
    result.back()
          .write()
          .metadataSingleFlag("Zero")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_rr_neph903");

    result.back()
          .write()
          .metadataSingleFlag("Spancheck")
          .hash("Description")
          .setString("Spancheck in progress");
    result.back()
          .write()
          .metadataSingleFlag("Spancheck")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_rr_neph903");

    result.back()
          .write()
          .metadataSingleFlag("StaticZeroSubtraction")
          .hash("Description")
          .setString("Zero subtraction does not include up to date shift information");
    result.back()
          .write()
          .metadataSingleFlag("StaticZeroSubtraction")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_rr_neph903");


    result.emplace_back(SequenceName({}, "raw_meta", "ZPARAMETERS"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("Instrument parameter values");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataHash("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataHash("Realtime").hash("Hide").setBool(true);
    result.back().write().metadataHash("Realtime").hash("NetworkPriority").setInt64(1);

    result.back()
          .write()
          .metadataHashChild("Bsr" + wlCode)
          .metadataReal("Description")
          .setString("Rayleigh scattering coefficient");
    result.back()
          .write()
          .metadataHashChild("Bsr" + wlCode)
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelength);
    result.back()
          .write()
          .metadataHashChild("Bsr" + wlCode)
          .metadataReal("Format")
          .setString("0000.00");
    result.back()
          .write()
          .metadataHashChild("Bsr" + wlCode)
          .metadataReal("Units")
          .setString("Mm\xE2\x81\xBB¹");

    result.back()
          .write()
          .metadataHashChild("ZBsZeroRatio" + wlCode)
          .metadataReal("Description")
          .setString(
                  "Zero offset subtraction as a factor of the calibrator extinction coefficient");
    result.back()
          .write()
          .metadataHashChild("ZBsZeroRatio" + wlCode)
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelength);
    result.back()
          .write()
          .metadataHashChild("ZBsZeroRatio" + wlCode)
          .metadataReal("Format")
          .setString("0.00000");

    result.back()
          .write()
          .metadataHashChild("ZBsSpanRatio" + wlCode)
          .metadataReal("Description")
          .setString("Span gas scattering coefficient ratio");
    result.back()
          .write()
          .metadataHashChild("ZBsSpanRatio" + wlCode)
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelength);
    result.back()
          .write()
          .metadataHashChild("ZBsSpanRatio" + wlCode)
          .metadataReal("Format")
          .setString("0.00000");

    result.back()
          .write()
          .metadataHashChild("V")
          .metadataReal("Description")
          .setString("Photomultiplier tube voltage");
    result.back().write().metadataHashChild("V").metadataReal("Format").setString("0000");
    result.back().write().metadataHashChild("V").metadataReal("Units").setString("V");


    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECK"),
                        spancheckController->metadata(NephelometerSpancheckController::FullResults,
                                                      processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(2);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadata("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "PCTc" + wlCode),
                        spancheckController->metadata(
                                NephelometerSpancheckController::TotalPercentError, processing),
                        time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadata("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("Name").setString(QObject::tr("Error"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "Cc" + wlCode), spancheckController->metadata(
            NephelometerSpancheckController::TotalSensitivityFactor, processing), time,
                        FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadata("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Box")
          .setString(QObject::tr("Spancheck Results"));
    result.back().write().metadata("Realtime").hash("Name").setString(QObject::tr("Sensitivity"));
    result.back().write().metadata("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);

    return result;
}

SequenceValue::Transfer AcquireRRNeph903::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_rr_neph903");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    const auto &wlCode = Wavelength::code(config.first().wavelength);

    result.emplace_back(SequenceName({}, "raw_meta", "Bszd" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient zero shift");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Zero Shift"));
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);


    result.emplace_back(SequenceName({}, "raw_meta", "ZBs" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Scattering"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);


    result.emplace_back(SequenceName({}, "raw_meta", "V"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Photomultiplier tube voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "ZCpRaw"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00000");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Photomultiplier tube output");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("PMT Output"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(8);

    result.emplace_back(SequenceName({}, "raw_meta", "Cl"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Lamp brightness");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Lamp Brightness"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(7);


    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(10);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Zero")
          .setString(QObject::tr("Zero in progress", "zero state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Spancheck")
          .setString(QObject::tr("Spancheck in progress", "spancheck state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Blank")
          .setString(QObject::tr("Blank in progress", "blank state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidRecord")
          .setString(QObject::tr("NO COMMS: Invalid record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveExitMenu")
          .setString(QObject::tr("STARTING COMMS: Exiting menu"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveEnterMenu")
          .setString(QObject::tr("STARTING COMMS: Entering control menu"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadTime")
          .setString(QObject::tr("STARTING COMMS: Reading time"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetTime")
          .setString(QObject::tr("STARTING COMMS: Setting time"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetLong")
          .setString(QObject::tr("STARTING COMMS: Setting long mode records"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSettingZero")
          .setString(QObject::tr("STARTING COMMS: Opening zero relay"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSettingSpan")
          .setString(QObject::tr("STARTING COMMS: Opening spancheck relay"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveInitialDiscard")
          .setString(QObject::tr("STARTING COMMS: Flushing initial unpolled"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveUnpolledStartReadFirstRecord")
          .setString(QObject::tr("STARTING COMMS: Waiting for first record"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKSTATE"),
                        spancheckController->metadata(
                                NephelometerSpancheckController::RealtimeState, processing), time,
                        FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);

    return result;
}

SequenceMatch::Composite AcquireRRNeph903::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "Tz");
    sel.append({}, {}, "Pz");
    sel.append({}, {}, "ZPARAMETERS");
    sel.append({}, {}, "ZSPANCHECK.*");
    sel.append({}, {}, "Bsz[BGRQ-09]");
    sel.append({}, {}, "PCTc[BGRQ-09]");
    sel.append({}, {}, "Cc[BGRQ-09]");
    sel.append({}, {}, "Bszd[BGRQ-09]");
    sel.append({}, {}, "ZSTATE");
    return sel;
}

void AcquireRRNeph903::logValue(double startTime,
                                double endTime,
                                SequenceName::Component name,
                                Variant::Root &&value,
                                int streamID)
{
    SequenceValue
            dv(SequenceName({}, "raw", std::move(name)), std::move(value), startTime, endTime);
    if (loggingEgress && FP::defined(startTime)) {
        if (!realtimeEgress) {
            loggingMux.incoming(streamID, std::move(dv), loggingEgress);
            return;
        }
        loggingMux.incoming(streamID, dv, loggingEgress);
    }
    if (!realtimeEgress)
        return;
    dv.setStart(endTime);
    dv.setEnd(endTime + 1.0);
    realtimeEgress->incomingData(std::move(dv));
}

bool AcquireRRNeph903::logFillCalibrator(double startTime,
                                         double endTime,
                                         SequenceName::Component name,
                                         int streamID)
{
    if (!config.first().calibratorFill)
        return false;
    if (!loggingEgress)
        return false;
    if (!FP::defined(startTime))
        return false;
    SequenceValue dv
            (SequenceName({}, "raw", std::move(name)), Variant::Root(FP::undefined()), startTime,
             endTime);
    loggingMux.incoming(streamID, std::move(dv), loggingEgress);
    return true;
}

void AcquireRRNeph903::realtimeValue(double time,
                                     SequenceName::Component name,
                                     Variant::Root &&value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

void AcquireRRNeph903::setRelay(int relay, bool enabled)
{
    if (controlStream == NULL)
        return;
    switch (relay) {
    case 0:
        if (enabled)
            controlStream->writeControl("X");
        else
            controlStream->writeControl("W");
        break;
    case 1:
        if (enabled)
            controlStream->writeControl("Z");
        else
            controlStream->writeControl("Y");
        break;
    default:
        break;
    }
}

static double calculateZeroChange(double previous, double current)
{
    if (!FP::defined(previous) || !FP::defined(current))
        return FP::undefined();
    return current - previous;
}

static double darkSubtract(double total, double dark)
{
    if (!FP::defined(total) || !FP::defined(dark))
        return FP::undefined();
    return total - dark;
}

static double convertScattering3(double input)
{
    if (!FP::defined(input))
        return FP::undefined();
    return ((input - 2810.0) / 65536.0) * 1E-3 * 1E6;
}

static double convertScattering4(double input)
{
    if (!FP::defined(input))
        return FP::undefined();
    return ((input - 2810.0) / 65536.0) * 2E-4 * 1E6;
}

/* We just drop the subtraction if the zero isn't defined, since we set
 * a flag for that elsewhere */
static double zeroSubtraction(double Bs, double Bsz)
{
    if (!FP::defined(Bsz))
        return Bs;
    if (!FP::defined(Bs))
        return FP::undefined();
    return Bs - Bsz;
}


void AcquireRRNeph903::handleZeroUpdate(double currentTime)
{
    if ((!zeroPersistentUpdated || persistentEgress == NULL) &&
            (!zeroRealtimeUpdated || realtimeEgress == NULL))
        return;
    if (!FP::defined(zeroEffectiveTime))
        return;

    const auto &wlCode = Wavelength::code(config.first().wavelength);

    SequenceValue::Transfer result;

    if (Tz.read().exists()) {
        result.emplace_back(SequenceIdentity({{}, "raw", "Tz"}, zeroEffectiveTime, FP::undefined()),
                            Tz);
    }
    if (Pz.read().exists()) {
        result.emplace_back(SequenceIdentity({{}, "raw", "Pz"}, zeroEffectiveTime, FP::undefined()),
                            Pz);
    }
    result.emplace_back(
            SequenceIdentity({{}, "raw", "Bsz" + wlCode}, zeroEffectiveTime, FP::undefined()), Bsz);


    if (zeroPersistentUpdated && persistentEgress != NULL) {
        zeroPersistentUpdated = false;
        persistentEgress->incomingData(result);
    }

    if (!zeroRealtimeUpdated || realtimeEgress == NULL)
        return;
    zeroRealtimeUpdated = false;

    for (auto &det : result) {
        det.setStart(currentTime);
    }

    result.emplace_back(
            SequenceIdentity({{}, "raw", "Bszd" + wlCode}, currentTime, FP::undefined()), Bszd);

    realtimeEgress->incomingData(std::move(result));
}

void AcquireRRNeph903::zeroCompleted(double currentTime)
{
    zeroEffectiveTime = currentTime;
    zeroPersistentUpdated = true;
    zeroRealtimeUpdated = true;

    double previousBsz = Bsz.read().toDouble();

    if (zeroBsCount != 0)
        Bsz.write().setDouble(zeroBsSum / (double) zeroBsCount);
    else
        Bsz.write().setEmpty();
    if (zeroTCount != 0)
        Tz.write().setDouble(zeroTSum / (double) zeroTCount);
    else
        Tz.write().setEmpty();
    if (zeroPCount != 0)
        Pz.write().setDouble(zeroPSum / (double) zeroPCount);
    else
        Pz.write().setEmpty();

    Bszd.write().setDouble(calculateZeroChange(previousBsz, Bsz.read().toDouble()));

    zeroBsCount = 0;
    zeroBsSum = 0;
    zeroTCount = 0;
    zeroTSum = 0;
    zeroPCount = 0;
    zeroPSum = 0;
}

static int errorCodeOffset(int code, int offset)
{
    if (code == 0) return 0;
    if (code < 0) return code;
    return code + offset;
}

#define RETURN_ERROR_CODE(_code, _offset) do { \
    int code = (_code); \
    int offset = (_offset); \
    if (code < 0) return code; \
    if (code > 0) return code + offset; \
} while (0)

static bool isAllDigits(const Util::ByteView &check)
{
    for (auto c : check) {
        char v = c;
        if (v < '0' || v > '9') return false;
    }
    return true;
}

/* Technically this is 2.55 (seconds between flashes) * 255 (signal dwell
 * time in flashes) = 650.25 seconds */
static const double maximumCalibratorInterval = 651.0;

/* 2.55 (seconds between flashes) * 100 (calibrator dwell time in flashes) =
 * 255.0 seconds */
static const double maximumScatteringInterval = 256.0;

void AcquireRRNeph903::incomingScattering(double startTime,
                                          double endTime,
                                          Variant::Root &&Bs,
                                          const Variant::Read &T,
                                          const Variant::Read &P,
                                          Variant::Root &&Cs)
{
    const auto &wlCode = Wavelength::code(config.first().wavelength);

    switch (sampleState) {
    case SAMPLE_RUN:
        logValue(startTime, endTime, "Bs" + wlCode, Variant::Root(Bs), LogStream_Scattering);
        break;

    case SAMPLE_ZERO_MEASURE: {
        double add = Bs.read().toDouble();
        if (FP::defined(add)) {
            zeroBsCount++;
            zeroBsSum += add;
        }

        add = T.toDouble();
        if (FP::defined(add)) {
            zeroTCount++;
            zeroTSum += add;
        }

        add = P.toDouble();
        if (FP::defined(add)) {
            zeroPCount++;
            zeroPSum += add;
        }
        break;
    }

    case SAMPLE_ZERO_BEGINFLUSH:
    case SAMPLE_ZERO_ENDFLUSH:
        break;

    case SAMPLE_SPANCHECK:
        break;
    }

    spancheckController->updateTotal(NephelometerSpancheckController::Scattering,
                                     Bs.read().toDouble(), 0);

    /* Always output this for display purposes */
    realtimeValue(endTime, "ZBs" + wlCode, std::move(Bs));

    if (Cs.read().exists()) {
        spancheckController->updateTotal(NephelometerSpancheckController::MeasurementCounts,
                                         Cs.read().toDouble(), 0);

        logValue(startTime, endTime, "Cs" + wlCode, std::move(Cs), LogStream_Scattering);
    }

    lastScatteringTime = endTime;
}

void AcquireRRNeph903::incomingCalibrator(double startTime,
                                          double endTime,
                                          Variant::Root &&Bsf,
                                          Variant::Root &&Cf)
{
    const auto &wlCode = Wavelength::code(config.front().wavelength);

    logValue(startTime, endTime, "Bsf" + wlCode, std::move(Bsf), LogStream_Calibrator);

    if (Cf.read().exists()) {
        spancheckController->updateTotal(NephelometerSpancheckController::ReferenceCounts,
                                         Cf.read().toDouble(), 0);

        logValue(startTime, endTime, "Cf" + wlCode, std::move(Cf), LogStream_Calibrator);
    }

    lastCalibratorTime = endTime;
}

void AcquireRRNeph903::incomingConditions(double startTime,
                                          double endTime,
                                          Variant::Root &&T,
                                          Variant::Root &&P,
                                          Variant::Root &&U)
{
    spancheckController->update(NephelometerSpancheckController::Temperature, T.read().toDouble());
    spancheckController->update(NephelometerSpancheckController::Pressure, P.read().toDouble());

    logValue(startTime, endTime, "T", std::move(T), LogStream_Status);
    logValue(startTime, endTime, "U", std::move(U), LogStream_Status);
    logValue(startTime, endTime, "P", std::move(P), LogStream_Status);
}

void AcquireRRNeph903::recordUpdate(double startTime, double endTime)
{
    Q_ASSERT(FP::defined(endTime));

    if (loggingEgress == NULL) {
        loggingMux.clear();
        priorScattering = FP::undefined();
        priorCalibrator = FP::undefined();
        lastScatteringTime = FP::undefined();
        lastCalibratorTime = FP::undefined();
    } else if (FP::defined(startTime)) {
        if (!haveEmittedLogMeta) {
            haveEmittedLogMeta = true;
            loggingMux.incoming(LogStream_Metadata, buildLogMeta(startTime), loggingEgress);
        }

        loggingMux.advance(LogStream_Metadata, startTime, loggingEgress);
    }
    if (realtimeEgress != NULL) {
        if (!haveEmittedRealtimeMeta) {
            haveEmittedRealtimeMeta = true;

            SequenceValue::Transfer meta = buildLogMeta(endTime);
            Util::append(buildRealtimeMeta(endTime), meta);
            realtimeEgress->incomingData(std::move(meta));
        }
    }

    /* State advance */
    switch (sampleState) {
    case SAMPLE_RUN:
    case SAMPLE_SPANCHECK:
        break;

    case SAMPLE_ZERO_BEGINFLUSH:
        if (!FP::defined(sampleStateEndTime) || endTime >= sampleStateEndTime) {
            sampleState = SAMPLE_ZERO_MEASURE;
            sampleStateEndTime = zeroSampleTime->apply(endTime, endTime, true);
            realtimeStateUpdated = true;

            qCDebug(log) << "Initial zero air flush completed, sampling zero air until"
                         << Logging::time(sampleStateEndTime);
        }
        break;

    case SAMPLE_ZERO_MEASURE:
        if (!FP::defined(sampleStateEndTime) || endTime >= sampleStateEndTime) {
            sampleState = SAMPLE_ZERO_ENDFLUSH;
            sampleStateEndTime = zeroFlushTime->apply(endTime, endTime, true);
            realtimeStateUpdated = true;

            qCDebug(log) << "Zero measurement completed, flushing air until"
                         << Logging::time(sampleStateEndTime);

            zeroCompleted(endTime);
            setRelay(config.first().zeroRelay, false);
        }
        break;

    case SAMPLE_ZERO_ENDFLUSH:
        if (!FP::defined(sampleStateEndTime) || endTime >= sampleStateEndTime) {
            sampleState = SAMPLE_RUN;
            sampleStateEndTime = FP::undefined();
            realtimeStateUpdated = true;

            qCDebug(log) << "Zero completed, returning to normal operation";
        }
        break;
    }

    handleZeroUpdate(endTime);

    if (parametersPersistentUpdated && persistentEgress != NULL) {
        SequenceValue dv = instrumentParameters;
        dv.setStart(startTime);
        dv.setEnd(FP::undefined());
        parametersPersistentUpdated = false;
        persistentEgress->incomingData(std::move(dv));
    }
    if (parametersRealtimeUpdated && realtimeEgress != NULL) {
        SequenceValue dv = instrumentParameters;
        dv.setStart(endTime);
        dv.setEnd(FP::undefined());
        parametersRealtimeUpdated = false;
        realtimeEgress->incomingData(std::move(dv));
    }

    Variant::Flags flags;

    switch (sampleState) {
    case SAMPLE_RUN:
        break;

    case SAMPLE_ZERO_BEGINFLUSH:
        flags.insert("Blank");
        break;

    case SAMPLE_ZERO_MEASURE:
        flags.insert("Zero");
        break;

    case SAMPLE_ZERO_ENDFLUSH:
        flags.insert("Blank");
        break;

    case SAMPLE_SPANCHECK:
        flags.insert("Spancheck");
        break;
    }

    if (!FP::defined(Bsz.read().toDouble()))
        flags.insert("StaticZeroSubtraction");

    logValue(startTime, endTime, "F1", Variant::Root(flags), LogStream_State);

    if (realtimeStateUpdated &&
            realtimeEgress != NULL &&
            (responseState == RESP_PASSIVE_RUN || responseState == RESP_UNPOLLED_RUN)) {
        realtimeStateUpdated = false;

        Variant::Root state;
        switch (sampleState) {
        case SAMPLE_RUN:
            state.write().setString("Run");
            break;
        case SAMPLE_ZERO_ENDFLUSH:
        case SAMPLE_ZERO_BEGINFLUSH:
            state.write().setString("Blank");
            break;
        case SAMPLE_ZERO_MEASURE:
            state.write().setString("Zero");
            break;
        case SAMPLE_SPANCHECK:
            state.write().setString("Spancheck");
            break;
        }
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZSTATE"}, std::move(state), endTime, FP::undefined()));
    }
}

int AcquireRRNeph903::processBasic(std::deque<Util::ByteView> fields,
                                   double frameTime,
                                   bool includeRelays)
{
    if (!FP::defined(frameTime))
        return 99;

    bool ok = false;
    double value = 0;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    if (fields.empty()) return 1;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode) {
        if (field.indexOf('e') == field.npos)
            return 2;
    }
    value = field.parse_real(&ok);
    if (!ok) return 3;
    if (!FP::defined(value)) return 4;
    Variant::Root Bs(value * 1E6);
    remap("ZBsRaw" + wlCode, Bs);
    Bs.write().setDouble(zeroSubtraction(Bs.read().toDouble(), Bsz.read().toDouble()));
    remap("Bs" + wlCode, Bs);

    if (fields.empty()) return 5;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode) {
        if (field.indexOf('e') == field.npos)
            return 6;
    }
    value = field.parse_real(&ok);
    if (!ok) return 7;
    if (!FP::defined(value)) return 8;
    Variant::Root Bsf(value * 1E6);
    remap("Bsf" + wlCode, Bsf);

    if (fields.empty()) return 9;
    field = fields.front().string_trimmed();
    fields.pop_front();
    value = field.parse_real(&ok);
    if (!ok) return 10;
    if (!FP::defined(value)) return 11;
    if (value < 10.0 || value > 1200.0) return 12;
    Variant::Root P(value);
    remap("P", P);

    if (fields.empty()) return 13;
    field = fields.front().string_trimmed();
    fields.pop_front();
    value = field.parse_real(&ok);
    if (!ok) return 14;
    if (!FP::defined(value)) return 15;
    if (value < 100.0 || value > 400.0) return 16;
    Variant::Root T(value - 273.15);
    remap("T", T);

    if (fields.empty()) return 17;
    field = fields.front().string_trimmed();
    fields.pop_front();
    value = field.parse_real(&ok);
    if (!ok) return 18;
    if (!FP::defined(value)) return 19;
    if (value < 0.0 || value > 100.0) return 20;
    Variant::Root U(value);
    remap("U", U);

    if (includeRelays) {
        /* Relay status unused */
        if (fields.empty()) return 21;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field.size() != 2) return 22;
        }
    }

    if (config.first().strictMode) {
        if (!fields.empty())
            return 98;
    }

    /* If we get here we don't have the actual calibrator state, so make
     * a best guess for when it's engaged by looking for values that are exactly
     * the same.  The noise should make the scattering change regardless
     * of the signal. */
    bool scatteringValid = true;
    bool calibratorValid = true;
    //bool calibratorEngaged = false;
    {
        double scat = Bs.read().toDouble();
        double cal = Bsf.read().toDouble();
        if (FP::defined(scat) &&
                FP::defined(cal) &&
                FP::defined(priorScattering) &&
                FP::defined(priorCalibrator)) {
            if (cal == priorCalibrator && scat != priorScattering) {
                calibratorValid = false;
            } else if (scat == priorScattering) {
                scatteringValid = false;
                //calibratorEngaged = true;
            }
        }
        priorScattering = scat;
        priorCalibrator = cal;
    }

    /* If we don't have a time or if it's been too long, output values even
     * if the above logic says they're not valid */
    if (!FP::defined(lastCalibratorTime) ||
            frameTime - lastCalibratorTime > maximumCalibratorInterval) {
        calibratorValid = true;
    }
    if (!FP::defined(lastScatteringTime) ||
            frameTime - lastScatteringTime > maximumScatteringInterval) {
        scatteringValid = true;
    }

    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;

    recordUpdate(startTime, endTime);
    if (scatteringValid) {
        incomingScattering(startTime, endTime, std::move(Bs), T, P);
    } else {
        logFillCalibrator(startTime, endTime, "Bs" + wlCode, LogStream_Scattering);
    }
    incomingConditions(startTime, endTime, std::move(T), std::move(P), std::move(U));
    if (calibratorValid) {
        incomingCalibrator(startTime, endTime, std::move(Bsf));
    } else {
        logFillCalibrator(startTime, endTime, "Bsf" + wlCode, LogStream_Calibrator);
    }

    return 0;
}

int AcquireRRNeph903::processExtended(std::deque<Util::ByteView> fields,
                                      double frameTime,
                                      char expectedID,
                                      Variant::Root &&Cs,
                                      Variant::Root &&Cd,
                                      Variant::Root &&Cf)
{
    if (!FP::defined(frameTime))
        return 99;

    bool ok = false;
    double value = 0;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    if (fields.empty()) return 1;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode) {
        if (field.indexOf('e') == field.npos) return 2;
    }
    value = field.parse_real(&ok);
    if (!ok) return 3;
    if (!FP::defined(value)) return 4;
    Variant::Root Bs(value * 1E6);
    remap("ZBsRaw" + wlCode, Bs);
    Bs.write().setDouble(zeroSubtraction(Bs.read().toDouble(), Bsz.read().toDouble()));
    remap("Bs" + wlCode, Bs);

    if (fields.empty()) return 5;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode) {
        if (field.indexOf('e') == field.npos) return 6;
    }
    value = field.parse_real(&ok);
    if (!ok) return 7;
    if (!FP::defined(value)) return 8;
    Variant::Root Bsf(value * 1E6);
    remap("Bsf" + wlCode, Bsf);

    if (fields.empty()) return 9;
    field = fields.front().string_trimmed();
    fields.pop_front();
    value = field.parse_real(&ok);
    if (!ok) return 10;
    if (!FP::defined(value)) return 11;
    if (value < 10.0 || value > 1200.0) return 12;
    Variant::Root P(value);
    remap("P", P);

    if (fields.empty()) return 13;
    field = fields.front().string_trimmed();
    fields.pop_front();
    value = field.parse_real(&ok);
    if (!ok) return 14;
    if (!FP::defined(value)) return 15;
    if (value < 100.0 || value > 400.0) return 16;
    Variant::Root T(value - 273.15);
    remap("T", T);

    if (fields.empty()) return 17;
    field = fields.front().string_trimmed();
    fields.pop_front();
    value = field.parse_real(&ok);
    if (!ok) return 18;
    if (!FP::defined(value)) return 19;
    if (value < 0.0 || value > 100.0) return 20;
    Variant::Root U(value);
    remap("U", U);

    /* Relay status unused */
    if (fields.empty()) return 21;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode) {
        if (field.size() != 2) return 22;
    }

    if (fields.empty()) return 23;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.empty()) return 24;
    if (config.first().strictMode) {
        if (field.size() != 1) return 25;
        if (!isAllDigits(field)) return 26;
    }
    Variant::Root vcalibratorEngaged;
    vcalibratorEngaged.write().setBool(field[0] != '0');
    remap("CALIBRATORENGAGED", vcalibratorEngaged);

    if (fields.empty()) return 27;
    field = fields.front().string_trimmed();
    fields.pop_front();
    value = field.parse_real(&ok);
    if (!ok) return 28;
    if (!FP::defined(value)) return 29;
    if (value < 50.0 || value > 1500.0) return 30;
    Variant::Root V(value);
    remap("V", V);

    if (fields.empty()) return 27;
    field = fields.front().string_trimmed();
    fields.pop_front();
    value = field.parse_real(&ok);
    if (!ok) return 28;
    if (!FP::defined(value)) return 29;
    if (value < 0.0 || value > 65536.0) return 30;
    Variant::Root Cp(value);
    remap("ZCpRaw", Cp);

    if (fields.empty()) return 31;
    field = fields.front().string_trimmed();
    fields.pop_front();
    value = field.parse_real(&ok);
    if (!ok) return 32;
    if (!FP::defined(value)) return 33;
    if (value < 0.0 || value > 65536.0) return 34;
    Variant::Root Cl(value);
    remap("Cl", Cl);

    if (fields.empty()) return 35;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode) {
        if (field.size() != 1) return 36;
        if (field[0] != expectedID) return 37;
    }

    if (config.first().strictMode) {
        if (!fields.empty())
            return 98;
    }

    bool calibratorEngaged = vcalibratorEngaged.read().toBool();
    bool scatteringValid = !calibratorEngaged;
    bool calibratorValid = calibratorEngaged;
    /* If we don't have a time or if it's been too long, output values even
     * if the above logic says they're not valid */
    if (!FP::defined(lastCalibratorTime) ||
            frameTime - lastCalibratorTime > maximumCalibratorInterval) {
        calibratorValid = true;
    }
    if (!FP::defined(lastScatteringTime) ||
            frameTime - lastScatteringTime > maximumScatteringInterval) {
        scatteringValid = true;
    }

    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;

    if (instrumentParameters.read().hash("V") != V) {
        instrumentParameters.write().hash("V").set(V);
        parametersPersistentUpdated = true;
        parametersRealtimeUpdated = true;
    }

    recordUpdate(startTime, endTime);
    if (!calibratorEngaged) {
        if (scatteringValid) {
            remap("Cp" + wlCode, Cp);
            logValue(startTime, endTime, "Cp" + wlCode, Variant::Root(Cp), LogStream_Scattering);
        } else {
            logFillCalibrator(startTime, endTime, "Cp" + wlCode, LogStream_Scattering);
        }
        logFillCalibrator(startTime, endTime, "Cpf" + wlCode, LogStream_Calibrator);
    } else {
        if (calibratorValid) {
            remap("Cpf" + wlCode, Cp);
            logValue(startTime, endTime, "Cpf" + wlCode, Variant::Root(Cp), LogStream_Calibrator);
        } else {
            logFillCalibrator(startTime, endTime, "Cpf" + wlCode, LogStream_Calibrator);
        }
        logFillCalibrator(startTime, endTime, "Cp" + wlCode, LogStream_Scattering);
    }
    if (scatteringValid) {
        incomingScattering(startTime, endTime, std::move(Bs), T, P, std::move(Cs));
    } else {
        logFillCalibrator(startTime, endTime, "Bs" + wlCode, LogStream_Scattering);
        if (Cs.read().exists()) {
            logFillCalibrator(startTime, endTime, "Cs" + wlCode, LogStream_Scattering);
        }
    }
    incomingConditions(startTime, endTime, std::move(T), std::move(P), std::move(U));
    if (calibratorValid) {
        incomingCalibrator(startTime, endTime, std::move(Bsf), std::move(Cf));
    } else {
        logFillCalibrator(startTime, endTime, "Bsf" + wlCode, LogStream_Calibrator);
        if (Cf.read().exists()) {
            logFillCalibrator(startTime, endTime, "Cf" + wlCode, LogStream_Calibrator);
        }
    }

    if (Cd.read().exists()) {
        spancheckController->update(NephelometerSpancheckController::DarkCounts,
                                    Cd.read().toDouble());

        logValue(startTime, endTime, "Cd", std::move(Cd), LogStream_Status);
    }

    realtimeValue(endTime, "V", std::move(V));
    realtimeValue(endTime, "ZCpRaw", std::move(Cp));
    realtimeValue(endTime, "Cl", std::move(Cl));

    return 0;
}

int AcquireRRNeph903::processADCBase(std::deque<Util::ByteView> fields,
                                     double frameTime,
                                     char expectedID,
                                     Variant::Root &&Bs,
                                     Variant::Root &&Bsf,
                                     Variant::Root &&Cs,
                                     Variant::Root &&Cd,
                                     Variant::Root &&Cf)
{

    if (!FP::defined(frameTime))
        return 99;

    bool ok = false;
    double value = 0;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    remap("ZBsRaw" + wlCode, Bs);
    Bs.write().setDouble(zeroSubtraction(Bs.read().toDouble(), Bsz.read().toDouble()));
    remap("Bs" + wlCode, Bs);

    remap("Bsf" + wlCode, Bsf);

    if (fields.empty()) return 1;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.empty()) return 2;
    if (config.first().strictMode) {
        if (field.size() != 1) return 3;
        if (!isAllDigits(field)) return 4;
    }
    Variant::Root vcalibratorEngaged;
    vcalibratorEngaged.write().setBool(field[0] != '0');
    remap("CALIBRATORENGAGED", vcalibratorEngaged);

    if (fields.empty()) return 5;
    field = fields.front().string_trimmed();
    fields.pop_front();
    value = field.parse_real(&ok);
    if (!ok) return 6;
    if (!FP::defined(value)) return 7;
    if (value < 50.0 || value > 1500.0) return 30;
    Variant::Root V(value);
    remap("V", V);

    if (fields.empty()) return 8;
    field = fields.front().string_trimmed();
    fields.pop_front();
    value = field.parse_real(&ok);
    if (!ok) return 9;
    if (!FP::defined(value)) return 29;
    if (value < 0.0 || value > 65536.0) return 30;
    Variant::Root Cp(value);
    remap("ZCpRaw", Cp);

    if (fields.empty()) return 10;
    field = fields.front().string_trimmed();
    fields.pop_front();
    value = field.parse_real(&ok);
    if (!ok) return 11;
    if (!FP::defined(value)) return 12;
    if (value < 0.0 || value > 65536.0) return 13;
    Variant::Root Cl(value);
    remap("Cl", Cl);

    if (fields.empty()) return 14;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode) {
        if (field.size() != 1) return 15;
        if (field[0] != expectedID) return 16;
    }

    if (config.first().strictMode) {
        if (!fields.empty())
            return 98;
    }

    bool calibratorEngaged = vcalibratorEngaged.read().toBool();
    bool scatteringValid = !calibratorEngaged;
    bool calibratorValid = calibratorEngaged;
    /* If we don't have a time or if it's been too long, output values even
     * if the above logic says they're not valid */
    if (!FP::defined(lastCalibratorTime) ||
            frameTime - lastCalibratorTime > maximumCalibratorInterval) {
        calibratorValid = true;
    }
    if (!FP::defined(lastScatteringTime) ||
            frameTime - lastScatteringTime > maximumScatteringInterval) {
        scatteringValid = true;
    }

    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;

    Variant::Root T;
    remap("T", T);
    Variant::Root P;
    remap("P", P);
    Variant::Root U;
    remap("U", U);

    if (instrumentParameters.read().hash("V") != V) {
        instrumentParameters.write().hash("V").set(V);
        parametersPersistentUpdated = true;
        parametersRealtimeUpdated = true;
    }

    recordUpdate(startTime, endTime);
    if (!calibratorEngaged) {
        if (scatteringValid) {
            remap("Cp" + wlCode, Cp);
            logValue(startTime, endTime, "Cp" + wlCode, Variant::Root(Cp), LogStream_Scattering);
        } else {
            logFillCalibrator(startTime, endTime, "Cp" + wlCode, LogStream_Scattering);
        }
        logFillCalibrator(startTime, endTime, "Cpf" + wlCode, LogStream_Calibrator);
    } else {
        if (calibratorValid) {
            remap("Cpf" + wlCode, Cp);
            logValue(startTime, endTime, "Cpf" + wlCode, Variant::Root(Cp), LogStream_Calibrator);
        } else {
            logFillCalibrator(startTime, endTime, "Cpf" + wlCode, LogStream_Calibrator);
        }
        logFillCalibrator(startTime, endTime, "Cp" + wlCode, LogStream_Scattering);
    }
    if (scatteringValid) {
        if (Bs.read().exists()) {
            incomingScattering(startTime, endTime, std::move(Bs), T, P, std::move(Cs));
        } else if (!logFillCalibrator(startTime, endTime, "Bs" + wlCode, LogStream_Scattering)) {
            loggingMux.advance(LogStream_Scattering, startTime, loggingEgress);
        } else {
            if (Cs.read().exists()) {
                logFillCalibrator(startTime, endTime, "Cs" + wlCode, LogStream_Scattering);
            }
        }
    }
    if (T.read().exists() || P.read().exists() || U.read().exists()) {
        incomingConditions(startTime, endTime, std::move(T), std::move(P), std::move(U));
    } else {
        loggingMux.advance(LogStream_Status, startTime, loggingEgress);
    }
    if (calibratorValid) {
        if (Bsf.read().exists()) {
            incomingCalibrator(startTime, endTime, std::move(Bsf), std::move(Cf));
        } else if (!logFillCalibrator(startTime, endTime, "Bsf" + wlCode, LogStream_Calibrator)) {
            loggingMux.advance(LogStream_Calibrator, startTime, loggingEgress);
        } else {
            if (Cf.read().exists()) {
                logFillCalibrator(startTime, endTime, "Cf" + wlCode, LogStream_Calibrator);
            }
        }
    }

    if (Cd.read().exists()) {
        spancheckController->update(NephelometerSpancheckController::DarkCounts,
                                    Cd.read().toDouble());

        logValue(startTime, endTime, "Cd", std::move(Cd), LogStream_Status);
    }

    realtimeValue(endTime, "V", std::move(V));
    realtimeValue(endTime, "ZCpRaw", std::move(Cp));
    realtimeValue(endTime, "Cl", std::move(Cl));

    return 0;
}

int AcquireRRNeph903::processShort(std::deque<Util::ByteView> fields, double &frameTime)
{
    bool ok = false;

    if (fields.empty()) return 1;
    /* Record number ignored */
    fields.pop_front();

    if (fields.empty()) return 2;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 iyear = field.parse_i32(&ok);
    if (!ok) return 3;
    if (iyear < 0) return 4;
    if (iyear > 99) {
        if (iyear < 1990) return 5;
        if (iyear > 2999) return 6;
    } else if (iyear < 90) {
        /* I don't think any instruments existed before 91, so just
         * assume that these are all in the 2000s (also assume that we're
         * not still using this by 2090) */
        iyear += 2000;
    } else {
        iyear += 1900;
    }
    Variant::Root year(iyear);
    remap("YEAR", year);
    iyear = year.read().toInt64();

    if (fields.empty()) return 7;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 imonth = field.parse_i32(&ok);
    if (!ok) return 8;
    if (imonth < 1) return 9;
    if (imonth > 12) return 10;
    Variant::Root month(imonth);
    remap("MONTH", month);
    imonth = month.read().toInt64();

    if (fields.empty()) return 11;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 iday = field.parse_i32(&ok);
    if (!ok) return 12;
    if (iday < 1) return 13;
    if (iday > 31) return 14;
    Variant::Root day(iday);
    remap("DAY", day);
    iday = day.read().toInt64();

    if (fields.empty()) return 15;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 ihour = field.parse_i32(&ok);
    if (!ok) return 16;
    if (ihour < 0) return 17;
    if (ihour > 23) return 18;
    Variant::Root hour(ihour);
    remap("HOUR", hour);
    ihour = hour.read().toInt64();

    if (fields.empty()) return 19;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 iminute = field.parse_i32(&ok);
    if (!ok) return 20;
    if (iminute < 0) return 21;
    if (iminute > 59) return 22;
    Variant::Root minute(iminute);
    remap("MINUTE", minute);
    iminute = minute.read().toInt64();

    if (fields.empty()) return 23;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 isecond = field.parse_i32(&ok);
    if (!ok) return 24;
    if (isecond < 0) return 25;
    if (isecond > 60) return 26;
    Variant::Root second(isecond);
    remap("SECOND", second);
    isecond = second.read().toInt64();

    if (!FP::defined(frameTime) && INTEGER::defined(iyear) && iyear >= 1900 &&
            iyear <= 2999 &&
            INTEGER::defined(imonth) &&
            imonth >= 1 &&
            imonth <= 12 &&
            INTEGER::defined(iday) &&
            iday >= 1 &&
            iday <= 31 &&
            INTEGER::defined(ihour) &&
            ihour >= 0 &&
            ihour <= 23 &&
            INTEGER::defined(iminute) &&
            iminute >= 0 &&
            iminute <= 59 &&
            INTEGER::defined(isecond) &&
            isecond >= 0 &&
            isecond <= 60) {
        frameTime = Time::fromDateTime(QDateTime(QDate((int) iyear, (int) imonth, (int) iday),
                                                 QTime((int) ihour, (int) iminute, (int) isecond),
                                                 Qt::UTC));

        configAdvance(frameTime);
    }
    if (FP::defined(lastRecordTime) && FP::defined(frameTime) && frameTime < lastRecordTime)
        return -1;
    if (!FP::defined(frameTime))
        return 99;

    return errorCodeOffset(processBasic(fields, frameTime, false), 100);
}

int AcquireRRNeph903::processVerbose(std::deque<Util::ByteView> fields, double &frameTime)
{
    bool ok = false;
    double value = 0;

    if (fields.empty()) return 1;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode) {
        if (field != "c") return 2;
    }

    /* Record number, unused */
    if (fields.empty()) return 3;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode) {
        if (field.size() != 4) return 4;
        if (!isAllDigits(field)) return 5;
    }

    /* Type identifier (or possibly number of fields), unused */
    if (fields.empty()) return 6;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode) {
        if (field.size() != 1) return 7;
        if (!isAllDigits(field)) return 8;
    }

    if (fields.size() == 3) {
        Q_ASSERT(!fields.empty());
        field = fields.front().string_trimmed();
        fields.pop_front();

        if (field.size() != 10) return 101;
        qint64 iyear = field.mid(0, 2).parse_i32(&ok);
        if (!ok) return 102;
        if (iyear < 0) return 103;
        if (iyear < 90) {
            /* I don't think any instruments existed before 91, so just
             * assume that these are all in the 2000s (also assume that we're
             * not still using this by 2090) */
            iyear += 2000;
        } else {
            iyear += 1900;
        }
        Variant::Root year(iyear);
        remap("YEAR", year);
        iyear = year.read().toInt64();

        qint64 imonth = field.mid(2, 2).parse_i32(&ok);
        if (!ok) return 104;
        if (imonth < 1) return 105;
        if (imonth > 12) return 106;
        Variant::Root month(imonth);
        remap("MONTH", month);
        imonth = month.read().toInt64();

        qint64 iday = field.mid(4, 2).parse_i32(&ok);
        if (!ok) return 107;
        if (iday < 1) return 108;
        if (iday > 31) return 109;
        Variant::Root day(iday);
        remap("DAY", day);
        iday = day.read().toInt64();

        qint64 ihour = field.mid(6, 2).parse_i32(&ok);
        if (!ok) return 110;
        if (ihour < 0) return 111;
        if (ihour > 23) return 112;
        Variant::Root hour(ihour);
        remap("HOUR", hour);
        ihour = hour.read().toInt64();

        qint64 iminute = field.mid(8, 2).parse_i32(&ok);
        if (!ok) return 21;
        if (iminute < 0) return 113;
        if (iminute > 59) return 114;
        Variant::Root minute(iminute);
        remap("MINUTE", minute);
        iminute = minute.read().toInt64();

        /* Reference brightness (?), unused */
        Q_ASSERT(!fields.empty());
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (!isAllDigits(field)) return 115;
        }

        /* Air rayleigh scattering (except the numbers aren't what they should
         * be), unused */
        Q_ASSERT(!fields.empty());
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field.indexOf('e') == field.npos) return 116;
        }

        Q_ASSERT(fields.empty());

        if (!FP::defined(frameTime) &&
                INTEGER::defined(iyear) &&
                iyear >= 1900 &&
                iyear <= 2999 &&
                INTEGER::defined(imonth) &&
                imonth >= 1 &&
                imonth <= 12 &&
                INTEGER::defined(iday) &&
                iday >= 1 &&
                iday <= 31 &&
                INTEGER::defined(ihour) &&
                ihour >= 0 &&
                ihour <= 23 &&
                INTEGER::defined(iminute) &&
                iminute >= 0 &&
                iminute <= 59) {
            frameTime = Time::fromDateTime(QDateTime(QDate((int) iyear, (int) imonth, (int) iday),
                                                     QTime((int) ihour, (int) iminute, 0),
                                                     Qt::UTC));

            configAdvance(frameTime);
        }
        if (!FP::defined(frameTime))
            return -1;
        loggingBaseTime = frameTime;

        if (FP::defined(lastRecordTime) && FP::defined(frameTime) && frameTime < lastRecordTime)
            return -1;

        lastRecordTime = frameTime;

        /* Don't count towards autoprobing */
        return -1;
    }

    if (fields.size() == 4) {
        /* I don't know what any of these numbers are, but just try to verify
         * the record anyway */

        Q_ASSERT(!fields.empty());
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field.indexOf('e') == field.npos) return 201;
        }

        Q_ASSERT(!fields.empty());
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field.indexOf('e') == field.npos) return 202;
        }

        Q_ASSERT(!fields.empty());
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (field.indexOf('e') == field.npos) return 203;
        }

        Q_ASSERT(!fields.empty());
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (config.first().strictMode) {
            if (!isAllDigits(field)) return 204;
        }

        /* Don't count towards autoprobing */
        return -1;
    }

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    if (fields.empty()) return 301;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 iminutesAfter = field.parse_i32(&ok);
    if (!ok) return 302;
    if (iminutesAfter < 0) return 303;
    if (iminutesAfter > 59) return 304;
    Variant::Root minutesAfter(iminutesAfter);
    remap("MINUTESAFTER", minutesAfter);
    iminutesAfter = minutesAfter.read().toInt64();

    /* Raw scattering as a fraction of the calibrator, unused */
    if (fields.empty()) return 305;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode) {
        if (field.indexOf('e') == field.npos) return 307;
    }

    /* Raw calibrator average, unused */
    if (fields.empty()) return 308;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode) {
        if (field.indexOf('e') == field.npos) return 309;
    }

    if (fields.empty()) return 308;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode) {
        if (field.indexOf('e') == field.npos)
            return 309;
    }
    value = field.parse_real(&ok);
    if (!ok) return 310;
    if (!FP::defined(value)) return 311;
    Variant::Root Bs(value * 1E6);
    remap("ZBsRaw" + wlCode, Bs);
    Bs.write().setDouble(zeroSubtraction(Bs.read().toDouble(), Bsz.read().toDouble()));
    remap("Bs" + wlCode, Bs);

    if (fields.empty()) return 312;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (config.first().strictMode) {
        if (field.indexOf('e') == field.npos)
            return 6;
    }
    value = field.parse_real(&ok);
    if (!ok) return 313;
    if (!FP::defined(value)) return 314;
    Variant::Root Bsf(value * 1E6);
    remap("Bsf" + wlCode, Bsf);

    if (fields.empty()) return 315;
    field = fields.front().string_trimmed();
    fields.pop_front();
    value = field.parse_real(&ok);
    if (!ok) return 316;
    if (!FP::defined(value)) return 317;
    if (value < 10.0 || value > 1200.0) return 318;
    Variant::Root P(value);
    remap("P", P);

    if (fields.empty()) return 319;
    field = fields.front().string_trimmed();
    fields.pop_front();
    value = field.parse_real(&ok);
    if (!ok) return 320;
    if (!FP::defined(value)) return 321;
    if (value < 100.0 || value > 400.0) return 322;
    Variant::Root T(value - 273.15);
    remap("T", T);

    if (fields.empty()) return 323;
    field = fields.front().string_trimmed();
    fields.pop_front();
    value = field.parse_real(&ok);
    if (!ok) return 324;
    if (!FP::defined(value)) return 325;
    if (value < 0.0 || value > 100.0) return 326;
    Variant::Root U(value);
    remap("U", U);

    if (config.first().strictMode) {
        if (!fields.empty())
            return 98;
    }

    if (!FP::defined(frameTime) &&
            FP::defined(loggingBaseTime) &&
            INTEGER::defined(iminutesAfter)) {
        frameTime = loggingBaseTime + (double) (iminutesAfter * 60);
        configAdvance(frameTime);
    }
    if (!FP::defined(frameTime))
        return -1;
    if (FP::defined(lastRecordTime) && FP::defined(frameTime) && frameTime < lastRecordTime)
        return -1;

    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;

    recordUpdate(startTime, endTime);
    incomingScattering(startTime, endTime, std::move(Bs), T, P);
    incomingConditions(startTime, endTime, std::move(T), std::move(P), std::move(U));
    incomingCalibrator(startTime, endTime, std::move(Bsf));

    return 0;
}

int AcquireRRNeph903::processSettingsPairs(std::deque<Util::ByteView> &fields)
{
    const auto &wlCode = Wavelength::code(config.front().wavelength);

    bool ok = false;
    double value = 0;

    /* Rayleigh scattering */
    if (fields.empty()) return 1;
    auto field = fields.back().string_trimmed();
    fields.pop_back();
    if (config.first().strictMode) {
        if (field.indexOf('e') == field.npos)
            return 2;
    }
    value = field.parse_real(&ok);
    if (!ok) return 3;
    if (!FP::defined(value)) return 4;
    if (value < 0.0 || value > 0.5) return 5;
    Variant::Root Bsr(value * 1E6);
    remap("Bsr" + wlCode, Bsr);
    bool updated = instrumentParameters.read().hash("Bsr" + wlCode) != Bsr.read();

    /* Unknown, presumably Rayleigh scattering integer */
    if (fields.empty()) return 6;
    field = fields.back();
    fields.pop_back();
    if (config.first().strictMode) {
        if (!isAllDigits(field)) return 7;
    }

    /* Zero offset ratio */
    if (fields.empty()) return 8;
    field = fields.back().string_trimmed();
    fields.pop_back();
    if (config.first().strictMode) {
        if (field.indexOf('e') == field.npos)
            return 9;
    }
    value = field.parse_real(&ok);
    if (!ok) return 10;
    if (!FP::defined(value)) return 11;
    Variant::Root ZBsZeroRatio(value);
    remap("ZBsZeroRatio" + wlCode, ZBsZeroRatio);
    updated = updated ||
            instrumentParameters.read().hash("ZBsZeroRatio" + wlCode) != ZBsZeroRatio.read();

    /* Unknown, presumably zero offset integer */
    if (fields.empty()) return 12;
    field = fields.back();
    fields.pop_back();
    if (config.first().strictMode) {
        if (!isAllDigits(field)) return 13;
    }

    /* Span gas ratio */
    if (fields.empty()) return 114;
    field = fields.back().string_trimmed();
    fields.pop_back();
    if (config.first().strictMode) {
        if (field.indexOf('e') == field.npos)
            return 15;
    }
    value = field.parse_real(&ok);
    if (!ok) return 16;
    if (!FP::defined(value)) return 17;
    if (value < 0.0) return 18;
    Variant::Root ZBsSpanRatio(value);
    remap("ZBsSpanRatio" + wlCode, ZBsSpanRatio);
    updated = updated ||
            instrumentParameters.read().hash("ZBsSpanRatio" + wlCode) != ZBsSpanRatio.read();

    /* Unknown, presumably span gas ratio integer */
    if (fields.empty()) return 19;
    field = fields.back();
    fields.pop_back();
    if (config.first().strictMode) {
        if (!isAllDigits(field)) return 20;
    }

    if (updated) {
        instrumentParameters.write().hash("Bsr" + wlCode).set(Bsr);
        instrumentParameters.write().hash("ZBsZeroRatio" + wlCode).set(ZBsZeroRatio);
        instrumentParameters.write().hash("ZBsSpanRatio" + wlCode).set(ZBsSpanRatio);

        parametersPersistentUpdated = true;
        parametersRealtimeUpdated = true;
    }

    return 0;
}

int AcquireRRNeph903::processTail1(std::deque<Util::ByteView> &fields,
                                   Variant::Root &Bs,
                                   Variant::Root &Cs,
                                   Variant::Root &Cd)
{
    bool ok = false;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    /* Unknown */
    if (fields.empty()) return 1;
    auto field = fields.back();
    fields.pop_back();
    if (config.first().strictMode) {
        if (!isAllDigits(field)) return 2;
    }

    /* Scattering integer in 2E-4 m-1, no dark subtraction, unused */
    if (fields.empty()) return 3;
    field = fields.back();
    fields.pop_back();
    if (config.first().strictMode) {
        if (!isAllDigits(field)) return 4;
    }
    Cs.write().setDouble(field.parse_real(&ok));
    if (!ok) return 5;
    if (!FP::defined(Cs.read().toDouble())) return 6;
    if (Cs.read().toDouble() < 0.0) return 7;
    if (Cs.read().toDouble() >= 65535.0) Cs.write().setDouble(FP::undefined());
    remap("Cs" + wlCode, Cs);

    /* Unknown, related to calibrator (possibly scattering) */
    if (fields.empty()) return 8;
    field = fields.back();
    fields.pop_back();
    if (config.first().strictMode) {
        if (!isAllDigits(field)) return 9;
    }

    /* Unknown, might be scattering in 1E-3 m-1, except it's slightly off
     * (wrong dark current?) and updates during calibrator */
    if (fields.empty()) return 10;
    field = fields.back();
    fields.pop_back();
    if (config.first().strictMode) {
        if (!isAllDigits(field)) return 11;
    }

    /* Scattering dark counts */
    if (fields.empty()) return 12;
    field = fields.back();
    fields.pop_back();
    if (config.first().strictMode) {
        if (!isAllDigits(field)) return 13;
    }
    Cd.write().setDouble(field.parse_real(&ok));
    if (!ok) return 14;
    if (!FP::defined(Cd.read().toDouble())) return 15;
    if (Cd.read().toDouble() < 0.0) return 16;
    if (Cd.read().toDouble() >= 65535.0) Cs.write().setDouble(FP::undefined());
    remap("Cd", Cd);

    /* This matches exactly with reported values, so always use it */
    Bs.write()
      .setDouble(convertScattering4(darkSubtract(Cs.read().toDouble(), Cd.read().toDouble())));

    /* Unknown, I'd expect this to be scattering 1E-3 dark counts since it's
     * around the same magnitude, but the values don't match up.  The notes
     * suggest that this may be the lamp dark current. */
    if (fields.empty()) return 17;
    field = fields.back();
    fields.pop_back();
    if (config.first().strictMode) {
        if (!isAllDigits(field)) return 18;
    }

    return 0;
}

int AcquireRRNeph903::processTail2(std::deque<Util::ByteView> &fields,
                                   Variant::Root &Bs,
                                   Variant::Root &Bsf,
                                   Variant::Root &Cs,
                                   Variant::Root &Cd,
                                   Variant::Root &Cf)
{
    bool ok = false;
    double value = 0;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    /* These look like scatterings, but they're slightly off what's displayed.
     * Maybe they need a dark count subtracted?  Except they don't match
     * the ones that are scatterings even when we have the dark current.  The
     * notes I have said they should be dark subtracted already, so when we
     * don't have another option, try that anyway. */

    /* Scattering in 2E-4 m-1 units */
    if (fields.empty()) return 1;
    auto field = fields.back();
    fields.pop_back();
    if (config.first().strictMode) {
        if (!isAllDigits(field)) return 2;
    }
    value = field.parse_real(&ok);
    if (!ok) return 3;
    if (!FP::defined(value)) return 4;
    if (value < 0.0) return 5;
    bool overflow = (value >= 65535.0);
    if (!overflow) {
        if (!FP::defined(Cs.read().toReal())) {
            Cs.write().setDouble(value);
            remap("Cs" + wlCode, Cs);

            Cd.write().setDouble(0.0);
            remap("Cd", Cd);

            if (!FP::defined(Bs.read().toReal())) {
                Bs.write().setDouble(convertScattering4(Cs.write().toDouble()));
            }
        } else if (!FP::defined(Bs.read().toReal())) {
            Variant::Root tmp(value);
            remap("Cs" + wlCode, tmp);
            Bs.write().setDouble(convertScattering4(tmp.read().toDouble()));
        }
    }

    /* Scattering in 1E-3 m-1 */
    if (fields.empty()) return 5;
    field = fields.back();
    fields.pop_back();
    if (config.first().strictMode) {
        if (!isAllDigits(field)) return 6;
    }
    value = field.parse_real(&ok);
    if (!ok) return 7;
    if (!FP::defined(value)) return 8;
    if (value < 0.0) return 9;
    if (overflow) {
        if (!FP::defined(Cs.read().toReal())) {
            Cs.write().setDouble(value);
            remap("Cs" + wlCode, Cs);

            Cd.write().setDouble(0.0);
            remap("Cd", Cd);

            if (!FP::defined(Bs.read().toReal())) {
                Bs.write().setDouble(convertScattering3(Cs.read().toDouble()));
            }
        } else if (!FP::defined(Bs.read().toReal())) {
            Variant::Root tmp(value);
            remap("Cs" + wlCode, tmp);
            Bs.write().setDouble(convertScattering3(tmp.read().toDouble()));
        }
    }

    /* Calibrator in 1E-3 m-1 */
    if (fields.empty()) return 10;
    field = fields.back();
    fields.pop_back();
    if (config.first().strictMode) {
        if (!isAllDigits(field)) return 11;
    }
    value = field.parse_real(&ok);
    if (!ok) return 12;
    if (!FP::defined(value)) return 13;
    if (value < 0.0) return 14;
    if (!FP::defined(Cf.read().toReal())) {
        Cf.write().setDouble(value);
        remap("Cf" + wlCode, Cf);
        if (!FP::defined(Bsf.read().toReal())) {
            Bsf.write().setDouble(convertScattering3(Cf.read().toDouble()));
        }
    } else if (!FP::defined(Bsf.read().toReal())) {
        Variant::Root tmp(value);
        remap("Cf" + wlCode, tmp);
        Bsf.write().setDouble(convertScattering3(tmp.read().toDouble()));
    }

    return 0;
}

int AcquireRRNeph903::processA(std::deque<Util::ByteView> fields, double frameTime)
{
    if (!FP::defined(frameTime))
        return 99;

    Variant::Root Bs;
    Variant::Root Bsf;
    Variant::Root Cs;
    Variant::Root Cd;
    Variant::Root Cf;
    RETURN_ERROR_CODE(processTail2(fields, Bs, Bsf, Cs, Cd, Cf), 100);
    return errorCodeOffset(
            processExtended(fields, frameTime, 'A', std::move(Cs), std::move(Cd), std::move(Cf)),
            200);
}

int AcquireRRNeph903::processB(std::deque<Util::ByteView> fields, double frameTime)
{
    if (!FP::defined(frameTime))
        return 99;

    RETURN_ERROR_CODE(processSettingsPairs(fields), 100);
    return errorCodeOffset(processExtended(fields, frameTime, 'B'), 200);
}

int AcquireRRNeph903::processC(std::deque<Util::ByteView> fields, double frameTime)
{
    if (!FP::defined(frameTime))
        return 99;

    Variant::Root Bs;
    Variant::Root Bsf;
    Variant::Root Cs;
    Variant::Root Cd;
    Variant::Root Cf;
    RETURN_ERROR_CODE(processSettingsPairs(fields), 100);
    RETURN_ERROR_CODE(processTail2(fields, Bs, Bsf, Cs, Cd, Cf), 200);
    return errorCodeOffset(
            processExtended(fields, frameTime, 'C', std::move(Cs), std::move(Cd), std::move(Cf)),
            300);
}

int AcquireRRNeph903::processD(std::deque<Util::ByteView> fields, double frameTime)
{
    if (!FP::defined(frameTime))
        return 99;

    Variant::Root Bs;
    Variant::Root Cs;
    Variant::Root Cd;
    RETURN_ERROR_CODE(processTail1(fields, Bs, Cs, Cd), 100);
    return errorCodeOffset(processExtended(fields, frameTime, 'D', std::move(Cs), std::move(Cd)),
                           200);
}

int AcquireRRNeph903::processE(std::deque<Util::ByteView> fields, double frameTime)
{
    if (!FP::defined(frameTime))
        return 99;

    Variant::Root Bs;
    Variant::Root Bsf;
    Variant::Root Cs;
    Variant::Root Cd;
    Variant::Root Cf;
    RETURN_ERROR_CODE(processTail1(fields, Bs, Cs, Cd), 100);
    RETURN_ERROR_CODE(processTail2(fields, Bs, Bsf, Cs, Cd, Cf), 200);
    return errorCodeOffset(
            processExtended(fields, frameTime, 'E', std::move(Cs), std::move(Cd), std::move(Cf)),
            300);
}

int AcquireRRNeph903::processF(std::deque<Util::ByteView> fields, double frameTime)
{
    if (!FP::defined(frameTime))
        return 99;

    Variant::Root Bs;
    Variant::Root Cs;
    Variant::Root Cd;
    RETURN_ERROR_CODE(processTail1(fields, Bs, Cs, Cd), 100);
    RETURN_ERROR_CODE(processSettingsPairs(fields), 200);
    return errorCodeOffset(processExtended(fields, frameTime, 'F', std::move(Cs), std::move(Cd)),
                           300);
}

int AcquireRRNeph903::processG(std::deque<Util::ByteView> fields, double frameTime)
{
    if (!FP::defined(frameTime))
        return 99;

    Variant::Root Bs;
    Variant::Root Bsf;
    Variant::Root Cs;
    Variant::Root Cd;
    Variant::Root Cf;
    RETURN_ERROR_CODE(processTail1(fields, Bs, Cs, Cd), 100);
    RETURN_ERROR_CODE(processSettingsPairs(fields), 200);
    RETURN_ERROR_CODE(processTail2(fields, Bs, Bsf, Cs, Cd, Cf), 300);
    return errorCodeOffset(
            processExtended(fields, frameTime, 'G', std::move(Cs), std::move(Cd), std::move(Cf)),
            400);
}

int AcquireRRNeph903::processI(std::deque<Util::ByteView> fields, double frameTime)
{
    if (!FP::defined(frameTime))
        return 99;

    Variant::Root Bs;
    Variant::Root Bsf;
    Variant::Root Cs;
    Variant::Root Cd;
    Variant::Root Cf;
    RETURN_ERROR_CODE(processTail2(fields, Bs, Bsf, Cs, Cd, Cf), 100);
    return errorCodeOffset(
            processADCBase(fields, frameTime, 'I', std::move(Bs), std::move(Bsf), std::move(Cs),
                           std::move(Cd), std::move(Cf)), 200);
}

int AcquireRRNeph903::processJ(std::deque<Util::ByteView> fields, double frameTime)
{
    if (!FP::defined(frameTime))
        return 99;

    RETURN_ERROR_CODE(processSettingsPairs(fields), 100);
    return errorCodeOffset(processADCBase(fields, frameTime, 'J'), 200);
}

int AcquireRRNeph903::processK(std::deque<Util::ByteView> fields, double frameTime)
{
    if (!FP::defined(frameTime))
        return 99;

    Variant::Root Bs;
    Variant::Root Bsf;
    Variant::Root Cs;
    Variant::Root Cd;
    Variant::Root Cf;
    RETURN_ERROR_CODE(processSettingsPairs(fields), 100);
    RETURN_ERROR_CODE(processTail2(fields, Bs, Bsf, Cs, Cd, Cf), 200);
    return errorCodeOffset(
            processADCBase(fields, frameTime, 'K', std::move(Bs), std::move(Bsf), std::move(Cs),
                           std::move(Cd), std::move(Cf)), 300);
}

int AcquireRRNeph903::processL(std::deque<Util::ByteView> fields, double frameTime)
{
    if (!FP::defined(frameTime))
        return 99;

    Variant::Root Bs;
    Variant::Root Cs;
    Variant::Root Cd;
    RETURN_ERROR_CODE(processTail1(fields, Bs, Cs, Cd), 100);
    return errorCodeOffset(
            processADCBase(fields, frameTime, 'L', std::move(Bs), Variant::Root(), std::move(Cs),
                           std::move(Cd)), 200);
}

int AcquireRRNeph903::processM(std::deque<Util::ByteView> fields, double frameTime)
{
    if (!FP::defined(frameTime))
        return 99;

    Variant::Root Bs;
    Variant::Root Bsf;
    Variant::Root Cs;
    Variant::Root Cd;
    Variant::Root Cf;
    RETURN_ERROR_CODE(processTail1(fields, Bs, Cs, Cd), 100);
    RETURN_ERROR_CODE(processTail2(fields, Bs, Bsf, Cs, Cd, Cf), 200);
    return errorCodeOffset(
            processADCBase(fields, frameTime, 'M', std::move(Bs), std::move(Bsf), std::move(Cs),
                           std::move(Cd), std::move(Cf)), 300);
}

int AcquireRRNeph903::processN(std::deque<Util::ByteView> fields, double frameTime)
{
    if (!FP::defined(frameTime))
        return 99;

    Variant::Root Bs;
    Variant::Root Cs;
    Variant::Root Cd;
    RETURN_ERROR_CODE(processTail1(fields, Bs, Cs, Cd), 100);
    RETURN_ERROR_CODE(processSettingsPairs(fields), 200);
    return errorCodeOffset(
            processADCBase(fields, frameTime, 'N', std::move(Bs), Variant::Root(), std::move(Cs),
                           std::move(Cd), Variant::Root()), 300);
}

int AcquireRRNeph903::processO(std::deque<Util::ByteView> fields, double frameTime)
{
    if (!FP::defined(frameTime))
        return 99;

    Variant::Root Bs;
    Variant::Root Bsf;
    Variant::Root Cs;
    Variant::Root Cd;
    Variant::Root Cf;
    RETURN_ERROR_CODE(processTail1(fields, Bs, Cs, Cd), 100);
    RETURN_ERROR_CODE(processSettingsPairs(fields), 200);
    RETURN_ERROR_CODE(processTail2(fields, Bs, Bsf, Cs, Cd, Cf), 300);
    return errorCodeOffset(
            processADCBase(fields, frameTime, 'O', std::move(Bs), std::move(Bsf), std::move(Cs),
                           std::move(Cd), std::move(Cf)), 400);
}

int AcquireRRNeph903::processRecord(const Util::ByteView &line, double &frameTime)
{
    Q_ASSERT(!config.isEmpty());

    /* Part of the logging dump format */
    {
        auto check = line;
        check.string_trimmed();
        if (check.empty() ||
                check.string_equal_insensitive("start") ||
                check.string_equal_insensitive("end"))
            return -1;
    }

    if (line.size() < 3)
        return 1;

    auto fields = CSV::acquisitionSplit(line);
    if (fields.size() < 2)
        return 2;

    /* First look for the display format identifier */
    if (fields.size() > 4 && fields[4].size() == 1) {
        switch (fields[4].front()) {
        case 'I':
        case 'i':
            return errorCodeOffset(processI(fields, frameTime), 1000);

        case 'J':
        case 'j':
            return errorCodeOffset(processJ(fields, frameTime), 2000);

        case 'K':
        case 'k':
            return errorCodeOffset(processK(fields, frameTime), 3000);

        case 'L':
        case 'l':
            return errorCodeOffset(processL(fields, frameTime), 4000);

        case 'M':
        case 'm':
            return errorCodeOffset(processM(fields, frameTime), 5000);

        case 'N':
        case 'n':
            return errorCodeOffset(processN(fields, frameTime), 6000);

        case 'O':
        case 'o':
            return errorCodeOffset(processO(fields, frameTime), 7000);

        default:
            break;
        }
    }
    if (fields.size() > 10 && fields.at(10).size() == 1) {
        switch (fields.at(10).front()) {
        case 'A':
        case 'a':
            return errorCodeOffset(processA(fields, frameTime), 8000);

        case 'B':
        case 'b':
            return errorCodeOffset(processB(fields, frameTime), 9000);

        case 'C':
        case 'c':
            return errorCodeOffset(processC(fields, frameTime), 10000);

        case 'D':
        case 'd':
            return errorCodeOffset(processD(fields, frameTime), 11000);

        case 'E':
        case 'e':
            return errorCodeOffset(processE(fields, frameTime), 12000);

        case 'F':
        case 'f':
            return errorCodeOffset(processF(fields, frameTime), 13000);

        case 'G':
        case 'g':
            return errorCodeOffset(processG(fields, frameTime), 14000);

        default:
            break;
        }
    }
    if (fields[0].size() == 1) {
        switch (fields[0].front()) {
        case 'H':
        case 'h':
            /* Ignore header lines (I assume that's what they are...) */
            if (fields.size() == 4) {
                if (fields[1] == "0000")
                    return -1;
            } else if (fields.size() == 8) {
                if (fields[1] == "0000")
                    return -1;
            }
            break;

        case 'c':
        case 'C':
            return errorCodeOffset(processVerbose(fields, frameTime), 15000);

        default:
            break;
        }
    }

    /* Try just the field length (either doesn't have a record code, or may
     * succeed due to relaxed interpretation of the record code) */
    switch (fields.size()) {
    case 6:
        /* Try harder at disambiguating this, since it's possible that
         * the hosting data logger is funny */
        if (fields[0].size() <= 1)
            return errorCodeOffset(processVerbose(fields, frameTime), 16000);
        return errorCodeOffset(processBasic(fields, frameTime), 17000);

    case 12:
        return errorCodeOffset(processShort(fields, frameTime), 18000);

    case 7:
        return errorCodeOffset(processVerbose(fields, frameTime), 19000);
    case 11:
        return errorCodeOffset(processVerbose(fields, frameTime), 20000);

    case 8:
        return errorCodeOffset(processI(fields, frameTime), 21000);
    case 23:
        return errorCodeOffset(processF(fields, frameTime), 22000);
    case 26:
        return errorCodeOffset(processG(fields, frameTime), 23000);
    default:
        break;
    }

    return 99;
}

void AcquireRRNeph903::configAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

void AcquireRRNeph903::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    spancheckController->setWavelength(0, config.first().wavelength, QString::fromStdString(
            Wavelength::code(config.first().wavelength)));
}

void AcquireRRNeph903::invalidateLogValues(double frameTime)
{
    priorScattering = FP::undefined();
    priorCalibrator = FP::undefined();
    lastScatteringTime = FP::undefined();
    lastCalibratorTime = FP::undefined();

    lastRecordTime = FP::undefined();
    loggingMux.clear();
    loggingLost(frameTime);
}

void AcquireRRNeph903::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (FP::defined(frameTime))
        configAdvance(frameTime);

    double unpolledTimeout = config.first().reportInterval;
    if (!FP::defined(unpolledTimeout) || unpolledTimeout < 0.0)
        unpolledTimeout = 1.0;
    unpolledTimeout = unpolledTimeout * 2.0 + 1.0;

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 5) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_PASSIVE_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                if (FP::defined(frameTime))
                    timeoutAt(frameTime + unpolledTimeout);

                realtimeStateUpdated = true;

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
        } else if (code < 0) {
            autoprobeValidRecords = 0;
            invalidateLogValues(frameTime);
        } else {
            lastRecordTime = FP::undefined();
            invalidateLogValues(frameTime);
        }
        break;
    }
    case RESP_PASSIVE_WAIT:
        if (processRecord(frame, frameTime) == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);

            responseState = RESP_PASSIVE_RUN;
            ++autoprobeValidRecords;
            generalStatusUpdated();

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else {
            autoprobeValidRecords = 0;
            invalidateLogValues(frameTime);
        }
        break;

    case RESP_INTERACTIVE_START_READPARAMETERS: {
        std::deque<Util::ByteView> fields(CSV::acquisitionSplit(frame));
        int code = processB(fields, frameTime);
        if (code == 0) {
            if (controlStream != NULL) {
                controlStream->writeControl("E");
            }

            if (FP::defined(frameTime)) {
                timeoutAt(frameTime + unpolledTimeout * 3.0);
                discardData(frameTime + 0.5, 1);
            }
            responseState = RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID;
        }
        break;
    }

    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            if (FP::defined(frameTime))
                timeoutAt(frameTime + unpolledTimeout);
            responseState = RESP_UNPOLLED_RUN;
            ++autoprobeValidRecords;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            autoprobeValidRecords = 0;

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            discardData(frameTime + 30.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        } else {
            lastRecordTime = FP::undefined();
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            ++autoprobeValidRecords;
            if (FP::defined(frameTime)) {
                timeoutAt(frameTime + unpolledTimeout);
                spancheckController->advance(frameTime, realtimeEgress);
            }
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();

            if (responseState == RESP_PASSIVE_RUN) {
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
                info.hash("ResponseState").setString("PassiveRun");
            } else {
                if (controlStream != NULL) {
                    controlStream->writeControl("V\r");
                }
                responseState = RESP_INTERACTIVE_START_EXITMENU;
                discardData(frameTime + 1.0);
                timeoutAt(frameTime + 30.0);
                info.hash("ResponseState").setString("Run");
            }
            generalStatusUpdated();
            autoprobeValidRecords = 0;

            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            spancheckController->terminate();
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_READTIME: {
        if (!FP::defined(frameTime)) return;

        /* 07 Jan 20:08:05 2014 */
        auto fields = Util::as_deque(Util::ByteView(frame).string_trimmed().split(' '));
        if (fields.size() != 4) return;
        bool ok = false;

        auto field = fields.front().string_trimmed();
        fields.pop_front();
        if (!isAllDigits(field)) return;
        int day = field.parse_i32(&ok);
        if (!ok) return;
        if (day < 1 || day > 31) return;

        Util::ByteArray lower(fields.front().string_trimmed());
        fields.pop_front();
        lower.string_to_lower();
        field = lower;
        int month = 0;
        if (field == "jan") month = 1;
        else if (field == "feb") month = 2;
        else if (field == "mar") month = 3;
        else if (field == "apr") month = 4;
        else if (field == "may") month = 5;
        else if (field == "jun") month = 6;
        else if (field == "jul") month = 7;
        else if (field == "aug") month = 8;
        else if (field == "sep") month = 9;
        else if (field == "nov") month = 11;
        else if (field == "dec") month = 12;
        else return;

        auto timeFields = Util::as_deque(fields.front().string_trimmed().split(':'));
        fields.pop_front();
        if (timeFields.size() != 3) return;

        int hour = timeFields.front().string_trimmed().parse_i32(&ok);
        timeFields.pop_front();
        if (!ok) return;
        if (hour < 0 || hour > 23) return;

        int minute = timeFields.front().string_trimmed().parse_i32(&ok);
        timeFields.pop_front();
        if (!ok) return;
        if (minute < 0 || minute > 59) return;

        int second = timeFields.front().string_trimmed().parse_i32(&ok);
        timeFields.pop_front();
        if (!ok) return;
        if (second < 0 || second > 61) return;

        int year = fields.front().string_trimmed().parse_i32(&ok);
        fields.pop_front();
        if (!ok) return;
        if (year < 1990 || year > 2999) return;

        QDateTime checkTime(QDate(year, month, day), QTime(hour, minute, second), Qt::UTC);

        if (fabs(Time::fromDateTime(checkTime) - frameTime) < 2.0) {
            if (controlStream != NULL) {
                controlStream->writeControl("V\r");
            }
            discardData(frameTime + 1.0);
            responseState = RESP_INTERACTIVE_START_SETLONG;
            timeoutAt(frameTime + 30.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                           Variant::Root("StartInteractiveSetLong"),
                                                           frameTime, FP::undefined()));
            }
            return;
        }

        if (FP::defined(frameTime)) {
            /* Times differ, so discard until we're aligned with a minute, then
             * set it */
            double alignedMinute = floor(frameTime / 60.0 + 1.0) * 60.0;
            discardData(alignedMinute);
            timeoutAt(alignedMinute + 10.0);
        }
        break;
    }

    default:
        break;
    }
}

void AcquireRRNeph903::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configAdvance(frameTime);

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        qCDebug(log) << "Timeout in unpolled state" << responseState << "at"
                     << Logging::time(frameTime);
        timeoutAt(frameTime + 30.0);

        if (controlStream != NULL) {
            controlStream->writeControl("V\r");
        }
        responseState = RESP_INTERACTIVE_START_EXITMENU;
        discardData(frameTime + 1.0);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("UnpolledRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        spancheckController->terminate();
        invalidateLogValues(frameTime);
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        spancheckController->terminate();
        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_START_EXITMENU:
    case RESP_INTERACTIVE_START_ENTERMENU:
    case RESP_INTERACTIVE_START_READTIME:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETLONG:
    case RESP_INTERACTIVE_START_READPARAMETERS:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        spancheckController->terminate();
        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        timeoutAt(frameTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("V\r");
        }
        responseState = RESP_INTERACTIVE_START_EXITMENU;
        discardData(frameTime + 1.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveExitMenu"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        spancheckController->terminate();
        invalidateLogValues(frameTime);
        break;

    default:
        spancheckController->terminate();
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireRRNeph903::discardDataCompleted(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configAdvance(frameTime);

    double unpolledTimeout = config.first().reportInterval;
    if (!FP::defined(unpolledTimeout) || unpolledTimeout < 0.0)
        unpolledTimeout = 1.0;
    unpolledTimeout = unpolledTimeout * 2.0 + 1.0;

    switch (responseState) {
    case RESP_INTERACTIVE_START_EXITMENU:
        if (controlStream != NULL) {
            controlStream->writeControl("S\r");
        }
        discardData(frameTime + 2.0);
        responseState = RESP_INTERACTIVE_START_ENTERMENU;
        timeoutAt(frameTime + 30.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveEnterMenu"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_ENTERMENU:
        if (controlStream != NULL) {
            controlStream->writeControl("T\r");
        }
        responseState = RESP_INTERACTIVE_START_READTIME;
        timeoutAt(frameTime + 30.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveReadTime"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_READTIME:
        if (controlStream != NULL) {
            QDateTime time(Time::toDateTime(frameTime + 0.5));

            Util::ByteArray cmd("T ");
            cmd += QByteArray::number(time.date().year() % 100).rightJustified(2, '0');
            cmd += QByteArray::number(time.date().month()).rightJustified(2, '0');
            cmd += QByteArray::number(time.date().day()).rightJustified(2, '0');
            cmd += QByteArray::number(time.time().hour()).rightJustified(2, '0');
            cmd += QByteArray::number(time.time().minute()).rightJustified(2, '0');
            cmd.push_back('\r');
            controlStream->writeControl(std::move(cmd));
        }
        responseState = RESP_INTERACTIVE_START_SETTIME;
        timeoutAt(frameTime + 30.0);
        discardData(frameTime + 1.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetTime"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETTIME:
        if (controlStream != NULL) {
            controlStream->writeControl("V\r");
        }
        discardData(frameTime + 1.0);
        responseState = RESP_INTERACTIVE_START_SETLONG;
        timeoutAt(frameTime + 30.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetLong"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETLONG:
        if (controlStream != NULL) {
            controlStream->writeControl("B");
        }

        if (config.first().zeroRelay >= 0) {
            discardData(frameTime + 0.5);
            responseState = RESP_INTERACTIVE_START_SETRELAY_ZERO;
            timeoutAt(frameTime + 10.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveSettingZero"), frameTime, FP::undefined()));
            }
        } else if (config.first().spanRelay >= 0) {
            discardData(frameTime + 0.5);
            responseState = RESP_INTERACTIVE_START_SETRELAY_SPAN;
            timeoutAt(frameTime + 10.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveSettingSpan"), frameTime, FP::undefined()));
            }
        } else {
            discardData(frameTime + 0.5, 2);
            timeoutAt(frameTime + unpolledTimeout * 3.0);

            responseState = RESP_INTERACTIVE_START_READPARAMETERS;
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveInitialDiscard"), frameTime, FP::undefined()));
            }
        }
        break;

    case RESP_INTERACTIVE_START_SETRELAY_ZERO:
        setRelay(config.first().zeroRelay, false);
        if (config.first().spanRelay >= 0) {
            discardData(frameTime + 0.5);
            responseState = RESP_INTERACTIVE_START_SETRELAY_SPAN;
            timeoutAt(frameTime + 10.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveSettingSpan"), frameTime, FP::undefined()));
            }
        } else {
            discardData(frameTime + 0.5, 2);
            timeoutAt(frameTime + unpolledTimeout * 3.0);

            responseState = RESP_INTERACTIVE_START_READPARAMETERS;
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveInitialDiscard"), frameTime, FP::undefined()));
            }
        }
        break;

    case RESP_INTERACTIVE_START_SETRELAY_SPAN:
        setRelay(config.first().spanRelay, false);
        discardData(frameTime + 0.5, 2);
        timeoutAt(frameTime + unpolledTimeout * 3.0);

        responseState = RESP_INTERACTIVE_START_READPARAMETERS;
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveInitialDiscard"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_READPARAMETERS:
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadParameters"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
        sampleState = SAMPLE_RUN;
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveUnpolledStartReadFirstRecord"), frameTime, FP::undefined()));
        }
        break;


    case RESP_INTERACTIVE_RESTART_WAIT:
        timeoutAt(frameTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("V\r");
        }
        responseState = RESP_INTERACTIVE_START_EXITMENU;
        discardData(frameTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    default:
        break;
    }
}


void AcquireRRNeph903::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frame);
    Q_UNUSED(frameTime);
}

SequenceValue::Transfer AcquireRRNeph903::constructSpancheckVariables()
{
    SequenceValue::Transfer result;
    if (!spancheckDetails.read().exists())
        return result;

    const auto &wlCode = Wavelength::code(config.front().wavelength);

    result.emplace_back(spancheckDetails);

    auto add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                        NephelometerSpancheckController::TotalPercentError,
                                                        QString::fromStdString(wlCode));
    if (add.read().exists()) {
        result.emplace_back(SequenceName({}, "raw", "PCTc" + wlCode), std::move(add),
                            spancheckDetails.getStart(), spancheckDetails.getEnd());
    }

    add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                   NephelometerSpancheckController::TotalSensitivityFactor,
                                                   QString::fromStdString(wlCode));
    if (add.read().exists()) {
        result.emplace_back(SequenceName({}, "raw", "Cc" + wlCode), std::move(add),
                            spancheckDetails.getStart(), spancheckDetails.getEnd());
    }

    return result;
}

void AcquireRRNeph903::updateSpancheckData()
{
    if (persistentEgress) {
        spancheckDetails.setEnd(lastRecordTime);
        persistentEgress->incomingData(constructSpancheckVariables());
    }

    spancheckDetails.setRoot(
            spancheckController->results(NephelometerSpancheckController::FullResults));
    spancheckDetails.setStart(lastRecordTime);
    spancheckDetails.setEnd(FP::undefined());

    persistentValuesUpdated();

    if (realtimeEgress) {
        realtimeEgress->incomingData(constructSpancheckVariables());
    }
}

SequenceValue::Transfer AcquireRRNeph903::getPersistentValues()
{
    PauseLock paused(*this);
    return constructSpancheckVariables();
}

Variant::Root AcquireRRNeph903::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireRRNeph903::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

void AcquireRRNeph903::beginZero(double time, bool ignoreState)
{
    Q_ASSERT(!config.isEmpty());

    if (!ignoreState && sampleState != SAMPLE_RUN) {
        qCDebug(log) << "Discarding zero request, sample state:" << sampleState
                     << ", response state:" << responseState;
        return;
    }

    if (!FP::defined(time))
        time = lastRecordTime;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        setRelay(config.first().zeroRelay, true);

        sampleState = SAMPLE_ZERO_BEGINFLUSH;
        realtimeStateUpdated = true;
        if (FP::defined(time)) {
            sampleStateEndTime = zeroFlushTime->apply(time, time, true);
        } else {
            sampleStateEndTime = FP::undefined();
        }
        qCDebug(log) << "Initiating zero, zero air flush ends at"
                     << Logging::time(sampleStateEndTime);

        break;

    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
        qCDebug(log) << "Discarding zero request during passive state" << responseState;
        return;

    case RESP_INTERACTIVE_START_EXITMENU:
    case RESP_INTERACTIVE_START_ENTERMENU:
    case RESP_INTERACTIVE_START_READTIME:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETLONG:
    case RESP_INTERACTIVE_START_SETRELAY_ZERO:
    case RESP_INTERACTIVE_START_SETRELAY_SPAN:
    case RESP_INTERACTIVE_START_READPARAMETERS:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Discarding zero request during start communications state"
                     << responseState;
        return;
    }
}

AcquireRRNeph903::SpancheckInterface::SpancheckInterface(AcquireRRNeph903 *neph) : parent(neph)
{ }

AcquireRRNeph903::SpancheckInterface::~SpancheckInterface()
{ }

void AcquireRRNeph903::SpancheckInterface::setBypass(bool enable)
{
    if (parent->state == NULL)
        return;
    if (enable) {
        parent->state->setBypassFlag("Spancheck");
        qCDebug(parent->log) << "Spancheck bypass flag set";
    } else {
        parent->state->clearBypassFlag("Spancheck");
        qCDebug(parent->log) << "Spancheck bypass flag cleared";
    }
}

void AcquireRRNeph903::SpancheckInterface::switchToFilteredAir()
{
    Q_ASSERT(!parent->config.isEmpty());

    parent->setRelay(parent->config.first().spanRelay, false);

    qCDebug(parent->log) << "Spancheck switching to filtered air";
}

void AcquireRRNeph903::SpancheckInterface::switchToGas(CPD3::Algorithms::Rayleigh::Gas gas)
{
    Q_UNUSED(gas)

    parent->setRelay(parent->config.first().spanRelay, true);

    qCDebug(parent->log) << "Spancheck switching to span gas";
}

void AcquireRRNeph903::SpancheckInterface::issueZero()
{
    Q_ASSERT(!parent->config.isEmpty());

    qCDebug(parent->log) << "Spancheck issuing zero";
    parent->beginZero(FP::undefined(), true);
}

void AcquireRRNeph903::SpancheckInterface::issueCommand(const QString &target,
                                                        const CPD3::Data::Variant::Read &command)
{
    if (parent->state == NULL)
        return;
    parent->state->sendCommand(target.toStdString(), command);
}

bool AcquireRRNeph903::SpancheckInterface::start()
{
    Q_ASSERT(!parent->config.isEmpty());

    switch (parent->responseState) {
    case AcquireRRNeph903::RESP_PASSIVE_RUN:
    case AcquireRRNeph903::RESP_UNPOLLED_RUN:
        break;
    default:
        return false;
    }
    switch (parent->sampleState) {
    case AcquireRRNeph903::SAMPLE_RUN:
        break;
    default:
        return false;
    }

    qCDebug(parent->log) << "Beginning spancheck";
    parent->sampleState = SAMPLE_SPANCHECK;
    parent->realtimeStateUpdated = true;

    parent->setRelay(parent->config.first().zeroRelay, true);

    parent->event(parent->lastRecordTime, QObject::tr("Spancheck initiated."), false);

    return true;
}

void AcquireRRNeph903::SpancheckInterface::completed()
{
    Q_ASSERT(!parent->config.isEmpty());

    parent->updateSpancheckData();
    if (parent->sampleState == SAMPLE_SPANCHECK)
        parent->sampleState = SAMPLE_RUN;
    parent->realtimeStateUpdated = true;

    qCDebug(parent->log) << "Spancheck completed";

    parent->event(parent->lastRecordTime, QObject::tr("Spancheck completed."), false,
                  parent->spancheckDetails.getValue());
}

void AcquireRRNeph903::SpancheckInterface::aborted()
{
    Q_ASSERT(!parent->config.isEmpty());

    switch (parent->responseState) {
    case AcquireRRNeph903::RESP_PASSIVE_RUN:
    case AcquireRRNeph903::RESP_UNPOLLED_RUN:
        break;
    default:
        return;
    }

    qCDebug(parent->log) << "Spancheck aborted";

    parent->setRelay(parent->config.first().spanRelay, false);
    parent->beginZero(FP::undefined(), true);

    parent->event(parent->lastRecordTime, QObject::tr("Spancheck aborted."), false);
}

void AcquireRRNeph903::command(const Variant::Read &command)
{
    Q_ASSERT(!config.isEmpty());

    spancheckController->command(command);

    if (command.hash("StartZero").exists()) {
        beginZero();
    }
}

Variant::Root AcquireRRNeph903::getCommands()
{
    Variant::Root result = spancheckController->getCommands();

    result["StartZero"].hash("DisplayName").setString("Start &Zero");
    result["StartZero"].hash("ToolTip").setString("Start a zero offset adjustment.");
    result["StartZero"].hash("Include").array(0).hash("Type").setString("Equal");
    result["StartZero"].hash("Include").array(0).hash("Value").setString("Run");
    result["StartZero"].hash("Include").array(0).hash("Variable").setString("ZSTATE");

    return result;
}

AcquisitionInterface::AutoprobeStatus AcquireRRNeph903::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireRRNeph903::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_START_EXITMENU:
    case RESP_INTERACTIVE_START_ENTERMENU:
    case RESP_INTERACTIVE_START_READTIME:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETLONG:
    case RESP_INTERACTIVE_START_SETRELAY_ZERO:
    case RESP_INTERACTIVE_START_SETRELAY_SPAN:
    case RESP_INTERACTIVE_START_READPARAMETERS:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("V\r");
        }
        responseState = RESP_INTERACTIVE_START_EXITMENU;
        discardData(time + 2.0);

        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireRRNeph903::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    double unpolledTimeout = config.first().reportInterval;
    if (!FP::defined(unpolledTimeout) || unpolledTimeout < 0.0)
        unpolledTimeout = 1.0;
    unpolledTimeout = unpolledTimeout * 7.0 + 3.0;

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobeValidRecords = 0;
    discardData(time + 0.5, 1);
    timeoutAt(time + unpolledTimeout);
    generalStatusUpdated();
}

void AcquireRRNeph903::autoprobePromote(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    double unpolledTimeout = config.first().reportInterval;
    if (!FP::defined(unpolledTimeout) || unpolledTimeout < 0.0)
        unpolledTimeout = 1.0;
    unpolledTimeout = unpolledTimeout * 2.0 + 1.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        qCDebug(log) << "Promoted from interactive run to interactive acquisition at"
                     << Logging::time(time);
        /* Reset timeout in case we didn't have a control stream */
        timeoutAt(time + unpolledTimeout);
        break;

    case RESP_INTERACTIVE_START_EXITMENU:
    case RESP_INTERACTIVE_START_ENTERMENU:
    case RESP_INTERACTIVE_START_READTIME:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETLONG:
    case RESP_INTERACTIVE_START_SETRELAY_ZERO:
    case RESP_INTERACTIVE_START_SETRELAY_SPAN:
    case RESP_INTERACTIVE_START_READPARAMETERS:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);
        break;

    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("V\r");
        }
        responseState = RESP_INTERACTIVE_START_EXITMENU;
        discardData(time + 2.0);

        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireRRNeph903::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    double unpolledTimeout = config.first().reportInterval;
    if (!FP::defined(unpolledTimeout) || unpolledTimeout < 0.0)
        unpolledTimeout = 1.0;
    unpolledTimeout = unpolledTimeout * 2.0 + 1.0;

    /* Reset timeout in case we didn't have a control stream */
    timeoutAt(time + unpolledTimeout);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);

        break;

    case RESP_UNPOLLED_RUN:
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from unpolled interactive state to passive acquisition at"
                     << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        realtimeStateUpdated = true;

        discardData(FP::undefined());
        generalStatusUpdated();
        break;
    }
}

static const quint16 serializedStateVersion = 1;

void AcquireRRNeph903::serializeState(QDataStream &stream)
{
    PauseLock paused(*this);

    stream << serializedStateVersion;

    stream << spancheckDetails.getStart();
    stream << spancheckDetails.read();

    stream << Bsz;
}

void AcquireRRNeph903::deserializeState(QDataStream &stream)
{
    quint16 version = 0;
    stream >> version;
    if (version != serializedStateVersion)
        return;

    PauseLock paused(*this);

    double start = FP::undefined();
    stream >> start;
    spancheckDetails.setStart(start);
    spancheckDetails.setEnd(FP::undefined());
    stream >> spancheckDetails.root();

    stream >> Bsz;
}

AcquisitionInterface::AutomaticDefaults AcquireRRNeph903::getDefaults()
{
    AutomaticDefaults result;
    result.name = "S$1$2";
    result.setSerialN81(9600);
    return result;
}


ComponentOptions AcquireRRNeph903Component::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);

    return options;
}

ComponentOptions AcquireRRNeph903Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireRRNeph903Component::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireRRNeph903Component::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireRRNeph903Component::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireRRNeph903Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireRRNeph903Component::createAcquisitionPassive(const ComponentOptions &options,
                                                                                  const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireRRNeph903(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireRRNeph903Component::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireRRNeph903(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireRRNeph903Component::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                    const std::string &loggingContext)
{
    std::unique_ptr<AcquireRRNeph903> i(new AcquireRRNeph903(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireRRNeph903Component::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                      const std::string &loggingContext)
{
    std::unique_ptr<AcquireRRNeph903> i(new AcquireRRNeph903(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
