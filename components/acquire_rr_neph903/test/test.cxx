/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray outgoing;
    QByteArray incoming;
    double unpolledRemaining;
    QDateTime time;

    char reportMode;

    double Cs;
    double Cf;
    double Cd;

    double Cl;
    double Cp;

    double V;
    double T;
    double U;
    double P;

    bool relay1;
    bool relay2;
    int recordCounter;

    ModelInstrument()
            : outgoing(),
              incoming(),
              unpolledRemaining(0),
              time(QDate(2013, 2, 3), QTime(1, 2, 0), Qt::UTC),
              reportMode(0)
    {
        Cd = 160;
        Cs = 16384 + Cd + 2810;
        Cf = 32768 + Cd + 2810;
        Cl = 30000;
        Cp = 29000;

        P = 900;
        T = 23.85;
        U = 12;

        relay1 = false;
        relay2 = false;
        recordCounter = 0;
    }

    double Bs() const
    {
        return (Cs - Cd - 2810) / 65536.0 * 2E-4 * 1E6;
    }

    double Bsf() const
    {
        return (Cf - Cd - 2810) / 65536.0 * 1E-3 * 1E6;
    }

    bool calibratorEngaged() const
    {
        return ((time.time().second() % 30) < 5);
    }

    double calibratorSimulatedNoise() const
    {
        return (double) (recordCounter % 10) / 5.0 + 0.1;
    }

    void outputBase(bool includeRelays = true, bool calibratorVariance = false)
    {
        outgoing.append(' ');
        if (!calibratorVariance) {
            outgoing.append(QByteArray::number(Bs() * 1E-6, 'e', 3).rightJustified(10));
            outgoing.append(' ');
            outgoing.append(QByteArray::number(Bsf() * 1E-6, 'e', 3).rightJustified(10));
        } else if (calibratorEngaged()) {
            outgoing.append(QByteArray::number(Bs() * 1E-6, 'e', 3).rightJustified(10));
            outgoing.append(' ');
            outgoing.append(QByteArray::number((Bsf() + calibratorSimulatedNoise()) * 1E-6, 'e',
                                               3).rightJustified(10));
        } else {
            outgoing.append(QByteArray::number((Bs() + calibratorSimulatedNoise()) * 1E-6, 'e',
                                               3).rightJustified(10));
            outgoing.append(' ');
            outgoing.append(QByteArray::number(Bsf() * 1E-6, 'e', 3).rightJustified(10));
        }
        outgoing.append(' ');
        outgoing.append(QByteArray::number(P, 'f', 0).rightJustified(4, '0'));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(T + 273.15, 'f', 0).rightJustified(3, '0'));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(U, 'f', 0).rightJustified(2, '0'));
        if (includeRelays) {
            outgoing.append(' ');
            outgoing.append(relay1 ? '1' : '0');
            outgoing.append(relay2 ? '1' : '0');
        }
    }

    void outputExtendedBase()
    {
        outgoing.append(calibratorEngaged() ? '1' : '0');
        outgoing.append(" 600 ");
        outgoing.append(QByteArray::number(Cp, 'f', 0).rightJustified(5, '0'));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(Cl, 'f', 0).rightJustified(5, '0'));
        outgoing.append(' ');
        outgoing.append(reportMode);
    }

    double scatteringInM3() const
    {
        double c = Cs - Cd - 2810.0;
        c *= 2E-4 / 1E-3;
        c += Cd + 2810.0;
        return c;
    }

    void outputTailType1()
    {
        outgoing.append(' ');
        outgoing.append(QByteArray::number(Cd, 'f', 0).rightJustified(5, '0'));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(Cd, 'f', 0).rightJustified(5, '0'));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(scatteringInM3(), 'f', 0).rightJustified(5, '0'));
        outgoing.append(" 02600 ");
        outgoing.append(QByteArray::number(Cs, 'f', 0).rightJustified(5, '0'));
        outgoing.append(" 06653");
    }

    void outputTailType2()
    {
        outgoing.append(' ');
        outgoing.append(QByteArray::number(Cf - Cd, 'f', 0).rightJustified(5, '0'));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(scatteringInM3() - Cd, 'f', 0).rightJustified(5, '0'));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(Cs - Cd, 'f', 0).rightJustified(5, '0'));
    }

    void outputTailParameters()
    {
        outgoing.append(" 49046  6.340e+01 17424  1.767e-02 12656  1.266e-05");
    }

    void outputVerboseTime(const QDateTime &time)
    {
        outgoing.append(QByteArray::number(time.date().year() % 100).rightJustified(2, '0'));
        outgoing.append(QByteArray::number(time.date().month()).rightJustified(2, '0'));
        outgoing.append(QByteArray::number(time.date().day()).rightJustified(2, '0'));
        outgoing.append(QByteArray::number(time.time().hour()).rightJustified(2, '0'));
        outgoing.append(QByteArray::number(time.time().minute()).rightJustified(2, '0'));
    }

    void advance(double seconds)
    {
        time = time.addMSecs((qint64) floor(seconds * 1000.0 + 0.5));
        if (reportMode != -1) {
            unpolledRemaining -= seconds;
            while (unpolledRemaining <= 0.0) {
                if (reportMode == 1) {
                    unpolledRemaining += 60.0;
                } else if (reportMode == 2) {
                    unpolledRemaining += 300.0;
                } else {
                    unpolledRemaining += 0.5;
                }

                switch (reportMode) {
                case 'A':
                    outputBase();
                    outgoing.append(' ');
                    outputExtendedBase();
                    outputTailType2();
                    outgoing.append("\r\n");
                    recordCounter++;
                    break;

                case 'B':
                    outputBase();
                    outgoing.append(' ');
                    outputExtendedBase();
                    outputTailParameters();
                    outgoing.append("\r\n");
                    recordCounter++;
                    break;

                case 'C':
                    outputBase();
                    outgoing.append(' ');
                    outputExtendedBase();
                    outputTailType2();
                    outputTailParameters();
                    outgoing.append("\r\n");
                    recordCounter++;
                    break;

                case 'D':
                    outputBase();
                    outgoing.append(' ');
                    outputExtendedBase();
                    outputTailType1();
                    outgoing.append("\r\n");
                    recordCounter++;
                    break;

                case 'E':
                    outputBase();
                    outgoing.append(' ');
                    outputExtendedBase();
                    outputTailType2();
                    outputTailType1();
                    outgoing.append("\r\n");
                    recordCounter++;
                    break;

                case 'F':
                    outputBase();
                    outgoing.append(' ');
                    outputExtendedBase();
                    outputTailParameters();
                    outputTailType1();
                    outgoing.append("\r\n");
                    recordCounter++;
                    break;

                case 'G':
                    outputBase();
                    outgoing.append(' ');
                    outputExtendedBase();
                    outputTailType2();
                    outputTailParameters();
                    outputTailType1();
                    outgoing.append("\r\n");
                    recordCounter++;
                    break;

                case 'I':
                    outputExtendedBase();
                    outputTailType2();
                    outgoing.append("\r\n");
                    recordCounter++;
                    break;

                case 'J':
                    outputExtendedBase();
                    outputTailParameters();
                    outgoing.append("\r\n");
                    recordCounter++;
                    break;

                case 'K':
                    outputExtendedBase();
                    outputTailType2();
                    outputTailParameters();
                    outgoing.append("\r\n");
                    recordCounter++;
                    break;

                case 'L':
                    outputExtendedBase();
                    outputTailType1();
                    outgoing.append("\r\n");
                    recordCounter++;
                    break;

                case 'M':
                    outputExtendedBase();
                    outputTailType2();
                    outputTailType1();
                    outgoing.append("\r\n");
                    recordCounter++;
                    break;

                case 'N':
                    outputExtendedBase();
                    outputTailParameters();
                    outputTailType1();
                    outgoing.append("\r\n");
                    recordCounter++;
                    break;

                case 'O':
                    outputExtendedBase();
                    outputTailType2();
                    outputTailParameters();
                    outputTailType1();
                    outgoing.append("\r\n");
                    recordCounter++;
                    break;

                case 0:
                    outputBase(true, true);
                    outgoing.append("\r\n");
                    recordCounter++;
                    break;

                case 1:
                    outgoing.append(
                            QByteArray::number(++recordCounter % 10000).rightJustified(4, '0'));
                    outgoing.append(' ');

                    outgoing.append(
                            QByteArray::number(time.date().year() % 100).rightJustified(2, '0'));
                    outgoing.append(' ');
                    outgoing.append(QByteArray::number(time.date().month()).rightJustified(2, '0'));
                    outgoing.append(' ');
                    outgoing.append(QByteArray::number(time.date().day()).rightJustified(2, '0'));
                    outgoing.append(' ');
                    outgoing.append(QByteArray::number(time.time().hour()).rightJustified(2, '0'));
                    outgoing.append(' ');
                    outgoing.append(
                            QByteArray::number(time.time().minute()).rightJustified(2, '0'));
                    outgoing.append(' ');
                    outgoing.append(
                            QByteArray::number(time.time().second()).rightJustified(2, '0'));
                    outgoing.append(' ');

                    outputBase(false);
                    outgoing.append("\r\n");
                    break;

                case 2:
                    if (recordCounter == 0) {
                        outgoing.append("start\r\n");
                        outgoing.append("h 0000 7 ");
                        outputVerboseTime(time);
                        outgoing.append("\r\n");
                        outgoing.append("h 0000 8 0020 ");
                        outputVerboseTime(time.addSecs(3600));
                        outgoing.append(" 0004 ");
                        outputVerboseTime(time);
                        outgoing.append(" 0000\r\n");
                    }
                    if (recordCounter == 0 || time.time().minute() == 0) {
                        outgoing.append("c ");
                        outgoing.append(
                                QByteArray::number(recordCounter % 10000).rightJustified(4, '0'));
                        outgoing.append(" 3 ");
                        outputVerboseTime(time);
                        outgoing.append(" 156  7.439e-01\r\n");

                        ++recordCounter;
                    }
                    if (recordCounter == 1 || time.time().minute() == 0) {
                        outgoing.append("c ");
                        outgoing.append(
                                QByteArray::number(recordCounter % 10000).rightJustified(4, '0'));
                        outgoing.append(" 4   6.340e+01  1.767e-02  1.266e-05 0300\r\n");

                        ++recordCounter;
                    }

                    outgoing.append("c ");
                    outgoing.append(
                            QByteArray::number(recordCounter % 10000).rightJustified(4, '0'));
                    outgoing.append(" 9 ");
                    outgoing.append(
                            QByteArray::number(time.time().minute()).rightJustified(2, '0'));
                    outgoing.append(" 1.550e-02  3.704e-01");
                    outputBase(false);
                    outgoing.append("\r\n");

                    ++recordCounter;
                    break;


                default:
                    Q_ASSERT(false);
                    break;
                }
            }

            while (!incoming.isEmpty()) {
                char c = incoming.at(0);
                incoming = incoming.mid(1);
                switch (c) {
                case 'W':
                case 'w':
                    relay1 = false;
                    break;

                case 'X':
                case 'x':
                    relay1 = true;
                    break;

                case 'Y':
                case 'y':
                    relay2 = false;
                    break;

                case 'Z':
                case 'z':
                    relay2 = true;
                    break;

                case 'S':
                case 's':
                    reportMode = -1;
                    incoming.clear();

                    outgoing.append(
                            "Menu Menu Menu\r\nMenu Menu Menu\r\nMenu Menu Menu\r\nMenu Menu Menu\r\nMenu Menu Menu\r\n");
                    outgoing.append(">");
                    break;

                case 'A':
                case 'a':
                    reportMode = 'A';
                    break;
                case 'B':
                case 'b':
                    reportMode = 'B';
                    break;
                case 'C':
                case 'c':
                    reportMode = 'C';
                    break;
                case 'D':
                case 'd':
                    reportMode = 'D';
                    break;
                case 'E':
                case 'e':
                    reportMode = 'E';
                    break;
                case 'F':
                case 'f':
                    reportMode = 'F';
                    break;
                case 'G':
                case 'g':
                    reportMode = 'G';
                    break;
                case 'I':
                case 'i':
                    reportMode = 'I';
                    break;
                case 'J':
                case 'j':
                    reportMode = 'J';
                    break;
                case 'K':
                case 'k':
                    reportMode = 'K';
                    break;
                case 'L':
                case 'l':
                    reportMode = 'L';
                    break;
                case 'M':
                case 'm':
                    reportMode = 'M';
                    break;
                case 'N':
                case 'n':
                    reportMode = 'N';
                    break;
                case 'O':
                case 'o':
                    reportMode = 'O';
                    break;

                case ' ':
                    reportMode = 0;
                    break;

                default:
                    break;
                }
            }
        } else {
            while (!incoming.isEmpty()) {
                int idx = incoming.indexOf('\r');
                int idx2 = incoming.indexOf('\n');
                if (idx2 != -1 && (idx == -1 || idx2 < idx))
                    idx = idx2;
                if (idx == -1)
                    break;
                outgoing.append(incoming.mid(0, idx + 1));

                QByteArray process(incoming.mid(0, idx));
                incoming = incoming.mid(idx + 1);

                if (process.isEmpty())
                    continue;
                switch (process.at(0)) {
                case 'T':
                case 't': {
                    QByteArray str(process.mid(1).trimmed());
                    if (str.isEmpty()) {
                        outgoing.append("\r\n");
                        outgoing.append(time.toString("dd MMM hh:mm:ss yyyy").toUtf8());
                        outgoing.append("\r\n");
                    } else if (str.length() == 10) {
                        time = QDateTime(QDate(str.mid(0, 2).toInt() + 2000, str.mid(2, 2).toInt(),
                                               str.mid(4, 2).toInt()),
                                         QTime(str.mid(6, 2).toInt(), str.mid(8, 2).toInt(), 0),
                                         Qt::UTC);
                    }
                    break;
                }

                case 'R':
                case 'r':
                case 'A':
                case 'a':
                    reportMode = 2;
                    incoming.clear();
                    break;
                case 'S':
                case 's':
                case 'B':
                case 'b':
                    reportMode = 1;
                    incoming.clear();
                    break;

                case 'M':
                case 'E':
                case 'X':
                case 'Y':
                case 'Z':
                case 'm':
                case 'e':
                case 'x':
                case 'y':
                case 'z':
                    break;

                case 'Q':
                case 'V':
                case 'W':
                case 'q':
                case 'v':
                case 'w':
                    reportMode = 0;
                    incoming.clear();
                    break;

                case 'H':
                case 'h':
                case '?':
                    outgoing.append(
                            "Menu Menu Menu\r\nMenu Menu Menu\r\nMenu Menu Menu\r\nMenu Menu Menu\r\nMenu Menu Menu\r\n");
                    break;

                default:
                    break;
                }

                outgoing.append("> ");
            }
        }
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream,
                   double time = FP::undefined(),
                   const SequenceName::Component &wl = "G")
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Bs" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Bsf" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Cf" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Cp" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Cpf" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("P", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("U", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Cd", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Tz", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Pz", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Bsz" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZPARAMETERS", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZSPANCHECK", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCTc" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Cc" + wl, Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream,
                           double time = FP::undefined(),
                           const SequenceName::Component &wl = "G")
    {
        if (!stream.hasMeta("Bszd" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZBs" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZCpRaw", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Cl", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZSTATE", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("T"))
            return false;
        if (!stream.checkContiguous("U"))
            return false;
        if (!stream.checkContiguous("P"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined(),
                     const SequenceName::Component &wl = "G")
    {
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("Bs" + wl, Variant::Root(model.Bs()), time))
            return false;
        if (!stream.hasAnyMatchingValue("Bsf" + wl, Variant::Root(model.Bsf()), time))
            return false;
        if (!stream.hasAnyMatchingValue("T", Variant::Root(model.T), time))
            return false;
        if (!stream.hasAnyMatchingValue("U", Variant::Root(model.U), time))
            return false;
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.P), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             double time = FP::undefined(),
                             const SequenceName::Component &wl = "G")
    {
        if (!stream.hasAnyMatchingValue("ZBs" + wl, Variant::Root(model.Bs()), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZSTATE", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkADCValues(StreamCapture &stream,
                        const ModelInstrument &model,
                        bool includeScattering = true,
                        bool includeCalibrator = true,
                        double time = FP::undefined(),
                        const SequenceName::Component &wl = "G")
    {
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (includeScattering &&
                !stream.hasAnyMatchingValue("Bs" + wl, Variant::Root(model.Bs()), time))
            return false;
        if (includeCalibrator &&
                !stream.hasAnyMatchingValue("Bsf" + wl, Variant::Root(model.Bsf()), time))
            return false;
        return true;
    }

    bool checkRealtimeADCValues(StreamCapture &stream,
                                const ModelInstrument &model,
                                bool includeScattering = true,
                                double time = FP::undefined(),
                                const SequenceName::Component &wl = "G")
    {
        if (includeScattering &&
                !stream.hasAnyMatchingValue("ZBs" + wl, Variant::Root(model.Bs()), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZSTATE", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkExtendedMode1(StreamCapture &stream,
                            const ModelInstrument &model,
                            double time = FP::undefined(),
                            const SequenceName::Component &wl = "G")
    {
        if (!stream.hasAnyMatchingValue("Cs" + wl, Variant::Root(model.Cs), time))
            return false;
        if (!stream.hasAnyMatchingValue("Cd", Variant::Root(model.Cd), time))
            return false;
        return true;
    }

    bool checkExtendedMode2(StreamCapture &stream,
                            const ModelInstrument &model,
                            double time = FP::undefined(),
                            const SequenceName::Component &wl = "G")
    {
        if (!stream.hasAnyMatchingValue("Cs" + wl, Variant::Root(model.Cs - model.Cd), time))
            return false;
        if (!stream.hasAnyMatchingValue("Cf" + wl, Variant::Root(model.Cf - model.Cd), time))
            return false;
        if (!stream.hasAnyMatchingValue("Cd", Variant::Root(0.0), time))
            return false;
        return true;
    }

    bool checkExtendedMode21(StreamCapture &stream,
                             const ModelInstrument &model,
                             double time = FP::undefined(),
                             const SequenceName::Component &wl = "G")
    {
        if (!stream.hasAnyMatchingValue("Cf" + wl, Variant::Root(model.Cf - model.Cd), time))
            return false;
        return true;
    }

    bool checkExtended(StreamCapture &stream,
                       const ModelInstrument &model,
                       double time = FP::undefined(),
                       const SequenceName::Component &wl = "G")
    {
        if (!stream.hasAnyMatchingValue("Cp" + wl, Variant::Root(model.Cp), time))
            return false;
        if (!stream.hasAnyMatchingValue("Cpf" + wl, Variant::Root(model.Cp), time))
            return false;
        return true;
    }

    bool checkRealtimeExtended(StreamCapture &stream,
                               const ModelInstrument &model,
                               double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("ZCpRaw", Variant::Root(model.Cp), time))
            return false;
        if (!stream.hasAnyMatchingValue("Cl", Variant::Root(model.Cl), time))
            return false;
        if (!stream.hasAnyMatchingValue("V", Variant::Root(600.0), time))
            return false;
        return true;
    }

    bool checkParameters(const SequenceValue::Transfer &values,
                         const SequenceName::Component &wl = "G")
    {
        for (const auto &check : values) {
            if (check.getArchive() != "raw")
                continue;
            if (check.getVariable() != "ZPARAMETERS")
                continue;
            const auto &v = check.read();
            if (!FP::equal(v.hash("Bsr" + wl).toDouble(), 12.66))
                continue;
            if (!FP::equal(v.hash("ZBsZeroRatio" + wl).toDouble(), 1.767e-02))
                continue;
            if (!FP::equal(v.hash("ZBsSpanRatio" + wl).toDouble(), 6.340e+01))
                continue;
            if (!FP::equal(v.hash("V").toDouble(), 600.0))
                continue;
            return true;
        }
        return false;
    }

    bool checkSpancheck(const SequenceValue::Transfer &values)
    {
        if (!StreamCapture::findValue(values, "raw", "ZSPANCHECK", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "PCTcG", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "CcG", Variant::Root()))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component =
                qobject_cast<AcquisitionComponent *>(ComponentLoader::create("acquire_rr_neph903"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_rr_neph903"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobeBase()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

    }

    void passiveAutoprobeLoggingShort()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = 1;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(30.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(30.0);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

    }

    void passiveAutoprobeLoggingVerbose()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = 2;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(150.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(150.0);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

    }

    void passiveAutoprobeA()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = 'A';
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkExtendedMode2(logging, instrument));
        QVERIFY(checkExtendedMode2(realtime, instrument));
        QVERIFY(checkExtended(logging, instrument));
        QVERIFY(checkRealtimeExtended(realtime, instrument));

    }

    void passiveAutoprobeB()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = 'B';
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkParameters(persistentValues));
        QVERIFY(checkParameters(realtime.values()));
        QVERIFY(checkExtended(logging, instrument));
        QVERIFY(checkRealtimeExtended(realtime, instrument));

    }

    void passiveAutoprobeC()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = 'C';
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkParameters(persistentValues));
        QVERIFY(checkParameters(realtime.values()));
        QVERIFY(checkExtendedMode2(logging, instrument));
        QVERIFY(checkExtendedMode2(realtime, instrument));
        QVERIFY(checkExtended(logging, instrument));
        QVERIFY(checkRealtimeExtended(realtime, instrument));

    }

    void passiveAutoprobeD()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = 'D';
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkExtendedMode1(logging, instrument));
        QVERIFY(checkExtendedMode1(realtime, instrument));
        QVERIFY(checkExtended(logging, instrument));
        QVERIFY(checkRealtimeExtended(realtime, instrument));

    }

    void passiveAutoprobeE()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = 'E';
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkExtendedMode1(logging, instrument));
        QVERIFY(checkExtendedMode21(logging, instrument));
        QVERIFY(checkExtendedMode1(realtime, instrument));
        QVERIFY(checkExtendedMode21(realtime, instrument));
        QVERIFY(checkExtended(logging, instrument));
        QVERIFY(checkRealtimeExtended(realtime, instrument));

    }

    void passiveAutoprobeF()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = 'F';
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkParameters(persistentValues));
        QVERIFY(checkParameters(realtime.values()));
        QVERIFY(checkExtendedMode1(logging, instrument));
        QVERIFY(checkExtendedMode1(realtime, instrument));
        QVERIFY(checkExtended(logging, instrument));
        QVERIFY(checkRealtimeExtended(realtime, instrument));

    }

    void passiveAutoprobeG()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = 'G';
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkParameters(persistentValues));
        QVERIFY(checkParameters(realtime.values()));
        QVERIFY(checkExtendedMode1(logging, instrument));
        QVERIFY(checkExtendedMode21(logging, instrument));
        QVERIFY(checkExtendedMode1(realtime, instrument));
        QVERIFY(checkExtendedMode21(realtime, instrument));
        QVERIFY(checkExtended(logging, instrument));
        QVERIFY(checkRealtimeExtended(realtime, instrument));

    }

    void passiveAutoprobeI()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = 'I';
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkADCValues(logging, instrument));
        QVERIFY(checkADCValues(realtime, instrument));
        QVERIFY(checkRealtimeADCValues(realtime, instrument));

        QVERIFY(checkExtended(logging, instrument));
        QVERIFY(checkRealtimeExtended(realtime, instrument));

    }

    void passiveAutoprobeJ()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = 'J';
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkADCValues(logging, instrument, false, false));
        QVERIFY(checkADCValues(realtime, instrument, false, false));
        QVERIFY(checkRealtimeADCValues(realtime, instrument, false));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkParameters(persistentValues));
        QVERIFY(checkParameters(realtime.values()));
        QVERIFY(checkExtended(logging, instrument));
        QVERIFY(checkRealtimeExtended(realtime, instrument));

    }

    void passiveAutoprobeK()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = 'K';
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkADCValues(logging, instrument));
        QVERIFY(checkADCValues(realtime, instrument));
        QVERIFY(checkRealtimeADCValues(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkParameters(persistentValues));
        QVERIFY(checkParameters(realtime.values()));
        QVERIFY(checkExtended(logging, instrument));
        QVERIFY(checkRealtimeExtended(realtime, instrument));

    }

    void passiveAutoprobeL()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = 'L';
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkADCValues(logging, instrument, true, false));
        QVERIFY(checkADCValues(realtime, instrument, true, false));
        QVERIFY(checkRealtimeADCValues(realtime, instrument));

        QVERIFY(checkExtendedMode1(logging, instrument));
        QVERIFY(checkExtendedMode1(realtime, instrument));
        QVERIFY(checkExtended(logging, instrument));
        QVERIFY(checkRealtimeExtended(realtime, instrument));

    }

    void passiveAutoprobeM()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = 'M';
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkADCValues(logging, instrument));
        QVERIFY(checkADCValues(realtime, instrument));
        QVERIFY(checkRealtimeADCValues(realtime, instrument));

        QVERIFY(checkExtendedMode1(logging, instrument));
        QVERIFY(checkExtendedMode21(logging, instrument));
        QVERIFY(checkExtendedMode1(realtime, instrument));
        QVERIFY(checkExtendedMode21(realtime, instrument));
        QVERIFY(checkExtended(logging, instrument));
        QVERIFY(checkRealtimeExtended(realtime, instrument));

    }

    void passiveAutoprobeN()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = 'N';
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkADCValues(logging, instrument, true, false));
        QVERIFY(checkADCValues(realtime, instrument, true, false));
        QVERIFY(checkRealtimeADCValues(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkParameters(persistentValues));
        QVERIFY(checkParameters(realtime.values()));
        QVERIFY(checkExtendedMode1(logging, instrument));
        QVERIFY(checkExtendedMode1(realtime, instrument));
        QVERIFY(checkExtended(logging, instrument));
        QVERIFY(checkRealtimeExtended(realtime, instrument));

    }

    void passiveAutoprobeO()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = 'O';
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkADCValues(logging, instrument));
        QVERIFY(checkADCValues(realtime, instrument));
        QVERIFY(checkRealtimeADCValues(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkParameters(persistentValues));
        QVERIFY(checkParameters(realtime.values()));
        QVERIFY(checkExtendedMode1(logging, instrument));
        QVERIFY(checkExtendedMode21(logging, instrument));
        QVERIFY(checkExtendedMode1(realtime, instrument));
        QVERIFY(checkExtendedMode21(realtime, instrument));
        QVERIFY(checkExtended(logging, instrument));
        QVERIFY(checkRealtimeExtended(realtime, instrument));

    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (int sleepTimer = 0;
                realTime.elapsed() < 30000 &&
                        interface->getAutoprobeStatus() ==
                                AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.25);
            if (sleepTimer++ % 10 == 0)
                QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkParameters(persistentValues));
        QVERIFY(checkParameters(realtime.values()));
        QVERIFY(checkExtendedMode1(logging, instrument));
        QVERIFY(checkExtendedMode21(logging, instrument));
        QVERIFY(checkExtendedMode1(realtime, instrument));
        QVERIFY(checkExtendedMode21(realtime, instrument));
        QVERIFY(checkExtended(logging, instrument));
        QVERIFY(checkRealtimeExtended(realtime, instrument));

    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = 'E';
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkExtendedMode1(logging, instrument));
        QVERIFY(checkExtendedMode21(logging, instrument));
        QVERIFY(checkExtendedMode1(realtime, instrument));
        QVERIFY(checkExtendedMode21(realtime, instrument));
        QVERIFY(checkExtended(logging, instrument));
        QVERIFY(checkRealtimeExtended(realtime, instrument));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 600; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        QTest::qSleep(50);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkParameters(persistentValues));
        QVERIFY(checkParameters(realtime.values()));
        QVERIFY(checkExtendedMode1(logging, instrument));
        QVERIFY(checkExtendedMode21(logging, instrument));
        QVERIFY(checkExtendedMode1(realtime, instrument));
        QVERIFY(checkExtendedMode21(realtime, instrument));
        QVERIFY(checkExtended(logging, instrument));
        QVERIFY(checkRealtimeExtended(realtime, instrument));

    }

    void interactiveZero()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Relay/Zero"].setInt64(1);
        cv["Zero/Flush"].setDouble(30.0);
        cv["Zero/Sample"].setDouble(60.0);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 240.0) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            if ((int) floor(control.time() * 4.0) % 10 == 0)
                QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));

        while (control.time() < 240.0) {
            if (logging.hasAnyMatchingValue("BsG", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.25);
            if ((int) floor(control.time() * 4.0) % 10 == 0)
                QTest::qSleep(50);
        }
        while (control.time() < 240.0) {
            if (logging.hasAnyMatchingValue("F1", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.5);
            if ((int) floor(control.time() * 4.0) % 10 == 0)
                QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        QVERIFY(logging.hasAnyMatchingValue("BsG", Variant::Root(), FP::undefined()));

        QVERIFY(!instrument.relay1);
        QVERIFY(!instrument.relay2);

        double checkTime = control.time();

        Variant::Write cmd = Variant::Write::empty();
        cmd["StartZero"].setBool(true);
        interface->incomingCommand(cmd);

        while (control.time() < 600.0) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Blank"), checkTime))
                break;
            if ((int) floor(control.time() * 4.0) % 10 == 0)
                QTest::qSleep(50);
        }
        QVERIFY(control.time() < 600.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Blank"), checkTime));
        for (int i = 0; i < 30; i++) {
            control.advance(0.125);
            QTest::qSleep(75);
        }

        QVERIFY(instrument.relay1);
        QVERIFY(!instrument.relay2);

        instrument.Cd = 120;
        instrument.Cs = 4096 + 2810 + instrument.Cd;

        checkTime = control.time();
        while (control.time() < 600.0) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Zero"), checkTime))
                break;
            if ((int) floor(control.time() * 4.0) % 10 == 0)
                QTest::qSleep(50);
        }
        QVERIFY(control.time() < 600.0);

        checkTime = control.time();
        while (control.time() < 600.0) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Blank"), checkTime))
                break;
            if ((int) floor(control.time() * 4.0) % 10 == 0)
                QTest::qSleep(50);
        }
        QVERIFY(control.time() < 600.0);
        for (int i = 0; i < 30; i++) {
            control.advance(0.125);
            QTest::qSleep(75);
        }

        QVERIFY(!instrument.relay1);
        QVERIFY(!instrument.relay2);

        instrument.Cd = 140;
        instrument.Cs = 16384 + 2810 + instrument.Cd;

        while (control.time() < 600.0) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            if ((int) floor(control.time() * 4.0) % 10 == 0)
                QTest::qSleep(50);
        }
        QVERIFY(control.time() < 600.0);

        QVERIFY(!instrument.relay1);
        QVERIFY(!instrument.relay2);

        for (int i = 0; i < 400; i++) {
            control.advance(0.25);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(logging.hasAnyMatchingValue("BsG", Variant::Root(37.5), checkTime));
        QVERIFY(realtime.hasAnyMatchingValue("BsG", Variant::Root(37.5), checkTime));
        QVERIFY(realtime.hasAnyMatchingValue("ZBsG", Variant::Root(37.5), checkTime));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkParameters(persistentValues));
        QVERIFY(checkParameters(realtime.values()));
        QVERIFY(checkExtendedMode1(logging, instrument, checkTime));
        QVERIFY(checkExtendedMode21(logging, instrument, checkTime));
        QVERIFY(checkExtendedMode1(realtime, instrument, checkTime));
        QVERIFY(checkExtendedMode21(realtime, instrument, checkTime));
        QVERIFY(checkExtended(logging, instrument, checkTime));
        QVERIFY(checkRealtimeExtended(realtime, instrument, checkTime));

    }

    void interactiveSpancheck()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Spancheck/Gas/MinimumSample"].setDouble(60.0);
        cv["Spancheck/Air/MinimumSample"].setDouble(60.0);
        cv["Spancheck/Gas/Flush"].setDouble(10.0);
        cv["Spancheck/Air/Flush"].setDouble(10.0);
        cv["Spancheck/Gas/Flush"].setDouble(10.0);
        cv["Spancheck/Air/Flush"].setDouble(10.0);
        cv["Relay/Zero"].setInt64(1);
        cv["Relay/Spancheck"].setInt64(2);
        cv["Zero/Flush"].setDouble(60.0);
        cv["Zero/Sample"].setDouble(120.0);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 240.0) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            if ((int) floor(control.time() * 4.0) % 10 == 0)
                QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));

        while (control.time() < 240.0) {
            if (logging.hasAnyMatchingValue("BsG", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.25);
            if ((int) floor(control.time() * 4.0) % 10 == 0)
                QTest::qSleep(50);
        }
        while (control.time() < 240.0) {
            if (logging.hasAnyMatchingValue("F1", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.5);
            if ((int) floor(control.time() * 4.0) % 10 == 0)
                QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        QVERIFY(logging.hasAnyMatchingValue("BsG", Variant::Root(), FP::undefined()));

        for (int i = 0; i < 20; i++) {
            control.advance(0.125);
            QTest::qSleep(50);
        }
        QVERIFY(!instrument.relay1);
        QVERIFY(!instrument.relay2);

        double checkTime = control.time();

        Variant::Write cmd = Variant::Write::empty();
        cmd["StartSpancheck"].setBool(true);
        interface->incomingCommand(cmd);

        while (control.time() < 600.0) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Spancheck"), checkTime))
                break;
            if ((int) floor(control.time() * 4.0) % 10 == 0)
                QTest::qSleep(50);
        }
        QVERIFY(control.time() < 600.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Spancheck"), checkTime));
        QVERIFY(realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("GasAirFlush"),
                                             checkTime, "Current"));

        for (int i = 0; i < 20; i++) {
            control.advance(0.125);
            QTest::qSleep(50);
        }
        QVERIFY(instrument.relay1);
        QVERIFY(!instrument.relay2);

        checkTime = control.time();
        while (control.time() < 600.0) {
            if (realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("GasSample"),
                                             checkTime, "Current"))
                break;
            control.advance(0.25);
            if ((int) floor(control.time() * 4.0) % 10 == 0)
                QTest::qSleep(50);
        }
        for (int i = 0; i < 20; i++) {
            control.advance(0.125);
            QTest::qSleep(50);
        }

        QVERIFY(instrument.relay1);
        QVERIFY(instrument.relay2);

        checkTime = control.time();
        while (control.time() < 600.0) {
            if (realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("AirSample"),
                                             checkTime, "Current"))
                break;
            control.advance(0.25);
            QTest::qSleep(50);
        }

        while (control.time() < 600.0) {
            if (realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("Inactive"),
                                             checkTime, "Current"))
                break;
            control.advance(0.5);
            if ((int) floor(control.time() * 4.0) % 10 == 0)
                QTest::qSleep(50);
        }
        QVERIFY(control.time() < 600.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSPANCHECKSTATE", Variant::Root("Inactive"),
                                             checkTime, "Current"));

        while (control.time() < 900.0) {
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Zero"), checkTime))
                break;
            control.advance(0.25);
            if ((int) floor(control.time() * 4.0) % 10 == 0)
                QTest::qSleep(50);
        }
        QVERIFY(control.time() < 900.0);
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Zero"), checkTime));

        for (int i = 0; i < 30; i++) {
            if (!instrument.relay2)
                break;
            control.advance(0.25);
            QTest::qSleep(50);
        }
        QVERIFY(!instrument.relay2);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(!checkSpancheck(persistent.values()));
        QVERIFY(checkSpancheck(persistentValues));
        QVERIFY(checkSpancheck(realtime.values()));

    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
