/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIRERRNEPH903_H
#define ACQUIRERRNEPH903_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QDateTime>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "acquisition/spancheck.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class AcquireRRNeph903 : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Passive autoprobe initialization, ignoring the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,
        /* Acquiring data in interactive mode, but only reading unpolled data */
        RESP_UNPOLLED_RUN,

        /* Waiting for the completion of the "V" command to exit the 
         * main menu */
                RESP_INTERACTIVE_START_EXITMENU,
        /* Waiting for the completion of the "S" command to enter the main
         * menu */
                RESP_INTERACTIVE_START_ENTERMENU,

        /* Waiting for the "T" command to read the time,
         * "06 Jan 18:03:09 2014" plus echo */
                RESP_INTERACTIVE_START_READTIME,
        /* Waiting for the "T" command to set the time */
                RESP_INTERACTIVE_START_SETTIME,

        /* After a delay from the "V" command, switch to long mode with an "B"
         * character */
                RESP_INTERACTIVE_START_SETLONG,

        /* Open both relays */
                RESP_INTERACTIVE_START_SETRELAY_ZERO,
        RESP_INTERACTIVE_START_SETRELAY_SPAN,

        /* Waiting for a valid "B" record so we get the parameters, before
         * switching to "E" record */
                RESP_INTERACTIVE_START_READPARAMETERS,

        /* Waiting for the first valid record */
                RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID,

        /* Waiting before attempting a communications restart */
                RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
                RESP_INTERACTIVE_INITIALIZE,
    };
    ResponseState responseState;
    int autoprobeValidRecords;
    double loggingBaseTime;

    enum {
        /* Normal operation. */
                SAMPLE_RUN,

        /* Zero control */
                SAMPLE_ZERO_BEGINFLUSH, SAMPLE_ZERO_MEASURE, SAMPLE_ZERO_ENDFLUSH,

        /* Spancheck in progress */
                SAMPLE_SPANCHECK,
    } sampleState;
    double sampleStateEndTime;

    class SpancheckInterface
            : public CPD3::Acquisition::NephelometerSpancheckController::Interface {
        AcquireRRNeph903 *parent;
    public:
        SpancheckInterface(AcquireRRNeph903 *neph);

        virtual ~SpancheckInterface();

        virtual void setBypass(bool enable);

        virtual void switchToFilteredAir();

        virtual void switchToGas(CPD3::Algorithms::Rayleigh::Gas gas);

        virtual void issueZero();

        virtual void issueCommand(const QString &target, const CPD3::Data::Variant::Read &command);

        virtual bool start();

        virtual void completed();

        virtual void aborted();
    };

    SpancheckInterface spancheckInterface;

    enum LogStreamID {
        LogStream_Metadata = 0, LogStream_State,

        LogStream_Status, LogStream_Scattering, LogStream_Calibrator,

        LogStream_TOTAL,
    };
    CPD3::Data::StaticMultiplexer loggingMux;

    friend class Configuration;

    class Configuration {
        double start;
        double end;
    public:
        double wavelength;
        bool strictMode;
        bool calibratorFill;
        double reportInterval;

        int zeroRelay;
        int spanRelay;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    friend class SpancheckInterface;

    CPD3::Acquisition::NephelometerSpancheckController *spancheckController;

    CPD3::Data::DynamicTimeInterval *zeroFlushTime;
    CPD3::Data::DynamicTimeInterval *zeroSampleTime;

    void setDefaultInvalid();

    CPD3::Data::Variant::Root instrumentMeta;

    CPD3::Data::SequenceValue spancheckDetails;
    CPD3::Data::SequenceValue instrumentParameters;

    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool realtimeStateUpdated;

    double priorScattering;
    double priorCalibrator;
    double lastScatteringTime;
    double lastCalibratorTime;

    double zeroEffectiveTime;
    CPD3::Data::Variant::Root Bsz;
    CPD3::Data::Variant::Root Bszd;
    CPD3::Data::Variant::Root Tz;
    CPD3::Data::Variant::Root Pz;
    bool zeroPersistentUpdated;
    bool zeroRealtimeUpdated;
    int zeroBsCount;
    double zeroBsSum;
    int zeroTCount;
    double zeroTSum;
    int zeroPCount;
    double zeroPSum;

    bool parametersPersistentUpdated;
    bool parametersRealtimeUpdated;


    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value,
                  int streamID);

    bool logFillCalibrator(double startTime,
                           double endTime,
                           CPD3::Data::SequenceName::Component name,
                           int streamID);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    void setRelay(int relay, bool enabled);

    void handleZeroUpdate(double currentTime);

    void zeroCompleted(double currentTime);

    void incomingScattering(double startTime,
                            double endTime,
                            CPD3::Data::Variant::Root &&Bs,
                            const CPD3::Data::Variant::Read &T = CPD3::Data::Variant::Read::empty(),
                            const CPD3::Data::Variant::Read &P = CPD3::Data::Variant::Read::empty(),
                            CPD3::Data::Variant::Root &&Cs = CPD3::Data::Variant::Root());

    void incomingCalibrator(double startTime,
                            double endTime,
                            CPD3::Data::Variant::Root &&Bsf,
                            CPD3::Data::Variant::Root &&Cf = CPD3::Data::Variant::Root());

    void incomingConditions(double startTime,
                            double endTime,
                            CPD3::Data::Variant::Root &&T,
                            CPD3::Data::Variant::Root &&P,
                            CPD3::Data::Variant::Root &&U);

    void recordUpdate(double startTime, double endTime);

    int processBasic(std::deque<CPD3::Util::ByteView> fields,
                     double frameTime,
                     bool includeRelays = true);

    int processExtended(std::deque<CPD3::Util::ByteView> fields,
                        double frameTime,
                        char expectedID,
                        CPD3::Data::Variant::Root &&Cs = CPD3::Data::Variant::Root(),
                        CPD3::Data::Variant::Root &&Cd = CPD3::Data::Variant::Root(),
                        CPD3::Data::Variant::Root &&Cf = CPD3::Data::Variant::Root());

    int processADCBase(std::deque<CPD3::Util::ByteView> fields,
                       double frameTime,
                       char expectedID,
                       CPD3::Data::Variant::Root &&Bs = CPD3::Data::Variant::Root(),
                       CPD3::Data::Variant::Root &&Bsf = CPD3::Data::Variant::Root(),
                       CPD3::Data::Variant::Root &&Cs = CPD3::Data::Variant::Root(),
                       CPD3::Data::Variant::Root &&Cd = CPD3::Data::Variant::Root(),
                       CPD3::Data::Variant::Root &&Cf = CPD3::Data::Variant::Root());

    int processShort(std::deque<CPD3::Util::ByteView> fields, double &frameTime);

    int processVerbose(std::deque<CPD3::Util::ByteView> fields, double &frameTime);

    int processSettingsPairs(std::deque<CPD3::Util::ByteView> &fields);

    int processTail1(std::deque<CPD3::Util::ByteView> &fields,
                     CPD3::Data::Variant::Root &Bs,
                     CPD3::Data::Variant::Root &Cs,
                     CPD3::Data::Variant::Root &Cd);

    int processTail2(std::deque<CPD3::Util::ByteView> &fields,
                     CPD3::Data::Variant::Root &Bs,
                     CPD3::Data::Variant::Root &Bsf,
                     CPD3::Data::Variant::Root &Cs,
                     CPD3::Data::Variant::Root &Cd,
                     CPD3::Data::Variant::Root &Cf);

    int processA(std::deque<CPD3::Util::ByteView> fields, double frameTime);

    int processB(std::deque<CPD3::Util::ByteView> fields, double frameTime);

    int processC(std::deque<CPD3::Util::ByteView> fields, double frameTime);

    int processD(std::deque<CPD3::Util::ByteView> fields, double frameTime);

    int processE(std::deque<CPD3::Util::ByteView> fields, double frameTime);

    int processF(std::deque<CPD3::Util::ByteView> fields, double frameTime);

    int processG(std::deque<CPD3::Util::ByteView> fields, double frameTime);

    int processI(std::deque<CPD3::Util::ByteView> fields, double frameTime);

    int processJ(std::deque<CPD3::Util::ByteView> fields, double frameTime);

    int processK(std::deque<CPD3::Util::ByteView> fields, double frameTime);

    int processL(std::deque<CPD3::Util::ByteView> fields, double frameTime);

    int processM(std::deque<CPD3::Util::ByteView> fields, double frameTime);

    int processN(std::deque<CPD3::Util::ByteView> fields, double frameTime);

    int processO(std::deque<CPD3::Util::ByteView> fields, double frameTime);

    int processRecord(const CPD3::Util::ByteView &line, double &frameTime);

    void configAdvance(double frameTime);

    void configurationChanged();

    void invalidateLogValues(double frameTime);

    CPD3::Data::SequenceValue::Transfer constructSpancheckVariables();

    void updateSpancheckData();

    void beginZero(double time = CPD3::FP::undefined(), bool ignoreState = false);

public:
    AcquireRRNeph903(const CPD3::Data::ValueSegment::Transfer &config,
                     const std::string &loggingContext);

    AcquireRRNeph903(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireRRNeph903();

    virtual CPD3::Data::SequenceValue::Transfer getPersistentValues();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    CPD3::Data::Variant::Root getCommands() override;

    virtual CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    virtual void serializeState(QDataStream &stream);

    virtual void deserializeState(QDataStream &stream);

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    virtual void command(const CPD3::Data::Variant::Read &value);

    virtual void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingInstrumentTimeout(double time);

    virtual void discardDataCompleted(double time);
};

class AcquireRRNeph903Component
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_rr_neph903"
                              FILE
                              "acquire_rr_neph903.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
