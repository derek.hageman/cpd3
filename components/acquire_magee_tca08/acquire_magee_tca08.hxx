/*
 * Copyright (c) 2020 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREMAGEETCA08_HXX
#define ACQUIREMAGEETCA08_HXX

#include "core/first.hxx"

#include <memory>
#include <deque>
#include <map>
#include <array>
#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class AcquireMageeTCA08 : public CPD3::Acquisition::FramedInstrument {
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;
    int autoprobeValidRecords;

    enum class ResponseState : int {
        /* Waiting a data record in passive mode */
                Passive_Run,

        /* Waiting for enough data to confirm communications */
                Passive_Wait,

        /* Passive initialization (same as passive wait, but discard the
         * first timeout). */
                Passive_Initialize,

        /* Same as passive wait but currently autoprobing */
                Autoprobe_Passive_Wait,

        /* Same as passive initialize but autoprobing */
                Autoprobe_Passive_Initialize,

        /* Stream disable ($TCA:END) and flush */
                Interactive_Start_Flush,

        /* $TCA:LAST DatabaseVer */
                Interactive_Start_Read_DatabaseVer,

        /* $TCA:LAST DatabaseVer (retry) */
                Interactive_Start_Read_DatabaseVer_Retry,

        /* $TCA:LAST FilterIntegrity */
                Interactive_Start_Read_FilterIntegrity,

        /* $TCA:LAST HeaterResistance */
                Interactive_Start_Read_HeaterResistance,

        /* $TCA:LAST Setup */
                Interactive_Start_Read_Setup,

        /* $TCA:INFO */
                Interactive_Start_Read_Info_Initial,

        /* Remaining lines in $TCA:INFO response */
                Interactive_Start_Read_Info_Continue,

        /* $TCA:LAST VerificationReport */
                Interactive_Start_Read_VerificationReport_Initial,

        /* Remaining lines in verification report response */
                Interactive_Start_Read_VerificationReport_Continue,

        /* $TCA:LAST OnlineResult */
                Interactive_Start_Read_OnlineResult,

        /* $TCA:LAST Data */
                Interactive_Start_Read_Data,

        /* Waiting before attempting a communications restart */
                Interactive_Restart_Wait,

        /* Initial state for interactive start */
                Interactive_Initialize,

        /* In interactive mode, awaiting the last data record */
                Interactive_Run_Data,
        /* In interactive mode, awaiting the last result record */
                Interactive_Run_OnlineResult,
        /* In interactive mode, sleeping before polling again */
                Interactive_Run_Wait,
    } responseState;
    int responseRetryCounter;

    class Configuration {
        double start;
        double end;

    public:
        bool strictMode;
        double pollInterval;
        double reportInterval;


        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under,
                      const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    std::deque<Configuration> config;

    void setDefaultInvalid();

    CPD3::Data::Variant::Root instrumentMeta;
    CPD3::Data::Variant::Root instrumentParameters;

    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool haveEmittedParameters;
    bool realtimeStateUpdated;

    CPD3::Data::Variant::Flags lastReportedFlags;

    enum class LogStream : std::size_t {
        Metadata = 0,

        Data,
        OnlineResult,

        TOTAL,
    };
    CPD3::Data::StaticMultiplexer loggingMux;
    std::array<double, static_cast<std::size_t>(LogStream::TOTAL)> streamBeginTime;

    std::uint_fast64_t lastDataID;
    double lastDataTime;

    std::uint_fast64_t lastResultID;
    double lastResultTime;

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root value);

    void loggingValue(LogStream stream,
                      double time,
                      CPD3::Data::SequenceName::Component name,
                      CPD3::Data::Variant::Root value);

    void advanceStream(LogStream stream, double minimumTime);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void invalidateLogValues(double frameTime);

    void configurationAdvance(double frameTime);

    int parseTime(std::deque<CPD3::Util::ByteView> &fields, double &outputTime);

    void emitMetadata(double frameTime);

    int processDataRecord(const CPD3::Util::ByteView &line, double &frameTime);

    int processOnlineResultRecord(const CPD3::Util::ByteView &line, double &frameTime);

    int processAnyRecord(const CPD3::Util::ByteView &line, double &frameTime);

public:
    AcquireMageeTCA08(const CPD3::Data::ValueSegment::Transfer &config,
                         const std::string &loggingContext);

    AcquireMageeTCA08(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireMageeTCA08();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables() override;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus() override;

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;
};

class AcquireMageeTCA08Component
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_magee_tca08"
                              FILE
                              "acquire_magee_tca08.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
