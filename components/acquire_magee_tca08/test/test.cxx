/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <array>
#include <cstdint>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double dataRemaining;
    double dataInterval;
    double resultRemaining;
    double resultInterval;
    bool enableUnpolled;
    int garbleVersion;

    QDateTime time;
    bool use24HourTime;
    bool useCSV;

    int dataID;
    std::array<std::uint8_t, 7> g;
    double flowS;
    double samplePumpSpeed;
    double flowA;
    double analyticPumpSpeed;

    struct Chamber {
        double T;
        double V1;
        double V2;
        double A1;
        double A2;
    };
    Chamber ch1;
    Chamber ch2;

    std::array<double, 4> fans;

    struct Licor {
        double T;
        double P;
        double CO2;
        double TD;
        double V;
    } licor;

    int resultID;
    double TC;
    double ambientCO2;

    ModelInstrument()
            : incoming(),
              outgoing(),
              dataRemaining(0),
              dataInterval(1.0),
              resultRemaining(0),
              resultInterval(60.0),
              enableUnpolled(false),
              garbleVersion(0),
              time(QDateTime(QDate(2013, 01, 01), QTime(0, 0, 0)))
    {
        use24HourTime = false;
        useCSV = false;

        dataID = 1000;
        g.fill(0);
        flowS = 16.6;
        samplePumpSpeed = 500;
        flowA = 0.5;
        analyticPumpSpeed = 700;
        ch1.T = 20;
        ch1.V1 = 0.5;
        ch1.V2 = 0.75;
        ch1.A1 = 0.05;
        ch1.A2 = 0.06;
        ch2.T = 90;
        ch2.V1 = 30;
        ch2.V2 = 32;
        ch2.A1 = 3.0;
        ch2.A2 = 4.0;
        fans[0] = 100;
        fans[1] = 101;
        fans[2] = 103;
        fans[3] = 104;
        licor.T = 30;
        licor.P = 850;
        licor.CO2 = 200000;
        licor.TD = 10;
        licor.V = 11.5;

        resultID = 10;
        TC = 29;
        ambientCO2 = 150000;
    }

    QByteArray convertTimestamp(const QDateTime &time) const
    {
        QByteArray result;
        result += QByteArray::number(time.date().month());
        result += "/";
        result += QByteArray::number(time.date().day());
        result += "/";
        result += QByteArray::number(time.date().year());
        result += " ";
        if (use24HourTime) {
            result += QByteArray::number(time.time().hour()).rightJustified(2, '0');
            result += ":";
            result += QByteArray::number(time.time().minute()).rightJustified(2, '0');
            result += ":";
            result += QByteArray::number(time.time().second()).rightJustified(2, '0');
        } else {
            result += time.time().toString("hh:mm:ss AP").toLatin1();
        }
        return result;
    }

    void dataResponse()
    {
        std::vector<QByteArray> fields;
        fields.emplace_back(QByteArray::number(dataID));
        fields.emplace_back(convertTimestamp(time));
        fields.emplace_back("51"); // SetupID
        fields.emplace_back(QByteArray::number(resultInterval));
        for (auto gv : g) {
            fields.emplace_back(QByteArray::number(gv));
        }
        if (time.time().second() == 0) {
            fields.emplace_back("Wait Sample"); // Ch1_Status
        } else {
            fields.emplace_back("S"); // Ch1_Status
        }
        fields.emplace_back("123"); // Ch1_SampleID
        fields.emplace_back("C5"); // Ch2_Status
        fields.emplace_back("124"); // Ch2_SampleID
        fields.emplace_back("N"); // MainBoardStatus
        fields.emplace_back("n"); // Ch1BoardStatus
        fields.emplace_back("n"); // Ch2BoardStatus
        fields.emplace_back("n"); // SensorBoardStatus
        fields.emplace_back(QByteArray::number(flowS));
        fields.emplace_back("0"); // setFlowS
        fields.emplace_back("0"); // FlowS_RAW
        fields.emplace_back(QByteArray::number(samplePumpSpeed));
        fields.emplace_back(QByteArray::number(flowA));
        fields.emplace_back("0"); // setFlowA
        fields.emplace_back("0"); // FlowA_RAW
        fields.emplace_back(QByteArray::number(analyticPumpSpeed));
        fields.emplace_back("1"); // Solenoid1
        fields.emplace_back("1"); // Solenoid2
        fields.emplace_back("0"); // Solenoid5
        fields.emplace_back("O"); // BallValve1
        fields.emplace_back("O"); // BallValve2
        fields.emplace_back("C"); // BallValve3
        fields.emplace_back("C"); // BallValve4
        fields.emplace_back(QByteArray::number(ch1.T));
        fields.emplace_back(QByteArray::number(ch2.T));
        fields.emplace_back(QByteArray::number(ch1.V1));
        fields.emplace_back("0"); // setCh1Voltage1
        fields.emplace_back(QByteArray::number(ch1.A1));
        fields.emplace_back(QByteArray::number(ch1.V2));
        fields.emplace_back("0"); // setCh1Voltage2
        fields.emplace_back(QByteArray::number(ch1.A2));
        fields.emplace_back(QByteArray::number(ch2.V1));
        fields.emplace_back("0"); // setCh2Voltage1
        fields.emplace_back(QByteArray::number(ch2.A1));
        fields.emplace_back(QByteArray::number(ch2.V2));
        fields.emplace_back("0"); // setCh2Voltage2
        fields.emplace_back(QByteArray::number(ch2.A2));
        fields.emplace_back("0"); // Ch1_SafetyTemp
        fields.emplace_back("0"); // Ch2_SafetyTemp
        fields.emplace_back("0"); // SafetyTempInt
        for (auto f : fans) {
            fields.emplace_back(QByteArray::number(f));
        }
        fields.emplace_back(QByteArray::number(licor.T));
        fields.emplace_back(QByteArray::number(licor.P / 10.0));
        fields.emplace_back(QByteArray::number(licor.CO2 * 1E-3));
        fields.emplace_back("0"); // LicorCO2abs
        fields.emplace_back("0"); // LicorH2O
        fields.emplace_back("0"); // LicorH2Oabs
        fields.emplace_back(QByteArray::number(licor.TD));
        fields.emplace_back(QByteArray::number(licor.V));

        bool first = true;
        for (const auto &f : fields) {
            if (!first) {
                if (useCSV)
                    outgoing += ",";
                else
                    outgoing += " ";
            }
            first = false;
            outgoing += f;
        }
        outgoing += "\r\n";
    }

    void resultResponse()
    {
        std::vector<QByteArray> fields;
        fields.emplace_back(QByteArray::number(resultID));
        fields.emplace_back(QByteArray::number(resultID));
        fields.emplace_back(convertTimestamp(time.addMSecs(static_cast<qint64>(resultInterval * -1000))));
        fields.emplace_back(convertTimestamp(time));
        fields.emplace_back(convertTimestamp(time.addMSecs(static_cast<qint64>(resultInterval * -1000))));
        fields.emplace_back(convertTimestamp(time));
        fields.emplace_back("0"); // TCcounts
        fields.emplace_back("0"); // TCmass
        fields.emplace_back(QByteArray::number(TC * 1E3));
        fields.emplace_back("0"); // AE33_BC6
        fields.emplace_back("0"); // AE33_ValidData
        fields.emplace_back("0"); // AE33_b
        fields.emplace_back("0"); // OC
        fields.emplace_back("0"); // EC
        fields.emplace_back(QByteArray::number(ambientCO2 * 1E-3));
        fields.emplace_back("0"); // Volume
        fields.emplace_back("0"); // Chamber
        fields.emplace_back("51"); // SetupID
        for (int i=0; i<14; i++) {
            fields.emplace_back("1.01E-3"); //[a-f][12]
        }

        bool first = true;
        for (const auto &f : fields) {
            if (!first) {
                if (useCSV)
                    outgoing += ",";
                else
                    outgoing += " ";
            }
            first = false;
            outgoing += f;
        }
        outgoing += "\r\n";
    }

    void advance(double seconds)
    {
        time = time.addMSecs(static_cast<qint64>(std::ceil(seconds * 1000.0)));

        dataRemaining -= seconds;
        while (dataRemaining < 0.0) {
            dataRemaining += dataInterval;
            dataID++;
            if (enableUnpolled)
                dataResponse();
        }

        resultRemaining -= seconds;
        while (resultRemaining < 0.0) {
            resultRemaining += resultInterval;
            resultID++;
            if (enableUnpolled)
                resultResponse();
        }

        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR).trimmed());

            incoming = incoming.mid(idxCR + 1);

            if (line == "$TCA:INFO") {
                outgoing += "Instrument name: Total Carbon Analyzer\r\n";
                outgoing += "Model number: TCA08\r\n";
                outgoing += "Serial Number: TCA-08-S00-00142\r\n";
                outgoing += "Firmware version: 311\r\n";
                outgoing += "Software version: 1.2.0.0\r\n";
                outgoing += "Current date and time: 4/16/2020 11:07:43 PM\r\n";
            } else if (line == "$TCA:LAST VerificationReport") {
                outgoing += "23 2/6/2020 4:52:12 PM 4 TC (ng) , Chamber\r\n";
                outgoing += "103 , 1\r\n";
                outgoing += "Zero verification successful.\r\n";
            } else if (line == "$TCA:LAST DatabaseVer") {
                if (garbleVersion < 0) {
                    garbleVersion++;
                } else if (garbleVersion > 0) {
                    garbleVersion--;
                    outgoing += "\r\nzz23asda\r";
                } else {
                    outgoing += "2 7/16/2019 10:46:04 AM 130\r\n";
                }
            } else if (line == "$TCA:LAST ExtDeviceData") {
                outgoing += "3230821 14676482 1 9.5 19 726.3\r\n";
            } else if (line == "$TCA:LAST ExtDeviceSetup") {
                outgoing += "172 14626610 1 4 0 0 0 0\r\n";
            } else if (line == "$TCA:LAST FilterIntegrity") {
                outgoing += "5271 5507 1 72.203 71.609 0.594\r\n";
            } else if (line == "$TCA:LAST HeaterResistance") {
                outgoing += "129 3/20/2020 3:19:47 PM 4970 0.298604685867781 0.29884442683571 0.299874334713188 0.299581759309228 86 86 77 86 8677\r\n";
            } else if (line == "$TCA:LAST Setup") {
                outgoing += "51 3/5/2020 5:18:24 PM TCA-08-S00-00142 16.7 0.5 4.91 4.99499615460112E-05 -0.0112575339247845 3.8925341983067 9.80863011674307E-07 -7.38879858215614E-08 -0.000199000664989231 14.5200004577637 14.3800001144409 1.1979 -5.7319 1.3047 -8.7509 60 300 3 12 57 3 495 3 12 57 3 180 245 245 200 245 245 200 100 50 100 6 5.8 71.65 1.2.0.0 311 1 Taipei Standard Time 1 1 0 0.45\r\n";
            } else if (line == "$TCA:LAST Data") {
                dataResponse();
            } else if (line == "$TCA:LAST OnlineResult") {
                resultResponse();
            } else if (line == "$TCA:END") {
                enableUnpolled = false;
            }
        }
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("X1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("X2", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("Q1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("Q2", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T3", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("TD3", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("P", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("ZPARAMETERS", Variant::Root(), {}, time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZSTATE", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("ZX1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("ZPUMP1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("ZPUMP2", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("V1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("V2", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("V3", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("V4", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("V5", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("A1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("A2", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("A3", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("A4", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("ZFAN1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("ZFAN2", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("ZFAN3", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("ZFAN4", Variant::Root(), {}, time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("X1"))
            return false;
        if (!stream.checkContiguous("X2"))
            return false;
        if (!stream.checkContiguous("Q1"))
            return false;
        if (!stream.checkContiguous("Q2"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("T3"))
            return false;
        if (!stream.checkContiguous("TD3"))
            return false;
        if (!stream.checkContiguous("P"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("X1", Variant::Root(model.TC), time))
            return false;
        if (!stream.hasAnyMatchingValue("X2", Variant::Root(model.ambientCO2), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q1", Variant::Root(model.flowS), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q2", Variant::Root(model.flowA), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.ch1.T), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.ch2.T), time))
            return false;
        if (!stream.hasAnyMatchingValue("T3", Variant::Root(model.licor.T), time))
            return false;
        if (!stream.hasAnyMatchingValue("TD3", Variant::Root(model.licor.TD), time))
            return false;
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.licor.P), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("ZX1", Variant::Root(model.licor.CO2), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZPUMP1", Variant::Root(model.samplePumpSpeed), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZPUMP2", Variant::Root(model.analyticPumpSpeed), time))
            return false;
        if (!stream.hasAnyMatchingValue("V1", Variant::Root(model.ch1.V1), time))
            return false;
        if (!stream.hasAnyMatchingValue("V2", Variant::Root(model.ch1.V2), time))
            return false;
        if (!stream.hasAnyMatchingValue("V3", Variant::Root(model.ch2.V1), time))
            return false;
        if (!stream.hasAnyMatchingValue("V4", Variant::Root(model.ch2.V2), time))
            return false;
        if (!stream.hasAnyMatchingValue("V5", Variant::Root(model.licor.V), time))
            return false;
        if (!stream.hasAnyMatchingValue("A1", Variant::Root(model.ch1.A1), time))
            return false;
        if (!stream.hasAnyMatchingValue("A2", Variant::Root(model.ch1.A2), time))
            return false;
        if (!stream.hasAnyMatchingValue("A3", Variant::Root(model.ch2.A1), time))
            return false;
        if (!stream.hasAnyMatchingValue("A4", Variant::Root(model.ch2.A2), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZFAN1", Variant::Root(model.fans[0]), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZFAN2", Variant::Root(model.fans[1]), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZFAN3", Variant::Root(model.fans[2]), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZFAN4", Variant::Root(model.fans[3]), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_magee_tca08"));
        QVERIFY(component);

        QLocale::setDefault(QLocale::C);
        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["ReportInterval"] = 60.0;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.enableUnpolled = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.25);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 300; i++) {
            control.advance(0.5);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["ReportInterval"] = 60.0;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 300; i++) {
            control.advance(0.5);
            QTest::qSleep(10);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveAutoprobeGarble()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["ReportInterval"] = 60.0;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.garbleVersion = 2;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 300; i++) {
            control.advance(0.5);
            QTest::qSleep(10);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.enableUnpolled = true;
        instrument.useCSV = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));
        control.advance(0.1);

        for (int i = 0; i < 300; i++) {
            control.advance(0.5);
            QTest::qSleep(10);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.enableUnpolled = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));

        for (int i = 0; i < 300; i++) {
            control.advance(0.5);
            QTest::qSleep(10);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

};

QTEST_MAIN(TestComponent)

#include "test.moc"
