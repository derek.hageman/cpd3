/*
 * Copyright (c) 2020 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "core/csv.hxx"
#include "core/timeutils.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_magee_tca08.hxx"


using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

static const std::array<std::string, 8> g0FlagTranslation{"", /* Online measurement */
                                                          "", /* Offline measurement */
                                                          "InstrumentCalibration",
                                                          "InstrumentVerification", "ChangeQuartz",
                                                          "Initializing", "SafetyStop",
                                                          "CriticalStop",};
static const std::array<std::string, 8> g3FlagTranslation
        {"Chamber1VoltageTimeout", "Chamber1Leakage", "Chamber1FilterIntegrityFailed",
         "Chamber1HeaterError", "Chamber1OverCurrent", "Chamber1VoltageSetError",
         "Chamber1TemperatureDisconnected", "Chamber1BallValveError",};
static const std::array<std::string, 8> g4FlagTranslation
        {"Chamber2VoltageTimeout", "Chamber2Leakage", "Chamber2FilterIntegrityFailed",
         "Chamber2HeaterError", "Chamber2OverCurrent", "Chamber2VoltageSetError",
         "Chamber2TemperatureDisconnected", "Chamber2BallValveError",};
static const std::array<std::string, 8> g5FlagTranslation{"DoorOpen", "", /* Analytical flow */
                                                          "", /* Sample flow */
                                                          "", /* Cooling fan */
                                                          "CO2Error", "", "", "",};
static const std::array<std::string, 8> g6FlagTranslation{"", /* Network detected */
                                                          "DatabaseError", "SetupError",
                                                          "ExternalDeviceError", "StorageError",
                                                          "InternalCommunicationError", "", "",};


AcquireMageeTCA08::Configuration::Configuration() : start(FP::undefined()),
                                                    end(FP::undefined()),
                                                    strictMode(true),
                                                    pollInterval(0.5),
                                                    reportInterval(3600.0)
{ }

AcquireMageeTCA08::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          strictMode(other.strictMode),
          pollInterval(other.pollInterval),
          reportInterval(other.reportInterval)
{ }

void AcquireMageeTCA08::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Magee");
    instrumentMeta["Model"].setString("TCA08");
    instrumentParameters.write().remove();

    autoprobeValidRecords = 0;
    responseRetryCounter = 0;

    streamBeginTime.fill(FP::undefined());

    lastReportedFlags.clear();
    lastDataID = 0;
    lastDataTime = FP::undefined();
    lastResultID = 0;
    lastResultTime = FP::undefined();

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    haveEmittedParameters = false;
    realtimeStateUpdated = false;
}

AcquireMageeTCA08::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s), end(e), strictMode(true), pollInterval(0.5), reportInterval(3600.0)
{
    setFromSegment(other);
}

AcquireMageeTCA08::Configuration::Configuration(const Configuration &under,
                                                const ValueSegment &over,
                                                double s,
                                                double e) : start(s),
                                                            end(e),
                                                            strictMode(under.strictMode),
                                                            pollInterval(under.pollInterval),
                                                            reportInterval(under.reportInterval)
{
    setFromSegment(over);
}

void AcquireMageeTCA08::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    if (config["PollInterval"].exists())
        pollInterval = config["PollInterval"].toDouble();

    {
        double v = config["ReportInterval"].toReal();
        if (FP::defined(v) && v > 0.0)
            reportInterval = v;
    }
}

AcquireMageeTCA08::AcquireMageeTCA08(const ValueSegment::Transfer &configData,
                                     const std::string &loggingContext) : FramedInstrument("tca08",
                                                                                           loggingContext),
                                                                          autoprobeStatus(
                                                                                  AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                          autoprobeValidRecords(0),
                                                                          responseState(
                                                                                  ResponseState::Passive_Initialize),
                                                                          loggingMux(
                                                                                  static_cast<std::size_t>(LogStream::TOTAL))
{
    setDefaultInvalid();
    config.emplace_back();

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireMageeTCA08::setToAutoprobe()
{
    responseState = ResponseState::Autoprobe_Passive_Initialize;
}

void AcquireMageeTCA08::setToInteractive()
{ responseState = ResponseState::Interactive_Initialize; }


ComponentOptions AcquireMageeTCA08Component::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireMageeTCA08::AcquireMageeTCA08(const ComponentOptions &, const std::string &loggingContext)
        : FramedInstrument("tca08", loggingContext),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          autoprobeValidRecords(0),
          responseState(ResponseState::Passive_Initialize),
          loggingMux(static_cast<std::size_t>(LogStream::TOTAL))
{
    setDefaultInvalid();
    config.emplace_back();
}

AcquireMageeTCA08::~AcquireMageeTCA08() = default;

void AcquireMageeTCA08::invalidateLogValues(double frameTime)
{
    autoprobeValidRecords = 0;

    lastReportedFlags.clear();
    lastDataID = 0;
    lastDataTime = FP::undefined();
    lastResultID = 0;
    lastResultTime = FP::undefined();

    for (std::size_t i = 0; i < static_cast<std::size_t>(LogStream::TOTAL); i++) {
        if (loggingEgress)
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    loggingMux.clear();
    streamBeginTime.fill(frameTime);

    loggingLost(frameTime);
}

SequenceValue::Transfer AcquireMageeTCA08::buildLogMeta(double time) const
{
    Q_ASSERT(!config.empty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_magee_tca08");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    if (instrumentMeta["SoftwareVersion"].exists())
        processing["SoftwareVersion"].set(instrumentMeta["SoftwareVersion"]);
    if (instrumentMeta["SerialNumber"].exists())
        processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);

    result.emplace_back(SequenceName({}, "raw_meta", "X1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
    result.back().write().metadataReal("Format").setString("0000.000");
    result.back().write().metadataReal("Description").setString("Total carbon concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample TC"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Persistent").setBoolean(true);

    result.emplace_back(SequenceName({}, "raw_meta", "X2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("ppb");
    result.back().write().metadataReal("Format").setString("00000000");
    result.back().write().metadataReal("Description").setString("Ambient CO₂ concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Ambient CO₂"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Persistent").setBoolean(true);

    result.emplace_back(SequenceName({}, "raw_meta", "Q1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Flows"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(0);

    result.emplace_back(SequenceName({}, "raw_meta", "Q2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Description").setString("Analytic flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Analytic"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Flows"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(1);

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Description").setString("Chamber 1 temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Temperature"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("2");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Chamber 1"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(0);

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Description").setString("Chamber 2 temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Temperature"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("3");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Chamber 2"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(3);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(0);

    result.emplace_back(SequenceName({}, "raw_meta", "T3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Description").setString("LI-COR temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Temperature"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("4");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("LI-COR"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(4);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(0);

    result.emplace_back(SequenceName({}, "raw_meta", "TD3"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Description").setString("LI-COR dewpoint");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Dewpoint"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("4");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("LI-COR"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(4);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(1);

    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Description").setString("LI-COR pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Pressure"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("4");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("LI-COR"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(4);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(2);

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("InstrumentCalibration")
          .hash("Description")
          .setString("Instrument calibration active");
    result.back()
          .write()
          .metadataSingleFlag("InstrumentCalibration")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("InstrumentVerification")
          .hash("Description")
          .setString("Instrument verification active");
    result.back()
          .write()
          .metadataSingleFlag("InstrumentVerification")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("ChangeQuartz")
          .hash("Description")
          .setString("Change quartz medium");
    result.back()
          .write()
          .metadataSingleFlag("ChangeQuartz")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("Initializing")
          .hash("Description")
          .setString("Instrument initializing");
    result.back()
          .write()
          .metadataSingleFlag("Initializing")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("SafetyStop")
          .hash("Description")
          .setString("Instrument in safety stop mode");
    result.back()
          .write()
          .metadataSingleFlag("SafetyStop")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("CriticalStop")
          .hash("Description")
          .setString("Instrument in critical stop mode");
    result.back()
          .write()
          .metadataSingleFlag("CriticalStop")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("Chamber1VoltageTimeout")
          .hash("Description")
          .setString("Chamber 1 voltage driver on for too long");
    result.back()
          .write()
          .metadataSingleFlag("Chamber1VoltageTimeout")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("Chamber1Leakage")
          .hash("Description")
          .setString("Chamber 1 leakage detected");
    result.back()
          .write()
          .metadataSingleFlag("Chamber1Leakage")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("Chamber1FilterIntegrityFailed")
          .hash("Description")
          .setString("Chamber 1 filter integrity failed");
    result.back()
          .write()
          .metadataSingleFlag("Chamber1FilterIntegrityFailed")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("Chamber1HeaterError")
          .hash("Description")
          .setString("Chamber 1 heater disconnected");
    result.back()
          .write()
          .metadataSingleFlag("Chamber1HeaterError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("Chamber1OverCurrent")
          .hash("Description")
          .setString("Chamber 1 heater current too high");
    result.back()
          .write()
          .metadataSingleFlag("Chamber1OverCurrent")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("Chamber1VoltageSetError")
          .hash("Description")
          .setString("Chamber 1 target voltage too high");
    result.back()
          .write()
          .metadataSingleFlag("Chamber1VoltageSetError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("Chamber1TemperatureDisconnected")
          .hash("Description")
          .setString("Chamber 1 temperature sensor disconnected");
    result.back()
          .write()
          .metadataSingleFlag("Chamber1TemperatureDisconnected")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("Chamber1BallValveError")
          .hash("Description")
          .setString("Chamber 1 ball valve error");
    result.back()
          .write()
          .metadataSingleFlag("Chamber1BallValveError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("Chamber2VoltageTimeout")
          .hash("Description")
          .setString("Chamber 2 voltage driver on for too long");
    result.back()
          .write()
          .metadataSingleFlag("Chamber2VoltageTimeout")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("Chamber2Leakage")
          .hash("Description")
          .setString("Chamber 2 leakage detected");
    result.back()
          .write()
          .metadataSingleFlag("Chamber2Leakage")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("Chamber2FilterIntegrityFailed")
          .hash("Description")
          .setString("Chamber 2 filter integrity failed");
    result.back()
          .write()
          .metadataSingleFlag("Chamber2FilterIntegrityFailed")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("Chamber2HeaterError")
          .hash("Description")
          .setString("Chamber 2 heater disconnected");
    result.back()
          .write()
          .metadataSingleFlag("Chamber2HeaterError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("Chamber2OverCurrent")
          .hash("Description")
          .setString("Chamber 2 heater current too high");
    result.back()
          .write()
          .metadataSingleFlag("Chamber2OverCurrent")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("Chamber2VoltageSetError")
          .hash("Description")
          .setString("Chamber 2 target voltage too high");
    result.back()
          .write()
          .metadataSingleFlag("Chamber2VoltageSetError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("Chamber2TemperatureDisconnected")
          .hash("Description")
          .setString("Chamber 2 temperature sensor disconnected");
    result.back()
          .write()
          .metadataSingleFlag("Chamber2TemperatureDisconnected")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("Chamber2BallValveError")
          .hash("Description")
          .setString("Chamber 2 ball valve error");
    result.back()
          .write()
          .metadataSingleFlag("Chamber2BallValveError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("DoorOpen")
          .hash("Description")
          .setString("Instrument door open");
    result.back()
          .write()
          .metadataSingleFlag("DoorOpen")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("CO2Error")
          .hash("Description")
          .setString("CO2 sensor error");
    result.back()
          .write()
          .metadataSingleFlag("CO2Error")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("DatabaseError")
          .hash("Description")
          .setString("Database error");
    result.back()
          .write()
          .metadataSingleFlag("DatabaseError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("SetupError")
          .hash("Description")
          .setString("Setup error");
    result.back()
          .write()
          .metadataSingleFlag("SetupError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("ExternalDeviceError")
          .hash("Description")
          .setString("External device error");
    result.back()
          .write()
          .metadataSingleFlag("ExternalDeviceError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("StorageError")
          .hash("Description")
          .setString("Problem accessing instrument internal storage");
    result.back()
          .write()
          .metadataSingleFlag("StorageError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.back()
          .write()
          .metadataSingleFlag("InternalCommunicationError")
          .hash("Description")
          .setString("Internal instrument communication error");
    result.back()
          .write()
          .metadataSingleFlag("InternalCommunicationError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_tca08");

    result.emplace_back(SequenceName({}, "raw_meta", "ZPARAMETERS"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("Instrument parameters");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataHashChild("DatabaseVersion")
          .metadataInteger("Description")
          .setString("Database version number");
    result.back()
          .write()
          .metadataHashChild("FilterIntegrity")
          .metadataString("Description")
          .setString("Filter integrity response");
    result.back()
          .write()
          .metadataHashChild("HeaterResistance")
          .metadataString("Description")
          .setString("Heater resistance response");
    result.back()
          .write()
          .metadataHashChild("Setup")
          .metadataString("Description")
          .setString("Latest instrument setup response");
    result.back()
          .write()
          .metadataHashChild("InfoTime")
          .metadataReal("Description")
          .setString("Time of info response");
    result.back()
          .write()
          .metadataHashChild("Info")
          .metadataArray("Description")
          .setString("Info response lines");
    result.back()
          .write()
          .metadataHashChild("Info")
          .metadataArray("Children")
          .metadataString("Info response line");
    result.back()
          .write()
          .metadataHashChild("VerificationReport")
          .metadataArray("Description")
          .setString("Verification report lines");
    result.back()
          .write()
          .metadataHashChild("VerificationReport")
          .metadataArray("Children")
          .metadataString("Verification report line");

    return result;
}

SequenceValue::Transfer AcquireMageeTCA08::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_magee_tca08");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    if (instrumentMeta["SoftwareVersion"].exists())
        processing["SoftwareVersion"].set(instrumentMeta["SoftwareVersion"]);
    if (instrumentMeta["SerialNumber"].exists())
        processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);


    result.emplace_back(SequenceName({}, "raw_meta", "ZX1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Units").setString("ppb");
    result.back().write().metadataReal("Format").setString("00000000");
    result.back().write().metadataReal("Description").setString("Current CO₂ concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Measurement CO₂"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(1);

    result.emplace_back(SequenceName({}, "raw_meta", "ZPUMP1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000");
    result.back().write().metadataReal("Description").setString("Sample pump speed");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Sample speed"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Flows"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(0);

    result.emplace_back(SequenceName({}, "raw_meta", "ZPUMP2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000");
    result.back().write().metadataReal("Description").setString("Analytic pump speed");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Analytic speed"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Flows"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(1);

    result.emplace_back(SequenceName({}, "raw_meta", "V1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Description").setString("Chamber 1 heater 1 voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Heater 1"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("2");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Chamber 1"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(1);

    result.emplace_back(SequenceName({}, "raw_meta", "V2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Description").setString("Chamber 1 heater 2 voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Heater 2"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("2");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Chamber 1"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(2);

    result.emplace_back(SequenceName({}, "raw_meta", "A1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("A");
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Description").setString("Chamber 1 heater 1 current");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Heater 1"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("2");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Chamber 1"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(1);

    result.emplace_back(SequenceName({}, "raw_meta", "A2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("A");
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Description").setString("Chamber 1 heater 2 current");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Heater 2"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("2");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Chamber 1"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(2);

    result.emplace_back(SequenceName({}, "raw_meta", "V3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Description").setString("Chamber 2 heater 1 voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Heater 1"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("3");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Chamber 2"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(3);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(1);

    result.emplace_back(SequenceName({}, "raw_meta", "V4"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Description").setString("Chamber 2 heater 2 voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Heater 2"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("3");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Chamber 2"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(3);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(2);

    result.emplace_back(SequenceName({}, "raw_meta", "A3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("A");
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Description").setString("Chamber 2 heater 1 current");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Heater 1"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("3");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Chamber 2"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(3);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(1);

    result.emplace_back(SequenceName({}, "raw_meta", "A4"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("A");
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Description").setString("Chamber 2 heater 2 current");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Heater 2"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("3");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Chamber 2"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(3);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(2);

    result.emplace_back(SequenceName({}, "raw_meta", "V5"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Description").setString("LI-COR voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Voltage"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("4");
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("LI-COR"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(4);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(3);

    result.emplace_back(SequenceName({}, "raw_meta", "ZFAN1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back().write().metadataReal("Description").setString("Fan 1");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Fan 1"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(5);

    result.emplace_back(SequenceName({}, "raw_meta", "ZFAN2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back().write().metadataReal("Description").setString("Fan 2");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Fan 2"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(5);

    result.emplace_back(SequenceName({}, "raw_meta", "ZFAN3"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back().write().metadataReal("Description").setString("Fan 3");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Fan 3"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(5);

    result.emplace_back(SequenceName({}, "raw_meta", "ZFAN4"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back().write().metadataReal("Description").setString("Fan 4");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Fan 4"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(5);


    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Name").setString(std::string());
    result.back().write().metadataString("Realtime").hash("Units").setString(std::string());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInteger(5);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidRecord")
          .setString(QObject::tr("NO COMMS: Invalid record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadDatabaseVer")
          .setString(QObject::tr("STARTING COMMS: Reading database version"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadFilterIntegrity")
          .setString(QObject::tr("STARTING COMMS: Reading filter integrity report"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadHeaterResistance")
          .setString(QObject::tr("STARTING COMMS: Reading heater resistance report"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadSetup")
          .setString(QObject::tr("STARTING COMMS: Reading setup"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadInfo")
          .setString(QObject::tr("STARTING COMMS: Reading information report"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadVerificationReport")
          .setString(QObject::tr("STARTING COMMS: Reading verification report"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadOnlineResult")
          .setString(QObject::tr("STARTING COMMS: Reading latest result"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadData")
          .setString(QObject::tr("STARTING COMMS: Reading data status"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InstrumentCalibration")
          .setString(QObject::tr("Calibration active"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InstrumentVerification")
          .setString(QObject::tr("Verification active"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("ChangeQuartz")
          .setString(QObject::tr("Change quartz medium"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Initializing")
          .setString(QObject::tr("Instrument initializing"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SafetyStop")
          .setString(QObject::tr("SAFETY STOP"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("CriticalStop")
          .setString(QObject::tr("CRITICAL STOP"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Chamber1VoltageTimeout")
          .setString(QObject::tr("Chamber 1 Voltage Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Chamber1Leakage")
          .setString(QObject::tr("Chamber 1 Leakage"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Chamber1FilterIntegrityFailed")
          .setString(QObject::tr("Chamber 1 Integrity Failed"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Chamber1HeaterError")
          .setString(QObject::tr("Chamber 1 Heater Error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Chamber1OverCurrent")
          .setString(QObject::tr("Chamber 1 Over Current"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Chamber1VoltageSetError")
          .setString(QObject::tr("Chamber 1 Voltage Set Error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Chamber1TemperatureDisconnected")
          .setString(QObject::tr("Chamber 1 Temperature Sensor Disconnected"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Chamber1BallValveError")
          .setString(QObject::tr("Chamber 1 Ball Valve Error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Chamber2VoltageTimeout")
          .setString(QObject::tr("Chamber 2 Voltage Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Chamber2Leakage")
          .setString(QObject::tr("Chamber 2 Leakage"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Chamber2FilterIntegrityFailed")
          .setString(QObject::tr("Chamber 2 Integrity Failed"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Chamber2HeaterError")
          .setString(QObject::tr("Chamber 2 Heater Error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Chamber2OverCurrent")
          .setString(QObject::tr("Chamber 2 Over Current"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Chamber2VoltageSetError")
          .setString(QObject::tr("Chamber 2 Voltage Set Error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Chamber2TemperatureDisconnected")
          .setString(QObject::tr("Chamber 2 Temperature Sensor Disconnected"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Chamber2BallValveError")
          .setString(QObject::tr("Chamber 2 Ball Valve Error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("DoorOpen")
          .setString(QObject::tr("Door Open"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("CO2Error")
          .setString(QObject::tr("CO2 Error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("DatabaseError")
          .setString(QObject::tr("Database Error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SetupError")
          .setString(QObject::tr("Setup Error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("ExternalDeviceError")
          .setString(QObject::tr("External Device Error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StorageError")
          .setString(QObject::tr("Internal Storage Error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InternalCommunicationError")
          .setString(QObject::tr("Internal Communications Error"));

    return result;
}

SequenceMatch::Composite AcquireMageeTCA08::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "X1");
    sel.append({}, {}, "X2");
    sel.append({}, {}, "ZSTATE");
    sel.append({}, {}, "ZPARAMETERS");
    return sel;
}

void AcquireMageeTCA08::loggingValue(LogStream stream,
                                     double time,
                                     SequenceName::Component name,
                                     Variant::Root value)
{
    if (!loggingEgress) {
        if (!realtimeEgress)
            return;
        realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                    time, time + 1.0);
        return;
    }

    if (realtimeEgress) {
        realtimeEgress->emplaceData(SequenceIdentity({}, "raw", name), value, time, time + 1.0);
    }

    auto index = static_cast<std::size_t>(stream);
    auto streamTime = streamBeginTime[index];
    if (!FP::defined(streamTime))
        return;
    if (streamTime >= time)
        return;

    loggingMux.incoming(index, SequenceValue(SequenceIdentity({}, "raw", std::move(name)),
                                             std::move(value), streamTime, time), loggingEgress);
}

void AcquireMageeTCA08::advanceStream(LogStream stream, double minimumTime)
{
    auto index = static_cast<std::size_t>(stream);
    auto &streamTime = streamBeginTime[index];
    if (minimumTime < streamTime)
        minimumTime = streamTime;

    loggingMux.advance(index, minimumTime, loggingEgress);
    streamTime = minimumTime;
}

void AcquireMageeTCA08::realtimeValue(double time,
                                      SequenceName::Component name,
                                      Variant::Root value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

template<std::size_t N>
static void toFlags(const Variant::Root &input,
                    const std::array<std::string, N> &possible,
                    Variant::Flags &output)
{
    auto bits = input.read().toInteger();
    std::int_fast64_t bit = 1;
    for (const auto &flag : possible) {
        if (bits & bit) {
            if (!flag.empty()) {
                output.insert(flag);
            }
        }
        bit <<= 1;
    }
}

int AcquireMageeTCA08::parseTime(std::deque<Util::ByteView> &fields, double &outputTime)
{
    if (fields.empty()) return 1;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    Util::ByteView dateField;
    Util::ByteView timeField;
    Util::ByteArray temp;
    auto spacefields = Util::as_deque(field.split(' '));
    if (spacefields.size() > 1) {
        dateField = spacefields.front();
        spacefields.pop_front();
        for (const auto &add : spacefields) {
            if (!temp.empty())
                temp.push_back(' ');
            temp += add;
        }
        timeField = temp;
    } else {
        dateField = field;
        if (fields.empty()) return 2;
        timeField = fields.front().string_trimmed();
        fields.pop_front();
        if (!fields.empty()) {
            if (fields.front() == "AM" || fields.front() == "PM") {
                temp = timeField;
                temp.push_back(' ');
                temp += fields.front();
                fields.pop_front();
                timeField = temp;
            }
        }
    }


    auto subfields = Util::as_deque(dateField.split('/'));
    if (subfields.size() != 3) return 3;

    Q_ASSERT(!subfields.empty());
    bool ok = false;
    std::int_fast64_t imonth = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 4;
    if (imonth < 1) return 5;
    if (imonth > 12) return 6;
    Variant::Root month(imonth);
    remap("MONTH", month);
    imonth = month.read().toInteger();

    Q_ASSERT(!subfields.empty());
    std::int_fast64_t iday = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 7;
    if (iday < 1) return 8;
    if (iday > 31) return 9;
    Variant::Root day(iday);
    remap("DAY", day);
    iday = day.read().toInteger();

    Q_ASSERT(!subfields.empty());
    std::int_fast64_t iyear = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 10;
    if (iyear < 1900) {
        if (iyear > 99) return 11;
        if (iyear < 0) return 12;
        iyear += 2000;
    }
    if (iyear > 2999) return 13;
    Variant::Root year(iyear);
    remap("YEAR", year);
    iyear = year.read().toInteger();


    subfields = Util::as_deque(timeField.split(':'));
    if (subfields.size() != 3) return 14;

    Q_ASSERT(!subfields.empty());
    std::int_fast64_t ihour = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 15;
    if (ihour < 0) return 16;
    if (ihour > 23) return 17;

    Q_ASSERT(!subfields.empty());
    std::int_fast64_t iminute = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 18;
    if (iminute < 0) return 19;
    if (iminute > 59) return 20;
    Variant::Root minute(iminute);
    remap("MINUTE", minute);
    iminute = minute.read().toInteger();

    Q_ASSERT(!subfields.empty());
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    if (field.string_end("AM")) {
        field = field.mid(0, field.size() - 2);
        if (ihour == 12)
            ihour = 0;
    } else if (field.string_end("PM")) {
        field = field.mid(0, field.size() - 2);
        if (ihour != 12)
            ihour += 12;
    }
    Variant::Root hour(ihour);
    remap("HOUR", hour);
    ihour = hour.read().toInteger();

    field = field.string_trimmed();
    while (field.size() > 1 && field.string_start("0")) {
        field = field.mid(1);
    }
    double fseconds = field.parse_real(&ok);
    if (!ok) return 21;
    if (fseconds < 0.0) return 22;
    if (fseconds > 60.0) return 23;
    Variant::Root second(fseconds);
    remap("SECOND", second);
    fseconds = second.read().toReal();

    if (INTEGER::defined(iyear) &&
            iyear >= 1900 &&
            iyear <= 2999 &&
            INTEGER::defined(imonth) &&
            imonth >= 1 &&
            imonth <= 12 &&
            INTEGER::defined(iday) &&
            iday >= 1 &&
            iday <= 31 &&
            INTEGER::defined(ihour) &&
            ihour >= 0 &&
            ihour <= 23 &&
            INTEGER::defined(iminute) &&
            iminute >= 0 &&
            iminute <= 59 &&
            FP::defined(fseconds) &&
            fseconds >= 0.0 &&
            fseconds <= 60.0) {
        auto baseSeconds = std::floor(fseconds);
        QDateTime dt
                (QDate(static_cast<int>(iyear), static_cast<int>(imonth), static_cast<int>(iday)),
                 QTime(static_cast<int>(ihour), static_cast<int>(iminute),
                       static_cast<int>(baseSeconds)), Qt::UTC);
        dt = dt.addMSecs(static_cast<int>((fseconds - baseSeconds) * 1000.0));
        outputTime = Time::fromDateTime(dt, true);
    }

    return 0;
}

void AcquireMageeTCA08::emitMetadata(double frameTime)
{
    if (!haveEmittedLogMeta && loggingEgress) {
        haveEmittedLogMeta = true;

        auto metaTime = frameTime;
        auto &streamTime = streamBeginTime[static_cast<std::size_t>(LogStream::Metadata)];
        if (FP::defined(streamTime) && streamTime > metaTime)
            metaTime = streamTime;

        loggingMux.incoming(static_cast<std::size_t>(LogStream::Metadata), buildLogMeta(metaTime),
                            loggingEgress);
        streamTime = metaTime;
    }

    if (!haveEmittedRealtimeMeta && realtimeEgress) {
        haveEmittedRealtimeMeta = true;

        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));

        if (instrumentParameters.read().exists()) {
            realtimeEgress->emplaceData(SequenceName({}, "raw", "ZPARAMETERS"),
                                        instrumentParameters, frameTime, FP::undefined());
        }
    }

    if (!haveEmittedParameters && persistentEgress && instrumentParameters.read().exists()) {
        haveEmittedParameters = true;
        persistentEgress->emplaceData(SequenceName({}, "raw", "ZPARAMETERS"), instrumentParameters,
                                      frameTime, FP::undefined());
    }
}

int AcquireMageeTCA08::processDataRecord(const Util::ByteView &line, double &frameTime)
{
    Q_ASSERT(!config.empty());

    if (line.size() < 5)
        return 1;

    auto fields = CSV::acquisitionSplit(line);
    bool ok = false;

    if (fields.empty()) return 2;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    auto recordID = field.parse_u64(&ok);
    if (!ok) return 3;

    double parsedTime = FP::undefined();
    int rc = parseTime(fields, parsedTime);
    if (rc > 0)
        return 100 + rc;
    if (!FP::defined(frameTime)) {
        frameTime = parsedTime;
        configurationAdvance(frameTime);
    }
    {
        auto lastTime = streamBeginTime[static_cast<std::size_t>(LogStream::Data)];
        if (FP::defined(lastTime) && FP::defined(frameTime) && frameTime < lastTime)
            return -1;
    }

    /* SetupID */
    if (fields.empty()) return 4;
    fields.pop_front();

    /* Timebase */
    if (fields.empty()) return 5;
    fields.pop_front();

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root G0(static_cast<std::int_fast64_t>(field.parse_u16(&ok)));
    if (!ok) return 6;
    if (!INTEGER::defined(G0.read().toInteger())) return 7;
    remap("G0RAW", G0);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root G1(static_cast<std::int_fast64_t>(field.parse_u16(&ok)));
    if (!ok) return 8;
    if (!INTEGER::defined(G1.read().toInteger())) return 9;
    remap("G1RAW", G1);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root G2(static_cast<std::int_fast64_t>(field.parse_u16(&ok)));
    if (!ok) return 10;
    if (!INTEGER::defined(G2.read().toInteger())) return 11;
    remap("G2RAW", G2);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root G3(static_cast<std::int_fast64_t>(field.parse_u16(&ok)));
    if (!ok) return 12;
    if (!INTEGER::defined(G3.read().toInteger())) return 13;
    remap("G3RAW", G3);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root G4(static_cast<std::int_fast64_t>(field.parse_u16(&ok)));
    if (!ok) return 14;
    if (!INTEGER::defined(G4.read().toInteger())) return 15;
    remap("G4RAW", G4);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root G5(static_cast<std::int_fast64_t>(field.parse_u16(&ok)));
    if (!ok) return 16;
    if (!INTEGER::defined(G5.read().toInteger())) return 17;
    remap("G5RAW", G5);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root G6(static_cast<std::int_fast64_t>(field.parse_u16(&ok)));
    if (!ok) return 18;
    if (!INTEGER::defined(G6.read().toInteger())) return 19;
    remap("G6RAW", G6);

    /* CH1_Status */
    if (fields.empty()) return 20;
    fields.pop_front();

    /* Status can be a multi-field string in space separated mode */
    while (!fields.empty()) {
        ok = false;
        fields.front().parse_i64(&ok);
        if (ok)
            break;
        fields.pop_front();
    }

    /* CH1_SampleID */
    if (fields.empty()) return 21;
    fields.pop_front();

    /* CH2_Status */
    if (fields.empty()) return 22;
    fields.pop_front();

    /* Status can be a multi-field string in space separated mode */
    while (!fields.empty()) {
        ok = false;
        fields.front().parse_i64(&ok);
        if (ok)
            break;
        fields.pop_front();
    }

    /* CH2_SampleID */
    if (fields.empty()) return 23;
    fields.pop_front();

    /* MainBoardStatus */
    if (fields.empty()) return 24;
    fields.pop_front();

    /* Ch1BoardStatus */
    if (fields.empty()) return 25;
    fields.pop_front();

    /* Ch2BoardStatus */
    if (fields.empty()) return 26;
    fields.pop_front();

    /* SensorBoardStatus */
    if (fields.empty()) return 27;
    fields.pop_front();

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Q1(field.parse_real(&ok));
    if (!ok) return 28;
    if (!FP::defined(Q1.read().toReal())) return 29;
    remap("Q1", Q1);

    /* setFlowS */
    if (fields.empty()) return 30;
    fields.pop_front();

    /* FlowS_RAW */
    if (fields.empty()) return 30;
    fields.pop_front();

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZPUMP1(field.parse_real(&ok));
    if (!ok) return 31;
    if (!FP::defined(ZPUMP1.read().toReal())) return 32;
    remap("ZPUMP1", ZPUMP1);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Q2(field.parse_real(&ok));
    if (!ok) return 33;
    if (!FP::defined(Q2.read().toReal())) return 34;
    remap("Q2", Q2);

    /* setFlowA */
    if (fields.empty()) return 35;
    fields.pop_front();

    /* FlowA_RAW */
    if (fields.empty()) return 36;
    fields.pop_front();

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZPUMP2(field.parse_real(&ok));
    if (!ok) return 37;
    if (!FP::defined(ZPUMP2.read().toReal())) return 38;
    remap("ZPUMP2", ZPUMP2);

    /* Solenoid1 */
    if (fields.empty()) return 39;
    fields.pop_front();

    /* Solenoid2 */
    if (fields.empty()) return 40;
    fields.pop_front();

    /* Solenoid5 */
    if (fields.empty()) return 41;
    fields.pop_front();

    /* BallValve1 */
    if (fields.empty()) return 42;
    fields.pop_front();

    /* BallValve2 */
    if (fields.empty()) return 43;
    fields.pop_front();

    /* BallValve3 */
    if (fields.empty()) return 44;
    fields.pop_front();

    /* BallValve4 */
    if (fields.empty()) return 45;
    fields.pop_front();

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T1(field.parse_real(&ok));
    if (!ok) return 46;
    if (!FP::defined(T1.read().toReal())) return 47;
    remap("T1", T1);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T2(field.parse_real(&ok));
    if (!ok) return 48;
    if (!FP::defined(T2.read().toReal())) return 49;
    remap("T2", T2);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root V1(field.parse_real(&ok));
    if (!ok) return 50;
    if (!FP::defined(T1.read().toReal())) return 51;
    remap("V1", V1);

    /* setCh1Voltage1 */
    if (fields.empty()) return 52;
    fields.pop_front();

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root A1(field.parse_real(&ok));
    if (!ok) return 53;
    if (!FP::defined(A1.read().toReal())) return 54;
    remap("A1", A1);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root V2(field.parse_real(&ok));
    if (!ok) return 55;
    if (!FP::defined(V2.read().toReal())) return 56;
    remap("V2", V2);

    /* setCh1Voltage2 */
    if (fields.empty()) return 57;
    fields.pop_front();

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root A2(field.parse_real(&ok));
    if (!ok) return 58;
    if (!FP::defined(A2.read().toReal())) return 59;
    remap("A2", A2);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root V3(field.parse_real(&ok));
    if (!ok) return 60;
    if (!FP::defined(V3.read().toReal())) return 61;
    remap("V3", V3);

    /* setCh2Voltage1 */
    if (fields.empty()) return 62;
    fields.pop_front();

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root A3(field.parse_real(&ok));
    if (!ok) return 63;
    if (!FP::defined(A3.read().toReal())) return 64;
    remap("A3", A3);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root V4(field.parse_real(&ok));
    if (!ok) return 65;
    if (!FP::defined(V4.read().toReal())) return 66;
    remap("V4", V4);

    /* setCh2Voltage2 */
    if (fields.empty()) return 67;
    fields.pop_front();

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root A4(field.parse_real(&ok));
    if (!ok) return 68;
    if (!FP::defined(A4.read().toReal())) return 69;
    remap("A4", A4);

    /* Ch1_SafetyTemp */
    if (fields.empty()) return 70;
    fields.pop_front();

    /* Ch2_SafetyTemp */
    if (fields.empty()) return 71;
    fields.pop_front();

    /* SafetyTempInt */
    if (fields.empty()) return 71;
    fields.pop_front();

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZFAN1(field.parse_real(&ok));
    if (!ok) return 72;
    if (!FP::defined(ZFAN1.read().toReal())) return 73;
    remap("ZFAN1", ZFAN1);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZFAN2(field.parse_real(&ok));
    if (!ok) return 74;
    if (!FP::defined(ZFAN2.read().toReal())) return 75;
    remap("ZFAN2", ZFAN2);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZFAN3(field.parse_real(&ok));
    if (!ok) return 76;
    if (!FP::defined(ZFAN3.read().toReal())) return 77;
    remap("ZFAN3", ZFAN3);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZFAN4(field.parse_real(&ok));
    if (!ok) return 78;
    if (!FP::defined(ZFAN4.read().toReal())) return 79;
    remap("ZFAN4", ZFAN4);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T3(field.parse_real(&ok));
    if (!ok) return 80;
    if (!FP::defined(T3.read().toReal())) return 81;
    remap("T3", T3);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root P(field.parse_real(&ok));
    if (!ok) return 83;
    if (!FP::defined(P.read().toReal())) return 84;
    P.write().setReal(P.read().toReal() * 10.0);
    remap("P", P);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZX1(field.parse_real(&ok));
    if (!ok) return 85;
    if (!FP::defined(ZX1.read().toReal())) return 86;
    ZX1.write().setReal(ZX1.read().toReal() * 1E3);
    remap("ZX1", ZX1);

    /* LicroCO2abs */
    if (fields.empty()) return 87;
    fields.pop_front();

    /* LicroH2O */
    if (fields.empty()) return 88;
    fields.pop_front();

    /* LicorH2OAbs */
    if (fields.empty()) return 89;
    fields.pop_front();

    if (fields.empty()) return 90;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root TD3(field.parse_real(&ok));
    if (!ok) return 91;
    if (!FP::defined(TD3.read().toReal())) return 92;
    remap("TD3", TD3);

    if (fields.empty()) return 93;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root V5(field.parse_real(&ok));
    if (!ok) return 94;
    if (!FP::defined(V5.read().toReal())) return 95;
    remap("V5", V5);

    if (!fields.empty()) {
        if (config.front().strictMode)
            return 999;
    }

    if (lastDataID == recordID &&
            FP::defined(lastDataTime) &&
            FP::defined(parsedTime) &&
            lastDataTime == parsedTime) {
        return -1;
    }
    lastDataID = recordID;
    lastDataTime = parsedTime;

    auto priorFlags = std::move(lastReportedFlags);
    lastReportedFlags.clear();
    toFlags(G0, g0FlagTranslation, lastReportedFlags);
    //toFlags(G1, g1FlagTranslation, flags);
    //toFlags(G2, g2FlagTranslation, flags);
    toFlags(G3, g3FlagTranslation, lastReportedFlags);
    toFlags(G4, g4FlagTranslation, lastReportedFlags);
    toFlags(G5, g5FlagTranslation, lastReportedFlags);
    toFlags(G6, g6FlagTranslation, lastReportedFlags);

    if (lastReportedFlags != priorFlags)
        realtimeStateUpdated = true;

    emitMetadata(frameTime);

    loggingValue(LogStream::Data, frameTime, "F1", Variant::Root(lastReportedFlags));
    loggingValue(LogStream::Data, frameTime, "Q1", std::move(Q1));
    loggingValue(LogStream::Data, frameTime, "Q2", std::move(Q2));
    loggingValue(LogStream::Data, frameTime, "T1", std::move(T1));
    loggingValue(LogStream::Data, frameTime, "T2", std::move(T2));
    loggingValue(LogStream::Data, frameTime, "T3", std::move(T3));
    loggingValue(LogStream::Data, frameTime, "P", std::move(P));
    loggingValue(LogStream::Data, frameTime, "TD3", std::move(TD3));

    realtimeValue(frameTime, "ZPUMP1", std::move(ZPUMP1));
    realtimeValue(frameTime, "ZPUMP2", std::move(ZPUMP2));
    realtimeValue(frameTime, "V1", std::move(V1));
    realtimeValue(frameTime, "V2", std::move(V2));
    realtimeValue(frameTime, "V3", std::move(V3));
    realtimeValue(frameTime, "V4", std::move(V4));
    realtimeValue(frameTime, "V5", std::move(V5));
    realtimeValue(frameTime, "A1", std::move(A1));
    realtimeValue(frameTime, "A2", std::move(A2));
    realtimeValue(frameTime, "A3", std::move(A3));
    realtimeValue(frameTime, "A4", std::move(A4));
    realtimeValue(frameTime, "ZFAN1", std::move(ZFAN1));
    realtimeValue(frameTime, "ZFAN2", std::move(ZFAN2));
    realtimeValue(frameTime, "ZFAN3", std::move(ZFAN3));
    realtimeValue(frameTime, "ZFAN4", std::move(ZFAN4));
    realtimeValue(frameTime, "ZX1", std::move(ZX1));

    if (realtimeStateUpdated && realtimeEgress && FP::defined(frameTime)) {
        realtimeStateUpdated = false;

        if (!lastReportedFlags.empty()) {
            if (lastReportedFlags.size() == 1) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        *lastReportedFlags.begin()), frameTime, FP::undefined()));
            } else {
                QStringList sorted;
                for (const auto &add : lastReportedFlags) {
                    sorted.append(QString::fromStdString(add));
                }
                std::sort(sorted.begin(), sorted.end());
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        QString("Status: %1").arg(sorted.join(','))), frameTime, FP::undefined()));
            }
        } else {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                  FP::undefined()));
        }
    }

    advanceStream(LogStream::Data, frameTime);
    advanceStream(LogStream::Metadata, frameTime);
    advanceStream(LogStream::OnlineResult, frameTime - config.front().reportInterval * 1.5);

    return 0;
}

int AcquireMageeTCA08::processOnlineResultRecord(const Util::ByteView &line, double &frameTime)
{
    Q_ASSERT(!config.empty());

    if (line.size() < 5)
        return 1;

    auto fields = CSV::acquisitionSplit(line);
    bool ok = false;

    if (fields.empty()) return 2;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    auto recordID = field.parse_u64(&ok);
    if (!ok) return 3;

    /* SampleID */
    if (fields.empty()) return 4;
    field = fields.front().string_trimmed();
    fields.pop_front();
    field.parse_u64(&ok);
    if (!ok) return 5;

    double startTime = FP::undefined();
    int rc = parseTime(fields, startTime);
    if (rc > 0)
        return 100 + rc;

    if (FP::defined(frameTime)) {
        startTime = frameTime - config.front().reportInterval;
    }

    double endTime = FP::undefined();
    rc = parseTime(fields, endTime);
    if (rc > 0)
        return 200 + rc;
    if (!FP::defined(frameTime)) {
        frameTime = endTime;
        configurationAdvance(frameTime);
    }
    {
        auto lastTime = streamBeginTime[static_cast<std::size_t>(LogStream::Data)];
        if (FP::defined(lastTime) && FP::defined(frameTime) && frameTime < lastTime)
            return -1;
        if (FP::defined(lastTime))
            startTime = lastTime;
    }

    /* Start time (local) */
    {
        double discard = FP::undefined();
        rc = parseTime(fields, discard);
        if (rc > 0)
            return 300 + rc;
    }

    /* End time (local) */
    {
        double discard = FP::undefined();
        rc = parseTime(fields, discard);
        if (rc > 0)
            return 400 + rc;
    }

    /* TCcounts */
    if (fields.empty()) return 4;
    fields.pop_front();

    /* TCmass */
    if (fields.empty()) return 5;
    fields.pop_front();

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root X1(field.parse_real(&ok));
    if (!ok) return 28;
    if (!FP::defined(X1.read().toReal())) return 29;
    X1.write().setReal(X1.read().toReal() * 1E-3);
    remap("X1", X1);

    /* BC6_AE33 */
    if (fields.empty()) return 5;
    fields.pop_front();

    /* AE33_ValidData */
    if (fields.empty()) return 30;
    fields.pop_front();

    /* AE33_b */
    if (fields.empty()) return 30;
    fields.pop_front();

    /* OC */
    if (fields.empty()) return 30;
    fields.pop_front();

    /* EC */
    if (fields.empty()) return 30;
    fields.pop_front();

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root X2(field.parse_real(&ok));
    if (!ok) return 31;
    if (!FP::defined(X2.read().toReal())) return 32;
    X2.write().setReal(X2.read().toReal() * 1E3);
    remap("X2", X2);

    /* Volume */
    if (fields.empty()) return 30;
    fields.pop_front();

    /* Chamber */
    if (fields.empty()) return 35;
    fields.pop_front();

    /* SetupID */
    if (fields.empty()) return 36;
    fields.pop_front();

    /* CO2 background polynomials */
    for (int i = 0; i < 12; i++) {
        if (fields.empty()) return 50 + i;
        fields.pop_front();
    }

    /* Two possible but unknown fields (looks like more polynomial data?) */
    if (!fields.empty()) fields.pop_front();
    if (!fields.empty()) fields.pop_front();

    if (!fields.empty()) {
        if (config.front().strictMode)
            return 999;
    }

    if (lastResultID == recordID &&
            FP::defined(lastResultTime) &&
            FP::defined(endTime) &&
            lastResultTime == endTime) {
        return -1;
    }
    lastResultID = recordID;
    lastResultTime = endTime;

    emitMetadata(frameTime);

    loggingValue(LogStream::OnlineResult, frameTime, "X1", std::move(X1));
    loggingValue(LogStream::OnlineResult, frameTime, "X2", std::move(X2));

    advanceStream(LogStream::Data, startTime - 60.0);
    advanceStream(LogStream::Metadata, frameTime);
    advanceStream(LogStream::OnlineResult, frameTime);

    return 0;
}

int AcquireMageeTCA08::processAnyRecord(const Util::ByteView &line, double &frameTime)
{
    auto fields = CSV::acquisitionSplit(line);
    if (fields.size() < 10)
        return 1;

    bool ok = false;
    fields[0].parse_u64(&ok);
    if (!ok)
        return 2;

    if (fields[1].indexOf('/') == Util::ByteView::npos) {
        fields[1].parse_u64(&ok);
        if (ok) {
            int code = processOnlineResultRecord(line, frameTime);
            if (code > 0)
                code += 1000;
            return code;
        }
    }

    int code = processDataRecord(line, frameTime);
    if (code > 0)
        code += 2000;
    return code;
}

void AcquireMageeTCA08::configurationAdvance(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.empty());
}

void AcquireMageeTCA08::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    configurationAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Passive_Run: {
        int code = processAnyRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + config.front().reportInterval + 5.0);
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            timeoutAt(FP::undefined());
            responseState = ResponseState::Passive_Wait;
            generalStatusUpdated();

            info.hash("Code").setInteger(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait: {
        int code = processAnyRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + config.front().reportInterval + 5.0);

            if (++autoprobeValidRecords > 3) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                responseState = ResponseState::Passive_Run;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                realtimeStateUpdated = true;

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case ResponseState::Passive_Initialize:
    case ResponseState::Passive_Wait: {
        int code = processAnyRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);
            responseState = ResponseState::Passive_Run;
            generalStatusUpdated();

            ++autoprobeValidRecords;
            timeoutAt(frameTime + config.front().reportInterval + 5.0);

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case ResponseState::Interactive_Start_Read_DatabaseVer:
    case ResponseState::Interactive_Start_Read_DatabaseVer_Retry: {
        auto fields = CSV::acquisitionSplit(frame);
        bool ok = false;
        if (fields.size() > 2) {
            fields.front().parse_i64(&ok);
            fields.pop_front();
        }
        double time = FP::undefined();
        int code = 0;
        if (!ok || (code = parseTime(fields, time)) != 0 || !FP::defined(time)) {
            if (--responseRetryCounter > 0) {
                if (controlStream)
                    controlStream->resetControl();
                responseState = ResponseState::Interactive_Start_Read_DatabaseVer_Retry;
                discardData(frameTime + 1.0);
                timeoutAt(frameTime + 5.0);
                break;
            }

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            responseState = ResponseState::Interactive_Restart_Wait;
            if (controlStream)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            invalidateLogValues(frameTime);
            break;
        }

        std::int_fast64_t version = 0;
        if (fields.size() == 1 &&
                (version = fields.front().parse_i64(&ok)) > 0 &&
                ok && INTEGER::defined(version)) {
            if (version != instrumentParameters["DatabaseVersion"].toInteger()) {
                instrumentParameters["DatabaseVersion"].setInteger(version);
                haveEmittedParameters = false;
                haveEmittedRealtimeMeta = false;
            }
        } else {
            auto str = frame.toQString().trimmed();
            if (str != instrumentParameters["DatabaseVersion"].toQString()) {
                instrumentParameters["DatabaseVersion"].setString(str);
                haveEmittedParameters = false;
                haveEmittedRealtimeMeta = false;
            }
        }

        responseState = ResponseState::Interactive_Start_Read_FilterIntegrity;
        timeoutAt(frameTime + 5.0);

        if (controlStream) {
            controlStream->writeControl("$TCA:LAST FilterIntegrity\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadFilterIntegrity"), frameTime, FP::undefined()));
        }
        break;
    }
    case ResponseState::Interactive_Start_Read_FilterIntegrity: {
        auto str = frame.toQString().trimmed();
        if (str != instrumentParameters["FilterIntegrity"].toQString()) {
            instrumentParameters["FilterIntegrity"].setString(str);
            haveEmittedParameters = false;
            haveEmittedRealtimeMeta = false;
        }

        responseState = ResponseState::Interactive_Start_Read_HeaterResistance;
        timeoutAt(frameTime + 5.0);

        if (controlStream) {
            controlStream->writeControl("$TCA:LAST HeaterResistance\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadHeaterResistance"), frameTime, FP::undefined()));
        }
        break;
    }
    case ResponseState::Interactive_Start_Read_HeaterResistance: {
        auto str = frame.toQString().trimmed();
        if (str != instrumentParameters["HeaterResistance"].toQString()) {
            instrumentParameters["HeaterResistance"].setString(str);
            haveEmittedParameters = false;
            haveEmittedRealtimeMeta = false;
        }

        responseState = ResponseState::Interactive_Start_Read_Setup;
        timeoutAt(frameTime + 5.0);

        if (controlStream) {
            controlStream->writeControl("$TCA:LAST Setup\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveReadSetup"),
                                  frameTime, FP::undefined()));
        }
        break;
    }
    case ResponseState::Interactive_Start_Read_Setup: {
        auto str = frame.toQString().trimmed();
        if (str != instrumentParameters["Setup"].toQString()) {
            instrumentParameters["Setup"].setString(str);
            haveEmittedParameters = false;
            haveEmittedRealtimeMeta = false;
        }

        responseState = ResponseState::Interactive_Start_Read_Info_Initial;
        timeoutAt(frameTime + 5.0);

        if (controlStream) {
            controlStream->writeControl("$TCA:INFO\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveReadInfo"),
                                  frameTime, FP::undefined()));
        }
        break;
    }

    case ResponseState::Interactive_Start_Read_Info_Initial:
        instrumentParameters["Info"].remove();
        instrumentParameters["InfoTime"].setReal(frameTime);

        responseState = ResponseState::Interactive_Start_Read_Info_Continue;
        timeoutAt(frameTime + 1.0);

        /* Fall through */
    case ResponseState::Interactive_Start_Read_Info_Continue: {
        auto str = frame.toQString().trimmed();
        instrumentParameters["Info"].toArray().after_back().setString(str);

        if (str.startsWith("Serial Number:", Qt::CaseInsensitive)) {
            auto snString = str.mid(14).trimmed();
            auto parseString = snString;
            if (parseString.startsWith("TCA-08-S")) {
                auto check = parseString.mid(8);
                auto idx = check.indexOf('-');
                if (idx > 0) {
                    parseString = check.mid(idx + 1);
                }
            }
            bool ok = false;
            std::int_fast64_t sn = parseString.toLongLong(&ok);
            if (ok && INTEGER::defined(sn) && sn > 0) {
                if (sn != instrumentMeta["SerialNumber"].toInteger()) {
                    haveEmittedRealtimeMeta = false;
                    haveEmittedLogMeta = false;
                    instrumentMeta["SerialNumber"].setInteger(sn);
                }
            } else {
                if (snString != instrumentMeta["SerialNumber"].toQString()) {
                    haveEmittedRealtimeMeta = false;
                    haveEmittedLogMeta = false;
                    instrumentMeta["SerialNumber"].setString(snString);
                }
            }
        } else if (str.startsWith("Firmware Version:", Qt::CaseInsensitive)) {
            auto fwString = str.mid(17).trimmed();

            if (fwString != instrumentMeta["FirmwareVersion"].toQString()) {
                haveEmittedRealtimeMeta = false;
                haveEmittedLogMeta = false;
                instrumentMeta["FirmwareVersion"].setString(fwString);
            }
        } else if (str.startsWith("Software Version:", Qt::CaseInsensitive)) {
            auto swString = str.mid(17).trimmed();

            if (swString != instrumentMeta["SoftwareVersion"].toQString()) {
                haveEmittedRealtimeMeta = false;
                haveEmittedLogMeta = false;
                instrumentMeta["SoftwareVersion"].setString(swString);
            }
        } else if (str.startsWith("Model number:", Qt::CaseInsensitive)) {
            auto modelString = str.mid(13).trimmed();

            if (!modelString.isEmpty() && modelString != instrumentMeta["Model"].toQString()) {
                haveEmittedRealtimeMeta = false;
                haveEmittedLogMeta = false;
                instrumentMeta["Model"].setString(modelString);
            }
        }
        break;
    }

    case ResponseState::Interactive_Start_Read_VerificationReport_Initial:
        instrumentParameters["VerificationReport"].remove();

        responseState = ResponseState::Interactive_Start_Read_VerificationReport_Continue;
        timeoutAt(frameTime + 1.0);

        /* Fall through */
    case ResponseState::Interactive_Start_Read_VerificationReport_Continue: {
        auto str = frame.toQString().trimmed();
        instrumentParameters["VerificationReport"].toArray().after_back().setString(str);
        break;
    }

    case ResponseState::Interactive_Start_Read_OnlineResult: {
        lastDataID = 0;
        lastDataTime = FP::undefined();

        int code = processOnlineResultRecord(frame, frameTime);
        if (code == 0) {
            responseState = ResponseState::Interactive_Start_Read_Data;
            timeoutAt(frameTime + 5.0);

            /* Ignore the existing one, so we output it when we succeed */
            lastResultID = 0;
            lastResultTime = FP::undefined();

            if (controlStream) {
                controlStream->writeControl("$TCA:LAST Data\r");
            }
            if (realtimeEgress) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadData"), frameTime, FP::undefined()));
            }
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "result rejected with code" << code;

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            responseState = ResponseState::Interactive_Restart_Wait;
            if (controlStream)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("StartInteractiveReadOnlineResult");
            info.hash("Code").setInteger(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the online result (code %1).").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }
    case ResponseState::Interactive_Start_Read_Data: {
        lastDataID = 0;
        lastDataTime = FP::undefined();

        int code = processDataRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            /* Ignore the existing one, so we output it when we succeed */
            lastDataID = 0;
            lastDataTime = FP::undefined();

            responseState = ResponseState::Interactive_Run_Data;
            timeoutAt(frameTime + 5.0);
            if (controlStream) {
                controlStream->writeControl("$TCA:LAST Data\r");
            }

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("StartInteractiveReadData");
            event(frameTime, QObject::tr("Communications established."), false, info);

            realtimeStateUpdated = true;
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "data rejected with code" << code;

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            responseState = ResponseState::Interactive_Restart_Wait;
            if (controlStream)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("StartInteractiveReadData");
            info.hash("Code").setInteger(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the online result (code %1).").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case ResponseState::Interactive_Run_Data: {
        int code = processDataRecord(frame, frameTime);
        if (code > 0) {
            qCDebug(log) << "Data line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            responseState = ResponseState::Interactive_Start_Flush;
            timeoutAt(frameTime + 10.0);
            discardData(frameTime + 1.0);
            if (controlStream)
                controlStream->writeControl("\r\n\r\r$TCA:END\r");

            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("RunData");
            info.hash("Code").setInteger(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
            break;
        }

        responseState = ResponseState::Interactive_Run_OnlineResult;
        if (controlStream)
            controlStream->writeControl("$TCA:LAST OnlineResult\r");
        timeoutAt(frameTime + 5.0);

        break;
    }
    case ResponseState::Interactive_Run_OnlineResult: {
        int code = processOnlineResultRecord(frame, frameTime);
        if (code > 0) {
            qCDebug(log) << "Result line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            responseState = ResponseState::Interactive_Start_Flush;
            timeoutAt(frameTime + 10.0);
            discardData(frameTime + 1.0);
            if (controlStream)
                controlStream->writeControl("\r\n\r\r$TCA:END\r");

            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("RunResult");
            info.hash("Code").setInteger(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
            break;
        }

        if (FP::defined(config.front().pollInterval) && config.front().pollInterval > 0.0) {
            responseState = ResponseState::Interactive_Run_Wait;
            timeoutAt(frameTime + config.front().pollInterval * 2.0 + 5.0);
            discardData(frameTime + config.front().pollInterval);
        } else {
            responseState = ResponseState::Interactive_Run_Data;
            timeoutAt(frameTime + 5.0);
            if (controlStream)
                controlStream->writeControl("$TCA:LAST Data\r");
        }
        break;
    }

    default:
        break;
    }
}

void AcquireMageeTCA08::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Interactive_Start_Read_DatabaseVer:
    case ResponseState::Interactive_Start_Read_DatabaseVer_Retry:
        if (--responseRetryCounter > 0) {
            responseState = ResponseState::Interactive_Start_Read_DatabaseVer_Retry;
            if (controlStream)
                controlStream->resetControl();
            discardData(frameTime + 1.0);
            timeoutAt(frameTime + 5.0);
            break;
        }
        /* Fall through */
    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_Read_FilterIntegrity:
    case ResponseState::Interactive_Start_Read_HeaterResistance:
    case ResponseState::Interactive_Start_Read_Setup:
    case ResponseState::Interactive_Start_Read_Info_Initial:
    case ResponseState::Interactive_Start_Read_VerificationReport_Initial:
    case ResponseState::Interactive_Start_Read_OnlineResult:
    case ResponseState::Interactive_Start_Read_Data:
        qCDebug(log) << "Timeout in interactive start state" << static_cast<int>(responseState)
                     << "at" << Logging::time(frameTime);

        invalidateLogValues(frameTime);

        responseState = ResponseState::Interactive_Restart_Wait;
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);
        autoprobeValidRecords = 0;
        generalStatusUpdated();

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        break;


    case ResponseState::Passive_Run: {
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = ResponseState::Passive_Wait;
        generalStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;
    }

    case ResponseState::Interactive_Run_Data:
    case ResponseState::Interactive_Run_OnlineResult:
    case ResponseState::Interactive_Run_Wait: {
        qCDebug(log) << "Timeout in interactive run state" << static_cast<int>(responseState)
                     << "at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        if (controlStream) {
            controlStream->writeControl("\r\n\r\r$TCA:END\r");
        }

        generalStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            switch (responseState) {
            case ResponseState::Interactive_Run_Data:
                info.hash("ResponseState").setString("ReadData");
                break;
            case ResponseState::Interactive_Run_OnlineResult:
                info.hash("ResponseState").setString("ReadResult");
                break;
            case ResponseState::Interactive_Run_Wait:
                info.hash("ResponseState").setString("PollWait");
                break;
            default:
                break;
            }
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;
    }

    case ResponseState::Passive_Initialize:
    case ResponseState::Passive_Wait:
        responseState = ResponseState::Passive_Wait;
        autoprobeValidRecords = 0;
        invalidateLogValues(frameTime);
        break;

    case ResponseState::Autoprobe_Passive_Wait: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case ResponseState::Autoprobe_Passive_Initialize:
        responseState = ResponseState::Autoprobe_Passive_Wait;
        autoprobeValidRecords = 0;
        invalidateLogValues(frameTime);
        break;

    case ResponseState::Interactive_Restart_Wait:
    case ResponseState::Interactive_Initialize:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        if (controlStream) {
            controlStream->writeControl("\r\n\r\r$TCA:END\r");
        }

        generalStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case ResponseState::Interactive_Start_Read_Info_Continue:
        responseState = ResponseState::Interactive_Start_Read_VerificationReport_Initial;
        timeoutAt(frameTime + 5.0);

        if (controlStream) {
            controlStream->writeControl("$TCA:LAST VerificationReport\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadVerificationReport"), frameTime, FP::undefined()));
        }
        break;

    case ResponseState::Interactive_Start_Read_VerificationReport_Continue:
        responseState = ResponseState::Interactive_Start_Read_OnlineResult;
        timeoutAt(frameTime + 5.0);

        if (controlStream) {
            controlStream->writeControl("$TCA:LAST OnlineResult\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadOnlineResult"), frameTime, FP::undefined()));
        }
        break;
    }
}

void AcquireMageeTCA08::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Interactive_Start_Flush:
        responseRetryCounter = 4;
        responseState = ResponseState::Interactive_Start_Read_DatabaseVer;

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadDatabaseVer"), frameTime, FP::undefined()));
        }
        /* Fall through */
    case ResponseState::Interactive_Start_Read_DatabaseVer_Retry:
        timeoutAt(frameTime + 5.0);

        if (controlStream) {
            controlStream->writeControl("$TCA:LAST DatabaseVer\r");
        }
        break;

    case ResponseState::Interactive_Restart_Wait:
    case ResponseState::Interactive_Initialize:
        timeoutAt(frameTime + 5.0);

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        if (controlStream) {
            controlStream->writeControl("\r\n\r\r$TCA:END\r");
        }

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case ResponseState::Interactive_Run_Wait:
        responseState = ResponseState::Interactive_Run_Data;
        timeoutAt(frameTime + 5.0);
        if (controlStream)
            controlStream->writeControl("$TCA:LAST Data\r");
        break;

    default:
        break;
    }
}

void AcquireMageeTCA08::incomingControlFrame(const Util::ByteArray &, double)
{ }


Variant::Root AcquireMageeTCA08::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireMageeTCA08::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case ResponseState::Passive_Run:
    case ResponseState::Interactive_Run_Data:
    case ResponseState::Interactive_Run_OnlineResult:
    case ResponseState::Interactive_Run_Wait:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

AcquisitionInterface::AutoprobeStatus AcquireMageeTCA08::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireMageeTCA08::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case ResponseState::Interactive_Run_Data:
    case ResponseState::Interactive_Run_OnlineResult:
    case ResponseState::Interactive_Run_Wait:
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << static_cast<int>(responseState) << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_Read_DatabaseVer:
    case ResponseState::Interactive_Start_Read_DatabaseVer_Retry:
    case ResponseState::Interactive_Start_Read_FilterIntegrity:
    case ResponseState::Interactive_Start_Read_HeaterResistance:
    case ResponseState::Interactive_Start_Read_Setup:
    case ResponseState::Interactive_Start_Read_Info_Initial:
    case ResponseState::Interactive_Start_Read_Info_Continue:
    case ResponseState::Interactive_Start_Read_VerificationReport_Initial:
    case ResponseState::Interactive_Start_Read_VerificationReport_Continue:
    case ResponseState::Interactive_Start_Read_OnlineResult:
    case ResponseState::Interactive_Start_Read_Data:
    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        qCDebug(log) << "Interactive autoprobe started from state"
                     << static_cast<int>(responseState) << "at" << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 10.0);
        discardData(time + 1.0);
        if (controlStream) {
            controlStream->writeControl("\r\n\r\r$TCA:END\r");
        }

        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireMageeTCA08::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    Q_ASSERT(!config.empty());
    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    discardData(time + 0.5, 1);
    timeoutAt(time + unpolledResponseTime * 4.0 + 6.0);

    qCDebug(log) << "Reset to passive autoprobe from state" << static_cast<int>(responseState)
                 << "at" << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = ResponseState::Autoprobe_Passive_Wait;
    autoprobeValidRecords = 0;
}

void AcquireMageeTCA08::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 5.0);

    switch (responseState) {
    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_Read_DatabaseVer:
    case ResponseState::Interactive_Start_Read_DatabaseVer_Retry:
    case ResponseState::Interactive_Start_Read_FilterIntegrity:
    case ResponseState::Interactive_Start_Read_HeaterResistance:
    case ResponseState::Interactive_Start_Read_Setup:
    case ResponseState::Interactive_Start_Read_Info_Initial:
    case ResponseState::Interactive_Start_Read_Info_Continue:
    case ResponseState::Interactive_Start_Read_VerificationReport_Initial:
    case ResponseState::Interactive_Start_Read_VerificationReport_Continue:
    case ResponseState::Interactive_Start_Read_OnlineResult:
    case ResponseState::Interactive_Start_Read_Data:
    case ResponseState::Interactive_Run_Data:
    case ResponseState::Interactive_Run_OnlineResult:
    case ResponseState::Interactive_Run_Wait:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << static_cast<int>(responseState)
                     << "to interactive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        qCDebug(log) << "Promoted from passive state" << static_cast<int>(responseState)
                     << "to interactive acquisition at" << Logging::time(time);

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 30.0);
        discardData(time + 1.0);
        if (controlStream) {
            controlStream->writeControl("\r\n\r\r$TCA:END\r");
        }

        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireMageeTCA08::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    Q_ASSERT(!config.empty());
    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    timeoutAt(time + unpolledResponseTime * 2.0 + 1.0);

    switch (responseState) {
    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from autoprobe passive state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        responseState = ResponseState::Passive_Initialize;
        break;

    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_Read_DatabaseVer:
    case ResponseState::Interactive_Start_Read_DatabaseVer_Retry:
    case ResponseState::Interactive_Start_Read_FilterIntegrity:
    case ResponseState::Interactive_Start_Read_HeaterResistance:
    case ResponseState::Interactive_Start_Read_Setup:
    case ResponseState::Interactive_Start_Read_Info_Initial:
    case ResponseState::Interactive_Start_Read_Info_Continue:
    case ResponseState::Interactive_Start_Read_VerificationReport_Initial:
    case ResponseState::Interactive_Start_Read_VerificationReport_Continue:
    case ResponseState::Interactive_Start_Read_OnlineResult:
    case ResponseState::Interactive_Start_Read_Data:
    case ResponseState::Interactive_Run_Data:
    case ResponseState::Interactive_Run_OnlineResult:
    case ResponseState::Interactive_Run_Wait:
    case ResponseState::Interactive_Restart_Wait:
        qCDebug(log) << "Promoted from state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);

        discardData(FP::undefined());

        responseState = ResponseState::Passive_Initialize;
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireMageeTCA08::getDefaults()
{
    AutomaticDefaults result;
    result.name = "M$1$2";
    result.setSerialN81(115200);
    return result;
}


ComponentOptions AcquireMageeTCA08Component::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);
    return options;
}

ComponentOptions AcquireMageeTCA08Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireMageeTCA08Component::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples(true));
    examples.append(getPassiveExamples());
    return examples;
}

QList<ComponentExample> AcquireMageeTCA08Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data")));

    return examples;
}

bool AcquireMageeTCA08Component::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireMageeTCA08Component::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

std::unique_ptr<
        AcquisitionInterface> AcquireMageeTCA08Component::createAcquisitionPassive(const ComponentOptions &options,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireMageeTCA08(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireMageeTCA08Component::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireMageeTCA08(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireMageeTCA08Component::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{
    std::unique_ptr<AcquireMageeTCA08> i(new AcquireMageeTCA08(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireMageeTCA08Component::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{
    std::unique_ptr<AcquireMageeTCA08> i(new AcquireMageeTCA08(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
