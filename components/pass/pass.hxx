/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef PASS_H
#define PASS_H

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "datacore/externalsource.hxx"

class Pass : public CPD3::CPD3Action {
Q_OBJECT

    std::string profile;
    std::vector<CPD3::Data::SequenceName::Component> stations;
    double start;
    double end;

    bool runUpdate;
    QString comment;
    bool useDetached;

    bool terminated;
    CPD3::CPD3Action *activeAction;
    std::mutex mutex;

    bool testTerminated();

    void executeAction(CPD3::CPD3Action *action);

    void invokeUpdated();

    Pass();

public:
    Pass(const CPD3::ComponentOptions &options,
         double start,
         double end,
         const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~Pass();

    virtual void signalTerminate();

signals:

    void terminateRequested();

protected:
    virtual void run();
};

class PassComponent : public QObject, virtual public CPD3::ActionComponentTime {
Q_OBJECT
    Q_INTERFACES(CPD3::ActionComponentTime)

    Q_PLUGIN_METADATA(IID
                              "CPD3.pass"
                              FILE
                              "pass.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Time::LogicalTimeUnit actionDefaultTimeUnit();

    virtual int actionRequireStations();

    virtual int actionAllowStations();

    virtual bool actionRequiresTime();

    virtual CPD3::CPD3Action *createTimeAction(const CPD3::ComponentOptions &options,
                                               double start,
                                               double end,
                                               const std::vector<std::string> &stations = {});

    virtual QString promptTimeActionContinue(const CPD3::ComponentOptions &options,
                                             double start,
                                             double end,
                                             const std::vector<std::string> &stations = {});
};

#endif
