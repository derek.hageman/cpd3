/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/range.hxx"
#include "core/environment.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"

#include "pass.hxx"


Q_LOGGING_CATEGORY(log_component_pass, "cpd3.component.pass", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;

Pass::Pass()
{ Q_ASSERT(false); }

Pass::Pass(const ComponentOptions &options,
           double start,
           double end,
           const std::vector<SequenceName::Component> &setStations) : profile(
        SequenceName::impliedProfile()),
                                                                      stations(setStations),
                                                                      start(start),
                                                                      end(end),
                                                                      runUpdate(true),
                                                                      comment(),
                                                                      useDetached(false),
                                                                      terminated(false),
                                                                      activeAction(NULL)
{
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }
    if (options.isSet("comment")) {
        comment = qobject_cast<ComponentOptionSingleString *>(options.get("comment"))->get();
    }
    if (options.isSet("noupdate")) {
        runUpdate = !qobject_cast<ComponentOptionBoolean *>(options.get("noupdate"))->get();
    }

    if (!FP::defined(start) || !FP::defined(end) || end - start > 31 * 86400) {
        useDetached = true;
    }
    if (options.isSet("detached")) {
        useDetached = qobject_cast<ComponentOptionBoolean *>(options.get("detached"))->get();
    }
}

Pass::~Pass()
{
}

void Pass::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
        if (activeAction != NULL)
            activeAction->signalTerminate();
    }
    emit terminateRequested();
}

bool Pass::testTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}

void Pass::executeAction(CPD3Action *action)
{
    if (action == NULL) {
        qCWarning(log_component_pass) << "Unable to create action";
        return;
    }

    action->feedback.forward(feedback);

    action->start();

    {
        std::unique_lock<std::mutex> lock(mutex);
        if (terminated) {
            lock.unlock();
            action->signalTerminate();
            action->wait();
            delete action;
            return;
        }
        activeAction = action;
    }

    action->wait();

    std::lock_guard<std::mutex> lock(mutex);
    delete action;
    activeAction = NULL;
    if (terminated)
        return;
}

void Pass::invokeUpdated()
{
    feedback.emitStage(tr("Generating data"),
                       tr("The system is initializing edited data generation."), false);

    QObject *component = ComponentLoader::create("update_passed");
    if (component == NULL) {
        qCWarning(log_component_pass) << "Can't load updated passed component";
        return;
    }

    ActionComponentTime *actionComponentTime;
    if (!(actionComponentTime = qobject_cast<ActionComponentTime *>(component))) {
        qCWarning(log_component_pass) << "Updated passed component is not a time action";
        return;
    }

    if (actionComponentTime->actionAllowStations() < (int)stations.size() ||
            actionComponentTime->actionRequireStations() > (int)stations.size()) {
        qCWarning(log_component_pass)
            << "Updated passed component does not accept the number of passed stations";
        return;
    }

    ComponentOptions o = actionComponentTime->getOptions();
    if (auto profileOption = qobject_cast<ComponentOptionStringSet *>(o.get("profile"))) {
        profileOption->set(QSet<QString>() << QString::fromStdString(profile));
    } else if (auto profileOption = qobject_cast<ComponentOptionSingleString *>(o.get("profile"))) {
        profileOption->set(QString::fromStdString(profile));
    } else {
        qCWarning(log_component_pass) << "Unable to restrict profiles to update passed";
    }

    if (auto detachedOption = qobject_cast<ComponentOptionBoolean *>(o.get("detached"))) {
        detachedOption->set(useDetached);
    }

    executeAction(actionComponentTime->createTimeAction(o, stations));
}

void Pass::run()
{
    if (testTerminated())
        return;

    profile = Util::to_lower(std::move(profile));
    if (profile.empty()) {
        qCDebug(log_component_pass) << "Invalid profile";
        return;
    }

    double startProcessing = Time::time();

    {
        Archive::Access reader;
        Archive::Access::ReadLock lock(reader);

        auto allStations = reader.availableStations(stations);
        if (allStations.empty()) {
            qCDebug(log_component_pass) << "No stations available";
            return;
        }
        stations.clear();
        std::copy(allStations.begin(), allStations.end(), Util::back_emplacer(stations));
    }

    if (testTerminated())
        return;

    std::sort(stations.begin(), stations.end());

    qCDebug(log_component_pass) << "Starting pass for" << stations.size() << "station(s)";

    {
        Archive::Access access;

        for (const auto &station : stations) {
            feedback.emitStage(tr("Pass %1").arg(QString::fromStdString(station).toUpper()),
                               tr("The system is writing the data passed indicator for the station."),
                               false);

            qCDebug(log_component_pass) << "Writing passed flag for" << station << "in"
                                        << Logging::range(start, end);

            for (;;) {
                if (testTerminated())
                    break;
                Archive::Access::WriteLock lock(access);

                SequenceValue::Transfer modify;

                Variant::Root data;
                if (!comment.isEmpty())
                    data.write().hash("Comment").setString(comment);
                data.write().hash("Information").hash("By").setString("pass");
                data.write().hash("Information").hash("At").setDouble(Time::time());
                data.write()
                    .hash("Information")
                    .hash("Environment")
                    .setString(Environment::describe());
                data.write()
                    .hash("Information")
                    .hash("Revision")
                    .setString(Environment::revision());

                std::int32_t priority = 0;
                {
                    {
                        StreamSink::Iterator existing;
                        access.readStream(Archive::Selection(start, end, {station}, {"passed"},
                                                             {profile}).withDefaultStation(false),
                                          &existing)->detach();
                        while (existing.hasNext()) {
                            auto check = existing.next();
                            if (!FP::equal(check.getStart(), start))
                                continue;
                            if (!FP::equal(check.getEnd(), end))
                                continue;
                            priority = std::max(priority,
                                                static_cast<std::int32_t>(check.getPriority()));
                        }
                    }
                    if (priority == 0x7FFFFFFF) {
                        for (priority = 0; priority != -1; priority++) {
                            bool hit = false;
                            StreamSink::Iterator data;
                            auto reader = access.readStream(
                                    Archive::Selection(start, end, {station}, {"passed"}, {profile})
                                            .withDefaultStation(false), &data);
                            while (data.hasNext()) {
                                auto check = data.next();
                                if (!FP::equal(check.getStart(), start))
                                    continue;
                                if (!FP::equal(check.getEnd(), end))
                                    continue;
                                if (check.getPriority() != priority)
                                    continue;
                                hit = true;
                                break;
                            }
                            data.abort();
                            reader->signalTerminate();
                            reader->wait();
                            if (!hit)
                                break;
                        }
                    }
                }
                modify.emplace_back(
                        SequenceIdentity({station, "passed", profile}, start, end, priority),
                        std::move(data));


                Variant::Root event;
                event["Text"]
                     .setString(tr("Data passed from %1 to %2", "pass event text").arg(
                             Time::toISO8601(start), Time::toISO8601(end)));
                event["Information"].hash("By").setString("pass");
                event["Information"].hash("At").setDouble(Time::time());
                event["Information"].hash("Environment").setString(Environment::describe());
                event["Information"].hash("Revision").setString(Environment::revision());
                event["Information"].hash("Start").setDouble(start);
                event["Information"].hash("End").setDouble(end);
                if (!comment.isEmpty())
                    event["Information"].hash("Comment").setString(comment);
                double tNow = Time::time();
                modify.emplace_back(SequenceIdentity({station, "events", "editing"}, tNow, tNow),
                                    std::move(event));

                access.writeSynchronous(modify);

                if (lock.commit())
                    break;
            }
        }
    }

    if (runUpdate)
        invokeUpdated();

    double endProcessing = Time::time();
    qCDebug(log_component_pass) << "Finished passing after"
                                << Logging::elapsed(endProcessing - startProcessing);

    QCoreApplication::processEvents();
}


ComponentOptions PassComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Execution profile"),
                                                tr("This is the profile name to pass.  Multiple profiles can be "
                                                           "defined to specify different sets of data that can be passed "
                                                           "independently.  Consult the station editing configuration for available profiles."),
                                                tr("aerosol")));

    options.add("noupdate",
                new ComponentOptionBoolean(tr("noupdate", "name"), tr("Disable data update"),
                                           tr("When enabled, this causes the immediate data update to be "
                                                      "skipped.  The result is that the clean and averaged data are "
                                                      "not updated until the next time the station tasks are run at "
                                              "the appropriate level.  This normally means that the data will "
                                              "not be updated until the following night when the execution tasks "
                                              "run automatically."), QString()));

    options.add("comment",
                new ComponentOptionSingleString(tr("comment", "name"), tr("Comment to attach"),
                                                tr("This is an optional comment to attach to the pass operation.  "
                                                   "Ideally this should explain the reasoning for the (re)passing if "
                                                   "there is anything anomalous about it."),
                                                QString()));

    options.add("detached", new ComponentOptionBoolean(tr("detached", "name"),
                                                       tr("Execute the pass in detached mode if available"),
                                                       tr("This option causes the data update to be executed in detached processes if available.  "
                                                          "This allows for better isolation in the event of failure or other problems."),
                                                       tr("Enabled for more than 31 days")));

    return options;
}

QList<ComponentExample> PassComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Pass data", "default example name"),
                                     tr("This will pass data for the default \"aerosol\" profile.")));

    return examples;
}

Time::LogicalTimeUnit PassComponent::actionDefaultTimeUnit()
{ return Time::Week; }

int PassComponent::actionRequireStations()
{ return 1; }

int PassComponent::actionAllowStations()
{ return INT_MAX; }

bool PassComponent::actionRequiresTime()
{ return false; }

CPD3Action *PassComponent::createTimeAction(const ComponentOptions &options,
                                            double start,
                                            double end, const std::vector<std::string> &stations)
{ return new Pass(options, start, end, stations); }

QString PassComponent::promptTimeActionContinue(const CPD3::ComponentOptions &options,
                                                double start,
                                                double end,
                                                const std::vector<std::string> &stations)
{
    Q_UNUSED(options);

    if (stations.size() > 1) {
        return Pass::tr("This operation will pass data for multiple stations at the same time.");
    }
    if (!FP::defined(start) || !FP::defined(end) || (end - start) > (86400 * 7 + 1)) {
        return Pass::tr("This operation will pass data for more than a single week of data.");
    }
    return QString();
}
