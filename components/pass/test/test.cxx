/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QDir>
#include <QFile>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/actioncomponent.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    ActionComponentTime *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ActionComponentTime *>(ComponentLoader::create("pass"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("profile")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("comment")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("noupdate")));
    }

    void basic()
    {
        {
            Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                    SequenceValue({"brw", "configuration", "editing"}, Variant::Root())});
        }

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createTimeAction(options, 1000, 2000, {"brw"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        auto data = Archive::Access(databaseFile).readSynchronous(
                Archive::Selection(SequenceName("brw", "passed", "aerosol")));
        QCOMPARE((int) data.size(), 1);
        QCOMPARE(data[0].getStart(), 1000.0);
        QCOMPARE(data[0].getEnd(), 2000.0);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
