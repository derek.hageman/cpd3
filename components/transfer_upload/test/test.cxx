/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QDir>
#include <QCryptographicHash>
#include <QHostInfo>
#include <QFile>
#include <QTemporaryFile>
#include <QBuffer>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/actioncomponent.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "transfer/package.hxx"
#include "transfer/datafile.hxx"
#include "io/drivers/file.hxx"

static const char *cert1Data = "-----BEGIN CERTIFICATE-----\n"
        "MIICbTCCAdYCCQDZ6mj3BI+kCTANBgkqhkiG9w0BAQUFADB7MQswCQYDVQQGEwJV\n"
        "UzERMA8GA1UECAwIQ29sb3JhZG8xEDAOBgNVBAcMB0JvdWxkZXIxFjAUBgNVBAoM\n"
        "DU5PQUEvR01EL0VTUkwxFzAVBgNVBAsMDkFlcm9zb2xzIEdyb3VwMRYwFAYDVQQD\n"
        "DA1EZXJlayBIYWdlbWFuMB4XDTEyMDcyNzE1MzkwNloXDTM5MTIxMjE1MzkwNlow\n"
        "ezELMAkGA1UEBhMCVVMxETAPBgNVBAgMCENvbG9yYWRvMRAwDgYDVQQHDAdCb3Vs\n"
        "ZGVyMRYwFAYDVQQKDA1OT0FBL0dNRC9FU1JMMRcwFQYDVQQLDA5BZXJvc29scyBH\n"
        "cm91cDEWMBQGA1UEAwwNRGVyZWsgSGFnZW1hbjCBnzANBgkqhkiG9w0BAQEFAAOB\n"
        "jQAwgYkCgYEAn3nIKzjLLEY8KG6+hUvrkjDkl9VU8HFKnp8wA2RsMta/UvKG4ZIY\n"
        "bNVsKSiAGm1G0i6Mtftw8quyihoxuy/hElH/XJChU5lersdrhalocHXoipNx8BeW\n"
        "T8sXuJOrh+IoDKqVI4I6Sz42qAlqBU7bNm40TE8sBPAOefyTGavC3e0CAwEAATAN\n"
        "BgkqhkiG9w0BAQUFAAOBgQBU2hljZZpJJ+zK6eePUpdELQGPGHJEbzfReeSBhRau\n"
        "iDj4jEa7IE3yrMv8NqC3Q+6lEVuJg0NInvDYsNYLPjMSPS+jKldrmrJvg+TFrp1w\n"
        "Qr3JoznNxwcwuDyrKjOVux6jo2aype3Zayo5VUizJX9Zo+xLe2MBVR1D0MXmoq8L\n"
        "zA==\n"
        "-----END CERTIFICATE-----";
static const char *key1Data = "-----BEGIN RSA PRIVATE KEY-----\n"
        "MIICXAIBAAKBgQCfecgrOMssRjwobr6FS+uSMOSX1VTwcUqenzADZGwy1r9S8obh\n"
        "khhs1WwpKIAabUbSLoy1+3Dyq7KKGjG7L+ESUf9ckKFTmV6ux2uFqWhwdeiKk3Hw\n"
        "F5ZPyxe4k6uH4igMqpUjgjpLPjaoCWoFTts2bjRMTywE8A55/JMZq8Ld7QIDAQAB\n"
        "AoGBAIQOd0f7PpsKCfS9R7zfklG7dP+Z4z07wzu4vCyC8uniVAoe1LxjmyA8VtV6\n"
        "OSIpDTUs4M4tSWlZ7n1XlYjY6/lNt3xsy5BBQMlWVI8BV/yXnWRCxkQGBvXYTUKe\n"
        "7LRsUcqyGpAPeuStO/V8GEM5H272yv5S4413D09C9cAhROMtAkEA07j2lnnSl/WR\n"
        "0NFEOT9+nbh6Z+yRTzybC86TBhLCN3bY4VvqSWi1PdcU3oiz81dcaAPTksdx18/Y\n"
        "cSfz295ewwJBAMDTq22Dt2HMPQymxNUn9/IdIPU34/fEqSHS3e9hAgrp2gDVeNHR\n"
        "ZEzMkW00rLgdukkW51PV9hVIhYXdxYOjZY8CQFsf/cnwLurGf/b/SrzVDjr1/oEi\n"
        "Obx/2j+vrmnrwvm6Rkhglir4TSGLo+jPr5vpmtUN6I8BFoeLZp31UyjrwZ8CQC3W\n"
        "Y2ruI7qozV5jimjNToCMchg4yAVPB5GVydIsskqb2onWNRlTeE9VVcCrA9/kmTLk\n"
        "serY8t2OVsdCt8AaKHsCQFMEhJCpXaeLnrzToSv0uqRYjcT+r3ZjSghhSepN0ZA+\n"
        "PPj1CMmq9JrQaQAv3rT3hCbDF38TPhKX64OYzbuvdxA=\n"
        "-----END RSA PRIVATE KEY-----\n";

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Transfer;

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;
    QString tmpPath;
    QDir tmpDir;

    ActionComponent *component;

    void recursiveRemove(const QDir &base)
    {
        if (base.path().isEmpty() || base == QDir::current())
            return;
        QFileInfoList list(base.entryInfoList(QDir::Files | QDir::NoDotAndDotDot));
        for (QFileInfoList::const_iterator rm = list.constBegin(), endRm = list.constEnd();
                rm != endRm;
                ++rm) {
            if (!QFile::remove(rm->absoluteFilePath())) {
                qDebug() << "Failed to remvoe file" << rm->absoluteFilePath();
                continue;
            }
        }
        list = base.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);
        for (QFileInfoList::const_iterator rm = list.constBegin(), endRm = list.constEnd();
                rm != endRm;
                ++rm) {
            QDir subDir(base);
            if (!subDir.cd(rm->fileName())) {
                qDebug() << "Failed to change directory" << rm->absoluteFilePath();
                continue;
            }
            recursiveRemove(subDir);
            base.rmdir(rm->fileName());
        }
    }

    bool checkDataContents(const SequenceValue::Transfer &a, ArchiveValue::Transfer b)
    {
        if (a.size() != b.size())
            return false;
        for (const auto &va : a) {
            bool hit = false;
            for (auto vb = b.begin(); vb != b.end(); ++vb) {
                if (!FP::equal(va.getStart(), vb->getStart()))
                    continue;
                if (!FP::equal(va.getEnd(), vb->getEnd()))
                    continue;
                if (va.getUnit() != vb->getUnit())
                    continue;
                if (va.getValue() != vb->getValue())
                    continue;

                b.erase(vb);
                hit = true;
                break;
            }
            if (!hit)
                return false;
        }
        return b.empty();
    }

    bool checkFileContents(const SequenceValue::Transfer &expected, const QString &name)
    {
        auto inputFile = IO::Access::file(name);
        if (!inputFile) {
            qDebug() << "Error opening file";
            return false;
        }

        auto buffer = IO::Access::buffer();

        TransferUnpack unpack;
        if (!unpack.exec(inputFile, buffer)) {
            qDebug() << "Error unpacking file:" << unpack.errorString();
            return false;
        }

        DataUnpack dataUnpack;
        ErasureSink::Buffer data;
        if (!dataUnpack.exec(buffer, &data)) {
            qDebug() << "Error unpacking data:" << dataUnpack.errorString();
            return false;
        }

        return checkDataContents(expected, data.archiveTake());
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ActionComponent *>(ComponentLoader::create("transfer_upload"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());

        tmpDir = QDir(QDir::tempPath());
        tmpPath = "CPD3Upload-";
        tmpPath.append(Random::string());
        tmpDir.mkdir(tmpPath);
        QVERIFY(tmpDir.cd(tmpPath));
    }

    void cleanupTestCase()
    {
        recursiveRemove(tmpDir);
        tmpDir.cdUp();
        tmpDir.rmdir(tmpPath);
    }

    void init()
    {
        recursiveRemove(tmpDir);
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("profile")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("flush")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("timeout")));
    }

    void basic()
    {
        tmpDir.mkdir("send");
        tmpDir.mkdir("sent");
        tmpDir.mkdir("archive");

        Variant::Root config;
        config["/Upload/aerosol/Send"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "send"));
        config["/Upload/aerosol/Sent"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "sent"));
        config["/Upload/aerosol/Filter"].setString("sfa:raw:.*");
        config["/Upload/aerosol/Key"].setString(key1Data);
        config["/Upload/aerosol/Certificate"].setString(cert1Data);
        config["/Upload/aerosol/Transfer/Type"].setString("Copy");
        config["/Upload/aerosol/Transfer/Local/Target"].setString(
                QString("%1/%2/${FILE}").arg(tmpDir.absolutePath(), "archive"));

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "transfer"}, config)});

        SequenceValue::Transfer transferData1
                {SequenceValue({"sfa", "raw", "T_S11"}, Variant::Root(1.0), 100, 200, 0),
                 SequenceValue({"sfa", "raw", "P_S11"}, Variant::Root(1.0), 100, 200, 0),};
        Archive::Access(databaseFile).writeSynchronous(transferData1);

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        {
            QDir checkDir(tmpDir);
            checkDir.cd("archive");
            QFileInfoList files(checkDir.entryInfoList(QDir::Files));
            QCOMPARE(files.size(), 1);
            QVERIFY(checkFileContents(transferData1, files.at(0).absoluteFilePath()));

        }

        {
            QDir checkDir(tmpDir);
            checkDir.cd("sent");
            QFileInfoList files(checkDir.entryInfoList(QDir::Files));
            QCOMPARE(files.size(), 1);
            QVERIFY(checkFileContents(transferData1, files.at(0).absoluteFilePath()));

        }

        QTest::qSleep(1000);

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        {
            QDir checkDir(tmpDir);
            checkDir.cd("archive");
            QFileInfoList files(checkDir.entryInfoList(QDir::Files));
            QCOMPARE(files.size(), 1);
            QVERIFY(checkFileContents(transferData1, files.at(0).absoluteFilePath()));

        }

        {
            QDir checkDir(tmpDir);
            checkDir.cd("sent");
            QFileInfoList files(checkDir.entryInfoList(QDir::Files));
            QCOMPARE(files.size(), 1);
            QVERIFY(checkFileContents(transferData1, files.at(0).absoluteFilePath()));

        }

        QTest::qSleep(1000);


        SequenceValue::Transfer transferData2
                {SequenceValue({"sfa", "raw", "T_S11"}, Variant::Root(2.0), 200, 300, 0),
                 SequenceValue({"sfa", "raw", "P_S11"}, Variant::Root(3.0), 300, 400, 0)};
        Archive::Access(databaseFile).writeSynchronous(transferData2);

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        {
            QDir checkDir(tmpDir);
            checkDir.cd("archive");
            QFileInfoList files(checkDir.entryInfoList(QDir::Files, QDir::Name));
            QCOMPARE(files.size(), 2);
            QVERIFY(checkFileContents(transferData1, files.at(0).absoluteFilePath()));
            QVERIFY(checkFileContents(transferData2, files.at(1).absoluteFilePath()));
        }
        {
            QDir checkDir(tmpDir);
            checkDir.cd("sent");
            QFileInfoList files(checkDir.entryInfoList(QDir::Files, QDir::Name));
            QCOMPARE(files.size(), 2);
            QVERIFY(checkFileContents(transferData1, files.at(0).absoluteFilePath()));
            QVERIFY(checkFileContents(transferData2, files.at(1).absoluteFilePath()));
        }
    }

    void retransferOngoing()
    {
        tmpDir.mkdir("send");
        tmpDir.mkdir("sent");
        tmpDir.mkdir("archive");

        double startTime = Time::time() - 10;

        Variant::Root config;
        config["/Upload/aerosol/Send"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "send"));
        config["/Upload/aerosol/Sent"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "sent"));
        config["/Upload/aerosol/Filter"].setString("sfa:raw:.*");
        config["/Upload/aerosol/Key"].setString(key1Data);
        config["/Upload/aerosol/Certificate"].setString(cert1Data);
        config["/Upload/aerosol/Transfer/Type"].setString("Copy");
        config["/Upload/aerosol/Transfer/Local/Target"].setString(
                QString("%1/%2/${FILE}").arg(tmpDir.absolutePath(), "archive"));

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "transfer"}, config)});

        SequenceValue::Transfer transferData1
                {SequenceValue({"sfa", "raw", "T_S11"}, Variant::Root(1.0), startTime,
                               startTime + 100, 0),
                 SequenceValue({"sfa", "raw", "P_S11"}, Variant::Root(1.0), startTime,
                               startTime + 100, 0)};
        Archive::Access(databaseFile).writeSynchronous(transferData1);

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        {
            QDir checkDir(tmpDir);
            checkDir.cd("archive");
            QFileInfoList files(checkDir.entryInfoList(QDir::Files));
            QCOMPARE(files.size(), 1);

        }

        {
            QDir checkDir(tmpDir);
            checkDir.cd("sent");
            QFileInfoList files(checkDir.entryInfoList(QDir::Files));
            QCOMPARE(files.size(), 1);
        }

        QTest::qSleep(1000);

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        {
            QDir checkDir(tmpDir);
            checkDir.cd("archive");
            QFileInfoList files(checkDir.entryInfoList(QDir::Files));
            QCOMPARE(files.size(), 2);
        }

        {
            QDir checkDir(tmpDir);
            checkDir.cd("sent");
            QFileInfoList files(checkDir.entryInfoList(QDir::Files));
            QCOMPARE(files.size(), 2);
        }

        QTest::qSleep(1000);

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        {
            QDir checkDir(tmpDir);
            checkDir.cd("archive");
            QFileInfoList files(checkDir.entryInfoList(QDir::Files));
            QCOMPARE(files.size(), 3);
        }

        {
            QDir checkDir(tmpDir);
            checkDir.cd("sent");
            QFileInfoList files(checkDir.entryInfoList(QDir::Files));
            QCOMPARE(files.size(), 3);
        }
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
