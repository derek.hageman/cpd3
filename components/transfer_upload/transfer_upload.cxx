/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <limits.h>
#include <QStringList>
#include <QSet>
#include <QFile>
#include <QDir>
#include <QEventLoop>
#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/range.hxx"
#include "core/environment.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "database/runlock.hxx"
#include "datacore/sequencefilter.hxx"
#include "acquisition/acquisitionnetwork.hxx"
#include "transfer/datafile.hxx"
#include "transfer/package.hxx"
#include "io/process.hxx"
#include "io/drivers/file.hxx"

#include "transfer_upload.hxx"


Q_LOGGING_CATEGORY(log_component_transfer_upload, "cpd3.component.transfer.upload", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Transfer;
using namespace CPD3::Acquisition;

/*
 * There are three transfer options and four storage modes that are combined
 * to make the full operating mode.
 *
 * Transfer options (now offloaded to general uploader):
 *      Internal FTP client
 *      External command invoke
 *      Write directly to file (e.x. NFS)
 *
 * Storage Options
 *      None (do not retain data files after transfer)
 *      Send/Sent (standard queue directory of files for sending and an those
 *          that have already been sent)
 *      Send/REMOVE (queue of files to send and remove those that have been
 *          sent)
 *      Keep (keep all files; they should be removed by an external program)
 *
 * When operating with a queue directory transfer failure is normally
 * not critical (they will just be retried the next time).
 */

TransferUpload::TransferUpload() : profile(SequenceName::impliedProfile()),
                                   stations(),
                                   lockTimeout(30.0),
                                   flushAcquisition(false),
                                   terminated(false),
                                   mutex()
{ }

TransferUpload::TransferUpload(const ComponentOptions &options,
                               const std::vector<SequenceName::Component> &setStations) : profile(
        SequenceName::impliedProfile()),
                                                                                          stations(
                                                                                                  setStations),
                                                                                          lockTimeout(
                                                                                                  30.0),
                                                                                          flushAcquisition(
                                                                                                  false),
                                                                                          terminated(
                                                                                                  false),
                                                                                          mutex()
{
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }
    if (options.isSet("timeout")) {
        lockTimeout = qobject_cast<ComponentOptionSingleDouble *>(options.get("timeout"))->get();
    }
    if (options.isSet("flush")) {
        flushAcquisition = qobject_cast<ComponentOptionBoolean *>(options.get("flush"))->get();
    }
}

TransferUpload::~TransferUpload()
{
}

void TransferUpload::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    terminateRequested();
}

bool TransferUpload::testTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}

TransferUpload::Uploader::Uploader(const Variant::Read &config, TransferUpload *p)
        : FileUploader(config), parent(p)
{ }

TransferUpload::Uploader::~Uploader()
{ }

QString TransferUpload::Uploader::applySubstitutions(const QString &input) const
{ return parent->substitutions.apply(input); }

void TransferUpload::Uploader::pushTemporaryOutput(const QFileInfo &info)
{
    parent->substitutions.push();
    parent->substitutions.setFile("upload", info);
    parent->substitutions.setFile("source", info);
    parent->substitutions.setFile("input", info);
}

void TransferUpload::Uploader::popTemporaryOutput()
{
    parent->substitutions.pop();
}

void TransferUpload::Uploader::pushInputFile(const QFileInfo &info)
{
    parent->substitutions.push();
    parent->substitutions.setFile("input", info);
    parent->substitutions.setFile("upload", info);
    parent->substitutions.setFile("source", info);
    parent->substitutions.setString("file", info.fileName());
}

void TransferUpload::Uploader::popInputFile()
{
    parent->substitutions.pop();
}

TransferUpload::Packer::Packer(TransferUpload *p) : parent(p)
{ }

TransferUpload::Packer::~Packer()
{ }

QString TransferUpload::Packer::applySubstitutions(const QString &input) const
{ return parent->substitutions.apply(input); }


bool TransferUpload::invokeCommand(const QString &program, const QStringList &arguments)
{
    qCDebug(log_component_transfer_upload) << "Invoking:" << program << arguments;

    auto proc = IO::Process::Spawn(program, arguments).forward().create();
    if (!proc)
        return false;

    IO::Process::Instance::ExitMonitor exitOk(*proc);
    exitOk.connectTerminate(terminateRequested);
    if (testTerminated())
        return false;

    if (!proc->start() || !proc->wait())
        return false;
    return exitOk.success();
}

bool TransferUpload::executeActions(const Variant::Read &config)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        QStringList arguments(substitutions.apply(config.toQString())
                                           .split(QRegExp("\\s+"), QString::SkipEmptyParts));
        if (arguments.isEmpty()) {
            qCWarning(log_component_transfer_upload) << "Empty command as the only action";
            return true;
        }
        QString program(arguments.takeFirst());

        feedback.emitStage(tr("Execute External Program"),
                           tr("An external program is currently being executed."), false);
        feedback.emitState(program);
        return invokeCommand(program, arguments);
    }

    case Variant::Type::Array: {
        auto children = config.toArray();
        if (children.empty())
            return true;

        feedback.emitStage(tr("Execute External Program"),
                           tr("An external program is currently being executed."), false);

        for (auto child : children) {
            QString program(substitutions.apply(child.hash("Program").toQString()));
            if (program.isEmpty()) {
                qCWarning(log_component_transfer_upload) << "Empty command in action list";
                continue;
            }

            QStringList arguments;
            for (const auto &add : child.hash("Arguments").toArray()) {
                arguments.append(substitutions.apply(add.toQString()));
            }

            feedback.emitState(program);
            bool status = invokeCommand(program, arguments);
            if (!status && !child.hash("AllowFailure").toBool()) {
                qCDebug(log_component_transfer_upload) << "Execution of" << program << "failed";
                return false;
            }
        }

        break;
    }

    case Variant::Type::Hash: {
        QString program(substitutions.apply(config["Program"].toQString()));
        if (program.isEmpty()) {
            qCWarning(log_component_transfer_upload) << "Empty command in action list";
            return true;
        }

        feedback.emitStage(tr("Execute External Program"),
                           tr("An external program is currently being executed."), false);

        QStringList arguments;
        for (const auto &add : config["Arguments"].toArray()) {
            arguments.append(substitutions.apply(add.toQString()));
        }

        feedback.emitState(program);
        bool status = invokeCommand(program, arguments);
        if (!status && !config["AllowFailure"].toBool()) {
            qCDebug(log_component_transfer_upload) << "Execution of" << program << "failed";
            return false;
        }
    }

    default:
        break;
    }

    return true;
}

bool TransferUpload::createTransferPackage(const Variant::Read &config,
                                           const CPD3::IO::Access::Handle &input,
                                           const CPD3::IO::Access::Handle &output)
{
    TransferPack packer;
    packer.compressStage();
    if (config["Encryption"].exists()) {
        packer.encryptStage(config["Encryption"]);
    }
    packer.signStage(config);
    packer.checksumStage();

    packer.feedback.forward(feedback);
    {
        Threading::Receiver rx;
        terminateRequested.connect(rx, [&] { packer.signalTerminate(); });
        if (testTerminated())
            return false;
        if (!packer.exec(input, output)) {
            qCInfo(log_component_transfer_upload) << "Failed to create transfer package";
            return false;
        }
    }
    return true;
}

bool TransferUpload::packageAnonymousData(const Variant::Read &config,
                                          const CPD3::IO::Access::Handle &input)
{
    auto temp = IO::Access::temporaryFile();
    if (!temp) {
        qCInfo(log_component_transfer_upload) << "Error opening package output file";
        return false;
    }
    if (!createTransferPackage(config, input, temp))
        return false;

    substitutions.setFile("package", QString::fromStdString(temp->filename()));

    if (!executeActions(config.hash("FilePackageActions")))
        return false;

    qCDebug(log_component_transfer_upload) << "Final transfer package is" << temp->size()
                                           << "byte(s)";

    Uploader upload(config["Transfer"], this);
    upload.addUpload(std::move(temp));
    Threading::Receiver rx;
    terminateRequested.connect(rx, std::bind(&Uploader::signalTerminate, &upload));
    if (testTerminated())
        return false;
    return upload.exec();
}

bool TransferUpload::packageDataToSend(const Variant::Read &config, const IO::Access::Handle &input)
{
    QString outputPattern(config["Storage/Send"].toQString());
    if (outputPattern.isEmpty())
        outputPattern = "${SEND}/${FILE}";
    QString outputName;
    for (qint64 instance = 1;; ++instance) {
        substitutions.setInteger("instance", instance);

        outputName = substitutions.apply(outputPattern);
        if (outputName.isEmpty()) {
            return false;
        }

        if (QFile::exists(outputName))
            continue;

        break;
    }

    if (config["Storage/CreateDirectory"].toBool()) {
        QFileInfo info(outputName);
        info.exists();
        info.dir().exists();
        info.dir().mkpath(".");
    }

    auto targetFile = IO::Access::file(outputName, IO::File::Mode::writeOnly());
    if (!createTransferPackage(config, input, targetFile)) {
        targetFile->remove();
        return false;
    }

    substitutions.setFile("package", QString::fromStdString(targetFile->filename()));

    if (!executeActions(config.hash("FilePackageActions"))) {
        targetFile->remove();
        return false;
    }

    qCDebug(log_component_transfer_upload) << "Final transfer package" << targetFile->filename()
                                           << "is" << targetFile->size() << "byte(s)";

    return true;
}

bool TransferUpload::packageDataFile(const Variant::Read &config,
                                     const CPD3::IO::Access::Handle &input,
                                     const QString &filePattern)
{
    TextSubstitutionStack::Context subctx(substitutions);
    substitutions.setInteger("instance", 1);
    substitutions.setString("uid", Random::string(8));
    substitutions.setString("file", substitutions.apply(filePattern));

    if (!executeActions(config.hash("FilePrepareActions"))) {
        return false;
    }

    const auto &storageType = config["Storage/Type"].toString();
    if (Util::equal_insensitive(storageType, "none", "anonymous")) {
        return packageAnonymousData(config, input);
    }

    return packageDataToSend(config, input);
}

static void setDefinedStarts(const SequenceValue::Transfer::iterator &begin,
                             const SequenceValue::Transfer::iterator &end,
                             double knownStart)
{
    for (auto mod = begin; mod != end; ++mod) {
        Q_ASSERT(!FP::defined(mod->getStart()));

        if (Range::compareStartEnd(knownStart, mod->getEnd()) > 0) {
            knownStart = mod->getEnd();
            for (auto back = begin; back != mod; ++back) {
                back->setStart(knownStart);
            }
        }

        mod->setStart(knownStart);
    }
}

bool TransferUpload::writePendingData(const SequenceName::Component &station,
                                      const Variant::Read &config,
                                      double lastRunTime,
                                      double sendEndTime,
                                      double &dataModified,
                                      const QString &filePattern)
{

    qCDebug(log_component_transfer_upload) << "Beginning transfer package creation";

    SequenceFilter filter;
    filter.configure(config["Filter"], {QString::fromStdString(station)});
    Archive::Selection::List selections;
    if (!config["Filter"].exists()) {
        filter.setDefaultAccept(true);

        /* Accept anything that has intersects the send interval */
        selections.push_back(Archive::Selection(lastRunTime, sendEndTime));
        if (FP::defined(lastRunTime)) {
            /* Also accept anything modified since the last run time */
            selections.push_back(
                    Archive::Selection(FP::undefined(), FP::undefined(), {}, {}, {}, {}, {}, {},
                                       lastRunTime));
        }
    } else {
        selections = filter.toArchiveSelection({station});
        if (selections.empty()) {
            /* Accept anything that has intersects the send interval */
            selections.push_back(Archive::Selection(lastRunTime, sendEndTime));
            if (FP::defined(lastRunTime)) {
                /* Also accept anything modified since the last run time */
                selections.push_back(
                        Archive::Selection(FP::undefined(), FP::undefined(), {}, {}, {}, {}, {}, {},
                                           lastRunTime));
            }
        } else {
            Archive::Selection::List additional = selections;
            /* Select anything modified since the last run time, or everything
             * if we've never run before */
            for (auto &modSel : selections) {
                modSel.modifiedAfter = lastRunTime;
                modSel.includeMetaArchive = false;
                modSel.includeDefaultStation = false;
            }
            if (FP::defined(lastRunTime)) {
                /* Select anything that intersects the send interval regardless
                 * of modification time. */
                for (auto &modSel : additional) {
                    modSel.start = lastRunTime;
                    modSel.end = sendEndTime;
                }
                std::move(additional.begin(), additional.end(), Util::back_emplacer(selections));
            }
        }
    }

    bool setUndefinedStarts = !config["AllowInfiniteStart"].toBool();

    SequenceName uploadEventUnit(station, "events", "transfer");
    bool includeUploadEvent = filter.accept(uploadEventUnit);

    feedback.emitStage(tr("Extract Data"),
                       tr("New data are currently being extracted and packaged for transfer."),
                       true);

    qint64 totalValues = 0;
    double firstTime = FP::undefined();
    double lastTime = FP::undefined();
    bool haveDefinedStart = false;

    auto dataFile = IO::Access::temporaryFile(true);
    if (!dataFile) {
        qCInfo(log_component_transfer_upload) << "Error opening output temporary file";
        return false;
    }
    DataPack packer(dataFile->stream(), false);
    packer.start();
    terminateRequested.connect(std::bind(&DataPack::signalTerminate, &packer));
    if (testTerminated()) {
        terminateRequested.disconnect();

        packer.signalTerminate();
        return false;
    }


    {
        Archive::Access access;
        Archive::Access::ReadLock lock(access, true);

        ArchiveSink::Iterator data;
        auto reader = access.readArchive(selections, &data);
        ArchiveValue::Transfer add;

        Threading::Receiver accessReceiver;
        terminateRequested.connect(accessReceiver, std::bind(&ArchiveSink::Iterator::abort, &data));
        terminateRequested.connect(accessReceiver,
                                   std::bind(&Archive::Access::ArchiveContext::signalTerminate,
                                             reader.get()));
        terminateRequested.connect(accessReceiver,
                                   std::bind(&Archive::Access::signalTerminate, &access));

        if (testTerminated()) {
            terminateRequested.disconnect();

            data.abort();
            reader->signalTerminate();
            reader->wait();
            reader.reset();

            access.signalTerminate();
            return false;
        }

        SequenceValue::Transfer values;
        for (;;) {
            add.clear();
            if (data.all(add) && add.empty())
                break;
            if (testTerminated()) {
                terminateRequested.disconnect();
                return false;
            }
            if (add.empty())
                break;
            if (totalValues == 0)
                firstTime = add.front().getStart();
            lastTime = add.back().getStart();

            for (auto &value : add) {
                if (Range::compareStartEnd(value.getStart(), sendEndTime) >= 0) {
                    continue;
                }
                if (!filter.acceptAdvancing(value))
                    continue;

                if (Range::compareEnd(value.getEnd(), sendEndTime) > 0) {
                    value.setEnd(sendEndTime);
                } else {
                    Q_ASSERT(FP::defined(value.getModified()));

                    /* Don't track modified times for values that extend beyond the end time of
                     * the send range.  This is so that we don't get stuck into an alternating
                     * state of long running values (metadata) continually setting the modified
                     * time backwards when there are no ongoing values to correct it back to
                     * the current time. */
                    if (!FP::defined(dataModified) || dataModified < value.getModified())
                        dataModified = value.getModified();
                }

                values.push_back(SequenceValue(value));
                totalValues++;

                if (!haveDefinedStart && FP::defined(value.getStart())) {
                    haveDefinedStart = true;
                    if (setUndefinedStarts && values.size() > 1) {
                        setDefinedStarts(values.begin(), values.end() - 1, value.getStart());
                    }
                }
            }
            if (setUndefinedStarts && !haveDefinedStart && values.size() < 1 * 1024 * 1024)
                continue;
            if (setUndefinedStarts && !haveDefinedStart && FP::defined(lastRunTime)) {
                setDefinedStarts(values.begin(), values.end(), lastRunTime);
            }

            packer.incomingData(std::move(values));
            values.clear();

            feedback.emitProgressTime(lastTime);
        }

        if (setUndefinedStarts && !haveDefinedStart && FP::defined(lastRunTime)) {
            setDefinedStarts(values.begin(), values.end(), lastRunTime);
        }

        packer.incomingData(std::move(values));

        accessReceiver.disconnect();
        reader->wait();
        reader.reset();
    }

    if (testTerminated()) {
        terminateRequested.disconnect();
        return false;
    }

    if (totalValues == 0) {
        terminateRequested.disconnect();
        qCDebug(log_component_transfer_upload) << "No data to upload";
        return false;
    }

    packer.flushData();

    if (includeUploadEvent) {
        Variant::Root event;

        event["Text"].setString(tr("Data uploaded"));
        event["Information"].hash("By").setString("transfer_upload");
        event["Information"].hash("At").setDouble(Time::time());
        event["Information"].hash("Environment").setString(Environment::describe());
        event["Information"].hash("Revision").setString(Environment::revision());
        event["Information"].hash("LastRunTime").setDouble(lastRunTime);
        event["Information"].hash("EndTime").setDouble(sendEndTime);
        event["Information"].hash("FirstTime").setDouble(firstTime);
        event["Information"].hash("TotalValues").setInt64(totalValues);
        event["Information"].hash("DataSize").setInt64(dataFile->size());

        double tEvent = sendEndTime;
        if (Range::compareStart(lastTime, tEvent) > 0) {
            tEvent = lastTime;
        }

        packer.incomingData(SequenceValue(uploadEventUnit, std::move(event), tEvent, tEvent, 0));
    }

    packer.endData();
    packer.wait();
    terminateRequested.disconnect();

    auto rawSize = dataFile->size();
    if (rawSize <= 0) {
        qCDebug(log_component_transfer_upload) << "Raw data file is empty";
        return false;
    }

    qCDebug(log_component_transfer_upload) << "Data package has" << totalValues << "value(s) in"
                                           << rawSize << "byte(s)";

    return packageDataFile(config, dataFile, filePattern);
}

bool TransferUpload::uploadPending(const Variant::Read &config, const QString &sendDirectory)
{
    const auto &storageType = config["Storage/Type"].toString();
    if (Util::equal_insensitive(storageType, "none", "anonymous")) {
        return true;
    }

    if (sendDirectory.isEmpty()) {
        qCWarning(log_component_transfer_upload) << "No directory to send";
        return false;
    }

    QDir dir(sendDirectory);
    QFileInfoList
            fileList(dir.entryInfoList(QDir::Files | QDir::Readable | QDir::Writable, QDir::Name));
    for (QFileInfoList::const_iterator operateFileInfo = fileList.constBegin(),
            endFileInfo = fileList.constEnd(); operateFileInfo != endFileInfo; ++operateFileInfo) {
        if (testTerminated())
            return false;

        TextSubstitutionStack::Context subctx(substitutions);
        substitutions.setString("file", operateFileInfo->fileName());

        QString fileName(operateFileInfo->absoluteFilePath());
        Uploader upload(config["Transfer"], this);
        upload.feedback.forward(feedback);
        upload.addUpload(IO::Access::file(fileName, IO::File::Mode::readOnly()));
        Threading::Receiver rx;
        terminateRequested.connect(rx, std::bind(&Uploader::signalTerminate, &upload));
        if (testTerminated())
            return false;
        if (!upload.exec()) {
            if (config["Transfer/AbortOnFailure"].toBool())
                return false;
            if (config["Transfer/HaltOnFailure"].toBool())
                return true;
            qCDebug(log_component_transfer_upload) << "Upload failed for" << fileName
                                                   << "but continuing anyway";
            continue;
        }

        if (testTerminated())
            return false;

        if (Util::equal_insensitive(storageType, "sendremove", "remove")) {
            QFile::remove(fileName);
            continue;
        } else if (Util::equal_insensitive(storageType, "leave", "keep", "sendkeep")) {
            continue;
        } else {
            QString outputName(config["Storage/Sent"].toQString());
            if (outputName.isEmpty())
                outputName = "${SENT}/${FILE}";
            outputName = substitutions.apply(outputName);

            if (outputName.isEmpty()) {
                qCWarning(log_component_transfer_upload) << "Empty completed file name";
                continue;
            }
            if (outputName != fileName) {
                if (config["Storage/CreateDirectory"].toBool()) {
                    QFileInfo info(outputName);
                    info.exists();
                    info.dir().exists();
                    info.dir().mkpath(".");
                }
                if (!FileUploader::moveLocalFile(fileName, outputName)) {
                    qCWarning(log_component_transfer_upload) << "Failed to move uploaded file";
                    continue;
                }
            }
        }
    }

    return true;
}

bool TransferUpload::uploadStation(const SequenceName::Component &station,
                                   const Variant::Read &config,
                                   double lastRunTime,
                                   double sendEndTime,
                                   double &dataModified)
{
    substitutions.clear();
    substitutions.setString("station", QString::fromStdString(station).toLower());
    substitutions.setTime("time", sendEndTime, false);
    substitutions.setTime("end", sendEndTime, false);
    substitutions.setTime("lasttime", lastRunTime, false);
    substitutions.setTime("start", lastRunTime, false);
    substitutions.setString("profile", QString::fromStdString(profile));

    QString filePattern(config["Filename"].toQString());
    if (filePattern.isEmpty())
        filePattern = "${STATION|UPPER}_${TIME}_${UID|UPPER}_${INSTANCE}.c3i";

    QString sendDirectory(config["Send"].toQString());
    if (sendDirectory.isEmpty())
        sendDirectory = "/aer/${STATION|LOWER}/log/send";
    sendDirectory = substitutions.apply(sendDirectory);
    substitutions.setFile("send", sendDirectory);

    QString sentDirectory(config["Sent"].toQString());
    if (sentDirectory.isEmpty())
        sentDirectory = "/aer/${STATION|LOWER}/log/sent";
    sentDirectory = substitutions.apply(sentDirectory);
    substitutions.setFile("sent", sentDirectory);

    if (!executeActions(config.hash("BeginActions")))
        return false;

    if (!writePendingData(station, config, lastRunTime, sendEndTime, dataModified, filePattern))
        return false;

    if (!executeActions(config.hash("UploadActions")))
        return false;

    if (!uploadPending(config, sendDirectory))
        return false;

    if (!executeActions(config.hash("EndActions")))
        return false;

    return true;
}

void TransferUpload::executeAcquisitionFlush()
{
    feedback.emitStage(tr("Flush Acquisition"),
                       tr("Data in the acquisition system's buffer is being flushed."), false);

    std::mutex mutex;
    std::condition_variable cv;
    enum class State {
        Wait, Connected, Failed
    } state = State::Wait;

    std::unique_ptr<AcquisitionNetworkClient>
            client(new AcquisitionNetworkClient(Variant::Read::empty()));

    client->connectionState.connect([&](bool connected) {
        {
            std::lock_guard<std::mutex> lock(mutex);
            switch (state) {
            case State::Wait:
                if (!connected)
                    return;
                state = State::Connected;
                break;
            case State::Connected:
                if (connected)
                    return;
                state = State::Failed;
                break;
            case State::Failed:
                return;
            }
        }
        cv.notify_all();
    });
    client->connectionFailed.connect([&]() {
        {
            std::lock_guard<std::mutex> lock(mutex);
            switch (state) {
            case State::Wait:
            case State::Connected:
                state = State::Failed;
                break;
            case State::Failed:
                return;
            }
        }
        cv.notify_all();
    });
    client->start();

    {
        std::unique_lock<std::mutex> lock(mutex);
        /*
         * Only wait a short time for a connection, since it shouldn't take long.  Most of the
         * time we'll just immediately abort from a failed connection anyway.
         */
        cv.wait_for(lock, std::chrono::seconds(5), [&] { return state != State::Wait; });
        if (state != State::Connected) {
            qCDebug(log_component_transfer_upload)
                << "Connection failed to acquisition system, so no flush was performed";
            return;
        }
    }

    client->dataFlush();

    /*
     * Wait a bit for it to be processed and the flush to start, since there's no direct feedback.
     */
    std::this_thread::sleep_for(std::chrono::seconds(5));

    client.reset();

    qCDebug(log_component_transfer_upload) << "Acquisition system data flush completed";
}

void TransferUpload::run()
{
    if (testTerminated())
        return;

    double start = Time::time();

    std::unordered_map<SequenceName::Component, Variant::Read> uploadActions;
    {
        Archive::Access access;
        Archive::Access::ReadLock lock(access);

        auto allStations = access.availableStations(stations);
        if (allStations.empty()) {
            qCDebug(log_component_transfer_upload) << "No stations available";
            return;
        }
        stations.clear();
        std::copy(allStations.begin(), allStations.end(), Util::back_emplacer(stations));

        double tnow = Time::time();
        SequenceSegment configSegment;
        {
            auto confList = SequenceSegment::Stream::read(
                    Archive::Selection(tnow - 1.0, tnow + 1.0, stations, {"configuration"},
                                       {"transfer"}), &access);
            auto f = Range::findIntersecting(confList, tnow);
            if (f == confList.end()) {
                qCDebug(log_component_transfer_upload) << "No configuration for" << stations;
                return;
            }
            configSegment = *f;
        }

        for (const auto &station : stations) {
            SequenceName stationUnit(station, "configuration", "transfer");
            auto config = configSegment.takeValue(stationUnit).read().hash("Upload").hash(profile);
            if (!config.exists())
                continue;
            config.detachFromRoot();

            uploadActions.emplace(station, std::move(config));
        }
    }
    if (uploadActions.empty()) {
        qCDebug(log_component_transfer_upload) << "No upload defined for " << stations;
        return;
    }

    if (testTerminated())
        return;

    if (flushAcquisition) {
        feedback.emitStage(tr("Acquisition Flush"),
                           tr("The local acquisition system is flushing data to the archive."),
                           false);
        qCDebug(log_component_transfer_upload) << "Starting local acquisition system data flush";
        executeAcquisitionFlush();
    }

    stations.clear();
    for (const auto &add : uploadActions) {
        stations.push_back(add.first);
    }
    std::sort(stations.begin(), stations.end());

    qCDebug(log_component_transfer_upload) << "Starting upload for" << stations.size()
                                           << "station(s)";

    for (const auto &station : stations) {
        if (testTerminated())
            break;

        feedback.emitStage(tr("Lock %1").arg(QString::fromStdString(station).toUpper()),
                           tr("The system is acquiring an exclusive lock for %1.").arg(
                                   QString::fromStdString(station).toUpper()), false);

        const auto &config = uploadActions[station];
        Q_ASSERT(config.exists());

        QString rcKey(QString("transferupload %1 %2").arg(QString::fromStdString(profile),
                                                          QString::fromStdString(station)));

        qCDebug(log_component_transfer_upload) << "Locking" << station;

        Database::RunLock rc;
        bool ok = false;
        double lastRunTime = rc.acquire(rcKey, lockTimeout, &ok);
        if (!ok) {
            feedback.emitFailure();
            qCDebug(log_component_transfer_upload) << "Upload skipped for" << station;
            continue;
        }
        double sendEndTime = rc.startTime();

        if (testTerminated()) {
            rc.fail();
            break;
        }

        double dataModified = lastRunTime;
        if (uploadStation(station, config, lastRunTime, sendEndTime, dataModified)) {
            rc.seenTime(dataModified);
            rc.release();
            qCDebug(log_component_transfer_upload) << "Upload completed for" << station;
        } else {
            feedback.emitFailure();
            rc.fail();
        }
    }

    double end = Time::time();
    qCDebug(log_component_transfer_upload) << "Finished upload after"
                                           << Logging::elapsed(end - start);

    QCoreApplication::processEvents();
}


ComponentOptions TransferUploadComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Execution profile"),
                                                tr("This is the profile name to execute.  Multiple profiles can be "
                                                   "defined to allow a single station to upload data to multiple "
                                                   "destinations at potentially different intervals.  "
                                                   "Consult the station configuration for available profiles."),
                                                tr("aerosol")));

    options.add("flush", new ComponentOptionBoolean(tr("flush", "name"), tr("Flush data"),
                                                    tr("When enabled, this causes a flush of the acquisition system "
                                                       "before initiating the actual data transfer.  This is used to "
                                                       "attempt to transmit as much data as is available now instead of "
                                                       "potentially still having some buffered for later transmission."),
                                                    tr("Disabled")));

    ComponentOptionSingleDouble *timeout =
            new ComponentOptionSingleDouble(tr("wait", "name"), tr("Maximum wait time"),
                                            tr("This is the maximum time to wait for another upload process to "
                                               "complete, in seconds.  If there are two concurrent processes for "
                                               "the same station and profile, the new one will wait at most this "
                                               "many seconds before giving up without uploading new data.  If "
                                               "this is set to undefined, it will wait forever for the other "
                                               "process to complete."), tr("30 seconds"));
    timeout->setMinimum(0.0);
    timeout->setAllowUndefined(true);
    options.add("timeout", timeout);

    return options;
}

QList<ComponentExample> TransferUploadComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(
            ComponentExample(options, tr("Upload all pending aerosol data", "default example name"),
                             tr("This will upload all pending data in the \"aerosol\" profile.  "
                                "This will not force the acquisition system (if any) to flush data, so "
                                "the total upload may be slightly behind real time (the data will "
                                "still be uploaded later once it is written).")));

    return examples;
}

int TransferUploadComponent::actionAllowStations()
{ return INT_MAX; }

CPD3Action *TransferUploadComponent::createAction(const ComponentOptions &options,
                                                  const std::vector<std::string> &stations)
{ return new TransferUpload(options, stations); }

