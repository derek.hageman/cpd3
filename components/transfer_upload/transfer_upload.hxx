/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef TRANSFERUPLOAD_H
#define TRANSFERUPLOAD_H

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>
#include <QStringList>
#include <QHash>
#include <QIODevice>
#include <QByteArray>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/textsubstitution.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/stream.hxx"
#include "transfer/upload.hxx"
#include "transfer/package.hxx"
#include "core/threading.hxx"
#include "io/access.hxx"

class TransferUpload;

class TransferUpload : public CPD3::CPD3Action {
Q_OBJECT

    class Uploader : public CPD3::Transfer::FileUploader {
        TransferUpload *parent;
    public:
        Uploader(const CPD3::Data::Variant::Read &config, TransferUpload *parent);

        virtual ~Uploader();

    protected:
        virtual QString applySubstitutions(const QString &input) const;

        virtual void pushTemporaryOutput(const QFileInfo &fileName);

        virtual void popTemporaryOutput();

        virtual void pushInputFile(const QFileInfo &file);

        virtual void popInputFile();
    };

    friend class Uploader;

    class Packer : public CPD3::Transfer::TransferPack {
        TransferUpload *parent;
    public:
        Packer(TransferUpload *parent);

        virtual ~Packer();

    protected:
        QString applySubstitutions(const QString &input) const override;
    };

    friend class Packer;

    std::string profile;
    std::vector<CPD3::Data::SequenceName::Component> stations;
    double lockTimeout;
    bool flushAcquisition;

    bool terminated;
    std::mutex mutex;

    bool testTerminated();

    CPD3::TextSubstitutionStack substitutions;

    bool createTransferPackage(const CPD3::Data::Variant::Read &config,
                               const CPD3::IO::Access::Handle &input,
                               const CPD3::IO::Access::Handle &output);

    bool packageAnonymousData(const CPD3::Data::Variant::Read &config,
                              const CPD3::IO::Access::Handle &input);

    bool packageDataToSend(const CPD3::Data::Variant::Read &config,
                           const CPD3::IO::Access::Handle &input);

    bool packageDataFile(const CPD3::Data::Variant::Read &config,
                         const CPD3::IO::Access::Handle &input,
                         const QString &filePattern);

    bool writePendingData(const CPD3::Data::SequenceName::Component &station,
                          const CPD3::Data::Variant::Read &config,
                          double lastRunTime,
                          double sendEndTime,
                          double &dataModified,
                          const QString &filePattern);

    bool invokeCommand(const QString &program, const QStringList &arguments);

    bool executeActions(const CPD3::Data::Variant::Read &config);

    bool uploadPending(const CPD3::Data::Variant::Read &config, const QString &sendDirectory);

    void executeAcquisitionFlush();

    bool uploadStation(const CPD3::Data::SequenceName::Component &station,
                       const CPD3::Data::Variant::Read &config,
                       double lastRunTime,
                       double sendEndTime,
                       double &dataModified);

    CPD3::Threading::Signal<> terminateRequested;

public:
    TransferUpload();

    TransferUpload(const CPD3::ComponentOptions &options,
                   const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~TransferUpload();

    virtual void signalTerminate();

protected:
    virtual void run();
};

class TransferUploadComponent : public QObject, virtual public CPD3::ActionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::ActionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.transfer_upload"
                              FILE
                              "transfer_upload.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int actionAllowStations();

    CPD3::CPD3Action *createAction
            (const CPD3::ComponentOptions &options = {},
             const std::vector<std::string> &stations = {}) override;
};

#endif
