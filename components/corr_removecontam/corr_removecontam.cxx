/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "smoothing/contamfilter.hxx"

#include "corr_removecontam.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;


QString CorrRemoveContamComponent::getGeneralSerializationName() const
{ return QString::fromLatin1("corr_removecontam"); }

ComponentOptions CorrRemoveContamComponent::getOptions()
{
    ComponentOptions options;

    options.add("contamination", new ComponentOptionSingleString(tr("contamination", "name"),
                                                                 tr("The contamination system in effect"),
                                                                 tr("This defines the standard contamination system that is used.  "
                                                                            "The variables affected by contamination are determined by which "
                                                                            "system is in use.  For example, the \"aerosol\" system affects "
                                                                            "scatterings, absorptions, extinctions, and counts.  This may "
                                                                            "not be used if variables are specified directly."),
                                                                 tr("aerosol",
                                                                    "default contamination mode")));

    options.add("variables", new DynamicSequenceSelectionOption(tr("variables", "name"),
                                                                tr("The variables that are affected by contamination"),
                                                                tr("This defines the variables that are affected by contamination.  "
                                                                "This may not be used if a system mode is specified."),
                                                                QString()));
    options.exclude("contamination", "variables");
    options.exclude("variables", "contamination");

    return options;
}

QList<ComponentExample> CorrRemoveContamComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will remove all variables affected by the standard "
                                                "definition of \"aerosol\" processing.")));

    options = getOptions();
    (qobject_cast<ComponentOptionSingleString *>(options.get("contamination")))->set(
            tr("met", "contamination none"));
    examples.append(ComponentExample(options, tr("Alternate system mode"),
                                     tr("This will remove all variables affected by the standard "
                                                "definition of \"met\" processing.")));

    options = getOptions();
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("variables")))->set(QString(),
                                                                                    QString(),
                                                                                    "T1_S11");
    examples.append(ComponentExample(options, tr("Single variable"),
                                     tr("This will remove only the variable T1_S11 when it is "
                                                "contaminated.  All other variables are left unchanged even if "
                                                "they would normally be affected by system contamination.")));

    return examples;
}

ProcessingStage *CorrRemoveContamComponent::createGeneralFilterDynamic(const ComponentOptions &options)
{
    return createGeneralFilterPredefined(options, FP::undefined(), FP::undefined(),
                                         QList<SequenceName>());
}

ProcessingStage *CorrRemoveContamComponent::createGeneralFilterPredefined(const ComponentOptions &options,
                                                                          double,
                                                                          double,
                                                                          const QList<
                                                                                  SequenceName> &inputs)
{
    ContaminationFilter *filter;
    if (options.isSet("variables")) {
        DynamicSequenceSelection *op = (qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("variables")))->getOperator();
        for (QList<SequenceName>::const_iterator add = inputs.constBegin(),
                endAdd = inputs.constEnd(); add != endAdd; ++add) {
            op->registerInput(*add);
        }
        filter = new ContaminationFilter(op);
    } else {
        QString contamMode("aerosol");
        if (options.isSet("contamination")) {
            contamMode =
                    qobject_cast<ComponentOptionSingleString *>(options.get("contamination"))->get()
                                                                                             .toLower();
        }
        filter = new ContaminationFilter(contamMode);
    }

    return filter;
}

ProcessingStage *CorrRemoveContamComponent::createGeneralFilterEditing(double start,
                                                                       double end,
                                                                       const SequenceName::Component &station,
                                                                       const SequenceName::Component &archive,
                                                                       const ValueSegment::Transfer &config)
{
    DynamicSequenceSelection
            *op = DynamicSequenceSelection::fromConfiguration(config, "Variables", start, end);

    ContaminationFilter *filter = new ContaminationFilter(op);
    filter->registerExpected(station, archive);
    return filter;
}

ProcessingStage *CorrRemoveContamComponent::deserializeGeneralFilter(QDataStream &stream)
{
    ContaminationFilter *filter = new ContaminationFilter(NULL);
    filter->deserialize(stream);
    return filter;
}
