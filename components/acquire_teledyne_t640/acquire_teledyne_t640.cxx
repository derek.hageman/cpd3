/*
 * Copyright (c) 2021 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <cstring>
#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "core/csv.hxx"
#include "core/timeutils.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"

#include "acquire_teledyne_t640.hxx"


using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;


static const std::array<std::string, 9> instrumentAlarmTranslation
        {"BoxTemperatureWarning", "FlowAlarm", "InternalSerialTimeout", "SystemResetWarning", "",
         "SampleTemperatureWarning", "BypassFlowWarning", "SystemFaultWarning", ""};


AcquireTeledyneT640::Configuration::Configuration() : start(FP::undefined()),
                                                      end(FP::undefined()),
                                                      pollInterval(0.5),
                                                      address(1)
{ }

AcquireTeledyneT640::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s), end(e), pollInterval(other.pollInterval), address(other.address)
{ }

void AcquireTeledyneT640::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Teledyne");
    instrumentMeta["Model"].setString("T640");

    autoprobeValidRecords = 0;
    realtimeStateAlarms.clear();

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    realtimeStateUpdated = false;
    havePM1Option = true;
}

AcquireTeledyneT640::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s), end(e), pollInterval(0.5), address(1)
{
    setFromSegment(other);
}

AcquireTeledyneT640::Configuration::Configuration(const Configuration &under,
                                                  const ValueSegment &over,
                                                  double s,
                                                  double e) : start(s),
                                                              end(e),
                                                              pollInterval(under.pollInterval),
                                                              address(under.address)
{
    setFromSegment(over);
}

void AcquireTeledyneT640::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["PollInterval"].exists())
        pollInterval = config["PollInterval"].toDouble();

    if (config["Address"].exists()) {
        auto check = config["Address"].toInteger();
        if (INTEGER::defined(check) && check > 0 && check <= 0xFF)
            address = static_cast<std::uint8_t>(check);
    }
}

AcquireTeledyneT640::AcquireTeledyneT640(const ValueSegment::Transfer &configData,
                                         const std::string &loggingContext) : FramedInstrument(
        "t640", loggingContext),
                                                                              autoprobeStatus(
                                                                                      AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                              autoprobeValidRecords(
                                                                                      0),
                                                                              responseState(
                                                                                      ResponseState::Passive_Initialize)
{
    setDefaultInvalid();
    config.emplace_back();

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireTeledyneT640::setToAutoprobe()
{
    responseState = ResponseState::Autoprobe_Passive_Initialize;
}

void AcquireTeledyneT640::setToInteractive()
{ responseState = ResponseState::Interactive_Initialize; }


ComponentOptions AcquireTeledyneT640Component::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireTeledyneT640::AcquireTeledyneT640(const ComponentOptions &,
                                         const std::string &loggingContext) : FramedInstrument(
        "t640", loggingContext),
                                                                              autoprobeStatus(
                                                                                      AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                              autoprobeValidRecords(
                                                                                      0),
                                                                              responseState(
                                                                                      ResponseState::Passive_Initialize)
{
    setDefaultInvalid();
    config.emplace_back();
}

AcquireTeledyneT640::~AcquireTeledyneT640() = default;

void AcquireTeledyneT640::invalidateLogValues(double frameTime)
{
    autoprobeValidRecords = 0;
    lastOutputData = Report();
    currentData = Report();
    lastOutputEnd = FP::undefined();
    loggingLost(frameTime);
}

SequenceValue::Transfer AcquireTeledyneT640::buildLogMeta(double time) const
{
    Q_ASSERT(!config.empty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_teledyne_t640");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    result.emplace_back(SequenceName({}, "raw_meta", "X", {"pm25"}), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
    result.back().write().metadataReal("Description").setString("Mass concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "X", {"pm10"}), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
    result.back().write().metadataReal("Description").setString("Mass concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    if (havePM1Option) {
        result.emplace_back(SequenceName({}, "raw_meta", "X", {"pm1"}), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
        result.back().write().metadataReal("Description").setString("Mass concentration");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    }

    result.emplace_back(SequenceName({}, "raw_meta", "U1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Sample RH");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample RH"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample T"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Ambient temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Ambient T"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(8);

    result.emplace_back(SequenceName({}, "raw_meta", "T3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("ASC tube jacket temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("ASC Tube"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "T4"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("LED temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("LED"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(6);

    result.emplace_back(SequenceName({}, "raw_meta", "T5"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Box temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Box"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(7);

    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Ambient pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Ambient P"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(9);

    result.emplace_back(SequenceName({}, "raw_meta", "Q1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.00");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(10);

    result.emplace_back(SequenceName({}, "raw_meta", "Q2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.00");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Bypass flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Bypass"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(11);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSPAN"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Description").setString("Span deviation");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Span Dev"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(12);

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back().write().metadataSingleFlag("BoxTemperatureWarning")
          .hash("Description")
          .setString("Box temperature warning active");
    result.back()
          .write()
          .metadataSingleFlag("BoxTemperatureWarning")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_teledyne_t640");

    result.back().write().metadataSingleFlag("FlowAlarm")
          .hash("Description")
          .setString("Flow alarm active");
    result.back()
          .write()
          .metadataSingleFlag("FlowAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_teledyne_t640");

    result.back()
          .write()
          .metadataSingleFlag("SystemFaultWarning")
          .hash("Description")
          .setString("System fault detected");
    result.back()
          .write()
          .metadataSingleFlag("SystemFaultWarning")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_teledyne_t640");

    result.back()
          .write()
          .metadataSingleFlag("SystemResetWarning")
          .hash("Description")
          .setString("System reset detected");
    result.back()
          .write()
          .metadataSingleFlag("SystemResetWarning")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_teledyne_t640");

    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureWarning")
          .hash("Description")
          .setString("Sample temperature warning active");
    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureWarning")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_teledyne_t640");

    result.back()
          .write()
          .metadataSingleFlag("BypassFlowWarning")
          .hash("Description")
          .setString("Bypass flow warning active");
    result.back()
          .write()
          .metadataSingleFlag("BypassFlowWarning")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_teledyne_t640");

    result.back()
          .write()
          .metadataSingleFlag("SystemFaultWarning")
          .hash("Description")
          .setString("System fault warning active");
    result.back()
          .write()
          .metadataSingleFlag("SystemFaultWarning")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_teledyne_t640");

    result.back()
          .write()
          .metadataSingleFlag("InternalSerialTimeout")
          .hash("Description")
          .setString("Internal serial timeout detected");
    result.back()
          .write()
          .metadataSingleFlag("InternalSerialTimeout")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_teledyne_t640");

    return result;
}

SequenceValue::Transfer AcquireTeledyneT640::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_teledyne_t640");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());


    result.emplace_back(SequenceName({}, "raw_meta", "ZXPM25"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
    result.back().write().metadataReal("Description").setString("Mass concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("PM2.5"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "ZXPM10"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
    result.back().write().metadataReal("Description").setString("Mass concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("PM10"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

    if (havePM1Option) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZXPM1"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
        result.back().write().metadataReal("Description").setString("Mass concentration");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("PM1"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    }

    result.emplace_back(SequenceName({}, "raw_meta", "PCT1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Pump duty cycle");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Pump"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(13);

    result.emplace_back(SequenceName({}, "raw_meta", "PCT2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Proportional valve cycle");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Proportional Valve"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(14);

    result.emplace_back(SequenceName({}, "raw_meta", "PCT3"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("ASC heater cycle");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("ASC heater"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(15);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Name").setString(std::string());
    result.back().write().metadataString("Realtime").hash("Units").setString(std::string());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInteger(16);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));

    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadPumpDutyCycle")
          .setString(QObject::tr("STARTING COMMS: Reading pump duty cycle"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadPM1")
          .setString(QObject::tr("STARTING COMMS: Reading PM1 concentration"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadConcentration")
          .setString(QObject::tr("STARTING COMMS: Reading mass concentration"));

    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("AlarmActive")
          .setString(QObject::tr("MULTIPLE ALARMS PRESENT"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BoxTemperatureWarning")
          .setString(QObject::tr("WARNING: Box temperature"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("FlowAlarm")
          .setString(QObject::tr("ALARM: Flow"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SystemFaultWarning")
          .setString(QObject::tr("WARNING: System fault"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SystemResetWarning")
          .setString(QObject::tr("WARNING: System reset"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("TemperatureAlarm")
          .setString(QObject::tr("ALARM: Temperature"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SystemServiceWarning")
          .setString(QObject::tr("WARNING: System service"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("OPCInstrumentWarning")
          .setString(QObject::tr("WARNING: OPC instrument"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SampleTemperatureWarning")
          .setString(QObject::tr("WARNING: Sample temperature"));

    return result;
}

SequenceMatch::Composite AcquireTeledyneT640::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    sel.append({}, {}, "X");
    return sel;
}

void AcquireTeledyneT640::logValue(double startTime,
                                   double endTime,
                                   SequenceName::Component name,
                                   Variant::Root &&value,
                                   CPD3::Data::SequenceName::Flavors &&flavors)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    SequenceValue dv({{}, "raw", std::move(name), std::move(flavors)}, std::move(value), startTime,
                     endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireTeledyneT640::realtimeValue(double time,
                                        SequenceName::Component name,
                                        Variant::Root &&value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

void AcquireTeledyneT640::emitDataRecord(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    if (!FP::defined(lastOutputEnd)) {
        lastOutputEnd = frameTime;
        return;
    }

    if (realtimeEgress && (realtimeStateUpdated || realtimeStateAlarms != currentData.alarms)) {
        realtimeStateUpdated = false;
        realtimeStateAlarms = currentData.alarms;

        if (realtimeStateAlarms.empty()) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                  FP::undefined()));
        } else if (realtimeStateAlarms.size() == 1) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root(*realtimeStateAlarms.begin()),
                                                       frameTime, FP::undefined()));
        } else {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("AlarmActive"), frameTime,
                                  FP::undefined()));
        }
    }

    if (lastOutputData == currentData)
        return;
    lastOutputData = currentData;

    double start = lastOutputEnd;
    double end = frameTime;
    lastOutputEnd = end;
    if (start >= end)
        return;

    Variant::Root F1(currentData.alarms);
    remap("F1", F1);

    Variant::Root XPM25(currentData.Xpm25);
    remap("XPM25", XPM25);

    Variant::Root XPM10(currentData.Xpm10);
    remap("XPM10", XPM10);

    Variant::Root XPM1;
    if (havePM1Option) {
        XPM1.write().setReal(currentData.Xpm1);
        remap("XPM1", XPM1);
    }

    Variant::Root U1(currentData.U1);
    remap("U1", U1);

    Variant::Root T1(currentData.T1);
    remap("T1", T1);

    Variant::Root T2(currentData.T2);
    remap("T2", T2);

    Variant::Root T3(currentData.T3);
    remap("T3", T3);

    Variant::Root T4(currentData.T4);
    remap("T4", T4);

    Variant::Root T5(currentData.T5);
    remap("T5", T5);

    Variant::Root P(currentData.P);
    remap("P", P);

    Variant::Root Q1(currentData.Q1);
    remap("Q1", Q1);

    Variant::Root Q2(currentData.Q2);
    remap("Q2", Q2);

    Variant::Root ZSPAN(currentData.ZSPAN);
    remap("ZSPAN", ZSPAN);

    Variant::Root PCT1(currentData.PCT1);
    remap("PCT1", PCT1);

    Variant::Root PCT2(currentData.PCT2);
    remap("PCT2", PCT2);

    Variant::Root PCT3(currentData.PCT3);
    remap("PCT3", PCT3);


    if (!haveEmittedLogMeta && loggingEgress) {
        haveEmittedLogMeta = true;

        loggingEgress->incomingData(buildLogMeta(start));
    }


    logValue(start, end, "F1", std::move(F1));
    logValue(start, end, "X", Variant::Root(XPM25), {"pm25"});
    logValue(start, end, "X", Variant::Root(XPM10), {"pm10"});
    if (havePM1Option) {
        logValue(start, end, "X", Variant::Root(XPM1), {"pm1"});
    }
    logValue(start, end, "T1", std::move(T1));
    logValue(start, end, "U1", std::move(U1));
    logValue(start, end, "T2", std::move(T2));
    logValue(start, end, "T3", std::move(T3));
    logValue(start, end, "T4", std::move(T4));
    logValue(start, end, "T5", std::move(T5));
    logValue(start, end, "P", std::move(P));
    logValue(start, end, "Q1", std::move(Q1));
    logValue(start, end, "Q2", std::move(Q2));
    logValue(start, end, "ZSPAN", std::move(ZSPAN));

    realtimeValue(frameTime, "ZXPM25", std::move(XPM25));
    realtimeValue(frameTime, "ZXPM10", std::move(XPM10));
    if (havePM1Option) {
        realtimeValue(frameTime, "ZXPM1", std::move(XPM1));
    }
    realtimeValue(frameTime, "PCT1", std::move(PCT1));
    realtimeValue(frameTime, "PCT2", std::move(PCT2));
    realtimeValue(frameTime, "PCT3", std::move(PCT3));
}

AcquireTeledyneT640::Report::Report() : Xpm25(FP::undefined()),
                                        Xpm10(FP::undefined()),
                                        Xpm1(FP::undefined()),
                                        U1(FP::undefined()),
                                        T1(FP::undefined()),
                                        T2(FP::undefined()),
                                        T3(FP::undefined()),
                                        T4(FP::undefined()),
                                        T5(FP::undefined()),
                                        P(FP::undefined()),
                                        Q1(FP::undefined()),
                                        Q2(FP::undefined()),
                                        ZSPAN(FP::undefined()),
                                        PCT1(FP::undefined()),
                                        PCT2(FP::undefined()),
                                        PCT3(FP::undefined())
{ }

bool AcquireTeledyneT640::Report::operator==(const Report &other) const
{
    return alarms == other.alarms &&
            FP::equal(Xpm25, other.Xpm25) &&
            FP::equal(Xpm10, other.Xpm10) &&
            FP::equal(Xpm1, other.Xpm1) &&
            FP::equal(U1, other.U1) &&
            FP::equal(T1, other.T1) &&
            FP::equal(T2, other.T2) &&
            FP::equal(T3, other.T3) &&
            FP::equal(T4, other.T4) &&
            FP::equal(T5, other.T5) &&
            FP::equal(P, other.P) &&
            FP::equal(Q1, other.Q1) &&
            FP::equal(Q2, other.Q2) &&
            FP::equal(ZSPAN, other.ZSPAN) &&
            FP::equal(PCT1, other.PCT1) &&
            FP::equal(PCT2, other.PCT2) &&
            FP::equal(PCT3, other.PCT3);
}

AcquireTeledyneT640::Command::Command() = default;

AcquireTeledyneT640::Command::Command(Util::ByteArray data) : packet(std::move(data))
{
    packet.pop_front();
}

AcquireTeledyneT640::Command::Command(const Command &) = default;

AcquireTeledyneT640::Command &AcquireTeledyneT640::Command::operator=(const Command &) = default;

AcquireTeledyneT640::Command::Command(Command &&) = default;

AcquireTeledyneT640::Command &AcquireTeledyneT640::Command::operator=(Command &&) = default;

void AcquireTeledyneT640::Command::stateDescription(Variant::Write state) const
{
    state["RawCommand"].setBinary(packet);
    if (packet.empty())
        return;

    switch (packet[0]) {
    case 0x01:
    case 0x02:
        state["Command"].setString("ReadDiscrete");
        if (packet.size() >= 5) {
            state["Address"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(1)));
            state["Count"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(3)));
        }
        break;
    case 0x03:
    case 0x04:
        state["Command"].setString("Read");
        if (packet.size() >= 5) {
            state["Register"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(1)));
            state["Count"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(3)));
        }
        break;
    case 0x06:
        state["Command"].setString("Write");
        if (packet.size() >= 5) {
            state["Register"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(1)));
            state["Value"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(3)));
        }
        break;
    case 0x10:
        state["Command"].setString("Write");
        if (packet.size() >= 5) {
            state["Register"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(1)));
            state["Count"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(3)));
        }
        break;
    case 0x17:
        state["Command"].setString("ReadWrite");
        if (packet.size() >= 9) {
            state["ReadRegister"].setInteger(
                    qFromBigEndian<quint16>(packet.data<const uchar *>(1)));
            state["ReadCount"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(3)));
            state["WriteRegister"].setInteger(
                    qFromBigEndian<quint16>(packet.data<const uchar *>(5)));
            state["WriteCount"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(7)));
        }
        break;
    default:
        break;
    }
}

AcquireTeledyneT640::Command::ResponseType AcquireTeledyneT640::Command::classify() const
{
    if (packet.empty())
        return ResponseType::Other;
    switch (packet[0]) {
    case 0x01:
    case 0x02:
        return ResponseType::InputRead;
    case 0x03:
    case 0x04:
    case 0x17:
        return ResponseType::RegisterRead;
    default:
        break;
    }
    return ResponseType::Other;
}

std::uint16_t AcquireTeledyneT640::Command::input() const
{
    if (packet.empty())
        return 0xFFFFU;

    switch (packet[0]) {
    case 0x01:
    case 0x02:
        if (packet.size() < 3)
            return 0xFFFFU;
        return qFromBigEndian<quint16>(packet.data<const uchar *>(1));
    default:
        break;
    }
    return 0xFFFFU;
}

AcquireTeledyneT640::RegisterType AcquireTeledyneT640::Command::reg() const
{
    if (packet.empty())
        return RegisterType::INVALID;

    switch (packet[0]) {
    case 0x03:
    case 0x04:
    case 0x06:
    case 0x10:
    case 0x17:
        if (packet.size() < 3)
            return RegisterType::INVALID;
        return static_cast<RegisterType>(qFromBigEndian<quint16>(packet.data<const uchar *>(1)));
    default:
        break;
    }
    return RegisterType::INVALID;
}

bool AcquireTeledyneT640::Command::checkResponse(const Util::ByteView &response) const
{
    if (packet.empty())
        return false;
    if (response.empty())
        return false;

    if (response[0] & 0x80) {
        if (response.size() < 2)
            return false;
        return true;
    }

    switch (packet[0]) {
    case 0x01:
    case 0x02: {
        if (packet.size() < 5)
            return false;
        auto expectedInputs = qFromBigEndian<quint16>(packet.data<const uchar *>(3));
        if (expectedInputs < 1 || expectedInputs > 251 * 8)
            return false;
        if (response.size() <= 1)
            return false;
        if (response[0] != packet[0])
            return false;
        auto expectedBytes = expectedInputs / 8;
        if (expectedInputs % 8)
            expectedBytes++;
        if (response[1] != expectedBytes)
            return false;
        if (response.size() != static_cast<std::size_t>(expectedBytes + 2))
            return false;
        break;
    }
    case 0x03:
    case 0x04:
    case 0x17: {
        if (packet.size() < 5)
            return false;
        auto expectedReg = qFromBigEndian<quint16>(packet.data<const uchar *>(3));
        if (expectedReg < 1 || expectedReg > 0x7D)
            return false;
        if (response.size() <= 2)
            return false;
        if (response[0] != packet[0])
            return false;
        if (response[1] != expectedReg * 2)
            return false;
        if (response.size() != static_cast<std::size_t>(expectedReg * 2 + 2))
            return false;
        break;
    }
    case 0x06:
    case 0x10:
        if (packet.size() < 5)
            return false;
        if (response.size() < 5)
            return false;
        if (qFromBigEndian<quint16>(packet.data<const uchar *>(1)) !=
                qFromBigEndian<quint16>(response.data<const uchar *>(1)))
            return false;
        if (qFromBigEndian<quint16>(packet.data<const uchar *>(3)) !=
                qFromBigEndian<quint16>(response.data<const uchar *>(3)))
            return false;
        break;
    default:
        return false;
    }

    return true;
}

Util::ByteView AcquireTeledyneT640::Command::toBytes(const Util::ByteView &response) const
{
    if (packet.empty())
        return {};
    switch (packet[0]) {
    case 0x03:
    case 0x04:
    case 0x17: {
        auto byteCount = response[1];
        return response.mid(2, byteCount);
    }
    default:
        break;
    }
    return {};
}

std::vector<
        std::uint16_t> AcquireTeledyneT640::Command::toData(const Util::ByteView &response) const
{
    auto raw = toBytes(response);
    if (raw.empty())
        return {};
    std::vector<std::uint16_t> result;
    for (auto ptr = raw.data<const uchar *>(), end = ptr + raw.size() - 2; ptr <= end; ptr += 2) {
        result.emplace_back(qFromBigEndian<quint16>(ptr));
    }
    return result;
}

float AcquireTeledyneT640::Command::toFloat(const Util::ByteView &response) const
{
    auto raw = toBytes(response);
    if (raw.empty())
        return 0;
    auto i = qFromBigEndian<quint32>(raw.data<const uchar *>());
    float result;
    std::memcpy(&result, &i, 4);
    return result;
}

std::vector<bool> AcquireTeledyneT640::Command::toBits(const CPD3::Util::ByteView &response) const
{
    if (packet.size() < 5)
        return {};
    switch (packet[0]) {
    case 0x01:
    case 0x02: {
        std::vector<bool> result;
        auto byteCount = response[1];
        auto bitCount = qFromBigEndian<quint16>(packet.data<const uchar *>(3));
        for (auto b : response.mid(2, byteCount)) {
            for (std::uint16_t bit = 0, remaining = std::min<std::uint16_t>(8, bitCount);
                    bit < remaining;
                    bit++) {
                result.push_back((b & (1U << bit)) != 0);
            }
            if (bitCount <= 8)
                break;
            bitCount -= 8;
        }
        return result;
    }
    default:
        break;
    }
    return {};
}

void AcquireTeledyneT640::sendRequest(CPD3::Util::ByteArray &&request)
{
    request.push_front(config.front().address);

    queuedCommands.emplace_back(request);

    Util::ByteArray header;
    qToBigEndian<quint16>(0, header.tail<uchar *>(2));
    qToBigEndian<quint16>(0, header.tail<uchar *>(2));
    qToBigEndian<quint16>(request.size(), header.tail<uchar *>(2));
    header += std::move(request);
    request = std::move(header);

    if (controlStream) {
        controlStream->writeControl(std::move(request));
    }
}

void AcquireTeledyneT640::sendReadCommand(std::uint16_t reg, std::uint8_t count)
{
    Util::ByteArray request;
    request.push_back(0x04);
    qToBigEndian<quint16>(reg, request.tail<uchar *>(2));
    qToBigEndian<quint16>(count, request.tail<uchar *>(2));
    return sendRequest(std::move(request));
}

void AcquireTeledyneT640::sendReadCommand(RegisterType reg)
{ return sendReadCommand(static_cast<std::uint16_t>(reg), 2); }

void AcquireTeledyneT640::sendReadInputs(std::uint16_t first, std::uint16_t count)
{
    Util::ByteArray request;
    request.push_back(0x02);
    qToBigEndian<quint16>(first, request.tail<uchar *>(2));
    qToBigEndian<quint16>(count, request.tail<uchar *>(2));
    return sendRequest(std::move(request));
}

void AcquireTeledyneT640::sendReadInputs()
{ return sendReadInputs(0, instrumentAlarmTranslation.size()); }

void AcquireTeledyneT640::sendWriteCommand(std::uint16_t reg,
                                           const std::vector<std::uint16_t> &data)
{
    Util::ByteArray request;
    switch (data.size()) {
    case 0:
        return;
    case 1:
        request.push_back(0x06);
        qToBigEndian<quint16>(reg, request.tail<uchar *>(2));
        qToBigEndian<quint16>(data.front(), request.tail<uchar *>(2));
        break;
    default:
        Q_ASSERT(data.size() <= 0x7B);
        request.push_back(0x10);
        qToBigEndian<quint16>(reg, request.tail<uchar *>(2));
        qToBigEndian<quint16>(data.size(), request.tail<uchar *>(2));
        request.push_back(data.size() * 2);
        for (auto add : data) {
            qToBigEndian<quint16>(add, request.tail<uchar *>(2));
        }
        break;
    }
    return sendRequest(std::move(request));
}

void AcquireTeledyneT640::sendWriteCommand(RegisterType reg, const std::vector<std::uint16_t> &data)
{ return sendWriteCommand(static_cast<std::uint16_t>(reg), data); }

void AcquireTeledyneT640::sendWriteCommand(RegisterType reg, std::uint16_t value)
{
    Util::ByteArray request;
    request.push_back(0x06);
    qToBigEndian<quint16>(static_cast<std::uint16_t>(reg), request.tail<uchar *>(2));
    qToBigEndian<quint16>(value, request.tail<uchar *>(2));
    return sendRequest(std::move(request));
}

Variant::Write AcquireTeledyneT640::invalidResponseBase(const Util::ByteArray &frame,
                                                        double time,
                                                        int code)
{
    Variant::Write info = Variant::Write::empty();
    info.hash("Code").setInteger(code);
    info.hash("Response").setBinary(frame);

    switch (responseState) {
    case ResponseState::Passive_Initialize:
        info.hash("ResponseState").setString("PassiveInitialize");
        timeoutAt(FP::undefined());
        responseState = ResponseState::Passive_Wait;
        break;
    case ResponseState::Passive_Wait:
        info.hash("ResponseState").setString("PassiveWait");
        timeoutAt(FP::undefined());
        responseState = ResponseState::Passive_Wait;
        break;
    case ResponseState::Passive_Run:
        info.hash("ResponseState").setString("PassiveRun");
        timeoutAt(FP::undefined());
        responseState = ResponseState::Passive_Wait;
        break;
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Restart_Wait:
        return Variant::Write::empty();
    case ResponseState::Interactive_Start_Read_Alarms:
    case ResponseState::Interactive_Start_Read_PumpDutyCycle:
        responseState = ResponseState::Interactive_Restart_Wait;
        if (controlStream)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(time + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        generalStatusUpdated();
        invalidateLogValues(time);
        return Variant::Write::empty();

    case ResponseState::Interactive_Start_Read_PM1:
        responseState = ResponseState::Interactive_Start_Read_Concentration;
        timeoutAt(time + 5.0);
        sendReadCommand(RegisterType::PM10Concentration);

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadConcentration"), time, FP::undefined()));
        }

        if (havePM1Option) {
            havePM1Option = false;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }

        return Variant::Write::empty();

    case ResponseState::Interactive_Start_Read_Concentration:
        info.hash("ResponseState").setString("InteractiveStartReadConcentration");

        responseState = ResponseState::Interactive_Restart_Wait;
        if (controlStream)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(time + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        break;


    case ResponseState::Interactive_Initialize:
        info.hash("ResponseState").setString("InteractiveInitialize");
        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 10.0);
        discardData(time + 1.0);
        break;
    case ResponseState::Interactive_Run_Read:
        info.hash("ResponseState").setString("InteractiveRunRead");
        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 10.0);
        discardData(time + 1.0);
        break;
    case ResponseState::Interactive_Run_Wait:
        info.hash("ResponseState").setString("InteractiveRunWait");
        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 10.0);
        discardData(time + 1.0);
        break;
    }
    generalStatusUpdated();

    invalidateLogValues(time);

    return info;
}

void AcquireTeledyneT640::invalidResponse(const Util::ByteArray &frame, double time, int code)
{
    Variant::Write info = invalidResponseBase(frame, time, code);
    if (!info.exists())
        return;

    qCDebug(log) << "Response at" << Logging::time(time) << ":" << frame << "rejected with code"
                 << code;

    event(time,
          QObject::tr("Invalid response received (code %1).  Communications dropped.").arg(code),
          true, info);
}

void AcquireTeledyneT640::invalidResponse(const Util::ByteArray &frame,
                                          double time,
                                          const Command &command,
                                          int code)
{
    Variant::Write info = invalidResponseBase(frame, time, code);
    if (!info.exists())
        return;

    qCDebug(log) << "Response at" << Logging::time(time) << ":" << frame << "in response to"
                 << static_cast<int>(command.reg()) << "rejected with code" << code;

    command.stateDescription(info);
    event(time,
          QObject::tr("Invalid response received (code %1).  Communications dropped.").arg(code),
          true, info);
}

void AcquireTeledyneT640::configurationAdvance(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.empty());
}

void AcquireTeledyneT640::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    configurationAdvance(frameTime);

    Util::ByteView response;
    if (frame.size() < 2)
        return invalidResponse(frame, frameTime, 1);
    if (frame.front() != config.front().address)
        return;
    response = frame.mid(1);

    Q_ASSERT(response.size() >= 2);

    if (queuedCommands.empty())
        return invalidResponse(frame, frameTime, 4);

    auto command = std::move(queuedCommands.front());
    queuedCommands.pop_front();
    if (!command.checkResponse(response))
        return invalidResponse(frame, frameTime, command, 5);
    if (response[0] & 0x80)
        return invalidResponse(frame, frameTime, command, 100 + response[1]);

    if (!haveEmittedRealtimeMeta && realtimeEgress) {
        haveEmittedRealtimeMeta = true;

        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    auto commandType = command.classify();
    if (commandType == Command::ResponseType::RegisterRead) {
        switch (command.reg()) {
        default:
            break;

        case RegisterType::PM10Concentration:
            currentData.Xpm10 = command.toFloat(response);
            if (FP::defined(currentData.Xpm10) &&
                    currentData.Xpm10 > -20 && currentData.Xpm10 < 5000)
                ++autoprobeValidRecords;
            break;
        case RegisterType::PM25Concentration:
            currentData.Xpm25 = command.toFloat(response);
            if (FP::defined(currentData.Xpm25) &&
                    currentData.Xpm25 > -20 &&
                    currentData.Xpm25 < 5000)
                ++autoprobeValidRecords;
            break;
        case RegisterType::PM1Concentration:
            currentData.Xpm1 = command.toFloat(response);
            if (FP::defined(currentData.Xpm1) && currentData.Xpm1 > -20 && currentData.Xpm1 < 5000)
                ++autoprobeValidRecords;
            break;

        case RegisterType::RH:
            currentData.U1 = command.toFloat(response);
            if (FP::defined(currentData.U1) && currentData.U1 > -1 && currentData.U1 < 150)
                ++autoprobeValidRecords;
            break;
        case RegisterType::SampleTemperature:
            currentData.T1 = command.toFloat(response);
            if (FP::defined(currentData.T1) && currentData.T1 > -50 && currentData.T1 < 150)
                ++autoprobeValidRecords;
            break;
        case RegisterType::AmbientTemperature:
            currentData.T2 = command.toFloat(response);
            if (FP::defined(currentData.T2) && currentData.T2 > -50 && currentData.T2 < 150)
                ++autoprobeValidRecords;
            break;
        case RegisterType::ASCTubeTemperature:
            currentData.T3 = command.toFloat(response);
            if (FP::defined(currentData.T3) && currentData.T3 > -50 && currentData.T3 < 150)
                ++autoprobeValidRecords;
            break;
        case RegisterType::LEDTemperature:
            currentData.T4 = command.toFloat(response);
            if (FP::defined(currentData.T4) && currentData.T4 > -50 && currentData.T4 < 150)
                ++autoprobeValidRecords;
            break;
        case RegisterType::BoxTemperature:
            currentData.T5 = command.toFloat(response);
            if (FP::defined(currentData.T5) && currentData.T5 > -50 && currentData.T5 < 150)
                ++autoprobeValidRecords;
            break;
        case RegisterType::AmbientPressure:
            currentData.P = command.toFloat(response) * 10;
            if (FP::defined(currentData.P) && currentData.P > 10 && currentData.P < 1100)
                ++autoprobeValidRecords;
            break;
        case RegisterType::SampleFlow:
            currentData.Q1 = command.toFloat(response);
            break;
        case RegisterType::BypassFlow:
            currentData.Q2 = command.toFloat(response);
            break;
        case RegisterType::SpanDeviation:
            currentData.ZSPAN = command.toFloat(response);
            break;

        case RegisterType::PumpDutyCycle:
            currentData.PCT1 = command.toFloat(response);
            break;
        case RegisterType::ProportionalValveDutyCycle:
            currentData.PCT2 = command.toFloat(response);
            break;
        case RegisterType::ASCHeaterDutyCycle:
            currentData.PCT3 = command.toFloat(response);
            break;
        }
    } else if (commandType == Command::ResponseType::InputRead) {
        auto startAlarm = command.input();
        if (startAlarm < instrumentAlarmTranslation.size()) {
            auto bits = command.toBits(response);
            for (auto index = startAlarm; index < instrumentAlarmTranslation.size(); index++) {
                const auto &code = instrumentAlarmTranslation[index];
                if (code.empty())
                    continue;
                if (bits[index - startAlarm]) {
                    currentData.alarms.insert(code);
                } else {
                    currentData.alarms.erase(code);
                }
            }
        }
    }

    double pollDelay = config.front().pollInterval;
    if (!FP::defined(pollDelay) || pollDelay < 0.0)
        pollDelay = 0.0;
    switch (responseState) {
    case ResponseState::Passive_Run:
        timeoutAt(frameTime + pollDelay + 10.0);
        emitDataRecord(frameTime);
        break;

    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
        if (autoprobeValidRecords > 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);
            responseState = ResponseState::Passive_Run;
            generalStatusUpdated();

            timeoutAt(frameTime + pollDelay + 10.0);

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        }
        break;

    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
        timeoutAt(frameTime + pollDelay + 10.0);
        if (autoprobeValidRecords > 3) {
            qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

            responseState = ResponseState::Passive_Run;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveAutoprobeWait");
            event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
        }
        break;

    case ResponseState::Interactive_Start_Read_Alarms:
        if (command.input() != 0 || command.classify() != Command::ResponseType::InputRead)
            return invalidResponse(frame, frameTime, command, 1000);
        responseState = ResponseState::Interactive_Start_Read_PumpDutyCycle;
        timeoutAt(frameTime + 5.0);
        sendReadCommand(RegisterType::PumpDutyCycle);

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadPumpDutyCycle"), frameTime, FP::undefined()));
        }
        break;
    case ResponseState::Interactive_Start_Read_PumpDutyCycle:
        if (command.reg() != RegisterType::PumpDutyCycle ||
                command.classify() != Command::ResponseType::RegisterRead)
            return invalidResponse(frame, frameTime, command, 1001);
        if (!FP::defined(currentData.PCT1) || currentData.PCT1 < 0.0 || currentData.PCT1 > 100.0)
            return invalidResponse(frame, frameTime, command, 1002);
        responseState = ResponseState::Interactive_Start_Read_PM1;
        timeoutAt(frameTime + 5.0);
        sendReadCommand(RegisterType::PM1Concentration);

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveReadPM"),
                                  frameTime, FP::undefined()));
        }
        break;
    case ResponseState::Interactive_Start_Read_PM1:
        if (command.reg() != RegisterType::PM1Concentration ||
                command.classify() != Command::ResponseType::RegisterRead)
            return invalidResponse(frame, frameTime, command, 1001);
        if (!FP::defined(currentData.Xpm1) || currentData.Xpm1 < -20.0 || currentData.Xpm1 > 1E5)
            return invalidResponse(frame, frameTime, command, 1002);
        responseState = ResponseState::Interactive_Start_Read_Concentration;
        timeoutAt(frameTime + 5.0);
        sendReadCommand(RegisterType::PM10Concentration);

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadConcentration"), frameTime, FP::undefined()));
        }
        if (!havePM1Option) {
            havePM1Option = true;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
        break;
    case ResponseState::Interactive_Start_Read_Concentration: {
        if (command.reg() != RegisterType::PM10Concentration ||
                command.classify() != Command::ResponseType::RegisterRead)
            return invalidResponse(frame, frameTime, command, 1003);
        if (!FP::defined(currentData.Xpm10) || currentData.Xpm10 < -100.0)
            return invalidResponse(frame, frameTime, command, 1004);

        qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

        lastOutputData = Report();
        currentData = Report();
        lastOutputEnd = FP::undefined();

        responseState = ResponseState::Interactive_Run_Read;
        timeoutAt(frameTime + 10.0);
        sendReadInputs();

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        generalStatusUpdated();

        Variant::Write info = Variant::Write::empty();
        info.hash("ResponseState").setString("StartInteractiveRead");
        event(frameTime, QObject::tr("Communications established."), false, info);

        realtimeStateUpdated = true;
        break;
    }

    case ResponseState::Interactive_Run_Read: {
        if (command.classify() == Command::ResponseType::InputRead) {
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::PM10Concentration);
            break;
        }

        switch (command.reg()) {
        case RegisterType::PM10Concentration:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::PM25Concentration);
            break;
        case RegisterType::PM25Concentration:
            if (!havePM1Option) {
                timeoutAt(frameTime + 10.0);
                sendReadCommand(RegisterType::RH);
                emitDataRecord(frameTime);
            } else {
                timeoutAt(frameTime + 10.0);
                sendReadCommand(RegisterType::PM1Concentration);
                emitDataRecord(frameTime);
            }
            break;
        case RegisterType::PM1Concentration:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::RH);
            emitDataRecord(frameTime);
            break;
        case RegisterType::RH:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::SampleTemperature);
            break;
        case RegisterType::SampleTemperature:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::AmbientTemperature);
            break;
        case RegisterType::AmbientTemperature:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::ASCTubeTemperature);
            break;
        case RegisterType::ASCTubeTemperature:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::LEDTemperature);
            break;
        case RegisterType::LEDTemperature:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::BoxTemperature);
            break;
        case RegisterType::BoxTemperature:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::AmbientPressure);
            break;
        case RegisterType::AmbientPressure:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::SampleFlow);
            break;
        case RegisterType::SampleFlow:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::BypassFlow);
            break;
        case RegisterType::BypassFlow:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::SpanDeviation);
            break;
        case RegisterType::SpanDeviation:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::PumpDutyCycle);
            break;
        case RegisterType::PumpDutyCycle:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::ProportionalValveDutyCycle);
            break;
        case RegisterType::ProportionalValveDutyCycle:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::ASCHeaterDutyCycle);
            break;

        case RegisterType::ASCHeaterDutyCycle:
            /* Fall through */
        default:
            if (FP::defined(pollDelay) && pollDelay > 0.0) {
                responseState = ResponseState::Interactive_Run_Wait;
                timeoutAt(frameTime + pollDelay * 2.0 + 5.0);
                discardData(frameTime + pollDelay);
            } else {
                responseState = ResponseState::Interactive_Run_Read;
                timeoutAt(frameTime + 10.0);
                sendReadInputs();
            }
            break;
        }

        break;
    }

    default:
        break;
    }
}

void AcquireTeledyneT640::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_Read_Alarms:
    case ResponseState::Interactive_Start_Read_PumpDutyCycle:
    case ResponseState::Interactive_Start_Read_Concentration:
        qCDebug(log) << "Timeout in interactive start state" << static_cast<int>(responseState)
                     << "at" << Logging::time(frameTime);

        invalidateLogValues(frameTime);

        responseState = ResponseState::Interactive_Restart_Wait;
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);
        autoprobeValidRecords = 0;
        generalStatusUpdated();

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        queuedCommands.clear();
        break;

    case ResponseState::Interactive_Start_Read_PM1:
        responseState = ResponseState::Interactive_Start_Read_Concentration;
        timeoutAt(frameTime + 5.0);
        sendReadCommand(RegisterType::PM10Concentration);

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadConcentration"), frameTime, FP::undefined()));
        }

        if (havePM1Option) {
            havePM1Option = false;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
        break;


    case ResponseState::Passive_Run: {
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = ResponseState::Passive_Wait;
        generalStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            if (!queuedCommands.empty())
                queuedCommands.front().stateDescription(info);
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        queuedCommands.clear();
        invalidateLogValues(frameTime);
        break;
    }

    case ResponseState::Interactive_Run_Read:
    case ResponseState::Interactive_Run_Wait: {
        qCDebug(log) << "Timeout in interactive run state" << static_cast<int>(responseState)
                     << "at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);

        generalStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            if (!queuedCommands.empty())
                queuedCommands.front().stateDescription(info);
            switch (responseState) {
            case ResponseState::Interactive_Run_Read:
                info.hash("ResponseState").setString("Read");
                break;
            case ResponseState::Interactive_Run_Wait:
                info.hash("ResponseState").setString("PollWait");
                break;
            default:
                break;
            }
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        queuedCommands.clear();
        break;
    }

    case ResponseState::Passive_Initialize:
    case ResponseState::Passive_Wait:
        responseState = ResponseState::Passive_Wait;
        autoprobeValidRecords = 0;
        invalidateLogValues(frameTime);
        queuedCommands.clear();
        break;

    case ResponseState::Autoprobe_Passive_Wait: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case ResponseState::Autoprobe_Passive_Initialize:
        responseState = ResponseState::Autoprobe_Passive_Wait;
        autoprobeValidRecords = 0;
        invalidateLogValues(frameTime);
        queuedCommands.clear();
        break;

    case ResponseState::Interactive_Restart_Wait:
    case ResponseState::Interactive_Initialize:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);

        generalStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }

        queuedCommands.clear();
        break;
    }
}

void AcquireTeledyneT640::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Interactive_Start_Flush:
        responseState = ResponseState::Interactive_Start_Read_Alarms;

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadSerialNumber"), frameTime, FP::undefined()));
        }

        timeoutAt(frameTime + 5.0);
        sendReadInputs();
        break;

    case ResponseState::Interactive_Restart_Wait:
        if (controlStream) {
            controlStream->resetControl();
        }
        /* Fall through */
    case ResponseState::Interactive_Initialize:
        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case ResponseState::Interactive_Run_Wait:
        responseState = ResponseState::Interactive_Run_Read;
        timeoutAt(frameTime + 10.0);
        sendReadInputs();
        break;

    default:
        break;
    }
}

static std::size_t requestSize(const Util::ByteView &data)
{
    Q_ASSERT(data.size() >= 1);

    auto functionCode = data[1];
    switch (functionCode) {
    case 0x03:
    case 0x04:
    case 0x06:
        if (data.size() < 6)
            return data.npos;
        return 2 + 4;
    case 0x10: {
        if (data.size() < 7)
            return data.npos;
        auto len = data[6];
        if (static_cast<std::size_t>(2 + 5 + len) > data.size())
            return data.npos;
        return 2 + 5 + len;
    }
    case 0x17: {
        if (data.size() < 11)
            return data.npos;
        auto len = data[10];
        if (static_cast<std::size_t>(2 + 9 + len) > data.size())
            return data.npos;
        return 2 + 9 + len;
    }
    default:
        break;
    }

    return 2;
}

void AcquireTeledyneT640::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    configurationAdvance(frameTime);

    if (frame.size() < 2)
        return;
    if (frame.front() != config.front().address)
        return;

    bool isFirstCommand = queuedCommands.empty();
    if (frame.size() < requestSize(frame))
        return;
    queuedCommands.emplace_front(frame);


    switch (responseState) {
    case ResponseState::Autoprobe_Passive_Initialize:
        responseState = ResponseState::Autoprobe_Passive_Wait;
    case ResponseState::Autoprobe_Passive_Wait:
        if (isFirstCommand)
            timeoutAt(frameTime + 10.0);
        break;
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Run:
        if (isFirstCommand)
            timeoutAt(frameTime + 2.0);
        break;

    default:
        break;
    }
}

std::size_t AcquireTeledyneT640::dataFrameStart(std::size_t offset,
                                                const Util::ByteArray &input) const
{
    auto addr = config.front().address;
    for (auto max = input.size(); offset < max; ++offset) {
        if (offset + 6 >= max)
            break;
        auto protocol = qFromBigEndian<quint16>(input.data<const uchar *>(offset + 2));
        if (protocol != 0)
            continue;
        auto len = qFromBigEndian<quint16>(input.data<const uchar *>(offset + 4));
        if (len <= 1)
            continue;
        if (offset + 6 + len > max)
            break;
        if (input[offset + 6] != addr) {
            offset += 6 + len;
            continue;
        }
        return offset + 6;
    }
    return input.npos;
}

std::size_t AcquireTeledyneT640::dataFrameEnd(std::size_t start, const Util::ByteArray &input) const
{
    if (start < 6)
        return input.npos;
    Q_ASSERT(input[start] == config.front().address);
    auto len = qFromBigEndian<quint16>(input.data<const uchar *>(start - 2));
    if (len <= 1)
        return input.npos;
    return start + len;
}

std::size_t AcquireTeledyneT640::controlFrameStart(std::size_t offset,
                                                   const Util::ByteArray &input) const
{
    auto addr = config.front().address;
    for (auto max = input.size(); offset < max; ++offset) {
        if (offset + 6 >= max)
            break;
        auto protocol = qFromBigEndian<quint16>(input.data<const uchar *>(offset + 2));
        if (protocol != 0)
            continue;
        auto len = qFromBigEndian<quint16>(input.data<const uchar *>(offset + 4));
        if (len <= 1)
            continue;
        if (offset + 6 + len > max)
            break;
        if (input[offset + 6] != addr) {
            offset += 6 + len;
            continue;
        }
        return offset + 6;
    }
    return input.npos;
}

std::size_t AcquireTeledyneT640::controlFrameEnd(std::size_t start,
                                                 const Util::ByteArray &input) const
{
    if (start < 6)
        return input.npos;
    Q_ASSERT(input[start] == config.front().address);
    auto len = qFromBigEndian<quint16>(input.data<const uchar *>(start - 2));
    if (len <= 1)
        return input.npos;
    return start + len;
}

void AcquireTeledyneT640::limitDataBuffer(Util::ByteArray &buffer)
{
    static constexpr std::size_t maxmimumSize = 0xFFFF + 6;
    if (buffer.size() <= maxmimumSize)
        return;

    auto firstAddressFound = buffer.indexOf(config.front().address);
    while (firstAddressFound != buffer.npos &&
            firstAddressFound <= 6 &&
            buffer.size() > maxmimumSize) {
        buffer.pop_front(firstAddressFound + 1);
        firstAddressFound = buffer.indexOf(config.front().address);
    }

    if (firstAddressFound == buffer.npos) {
        buffer.clear();
        return;
    }

    buffer.pop_front(firstAddressFound - 6);
}


Variant::Root AcquireTeledyneT640::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireTeledyneT640::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case ResponseState::Passive_Run:
    case ResponseState::Interactive_Run_Read:
    case ResponseState::Interactive_Run_Wait:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

AcquisitionInterface::AutoprobeStatus AcquireTeledyneT640::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireTeledyneT640::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case ResponseState::Interactive_Run_Read:
    case ResponseState::Interactive_Run_Wait:
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << static_cast<int>(responseState) << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_Read_Alarms:
    case ResponseState::Interactive_Start_Read_PumpDutyCycle:
    case ResponseState::Interactive_Start_Read_PM1:
    case ResponseState::Interactive_Start_Read_Concentration:
    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        qCDebug(log) << "Interactive autoprobe started from state"
                     << static_cast<int>(responseState) << "at" << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 10.0);
        discardData(time + 1.0);

        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireTeledyneT640::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    Q_ASSERT(!config.empty());
    double unpolledResponseTime = config.front().pollInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 10.0;
    else
        unpolledResponseTime += 5.0;

    discardData(time + 0.5);
    timeoutAt(time + unpolledResponseTime * 4.0 + 6.0);

    qCDebug(log) << "Reset to passive autoprobe from state" << static_cast<int>(responseState)
                 << "at" << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = ResponseState::Autoprobe_Passive_Wait;
    autoprobeValidRecords = 0;
}

void AcquireTeledyneT640::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 5.0);

    switch (responseState) {
    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_Read_Alarms:
    case ResponseState::Interactive_Start_Read_PumpDutyCycle:
    case ResponseState::Interactive_Start_Read_PM1:
    case ResponseState::Interactive_Start_Read_Concentration:
    case ResponseState::Interactive_Run_Read:
    case ResponseState::Interactive_Run_Wait:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << static_cast<int>(responseState)
                     << "to interactive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        qCDebug(log) << "Promoted from passive state" << static_cast<int>(responseState)
                     << "to interactive acquisition at" << Logging::time(time);

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 30.0);
        discardData(time + 1.0);

        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireTeledyneT640::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    Q_ASSERT(!config.empty());
    double unpolledResponseTime = config.front().pollInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 10.0;
    else
        unpolledResponseTime += 5.0;

    timeoutAt(time + unpolledResponseTime * 2.0 + 1.0);

    switch (responseState) {
    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from autoprobe passive state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        responseState = ResponseState::Passive_Initialize;
        break;

    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_Read_Alarms:
    case ResponseState::Interactive_Start_Read_PumpDutyCycle:
    case ResponseState::Interactive_Start_Read_PM1:
    case ResponseState::Interactive_Start_Read_Concentration:
    case ResponseState::Interactive_Run_Read:
    case ResponseState::Interactive_Run_Wait:
    case ResponseState::Interactive_Restart_Wait:
        qCDebug(log) << "Promoted from state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);

        discardData(FP::undefined());

        responseState = ResponseState::Passive_Initialize;
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireTeledyneT640::getDefaults()
{
    AutomaticDefaults result;
    result.name = "M$1$2";
    return result;
}


ComponentOptions AcquireTeledyneT640Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireTeledyneT640Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireTeledyneT640Component::createAcquisitionPassive(const ComponentOptions &options,
                                                                                     const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(new AcquireTeledyneT640(options, loggingContext));
}

std::unique_ptr<
        AcquisitionInterface> AcquireTeledyneT640Component::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(new AcquireTeledyneT640(config, loggingContext));
}

std::unique_ptr<
        AcquisitionInterface> AcquireTeledyneT640Component::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{
    std::unique_ptr<AcquireTeledyneT640> i(new AcquireTeledyneT640(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireTeledyneT640Component::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireTeledyneT640> i(new AcquireTeledyneT640(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
