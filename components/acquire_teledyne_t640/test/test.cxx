/*
 * Copyright (c) 2021 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <array>
#include <cstdint>
#include <cstring>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>
#include <QtEndian>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"
#include "algorithms/crc.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    std::uint8_t address;
    double updateRemaining;

    Algorithms::CRC16 crc;

    std::array<bool, 9> alarms;

    double Xpm25;
    double Xpm10;
    double Xpm1;

    double U1;
    double T1;
    double T2;
    double T3;
    double T4;
    double T5;
    double P;

    double Q1;
    double Q2;
    double ZSPAN;

    double PCT1;
    double PCT2;
    double PCT3;

    ModelInstrument() : incoming(), outgoing(), address(1), updateRemaining(0.0)
    {
        alarms.fill(false);

        Xpm25 = 24.25;
        Xpm10 = 23.25;
        Xpm1 = 22.5;

        U1 = 66.0;
        T1 = 24.0;
        T2 = 24.5;
        T3 = 25.0;
        T4 = 25.5;
        T5 = 26.0;
        P = 825.0;

        Q1 = 0.5;
        Q2 = 0.75;

        ZSPAN = 2.5;

        PCT1 = 20.0;
        PCT2 = 21.0;
        PCT3 = 22.0;
    }

    void sendResponse(const QByteArray &data)
    {
        QByteArray header(6, 0);
        qToBigEndian<quint16>(0, reinterpret_cast<uchar *>(header.data()));
        qToBigEndian<quint16>(0, reinterpret_cast<uchar *>(header.data()) + 2);
        qToBigEndian<quint16>(data.size(), reinterpret_cast<uchar *>(header.data()) + 4);
        outgoing += header;
        outgoing += data;
    }

    void sendError(int functionCode, int exceptionCode = 0x01)
    {
        QByteArray data(2, 0);
        data[0] = functionCode | 0x80;
        data[1] = exceptionCode;
        return sendResponse(data);
    }

    QByteArray packFloat(float value)
    {
        quint32 i;
        std::memcpy(&i, &value, 4);
        QByteArray result(4, 0);
        qToBigEndian<quint32>(i, reinterpret_cast<uchar *>(result.data()));
        return result;
    }

    static std::size_t requestSize(const Util::ByteView &data)
    {
        Q_ASSERT(data.size() >= 1);

        auto functionCode = data[1];
        switch (functionCode) {
        case 0x01:
        case 0x02:
        case 0x03:
        case 0x04:
        case 0x06:
            if (data.size() < 6)
                return data.npos;
            return 2 + 4;
        case 0x10: {
            if (data.size() < 7)
                return data.npos;
            auto len = data[6];
            if (static_cast<std::size_t>(2 + 5 + len) > data.size())
                return data.npos;
            return 2 + 5 + len;
        }
        case 0x17: {
            if (data.size() < 11)
                return data.npos;
            auto len = data[10];
            if (static_cast<std::size_t>(2 + 9 + len) > data.size())
                return data.npos;
            return 2 + 9 + len;
        }
        default:
            break;
        }

        return 2;
    }

    QByteArray nextFrame()
    {
        for (; incoming.size() > 7; incoming.remove(0, 1)) {
            //auto txID = qFromBigEndian<quint16>(reinterpret_cast<const uchar *>(incoming.data()));
            auto protocolID =
                    qFromBigEndian<quint16>(reinterpret_cast<const uchar *>(incoming.data()) + 2);
            auto length =
                    qFromBigEndian<quint16>(reinterpret_cast<const uchar *>(incoming.data()) + 4);
            //auto unitID = static_cast<std::uint8_t>(incoming[6]);
            if (protocolID != 0)
                continue;
            if (incoming.size() - 6 < length)
                return {};
            auto result = incoming.mid(6, length);
            incoming.remove(0, length + 6);
            return result;
        }
        return {};
    }

    void advance(double seconds)
    {
        updateRemaining -= seconds;
        if (updateRemaining <= 0.0) {
            while (updateRemaining <= 0.0) {
                updateRemaining += 1.0;
            }
            alarms[0] = !alarms[0];
        }

        QByteArray request;
        while (!(request = nextFrame()).isEmpty()) {
            auto expectedSize = requestSize(request);
            if (static_cast<int>(expectedSize) != request.size())
                continue;

            std::uint8_t functionCode = request[1];
            switch (functionCode) {
            case 0x01:
            case 0x02: {
                auto start = qFromBigEndian<quint16>(
                        reinterpret_cast<const uchar *>(request.data()) + 2);
                int count = qFromBigEndian<quint16>(
                        reinterpret_cast<const uchar *>(request.data()) + 4);
                auto totalSize = count / 8;
                if (count % 8)
                    totalSize++;
                QByteArray response(totalSize, 0);
                for (auto bit = start; count >= 0; count--, bit++) {
                    if (bit >= alarms.size())
                        break;
                    if (!alarms[bit])
                        continue;
                    auto pos = (bit - start);
                    std::uint8_t add = response[pos / 8];
                    add |= (1U << (pos % 8));
                    response[pos / 8] = add;
                }

                response.insert(0, response.size());
                response.insert(0, functionCode);
                response.insert(0, address);
                sendResponse(response);
                break;
            }
            case 0x03:
            case 0x04: {
                auto reg = qFromBigEndian<quint16>(
                        reinterpret_cast<const uchar *>(request.data()) + 2);
                auto count = qFromBigEndian<quint16>(
                        reinterpret_cast<const uchar *>(request.data()) + 4);

                QByteArray response;
                switch (reg) {
                case 6:
                    response = packFloat(Xpm10);
                    break;
                case 8:
                    response = packFloat(Xpm25);
                    break;
                case 32:
                    response = packFloat(T4);
                    break;
                case 34:
                    response = packFloat(P / 10.0);
                    break;
                case 36:
                    response = packFloat(U1);
                    break;
                case 38:
                    response = packFloat(T5);
                    break;
                case 40:
                    response = packFloat(T2);
                    break;
                case 42:
                    response = packFloat(T3);
                    break;
                case 44:
                    response = packFloat(T1);
                    break;
                case 46:
                    response = packFloat(Q1);
                    break;
                case 48:
                    response = packFloat(Q2);
                    break;
                case 56:
                    response = packFloat(PCT1);
                    break;
                case 58:
                    response = packFloat(PCT2);
                    break;
                case 60:
                    response = packFloat(PCT3);
                    break;
                case 64:
                    response = packFloat(Xpm1);
                    break;
                case 86:
                    response = packFloat(ZSPAN);
                    break;
                default:
                    break;
                }

                while (response.size() < count * 2) {
                    response.push_back(static_cast<char>(0));
                }
                response.resize(count * 2);

                response.insert(0, response.size());
                response.insert(0, functionCode);
                response.insert(0, address);
                sendResponse(response);

                break;
            }

            default:
                sendError(functionCode, 0x01);
                break;
            }
        }
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;

    static Variant::Read findSizedValue(StreamCapture &stream,
                                        const SequenceName::Component &archive,
                                        const SequenceName::Component &variable,
                                        const SequenceName::Flavors &flavors)
    {
        for (const auto &v : stream.values()) {
            if (v.getArchive() != archive)
                continue;
            if (v.getVariable() != variable)
                continue;
            if (v.getFlavors() != flavors)
                continue;
            return v.read();
        }
        return Variant::Read::empty();
    }

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), {}, time))
            return false;
        if (!findSizedValue(stream, "raw_meta", "X", {"pm10"}).exists())
            return false;
        if (!findSizedValue(stream, "raw_meta", "X", {"pm25"}).exists())
            return false;
        if (!findSizedValue(stream, "raw_meta", "X", {"pm1"}).exists())
            return false;
        if (!stream.hasMeta("U1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T3", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T4", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T5", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("P", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("Q1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("Q2", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("ZSPAN", Variant::Root(), {}, time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZSTATE", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("ZXPM25", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("ZXPM10", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("ZXPM1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("PCT1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("PCT2", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("PCT3", Variant::Root(), {}, time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("U1"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("T3"))
            return false;
        if (!stream.checkContiguous("T4"))
            return false;
        if (!stream.checkContiguous("T5"))
            return false;
        if (!stream.checkContiguous("P"))
            return false;
        if (!stream.checkContiguous("Q1"))
            return false;
        if (!stream.checkContiguous("Q2"))
            return false;
        if (!stream.checkContiguous("ZSPAN"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (findSizedValue(stream, "raw", "X", {"pm10"}).toReal() != model.Xpm10)
            return false;
        if (findSizedValue(stream, "raw", "X", {"pm25"}).toReal() != model.Xpm25)
            return false;
        if (findSizedValue(stream, "raw", "X", {"pm1"}).toReal() != model.Xpm1)
            return false;
        if (!stream.hasAnyMatchingValue("U1", Variant::Root(model.U1), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.T1), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.T2), time))
            return false;
        if (!stream.hasAnyMatchingValue("T3", Variant::Root(model.T3), time))
            return false;
        if (!stream.hasAnyMatchingValue("T4", Variant::Root(model.T4), time))
            return false;
        if (!stream.hasAnyMatchingValue("T5", Variant::Root(model.T5), time))
            return false;
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.P), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q1", Variant::Root(model.Q1), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q2", Variant::Root(model.Q2), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZSPAN", Variant::Root(model.ZSPAN), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("ZXPM10", Variant::Root(model.Xpm10), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZXPM25", Variant::Root(model.Xpm25), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZXPM1", Variant::Root(model.Xpm1), time))
            return false;
        if (!stream.hasAnyMatchingValue("PCT1", Variant::Root(model.PCT1), time))
            return false;
        if (!stream.hasAnyMatchingValue("PCT2", Variant::Root(model.PCT2), time))
            return false;
        if (!stream.hasAnyMatchingValue("PCT3", Variant::Root(model.PCT3), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_teledyne_t640"));
        QVERIFY(component);

        QLocale::setDefault(QLocale::C);
        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        QByteArray readConcentration(12, 0);
        readConcentration[5] = 6;
        readConcentration[6] = instrument.address;
        readConcentration[7] = 0x03;
        qToBigEndian<quint16>(6, reinterpret_cast<uchar *>(readConcentration.data()) + 8);
        qToBigEndian<quint16>(2, reinterpret_cast<uchar *>(readConcentration.data()) + 10);

        QByteArray readAlarms(12, 0);
        readAlarms[5] = 6;
        readAlarms[6] = instrument.address;
        readAlarms[7] = 0x02;
        qToBigEndian<quint16>(0, reinterpret_cast<uchar *>(readAlarms.data()) + 8);
        qToBigEndian<quint16>(9, reinterpret_cast<uchar *>(readAlarms.data()) + 10);

        for (int i = 0; i < 20; i++) {
            control.externalControl(readConcentration);
            control.advance(0.25);
            control.externalControl(readAlarms);
            control.advance(0.25);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated.wait([&] {
            return interface->getAutoprobeStatus() ==
                    AcquisitionInterface::AutoprobeStatus::Success;
        }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.externalControl(readConcentration);
            control.advance(0.25);
            control.externalControl(readAlarms);
            control.advance(0.25);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QCOMPARE(findSizedValue(logging, "raw", "X", {"pm10"}).toReal(), instrument.Xpm10);
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 300; i++) {
            control.advance(0.5);
            QTest::qSleep(10);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QByteArray readConcentration(12, 0);
        readConcentration[5] = 6;
        readConcentration[6] = instrument.address;
        readConcentration[7] = 0x03;
        qToBigEndian<quint16>(6, reinterpret_cast<uchar *>(readConcentration.data()) + 8);
        qToBigEndian<quint16>(2, reinterpret_cast<uchar *>(readConcentration.data()) + 10);

        QByteArray readAlarms(12, 0);
        readAlarms[5] = 6;
        readAlarms[6] = instrument.address;
        readAlarms[7] = 0x02;
        qToBigEndian<quint16>(0, reinterpret_cast<uchar *>(readAlarms.data()) + 8);
        qToBigEndian<quint16>(9, reinterpret_cast<uchar *>(readAlarms.data()) + 10);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.externalControl(readConcentration);
            control.advance(0.25);
            control.externalControl(readAlarms);
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }

        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));
        control.advance(0.1);

        for (int i = 0; i < 300; i++) {
            control.externalControl(readConcentration);
            control.advance(0.25);
            control.externalControl(readAlarms);
            control.advance(0.25);
            QTest::qSleep(10);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QCOMPARE(findSizedValue(logging, "raw", "X", {"pm10"}).toReal(), instrument.Xpm10);
    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));

        for (int i = 0; i < 300; i++) {
            control.advance(0.5);
            QTest::qSleep(10);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
