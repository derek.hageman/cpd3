/*
 * Copyright (c) 2021 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIRETELEDYNET640_HXX
#define ACQUIRETELEDYNET640_HXX

#include "core/first.hxx"

#include <memory>
#include <deque>
#include <map>
#include <array>
#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"
#include "algorithms/crc.hxx"

class AcquireTeledyneT640 : public CPD3::Acquisition::FramedInstrument {
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;
    int autoprobeValidRecords;

    enum class ResponseState : int {
        /* Waiting a data record in passive mode */
        Passive_Run,

        /* Waiting for enough data to confirm communications */
        Passive_Wait,

        /* Passive initialization (same as passive wait, but discard the
         * first timeout). */
        Passive_Initialize,

        /* Same as passive wait but currently autoprobing */
        Autoprobe_Passive_Wait,

        /* Same as passive initialize but autoprobing */
        Autoprobe_Passive_Initialize,

        /* Stream flush */
        Interactive_Start_Flush,

        /* Reading active alarms */
        Interactive_Start_Read_Alarms,

        /* Reading pump duty cycle */
        Interactive_Start_Read_PumpDutyCycle,

        /* Reading PM1 concentration */
        Interactive_Start_Read_PM1,

        /* Reading initial concentration */
        Interactive_Start_Read_Concentration,

        /* Waiting before attempting a communications restart */
        Interactive_Restart_Wait,

        /* Initial state for interactive start */
        Interactive_Initialize,

        /* In interactive mode, waiting for a command response */
        Interactive_Run_Read, /* In interactive mode, waiting before polling again */
        Interactive_Run_Wait,
    } responseState;

    enum class RegisterType : std::uint16_t {
        INVALID = 0xFFFF,

        PM10Concentration = 6, /* 2 registers, float */
        PM25Concentration = 8, /* 2 registers, float */
        PM1Concentration = 64, /* 2 registers, float */
        RH = 36, /* 2 registers, float */
        SampleTemperature = 44, /* 2 registers, float */
        AmbientTemperature = 40, /* 2 registers, float */
        ASCTubeTemperature = 42, /* 2 registers, float */
        LEDTemperature = 32, /* 2 registers, float */
        BoxTemperature = 38, /* 2 registers, float */
        AmbientPressure = 34, /* 2 registers, float */
        SampleFlow = 46, /* 2 registers, float */
        BypassFlow = 48, /* 2 registers, float */
        SpanDeviation = 86, /* 2 registers, float */

        /* Getting nonsense out of these, so currently unused */
        PumpTachometer = 0, /* 2 registers, integer */
        AmplitudeCount = 2, /* 2 registers, integer */
        LengthCount = 4, /* 2 registers, integer */

        PumpDutyCycle = 56, /* 2 registers, float */
        ProportionalValveDutyCycle = 58, /* 2 registers, float */
        ASCHeaterDutyCycle = 60, /* 2 registers, float */
    };

    struct Report {
        CPD3::Data::Variant::Flags alarms;
        double Xpm25;
        double Xpm10;
        double Xpm1;

        double U1;
        double T1;
        double T2;
        double T3;
        double T4;
        double T5;
        double P;

        double Q1;
        double Q2;

        double ZSPAN;

        double PCT1;
        double PCT2;
        double PCT3;

        Report();

        bool operator==(const Report &other) const;
    };

    Report lastOutputData;
    Report currentData;
    double lastOutputEnd;

    class Command {
        CPD3::Util::ByteArray packet;
    public:
        Command();

        explicit Command(CPD3::Util::ByteArray data);

        Command(const Command &);

        Command &operator=(const Command &);

        Command(Command &&);

        Command &operator=(Command &&);

        void stateDescription(CPD3::Data::Variant::Write state) const;

        enum class ResponseType {
            InputRead, RegisterRead, Other,
        };

        ResponseType classify() const;

        std::uint16_t input() const;

        RegisterType reg() const;

        bool checkResponse(const CPD3::Util::ByteView &response) const;

        std::vector<std::uint16_t> toData(const CPD3::Util::ByteView &response) const;

        CPD3::Util::ByteView toBytes(const CPD3::Util::ByteView &response) const;

        float toFloat(const CPD3::Util::ByteView &response) const;

        std::vector<bool> toBits(const CPD3::Util::ByteView &response) const;
    };

    std::deque<Command> queuedCommands;

    void sendRequest(CPD3::Util::ByteArray &&request);

    void sendReadCommand(std::uint16_t reg, std::uint8_t count = 1);

    void sendReadCommand(RegisterType reg);

    void sendReadInputs(std::uint16_t first, std::uint16_t count = 1);

    void sendReadInputs();

    void sendWriteCommand(std::uint16_t reg, const std::vector<std::uint16_t> &data);

    void sendWriteCommand(RegisterType reg, const std::vector<std::uint16_t> &data);

    void sendWriteCommand(RegisterType reg, std::uint16_t value);

    class Configuration {
        double start;
        double end;
    public:
        double pollInterval;
        std::uint8_t address;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under,
                      const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    std::deque<Configuration> config;

    void setDefaultInvalid();

    CPD3::Data::Variant::Root instrumentMeta;

    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool realtimeStateUpdated;
    CPD3::Data::Variant::Flags realtimeStateAlarms;
    bool havePM1Option;

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value,
                  CPD3::Data::SequenceName::Flavors &&flavors = {});

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void invalidateLogValues(double frameTime);

    void configurationAdvance(double frameTime);

    CPD3::Data::Variant::Write invalidResponseBase(const CPD3::Util::ByteArray &frame,
                                                   double time,
                                                   int code);

    void invalidResponse(const CPD3::Util::ByteArray &frame, double time, int code);

    void invalidResponse(const CPD3::Util::ByteArray &frame,
                         double time,
                         const Command &command,
                         int code);

    void updateRunState(double frameTime);

    void emitDataRecord(double frameTime);

public:
    AcquireTeledyneT640(const CPD3::Data::ValueSegment::Transfer &config,
                        const std::string &loggingContext);

    AcquireTeledyneT640(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireTeledyneT640();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables() override;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus() override;

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;

    std::size_t dataFrameStart(std::size_t offset,
                               const CPD3::Util::ByteArray &input) const override;

    std::size_t dataFrameEnd(std::size_t start, const CPD3::Util::ByteArray &input) const override;

    std::size_t controlFrameStart(std::size_t offset,
                                  const CPD3::Util::ByteArray &input) const override;

    std::size_t controlFrameEnd(std::size_t start,
                                const CPD3::Util::ByteArray &input) const override;

    void limitDataBuffer(CPD3::Util::ByteArray &buffer) override;
};

class AcquireTeledyneT640Component
        : public QObject, virtual public CPD3::Acquisition::AcquisitionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_teledyne_t640"
                              FILE
                              "acquire_teledyne_t640.json")

    CPD3::ComponentOptions getBaseOptions();

public:

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
