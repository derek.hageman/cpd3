/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef WHERE_H
#define WHERE_H

#include "core/first.hxx"

#include <unordered_set>
#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "datacore/processingstage.hxx"

class WhereComponent : public QObject, public CPD3::Data::ProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.where"
                              FILE
                              "where.json")

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    CPD3::Data::ProcessingStage *createGeneralFilterDynamic(const CPD3::ComponentOptions &options = CPD3::ComponentOptions()) override;

    CPD3::Data::ProcessingStage *createGeneralFilterEditing(double start,
                                                            double end,
                                                            const CPD3::Data::SequenceName::Component &station,
                                                            const CPD3::Data::SequenceName::Component &archive,
                                                            const CPD3::Data::ValueSegment::Transfer &config) override;

};

#endif
