/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <vector>

#include "where.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "editing/editcore.hxx"
#include "editing/editdirective.hxx"
#include "editing/triggerscript.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Editing;


ComponentOptions WhereComponent::getOptions()
{
    ComponentOptions options;

    options.add("undefine", new ComponentOptionBoolean(tr("undefine", "name"),
                                                       tr("Undefine values instead of removing them"),
                                                       tr("When set values that fail the conditions are set to undefined "
                                                          "instead of being removed from the data stream entirely."),
                                                       QString()));

    options.add("invert",
                new ComponentOptionBoolean(tr("invert", "name"), tr("Invert the condition"),
                                           tr("Invert the condition and apply the effect when it is not met."),
                                           QString()));


    options.add("inputs", new DynamicSequenceSelectionOption(tr("inputs", "name"),
                                                             tr("The inputs to the condition"),
                                                             tr("This option sets the inputs used in the script condition."),
                                                             tr("Everything",
                                                                "default values include")));

    options.add("outputs", new DynamicSequenceSelectionOption(tr("outputs", "name"),
                                                              tr("The ouputs affected by the condition"),
                                                              tr("This option sets the data that is removed or undefined when the condition is met."),
                                                              tr("Everything",
                                                                 "default values exclude")));

    options.add("include",
                new ComponentOptionScript(tr("include", "name"), tr("The test condition"),
                                          tr("This sets the code executed at each data step.  "
                                             "The test is evaluated as segments called 'data', so to test a variable the syntax "
                                             "results in code like: 'return data.BsG_S11 > 2.0' "),
                                          QString()));

    options.add("fanout-station", new ComponentOptionBoolean(tr("fanout-station", "name"),
                                                             tr("Fan out handling for different stations"),
                                                             tr("If set then the condition is instantiated uniquely for "
                                                                "each station (exclude the default, if any)."),
                                                             tr("Enabled")));

    options.add("fanout-archive", new ComponentOptionBoolean(tr("fanout-archive", "name"),
                                                             tr("Fan out handling for different stations"),
                                                             tr("If set then the condition is instantiated uniquely for "
                                                                "each archive (excluding metadata)."),
                                                             tr("Enabled")));

    options.add("fanout-variable", new ComponentOptionBoolean(tr("fanout-variable", "name"),
                                                              tr("Fan out handling for different variables"),
                                                              tr("If set then the condition is instantiated uniquely for "
                                                                 "variable."), QString()));

    options.add("fanout-flavors", new ComponentOptionBoolean(tr("fanout-flavors", "name"),
                                                             tr("Fan out handling for different flavor combinations"),
                                                             tr("If set then the condition is instantiated uniquely for "
                                                                "each combination of flavors (e.x. PM1 vs PM10).  This also "
                                                                "automatically excludes information flavors like statistics."),
                                                             tr("Enabled")));

    return options;
}

QList<ComponentExample> WhereComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    qobject_cast<ComponentOptionScript *>(options.get("include"))->set(
            "return data.BsG_S11 > 2.0 and data.BsG_S11 > data.BsR_S11");
    examples.append(ComponentExample(options, tr("Simple condition", "simple condition name"),
                                     tr("This selects all data for which BsG_S11 is greater than 2.0 and "
                                        "is greater than BsR_S11.  If either are not defined then no "
                                        "output is produced.")));

    options = getOptions();
    qobject_cast<ComponentOptionScript *>(options.get("include"))->set(
            "return data.BsG_S11 == 0 or data.BsR_S11 == 0");
    examples.append(ComponentExample(options, tr("Zero multiple", "simple condition name"),
                                     tr("This selects all data for which either BsG_S11 or BsR_S11 is "
                                        "exactly zero.  If one is undefined but the other is zero then "
                                        "output is still produced.")));

    options = getOptions();
    qobject_cast<ComponentOptionScript *>(options.get("include"))->set(
            "hour = (data.START % 86400) / 3600; return hour >= 8 and hour < 12");
    qobject_cast<ComponentOptionBoolean *>(options.get("invert"))->set(true);
    examples.append(ComponentExample(options, tr("Time condition", "simple condition name"),
                                     tr("This excludes all data between UTC hour 8 and 12.")));

    return examples;
}

namespace {
class StubProcessingStage : public Data::AsyncProcessingStage {
public:
    StubProcessingStage() = default;

    virtual ~StubProcessingStage() = default;

protected:
    void process(Data::SequenceValue::Transfer &&incoming) override
    { egress->incomingData(std::move(incoming)); }
};

class MetadataScriptApplied : public Action, public EditDataModification {
    SequenceMatch::OrderedLookup selection;
    SequenceMatch::OrderedLookup inputs;
    Variant::Root meta;
public:
    MetadataScriptApplied(Data::SequenceMatch::OrderedLookup selection,
                          const std::string &code,
                          bool undefine,
                          bool invert) : selection(selection), inputs(std::move(selection))
    {
        meta["By"].setString("where");
        meta["At"].setDouble(Time::time());
        meta["Environment"].setString(Environment::describe());
        meta["Revision"].setString(Environment::revision());
        meta["Parameters"].hash("Code").setString(code);
        meta["Parameters"].hash("Undefine").setBoolean(undefine);
        meta["Parameters"].hash("Invert").setBoolean(invert);
    }

    MetadataScriptApplied(Data::SequenceMatch::OrderedLookup selection,
                          const Variant::Read &trigger,
                          bool undefine,
                          bool invert) : selection(selection), inputs(std::move(selection))
    {
        meta["By"].setString("where");
        meta["At"].setDouble(Time::time());
        meta["Environment"].setString(Environment::describe());
        meta["Revision"].setString(Environment::revision());
        meta["Parameters"].hash("Trigger").set(trigger);
        meta["Parameters"].hash("Undefine").setBoolean(undefine);
        meta["Parameters"].hash("Invert").setBoolean(invert);
    }

    virtual ~MetadataScriptApplied() = default;

    Action *clone() const override
    { return new MetadataScriptApplied(*this); }

    void unhandledInput(const Data::SequenceName &name,
                        QList<EditDataTarget *> &,
                        QList<EditDataTarget *> &,
                        QList<EditDataModification *> &modifiers) override
    {
        if (!name.isMeta())
            return;
        if (inputs.registerInput(name.fromMeta())) {
            modifiers.append(this);
            return;
        }
    }

    void incomingDataAdvance(double time) override
    { outputAdvance(time); }

    void modifySequenceValue(Data::SequenceValue &value) override
    {
        auto target = value.write().metadata("Processing");
        if (!target.exists())
            return;
        target.toArray().after_back().set(meta);
    }

protected:
    MetadataScriptApplied(const MetadataScriptApplied &other) : Action(other),
                                                                selection(other.selection),
                                                                inputs(selection),
                                                                meta(other.meta)
    { }
};

}

static void applyFanoutBits(const ComponentOptions &options,
                            EditDirective::FanoutMode &fanout,
                            const QString &id,
                            EditDirective::FanoutMode bits)
{
    if (!options.isSet(id))
        return;

    if (qobject_cast<ComponentOptionBoolean *>(options.get(id))->get()) {
        fanout |= bits;
    } else {
        fanout &= ~bits;
    }
}

ProcessingStage *WhereComponent::createGeneralFilterDynamic(const ComponentOptions &options)
{
    std::vector<std::unique_ptr<EditDirective>> directives;

    EditDirective::FanoutMode fanout = EditDirective::Fanout_Default;
    applyFanoutBits(options, fanout, "fanout-station", EditDirective::Fanout_Station);
    applyFanoutBits(options, fanout, "fanout-archive", EditDirective::Fanout_Archive);
    applyFanoutBits(options, fanout, "fanout-variable", EditDirective::Fanout_Variable);
    applyFanoutBits(options, fanout, "fanout-flavors", EditDirective::Fanout_Flavors);

    struct SelectionsOverlay : public Time::Bounds {
        SequenceMatch::OrderedLookup inputs;
        SequenceMatch::OrderedLookup outputs;

        SelectionsOverlay(const SelectionsOverlay &other, double start, double end) : Time::Bounds(
                start, end), inputs(other.inputs), outputs(other.outputs)
        { }


        struct Inputs : public Time::Bounds {
            SequenceMatch::OrderedLookup inputs;

            Inputs(SequenceMatch::OrderedLookup inputs, double start, double end) : Time::Bounds(
                    start, end), inputs(std::move(inputs))
            { }
        };

        SelectionsOverlay(const Inputs &other, double start, double end) : Time::Bounds(start, end),
                                                                           inputs(other.inputs),
                                                                           outputs()
        { }

        SelectionsOverlay(const SelectionsOverlay &under,
                          const Inputs &over,
                          double start,
                          double end) : Time::Bounds(start, end),
                                        inputs(over.inputs),
                                        outputs(under.outputs)
        { }

        struct Outputs : public Time::Bounds {
            SequenceMatch::OrderedLookup outputs;

            Outputs(SequenceMatch::OrderedLookup outputs, double start, double end) : Time::Bounds(
                    start, end), outputs(std::move(outputs))
            { }
        };

        SelectionsOverlay(const Outputs &other, double start, double end) : Time::Bounds(start,
                                                                                         end),
                                                                            inputs(),
                                                                            outputs(other.outputs)
        { }

        SelectionsOverlay(const SelectionsOverlay &under,
                          const Outputs &over,
                          double start,
                          double end) : Time::Bounds(start, end),
                                        inputs(under.inputs),
                                        outputs(over.outputs)
        { }
    };

    if (options.isSet("include")) {
        std::deque<SelectionsOverlay> segments;

        if (!options.isSet("inputs")) {
            Range::overlayFragmenting(segments, SelectionsOverlay::Inputs(
                    SequenceMatch::OrderedLookup(SequenceMatch::Element({},
                                                                        Archive::Selection::excludeMetaArchiveMatcher(),
                                                                        {}, {},
                                                                        SequenceName::defaultLacksFlavors())),
                    FP::undefined(), FP::undefined()));
        } else {
            for (const auto &add : qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("inputs"))->getSegments()) {
                Range::overlayFragmenting(segments,
                                          SelectionsOverlay::Inputs(add.lookup, add.getStart(),
                                                                    add.getEnd()));
            }
        }
        if (!options.isSet("outputs")) {
            Range::overlayFragmenting(segments, SelectionsOverlay::Outputs(
                    SequenceMatch::OrderedLookup(SequenceMatch::Element({},
                                                                        Archive::Selection::excludeMetaArchiveMatcher(),
                                                                        {}, {},
                                                                        SequenceName::defaultLacksFlavors())),
                    FP::undefined(), FP::undefined()));
        } else {
            for (const auto &add : qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("outputs"))->getSegments()) {
                Range::overlayFragmenting(segments,
                                          SelectionsOverlay::Outputs(add.lookup, add.getStart(),
                                                                     add.getEnd()));
            }
        }

        std::string code =
                qobject_cast<ComponentOptionScript *>(options.get("include"))->get().toStdString();

        bool undefine = false;
        if (options.isSet("undefine")) {
            undefine = qobject_cast<ComponentOptionBoolean *>(options.get("undefine"))->get();
        }
        bool invert = false;
        if (options.isSet("invert")) {
            invert = qobject_cast<ComponentOptionBoolean *>(options.get("invert"))->get();
        }

        for (const auto &sel : segments) {
            TriggerScript *trigger = new TriggerScript(code, sel.inputs);
            trigger->setInverted(!invert);

            Action *action;
            if (undefine) {
                action = new ActionInvalidate(sel.outputs);
            } else {
                action = new ActionRemove(sel.outputs);
            }

            directives.emplace_back(new EditDirective(sel.start, sel.end, new TriggerAlways,
                                                      new MetadataScriptApplied(sel.outputs, code,
                                                                                undefine, invert),
                                                      EditDirective::Fanout_None,
                                                      EditDirective::Fanout_None));

            directives.emplace_back(
                    new EditDirective(sel.start, sel.end, trigger, action, fanout, fanout));
        }
    }

    if (directives.empty())
        return new StubProcessingStage;
    return new EditCore(std::move(directives));
}

static SequenceMatch::OrderedLookup toSelection(const Variant::Read &config,
                                                const SequenceName::Component &station,
                                                const SequenceName::Component &archive)
{
    /* We need to exclude the metadata archive by default, since edits rarely
     * want that explicitly. */
    SequenceMatch::OrderedLookup sel
            (config, {}, {QString::fromStdString(Archive::Selection::excludeMetaArchiveMatcher())},
             {});
    if (!sel.valid()) {
        sel = SequenceMatch::OrderedLookup(
                SequenceMatch::Element({}, Archive::Selection::excludeMetaArchiveMatcher(), {}, {},
                                       SequenceName::defaultLacksFlavors()));
    }
    sel.registerExpected(station, archive);
    return sel;
}

ProcessingStage *WhereComponent::createGeneralFilterEditing(double,
                                                            double,
                                                            const SequenceName::Component &station,
                                                            const SequenceName::Component &archive,
                                                            const ValueSegment::Transfer &config)
{
    std::vector<std::unique_ptr<EditDirective>> directives;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    for (const auto &child : children) {
        ValueSegment::Transfer directiveConfiguration;
        for (const auto &seg : config) {
            auto single = seg[child];
            if (!directiveConfiguration.empty() &&
                    directiveConfiguration.back().getEnd() == seg.getStart() &&
                    directiveConfiguration.back().getValue() == single) {
                directiveConfiguration.back().setEnd(seg.getEnd());
                continue;
            }
            directiveConfiguration.emplace_back(seg.getStart(), seg.getEnd(),
                                                Variant::Root(single));
        }

        for (const auto &add : directiveConfiguration) {
            auto trigger = DirectiveContainer::createTrigger(add["Trigger"], station, archive);
            if (!trigger)
                continue;
            bool invert = add["Invert"].toBoolean();
            bool undefine = add["Undefine"].toBoolean();
            EditDirective::FanoutMode fanout = DirectiveContainer::toFanout(add["Fanout"]);
            auto selection = toSelection(add["Selection"], station, archive);

            trigger->setInverted(!invert);

            Action *action;
            if (undefine) {
                action = new ActionInvalidate(selection, add["PreserveType"].toBoolean());
            } else {
                action = new ActionRemove(selection);
            }

            directives.emplace_back(
                    new EditDirective(add.getStart(), add.getEnd(), new TriggerAlways,
                                      new MetadataScriptApplied(selection, add["Trigger"], undefine,
                                                                invert), EditDirective::Fanout_None,
                                      EditDirective::Fanout_None));

            directives.emplace_back(
                    new EditDirective(add.getStart(), add.getEnd(), trigger, action, fanout,
                                      fanout));
        }
    }

    if (directives.empty())
        return new StubProcessingStage;
    return new EditCore(std::move(directives));
}

