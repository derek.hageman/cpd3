/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/processingstage.hxx"
#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestComponent : public QObject {
Q_OBJECT

    ProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<ProcessingStageComponent *>(ComponentLoader::create("where"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("undefine")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("invert")));

        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("inputs")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("outputs")));
        QVERIFY(qobject_cast<ComponentOptionScript *>(options.get("include")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("fanout-station")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("fanout-archive")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("fanout-variable")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("fanout-flavors")));
    }

    void basicInclude()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<ComponentOptionScript *>(options.get("include"))->set(
                "return data.BsG_S11 > 2.0");

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName bg("brw", "raw", "BsG_S11");
        SequenceName bgm("brw", "raw_meta", "BsG_S11");
        SequenceName br("brw", "raw", "BsR_S11");
        SequenceName sg("sgp", "raw", "BsG_S11");

        Variant::Root meta;
        meta.write().metadataReal("Format").setString("0.0");
        meta.write().metadataReal("Processing").setType(Variant::Type::Array);

        filter->incomingData(SequenceValue(bg, Variant::Root(1.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(br, Variant::Root(2.5), 1.1, 1.9));
        filter->incomingData(SequenceValue(bgm, meta, 1.95, 100.0));
        filter->incomingData(SequenceValue(bg, Variant::Root(2.1), 2.0, 3.0));
        filter->incomingData(SequenceValue(br, Variant::Root(0.5), 2.1, 2.9));
        filter->incomingData(SequenceValue(sg, Variant::Root(1.2), 2.5, 3.5));
        filter->incomingData(SequenceValue(bg, Variant::Root(1.3), 3.0, 4.0));
        filter->incomingData(SequenceValue(sg, Variant::Root(2.2), 3.5, 4.5));
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 4);
        QCOMPARE(e.values()[0].getIdentity(), SequenceIdentity(bgm, 1.95, 100.0));
        QCOMPARE(e.values()[1].getIdentity(), SequenceIdentity(bg, 2.0, 3.0));
        QCOMPARE(e.values()[2].getIdentity(), SequenceIdentity(br, 2.1, 2.9));
        QCOMPARE(e.values()[3].getIdentity(), SequenceIdentity(sg, 3.5, 4.5));

        QVERIFY(e.values()[0].read().metadata("Processing").toArray().size() > 0);
        QCOMPARE(e.values()[1].read(), Variant::Root(2.1).read());
        QCOMPARE(e.values()[2].read(), Variant::Root(0.5).read());
        QCOMPARE(e.values()[3].read(), Variant::Root(2.2).read());
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
