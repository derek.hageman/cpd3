/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;
    QDateTime logTime;
    int unpolledMode;
    double unpolledInterval;

    quint16 flags;
    double countRate;
    double Pinlet;
    double Pvacuum;
    double Qu;
    double Tsaturator;
    double Tgrowth;
    double Toptics;
    double Twater;
    double Tambient;
    double Tinlet;
    double Alaser;
    double PCTnozzle;
    double Vphoto;
    double Vpulse;

    ModelInstrument()
            : incoming(),
              outgoing(),
              unpolledRemaining(0),
              logTime(QDate(2013, 10, 01), QTime(0, 0, 0)),
              unpolledMode(3),
              unpolledInterval(1.0)
    {

        flags = 0;
        countRate = 6000;
        Pinlet = 984.0;
        Pvacuum = 580.0;
        Qu = 0.6;
        Tsaturator = 40.0;
        Tgrowth = 25.0;
        Toptics = 28.0;
        Twater = 35.0;
        Tambient = 25.0;
        Tinlet = 26.0;
        Alaser = 100.0;
        PCTnozzle = 100.0;
        Vphoto = 140.0;
        Vpulse = 10.0;
    }

    double concentration(double Q = 0.12) const
    {
        return countRate / (Q * (1000.0 / 60.0));
    }

    void outputDateTime()
    {
        outgoing.append(QByteArray::number(logTime.date().year()));
        outgoing.append('/');
        outgoing.append(QByteArray::number(logTime.date().month()));
        outgoing.append('/');
        outgoing.append(QByteArray::number(logTime.date().day()));
        outgoing.append(',');
        outgoing.append(QByteArray::number(logTime.time().hour()));
        outgoing.append(':');
        outgoing.append(QByteArray::number(logTime.time().minute()));
        outgoing.append(':');
        outgoing.append(QByteArray::number(logTime.time().second()));
    }

    void outputD(bool addSRecord = false)
    {
        outgoing.append("D,");
        outputDateTime();
        outgoing.append(',');
        outgoing.append(QByteArray::number(flags, 16));
        outgoing.append(',');
        outgoing.append(QByteArray::number(concentration(), 'e', 2));
        outgoing.append(",1.0,1.000,");
        outgoing.append(QByteArray::number(countRate, 'f', 0));
        outgoing.append(',');
        outgoing.append(QByteArray::number(Vphoto, 'f', 0));
        outgoing.append(",,");
        outgoing.append(QByteArray::number(Vpulse, 'f', 0));
        outgoing.append(",0");
        if (addSRecord) {
            outgoing.append(',');
            outputS();
            return;
        }
        outgoing.append('\r');
    }

    void outputS()
    {
        outgoing.append("S,");
        outgoing.append(QByteArray::number(Pinlet, 'f', 0));
        outgoing.append(',');
        outgoing.append(QByteArray::number(Pvacuum, 'f', 0));
        outgoing.append(',');
        outgoing.append(QByteArray::number(Tsaturator, 'f', 0));
        outgoing.append(',');
        outgoing.append(QByteArray::number(Tgrowth, 'f', 0));
        outgoing.append(',');
        outgoing.append(QByteArray::number(Toptics, 'f', 0));
        outgoing.append(',');
        outgoing.append(QByteArray::number(Twater, 'f', 0));
        outgoing.append(',');
        outgoing.append(QByteArray::number(Tinlet, 'f', 0));
        outgoing.append('\r');
    }

    void outputU()
    {
        outgoing.append("U1");
        for (int i = 0; i < 10; i++) {
            outgoing.append(',');
            outgoing.append(QByteArray::number(concentration(), 'e', 2));
        }
        for (int i = 0; i < 10; i++) {
            outgoing.append(',');
            outgoing.append(QByteArray::number(countRate / 10.0, 'f', 0));
        }
        for (int i = 0; i < 10; i++) {
            outgoing.append(',');
        }
        for (int i = 0; i < 10; i++) {
            outgoing.append(",0.100");
        }
        outgoing.append(",0.0,");
        outgoing.append(QByteArray::number(Pinlet, 'f', 0));
        outgoing.append(",0.00,");
        outgoing.append(QByteArray::number(Vpulse, 'f', 0));
        outgoing.append(",0,");
        outgoing.append(QByteArray::number(flags, 16));
        outgoing.append('\r');
    }

    void outputFile()
    {
        outputDateTime();
        outgoing.append(',');
        outgoing.append(QByteArray::number(concentration(), 'e', 2));
        outgoing.append(',');
        outgoing.append(QByteArray::number(countRate, 'f', 0));
        outgoing.append(",1.000,,");
        outgoing.append(QByteArray::number(Pinlet, 'f', 0));
        outgoing.append(",0.00,");
        outgoing.append(QByteArray::number(Vpulse, 'f', 0));
        outgoing.append(",0,");
        outgoing.append(QByteArray::number(flags, 16));
        outgoing.append('\r');
    }

    void advance(double seconds)
    {

        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR));

            if (line.startsWith("SA,") ||
                    line.startsWith("SFC,") ||
                    line.startsWith("SP,") ||
                    line.startsWith("ST,")) {
                outgoing.append("OK\r");
            } else if (line == "SR") {
                outgoing.append(QByteArray::number(logTime.date().year()));
                outgoing.append(',');
                outgoing.append(QByteArray::number(logTime.date().month()));
                outgoing.append(',');
                outgoing.append(QByteArray::number(logTime.date().day()));
                outgoing.append(',');
                outgoing.append(QByteArray::number(logTime.time().hour()));
                outgoing.append(',');
                outgoing.append(QByteArray::number(logTime.time().minute()));
                outgoing.append(',');
                outgoing.append(QByteArray::number(logTime.time().second()));
                outgoing.append('\r');
            } else if (line.startsWith("SR,")) {
                QList<QByteArray> fields(line.split(','));
                if (fields.size() != 7) {
                    outgoing.append("ERROR\r");
                } else {
                    logTime.setDate(QDate(fields.at(1).toInt(), fields.at(2).toInt(),
                                          fields.at(3).toInt()));
                    logTime.setTime(QTime(fields.at(4).toInt(), fields.at(5).toInt(),
                                          fields.at(6).toInt()));
                    if (logTime.isValid()) {
                        outgoing.append("OK\r");
                    } else {
                        outgoing.append("ERROR\r");
                    }
                }
            } else if (line == "SA") {
                outgoing.append("0\r");
            } else if (line == "SFC") {
                outgoing.append("1205\r");
            } else if (line == "SP") {
                outgoing.append("0\r");
            } else if (line == "ST") {
                outgoing.append("0\r");
            } else if (line == "SSTART") {
                outgoing.append("3\r");
            } else if (line.startsWith("SSTART,")) {
                QList<QByteArray> fields(line.split(','));
                if (fields.size() != 2) {
                    outgoing.append("ERROR\r");
                } else {
                    outgoing.append("OK\r");
                    if (fields.at(1) == "3") {
                        outputU();
                    }
                }
            } else if (line == "RAI") {
                outgoing.append("3\r");
            } else if (line == "RALL") {
                outgoing.append("Stuff\rThings\r");
            } else if (line == "RCT") {
                outputDateTime();
                outgoing.append('\r');
            } else if (line == "RD") {
                outgoing.append(QByteArray::number(concentration(), 'f', 0));
                outgoing.append('\r');
            } else if (line == "RIE") {
                outgoing.append(QByteArray::number(flags, 16));
                outgoing.append('\r');
            } else if (line == "RIF") {
                outgoing.append(QByteArray::number(Qu, 'f', 2));
                outgoing.append('\r');
            } else if (line == "RIS") {
                outgoing.append(QByteArray::number(concentration(), 'e', 2));
                outgoing.append(",100.0,,");
                outgoing.append(QByteArray::number(Pinlet, 'f', 0));
                outgoing.append(',');
                outgoing.append(QByteArray::number(PCTnozzle, 'f', 0));
                outgoing.append(',');
                outgoing.append(QByteArray::number(Qu, 'f', 2));
                outgoing.append(",0.00,");
                outgoing.append(QByteArray::number(Vpulse, 'f', 0));
                outgoing.append(',');
                outgoing.append(QByteArray::number(Toptics, 'f', 1));
                outgoing.append(',');
                outgoing.append(QByteArray::number(Tgrowth, 'f', 1));
                outgoing.append(',');
                outgoing.append(QByteArray::number(Tsaturator, 'f', 1));
                outgoing.append(',');
                outgoing.append(QByteArray::number(Twater, 'f', 1));
                outgoing.append(",0\r");
            } else if (line == "RL") {
                outgoing.append(QByteArray::number(Alaser, 'f', 0));
                outgoing.append('\r');
            } else if (line == "RLL") {
                outgoing.append("FULL (4012)\r");
            } else if (line == "RPA") {
                outgoing.append(QByteArray::number(Pinlet, 'f', 2));
                outgoing.append('\r');
            } else if (line == "RPN") {
                outgoing.append(QByteArray::number(PCTnozzle, 'f', 0));
                outgoing.append('\r');
            } else if (line == "RPV") {
                outgoing.append(QByteArray::number(Pvacuum, 'f', 0));
                outgoing.append('\r');
            } else if (line == "RRD") {
                outputD();
            } else if (line == "RRS") {
                outputS();
            } else if (line == "RTA") {
                outgoing.append(QByteArray::number(Tambient, 'f', 1));
                outgoing.append('\r');
            } else if (line == "RTC") {
                outgoing.append(QByteArray::number(Tsaturator, 'f', 1));
                outgoing.append('\r');
            } else if (line == "RTG") {
                outgoing.append(QByteArray::number(Tgrowth, 'f', 1));
                outgoing.append('\r');
            } else if (line == "RTO") {
                outgoing.append(QByteArray::number(Toptics, 'f', 1));
                outgoing.append('\r');
            } else if (line == "RV") {
                outgoing.append("Model 3783 Ver 0.20 S/N 101\r");
            } else if (line.startsWith("SM,")) {
                QList<QByteArray> fields(line.split(','));
                if (fields.size() != 3) {
                    outgoing.append("ERROR\r");
                } else {
                    unpolledMode = fields.at(1).toInt();
                    if (unpolledMode >= 0 && unpolledMode <= 10) {
                        unpolledInterval = fields.at(2).toDouble() / 10.0;
                        unpolledRemaining = unpolledInterval;
                        outgoing.append("OK\r");
                        if (unpolledMode == 0)
                            unpolledRemaining = FP::undefined();
                    } else {
                        outgoing.append("ERROR\r");
                    }
                }
            } else {
                outgoing.append("ERROR\r");
            }

            if (idxCR >= incoming.size() - 1) {
                incoming.clear();
                break;
            }
            incoming = incoming.mid(idxCR + 1);
        }

        logTime = logTime.addMSecs((qint64) ceil(seconds * 1000.0));
        if (FP::defined(unpolledRemaining)) {
            unpolledRemaining -= seconds;
            while (unpolledRemaining <= 0.0) {
                unpolledRemaining += unpolledInterval;
                switch (unpolledMode) {
                case 0:
                    break;
                case 1:
                    outputD();
                    break;
                case 2:
                    outputS();
                    break;
                case 3:
                    outputD(true);
                    break;
                case -1:
                    outputFile();
                    break;
                case -2:
                    outputU();
                    break;
                default:
                    break;
                }
            }
        }

    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("N", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T4", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T5", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Tu", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Qu", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("P1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("P2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("A", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCT", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("C", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZND", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZFULL", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("N"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("T3"))
            return false;
        if (!stream.checkContiguous("T4"))
            return false;
        if (!stream.checkContiguous("T5"))
            return false;
        if (!stream.checkContiguous("Tu"))
            return false;
        if (!stream.checkContiguous("Qu"))
            return false;
        if (!stream.checkContiguous("P1"))
            return false;
        if (!stream.checkContiguous("P2"))
            return false;
        if (!stream.checkContiguous("A"))
            return false;
        if (!stream.checkContiguous("V1"))
            return false;
        if (!stream.checkContiguous("V2"))
            return false;
        if (!stream.checkContiguous("PCT"))
            return false;
        if (!stream.checkContiguous("F1"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream, const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("N", Variant::Root(model.concentration()), time))
            return false;
        if (!stream.hasAnyMatchingValue("V2", Variant::Root(model.Vpulse), time))
            return false;
        if (!stream.hasAnyMatchingValue("P1", Variant::Root(model.Pinlet), time))
            return false;
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkStatusValues(StreamCapture &stream, const ModelInstrument &model,
                           double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("Tu", Variant::Root(model.Tinlet), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.Tsaturator), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.Tgrowth), time))
            return false;
        if (!stream.hasAnyMatchingValue("T3", Variant::Root(model.Toptics), time))
            return false;
        if (!stream.hasAnyMatchingValue("T4", Variant::Root(model.Twater), time))
            return false;
        if (!stream.hasAnyMatchingValue("T5", Variant::Root(model.Tambient), time))
            return false;
        if (!stream.hasAnyMatchingValue("V1", Variant::Root(model.Vphoto), time))
            return false;
        if (!stream.hasAnyMatchingValue("P2", Variant::Root(model.Pvacuum), time))
            return false;
        if (!stream.hasAnyMatchingValue("PCT", Variant::Root(model.PCTnozzle), time))
            return false;
        if (!stream.hasAnyMatchingValue("A", Variant::Root(model.Alaser), time))
            return false;
        if (!stream.hasAnyMatchingValue("Qu", Variant::Root(model.Qu), time))
            return false;
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("C", Variant::Root(model.countRate), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZND", Variant::Root(model.concentration()), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_tsi_cpc3783"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_tsi_cpc3783"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(1.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, FP::undefined()));
        QVERIFY(checkValues(realtime, instrument, FP::undefined()));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(0.1);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkStatusValues(logging, instrument));
        QVERIFY(checkStatusValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledMode = -1;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("N", Variant::Root(instrument.concentration())))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        for (int i = 0; i < 20; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, FP::undefined()));
        QVERIFY(checkValues(realtime, instrument, FP::undefined()));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("N", Variant::Root(instrument.concentration())))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        double checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        control.advance(0.125);
        checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        for (int i = 0; i < 20; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkStatusValues(logging, instrument));
        QVERIFY(checkStatusValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void passiveSampleRecord()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledMode = -2;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("N", Variant::Root(instrument.concentration())))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        for (int i = 0; i < 20; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, FP::undefined()));
        QVERIFY(checkValues(realtime, instrument, FP::undefined()));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
