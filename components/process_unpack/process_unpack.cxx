/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "core/component.hxx"
#include "datacore/externalsource.hxx"
#include "transfer/package.hxx"
#include "transfer/datafile.hxx"

#include "process_unpack.hxx"


Q_LOGGING_CATEGORY(log_component_process_unpack, "cpd3.component.process.unpack", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Transfer;


ProcessUnpack::ProcessUnpack(const ComponentOptions &options) : file(),
                                                                certificate(),
                                                                key(),
                                                                setToModified(false),
                                                                useDeleted(false)
{

    if (options.isSet("certificate")) {
        certificate = Variant::Root(
                qobject_cast<ComponentOptionSingleString *>(options.get("certificate"))->get());
    }
    if (options.isSet("key")) {
        key = Variant::Root(qobject_cast<ComponentOptionSingleString *>(options.get("key"))->get());
    }
    if (options.isSet("deleted")) {
        useDeleted = qobject_cast<ComponentOptionBoolean *>(options.get("deleted"))->get();
    }
    if (options.isSet("modified")) {
        setToModified = qobject_cast<ComponentOptionBoolean *>(options.get("modified"))->get();
    }
}

ProcessUnpack::~ProcessUnpack() = default;

void ProcessUnpack::incomingData(const Util::ByteView &in)
{
    if (in.empty())
        return;
    Q_ASSERT(!dataEnded);

    if (!file) {
        file = IO::Access::temporaryFile();
        if (!file) {
            qCWarning(log_component_process_unpack) << "Unable to open temporary file";
            return;
        }
    }
    if (!output) {
        output = file->block();
        if (!output) {
            qCWarning(log_component_process_unpack) << "Unable to open temporary output";
            return;
        }
    }

    output->write(in);
}

void ProcessUnpack::endData()
{
    auto lock = acquireLock();
    dataEnded = true;
    externalNotify();
}


bool ProcessUnpack::prepare()
{ return dataEnded; }

namespace {
class Unpacker : public TransferUnpack {
    Data::Variant::Read certificate;
    Data::Variant::Read key;
public:
    Unpacker(const Variant::Read &cert, const Variant::Read &k) : certificate(cert), key(k)
    { }

    virtual ~Unpacker()
    { }

protected:
    Data::Variant::Read getCertificate(const QByteArray &, bool, int) override
    { return certificate; }

    Data::Variant::Read getKeyForCertificate(const QByteArray &, int) override
    { return key; }
};

class DataExtractor : public ErasureSink {
    bool useDeleted;
    bool setToModified;
    StreamSink *egress;
public:
    DataExtractor(bool del, bool mod, StreamSink *e) : useDeleted(del),
                                                       setToModified(mod),
                                                       egress(e)
    { }

    virtual ~DataExtractor() = default;

    void incomingValue(const ArchiveValue &value) override
    {
        if (useDeleted)
            return;
        if (!setToModified) {
            egress->incomingData(value);
            return;
        }

        egress->emplaceData(value.getIdentity(), Variant::Root(value.getModified()));
    }

    void incomingValue(ArchiveValue &&value) override
    {
        if (useDeleted)
            return;
        if (!setToModified) {
            egress->incomingData(std::move(value));
            return;
        }

        egress->emplaceData(std::move(value.getIdentity()), Variant::Root(value.getModified()));
    }

    void incomingErasure(const ArchiveErasure &erasure) override
    {
        if (!useDeleted)
            return;
        if (!setToModified) {
            egress->emplaceData(erasure.getIdentity(), Variant::Root());
            return;
        }

        egress->emplaceData(erasure.getIdentity(), Variant::Root(erasure.getModified()));
    }

    void incomingErasure(ArchiveErasure &&erasure) override
    {
        if (!useDeleted)
            return;
        if (!setToModified) {
            egress->emplaceData(std::move(erasure.getIdentity()), Variant::Root());
            return;
        }

        egress->emplaceData(std::move(erasure.getIdentity()), Variant::Root(erasure.getModified()));
    }

    void endStream() override
    { }
};
}

bool ProcessUnpack::process()
{
    if (!dataEnded)
        return true;

    if (!file) {
        egress->endData();
        return false;
    }
    output.reset();

    auto unpacked = IO::Access::temporaryFile();
    if (!unpacked) {
        qCWarning(log_component_process_unpack) << "Unable open output file";
        egress->endData();
        return false;
    }

    {
        Unpacker unpacker(certificate, key);
        if (!unpacker.exec(file, unpacked)) {
            qCWarning(log_component_process_unpack) << "Error unpacking file:"
                                                    << unpacker.errorString();
            egress->endData();
            return false;
        }
    }

    {
        DataExtractor ex(useDeleted, setToModified, egress);
        DataUnpack unpacker;
        if (!unpacker.exec(unpacked, &ex)) {
            qCWarning(log_component_process_unpack) << "Error extracting data file";
            egress->endData();
            return false;
        }
    }

    egress->endData();

    return false;
}


ComponentOptions ProcessUnpackComponent::getOptions()
{
    ComponentOptions options;

    options.add("certificate", new ComponentOptionSingleString(tr("certificate", "name"),
                                                               tr("The certificate used with on the transfer file"),
                                                               tr("This is the certificate used on the transfer file, if needed."),
                                                               QString()));

    options.add("key", new ComponentOptionSingleString(tr("key", "name"),
                                                       tr("The key used with on the transfer file"),
                                                       tr("This is the key used on the transfer file, if needed."),
                                                       QString()));

    options.add("deleted", new ComponentOptionBoolean(tr("deleted", "name"),
                                                      tr("Output deleted value erasure markers"),
                                                      tr("When enabled, values are created for all erasure markers in the "
                                                                 "transfer file.  This disables normal data output, so all "
                                                                 "the values produced are erasure markers."),
                                                      QString()));

    options.add("modified", new ComponentOptionBoolean(tr("modified", "name"),
                                                       tr("Output modified time markers"),
                                                       tr("When enabled, the values in the output are replaced with their "
                                                                  "modification times."),
                                                       QString()));

    return options;
}

QList<ComponentExample> ProcessUnpackComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This unpacks the given transfer archive file.")));

    return examples;
}

ExternalConverter *ProcessUnpackComponent::createDataIngress(const ComponentOptions &options)
{ return new ProcessUnpack(options); }
