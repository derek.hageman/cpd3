/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef PROCESSUNPACK_H
#define PROCESSUNPACK_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QIODevice>
#include <QString>
#include <QList>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/root.hxx"
#include "io/drivers/file.hxx"

class ProcessUnpack : public CPD3::Data::ExternalSourceProcessor {
    std::shared_ptr<CPD3::IO::File::Backing> file;
    std::unique_ptr<CPD3::IO::Generic::Block> output;
    CPD3::Data::Variant::Root certificate;
    CPD3::Data::Variant::Root key;

    bool setToModified;
    bool useDeleted;

public:
    ProcessUnpack(const CPD3::ComponentOptions &options);

    virtual ~ProcessUnpack();

    void incomingData(const CPD3::Util::ByteView &in) override;

    void endData() override;

protected:
    bool prepare() override;

    bool process() override;
};

class ProcessUnpackComponent
        : public QObject, virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.process_unpack"
                              FILE
                              "process_unpack.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    QList<CPD3::ComponentExample> getExamples();

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;
};


#endif
