/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QBuffer>

#include "datacore/externalsource.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "transfer/package.hxx"
#include "transfer/datafile.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Transfer;

static const char *cert1Data = "-----BEGIN CERTIFICATE-----\n"
        "MIICbTCCAdYCCQDZ6mj3BI+kCTANBgkqhkiG9w0BAQUFADB7MQswCQYDVQQGEwJV\n"
        "UzERMA8GA1UECAwIQ29sb3JhZG8xEDAOBgNVBAcMB0JvdWxkZXIxFjAUBgNVBAoM\n"
        "DU5PQUEvR01EL0VTUkwxFzAVBgNVBAsMDkFlcm9zb2xzIEdyb3VwMRYwFAYDVQQD\n"
        "DA1EZXJlayBIYWdlbWFuMB4XDTEyMDcyNzE1MzkwNloXDTM5MTIxMjE1MzkwNlow\n"
        "ezELMAkGA1UEBhMCVVMxETAPBgNVBAgMCENvbG9yYWRvMRAwDgYDVQQHDAdCb3Vs\n"
        "ZGVyMRYwFAYDVQQKDA1OT0FBL0dNRC9FU1JMMRcwFQYDVQQLDA5BZXJvc29scyBH\n"
        "cm91cDEWMBQGA1UEAwwNRGVyZWsgSGFnZW1hbjCBnzANBgkqhkiG9w0BAQEFAAOB\n"
        "jQAwgYkCgYEAn3nIKzjLLEY8KG6+hUvrkjDkl9VU8HFKnp8wA2RsMta/UvKG4ZIY\n"
        "bNVsKSiAGm1G0i6Mtftw8quyihoxuy/hElH/XJChU5lersdrhalocHXoipNx8BeW\n"
        "T8sXuJOrh+IoDKqVI4I6Sz42qAlqBU7bNm40TE8sBPAOefyTGavC3e0CAwEAATAN\n"
        "BgkqhkiG9w0BAQUFAAOBgQBU2hljZZpJJ+zK6eePUpdELQGPGHJEbzfReeSBhRau\n"
        "iDj4jEa7IE3yrMv8NqC3Q+6lEVuJg0NInvDYsNYLPjMSPS+jKldrmrJvg+TFrp1w\n"
        "Qr3JoznNxwcwuDyrKjOVux6jo2aype3Zayo5VUizJX9Zo+xLe2MBVR1D0MXmoq8L\n"
        "zA==\n"
        "-----END CERTIFICATE-----";
static const char *key1Data = "-----BEGIN RSA PRIVATE KEY-----\n"
        "MIICXAIBAAKBgQCfecgrOMssRjwobr6FS+uSMOSX1VTwcUqenzADZGwy1r9S8obh\n"
        "khhs1WwpKIAabUbSLoy1+3Dyq7KKGjG7L+ESUf9ckKFTmV6ux2uFqWhwdeiKk3Hw\n"
        "F5ZPyxe4k6uH4igMqpUjgjpLPjaoCWoFTts2bjRMTywE8A55/JMZq8Ld7QIDAQAB\n"
        "AoGBAIQOd0f7PpsKCfS9R7zfklG7dP+Z4z07wzu4vCyC8uniVAoe1LxjmyA8VtV6\n"
        "OSIpDTUs4M4tSWlZ7n1XlYjY6/lNt3xsy5BBQMlWVI8BV/yXnWRCxkQGBvXYTUKe\n"
        "7LRsUcqyGpAPeuStO/V8GEM5H272yv5S4413D09C9cAhROMtAkEA07j2lnnSl/WR\n"
        "0NFEOT9+nbh6Z+yRTzybC86TBhLCN3bY4VvqSWi1PdcU3oiz81dcaAPTksdx18/Y\n"
        "cSfz295ewwJBAMDTq22Dt2HMPQymxNUn9/IdIPU34/fEqSHS3e9hAgrp2gDVeNHR\n"
        "ZEzMkW00rLgdukkW51PV9hVIhYXdxYOjZY8CQFsf/cnwLurGf/b/SrzVDjr1/oEi\n"
        "Obx/2j+vrmnrwvm6Rkhglir4TSGLo+jPr5vpmtUN6I8BFoeLZp31UyjrwZ8CQC3W\n"
        "Y2ruI7qozV5jimjNToCMchg4yAVPB5GVydIsskqb2onWNRlTeE9VVcCrA9/kmTLk\n"
        "serY8t2OVsdCt8AaKHsCQFMEhJCpXaeLnrzToSv0uqRYjcT+r3ZjSghhSepN0ZA+\n"
        "PPj1CMmq9JrQaQAv3rT3hCbDF38TPhKX64OYzbuvdxA=\n"
        "-----END RSA PRIVATE KEY-----\n";

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestComponent : public QObject {
Q_OBJECT

    ExternalConverterComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("process_unpack"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("certificate")));
    }

    void basic()
    {
        SequenceValue::Transfer values;
        values.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(1.0), 100.0, 200.0);
        values.emplace_back(SequenceName("brw", "raw", "U_S11"), Variant::Root(2.0), 100.0, 200.0);
        values.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(3.0), 200.0, 300.0);

        QByteArray data;
        {
            auto dataFile = IO::Access::buffer();
            DataPack dataPack(dataFile->stream(), false);
            dataPack.start();
            dataPack.incomingData(values);
            dataPack.endData();
            Threading::pollInEventLoop([&] { return !dataPack.isFinished(); });
            dataPack.wait();

            TransferPack filePack;
            filePack.compressStage();
            Variant::Root config;
            config["Certificate"].setString(cert1Data);
            config["Key"].setString(key1Data);
            filePack.signStage(config);
            filePack.checksumStage();

            auto outputFile = IO::Access::buffer();
            QVERIFY(filePack.exec(dataFile, outputFile));
            outputFile->block()->read(data);
        }
        QVERIFY(data.size() > 0);

        ComponentOptions options(component->getOptions());
        ExternalConverter *inp = component->createDataIngress(options);
        QVERIFY(inp != NULL);

        inp->start();
        StreamSink::Buffer output;
        inp->setEgress(&output);

        inp->incomingData(data);
        inp->endData();

        QVERIFY(inp->wait());
        delete inp;

        QVERIFY(output.ended());
        QCOMPARE(output.values(), values);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
