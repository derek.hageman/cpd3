/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QTemporaryFile>

#include "datacore/processingstage.hxx"
#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}


class TestComponent : public QObject {
Q_OBJECT

    ProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<ProcessingStageComponent *>(ComponentLoader::create("tap"));
        QVERIFY(component);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionFile *>(options.get("file")));
    }

    void tapOptions()
    {
        QTemporaryFile targetFile;
        QVERIFY(targetFile.open());

        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<ComponentOptionFile *>(options.get("file"))->set(targetFile.fileName());

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceValue::Transfer values;
        SequenceName u("brw", "raw", "BsG_S11");
        for (int i = 0; i < 1000; i++) {
            values.emplace_back(u, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i);
            filter->incomingData(SequenceValue(u, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i));
            if (i % 200 == 0)
                QTest::qSleep(50);
        }
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 1000);
        QCOMPARE(e.values(), values);

        StreamSink::Buffer r;
        StandardDataInput reader;
        reader.start();
        reader.setEgress(&r);
        reader.incomingData(targetFile.readAll());
        reader.endData();
        reader.wait();

        QCOMPARE((int) r.values().size(), 1000);
        QCOMPARE(r.values(), values);
    }

    void tapEditing()
    {
        QTemporaryFile targetFile;
        QVERIFY(targetFile.open());

        Variant::Root config;
        config["File"] = targetFile.fileName();

        ProcessingStage *filter =
                component->createGeneralFilterEditing(FP::undefined(), FP::undefined(), "brw",
                                                      "raw", ValueSegment::Transfer{
                                ValueSegment(FP::undefined(), FP::undefined(), config)});
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceValue::Transfer values;
        SequenceName u("brw", "raw", "BsG_S11");
        for (int i = 0; i < 1000; i++) {
            values.emplace_back(u, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i);
            filter->incomingData(SequenceValue(u, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i));
            if (i % 200 == 0)
                QTest::qSleep(50);
        }
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 1000);
        QCOMPARE(e.values(), values);

        StreamSink::Buffer r;
        StandardDataInput reader;
        reader.start();
        reader.setEgress(&r);
        reader.incomingData(targetFile.readAll());
        reader.endData();
        reader.wait();

        QCOMPARE((int) r.values().size(), 1000);
        QCOMPARE(r.values(), values);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
