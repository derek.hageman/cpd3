/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef TAP_H
#define TAP_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/externalsink.hxx"
#include "io/drivers/file.hxx"

class Tap : public CPD3::Data::ProcessingStageThread {
    std::unique_ptr<CPD3::Data::DynamicString> targetFileName;

    std::shared_ptr<CPD3::IO::File::Backing> file;
    std::unique_ptr<CPD3::Data::StandardDataOutput> output;
    CPD3::Threading::Signal<> terminateRequested;

    void attachOutput();
public:
    Tap(const CPD3::ComponentOptions &options);

    Tap(double start,
        double end,
        const CPD3::Data::SequenceName::Component &station,
        const CPD3::Data::SequenceName::Component &archive,
        const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~Tap();

    void signalTerminate() override;

protected:
    void process(CPD3::Data::SequenceValue::Transfer &&incoming) override;

    bool finalize() override;
};

class TapComponent : public QObject, public CPD3::Data::ProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.tap"
                              FILE
                              "tap.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    CPD3::Data::ProcessingStage *createGeneralFilterDynamic(const CPD3::ComponentOptions &options = {}) override;

    CPD3::Data::ProcessingStage *createGeneralFilterEditing(double start,
                                                            double end,
                                                            const CPD3::Data::SequenceName::Component &station,
                                                            const CPD3::Data::SequenceName::Component &archive,
                                                            const CPD3::Data::ValueSegment::Transfer &config) override;

};

#endif
