/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "core/environment.hxx"

#include "tap.hxx"


Q_LOGGING_CATEGORY(log_component_tap, "cpd3.component.tap", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;


Tap::Tap(const ComponentOptions &options)
{
    if (options.isSet("file")) {
        targetFileName.reset(new DynamicPrimitive<QString>::Constant(
                qobject_cast<ComponentOptionFile *>(options.get("file"))->get()));
    } else {
        targetFileName.reset(new DynamicPrimitive<QString>::Constant("tap.c3d"));
    }
}

Tap::Tap(double start,
         double end,
         const SequenceName::Component &,
         const SequenceName::Component &,
         const ValueSegment::Transfer &config)
{
    targetFileName.reset(DynamicStringOption::fromConfiguration(config, "File", start, end));
}

Tap::~Tap()
{
    terminateRequested.disconnect();
    if (output) {
        output->signalTerminate();
        output->wait();
        output.reset();
    }
}

void Tap::attachOutput()
{
    if (!file)
        return;
    auto stream = file->stream();
    if (!stream) {
        qCWarning(log_component_tap) << "Can't open file" << file->filename();
        file.reset();
        return;
    }

    output.reset(new StandardDataOutput(std::move(stream)));
    terminateRequested.connect(std::bind(&StandardDataOutput::signalTerminate, output.get()));
    output->start();

    qCDebug(log_component_tap) << "Writing tap output to" << file->filename();
}

void Tap::process(SequenceValue::Transfer &&incoming)
{
    if (incoming.empty())
        return;

    egress->incomingData(incoming);

    auto fileName = targetFileName->get(incoming.front().getStart(), incoming.back().getEnd());
    if (fileName.isEmpty()) {
        finalize();
        return;
    }
    if (!file || file->filename() != fileName.toStdString()) {
        finalize();

        file = IO::Access::file(fileName, IO::File::Mode::writeOnly().textMode().bufferedMode());
        attachOutput();
    }

    if (output) {
        output->incomingData(std::move(incoming));
    }
}

bool Tap::finalize()
{
    if (output) {
        terminateRequested.disconnect();
        output->endData();
        output->wait();
        output.reset();
    }
    file.reset();
    return true;
}

void Tap::signalTerminate()
{
    ProcessingStageThread::signalTerminate();
    terminateRequested();
}


ComponentOptions TapComponent::getOptions()
{
    ComponentOptions options;

    ComponentOptionFile *file =
            new ComponentOptionFile(tr("file", "name"), tr("The file to write to"),
                                    tr("This is the output file to write to.  All data seen by this "
                                       "component is written to this file before being passed along."),
                                    tr("tap.c3d", "default tap"), 1);
    file->setMode(ComponentOptionFile::Write);
    options.add("file", file);

    return options;
}

ProcessingStage *TapComponent::createGeneralFilterDynamic(const ComponentOptions &options)
{ return new Tap(options); }

ProcessingStage *TapComponent::createGeneralFilterEditing(double start,
                                                          double end,
                                                          const SequenceName::Component &station,
                                                          const SequenceName::Component &archive,
                                                          const ValueSegment::Transfer &config)
{ return new Tap(start, end, station, archive, config); }
