/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREVAISALAWXT520_H
#define ACQUIREVAISALAWXT520_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QDateTime>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "algorithms/crc.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class AcquireVaisalaWXT5xx : public CPD3::Acquisition::FramedInstrument {
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Passive autoprobe initialization, ignorging the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,
        /* Acquiring data in interactive mode, but only reading unpolled data */
        RESP_UNPOLLED_RUN,

        /* Waiting for completion of the initial flush */
                RESP_INTERACTIVE_START_FLUSH,
        /* Waiting for a response to the address query in ASCII/NMEA mode */
                RESP_INTERACTIVE_START_READ_ADDRESS_ASCII,
        /* Waiting for a response to the address query in SDI-12 mode */
                RESP_INTERACTIVE_START_READ_ADDRESS_SDI12,
        /* Waiting for a response after sending all possible reset commands */
                RESP_INTERACTIVE_START_RESET_EVERYTHING,
        /* Trying a last time to read the NMEA address */
                RESP_INTERACTIVE_START_READ_ADDRESS_NMEA_FINAL,

        /* Reading the current communications parameters */
                RESP_INTERACTIVE_START_READ_COMMUNICATIONS,

        /* Reading the identification from the instrument */
                RESP_INTERACTIVE_START_READ_IDENTIFICATION,

        /* Setting the current wind reporting parameters */
                RESP_INTERACTIVE_START_WINDS,
        /* Setting the temperature, pressure and RH parameters */
                RESP_INTERACTIVE_START_TEMPERATUREPRESSURERH,
        /* Setting the precipitation parameters */
                RESP_INTERACTIVE_START_PRECIPITATION, /* Setting the auxiliary parameters */
                RESP_INTERACTIVE_START_AUXILIARY,
        /* Setting the monitor parameters */
                RESP_INTERACTIVE_START_MONITOR,

        /* Waiting for the first valid record */
                RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID,

        /* Waiting before attempting a communications restart */
                RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
                RESP_INTERACTIVE_INITIALIZE,
    };
    ResponseState responseState;
    int autoprobeValidRecords;

    enum LogStreamID {
        LogStream_Metadata = 0,
        LogStream_State,

        LogStream_Winds,
        LogStream_TemperaturePressureRH,
        LogStream_Precipitation, LogStream_Monitor, LogStream_Auxiliary,

        LogStream_TOTAL,
        LogStream_RecordBaseStart = LogStream_Winds,
    };
    CPD3::Data::StaticMultiplexer loggingMux;
    quint32 streamAge[LogStream_TOTAL];
    double streamTime[LogStream_TOTAL];

    friend class Configuration;

    struct Auxiliary {
        enum class Setting {
            On, Off, Auto
        };

        static void fromConfiguration(const CPD3::Data::Variant::Read &input, Setting &target);

        static bool settingEqual(Setting a, Setting b);

        Setting solarRadiation;
        Setting level;
        Setting temperature;
        Setting rain;

        Auxiliary();

        bool operator==(const Auxiliary &other) const;

        bool operator!=(const Auxiliary &other) const;
    };

    class Configuration {
        double start;
        double end;
    public:
        bool strictMode;
        double reportInterval;
        int address;
        Auxiliary auxiliary;

        enum {
            Unchanged, Enable, Disable
        } heating;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    CPD3::Algorithms::CRC16 crc;

    void setDefaultInvalid();

    CPD3::Data::Variant::Root instrumentMeta;

    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;

    double rainAccumulationTime;
    double rainAccumulationTotal;

    bool instrumentSDI12Mode;
    int instrumentAddress;
    Auxiliary auxiliaryEffective;

    CPD3::Data::Variant::Flags heatingFlags;


    void logValue(double frameTime,
                  int streamID,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    void advanceStream(double frameTime, int streamID);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void updateMetadata(double frameTime);

    void advanceState(double frameTime);

    void configAdvance(double frameTime);

    void configurationChanged();

    void invalidateLogValues(double frameTime);

    int handleASCIIField(CPD3::Data::Variant::Root &output,
                         CPD3::Util::ByteView field,
                         int (*convert)(CPD3::Data::Variant::Root &value, char unit),
                         char *outputUnit = NULL);

    int processRecordASCII(CPD3::Util::ByteView line, double frameTime);

    int processRecordNMEA(CPD3::Util::ByteView line, double frameTime);

    int processRecord(const CPD3::Util::ByteView &line, double frameTime);

    void sendCommandASCII(const CPD3::Util::ByteView &command, int addr = -1);

    inline void sendCommandASCII(const char *command, int addr = -1)
    { return sendCommandASCII(CPD3::Util::ByteView(command), addr); }

    void sendCommandSDI12(const CPD3::Util::ByteView &command, int addr = -1);

    inline void sendCommandSDI12(const char *command, int addr = -1)
    { return sendCommandSDI12(CPD3::Util::ByteView(command), addr); }

    void sendCommandNMEA(const CPD3::Util::ByteView &command);

    inline void sendCommandNMEA(const char *command)
    { return sendCommandNMEA(CPD3::Util::ByteView(command)); }

    bool breakdownSettingsFields(const CPD3::Util::ByteView &frame,
                                 double frameTime,
                                 std::deque<std::pair<char, CPD3::Util::ByteView>> &output);

public:
    AcquireVaisalaWXT5xx(const CPD3::Data::ValueSegment::Transfer &config,
                         const std::string &loggingContext);

    AcquireVaisalaWXT5xx(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireVaisalaWXT5xx();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:

    virtual void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingInstrumentTimeout(double time);

    virtual void discardDataCompleted(double time);
};

class AcquireVaisalaWXT5xxComponent
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_vaisala_wxt5xx"
                              FILE
                              "acquire_vaisala_wxt5xx.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
