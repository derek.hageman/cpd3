/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cctype>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/wavelength.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_vaisala_wxt5xx.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;


AcquireVaisalaWXT5xx::Configuration::Configuration() : start(FP::undefined()),
                                                       end(FP::undefined()),
                                                       strictMode(true),
                                                       reportInterval(1.0),
                                                       address(-1),
                                                       auxiliary(),
                                                       heating(Unchanged)
{ }

AcquireVaisalaWXT5xx::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          strictMode(other.strictMode),
          reportInterval(other.reportInterval),
          address(other.address),
          auxiliary(other.auxiliary),
          heating(other.heating)
{ }

AcquireVaisalaWXT5xx::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          strictMode(true),
          reportInterval(1.0),
          address(-1),
          auxiliary(),
          heating(Unchanged)
{
    setFromSegment(other);
}

AcquireVaisalaWXT5xx::Configuration::Configuration(const Configuration &under,
                                                   const ValueSegment &over,
                                                   double s,
                                                   double e) : start(s),
                                                               end(e),
                                                               strictMode(under.strictMode),
                                                               reportInterval(under.reportInterval),
                                                               address(under.address),
                                                               auxiliary(under.auxiliary),
                                                               heating(under.heating)
{
    setFromSegment(over);
}

static int toAddress(char ch)
{
    if (ch >= '0' && ch <= '9') {
        return (int) (ch - '0');
    } else if (ch >= 'A' && ch <= 'Z') {
        return (int) (ch - 'A') + 10;
    } else if (ch >= 'a' && ch <= 'z') {
        return (int) (ch - 'a') + 10 + 26;
    }
    return -1;
}

AcquireVaisalaWXT5xx::Auxiliary::Auxiliary() : solarRadiation(Setting::Auto),
                                               level(Setting::Auto),
                                               temperature(Setting::Auto),
                                               rain(Setting::Auto)
{ }

void AcquireVaisalaWXT5xx::Auxiliary::fromConfiguration(const Variant::Read &input, Setting &target)
{
    if (!input.exists())
        return;
    switch (input.getType()) {
    case Variant::Type::Boolean:
        target = input.toBoolean() ? Setting::On : Setting::Off;
        return;
    default:
        break;
    }

    const auto &s = input.toString();
    if (Util::equal_insensitive(s, "on", "true")) {
        target = Setting::On;
    } else if (Util::equal_insensitive(s, "off", "false")) {
        target = Setting::Off;
    } else {
        target = Setting::Auto;
    }
}

bool AcquireVaisalaWXT5xx::Auxiliary::settingEqual(Setting a, Setting b)
{
    switch (a) {
    case Setting::On:
        switch (b) {
        case Setting::On:
            return true;
        case Setting::Off:
            return false;
        case Setting::Auto:
            return true;
        }
        break;
    case Setting::Off:
        switch (b) {
        case Setting::On:
            return false;
        case Setting::Off:
            return true;
        case Setting::Auto:
            return true;
        }
    case Setting::Auto:
        return true;
    }
    Q_ASSERT(false);
    return false;
}

bool AcquireVaisalaWXT5xx::Auxiliary::operator==(const Auxiliary &other) const
{
    return settingEqual(solarRadiation, other.solarRadiation) &&
            settingEqual(level, other.level) &&
            settingEqual(temperature, other.temperature) &&
            settingEqual(rain, other.rain);
}

bool AcquireVaisalaWXT5xx::Auxiliary::operator!=(const Auxiliary &other) const
{ return !(*this == other); }

void AcquireVaisalaWXT5xx::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    {
        double v = config["ReportInterval"].toDouble();
        if (FP::defined(v) && v > 0.0)
            reportInterval = v;
    }

    if (config["Address"].exists()) {
        qint64 addr = config["Address"].toInt64();
        if (INTEGER::defined(addr) && addr >= 0 && addr < 10 + 26 + 26) {
            address = (int) addr;
        } else if (!config["Address"].toString().empty()) {
            address = toAddress(config["Address"].toString()[0]);
        } else {
            address = -1;
        }
    }

    Auxiliary::fromConfiguration(config["Auxiliary/Temperature"], auxiliary.temperature);
    Auxiliary::fromConfiguration(config["Auxiliary/Rain"], auxiliary.rain);
    Auxiliary::fromConfiguration(config["Auxiliary/SolarRadiation"], auxiliary.solarRadiation);
    Auxiliary::fromConfiguration(config["Auxiliary/LevelSensor"], auxiliary.level);

    if (config["Heating"].exists()) {
        if (config["Heating"].getType() == Variant::Type::Boolean)
            heating = config["Heating"].toBool() ? Enable : Disable;
        else
            heating = Unchanged;
    }
}


void AcquireVaisalaWXT5xx::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Vaisala");
    instrumentMeta["Model"].setString("WXT5xx");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    rainAccumulationTime = FP::undefined();
    rainAccumulationTotal = FP::undefined();

    instrumentSDI12Mode = false;
    instrumentAddress = -1;
    auxiliaryEffective = Auxiliary();

    heatingFlags.clear();

    loggingMux.clear();
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;
    }
}

AcquireVaisalaWXT5xx::AcquireVaisalaWXT5xx(const ValueSegment::Transfer &configData,
                                           const std::string &loggingContext) : FramedInstrument(
        "wxt5xx", loggingContext),
                                                                                autoprobeStatus(
                                                                                        AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                responseState(
                                                                                        RESP_PASSIVE_WAIT),
                                                                                loggingMux(
                                                                                        LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireVaisalaWXT5xx::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void AcquireVaisalaWXT5xx::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireVaisalaWXT5xxComponent::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireVaisalaWXT5xx::AcquireVaisalaWXT5xx(const ComponentOptions &options,
                                           const std::string &loggingContext) : FramedInstrument(
        "wxt5xx", loggingContext),
                                                                                autoprobeStatus(
                                                                                        AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                responseState(
                                                                                        RESP_PASSIVE_WAIT),
                                                                                loggingMux(
                                                                                        LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());
}

AcquireVaisalaWXT5xx::~AcquireVaisalaWXT5xx() = default;

void AcquireVaisalaWXT5xx::logValue(double frameTime,
                                    int streamID,
                                    SequenceName::Component name,
                                    CPD3::Data::Variant::Root &&value)
{
    double startTime = streamTime[streamID];
    double endTime = frameTime;
    Q_ASSERT(FP::defined(endTime));
    if (!FP::defined(startTime) || startTime == endTime || !loggingEgress) {
        if (loggingEgress)
            streamTime[streamID] = endTime;
        if (!realtimeEgress)
            return;
        realtimeEgress->emplaceData(SequenceName({}, "raw", std::move(name)), std::move(value),
                                    frameTime, frameTime + 0.5);
        return;
    }

    SequenceValue
            dv(SequenceName({}, "raw", std::move(name)), std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        loggingMux.incoming(streamID, std::move(dv), loggingEgress);
        return;
    }

    loggingMux.incoming(streamID, dv, loggingEgress);
    dv.setStart(endTime);
    dv.setEnd(endTime + 1.0);
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireVaisalaWXT5xx::realtimeValue(double time,
                                         SequenceName::Component name,
                                         Variant::Root &&value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

void AcquireVaisalaWXT5xx::advanceStream(double frameTime, int streamID)
{
    streamAge[streamID]++;
    streamTime[streamID] = frameTime;
}

SequenceValue::Transfer AcquireVaisalaWXT5xx::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_vaisala_wxt5xx");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["Model"].set(instrumentMeta["Model"]);
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    if (instrumentMeta["SerialNumber"].exists())
        processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    if (instrumentMeta["ID"].exists())
        processing["ID"].set(instrumentMeta["ID"]);

    result.emplace_back(SequenceName({}, "raw_meta", "WS"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("m/s");
    result.back().write().metadataReal("Description").setString("Wind speed");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("Vector2D");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Direction")
          .setString("WD");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Magnitude")
          .setString("WS");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Wind Speed"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "WD"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Units").setString("degrees");
    result.back().write().metadataReal("Description").setString("Wind direction from true north");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("Vector2D");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Direction")
          .setString("WD");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Magnitude")
          .setString("WS");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Wind Direction"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Ambient temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Ambient"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Internal temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Internal"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "T3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Heater temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Heater"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "U1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Ambient relative humidity");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Ambient"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Ambient pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Ambient"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "WI"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.00");
    result.back().write().metadataReal("Units").setString("mm/h");
    result.back().write().metadataReal("Description").setString("Precipitation rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Precipitation"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);

    result.emplace_back(SequenceName({}, "raw_meta", "ZWSGust"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("m/s");
    result.back().write().metadataReal("Description").setString("Wind gust speed");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    if (auxiliaryEffective.solarRadiation == Auxiliary::Setting::On) {
        result.emplace_back(SequenceName({}, "raw_meta", "VA"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("W/m\xC2\xB2");
        result.back().write().metadataReal("Description").setString("Solar radiation");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Solar radiation"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);
    }

    if (auxiliaryEffective.level == Auxiliary::Setting::On) {
        result.emplace_back(SequenceName({}, "raw_meta", "Ld"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.00");
        result.back().write().metadataReal("Units").setString("m");
        result.back().write().metadataReal("Description").setString("Level sensor");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Level"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(6);
    }

    if (auxiliaryEffective.temperature == Auxiliary::Setting::On) {
        result.emplace_back(SequenceName({}, "raw_meta", "T4"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.0");
        result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
        result.back().write().metadataReal("Description").setString("Auxiliary temperature sensor");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Auxiliary"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);
    }

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("HeaterOn")
          .hash("Description")
          .setString("Heater turned on");
    result.back().write().metadataSingleFlag("HeaterOn").hash("Bits").setInt64(0x00010000);
    result.back()
          .write()
          .metadataSingleFlag("HeaterOn")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_wxt5xx");

    result.back()
          .write()
          .metadataSingleFlag("HeatingMiddleTemperature")
          .hash("Description")
          .setString(
                  "Heater temperature is between the low and middle limit and is operating at full duty cycle");
    result.back().write().metadataSingleFlag("HeaterOn").hash("Bits").setInt64(0x00020000);
    result.back()
          .write()
          .metadataSingleFlag("HeatingMiddleTemperature")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_wxt5xx");

    result.back()
          .write()
          .metadataSingleFlag("HeatingLowTemperature")
          .hash("Description")
          .setString(
                  "Heater temperature is between below the low limit and is operating at half duty cycle");
    result.back().write().metadataSingleFlag("HeaterOn").hash("Bits").setInt64(0x00040000);
    result.back()
          .write()
          .metadataSingleFlag("HeatingLowTemperature")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_vaisala_wxt5xx");


    return result;
}

SequenceValue::Transfer AcquireVaisalaWXT5xx::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_vaisala_wxt5xx");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    if (instrumentMeta["SerialNumber"].exists())
        processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    if (instrumentMeta["ID"].exists())
        processing["ID"].set(instrumentMeta["ID"]);

    result.emplace_back(SequenceName({}, "raw_meta", "V1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Supply voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Supply"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(7);

    result.emplace_back(SequenceName({}, "raw_meta", "V2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Reference voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Reference"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(7);

    result.emplace_back(SequenceName({}, "raw_meta", "V3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Heater voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Heater"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(7);

    return result;
}


void AcquireVaisalaWXT5xx::updateMetadata(double frameTime)
{
    if (!loggingEgress) {
        haveEmittedLogMeta = false;
        loggingMux.clear();
        memset(streamAge, 0, sizeof(streamAge));
        for (int i = 0; i < LogStream_TOTAL; i++) {
            streamTime[i] = FP::undefined();
        }
    } else {
        if (!haveEmittedLogMeta) {
            haveEmittedLogMeta = true;
            loggingMux.incoming(LogStream_Metadata, buildLogMeta(frameTime), loggingEgress);
        }
        loggingMux.advance(LogStream_Metadata, frameTime, loggingEgress);
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }
}

void AcquireVaisalaWXT5xx::advanceState(double frameTime)
{
    Variant::Flags flags = heatingFlags;

    for (int i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
        if (streamAge[i] < 2)
            continue;
        if (!loggingEgress)
            continue;

        if (FP::defined(streamTime[LogStream_State])) {
            double startTime = streamTime[LogStream_State];
            double endTime = frameTime;

            loggingMux.incoming(LogStream_State,
                                SequenceValue({{}, "raw", "F1"}, Variant::Root(flags), startTime,
                                              endTime), loggingEgress);
        } else {
            loggingMux.advance(LogStream_State, frameTime, loggingEgress);
        }
        streamTime[LogStream_State] = frameTime;

        for (i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
            if (streamAge[i] == 0) {
                loggingMux.advance(i, frameTime, loggingEgress);
                streamTime[i] = FP::undefined();
            }
            streamAge[i] = 0;
        }
        break;
    }

    if (realtimeEgress) {
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "F1"}, Variant::Root(std::move(flags)), frameTime,
                              FP::undefined()));
    }
}

static Util::ByteArray encodeCRC(quint16 crc)
{
    auto result = Util::ByteArray::filled(0, 3);
    result[0] = (char) (0x40 | (crc >> 12));
    result[1] = (char) (0x40 | ((crc >> 6) & 0x3F));
    result[2] = (char) (0x40 | (crc & 0x3F));
    return result;
}

static void scaleValue(Variant::Root &v, double scale)
{
    double f = v.read().toDouble();
    if (!FP::defined(f))
        return;
    v.write().setDouble(f * scale);
}

static int convertWindDirection(Variant::Root &value, char unit)
{
    switch (unit) {
    case 0:
        return 0;
    case '#':
        value.write().setDouble(FP::undefined());
        return -1;
    case 'D':
    case 'd':
    case 'R':
    case 'r':
        break;
    default:
        return 5;
    }
    return 0;
}

static int convertWindSpeed(Variant::Root &value, char unit)
{
    switch (unit) {
    case 0:
        return 0;
    case '#':
        value.write().setDouble(FP::undefined());
        return -1;
    case 'M':
    case 'm':
        break;
    case 'K':
    case 'k':
        scaleValue(value, 1000.0 / 3600.0);
        break;
    case 'S':
    case 's':
        scaleValue(value, 0.44704);
        break;
    case 'N':
    case 'n':
        scaleValue(value, 0.514444);
        break;
    default:
        return 5;
    }
    return 0;
}

static int convertTemperature(Variant::Root &value, char unit)
{
    switch (unit) {
    case 0:
        return 0;
    case '#':
        value.write().setDouble(FP::undefined());
        return -1;
    case 'C':
    case 'c':
        break;
    case 'F':
    case 'f': {
        double v = value.read().toDouble();
        if (!FP::defined(v))
            break;
        v -= 32.0;
        v *= 5.0 / 9.0;
        value.write().setDouble(v);
        break;
    }
    default:
        return 5;
    }
    return 0;
}

static int convertRH(Variant::Root &value, char unit)
{
    switch (unit) {
    case 0:
        return 0;
    case '#':
        value.write().setDouble(FP::undefined());
        return -1;
    case 'P':
    case 'p':
        break;
    default:
        return 5;
    }
    return 0;
}

static int convertPressure(Variant::Root &value, char unit)
{
    switch (unit) {
    case 0:
        return 0;
    case '#':
        value.write().setDouble(FP::undefined());
        return -1;
    case 'H':
    case 'h':
        break;
    case 'P':
    case 'p':
        scaleValue(value, 1.0 / 100.0);
        break;
    case 'B':
    case 'b':
        scaleValue(value, 1000.0);
        break;
    case 'M':
    case 'm':
        scaleValue(value, 1.333224);
        break;
    case 'I':
    case 'i':
        scaleValue(value, 33.86384);
        break;
    default:
        return 5;
    }
    return 0;
}

static int convertPrecipitation(Variant::Root &value, char unit)
{
    switch (unit) {
    case 0:
        return 0;
    case '#':
        value.write().setDouble(FP::undefined());
        return -1;
    case 'H':
    case 'h':
        /* Hits/hour, can't convert to sane units */
        value.write().setDouble(FP::undefined());
        return -1;
    case 'M':
    case 'm':
        break;
    case 'I':
    case 'i':
        scaleValue(value, 25.4);
        break;
    default:
        return 5;
    }
    return 0;
}

static int convertVoltage(Variant::Root &value, char unit)
{
    switch (unit) {
    case 0:
        break;
    case '#':
        value.write().setDouble(FP::undefined());
        return -1;
    case 'V':
    case 'v':
    case 'W':
    case 'w':
    case 'F':
    case 'f':
    case 'N':
    case 'n':
        break;
    default:
        return 5;
    }
    return 0;
}

int AcquireVaisalaWXT5xx::handleASCIIField(Variant::Root &output,
                                           Util::ByteView field,
                                           int (*convert)(Variant::Root &value, char unit),
                                           char *outputUnit)
{
    if (outputUnit != NULL)
        *outputUnit = 0;

    if (field.size() < 1) return 6;

    char unit = (char) 0;
    switch (field[field.size() - 1]) {
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
        break;
    default:
        unit = field[field.size() - 1];
        field = field.mid(0, field.size() - 1);
        break;
    }

    if (unit == '#') {
        output.write().setDouble(FP::undefined());
        return -1;
    }

    if (field.size() < 1) return 7;

    bool ok = false;
    double v = field.string_trimmed().parse_real(&ok);
    if (!FP::defined(v)) return 8;

    output.write().setDouble(v);
    if (convert != NULL) {
        int code = convert(output, unit);
        if (code != 0)
            return code;
    }
    if (outputUnit != NULL)
        *outputUnit = unit;

    return 0;
}

int AcquireVaisalaWXT5xx::processRecordASCII(Util::ByteView line, double frameTime)
{
    if (line.size() < 4)
        return 1;

    int addr = toAddress(line[0]);
    if (addr < 0) return 2;
    if (config.first().address != -1 && config.first().address != addr) return -1;

    if (line[1] == 'r') {
        auto expected = encodeCRC(crc.calculate(line.data(), line.size() - 3));
        if (expected != line.mid(line.size() - 3))
            return 3;
        line = line.mid(0, line.size() - 3);
    } else if (line[1] != 'R') {
        return 4;
    }

    auto fields = CSV::acquisitionSplit(line);
    std::deque<Util::ByteView> subfields;

    if (fields.empty()) return 5;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.size() < 3) return 6;

    bool processWinds = false;
    bool processTemperaturePressureRH = false;
    bool processPrecipitation = false;
    bool processMonitor = false;
    bool processAuxiliary = false;
    switch (field[2]) {
    case '0':
        processWinds = true;
        processTemperaturePressureRH = true;
        processPrecipitation = true;
        processMonitor = true;
        processAuxiliary = true;
        break;
    case '1':
        processWinds = true;
        break;
    case '2':
        processTemperaturePressureRH = true;
        break;
    case '3':
        processPrecipitation = true;
        break;
    case '4':
        processAuxiliary = true;
        break;
    case '5':
        processMonitor = true;
        break;
    default:
        return 7;
    }

    updateMetadata(frameTime);

    int recognizedFields = 0;
    Variant::Root rainAccumulation;
    bool haveRainIntensity = false;
    while (!fields.empty()) {
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (field.size() < 2) return 100;
        subfields = Util::as_deque(field.split('='));
        if (subfields.empty()) return 101;

        field = subfields.front();
        subfields.pop_front();
        if (processWinds) {
            if (field == "Dn") {
                /* Wind direction minimum, ignored */
                continue;
            } else if (field == "Dm") {
                if (subfields.empty()) return 110;
                Variant::Root WD;
                int code = handleASCIIField(WD, subfields.front(), convertWindDirection);
                subfields.pop_front();
                if (code > 0) {
                    return 110 + code;
                } else if (code == 0) {
                    recognizedFields++;
                }
                remap("WD", WD);
                if (config.first().strictMode) {
                    if (!subfields.empty())
                        return 111;
                }
                logValue(frameTime, LogStream_Winds, "WD", std::move(WD));
                continue;
            } else if (field == "Dx") {
                /* Wind direction maximum, ignored */
                continue;
            } else if (field == "Sn") {
                /* Wind speed minimum, ignored */
                continue;
            } else if (field == "Sm") {
                if (subfields.empty()) return 120;
                Variant::Root WS;
                int code = handleASCIIField(WS, subfields.front(), convertWindSpeed);
                subfields.pop_front();
                if (code > 0) {
                    return 120 + code;
                } else if (code == 0) {
                    recognizedFields++;
                }
                remap("WS", WS);
                if (config.first().strictMode) {
                    if (!subfields.empty())
                        return 121;
                }
                logValue(frameTime, LogStream_Winds, "WS", std::move(WS));
                continue;
            } else if (field == "Sx") {
                if (subfields.empty()) return 130;
                Variant::Root ZWSGust;
                int code = handleASCIIField(ZWSGust, subfields.front(), convertWindSpeed);
                subfields.pop_front();
                if (code > 0) {
                    return 130 + code;
                }
                remap("ZWSGust", ZWSGust);
                if (config.first().strictMode) {
                    if (!subfields.empty())
                        return 131;
                }
                logValue(frameTime, LogStream_Winds, "ZWSGust", std::move(ZWSGust));
                continue;
            }
        }
        if (processTemperaturePressureRH) {
            if (field == "Ta") {
                if (subfields.empty()) return 210;
                Variant::Root T;
                int code = handleASCIIField(T, subfields.front(), convertTemperature);
                subfields.pop_front();
                if (code > 0) {
                    return 210 + code;
                } else if (code == 0) {
                    recognizedFields++;
                }
                remap("T1", T);
                if (config.first().strictMode) {
                    if (!subfields.empty())
                        return 211;
                }
                logValue(frameTime, LogStream_TemperaturePressureRH, "T1", std::move(T));
                continue;
            } else if (field == "Ua") {
                if (subfields.empty()) return 220;
                Variant::Root U;
                int code = handleASCIIField(U, subfields.front(), convertRH);
                subfields.pop_front();
                if (code > 0) {
                    return 220 + code;
                } else if (code == 0) {
                    recognizedFields++;
                }
                remap("U1", U);
                if (config.first().strictMode) {
                    if (!subfields.empty())
                        return 221;
                }
                logValue(frameTime, LogStream_TemperaturePressureRH, "U1", std::move(U));
                continue;
            } else if (field == "Pa") {
                if (subfields.empty()) return 230;
                Variant::Root P;
                int code = handleASCIIField(P, subfields.front(), convertPressure);
                subfields.pop_front();
                if (code > 0) {
                    return 230 + code;
                } else if (code == 0) {
                    recognizedFields++;
                }
                remap("P", P);
                if (config.first().strictMode) {
                    if (!subfields.empty())
                        return 231;
                }
                logValue(frameTime, LogStream_TemperaturePressureRH, "P", std::move(P));
                continue;
            } else if (field == "Tp") {
                if (subfields.empty()) return 240;
                Variant::Root T;
                int code = handleASCIIField(T, subfields.front(), convertTemperature);
                subfields.pop_front();
                if (code > 0) {
                    return 240 + code;
                }
                remap("T2", T);
                if (config.first().strictMode) {
                    if (!subfields.empty())
                        return 241;
                }
                logValue(frameTime, LogStream_TemperaturePressureRH, "T2", std::move(T));
                continue;
            }
        }
        if (processPrecipitation) {
            if (field == "Rc") {
                if (subfields.empty()) return 310;
                int code =
                        handleASCIIField(rainAccumulation, subfields.front(), convertPrecipitation);
                subfields.pop_front();
                if (code > 0) {
                    return 210 + code;
                }
                remap("ZWIt", rainAccumulation);
                if (config.first().strictMode) {
                    if (!subfields.empty())
                        return 211;
                }
                continue;
            } else if (field == "Rd") {
                /* Rain duration, ignored */
                continue;
            } else if (field == "Ri") {
                if (subfields.empty()) return 320;
                Variant::Root WI;
                int code = handleASCIIField(WI, subfields.front(), convertWindDirection);
                subfields.pop_front();
                if (code > 0) {
                    return 320 + code;
                } else if (code == 0) {
                    recognizedFields++;
                    haveRainIntensity = true;
                }
                remap("WI", WI);
                if (config.first().strictMode) {
                    if (!subfields.empty())
                        return 321;
                }
                logValue(frameTime, LogStream_Precipitation, "WI", std::move(WI));
                continue;
            } else if (field == "Hc") {
                /* Hail accumulation, ignored */
                continue;
            } else if (field == "Hd") {
                /* Hail duration, ignored */
                continue;
            } else if (field == "Hi") {
                /* Hail intensity, ignored */
                continue;
            } else if (field == "Rp") {
                /* Rain peak intensity, ignored */
                continue;
            } else if (field == "Hp") {
                /* Hail peak intensity, ignored */
                continue;
            }
        }
        if (processMonitor) {
            if (field == "Th") {
                if (subfields.empty()) return 510;
                Variant::Root T;
                int code = handleASCIIField(T, subfields.front(), convertTemperature);
                subfields.pop_front();
                if (code > 0) {
                    return 510 + code;
                }
                remap("T", T);
                if (config.first().strictMode) {
                    if (!subfields.empty())
                        return 511;
                }
                logValue(frameTime, LogStream_Monitor, "T3", std::move(T));
                continue;
            } else if (field == "Vh") {
                if (subfields.empty()) return 520;
                Variant::Root V3;
                char unit = 0;
                int code = handleASCIIField(V3, subfields.front(), convertVoltage, &unit);
                subfields.pop_front();
                if (code > 0) {
                    return 520 + code;
                }
                remap("V3", V3);
                if (config.first().strictMode) {
                    if (!subfields.empty())
                        return 521;
                }
                realtimeValue(frameTime, "V3", std::move(V3));

                heatingFlags.clear();
                switch (unit) {
                case 'V':
                case 'v':
                    heatingFlags.insert("HeaterOn");
                    break;
                case 'W':
                case 'w':
                    heatingFlags.insert("HeaterOn");
                    heatingFlags.insert("HeatingMiddleTemperature");
                    break;
                case 'F':
                case 'f':
                    heatingFlags.insert("HeaterOn");
                    heatingFlags.insert("HeatingLowTemperature");
                    break;
                default:
                    break;
                }
                continue;
            } else if (field == "Vs") {
                if (subfields.empty()) return 530;
                Variant::Root V1;
                int code = handleASCIIField(V1, subfields.front(), convertVoltage);
                subfields.pop_front();
                if (code > 0) {
                    return 530 + code;
                }
                remap("V1", V1);
                if (config.first().strictMode) {
                    if (!subfields.empty())
                        return 531;
                }
                realtimeValue(frameTime, "V1", std::move(V1));
                continue;
            } else if (field == "Vr") {
                if (subfields.empty()) return 540;
                Variant::Root V2;
                int code = handleASCIIField(V2, subfields.front(), convertVoltage);
                subfields.pop_front();
                if (code > 0) {
                    return 540 + code;
                }
                remap("V2", V2);
                if (config.first().strictMode) {
                    if (!subfields.empty())
                        return 531;
                }
                realtimeValue(frameTime, "V2", std::move(V2));
                continue;
            } else if (field == "Id") {
                if (!subfields.empty()) {
                    auto instrumentID = subfields.front().toString();
                    subfields.pop_front();
                    if (instrumentMeta["ID"].toString() != instrumentID) {
                        instrumentMeta["ID"].setString(std::move(instrumentID));
                        haveEmittedLogMeta = false;
                        haveEmittedRealtimeMeta = false;
                        sourceMetadataUpdated();
                    }
                }
                continue;
            }
        }
        if (processAuxiliary) {
            if (field == "Tr") {
                if (subfields.empty()) return 410;
                Variant::Root T;
                int code = handleASCIIField(T, subfields.front(), convertTemperature);
                subfields.pop_front();
                if (code > 0) {
                    return 410 + code;
                } else if (code == 0) {
                    recognizedFields++;
                }
                remap("T4", T);
                if (config.first().strictMode) {
                    if (!subfields.empty())
                        return 411;
                }
                if (auxiliaryEffective.temperature != Auxiliary::Setting::On &&
                        config.front().auxiliary.temperature != Auxiliary::Setting::Off) {
                    auxiliaryEffective.temperature = Auxiliary::Setting::On;
                    haveEmittedRealtimeMeta = false;
                    haveEmittedLogMeta = false;
                }
                if (auxiliaryEffective.temperature != Auxiliary::Setting::On)
                    continue;
                logValue(frameTime, LogStream_Auxiliary, "T4", std::move(T));
                continue;
            } else if (field == "Ra") {
                if (subfields.empty()) return 420;
                if (auxiliaryEffective.rain != Auxiliary::Setting::On &&
                        config.front().auxiliary.rain == Auxiliary::Setting::On) {
                    auxiliaryEffective.rain = Auxiliary::Setting::On;
                }
                if (auxiliaryEffective.rain != Auxiliary::Setting::On)
                    continue;
                int code =
                        handleASCIIField(rainAccumulation, subfields.front(), convertPrecipitation);
                subfields.pop_front();
                if (code > 0) {
                    return 420 + code;
                }
                remap("ZWItAux", rainAccumulation);
                if (config.first().strictMode) {
                    if (!subfields.empty())
                        return 421;
                }
                continue;
            } else if (field == "Sl") {
                if (subfields.empty()) return 430;
                Variant::Root Ld;
                int code = handleASCIIField(Ld, subfields.front(), convertVoltage);
                subfields.pop_front();
                if (code > 0) {
                    return 430 + code;
                } else if (code == 0) {
                    recognizedFields++;
                }
                remap("Ld", Ld);
                if (config.first().strictMode) {
                    if (!subfields.empty())
                        return 431;
                }
                if (auxiliaryEffective.level != Auxiliary::Setting::On &&
                        config.front().auxiliary.level != Auxiliary::Setting::Off) {
                    auxiliaryEffective.level = Auxiliary::Setting::On;
                    haveEmittedRealtimeMeta = false;
                    haveEmittedLogMeta = false;
                }
                if (auxiliaryEffective.level != Auxiliary::Setting::On)
                    continue;
                logValue(frameTime, LogStream_Auxiliary, "Ld", std::move(Ld));
                continue;
            } else if (field == "Sr") {
                if (subfields.empty()) return 440;
                Variant::Root VA;
                int code = handleASCIIField(VA, subfields.front(), convertVoltage);
                subfields.pop_front();
                if (code > 0) {
                    return 440 + code;
                } else if (code == 0) {
                    recognizedFields++;
                }
                remap("VA", VA);
                if (config.first().strictMode) {
                    if (!subfields.empty())
                        return 441;
                }
                if (auxiliaryEffective.solarRadiation != Auxiliary::Setting::On &&
                        config.front().auxiliary.solarRadiation != Auxiliary::Setting::Off) {
                    auxiliaryEffective.solarRadiation = Auxiliary::Setting::On;
                    haveEmittedRealtimeMeta = false;
                    haveEmittedLogMeta = false;
                }
                if (auxiliaryEffective.solarRadiation != Auxiliary::Setting::On)
                    continue;
                logValue(frameTime, LogStream_Auxiliary, "VA", std::move(VA));
                continue;
            }
        }

        if (config.first().strictMode) {
            if (!fields.empty())
                return 98;
        }
    }

    double accum = rainAccumulation.read().toDouble();
    if (processPrecipitation && FP::defined(accum)) {
        if (!haveRainIntensity) {
            if (FP::defined(rainAccumulationTime) &&
                    FP::defined(rainAccumulationTotal) &&
                    frameTime > rainAccumulationTime &&
                    rainAccumulationTotal <= accum) {
                Variant::Root WI((accum - rainAccumulationTotal) /
                                         (frameTime - rainAccumulationTime) /
                                         3600.0);
                remap("WI", WI);
                logValue(frameTime, LogStream_Precipitation, "WI", std::move(WI));
                haveRainIntensity = true;
            }
        }
        rainAccumulationTime = frameTime;
        rainAccumulationTotal = accum;
    }

    if (processWinds)
        advanceStream(frameTime, LogStream_Winds);
    if (processTemperaturePressureRH)
        advanceStream(frameTime, LogStream_TemperaturePressureRH);
    if (processPrecipitation)
        advanceStream(frameTime, LogStream_Precipitation);
    if (processMonitor)
        advanceStream(frameTime, LogStream_Monitor);
    advanceState(frameTime);

    if (config.first().strictMode) {
        if (!fields.empty())
            return 99;
    }

    if (recognizedFields <= 0)
        return -1;

    return 0;
}

int AcquireVaisalaWXT5xx::processRecordNMEA(Util::ByteView line, double frameTime)
{
    if (line.size() < 9) return 1;
    if (line.front() != '$') return 2;

    {
        auto csum = line.mid(line.size() - 3);
        if (csum[0] != '*') return 3;
        csum = csum.mid(1);
        bool ok = false;
        quint8 expected = (quint8) csum.string_trimmed().parse_u16(&ok, 16);
        if (!ok || expected > 0xFF) return 4;

        line = line.mid(0, line.size() - 3);

        quint8 calculated = 0;
        for (const char *add = line.data<const char *>(1), *end = add + line.size() - 1;
                add != end;
                ++add) {
            calculated ^= (quint8) (*add);
        }
        if (calculated != expected) return 5;
    }

    auto fields = CSV::acquisitionSplit(line);
    bool ok = false;

    if (fields.empty()) return 6;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (field == "$WIMWV") {
        if (fields.size() < 2) return 100;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root WD(field.parse_real(&ok));
        if (!ok) { WD.write().setEmpty(); }
        field = fields.front().string_trimmed();
        fields.pop_front();
        char WDUnit = 0;
        if (!field.empty())
            WDUnit = field[0];

        if (fields.size() < 2) return 101;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root WS(field.parse_real(&ok));
        if (!ok) { WS.write().setEmpty(); }
        field = fields.front().string_trimmed();
        fields.pop_front();
        char WSUnit = 0;
        if (!field.empty())
            WSUnit = field[0];

        if (fields.empty()) return 102;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (field.empty()) return 103;
        if (field[0] != 'A') {
            if (field[0] != 'V') {
                if (config.first().strictMode)
                    return 104;
            }
            WS.write().setDouble(FP::undefined());
            WD.write().setDouble(FP::undefined());
        } else {
            if (!FP::defined(WD.read().toReal()) && WDUnit != '#')
                return 105;
            if (!FP::defined(WS.read().toReal()) && WSUnit != '#')
                return 106;
            int code = convertWindDirection(WD, WDUnit);
            if (code != 0)
                return 110 + code;
            code = convertWindSpeed(WS, WSUnit);
            if (code != 0)
                return 120 + code;
        }

        updateMetadata(frameTime);
        logValue(frameTime, LogStream_Winds, "WS", std::move(WS));
        logValue(frameTime, LogStream_Winds, "WD", std::move(WD));
        advanceStream(frameTime, LogStream_Winds);
        advanceState(frameTime);
    } else if (field == "$WIXDR") {
        int idOffset = instrumentAddress;
        if (idOffset < 0)
            idOffset = config.first().address;
        if (idOffset < 0)
            idOffset = 0;

        updateMetadata(frameTime);

        bool haveRainIntensity = false;
        bool haveWindSpeedDirection = false;
        bool haveTemperaturePressureRH = false;
        bool havePrecipitation = false;
        bool haveAuxiliary = false;
        bool haveMonitor = false;
        Variant::Root rainAccumulation;
        while (!fields.empty()) {
            if (fields.size() < 4) return 200;

            field = fields.front().string_trimmed();
            fields.pop_front();
            if (field.size() < 1) return 201;
            char transducerType = field[0];

            if (transducerType == 'G') {
                auto instrumentID = fields.front().string_trimmed().toString();
                fields.pop_front();
                fields.pop_front();
                field = fields.front().string_trimmed();
                fields.pop_front();
                int id = field.parse_i32(&ok);
                if (!ok || id < 0) return 203;
                if (id < idOffset) continue;
                id -= idOffset;

                if (id == 4) {
                    if (instrumentMeta["ID"].toString() != instrumentID) {
                        instrumentMeta["ID"].setString(std::move(instrumentID));
                        haveEmittedLogMeta = false;
                        haveEmittedRealtimeMeta = false;
                        sourceMetadataUpdated();
                    }
                }

                continue;
            }
            field = fields.front().string_trimmed();
            fields.pop_front();
            Variant::Root v(field.parse_real(&ok));
            if (!ok) { v.write().setEmpty(); }

            field = fields.front().string_trimmed();
            fields.pop_front();
            char unit = 0;
            if (!field.empty())
                unit = field[0];

            field = fields.front().string_trimmed();
            fields.pop_front();
            int id = field.parse_i32(&ok);
            if (!ok || id < 0) return 202;
            if (id < idOffset) continue;
            id -= idOffset;

            int code;
            switch (transducerType) {
            case 'C':
            case 'c':
                code = convertTemperature(v, unit);
                if (code > 0)
                    return 210 + code;
                switch (id) {
                case 0:
                    remap("T1", v);
                    logValue(frameTime, LogStream_TemperaturePressureRH, "T1", std::move(v));
                    haveTemperaturePressureRH = true;
                    break;
                case 1:
                    remap("T2", v);
                    logValue(frameTime, LogStream_TemperaturePressureRH, "T2", std::move(v));
                    haveTemperaturePressureRH = true;
                    break;
                case 2:
                    remap("T3", v);
                    logValue(frameTime, LogStream_Monitor, "T3", std::move(v));
                    haveMonitor = true;
                    break;
                case 3:
                    if (auxiliaryEffective.temperature != Auxiliary::Setting::On &&
                            config.front().auxiliary.temperature != Auxiliary::Setting::Off) {
                        auxiliaryEffective.temperature = Auxiliary::Setting::On;
                        haveEmittedRealtimeMeta = false;
                        haveEmittedLogMeta = false;
                    }
                    if (auxiliaryEffective.temperature == Auxiliary::Setting::On) {
                        remap("T4", v);
                        logValue(frameTime, LogStream_Auxiliary, "T4", std::move(v));
                        haveAuxiliary = true;
                    }
                    break;
                default:
                    if (config.first().strictMode)
                        return 210;
                    break;
                }
                break;

            case 'A':
            case 'a':
                code = convertWindDirection(v, unit);
                if (code > 0)
                    return 220 + code;
                switch (id) {
                case 0:
                    /* Wind direction minimum, ignored */
                    break;
                case 1:
                    remap("WD", v);
                    logValue(frameTime, LogStream_Winds, "WD", std::move(v));
                    haveWindSpeedDirection = true;
                    break;
                case 2:
                    /* Wind direction maximum, ignored */
                    break;
                default:
                    if (config.first().strictMode)
                        return 220;
                    break;
                }
                break;

            case 'S':
            case 's':
                code = convertWindSpeed(v, unit);
                if (code > 0)
                    return 230 + code;
                switch (id) {
                case 0:
                    /* Wind speed minimum, ignored */
                    break;
                case 1:
                    remap("WS", v);
                    logValue(frameTime, LogStream_Winds, "WS", std::move(v));
                    haveWindSpeedDirection = true;
                    break;
                case 2:
                    remap("ZWSGust", v);
                    logValue(frameTime, LogStream_Winds, "ZWSGust", std::move(v));
                    haveWindSpeedDirection = true;
                    break;
                default:
                    if (config.first().strictMode)
                        return 230;
                    break;
                }
                break;

            case 'P':
            case 'p':
                code = convertPressure(v, unit);
                if (code > 0)
                    return 240 + code;
                switch (id) {
                case 0:
                    remap("P", v);
                    logValue(frameTime, LogStream_TemperaturePressureRH, "P", std::move(v));
                    haveTemperaturePressureRH = true;
                    break;
                default:
                    if (config.first().strictMode)
                        return 240;
                    break;
                }
                break;

            case 'H':
            case 'h':
                code = convertRH(v, unit);
                if (code > 0)
                    return 250 + code;
                switch (id) {
                case 0:
                    remap("U1", v);
                    logValue(frameTime, LogStream_TemperaturePressureRH, "U1", std::move(v));
                    haveTemperaturePressureRH = true;
                    break;
                default:
                    if (config.first().strictMode)
                        return 250;
                    break;
                }
                break;

            case 'V':
            case 'v':
                code = convertPrecipitation(v, unit);
                if (code > 0)
                    return 260 + code;
                switch (id) {
                case 0:
                    remap("ZWIt", v);
                    rainAccumulation = std::move(v);
                    break;
                case 1:
                    /* Hail accumulation, ignored, but shared with auxiliary rain */
                    if (config.first().auxiliary.rain == Auxiliary::Setting::On) {
                        remap("ZWItAux", v);
                        rainAccumulation = std::move(v);
                    }
                    break;
                default:
                    if (config.first().strictMode)
                        return 260;
                    break;
                }
                break;

            case 'Z':
            case 'z':
                switch (id) {
                case 0:
                    /* Rain duration, ignored */
                    break;
                case 1:
                    /* Hail duration, ignored */
                    break;
                default:
                    if (config.first().strictMode)
                        return 270;
                    break;
                }
                break;

            case 'R':
            case 'r':
                code = convertPrecipitation(v, unit);
                if (code > 0)
                    return 280 + code;
                switch (id) {
                case 0:
                    remap("WI", v);
                    logValue(frameTime, LogStream_Precipitation, "WI", std::move(v));
                    havePrecipitation = true;
                    haveRainIntensity = true;
                    break;
                case 1:
                    /* Hail intensity, ignored */
                    break;
                case 2:
                    /* Rain peak intensity, ignored */
                    break;
                case 3:
                    /* Hail peak intensity, ignored */
                    break;
                default:
                    if (config.first().strictMode)
                        return 280;
                    break;
                }
                break;

            case 'U':
            case 'u':
                code = convertVoltage(v, unit);
                if (code > 0)
                    return 290 + code;
                switch (id) {
                case 0:
                    remap("V1", v);
                    realtimeValue(frameTime, "V1", std::move(v));
                    break;
                case 1:
                    remap("V3", v);
                    realtimeValue(frameTime, "V3", std::move(v));

                    heatingFlags.clear();
                    switch (unit) {
                    case 'V':
                    case 'v':
                        heatingFlags.insert("HeaterOn");
                        break;
                    case 'W':
                    case 'w':
                        heatingFlags.insert("HeaterOn");
                        heatingFlags.insert("HeatingMiddleTemperature");
                        break;
                    case 'F':
                    case 'f':
                        heatingFlags.insert("HeaterOn");
                        heatingFlags.insert("HeatingLowTemperature");
                        break;
                    default:
                        break;
                    }
                    break;
                case 2:
                    remap("V2", v);
                    realtimeValue(frameTime, "V2", std::move(v));
                    break;
                case 3:
                    if (auxiliaryEffective.solarRadiation != Auxiliary::Setting::On &&
                            config.front().auxiliary.solarRadiation != Auxiliary::Setting::Off) {
                        auxiliaryEffective.solarRadiation = Auxiliary::Setting::On;
                        haveEmittedRealtimeMeta = false;
                        haveEmittedLogMeta = false;
                    }
                    if (auxiliaryEffective.solarRadiation == Auxiliary::Setting::On) {
                        remap("VA", v);
                        logValue(frameTime, LogStream_Auxiliary, "VA", std::move(v));
                        haveAuxiliary = true;
                    }
                    break;
                case 4:
                    if (auxiliaryEffective.level != Auxiliary::Setting::On &&
                            config.front().auxiliary.level != Auxiliary::Setting::Off) {
                        auxiliaryEffective.level = Auxiliary::Setting::On;
                        haveEmittedRealtimeMeta = false;
                        haveEmittedLogMeta = false;
                    }
                    if (auxiliaryEffective.level == Auxiliary::Setting::On) {
                        remap("Ld", v);
                        logValue(frameTime, LogStream_Auxiliary, "Ld", std::move(v));
                        haveAuxiliary = true;
                    }
                    break;
                default:
                    if (config.first().strictMode)
                        return 290;
                    break;
                }
                break;

            case 'G':
            case 'g':
                break;

            default: {
                if (config.first().strictMode)
                    return 299;
                break;
            }
            }
        }

        double accum = rainAccumulation.read().toDouble();
        if (FP::defined(accum)) {
            if (!haveRainIntensity) {
                if (FP::defined(rainAccumulationTime) &&
                        FP::defined(rainAccumulationTotal) &&
                        frameTime > rainAccumulationTime &&
                        rainAccumulationTotal <= accum) {
                    Variant::Root WI((accum - rainAccumulationTotal) /
                                             (frameTime - rainAccumulationTime) /
                                             3600.0);
                    remap("WI", WI);
                    logValue(frameTime, LogStream_Precipitation, "WI", std::move(WI));
                    haveRainIntensity = true;
                    havePrecipitation = true;
                }
            }
            rainAccumulationTime = frameTime;
            rainAccumulationTotal = accum;
        }

        if (haveWindSpeedDirection)
            advanceStream(frameTime, LogStream_Winds);
        if (haveTemperaturePressureRH)
            advanceStream(frameTime, LogStream_TemperaturePressureRH);
        if (havePrecipitation)
            advanceStream(frameTime, LogStream_Precipitation);
        if (haveAuxiliary)
            advanceStream(frameTime, LogStream_Auxiliary);
        if (haveMonitor)
            advanceStream(frameTime, LogStream_Monitor);
        advanceState(frameTime);

        if (!haveWindSpeedDirection && !haveTemperaturePressureRH && !havePrecipitation)
            return -1;
    } else {
        if (config.first().strictMode) {
            return 999;
        }
        return -1;
    }

    if (config.first().strictMode) {
        if (!fields.empty())
            return 998;
    }

    return 0;
}

int AcquireVaisalaWXT5xx::processRecord(const Util::ByteView &line, double frameTime)
{
    Q_ASSERT(!config.isEmpty());

    if (line.size() == 1)
        return -1;
    if (line.size() < 4)
        return 1;

    /* Require manufacturer specific in passive autoprobe */
    if (line.front() == '$' &&
            (responseState != RESP_AUTOPROBE_PASSIVE_WAIT &&
                    responseState != RESP_AUTOPROBE_PASSIVE_INITIALIZE)) {
        int code = processRecordNMEA(line, frameTime);
        if (code < 0)
            return code;
        if (code > 0)
            return code + 1000;
    } else {
        int code = processRecordASCII(line, frameTime);
        if (code < 0)
            return code;
        if (code > 0)
            return code + 2000;
    }

    return 0;
}

void AcquireVaisalaWXT5xx::configAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

void AcquireVaisalaWXT5xx::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
}

void AcquireVaisalaWXT5xx::invalidateLogValues(double frameTime)
{
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;

        if (loggingEgress)
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    loggingMux.clear();
    loggingLost(frameTime);

    rainAccumulationTime = FP::undefined();
    rainAccumulationTotal = FP::undefined();

    autoprobeValidRecords = 0;

    heatingFlags.clear();
}

void AcquireVaisalaWXT5xx::sendCommandSDI12(const Util::ByteView &command, int addr)
{
    if (!controlStream)
        return;
    if (addr < 0)
        addr = instrumentAddress;
    if (addr < 0)
        addr = 0;
    Util::ByteArray send;
    if (addr < 10) {
        send.push_back('0' + addr);
    } else if (addr < 10 + 26) {
        send.push_back('A' + addr - 10);
    } else if (addr < 10 + 26 + 26) {
        send.push_back('A' + addr - (10 + 26));
    } else {
        send.push_back('0');
    }
    send += command;
    send.push_back('!');
    controlStream->writeControl(std::move(send));
}

void AcquireVaisalaWXT5xx::sendCommandASCII(const Util::ByteView &command, int addr)
{
    if (!controlStream)
        return;
    if (addr < 0)
        addr = instrumentAddress;
    if (addr < 0)
        addr = 0;
    Util::ByteArray send;
    if (addr < 10) {
        send.push_back('0' + addr);
    } else if (addr < 10 + 26) {
        send.push_back('A' + addr - 10);
    } else if (addr < 10 + 26 + 26) {
        send.push_back('A' + addr - (10 + 26));
    } else {
        send.push_back('0');
    }
    send += command;
    send += "\r\n";
    controlStream->writeControl(std::move(send));
}

void AcquireVaisalaWXT5xx::sendCommandNMEA(const Util::ByteView &command)
{
    if (!controlStream)
        return;
    quint8 calculated = 0;
    for (const char *add = command.data<const char *>(), *end = add + command.size();
            add != end;
            ++add) {
        calculated ^= (quint8) (*add);
    }

    QByteArray send(1, '$');
    send.append(command);
    send.append('*');
    send.append(QByteArray::number(calculated, 16).toLower());
    send.append("\r\n");
    controlStream->writeControl(send);
}

bool AcquireVaisalaWXT5xx::breakdownSettingsFields(const Util::ByteView &frame,
                                                   double frameTime,
                                                   std::deque<std::pair<char,
                                                                        CPD3::Util::ByteView>> &output)
{
    auto fields = CSV::acquisitionSplit(frame);
    if (fields.size() < 2) {
        qCDebug(log) << "Interactive start comms read state" << responseState << "failed to parse"
                     << frame << "at" << Logging::time(frameTime);

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);
        invalidateLogValues(frameTime);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        return false;
    }
    auto field = fields.front();
    fields.pop_front();
    if (field.empty())
        return false;
    int addr = toAddress(field[0]);
    if (addr < 0)
        return false;
    if (addr != instrumentAddress)
        return false;

    while (!fields.empty()) {
        field = fields.front();
        fields.pop_front();
        if (field.size() < 2 || field[1] != '=') {
            qCDebug(log) << "Interactive start comms read state" << responseState
                         << "failed to breakdown field in" << frame << "at"
                         << Logging::time(frameTime);

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            invalidateLogValues(frameTime);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            return false;
        }
        output.emplace_back(field[0], field.mid(2));
    }

    return true;
}

void AcquireVaisalaWXT5xx::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (FP::defined(frameTime))
        configAdvance(frameTime);

    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 5) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_PASSIVE_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                timeoutAt(frameTime + unpolledResponseTime + 1.0);

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
        } else if (code > 0) {
            autoprobeValidRecords = 0;
            invalidateLogValues(frameTime);
        }
        break;
    }
    case RESP_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);

            responseState = RESP_PASSIVE_RUN;
            ++autoprobeValidRecords;
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else if (code > 0) {
            autoprobeValidRecords = 0;
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            timeoutAt(frameTime + unpolledResponseTime + 1.0);
            responseState = RESP_UNPOLLED_RUN;
            ++autoprobeValidRecords;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            autoprobeValidRecords = 0;

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            discardData(frameTime + 30.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + unpolledResponseTime + 1.0);
            ++autoprobeValidRecords;
        } else if (code > 0) {
            qCDebug(log) << "Line at " << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();

            if (responseState == RESP_PASSIVE_RUN) {
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
                info.hash("ResponseState").setString("PassiveRun");
            } else {
                if (controlStream) {
                    controlStream->writeControl("\r\n");
                    sendCommandASCII("XU,I=0,M=P");
                    controlStream->writeControl("!\r\n");
                    sendCommandSDI12("XXU,I=0");
                    controlStream->writeControl("!\r\n");
                }
                responseState = RESP_INTERACTIVE_START_FLUSH;
                discardData(frameTime + unpolledResponseTime + 1.0);
                timeoutAt(frameTime + unpolledResponseTime + 6.0);
                info.hash("ResponseState").setString("Run");
            }
            generalStatusUpdated();
            autoprobeValidRecords = 0;

            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_READ_ADDRESS_NMEA_FINAL:
    case RESP_INTERACTIVE_START_READ_ADDRESS_SDI12:
    case RESP_INTERACTIVE_START_READ_ADDRESS_ASCII: {
        if (frame.size() != 1)
            break;
        int addr = toAddress(frame.front());
        if (addr < 0)
            break;
        if (config.first().address >= 0 && config.first().address != addr)
            break;

        instrumentAddress = addr;
        instrumentSDI12Mode = (responseState == RESP_INTERACTIVE_START_READ_ADDRESS_SDI12);

        responseState = RESP_INTERACTIVE_START_READ_COMMUNICATIONS;
        if (instrumentSDI12Mode) {
            sendCommandSDI12("XXU");
        } else {
            sendCommandASCII("XU");
        }
        timeoutAt(frameTime + 5.0);
        break;
    }

    case RESP_INTERACTIVE_START_RESET_EVERYTHING: {
        if (frame.size() < 1)
            break;

        if (frame.front() == '$') {
            if (frame.size() < 9)
                break;
            {
                auto csum = frame.mid(frame.size() - 3);
                if (csum.front() != '*') break;
                csum = csum.mid(1);
                bool ok = false;
                quint8 expected = (quint8) csum.string_trimmed().parse_u16(&ok, 16);
                if (!ok || expected > 0xFF) break;

                quint8 calculated = 0;
                for (const char *add = frame.data<const char *>(1), *end = add + frame.size() - 4;
                        add != end;
                        ++add) {
                    calculated ^= (quint8) (*add);
                }
                if (calculated != expected) break;
            }

            responseState = RESP_INTERACTIVE_START_READ_ADDRESS_NMEA_FINAL;
            instrumentSDI12Mode = false;
            break;
        }

        int addr = toAddress(frame.front());
        if (addr < 0)
            break;

        if (frame.size() != 1) {
            if (!frame.mid(1).string_start_insensitive("tx,start-up"))
                break;
            instrumentSDI12Mode = true;
        } else {
            instrumentSDI12Mode = false;
        }
        if (config.first().address >= 0 && config.first().address != addr)
            break;
        instrumentAddress = addr;

        responseState = RESP_INTERACTIVE_START_READ_COMMUNICATIONS;
        if (instrumentSDI12Mode) {
            sendCommandSDI12("XXU");
        } else {
            sendCommandASCII("XU");
        }
        timeoutAt(frameTime + 5.0);
        break;
    }

    case RESP_INTERACTIVE_START_READ_COMMUNICATIONS: {
        if (frame.size() < 4) {
            if (instrumentSDI12Mode) {
                if (frame.mid(1, 3) != "XXU")
                    break;
            } else {
                if (frame.mid(1, 2) != "XU")
                    break;
            }
        }
        std::deque<std::pair<char, CPD3::Util::ByteView>> output;
        if (!breakdownSettingsFields(frame, frameTime, output))
            break;

        Util::ByteArray command;
        if (instrumentSDI12Mode)
            command = "XXU,I=0";
        else
            command = "XU,I=0";

        while (!output.empty()) {
            auto field = std::move(output.front());
            output.pop_front();

            switch (field.first) {
            case 'V':
            case 'v': {
                if (field.second.empty())
                    break;
                auto firmwareVersion = field.second.toString();
                if (instrumentMeta["FirmwareVersion"].toString() != firmwareVersion) {
                    instrumentMeta["FirmwareVersion"].setString(std::move(firmwareVersion));
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    sourceMetadataUpdated();
                }
                break;
            }
            case 'M':
            case 'm': {
                if (field.second.empty())
                    break;
                switch (field.second.front()) {
                case 'A':
                case 'a':
                    command += ",M=P";
                    break;

                case 'N':
                    command += ",M=Q";
                    break;

                default:
                    break;
                }
                break;
            }
            case 'N':
            case 'n': {
                if (field.second.empty())
                    break;
                if (!field.second.string_start("WXT"))
                    break;
                auto modelNumber = Util::to_upper(field.second.toString());
                if (instrumentMeta["Model"].toString() != modelNumber) {
                    instrumentMeta["Model"].setString(std::move(modelNumber));
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    sourceMetadataUpdated();
                }
                break;
            }
            default:
                break;
            }
        }

        if (instrumentSDI12Mode)
            sendCommandSDI12(command);
        else
            sendCommandASCII(command);

        discardData(frameTime + 1.0);
        timeoutAt(frameTime + 30.0);
        break;
    }

    case RESP_INTERACTIVE_START_READ_IDENTIFICATION: {
        if (frame.size() < 3 + 8 + 6 + 3 + 8) {
            qCDebug(log) << "Interactive start comms read identification at"
                         << Logging::time(frameTime) << "failed to parse" << frame;

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            invalidateLogValues(frameTime);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            break;
        }

        int addr = toAddress(frame.front());
        if (addr < 0)
            break;
        if (addr != instrumentAddress)
            break;
        auto id = frame.mid(1, 2 + 8 + 6);
        if (!id.mid(0, 13).string_end_insensitive("13VAISALA_WXT"))
            break;
        if (id.size() > 13) {
            QString modelNumber(id.mid(13).toQByteArray());
            modelNumber.prepend("WXT");
            if (instrumentMeta["Model"].toQString() != modelNumber) {
                instrumentMeta["Model"].setString(modelNumber);
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }
        }
        {
            QString firmwareVersion(frame.mid(1 + 2 + 8 + 6, 1).toQByteArrayRef());
            firmwareVersion.append('.');
            firmwareVersion.append(frame.mid(1 + 2 + 8 + 6 + 1, 2).toQByteArrayRef());
            if (instrumentMeta["FirmwareVersion"].toQString() != firmwareVersion) {
                instrumentMeta["FirmwareVersion"].setString(firmwareVersion);
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }
        }
        {
            QString sn(frame.mid(1 + 2 + 8 + 6 + 3).toQByteArrayRef());
            bool ok = false;
            qint64 i = sn.toLongLong(&ok);
            if (ok && i > 0) {
                if (instrumentMeta["SerialNumber"].toInt64() != i) {
                    instrumentMeta["SerialNumber"].setInt64(i);
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    sourceMetadataUpdated();
                }
            } else if (instrumentMeta["SerialNumber"].toQString() != sn) {
                instrumentMeta["SerialNumber"].setString(sn);
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }
        }

        responseState = RESP_INTERACTIVE_START_WINDS;
        if (instrumentSDI12Mode)
            sendCommandSDI12("XWU");
        else
            sendCommandASCII("WU");
        timeoutAt(frameTime + 5.0);
        break;
    }

    case RESP_INTERACTIVE_START_WINDS: {
        std::deque<std::pair<char, CPD3::Util::ByteView>> output;
        if (!breakdownSettingsFields(frame, frameTime, output))
            break;

        Util::ByteArray command;
        if (instrumentSDI12Mode)
            command = "XWU,R=";
        else
            command = "WU,R=";
        command += "01001100&01001100";
        command += ",I=1,A=1,G=3,U=M,N=T,F=4";

        if (instrumentSDI12Mode)
            sendCommandSDI12(command);
        else
            sendCommandASCII(command);

        discardData(frameTime + 1.0);
        timeoutAt(frameTime + 30.0);
        break;
    }

    case RESP_INTERACTIVE_START_TEMPERATUREPRESSURERH: {
        std::deque<std::pair<char, CPD3::Util::ByteView>> output;
        if (!breakdownSettingsFields(frame, frameTime, output))
            break;

        Util::ByteArray command;
        if (instrumentSDI12Mode)
            command = "XTU,R=";
        else
            command = "TU,R=";
        command += "11110000&11110000";
        command += ",I=1,P=H,T=C";

        if (instrumentSDI12Mode)
            sendCommandSDI12(command);
        else
            sendCommandASCII(command);

        discardData(frameTime + 1.0);
        timeoutAt(frameTime + 30.0);
        break;
    }

    case RESP_INTERACTIVE_START_PRECIPITATION: {
        std::deque<std::pair<char, CPD3::Util::ByteView>> output;
        if (!breakdownSettingsFields(frame, frameTime, output))
            break;

        Util::ByteArray command;
        if (instrumentSDI12Mode)
            command = "XRU,R=";
        else
            command = "RU,R=";
        command += "00100100&00100100";
        command += ",I=1,U=M,S=M,M=T";

        if (instrumentSDI12Mode)
            sendCommandSDI12(command);
        else
            sendCommandASCII(command);

        discardData(frameTime + 1.0);
        timeoutAt(frameTime + 30.0);
        break;
    }

    case RESP_INTERACTIVE_START_AUXILIARY: {
        std::deque<std::pair<char, CPD3::Util::ByteView>> output;
        if (!breakdownSettingsFields(frame, frameTime, output)) {
            /* Failure possible on WXT52x */
            discardData(frameTime + 1.0);
            timeoutAt(frameTime + 30.0);
            break;
        }

        Util::ByteArray polledInclude("11111111");
        Util::ByteArray unpolledInclude("11111111");
        while (!output.empty()) {
            auto field = std::move(output.front());
            output.pop_front();

            switch (field.first) {
            case 'R':
            case 'r': {
                int split = field.second.indexOf('&');
                if (split != -1) {
                    polledInclude = field.second.mid(0, split);
                    unpolledInclude = field.second.mid(split + 1);
                }
                break;
            }

            default:
                break;
            }
        }

        Auxiliary oldEffective = auxiliaryEffective;

        unpolledInclude = unpolledInclude.toQByteArray().leftJustified(8, '0');
        switch (config.first().auxiliary.temperature) {
        case Auxiliary::Setting::On:
            polledInclude[0] = '1';
            unpolledInclude[0] = '1';
            auxiliaryEffective.temperature = Auxiliary::Setting::On;
            break;
        case Auxiliary::Setting::Off:
            polledInclude[0] = '0';
            unpolledInclude[0] = '0';
            auxiliaryEffective.temperature = Auxiliary::Setting::Off;
            break;
        case Auxiliary::Setting::Auto:
            auxiliaryEffective.temperature =
                    (polledInclude[0] == '1' || unpolledInclude[0] == '1') ? Auxiliary::Setting::On
                                                                           : Auxiliary::Setting::Off;
            break;
        }
        switch (config.first().auxiliary.rain) {
        case Auxiliary::Setting::On:
            polledInclude[1] = '1';
            unpolledInclude[1] = '1';
            auxiliaryEffective.rain = Auxiliary::Setting::On;
            break;
        case Auxiliary::Setting::Off:
            polledInclude[1] = '0';
            unpolledInclude[1] = '0';
            auxiliaryEffective.rain = Auxiliary::Setting::Off;
            break;
        case Auxiliary::Setting::Auto:
            auxiliaryEffective.rain =
                    (polledInclude[1] == '1' || unpolledInclude[1] == '1') ? Auxiliary::Setting::On
                                                                           : Auxiliary::Setting::Off;
            break;
        }
        switch (config.first().auxiliary.level) {
        case Auxiliary::Setting::On:
            polledInclude[2] = '1';
            unpolledInclude[2] = '1';
            auxiliaryEffective.level = Auxiliary::Setting::On;
            break;
        case Auxiliary::Setting::Off:
            polledInclude[2] = '0';
            unpolledInclude[2] = '0';
            auxiliaryEffective.level = Auxiliary::Setting::Off;
            break;
        case Auxiliary::Setting::Auto:
            auxiliaryEffective.level =
                    (polledInclude[2] == '1' || unpolledInclude[2] == '1') ? Auxiliary::Setting::On
                                                                           : Auxiliary::Setting::Off;
            break;
        }
        switch (config.first().auxiliary.solarRadiation) {
        case Auxiliary::Setting::On:
            polledInclude[3] = '1';
            unpolledInclude[3] = '1';
            auxiliaryEffective.solarRadiation = Auxiliary::Setting::On;
            break;
        case Auxiliary::Setting::Off:
            polledInclude[3] = '0';
            unpolledInclude[3] = '0';
            auxiliaryEffective.solarRadiation = Auxiliary::Setting::Off;
            break;
        case Auxiliary::Setting::Auto:
            auxiliaryEffective.solarRadiation =
                    (polledInclude[3] == '1' || unpolledInclude[3] == '1') ? Auxiliary::Setting::On
                                                                           : Auxiliary::Setting::Off;
            break;
        }

        if (oldEffective != auxiliaryEffective) {
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }


        Util::ByteArray command;
        if (instrumentSDI12Mode)
            command = "XIU,R=";
        else
            command = "IU,R=";
        command += polledInclude;
        command += "&";
        command += unpolledInclude;

        if (instrumentSDI12Mode)
            sendCommandSDI12(command);
        else
            sendCommandASCII(command);

        discardData(frameTime + 1.0);
        timeoutAt(frameTime + 30.0);
        break;
    }

    case RESP_INTERACTIVE_START_MONITOR: {
        std::deque<std::pair<char, CPD3::Util::ByteView>> output;
        if (!breakdownSettingsFields(frame, frameTime, output))
            break;

        Util::ByteArray command;
        if (instrumentSDI12Mode)
            command = "XSU,R=";
        else
            command = "SU,R=";
        command += "11111000&11111000";
        command += ",I=1,S=N";

        switch (config.first().heating) {
        case Configuration::Unchanged:
            break;
        case Configuration::Enable:
            command += ",H=Y";
            break;
        case Configuration::Disable:
            command += ",H=N";
            break;
        }

        if (instrumentSDI12Mode)
            sendCommandSDI12(command);
        else
            sendCommandASCII(command);

        discardData(frameTime + 1.0);
        timeoutAt(frameTime + 30.0);
        break;
    }

    default:
        break;
    }

}

void AcquireVaisalaWXT5xx::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configAdvance(frameTime);

    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        qCDebug(log) << "Timeout in unpolled state" << responseState << "at"
                     << Logging::time(frameTime);
        timeoutAt(frameTime + unpolledResponseTime + 30.0);

        if (controlStream) {
            controlStream->writeControl("\r\n");
            sendCommandASCII("XU,I=0,M=P");
            controlStream->writeControl("!\r\n");
            sendCommandSDI12("XXU,I=0");
            controlStream->writeControl("!\r\n");
        }
        responseState = RESP_INTERACTIVE_START_FLUSH;
        discardData(frameTime + unpolledResponseTime + 2.0);
        generalStatusUpdated();

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("UnpolledRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_START_READ_ADDRESS_ASCII:
        responseState = RESP_INTERACTIVE_START_READ_ADDRESS_SDI12;
        timeoutAt(frameTime + 2.0);
        if (controlStream != NULL) {
            controlStream->writeControl("?!");
        }
        break;

    case RESP_INTERACTIVE_START_READ_ADDRESS_SDI12:
        if (controlStream != NULL) {
            if (config.first().address != -1) {
                QByteArray send;
                if (config.first().address < 10) {
                    send.append((char) ('0' + config.first().address));
                } else if (config.first().address < 10 + 26) {
                    send.append((char) ('A' + config.first().address - 10));
                } else if (config.first().address < 10 + 26 + 26) {
                    send.append((char) ('A' + config.first().address - (10 + 26)));
                } else {
                    send.append('0');
                }
                send.append("XZ");
                controlStream->writeControl(send + "\r\n");
                controlStream->writeControl(send + "!");
            } else {
                for (int i = 0; i < 10 + 26 + 26; i++) {
                    QByteArray send;
                    if (config.first().address < 10) {
                        send.append((char) ('0' + config.first().address));
                    } else if (config.first().address < 10 + 26) {
                        send.append((char) ('A' + config.first().address - 10));
                    } else if (config.first().address < 10 + 26 + 26) {
                        send.append((char) ('A' + config.first().address - (10 + 26)));
                    } else {
                        send.append('0');
                    }
                    send.append("XZ");
                    controlStream->writeControl(send + "\r\n");
                    controlStream->writeControl(send + "!");
                }
            }
        }

        responseState = RESP_INTERACTIVE_START_RESET_EVERYTHING;
        timeoutAt(frameTime + 3.0);
        break;

    case RESP_INTERACTIVE_START_FLUSH:
    case RESP_INTERACTIVE_START_READ_ADDRESS_NMEA_FINAL:
    case RESP_INTERACTIVE_START_READ_COMMUNICATIONS:
    case RESP_INTERACTIVE_START_READ_IDENTIFICATION:
    case RESP_INTERACTIVE_START_WINDS:
    case RESP_INTERACTIVE_START_TEMPERATUREPRESSURERH:
    case RESP_INTERACTIVE_START_PRECIPITATION:
    case RESP_INTERACTIVE_START_MONITOR:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_INTERACTIVE_START_RESET_EVERYTHING:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        timeoutAt(FP::undefined());
        if (controlStream != NULL)
            controlStream->resetControl();
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        invalidateLogValues(frameTime);
        break;

        /* Timeout allowed for WXT52x */
    case RESP_INTERACTIVE_START_AUXILIARY:
        responseState = RESP_INTERACTIVE_START_MONITOR;
        timeoutAt(frameTime + 2.0);
        if (instrumentSDI12Mode)
            sendCommandSDI12("XSU");
        else
            sendCommandASCII("SU");
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        if (controlStream) {
            controlStream->writeControl("\r\n");
            sendCommandASCII("XU,I=0,M=P");
            controlStream->writeControl("!\r\n");
            sendCommandSDI12("XXU,I=0");
            controlStream->writeControl("!\r\n");
        }
        timeoutAt(frameTime + unpolledResponseTime + 30.0);
        responseState = RESP_INTERACTIVE_START_FLUSH;
        discardData(frameTime + unpolledResponseTime + 2.0);
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        invalidateLogValues(frameTime);
        break;

    default:
        invalidateLogValues(frameTime);
        break;
    }
}

static bool maybeHasAuxiliary(const Variant::Read &model)
{
    if (!model.exists())
        return true;

    const auto &m = model.toString();

    static const std::string wxt52xPrefix = "wxt52";
    if (m.length() < wxt52xPrefix.length())
        return true;
    return !std::equal(wxt52xPrefix.begin(), wxt52xPrefix.end(), m.begin(),
                       [](char cA, char cB) -> bool {
                           return cA == std::tolower(cB);
                       });

}

void AcquireVaisalaWXT5xx::discardDataCompleted(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configAdvance(frameTime);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_INTERACTIVE_START_FLUSH:
        responseState = RESP_INTERACTIVE_START_READ_ADDRESS_ASCII;
        timeoutAt(frameTime + 2.0);
        if (controlStream != NULL) {
            controlStream->writeControl("?\r\n");
        }
        break;

    case RESP_INTERACTIVE_START_READ_COMMUNICATIONS:
        timeoutAt(frameTime + 2.0);
        if (instrumentSDI12Mode) {
            responseState = RESP_INTERACTIVE_START_READ_IDENTIFICATION;
            sendCommandSDI12("I");
        } else {
            responseState = RESP_INTERACTIVE_START_WINDS;
            sendCommandASCII("TU");
        }
        break;

    case RESP_INTERACTIVE_START_WINDS:
        responseState = RESP_INTERACTIVE_START_TEMPERATUREPRESSURERH;
        timeoutAt(frameTime + 2.0);
        if (instrumentSDI12Mode)
            sendCommandSDI12("XTU");
        else
            sendCommandASCII("TU");
        break;

    case RESP_INTERACTIVE_START_TEMPERATUREPRESSURERH:
        responseState = RESP_INTERACTIVE_START_PRECIPITATION;
        timeoutAt(frameTime + 2.0);
        if (instrumentSDI12Mode)
            sendCommandSDI12("XRU");
        else
            sendCommandASCII("RU");
        break;

    case RESP_INTERACTIVE_START_PRECIPITATION:
        if (!maybeHasAuxiliary(instrumentMeta["Model"])) {
            responseState = RESP_INTERACTIVE_START_MONITOR;
            timeoutAt(frameTime + 2.0);
            if (instrumentSDI12Mode)
                sendCommandSDI12("XSU");
            else
                sendCommandASCII("SU");
        } else {
            responseState = RESP_INTERACTIVE_START_AUXILIARY;
            timeoutAt(frameTime + 2.0);
            if (instrumentSDI12Mode)
                sendCommandSDI12("XIU");
            else
                sendCommandASCII("IU");
        }
        break;

    case RESP_INTERACTIVE_START_AUXILIARY:
        responseState = RESP_INTERACTIVE_START_MONITOR;
        timeoutAt(frameTime + 2.0);
        if (instrumentSDI12Mode)
            sendCommandSDI12("XSU");
        else
            sendCommandASCII("SU");
        break;

    case RESP_INTERACTIVE_START_MONITOR: {
        responseState = RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID;

        /* We can't handle SDI12 mode (yet?) so, just always set it to NMEA and hope for the
         * best */
        if (instrumentSDI12Mode) {
            sendCommandSDI12("XXU,M=N");
            instrumentSDI12Mode = false;
        } else {
            sendCommandASCII("XU,M=N");
        }

        discardData(frameTime + 2.0, 1);
        timeoutAt(frameTime + unpolledResponseTime * 3.0 + 3.0);
        break;
    }

    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
        break;


    case RESP_INTERACTIVE_RESTART_WAIT:
        if (controlStream) {
            controlStream->writeControl("\r\n");
            sendCommandASCII("XU,I=0,M=P");
            controlStream->writeControl("!\r\n");
            sendCommandSDI12("XXU,I=0");
            controlStream->writeControl("!\r\n");
        }
        timeoutAt(frameTime + unpolledResponseTime + 30.0);
        responseState = RESP_INTERACTIVE_START_FLUSH;
        discardData(frameTime + unpolledResponseTime + 2.0);
        break;

    default:
        break;
    }
}


void AcquireVaisalaWXT5xx::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frame);
    Q_UNUSED(frameTime);
}

Variant::Root AcquireVaisalaWXT5xx::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireVaisalaWXT5xx::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}


AcquisitionInterface::AutoprobeStatus AcquireVaisalaWXT5xx::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireVaisalaWXT5xx::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << " at " << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_FLUSH:
    case RESP_INTERACTIVE_START_READ_ADDRESS_ASCII:
    case RESP_INTERACTIVE_START_READ_ADDRESS_SDI12:
    case RESP_INTERACTIVE_START_RESET_EVERYTHING:
    case RESP_INTERACTIVE_START_READ_ADDRESS_NMEA_FINAL:
    case RESP_INTERACTIVE_START_READ_COMMUNICATIONS:
    case RESP_INTERACTIVE_START_READ_IDENTIFICATION:
    case RESP_INTERACTIVE_START_WINDS:
    case RESP_INTERACTIVE_START_TEMPERATUREPRESSURERH:
    case RESP_INTERACTIVE_START_PRECIPITATION:
    case RESP_INTERACTIVE_START_AUXILIARY:
    case RESP_INTERACTIVE_START_MONITOR:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        if (controlStream) {
            controlStream->writeControl("\r\n");
            sendCommandASCII("XU,I=0,M=P");
            controlStream->writeControl("!\r\n");
            sendCommandSDI12("XXU,I=0");
            controlStream->writeControl("!\r\n");
        }
        timeoutAt(time + unpolledResponseTime + 30.0);
        responseState = RESP_INTERACTIVE_START_FLUSH;
        discardData(time + unpolledResponseTime + 2.0);

        generalStatusUpdated();
        break;
    }
}

void AcquireVaisalaWXT5xx::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobeValidRecords = 0;
    discardData(time + 0.5, 1);
    timeoutAt(time + unpolledResponseTime * 7.0 + 3.0);
    generalStatusUpdated();
}

void AcquireVaisalaWXT5xx::autoprobePromote(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        qCDebug(log) << "Promoted from interactive run to interactive acquisition at"
                     << Logging::time(time);
        /* Reset timeout in case we didn't have a control stream */
        timeoutAt(time + unpolledResponseTime + 2.0);
        break;

    case RESP_INTERACTIVE_START_FLUSH:
    case RESP_INTERACTIVE_START_READ_ADDRESS_ASCII:
    case RESP_INTERACTIVE_START_READ_ADDRESS_SDI12:
    case RESP_INTERACTIVE_START_RESET_EVERYTHING:
    case RESP_INTERACTIVE_START_READ_ADDRESS_NMEA_FINAL:
    case RESP_INTERACTIVE_START_READ_COMMUNICATIONS:
    case RESP_INTERACTIVE_START_READ_IDENTIFICATION:
    case RESP_INTERACTIVE_START_WINDS:
    case RESP_INTERACTIVE_START_TEMPERATUREPRESSURERH:
    case RESP_INTERACTIVE_START_PRECIPITATION:
    case RESP_INTERACTIVE_START_AUXILIARY:
    case RESP_INTERACTIVE_START_MONITOR:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);
        break;

    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        if (controlStream) {
            controlStream->writeControl("\r\n");
            sendCommandASCII("XU,I=0,M=P");
            controlStream->writeControl("!\r\n");
            sendCommandSDI12("XXU,I=0");
            controlStream->writeControl("!\r\n");
        }
        timeoutAt(time + unpolledResponseTime + 30.0);
        responseState = RESP_INTERACTIVE_START_FLUSH;
        discardData(time + unpolledResponseTime + 2.0);

        generalStatusUpdated();
        break;
    }
}

void AcquireVaisalaWXT5xx::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    /* Reset timeout in case we didn't have a control stream */
    timeoutAt(time + unpolledResponseTime + 2.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);

        break;

    case RESP_UNPOLLED_RUN:
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from unpolled interactive state to passive acquisition at"
                     << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;

        discardData(FP::undefined());
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireVaisalaWXT5xx::getDefaults()
{
    AutomaticDefaults result;
    result.name = "XM$2";
    result.setSerialN81(19200);
    return result;
}


ComponentOptions AcquireVaisalaWXT5xxComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options);

    return options;
}

ComponentOptions AcquireVaisalaWXT5xxComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireVaisalaWXT5xxComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireVaisalaWXT5xxComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireVaisalaWXT5xxComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options);
}

QList<ComponentExample> AcquireVaisalaWXT5xxComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireVaisalaWXT5xxComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                      const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireVaisalaWXT5xx(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireVaisalaWXT5xxComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                      const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireVaisalaWXT5xx(config, loggingContext)); }

std::unique_ptr<AcquisitionInterface> AcquireVaisalaWXT5xxComponent::createAcquisitionAutoprobe(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireVaisalaWXT5xx> i(new AcquireVaisalaWXT5xx(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireVaisalaWXT5xxComponent::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireVaisalaWXT5xx> i(new AcquireVaisalaWXT5xx(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
