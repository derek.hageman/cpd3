/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"
#include "algorithms/crc.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;
    enum {
        Polled_ASCII,
        Polled_ASCII_CRC,
        Unpolled_ASCII,
        Unpolled_ASCII_CRC,
        Unpolled_NMEA,
        Polled_NMEA,
        SDI12,
        SDI12_Cont
    } outputMode;
    int address;
    int unpolledInterval;
    bool is3xSeries;

    enum {
        Enable_Dn = (1 << 7),
        Enable_Dm = (1 << 6),
        Enable_Dx = (1 << 5),
        Enable_Sn = (1 << 4),
        Enable_Sm = (1 << 3),
        Enable_Sx = (1 << 2),

        Enable_Pa = (1 << 7),
        Enable_Ta = (1 << 6),
        Enable_Tp = (1 << 5),
        Enable_Ua = (1 << 4),

        Enable_Rc = (1 << 7),
        Enable_Rd = (1 << 6),
        Enable_Ri = (1 << 5),
        Enable_Hc = (1 << 4),
        Enable_Hd = (1 << 3),
        Enable_Hi = (1 << 2),
        Enable_Rp = (1 << 1),
        Enable_Hp = (1 << 0),

        Enable_Tr = (1 << 7), Enable_Ra = (1 << 6), Enable_Sl = (1 << 5), Enable_Sr = (1 << 4),

        Enable_Th = (1 << 7),
        Enable_Vh = (1 << 6),
        Enable_Vs = (1 << 5),
        Enable_Vr = (1 << 4),
        Enable_Id = (1 << 3),
    };
    std::uint16_t enableWinds;
    std::uint16_t enableTPU;
    std::uint16_t enablePrecipitation;
    std::uint16_t enableAuxiliary;
    std::uint16_t enableMonitor;

    double Dn;
    double Dm;
    double Dx;
    double Sn;
    double Sm;
    double Sx;

    double Pa;
    double Ta;
    double Tp;
    double Ua;

    double Rc;
    double Rd;
    double Ri;
    double Hc;
    double Hd;
    double Hi;
    double Rp;
    double Hp;

    double Tr;
    double Ra;
    double Sl;
    double Sr;

    double Th;
    double Vh;
    double Vs;
    double Vr;

    ModelInstrument()
            : incoming(),
              outgoing(),
              unpolledRemaining(0), outputMode(Unpolled_ASCII_CRC),
              address(0),
              unpolledInterval(1), is3xSeries(true),
              enableWinds(0xFFFF),
              enableTPU(0xFFFF),
              enablePrecipitation(0xFFFF), enableAuxiliary(0xFFFF),
              enableMonitor(0xFFFF)
    {
        Dn = 170;
        Dm = 180;
        Dx = 190;
        Sn = 5;
        Sm = 6;
        Sx = 7;

        Pa = 1000;
        Ta = 25;
        Tp = 26;
        Ua = 70;

        Rc = 600;
        Rd = 30;
        Ri = 8;

        Hc = 500;
        Hd = 31;
        Hi = 9;
        Rp = 10;
        Hp = 11;

        Tr = 27.0;
        Ra = 601;
        Sl = 2.5;
        Sr = 900;

        Th = 27;
        Vh = 12;
        Vs = 9.5;
        Vr = 3.5;
    }

    static void addASCII(QByteArray &result, const char *name, char unit, double value)
    {
        result.append(',');
        result.append(name);
        result.append('=');
        result.append(QByteArray::number(value, 'f', 1));
    }

    QByteArray windASCII(std::uint16_t enable) const
    {
        QByteArray result;
        if (enable & Enable_Dn)
            addASCII(result, "Dn", 'D', Dn);
        if (enable & Enable_Dm)
            addASCII(result, "Dm", 'D', Dm);
        if (enable & Enable_Dx)
            addASCII(result, "Dx", 'D', Dx);
        if (enable & Enable_Sn)
            addASCII(result, "Sn", 'M', Sn);
        if (enable & Enable_Sm)
            addASCII(result, "Sm", 'M', Sm);
        if (enable & Enable_Sx)
            addASCII(result, "Sx", 'M', Sx);
        return result;
    }

    QByteArray tpuASCII(std::uint16_t enable) const
    {
        QByteArray result;
        if (enable & Enable_Ta)
            addASCII(result, "Ta", 'C', Ta);
        if (enable & Enable_Ua)
            addASCII(result, "Ua", 'P', Ua);
        if (enable & Enable_Pa)
            addASCII(result, "Pa", 'H', Pa);
        if (enable & Enable_Tp)
            addASCII(result, "Tp", 'C', Tp);
        return result;
    }

    QByteArray precipitationASCII(std::uint16_t enable) const
    {
        QByteArray result;
        if (enable & Enable_Rc)
            addASCII(result, "Rc", 'M', Rc);
        if (enable & Enable_Rd)
            addASCII(result, "Rd", 's', Rd);
        if (enable & Enable_Ri)
            addASCII(result, "Ri", 'M', Ri);
        if (enable & Enable_Hc)
            addASCII(result, "Hc", 'M', Hc);
        if (enable & Enable_Hd)
            addASCII(result, "Hd", 's', Hd);
        if (enable & Enable_Hi)
            addASCII(result, "Hi", 'M', Hi);
        if (enable & Enable_Rp)
            addASCII(result, "Rp", 'M', Rp);
        if (enable & Enable_Hp)
            addASCII(result, "Hp", 'M', Hp);
        return result;
    }

    QByteArray monitorASCII(std::uint16_t enable) const
    {
        QByteArray result;
        if (enable & Enable_Th)
            addASCII(result, "Th", 'C', Th);
        if (enable & Enable_Vh)
            addASCII(result, "Vh", 'N', Vh);
        if (enable & Enable_Vs)
            addASCII(result, "Vs", 'V', Vs);
        if (enable & Enable_Vr)
            addASCII(result, "Vr", 'V', Vr);
        if (enable & Enable_Id)
            result.append(",Id=HEL___");
        return result;
    }

    QByteArray auxiliaryASCII(std::uint16_t enable) const
    {
        QByteArray result;
        if (enable & Enable_Tr)
            addASCII(result, "Tr", 'C', Tr);
        if (enable & Enable_Ra)
            addASCII(result, "Ra", 'M', Ra);
        if (enable & Enable_Sl)
            addASCII(result, "Sl", 'V', Sl);
        if (enable & Enable_Sr)
            addASCII(result, "Sr", 'V', Sr);
        return result;
    }

    void addNMEA(QByteArray &result, char type, char unit, int index, double value) const
    {
        result.append(',');
        result.append(type);
        result.append(',');
        result.append(QByteArray::number(value, 'f', 1));
        result.append(',');
        result.append(unit);
        result.append(',');
        result.append(QByteArray::number(index + address));
    }

    QByteArray windNMEA(std::uint16_t enable) const
    {
        QByteArray result;
        if (enable & Enable_Dn)
            addNMEA(result, 'A', 'D', 0, Dn);
        if (enable & Enable_Dm)
            addNMEA(result, 'A', 'D', 1, Dm);
        if (enable & Enable_Dx)
            addNMEA(result, 'A', 'D', 2, Dx);
        if (enable & Enable_Sn)
            addNMEA(result, 'S', 'M', 0, Sn);
        if (enable & Enable_Sm)
            addNMEA(result, 'S', 'M', 1, Sm);
        if (enable & Enable_Sx)
            addNMEA(result, 'S', 'M', 2, Sx);
        return result;
    }

    QByteArray tpuNMEA(std::uint16_t enable) const
    {
        QByteArray result;
        if (enable & Enable_Ta)
            addNMEA(result, 'C', 'C', 0, Ta);
        if (enable & Enable_Ua)
            addNMEA(result, 'H', 'P', 0, Ua);
        if (enable & Enable_Pa)
            addNMEA(result, 'P', 'H', 0, Pa);
        if (enable & Enable_Tp)
            addNMEA(result, 'C', 'C', 1, Tp);
        return result;
    }

    QByteArray precipitationNMEA(std::uint16_t enable) const
    {
        QByteArray result;
        if (enable & Enable_Rc)
            addNMEA(result, 'V', 'M', 0, Rc);
        if (enable & Enable_Rd)
            addNMEA(result, 'Z', 'S', 0, Rd);
        if (enable & Enable_Ri)
            addNMEA(result, 'R', 'M', 0, Ri);
        if (enable & Enable_Hc)
            addNMEA(result, 'V', 'M', 1, Hc);
        if (enable & Enable_Hd)
            addNMEA(result, 'Z', 'S', 1, Hd);
        if (enable & Enable_Hi)
            addNMEA(result, 'R', 'M', 1, Hi);
        if (enable & Enable_Rp)
            addNMEA(result, 'R', 'M', 2, Rp);
        if (enable & Enable_Hp)
            addNMEA(result, 'R', 'M', 3, Hp);
        return result;
    }

    QByteArray monitorNMEA(std::uint16_t enable) const
    {
        QByteArray result;
        if (enable & Enable_Th)
            addNMEA(result, 'C', 'C', 2, Th);
        if (enable & Enable_Vh)
            addNMEA(result, 'U', 'N', 1, Vh);
        if (enable & Enable_Vs)
            addNMEA(result, 'U', 'V', 0, Vs);
        if (enable & Enable_Vr)
            addNMEA(result, 'U', 'V', 2, Vr);
        if (enable & Enable_Id)
            result.append(",G,HEL___,,4");
        return result;
    }

    QByteArray auxiliaryNMEA(std::uint16_t enable) const
    {
        QByteArray result;
        if (enable & Enable_Ra)
            addNMEA(result, 'V', 'M', 1, Ra);
        if (enable & Enable_Tr)
            addNMEA(result, 'C', 'C', 3, Tr);
        if (enable & Enable_Sr)
            addNMEA(result, 'U', 'V', 3, Sr);
        if (enable & Enable_Sl)
            addNMEA(result, 'U', 'V', 4, Sl);
        return result;
    }

    static void addASCIIChecksum(QByteArray &record)
    {
        Q_ASSERT(record.length() > 0);
        std::uint16_t crc = Algorithms::CRC16().calculate(record);
        record.append((char) (0x40 | (crc >> 12)));
        record.append((char) (0x40 | ((crc >> 6) & 0x3F)));
        record.append((char) (0x40 | (crc & 0x3F)));
    }

    QByteArray addressCharacter() const
    {
        int a = address;
        if (a < 10)
            return QByteArray(1, (char) (a + '0'));
        a -= 10;
        if (a < 26)
            return QByteArray(1, (char) (a + 'A'));
        a -= 26;
        if (a < 26)
            return QByteArray(1, (char) (a + 'a'));
        return QByteArray(1, '0');
    }

    void sendASCIIBasic(const QByteArray &record)
    {
        QByteArray send(record);
        send.prepend(addressCharacter());
        switch (outputMode) {
        case Polled_ASCII_CRC:
        case Unpolled_ASCII_CRC:
            addASCIIChecksum(send);
            break;
        default:
            break;
        }
        send.append("\r\n");
        outgoing.append(send);
    }

    void sendASCIIRecord(const QByteArray &record)
    {
        QByteArray send(record);
        send.prepend(addressCharacter());
        switch (outputMode) {
        case Polled_ASCII_CRC:
        case Unpolled_ASCII_CRC:
            addASCIIChecksum(send);
            break;
        default:
            break;
        }
        send.append("\r\n");
        outgoing.append(send);
    }

    static void addNMEAChecksum(QByteArray &record)
    {
        Q_ASSERT(record.length() > 1);
        quint8 sum = 0;
        const char *add = record.constBegin();
        const char *end = add + record.length();
        for (; add != end; ++add) {
            sum ^= (quint8) (*add);
        }
        record.append('*');
        record.append(QByteArray::number(sum, 16).toUpper().rightJustified(2, '0'));
    }

    void sendNMEA(const QByteArray &record)
    {
        QByteArray output(record);
        addNMEAChecksum(output);
        output.prepend('$');
        output.append("\r\n");
        outgoing.append(output);
    }

    void nmeaMWV()
    {
        QByteArray result("WIMWV,");
        result.append(QByteArray::number(Dm, 'f', 1));
        result.append(",R,");
        result.append(QByteArray::number(Sm, 'f', 1));
        result.append(",M,A");
        sendNMEA(result);
    }

    void nmeaXDR(bool composite = false)
    {
        if (!composite) {
            {
                QByteArray record("WIXDR");
                record.append(windNMEA(enableWinds));
                sendNMEA(record);
            }
            {
                QByteArray record("WIXDR");
                record.append(tpuNMEA(enableTPU));
                sendNMEA(record);
            }
            {
                QByteArray record("WIXDR");
                record.append(precipitationNMEA(enablePrecipitation));
                sendNMEA(record);
            }
            if (is3xSeries) {
                QByteArray record("WIXDR");
                record.append(auxiliaryNMEA(enableAuxiliary));
                sendNMEA(record);
            }
            {
                QByteArray record("WIXDR");
                record.append(monitorNMEA(enableMonitor));
                sendNMEA(record);
            }
        } else {
            QByteArray result("WIXDR");
            result.append(windNMEA(enableWinds >> 8));
            result.append(tpuNMEA(enableTPU >> 8));
            result.append(precipitationNMEA(enablePrecipitation >> 8));
            if (is3xSeries)
                result.append(auxiliaryNMEA(enableAuxiliary >> 8));
            result.append(monitorNMEA(enableMonitor >> 8));
            sendNMEA(result);
        }
    }

    bool isNMEA() const
    {
        switch (outputMode) {
        case Polled_ASCII:
        case Polled_ASCII_CRC:
        case Unpolled_ASCII:
        case Unpolled_ASCII_CRC:
            return false;
        case Unpolled_NMEA:
        case Polled_NMEA:
            return true;
        case SDI12:
        case SDI12_Cont:
            return false;
        }
        return false;
    }

    void advance(double seconds)
    {
        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR).trimmed());

            if (line == "?" || line == addressCharacter()) {
                outgoing.append(addressCharacter());
                outgoing.append("\r\n");
            } else if (line == addressCharacter() + "XZ") {
                if (isNMEA()) {
                    sendNMEA("WITXT,01,01,07,Start-up");
                } else {
                    outgoing.append(addressCharacter());
                    outgoing.append("TX,Start-up\r\n");
                }
            } else if (!isNMEA() && line == addressCharacter() + "R1") {
                sendASCIIRecord("R1" + windASCII(enableWinds));
            } else if (!isNMEA() && line == addressCharacter() + "R2") {
                sendASCIIRecord("R2" + tpuASCII(enableTPU));
            } else if (!isNMEA() && line == addressCharacter() + "R3") {
                sendASCIIRecord("R3" + precipitationASCII(enablePrecipitation));
            } else if (is3xSeries && !isNMEA() && line == addressCharacter() + "R4") {
                sendASCIIRecord("R4" + auxiliaryASCII(enableAuxiliary));
            } else if (!isNMEA() && line == addressCharacter() + "R5") {
                sendASCIIRecord("R5" + monitorASCII(enableMonitor));
            } else if (!isNMEA() && line == addressCharacter() + "R0") {
                sendASCIIRecord("R1" + windASCII(enableWinds));
                sendASCIIRecord("R2" + tpuASCII(enableTPU));
                sendASCIIRecord("R3" + precipitationASCII(enablePrecipitation));
                if (is3xSeries)
                    sendASCIIRecord("R4" + precipitationASCII(enableAuxiliary));
                sendASCIIRecord("R5" + monitorASCII(enableMonitor));
            } else if (line == addressCharacter() + "XU") {
                outgoing.append(addressCharacter());
                outgoing.append("XU,A=");
                outgoing.append(addressCharacter());
                outgoing.append(",M=");
                switch (outputMode) {
                case Polled_ASCII:
                    outgoing.append('P');
                    break;
                case Polled_ASCII_CRC:
                    outgoing.append('p');
                    break;
                case Unpolled_ASCII:
                    outgoing.append('A');
                    break;
                case Unpolled_ASCII_CRC:
                    outgoing.append('a');
                    break;
                case Unpolled_NMEA:
                    outgoing.append('N');
                    break;
                case Polled_NMEA:
                    outgoing.append('Q');
                    break;
                case SDI12:
                    outgoing.append('S');
                    break;
                case SDI12_Cont:
                    outgoing.append('R');
                    break;
                }
                outgoing.append(",T=T,C=2,I=");
                outgoing.append(QByteArray::number(unpolledInterval));
                if (is3xSeries)
                    outgoing.append(",B=19200,D=8,S=1,L=10,N=WXT530,V=1.00\r\n");
                else
                    outgoing.append(",B=19200,D=8,S=1,L=10,N=WXT520,V=1.00\r\n");
            } else if (line.startsWith(addressCharacter() + "XU,")) {
                outgoing.append(line + "\r\n");
                QList<QByteArray> fields(line.split(','));
                if (!fields.isEmpty())
                    fields.removeFirst();
                for (const auto &f : fields) {
                    int idx = f.indexOf('=');
                    if (idx <= 0)
                        continue;
                    QByteArray key(f.mid(0, idx));
                    QByteArray value(f.mid(idx + 1));
                    if (value.isEmpty())
                        continue;
                    if (key == "M") {
                        switch (value.at(0)) {
                        case 'A':
                            outputMode = Unpolled_ASCII;
                            break;
                        case 'a':
                            outputMode = Unpolled_ASCII_CRC;
                            break;
                        case 'P':
                            outputMode = Polled_ASCII;
                            break;
                        case 'p':
                            outputMode = Polled_ASCII_CRC;
                            break;
                        case 'N':
                            outputMode = Unpolled_NMEA;
                            break;
                        case 'Q':
                            outputMode = Polled_NMEA;
                            break;
                        case 'S':
                            outputMode = SDI12;
                            break;
                        case 'R':
                            outputMode = SDI12_Cont;
                            break;
                        default:
                            break;
                        }
                    }
                }
            } else if (line == addressCharacter() + "WU") {
                outgoing.append(addressCharacter());
                outgoing.append("WU,R=");
                outgoing.append(QByteArray::number(enableWinds & 0xFF, 2).rightJustified(8, '0'));
                outgoing.append('&');
                outgoing.append(
                        QByteArray::number((enableWinds >> 8) & 0xFF, 2).rightJustified(8, '0'));
                outgoing.append(",I=1,A=1,G=3,U=M,D=0,N=T,F=4\r\n");
            } else if (line.startsWith(addressCharacter() + "XU,")) {
                outgoing.append(line + "\r\n");
                QList<QByteArray> fields(line.split(','));
                if (!fields.isEmpty())
                    fields.removeFirst();
                for (const auto &f : fields) {
                    int idx = f.indexOf('=');
                    if (idx <= 0)
                        continue;
                    QByteArray key(f.mid(0, idx));
                    QByteArray value(f.mid(idx + 1));
                    if (value.isEmpty())
                        continue;
                    if (key == "R") {
                        idx = value.indexOf('&');
                        if (idx > 0) {
                            enableWinds = static_cast<std::uint16_t>(value.mid(0, idx).toInt(0, 2) |
                                    (value.mid(idx + 1).toInt(0, 2) << 8));
                        }
                    }
                }
            } else if (line == addressCharacter() + "TU") {
                outgoing.append(addressCharacter());
                outgoing.append("TU,R=");
                outgoing.append(QByteArray::number(enableTPU & 0xFF, 2).rightJustified(8, '0'));
                outgoing.append('&');
                outgoing.append(QByteArray::number((enableTPU >> 8) & 0xFF, 2).rightJustified(8, '0'));
                outgoing.append(",I=1,P=H,T=C\r\n");
            } else if (line.startsWith(addressCharacter() + "TU,")) {
                outgoing.append(line + "\r\n");
                QList<QByteArray> fields(line.split(','));
                if (!fields.isEmpty())
                    fields.removeFirst();
                for (const auto &f : fields) {
                    int idx = f.indexOf('=');
                    if (idx <= 0)
                        continue;
                    QByteArray key(f.mid(0, idx));
                    QByteArray value(f.mid(idx + 1));
                    if (value.isEmpty())
                        continue;
                    if (key == "R") {
                        idx = value.indexOf('&');
                        if (idx > 0) {
                            enableTPU = static_cast<std::uint16_t>(value.mid(0, idx).toInt(0, 2) |
                                    (value.mid(idx + 1).toInt(0, 2) << 8));
                        }
                    }
                }
            } else if (line == addressCharacter() + "RU") {
                outgoing.append(addressCharacter());
                outgoing.append("RU,R=");
                outgoing.append(
                        QByteArray::number(enablePrecipitation & 0xFF, 2).rightJustified(8, '0'));
                outgoing.append('&');
                outgoing.append(
                        QByteArray::number((enablePrecipitation >> 8) & 0xFF, 2).rightJustified(8,
                                                                                                '0'));
                outgoing.append(",I=1,P=H,T=C\r\n");
            } else if (line.startsWith(addressCharacter() + "RU,")) {
                outgoing.append(line + "\r\n");
                QList<QByteArray> fields(line.split(','));
                if (!fields.isEmpty())
                    fields.removeFirst();
                for (const auto &f : fields) {
                    int idx = f.indexOf('=');
                    if (idx <= 0)
                        continue;
                    QByteArray key(f.mid(0, idx));
                    QByteArray value(f.mid(idx + 1));
                    if (value.isEmpty())
                        continue;
                    if (key == "R") {
                        idx = value.indexOf('&');
                        if (idx > 0) {
                            enablePrecipitation =
                                    static_cast<std::uint16_t>(value.mid(0, idx).toInt(0, 2) |
                                            (value.mid(idx + 1).toInt(0, 2) << 8));
                        }
                    }
                }
            } else if (line == addressCharacter() + "SU") {
                outgoing.append(addressCharacter());
                outgoing.append("SU,R=");
                outgoing.append(QByteArray::number(enableMonitor & 0xFF, 2).rightJustified(8, '0'));
                outgoing.append('&');
                outgoing.append(
                        QByteArray::number((enableMonitor >> 8) & 0xFF, 2).rightJustified(8, '0'));
                outgoing.append(",I=1,P=H,T=C\r\n");
            } else if (line.startsWith(addressCharacter() + "SU,")) {
                outgoing.append(line + "\r\n");
                QList<QByteArray> fields(line.split(','));
                if (!fields.isEmpty())
                    fields.removeFirst();
                for (const auto &f : fields) {
                    int idx = f.indexOf('=');
                    if (idx <= 0)
                        continue;
                    QByteArray key(f.mid(0, idx));
                    QByteArray value(f.mid(idx + 1));
                    if (value.isEmpty())
                        continue;
                    if (key == "R") {
                        idx = value.indexOf('&');
                        if (idx > 0) {
                            enableMonitor = static_cast<std::uint16_t>(value.mid(0, idx).toInt(0, 2) |
                                    (value.mid(idx + 1).toInt(0, 2) << 8));
                        }
                    }
                }
            } else if (is3xSeries && line == addressCharacter() + "IU") {
                outgoing.append(addressCharacter());
                outgoing.append("IU,R=");
                outgoing.append(
                        QByteArray::number(enableAuxiliary & 0xFF, 2).rightJustified(8, '0'));
                outgoing.append('&');
                outgoing.append(
                        QByteArray::number((enableAuxiliary >> 8) & 0xFF, 2).rightJustified(8,
                                                                                            '0'));
                outgoing.append("\r\n");
            } else if (is3xSeries && line.startsWith(addressCharacter() + "IU,")) {
                outgoing.append(line + "\r\n");
                QList<QByteArray> fields(line.split(','));
                if (!fields.isEmpty())
                    fields.removeFirst();
                for (const auto &f : fields) {
                    int idx = f.indexOf('=');
                    if (idx <= 0)
                        continue;
                    QByteArray key(f.mid(0, idx));
                    QByteArray value(f.mid(idx + 1));
                    if (value.isEmpty())
                        continue;
                    if (key == "R") {
                        idx = value.indexOf('&');
                        if (idx > 0) {
                            enableAuxiliary =
                                    static_cast<std::uint16_t>(value.mid(0, idx).toInt(0, 2) |
                                            (value.mid(idx + 1).toInt(0, 2) << 8));
                        }
                    }
                }
            }

            if (idxCR >= incoming.size() - 1) {
                incoming.clear();
                break;
            }
            incoming = incoming.mid(idxCR + 1);
        }

        if (FP::defined(unpolledRemaining) && unpolledInterval > 0) {
            unpolledRemaining -= seconds;
            while (unpolledRemaining <= 0.0) {
                unpolledRemaining += unpolledInterval;

                switch (outputMode) {
                case Polled_ASCII:
                case Polled_ASCII_CRC:
                    continue;
                case Unpolled_ASCII:
                case Unpolled_ASCII_CRC: {
                    QByteArray result;
                    if (outputMode == Unpolled_ASCII)
                        result.append('R');
                    else
                        result.append('r');
                    result.append('0');
                    result.append(windASCII(enableWinds >> 8));
                    result.append(tpuASCII(enableTPU >> 8));
                    result.append(precipitationASCII(enablePrecipitation >> 8));
                    if (is3xSeries)
                        result.append(auxiliaryASCII(enableAuxiliary >> 8));
                    result.append(monitorASCII(enableMonitor >> 8));
                    sendASCIIBasic(result);
                    break;
                }
                case Unpolled_NMEA:
                    nmeaXDR();
                    break;
                case Polled_NMEA:
                    continue;
                case SDI12:
                case SDI12_Cont:
                    continue;
                }
            }
        }
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream,
                   const ModelInstrument &model,
                   double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("WS", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("WD", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("U1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("P", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("WI", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZWSGust", Variant::Root(), QString(), time))
            return false;
        if (model.is3xSeries) {
            if (!stream.hasMeta("T4", Variant::Root(), QString(), time))
                return false;
            if (!stream.hasMeta("Ld", Variant::Root(), QString(), time))
                return false;
            if (!stream.hasMeta("VA", Variant::Root(), QString(), time))
                return false;
        }
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("V1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V3", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("WS"))
            return false;
        if (!stream.checkContiguous("WD"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("T3"))
            return false;
        if (!stream.checkContiguous("U1"))
            return false;
        if (!stream.checkContiguous("P"))
            return false;
        if (!stream.checkContiguous("WI"))
            return false;
        if (!stream.checkContiguous("ZWSGust"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream, const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("WS", Variant::Root(model.Sm), time))
            return false;
        if (!stream.hasAnyMatchingValue("WD", Variant::Root(model.Dm), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.Ta), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.Tp), time))
            return false;
        if (!stream.hasAnyMatchingValue("T3", Variant::Root(model.Th), time))
            return false;
        if (!stream.hasAnyMatchingValue("U1", Variant::Root(model.Ua), time))
            return false;
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.Pa), time))
            return false;
        if (!stream.hasAnyMatchingValue("WI", Variant::Root(model.Ri), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZWSGust", Variant::Root(model.Sx), time))
            return false;
        if (model.is3xSeries) {
            if (!stream.hasAnyMatchingValue("T4", Variant::Root(model.Tr), time))
                return false;
            if (!stream.hasAnyMatchingValue("Ld", Variant::Root(model.Sl), time))
                return false;
            if (!stream.hasAnyMatchingValue("VA", Variant::Root(model.Sr), time))
                return false;
        }
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                             double time = FP::undefined())
    {
        Q_UNUSED(model);
        if (!stream.hasAnyMatchingValue("V1", Variant::Root(model.Vs), time))
            return false;
        if (!stream.hasAnyMatchingValue("V2", Variant::Root(model.Vr), time))
            return false;
        if (!stream.hasAnyMatchingValue("V3", Variant::Root(model.Vh), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_vaisala_wxt5xx"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_vaisala_wxt5xx"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobeASCII520()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.outputMode = ModelInstrument::Unpolled_ASCII;
        instrument.is3xSeries = false;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void passiveAutoprobeASCII530()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.outputMode = ModelInstrument::Unpolled_ASCII;
        instrument.is3xSeries = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void passiveAutoprobeASCIICRC()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.outputMode = ModelInstrument::Unpolled_ASCII_CRC;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void interactiveAutoprobe520()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.is3xSeries = false;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.2);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void interactiveAutoprobe530()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.is3xSeries = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.2);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 200; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void passiveAcquisitionNMEA520()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.outputMode = ModelInstrument::Unpolled_NMEA;
        instrument.is3xSeries = false;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 200; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void passiveAcquisitionNMEA530()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.outputMode = ModelInstrument::Unpolled_NMEA;
        instrument.is3xSeries = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 200; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.is3xSeries = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 200; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
