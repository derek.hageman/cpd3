/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "datacore/segment.hxx"
#include "fileinput/ebas.hxx"

#include "import_ebas.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Input;


ComponentOptions ImportEBASComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Import profile"),
                                                tr("This is the base profile configuration used to convert data.  "
                                                   "The profile determines the templates the data are matched "
                                                   "against to produce the final output.  "
                                                   "Consult the station configuration for available profiles."),
                                                tr("aerosol")));

    options.add("archive", new ComponentOptionSingleString(tr("archive", "name"),
                                                           tr("The archive that to import the data as"),
                                                           tr("This is the archive that the data is imported as.  For example, "
                                                              "\"raw\" or \"clean\".  This can be overridden by specific "
                                                              "import profiles."),
                                                           tr("raw", "default archive")));

    options.add("station", new ComponentOptionSingleString(tr("station", "name"),
                                                           tr("The station that to import the data as"),
                                                           tr("This is the station that the data is imported as.  For example, "
                                                              "\"bnd\".  This can be overridden by specific import profiles.  "
                                                              "Normally this is determined by the contents of the file."),
                                                           tr("Automatic", "default station")));

    options.add("suffix", new ComponentOptionSingleString(tr("instrument", "name"),
                                                          tr("The the instrument suffix to use"),
                                                          tr("This is the instrument suffix to use when generating variable "
                                                             "names.  For example, \"S11\".  This can be override by specific "
                                                             "profiles."),
                                                          tr("Automatic", "default suffix")));

    return options;
}

QList<ComponentExample> ImportEBASComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This imports EBAS data using the normal profile and fully "
                                        "automatic determination.")));

    return examples;
}

ExternalConverter *ImportEBASComponent::createDataIngress(const ComponentOptions &options)
{
    SequenceName::Component station;
    if (options.isSet("station")) {
        station = qobject_cast<ComponentOptionSingleString *>(options.get("station"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }

    SequenceName configUnit(station, "configuration", "import_ebas");
    auto config = SequenceSegment::Stream::read(Archive::Selection(FP::undefined(), FP::undefined(),
                                                                   !configUnit.getStation().empty()
                                                                   ? Archive::Selection::Match{
                                                                           configUnit.getStation()}
                                                                   : Archive::Selection::Match(),
                                                                   {configUnit.getArchive()},
                                                                   {configUnit.getVariable()}));


    QString profile(QString::fromStdString(SequenceName::impliedProfile()));
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower();
    }

    EBASEngine *engine = new EBASEngine(config, configUnit, QString("Profiles/%1").arg(profile));
    Q_ASSERT(engine);
    if (!station.empty()) {
        engine->setStation(QString::fromStdString(station));
    }
    if (options.isSet("archive")) {
        engine->setArchive(
                qobject_cast<ComponentOptionSingleString *>(options.get("archive"))->get()
                                                                                   .toLower());
    }
    if (options.isSet("suffix")) {
        engine->setSuffix(
                qobject_cast<ComponentOptionSingleString *>(options.get("suffix"))->get());
    }
    return engine;
}
