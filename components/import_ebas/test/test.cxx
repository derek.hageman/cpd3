/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QBuffer>
#include <QTemporaryFile>

#include "datacore/externalsource.hxx"
#include "datacore/archive/access.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    static bool valuesEqual(const Variant::Read &a, const Variant::Read &b)
    {
        if (a.getType() != Variant::Type::Real || b.getType() != Variant::Type::Real)
            return a == b;
        double va = a.toDouble();
        double vb = b.toDouble();
        if (!FP::defined(va))
            return !FP::defined(vb);
        if (!FP::defined(vb))
            return false;
        double eps = qMax(va, vb) * 1E-6;
        if (eps == 0)
            return va == vb;
        return fabs(va - vb) < eps;
    }

    static bool valueListMatch(SequenceValue::Transfer values, SequenceValue::Transfer expected)
    {
        for (auto f = expected.begin(); f != expected.end();) {
            bool hit = false;
            for (auto c = values.begin(), endC = values.end(); c != endC; ++c) {
                if (!FP::equal(c->getStart(), f->getStart()))
                    continue;
                if (!FP::equal(c->getEnd(), f->getEnd()))
                    continue;
                if (c->getUnit() != f->getUnit())
                    continue;

                Variant::Root merged;
                if (c->getValue().getType() != Variant::Type::Array ||
                        f->getValue().getType() != Variant::Type::Array) {
                    merged = Variant::Root::overlay(c->root(), f->root());
                } else {
                    for (std::size_t i = 0,
                            max = std::max(c->read().toArray().size(), f->read().toArray().size());
                            i < max;
                            i++) {
                        merged.write()
                              .array(i)
                              .set(Variant::Root::overlay(Variant::Root(c->getValue().array(i)),
                                                          Variant::Root(f->getValue().array(i))));
                    }
                }
                if (!valuesEqual(merged, c->getValue())) {
                    qDebug() << "Value overlay mismatch for " << *f << ".  Merged: " << merged
                             << " Input: " << c->getValue();
                    return false;
                }

                values.erase(c);
                f = expected.erase(f);
                hit = true;
                break;
            }
            if (!hit)
                ++f;
        }
        if (!values.empty()) {
            qDebug() << "Unmatched values in result: " << values;
        }
        if (!expected.empty()) {
            qDebug() << "Unmatched values in expected: " << expected;
        }
        return values.empty() && expected.empty();
    }

    static Variant::Root mo(const Variant::Read &under,
                            const QString &path,
                            const Variant::Read &over)
    {
        Variant::Root ret(under);
        ret[path].set(over);
        return ret;
    }

    static Variant::Root stats(double mean, double q15, double q84)
    {
        Variant::Root ret;
        ret["Mean"] = mean;
        ret["Quantiles"].keyframe(15.87 / 100.0) = q15;
        ret["Quantiles"].keyframe(84.13 / 100.0) = q84;
        return ret;
    }

    ExternalConverterComponent *component;
    QTemporaryFile databaseFile;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component =
                qobject_cast<ExternalConverterComponent *>(ComponentLoader::create("import_ebas"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("station")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("archive")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("suffix")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("profile")));
    }

    void importBasic()
    {
        Variant::Root config;

        config["Profiles/aerosol/Groups/Bs/Match/Type"] = "And";
        config["Profiles/aerosol/Groups/Bs/Match/Components/#0/Type"] = "Text";
        config["Profiles/aerosol/Groups/Bs/Match/Components/#0/Left/Key"] = "Component";
        config["Profiles/aerosol/Groups/Bs/Match/Components/#0/Right/Constant"] =
                "aerosol_light_scattering_coefficient";
        config["Profiles/aerosol/Groups/Bs/Match/Components/#1/Type"] = "Defined";
        config["Profiles/aerosol/Groups/Bs/Match/Components/#1/Value/Key"] = "Quantile";
        config["Profiles/aerosol/Groups/Bs/Match/Components/#1/Invert"] = true;
        config["Profiles/aerosol/Groups/Bs/Data/Type"] = "Statistics";
        config["Profiles/aerosol/Groups/Bs/Data/Variable/Convert"] =
                "Bs${WAVELENGTHCODE}_${SUFFIX}";
        config["Profiles/aerosol/Groups/Bs/Data/Flavors/Key"] = "Matrix";
        config["Profiles/aerosol/Groups/Bs/Data/Flavors/Convert/Type"] = "MatrixFlavors";
        config["Profiles/aerosol/Groups/Bs/Data/Metadata/Base/*dDescription"] =
                "Aerosol light scattering coefficient";
        config["Profiles/aerosol/Groups/Bs/Data/Metadata/Overlay/Wavelength/Path"] = "^Wavelength";
        config["Profiles/aerosol/Groups/Bs/Data/Metadata/Overlay/Wavelength/Key"] = "Wavelength";
        config["Profiles/aerosol/Groups/Bs/Data/Metadata/Overlay/ReportT/Path"] = "^ReportT";
        config["Profiles/aerosol/Groups/Bs/Data/Metadata/Overlay/ReportT/Key"] = "ReportT";
        config["Profiles/aerosol/Groups/Bs/Data/Metadata/Overlay/Units/Path"] = "^Units";
        config["Profiles/aerosol/Groups/Bs/Data/Metadata/Overlay/Units/Key"] = "Units";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Type"] = "And";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Components/#0/Type"] = "Text";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Components/#0/Left/Key"] =
                "Component";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Components/#0/Right/Constant"] =
                "aerosol_light_scattering_coefficient";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Components/#1/Type"] =
                "Defined";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Components/#1/Value/Key"] =
                "Quantile";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Components/#2/Type"] = "Exact";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Components/#2/Left/Key/Reference/Key"] =
                "Wavelength";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Components/#2/Right/Key"] =
                "Wavelength";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Name/Key"] = "Quantile";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Name/Convert/Type"] = "Format";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Name/Convert/Format"] = "000.00000";

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"_", "configuration", "import_ebas"}, config)});

        ComponentOptions options(component->getOptions());
        ExternalConverter *inp = component->createDataIngress(options);
        QVERIFY(inp != NULL);

        inp->start();
        StreamSink::Buffer output;
        inp->setEgress(&output);

        inp->incomingData(QByteArray("92 1001\n"
                                             "Ogren, John\n"
                                             "US06L, National Oceanic and Atmospheric Administration/Earth System Research Laboratory/Global Monitoring Division, NOAA/ESRL/GMD,, 325 Broadway,, 80305, \"Boulder, CO\", USA\n"
                                             "Ogren, John\n"
                                             "GAW-WDCA NOAA-ESRL\n"
                                             "1 1\n"
                                             "2012 01 01 2015 06 16\n"
                                             "0.041667\n"
                                             "Days from the file reference point (start_time)\n"
                                             "26\n"
                                             "1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
                                             "9999.999999 9999 9999 99999.99 9999.99 9999.99 999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9.999999999999999999999999\n"
                                             "end_time of measurement, days from the file reference point\n"
                                             "start_time of measurement, year\n"
                                             "end_time of measurement, year\n"
                                             "pressure, hPa, Location=instrument internal, Matrix=instrument\n"
                                             "temperature, K, Location=instrument internal, Matrix=instrument\n"
                                             "relative_humidity, %, Location=instrument internal, Matrix=instrument\n"
                                             "number of wavelengths\n"
                                             "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=450nm\n"
                                             "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=550nm\n"
                                             "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=700nm\n"
                                             "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=450nm\n"
                                             "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=550nm\n"
                                             "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=700nm\n"
                                             "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=450nm, Statistics=percentile:15.87\n"
                                             "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=550nm, Statistics=percentile:15.87\n"
                                             "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=700nm, Statistics=percentile:15.87\n"
                                             "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=450nm, Statistics=percentile:15.87\n"
                                             "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=550nm, Statistics=percentile:15.87\n"
                                             "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=700nm, Statistics=percentile:15.87\n"
                                             "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=450nm, Statistics=percentile:84.13\n"
                                             "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=550nm, Statistics=percentile:84.13\n"
                                             "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=700nm, Statistics=percentile:84.13\n"
                                             "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=450nm, Statistics=percentile:84.13\n"
                                             "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=550nm, Statistics=percentile:84.13\n"
                                             "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=700nm, Statistics=percentile:84.13\n"
                                             "numflag\n"
                                             "0\n"
                                             "52\n"
                                             "Data definition:              EBAS_1.1\n"
                                             "Set type code:                TU\n"
                                             "Station code:                 US6005G\n"
                                             "Platform code:                US6005S\n"
                                             "Timezone:                     UTC\n"
                                             "Startdate:                    20120101000000\n"
                                             "Timeref:                      00_00\n"
                                             "Revision date:                20150616205734\n"
                                             "Component:                    aerosol_light_scattering_coefficient\n"
                                             "Unit:                         1/Mm\n"
                                             "Matrix:                       pm1\n"
                                             "Period code:                  6h\n"
                                             "Resolution code:              1h\n"
                                             "Sample duration:              1h\n"
                                             "Laboratory code:              US06L\n"
                                             "Instrument type:              nephelometer\n"
                                             "Instrument manufacturer:      TSI\n"
                                             "Instrument model:             3563\n"
                                             "Instrument name:              TSI_3563_THD\n"
                                             "Instrument serial number:     1070\n"
                                             "Method ref:                   US06L_scat_coef\n"
                                             "Add. qualifier:               1mn\n"
                                             "File name:                    US6005G.20120101000000.20150616205734.aerosol_light_scattering_coefficient.pm1.1d.1h.lev2.nas\n"
                                             "Station WDCA-ID:              GAWAUSCATHD\n"
                                             "Station WDCA-Name:            Trinidad Head, California\n"
                                             "Station GAW-ID:               THD\n"
                                             "Station state/province:       California\n"
                                             "Station latitude:             41.05000\n"
                                             "Station longitude:            -124.15000\n"
                                             "Station altitude:             213m\n"
                                             "Station land use:             Residential\n"
                                             "Station setting:              Coastal\n"
                                             "Station GAW type:             G\n"
                                             "Station WMO region:           4\n"
                                             "Originator:                   Ogren, John, John.A.Ogren@noaa.gov, National Oceanic and Atmospheric Administration/Earth System Research Laboratory/Global Monitoring Division, NOAA/ESRL/GMD,, 325 Broadway,, 80305, \"Boulder, CO\", USA\n"
                                             "Submitter:                    Ogren, John, John.A.Ogren@noaa.gov, National Oceanic and Atmospheric Administration/Earth System Research Laboratory/Global Monitoring Division, NOAA/ESRL/GMD,, 325 Broadway,, 80305, \"Boulder, CO\", USA\n"
                                             "Data level:                   2\n"
                                             "Version:                      1\n"
                                             "Version description:          Version numbering not tracked, generated by data.aggregate.ebas r4678\n"
                                             "Height AGL:                   10m\n"
                                             "Inlet type:                   Impactor--direct\n"
                                             "Inlet description:            Switched impactor at 1 um\n"
                                             "Humidity/temperature control: Heating to 40% RH, limit 40 deg. C\n"
                                             "Volume std. temperature:      273.15K\n"
                                             "Volume std. pressure:         1013.25hPa\n"
                                             "Detection limit:              0.4 1/Mm\n"
                                             "Detection limit expl.:        Determined by instrument noise on filtered air (2 standard deviations), no detection limit flag used\n"
                                             "Zero/negative values code:    zero/negative possible\n"
                                             "Zero/negative values:         Zero and neg. values may appear due to statistical variations at very low concentrations\n"
                                             "Standard method:              cal-gas=CO2+AIR_truncation-correction=Anderson1998\n"
                                             "Acknowledgement:              Request acknowledgement details from data originator\n"
                                             " start_time    end_time st_y ed_y    P_int   T_int  RH_int nWav         sc450         sc550         sc700        bsc450        bsc550        bsc700     lStdSc450     lStdSc550     lStdSc700    lStdBsc450    lStdBsc550    lStdBsc700     uStdSc450     uStdSc550     uStdSc700    uStdBsc450    uStdBsc550    uStdBsc700 numflag\n"
                                             "   0.000000    0.041667 2012 2012   992.68  301.38   20.67    3   24.40764706   17.94294118   11.56411765    2.64882353    1.88705882    1.64235294   19.42582400   13.88651200    9.14937600    2.04715200    1.52323200    1.13392000   29.07600000   21.56990400   13.71804800    3.36000000    2.39147200    2.11764800 0.500000000000000000000000\n"
                                             "   0.041667    0.083333 2012 2012   993.53  301.12   21.03    3   12.97352941    9.46058824    5.85470588    1.56176471    1.06529412    0.74176471    8.20488000    6.24438400    3.78547200    1.00696000    0.80539200    0.59000000   16.21441600   11.74912000    7.37598400    2.04372800    1.41608000    0.93921600 0.000000000000000000000000\n"
                                             "   0.083333    0.125000 2012 2012   993.83  300.86   17.30    3   16.20692308   11.60461538    7.63769231    1.82384615    1.44692308    1.16000000   12.60881600    9.32374800    6.42323200    1.34948400    1.13183600    0.95610000   20.15934800   13.57692000    9.03588800    2.26956000    1.85286800    1.40191200 0.000000000000000000000000\n"
                                             "   0.125000    0.166667 2012 2012   993.58  300.86   20.03    3   16.83176471   11.90764706    7.50764706    1.87411765    1.45823529    1.20058824   12.47235200    8.82156800    5.70166400    1.42539200    1.07470400    0.90940800   22.24896000   15.52276800    9.39648000    2.32686400    1.77137600    1.45833600 0.000000000000000000000000\n"
                                             "   0.166667    0.208333 2012 2012   992.85  300.92   20.31    3   14.38588235   10.83941176    6.49647059    1.48470588    1.40823529    0.94470588   11.42624000    8.37988800    5.01822400    1.10539200    1.01539200    0.66000000   17.72198400   13.11284800    7.75667200    1.80137600    1.88676800    1.23225600 0.000000000000000000000000\n"
                                             "   0.208333    0.250000 2012 2012   992.24 9999.99   19.46    3   11.43176471    8.11823529    5.16705882    1.23235294    1.06823529    0.79176471    4.94539200    2.83852800    2.09470400    0.38156800    0.33078400    0.34696000   20.09030400   15.10755200    9.31059200    2.15843200    1.64225600    1.37912000 0.000000000000000000000000\n"));
        inp->endData();

        QVERIFY(inp->wait(30000));
        delete inp;

        SequenceName BsB("thd", "raw", "BsB_X1", {SequenceName::flavor_pm1});
        SequenceName BsG("thd", "raw", "BsG_X1", {SequenceName::flavor_pm1});
        SequenceName BsR("thd", "raw", "BsR_X1", {SequenceName::flavor_pm1});

        Variant::Root Bs_meta;
        Bs_meta.write()
               .metadataReal("Description")
               .setString("Aerosol light scattering coefficient");
        Bs_meta.write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        Bs_meta.write().metadataReal("ReportT").setDouble(0);

        QVERIFY(output.ended());
        QVERIFY(valueListMatch(output.values(), (SequenceValue::Transfer{
                SequenceValue(BsB.toMeta(), mo(Bs_meta, "^Wavelength", Variant::Root(450.0)),
                              1325376000, 1325397600),
                SequenceValue(BsG.toMeta(), mo(Bs_meta, "^Wavelength", Variant::Root(550.0)),
                              1325376000, 1325397600),
                SequenceValue(BsR.toMeta(), mo(Bs_meta, "^Wavelength", Variant::Root(700.0)),
                              1325376000, 1325397600),
                SequenceValue(BsB, Variant::Root(24.40764706), 1325376000, 1325379600),
                SequenceValue(BsB, Variant::Root(12.97352941), 1325379600, 1325383200),
                SequenceValue(BsB, Variant::Root(16.20692308), 1325383200, 1325386800),
                SequenceValue(BsB, Variant::Root(16.83176471), 1325386800, 1325390400),
                SequenceValue(BsB, Variant::Root(14.38588235), 1325390400, 1325394000),
                SequenceValue(BsB, Variant::Root(11.43176471), 1325394000, 1325397600),
                SequenceValue(BsB.withFlavor("stats"), stats(24.40764706, 19.42582400, 29.07600000),
                              1325376000, 1325379600),
                SequenceValue(BsB.withFlavor("stats"), stats(12.97352941, 8.20488000, 16.21441600),
                              1325379600, 1325383200),
                SequenceValue(BsB.withFlavor("stats"), stats(16.20692308, 12.60881600, 20.15934800),
                              1325383200, 1325386800),
                SequenceValue(BsB.withFlavor("stats"), stats(16.83176471, 12.47235200, 22.24896000),
                              1325386800, 1325390400),
                SequenceValue(BsB.withFlavor("stats"), stats(14.38588235, 11.42624000, 17.72198400),
                              1325390400, 1325394000),
                SequenceValue(BsB.withFlavor("stats"), stats(11.43176471, 4.94539200, 20.09030400),
                              1325394000, 1325397600),
                SequenceValue(BsG, Variant::Root(17.94294118), 1325376000, 1325379600),
                SequenceValue(BsG, Variant::Root(9.46058824), 1325379600, 1325383200),
                SequenceValue(BsG, Variant::Root(11.60461538), 1325383200, 1325386800),
                SequenceValue(BsG, Variant::Root(11.90764706), 1325386800, 1325390400),
                SequenceValue(BsG, Variant::Root(10.83941176), 1325390400, 1325394000),
                SequenceValue(BsG, Variant::Root(8.11823529), 1325394000, 1325397600),
                SequenceValue(BsG.withFlavor("stats"), stats(17.94294118, 13.88651200, 21.56990400),
                              1325376000, 1325379600),
                SequenceValue(BsG.withFlavor("stats"), stats(9.46058824, 6.24438400, 11.74912000),
                              1325379600, 1325383200),
                SequenceValue(BsG.withFlavor("stats"), stats(11.60461538, 9.32374800, 13.57692000),
                              1325383200, 1325386800),
                SequenceValue(BsG.withFlavor("stats"), stats(11.90764706, 8.82156800, 15.52276800),
                              1325386800, 1325390400),
                SequenceValue(BsG.withFlavor("stats"), stats(10.83941176, 8.37988800, 13.11284800),
                              1325390400, 1325394000),
                SequenceValue(BsG.withFlavor("stats"), stats(8.11823529, 2.83852800, 15.10755200),
                              1325394000, 1325397600),
                SequenceValue(BsR, Variant::Root(11.56411765), 1325376000, 1325379600),
                SequenceValue(BsR, Variant::Root(5.85470588), 1325379600, 1325383200),
                SequenceValue(BsR, Variant::Root(7.63769231), 1325383200, 1325386800),
                SequenceValue(BsR, Variant::Root(7.50764706), 1325386800, 1325390400),
                SequenceValue(BsR, Variant::Root(6.49647059), 1325390400, 1325394000),
                SequenceValue(BsR, Variant::Root(5.16705882), 1325394000, 1325397600),
                SequenceValue(BsR.withFlavor("stats"), stats(11.56411765, 9.14937600, 13.71804800),
                              1325376000, 1325379600),
                SequenceValue(BsR.withFlavor("stats"), stats(5.85470588, 3.78547200, 7.37598400),
                              1325379600, 1325383200),
                SequenceValue(BsR.withFlavor("stats"), stats(7.63769231, 6.42323200, 9.03588800),
                              1325383200, 1325386800),
                SequenceValue(BsR.withFlavor("stats"), stats(7.50764706, 5.70166400, 9.39648000),
                              1325386800, 1325390400),
                SequenceValue(BsR.withFlavor("stats"), stats(6.49647059, 5.01822400, 7.75667200),
                              1325390400, 1325394000),
                SequenceValue(BsR.withFlavor("stats"), stats(5.16705882, 2.09470400, 9.31059200),
                              1325394000, 1325397600)})));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
