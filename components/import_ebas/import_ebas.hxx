/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef IMPORTEBAS_H
#define IMPORTEBAS_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QIODevice>

#include "core/component.hxx"
#include "datacore/externalsource.hxx"
#include "fileinput/ebas.hxx"

class ImportEBASComponent : public QObject, virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.import_ebas"
                              FILE
                              "import_ebas.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Data::ExternalConverter
            *createDataIngress(const CPD3::ComponentOptions &options = CPD3::ComponentOptions());
};


#endif
