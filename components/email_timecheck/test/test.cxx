/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/externalsource.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Data {
namespace Variant {
bool operator==(const Root &a, const Root &b)
{ return a.read() == b.read(); }
}
}
}


class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    ExternalSourceComponent *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ExternalSourceComponent *>(
                ComponentLoader::create("email_timecheck"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("profile")));
    }

    void latestFileMessage()
    {
        double timeStart = Time::time();
        QTest::qSleep(2000);
        QTemporaryFile
                tempFile(QDir(QDir::tempPath()).filePath(Time::toISO8601ND(timeStart) + "_XXXXXX"));
        QVERIFY(tempFile.open());
        auto suffix = tempFile.fileName().section('_', -1, -1);
        tempFile.close();

        Variant::Root v;
        v["TimeCheck/aerosol/Latest/Path"].setString(QDir(QDir::tempPath()).path());
        v["TimeCheck/aerosol/Latest/Filter"].setString("${CAP|[^_]+}_" + suffix);
        v["TimeCheck/aerosol/Threshold/Units"].setString("second");
        v["TimeCheck/aerosol/Threshold/Count"].setInt64(1);
        v["TimeCheck/aerosol/WarningMessage"].setString("Time not synchronized at ${REMOTE}");

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "email"}, v, FP::undefined(),
                              FP::undefined())});

        ComponentOptions options(component->getOptions());
        ExternalConverter
                *input = component->createExternalIngress(options, 1388534400, 1388793600);
        QVERIFY(input != NULL);
        input->start();

        StreamSink::Buffer ingress;
        input->setEgress(&ingress);

        QVERIFY(input->wait());
        delete input;

        QVERIFY(ingress.ended());
        std::vector<Variant::Root> outputValues;
        for (const auto &dv : ingress.values()) {
            outputValues.emplace_back(dv.root());
        }

        std::vector<Variant::Root> values;
        v.write().setEmpty();
        v["Type"].setString("Break");
        values.emplace_back(v);

        v.write().setEmpty();
        v["Type"].setString("Text");
        v["Text"].setString("Time not synchronized at " + Time::toISO8601(timeStart));
        values.emplace_back(v);

        QCOMPARE(outputValues, values);
    }

    void latestFileNoMessage()
    {
        double timeStart = Time::time();
        QTemporaryFile
                tempFile(QDir(QDir::tempPath()).filePath(Time::toISO8601ND(timeStart) + "_XXXXXX"));
        QVERIFY(tempFile.open());
        auto suffix = tempFile.fileName().section('_', -1, -1);
        tempFile.close();

        Variant::Root v;
        v["TimeCheck/aerosol/Latest/Path"].setString(QDir(QDir::tempPath()).path());
        v["TimeCheck/aerosol/Latest/Filter"].setString("${CAP|[^_]+}_" + suffix);
        v["TimeCheck/aerosol/Threshold/Units"].setString("second");
        v["TimeCheck/aerosol/Threshold/Count"].setInt64(1);
        v["TimeCheck/aerosol/WarningMessage"].setString("Time not synchronized at ${REMOTE}");

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "email"}, std::move(v), FP::undefined(),
                              FP::undefined())});

        ComponentOptions options(component->getOptions());
        ExternalConverter
                *input = component->createExternalIngress(options, 1388534400, 1388793600);
        QVERIFY(input != NULL);
        input->start();

        StreamSink::Buffer ingress;
        input->setEgress(&ingress);

        QVERIFY(input->wait());
        delete input;

        QVERIFY(ingress.ended());
        QVERIFY(ingress.values().empty());
    }

    void heartbeatFileMessage()
    {
        double timeStart = Time::time();
        QTest::qSleep(2000);
        QTemporaryFile tempFile;
        QVERIFY(tempFile.open());
        {
            QByteArray line("Stratum=1 TIME=");
            line.append(Time::toISO8601(timeStart).toLatin1().constData());
            line.append('\n');
            QVERIFY(tempFile.write(line) == (qint64) line.size());
        }
        tempFile.close();

        Variant::Root v;
        v["TimeCheck/aerosol/Heartbeat/File"].setString(tempFile.fileName());
        v["TimeCheck/aerosol/Heartbeat/Require/Match"].setString("Stratum=(\\d+)");
        v["TimeCheck/aerosol/Heartbeat/Require/Threshold"].setDouble(4);
        v["TimeCheck/aerosol/Heartbeat/Require/Below"].setBool(true);
        v["TimeCheck/aerosol/Heartbeat/Extract"].setString("TIME=${CAP|\\\\S+}");
        v["TimeCheck/aerosol/Threshold/Units"].setString("second");
        v["TimeCheck/aerosol/Threshold/Count"].setInt64(1);
        v["TimeCheck/aerosol/WarningMessage"].setString("Time not synchronized at ${REMOTE}");

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "email"}, v, FP::undefined(),
                              FP::undefined())});

        ComponentOptions options(component->getOptions());
        ExternalConverter
                *input = component->createExternalIngress(options, 1388534400, 1388793600);
        QVERIFY(input != NULL);
        input->start();

        StreamSink::Buffer ingress;
        input->setEgress(&ingress);

        QVERIFY(input->wait());
        delete input;

        QVERIFY(ingress.ended());
        std::vector<Variant::Root> outputValues;
        for (const auto &dv : ingress.values()) {
            outputValues.emplace_back(dv.root());
        }

        std::vector<Variant::Root> values;
        v.write().setEmpty();
        v["Type"].setString("Break");
        values.emplace_back(v);

        v.write().setEmpty();
        v["Type"].setString("Text");
        v["Text"].setString("Time not synchronized at " + Time::toISO8601(timeStart));
        values.emplace_back(v);

        QCOMPARE(outputValues, values);
    }

    void heartbeatFileNoMessage()
    {
        double timeStart = Time::time();
        QTemporaryFile tempFile;
        QVERIFY(tempFile.open());
        {
            QByteArray line("Stratum=1 TIME=");
            line.append(Time::toISO8601(timeStart).toLatin1().constData());
            line.append('\n');
            QVERIFY(tempFile.write(line) == (qint64) line.size());
        }
        tempFile.close();

        Variant::Root v;
        v["TimeCheck/aerosol/Heartbeat/File"].setString(tempFile.fileName());
        v["TimeCheck/aerosol/Heartbeat/Require/Match"].setString("Stratum=(\\d+)");
        v["TimeCheck/aerosol/Heartbeat/Require/Threshold"].setDouble(4);
        v["TimeCheck/aerosol/Heartbeat/Require/Below"].setBool(true);
        v["TimeCheck/aerosol/Heartbeat/Extract"].setString("TIME=${CAP|\\\\S+}");
        v["TimeCheck/aerosol/Threshold/Units"].setString("second");
        v["TimeCheck/aerosol/Threshold/Count"].setInt64(1);
        v["TimeCheck/aerosol/WarningMessage"].setString("Time not synchronized at ${REMOTE}");

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "email"}, std::move(v), FP::undefined(),
                              FP::undefined())});

        ComponentOptions options(component->getOptions());
        ExternalConverter
                *input = component->createExternalIngress(options, 1388534400, 1388793600);
        QVERIFY(input != NULL);
        input->start();

        StreamSink::Buffer ingress;
        input->setEgress(&ingress);

        QVERIFY(input->wait());
        delete input;

        QVERIFY(ingress.ended());
        QVERIFY(ingress.values().empty());
    }

    void lossMessage()
    {
        double timeStart = Time::time();
        QTemporaryFile
                tempFile(QDir(QDir::tempPath()).filePath(Time::toISO8601ND(timeStart) + "_XXXXXX"));
        QVERIFY(tempFile.open());
        auto suffix = tempFile.fileName().section('_', -1, -1);
        tempFile.close();

        QTest::qSleep(2000);

        Variant::Root v;
        v["TimeCheck/aerosol/Latest/Path"].setString(QDir(QDir::tempPath()).path());
        v["TimeCheck/aerosol/Latest/Filter"].setString("${CAP|[^_]+}_" + suffix);
        v["TimeCheck/aerosol/Threshold/Units"].setString("second");
        v["TimeCheck/aerosol/Threshold/Count"].setInt64(1);
        v["TimeCheck/aerosol/Loss/Units"].setString("second");
        v["TimeCheck/aerosol/Loss/Count"].setInt64(1);
        v["TimeCheck/aerosol/LossMessage"].setString("Time synchronize lost at ${REMOTE}");

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "email"}, v, FP::undefined(),
                              FP::undefined())});

        ComponentOptions options(component->getOptions());
        ExternalConverter
                *input = component->createExternalIngress(options, 1388534400, 1388793600);
        QVERIFY(input != NULL);
        input->start();

        StreamSink::Buffer ingress;
        input->setEgress(&ingress);

        QVERIFY(input->wait());
        delete input;

        QVERIFY(ingress.ended());
        std::vector<Variant::Root> outputValues;
        for (const auto &dv : ingress.values()) {
            outputValues.emplace_back(dv.root());
        }

        std::vector<Variant::Root> values;
        v.write().setEmpty();
        v["Type"].setString("Break");
        values.emplace_back(v);

        v.write().setEmpty();
        v["Type"].setString("Text");
        v["Text"].setString("Time synchronize lost at " + Time::toISO8601(timeStart));
        values.emplace_back(v);

        QCOMPARE(outputValues, values);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
