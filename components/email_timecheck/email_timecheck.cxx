/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/range.hxx"
#include "core/timeparse.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/valueoptionparse.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/variant/composite.hxx"
#include "datacore/segment.hxx"
#include "io/drivers/file.hxx"

#include "email_timecheck.hxx"


Q_LOGGING_CATEGORY(log_email_timecheck, "cpd3.email.timecheck", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Transfer;

EmailTimeCheck::EmailTimeCheck() : profile(SequenceName::impliedProfile()), stations()
{ }

EmailTimeCheck::EmailTimeCheck(const ComponentOptions &options,
                               const std::vector<SequenceName::Component> &setStations) : profile(
        SequenceName::impliedProfile()), stations(setStations)
{
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }
}

EmailTimeCheck::~EmailTimeCheck()
{
}

void EmailTimeCheck::incomingData(const CPD3::Util::ByteView &)
{ externalNotify(); }

void EmailTimeCheck::endData()
{
    externalNotify();
}

EmailTimeCheck::MatchingStack::MatchingStack(const CPD3::TextSubstitutionStack &base,
                                             QList<FileDownloader::TimeCapture> &caps)
        : TextSubstitutionStack(base), captures(caps)
{ }

EmailTimeCheck::MatchingStack::~MatchingStack()
{ }

QString EmailTimeCheck::MatchingStack::substitution(const QStringList &elements) const
{
    QString check(FileDownloader::getTimeCapture(elements, captures));
    if (!check.isEmpty())
        return check;
    QString name(elements.first().toLower());
    if (name == "instance") {
        return "\\d+";
    } else if (name == "uid") {
        return "[a-zA-Z0-9]{8}";
    }
    return QRegExp::escape(TextSubstitutionStack::substitution(elements));
}


namespace {
struct RequireData {
    QRegExp pattern;
    double threshold;
    bool below;
    int line;
    bool matched;

    RequireData() : pattern(), threshold(FP::undefined()), below(false), line(-1), matched(false)
    { }

    RequireData(const Variant::Read &value, TextSubstitutionStack *subs) : pattern(
            subs->apply(value["Match"].toQString()),
            value["CaseSensitive"].toBool() ? Qt::CaseSensitive : Qt::CaseInsensitive),
                                                                           threshold(
                                                                                   value["Threshold"]
                                                                                           .toDouble()),
                                                                           below(value["Below"].toBool()),
                                                                           line(-1),
                                                                           matched(false)
    {
        qint64 i = value["Line"].toInt64();
        if (INTEGER::defined(i) && i > 0)
            line = (int) i;
    }

    void execute(int line, const QString &str)
    {
        if (this->line > 0 && line != this->line)
            return;

        if (pattern.indexIn(str) < 0)
            return;
        if (FP::defined(threshold)) {
            QStringList caps(pattern.capturedTexts());
            if (caps.size() > 1) {
                bool ok = false;
                double v = caps.at(1).toDouble(&ok);
                if (!ok || !FP::defined(v))
                    return;
                if (below) {
                    if (v >= threshold)
                        return;
                } else {
                    if (v <= threshold)
                        return;
                }
            }
        }

        matched = true;
    }
};

struct ExtractData {
    QRegularExpression pattern;
    QList<FileDownloader::TimeCapture> captures;
    int line;

    ExtractData() : pattern(), captures(), line(-1)
    { }

    ExtractData(const Variant::Read &value, TextSubstitutionStack *subs) : pattern(
            subs->apply(value["Match"].toQString()),
            value["CaseSensitive"].toBool() ? QRegularExpression::NoPatternOption
                                            : QRegularExpression::CaseInsensitiveOption), line(-1)
    {
        qint64 i = value["Line"].toInt64();
        if (INTEGER::defined(i) && i > 0)
            line = (int) i;
    }

    bool execute(int line, const QString &str, FileDownloader::TimeExtractContext &context) const
    {
        if (this->line > 0 && line != this->line)
            return false;

        auto match = pattern.match(str);
        if (!match.hasMatch())
            return false;

        context.integrate(match, captures);
        return true;
    }
};

};

double EmailTimeCheck::extractContents(const QString &fileName, const Variant::Read &config)
{
    if (fileName.isEmpty() || !config.exists())
        return FP::undefined();

    auto file = IO::Access::file(fileName, IO::File::Mode::readOnly())->block();
    if (!file)
        return FP::undefined();
    double referenceTime = FP::undefined();
    if (config["ReferenceModified"].toBool()) {
        QDateTime fmod(QFileInfo(fileName).lastModified());
        if (fmod.isValid()) {
            referenceTime = Time::fromDateTime(fmod.toUTC());
        }
    }

    QList<RequireData> require;
    switch (config["Require"].getType()) {
    case Variant::Type::Hash:
        require.append(RequireData(config["Require"], &substitutions));
        break;
    case Variant::Type::String: {
        RequireData add;
        add.pattern =
                QRegExp(substitutions.apply(config["Require"].toQString()), Qt::CaseInsensitive);
        require.append(add);
        break;
    }
    default: {
        for (auto add : config["Require"].toArray()) {
            require.append(RequireData(add, &substitutions));
        }
        break;
    }
    }

    QList<ExtractData> extract;
    switch (config["Extract"].getType()) {
    case Variant::Type::Hash: {
        QList<FileDownloader::TimeCapture> captures;
        MatchingStack ms(substitutions, captures);
        extract.append(ExtractData(config["Extract"], &ms));
        extract.last().captures = captures;
        break;
    }
    case Variant::Type::String: {
        ExtractData add;
        MatchingStack ms(substitutions, add.captures);
        add.pattern = QRegularExpression(ms.apply(config["Extract"].toQString()), QRegularExpression::CaseInsensitiveOption);
        extract.append(add);
        break;
    }
    default: {
        for (auto add : config["Extract"].toArray()) {
            QList<FileDownloader::TimeCapture> captures;
            MatchingStack ms(substitutions, captures);
            extract.append(ExtractData(add, &ms));
            extract.last().captures = captures;
        }
        break;
    }
    }

    int lineIndex = 0;
    FileDownloader::TimeExtractContext globalContext;
    while (auto line = file->readLine()) {
        if (isTerminated())
            return FP::undefined();

        QString str(line.toQString());

        for (auto &r : require) {
            r.execute(lineIndex, str);
        }

        for (const auto &e : extract) {
            e.execute(lineIndex, str, globalContext);
        }
    }

    for (const auto &r : require) {
        if (!r.matched)
            return FP::undefined();
    }

    return globalContext.extract(referenceTime);
}

bool EmailTimeCheck::begin()
{
    SequenceSegment configSegment;

    do {
        Archive::Access access;
        Archive::Access::ReadLock lock(access);

        auto allStations = access.availableStations(stations);
        if (allStations.empty()) {
            qCDebug(log_email_timecheck) << "No stations available";
            return false;
        }
        stations.clear();
        std::copy(allStations.begin(), allStations.end(), Util::back_emplacer(stations));

        double tnow = Time::time();
        {
            auto confList = SequenceSegment::Stream::read(
                    Archive::Selection(tnow - 1.0, tnow + 1.0, stations, {"configuration"},
                                       {"email"}), &access);
            auto f = Range::findIntersecting(confList, tnow);
            if (f == confList.end())
                break;
            configSegment = *f;
        }
    } while (false);

    if (isTerminated())
        return false;

    for (const auto &station : stations) {
        if (isTerminated())
            return false;

        SequenceName stationUnit(station, "configuration", "email");
        auto config = configSegment.takeValue(stationUnit).read().hash("TimeCheck").hash(profile);
        if (!config.exists())
            continue;
        config.detachFromRoot();

        substitutions.clear();
        substitutions.setString("station", QString::fromStdString(station).toLower());

        double latestRemoteTime = FP::undefined();
        double latestLocalTime = FP::undefined();
        bool anyInspected = false;

        std::vector<Variant::Read> latestConfig;
        switch (config["Latest"].getType()) {
        case Variant::Type::Array:
            for (auto v: config["Latest"].toArray()) {
                latestConfig.emplace_back(std::move(v));
            }
            break;
        default:
            latestConfig.push_back(config["Latest"]);
            break;
        }

        for (const auto &c: latestConfig) {
            QString path(substitutions.apply(c["Path"].toQString()));
            if (!path.isEmpty()) {
                QList<FileDownloader::TimeCapture> captures;
                MatchingStack ms(substitutions, captures);
                QRegularExpression reFilter(ms.apply(c["Filter"].toQString()));

                QDir dir(path);
                QFileInfoList files(dir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot));
                for (QFileInfoList::const_iterator info = files.constBegin(),
                        endFiles = files.constEnd(); info != endFiles; ++info) {
                    if (isTerminated())
                        return false;

                    if (reFilter.isValid() && !reFilter.pattern().isEmpty()) {
                        auto filterMatch =
                                reFilter.match(info->fileName(), 0, QRegularExpression::NormalMatch,
                                               QRegularExpression::AnchoredMatchOption);
                        if (!filterMatch.hasMatch())
                            continue;
                        if (filterMatch.capturedLength() != info->fileName().length())
                            continue;

                        double referenceTime = FP::undefined();
                        if (c["ReferenceModified"].toBool()) {
                            QDateTime fmod(info->lastModified());
                            if (fmod.isValid()) {
                                referenceTime = Time::fromDateTime(fmod.toUTC(), true);
                            }
                        }

                        FileDownloader::TimeExtractContext context;
                        context.integrate(filterMatch, captures);
                        double time = context.extract(referenceTime);
                        if (FP::defined(time)) {
                            if (!FP::defined(latestRemoteTime) || time > latestRemoteTime) {
                                double mtime = Time::fromDateTime(info->lastModified(), true);
                                if (FP::defined(mtime)) {
                                    latestRemoteTime = time;
                                    latestLocalTime = mtime;
                                }
                            }
                            anyInspected = true;
                        }
                    }

                    double time = extractContents(info->absolutePath(), c["Contents"]);
                    if (FP::defined(time)) {
                        anyInspected = true;
                        if (!FP::defined(latestRemoteTime) || time > latestRemoteTime) {
                            QDateTime fmod(info->lastModified());
                            if (fmod.isValid()) {
                                double mtime = Time::fromDateTime(fmod, true);
                                if (FP::defined(mtime)) {
                                    latestRemoteTime = time;
                                    latestLocalTime = mtime;
                                }
                            }
                        }
                    }
                }
            }
        }

        auto path = substitutions.apply(config["Heartbeat/File"].toQString());
        if (!path.isEmpty()) {
            double time = extractContents(path, config["Heartbeat"]);
            if (FP::defined(time)) {
                anyInspected = true;
                if (!FP::defined(latestRemoteTime) || time > latestRemoteTime) {
                    QDateTime fmod(QFileInfo(path).lastModified());
                    if (fmod.isValid()) {
                        double mtime = Time::fromDateTime(fmod, true);
                        if (FP::defined(mtime)) {
                            latestRemoteTime = time;
                            latestLocalTime = mtime;
                        }
                    }
                }
            }

            if (!anyInspected && QFile(path).exists()) {
                anyInspected = true;
            }
        }

        if (!anyInspected)
            continue;

        qCDebug(log_email_timecheck) << "Latest remote file at time"
                                     << Logging::time(latestRemoteTime) << "received at"
                                     << Logging::time(latestLocalTime);

        QString output;
        if (!FP::defined(latestRemoteTime) || !FP::defined(latestLocalTime)) {
            output = substitutions.apply(config["NoTimeMessage"].toQString());
        } else {
            double downTime =
                    Variant::Composite::offsetTimeInterval(config["StationDown"], Time::time(),
                                                           false, Time::Day, 31);
            if (downTime > latestLocalTime)
                continue;

            double difference = latestLocalTime - latestRemoteTime;

            downTime = Variant::Composite::offsetTimeInterval(config["Loss"], Time::time(), false,
                                                              Time::Hour, 18);
            if (downTime > latestLocalTime) {
                output = config["LossMessage"].toQString();
            } else if (difference < 0.0) {
                double check =
                        Variant::Composite::offsetTimeInterval(config["Threshold"], latestLocalTime,
                                                               true, Time::Minute, 2);
                if (check >= latestRemoteTime)
                    continue;
                output = config["AheadMessage"].toQString();
            } else {
                double check =
                        Variant::Composite::offsetTimeInterval(config["Threshold"], latestLocalTime,
                                                               false, Time::Minute, 2);
                if (check <= latestRemoteTime)
                    continue;
                output = config["BehindMessage"].toQString();
            }
            if (output.isEmpty()) {
                output = config["WarningMessage"].toQString();
            }

            TextSubstitutionStack::Context subctx(substitutions);
            substitutions.setTime("local", latestLocalTime);
            substitutions.setTime("remote", latestRemoteTime);
            substitutions.setDuration("difference", difference,
                                      EmailTimeCheckComponent::tr("%1 behind", "time behind"),
                                      EmailTimeCheckComponent::tr("%1 ahead", "time ahead"));

            output = substitutions.apply(output);
        }

        if (output.isEmpty())
            continue;

        if (messages.empty()) {
            Variant::Root add(config["Message"]);
            add["Type"].setString("Break");
            messages.emplace_back(std::move(add));
        }

        Variant::Root add(config["Message"]);
        add["Type"].setString("Text");
        add["Text"].setString(output);
        messages.emplace_back(std::move(add));
    }

    return true;
}

bool EmailTimeCheck::prepare()
{ return true; }

bool EmailTimeCheck::process()
{
    SequenceValue::Transfer toOutput;
    double tnow = Time::time();
    qCDebug(log_email_timecheck) << "Generated" << messages.size() << "message(s)";
    int priority = 0;
    for (auto &v : messages) {
        toOutput.emplace_back(
                SequenceIdentity({"message", "message", "message"}, tnow, tnow, priority++),
                std::move(v));
    }

    messages.clear();
    outputData(toOutput);
    return false;
}


ComponentOptions EmailTimeCheckComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Execution profile"),
                                                tr("This is the profile name to execute.  Multiple profiles can be "
                                                   "defined to specify different output types for the same station.  "
                                                   "Consult the station configuration for available profiles."),
                                                tr("aerosol")));

    return options;
}

QList<ComponentExample> EmailTimeCheckComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Generate output alerts", "default example name"),
                                     tr("This will generate the alerts for the \"aerosol\" profile.")));

    return examples;
}

int EmailTimeCheckComponent::ingressAllowStations()
{ return INT_MAX; }

bool EmailTimeCheckComponent::ingressRequiresTime()
{ return false; }

ExternalConverter *EmailTimeCheckComponent::createExternalIngress(const ComponentOptions &options,
                                                                  double start,
                                                                  double end,
                                                                  const std::vector<
                                                                          SequenceName::Component> &stations)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    return new EmailTimeCheck(options, stations);
}
