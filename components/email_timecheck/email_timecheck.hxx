/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef EMAILTIMECHECK_H
#define EMAILTIMECHECK_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>
#include <QStringList>

#include "core/component.hxx"
#include "core/number.hxx"
#include "core/textsubstitution.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/root.hxx"
#include "transfer/download.hxx"

class EmailTimeCheck : public CPD3::Data::ExternalSourceProcessor {
    std::string profile;
    std::vector<CPD3::Data::SequenceName::Component> stations;

    CPD3::TextSubstitutionStack substitutions;

    class MatchingStack : public CPD3::TextSubstitutionStack {
        QList<CPD3::Transfer::FileDownloader::TimeCapture> &captures;
    public:
        MatchingStack(const CPD3::TextSubstitutionStack &base,
                      QList<CPD3::Transfer::FileDownloader::TimeCapture> &captures);

        virtual ~MatchingStack();

    protected:
        virtual QString substitution(const QStringList &elements) const;
    };

    double extractContents(const QString &fileName, const CPD3::Data::Variant::Read &config);

    std::vector<CPD3::Data::Variant::Root> messages;

public:
    EmailTimeCheck();

    EmailTimeCheck(const CPD3::ComponentOptions &options,
                   const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~EmailTimeCheck();

    void incomingData(const CPD3::Util::ByteView &data) override;

    void endData() override;

protected:
    bool prepare() override;

    bool process() override;

    bool begin() override;
};

class EmailTimeCheckComponent : public QObject, virtual public CPD3::Data::ExternalSourceComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalSourceComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.email_timecheck"
                              FILE
                              "email_timecheck.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int ingressAllowStations();

    virtual bool ingressRequiresTime();

    virtual CPD3::Data::ExternalConverter *createExternalIngress(const CPD3::ComponentOptions &options,
                                                                 double start,
                                                                 double end,
                                                                 const std::vector<CPD3::Data::SequenceName::Component> &stations = {});
};

#endif
