/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>
#include <QDateTime>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"
#include "algorithms/dewpoint.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Algorithms;

class ModelInstrument {
public:
    QByteArray outgoing;
    QByteArray incoming;
    QDateTime updateTime;
    double unpolledRemaining;
    double updateRemaining;

    double unpolledInterval;
    double updateInterval;

    bool isAuto;

    double WS;
    double WD;
    double WSGust;
    QByteArray windVariabilityString;
    double WZ;
    QByteArray runwayCodes;
    QByteArray WX;
    QByteArray cloudLevelStrings;
    double T;
    double TD;
    double P;
    double WI;

    QByteArray trendString;

    QByteArray remarkStrings;
    QByteArray wmoCloudCode;

    double elevation;
    enum {
        WS_MPS, WS_Knots,
    } windSpeedUnits;
    enum {
        WZ_KM, WZ_M, WZ_SM,
    } visibilityUnits;
    enum {
        P_HPA, P_INHG,
    } pressureUnits;

    QByteArray prefix;
    QByteArray suffix;

    ModelInstrument() : updateTime(QDate(2014, 02, 01), QTime(0, 0, 0), Qt::UTC),
                        unpolledRemaining(0),
                        updateRemaining(0),
                        unpolledInterval(60),
                        updateInterval(3600),
                        isAuto(false)
    {
        WS = 3.0;
        WD = 120.0;
        WSGust = 10.0;
        windVariabilityString = "110V150";

        WZ = 3000;

        runwayCodes = "R04/P1500N R22/P1500U";

        WX = "+SN";

        T = 21.0;
        TD = 15.0;

        P = 29.64;
        elevation = 0.0;

        WI = 0.02;

        cloudLevelStrings = "BKN022 OVC050";
        trendString = "NOSIG";

        remarkStrings = "AO2";
        wmoCloudCode = "8/123";

        windSpeedUnits = WS_MPS;
        visibilityUnits = WZ_M;
        pressureUnits = P_INHG;

        prefix = "METAR LBBG ";
        suffix = "=\r\n";
    }

    void appendFractionOrInteger(double input)
    {
        if (input <= 0.25)
            outgoing.append("0");
        else if (input <= 0.375)
            outgoing.append("1/4");
        else if (input <= 0.625)
            outgoing.append("1/2");
        else if (input <= 0.875)
            outgoing.append("3/4");
        else
            outgoing.append(QByteArray::number(input, 'f', 0));
    }

    void advance(double seconds)
    {
        updateRemaining -= seconds;
        while (updateRemaining <= 0.0) {
            updateRemaining += updateInterval;
            updateTime = updateTime.addMSecs((qint64) floor(updateInterval * 1000.0 + 0.5));
        }

        unpolledRemaining -= seconds;
        while (unpolledRemaining <= 0.0) {
            unpolledRemaining += unpolledInterval;

            outgoing.append(prefix);

            outgoing.append(QByteArray::number(updateTime.date().day()).rightJustified(2, '0'));
            outgoing.append(QByteArray::number(updateTime.time().hour()).rightJustified(2, '0'));
            outgoing.append(QByteArray::number(updateTime.time().minute()).rightJustified(2, '0'));
            outgoing.append('Z');

            if (isAuto) {
                outgoing.append(" AUTO");
            }

            if (FP::defined(WS) && FP::defined(WD)) {
                outgoing.append(' ');
                outgoing.append(QByteArray::number(WD, 'f', 0).rightJustified(3, '0'));
                outgoing.append(QByteArray::number(WS, 'f', 0).rightJustified(2, '0'));

                if (FP::defined(WSGust)) {
                    outgoing.append('G');
                    outgoing.append(QByteArray::number(WSGust, 'f', 0).rightJustified(2, '0'));
                }

                switch (windSpeedUnits) {
                case WS_MPS:
                    outgoing.append("MPS");
                    break;
                case WS_Knots:
                    outgoing.append("KT");
                    break;
                }
            }

            if (!windVariabilityString.isEmpty()) {
                outgoing.append(' ');
                outgoing.append(windVariabilityString);
            }

            if (FP::defined(WZ)) {
                outgoing.append(' ');
                switch (visibilityUnits) {
                case WZ_M:
                    outgoing.append(QByteArray::number(WZ, 'f', 0).rightJustified(4, '0'));
                    break;
                case WZ_KM:
                    appendFractionOrInteger(WZ);
                    outgoing.append("KM");
                    break;
                case WZ_SM:
                    appendFractionOrInteger(WZ);
                    outgoing.append("SM");
                    break;
                }
            }

            if (!runwayCodes.isEmpty()) {
                outgoing.append(' ');
                outgoing.append(runwayCodes);
            }

            if (!WX.isEmpty()) {
                outgoing.append(' ');
                outgoing.append(WX);
            }

            if (!cloudLevelStrings.isEmpty()) {
                outgoing.append(' ');
                outgoing.append(cloudLevelStrings);
            }

            if (FP::defined(T) && FP::defined(TD)) {
                outgoing.append(' ');

                if (T < 0.0) {
                    outgoing.append('M');
                    outgoing.append(QByteArray::number(-T, 'f', 0).rightJustified(2, '0'));
                } else {
                    outgoing.append(QByteArray::number(T, 'f', 0).rightJustified(2, '0'));
                }
                outgoing.append('/');
                if (TD < 0.0) {
                    outgoing.append('M');
                    outgoing.append(QByteArray::number(-TD, 'f', 0).rightJustified(2, '0'));
                } else {
                    outgoing.append(QByteArray::number(TD, 'f', 0).rightJustified(2, '0'));
                }
            }

            if (FP::defined(P)) {
                outgoing.append(' ');
                switch (pressureUnits) {
                case P_HPA:
                    outgoing.append('Q');
                    outgoing.append(QByteArray::number(P, 'f', 0).rightJustified(4, '0'));
                    break;
                case P_INHG:
                    outgoing.append('A');
                    outgoing.append(QByteArray::number(P * 100.0, 'f', 0).rightJustified(4, '0'));
                    break;
                }
            }

            if (!trendString.isEmpty()) {
                outgoing.append(' ');
                outgoing.append(trendString);
            }

            if (!remarkStrings.isEmpty() || !wmoCloudCode.isEmpty() || FP::defined(WI)) {
                outgoing.append(" RMK");

                if (!remarkStrings.isEmpty()) {
                    outgoing.append(' ');
                    outgoing.append(remarkStrings);
                }

                if (!wmoCloudCode.isEmpty()) {
                    outgoing.append(' ');
                    outgoing.append(wmoCloudCode);
                }

                if (FP::defined(WI)) {
                    outgoing.append(" P");
                    outgoing.append(QByteArray::number(WI * 100.0, 'f', 0).rightJustified(4, '0'));
                }
            }

            outgoing.append(suffix);
        }
    }

    double windSpeed(double input) const
    {
        Q_ASSERT(FP::defined(input));
        switch (windSpeedUnits) {
        case WS_MPS:
            return input;
        case WS_Knots:
            return input * 0.514444444;
        }
        return FP::undefined();
    }

    double visibility(double input) const
    {
        Q_ASSERT(FP::defined(input));
        switch (visibilityUnits) {
        case WZ_KM:
            return input;
        case WZ_M:
            return input * 1E-3;
        case WZ_SM:
            return input * 1.60934;
        }
        return FP::undefined();
    }

    double absolutePressure(double input) const
    {
        Q_ASSERT(FP::defined(input));
        switch (pressureUnits) {
        case P_HPA:
            input /= 33.86389;
            break;
        case P_INHG:
            break;
        }

        double E = elevation * 3.28084;
        static const double N = 0.1903;
        static const double K = 1.313E-5;

        input = pow(input, N) - K * E;
        if (input <= 0.0)
            return FP::undefined();
        input = pow(input, (1.0 / N));

        input *= 33.86389;
        return input;
    }

    double rh() const
    {
        return Dewpoint::rh(T, TD);
    }

    double precipitationRate(double input) const
    {
        Q_ASSERT(FP::defined(input));
        return input * 25.4;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, const ModelInstrument &model,
                   double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;

        if (FP::defined(model.WS) && !stream.hasMeta("WS", Variant::Root(), QString(), time))
            return false;
        if (FP::defined(model.WD) && !stream.hasMeta("WD", Variant::Root(), QString(), time))
            return false;
        if (FP::defined(model.WSGust) &&
                !stream.hasMeta("ZWSGust", Variant::Root(), QString(), time))
            return false;
        if (!model.windVariabilityString.isEmpty() &&
                !stream.hasMeta("ZWDVariability", Variant::Root(), QString(), time))
            return false;
        if (FP::defined(model.WZ) && !stream.hasMeta("WZ", Variant::Root(), QString(), time))
            return false;
        if (!model.WX.isEmpty() && !stream.hasMeta("WX1", Variant::Root(), QString(), time))
            return false;
        if (!model.WX.isEmpty() && !stream.hasMeta("WX2", Variant::Root(), QString(), time))
            return false;
        if (FP::defined(model.T) && !stream.hasMeta("T", Variant::Root(), QString(), time))
            return false;
        if (FP::defined(model.TD) && !stream.hasMeta("TD", Variant::Root(), QString(), time))
            return false;
        if (FP::defined(model.T) &&
                FP::defined(model.TD) && !stream.hasMeta("U", Variant::Root(), QString(), time))
            return false;
        if (FP::defined(model.T) &&
                !stream.hasMeta("P", Variant::Root(model.elevation), "^Elevation", time))
            return false;
        if (FP::defined(model.WI) && !stream.hasMeta("WI", Variant::Root(), QString(), time))
            return false;
        if (!model.wmoCloudCode.isEmpty() &&
                !stream.hasMeta("ZWMOCloud", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, const ModelInstrument &model,
                           double time = FP::undefined())
    {
        Q_UNUSED(stream);
        Q_UNUSED(model);
        Q_UNUSED(time);
        return true;
    }

    bool checkContiguous(StreamCapture &stream, const ModelInstrument &model)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (FP::defined(model.WS) && !stream.checkContiguous("WS"))
            return false;
        if (FP::defined(model.WD) && !stream.checkContiguous("WD"))
            return false;
        if (FP::defined(model.WSGust) && !stream.checkContiguous("ZWSGust"))
            return false;
        if (!model.windVariabilityString.isEmpty() && !stream.checkContiguous("ZWDVariability"))
            return false;
        if (FP::defined(model.WZ) && !stream.checkContiguous("WZ"))
            return false;
        if (!model.WX.isEmpty() && !stream.checkContiguous("WX1"))
            return false;
        if (!model.WX.isEmpty() && !stream.checkContiguous("WX2"))
            return false;
        if (FP::defined(model.T) && !stream.checkContiguous("T"))
            return false;
        if (FP::defined(model.TD) && !stream.checkContiguous("TD"))
            return false;
        if (FP::defined(model.T) && FP::defined(model.TD) && !stream.checkContiguous("U"))
            return false;
        if (FP::defined(model.P) && !stream.checkContiguous("P"))
            return false;
        if (FP::defined(model.WI) && !stream.checkContiguous("WI"))
            return false;
        if (!model.wmoCloudCode.isEmpty() && !stream.checkContiguous("ZWMOCloud"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream, const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (FP::defined(model.WS) &&
                !stream.hasAnyMatchingValue("WS", Variant::Root(model.windSpeed(model.WS)), time))
            return false;
        if (FP::defined(model.WD) &&
                !stream.hasAnyMatchingValue("WD", Variant::Root(model.WD), time))
            return false;
        if (FP::defined(model.WSGust) &&
                !stream.hasAnyMatchingValue("ZWSGust", Variant::Root(model.windSpeed(model.WSGust)),
                                            time))
            return false;
        if (!model.windVariabilityString.isEmpty() &&
                !stream.hasAnyMatchingValue("ZWDVariability", Variant::Root(), time))
            return false;
        if (FP::defined(model.WZ) &&
                !stream.hasAnyMatchingValue("WZ", Variant::Root(model.visibility(model.WZ)), time))
            return false;
        if (!model.WX.isEmpty() && !stream.hasAnyMatchingValue("WX1", Variant::Root(), time))
            return false;
        if (!model.WX.isEmpty() &&
                !stream.hasAnyMatchingValue("WX2", Variant::Root(QString::fromLatin1(model.WX)),
                                            time))
            return false;
        if (FP::defined(model.T) && !stream.hasAnyMatchingValue("T", Variant::Root(model.T), time))
            return false;
        if (FP::defined(model.TD) &&
                !stream.hasAnyMatchingValue("TD", Variant::Root(model.TD), time))
            return false;
        if (FP::defined(model.T) &&
                FP::defined(model.TD) &&
                !stream.hasAnyMatchingValue("U", Variant::Root(model.rh()), time))
            return false;
        if (FP::defined(model.P) &&
                !stream.hasAnyMatchingValue("P", Variant::Root(model.absolutePressure(model.P)),
                                            time))
            return false;
        if (FP::defined(model.WI) &&
                !stream.hasAnyMatchingValue("WI", Variant::Root(model.precipitationRate(model.WI)),
                                            time))
            return false;
        if (!model.wmoCloudCode.isEmpty() &&
                !stream.hasAnyMatchingValue("ZWMOCloud", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                             double time = FP::undefined())
    {
        Q_UNUSED(stream);
        Q_UNUSED(model);
        Q_UNUSED(time);
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_generic_metar"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_generic_metar"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("elevation")));
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Elevation"].setDouble(3000);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.elevation = 3000.0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(30.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 2000; i++) {
            control.advance(30.0);
            if (i % 100 == 0)
                QTest::qSleep(50);
        }
        QTest::qSleep(50);
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging, instrument));

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        /* Make it like the Vaisala AWOS at SPL */
        ModelInstrument instrument;
        instrument.isAuto = true;
        instrument.WS = 3.0;
        instrument.WD = 280.0;
        instrument.WSGust = FP::undefined();
        instrument.windVariabilityString.clear();
        instrument.WZ = FP::undefined();
        instrument.runwayCodes.clear();
        instrument.WX.clear();
        instrument.cloudLevelStrings = "OVC001";
        instrument.T = FP::undefined();
        instrument.TD = FP::undefined();
        instrument.P = 29.64;
        instrument.WI = FP::undefined();
        instrument.trendString.clear();
        instrument.remarkStrings = "A02 PWINO TSNO";
        instrument.wmoCloudCode.clear();
        instrument.windSpeedUnits = ModelInstrument::WS_Knots;
        instrument.pressureUnits = ModelInstrument::P_INHG;
        instrument.prefix.clear();
        instrument.prefix.append((char) 0x1B);
        instrument.prefix.append("tMETAR K3MW ");
        instrument.suffix.clear();
        instrument.suffix.append((char) 0x1B);
        instrument.suffix.append("=#p>8190>41BC");
        instrument.suffix.append((char) 0x1B);

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(30.0);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 2000; i++) {
            control.advance(30.0);
            if (i % 100 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging, instrument));

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }


    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["UnpolledFrequency"].setDouble(3600.0);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledInterval = 3600.0;
        instrument.WS = 5.0;
        instrument.WD = 280.0;
        instrument.WSGust = FP::undefined();
        instrument.windVariabilityString.clear();
        instrument.T = 5.0;
        instrument.TD = -4.0;
        instrument.WZ = 0.5;
        instrument.runwayCodes.clear();
        instrument.WX.clear();
        instrument.cloudLevelStrings.clear();
        instrument.P = 1005.0;
        instrument.WI = FP::undefined();
        instrument.trendString = "TEMPO 7000 -RADZ SCT020 WHT";
        instrument.remarkStrings.clear();
        instrument.wmoCloudCode.clear();
        instrument.pressureUnits = ModelInstrument::P_HPA;
        instrument.visibilityUnits = ModelInstrument::WZ_SM;

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 2000; i++) {
            control.advance(120.0);
            if (i % 100 == 0)
                QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging, instrument));

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.updateInterval = 60.0;
        instrument.WS = FP::undefined();
        instrument.WD = FP::undefined();
        instrument.WSGust = FP::undefined();
        instrument.windVariabilityString.clear();
        instrument.T = -5.0;
        instrument.TD = -7.0;
        instrument.WZ = FP::undefined();
        instrument.runwayCodes.clear();
        instrument.WX.clear();
        instrument.cloudLevelStrings.clear();
        instrument.P = FP::undefined();
        instrument.WI = FP::undefined();
        instrument.trendString.clear();
        instrument.remarkStrings = "SLP172";
        instrument.wmoCloudCode.clear();
        instrument.visibilityUnits = ModelInstrument::WZ_KM;
        instrument.pressureUnits = ModelInstrument::P_HPA;

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 2000; i++) {
            control.advance(30.0);
            if (i % 100 == 0)
                QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        instrument.P = 1017.2;

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging, instrument));

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
