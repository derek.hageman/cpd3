/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "algorithms/dewpoint.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_generic_metar.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Algorithms;


AcquireGenericMETAR::Configuration::Configuration() : start(FP::undefined()),
                                                      end(FP::undefined()),
                                                      reportInterval(60.0),
                                                      timeOrigin(FP::undefined()),
                                                      elevation(0.0),
                                                      reportEveryRecord(false)
{ }

AcquireGenericMETAR::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          reportInterval(other.reportInterval),
          timeOrigin(other.timeOrigin),
          elevation(other.elevation),
          reportEveryRecord(other.reportEveryRecord)
{ }

AcquireGenericMETAR::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          reportInterval(60.0),
          timeOrigin(FP::undefined()),
          elevation(0.0),
          reportEveryRecord(false)
{
    setFromSegment(other);
}

AcquireGenericMETAR::Configuration::Configuration(const Configuration &under,
                                                  const ValueSegment &over,
                                                  double s,
                                                  double e) : start(s),
                                                              end(e),
                                                              reportInterval(under.reportInterval),
                                                              timeOrigin(under.timeOrigin),
                                                              elevation(under.elevation),
                                                              reportEveryRecord(
                                                                      under.reportEveryRecord)
{
    setFromSegment(over);
}

void AcquireGenericMETAR::Configuration::setFromSegment(const ValueSegment &config)
{
    {
        double v = config["ReportInterval"].toDouble();
        if (FP::defined(v) && v > 0.0)
            reportInterval = v;
    }

    if (config["BaseTime"].exists())
        timeOrigin = config["BaseTime"].toDouble();

    if (config["ReportEveryRecord"].exists())
        reportEveryRecord = config["ReportEveryRecord"].toBool();


    if (FP::defined(config["Altitude"].toDouble()))
        elevation = config["Altitude"].toDouble();
    if (FP::defined(config["Elevation"].toDouble()))
        elevation = config["Elevation"].toDouble();
}


void AcquireGenericMETAR::setDefaultInvalid()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    lastObservationTime = FP::undefined();
    pendingExclude = 0;
}


AcquireGenericMETAR::AcquireGenericMETAR(const ValueSegment::Transfer &configData,
                                         const std::string &loggingContext) : FramedInstrument(
        "metar", loggingContext),
                                                                              lastRecordTime(
                                                                                      FP::undefined()),
                                                                              autoprobeStatus(
                                                                                      AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                              responseState(
                                                                                      RESP_WAIT)
{
    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }

    configurationChanged();
}

void AcquireGenericMETAR::setToAutoprobe()
{ responseState = RESP_AUTOPROBE_INITIALIZE; }

void AcquireGenericMETAR::setToInteractive()
{ responseState = RESP_INITIALIZE; }


ComponentOptions AcquireGenericMETARComponent::getBaseOptions()
{
    ComponentOptions options;

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(tr("elevation", "name"), tr("Station elevation"),
                                            tr("This is the station elevation in meters used to convert altimeter "
                                               "settings to absolute pressures."),
                                            tr("0m", "default elevation"));
    options.add("elevation", d);

    return options;
}

AcquireGenericMETAR::AcquireGenericMETAR(const ComponentOptions &options,
                                         const std::string &loggingContext) : FramedInstrument(
        "metar", loggingContext),
                                                                              lastRecordTime(
                                                                                      FP::undefined()),
                                                                              autoprobeStatus(
                                                                                      AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                              responseState(
                                                                                      RESP_WAIT)
{
    setDefaultInvalid();

    config.append(Configuration());

    if (options.isSet("basetime")) {
        config.last().timeOrigin =
                qobject_cast<ComponentOptionSingleTime *>(options.get("basetime"))->get();
    }
    if (options.isSet("elevation")) {
        config.last().elevation =
                qobject_cast<ComponentOptionSingleDouble *>(options.get("elevation"))->get();
    }

    configurationChanged();
}

AcquireGenericMETAR::~AcquireGenericMETAR()
{
}


void AcquireGenericMETAR::logValue(double startTime,
                                   double endTime,
                                   SequenceName::Component name,
                                   Variant::Root &&value)
{
    if (!loggingEgress)
        return;
    loggingEgress->emplaceData(SequenceName({}, "raw", std::move(name)), std::move(value),
                               startTime, endTime);
}

void AcquireGenericMETAR::realtimeValue(double time,
                                        SequenceName::Component name,
                                        Variant::Root value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceName({}, "raw", std::move(name)), std::move(value), time,
                                time + 1.0);
}

SequenceValue::Transfer AcquireGenericMETAR::buildLogMeta(double time,
                                                          quint32 include,
                                                          quint32 exclude) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_generic_metar");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["ICAOStation"].set(instrumentMeta["ICAOStation"]);

    if (include & Value_Wind) {
        result.emplace_back(SequenceName({}, "raw_meta", "WS"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.0");
        result.back().write().metadataReal("Units").setString("m/s");
        result.back().write().metadataReal("Description").setString("Wind speed");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("Vector2D");
        result.back()
              .write()
              .metadataReal("Smoothing")
              .hash("Parameters")
              .hash("Direction")
              .setString("WD");
        result.back()
              .write()
              .metadataReal("Smoothing")
              .hash("Parameters")
              .hash("Magnitude")
              .setString("WS");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Wind Speed"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

        result.emplace_back(SequenceName({}, "raw_meta", "WD"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.0");
        result.back().write().metadataReal("Units").setString("degrees");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Wind direction from true north");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("Vector2D");
        result.back()
              .write()
              .metadataReal("Smoothing")
              .hash("Parameters")
              .hash("Direction")
              .setString("WD");
        result.back()
              .write()
              .metadataReal("Smoothing")
              .hash("Parameters")
              .hash("Magnitude")
              .setString("WS");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Wind Direction"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
    } else if (exclude & Value_Wind) {
        result.emplace_back(SequenceName({}, "raw_meta", "WS"), Variant::Root(), time,
                            FP::undefined());
        result.emplace_back(SequenceName({}, "raw_meta", "WD"), Variant::Root(), time,
                            FP::undefined());
    }

    if (include & Value_WindGust) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZWSGust"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.0");
        result.back().write().metadataReal("Units").setString("m/s");
        result.back().write().metadataReal("Description").setString("Wind gust speed");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    } else if (exclude & Value_WindGust) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZWSGust"), Variant::Root(), time,
                            FP::undefined());
    }

    if (include & Value_WindVariability) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZWDVariability"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataHash("Description").setString("Wind speed");
        result.back().write().metadataHash("Source").set(instrumentMeta);
        result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
        result.back().write().metadataHashChild("Begin").metadataReal("Format").setString("000.0");
        result.back().write().metadataHashChild("Begin").metadataReal("Units").setString("degrees");
        result.back()
              .write()
              .metadataHashChild("Begin")
              .metadataReal("Description")
              .setString("Beginning wind direction from true north");
        result.back().write().metadataHashChild("End").metadataReal("Format").setString("000.0");
        result.back().write().metadataHashChild("End").metadataReal("Units").setString("degrees");
        result.back()
              .write()
              .metadataHashChild("End")
              .metadataReal("Description")
              .setString("Ending wind direction from true north");
    } else if (exclude & Value_WindGust) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZWDVariability"), Variant::Root(), time,
                            FP::undefined());
    }

    if (include & Value_WindGust) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZWSGust"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.0");
        result.back().write().metadataReal("Units").setString("m/s");
        result.back().write().metadataReal("Description").setString("Wind gust speed");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    } else if (exclude & Value_WindGust) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZWSGust"), Variant::Root(), time,
                            FP::undefined());
    }

    if (include & Value_WindVariability) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZWDVariability"), Variant::Root(), time,
                            FP::undefined());
        result.back()
              .write()
              .metadataHash("Description")
              .setString("Wind direction variability range");
        result.back().write().metadataHash("Source").set(instrumentMeta);
        result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
        result.back().write().metadataHashChild("Begin").metadataReal("Format").setString("000.0");
        result.back().write().metadataHashChild("Begin").metadataReal("Units").setString("degrees");
        result.back()
              .write()
              .metadataHashChild("Begin")
              .metadataReal("Description")
              .setString("Beginning variability angle from true north");
        result.back().write().metadataHashChild("End").metadataReal("Format").setString("000.0");
        result.back().write().metadataHashChild("End").metadataReal("Units").setString("degrees");
        result.back()
              .write()
              .metadataHashChild("End")
              .metadataReal("Description")
              .setString("Ending variability angle from true north");
    } else if (exclude & Value_WindVariability) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZWDVariability"), Variant::Root(), time,
                            FP::undefined());
    }

    if (include & Value_WZ) {
        result.emplace_back(SequenceName({}, "raw_meta", "WZ"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.0");
        result.back().write().metadataReal("Units").setString("km");
        result.back().write().metadataReal("Description").setString("Visibility distance");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Visibility"));
    } else if (exclude & Value_WZ) {
        result.emplace_back(SequenceName({}, "raw_meta", "WZ"), Variant::Root(), time,
                            FP::undefined());
    }

    if (include & Value_WX) {
        result.emplace_back(SequenceName({}, "raw_meta", "WX1"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataFlags("Format").setString("FFFFFFFF");
        result.back().write().metadataFlags("Description").setString("Present weather flags");
        result.back().write().metadataFlags("Source").set(instrumentMeta);
        result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

        result.back().write().metadataSingleFlag("Rain").hash("Description").setString("Rain");
        result.back().write().metadataSingleFlag("Rain").hash("Bits").setInt64(0x00000001);
        result.back()
              .write()
              .metadataSingleFlag("Rain")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back().write().metadataSingleFlag("Snow").hash("Description").setString("Snow");
        result.back().write().metadataSingleFlag("Snow").hash("Bits").setInt64(0x00000002);
        result.back()
              .write()
              .metadataSingleFlag("Snow")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("Drizzle")
              .hash("Description")
              .setString("Drizzle");
        result.back().write().metadataSingleFlag("Drizzle").hash("Bits").setInt64(0x00000004);
        result.back()
              .write()
              .metadataSingleFlag("Drizzle")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("IcePellets")
              .hash("Description")
              .setString("Ice pellets");
        result.back().write().metadataSingleFlag("IcePellets").hash("Bits").setInt64(0x00000008);
        result.back()
              .write()
              .metadataSingleFlag("IcePellets")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("IceCrystals")
              .hash("Description")
              .setString("Ice crystals");
        result.back().write().metadataSingleFlag("IceCrystals").hash("Bits").setInt64(0x00000010);
        result.back()
              .write()
              .metadataSingleFlag("IceCrystals")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back().write().metadataSingleFlag("Hail").hash("Description").setString("Hail");
        result.back().write().metadataSingleFlag("Hail").hash("Bits").setInt64(0x00000020);
        result.back()
              .write()
              .metadataSingleFlag("Hail")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("SmallHail")
              .hash("Description")
              .setString("Small hail");
        result.back().write().metadataSingleFlag("SmallHail").hash("Bits").setInt64(0x00000040);
        result.back()
              .write()
              .metadataSingleFlag("SmallHail")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("SnowGrains")
              .hash("Description")
              .setString("Snow grains");
        result.back().write().metadataSingleFlag("SnowGrains").hash("Bits").setInt64(0x00000080);
        result.back()
              .write()
              .metadataSingleFlag("SnowGrains")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("UnknownPrecipitation")
              .hash("Description")
              .setString("Unknown precipitation");
        result.back()
              .write()
              .metadataSingleFlag("UnknownPrecipitation")
              .hash("Bits")
              .setInt64(0x00000100);
        result.back()
              .write()
              .metadataSingleFlag("UnknownPrecipitation")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("Heavy")
              .hash("Description")
              .setString("Heavy intensity");
        result.back().write().metadataSingleFlag("Heavy").hash("Bits").setInt64(0x00001000);
        result.back()
              .write()
              .metadataSingleFlag("Heavy")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("Light")
              .hash("Description")
              .setString("Light intensity");
        result.back().write().metadataSingleFlag("Light").hash("Bits").setInt64(0x00002000);
        result.back()
              .write()
              .metadataSingleFlag("Light")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("Partial")
              .hash("Description")
              .setString("Partial");
        result.back().write().metadataSingleFlag("Partial").hash("Bits").setInt64(0x00008000);
        result.back()
              .write()
              .metadataSingleFlag("Partial")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back().write().metadataSingleFlag("Shower").hash("Description").setString("Shower");
        result.back().write().metadataSingleFlag("Shower").hash("Bits").setInt64(0x00010000);
        result.back()
              .write()
              .metadataSingleFlag("Shower")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("Patches")
              .hash("Description")
              .setString("Patches");
        result.back().write().metadataSingleFlag("Patches").hash("Bits").setInt64(0x00020000);
        result.back()
              .write()
              .metadataSingleFlag("Patches")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("InTheVicinity")
              .hash("Description")
              .setString("Effect in the vicinity of the station");
        result.back().write().metadataSingleFlag("InTheVicinity").hash("Bits").setInt64(0x00040000);
        result.back()
              .write()
              .metadataSingleFlag("InTheVicinity")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("Shallow")
              .hash("Description")
              .setString("Shallow");
        result.back().write().metadataSingleFlag("Shallow").hash("Bits").setInt64(0x00080000);
        result.back()
              .write()
              .metadataSingleFlag("Shallow")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("LowDrifting")
              .hash("Description")
              .setString("Low drifting");
        result.back().write().metadataSingleFlag("LowDrifting").hash("Bits").setInt64(0x00100000);
        result.back()
              .write()
              .metadataSingleFlag("LowDrifting")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("Blowing")
              .hash("Description")
              .setString("Blowing");
        result.back().write().metadataSingleFlag("Blowing").hash("Bits").setInt64(0x00200000);
        result.back()
              .write()
              .metadataSingleFlag("Blowing")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("Freezing")
              .hash("Description")
              .setString("Freezing");
        result.back().write().metadataSingleFlag("Freezing").hash("Bits").setInt64(0x00400000);
        result.back()
              .write()
              .metadataSingleFlag("Freezing")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back().write().metadataSingleFlag("Mist").hash("Description").setString("Mist");
        result.back().write().metadataSingleFlag("Mist").hash("Bits").setInt64(0x02000000);
        result.back()
              .write()
              .metadataSingleFlag("Mist")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back().write().metadataSingleFlag("Fog").hash("Description").setString("Fog");
        result.back().write().metadataSingleFlag("Fog").hash("Bits").setInt64(0x04000000);
        result.back()
              .write()
              .metadataSingleFlag("Fog")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back().write().metadataSingleFlag("Smoke").hash("Description").setString("Smoke");
        result.back().write().metadataSingleFlag("Smoke").hash("Bits").setInt64(0x08000000);
        result.back()
              .write()
              .metadataSingleFlag("Smoke")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("Thunderstorm")
              .hash("Description")
              .setString("Thunderstorm");
        result.back().write().metadataSingleFlag("Thunderstorm").hash("Bits").setInt64(0x10000000);
        result.back()
              .write()
              .metadataSingleFlag("Thunderstorm")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("VolcanicAsh")
              .hash("Description")
              .setString("Volcanic ash");
        result.back().write().metadataSingleFlag("VolcanicAsh").hash("Bits").setInt64(0x08000000);
        result.back()
              .write()
              .metadataSingleFlag("VolcanicAsh")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("WidespreadDust")
              .hash("Description")
              .setString("Widespread dust");
        result.back()
              .write()
              .metadataSingleFlag("WidespreadDust")
              .hash("Bits")
              .setInt64(0x08000000);
        result.back()
              .write()
              .metadataSingleFlag("WidespreadDust")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back().write().metadataSingleFlag("Sand").hash("Description").setString("Sand");
        result.back().write().metadataSingleFlag("Sand").hash("Bits").setInt64(0x08000000);
        result.back()
              .write()
              .metadataSingleFlag("Sand")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back().write().metadataSingleFlag("Spray").hash("Description").setString("Spray");
        result.back()
              .write()
              .metadataSingleFlag("Spray")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("WellDevelopedWhirls")
              .hash("Description")
              .setString("Well developed whirls present");
        result.back()
              .write()
              .metadataSingleFlag("WellDevelopedWhirls")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("Squalls")
              .hash("Description")
              .setString("Squalls present");
        result.back().write().metadataSingleFlag("Squalls").hash("Bits").setInt64(0x20000000);
        result.back()
              .write()
              .metadataSingleFlag("Squalls")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("Sandstorm")
              .hash("Description")
              .setString("Sandstorm present");
        result.back().write().metadataSingleFlag("Sandstorm").hash("Bits").setInt64(0x40000000);
        result.back()
              .write()
              .metadataSingleFlag("Sandstorm")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");

        result.back()
              .write()
              .metadataSingleFlag("Tornado")
              .hash("Description")
              .setString("Tornado present");
        result.back().write().metadataSingleFlag("Tornado").hash("Bits").setInt64(0x80000000);
        result.back()
              .write()
              .metadataSingleFlag("Tornado")
              .hash("Origin")
              .toArray()
              .after_back()
              .setString("acquire_generic_metar");


        result.emplace_back(SequenceName({}, "raw_meta", "WX2"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataString("Description").setString("Present weather code");
        result.back().write().metadataString("Source").set(instrumentMeta);
        result.back().write().metadataString("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataString("Realtime")
              .hash("Name")
              .setString(QObject::tr("Weather"));
        result.back().write().metadataString("Smoothing").hash("Mode").setString("None");
    } else if (exclude & Value_WX) {
        result.emplace_back(SequenceName({}, "raw_meta", "WX1"), Variant::Root(), time,
                            FP::undefined());
        result.emplace_back(SequenceName({}, "raw_meta", "WX2"), Variant::Root(), time,
                            FP::undefined());
    }

    if (include & Value_WMOCloud) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZWMOCloud"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataHash("Description").setString("WMO cloud reporting");
        result.back().write().metadataHash("Source").set(instrumentMeta);
        result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
        result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");

        result.back()
              .write()
              .metadataHashChild("Low")
              .metadataString("Description")
              .setString("Low cloud type");
        result.back()
              .write()
              .metadataHashChild("Low")
              .metadataString("EnumValues")
              .hash("None")
              .hash("Description")
              .setString("No clouds present");
        result.back()
              .write()
              .metadataHashChild("Low")
              .metadataString("EnumValues")
              .hash("Cumulus")
              .hash("Description")
              .setString("Cumulus clouds in fair weather");
        result.back()
              .write()
              .metadataHashChild("Low")
              .metadataString("EnumValues")
              .hash("ToweringCumulus")
              .hash("Description")
              .setString("Towering cumulus clouds");
        result.back()
              .write()
              .metadataHashChild("Low")
              .metadataString("EnumValues")
              .hash("CumulonimbusNoAnvil")
              .hash("Description")
              .setString("Cumulonimbus clouds with no anvil");
        result.back()
              .write()
              .metadataHashChild("Low")
              .metadataString("EnumValues")
              .hash("Stratocumulus")
              .hash("Description")
              .setString("Stratocumulus from cumulus clouds");
        result.back()
              .write()
              .metadataHashChild("Low")
              .metadataString("EnumValues")
              .hash("StratocumulusNotCumulus")
              .hash("Description")
              .setString("Stratocumulus without cumulus clouds");
        result.back()
              .write()
              .metadataHashChild("Low")
              .metadataString("EnumValues")
              .hash("Stratus")
              .hash("Description")
              .setString("Stratus or Fractostratus clouds in fair weather");
        result.back()
              .write()
              .metadataHashChild("Low")
              .metadataString("EnumValues")
              .hash("Fractus")
              .hash("Description")
              .setString("Fractocumulus or Fractostratus in bad weather");
        result.back()
              .write()
              .metadataHashChild("Low")
              .metadataString("EnumValues")
              .hash("CumulusAndStratocumulus")
              .hash("Description")
              .setString("Cumulus and Stratocumulus clouds");
        result.back()
              .write()
              .metadataHashChild("Low")
              .metadataString("EnumValues")
              .hash("Cumulonimbus")
              .hash("Description")
              .setString("Cumulonimbus thunderstorm");

        result.back()
              .write()
              .metadataHashChild("Middle")
              .metadataString("Description")
              .setString("middle cloud type");
        result.back()
              .write()
              .metadataHashChild("Middle")
              .metadataString("EnumValues")
              .hash("None")
              .hash("Description")
              .setString("No clouds present");
        result.back()
              .write()
              .metadataHashChild("Middle")
              .metadataString("EnumValues")
              .hash("ThinAltostratus")
              .hash("Description")
              .setString("Thin Altostratus clouds");
        result.back()
              .write()
              .metadataHashChild("Middle")
              .metadataString("EnumValues")
              .hash("ThickAltostratus")
              .hash("Description")
              .setString("Thick Altostratus clouds");
        result.back()
              .write()
              .metadataHashChild("Middle")
              .metadataString("EnumValues")
              .hash("ThinAltocumulus")
              .hash("Description")
              .setString("Thin Altocumulus clouds");
        result.back()
              .write()
              .metadataHashChild("Middle")
              .metadataString("EnumValues")
              .hash("PatchyAltocumulus")
              .hash("Description")
              .setString("Patchy Altocumulus clouds");
        result.back()
              .write()
              .metadataHashChild("Middle")
              .metadataString("EnumValues")
              .hash("ThickeningAltocumulus")
              .hash("Description")
              .setString("Thickening Altocumulus clouds");
        result.back()
              .write()
              .metadataHashChild("Middle")
              .metadataString("EnumValues")
              .hash("AltocumulusFromCumulus")
              .hash("Description")
              .setString("Altocumulus from cumulus clouds");
        result.back()
              .write()
              .metadataHashChild("Middle")
              .metadataString("EnumValues")
              .hash("Altocumulus")
              .hash("Description")
              .setString("Altocumulus with altostratus or nimbostratus clouds");
        result.back()
              .write()
              .metadataHashChild("Middle")
              .metadataString("EnumValues")
              .hash("AltocumulusWithTurrets")
              .hash("Description")
              .setString("Altocumulus clouds with turrets");
        result.back()
              .write()
              .metadataHashChild("Middle")
              .metadataString("EnumValues")
              .hash("ChaoticAltocumulus")
              .hash("Description")
              .setString("Chaotic Altocumulus clouds");

        result.back()
              .write()
              .metadataHashChild("High")
              .metadataString("Description")
              .setString("High cloud type");
        result.back()
              .write()
              .metadataHashChild("High")
              .metadataString("EnumValues")
              .hash("None")
              .hash("Description")
              .setString("No clouds present");
        result.back()
              .write()
              .metadataHashChild("High")
              .metadataString("EnumValues")
              .hash("CirrusFilaments")
              .hash("Description")
              .setString("Cirrus cloud filaments");
        result.back()
              .write()
              .metadataHashChild("High")
              .metadataString("EnumValues")
              .hash("DenseCirrus")
              .hash("Description")
              .setString("Dense Cirrus clouds");
        result.back()
              .write()
              .metadataHashChild("High")
              .metadataString("EnumValues")
              .hash("CirrusWithCumulonimbus")
              .hash("Description")
              .setString("Cirrus with Cumulonimbus clouds");
        result.back()
              .write()
              .metadataHashChild("High")
              .metadataString("EnumValues")
              .hash("ThickeningCirrus")
              .hash("Description")
              .setString("Thickening Cirrus clouds");
        result.back()
              .write()
              .metadataHashChild("High")
              .metadataString("EnumValues")
              .hash("LowCirrostratus")
              .hash("Description")
              .setString("Cirrostratus or Cirrus clouds low in the sky");
        result.back()
              .write()
              .metadataHashChild("High")
              .metadataString("EnumValues")
              .hash("HighCirrostratus")
              .hash("Description")
              .setString("Cirrostratus or Cirrus clouds high in the sky");
        result.back()
              .write()
              .metadataHashChild("High")
              .metadataString("EnumValues")
              .hash("WholeSkyCirrostratus")
              .hash("Description")
              .setString("Whole sky Cirrostratus clouds");
        result.back()
              .write()
              .metadataHashChild("High")
              .metadataString("EnumValues")
              .hash("PartialCirrostratus")
              .hash("Description")
              .setString("Partial Cirrostratus clouds");
        result.back()
              .write()
              .metadataHashChild("High")
              .metadataString("EnumValues")
              .hash("Cirrocumulus")
              .hash("Description")
              .setString("Cirrocumulus clouds");


        result.back()
              .write()
              .metadataHashChild("Code")
              .metadataString("Description")
              .setString("Reported identifier code");
    } else if (exclude & Value_WMOCloud) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZWMOCloud"), Variant::Root(), time,
                            FP::undefined());
    }

    if (include & Value_T) {
        result.emplace_back(SequenceName({}, "raw_meta", "T"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.0");
        result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
        result.back().write().metadataReal("Description").setString("Ambient temperature");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Ambient"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    } else if (exclude & Value_T) {
        result.emplace_back(SequenceName({}, "raw_meta", "T"), Variant::Root(), time,
                            FP::undefined());
    }

    if (include & Value_TD) {
        result.emplace_back(SequenceName({}, "raw_meta", "TD"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.0");
        result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
        result.back().write().metadataReal("Description").setString("Ambient dewpoint");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        if (!(include & Value_U)) {
            result.back()
                  .write()
                  .metadataReal("Realtime")
                  .hash("Name")
                  .setString(QObject::tr("Dewpoint"));
            result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
        }
    } else if (exclude & Value_TD) {
        result.emplace_back(SequenceName({}, "raw_meta", "TD"), Variant::Root(), time,
                            FP::undefined());
    }

    if (include & Value_U) {
        result.emplace_back(SequenceName({}, "raw_meta", "U"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.0");
        result.back().write().metadataReal("Units").setString("%");
        result.back().write().metadataReal("Description").setString("Ambient relative humidity");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("RH");
        result.back().write().metadataReal("Smoothing").hash("Parameters").hash("T").setString("T");
        result.back()
              .write()
              .metadataReal("Smoothing")
              .hash("Parameters")
              .hash("Dewpoint")
              .setString("TD");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Ambient"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    } else if (exclude & Value_U) {
        result.emplace_back(SequenceName({}, "raw_meta", "U"), Variant::Root(), time,
                            FP::undefined());
    }

    if (include & Value_P) {
        result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("0000.0");
        result.back().write().metadataReal("Units").setString("hPa");
        result.back().write().metadataReal("Description").setString("Ambient pressure");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Elevation").setDouble(config.first().elevation);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Ambient"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    } else if (exclude & Value_P) {
        result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time,
                            FP::undefined());
    }

    if (include & Value_WI) {
        result.emplace_back(SequenceName({}, "raw_meta", "WI"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.000");
        result.back().write().metadataReal("Units").setString("mm/h");
        result.back().write().metadataReal("Description").setString("Precipitation rate");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Precipitation"));
    } else if (exclude & Value_WI) {
        result.emplace_back(SequenceName({}, "raw_meta", "WI"), Variant::Root(), time,
                            FP::undefined());
    }

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    return result;
}

SequenceValue::Transfer AcquireGenericMETAR::buildRealtimeMeta(double time,
                                                               quint32 include,
                                                               quint32 exclude) const
{
    Q_UNUSED(time);
    Q_UNUSED(include);
    Q_UNUSED(exclude);
    return SequenceValue::Transfer();
}

/* We look for the "METAR" sequence to start the frame and end it
 * on any non-allowed character or an "=". */
std::size_t AcquireGenericMETAR::dataFrameStart(std::size_t offset,
                                                const Util::ByteArray &input) const
{
    for (auto max = input.size(); offset < max; ++offset) {
        auto ch = input[offset];
        if (ch != 'M' && ch != 'm')
            continue;
        if (offset + 4 >= max)
            break;

        ch = input[offset + 1];
        if (ch != 'E' && ch != 'e')
            continue;

        ch = input[offset + 2];
        if (ch != 'T' && ch != 't')
            continue;

        ch = input[offset + 3];
        if (ch != 'A' && ch != 'a')
            continue;

        ch = input[offset + 4];
        if (ch != 'R' && ch != 'r')
            continue;

        return offset;
    }
    return input.npos;
}

static bool allowedCharacter(char ch)
{
    if (ch >= '0' && ch <= '9')
        return true;
    if (ch >= 'A' && ch <= 'Z')
        return true;
    if (ch >= 'a' && ch <= 'z')
        return true;
    if (ch == '/')
        return true;
    if (ch == '+' || ch == '-')
        return true;
    if (ch == ' ' || ch == '\t')
        return true;
    return false;
}

std::size_t AcquireGenericMETAR::dataFrameEnd(std::size_t start, const Util::ByteArray &input) const
{
    for (auto max = input.size(); start < max; start++) {
        auto ch = input[start];
        if (ch == '=')
            return start;
        if (!allowedCharacter(ch))
            return start;
    }
    return input.npos;
}

quint32 AcquireGenericMETAR::ValueData::availableFields() const
{
    quint32 result = 0;
    if (WD.read().exists() || WS.read().exists())
        result |= Value_Wind;
    if (ZWSGust.read().exists())
        result |= Value_WindGust;
    if (ZWDVariability.read().exists())
        result |= Value_WindVariability;
    if (WZ.read().exists())
        result |= Value_WZ;
    if (WX1.read().exists() || WX2.read().exists())
        result |= Value_WX;
    if (ZWMOCloud.read().exists())
        result |= Value_WMOCloud;
    if (T.read().exists())
        result |= Value_T;
    if (TD.read().exists())
        result |= Value_TD;
    if (U.read().exists())
        result |= Value_U;
    if (P.read().exists())
        result |= Value_P;
    if (WI.read().exists())
        result |= Value_WI;
    return result;
}

static double altimeterToAbsolute(double P, double E)
{
    if (!FP::defined(P) || !FP::defined(E))
        return FP::undefined();
    if (P <= 0.0)
        return FP::undefined();
    /* The only formula I have for this expects the pressure in inHg and
     * the elevation in ft */
    P /= 33.86389;
    E *= 3.28084;

    static const double N = 0.1903;
    static const double K = 1.313E-5;

    P = pow(P, N) - K * E;
    if (P <= 0.0)
        return FP::undefined();
    P = pow(P, (1.0 / N));

    P *= 33.86389;
    return P;
}

static bool validDigit(char c)
{ return (c >= '0' && c <= '9'); }

static bool isAllDigits(const Util::ByteView &data)
{
    for (auto check : data) {
        if (!validDigit(check))
            return false;
    }
    return true;
}

bool AcquireGenericMETAR::processPresentWeatherCode(Util::ByteView field, ValueData &values) const
{
    Variant::Flags flags;

    if (field.string_start("+")) {
        field = field.mid(1);
        flags.insert("Heavy");
    } else if (field.string_start("-")) {
        field = field.mid(1);
        flags.insert("Light");
    } else if (field.string_start("VC")) {
        field = field.mid(2);
        flags.insert("InTheVicinity");
    }

    if (field.string_start("MI")) {
        field = field.mid(2);
        flags.insert("Shallow");
    } else if (field.string_start("PR")) {
        field = field.mid(2);
        flags.insert("Partial");
    } else if (field.string_start("BC")) {
        field = field.mid(2);
        flags.insert("Patches");
    } else if (field.string_start("DR")) {
        field = field.mid(2);
        flags.insert("LowDrifting");
    } else if (field.string_start("BL")) {
        field = field.mid(2);
        flags.insert("Blowing");
    } else if (field.string_start("SH")) {
        field = field.mid(2);
        flags.insert("Shower");
    } else if (field.string_start("TS")) {
        field = field.mid(2);
        flags.insert("ThunderStorm");
    } else if (field.string_start("FZ")) {
        field = field.mid(2);
        flags.insert("Freezing");
    }

    if (field.string_start("DZ")) {
        field = field.mid(2);
        flags.insert("Drizzle");
    } else if (field.string_start("RA")) {
        field = field.mid(2);
        flags.insert("Rain");
    } else if (field.string_start("SN")) {
        field = field.mid(2);
        flags.insert("Snow");
    } else if (field.string_start("SG")) {
        field = field.mid(2);
        flags.insert("SnowGrains");
    } else if (field.string_start("IC")) {
        field = field.mid(2);
        flags.insert("IceCrystals");
    } else if (field.string_start("PL")) {
        field = field.mid(2);
        flags.insert("IcePellets");
    } else if (field.string_start("GR")) {
        field = field.mid(2);
        flags.insert("Hail");
    } else if (field.string_start("GS")) {
        field = field.mid(2);
        flags.insert("SmallHail");
    } else if (field.string_start("UP")) {
        field = field.mid(2);
        flags.insert("UnknownPrecipitation");
    }

    if (field.string_start("BR")) {
        field = field.mid(2);
        flags.insert("Mist");
    } else if (field.string_start("FG")) {
        field = field.mid(2);
        flags.insert("Fog");
    } else if (field.string_start("FU")) {
        field = field.mid(2);
        flags.insert("Smoke");
    } else if (field.string_start("VA")) {
        field = field.mid(2);
        flags.insert("VolcanicAsh");
    } else if (field.string_start("DU")) {
        field = field.mid(2);
        flags.insert("WidespreadDust");
    } else if (field.string_start("SA")) {
        field = field.mid(2);
        flags.insert("Sand");
    } else if (field.string_start("HZ")) {
        field = field.mid(2);
        flags.insert("Haze");
    } else if (field.string_start("PY")) {
        field = field.mid(2);
        flags.insert("Spray");
    }

    if (field.string_start("PO")) {
        field = field.mid(2);
        flags.insert("WellDevelopedWhirls");
    } else if (field.string_start("SQ")) {
        field = field.mid(2);
        flags.insert("Squalls");
    } else if (field.string_start("FC")) {
        field = field.mid(2);
        flags.insert("Tornado");
    } else if (field.string_start("SS")) {
        field = field.mid(2);
        flags.insert("Sandstorm");
    }

    if (!field.empty())
        return 1;

    values.WX1.write().setFlags(std::move(flags));
    return 0;
}

static double interpretCloudCoverCode(const Util::ByteView &code)
{
    if (code == "SKC")
        return 0.0;
    if (code == "CLR")
        return 0.0;
    if (code == "NSC")
        return 0.0;
    if (code == "FEW")
        return 2.0 / 8.0;
    if (code == "SCT")
        return 4.0 / 8.0;
    if (code == "BKN")
        return 7.0 / 8.0;
    if (code == "OVC")
        return 1.0;
    return FP::undefined();
}


int AcquireGenericMETAR::processField(const Util::ByteView &field, int &type, ValueData &values)
{
    bool ok = false;
    auto length = field.size();

    if (type >= Field_RemarkSectionBegin) {

        if (length == 5 && field.string_start("8/")) {
            switch (field[2]) {
            case '0':
                values.ZWMOCloud.write().hash("Low").setString("None");
                break;
            case '1':
                values.ZWMOCloud.write().hash("Low").setString("Cumulus");
                break;
            case '2':
                values.ZWMOCloud.write().hash("Low").setString("ToweringCumulus");
                break;
            case '3':
                values.ZWMOCloud.write().hash("Low").setString("CumulonimbusNoAnvil");
                break;
            case '4':
                values.ZWMOCloud.write().hash("Low").setString("Stratocumulus");
                break;
            case '5':
                values.ZWMOCloud.write().hash("Low").setString("StratocumulusNotCumulus");
                break;
            case '6':
                values.ZWMOCloud.write().hash("Low").setString("Stratus");
                break;
            case '7':
                values.ZWMOCloud.write().hash("Low").setString("Fractus");
                break;
            case '8':
                values.ZWMOCloud.write().hash("Low").setString("CumulusAndStratocumulus");
                break;
            case '9':
                values.ZWMOCloud.write().hash("Low").setString("Cumulonimbus");
                break;
            default:
                return 500;
            }

            switch (field[3]) {
            case '0':
                values.ZWMOCloud.write().hash("Middle").setString("None");
                break;
            case '1':
                values.ZWMOCloud.write().hash("Middle").setString("ThinAltostratus");
                break;
            case '2':
                values.ZWMOCloud.write().hash("Middle").setString("ThickAltostratus");
                break;
            case '3':
                values.ZWMOCloud.write().hash("Middle").setString("ThinAltocumulus");
                break;
            case '4':
                values.ZWMOCloud.write().hash("Middle").setString("PatchyAltocumulus");
                break;
            case '5':
                values.ZWMOCloud.write().hash("Middle").setString("ThickeningAltocumulus");
                break;
            case '6':
                values.ZWMOCloud.write().hash("Middle").setString("AltocumulusFromCumulus");
                break;
            case '7':
                values.ZWMOCloud.write().hash("Middle").setString("Altocumulus");
                break;
            case '8':
                values.ZWMOCloud.write().hash("Middle").setString("AltocumulusWithTurrets");
                break;
            case '9':
                values.ZWMOCloud.write().hash("Middle").setString("ChaoticAltocumulus");
                break;
            case '/':
                break;
            default:
                return 501;
            }

            switch (field[4]) {
            case '0':
                values.ZWMOCloud.write().hash("High").setString("None");
                break;
            case '1':
                values.ZWMOCloud.write().hash("High").setString("CirrusFilaments");
                break;
            case '2':
                values.ZWMOCloud.write().hash("High").setString("DenseCirrus");
                break;
            case '3':
                values.ZWMOCloud.write().hash("High").setString("CirrusWithCumulonimbus");
                break;
            case '4':
                values.ZWMOCloud.write().hash("High").setString("ThickeningCirrus");
                break;
            case '5':
                values.ZWMOCloud.write().hash("High").setString("LowCirrostratus");
                break;
            case '6':
                values.ZWMOCloud.write().hash("High").setString("HighCirrostratus");
                break;
            case '7':
                values.ZWMOCloud.write().hash("High").setString("WholeSkyCirrostratus");
                break;
            case '8':
                values.ZWMOCloud.write().hash("High").setString("PartialCirrostratus");
                break;
            case '9':
                values.ZWMOCloud.write().hash("High").setString("Cirrocumulus");
                break;
            case '/':
                break;
            default:
                return 502;
            }

            values.ZWMOCloud.write().hash("Code").setString(field.mid(2).toString());
            type = Field_WMOCloudCode;
            return 0;
        }

        if (length == 6 && field.string_start("SLP") && isAllDigits(field.mid(3, 3))) {
            int p = field.mid(3, 3).parse_i32(&ok);
            if (!ok) return 510;
            if (p < 0 || p > 999) return 511;

            if (!values.P.read().exists()) {
                Variant::Root PALT((double) p * 0.1 + 1000.0);
                remap("ZPALT", PALT);
                values.P
                      .write()
                      .setDouble(altimeterToAbsolute(PALT.read().toDouble(),
                                                     config.first().elevation));
            }

            type = Field_RemarkSeaLevelPressure;
            return 0;
        }

        if (length == 9 && field.string_start("T") && isAllDigits(field.mid(1, 8))) {
            double multiplier = 0;
            switch (field[1]) {
            case '0':
                multiplier = 0.1;
                break;
            case '1':
                multiplier = -0.1;
                break;
            default:
                return 520;
            }

            values.T.write().setDouble((double) field.mid(2, 3).parse_i32(&ok) * multiplier);
            if (!ok) return 521;


            switch (field[5]) {
            case '0':
                multiplier = 0.1;
                break;
            case '1':
                multiplier = -0.1;
                break;
            default:
                return 522;
            }
            values.TD.write().setDouble((double) field.mid(6, 3).parse_i32(&ok) * multiplier);
            if (!ok) return 523;

            type = Field_RemarkTemperatureAndDewpoint;
            return 0;
        }

        if (length == 5 && field.string_start("P") && isAllDigits(field.mid(1, 4))) {
            values.WI.write().setDouble((double) field.mid(1, 4).parse_i32(&ok) * 0.01 * 25.4);
            if (!ok) return 530;

            type = Field_RemarkPrecipitationRate;
            return 0;
        }

        if (length == 5 && field.string_start("6") && isAllDigits(field)) {
            if (!values.WI.read().exists()) {
                double interval = 6.0;

                if (FP::defined(lastObservationTime)) {
                    double secondOfDay =
                            lastObservationTime - floor(lastObservationTime / 86400.0) * 86400.0;
                    int hourOfDay = (int) qRound(secondOfDay / 3600.0);

                    /* This appears to be a six hour emptied rain gauge
                     * that's allowed to be read more often (nominally
                     * every three hours), so set the rate equal to the
                     * number of hours after a six hour mark.  That makes
                     * it work for the nominal case but also allows for 
                     * more frequent reading than every three as well. */
                    int hoursAfterSix = hourOfDay % 6;
                    if (hoursAfterSix == 0) {
                        interval = 6.0;
                    } else {
                        interval = (double) hoursAfterSix;
                    }
                }

                values.WI
                      .write()
                      .setDouble((double) field.mid(1, 4).parse_i32(&ok) * 0.01 * 25.4 / interval);
                if (!ok) return 550;
            }

            type = Field_RemarkPrecipitationRate;
            return 0;
        }

        if (length == 5 && field.string_start("7") && isAllDigits(field)) {
            if (!values.WI.read().exists()) {
                values.WI
                      .write()
                      .setDouble((double) field.mid(1, 4).parse_i32(&ok) * 0.01 * 25.4 / 24.0);
                if (!ok) return 540;
            }

            type = Field_RemarkPrecipitationRate;
            return 0;
        }

        type = Field_RemarkUnknown;
        return -1;
    }

    if (type < Field_Trend && field == "AUTO") {
        type = Field_Auto;
        return 0;
    }

    if (type < Field_Trend &&
            (length == 7 || (length == 10 && field[5] == 'G')) &&
            field.string_end("KT") &&
            (isAllDigits(field.mid(0, 5)) ||
                    (isAllDigits(field.mid(3, 2)) && field.mid(0, 3) == "VRB"))) {
        if (isAllDigits(field.mid(0, 3))) {
            int d = field.mid(0, 3).parse_i32(&ok);
            if (!ok) return 10;
            if (d < 0 || d > 360) return 11;
            values.WD.write().setDouble(d);
        } else if (field.mid(0, 3) != "VRB") {
            return 12;
        }

        values.WS.write().setDouble(field.mid(3, 2).parse_i32(&ok) * 0.514444444);
        if (!ok) return 13;

        if (length == 10) {
            values.ZWSGust.write().setDouble(field.mid(6, 2).parse_i32(&ok) * 0.514444444);
            if (!ok) return 14;
        }

        type = Field_WindAbsolute;
        return 0;
    }

    if (type < Field_Trend &&
            (length == 8 || (length == 11 && field[5] == 'G')) &&
            field.string_end("MPS") &&
            (isAllDigits(field.mid(0, 5)) ||
                    (isAllDigits(field.mid(3, 2)) && field.mid(0, 3) == "VRB"))) {
        if (isAllDigits(field.mid(0, 3))) {
            int d = field.mid(0, 3).parse_i32(&ok);
            if (!ok) return 20;
            if (d < 0 || d > 360) return 21;
            values.WD.write().setDouble(d);
        } else if (field.mid(0, 3) != "VRB") {
            return 22;
        }

        values.WS.write().setDouble(field.mid(3, 2).parse_i32(&ok));
        if (!ok) return 23;

        if (length == 11) {
            values.ZWSGust.write().setDouble(field.mid(6, 2).parse_i32(&ok));
            if (!ok) return 24;
        }

        type = Field_WindAbsolute;
        return 0;
    }

    if (type < Field_Trend &&
            length == 7 &&
            field[3] == 'V' &&
            isAllDigits(field.mid(0, 3)) &&
            isAllDigits(field.mid(4, 3))) {
        Variant::Root begin((double) field.mid(0, 3).parse_i32(&ok));
        if (!ok) return 30;
        Variant::Root end((double) field.mid(4, 3).parse_i32(&ok));
        if (!ok) return 31;

        remap("ZVariabilityBegin", begin);
        remap("ZVariabilityEnd", end);
        values.ZWDVariability.write().hash("Begin").set(begin);
        values.ZWDVariability.write().hash("End").set(end);

        type = Field_WindVariabiltiy;
        return 0;
    }

    if (type < Field_Trend && length == 4 && isAllDigits(field)) {
        values.WZ.write().setDouble(field.parse_i32(&ok) * 1E-3);
        if (!ok) return 41;

        type = Field_Visiblity;
        return 0;
    }

    if (type < Field_Trend && length >= 3 && length <= 5 && field.string_end("SM")) {
        auto fraction = field.mid(0, field.size() - 2).split('/');
        double v;
        if (fraction.size() == 2) {
            v = (double) fraction[0].toQByteArray().trimmed().toInt(&ok);
            if (!ok) return 51;
            double d = (double) fraction[1].toQByteArray().trimmed().toInt(&ok);
            if (!ok) return 52;
            if (d == 0.0) return 53;
            v /= d;
        } else if (fraction.size() == 1) {
            v = (double) fraction[0].toQByteArray().trimmed().toInt(&ok);
            if (!ok) return 54;
        } else {
            return 55;
        }

        values.WZ.write().setDouble(v * 1.60934);

        type = Field_Visiblity;
        return 0;
    }

    if (type < Field_Trend && length >= 3 && length <= 5 && field.string_end("KM")) {
        auto fraction = field.mid(0, field.size() - 2).split('/');
        double v;
        if (fraction.size() == 2) {
            v = (double) fraction[0].toQByteArray().trimmed().toInt(&ok);
            if (!ok) return 61;
            double d = (double) fraction[1].toQByteArray().trimmed().toInt(&ok);
            if (!ok) return 62;
            if (d == 0.0) return 63;
            v /= d;
        } else if (fraction.size() == 1) {
            v = (double) fraction[0].toQByteArray().trimmed().toInt(&ok);
            if (!ok) return 64;
        } else {
            return 65;
        }

        values.WZ.write().setDouble(v);

        type = Field_Visiblity;
        return 0;
    }

    if (type < Field_Trend &&
            length == 10 &&
            field.string_start("R") &&
            field[3] == '/' &&
            field[4] == 'P') {
        type = Field_RunwayVisualRange;
        return 0;
    }

    if (type < Field_Trend && length >= 2 && length <= 10) {
        if (processPresentWeatherCode(field, values) == 0) {
            values.WX2.write().setString(field.toString());

            type = Field_WXCode;
            return 0;
        }
    }

    if (type < Field_Trend && length >= 3 && field.string_start("VV")) {
        type = Field_CloudLayer;
        return 0;
    }

    if (type < Field_Trend && length >= 6 && length <= 9) {
        double c = interpretCloudCoverCode(field.mid(0, 3));
        if (FP::defined(c)) {
            /* 100 ft to km */
            Variant::Root
                    level((double) field.mid(3).toQByteArray().trimmed().toInt(&ok) * 0.03048);
            Variant::Root cover(c);
            remap("ZCLOUDLEVEL", level);
            remap("ZCLOUDFRACTION", cover);

            type = Field_CloudLayer;
            return 0;
        }
    }

    if (type < Field_Trend && field == "CAVOK") {
        type = Field_CloudLayer;
        return 0;
    }

    if (type < Field_Trend &&
            length >= 5 &&
            length <= 6 &&
            field[2] == '/' &&
            isAllDigits(field.mid(0, 2))) {
        values.T.write().setDouble((double) field.mid(0, 2).parse_i32(&ok));
        if (!ok) return 70;
        auto next = field.mid(3);
        int multiplier = 1;
        if (next.string_start("M")) {
            multiplier = -1;
            next = next.mid(1);
        }

        values.TD.write().setDouble((double) (next.parse_i32(&ok) * multiplier));
        if (!ok) return 71;

        type = Field_TemperatureAndDewpoint;
        return 0;
    }

    if (type < Field_Trend &&
            length >= 6 &&
            length <= 7 &&
            field[0] == 'M' &&
            field[3] == '/' &&
            isAllDigits(field.mid(1, 2))) {
        values.T.write().setDouble((double) (field.mid(1, 2).parse_i32(&ok) * -1));
        if (!ok) return 80;
        auto next = field.mid(4);
        double multiplier = 1.0;
        if (next.string_start("M")) {
            multiplier = -1;
            next = next.mid(1);
        }

        values.TD.write().setDouble((double) (next.parse_i32(&ok) * multiplier));
        if (!ok) return 81;

        type = Field_TemperatureAndDewpoint;
        return 0;
    }

    if (type < Field_Trend && length == 5 && field.string_start("A")) {
        Variant::Root
                PALT((double) field.mid(1).toQByteArray().trimmed().toInt(&ok) * 0.01 * 33.86389);
        if (!ok) return 90;
        remap("ZPALT", PALT);
        values.P
              .write()
              .setDouble(altimeterToAbsolute(PALT.read().toDouble(), config.first().elevation));

        type = Field_Altimeter;
        return 0;
    }

    if (type < Field_Trend && length == 5 && field.string_start("Q")) {
        Variant::Root PALT((double) field.mid(1).toQByteArray().trimmed().toInt(&ok));
        if (!ok) return 100;
        remap("ZPALT", PALT);
        values.P
              .write()
              .setDouble(altimeterToAbsolute(PALT.read().toDouble(), config.first().elevation));

        type = Field_Altimeter;
        return 0;
    }

    if (field == "RMK") {
        type = Field_RemarkIdentifier;
        return 0;
    }

    if (field == "NOSIG" || field == "TEMPO") {
        type = Field_Trend;
        return 0;
    }

    if (length == 8 &&
            isAllDigits(field.mid(0, 4)) &&
            (isAllDigits(field.mid(4, 4)) ||
                    (field.mid(4, 2) == "//" && isAllDigits(field.mid(6, 2))))) {
        type = Field_RunwayCondition;
        return 0;
    }
    if (length == 9 &&
            isAllDigits(field.mid(0, 2)) &&
            isAllDigits(field.mid(3, 2)) &&
            (isAllDigits(field.mid(5, 4)) ||
                    (field.mid(6, 2) == "//" && isAllDigits(field.mid(7, 2))))) {
        type = Field_RunwayCondition;
        return 0;
    }

    if (type >= Field_Trend)
        return -1;

    return 1;
}

int AcquireGenericMETAR::processRecord(Util::ByteArray line, double &frameTime)
{
    Q_ASSERT(!config.isEmpty());

    if (line.size() < 3)
        return 1;

    line.string_to_upper();
    auto fields = CSV::acquisitionSplit(line);
    Util::ByteView field;
    bool ok = false;

    if (fields.empty()) return 2;
    if (fields[0] == "METAR")
        fields.pop_front();

    if (fields.empty()) return 3;
    if (fields.front().size() == 4) {
        auto code = fields.front().toString();
        fields.pop_front();
        if (code != instrumentMeta["ICAOStation"].toString()) {
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            instrumentMeta["ICAOStation"].setString(code);
            sourceMetadataUpdated();
        }
    }

    if (fields.empty()) return 4;
    field = fields.front();
    fields.pop_front();
    if (field.size() != 7) return 5;
    if (field[6] != 'Z') return 6;

    qint64 iday = field.mid(0, 2).parse_i64(&ok);
    if (!ok) return 7;
    if (iday < 1 || iday > 31) return 8;
    Variant::Root day(iday);
    remap("DAY", day);
    iday = day.read().toInt64();

    qint64 ihour = field.mid(2, 2).parse_i64(&ok);
    if (!ok) return 9;
    if (ihour < 0 || ihour > 23) return 10;
    Variant::Root hour(ihour);
    remap("HOUR", hour);
    ihour = hour.read().toInt64();

    qint64 iminute = field.mid(4, 2).parse_i64(&ok);
    if (!ok) return 11;
    if (iminute < 0 || iminute > 59) return 12;
    Variant::Root minute(iminute);
    remap("MINUTE", minute);
    iminute = minute.read().toInt64();

    double observationTime = frameTime;
    if (!FP::defined(observationTime))
        observationTime = config.first().timeOrigin;
    if (FP::defined(observationTime)) {
        QDateTime dt(Time::toDateTime(observationTime + 0.5));
        dt.setTime(QTime(0, 0, 0));
        dt.setDate(QDate(dt.date().year(), dt.date().month(), 1));
        observationTime = Time::fromDateTime(dt);

        if (INTEGER::defined(iday))
            observationTime += (double) ((iday - 1) * 86400);
        if (INTEGER::defined(ihour))
            observationTime += (double) (ihour * 3600);
        if (INTEGER::defined(iminute))
            observationTime += (double) (iminute * 60);
    }
    if (!FP::defined(frameTime))
        frameTime = observationTime;

    double observationElapsed = FP::undefined();
    if (FP::defined(lastObservationTime) && FP::defined(observationTime))
        observationElapsed = observationTime - lastObservationTime;
    lastObservationTime = observationTime;

    int fieldType = Field_Begin;
    int errorOffset = 1000;
    ValueData values;
    while (!fields.empty()) {
        int code = processField(fields.front(), fieldType, values);
        fields.pop_front();
        if (code < 0) {
            errorOffset += 1000;
            continue;
        } else if (code > 0) {
            return errorOffset + code;
        }

        if (fieldType < Field_Last)
            fieldType = fieldType + 1;
        errorOffset += 1000;
    }


    if (!FP::defined(frameTime))
        return -1;
    if (FP::defined(lastRecordTime) && frameTime < lastRecordTime)
        return -1;
    if (!config.first().reportEveryRecord &&
            FP::defined(observationElapsed) &&
            observationElapsed == 0.0)
        return -1;

    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;
    if (FP::defined(startTime) && FP::defined(endTime)) {
        if (startTime >= endTime)
            return -1;
    }

    remap("WS", values.WS);
    remap("WD", values.WD);
    remap("ZWSGust", values.ZWSGust);
    remap("ZWDVariability", values.ZWDVariability);
    remap("WZ", values.WZ);
    remap("WX1", values.WX1);
    remap("WX2", values.WX2);
    remap("ZWMOCloud", values.ZWMOCloud);
    remap("T", values.T);
    remap("TD", values.TD);
    remap("P", values.P);
    remap("WI", values.WI);

    if (values.T.read().exists() && values.TD.read().exists()) {
        values.U
              .write()
              .setDouble(Dewpoint::rh(values.T.read().toDouble(), values.TD.read().toDouble()));
    }
    remap("U", values.U);

    quint32 priorContents = priorValues.availableFields();
    if (priorContents == 0) {
        priorValues = values;
        return 0;
    }

    quint32 contents = values.availableFields();
    quint32 nextExclude = priorContents & (~contents);

    /* Bit of decoupling here: the logging egress is aligned on the effective
     * time, so the values we're emitting are actually the previous values
     * we got, while the realtime values are the absolute latest. */

    if (!haveEmittedLogMeta && loggingEgress != NULL && FP::defined(startTime)) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime, priorContents, pendingExclude));
    }
    if ((priorContents & Values_LOGGING) != (contents & Values_LOGGING)) {
        haveEmittedLogMeta = false;
    }
    pendingExclude = nextExclude;

    if (priorContents != contents) {
        haveEmittedRealtimeMeta = false;
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;

        SequenceValue::Transfer meta = buildLogMeta(frameTime, contents, nextExclude);
        Util::append(buildRealtimeMeta(frameTime, contents, nextExclude), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    if (priorContents & Value_Wind) {
        logValue(startTime, endTime, "WS", std::move(priorValues.WS));
        logValue(startTime, endTime, "WD", std::move(priorValues.WD));
    }
    if (priorContents & Value_WindGust) {
        logValue(startTime, endTime, "ZWSGust", std::move(priorValues.ZWSGust));
    }
    if (priorContents & Value_WindVariability) {
        logValue(startTime, endTime, "ZWDVariability", std::move(priorValues.ZWDVariability));
    }
    if (priorContents & Value_WZ) {
        logValue(startTime, endTime, "WZ", std::move(priorValues.WZ));
    }
    if (priorContents & Value_WX) {
        logValue(startTime, endTime, "WX1", std::move(priorValues.WX1));
        logValue(startTime, endTime, "WX2", std::move(priorValues.WX2));
    }
    if (priorContents & Value_WMOCloud) {
        logValue(startTime, endTime, "ZWMOCloud", std::move(priorValues.ZWMOCloud));
    }
    if (priorContents & Value_T) {
        logValue(startTime, endTime, "T", std::move(priorValues.T));
    }
    if (priorContents & Value_TD) {
        logValue(startTime, endTime, "TD", std::move(priorValues.TD));
    }
    if (priorContents & Value_U) {
        logValue(startTime, endTime, "U", std::move(priorValues.U));
    }
    if (priorContents & Value_P) {
        logValue(startTime, endTime, "P", std::move(priorValues.P));
    }
    if (priorContents & Value_WI) {
        logValue(startTime, endTime, "WI", std::move(priorValues.WI));
    }

    logValue(startTime, endTime, "F1", Variant::Root(Variant::Type::Flags));


    if (contents & Value_Wind) {
        realtimeValue(frameTime, "WS", values.WS);
        realtimeValue(frameTime, "WD", values.WD);
    }
    if (contents & Value_WindGust) {
        realtimeValue(frameTime, "ZWSGust", values.ZWSGust);
    }
    if (contents & Value_WindVariability) {
        realtimeValue(frameTime, "ZWDVariability", values.ZWDVariability);
    }
    if (contents & Value_WZ) {
        realtimeValue(frameTime, "WZ", values.WZ);
    }
    if (contents & Value_WX) {
        realtimeValue(frameTime, "WX1", values.WX1);
        realtimeValue(frameTime, "WX2", values.WX2);
    }
    if (contents & Value_WMOCloud) {
        realtimeValue(frameTime, "ZWMOCloud", values.ZWMOCloud);
    }
    if (contents & Value_T) {
        realtimeValue(frameTime, "T", values.T);
    }
    if (contents & Value_TD) {
        realtimeValue(frameTime, "TD", values.TD);
    }
    if (contents & Value_U) {
        realtimeValue(frameTime, "U", values.U);
    }
    if (contents & Value_P) {
        realtimeValue(frameTime, "P", values.P);
    }
    if (contents & Value_WI) {
        realtimeValue(frameTime, "WI", values.WI);
    }
    realtimeValue(frameTime, "F1", Variant::Root(Variant::Type::Flags));

    priorValues = std::move(values);
    return 0;
}

void AcquireGenericMETAR::configurationAdvance(double frameTime)
{
    int oldSize = config.size();
    Q_ASSERT(!config.isEmpty());
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());
    if (oldSize == config.size())
        return;

    configurationChanged();
}

void AcquireGenericMETAR::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
}

void AcquireGenericMETAR::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (FP::defined(frameTime))
        configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Autoprobe succeeded at" << Logging::time(frameTime);

            responseState = RESP_RUN;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            if (FP::defined(frameTime)) {
                timeoutAt(frameTime + config.first().reportInterval * 2.0 + 1.0);
            }

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("AutoprobeWait");
            event(frameTime, QObject::tr("Autoprobe succeeded."), false, info);
        } else if (code > 0) {
            qCDebug(log) << "Autoprobe failed at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            responseState = RESP_WAIT;

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        } else {
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    case RESP_WAIT:
    case RESP_INITIALIZE: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Comms established at" << Logging::time(frameTime);

            responseState = RESP_RUN;
            generalStatusUpdated();

            if (FP::defined(frameTime)) {
                timeoutAt(frameTime + config.first().reportInterval * 2.0 + 1.0);
            }

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("Wait");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else {
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    case RESP_RUN: {
        int code = processRecord(frame, frameTime);
        if (code <= 0) {
            if (FP::defined(frameTime)) {
                timeoutAt(frameTime + config.first().reportInterval * 2.0 + 1.0);
            }
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            responseState = RESP_WAIT;
            discardData(frameTime + 0.5);
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            info.hash("ResponseState").setString("Run");
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    default:
        break;
    }
}

void AcquireGenericMETAR::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    switch (responseState) {
    case RESP_RUN: {
        qCDebug(log) << "Timeout in unpolled mode at" << Logging::time(frameTime);

        responseState = RESP_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        discardData(frameTime + 2.0);
        generalStatusUpdated();

        Variant::Write info = Variant::Write::empty();
        info.hash("ResponseState").setString("Run");
        event(frameTime, QObject::tr("Timeout waiting for report.  Communications dropped."), true,
              info);

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;
    }

    case RESP_AUTOPROBE_WAIT:
        qCDebug(log) << "Timeout waiting for autoprobe records at" << Logging::time(frameTime);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (controlStream != NULL)
            controlStream->resetControl();
        discardData(frameTime + 2.0);
        responseState = RESP_WAIT;

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_INITIALIZE:
        timeoutAt(frameTime + config.first().reportInterval * 2.0 + 1.0);
        responseState = RESP_WAIT;
        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_AUTOPROBE_INITIALIZE:
        timeoutAt(frameTime + config.first().reportInterval * 2.0 + 1.0);
        responseState = RESP_AUTOPROBE_WAIT;
        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    default:
        discardData(frameTime + 0.5);
        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;
    }
}

void AcquireGenericMETAR::discardDataCompleted(double frameTime)
{
    Q_UNUSED(frameTime);
}

void AcquireGenericMETAR::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frameTime);
    Q_UNUSED(frame);
}

AcquisitionInterface::AutoprobeStatus AcquireGenericMETAR::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

Variant::Root AcquireGenericMETAR::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireGenericMETAR::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

void AcquireGenericMETAR::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_RUN:
        /* Already running, just notify that we succeeded. */
        qCDebug(log) << "Interactive autoprobe requested with communications established at"
                     << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

        /* Fall through */
    default:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + config.first().reportInterval * 2.0 + 1.0);
        discardData(time + 0.5);
        responseState = RESP_AUTOPROBE_WAIT;
        generalStatusUpdated();
        break;
    }
}

void AcquireGenericMETAR::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_WAIT;
    discardData(time + 0.5, 1);
    timeoutAt(time + config.first().reportInterval * 4.0 + 2.0);
}

void AcquireGenericMETAR::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + config.first().reportInterval * 2.0 + 1.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to interactive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_INITIALIZE:
    case RESP_AUTOPROBE_WAIT:
        qCDebug(log) << "Promoted from passive autoprobe to interactive acquisition at"
                     << Logging::time(time);

        responseState = RESP_WAIT;
        break;
    }
}

void AcquireGenericMETAR::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + config.first().reportInterval * 2.0 + 1.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from passive autoprobe to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_WAIT;
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireGenericMETAR::getDefaults()
{
    AutomaticDefaults result;
    result.name = "XM$2";
    result.setSerialN81(9600);
    result.autoprobeInitialOrder = 90;
    result.autoprobeLevelPriority = 100;
    return result;
}


ComponentOptions AcquireGenericMETARComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);

    options.add("basetime",
                new ComponentOptionSingleTime(tr("basetime", "name"), tr("File time origin"),
                                              tr("When dealing with files without a full timestamp on each "
                                                 "record, this time is used to reconstruct the full time from "
                                                 "day of month and time of day."), ""));

    return options;
}

ComponentOptions AcquireGenericMETARComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireGenericMETARComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples(true));
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireGenericMETARComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireGenericMETARComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireGenericMETARComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireGenericMETARComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                     const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireGenericMETAR(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireGenericMETARComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireGenericMETAR(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireGenericMETARComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{
    std::unique_ptr<AcquireGenericMETAR> i(new AcquireGenericMETAR(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireGenericMETARComponent::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireGenericMETAR> i(new AcquireGenericMETAR(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
