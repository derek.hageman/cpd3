/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREGENERICMETAR_H
#define ACQUIREGENERICMETAR_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "acquisition/spancheck.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class AcquireGenericMETAR : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Waiting for enough valid records to confirm autoprobe success */
        RESP_AUTOPROBE_WAIT,

        /* Waiting for a valid record to start acquisition */
        RESP_WAIT,

        /* Acquiring data in passive mode */
        RESP_RUN,

        /* Same as wait, but discard the first timeout */
        RESP_INITIALIZE,

        /* For autoprobing */
        RESP_AUTOPROBE_INITIALIZE,
    };
    ResponseState responseState;

    friend class Configuration;

    class Configuration {
        double start;
        double end;
    public:
        double reportInterval;
        double timeOrigin;
        double elevation;
        bool reportEveryRecord;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under,
                      const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    enum {
        Field_Begin = 0,

        Field_Auto,
        Field_WindAbsolute,
        Field_WindVariabiltiy,
        Field_Visiblity,
        Field_RunwayVisualRange,
        Field_WXCode,
        Field_CloudLayer,
        Field_TemperatureAndDewpoint,
        Field_Altimeter,
        Field_Trend,
        Field_RunwayCondition,

        Field_RemarkIdentifier,
        Field_RemarkUnknown,
        Field_WMOCloudCode,
        Field_RemarkSeaLevelPressure,
        Field_RemarkTemperatureAndDewpoint,
        Field_RemarkPrecipitationRate,

        Field_Last,

        Field_RemarkSectionBegin = Field_RemarkUnknown,
    };
    enum {
        Value_Wind = 0x00000001,
        Value_WindGust = 0x00000002,
        Value_WindVariability = 0x00000004,
        Value_WZ = 0x00000008,
        Value_WX = 0x00000010,
        Value_WMOCloud = 0x00000020,
        Value_T = 0x00000040,
        Value_TD = 0x00000080,
        Value_U = 0x00000100,
        Value_P = 0x00000200,
        Value_WI = 0x00000400,

        Values_LOGGING = 0xFFFFFFFF,
    };

    friend struct ValueData;

    struct ValueData {
        CPD3::Data::Variant::Root WD;
        CPD3::Data::Variant::Root WS;
        CPD3::Data::Variant::Root ZWSGust;
        CPD3::Data::Variant::Root ZWDVariability;
        CPD3::Data::Variant::Root WZ;
        CPD3::Data::Variant::Root WX1;
        CPD3::Data::Variant::Root WX2;
        CPD3::Data::Variant::Root ZWMOCloud;
        CPD3::Data::Variant::Root T;
        CPD3::Data::Variant::Root TD;
        CPD3::Data::Variant::Root U;
        CPD3::Data::Variant::Root P;
        CPD3::Data::Variant::Root WI;

        quint32 availableFields() const;
    };

    void setDefaultInvalid();

    CPD3::Data::Variant::Root instrumentMeta;

    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;

    ValueData priorValues;
    double lastObservationTime;
    quint32 pendingExclude;

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name, CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root value);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time,
                                                     quint32 include,
                                                     quint32 exclude) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time,
                                                          quint32 include,
                                                          quint32 exclude) const;

    bool processPresentWeatherCode(CPD3::Util::ByteView field, ValueData &values) const;

    int processField(const CPD3::Util::ByteView &field, int &type, ValueData &values);

    int processRecord(CPD3::Util::ByteArray line, double &frameTime);

    void configurationAdvance(double frameTime);

    void configurationChanged();

public:
    AcquireGenericMETAR(const CPD3::Data::ValueSegment::Transfer &config,
                        const std::string &loggingContext);

    AcquireGenericMETAR(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireGenericMETAR();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    std::size_t dataFrameStart(std::size_t offset,
                               const CPD3::Util::ByteArray &input) const override;

    std::size_t dataFrameEnd(std::size_t start, const CPD3::Util::ByteArray &input) const override;

    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;
};

class AcquireGenericMETARComponent
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_generic_metar"
                              FILE
                              "acquire_generic_metar.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
