/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/processingstage.hxx"
#include "core/component.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return a.getIdentity() == b.getIdentity(); }
}
}

class TestComponent : public QObject {
Q_OBJECT

    ProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component =
                qobject_cast<ProcessingStageComponent *>(ComponentLoader::create("smooth_4plp"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<TimeIntervalSelectionOption *>(options.get("tc")));
        QVERIFY(qobject_cast<TimeIntervalSelectionOption *>(options.get("gap")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("ignore-undefined")));
    }

    void general()
    {
        ComponentOptions options;
        options = component->getOptions();

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName value("brw", "raw", "T_S11");

        filter->incomingData(SequenceValue(value, Variant::Root(1.0), 60.0, 120.0));
        filter->incomingData(SequenceValue(value, Variant::Root(1.0), 120.0, 180.0));
        filter->setEgress(NULL);

        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            filter->serialize(stream);
        }
        filter->signalTerminate();
        QVERIFY(filter->wait(30));
        delete filter;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            filter = component->deserializeGeneralFilter(stream);
            QVERIFY(stream.atEnd());
        }
        QVERIFY(filter != NULL);
        filter->start();
        filter->setEgress(&e);

        filter->incomingData(SequenceValue(value, Variant::Root(1.0), 180.0, 240.0));
        filter->incomingData(SequenceValue(value, Variant::Root(1.0), 240.0, 300.0));
        filter->endData();

        QVERIFY(filter->wait(30));
        delete filter;

        QCOMPARE(e.values(),
                 (SequenceValue::Transfer{SequenceValue(value, Variant::Root(1.0), 60.0, 120.0),
                                          SequenceValue(value, Variant::Root(1.0), 120.0, 180.0),
                                          SequenceValue(value, Variant::Root(1.0), 180.0, 240.0),
                                          SequenceValue(value, Variant::Root(1.0), 240.0, 300.0)}));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
