/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "smoothing/smoothingengine.hxx"

#include "smooth_4plp.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;


QString SmoothFourPoleLowPassComponent::getGeneralSerializationName() const
{ return QString::fromLatin1("smooth_4plp"); }

ComponentOptions SmoothFourPoleLowPassComponent::getOptions()
{
    ComponentOptions options;

    TimeIntervalSelectionOption *tis =
            new TimeIntervalSelectionOption(tr("tc", "name"), tr("The time constant"),
                                            tr("This is the time constant of the filter.  A time constant for a "
                                                       "digital filter is the time required for a step change in values "
                                                       "to reach 63.2% of the final value."),
                                            tr("Three minutes", "default time constant"));
    tis->setAllowZero(false);
    tis->setAllowNegative(false);
    tis->setAllowUndefined(false);
    tis->setDefaultAligned(false);
    options.add("tc", tis);

    tis = new TimeIntervalSelectionOption(tr("gap", "name"), tr("The maximum allowed gap"),
                                          tr("If two values are seperated by this much time then the smoother is "
                                                     "reset.  That is, data separated by this much will be smoothed as "
                                                     "independent runs of data."),
                                          tr("Infinite", "default gap"));
    tis->setAllowZero(true);
    tis->setAllowNegative(false);
    tis->setAllowUndefined(true);
    tis->setDefaultAligned(true);
    options.add("gap", tis);

    options.add("ignore-undefined", new ComponentOptionBoolean(tr("ignore-undefined", "name"),
                                                               tr("Ignore undefined values"),
                                                               tr("If this option is set undefined values are ignored instead of "
                                                                          "forcing a smoother reset."),
                                                               tr("Disabled, undefined values cause a reset",
                                                                  "default ignore undefined mode")));

    return options;
}

QList<ComponentExample> SmoothFourPoleLowPassComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Default", "default example name"),
                                     tr("This smooths all available data with a single pole low pass "
                                                "digital filter with a time constant of three minutes.  "
                                                "Gaps of any size are smoothed over (taken as continuous "
                                                "sequences of data) but undefined values cause the smoother to "
                                                "reset.")));

    options = getOptions();
    qobject_cast<TimeIntervalSelectionOption *>(options.get("tc"))->set(Time::Hour, 1, false);
    examples.append(ComponentExample(options, tr("Alternate time constant", "default example name"),
                                     tr("This smooths data with a digital filter with a time constant "
                                                "of one hour.")));

    return examples;
}

ProcessingStage *SmoothFourPoleLowPassComponent::createGeneralFilterDynamic(const ComponentOptions &options)
{
    bool smoothUndefined = false;
    if (options.isSet("ignore-undefined")) {
        smoothUndefined =
                qobject_cast<ComponentOptionBoolean *>(options.get("ignore-undefined"))->get();
    }

    DynamicTimeInterval *gap;
    if (options.isSet("gap")) {
        gap = qobject_cast<TimeIntervalSelectionOption *>(options.get("gap"))->getInterval();
    } else {
        gap = NULL;
    }

    DynamicTimeInterval *tc;
    if (options.isSet("tc")) {
        tc = qobject_cast<TimeIntervalSelectionOption *>(options.get("tc"))->getInterval();
    } else {
        tc = new DynamicTimeInterval::Constant(Time::Minute, 3, false);
    }

    return SmoothingEngine::createFourPoleLowPassDigitalFilter(tc, gap, smoothUndefined);
}

ProcessingStage *SmoothFourPoleLowPassComponent::createGeneralFilterEditing(double start,
                                                                            double end,
                                                                            const SequenceName::Component &station,
                                                                            const SequenceName::Component &archive,
                                                                            const ValueSegment::Transfer &config)
{
    Q_UNUSED(station);
    Q_UNUSED(archive);

    DynamicTimeInterval *gap = DynamicTimeInterval::fromConfiguration(config, "Gap", start, end);
    DynamicTimeInterval
            *tc = DynamicTimeInterval::fromConfiguration(config, "TimeConstant", start, end);

    bool smoothUndefined = false;
    for (const auto &seg : config) {
        if (!Range::intersects(seg.getStart(), seg.getEnd(), start, end))
            continue;
        if (seg.getValue().getPath("IgnoreUndefined").exists()) {
            smoothUndefined = seg.getValue().getPath("IgnoreUndefined").toBool();
        }
    }

    return SmoothingEngine::createFourPoleLowPassDigitalFilter(tc, gap, smoothUndefined);
}

void SmoothFourPoleLowPassComponent::extendGeneralFilterEditing(double &start,
                                                                double &end,
                                                                const SequenceName::Component &,
                                                                const SequenceName::Component &,
                                                                const ValueSegment::Transfer &config,
                                                                CPD3::Data::Archive::Access *)
{
    if (!FP::defined(start))
        return;

    DynamicTimeInterval
            *tc = DynamicTimeInterval::fromConfiguration(config, "TimeConstant", start, end);
    double tcPoint = tc->apply(start, start) - start;
    if (FP::defined(tcPoint) && tcPoint >= 0.0)
        start -= tcPoint * 5;

    delete tc;
}

ProcessingStage *SmoothFourPoleLowPassComponent::deserializeGeneralFilter(QDataStream &stream)
{
    return SmoothingEngine::deserializeFourPoleLowPassDigitalFilter(stream);
}
