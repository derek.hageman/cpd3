/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIRETSICPC3010_H
#define ACQUIRETSICPC3010_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QHash>
#include <QSet>
#include <QString>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class AcquireTSICPC3010 : public CPD3::Acquisition::FramedInstrument {
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum {
        /* Passive autoprobe initialization, ignorging the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,

        /* Acquiring data in interactive mode, counts read response */
                RESP_INTERACTIVE_RUN_READ_COUNTS,
        /* Acquiring data in interactive mode, condenser temperature */
                RESP_INTERACTIVE_RUN_READ_CONDENSERTEMP,
        /* Acquiring data in interactive mode, saturator temperature */
                RESP_INTERACTIVE_RUN_READ_SATURATORTEMP,
        /* Acquiring data in interactive mode, display concentration */
                RESP_INTERACTIVE_RUN_READ_DISPLAY,
        /* Acquiring data in interactive mode, liquid full status */
                RESP_INTERACTIVE_RUN_READ_LIQUID,
        /* Acquiring data in interactive mode, status */
                RESP_INTERACTIVE_RUN_READ_STATUS,
        /* Acquiring data in interactive mode, vacuum valid */
                RESP_INTERACTIVE_RUN_READ_VACUUM,

        /* Handling an external command */
                RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND,

        /* Starting communications, waiting for initial read counts */
                RESP_INTERACTIVE_START_READ_COUNTS,
        /* Starting communications, reading condenser temperature */
                RESP_INTERACTIVE_START_READ_CONDENSERTEMP,
        /* Starting communications, reading saturator temperature */
                RESP_INTERACTIVE_START_READ_SATURATORTEMP,
        /* Starting communications, issued the "RV" command, which a 3010
         * should respond to, but other counters should not */
                RESP_INTERACTIVE_START_DISAMBIGUATE_RV,
        /* Starting communications, issued the "I" command which should
         * return an error on 3010s */
                RESP_INTERACTIVE_START_DISAMBIGUATE_I,
        /* Starting communications, issued the "C" command which should
         * return an error on 3010s */
                RESP_INTERACTIVE_START_DISAMBIGUATE_C,

        /* Waiting before attempting a communications restart */
                RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
                RESP_INTERACTIVE_INITIALIZE,
    } responseState;
    int autoprobePassiveValidRecords;

    enum CommandType {
        /* Unknown command, so all responses until another command is
         * received are discarded. */
                COMMAND_IGNORED,

        /* D, Read counts (compatibility) */
                COMMAND_D, /* DC, Read counts */
                COMMAND_DC,

        /* R0, Read liquid status */
                COMMAND_R0, /* R1, Read condenser temperature */
                COMMAND_R1, /* R2, Read saturator temperature */
                COMMAND_R2, /* R5, Read ready/notready status */
                COMMAND_R5, /* RV, Read vacuum status */
                COMMAND_RV,

        /* RD, Read display concentration */
                COMMAND_RD,

        /* A generic command responding with a single "OK" */
                COMMAND_OK, /* A command we expect an error from (disambiguation) */
                COMMAND_ERROR,

        /* A specifically invalid command */
                COMMAND_INVALID,
    };

    class Command {
        CommandType type;
        int counter;
        int retry;
    public:
        Command();

        Command(CommandType t, int retry = 0);

        Command(const Command &other);

        Command(const Command &other, int retry);

        Command &operator=(const Command &other);

        inline CommandType getType() const
        { return type; }

        inline int getCounter() const
        { return counter; }

        inline void setCounter(int i)
        { counter = i; }

        inline int getRetry() const
        { return retry; }

        CPD3::Data::Variant::Root stateDescription() const;
    };

    QList<Command> commandQueue;

    enum LogStreamID {
        LogStream_Metadata = 0, LogStream_State,

        LogStream_Counts, LogStream_CondenserTemperature, LogStream_SaturatorTemperature,

        LogStream_TOTAL, LogStream_RecordBaseStart = LogStream_Counts,
    };
    CPD3::Data::StaticMultiplexer loggingMux;
    quint32 streamAge[LogStream_TOTAL];
    double streamTime[LogStream_TOTAL];

    class Configuration {
        double start;
        double end;
    public:
        double flow;
        bool useMeasuredTime;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    CPD3::Data::Variant::Root instrumentMeta;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool forceRealtimeStateEmit;

    qint64 countSeconds;
    qint64 countPulses;

    bool liquidFull;
    bool instrumentReady;
    bool sufficientVacuum;

    std::deque<CPD3::Util::ByteArray> externalCommandQueue;

    void setDefaultInvalid();

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void logValue(double startTime,
                  double endTime,
                  int streamID,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    void emitMetadata(double frameTime);

    void streamAdvance(int streamID, double time);

    void describeState(CPD3::Data::Variant::Write &info, const Command &command = Command()) const;

    void configurationChanged();

    void configurationAdvance(double frameTime);

    int processResponse(const CPD3::Util::ByteView &frame, double frameTime);

    void invalidateLogValues(double frameTime);

public:
    AcquireTSICPC3010(const CPD3::Data::ValueSegment::Transfer &config,
                      const std::string &loggingContext);

    AcquireTSICPC3010(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireTSICPC3010();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    CPD3::Data::Variant::Root getCommands() override;

    virtual CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    virtual void command(const CPD3::Data::Variant::Read &value);

    virtual void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingInstrumentTimeout(double time);

    virtual void discardDataCompleted(double time);
};

class AcquireTSICPC3010Component
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_tsi_cpc3010"
                              FILE
                              "acquire_tsi_cpc3010.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
