/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QRegExp>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "core/environment.hxx"

#include "corr_dilution.hxx"

using namespace CPD3;
using namespace CPD3::Data;

void CorrDilution::handleOptions(const ComponentOptions &options)
{
    restrictedInputs = false;

    if (options.isSet("sample")) {
        defaultSampleFlow = qobject_cast<DynamicInputOption *>(options.get("sample"))->getInput();
    } else {
        defaultSampleFlow = NULL;
    }

    if (options.isSet("dilution")) {
        defaultDilutionFlow =
                qobject_cast<DynamicInputOption *>(options.get("dilution"))->getInput();
    } else {
        defaultDilutionFlow = NULL;
    }

    if (options.isSet("correct")) {
        restrictedInputs = true;

        Processing p;
        p.operateDiluted = qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("correct"))->getOperator();
        p.operateFactor = new DynamicSequenceSelection::None;
        p.operateFlags = new DynamicSequenceSelection::None;

        if (defaultDilutionFlow != NULL)
            p.dilutionFlows.push_back(defaultDilutionFlow->clone());

        if (defaultSampleFlow != NULL)
            p.sampleFlows.push_back(defaultSampleFlow->clone());

        processing.push_back(p);
    }

    if (options.isSet("suffix")) {
        filterSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->get());
    }
}


CorrDilution::CorrDilution()
{ Q_ASSERT(false); }

CorrDilution::CorrDilution(const ComponentOptions &options)
{
    handleOptions(options);
}

CorrDilution::CorrDilution(const ComponentOptions &options,
                           double start,
                           double end,
                           const QList<SequenceName> &inputs)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    handleOptions(options);

    for (QList<SequenceName>::const_iterator unit = inputs.constBegin(), endU = inputs.constEnd();
            unit != endU;
            ++unit) {
        CorrDilution::unhandled(*unit, NULL);
    }
}

CorrDilution::CorrDilution(double start,
                           double end,
                           const SequenceName::Component &station,
                           const SequenceName::Component &archive,
                           const ValueSegment::Transfer &config)
{
    defaultSampleFlow = NULL;
    defaultDilutionFlow = NULL;
    restrictedInputs = true;

    struct ChildData {
        std::unordered_set<Variant::PathElement::HashIndex> dilution;
        std::unordered_set<Variant::PathElement::HashIndex> sample;
    };
    std::unordered_map<Variant::PathElement::HashIndex, ChildData> children;
    for (const auto &add : config) {
        auto h = add.read().toHash();
        for (auto child : h) {
            auto &target = children[child.first];
            Util::merge(child.second["Dilution"].toHash().keys(), target.dilution);
            Util::merge(child.second["Sample"].toHash().keys(), target.sample);
        }
    }
    children.erase(Variant::PathElement::HashIndex());

    for (auto &child : children) {
        child.second.dilution.erase(Variant::PathElement::HashIndex());
        child.second.sample.erase(Variant::PathElement::HashIndex());

        Processing p;

        p.operateDiluted = DynamicSequenceSelection::fromConfiguration(config,
                                                                       QString("%1/CorrectDiluted").arg(
                                                                               QString::fromStdString(
                                                                                       child.first)),
                                                                       start, end);
        p.operateFactor = DynamicSequenceSelection::fromConfiguration(config,
                                                                      QString("%1/Factor").arg(
                                                                              QString::fromStdString(
                                                                                      child.first)),
                                                                      start, end);
        p.operateFlags = DynamicSequenceSelection::fromConfiguration(config,
                                                                     QString("%1/Flags").arg(
                                                                             QString::fromStdString(
                                                                                     child.first)),
                                                                     start, end);

        p.operateDiluted->registerExpected(station, archive);
        p.operateFactor->registerExpected(station, archive);
        p.operateFlags->registerExpected(station, archive);

        for (const auto &f : child.second.dilution) {
            p.dilutionFlows
             .push_back(DynamicInput::fromConfiguration(config, QString("%1/Dilution/%2/Flow").arg(
                     QString::fromStdString(child.first), QString::fromStdString(f)), start, end));
            p.dilutionFlows.back()->registerExpected(station, archive);
        }
        for (const auto &f : child.second.sample) {
            p.sampleFlows
             .push_back(DynamicInput::fromConfiguration(config, QString("%1/Sample/%2/Flow").arg(
                     QString::fromStdString(child.first), QString::fromStdString(f)), start, end));
            p.sampleFlows.back()->registerExpected(station, archive);
        }

        processing.push_back(p);
    }
}


CorrDilution::~CorrDilution()
{
    if (defaultDilutionFlow != NULL)
        delete defaultDilutionFlow;
    if (defaultSampleFlow != NULL)
        delete defaultSampleFlow;

    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        delete p->operateDiluted;
        delete p->operateFactor;
        delete p->operateFlags;

        qDeleteAll(p->dilutionFlows);
        qDeleteAll(p->sampleFlows);
    }
}

void CorrDilution::handleNewProcessing(const SequenceName &unit,
                                       int id,
                                       SegmentProcessingStage::SequenceHandlerControl *control)
{
    Processing *p = &processing[id];

    SequenceName::Set reg;

    p->operateDiluted
     ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->operateDiluted->getAllUnits(), reg);

    p->operateFactor->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->operateFactor->getAllUnits(), reg);

    p->operateFlags->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
    Util::merge(p->operateFlags->getAllUnits(), reg);

    if (!control)
        return;

    for (const auto &n : reg) {
        bool isNew = !control->isHandled(n);
        control->filterUnit(n, id);
        if (isNew)
            registerPossibleInput(n, control, id);
    }
}

void CorrDilution::registerPossibleInput(const SequenceName &unit,
                                         SegmentProcessingStage::SequenceHandlerControl *control,
                                         int filterID)
{
    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        bool usedAsInput = false;

        for (std::vector<DynamicInput *>::const_iterator r = processing[id].dilutionFlows.begin(),
                endR = processing[id].dilutionFlows.end(); r != endR; ++r) {
            if (!(*r)->registerInput(unit))
                continue;
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }

        for (std::vector<DynamicInput *>::const_iterator r = processing[id].sampleFlows.begin(),
                endR = processing[id].sampleFlows.end(); r != endR; ++r) {
            if (!(*r)->registerInput(unit))
                continue;
            if (control != NULL)
                control->inputUnit(unit, id);
            usedAsInput = true;
        }

        if (usedAsInput && id != filterID)
            handleNewProcessing(unit, id, control);
    }

    if (defaultDilutionFlow != NULL && defaultDilutionFlow->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultSampleFlow != NULL && defaultSampleFlow->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
}

bool CorrDilution::initialHandler(const SequenceName &unit,
                                  SegmentProcessingStage::SequenceHandlerControl *control)
{
    registerPossibleInput(unit, control);

    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].operateDiluted->registerInput(unit)) {
            if (control != NULL) {
                control->filterUnit(unit, id);
            }
            handleNewProcessing(unit, id, control);
            return true;
        }
        if (processing[id].operateFactor->registerInput(unit)) {
            if (control != NULL) {
                control->filterUnit(unit, id);
            }
            handleNewProcessing(unit, id, control);
            return true;
        }
        if (processing[id].operateFlags->registerInput(unit)) {
            if (control != NULL) {
                control->filterUnit(unit, id);
            }
            handleNewProcessing(unit, id, control);
            return true;
        }
    }

    return false;
}

void CorrDilution::unhandled(const SequenceName &unit,
                             SegmentProcessingStage::SequenceHandlerControl *control)
{
    if (initialHandler(unit, control))
        return;
    if (restrictedInputs)
        return;

    if (unit.hasFlavor(SequenceName::flavor_cover) || unit.hasFlavor(SequenceName::flavor_stats))
        return;

    if (defaultDilutionFlow == NULL || defaultSampleFlow == NULL)
        return;

    if (!Util::starts_with(unit.getVariable(), "B") &&
            !Util::starts_with(unit.getVariable(), "N") &&
            !Util::starts_with(unit.getVariable(), "X")) {
        return;
    }

    auto suffix = Util::suffix(unit.getVariable(), '_');
    if (suffix.empty())
        return;
    if (!filterSuffixes.empty() && !filterSuffixes.count(suffix))
        return;

    Processing p;

    p.operateDiluted =
            new DynamicSequenceSelection::Match(unit.getStationQString(), unit.getArchiveQString(),
                                                QString("(?:[BX][^n_]*_%1)|(?:[BX][^n][^_]*_%1)|(?:[N](?:(?:ns?)|b|v)?[^mnsbv_]*_%1)")
                                                        .arg(QRegExp::escape(
                                                                QString::fromStdString(suffix))),
                                                unit.getFlavors());
    p.operateDiluted->registerInput(unit);

    p.operateFactor = new DynamicSequenceSelection::None;

    SequenceName flagUnit = unit;
    flagUnit.setVariable("F1_" + suffix);
    p.operateFlags = new DynamicSequenceSelection::Single(flagUnit);
    p.operateFlags->registerInput(unit);
    p.operateFlags->registerInput(flagUnit);

    p.dilutionFlows.push_back(defaultDilutionFlow->clone());
    p.sampleFlows.push_back(defaultSampleFlow->clone());

    int id = (int) processing.size();
    processing.push_back(p);

    initialHandler(unit, control);

    if (!control)
        return;

    SequenceName::Set reg;
    Util::merge(processing[id].operateDiluted->getAllUnits(), reg);
    Util::merge(processing[id].operateFlags->getAllUnits(), reg);
    for (std::vector<DynamicInput *>::const_iterator r = processing[id].dilutionFlows.begin(),
            endR = processing[id].dilutionFlows.end(); r != endR; ++r) {
        Util::merge((*r)->getUsedInputs(), reg);
    }
    for (std::vector<DynamicInput *>::const_iterator r = processing[id].sampleFlows.begin(),
            endR = processing[id].sampleFlows.end(); r != endR; ++r) {
        Util::merge((*r)->getUsedInputs(), reg);
    }
    reg.insert(flagUnit);

    /* Since we have static names for things, do input requests for all of them */
    for (const auto &n : reg) {
        initialHandler(n, control);
    }
}

static const Variant::Flag FLAG_NAME = "Dilution";

void CorrDilution::process(int id, SequenceSegment &data)
{
    Processing *proc = &processing[id];

    for (const auto &i : proc->operateFlags->get(data)) {
        if (!data.exists(i))
            continue;
        auto d = data[i];
        if (!d.exists())
            continue;
        d.applyFlag(FLAG_NAME);
    }

    bool valid = true;
    double totalDilution = 0.0;
    for (std::vector<DynamicInput *>::const_iterator r = processing[id].dilutionFlows.begin(),
            endR = processing[id].dilutionFlows.end(); r != endR; ++r) {
        double add = (*r)->get(data);
        if (!FP::defined(add)) {
            valid = false;
            break;
        }
        totalDilution += add;
    }
    double totalSample = 0.0;
    for (std::vector<DynamicInput *>::const_iterator r = processing[id].sampleFlows.begin(),
            endR = processing[id].sampleFlows.end(); r != endR; ++r) {
        double add = (*r)->get(data);
        if (!FP::defined(add)) {
            valid = false;
            break;
        }
        totalSample += add;
    }

    if (!valid || totalSample <= 0.0 || totalSample <= totalDilution) {
        for (const auto &i : proc->operateDiluted->get(data)) {
            Variant::Composite::invalidate(data[i]);
        }
        return;
    }

    double factor = totalSample / (totalSample - totalDilution);
    for (const auto &i : proc->operateDiluted->get(data)) {
        if (!data.exists(i))
            continue;
        Variant::Composite::applyInplace(data[i], [=](Variant::Write &d) {
            double v = d.toDouble();
            if (FP::defined(v))
                d.setReal(v * factor);
            else
                Variant::Composite::invalidate(d);
        });
    }

    for (const auto &i : proc->operateFactor->get(data)) {
        data[i].setDouble(factor);
    }
}

void CorrDilution::processMeta(int id, SequenceSegment &data)
{
    Processing *p = &processing[id];

    Variant::Root meta;

    meta["By"].setString("corr_dilution");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    for (auto r : processing[id].dilutionFlows) {
        meta["Parameters"].hash("DilutionQ") = r->describe(data);
    }
    for (auto r : processing[id].sampleFlows) {
        meta["Parameters"].hash("SampleQ") = r->describe(data);
    }

    for (auto i : p->operateDiluted->get(data)) {
        i.setMeta();
        if (!data.exists(i))
            continue;
        data[i].metadata("Processing").toArray().after_back().set(meta);
        clearPropagatedSmoothing(data[i].metadata("Smoothing"));
    }

    for (auto i : p->operateFactor->get(data)) {
        i.setMeta();
        if (!data[i].exists()) {
            data[i].metadataReal("Description").setString("Dilution factor");
            data[i].metadataReal("Format").setString("00.000");
        }
        data[i].metadata("Processing").toArray().after_back().set(meta);
    }

    for (auto i : p->operateFlags->get(data)) {
        i.setMeta();
        if (!data.exists(i))
            continue;
        auto flagMeta = data[i].metadataSingleFlag(FLAG_NAME);
        flagMeta.hash("Origin").toArray().after_back().setString("corr_dilution");
        flagMeta.hash("Bits").setInt64(0x0800);
        flagMeta.hash("Description").setString("Dilution correction applied");
    }
}


SequenceName::Set CorrDilution::requestedInputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        Util::merge(p->operateDiluted->getAllUnits(), out);
        Util::merge(p->operateFlags->getAllUnits(), out);
        for (std::vector<DynamicInput *>::const_iterator r = p->dilutionFlows.begin(),
                endR = p->dilutionFlows.end(); r != endR; ++r) {
            Util::merge((*r)->getUsedInputs(), out);
        }
        for (std::vector<DynamicInput *>::const_iterator r = p->sampleFlows.begin(),
                endR = p->sampleFlows.end(); r != endR; ++r) {
            Util::merge((*r)->getUsedInputs(), out);
        }
    }
    return out;
}

QSet<double> CorrDilution::metadataBreaks(int id)
{
    Processing *p = &processing[id];
    return p->operateDiluted->getChangedPoints() |
            p->operateFactor->getChangedPoints() |
            p->operateFlags->getChangedPoints();
}

template<typename T>
static QDataStream &operator<<(QDataStream &stream, const std::vector<T> &input)
{
    stream << (quint32) input.size();
    for (typename std::vector<T>::const_iterator i = input.begin(), end = input.end();
            i != end;
            ++i) {
        stream << *i;
    }
    return stream;
}

template<typename T>
static QDataStream &operator>>(QDataStream &stream, std::vector<T> &output)
{
    output.clear();
    quint32 n = 0;
    stream >> n;
    output.reserve(n);
    for (quint32 i = 0; i < n; i++) {
        T v;
        stream >> v;
        output.push_back(v);
    }
    return stream;
}


CorrDilution::CorrDilution(QDataStream &stream)
{
    stream >> defaultDilutionFlow;
    stream >> defaultSampleFlow;
    stream >> filterSuffixes;
    stream >> restrictedInputs;

    quint32 n;
    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        Processing p;
        stream >> p.dilutionFlows;
        stream >> p.sampleFlows;
        stream >> p.operateDiluted;
        stream >> p.operateFactor;
        stream >> p.operateFlags;

        processing.push_back(p);
    }
}

void CorrDilution::serialize(QDataStream &stream)
{
    stream << defaultDilutionFlow;
    stream << defaultSampleFlow;
    stream << filterSuffixes;
    stream << restrictedInputs;

    quint32 n = (quint32) processing.size();
    stream << n;
    for (int i = 0; i < (int) n; i++) {
        stream << processing[i].dilutionFlows;
        stream << processing[i].sampleFlows;
        stream << processing[i].operateDiluted;
        stream << processing[i].operateFactor;
        stream << processing[i].operateFlags;
    }
}


QString CorrDilutionComponent::getBasicSerializationName() const
{ return QString::fromLatin1("corr_dilution"); }

ComponentOptions CorrDilutionComponent::getOptions()
{
    ComponentOptions options;

    options.add("correct", new DynamicSequenceSelectionOption(tr("correct", "name"),
                                                              tr("Variables to correct"),
                                                              tr("These are the variables to apply the correction to."),
                                                              tr("All counts, scatterings, extinctions, and absorptions")));

    options.add("suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments", "name"),
                                                                 tr("Instrument suffixes to correct"),
                                                                 tr("These are the instrument suffixes to correct.  "
                                                                    "For example S11 would usually specifies the reference nephelometer.  "
                                                                    "This option is mutually exclusive with manual variable specification."),
                                                                 tr("All instrument suffixes")));
    options.exclude("correct", "suffix");
    options.exclude("suffix", "correct");

    options.add("sample", new DynamicInputOption(tr("sample", "name"), tr("Sample flow"),
                                                 tr("This is the sample flow rate to use.  The dilution correction "
                                                    "factor is equal to the total flow (the sum of all sample and "
                                                    "dilution flows) divided by the sum of the sample flows."),
                                                 QString()));
    options.add("dilution", new DynamicInputOption(tr("dilution", "name"), tr("Dilution flow"),
                                                   tr("This is the dilution flow rate to use.  The dilution correction "
                                                      "factor is equal to the total flow (the sum of all sample and "
                                                      "dilution flows) divided by the sum of the sample flows."),
                                                   QString()));

    return options;
}

QList<ComponentExample> CorrDilutionComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")))->add("S11");
    (qobject_cast<DynamicInputOption *>(options.get("sample")))->set(30.0);
    (qobject_cast<DynamicInputOption *>(options.get("dilution")))->set(10.0);
    examples.append(ComponentExample(options, tr("Single instrument with manual settings"),
                                     tr("This will correct S11 parameters using a fixed sample flow "
                                        "of 30 lpm and a fixed dilution flow of 10 lpm.")));

    return examples;
}

SegmentProcessingStage *CorrDilutionComponent::createBasicFilterDynamic(const ComponentOptions &options)
{
    return new CorrDilution(options);
}

SegmentProcessingStage *CorrDilutionComponent::createBasicFilterPredefined(const ComponentOptions &options,
                                                                           double start,
                                                                           double end,
                                                                           const QList<
                                                                                   SequenceName> &inputs)
{
    return new CorrDilution(options, start, end, inputs);
}

SegmentProcessingStage *CorrDilutionComponent::createBasicFilterEditing(double start,
                                                                        double end,
                                                                        const SequenceName::Component &station,
                                                                        const SequenceName::Component &archive,
                                                                        const ValueSegment::Transfer &config)
{
    return new CorrDilution(start, end, station, archive, config);
}

SegmentProcessingStage *CorrDilutionComponent::deserializeBasicFilter(QDataStream &stream)
{
    return new CorrDilution(stream);
}
