/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CORRDILUTION_H
#define CORRDILUTION_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QSet>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/dynamicinput.hxx"

class CorrDilution : public CPD3::Data::SegmentProcessingStage {
    class Processing {
    public:
        std::vector<CPD3::Data::DynamicInput *> dilutionFlows;
        std::vector<CPD3::Data::DynamicInput *> sampleFlows;

        CPD3::Data::DynamicSequenceSelection *operateDiluted;
        CPD3::Data::DynamicSequenceSelection *operateFlags;
        CPD3::Data::DynamicSequenceSelection *operateFactor;

        Processing()
                : dilutionFlows(),
                  sampleFlows(),
                  operateDiluted(NULL),
                  operateFlags(NULL),
                  operateFactor(NULL)
        { }
    };

    CPD3::Data::DynamicInput *defaultDilutionFlow;
    CPD3::Data::DynamicInput *defaultSampleFlow;
    CPD3::Data::SequenceName::ComponentSet filterSuffixes;

    bool restrictedInputs;

    void handleOptions(const CPD3::ComponentOptions &options);

    std::vector<Processing> processing;

    void handleNewProcessing(const CPD3::Data::SequenceName &unit,
                             int id,
                             CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control);

    void registerPossibleInput(const CPD3::Data::SequenceName &unit,
                               CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                               int filterID = -1);

    bool initialHandler(const CPD3::Data::SequenceName &unit,
                        CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control);

public:
    CorrDilution();

    CorrDilution(const CPD3::ComponentOptions &options);

    CorrDilution(const CPD3::ComponentOptions &options,
                 double start, double end, const QList<CPD3::Data::SequenceName> &inputs);

    CorrDilution(double start,
                 double end,
                 const CPD3::Data::SequenceName::Component &station,
                 const CPD3::Data::SequenceName::Component &archive,
                 const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CorrDilution();

    void unhandled(const CPD3::Data::SequenceName &unit,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    QSet<double> metadataBreaks(int id) override;

    CorrDilution(QDataStream &stream);

    void serialize(QDataStream &stream) override;
};

class CorrDilutionComponent
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.corr_dilution"
                              FILE
                              "corr_dilution.json")
public:
    virtual QString getBasicSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Data::SegmentProcessingStage
            *createBasicFilterDynamic(const CPD3::ComponentOptions &options);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined
            (const CPD3::ComponentOptions &options,
             double start,
             double end, const QList<CPD3::Data::SequenceName> &inputs);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                         double end,
                                                                         const CPD3::Data::SequenceName::Component &station,
                                                                         const CPD3::Data::SequenceName::Component &archive,
                                                                         const CPD3::Data::ValueSegment::Transfer &config);

    virtual CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream);
};

#endif
