/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    class ID {
    public:
        ID() : filter(), input()
        { }

        ID(const QSet<SequenceName> &setFilter, const QSet<SequenceName> &setInput) : filter(
                setFilter), input(setInput)
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }

        QSet<SequenceName> filter;
        QSet<SequenceName> input;
    };

    QHash<int, ID> contents;

    virtual void filterUnit(const SequenceName &unit, int targetID)
    {
        contents[targetID].input.remove(unit);
        contents[targetID].filter.insert(unit);
    }

    virtual void inputUnit(const SequenceName &unit, int targetID)
    {
        if (contents[targetID].filter.contains(unit))
            return;
        contents[targetID].input.insert(unit);
    }

    virtual bool isHandled(const SequenceName &unit)
    {
        for (QHash<int, ID>::const_iterator it = contents.constBegin();
                it != contents.constEnd();
                ++it) {
            if (it.value().filter.contains(unit))
                return true;
            if (it.value().input.contains(unit))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("corr_dilution"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("correct")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("sample")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("dilution")));

        QVERIFY(options.excluded().value("correct").contains("suffix"));
        QVERIFY(options.excluded().value("suffix").contains("correct"));
    }

    void dynamicDefaults()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<DynamicInputOption *>(options.get("sample"))->set(30.0);
        qobject_cast<DynamicInputOption *>(options.get("dilution"))->set(15.0);
        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        SegmentProcessingStage *filter2;
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "Vx_S11q"), &controller);
        QVERIFY(controller.contents.isEmpty());
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2 != NULL);
            filter2->unhandled(SequenceName("brw", "raw", "Vx_S11q"), &controller);
            QVERIFY(controller.contents.isEmpty());
            delete filter2;
        }

        SequenceName F1_S11_1("brw", "raw", "F1_S11");
        SequenceName BsB_S11_1("brw", "raw", "BsB_S11");
        SequenceName BsG_S11_1("brw", "raw", "BsG_S11");
        SequenceName F1_S11_2("mlo", "raw", "F1_S11");
        SequenceName BsB_S11_2("mlo", "raw", "BsB_S11");
        SequenceName Q_S11_2("mlo", "raw", "Q_S11");
        SequenceName F1_S12_1("brw", "raw", "F1_S12");
        SequenceName BsB_S12_1("brw", "raw", "BsB_S12");

        filter->unhandled(BsB_S11_1, &controller);
        filter->unhandled(BsG_S11_1, &controller);
        filter->unhandled(F1_S11_1, &controller);
        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << BsB_S11_1 << BsG_S11_1 << F1_S11_1,
                                QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int S11_1 = idL.at(0);

        filter->unhandled(BsB_S11_2, &controller);
        QCOMPARE(controller.contents.size(), 2);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << BsB_S11_2 << F1_S11_2,
                                                 QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int S11_2 = idL.at(0);

        filter->unhandled(F1_S12_1, &controller);
        filter->unhandled(BsB_S12_1, &controller);
        QCOMPARE(controller.contents.size(), 3);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << BsB_S12_1 << F1_S12_1,
                                                 QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int S12_1 = idL.at(0);

        data.setStart(15.0);
        data.setEnd(16.0);

        data.setValue(BsB_S11_1, Variant::Root(12.0));
        data.setValue(BsG_S11_1, Variant::Root(15.0));
        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        filter->process(S11_1, data);
        QCOMPARE(data.value(BsB_S11_1).toDouble(), 24.0);
        QCOMPARE(data.value(BsG_S11_1).toDouble(), 30.0);
        QVERIFY(data.value(F1_S11_1).testFlag("Dilution"));

        {
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
            }
            QVERIFY(filter2 != NULL);
            data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
            data.setValue(BsB_S11_1, Variant::Root(12.0));
            data.setValue(BsG_S11_1, Variant::Root(15.0));
            data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
            filter2->process(S11_1, data);
            QCOMPARE(data.value(BsB_S11_1).toDouble(), 24.0);
            QCOMPARE(data.value(BsG_S11_1).toDouble(), 30.0);
            QVERIFY(data.value(F1_S11_1).testFlag("Dilution"));
            delete filter2;
        }

        data.setValue(BsB_S11_1, Variant::Root(10.0));
        data.setValue(BsG_S11_1, Variant::Root(15.0));
        data.setValue(F1_S11_1, Variant::Root(Variant::Type::Flags));
        filter->process(S11_1, data);
        QCOMPARE(data.value(BsB_S11_1).toDouble(), 20.0);
        QCOMPARE(data.value(BsG_S11_1).toDouble(), 30.0);
        QVERIFY(data.value(F1_S11_1).testFlag("Dilution"));

        data.setValue(F1_S11_2, Variant::Root(Variant::Type::Flags));
        data.setValue(BsB_S11_2, Variant::Root(20.0));
        filter->process(S11_2, data);
        QCOMPARE(data.value(BsB_S11_2).toDouble(), 40.0);
        QVERIFY(data.value(F1_S11_1).testFlag("Dilution"));

        data.setValue(F1_S12_1, Variant::Root(Variant::Type::Flags));
        data.setValue(BsB_S12_1, Variant::Root(40.0));
        filter->process(S12_1, data);
        QCOMPARE(data.value(BsB_S12_1).toDouble(), 80.0);
        QVERIFY(data.value(F1_S12_1).testFlag("Dilution"));

        data.setValue(F1_S11_1.toMeta(), Variant::Root(Variant::Type::MetadataFlags));
        data.setValue(BsG_S11_1.toMeta(), Variant::Root(Variant::Type::MetadataReal));
        data.setValue(BsB_S11_1.toMeta(), Variant::Root(Variant::Type::MetadataReal));

        filter->processMeta(S11_1, data);
        QVERIFY(data.value(F1_S11_1.toMeta()).metadataSingleFlag("Dilution").exists());
        QCOMPARE(data.value(BsB_S11_1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data.value(BsG_S11_1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);

        delete filter;
    }

    void dynamicOptions()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<DynamicInputOption *>(options.get("sample"))->set(
                SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("Q_Q12")),
                Calibration());
        qobject_cast<DynamicInputOption *>(options.get("dilution"))->set(
                SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("Q_Q11")),
                Calibration());
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("correct"))->set("", "", "I.*");
        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "Q_A11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "T_S11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BsG_S11"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName Q_Q11("brw", "raw", "Q_Q11");
        SequenceName Q_Q12("brw", "raw", "Q_Q12");
        SequenceName I1_S11("brw", "raw", "I1_S11");
        SequenceName I2_S11("brw", "raw", "I2_S11");
        filter->unhandled(Q_Q11, &controller);
        filter->unhandled(I1_S11, &controller);
        filter->unhandled(I2_S11, &controller);
        filter->unhandled(Q_Q12, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << I1_S11 << I2_S11,
                                                 QSet<SequenceName>() << Q_Q11 << Q_Q12));
        QCOMPARE(idL.size(), 1);
        int id = idL.at(0);

        data.setValue(Q_Q11, Variant::Root(1.0));
        data.setValue(Q_Q12, Variant::Root(2.0));
        data.setValue(I1_S11, Variant::Root(10.0));
        data.setValue(I2_S11, Variant::Root(20.0));
        filter->process(id, data);
        QCOMPARE(data.value(I1_S11).toDouble(), 20.0);
        QCOMPARE(data.value(I2_S11).toDouble(), 40.0);

        delete filter;


        options = component->getOptions();
        qobject_cast<DynamicInputOption *>(options.get("sample"))->set(30.0);
        qobject_cast<DynamicInputOption *>(options.get("dilution"))->set(20.0);
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->add("S11");
        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        filter->unhandled(SequenceName("brw", "raw", "BsG_S12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "F1_S12"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName F1_S11("brw", "raw", "F1_S11");
        SequenceName BsG_S11("brw", "raw", "BsG_S11");
        filter->unhandled(F1_S11, &controller);
        filter->unhandled(BsG_S11, &controller);
        filter->unhandled(SequenceName("brw", "raw", "BsR_S12"), &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << F1_S11 << BsG_S11,
                                                 QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        data.setValue(BsG_S11, Variant::Root(10.0));
        data.setValue(F1_S11, Variant::Root(Variant::Type::Flags));
        filter->process(id, data);
        QCOMPARE(data.value(BsG_S11).toDouble(), 30.0);
        QVERIFY(data.value(F1_S11).testFlag("Dilution"));

        delete filter;
    }

    void predefined()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<DynamicInputOption *>(options.get("sample"))->set(30.0);
        qobject_cast<DynamicInputOption *>(options.get("dilution"))->set(15.0);

        SequenceName F1_S11("brw", "raw", "F1_S11");
        SequenceName BsG_S11("brw", "raw", "BsG_S11");
        QList<SequenceName> input;
        input << BsG_S11;

        SegmentProcessingStage *filter =
                component->createBasicFilterPredefined(options, FP::undefined(), FP::undefined(),
                                                       input);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{BsG_S11, F1_S11}));

        delete filter;
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Main/CorrectDiluted"] = "::Bs[BGR]_S11:=";
        cv["Main/Flags"] = "::F1_S11:=";
        cv["Main/Sample/Neph/Flow"] = "::Q_Q12:=";
        cv["Main/Sample/PSAP/Flow"] = 1.0;
        cv["Main/Dilution/Main/Flow"] = "::Q_Q11:=";
        cv["Alternate/CorrectDiluted"] = "::Bs[BGR]_S12:=";
        cv["Alternate/Factor"] = "::ZFACTOR_S12:=";
        cv["Alternate/Sample/Main/Flow"] = 30.0;
        cv["Alternate/Dilution/Main/Flow"] = 20.0;
        config.emplace_back(FP::undefined(), 13.0, cv);
        cv.write().setEmpty();
        cv["Main/CorrectDiluted"] = "::BsG_S11:=";
        cv["Main/Sample/Neph/Flow"] = "::Q_Q12:=";
        cv["Main/Sample/PSAP/Flow"] = 2.0;
        cv["Main/Dilution/Main/Flow"] = "::Q_Q11:=";
        config.emplace_back(13.0, FP::undefined(), cv);

        SegmentProcessingStage
                *filter = component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config);
        QVERIFY(filter != NULL);
        TestController controller;

        SequenceName F1_S11("brw", "raw", "F1_S11");
        SequenceName BsB_S11("brw", "raw", "BsB_S11");
        SequenceName BsG_S11("brw", "raw", "BsG_S11");
        SequenceName BsR_S11("brw", "raw", "BsR_S11");
        SequenceName BsR_S12("brw", "raw", "BsR_S12");
        SequenceName ZFACTOR_S12("brw", "raw", "ZFACTOR_S12");
        SequenceName Q_Q12("brw", "raw", "Q_Q12");
        SequenceName Q_Q11("brw", "raw", "Q_Q11");

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{F1_S11, BsG_S11, Q_Q12, Q_Q11}));

        filter->unhandled(Q_Q12, &controller);
        filter->unhandled(F1_S11, &controller);
        filter->unhandled(BsB_S11, &controller);
        filter->unhandled(BsG_S11, &controller);
        filter->unhandled(BsR_S11, &controller);
        filter->unhandled(BsR_S12, &controller);
        filter->unhandled(SequenceName("brw", "raw", "F1_S12"), &controller);
        filter->unhandled(Q_Q11, &controller);

        QList<int> idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                                          BsB_S11 <<
                                                                                          BsG_S11 <<
                                                                                          BsR_S11 << F1_S11,
                                                                     QSet<SequenceName>() <<
                                                                                          Q_Q12 <<
                                                                                          Q_Q11));
        QCOMPARE(idL.size(), 1);
        int S11 = idL.at(0);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << BsR_S12 << ZFACTOR_S12,
                                                 QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int S12 = idL.at(0);

        QCOMPARE(filter->metadataBreaks(S11), QSet<double>() << 13.0);
        QCOMPARE(filter->metadataBreaks(S12), QSet<double>() << 13.0);

        SequenceSegment data1;
        data1.setStart(12.0);
        data1.setEnd(13.0);

        data1.setValue(BsB_S11, Variant::Root());
        data1.setValue(BsG_S11, Variant::Root(1.0));
        data1.setValue(BsR_S11, Variant::Root(2.0));
        data1.setValue(Q_Q12, Variant::Root(29.0));
        data1.setValue(Q_Q11, Variant::Root(15.0));
        data1.setValue(F1_S11, Variant::Root(Variant::Type::Flags));
        filter->process(S11, data1);
        QVERIFY(!data1.value(BsB_S11).exists());
        QCOMPARE(data1.value(BsG_S11).toDouble(), 2.0);
        QCOMPARE(data1.value(BsR_S11).toDouble(), 4.0);
        QVERIFY(data1.value(F1_S11).testFlag("Dilution"));

        data1.setValue(BsR_S12, Variant::Root(4.0));
        filter->process(S12, data1);
        QCOMPARE(data1.value(BsR_S12).toDouble(), 12.0);
        QCOMPARE(data1.value(ZFACTOR_S12).toDouble(), 3.0);

        data1.setValue(F1_S11.toMeta(), Variant::Root(Variant::Type::MetadataFlags));
        data1.setValue(BsB_S11.toMeta(), Variant::Root(Variant::Type::MetadataReal));
        data1.setValue(BsG_S11.toMeta(), Variant::Root(Variant::Type::MetadataReal));
        data1.setValue(BsR_S11.toMeta(), Variant::Root(Variant::Type::MetadataReal));

        filter->processMeta(S11, data1);
        QCOMPARE(data1.value(BsB_S11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data1.value(BsG_S11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data1.value(BsR_S11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QVERIFY(data1.value(F1_S11.toMeta()).metadataSingleFlag("Dilution").exists());

        data1.setValue(BsR_S12.toMeta(), Variant::Root(Variant::Type::MetadataReal));

        filter->processMeta(S12, data1);
        QCOMPARE(data1.value(BsR_S12.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);


        SequenceSegment data2;
        data2.setStart(13.0);
        data2.setEnd(15.0);

        data2.setValue(BsB_S11, Variant::Root(4.0));
        data2.setValue(BsG_S11, Variant::Root(5.0));
        data2.setValue(BsR_S11, Variant::Root(6.0));
        data2.setValue(Q_Q12, Variant::Root(28.0));
        data2.setValue(Q_Q11, Variant::Root(15.0));
        data2.setValue(F1_S11, Variant::Root(Variant::Type::Flags));
        filter->process(S11, data2);
        QCOMPARE(data2.value(BsB_S11).toDouble(), 4.0);
        QCOMPARE(data2.value(BsG_S11).toDouble(), 10.0);
        QCOMPARE(data2.value(BsR_S11).toDouble(), 6.0);
        QVERIFY(!data2.value(F1_S11).testFlag("Dilution"));

        data2.setValue(BsR_S12, Variant::Root(3.0));
        filter->process(S12, data2);
        QCOMPARE(data2.value(BsR_S12).toDouble(), 3.0);

        data2.setValue(F1_S11.toMeta(), Variant::Root());
        data2.setValue(BsG_S11.toMeta(), Variant::Root(Variant::Type::MetadataReal));

        filter->processMeta(S11, data2);
        QVERIFY(!data2.value(BsB_S11.toMeta()).exists());
        QCOMPARE(data2.value(BsG_S11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QVERIFY(!data2.value(BsR_S11.toMeta()).exists());
        QVERIFY(!data2.value(F1_S11.toMeta()).exists());

        filter->processMeta(S12, data2);
        QVERIFY(!data2.value(BsR_S12.toMeta()).exists());

        data2.setValue(BsB_S11, Variant::Root(4.0));
        data2.setValue(BsG_S11, Variant::Root(5.0));
        data2.setValue(BsR_S11, Variant::Root(6.0));
        data2.setValue(Q_Q12, Variant::Root());
        data2.setValue(Q_Q11, Variant::Root(15.0));
        filter->process(S11, data2);
        QCOMPARE(data2.value(BsB_S11).toDouble(), 4.0);
        QVERIFY(!FP::defined(data2.value(BsG_S11).toReal()));
        QCOMPARE(data2.value(BsR_S11).toDouble(), 6.0);

        data2.setValue(BsB_S11, Variant::Root(4.0));
        data2.setValue(BsG_S11, Variant::Root(5.0));
        data2.setValue(BsR_S11, Variant::Root(6.0));
        data2.setValue(Q_Q12, Variant::Root(28.0));
        data2.setValue(Q_Q11, Variant::Root());
        filter->process(S11, data2);
        QCOMPARE(data2.value(BsB_S11).toDouble(), 4.0);
        QVERIFY(!FP::defined(data2.value(BsG_S11).toReal()));
        QCOMPARE(data2.value(BsR_S11).toDouble(), 6.0);

        delete filter;
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
