/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CALCRADIATION_HXX
#define CALCRADIATION_HXX

#include "core/first.hxx"

#include <vector>
#include <memory>
#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

enum class Calculation {
    Divide, Multiply, Pyrgeometer,
};


class CalcRadiation : public CPD3::Data::SegmentProcessingStage {
    struct Processing {
        std::unique_ptr<CPD3::Data::DynamicSequenceSelection> operateOutput;

        std::unique_ptr<CPD3::Data::DynamicPrimitive<Calculation>> calculation;

        std::unique_ptr<CPD3::Data::DynamicInput> inputSensor;
        std::unique_ptr<CPD3::Data::DynamicInput> inputFactor;
        std::unique_ptr<CPD3::Data::DynamicInput> inputSigma;
        std::unique_ptr<CPD3::Data::DynamicInput> inputE;
        std::unique_ptr<CPD3::Data::DynamicInput> inputDCF;
        std::unique_ptr<CPD3::Data::DynamicInput> inputCaseTemperature;
        std::unique_ptr<CPD3::Data::DynamicInput> inputDomeTemperature;
    };
    std::vector<Processing> processing;

    std::unique_ptr<CPD3::Data::DynamicPrimitive<Calculation>> defaultCalculation;
    std::unique_ptr<CPD3::Data::DynamicInput> defaultFactor;
    std::unique_ptr<CPD3::Data::DynamicInput> defaultSigma;
    std::unique_ptr<CPD3::Data::DynamicInput> defaultE;
    std::unique_ptr<CPD3::Data::DynamicInput> defaultDCF;
    std::unique_ptr<CPD3::Data::DynamicInput> defaultCaseTemperature;
    std::unique_ptr<CPD3::Data::DynamicInput> defaultDomeTemperature;
    CPD3::Data::SequenceName::ComponentSet filterSuffixes;

    bool restrictedInputs;

    static double calculate(Processing &proc, CPD3::Data::SequenceSegment &data);

    void handleNewProcessing(const CPD3::Data::SequenceName &name,
                             std::size_t id,
                             CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control);

    void registerPossibleInput(const CPD3::Data::SequenceName &name,
                               CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control,
                               std::size_t filterID = static_cast<std::size_t>(-1));

public:
    CalcRadiation() = delete;

    CalcRadiation(const CPD3::ComponentOptions &options);

    CalcRadiation(const CPD3::ComponentOptions &options,
                  double start,
                  double end,
                  const QList<CPD3::Data::SequenceName> &inputs);

    CalcRadiation(double start,
                  double end,
                  const CPD3::Data::SequenceName::Component &station,
                  const CPD3::Data::SequenceName::Component &archive,
                  const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CalcRadiation();

    void unhandled(const CPD3::Data::SequenceName &name,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;

    QSet<double> metadataBreaks(int id) override;

    CalcRadiation(QDataStream &stream);

    void serialize(QDataStream &stream) override;
};

class CalcRadiationComponent
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.calc_radiation"
                              FILE
                              "calc_radiation.json")
public:
    QString getBasicSerializationName() const override;

    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    CPD3::Data::SegmentProcessingStage *createBasicFilterDynamic(const CPD3::ComponentOptions &options) override;

    CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined(const CPD3::ComponentOptions &options,
                                                                    double start,
                                                                    double end,
                                                                    const QList<
                                                                            CPD3::Data::SequenceName> &inputs) override;

    CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                 double end,
                                                                 const CPD3::Data::SequenceName::Component &station,
                                                                 const CPD3::Data::SequenceName::Component &archive,
                                                                 const CPD3::Data::ValueSegment::Transfer &config) override;

    CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream) override;
};

#endif
