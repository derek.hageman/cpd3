/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cmath>
#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    struct ID {
        SequenceName::Set filter;
        SequenceName::Set input;

        ID() = default;

        ID(SequenceName::Set filter, SequenceName::Set input) : filter(std::move(filter)),
                                                                input(std::move(input))
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }
    };

    std::unordered_map<int, ID> contents;

    int find(const SequenceName::Set &filter, const SequenceName::Set &input = {}) const
    {
        for (const auto &check : contents) {
            if (check.second.filter != filter)
                continue;
            if (check.second.input != input)
                continue;
            return check.first;
        }
        return -1;
    }

    void filterUnit(const SequenceName &name, int targetID) override
    {
        contents[targetID].input.erase(name);
        contents[targetID].filter.insert(name);
    }

    void inputUnit(const SequenceName &name, int targetID) override
    {
        if (contents[targetID].filter.count(name))
            return;
        contents[targetID].input.insert(name);
    }

    bool isHandled(const SequenceName &name) override
    {
        for (const auto &check : contents) {
            if (check.second.filter.count(name))
                return true;
            if (check.second.input.count(name))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("calc_radiation"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("input")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("factor")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("sigma")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("e")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("dcf")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("case")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("dome")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("output")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("type")));
    }

    void dynamicDefaults()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("type"))->set("pyrgeometer");
        qobject_cast<DynamicInputOption *>(options.get("factor"))->set(0.5);
        qobject_cast<DynamicInputOption *>(options.get("dcf"))->set(0.1);
        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterDynamic(options));
        QVERIFY(filter.get());
        TestController controller;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "Q_X1"), &controller);
        QVERIFY(controller.contents.empty());
        {
            std::unique_ptr<SegmentProcessingStage> filter2;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2.reset(component->deserializeBasicFilter(stream));
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2.get());
            filter2->unhandled(SequenceName("brw", "raw", "Q_X1"), &controller);
            QVERIFY(controller.contents.empty());
        }

        SequenceName V_R1_1("brw", "raw", "V_R1");
        SequenceName R_R1_1("brw", "raw", "R_R1");
        SequenceName T1_R1_1("brw", "raw", "T1_R1");
        SequenceName T2_R1_1("brw", "raw", "T2_R1");
        SequenceName V_R1_2("sgp", "raw", "V_R1");
        SequenceName R_R1_2("sgp", "raw", "R_R1");
        SequenceName T1_R1_2("sgp", "raw", "T1_R1");
        SequenceName T2_R1_2("sgp", "raw", "T2_R1");
        SequenceName V_RM1_1("brw", "raw", "V1_RM1");
        SequenceName R_RM1_1("brw", "raw", "R1_RM1");
        SequenceName T1_RM1_1("brw", "raw", "T1_RM1");
        SequenceName T2_RM1_1("brw", "raw", "T2_RM1");

        filter->unhandled(V_R1_1, &controller);
        filter->unhandled(R_R1_1, &controller);
        QCOMPARE((int) controller.contents.size(), 1);
        int R1_1 = controller.find({R_R1_1}, {V_R1_1, T1_R1_1, T2_R1_1});
        QVERIFY(R1_1 != -1);

        filter->unhandled(SequenceName("brw", "raw", "X_P01"), &controller);
        QCOMPARE((int) controller.contents.size(), 1);

        filter->unhandled(V_R1_2, &controller);
        QCOMPARE((int) controller.contents.size(), 2);
        int R1_2 = controller.find({R_R1_2}, {V_R1_2, T1_R1_2, T2_R1_2});
        QVERIFY(R1_2 != -1);

        filter->unhandled(R_RM1_1, &controller);
        filter->unhandled(V_RM1_1, &controller);
        QCOMPARE((int) controller.contents.size(), 3);
        int RM_1 = controller.find({R_RM1_1}, {V_RM1_1, T1_RM1_1, T2_RM1_1});
        QVERIFY(RM_1 != -1);

        data.setStart(15.0);
        data.setEnd(16.0);

        data.setValue(V_R1_1, Variant::Root(100.0));
        data.setValue(T1_R1_1, Variant::Root(20.0));
        data.setValue(T2_R1_1, Variant::Root(21.0));
        filter->process(R1_1, data);
        QCOMPARE(data.value(R_R1_1).toReal(), 468.1915880232366);

        {
            std::unique_ptr<SegmentProcessingStage> filter2;
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2.reset(component->deserializeBasicFilter(stream));
            }
            QVERIFY(filter2.get());
            data.setValue(R_R1_1, Variant::Root());
            data.setValue(V_R1_1, Variant::Root(100.0));
            data.setValue(T1_R1_1, Variant::Root(20.0));
            data.setValue(T2_R1_1, Variant::Root(21.0));
            filter->process(R1_1, data);
            QCOMPARE(data.value(R_R1_1).toReal(), 468.1915880232366);
        }

        data.setValue(V_R1_2, Variant::Root(90.0));
        data.setValue(T1_R1_2, Variant::Root(25.0));
        filter->process(R1_2, data);
        QCOMPARE(data.value(R_R1_2).toReal(), 493.0752867066491);

        data.setValue(V_RM1_1, Variant::Root(120.0));
        data.setValue(T1_RM1_1, Variant::Root(-5.0));
        filter->process(RM_1, data);
        QCOMPARE(data.value(R_RM1_1).toReal(), 353.17230516909456);

        data.setValue(V_RM1_1, Variant::Root(FP::undefined()));
        data.setValue(T1_RM1_1, Variant::Root(-5.0));
        filter->process(RM_1, data);
        QVERIFY(!FP::defined(data.value(R_RM1_1).toReal()));

        data.setValue(V_RM1_1, Variant::Root(100.0));
        data.setValue(T1_RM1_1, Variant::Root(FP::undefined()));
        filter->process(RM_1, data);
        QVERIFY(!FP::defined(data.value(R_RM1_1).toReal()));

        filter->processMeta(R1_1, data);
        QCOMPARE(data.value(R_R1_1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
    }

    void dynamicOptions()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<DynamicInputOption *>(options.get("factor"))->set(100.0);
        qobject_cast<DynamicInputOption *>(options.get("input"))->set(SequenceMatch::OrderedLookup(
                SequenceMatch::Element(QString("brw"), QString("raw"), "V_X1",
                                       SequenceName::Flavors())), Calibration());
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("output"))->set("brw", "raw",
                                                                                   "R1_X1",
                                                                                   SequenceName::Flavors());

        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterDynamic(options));
        QVERIFY(filter.get());
        TestController controller;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "Wx_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "V1_R11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "Tx_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "R_R11"), &controller);
        QVERIFY(controller.contents.empty());

        SequenceName V_X1("brw", "raw", "V_X1");
        SequenceName R1_X1("brw", "raw", "R1_X1");
        filter->unhandled(V_X1, &controller);
        filter->unhandled(R1_X1, &controller);

        QCOMPARE((int) controller.contents.size(), 1);
        int id = controller.find({R1_X1}, {V_X1});
        QVERIFY(id != -1);

        data.setValue(V_X1, Variant::Root(90.0));
        filter->process(id, data);
        QCOMPARE(data.value(R1_X1).toReal(), 0.9);

        filter.reset();


        options = component->getOptions();
        qobject_cast<ComponentOptionEnum *>(options.get("type"))->set("pyrgeometer");
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->add("R31");
        qobject_cast<DynamicInputOption *>(options.get("factor"))->set(0.25);
        qobject_cast<DynamicInputOption *>(options.get("sigma"))->set(1E-8);
        qobject_cast<DynamicInputOption *>(options.get("e"))->set(0.5);
        qobject_cast<DynamicInputOption *>(options.get("dcf"))->set(0.9);
        qobject_cast<DynamicInputOption *>(options.get("case"))->set(21.0);
        qobject_cast<DynamicInputOption *>(options.get("dome"))->set(20.0);

        filter.reset(component->createBasicFilterDynamic(options));
        QVERIFY(filter != NULL);
        controller.contents.clear();

        SequenceName V_R31("brw", "raw", "V_R31");
        SequenceName R_R31("brw", "raw", "R_R31");

        filter->unhandled(SequenceName("brw", "raw", "V_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "V1_R11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "R_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "R1_R11"), &controller);
        QVERIFY(controller.contents.empty());

        filter->unhandled(V_R31, &controller);

        QCOMPARE((int) controller.contents.size(), 1);
        id = controller.find({R_R31}, {V_R31});
        QVERIFY(id != -1);

        data.setValue(V_R31, Variant::Root(60.0));
        data.setValue(R_R31, Variant::Root());
        filter->process(id, data);
        QCOMPARE(data.value(R_R31).toReal(), 53.34378327027402);

        filter.reset();
    }

    void predefined()
    {
        ComponentOptions options(component->getOptions());

        SequenceName V_R11("brw", "raw", "V_R11");
        SequenceName R_R11("brw", "raw", "R_R11");
        SequenceName T1_R11("brw", "raw", "T1_R11");
        SequenceName T2_R11("brw", "raw", "T2_R11");
        QList<SequenceName> input;
        input << V_R11;

        std::unique_ptr<SegmentProcessingStage> filter
                (component->createBasicFilterPredefined(options, FP::undefined(), FP::undefined(),
                                                        input));
        QVERIFY(filter.get());
        TestController controller;
        QList<int> idL;

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{V_R11, T1_R11, T2_R11}));
        QCOMPARE(filter->predictedOutputs(), (SequenceName::Set{R_R11}));
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["XM1/Type"] = "Pyrgeometer";
        cv["XM1/CalculateRadiation"] = "::R_XM1:=";
        cv["XM1/Sensor"] = "::V_XM1:=";
        cv["XM1/CalibrationFactor"] = 0.75;
        cv["XM1/Sigma"] = 2E-8;
        cv["XM1/E"] = 1.5;
        cv["XM1/DomeCorrectionFactor"] = 0.25;
        cv["XM1/CaseTemperature"] = "::T1_XM1:=";
        cv["XM1/DomeTemperature"] = "::T2_XM1:=";
        config.emplace_back(FP::undefined(), 13.0, cv);
        cv.write().setEmpty();
        cv["XM1/CalculateRadiation"] = "::R_XM1:=";
        cv["XM1/Sensor"] = "::V2_XM1:=";
        cv["XM1/CalibrationFactor"] = 100.0;
        config.emplace_back(13.0, FP::undefined(), cv);

        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config));
        QVERIFY(filter.get());
        TestController controller;

        SequenceName R_XM1("brw", "raw", "R_XM1");
        SequenceName V_XM1("brw", "raw", "V_XM1");
        SequenceName V2_XM1("brw", "raw", "V2_XM1");
        SequenceName T1_XM1("brw", "raw", "T1_XM1");
        SequenceName T2_XM1("brw", "raw", "T2_XM1");

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{V_XM1, V2_XM1, T1_XM1, T2_XM1}));
        QCOMPARE(filter->predictedOutputs(), (SequenceName::Set{R_XM1}));

        filter->unhandled(R_XM1, &controller);
        filter->unhandled(V_XM1, &controller);
        filter->unhandled(V2_XM1, &controller);
        filter->unhandled(T1_XM1, &controller);
        filter->unhandled(T2_XM1, &controller);

        int id = controller.find({R_XM1}, {V_XM1, V2_XM1, T1_XM1, T2_XM1});
        QVERIFY(id != -1);

        QCOMPARE(filter->metadataBreaks(id), QSet<double>() << 13.0);

        SequenceSegment data1;
        data1.setStart(12.0);
        data1.setEnd(13.0);

        data1.setValue(V_XM1, Variant::Root(10.0));
        data1.setValue(T1_XM1, Variant::Root(25.0));
        data1.setValue(T2_XM1, Variant::Root(26.0));
        filter->process(id, data1);
        QCOMPARE(data1.value(R_XM1).toReal(), 244.0284726638626);

        data1.setValue(V_XM1, Variant::Root());
        filter->process(id, data1);
        QVERIFY(!FP::defined(data1.value(R_XM1).toReal()));

        filter->processMeta(id, data1);
        QCOMPARE(data1.value(R_XM1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);


        SequenceSegment data2;
        data2.setStart(13.0);
        data2.setEnd(15.0);

        data2.setValue(V2_XM1, Variant::Root(250.0));
        filter->process(id, data2);
        QCOMPARE(data2.value(R_XM1).toReal(), 2.5);

        filter->processMeta(id, data2);
        QCOMPARE(data2.value(R_XM1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
