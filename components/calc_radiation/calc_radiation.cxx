/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cmath>
#include <QRegularExpression>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "core/environment.hxx"
#include "core/qtcompat.hxx"
#include "core/timeutils.hxx"

#include "calc_radiation.hxx"

using namespace CPD3;
using namespace CPD3::Data;


CalcRadiation::CalcRadiation(const ComponentOptions &options)
{
    restrictedInputs = false;

    Calculation calc = Calculation::Divide;
    if (options.isSet("type")) {
        calc = static_cast<Calculation>(qobject_cast<ComponentOptionEnum *>(
                options.get("type"))->get().getID());
    }
    defaultCalculation.reset(new DynamicPrimitive<Calculation>::Constant(calc));

    if (options.isSet("factor")) {
        defaultFactor.reset(qobject_cast<DynamicInputOption *>(options.get("factor"))->getInput());
    }
    if (options.isSet("sigma")) {
        defaultSigma.reset(qobject_cast<DynamicInputOption *>(options.get("sigma"))->getInput());
    }
    if (options.isSet("e")) {
        defaultE.reset(qobject_cast<DynamicInputOption *>(options.get("e"))->getInput());
    }
    if (options.isSet("dcf")) {
        defaultDCF.reset(qobject_cast<DynamicInputOption *>(options.get("dcf"))->getInput());
    }
    if (options.isSet("case")) {
        defaultCaseTemperature.reset(
                qobject_cast<DynamicInputOption *>(options.get("case"))->getInput());
    }
    if (options.isSet("dome")) {
        defaultDomeTemperature.reset(
                qobject_cast<DynamicInputOption *>(options.get("dome"))->getInput());
    }

    if (options.isSet("input") || options.isSet("output")) {
        restrictedInputs = true;

        Processing proc;

        SequenceName::Component suffix;
        SequenceName::Component station;
        SequenceName::Component archive;
        SequenceName::Flavors flavors;
        if (options.isSet("output")) {
            proc.operateOutput
                .reset(qobject_cast<DynamicSequenceSelectionOption *>(
                        options.get("output"))->getOperator());

            QRegularExpression reCheck("R(\\d*_.+)");
            for (const auto &name : proc.operateOutput->getAllUnits()) {
                if (!suffix.empty())
                    break;

                station = name.getStation();
                archive = name.getArchive();
                flavors = name.getFlavors();

                auto r =
                        reCheck.match(name.getVariableQString(), 0, QRegularExpression::NormalMatch,
                                      QRegularExpression::AnchoredMatchOption);
                if (r.hasMatch()) {
                    suffix = r.captured(1).toStdString();
                }
            }
        }
        if (options.isSet("input")) {
            proc.inputSensor
                .reset(qobject_cast<DynamicInputOption *>(options.get("input"))->getInput());

            QRegularExpression reCheck("(?:W|V)(\\d*_.+)");
            for (const auto &name : proc.inputSensor->getUsedInputs()) {
                if (!suffix.empty())
                    break;

                station = name.getStation();
                archive = name.getArchive();
                flavors = name.getFlavors();

                auto r =
                        reCheck.match(name.getVariableQString(), 0, QRegularExpression::NormalMatch,
                                      QRegularExpression::AnchoredMatchOption);
                if (r.hasMatch()) {
                    suffix = r.captured(1).toStdString();
                }
            }
        }


        if (!proc.operateOutput) {
            if (suffix.empty()) {
                proc.operateOutput
                    .reset(new DynamicSequenceSelection::Match(QString::fromStdString(station),
                                                               QString::fromStdString(archive),
                                                               "R\\d*_.*", flavors));
            } else {
                proc.operateOutput
                    .reset(new DynamicSequenceSelection::Basic(
                            SequenceName(station, archive, "R" + suffix, flavors)));
            }
        }
        if (!proc.inputSensor) {
            auto sel = new DynamicInput::Variable;
            proc.inputSensor.reset(sel);
            if (suffix.empty()) {
                sel->set(SequenceMatch::OrderedLookup(
                        SequenceMatch::Element(station, archive, "V\\d*_.*", flavors)),
                         Calibration());
            } else {
                sel->set(SequenceMatch::OrderedLookup(
                        SequenceMatch::Element(station, archive, "V" + suffix, flavors)),
                         Calibration());
            }
        }

        proc.calculation = std::move(defaultCalculation);

        if (defaultFactor) {
            proc.inputFactor = std::move(defaultFactor);
        } else {
            proc.inputFactor.reset(new DynamicInput::Constant);
        }

        if (defaultSigma) {
            proc.inputSigma = std::move(defaultSigma);
        } else {
            proc.inputSigma.reset(new DynamicInput::Constant);
        }

        if (defaultE) {
            proc.inputE = std::move(defaultE);
        } else {
            proc.inputE.reset(new DynamicInput::Constant);
        }

        if (defaultDCF) {
            proc.inputDCF = std::move(defaultDCF);
        } else {
            proc.inputDCF.reset(new DynamicInput::Constant);
        }

        if (defaultCaseTemperature) {
            proc.inputCaseTemperature = std::move(defaultCaseTemperature);
        } else {
            proc.inputCaseTemperature.reset(new DynamicInput::Constant);
        }

        if (defaultDomeTemperature) {
            proc.inputDomeTemperature = std::move(defaultDomeTemperature);
        } else {
            proc.inputDomeTemperature.reset(new DynamicInput::Constant);
        }

        processing.emplace_back(std::move(proc));
    }

    if (options.isSet("suffix")) {
        filterSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->get());
    }
}

CalcRadiation::CalcRadiation(const ComponentOptions &options,
                             double,
                             double,
                             const QList<SequenceName> &inputs) : CalcRadiation(options)
{
    for (const auto &name : inputs) {
        CalcRadiation::unhandled(name, nullptr);
    }
}

static Calculation convertCalculation(const Variant::Read &value)
{
    const auto &type = value.toString();
    if (Util::equal_insensitive(type, "Pyrgeometer", "PIR", "EppleyPIR", "CGR4", "KippZonenCGR4"))
        return Calculation::Pyrgeometer;
    else if (Util::equal_insensitive(type, "Multiply", "Multiplier"))
        return Calculation::Multiply;
    return Calculation::Divide;
}

CalcRadiation::CalcRadiation(double start,
                             double end,
                             const SequenceName::Component &station,
                             const SequenceName::Component &archive,
                             const ValueSegment::Transfer &config)
{
    restrictedInputs = true;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    for (const auto &child : children) {
        Processing proc;

        proc.operateOutput
            .reset(DynamicSequenceSelection::fromConfiguration(config,
                                                               QString("%1/CalculateRadiation").arg(
                                                                       QString::fromStdString(
                                                                               child)), start,
                                                               end));

        proc.inputSensor
            .reset(DynamicInput::fromConfiguration(config, QString("%1/Sensor").arg(
                    QString::fromStdString(child)), start, end));
        proc.inputFactor
            .reset(DynamicInput::fromConfiguration(config, QString("%1/CalibrationFactor").arg(
                    QString::fromStdString(child)), start, end));
        proc.inputSigma
            .reset(DynamicInput::fromConfiguration(config, QString("%1/Sigma").arg(
                    QString::fromStdString(child)), start, end));
        proc.inputE
            .reset(DynamicInput::fromConfiguration(config, QString("%1/E").arg(
                    QString::fromStdString(child)), start, end));
        proc.inputDCF
            .reset(DynamicInput::fromConfiguration(config, QString("%1/DomeCorrectionFactor").arg(
                    QString::fromStdString(child)), start, end));
        proc.inputCaseTemperature
            .reset(DynamicInput::fromConfiguration(config, QString("%1/CaseTemperature").arg(
                    QString::fromStdString(child)), start, end));
        proc.inputDomeTemperature
            .reset(DynamicInput::fromConfiguration(config, QString("%1/DomeTemperature").arg(
                    QString::fromStdString(child)), start, end));

        proc.calculation
            .reset(DynamicPrimitive<Calculation>::fromConfiguration(convertCalculation, config,
                                                                    QString("%1/Type").arg(
                                                                            QString::fromStdString(
                                                                                    child)), start,
                                                                    end));

        proc.operateOutput->registerExpected(station, archive);
        proc.inputSensor->registerExpected(station, archive);
        proc.inputFactor->registerExpected(station, archive);
        proc.inputSigma->registerExpected(station, archive);
        proc.inputE->registerExpected(station, archive);
        proc.inputDCF->registerExpected(station, archive);
        proc.inputCaseTemperature->registerExpected(station, archive);
        proc.inputDomeTemperature->registerExpected(station, archive);

        processing.emplace_back(std::move(proc));
    }
}


CalcRadiation::~CalcRadiation() = default;

void CalcRadiation::handleNewProcessing(const SequenceName &name,
                                        std::size_t id,
                                        SegmentProcessingStage::SequenceHandlerControl *control)
{
    auto &proc = processing[id];

    SequenceName::Set reg;

    proc.operateOutput
        ->registerExpected(name.getStation(), name.getArchive(), {}, name.getFlavors());
    Util::merge(proc.operateOutput->getAllUnits(), reg);

    if (!control)
        return;

    for (const auto &n : reg) {
        bool isNew = !control->isHandled(n);
        control->filterUnit(n, id);
        if (isNew)
            registerPossibleInput(n, control, id);
    }
}

void CalcRadiation::registerPossibleInput(const SequenceName &name,
                                          SegmentProcessingStage::SequenceHandlerControl *control,
                                          std::size_t filterID)
{
    for (std::size_t id = 0, max = processing.size(); id < max; id++) {
        bool usedAsInput = false;
        auto &proc = processing[id];

        if (proc.inputSensor->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputFactor->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputSigma->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputE->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputDCF->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputCaseTemperature->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputDomeTemperature->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (usedAsInput && id != filterID)
            handleNewProcessing(name, id, control);
    }

    if (defaultFactor && defaultFactor->registerInput(name) && control)
        control->deferHandling(name);
    if (defaultSigma && defaultSigma->registerInput(name) && control)
        control->deferHandling(name);
    if (defaultE && defaultE->registerInput(name) && control)
        control->deferHandling(name);
    if (defaultDCF && defaultDCF->registerInput(name) && control)
        control->deferHandling(name);
    if (defaultCaseTemperature && defaultCaseTemperature->registerInput(name) && control)
        control->deferHandling(name);
    if (defaultDomeTemperature && defaultDomeTemperature->registerInput(name) && control)
        control->deferHandling(name);
}

void CalcRadiation::unhandled(const SequenceName &name,
                              SegmentProcessingStage::SequenceHandlerControl *control)
{
    registerPossibleInput(name, control);

    for (std::size_t id = 0, max = processing.size(); id < max; id++) {
        if (processing[id].operateOutput->registerInput(name)) {
            if (control)
                control->filterUnit(name, id);
            handleNewProcessing(name, id, control);
            return;
        }
    }

    if (restrictedInputs)
        return;

    if (name.hasFlavor(SequenceName::flavor_cover) || name.hasFlavor(SequenceName::flavor_stats))
        return;
    if (Util::starts_with(name.getVariable(), "R")) {
        if (control)
            control->deferHandling(name);
        return;
    }

    QRegularExpression reCheck;
    if (!filterSuffixes.empty()) {
        reCheck = QRegularExpression("V(\\d*)_(.+)");
    } else {
        reCheck = QRegularExpression("V(\\d*)_(R.*)");
    }
    auto r = reCheck.match(name.getVariableQString(), 0, QRegularExpression::NormalMatch,
                           QRegularExpression::AnchoredMatchOption);
    if (!r.hasMatch())
        return;
    auto suffix = r.captured(2).toStdString();
    if (suffix.empty())
        return;
    if (!filterSuffixes.empty() && !filterSuffixes.count(suffix))
        return;

    auto prefix = r.captured(1).toStdString();

    SequenceName radiationName
            (name.getStation(), name.getArchive(), "R" + prefix + "_" + suffix, name.getFlavors());
    SequenceName caseTemperatureName
            (name.getStation(), name.getArchive(), "T1_" + suffix, name.getFlavors());
    SequenceName domeTemperatureName
            (name.getStation(), name.getArchive(), "T2_" + suffix, name.getFlavors());

    Processing proc;

    proc.operateOutput.reset(new DynamicSequenceSelection::Single(radiationName));
    proc.operateOutput->registerInput(name);
    proc.operateOutput->registerInput(radiationName);
    proc.operateOutput->registerInput(domeTemperatureName);
    proc.operateOutput->registerInput(caseTemperatureName);

    proc.inputSensor.reset(new DynamicInput::Basic(name));
    proc.inputSensor->registerInput(name);
    proc.inputSensor->registerInput(radiationName);
    proc.inputSensor->registerInput(domeTemperatureName);
    proc.inputSensor->registerInput(caseTemperatureName);

    if (defaultFactor) {
        proc.inputFactor.reset(defaultFactor->clone());
    } else {
        proc.inputFactor.reset(new DynamicInput::Constant);
    }
    proc.inputFactor->registerInput(name);
    proc.inputFactor->registerInput(radiationName);
    proc.inputFactor->registerInput(domeTemperatureName);
    proc.inputFactor->registerInput(caseTemperatureName);

    if (defaultSigma) {
        proc.inputSigma.reset(defaultSigma->clone());
    } else {
        proc.inputSigma.reset(new DynamicInput::Constant);
    }
    proc.inputSigma->registerInput(name);
    proc.inputSigma->registerInput(radiationName);
    proc.inputSigma->registerInput(domeTemperatureName);
    proc.inputSigma->registerInput(caseTemperatureName);

    if (defaultE) {
        proc.inputE.reset(defaultE->clone());
    } else {
        proc.inputE.reset(new DynamicInput::Constant);
    }
    proc.inputE->registerInput(name);
    proc.inputE->registerInput(radiationName);
    proc.inputE->registerInput(domeTemperatureName);
    proc.inputE->registerInput(caseTemperatureName);

    if (defaultDCF) {
        proc.inputDCF.reset(defaultDCF->clone());
    } else {
        proc.inputDCF.reset(new DynamicInput::Constant);
    }
    proc.inputDCF->registerInput(name);
    proc.inputDCF->registerInput(radiationName);
    proc.inputDCF->registerInput(domeTemperatureName);
    proc.inputDCF->registerInput(caseTemperatureName);

    if (defaultCaseTemperature) {
        proc.inputCaseTemperature.reset(defaultCaseTemperature->clone());
    } else {
        proc.inputCaseTemperature.reset(new DynamicInput::Basic(caseTemperatureName));
    }
    proc.inputCaseTemperature->registerInput(name);
    proc.inputCaseTemperature->registerInput(radiationName);
    proc.inputCaseTemperature->registerInput(domeTemperatureName);
    proc.inputCaseTemperature->registerInput(caseTemperatureName);

    if (defaultDomeTemperature) {
        proc.inputDomeTemperature.reset(defaultDomeTemperature->clone());
    } else {
        proc.inputDomeTemperature.reset(new DynamicInput::Basic(domeTemperatureName));
    }
    proc.inputDomeTemperature->registerInput(name);
    proc.inputDomeTemperature->registerInput(radiationName);
    proc.inputDomeTemperature->registerInput(domeTemperatureName);
    proc.inputDomeTemperature->registerInput(caseTemperatureName);


    if (defaultCalculation) {
        proc.calculation.reset(defaultCalculation->clone());
    } else {
        proc.calculation.reset(new DynamicPrimitive<Calculation>::Constant(Calculation::Divide));
    }

    auto id = processing.size();

    if (control) {
        for (const auto &n : proc.operateOutput->getAllUnits()) {
            if (control->isHandled(n))
                continue;
            control->filterUnit(n, id);
        }

        for (const auto &n : proc.inputSensor->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : proc.inputFactor->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : proc.inputSigma->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : proc.inputE->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : proc.inputDCF->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : proc.inputCaseTemperature->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : proc.inputDomeTemperature->getUsedInputs()) {
            control->inputUnit(n, id);
        }
    }

    processing.emplace_back(std::move(proc));
}


double CalcRadiation::calculate(Processing &proc, CPD3::Data::SequenceSegment &data)
{
    double input = proc.inputSensor->get(data);
    if (!FP::defined(input))
        return FP::undefined();

    switch (proc.calculation->get(data)) {
    case Calculation::Divide: {
        double factor = proc.inputFactor->get(data);
        if (!FP::defined(factor))
            return FP::undefined();
        if (factor == 0.0)
            return FP::undefined();
        return input / factor;
    }
    case Calculation::Multiply: {
        double factor = proc.inputFactor->get(data);
        if (!FP::defined(factor))
            return FP::undefined();
        return input * factor;
    }
    case Calculation::Pyrgeometer: {
        /* calibration*voltage + sigma*(E*(TC^4) + DCF*((TC^4)-(TD^4))) */

        double factor = proc.inputFactor->get(data);
        if (!FP::defined(factor))
            return FP::undefined();
        input *= factor;

        double TC = proc.inputCaseTemperature->get(data);
        if (!FP::defined(TC))
            return FP::undefined();
        if (TC < 150.0)
            TC += 273.15;
        TC = TC * TC * TC * TC;

        double sigma = proc.inputSigma->get(data);
        if (!FP::defined(sigma))
            sigma = 5.670374419E-8;
        double E = proc.inputE->get(data);
        if (!FP::defined(E))
            E = 1.0;

        double DCF = proc.inputDCF->get(data);
        if (FP::defined(DCF)) {
            double TD = proc.inputDomeTemperature->get(data);
            if (FP::defined(TD)) {
                if (TD < 150.0)
                    TD += 273.15;

                TD = TD * TD * TD * TD;
                input += sigma * (E * TC + DCF * (TC - TD));
            } else {
                input += sigma * E * TC;
            }
        } else {
            input += sigma * E * TC;
        }

        return input;
    }
    }

    Q_ASSERT(false);
    return FP::undefined();
}

void CalcRadiation::process(int id, SequenceSegment &data)
{
    auto &proc = processing[id];

    double value = calculate(proc, data);
    for (const auto &i : proc.operateOutput->get(data)) {
        data[i].setReal(value);
    }
}

void CalcRadiation::processMeta(int id, SequenceSegment &data)
{
    auto &proc = processing[id];

    Variant::Root meta;

    meta["By"].setString("calc_radiation");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    switch (proc.calculation->get(data)) {
    case Calculation::Divide:
        meta["Parameters/Type"] = "Division";
        meta["Parameters/Factor"] = proc.inputFactor->describe(data);
        break;
    case Calculation::Multiply:
        meta["Parameters/Type"] = "Multiplication";
        meta["Parameters/Factor"] = proc.inputFactor->describe(data);
        break;
    case Calculation::Pyrgeometer:
        meta["Parameters/Type"] = "Pyrgeometer";
        meta["Parameters/Factor"] = proc.inputFactor->describe(data);
        meta["Parameters/DCF"] = proc.inputDCF->describe(data);
        meta["Parameters/Sigma"] = proc.inputSigma->describe(data);
        meta["Parameters/E"] = proc.inputE->describe(data);
        meta["Parameters/CaseTemperature"] = proc.inputCaseTemperature->describe(data);
        meta["Parameters/DomeTemperature"] = proc.inputDomeTemperature->describe(data);
        break;
    }

    Variant::Read base = Variant::Read::empty();
    for (auto i : proc.inputSensor->getUsedInputs()) {
        i.setMeta();
        if (!data.exists(i))
            continue;
        base = data[i];
        if (!base.exists())
            continue;
        break;
    }

    for (auto i : proc.operateOutput->get(data)) {
        i.setMeta();
        if (!data[i].exists())
            data[i].set(base);

        data[i].metadataReal("Format").setString("0000.00");
        data[i].metadataReal("Units").setString("W/m\xC2\xB2");
        if (!data[i].metadataReal("Description").exists())
            data[i].metadataReal("Description").setString("Calculated radiation");

        data[i].metadata("Processing").toArray().after_back().set(meta);

        data[i].metadataReal("MVC").remove();
        data[i].metadataReal("GroupUnits").remove();
        data[i].metadataReal("Smoothing").remove();
    }
}


SequenceName::Set CalcRadiation::requestedInputs()
{
    SequenceName::Set out;
    for (const auto &p : processing) {
        Util::merge(p.inputSensor->getUsedInputs(), out);
        Util::merge(p.inputFactor->getUsedInputs(), out);
        Util::merge(p.inputSigma->getUsedInputs(), out);
        Util::merge(p.inputE->getUsedInputs(), out);
        Util::merge(p.inputDCF->getUsedInputs(), out);
        Util::merge(p.inputCaseTemperature->getUsedInputs(), out);
        Util::merge(p.inputDomeTemperature->getUsedInputs(), out);
    }
    return out;
}

SequenceName::Set CalcRadiation::predictedOutputs()
{
    SequenceName::Set out;
    for (const auto &p : processing) {
        Util::merge(p.operateOutput->getAllUnits(), out);
    }
    return out;
}

QSet<double> CalcRadiation::metadataBreaks(int id)
{
    const auto &p = processing[id];
    QSet<double> result;
    Util::merge(p.operateOutput->getChangedPoints(), result);
    Util::merge(p.calculation->getChangedPoints(), result);
    Util::merge(p.inputSensor->getChangedPoints(), result);
    Util::merge(p.inputFactor->getChangedPoints(), result);
    Util::merge(p.inputSigma->getChangedPoints(), result);
    Util::merge(p.inputE->getChangedPoints(), result);
    Util::merge(p.inputDCF->getChangedPoints(), result);
    Util::merge(p.inputCaseTemperature->getChangedPoints(), result);
    Util::merge(p.inputDomeTemperature->getChangedPoints(), result);
    return result;
}

QDataStream &operator>>(QDataStream &stream, Calculation &op)
{
    quint32 n = 0;
    stream >> n;
    op = static_cast<Calculation>(n);
    return stream;
}

QDataStream &operator<<(QDataStream &stream, Calculation op)
{
    stream << static_cast<quint32>(op);
    return stream;
}

QDebug operator<<(QDebug stream, Calculation op)
{
    switch (op) {
    case Calculation::Divide:
        stream << "Divide";
        break;
    case Calculation::Multiply:
        stream << "Multiply";
        break;
    case Calculation::Pyrgeometer:
        stream << "Pyrgeometer";
        break;
    }
    return stream;
}

CalcRadiation::CalcRadiation(QDataStream &stream)
{
    stream >> defaultCalculation;
    stream >> defaultFactor;
    stream >> defaultSigma;
    stream >> defaultE;
    stream >> defaultDCF;
    stream >> defaultCaseTemperature;
    stream >> defaultDomeTemperature;
    stream >> filterSuffixes;
    stream >> restrictedInputs;

    Deserialize::container(stream, processing, [&stream]() {
        Processing proc;

        stream >> proc.operateOutput;
        stream >> proc.calculation;
        stream >> proc.inputSensor;
        stream >> proc.inputFactor;
        stream >> proc.inputSigma;
        stream >> proc.inputE;
        stream >> proc.inputDCF;
        stream >> proc.inputCaseTemperature;
        stream >> proc.inputDomeTemperature;

        return proc;
    });
}

void CalcRadiation::serialize(QDataStream &stream)
{
    stream << defaultCalculation;
    stream << defaultFactor;
    stream << defaultSigma;
    stream << defaultE;
    stream << defaultDCF;
    stream << defaultCaseTemperature;
    stream << defaultDomeTemperature;
    stream << filterSuffixes;
    stream << restrictedInputs;

    Serialize::container(stream, processing, [&stream](const Processing &proc) {
        stream << proc.operateOutput;
        stream << proc.calculation;
        stream << proc.inputSensor;
        stream << proc.inputFactor;
        stream << proc.inputSigma;
        stream << proc.inputE;
        stream << proc.inputDCF;
        stream << proc.inputCaseTemperature;
        stream << proc.inputDomeTemperature;
    });
}


QString CalcRadiationComponent::getBasicSerializationName() const
{ return QString::fromLatin1("calc_radiation"); }

ComponentOptions CalcRadiationComponent::getOptions()
{
    ComponentOptions options;

    options.add("input", new DynamicInputOption(tr("input", "name"), tr("Input sensor"),
                                                tr("This is the input sensor reading.  This option is "
                                                   "mutually exclusive with with instrument specification."),
                                                tr("All radiation voltages")));

    options.add("factor", new DynamicInputOption(tr("factor", "name"), tr("Calibration factor"),
                                                 tr("This is the calibration factor applied to the input.  This factor should convert the input sensor reading to W/m\xC2\xB2."),
                                                 {}));
    options.add("sigma", new DynamicInputOption(tr("sigma", "name"), tr("Sigma value"),
                                                tr("This is the Stefan–Boltzmann constant used."),
                                                tr("5.670374419E-8 W/m\xC2\xB2K\xE2\x81\xB4")));
    options.add("e", new DynamicInputOption(tr("e", "name"), tr("E temperature factor"),
                                            tr("This is the E factor used for case temperature correction.  This is applied to the case temperature before the Stefan–Boltzmann constant."),
                                            tr("1")));
    options.add("dcf",
                new DynamicInputOption(tr("dcf", "name"), tr("Dome temperature correction factor"),
                                       tr("This is the dome temperature correction factor.  This is applied to the difference of the case and dome temperatures each to the fourth, before the Stefan–Boltzmann constant."),
                                       tr("Disabled")));
    options.add("case", new DynamicInputOption(tr("case", "name"), tr("Case temperature"),
                                               tr("This is case temperature used for pyrgeometer corrections."),
                                               tr("Automatically selected")));
    options.add("dome", new DynamicInputOption(tr("dome", "name"), tr("Dome temperature"),
                                               tr("This is dome temperature used for pyrgeometer corrections."),
                                               tr("Automatically selected")));

    options.add("output",
                new DynamicSequenceSelectionOption(tr("output", "name"), tr("Output value"),
                                                   tr("This is output radiation intensity.  This option is "
                                                      "mutually exclusive with with instrument specification."),
                                                   {}));
    options.add("suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments", "name"),
                                                                 tr("Instrument suffixes to correct"),
                                                                 tr("These are the instrument suffixes to calculate temperatures for.  "
                                                                    "This option is mutually exclusive with manual "
                                                                    "variable specification."),
                                                                 tr("All radiation instruments")));
    options.exclude("input", "suffix");
    options.exclude("output", "suffix");
    options.exclude("suffix", "input");
    options.exclude("suffix", "output");


    ComponentOptionEnum *calc =
            new ComponentOptionEnum(tr("type", "name"), tr("Calibration performed"),
                                    tr("This is the calibration performed on the inputs to generate the radiation intensity."),
                                    tr("Division"), -1);

    calc->add(static_cast<int>(Calculation::Divide), "division", tr("Division", "calculation name"),
              tr("Division by the calibration factor", "calculation description"));
    calc->alias(static_cast<int>(Calculation::Divide), tr("NIP"));
    calc->alias(static_cast<int>(Calculation::Divide), tr("EppleyNIP"));
    calc->alias(static_cast<int>(Calculation::Divide), tr("PSP"));
    calc->alias(static_cast<int>(Calculation::Divide), tr("EppleyPSP"));
    calc->alias(static_cast<int>(Calculation::Divide), tr("BW"));
    calc->alias(static_cast<int>(Calculation::Divide), tr("EppleyBW"));
    calc->alias(static_cast<int>(Calculation::Divide), tr("CM22"));
    calc->alias(static_cast<int>(Calculation::Divide), tr("KippZonenCM22"));
    calc->alias(static_cast<int>(Calculation::Divide), tr("CHP1"));
    calc->alias(static_cast<int>(Calculation::Divide), tr("KippZonenCHP1"));

    calc->add(static_cast<int>(Calculation::Multiply), "multiplication",
              tr("Multiplication", "calculation name"),
              tr("Multiplication by the calibration factor", "calculation description"));

    calc->add(static_cast<int>(Calculation::Pyrgeometer), "pyrgeometer",
              tr("Pyrgeometer", "calculation name"),
              tr("Pyrgeometer calculation (R=V*cal+\xCF\x83*T\xE2\x81\xB4)",
                     "calculation description"));
    calc->alias(static_cast<int>(Calculation::Pyrgeometer), tr("PIR"));
    calc->alias(static_cast<int>(Calculation::Pyrgeometer), tr("EppleyPIR"));
    calc->alias(static_cast<int>(Calculation::Pyrgeometer), tr("CGR4"));
    calc->alias(static_cast<int>(Calculation::Pyrgeometer), tr("KippZonenCGR4"));

    options.add("type", calc);

    return options;
}

QList<ComponentExample> CalcRadiationComponent::getExamples()
{
    QList<ComponentExample> examples;

    return examples;
}

SegmentProcessingStage *CalcRadiationComponent::createBasicFilterDynamic(const ComponentOptions &options)
{ return new CalcRadiation(options); }

SegmentProcessingStage *CalcRadiationComponent::createBasicFilterPredefined(const ComponentOptions &options,
                                                                            double start,
                                                                            double end,
                                                                            const QList<
                                                                                    SequenceName> &inputs)
{ return new CalcRadiation(options, start, end, inputs); }

SegmentProcessingStage *CalcRadiationComponent::createBasicFilterEditing(double start,
                                                                         double end,
                                                                         const SequenceName::Component &station,
                                                                         const SequenceName::Component &archive,
                                                                         const ValueSegment::Transfer &config)
{ return new CalcRadiation(start, end, station, archive, config); }

SegmentProcessingStage *CalcRadiationComponent::deserializeBasicFilter(QDataStream &stream)
{ return new CalcRadiation(stream); }
