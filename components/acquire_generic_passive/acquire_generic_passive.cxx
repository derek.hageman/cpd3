/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QRegExp>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/timeparse.hxx"
#include "core/qtcompat.hxx"
#include "core/textsubstitution.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_generic_passive.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Transfer;


AcquireGenericPassive::Configuration::Configuration() : start(FP::undefined()),
                                                        end(FP::undefined()),
                                                        records(),
                                                        timeout(10.0),
                                                        referenceTime(FP::undefined()),
                                                        allowMultipleMatches(false),
                                                        allowUnmatchedLines(false),
                                                        autoprobeRequiredRecords(10),
                                                        autoprobeRequireAllRecords(true),
                                                        instrumentMeta()
{ }

AcquireGenericPassive::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          records(other.records),
          timeout(other.timeout), referenceTime(other.referenceTime),
          allowMultipleMatches(other.allowMultipleMatches),
          allowUnmatchedLines(other.allowUnmatchedLines),
          autoprobeRequiredRecords(other.autoprobeRequiredRecords),
          autoprobeRequireAllRecords(other.autoprobeRequireAllRecords),
          instrumentMeta(other.instrumentMeta)
{ }

AcquireGenericPassive::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          records(),
          timeout(10.0), referenceTime(FP::undefined()),
          allowMultipleMatches(false),
          allowUnmatchedLines(false),
          autoprobeRequiredRecords(10),
          autoprobeRequireAllRecords(true),
          instrumentMeta()
{
    setFromSegment(other);
}

AcquireGenericPassive::Configuration::Configuration(const Configuration &under,
                                                    const ValueSegment &over,
                                                    double s,
                                                    double e) : start(s),
                                                                end(e),
                                                                records(under.records),
                                                                timeout(under.timeout),
                                                                referenceTime(under.referenceTime),
                                                                allowMultipleMatches(
                                                                        under.allowMultipleMatches),
                                                                allowUnmatchedLines(
                                                                        under.allowUnmatchedLines),
                                                                autoprobeRequiredRecords(
                                                                        under.autoprobeRequiredRecords),
                                                                autoprobeRequireAllRecords(
                                                                        under.autoprobeRequireAllRecords),
                                                                instrumentMeta(under.instrumentMeta)
{
    setFromSegment(over);
}

static void setFieldsAndCaptures(const Variant::Read &config,
                                 std::vector<std::size_t> &fields,
                                 std::vector<std::size_t> &captures)
{
    switch (config["Fields"].getType()) {
    case Variant::Type::Array: {
        for (auto add : config["Fields"].toArray()) {
            auto i = add.toInteger();
            if (INTEGER::defined(i) && i >= 1)
                fields.push_back(static_cast<std::size_t>(i) - 1);
        }
        break;
    }

    default: {
        auto i = config["Fields"].toInteger();
        if (INTEGER::defined(i) && i >= 1)
            fields.push_back(static_cast<std::size_t>(i) - 1);
        break;
    }
    }

    switch (config["Captures"].getType()) {
    case Variant::Type::Array: {
        for (auto add : config["Captures"].toArray()) {
            auto i = add.toInteger();
            if (INTEGER::defined(i) && i >= 0)
                captures.push_back(static_cast<std::size_t>(i));
        }
        break;
    }

    default: {
        auto i = config["Captures"].toInteger();
        if (INTEGER::defined(i) && i >= 0)
            captures.push_back(static_cast<std::size_t>(i));
        break;
    }
    }
}

static void setHeaders(const Variant::Read &config, std::vector<QRegularExpression> &headers)
{
    bool caseInsensitive = true;
    if (config["HeaderCaseInsensitive"].exists())
        caseInsensitive = config["HeaderCaseInsensitive"].toBool();
    switch (config["Headers"].getType()) {
    case Variant::Type::Array: {
        for (auto add : config["Headers"].toArray()) {
            auto pattern = add.toQString();
            if (pattern.isEmpty())
                continue;
            QRegularExpression re(pattern,
                                  caseInsensitive ? QRegularExpression::CaseInsensitiveOption
                                                  : QRegularExpression::NoPatternOption);
            if (!re.isValid())
                continue;
            headers.emplace_back(std::move(re));
        }
        break;
    }

    default: {
        QString pattern = config["Headers"].toQString();
        if (!pattern.isEmpty()) {
            QRegularExpression re(pattern,
                                  caseInsensitive ? QRegularExpression::CaseInsensitiveOption
                                                  : QRegularExpression::NoPatternOption);
            if (re.isValid()) {
                headers.emplace_back(std::move(re));
            }
        }
        break;
    }
    }
}

void AcquireGenericPassive::Configuration::addVariable(const SequenceName::Component &name,
                                                       const Variant::Read &config,
                                                       std::vector<
                                                               AcquireGenericPassive::OutputVariable> &output)
{
    AcquireGenericPassive::OutputVariable variable;

    if (name.empty())
        return;
    variable.name = name;
    variable.metadata.write().set(config["Metadata"]);
    variable.calibration = Variant::Composite::toCalibration(config["Calibration"]);

    setFieldsAndCaptures(config, variable.fields, variable.captures);
    setHeaders(config, variable.headers);
    if (variable.fields.empty() && variable.captures.empty() && variable.headers.empty())
        return;

    variable.maximumAge = config["MaximumAge"].toDouble();
    if (!FP::defined(variable.maximumAge) || variable.maximumAge < 0.0)
        variable.maximumAge = 10.0;

    {
        const auto &check = config["SearchBehavior"].toString();
        if (Util::equal_insensitive(check, "defined", "firstvalid"))
            variable.valueSearchBehavior = OutputVariable::SearchFirstDefined;
        else
            variable.valueSearchBehavior = OutputVariable::SearchFirstNonEmpty;
    }

    {
        const auto &check = config["Accumulation"].toString();
        if (Util::equal_insensitive(check, "difference", "absolute"))
            variable.accumulation = OutputVariable::AccumulateDifference;
        else if (Util::equal_insensitive(check, "rate"))
            variable.accumulation = OutputVariable::AccumulateRate;
        else
            variable.accumulation = OutputVariable::AccumulateDisabled;
    }

    {
        const auto &check = config["MissingBehavior"].toString();
        if (Util::equal_insensitive(check, "skip", "ignore"))
            variable.missingBehavior = OutputVariable::MissingSkip;
        else if (Util::equal_insensitive(check, "skipwithgap", "ignorewithgap", "gap"))
            variable.missingBehavior = OutputVariable::MissingSkipWithGap;
        else if (Util::equal_insensitive(check, "mvc", "outputmvc", "outputinvalid"))
            variable.missingBehavior = OutputVariable::MissingOutputMVC;
        else
            variable.missingBehavior = OutputVariable::MissingReturnInvalidRecord;
    }

    {
        const auto &check = config["MVCBehavior"].toString();
        if (Util::equal_insensitive(check, "skip", "ignore"))
            variable.mvcBehavior = OutputVariable::MVCSkip;
        else if (Util::equal_insensitive(check, "skipwithgap", "ignorewithgap", "gap"))
            variable.mvcBehavior = OutputVariable::MVCSkipWithGap;
        else if (Util::equal_insensitive(check, "invalid", "invalidrecord"))
            variable.mvcBehavior = OutputVariable::MVCReturnInvalidRecord;
        else
            variable.mvcBehavior = OutputVariable::MVCOutputMVC;
    }

    auto check = config["MVC"].toQString();
    if (!check.isEmpty()) {
        bool caseInsensitive = true;
        if (config["MVCCaseInsensitive"].exists())
            caseInsensitive = config["MVCCaseInsensitive"].toBool();
        variable.mvc = QRegularExpression(check, caseInsensitive
                                                 ? QRegularExpression::CaseInsensitiveOption
                                                 : QRegularExpression::NoPatternOption);
        if (QRegExp::escape(check) == check) {
            if (!variable.metadata.read().metadata("MVC").exists() &&
                    !variable.metadata.read().metadata("Format").exists()) {
                variable.metadata.write().metadata("MVC").setString(check);
            }
        }
    }

    output.emplace_back(std::move(variable));
}

void AcquireGenericPassive::Configuration::addRecord(const QString &pattern,
                                                     const Variant::Read &config)
{
    Record record;

    if (pattern.isEmpty()) {
        record.matcher = QRegularExpression(".+");
        record.requireValidVariable = true;
        if (config["RequireValidVariable"].exists())
            record.requireValidVariable = config["RequireValidVariable"].toBool();
    } else {
        class CaptureSubstitution : public TextSubstitution {
            QList<FileDownloader::TimeCapture> &captures;
        public:
            explicit CaptureSubstitution(QList<FileDownloader::TimeCapture> &caps) : captures(caps)
            { }

            virtual ~CaptureSubstitution() = default;

        protected:
            QString substitution(const QStringList &elements) const override
            {
                {
                    int index = 0;
                    if (index >= elements.size())
                        return {};
                    auto type = elements[index++].toLower();

                    if (type == "value") {
                        QString pattern = R"([+-]?\d+(?:\.\d*)?(?:[Ee][+-]?\d+)?)";
                        if (index < elements.size())
                            pattern = elements[index++];
                        if (!pattern.isEmpty()) {
                            captures.push_back(FileDownloader::TimeCapture::Ignore);
                            return "(" + pattern + ")";
                        }
                        return {};
                    }
                }
                return FileDownloader::getTimeCapture(elements, captures);
            }
        };

        CaptureSubstitution caps(record.timeCaptures);

        record.matcher = QRegularExpression(caps.apply(pattern),
                                            config["MatchCaseInsensitive"].toBool()
                                            ? QRegularExpression::CaseInsensitiveOption
                                            : QRegularExpression::NoPatternOption);
        record.requireValidVariable = config["RequireValidVariable"].toBool();
    }
    if (!record.matcher.isValid()) {
        qWarning() << "Record pattern" << pattern << "is invalid: " << record.matcher.errorString();
        return;
    }

    {
        auto check = config["Header/Match"].toQString();
        if (!check.isEmpty()) {
            record.header = QRegularExpression(check, config["Header/MatchCaseInsensitive"].toBool()
                                                      ? QRegularExpression::CaseInsensitiveOption
                                                      : QRegularExpression::NoPatternOption);
        }
    }

    for (const auto &rep : config["Substitutions"].toArray()) {
        Record::Replacement replacement;
        replacement.match = QRegularExpression(rep["Match"].toQString(),
                                               rep["CaseInsensitive"].toBool()
                                               ? QRegularExpression::CaseInsensitiveOption
                                               : QRegularExpression::NoPatternOption);
        if (!replacement.match.isValid() || replacement.match.pattern().isEmpty())
            continue;
        replacement.replace = rep["Replacement"].toQString();
        record.replacements.emplace_back(std::move(replacement));
    }

    record.interval = config["Interval"].toDouble();
    record.roundingLimit = config["Rounding"].toDouble();

    record.requireValidTime = config["Time/RequireValid"].toBool();
    {
        switch (config["Time/Fields"].getType()) {
        case Variant::Type::Array: {
            for (auto add : config["Time/Fields"].toArray()) {
                auto i = add.toInteger();
                if (INTEGER::defined(i) && i >= 1)
                    record.timeFields.push_back(static_cast<std::size_t>(i) - 1);
            }
            break;
        }

        default: {
            auto i = config["Time/Fields"].toInteger();
            if (INTEGER::defined(i) && i >= 1)
                record.timeFields.push_back(static_cast<std::size_t>(i) - 1);
            break;
        }
        }
    }

    record.separator = config["Fields/Separator"].toQString();
    if (record.separator.isEmpty())
        record.separator = QString::fromLatin1(",");
    record.quote = config["Fields/Quote"].toQString();
    if (record.quote.isEmpty())
        record.quote = QString::fromLatin1("\"");
    record.quoteEscape = config["Fields/QuoteEscape"].toQString();
    if (record.quoteEscape.isEmpty())
        record.quoteEscape = QString::fromLatin1("\"\"\"");


    {
        auto children = config["Variables"].toChildren();
        for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
            if (!add.value().exists())
                continue;
            auto name = add.value().hash("Name").toString();
            if (name.empty())
                name = add.stringKey();
            addVariable(name, add.value(), record.variables);
        }
    }

    if (record.requireValidVariable && record.variables.empty()) {
        qWarning() << "Record with matcher" << record.matcher.pattern()
                   << "requires a valid variable, but there are no possible variables";
    }
    records.emplace_back(std::move(record));
}

void AcquireGenericPassive::Configuration::setFromSegment(const ValueSegment &config)
{
    double d = config["Timeout"].toDouble();
    if (FP::defined(d) && d > 0.0)
        timeout = d;

    if (config["BaseTime"].exists())
        referenceTime = config["BaseTime"].toDouble();

    if (config["AllowMultipleMatches"].exists())
        allowMultipleMatches = config["AllowMultipleMatches"].toBool();
    if (config["AllowUnmatchedLines"].exists())
        allowUnmatchedLines = config["AllowUnmatchedLines"].toBool();

    qint64 i64 = config["Autoprobe/RequiredRecords"].toInt64();
    if (INTEGER::defined(i64) && i64 > 0)
        autoprobeRequiredRecords = (int) i64;
    if (config["Autoprobe/RequireAllRecords"].exists()) {
        autoprobeRequireAllRecords = config["Autoprobe/RequireAllRecords"].toBool();
    }

    if (config["InstrumentMetadata"].exists()) {
        instrumentMeta.write().set(config["InstrumentMetadata"]);
    }

    switch (config["Records"].getType()) {
    case Variant::Type::Array: {
        records.clear();
        for (auto add : config["Records"].toArray()) {
            addRecord(add.hash("Match").toQString(), add);
        }
        break;
    }

    case Variant::Type::Hash: {
        records.clear();
        addRecord(config["Records/Match"].toQString(), config["Records"]);
        break;
    }

    default:
        break;
    }
}


void AcquireGenericPassive::setDefaultInvalid()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    lateAdvanceTime = FP::undefined();
}


AcquireGenericPassive::AcquireGenericPassive(const ValueSegment::Transfer &configData,
                                             const std::string &loggingContext) : FramedInstrument(
        "genericpassive", loggingContext),
                                                                                  autoprobeStatus(
                                                                                          AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                  responseState(
                                                                                          RESP_WAIT),
                                                                                  autoprobeValidCount(
                                                                                          0),
                                                                                  autoprobeValidSeen(),
                                                                                  lastValidTime(
                                                                                          FP::undefined()),
                                                                                  loggingMux(
                                                                                          LogStream_TOTAL),
                                                                                  loggingStateTime(
                                                                                          FP::undefined()),
                                                                                  realtimeStateTime(
                                                                                          FP::undefined())
{
    setDefaultInvalid();
    for (const auto &add : configData) {
        if (!add.getValue()["Records"].exists())
            continue;
        Range::overlayFragmenting(config, add);
    }
    if (config.empty()) {
        config.emplace_back();
    } else {
        /* Have to give it a start in case we're looking up the time
         * (or we'd never have a record to look it up with) */
        config.front().setStart(FP::undefined());
        config.back().setEnd(FP::undefined());
    }
}

void AcquireGenericPassive::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_INITIALIZE;
    autoprobeValidCount = 0;
    autoprobeValidSeen.clear();
}

void AcquireGenericPassive::setToInteractive()
{ responseState = RESP_INITIALIZE; }


ComponentOptions AcquireGenericPassiveComponent::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireGenericPassive::AcquireGenericPassive(const ComponentOptions &options,
                                             const std::string &loggingContext) : FramedInstrument(
        "genericpassive", loggingContext),
                                                                                  autoprobeStatus(
                                                                                          AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                  responseState(
                                                                                          RESP_WAIT),
                                                                                  autoprobeValidCount(
                                                                                          0),
                                                                                  autoprobeValidSeen(),
                                                                                  lastValidTime(
                                                                                          FP::undefined()),
                                                                                  loggingMux(
                                                                                          LogStream_TOTAL),
                                                                                  loggingStateTime(
                                                                                          FP::undefined()),
                                                                                  realtimeStateTime(
                                                                                          FP::undefined())
{

    setDefaultInvalid();

    Configuration base;

    if (options.isSet("basetime")) {
        base.referenceTime =
                qobject_cast<ComponentOptionSingleTime *>(options.get("basetime"))->get();
    }

    config.emplace_back(std::move(base));
}

AcquireGenericPassive::~AcquireGenericPassive()
{
}

SequenceValue::Transfer AcquireGenericPassive::buildLogMeta(double time) const
{
    Q_ASSERT(!config.empty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_generic_passive");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    for (const auto &record : config.front().records) {
        for (const auto &variable : record.variables) {
            result.emplace_back(SequenceName({}, "raw_meta", variable.name),
                                Variant::Root(variable.metadata), time, FP::undefined());
            if (result.back().write().metadataReal("Format").toString().empty()) {
                result.back().write().metadataReal("Format").setString("00.000");
            }
            if (!result.back().write().metadataReal("Source").exists()) {
                result.back().write().metadataReal("Source").set(config.front().instrumentMeta);
            }
            result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
            if (!variable.calibration.isIdentity()) {
                Variant::Composite::fromCalibration(
                        result.back().write().metadataReal("Calibration"), variable.calibration);
            }
            for (auto i : variable.fields) {
                result.back()
                      .write()
                      .metadataReal("SourceFields")
                      .toArray()
                      .after_back()
                      .setInteger(i + 1);
            }
            for (auto i : variable.captures) {
                result.back()
                      .write()
                      .metadataReal("SourceCaptures")
                      .toArray()
                      .after_back()
                      .setInteger(i);
            }
            for (const auto &h : variable.headers) {
                result.back()
                      .write()
                      .metadataReal("SourceHeaders")
                      .toArray()
                      .after_back()
                      .setString(h.pattern());
            }
            result.back().write().metadataReal("RecordMatcher").setString(record.matcher.pattern());
        }
    }

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(config.front().instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    return result;
}

void AcquireGenericPassive::introduceGap(const OutputVariable &variable, double frameTime)
{
    auto vs = variableState.find(variable.name);
    if (vs == variableState.end())
        return;

    vs->second.lastTime = frameTime;
    vs->second.lastValue = FP::undefined();
    if (!FP::defined(frameTime))
        return;

    vs->second.expiredAt = frameTime + variable.maximumAge;
    loggingMux.advance(vs->second.streamID, frameTime, loggingEgress);
}

AcquireGenericPassive::VariableState &AcquireGenericPassive::acquireVariable(const OutputVariable &variable)
{
    auto vs = variableState.find(variable.name);

    if (vs == variableState.end()) {
        std::size_t streamID = static_cast<std::size_t>(-1);
        if (!availableStreams.empty()) {
            streamID = availableStreams.front();
            availableStreams.pop_front();
        } else {
            for (const auto &check : variableState) {
                streamID = std::max(streamID, check.second.streamID);
            }
            if (streamID == static_cast<std::size_t>(-1)) {
                streamID = LogStream_RecordBaseStart;
            } else {
                ++streamID;
            }
            loggingMux.setStreams(streamID + 1);
        }

        vs = variableState.emplace(variable.name, VariableState()).first;

        vs->second.streamID = streamID;
        vs->second.expiredAt = FP::undefined();
        vs->second.lastTime = FP::undefined();
        vs->second.lastValue = FP::undefined();
    }

    return vs->second;
}

void AcquireGenericPassive::variableUpdated(const OutputVariable &variable,
                                            double frameTime,
                                            double value)
{
    auto &vs = acquireVariable(variable);

    if (!FP::defined(frameTime)) {
        vs.lastTime = FP::undefined();
        return;
    }

    auto rs = restoreState.find(variable.name);

    /* Make it consistent with the expire logic below, so all variables from the
     * record expire, instead of all except the first one */
    if (FP::defined(vs.expiredAt) && frameTime > vs.expiredAt) {
        vs.lastTime = FP::undefined();
        vs.lastValue = FP::undefined();
    }
    vs.expiredAt = frameTime + variable.maximumAge;

    double priorValue = vs.lastValue;
    if (!FP::defined(priorValue)) {
        if (rs != restoreState.end())
            priorValue = rs->second.value;
    }
    vs.lastValue = value;

    double priorTime = vs.lastTime;
    if (!FP::defined(priorTime)) {
        if (rs != restoreState.end())
            priorTime = rs->second.time;
    }

    if (rs != restoreState.end())
        restoreState.erase(rs);

    double startTime = vs.lastTime;
    double endTime = frameTime;
    vs.lastTime = endTime;

    switch (variable.accumulation) {
    case OutputVariable::AccumulateDisabled:
        break;
    case OutputVariable::AccumulateDifference:
        if (FP::defined(priorValue) && FP::defined(value))
            value = value - priorValue;
        else
            value = FP::undefined();
        break;
    case OutputVariable::AccumulateRate:
        if (FP::defined(priorValue) &&
                FP::defined(value) &&
                FP::defined(priorTime) &&
                FP::defined(frameTime) &&
                frameTime > priorTime)
            value = (value - priorValue) / (frameTime - priorTime);
        else
            value = FP::undefined();
        break;
    }

    Variant::Root result(variable.calibration.apply(value));
    remap(variable.name, result);

    SequenceValue dv(SequenceName({}, "raw", variable.name), std::move(result), frameTime,
                     frameTime + config.front().timeout);

    if (realtimeEgress) {
        if (!haveEmittedRealtimeMeta) {
            haveEmittedRealtimeMeta = true;
            realtimeEgress->incomingData(buildLogMeta(frameTime));
        }

        realtimeEgress->incomingData(dv);

        if (FP::defined(realtimeStateTime) && realtimeStateTime != frameTime) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "F1"}, Variant::Root(Variant::Type::Flags), frameTime,
                                  frameTime + config.front().timeout));
        }
        realtimeStateTime = frameTime;
    }

    if (loggingEgress) {
        if (!haveEmittedLogMeta) {
            haveEmittedLogMeta = true;
            loggingMux.incoming(LogStream_Metadata, buildLogMeta(frameTime), loggingEgress);
        }

        if (FP::defined(loggingStateTime) && loggingStateTime < frameTime) {
            loggingMux.incoming(LogStream_State, SequenceValue({{}, "raw", "F1"},
                                                               Variant::Root(Variant::Type::Flags),
                                                               loggingStateTime, frameTime),
                                loggingEgress);
        }
        loggingStateTime = frameTime;

        loggingMux.advance(LogStream_Metadata, frameTime, loggingEgress);
        loggingMux.advance(LogStream_State, frameTime, loggingEgress);

        for (auto adv : availableStreams) {
            loggingMux.advance(adv, frameTime, loggingEgress);
        }

        for (auto &check : variableState) {
            if (check.second.streamID == vs.streamID)
                continue;
            if (!FP::defined(check.second.expiredAt)) {
                check.second.lastTime = FP::undefined();
                check.second.lastValue = FP::undefined();
                loggingMux.advance(check.second.streamID, frameTime, loggingEgress);
                continue;
            }
            if (check.second.expiredAt >= frameTime)
                continue;
            check.second.lastTime = FP::undefined();
            check.second.lastValue = FP::undefined();
            check.second.expiredAt = FP::undefined();
            loggingMux.advance(check.second.streamID, frameTime, loggingEgress);
        }
    } else {
        loggingStateTime = FP::undefined();
        for (const auto &check : variableState) {
            availableStreams.push_back(check.second.streamID);
        }
        variableState.clear();
        loggingMux.clear();
    }

    if (FP::defined(startTime) && loggingEgress) {
        dv.setStart(startTime);
        dv.setEnd(endTime);
        loggingMux.incoming(vs.streamID, std::move(dv), loggingEgress);
    }
}

void AcquireGenericPassive::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    lastValidTime = FP::undefined();
    loggingStateTime = FP::undefined();
    realtimeStateTime = FP::undefined();

    for (const auto &check : variableState) {
        availableStreams.push_back(check.second.streamID);
    }
    variableState.clear();
    loggingMux.clear();

    transientState.clear();

    sourceMetadataUpdated();
}

void AcquireGenericPassive::configurationAdvance(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    Q_ASSERT(!config.empty());
    auto oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.empty());

    if (oldSize == config.size())
        return;

    configurationChanged();
}

QString AcquireGenericPassive::OutputVariable::value(const QStringList &fields,
                                                     const QStringList &captures,
                                                     const std::unordered_map<
                                                             SequenceName::Component,
                                                             TransientState> &state) const
{
    QString firstPossible;
    for (auto field : this->fields) {
        if (static_cast<int>(field) >= fields.size())
            continue;
        const auto &v = fields[field];
        if (v.isEmpty())
            continue;
        switch (valueSearchBehavior) {
        case SearchFirstNonEmpty:
            return v;
        case SearchFirstDefined:
            if (firstPossible.isEmpty())
                firstPossible = v;
            if (mvc.isValid() && Util::exact_match(v, mvc))
                continue;
            return v;
        }
    }
    for (auto field : this->captures) {
        if (static_cast<int>(field) >= captures.size())
            continue;
        const auto &v = captures[field];
        if (v.isEmpty())
            continue;
        switch (valueSearchBehavior) {
        case SearchFirstNonEmpty:
            return v;
        case SearchFirstDefined:
            if (firstPossible.isEmpty())
                firstPossible = v;
            if (mvc.isValid() && Util::exact_match(v, mvc))
                continue;
            return v;
        }
    }
    {
        auto check = state.find(name);
        if (check != state.end()) {
            for (auto field : check->second.headerIndices) {
                if (static_cast<int>(field) >= fields.size())
                    continue;
                const auto &v = fields[field];
                if (v.isEmpty())
                    continue;
                switch (valueSearchBehavior) {
                case SearchFirstNonEmpty:
                    return v;
                case SearchFirstDefined:
                    if (firstPossible.isEmpty())
                        firstPossible = v;
                    if (mvc.isValid() && Util::exact_match(v, mvc))
                        continue;
                    return v;
                }
            }
        }
    }
    return firstPossible;
}

int AcquireGenericPassive::processVariable(const QStringList &fields,
                                           const QStringList &captures,
                                           double frameTime,
                                           const OutputVariable &variable)
{
    auto value = variable.value(fields, captures, transientState);

    if (value.isEmpty()) {
        switch (variable.missingBehavior) {
        case OutputVariable::MissingSkip:
            return -1;
        case OutputVariable::MissingSkipWithGap:
            introduceGap(variable, frameTime);
            return -1;
        case OutputVariable::MissingReturnInvalidRecord:
            return 1;
        case OutputVariable::MissingOutputMVC:
            variableUpdated(variable, frameTime, FP::undefined());
            return -1;
        }
    }

    if (variable.mvc.isValid() && Util::exact_match(value, variable.mvc)) {
        switch (variable.mvcBehavior) {
        case OutputVariable::MVCSkip:
            return -1;
        case OutputVariable::MVCSkipWithGap:
            introduceGap(variable, frameTime);
            return -1;
        case OutputVariable::MVCReturnInvalidRecord:
            return 2;
        case OutputVariable::MVCOutputMVC:
            variableUpdated(variable, frameTime, FP::undefined());
            return -1;
        }
    }

    bool ok = false;
    double d = value.toDouble(&ok);
    if (!ok) return 3;
    if (!FP::defined(d)) return 4;

    variableUpdated(variable, frameTime, d);
    return 0;
}

void AcquireGenericPassive::processHeader(const QStringList &headers,
                                          const OutputVariable &variable)
{
    std::vector<std::size_t> indices;
    for (const auto &pattern : variable.headers) {
        std::size_t index = 0;
        for (const auto &check : headers) {
            if (Util::exact_match(check, pattern)) {
                indices.push_back(index);
            }
            ++index;
        }
    }
    transientState[variable.name].headerIndices = std::move(indices);
}

int AcquireGenericPassive::processRecord(const Util::ByteView &line,
                                         double &frameTime,
                                         const Record &record)
{
    QString strLine(line.toQString());
    for (const auto &check : record.replacements) {
        QString result;
        int endOfLastMatch = 0;

        for (;;) {
            auto r = check.match.match(strLine, endOfLastMatch);
            if (!r.hasMatch())
                break;

            auto begin = r.capturedStart();
            auto end = r.capturedEnd();

            if (begin > endOfLastMatch) {
                result += strLine.mid(endOfLastMatch, (begin - endOfLastMatch));
            }

            result += NumberedSubstitution(r).apply(check.replace);

            endOfLastMatch = end;
            if (begin == end) {
                result += strLine.mid(endOfLastMatch, 1);
                endOfLastMatch++;
                if (endOfLastMatch >= strLine.length())
                    break;
            }
        }

        if (endOfLastMatch < strLine.length()) {
            result += strLine.mid(endOfLastMatch);
        }

        strLine = result;
    }

    bool headerMatch = false;
    if (record.header.isValid() && Util::exact_match(strLine, record.header)) {
        QStringList fields(CSV::split(strLine, record.separator, record.quote, record.quoteEscape));
        for (const auto &var : record.variables) {
            processHeader(fields, var);
        }
        headerMatch = true;
    }
    auto matched = record.matcher
                         .match(strLine, 0, QRegularExpression::NormalMatch,
                                QRegularExpression::AnchoredMatchOption);
    if (!matched.hasMatch())
        return headerMatch ? -2 : -1;
    if (matched.capturedLength() != strLine.length())
        return headerMatch ? -2 : -1;

    QStringList fields(CSV::split(strLine, record.separator, record.quote, record.quoteEscape));
    QStringList captures(matched.capturedTexts());

    QStringList timeFields;
    for (auto field : record.timeFields) {
        if (static_cast<int>(field) >= fields.size())
            continue;
        timeFields.append(fields[field]);
    }
    if (!timeFields.empty()) {
        double parsedTime = FP::undefined();
        try {
            parsedTime = TimeParse::parseListSingle(timeFields);
            if (FP::defined(parsedTime) &&
                    FP::defined(record.interval) &&
                    FP::defined(record.roundingLimit) &&
                    record.interval > 0.0 &&
                    parsedTime > 0.0) {
                double rounded = floor(parsedTime / record.interval + 0.5) * record.interval;
                if (fabs(rounded - parsedTime) < record.roundingLimit)
                    parsedTime = rounded;
            }
        } catch (TimeParsingException &tpe) {
            qCDebug(log) << "Error parsing time fields" << timeFields << ":"
                         << tpe.getDescription();
            if (record.requireValidTime)
                return 2;
        }

        if (!FP::defined(frameTime)) {
            /* Don't move backwards in time */
            if (FP::defined(lastValidTime) && FP::defined(parsedTime) && lastValidTime > parsedTime)
                return -1;
            frameTime = parsedTime;
            lateAdvanceTime = frameTime;
        }
    } else if (!record.timeCaptures.empty()) {
        FileDownloader::TimeExtractContext ctx;
        ctx.integrate(captures, record.timeCaptures);
        double reference = config.front().referenceTime;
        if (!FP::defined(reference))
            reference = frameTime;
        if (!FP::defined(reference))
            reference = lastValidTime;
        double parsedTime = ctx.extract(reference);

        if (!FP::defined(parsedTime)) {
            qCDebug(log) << "Error parsing time captures";
            if (record.requireValidTime)
                return 3;
        }

        if (!FP::defined(frameTime)) {
            /* Don't move backwards in time */
            if (FP::defined(lastValidTime) && FP::defined(parsedTime) && lastValidTime > parsedTime)
                return -1;
            frameTime = parsedTime;
            lateAdvanceTime = frameTime;
        }
    } else {
        if (record.requireValidTime)
            return 1;
    }
    if (!FP::defined(frameTime)) {
        frameTime = lastValidTime;
    } else {
        lastValidTime = frameTime;
    }

    bool anyValid = false;
    int index = -1;
    for (const auto &var : record.variables) {
        ++index;
        int code = processVariable(fields, captures, frameTime, var);
        if (code < 0)
            continue;
        if (code > 0)
            return (index + 1) * 10 + code;
        anyValid = true;
    }

    if (!anyValid) {
        if (headerMatch)
            return -2;
        if (record.requireValidVariable)
            return 4;
        return -1;
    }
    return 0;
}

int AcquireGenericPassive::processAny(const Util::ByteView &line, double &frameTime)
{
    Q_ASSERT(!config.empty());

    if (line.size() < 1)
        return 1;

    int index = -1;
    bool anyMatched = false;
    bool anyHeader = false;
    for (const auto &record : config.front().records) {
        ++index;
        int code = processRecord(line, frameTime, record);
        if (code < 0) {
            if (code == -2)
                anyHeader = true;
            continue;
        }
        if (code > 0)
            return (index + 1) * 10000 + code;

        while (static_cast<std::size_t>(index) >= autoprobeValidSeen.size()) {
            autoprobeValidSeen.push_back(false);
        }
        autoprobeValidSeen[index] = true;

        if (!config.front().allowMultipleMatches)
            return 0;
        anyMatched = true;
    }

    if (!anyMatched) {
        if (anyHeader)
            return -1;
        if (config.front().allowUnmatchedLines)
            return -1;
        return 9;
    }

    if (FP::defined(lateAdvanceTime)) {
        configurationAdvance(lateAdvanceTime);
        lateAdvanceTime = FP::undefined();
    }
    return 0;
}

bool AcquireGenericPassive::autoprobeIsDone() const
{
    if (autoprobeValidCount < config.front().autoprobeRequiredRecords)
        return false;
    if (config.front().autoprobeRequireAllRecords) {
        for (std::size_t index = 0, max = config.front().records.size(); index < max; ++index) {
            if (index >= autoprobeValidSeen.size())
                return false;
            if (!autoprobeValidSeen[index])
                return false;
        }
    }
    return true;
}

void AcquireGenericPassive::invalidateLogValues(double frameTime)
{
    autoprobeValidCount = 0;
    autoprobeValidSeen.clear();

    for (int i = 0; i < LogStream_TOTAL; i++) {
        if (loggingEgress && FP::defined(frameTime))
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    for (auto &vs : variableState) {
        vs.second.lastTime = FP::undefined();
        if (loggingEgress && FP::defined(frameTime))
            loggingMux.advance(vs.second.streamID, frameTime, loggingEgress);
    }
    loggingMux.clear();
    loggingLost(frameTime);
}

void AcquireGenericPassive::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_INITIALIZE:
    case RESP_AUTOPROBE_WAIT: {
        int code = processAny(frame, frameTime);
        if (code == 0) {
            ++autoprobeValidCount;
            if (autoprobeIsDone()) {
                qCDebug(log) << "Autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                timeoutAt(frameTime + config.front().timeout);

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("AutoprobeWait");
                event(frameTime, QObject::tr("Autoprobe succeeded."), false, info);
            } else {
                timeoutAt(frameTime + config.front().timeout);

                responseState = RESP_AUTOPROBE_WAIT;
            }
        } else if (code > 0) {
            qCDebug(log) << "Autoprobe failed at" << Logging::time(frameTime) << ":" << frame
                        << "rejected with code" << code;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            responseState = RESP_WAIT;

            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_WAIT:
    case RESP_INITIALIZE: {
        int code = processAny(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Comms established at" << Logging::time(frameTime);

            responseState = RESP_RUN;
            generalStatusUpdated();

            timeoutAt(frameTime + config.front().timeout);
            ++autoprobeValidCount;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("Wait");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_RUN: {
        int code = processAny(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + config.front().timeout);
            ++autoprobeValidCount;
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            responseState = RESP_WAIT;
            discardData(frameTime + 0.5, 1);
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            info.hash("ResponseState").setString("Run");
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        }
        break;
    }

    default:
        break;
    }
}

void AcquireGenericPassive::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    switch (responseState) {
    case RESP_RUN: {
        qCDebug(log) << "Timeout in unpolled mode at" << Logging::time(frameTime);

        responseState = RESP_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        discardData(frameTime + 2.0, 1);
        generalStatusUpdated();

        Variant::Write info = Variant::Write::empty();
        info.hash("ResponseState").setString("Run");
        event(frameTime, QObject::tr("Timeout waiting for report.  Communications dropped."), true,
              info);

        invalidateLogValues(frameTime);
        break;
    }

    case RESP_AUTOPROBE_WAIT:
        qCDebug(log) << "Timeout waiting for autoprobe records at" << Logging::time(frameTime);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (controlStream != NULL)
            controlStream->resetControl();
        discardData(frameTime + 2.0, 1);
        responseState = RESP_WAIT;
        invalidateLogValues(frameTime);
        break;

    case RESP_INITIALIZE:
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + 4.0);
        responseState = RESP_WAIT;
        autoprobeValidCount = 0;
        autoprobeValidSeen.clear();
        break;

    case RESP_AUTOPROBE_INITIALIZE:
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + 4.0);
        responseState = RESP_AUTOPROBE_WAIT;
        autoprobeValidCount = 0;
        autoprobeValidSeen.clear();
        break;

    default:
        discardData(frameTime + 0.5, 1);
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireGenericPassive::discardDataCompleted(double frameTime)
{
    Q_UNUSED(frameTime);
}

void AcquireGenericPassive::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frameTime);
    Q_UNUSED(frame);
}

AcquisitionInterface::AutoprobeStatus AcquireGenericPassive::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

Variant::Root AcquireGenericPassive::getSourceMetadata()
{
    PauseLock paused(*this);
    Q_ASSERT(!config.empty());
    return config.front().instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireGenericPassive::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

void AcquireGenericPassive::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_RUN:
        if (autoprobeIsDone()) {
            /* Already running, just notify that we succeeded. */
            qCDebug(log) << "Interactive autoprobe requested with communications established at"
                         << Logging::time(time);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            break;
        }

        /* Fall through */
    default:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        autoprobeValidCount = 0;
        autoprobeValidSeen.clear();

        timeoutAt(time + config.front().timeout * 2.0);
        discardData(time + 0.5, 1);
        responseState = RESP_AUTOPROBE_WAIT;
        generalStatusUpdated();
        break;
    }
}

void AcquireGenericPassive::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    autoprobeValidCount = 0;
    autoprobeValidSeen.clear();

    timeoutAt(time + config.front().timeout * 3.0);
    discardData(time + 0.5, 1);
    responseState = RESP_AUTOPROBE_WAIT;
}

void AcquireGenericPassive::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + config.front().timeout);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state " << responseState << " to interactive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from passive autoprobe to interactive acquisition at"
                     << Logging::time(time);

        responseState = RESP_WAIT;
        break;
    }
}

void AcquireGenericPassive::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + config.front().timeout);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from passive autoprobe to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_WAIT;
        break;
    }
}

static const quint16 serializedStateVersion = 2;

void AcquireGenericPassive::serializeState(QDataStream &stream)
{
    PauseLock paused(*this);

    stream << serializedStateVersion;

    stream << static_cast<quint32>(variableState.size());
    for (const auto &vs : variableState) {
        stream << vs.first;
        stream << vs.second.lastTime << vs.second.lastValue;
    }
}

void AcquireGenericPassive::deserializeState(QDataStream &stream)
{
    quint16 version = 0;
    stream >> version;
    if (version != serializedStateVersion)
        return;

    PauseLock paused(*this);

    quint32 n = 0;
    stream >> n;
    restoreState.clear();
    for (quint32 i = 0; i < n; i++) {
        SequenceName::Component name;
        stream >> name;
        RestoredState rs;
        stream >> rs.time >> rs.value;
        restoreState.emplace(std::move(name), std::move(rs));
    }
}

AcquisitionInterface::AutomaticDefaults AcquireGenericPassive::getDefaults()
{
    AutomaticDefaults result;
    result.name = "X$2";
    result.setSerialN81(9600);
    return result;
}


ComponentOptions AcquireGenericPassiveComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());

    options.add("basetime",
                new ComponentOptionSingleTime(tr("basetime", "name"), tr("File time reference"),
                                              tr("The time reference used when parsing partial times in files."),
                                              ""));

    LineIngressWrapper::addOptions(options);
    return options;
}

ComponentOptions AcquireGenericPassiveComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireGenericPassiveComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireGenericPassiveComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireGenericPassiveComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options);
}

QList<ComponentExample> AcquireGenericPassiveComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireGenericPassiveComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                       const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(
            new AcquireGenericPassive(options, loggingContext));
}

static bool isValidVariable(const QString &name, const Variant::Read &config)
{
    if (name.isEmpty())
        return false;

    std::vector<std::size_t> fields;
    std::vector<std::size_t> captures;
    std::vector<QRegularExpression> headers;
    setFieldsAndCaptures(config, fields, captures);
    setHeaders(config, headers);
    if (fields.empty() && captures.empty() && headers.empty())
        return false;

    return true;
}

static bool isValidRecord(const QString &pattern, const Variant::Read &config)
{
    if (!pattern.isEmpty()) {
        class CaptureSubstitution : public TextSubstitution {
        public:
            CaptureSubstitution()
            { }

            virtual ~CaptureSubstitution() = default;

        protected:
            QString substitution(const QStringList &elements) const override
            {
                QList<FileDownloader::TimeCapture> captures;
                {
                    int index = 0;
                    if (index >= elements.size())
                        return {};
                    auto type = elements[index++].toLower();

                    if (type == "value") {
                        QString pattern = R"([+-]?\d+(?:\.\d*)?(?:[Ee][+-]?\d+)?)";
                        if (index < elements.size())
                            pattern = elements[index++];
                        if (!pattern.isEmpty()) {
                            captures.push_back(FileDownloader::TimeCapture::Ignore);
                            return "(" + pattern + ")";
                        }
                        return {};
                    }
                }
                return FileDownloader::getTimeCapture(elements, captures);
            }
        };

        QRegularExpression reCheck(CaptureSubstitution().apply(pattern));
        if (!reCheck.isValid())
            return false;
    }

    {
        auto children = config["Variables"].toChildren();
        for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
            if (!add.value().exists())
                continue;
            QString name = add.value().hash("Name").toQString();
            if (name.isEmpty())
                name = QString::fromStdString(add.stringKey());
            if (isValidVariable(name, add.value()))
                return true;
        }
    }

    return false;
}

static bool isValidConfiguration(const ValueSegment &config)
{
    switch (config["Records"].getType()) {
    case Variant::Type::Array: {
        for (auto add : config["Records"].toArray()) {
            if (isValidRecord(add.hash("Match").toQString(), add))
                return true;
        }
        break;
    }

    case Variant::Type::Hash:
        if (isValidRecord(config["Records/Match"].toQString(), config["Records"]))
            return true;
        break;

    default:
        break;
    }

    return false;
}

static bool isValidConfiguration(const ValueSegment::Transfer &config)
{
    for (const auto &check : config) {
        if (isValidConfiguration(check))
            return true;
    }
    return false;
}

static std::string assembleLoggingCategory(const std::string &loggingContext)
{
    std::string result = "cpd3.acquisition.instrument.genericpassive";
    if (!loggingContext.empty()) {
        result += '.';
        result += loggingContext;
    }
    return result;
}

std::unique_ptr<
        AcquisitionInterface> AcquireGenericPassiveComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{
    if (!isValidConfiguration(config)) {
        qCWarning(QLoggingCategory(assembleLoggingCategory(loggingContext).data()))
            << "Configuration will not result in a valid acquisition interface";
    }
    return std::unique_ptr<AcquisitionInterface>(new AcquireGenericPassive(config, loggingContext));
}

std::unique_ptr<AcquisitionInterface> AcquireGenericPassiveComponent::createAcquisitionAutoprobe(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    if (config.empty())
        return {};
    if (!isValidConfiguration(config)) {
        qCWarning(QLoggingCategory(assembleLoggingCategory(loggingContext).data()))
            << "Autoprobe creation rejected because the configuration will not result in a valid acquisition interface";
        return {};
    }
    std::unique_ptr<AcquireGenericPassive> i(new AcquireGenericPassive(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireGenericPassiveComponent::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    if (!isValidConfiguration(config)) {
        qCWarning(QLoggingCategory(assembleLoggingCategory(loggingContext).data()))
            << "Configuration will not result in a valid acquisition interface";
    }
    std::unique_ptr<AcquireGenericPassive> i(new AcquireGenericPassive(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
