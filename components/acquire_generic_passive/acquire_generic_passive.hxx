/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREGENERICPASSIVE_H
#define ACQUIREGENERICPASSIVE_H

#include "core/first.hxx"

#include <vector>
#include <deque>
#include <unordered_map>

#include <QtGlobal>
#include <QObject>
#include <QRegularExpression>
#include <QStringList>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "acquisition/spancheck.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "transfer/download.hxx"
#include "core/number.hxx"

class AcquireGenericPassive : public CPD3::Acquisition::FramedInstrument {
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Waiting for enough valid records to confirm autoprobe success */
        RESP_AUTOPROBE_WAIT,

        /* Waiting for a valid record to start acquisition */
        RESP_WAIT,

        /* Acquiring data in passive mode */
        RESP_RUN,

        /* Same as wait, but discard the first timeout */
        RESP_INITIALIZE,

        /* For autoprobing */
        RESP_AUTOPROBE_INITIALIZE,
    };
    ResponseState responseState;
    int autoprobeValidCount;
    std::vector<bool> autoprobeValidSeen;
    double lastValidTime;

    enum LogStreamID {
        LogStream_Metadata = 0, LogStream_State,

        LogStream_TOTAL, LogStream_RecordBaseStart = LogStream_State + 1,
    };
    CPD3::Data::StaticMultiplexer loggingMux;
    double loggingStateTime;
    double realtimeStateTime;
    std::deque<std::size_t> availableStreams;

    struct VariableState {
        std::size_t streamID;
        double expiredAt;

        double lastTime;
        double lastValue;
    };
    std::unordered_map<CPD3::Data::SequenceName::Component, VariableState> variableState;

    struct RestoredState {
        double time;
        double value;
    };
    std::unordered_map<CPD3::Data::SequenceName::Component, RestoredState> restoreState;

    struct TransientState {
        std::vector<std::size_t> headerIndices;
    };
    std::unordered_map<CPD3::Data::SequenceName::Component, TransientState> transientState;

    struct OutputVariable {
        CPD3::Data::SequenceName::Component name;
        CPD3::Data::Variant::Root metadata;

        CPD3::Calibration calibration;

        enum {
            AccumulateDisabled, AccumulateDifference, AccumulateRate,
        } accumulation;

        std::vector<std::size_t> fields;
        std::vector<std::size_t> captures;
        std::vector<QRegularExpression> headers;

        double maximumAge;

        enum {
            SearchFirstNonEmpty, SearchFirstDefined,
        } valueSearchBehavior;

        enum {
            MissingSkip, MissingSkipWithGap, MissingReturnInvalidRecord, MissingOutputMVC,
        } missingBehavior;

        QRegularExpression mvc;
        enum {
            MVCSkip, MVCSkipWithGap, MVCReturnInvalidRecord, MVCOutputMVC,
        } mvcBehavior;

        QString value(const QStringList &fields,
                      const QStringList &captures,
                      const std::unordered_map<CPD3::Data::SequenceName::Component,
                                               TransientState> &variableState) const;
    };

    struct Record {
        struct Replacement {
            QRegularExpression match;
            QString replace;
        };
        std::vector<Replacement> replacements;

        QRegularExpression matcher;
        QRegularExpression header;
        bool requireValidTime;
        bool requireValidVariable;

        double interval;
        double roundingLimit;

        std::vector<std::size_t> timeFields;
        QList<CPD3::Transfer::FileDownloader::TimeCapture> timeCaptures;

        std::vector<OutputVariable> variables;

        QString separator;
        QString quote;
        QString quoteEscape;
    };

    friend class Configuration;

    class Configuration {
        double start;
        double end;

        void addVariable(const CPD3::Data::SequenceName::Component &name,
                         const CPD3::Data::Variant::Read &config,
                         std::vector<AcquireGenericPassive::OutputVariable> &output);

        void addRecord(const QString &pattern, const CPD3::Data::Variant::Read &config);

    public:
        std::vector<Record> records;
        double timeout;

        double referenceTime;

        bool allowMultipleMatches;
        bool allowUnmatchedLines;

        int autoprobeRequiredRecords;
        bool autoprobeRequireAllRecords;

        CPD3::Data::Variant::Root instrumentMeta;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under,
                      const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    std::deque<Configuration> config;

    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    double lateAdvanceTime;

    void setDefaultInvalid();

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    void introduceGap(const OutputVariable &variable, double frameTime);

    VariableState &acquireVariable(const OutputVariable &variable);

    void variableUpdated(const OutputVariable &variable, double frameTime, double value);

    void configurationChanged();

    void configurationAdvance(double frameTime);

    int processVariable(const QStringList &fields,
                        const QStringList &captures,
                        double frameTime,
                        const OutputVariable &variable);

    void processHeader(const QStringList &headers, const OutputVariable &variable);

    int processRecord(const CPD3::Util::ByteView &line, double &frameTime, const Record &record);

    int processAny(const CPD3::Util::ByteView &line, double &frameTime);

    bool autoprobeIsDone() const;

    void invalidateLogValues(double frameTime);

public:
    AcquireGenericPassive(const CPD3::Data::ValueSegment::Transfer &config,
                          const std::string &loggingContext);

    AcquireGenericPassive(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireGenericPassive();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    void serializeState(QDataStream &stream) override;

    void deserializeState(QDataStream &stream) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    virtual void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingInstrumentTimeout(double time);

    virtual void discardDataCompleted(double time);
};

class AcquireGenericPassiveComponent
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_generic_passive"
                              FILE
                              "acquire_generic_passive.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
