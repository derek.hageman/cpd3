/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>
#include <QDateTime>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/timeutils.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;
    QDateTime time;

    enum {
        Record_Combined, Record_CombinedTime, Record_A, Record_B, Record_Accumulate,
    } record;

    double A;
    double B;

    ModelInstrument() : unpolledRemaining(0),
                        time(QDate(2013, 2, 3), QTime(0, 0, 0), Qt::UTC),
                        record(Record_Combined)
    {
        A = 5.0;
        B = 6.0;
    }

    void outputTime(const QDateTime &time)
    {
        outgoing.append(QByteArray::number(time.date().year()).rightJustified(4, '0'));
        outgoing.append('-');
        outgoing.append(QByteArray::number(time.date().month()).rightJustified(2, '0'));
        outgoing.append('-');
        outgoing.append(QByteArray::number(time.date().day()).rightJustified(2, '0'));
        outgoing.append('T');
        outgoing.append(QByteArray::number(time.time().hour()).rightJustified(2, '0'));
        outgoing.append(':');
        outgoing.append(QByteArray::number(time.time().minute()).rightJustified(2, '0'));
        outgoing.append(':');
        outgoing.append(QByteArray::number(time.time().second()).rightJustified(2, '0'));
        outgoing.append('Z');
    }

    void advance(double seconds)
    {
        unpolledRemaining -= seconds;
        while (unpolledRemaining <= 0.0) {
            unpolledRemaining += 1.0;

            switch (record) {
            case Record_CombinedTime:
                outputTime(time);
                outgoing.append(',');
            case Record_Combined:
                outgoing.append(QByteArray::number(A, 'f', 2));
                outgoing.append(',');
                outgoing.append(QByteArray::number(B, 'f', 2));
                outgoing.append('\n');
                break;
            case Record_A:
                outgoing.append("A,");
                outgoing.append(QByteArray::number(A, 'f', 2));
                outgoing.append('\n');
                record = Record_B;
                break;
            case Record_B:
                outgoing.append("B,");
                outgoing.append(QByteArray::number(B, 'f', 2));
                outgoing.append('\n');
                record = Record_A;
                break;
            case Record_Accumulate:
                outputTime(time);
                outgoing.append(',');
                outgoing.append(QByteArray::number(time.toTime_t()));
                outgoing.append('\n');
                break;
            }

            time = time.addSecs(1);
        }
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("A", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("B", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("A"))
            return false;
        if (!stream.checkContiguous("B"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream, const ModelInstrument &model,
                     double time = FP::undefined(),
                     const Calibration &calA = Calibration(),
                     const Calibration &calB = Calibration())
    {
        if (!stream.hasAnyMatchingValue("A", Variant::Root(calA.apply(model.A)), time))
            return false;
        if (!stream.hasAnyMatchingValue("B", Variant::Root(calB.apply(model.B)), time))
            return false;
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_generic_passive"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_generic_passive"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Records/Variables/A/Fields/#0"].setInt64(1);
        cv["Records/Variables/B/Fields/#0"].setInt64(2);
        cv["Autoprobe/RequireAllRecords"].setBool(true);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        logging.checkAscending();
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));

    }

    void passiveAutoprobeMultiple()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Records/#0/Match"].setString(".+");
        cv["Records/#0/Variables/A/Fields/#0"].setInt64(1);
        cv["Records/#1/Match"].setString(".+");
        cv["Records/#1/Variables/B/Fields/#0"].setInt64(2);
        cv["AllowMultipleMatches"].setBool(true);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        logging.checkAscending();
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));

    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Records/#0/Match"].setString("(\\d{4})([\\dTZ:-]+),[^,]+,(\\S+)");
        cv["Records/#0/Time/Fields/#0"].setInt64(1);
        cv["Records/#0/Time/RequireValid"].setBool(true);
        cv["Records/#0/Variables/A/Fields/#0"].setInt64(2);
        cv["Records/#0/Variables/B/Captures/#0"].setInt64(3);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.record = ModelInstrument::Record_CombinedTime;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(1.0);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        logging.checkAscending();
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));

    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        Calibration calB(QVector<double>() << 0.25 << 0.9);
        cv["Records/#0/Match"].setString("A(.+)");
        cv["Records/#0/Variables/A/Fields/#0"].setInt64(2);
        cv["Records/#1/Match"].setString("B,(\\S+)");
        cv["Records/#1/Variables/B/Captures/#0"].setInt64(1);
        Variant::Composite::fromCalibration(cv["Records/#1/Variables/B/Calibration"], calB);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.record = ModelInstrument::Record_A;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 100; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        logging.checkAscending();
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));

        QVERIFY(checkValues(logging, instrument, FP::undefined(), Calibration(), calB));
        QVERIFY(checkValues(realtime, instrument, FP::undefined(), Calibration(), calB));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        Calibration calA(QVector<double>() << 0.5 << 1.1);
        cv["Records/#0/Match"].setString(
                R"(${CAP|\\d\{4\}}-${CAP|\\d\{2\}}-${CAP|\\d\{2\}}T${CAP|\\d\{2\}}:${CAP|\\d\{2\}}:${CAP|\\d\{2\}}Z,.+)");
        cv["Records/#0/Time/RequireValid"].setBool(true);
        cv["Records/#0/Variables/A/Fields/#0"].setInt64(2);
        Variant::Composite::fromCalibration(cv["Records/#0/Variables/A/Calibration"], calA);
        cv["Records/#0/Variables/B/Fields/#0"].setInt64(3);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.record = ModelInstrument::Record_CombinedTime;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 100; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        logging.checkAscending();
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));

        QVERIFY(checkValues(logging, instrument, FP::undefined(), calA));
        QVERIFY(checkValues(realtime, instrument, FP::undefined(), calA));

    }

    void autoprobeReject()
    {
        ValueSegment::Transfer config;

        QVERIFY(!component->createAcquisitionAutoprobe(config));

        Variant::Root cv;
        cv["AllowMultipleMatches"].setBool(true);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);
        QVERIFY(!component->createAcquisitionAutoprobe(config));

        config.clear();
        cv["Records/#0/Match"].setString(
                "(\\d{4})-(\\d{2})-(\\d{2})T(\\d{2}):(\\d{2}):(\\d{2})Z,.+");
        cv["Records/#0/Time/Captures/#0"].setInt64(1);
        cv["Records/#0/Time/Captures/#1"].setInt64(2);
        cv["Records/#0/Time/Captures/#2"].setInt64(3);
        cv["Records/#0/Time/Captures/#3"].setInt64(4);
        cv["Records/#0/Time/Captures/#4"].setInt64(5);
        cv["Records/#0/Time/Captures/#5"].setInt64(6);
        cv["Records/#0/Time/RequireValid"].setBool(true);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);
        QVERIFY(!component->createAcquisitionAutoprobe(config));

        config.clear();
        Variant::Composite::fromCalibration(cv["Records/#0/Variables/A/Calibration"],
                                            Calibration());
        config.emplace_back(FP::undefined(), FP::undefined(), cv);
        QVERIFY(!component->createAcquisitionAutoprobe(config));
    }

    void accumulator()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Records/#0/Match"].setString("${ISOTIME},.+");
        cv["Records/#0/Time/RequireValid"].setBool(true);
        cv["Records/#0/Variables/V1/Fields/#0"].setInt64(2);
        cv["Records/#0/Variables/V1/Accumulation"].setString("Difference");
        cv["Records/#0/Variables/V2/Fields/#0"].setInt64(2);
        cv["Records/#0/Variables/V2/Accumulation"].setString("Rate");
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.record = ModelInstrument::Record_Accumulate;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 50; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface.reset();

            QVERIFY(logging.hasAnyMatchingValue("V1", Variant::Root(1.0)));
            QVERIFY(logging.hasAnyMatchingValue("V2", Variant::Root(1.0)));
            QVERIFY(realtime.hasAnyMatchingValue("V1", Variant::Root(1.0)));
            QVERIFY(realtime.hasAnyMatchingValue("V2", Variant::Root(1.0)));

            logging.reset();
            realtime.reset();

            interface = component->createAcquisitionPassive(config);
            control.attach(interface.get());
            interface->start();
            interface->setLoggingEgress(&logging);
            interface->setRealtimeEgress(&realtime);
            interface->setPersistentEgress(&persistent);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }
        }

        for (int i = 0; i < 50; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        logging.checkAscending();
        QVERIFY(checkContiguous(logging));

        QVERIFY(logging.hasAnyMatchingValue("V1", Variant::Root(1.0)));
        QVERIFY(logging.hasAnyMatchingValue("V2", Variant::Root(1.0)));
        QVERIFY(realtime.hasAnyMatchingValue("V1", Variant::Root(1.0)));
        QVERIFY(realtime.hasAnyMatchingValue("V2", Variant::Root(1.0)));

    }

    void timeCorruption()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Records/#0/Match"].setString("${ISOTIME},.+");
        cv["Records/#0/Time/RequireValid"].setBool(true);
        cv["Records/#0/Variables/A/Fields/#0"].setInt64(2);
        cv["Records/#0/Variables/A/MaximumAge"].setReal(60);
        cv["Records/#0/Variables/A/Metadata/*dDescription"].setString("Stuff");
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        StreamCapture values;

        ComponentOptions options;
        LineIngressWrapper::addOptions(options, true);
        std::unique_ptr<LineIngressWrapper>
                ingress(LineIngressWrapper::create(std::move(interface), options, true));
        ingress->setEgress(&values);
        ingress->start();

        ingress->incomingData("2013-06-28T21:20:00Z,1.0\n");
        ingress->incomingData("2013-06-28T21:21:00Z,1.0\n");
        ingress->incomingData("2013-06-28T21:22:00Z,1.0\n");
        ingress->incomingData("2013-06-28T21:23:00Z,1.0\n");
        ingress->incomingData("2013-06-28T20:21:00Z,1.0\n");
        ingress->incomingData("2013-06-28T20:22:00Z,1.0\n");
        ingress->incomingData("2013-06-28T21:24:00Z,1.0\n");
        ingress->incomingData("2013-06-28T21:25:00Z,1.0\n");
        ingress->endData();


        ingress->wait();
        ingress.reset();

        QVERIFY(!values.values().empty());
    }

    void timeExpire()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Records/#0/Match"].setString("${ISOTIME},.+");
        cv["Records/#0/Time/RequireValid"].setBool(true);
        cv["Records/#0/Variables/A/Fields/#0"].setInt64(2);
        cv["Records/#0/Variables/A/MaximumAge"].setReal(60);
        cv["Records/#0/Variables/A/Metadata/*dDescription"].setString("Stuff");
        cv["Records/#0/Variables/B/Fields/#0"].setInt64(2);
        cv["Records/#0/Variables/B/MaximumAge"].setReal(60);
        cv["Records/#0/Variables/B/Metadata/*dDescription"].setString("Stuff");
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        StreamCapture values;

        ComponentOptions options;
        LineIngressWrapper::addOptions(options, true);
        std::unique_ptr<LineIngressWrapper>
                ingress(LineIngressWrapper::create(std::move(interface), options, true));
        ingress->setEgress(&values);
        ingress->start();

        ingress->incomingData("2013-06-28T21:20:00Z,1.0\n");
        ingress->incomingData("2013-06-28T21:21:00Z,1.0\n");
        ingress->incomingData("2013-06-28T21:22:00Z,1.0\n");
        ingress->incomingData("2013-06-28T21:24:00Z,1.0\n");
        ingress->incomingData("2013-06-28T21:25:00Z,1.0\n");
        ingress->endData();


        ingress->wait();
        ingress.reset();

        QVERIFY(!values.values().empty());
    }

    void headers()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Records/Variables/A/Headers/#0"].setString("FieldA");
        cv["Records/Variables/B/Headers/#0"].setString("FieldB");
        cv["Records/Substitutions/#0/Match"].setString("\\s+");
        cv["Records/Substitutions/#0/Replacement"].setString("");
        cv["Records/Header/Match"].setString("Field.*");
        cv["Records/Match"].setString("\\d.*");
        cv["Autoprobe/RequireAllRecords"].setBool(true);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        interface->incomingData("FieldA, FieldB\n", control.time());

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        logging.checkAscending();
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
