/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_maycomm_tdl.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;


AcquireMaycommTDL::Configuration::Configuration() : start(FP::undefined()), end(FP::undefined())
{ }

AcquireMaycommTDL::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s), end(e)
{
    Q_UNUSED(other);
}

AcquireMaycommTDL::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s), end(e)
{
    setFromSegment(other);
}

AcquireMaycommTDL::Configuration::Configuration(const Configuration &under,
                                                const ValueSegment &over,
                                                double s,
                                                double e) : start(s), end(e)
{
    Q_UNUSED(under);
    setFromSegment(over);
}

void AcquireMaycommTDL::Configuration::setFromSegment(const ValueSegment &config)
{
    Q_UNUSED(config);

}


void AcquireMaycommTDL::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("MayComm");
    instrumentMeta["Model"].setString("TDL");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
}


AcquireMaycommTDL::AcquireMaycommTDL(const ValueSegment::Transfer &configData,
                                     const std::string &loggingContext) : FramedInstrument("tdl",
                                                                                           loggingContext),
                                                                          lastRecordTime(
                                                                                  FP::undefined()),
                                                                          autoprobeStatus(
                                                                                  AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                          responseState(RESP_WAIT),
                                                                          autoprobeValidRecords(0)
{
    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireMaycommTDL::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void AcquireMaycommTDL::setToInteractive()
{ responseState = RESP_INITIALIZE; }


ComponentOptions AcquireMaycommTDLComponent::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireMaycommTDL::AcquireMaycommTDL(const ComponentOptions &options,
                                     const std::string &loggingContext) : FramedInstrument("tdl",
                                                                                           loggingContext),
                                                                          lastRecordTime(
                                                                                  FP::undefined()),
                                                                          autoprobeStatus(
                                                                                  AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                          responseState(RESP_WAIT),
                                                                          autoprobeValidRecords(0)
{
    Q_UNUSED(options);
    setDefaultInvalid();
    config.append(Configuration());
}

AcquireMaycommTDL::~AcquireMaycommTDL()
{
}


void AcquireMaycommTDL::logValue(double startTime,
                                 double endTime,
                                 SequenceName::Component name,
                                 Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress) return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireMaycommTDL::realtimeValue(double time,
                                      SequenceName::Component name,
                                      Variant::Root &&value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

SequenceValue::Transfer AcquireMaycommTDL::buildLogMeta(double time)
{
    Q_ASSERT(!config.isEmpty());
    Q_UNUSED(time);
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_maycomm_tdl");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    result.emplace_back(SequenceName({}, "raw_meta", "X"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.00");
    result.back().write().metadataReal("Units").setString("g/m\xC2\xB3");
    result.back().write().metadataReal("Description").setString("Water concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Pressure"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "T"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Temperature"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

    return result;
}

SequenceValue::Transfer AcquireMaycommTDL::buildRealtimeMeta(double)
{ return SequenceValue::Transfer(); }

static bool validDigit(char c)
{ return (c >= '0' && c <= '9'); }

static bool verifyField(const QByteArray &line)
{
    for (int i = 0; i < line.length(); i++) {
        if (i == line.length() - 3 && line.at(i) != '.') {
            return false;
        } else if (i != line.length() - 3) {
            if (i == 0 && line.at(i) == '-') {
                continue;
            } else if (!validDigit(line.at(i))) {
                return false;
            };
        }
    }
    return true;
}

static bool onlyContainsInts(const QByteArray line)
{
    for (int i = 0; i < line.size(); i++) {
        if (!validDigit(line.at(i))) {
            return false;
        }
    }
    return true;
}

int AcquireMaycommTDL::processRecord(Util::ByteView line, double startTime, double endTime)
{

    bool ok = false;
    if (line.size() < 7) return 1;
    auto raw = line.mid(0, 7).toQByteArray().trimmed();
    line = line.mid(7);
    if (!verifyField(raw)) return 2;
    Variant::Root X(raw.toDouble(&ok));
    if (!ok) return 3;
    if (!FP::defined(X.read().toDouble()) || X.read().toDouble() < -10) return 4;
    remap("X", X);

    if (line.size() < 8) return 5;
    raw = line.mid(1, 7).toQByteArray().trimmed();
    line = line.mid(8);
    if (!verifyField(raw)) return 6;
    Variant::Root P(raw.toDouble(&ok));
    if (!ok) return 7;
    if (!FP::defined(P.read().toDouble()) || P.read().toDouble() < 10 || P.read().toDouble() > 2000)
        return 8;
    remap("P", P);

    if (line.size() < 7) return 9;
    raw = line.mid(1, 6).toQByteArray().trimmed();
    line = line.mid(7);
    if (!verifyField(raw)) return 10;
    Variant::Root T(raw.toDouble(&ok));
    if (!ok) return 11;
    if (!FP::defined(T.read().toDouble()) ||
            T.read().toDouble() < -150 ||
            T.read().toDouble() > 100)
        return 12;
    remap("T", T);

    if (!line.empty()) {
        QList<QByteArray> fields(line.toQByteArray().simplified().split(' '));

        if (fields.size() == 4) {
            for (int i = 0; i < fields.size(); i++) {
                if (!onlyContainsInts(fields.at(i))) {
                    return 200 + i;
                }
            }
        } else {
            return 100;
        }
    }

    if (!haveEmittedLogMeta && loggingEgress && FP::defined(startTime)) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(endTime);
        Util::append(buildRealtimeMeta(endTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    logValue(startTime, endTime, "X", std::move(X));
    logValue(startTime, endTime, "P", std::move(P));
    logValue(startTime, endTime, "T", std::move(T));

    ++autoprobeValidRecords;
    return 0;
}


void AcquireMaycommTDL::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    double startTime;
    if (!FP::defined(lastRecordTime)) {
        startTime = frameTime - 1.0;
        lastRecordTime = frameTime;
    } else {
        startTime = lastRecordTime;
        if (lastRecordTime != frameTime)
            lastRecordTime = frameTime;
    }

    {
        Q_ASSERT(!config.isEmpty());

        if (!Range::intersectShift(config, frameTime)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.isEmpty());
    }

    switch (responseState) {
    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE: {
        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            if (autoprobeValidRecords > 10) {
                qCDebug(log) << "Autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                timeoutAt(frameTime + 3.0);

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("AutoprobeWait");
                event(frameTime, QObject::tr("Autoprobe succeeded."), false, info);
            } else {
                timeoutAt(frameTime + 3.0);

                responseState = RESP_AUTOPROBE_WAIT;
            }
        } else {
            qCDebug(log) << "Autoprobe failed at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            responseState = RESP_WAIT;
            autoprobeValidRecords = 0;

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    case RESP_WAIT:
    case RESP_INITIALIZE:
        if (processRecord(frame, startTime, frameTime) == 0) {
            qCDebug(log) << "Comms established at" << Logging::time(frameTime);

            responseState = RESP_RUN;
            generalStatusUpdated();

            timeoutAt(frameTime + 3.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("Wait");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else {
            autoprobeValidRecords = 0;

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;

    case RESP_RUN: {
        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + 3.0);
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            responseState = RESP_WAIT;
            discardData(frameTime + 0.5, 1);
            generalStatusUpdated();
            Variant::Write info = Variant::Write::empty();
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            info.hash("ResponseState").setString("Run");
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            autoprobeValidRecords = 0;

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        } else {
            autoprobeValidRecords = 0;
        }
        break;
    }

    default:
        break;
    }
}

void AcquireMaycommTDL::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    switch (responseState) {
    case RESP_RUN: {
        qCDebug(log) << "Timeout in unpolled mode at" << Logging::time(frameTime);

        if (controlStream != NULL)
            controlStream->resetControl();
        responseState = RESP_WAIT;
        discardData(frameTime + 2.0, 1);
        generalStatusUpdated();
        Variant::Write info = Variant::Write::empty();
        info.hash("ResponseState").setString("Run");
        event(frameTime, QObject::tr("Timeout waiting for report.  Communications dropped."), true,
              info);

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;
    }

    case RESP_AUTOPROBE_WAIT:
        qCDebug(log) << "Timeout waiting for autoprobe records" << Logging::time(frameTime);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (controlStream != NULL)
            controlStream->resetControl();
        discardData(frameTime + 2.0, 1);
        responseState = RESP_WAIT;
        autoprobeValidRecords = 0;

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_INITIALIZE:
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + 4.0);
        responseState = RESP_WAIT;
        autoprobeValidRecords = 0;
        break;

    case RESP_AUTOPROBE_INITIALIZE:
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + 4.0);
        responseState = RESP_AUTOPROBE_WAIT;
        autoprobeValidRecords = 0;
        break;

    default:
        discardData(frameTime + 0.5, 1);
        break;
    }
}

void AcquireMaycommTDL::discardDataCompleted(double frameTime)
{
    Q_UNUSED(frameTime);
}

void AcquireMaycommTDL::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frameTime);
    Q_UNUSED(frame);
}

AcquisitionInterface::AutoprobeStatus AcquireMaycommTDL::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

Variant::Root AcquireMaycommTDL::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireMaycommTDL::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

void AcquireMaycommTDL::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_RUN:
        if (autoprobeValidRecords > 10) {
            /* Already running, just notify that we succeeded. */
            qCDebug(log) << "Interactive autoprobe requested with communications established at"
                         << Logging::time(time);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            break;
        }

        /* Fall through */
    default:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);
        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        autoprobeValidRecords = 0;
        timeoutAt(time + 4.0);
        discardData(time + 0.5, 1);
        responseState = RESP_AUTOPROBE_WAIT;
        generalStatusUpdated();
        break;
    }
}

void AcquireMaycommTDL::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    autoprobeValidRecords = 0;
    timeoutAt(time + 15.0);
    discardData(time + 0.5, 1);
    responseState = RESP_AUTOPROBE_WAIT;
    generalStatusUpdated();
}

void AcquireMaycommTDL::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 3.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to interactive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from passive autoprobe to interactive acquisition at"
                     << Logging::time(time);
        responseState = RESP_WAIT;
        break;
    }
}

void AcquireMaycommTDL::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 3.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from passive autoprobe to passive acquisition at"
                     << Logging::time(time);
        responseState = RESP_WAIT;
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireMaycommTDL::getDefaults()
{
    AutomaticDefaults result;
    result.name = "G$1$2";
    result.setSerialN81(9600);
    return result;
}


ComponentOptions AcquireMaycommTDLComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options);
    return options;
}

ComponentOptions AcquireMaycommTDLComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireMaycommTDLComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireMaycommTDLComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireMaycommTDLComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireMaycommTDLComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireMaycommTDLComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireMaycommTDL(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireMaycommTDLComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireMaycommTDL(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireMaycommTDLComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{
    std::unique_ptr<AcquireMaycommTDL> i(new AcquireMaycommTDL(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireMaycommTDLComponent::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{
    std::unique_ptr<AcquireMaycommTDL> i(new AcquireMaycommTDL(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
