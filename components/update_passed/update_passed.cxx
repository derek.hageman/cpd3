/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/range.hxx"
#include "core/environment.hxx"
#include "core/actioncomponent.hxx"
#include "core/qtcompat.hxx"
#include "core/memory.hxx"
#include "datacore/stream.hxx"
#include "database/runlock.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/externalsink.hxx"
#include "io/process.hxx"

#include "update_passed.hxx"


Q_LOGGING_CATEGORY(log_component_update_passed, "cpd3.component.update.passed", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Editing;

UpdatePassed::UpdatePassed()
        : profiles(),
          stations(),
          start(FP::undefined()),
          end(FP::undefined()),
          doLock(true),
          lockTimeout(30.0),
          fragmentLimit(new DynamicTimeInterval::Constant(Time::Day, 8)),
          haveAfter(false),
          afterTime(FP::undefined()),
          terminated(false),
          runDetached(false)
{ }

UpdatePassed::UpdatePassed(const ComponentOptions &options,
                           double start,
                           double end,
                           const std::vector<SequenceName::Component> &setStations)
        : profiles(),
          stations(setStations),
          start(start),
          end(end),
          doLock(false),
          lockTimeout(30.0),
          fragmentLimit(NULL),
          haveAfter(false),
          afterTime(FP::undefined()),
          terminated(false),
          runDetached(false)
{
    if (options.isSet("profile")) {
        std::unordered_set<std::string> unique;
        for (const auto &add : qobject_cast<ComponentOptionStringSet *>(
                options.get("profile"))->get()) {
            if (add.isEmpty())
                continue;
            unique.insert(add.toLower().toStdString());
        }
        Util::append(unique, profiles);
    }
    if (options.isSet("fragment")) {
        fragmentLimit =
                qobject_cast<TimeIntervalSelectionOption *>(options.get("fragment"))->getInterval();
    } else {
        fragmentLimit = new DynamicTimeInterval::Constant(Time::Day, 8);
    }
    if (options.isSet("detached") &&
            qobject_cast<ComponentOptionBoolean *>(options.get("detached"))->get()) {
        runDetached = true;
    }
}

UpdatePassed::UpdatePassed(const ComponentOptions &options,
                           const std::vector<SequenceName::Component> &setStations)
        : profiles(),
          stations(setStations),
          start(FP::undefined()),
          end(FP::undefined()),
          doLock(true),
          lockTimeout(30.0),
          fragmentLimit(NULL),
          haveAfter(false),
          afterTime(FP::undefined()),
          terminated(false),
          runDetached(false)
{
    if (options.isSet("profile")) {
        std::unordered_set<std::string> unique;
        for (const auto &add : qobject_cast<ComponentOptionStringSet *>(
                options.get("profile"))->get()) {
            if (add.isEmpty())
                continue;
            unique.insert(add.toLower().toStdString());
        }
        Util::append(unique, profiles);
    }
    if (options.isSet("timeout")) {
        lockTimeout = qobject_cast<ComponentOptionSingleDouble *>(options.get("timeout"))->get();
    }
    if (options.isSet("after")) {
        haveAfter = true;
        afterTime = qobject_cast<ComponentOptionSingleTime *>(options.get("after"))->get();
    }
    if (options.isSet("fragment")) {
        fragmentLimit =
                qobject_cast<TimeIntervalSelectionOption *>(options.get("fragment"))->getInterval();
    } else {
        fragmentLimit = new DynamicTimeInterval::Constant(Time::Day, 8);
    }
    if (options.isSet("detached") &&
            qobject_cast<ComponentOptionBoolean *>(options.get("detached"))->get()) {
        runDetached = true;
    }
}

UpdatePassed::~UpdatePassed()
{
    delete fragmentLimit;
}

void UpdatePassed::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    terminateRequested();
}

bool UpdatePassed::testTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}

class UpdatePassedInterval : public Time::Bounds {
public:
    double beginTime;
    double endTime;

    UpdatePassedInterval(const Time::Bounds &b) : Time::Bounds(b),
                                                  beginTime(FP::undefined()),
                                                  endTime(FP::undefined())
    { }

    UpdatePassedInterval(const UpdatePassedInterval &b) : Time::Bounds(b),
                                                          beginTime(b.beginTime),
                                                          endTime(b.endTime)
    { }

    UpdatePassedInterval(double st, double ed) : Time::Bounds(st, ed),
                                                 beginTime(FP::undefined()),
                                                 endTime(FP::undefined())
    { }

    UpdatePassedInterval &operator=(const UpdatePassedInterval &other)
    {
        if (&other == this)
            return *this;
        Time::Bounds::operator=(other);
        beginTime = other.beginTime;
        endTime = other.endTime;
        return *this;
    }

    template<typename T>
    UpdatePassedInterval(const UpdatePassedInterval &u, const T &o, double st, double ed) :
            Time::Bounds(u, o, st, ed), beginTime(u.beginTime), endTime(u.endTime)
    { }

    template<typename T>
    UpdatePassedInterval(const T &o, double st, double ed) :
            Time::Bounds(o, st, ed), beginTime(FP::undefined()), endTime(FP::undefined())
    { }
};

Q_DECLARE_TYPEINFO(UpdatePassedInterval, Q_PRIMITIVE_TYPE);

namespace {
class ArchiveWrite final : public CPD3::Data::StreamSink {
    Archive::Access &access;
    SequenceValue::Transfer buffer;

    void commit(bool final = false)
    {
        if (buffer.size() < stallThreshold && !final)
            return;

        access.writeSynchronous(std::move(buffer));
        buffer.clear();
    }

public:
    explicit ArchiveWrite(Archive::Access &access) : access(access)
    { }

    virtual ~ArchiveWrite() = default;

    void incomingData(const SequenceValue::Transfer &values) override
    {
        if (values.empty())
            return;

        commit();
        progress(values.back().getStart());
        Util::append(values, buffer);
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        if (values.empty())
            return;
        commit();
        progress(values.back().getStart());
        Util::append(std::move(values), buffer);
    }

    void incomingData(const SequenceValue &value) override
    {
        commit();
        progress(value.getStart());
        buffer.emplace_back(value);
    }

    void incomingData(SequenceValue &&value) override
    {
        commit();
        progress(value.getStart());
        buffer.emplace_back(std::move(value));
    }

    void endData() override
    {
        commit(true);
    }

    void signalTerminate()
    {

    }

    Threading::Signal<double> progress;
};
}

static std::string detachedTaskProgram()
{
    QDir programDir(QCoreApplication::applicationDirPath());
    if (!programDir.exists())
        return "cpd3_detached_task";
    {
        QFileInfo file(programDir.absoluteFilePath("cpd3_detached_task"));
        if (file.exists())
            return file.absoluteFilePath().toStdString();
    }
    {
        QFileInfo file(programDir.absoluteFilePath("cpd3_detached_task.exe"));
        if (file.exists())
            return file.absoluteFilePath().toStdString();
    }
    return "cpd3_detached_task";
}

bool UpdatePassed::tryDetached(const SequenceName::Component &station,
                               const std::string &profile,
                               double start,
                               double end,
                               int index,
                               int total)
{
    if (!runDetached)
        return false;

#ifdef Q_OS_UNIX
    IO::Process::Spawn spawn(detachedTaskProgram(), {});
    spawn.forward();
    spawn.inputStream = IO::Process::Spawn::StreamMode::Capture;
#else
    std::string serializedArgument;
    {
        Variant::Root config;
        config["Name"].setString("update_passed");
        config["Stations"].array(0).setString(station);
        config["Start"].setReal(start);
        config["End"].setReal(end);
        config["Options/profile"].setString(profile);
        config["Options/fragment"].setReal(FP::undefined());
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << config;
        }
        serializedArgument = data.toBase64().toStdString();
    }
    if (serializedArgument.size() >= 32767) {
        qCDebug(log_component_update_passed)
            << "Serialized request too large for deteched execution, using internal";
        return false;
    }

    IO::Process::Spawn spawn(detachedTaskProgram(), {std::move(serializedArgument)});
    spawn.forward();
#endif

    auto proc = spawn.create();
    if (!proc) {
        qCDebug(log_component_update_passed)
            << "Failed to create detached execution, using internal";
        return false;
    }

    IO::Process::Instance::ExitMonitor monitor(*proc);
    monitor.connectTerminate(terminateRequested);
    if (testTerminated())
        return true;

    if (!proc->start()) {
        qCDebug(log_component_update_passed)
            << "Failed to start detached execution, using internal";
        return false;
    }

#ifdef Q_OS_UNIX
    {
        Variant::Root config;
        config["Name"].setString("update_passed");
        config["Stations"].array(0).setString(station);
        config["Start"].setReal(start);
        config["End"].setReal(end);
        config["Options/profile"].setString(profile);
        config["Options/fragment"].setReal(FP::undefined());
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << config;
        }

        auto stream = proc->inputStream();
        stream->write(std::move(data));
        stream->close();
    }
#endif

    qCDebug(log_component_update_passed) << "Started detached update";

    if (total > 1) {
        feedback.emitStage(tr("Detached %1 - %2/%3").arg(QString::fromStdString(station).toUpper())
                                                    .arg(index)
                                                    .arg(total),
                           tr("Detached processing is active.  Detailed progress will not be displayed until the processing is complete."),
                           false);
    } else {
        feedback.emitStage(tr("Detached %1").arg(QString::fromStdString(station).toUpper()),
                           tr("Detached processing is active.  Progress will not be displayed until the processing is complete."),
                           false);
    }
    return monitor.success();
}

bool UpdatePassed::processBounds(const SequenceName::Component &station,
                                 const std::string &profile,
                                 double start,
                                 double end,
                                 int index,
                                 int total)
{
    if (tryDetached(station, profile, start, end, index, total))
        return true;

    Archive::Access access;
    Archive::Access::WriteLock lock(access);

    ArchiveWrite archiveWrite(access);

    EditingEngine engineInput(start, end, station, QString::fromStdString(profile), &access);

    Threading::Receiver terminateReceiver;
    terminateRequested.connect(terminateReceiver,
                               std::bind(&EditingEngine::signalTerminate, &engineInput));
    terminateRequested.connect(terminateReceiver,
                               std::bind(&Archive::Access::signalTerminate, &access));
    terminateRequested.connect(terminateReceiver,
                               std::bind(&ArchiveWrite::signalTerminate, &archiveWrite));
    if (testTerminated()) {
        archiveWrite.signalTerminate();
        access.signalTerminate();
        engineInput.signalTerminate();
        engineInput.wait();
        return true;
    }

    engineInput.start();

    if (testTerminated()) {
        engineInput.wait();
        return true;
    }

    {
        Threading::Signal<double> sig;
        bool firstPurgeProgress = true;
        auto progress = sig.connect(terminateReceiver, [&](double time) {
            if (firstPurgeProgress) {
                firstPurgeProgress = false;
                if (total > 1) {
                    feedback.emitStage(tr("Cleaning up %1 - %2/%3").arg(
                            QString::fromStdString(station).toUpper()).arg(index).arg(total),
                                       tr("Existing data are being removed for the affected time range."),
                                       true);
                } else {
                    feedback.emitStage(
                            tr("Cleaning up %1").arg(QString::fromStdString(station).toUpper()),
                            tr("Existing data are being removed for the affected time range."),
                            true);
                }
            }
            feedback.emitProgressTime(time);
        });
        engineInput.purgeArchive(sig);
    }
    Memory::release_unused();

    qCDebug(log_component_update_passed) << "Started data update";

    if (total > 1) {
        feedback.emitStage(
                tr("Processing %1 - %2/%3").arg(QString::fromStdString(station).toUpper())
                                           .arg(index)
                                           .arg(total),
                tr("Initial data are being processed before archival."), false);
    } else {
        feedback.emitStage(tr("Processing %1").arg(QString::fromStdString(station).toUpper()),
                           tr("Initial data are being processed before archival."), false);
    }
    bool firstArchiveProgress = true;
    auto progress = archiveWrite.progress.connect(terminateReceiver, [&](double time) {
        if (firstArchiveProgress) {
            firstArchiveProgress = false;
            if (total > 1) {
                feedback.emitStage(
                        tr("Archiving %1 - %2/%3").arg(QString::fromStdString(station).toUpper())
                                                  .arg(index)
                                                  .arg(total),
                        tr("Updated data are being written to the archive."), true);
            } else {
                feedback.emitStage(
                        tr("Archiving %1").arg(QString::fromStdString(station).toUpper()),
                        tr("Updated data are being written to the archive."), true);
            }
        }
        feedback.emitProgressTime(time);
    });
    engineInput.setEgress(&archiveWrite);

    if (testTerminated()) {
        engineInput.wait();
        return true;
    }

    while (!engineInput.wait(100)) {
        QCoreApplication::processEvents();
    }
    engineInput.wait();

    if (testTerminated())
        return true;

    progress.disconnect();

    feedback.emitStage(tr("Finalizing %1").arg(QString::fromStdString(station).toUpper()),
                       tr("The final archive write is taking place."), false);

    return lock.commit();
}

bool UpdatePassed::processStation(const SequenceName::Component &station,
                                  const std::string &profile,
                                  double modifiedAfter,
                                  double &dataModified)
{
    double stationBeginTime = Time::time();

    QList<UpdatePassedInterval> intervals;
    if (doLock) {
        Archive::Access access;
        Archive::Access::ReadLock lock(access, true);

        ArchiveSink::Iterator data;
        auto reader = access.readArchive(
                Archive::Selection(FP::undefined(), FP::undefined(), {station}, {"passed"},
                                   {profile}, {}, {}, {}, modifiedAfter).withDefaultStation(false)
                                                                        .withMetaArchive(false),
                &data);

        Threading::Receiver terminateReceiver;
        terminateRequested.connect(terminateReceiver,
                                   std::bind(&Archive::Access::signalTerminate, &access));
        terminateRequested.connect(terminateReceiver,
                                   std::bind(&Archive::Access::ArchiveContext::signalTerminate,
                                             reader.get()));
        terminateRequested.connect(terminateReceiver,
                                   std::bind(&ArchiveSink::Iterator::abort, &data));
        if (testTerminated()) {
            data.abort();
            reader->signalTerminate();
            access.signalTerminate();
            return false;
        }

        while (data.hasNext()) {
            auto add = data.next();

            Q_ASSERT(FP::defined(add.getModified()));

            /* Technically a race, but we can't avoid it since we set
             * the time to the last modified and we don't want to
             * continually re-pass the same interval */
            if (FP::defined(modifiedAfter) && add.getModified() - modifiedAfter < 1E-3)
                continue;

            if (FP::defined(add.getStart()))
                add.setStart(std::floor(add.getStart() / 3600.0) * 3600.0);
            if (FP::defined(add.getEnd()))
                add.setEnd(std::ceil(add.getEnd() / 3600.0) * 3600.0);

            Range::overlayFragmenting(intervals, add);

            if (!FP::defined(dataModified) || dataModified < add.getModified())
                dataModified = add.getModified();
        }

        reader->wait();
        reader.reset();

        qCDebug(log_component_update_passed) << "Found" << intervals.size()
                                             << "updated intervals after"
                                             << Logging::time(modifiedAfter)
                                             << "with the last modified at"
                                             << Logging::time(dataModified);
    } else {
        double effectiveStart = start;
        if (FP::defined(effectiveStart))
            effectiveStart = std::floor(effectiveStart / 3600.0) * 3600.0;
        double effectiveEnd = end;
        if (FP::defined(effectiveEnd))
            effectiveEnd = std::ceil(effectiveEnd / 3600.0) * 3600.0;
        intervals << UpdatePassedInterval(effectiveStart, effectiveEnd);
    }

    if (intervals.isEmpty()) {
        qCDebug(log_component_update_passed) << "No data to update";
        return false;
    }

    for (int idx = intervals.size() - 1; idx > 0; --idx) {
        Q_ASSERT(FP::defined(intervals.at(idx).getStart()));
        Q_ASSERT(FP::defined(intervals.at(idx - 1).getEnd()));

        if (intervals.at(idx).getStart() - intervals.at(idx - 1).getEnd() > 1.0)
            continue;

        intervals[idx - 1].setEnd(intervals.at(idx).getEnd());
        intervals.removeAt(idx);
    }

    /* Fragment to keep from passing too much at once */
    for (auto interval = intervals.begin(); interval != intervals.end(); ++interval) {
        while (FP::defined(interval->getStart()) && FP::defined(interval->getEnd())) {
            double limit = fragmentLimit->apply(interval->getStart(), interval->getStart(), true);
            if (!FP::defined(limit))
                break;
            if (limit >= interval->getEnd())
                break;
            UpdatePassedInterval newInterval(*interval);
            newInterval.setStart(limit);
            interval->setEnd(newInterval.getStart());

            interval = intervals.insert(interval + 1, newInterval);
        }
    }

    int total = intervals.size();
    int index = 0;
    for (auto &bounds : intervals) {
        ++index;
        if (testTerminated())
            return false;

        bounds.beginTime = Time::time();

        qDebug(log_component_update_passed) << "Updating data for" << station << profile << "in"
                                            << bounds;

        for (bool firstTry = true;; firstTry = false) {
            if (firstTry) {
                if (total > 1) {
                    feedback.emitStage(tr("Starting %1 update - %2/%3").arg(QString::fromStdString(
                                                                               station).toUpper())
                                                                       .arg(index)
                                                                       .arg(total),
                                       tr("The system is starting the data update procedure."),
                                       false);
                } else {
                    feedback.emitStage(
                            tr("Starting %1 update").arg(QString::fromStdString(station).toUpper()),
                            tr("The system is starting the data update procedure."), false);
                }
            } else {
                if (total > 1) {
                    feedback.emitStage(
                            tr("Restarting %1 update - %2/%3").arg(QString::fromStdString(
                                                                      station).toUpper())
                                                              .arg(index)
                                                              .arg(total),
                            tr("The system is restarting the data update procedure due an archive access inconsistency."),
                            false);
                } else {
                    feedback.emitStage(tr("Restarting %1 update").arg(
                            QString::fromStdString(station).toUpper()),
                                       tr("The system is starting the data update procedure due an archive access inconsistency."),
                                       false);
                }
            }

            if (processBounds(station, profile, bounds.getStart(), bounds.getEnd(), index, total))
                break;

            Memory::release_unused();
        }
        Memory::release_unused();

        if (testTerminated())
            return false;

        bounds.endTime = Time::time();

        qCDebug(log_component_update_passed) << "Completed data update for" << station << profile
                                             << "in" << bounds << "afte "
                                             << Logging::elapsed(bounds.endTime - bounds.beginTime);
    }


    Variant::Root event;

    event["Text"].setString(
            tr("Completed updating %n passed intervals(s)", "update event text", intervals.size()));
    event["Information"].hash("By").setString("update_passed");
    event["Information"].hash("At").setDouble(Time::time());
    event["Information"].hash("Environment").setString(Environment::describe());
    event["Information"].hash("Revision").setString(Environment::revision());
    event["Information"].hash("TotalIntervals").setInt64(intervals.size());
    event["Information"].hash("ModifiedAfter").setDouble(modifiedAfter);
    event["Information"].hash("DataModified").setDouble(dataModified);
    event["Information"].hash("FirstStart").setDouble(intervals.first().getStart());
    event["Information"].hash("LastEnd").setDouble(intervals.last().getEnd());
    event["Information"].hash("BeginTime").setDouble(stationBeginTime);
    for (QList<UpdatePassedInterval>::const_iterator bounds = intervals.constBegin(),
            end = intervals.constEnd(); bounds != end; ++bounds) {
        auto v = event["Intervals"].toArray().after_back();
        v.hash("Start").setDouble(bounds->getStart());
        v.hash("End").setDouble(bounds->getEnd());
        v.hash("Elapsed").setDouble(bounds->endTime - bounds->beginTime);
    }

    if (testTerminated())
        return false;

    double now = Time::time();
    Archive::Access().writeSynchronous(SequenceValue::Transfer{
            SequenceValue({station, "events", "editing"}, std::move(event), now, now)});

    return true;
}

typedef std::pair<SequenceName::Component, std::string> ProcessingKey;

static bool processingKeySort(const ProcessingKey &a, const ProcessingKey &b)
{
    int cr = a.first.compare(b.first);
    if (cr != 0)
        return cr < 0;
    return a.second < b.second;
}

void UpdatePassed::run()
{
    if (testTerminated())
        return;

    double startProcessing = Time::time();

    std::vector<ProcessingKey> keys;
    if (doLock) {
        auto allKeys = Archive::Access().availableNames(
                Archive::Selection(FP::undefined(), FP::undefined(), stations, {"passed"}, profiles)
                        .withDefaultStation(false)
                        .withMetaArchive(false), Archive::Access::IndexOnly);
        if (allKeys.empty()) {
            qCDebug(log_component_update_passed) << "No passed data available";
            return;
        }
        std::unordered_set<ProcessingKey> uniqueKeys;
        for (const auto &add : allKeys) {
            uniqueKeys.insert(ProcessingKey(add.getStation(), Util::to_lower(add.getVariable())));
        }
        Util::append(uniqueKeys, keys);
    } else {
        Archive::Access access;
        Archive::Access::ReadLock lock(access);

        auto allStations = access.availableStations(stations);
        if (allStations.empty()) {
            qCDebug(log_component_update_passed) << "No stations available";
            return;
        }
        stations.clear();
        std::copy(allStations.begin(), allStations.end(), Util::back_emplacer(stations));

        auto allProfiles = Archive::Access().availableSelected(
                Archive::Selection(FP::undefined(), FP::undefined(), stations, {"passed"}, profiles)
                        .withDefaultStation(false)
                        .withMetaArchive(false), Archive::Access::IndexOnly).variables;
        if (profiles.empty()) {
            allProfiles.insert(SequenceName::impliedProfile(&access));
        }
        allProfiles.erase("");
        if (allProfiles.empty()) {
            qCDebug(log_component_update_passed) << "No profiles available";
            return;
        }
        profiles.clear();
        std::copy(allProfiles.begin(), allProfiles.end(), Util::back_emplacer(profiles));

        std::unordered_set<ProcessingKey> uniqueKeys;
        for (const auto &station : stations) {
            for (const auto &profile : profiles) {
                uniqueKeys.insert(ProcessingKey(station, Util::to_lower(profile)));
            }
        }
        Util::append(uniqueKeys, keys);
    }

    if (testTerminated())
        return;

    std::sort(keys.begin(), keys.end(), processingKeySort);
    if (keys.empty()) {
        qCDebug(log_component_update_passed) << "No data to update";
        return;
    }

    qCDebug(log_component_update_passed) << "Starting passed update for" << keys.size()
                                         << "keys(s)";

    for (const auto &key : keys) {
        if (testTerminated())
            break;

        const auto &station = key.first;
        const auto &profile = key.second;
        QString rcKey(QString("updatepassed %1 %2").arg(QString::fromStdString(profile),
                                                        QString::fromStdString(station)));

        Database::RunLock rc;

        double modifiedAfter = FP::undefined();
        double dataModified = FP::undefined();
        if (doLock) {
            feedback.emitStage(tr("Lock %1").arg(QString::fromStdString(station).toUpper()),
                               tr("The system is acquiring an exclusive lock for %1.").arg(
                                       QString::fromStdString(station).toUpper()), false);
            bool ok = false;
            qCDebug(log_component_update_passed) << "Locking " << station << profile;

            modifiedAfter = rc.acquire(rcKey, lockTimeout, &ok);
            if (!ok) {
                feedback.emitFailure();
                qCDebug(log_component_update_passed) << "Update skipped for" << station;
                continue;
            }
            if (haveAfter)
                modifiedAfter = afterTime;
        }

        if (testTerminated()) {
            rc.fail();
            break;
        }

        if (processStation(station, profile, modifiedAfter, dataModified)) {
            if (doLock) {
                if (FP::defined(dataModified)) {
                    rc.seenTime(dataModified);
                    rc.release();
                } else if (haveAfter) {
                    rc.seenTime(afterTime);
                    rc.release();
                } else {
                    rc.fail();
                }
            }
            qCDebug(log_component_update_passed) << "Updated completed for" << station;
        } else if (doLock) {
            if (haveAfter) {
                rc.seenTime(afterTime);
                rc.release();
            } else {
                rc.fail();
            }
        }
    }

    qCDebug(log_component_update_passed) << "Starting tasks run";
    {
        QObject *component = ComponentLoader::create("tasks");
        ActionComponent *ac = nullptr;
        if (component)
            ac = qobject_cast<ActionComponent *>(component);

        std::unique_ptr<CPD3Action> tasks;
        if (ac) {
            auto t = ac->createAction(ac->getOptions());
            if (t)
                tasks.reset(t);
        }

        if (tasks) {
            tasks->feedback.forward(feedback);
            terminateRequested.connect(std::bind(&CPD3Action::signalTerminate, tasks.get()));
            if (!testTerminated()) {
                tasks->start();
                tasks->wait();
            }
        } else {
            qCWarning(log_component_update_passed) << "Failed to execute tasks component";
        }

        terminateRequested.disconnect();
    }

    double endProcessing = Time::time();
    qCDebug(log_component_update_passed) << "Finished update after"
                                         << Logging::elapsed(endProcessing - startProcessing);

    QCoreApplication::processEvents();
}


ComponentOptions UpdatePassedComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile", new ComponentOptionStringSet(tr("profile", "name"), tr("Pass profile"),
                                                        tr("This sets the profiles to update.  If set then only the profiles "
                                                           "listed here will be considered for updating.  "
                                                           "Consult the station editing configuration for available profiles."),
                                                        tr("All passed profiles and the aerosol profile")));

    ComponentOptionSingleDouble *timeout =
            new ComponentOptionSingleDouble(tr("wait", "name"), tr("Maximum wait time"),
                                            tr("This is the maximum time to wait for another process to "
                                               "complete, in seconds.  If there are two concurrent processes for "
                                               "the same station and profile, the new one will wait at most this "
                                               "many seconds before giving up without performing an update."),
                                            tr("Thirty seconds"));
    timeout->setMinimum(0.0);
    timeout->setAllowUndefined(true);
    options.add("timeout", timeout);

    ComponentOptionSingleTime *after =
            new ComponentOptionSingleTime(tr("after", "name"), tr("Override time"),
                                          tr("This sets the time to re-pass after.  When set, this will update "
                                             "all data passed after the given time.  This can be used to "
                                             "ignore the record of the last update, but doing so can cause "
                                             "there to be passed data that does not exist in the final "
                                             "archive.  No effect when used with an explicit time range."),
                                          tr("Last run time"));
    after->setAllowUndefined(true);
    options.add("after", after);

    TimeIntervalSelectionOption *fragment = new TimeIntervalSelectionOption(tr("fragment", "name"),
                                                                            tr("Interval fragmentation limit"),
                                                                            tr("This sets the maximum time that an individual "
                                                                               "fragment of data is updated at.  Any intervals longer than "
                                                                               "this threshold will be cut down to be shorter.  "
                                                                               "This is intended to prevent excessively long archive access."),
                                                                            tr("8 days"));
    fragment->setAllowZero(false);
    fragment->setAllowNegative(false);
    fragment->setAllowUndefined(true);
    fragment->setDefaultAligned(false);
    options.add("fragment", fragment);

    options.add("detached", new ComponentOptionBoolean(tr("detached", "name"),
                                                       tr("Execute the update in detached mode"),
                                                       tr("This option causes the update to be executed in detached processes.  "
                                                          "This allows for better isolation in the event of failure or other problems."),
                                                       QString()));

    return options;
}

QString UpdatePassedComponent::promptTimeActionContinue(const ComponentOptions &options,
                                                        double start,
                                                        double end,
                                                        const std::vector<std::string> &stations)
{
    Q_UNUSED(stations);
    Q_UNUSED(options);
    if (!FP::defined(start) || !FP::defined(end) || (end - start) > 31 * 86400) {
        return UpdatePassedComponent::tr(
                "This operation may cause a large amount of reprocessing.");
    }
    return QString();
}

QString UpdatePassedComponent::promptTimeActionContinue(const ComponentOptions &options,
                                                        const std::vector<std::string> &stations)
{
    Q_UNUSED(stations);
    if (options.isSet("after")) {
        if (FP::defined(qobject_cast<ComponentOptionSingleTime *>(options.get("after"))->get())) {
            return UpdatePassedComponent::tr(
                    "This operation will may result in passed data not being available for access.");
        } else {
            return UpdatePassedComponent::tr("This operation will re-process all passed data.");
        }
    }
    return QString();
}

QList<ComponentExample> UpdatePassedComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Update all stations", "default example name"),
                                     tr("This will update passed data for all profiles and all stations.")));

    return examples;
}

int UpdatePassedComponent::actionAllowStations()
{ return INT_MAX; }

bool UpdatePassedComponent::actionRequiresTime()
{ return false; }

CPD3Action *UpdatePassedComponent::createTimeAction(const ComponentOptions &options,
                                                    double start,
                                                    double end,
                                                    const std::vector<std::string> &stations)
{ return new UpdatePassed(options, start, end, stations); }

CPD3Action *UpdatePassedComponent::createTimeAction(const ComponentOptions &options,
                                                    const std::vector<std::string> &stations)
{ return new UpdatePassed(options, stations); }
