/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QDir>
#include <QFile>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/actioncomponent.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/dynamictimeinterval.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    ActionComponentTime *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ActionComponentTime *>(ComponentLoader::create("update_passed"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionStringSet *>(options.get("profile")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("timeout")));
        QVERIFY(qobject_cast<ComponentOptionSingleTime *>(options.get("after")));
        QVERIFY(qobject_cast<TimeIntervalSelectionOption *>(options.get("fragment")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("detached")));
    }

    void updated()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "passed", "aerosol"}, Variant::Root(), 3600.0, 7199.0)});

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createTimeAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        auto events = Archive::Access(databaseFile).readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"sfa"}, {"events"},
                                   {"editing"}));
        QCOMPARE((int) events.size(), 1);
        QCOMPARE(events.front().read()["Information/FirstStart"].toDouble(), 3600.0);
        QCOMPARE(events.front().read()["Information/LastEnd"].toDouble(), 7200.0);

        QTest::qSleep(50);

        action = component->createTimeAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        events = Archive::Access(databaseFile).readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"sfa"}, {"events"},
                                   {"editing"}));
        QCOMPARE((int) events.size(), 1);

        QTest::qSleep(50);

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "passed", "aerosol"}, Variant::Root(), 3600.0, 7200.0, 1)});

        action = component->createTimeAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        events = Archive::Access(databaseFile).readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"sfa"}, {"events"},
                                   {"editing"}));
        QCOMPARE((int) events.size(), 2);

        action = component->createTimeAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        events = Archive::Access(databaseFile).readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"sfa"}, {"events"},
                                   {"editing"}));
        QCOMPARE((int) events.size(), 2);


        qobject_cast<ComponentOptionSingleTime *>(options.get("after"))->set(FP::undefined());
        action = component->createTimeAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        events = Archive::Access(databaseFile).readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"sfa"}, {"events"},
                                   {"editing"}));
        QCOMPARE((int) events.size(), 3);
    }

    void fragment()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "passed", "aerosol"}, Variant::Root(), 3600.0, 2685600)});

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createTimeAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        auto events = Archive::Access(databaseFile).readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"sfa"}, {"events"},
                                   {"editing"}));
        QCOMPARE((int) events.size(), 1);
        QCOMPARE(events.front().read()["Information/FirstStart"].toDouble(), 3600.0);
        QCOMPARE(events.front().read()["Information/LastEnd"].toDouble(), 2685600.0);
        QCOMPARE((int) events.front().read()["Intervals"].toArray().size(), 2);

        action = component->createTimeAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        events = Archive::Access(databaseFile).readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"sfa"}, {"events"},
                                   {"editing"}));
        QCOMPARE((int) events.size(), 1);
    }

    void times()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "passed", "aerosol"}, Variant::Root(), 3600.0, 7200.0)});

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createTimeAction(options, 10800.0, 14399.0, {"sfa"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        auto events = Archive::Access(databaseFile).readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"sfa"}, {"events"},
                                   {"editing"}));
        QCOMPARE((int) events.size(), 1);
        QCOMPARE(events.front().read()["Information/FirstStart"].toDouble(), 10800.0);
        QCOMPARE(events.front().read()["Information/LastEnd"].toDouble(), 14400.0);
    }

    void fragmentTimes()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "passed", "aerosol"}, Variant::Root(), 3600.0, 2000.0)});

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createTimeAction(options, 3600.0, 2685600.0, {"sfa"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        auto events = Archive::Access(databaseFile).readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"sfa"}, {"events"},
                                   {"editing"}));
        QCOMPARE((int) events.size(), 1);
        QCOMPARE(events.front().read()["Information/FirstStart"].toDouble(), 3600.0);
        QCOMPARE(events.front().read()["Information/LastEnd"].toDouble(), 2685600.0);
        QCOMPARE((int) events.front().read()["Intervals"].toArray().size(), 2);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
