/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef UPDATEPASSED_H
#define UPDATEPASSED_H

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>
#include <QStringList>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "editing/editingengine.hxx"
#include "datacore/dynamictimeinterval.hxx"

class UpdatePassed : public CPD3::CPD3Action {
Q_OBJECT

    std::vector<std::string> profiles;
    std::vector<CPD3::Data::SequenceName::Component> stations;
    double start;
    double end;
    bool doLock;
    double lockTimeout;
    CPD3::Data::DynamicTimeInterval *fragmentLimit;

    bool haveAfter;
    double afterTime;

    bool terminated;
    bool runDetached;

    std::mutex mutex;

    CPD3::Threading::Signal<> terminateRequested;


    bool testTerminated();

    bool tryDetached(const CPD3::Data::SequenceName::Component &station,
                     const std::string &profile,
                     double start,
                     double end,
                     int index,
                     int total);

    bool processBounds(const CPD3::Data::SequenceName::Component &station,
                       const std::string &profile,
                       double start,
                       double end,
                       int index,
                       int total);

    bool processStation(const CPD3::Data::SequenceName::Component &station,
                        const std::string &profile,
                        double modifiedAfter,
                        double &dataModified);

public:
    UpdatePassed();

    UpdatePassed(const CPD3::ComponentOptions &options,
                 double start,
                 double end,
                 const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    UpdatePassed(const CPD3::ComponentOptions &options,
                 const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~UpdatePassed();

    virtual void signalTerminate();

protected:
    virtual void run();
};

class UpdatePassedComponent : public QObject, virtual public CPD3::ActionComponentTime {
Q_OBJECT
    Q_INTERFACES(CPD3::ActionComponentTime)

    Q_PLUGIN_METADATA(IID
                              "CPD3.update_passed"
                              FILE
                              "update_passed.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int actionAllowStations();

    virtual bool actionRequiresTime();

    QString promptTimeActionContinue(const CPD3::ComponentOptions &options,
                                     double start,
                                     double end,
                                     const std::vector<std::string> &stations = {}) override;

    QString promptTimeActionContinue(const CPD3::ComponentOptions &options = {},
                                     const std::vector<std::string> &stations = {}) override;

    CPD3::CPD3Action *createTimeAction(const CPD3::ComponentOptions &options,
                                       double start,
                                       double end,
                                       const std::vector<std::string> &stations = {}) override;

    CPD3::CPD3Action *createTimeAction(const CPD3::ComponentOptions &options = {},
                                       const std::vector<std::string> &stations = {}) override;
};

#endif
