/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIRERRPSAP3W_H
#define ACQUIRERRPSAP3W_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"
#include "smoothing/baseline.hxx"

class AcquireRRPSAP3W : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Waiting for enough valid records to confirm autoprobe success */
        RESP_AUTOPROBE_WAIT,

        /* Waiting for a valid record to start acquisition */
        RESP_WAIT,

        /* Acquiring data in passive mode */
        RESP_RUN,

        /* Same as wait, but discard the first timeout */
        RESP_INITIALIZE,

        /* Same as initialize but autoprobing */
        RESP_AUTOPROBE_INITIALIZE,
    };
    ResponseState responseState;
    int autoprobeValidRecords;
    int autoprobeTotalFrames;

    enum SampleState {
        /* Normal operation */
                SAMPLE_RUN,

        /* Sampling on a filter but waiting for the normalization */
                SAMPLE_FILTER_NORMALIZE, /* Filter currently changing */
                SAMPLE_FILTER_CHANGING,

        /* Sampling on a white filter but waiting for the normalization */
                SAMPLE_WHITE_FILTER_NORMALIZE, /* Changing a white filter */
                SAMPLE_WHITE_FILTER_CHANGING,

        /* A filter change is required */
                SAMPLE_REQUIRE_FILTER_CHANGE, /* A white filter change is required */
                SAMPLE_REQUIRE_WHITE_FILTER_CHANGE,
    };
    SampleState sampleState;

    class Configuration {
        double start;
        double end;
    public:
        CPD3::Calibration flowCalibration;
        double area;
        double wavelengths[3];
        bool strictMode;
        double reportInterval;

        bool reverseWeiss;
        bool useReportedWeiss;
        bool useReportedArea;
        bool useReportedAbsorptions;

        double instrumentArea;
        double weissA;
        double weissB;

        bool useMeasuredTime;
        bool enableNormalizationRecovery;
        bool enableAbsorptionRecovery;

        bool enableAutodetectStart;
        bool enableAutodetectEnd;

        double verifyNormalizationBand;
        double verifyWhiteBand;


        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    double lastStartLogTime;

    class AccumulatorMeasurement {
    public:
        double endTime;
        qint64 psapTime;
        qint64 sum;

        AccumulatorMeasurement();

        void reset();
    };

    AccumulatorMeasurement accumulatorSample[3];
    AccumulatorMeasurement accumulatorReference[3];
    AccumulatorMeasurement accumulatorConversions[3];
    AccumulatorMeasurement accumulatorFlow;
    AccumulatorMeasurement accumulatorFlowSeconds;
    double accumulatorDarkSample;
    double accumulatorDarkReference;
    double accumulatorBinStartTime;
    int accumulatorBinStartIndex;
    qint64 accumulatorBinStartPSAPTime;

    double reportedWeissA;
    double reportedWeissB;
    double reportedArea;
    qint64 reportedResetTime;
    double reportedResetIn[3];
    double reportedFlowCalibration[3];


    class IntensityData {
    public:
        double startTime;
        double Ip[3];
        double If[3];
        double In[3];

        IntensityData();
    };

    friend QDataStream &operator<<(QDataStream &stream, const IntensityData &data);

    friend QDataStream &operator>>(QDataStream &stream, IntensityData &data);

    /* State that needs to be saved */
    double accumulatedVolume;
    double filterNormalization[3];
    bool filterIsNotWhite;
    qint64 normalizationPSAPTime;
    double normalizationStartTime;


    IntensityData normalizationLatest;
    IntensityData filterStart;
    IntensityData filterWhite;

    int normalizationAcceptRemaining;
    double whiteFilterChangeStartTime;

    double priorIr[3];
    double priorIn[3];

    CPD3::Data::Variant::Root instrumentMeta;
    enum MetadataState {
        NotEmitted, MetadataEmitted, MetadataEmittedWithoutHighPrecsionTransmittance,
    };
    MetadataState logMetaState;
    MetadataState realtimeMetaState;
    bool forceRealtimeStateEmit;

    bool forceFilterBreak;
    bool requireFilterBreak;

    qint64 filterChangeStartPSAPTime;
    qint64 filterChangeEndPSAPTime;

    CPD3::Data::DynamicTimeInterval *acceptNormalizationTime;
    CPD3::Smoothing::BaselineSmoother *filterNormalizeIp[3];
    CPD3::Smoothing::BaselineSmoother *filterNormalizeIf[3];
    CPD3::Smoothing::BaselineSmoother *filterNormalizeIn[3];
    CPD3::Smoothing::BaselineSmoother *filterChangeIf[3];
    CPD3::Smoothing::BaselineSmoother *filterChangeIrc[3];

    double lastTransmittanceCheckTime;
    double transmittanceWarningThreshold;

    struct CurrentData {
        CPD3::Data::Variant::Root Ip[3];
        CPD3::Data::Variant::Root If[3];
        CPD3::Data::Variant::Root In[3];
        CPD3::Data::Variant::Root Irc[3];
        CPD3::Data::Variant::Root Ir[3];
        CPD3::Data::Variant::Root Ba[3];
        CPD3::Data::Variant::Root Bac[3];
        CPD3::Data::Variant::Root IpD;
        CPD3::Data::Variant::Root IfD;
        CPD3::Data::Variant::Root Qraw;
        CPD3::Data::Variant::Root Q;
        CPD3::Data::Variant::Root VQ;
        CPD3::Data::Variant::Root Qt;
        CPD3::Data::Variant::Root L;
        CPD3::Data::Variant::Root flags;
        CPD3::Data::Variant::Root baseFlags;
        qint64 psapTime;
    };
    CurrentData data;

    void resetAccumulatorStream();

    void setDefaultInvalid();

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root value);

    CPD3::Data::Variant::Root buildFilterValueMeta() const;

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time, bool differenceAbsorptions) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    CPD3::Data::Variant::Root buildFilterValue(const IntensityData &data) const;

    CPD3::Data::Variant::Root buildNormalizationValue() const;

    void emitFilterEnd(double endTime);

    void emitWhiteEnd(double endTime);

    void emitNormalizationEnd(double endTime);

    bool isSameNormalization(double currentTime) const;

    void advanceNormalization(double startTime, double endTime);

    bool canRecoverNormalization(double currentTime) const;

    void recoverNormalization(double currentTime);

    void invalidateNormalization();

    bool autodetectStart() const;

    bool autodetectEnd() const;

    bool normalizationDone() const;

    bool isWhiteFilter() const;

    bool whiteFilterTimeout(double time) const;

    void resetSmoothers();

    void insertDiagnosticCommonValues(CPD3::Data::Variant::Write &target) const;

    void insertDiagnosticFilterValues(CPD3::Data::Variant::Write &target,
                                      const QString &name,
                                      const IntensityData &source) const;

    void insertDiagnosticNormalization(CPD3::Data::Variant::Write &target) const;

    void insertDiagnosticFilterNormalization(CPD3::Data::Variant::Write &target) const;

    void applyStateControl(double startTime, double endTime);

    void configurationChanged();

    void configAdvance(double frameTime);

    void invalidateLogging(double frameTime);

    int parseTime(const CPD3::Util::ByteView &field,
                  qint64 &time,
                  const CPD3::Data::SequenceName::Component &parsePrefix = {});

    int consumeTime(std::deque<CPD3::Util::ByteView> &fields, double &frameTime, qint64 &psapTime);

    void outputMetadata(double currentTime, bool haveHighPrecsionTransmittance = true);

    void outputFlags(double startTime, double endTime, bool includeWeiss = false);

    void outputIntensityData(double startTime, double endTime, double nominalTimeAdvance = 1.0);

    void outputReportedData(double startTime, double endTime);

    int processRecordR(std::deque<CPD3::Util::ByteView> fields, double &frameTime);

    int processRecordA(std::deque<CPD3::Util::ByteView> fields, double &frameTime);

    void differenceAccumulator(AccumulatorMeasurement &accumulator,
                               double &difference,
                               double &dT,
                               qint64 current,
                               double endTime,
                               qint64 psapTime,
                               qint64 wrap);

    int handleAccumulatorExtra(int timeIndex,
                               std::deque<CPD3::Util::ByteView> &fields,
                               double startTime,
                               double endTime,
                               qint64 psapTime);

    int processRecordAccumulators(std::deque<CPD3::Util::ByteView> &fields,
                                  double startTime,
                                  double endTime,
                                  qint64 psapTime);

    int processRecordO(std::deque<CPD3::Util::ByteView> fields,
                       double &frameTime,
                       bool tryTime = true,
                       qint64 psapTime = CPD3::INTEGER::undefined());

    int processRecordI(std::deque<CPD3::Util::ByteView> fields, double &frameTime);

    int processRecord(CPD3::Util::ByteView line, double &frameTime);

public:
    AcquireRRPSAP3W(const CPD3::Data::ValueSegment::Transfer &config,
                    const std::string &loggingContext);

    AcquireRRPSAP3W(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireRRPSAP3W();

    virtual CPD3::Data::SequenceValue::Transfer getPersistentValues();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    CPD3::Data::Variant::Root getCommands() override;

    virtual CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    virtual void serializeState(QDataStream &stream);

    virtual void deserializeState(QDataStream &stream);

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    virtual void command(const CPD3::Data::Variant::Read &value);

    virtual void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingInstrumentTimeout(double time);

    virtual void discardDataCompleted(double time);
};

class AcquireRRPSAP3WComponent
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_rr_psap3w"
                              FILE
                              "acquire_rr_psap3w.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
