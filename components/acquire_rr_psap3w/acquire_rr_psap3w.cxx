/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <array>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_rr_psap3w.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Smoothing;

static const std::array<std::string, 3> colorLookup = {"B", "G", "R"};

/* The documentation isn't clear if this is actually the case in the R
 * records.  It doesn't make sense in the context of the accumulators:
 * insufficient information to reconstruct it, since it could be variable
 * between bins, and the whole point of the accumulators is doing bin
 * differences without looking at the intermediate data.  So we'll just
 * assume it's consistent across both.  The difference is small anyway
 * (on the order of ~4000 in 800000). */
#define R_RECORD_DARK_SUBTRACTED

/* Allow this to be approximated if it's not really sane. */
#define ACCUMULATOR_DARK_SUBTRACTED

static const char *instrumentFlagTranslation[16] = {NULL,                       /* 0x0001 */
                                                    NULL,                       /* 0x0002 */
                                                    NULL,                       /* 0x0004 */
                                                    NULL,                       /* 0x0008 */
                                                    "FlowError",                /* 0x0010 */
                                                    "FlowOverride",             /* 0x0020 */
                                                    NULL,                       /* 0x0040 */
                                                    "Weiss",                    /* 0x0080 */
                                                    "RedDetectorLow",           /* 0x0100 */
                                                    "GreenDetectorLow",         /* 0x0200 */
                                                    "BlueDetectorLow",          /* 0x0400 */
                                                    NULL,                       /* 0x0800 */
                                                    "RedDetectorHigh",          /* 0x1000 */
                                                    "GreenDetectorHigh",        /* 0x2000 */
                                                    "BlueDetectorHigh",         /* 0x4000 */
                                                    "CurrentOverrange",         /* 0x8000 */
};
static const quint16 instrumentFlagWeiss = 0x0080;

AcquireRRPSAP3W::Configuration::Configuration() : start(FP::undefined()),
                                                  end(FP::undefined()),
                                                  flowCalibration(),
                                                  area(17.81),
                                                  wavelengths(),
                                                  strictMode(true),
                                                  reportInterval(1.0),
                                                  reverseWeiss(true),
                                                  useReportedWeiss(true),
                                                  useReportedArea(true),
                                                  useReportedAbsorptions(false),
                                                  instrumentArea(17.81),
                                                  weissA(0.814),
                                                  weissB(1.237),
                                                  useMeasuredTime(false),
                                                  enableNormalizationRecovery(true),
                                                  enableAbsorptionRecovery(false),
                                                  enableAutodetectStart(true),
                                                  enableAutodetectEnd(true),
                                                  verifyNormalizationBand(0.3),
                                                  verifyWhiteBand(0.9)
{
    wavelengths[0] = 467;
    wavelengths[1] = 530;
    wavelengths[2] = 660;
}

AcquireRRPSAP3W::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          flowCalibration(other.flowCalibration),
          area(other.area),
          wavelengths(),
          strictMode(other.strictMode),
          reportInterval(other.reportInterval),
          reverseWeiss(other.reverseWeiss),
          useReportedWeiss(other.useReportedWeiss),
          useReportedArea(other.useReportedArea),
          useReportedAbsorptions(other.useReportedAbsorptions),
          instrumentArea(other.instrumentArea),
          weissA(other.weissA),
          weissB(other.weissB),
          useMeasuredTime(other.useMeasuredTime),
          enableNormalizationRecovery(other.enableNormalizationRecovery),
          enableAbsorptionRecovery(other.enableAbsorptionRecovery),
          enableAutodetectStart(other.enableAutodetectStart),
          enableAutodetectEnd(other.enableAutodetectEnd),
          verifyNormalizationBand(other.verifyNormalizationBand),
          verifyWhiteBand(other.verifyWhiteBand)
{
    memcpy(wavelengths, other.wavelengths, sizeof(wavelengths));
}

AcquireRRPSAP3W::IntensityData::IntensityData() : startTime(FP::undefined())
{
    for (int color = 0; color < 3; color++) {
        Ip[color] = FP::undefined();
        If[color] = FP::undefined();
        In[color] = FP::undefined();
    }
}

QDataStream &operator<<(QDataStream &stream, const AcquireRRPSAP3W::IntensityData &data)
{
    stream << data.startTime;
    for (int color = 0; color < 3; color++) {
        stream << data.Ip[color];
        stream << data.If[color];
        stream << data.In[color];
    }
    return stream;
}

QDataStream &operator>>(QDataStream &stream, AcquireRRPSAP3W::IntensityData &data)
{
    stream >> data.startTime;
    for (int color = 0; color < 3; color++) {
        stream >> data.Ip[color];
        stream >> data.If[color];
        stream >> data.In[color];
    }
    return stream;
}

AcquireRRPSAP3W::AccumulatorMeasurement::AccumulatorMeasurement() : endTime(FP::undefined()),
                                                                    psapTime(INTEGER::undefined()),
                                                                    sum(INTEGER::undefined())
{ }

void AcquireRRPSAP3W::AccumulatorMeasurement::reset()
{
    endTime = FP::undefined();
    psapTime = INTEGER::undefined();
    sum = INTEGER::undefined();
}

void AcquireRRPSAP3W::resetAccumulatorStream()
{
    for (int i = 0; i < 3; i++) {
        reportedResetIn[i] = FP::undefined();

        accumulatorSample[i].reset();
        accumulatorReference[i].reset();
        accumulatorConversions[i].reset();
    }
    accumulatorFlow.reset();
    accumulatorFlowSeconds.reset();
    accumulatorDarkSample = FP::undefined();
    accumulatorDarkReference = FP::undefined();

    accumulatorBinStartTime = FP::undefined();
    accumulatorBinStartIndex = 0;
    accumulatorBinStartPSAPTime = INTEGER::undefined();

    reportedWeissA = FP::undefined();
    reportedWeissB = FP::undefined();
    reportedArea = FP::undefined();
    reportedResetTime = INTEGER::undefined();
    reportedFlowCalibration[0] = 1000;
    reportedFlowCalibration[1] = 1240;
    reportedFlowCalibration[2] = 2600;
}

void AcquireRRPSAP3W::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("RadianceResearch");
    instrumentMeta["Model"].setString("PSAP-3W");

    lastStartLogTime = FP::undefined();

    resetAccumulatorStream();

    accumulatedVolume = FP::undefined();
    for (int color = 0; color < 3; color++) {
        filterNormalization[color] = FP::undefined();
        priorIr[color] = FP::undefined();
        priorIn[color] = FP::undefined();
    }
    filterIsNotWhite = false;
    normalizationStartTime = FP::undefined();
    normalizationPSAPTime = INTEGER::undefined();

    lastTransmittanceCheckTime = FP::undefined();
    transmittanceWarningThreshold = 0.002;

    normalizationAcceptRemaining = 0;
    whiteFilterChangeStartTime = FP::undefined();

    logMetaState = NotEmitted;
    realtimeMetaState = NotEmitted;
    forceRealtimeStateEmit = true;

    forceFilterBreak = false;
    requireFilterBreak = false;

    filterChangeStartPSAPTime = INTEGER::undefined();
    filterChangeEndPSAPTime = INTEGER::undefined();
}

AcquireRRPSAP3W::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          flowCalibration(),
          area(17.81),
          wavelengths(),
          strictMode(true),
          reportInterval(1.0),
          reverseWeiss(true),
          useReportedWeiss(true),
          useReportedArea(true),
          useReportedAbsorptions(false),
          instrumentArea(17.81),
          weissA(0.814),
          weissB(1.237),
          useMeasuredTime(false),
          enableNormalizationRecovery(true),
          enableAbsorptionRecovery(false),
          enableAutodetectStart(true),
          enableAutodetectEnd(true),
          verifyNormalizationBand(0.3),
          verifyWhiteBand(0.9)
{
    wavelengths[0] = 467;
    wavelengths[1] = 530;
    wavelengths[2] = 660;
    setFromSegment(other);
}

AcquireRRPSAP3W::Configuration::Configuration(const Configuration &under, const ValueSegment &over,
                                              double s,
                                              double e) : start(s),
                                                          end(e),
                                                          flowCalibration(under.flowCalibration),
                                                          area(under.area),
                                                          wavelengths(),
                                                          strictMode(under.strictMode),
                                                          reportInterval(under.reportInterval),
                                                          reverseWeiss(under.reverseWeiss),
                                                          useReportedWeiss(under.useReportedWeiss),
                                                          useReportedArea(under.useReportedArea),
                                                          useReportedAbsorptions(
                                                                  under.useReportedAbsorptions),
                                                          instrumentArea(under.instrumentArea),
                                                          weissA(under.weissA),
                                                          weissB(under.weissB),
                                                          useMeasuredTime(under.useMeasuredTime),
                                                          enableNormalizationRecovery(
                                                                  under.enableNormalizationRecovery),
                                                          enableAbsorptionRecovery(
                                                                  under.enableAbsorptionRecovery),
                                                          enableAutodetectStart(
                                                                  under.enableAutodetectStart),
                                                          enableAutodetectEnd(
                                                                  under.enableAutodetectEnd),
                                                          verifyNormalizationBand(
                                                                  under.verifyNormalizationBand),
                                                          verifyWhiteBand(under.verifyWhiteBand)
{
    memcpy(wavelengths, under.wavelengths, sizeof(wavelengths));
    setFromSegment(over);
}

void AcquireRRPSAP3W::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["FlowCalibration"].exists())
        flowCalibration = Variant::Composite::toCalibration(config["FlowCalibration"]);

    if (config["UseMeasuredTime"].exists())
        useMeasuredTime = config["UseMeasuredTime"].toBool();

    if (FP::defined(config["Area/#0"].toDouble())) {
        area = config["Area/#0"].toDouble();
        if (FP::defined(area) && area < 0.1)
            area *= 1E6;
    }
    if (FP::defined(config["Area"].toDouble())) {
        area = config["Area"].toDouble();
        if (FP::defined(area) && area < 0.1)
            area *= 1E6;
    }

    if (FP::defined(config["Wavelength/B"].toDouble()))
        wavelengths[0] = config["Wavelength/B"].toDouble();
    if (FP::defined(config["Wavelength/G"].toDouble()))
        wavelengths[1] = config["Wavelength/G"].toDouble();
    if (FP::defined(config["Wavelength/R"].toDouble()))
        wavelengths[2] = config["Wavelength/R"].toDouble();

    if (config["Weiss/Reverse"].exists())
        reverseWeiss = config["Weiss/Reverse"].toBool();
    if (config["Weiss/UseReported"].exists())
        useReportedWeiss = config["Weiss/UseReported"].toBool();
    if (FP::defined(config["Weiss/A"].toDouble()))
        weissA = config["Weiss/A"].toDouble();
    if (FP::defined(config["Weiss/B"].toDouble()))
        weissB = config["Weiss/B"].toDouble();

    if (config["UseReportedArea"].exists())
        useReportedArea = config["UseReportedArea"].toBool();
    if (FP::defined(config["InstrumentArea"].toDouble()))
        instrumentArea = config["InstrumentArea"].toDouble();

    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    {
        double v = config["ReportInterval"].toDouble();
        if (FP::defined(v) && v > 0.0)
            reportInterval = v;
    }

    if (config["Absorption/UseReported"].exists())
        useReportedAbsorptions = config["Absorption/UseReported"].toBool();
    if (config["Absorption/EnableRecovery"].exists())
        enableAbsorptionRecovery = config["Absorption/EnableRecovery"].toBool();

    if (config["Autodetect/Start/Enable"].exists())
        enableAutodetectStart = config["Autodetect/Start/Enable"].toBool();
    if (config["Autodetect/End/Enable"].exists())
        enableAutodetectEnd = config["Autodetect/End/Enable"].toBool();

    if (config["Filter/EnableNormalizationRecovery"].exists())
        enableNormalizationRecovery = config["Filter/EnableNormalizationRecovery"].toBool();
    if (config["Filter/VerifyWhiteBand"].exists())
        verifyWhiteBand = config["Filter/VerifyWhiteBand"].toDouble();
    if (config["Filter/VerifyNormalizationBand"].exists())
        verifyNormalizationBand = config["Filter/VerifyNormalizationBand"].toDouble();
}

AcquireRRPSAP3W::AcquireRRPSAP3W(const ValueSegment::Transfer &configData,
                                 const std::string &loggingContext) : FramedInstrument("psap3w",
                                                                                       loggingContext),
                                                                      lastRecordTime(
                                                                              FP::undefined()),
                                                                      autoprobeStatus(
                                                                              AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                      responseState(RESP_WAIT),
                                                                      autoprobeValidRecords(0),
                                                                      autoprobeTotalFrames(0),
                                                                      sampleState(
                                                                              SAMPLE_FILTER_NORMALIZE)
{
    setDefaultInvalid();
    config.append(Configuration());

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }

    Variant::Write defaultSmoother = Variant::Write::empty();
    defaultSmoother["Type"].setString("FixedTime");
    defaultSmoother["RSD"].setDouble(0.001);
    defaultSmoother["Time"].setDouble(60.0);
    defaultSmoother["MinimumTime"].setDouble(30.0);
    defaultSmoother["DiscardTime"].setDouble(8.0);
    for (int color = 0; color < 3; color++) {
        filterNormalizeIp[color] = BaselineSmoother::fromConfiguration(defaultSmoother, configData,
                                                                       "Filter/Normalize");
        filterNormalizeIf[color] = BaselineSmoother::fromConfiguration(defaultSmoother, configData,
                                                                       "Filter/Normalize");
        filterNormalizeIn[color] = BaselineSmoother::fromConfiguration(defaultSmoother, configData,
                                                                       "Filter/Normalize");
    }

    defaultSmoother["RSD"].setDouble(0.02);
    defaultSmoother["DiscardTime"].setEmpty();
    defaultSmoother["Band"].setDouble(2.0);
    for (int color = 0; color < 3; color++) {
        filterChangeIf[color] = BaselineSmoother::fromConfiguration(defaultSmoother, configData,
                                                                    "Autodetect/Smoother");
        filterChangeIrc[color] = BaselineSmoother::fromConfiguration(defaultSmoother, configData,
                                                                     "Autodetect/Smoother");
    }

    DynamicTimeInterval::Variable *defaultTime = new DynamicTimeInterval::Variable;
    defaultTime->set(Time::Hour, 2, false);
    acceptNormalizationTime =
            DynamicTimeInterval::fromConfiguration(configData, "Filter/ResumeTimeout",
                                                   FP::undefined(), FP::undefined(), true,
                                                   defaultTime);

    delete defaultTime;
}

void AcquireRRPSAP3W::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_INITIALIZE;
    autoprobeValidRecords = 0;
    autoprobeTotalFrames = 0;
}

void AcquireRRPSAP3W::setToInteractive()
{ responseState = RESP_INITIALIZE; }


ComponentOptions AcquireRRPSAP3WComponent::getBaseOptions()
{
    ComponentOptions options;

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(tr("spot", "name"), tr("Area of the sample spot"),
                                            tr("This is the area of the sample spot.  If it is greater "
                                                           "than 0.1 it is assumed to be in mm\xC2\xB2, otherwise it is "
                                                           "treated as being in m\xC2\xB2."),
                                            tr("17.83 mm\xC2\xB2"), 1);
    d->setMinimum(0.0, false);
    options.add("area", d);

    ComponentOptionSingleCalibration *cal =
            new ComponentOptionSingleCalibration(tr("cal-q", "name"), tr("Flow rate calibration"),
                                                 tr("This is the calibration applied to the flow reported by "
                                                            "the instrument."), "");
    cal->setAllowInvalid(false);
    cal->setAllowConstant(true);
    options.add("cal-q", cal);

    BaselineSmootherOption *smoother = new BaselineSmootherOption(tr("filter-normalize", "name"),
                                                                  tr("Filter normalization smoother"),
                                                                  tr("This defines the smoothing used to establish a normalization at "
                                                                             "the start of the filter."),
                                                                  tr("60 seconds max, 30 seconds min, 8 second delay, <0.001 RSD",
                                                                     "normalize smoother"));
    smoother->setDefault(new BaselineFixedTime(30, 60, 0.001, FP::undefined(), 8.0));
    smoother->setSpikeDetection(false);
    smoother->setStabilityDetection(true);
    options.add("filter-normalize", smoother);

    smoother = new BaselineSmootherOption(tr("filter-change", "name"), tr("Filter change smoother"),
                                          tr("This defines the smoothing used to detect the start and end of "
                                                     "filter changes."),
                                          tr("60 seconds max, 30 seconds min, <0.02 RSD, 2.0 spike factor",
                                             "normalize smoother"));
    smoother->setDefault(new BaselineFixedTime(30, 60, 0.02, 2.0, 0.0));
    smoother->setSpikeDetection(true);
    smoother->setStabilityDetection(true);
    options.add("filter-change", smoother);

    options.add("disable-recovery", new ComponentOptionBoolean(tr("disable-recovery", "name"),
                                                               tr("Disable normalization recovery"),
                                                               tr("When set, this disables recovery of the normalization values from "
                                                                          "the instrument reported transmittance.  This can be useful if "
                                                                          "the reported transmittance is not valid."),
                                                               QString()));

    return options;
}

AcquireRRPSAP3W::AcquireRRPSAP3W(const ComponentOptions &options, const std::string &loggingContext)
        : FramedInstrument("psap3w", loggingContext),
          lastRecordTime(FP::undefined()),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          responseState(RESP_WAIT),
          autoprobeValidRecords(0),
          sampleState(SAMPLE_FILTER_NORMALIZE)
{
    setDefaultInvalid();
    config.append(Configuration());

    if (options.isSet("area")) {
        double value = qobject_cast<ComponentOptionSingleDouble *>(options.get("area"))->get();
        Q_ASSERT(FP::defined(value) && value > 0.0);
        if (value < 0.1)
            value *= 1E6;
        config.first().area = value;
    }

    if (options.isSet("cal-q")) {
        config.first().flowCalibration =
                qobject_cast<ComponentOptionSingleCalibration *>(options.get("cal-q"))->get();
    }

    if (options.isSet("disable-recovery")) {
        config.first().enableNormalizationRecovery =
                !qobject_cast<ComponentOptionBoolean *>(options.get("disable-recovery"))->get();
    }

    BaselineSmootherOption *smootherOption =
            qobject_cast<BaselineSmootherOption *>(options.get("filter-normalize"));
    for (int color = 0; color < 3; color++) {
        filterNormalizeIp[color] = smootherOption->getSmoother();
        filterNormalizeIf[color] = smootherOption->getSmoother();
        filterNormalizeIn[color] = smootherOption->getSmoother();
    }

    smootherOption = qobject_cast<BaselineSmootherOption *>(options.get("filter-change"));
    for (int color = 0; color < 3; color++) {
        filterChangeIf[color] = smootherOption->getSmoother();
        filterChangeIrc[color] = smootherOption->getSmoother();
    }

    /* Set these so it never rejects things (since it's under external/no
     * control). */
    config.first().verifyNormalizationBand = FP::undefined();
    acceptNormalizationTime = new DynamicTimeInterval::Undefined;

    /* We have no other indicator for filter changes, so we must use them */
    config.first().enableAutodetectStart = true;
    config.first().enableAutodetectEnd = true;
}

AcquireRRPSAP3W::~AcquireRRPSAP3W()
{
    delete acceptNormalizationTime;
    for (int color = 0; color < 3; color++) {
        delete filterNormalizeIp[color];
        delete filterNormalizeIf[color];
        delete filterNormalizeIn[color];
        delete filterChangeIf[color];
        delete filterChangeIrc[color];
    }
}

void AcquireRRPSAP3W::logValue(double startTime,
                               double endTime,
                               SequenceName::Component name,
                               Variant::Root value)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireRRPSAP3W::realtimeValue(double time, SequenceName::Component name, Variant::Root value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", name), std::move(value), time,
                                time + 1.0);
}

Variant::Root AcquireRRPSAP3W::buildFilterValueMeta() const
{
    Variant::Root result;
    auto target = result.write();

    for (int color = 0; color < 3; color++) {
        target.metadataHashChild("Ip" + colorLookup[color]).metadataReal("Description")
              .setString("Sample detector count");
        target.metadataHashChild("Ip" + colorLookup[color]).metadataReal("Format")
              .setString("0000000.0");
        target.metadataHashChild("Ip" + colorLookup[color]).metadataReal("Wavelength")
              .setDouble(config.first().wavelengths[color]);

        target.metadataHashChild("If" + colorLookup[color]).metadataReal("Description")
              .setString("Reference detector count");
        target.metadataHashChild("If" + colorLookup[color]).metadataReal("Format")
              .setString("0000000.0");
        target.metadataHashChild("If" + colorLookup[color]).metadataReal("Wavelength")
              .setDouble(config.first().wavelengths[color]);

        target.metadataHashChild("In" + colorLookup[color]).metadataReal("Description")
              .setString("Normalized intensity");
        target.metadataHashChild("In" + colorLookup[color]).metadataReal("Format")
              .setString("00.0000000");
        target.metadataHashChild("In" + colorLookup[color]).metadataReal("Wavelength")
              .setDouble(config.first().wavelengths[color]);
    }

    return result;
}

SequenceValue::Transfer AcquireRRPSAP3W::buildLogMeta(double time, bool differenceAbsorptions) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_rr_psap3w");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);

    Variant::Root flowProcessing = processing;
    for (int i = 0; i < 3; i++) {
        if (!FP::defined(reportedFlowCalibration[i]))
            continue;
        flowProcessing["Parameters"].hash("RawCalibration")
                                    .array(i)
                                    .setDouble(reportedFlowCalibration[i]);
    }

    result.emplace_back(SequenceName({}, "raw_meta", "Q"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(flowProcessing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Flow"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(6);

    result.emplace_back(SequenceName({}, "raw_meta", "Qt"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.00000");
    result.back().write().metadataReal("Units").setString("m³");
    result.back().write().metadataReal("Description").setString("Accumulated sample volume");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");

    result.emplace_back(SequenceName({}, "raw_meta", "L"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0000");
    result.back().write().metadataReal("Units").setString("m");
    result.back().write().metadataReal("Description").setString("Integrated sample length, Qt/A");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");

    result.emplace_back(SequenceName({}, "raw_meta", "IrB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0.0000000");
    result.back().write().metadataReal("GroupUnits").setString("Transmittance");
    result.back().write().metadataReal("Description").setString("Transmittance");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("Transmittance"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "IrG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0.0000000");
    result.back().write().metadataReal("GroupUnits").setString("Transmittance");
    result.back().write().metadataReal("Description").setString("Transmittance");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("Transmittance"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "IrR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0.0000000");
    result.back().write().metadataReal("GroupUnits").setString("Transmittance");
    result.back().write().metadataReal("Description").setString("Transmittance");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("Transmittance"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

    Variant::Root absorptionProcessing = processing;
    if (!differenceAbsorptions) {
        double v = reportedWeissA;
        if (!FP::defined(v) || !config.first().useReportedWeiss)
            v = config.first().weissA;
        absorptionProcessing["Parameters"].hash("A").setDouble(v);
        v = reportedWeissB;
        if (!FP::defined(v) || !config.first().useReportedWeiss)
            v = config.first().weissB;
        absorptionProcessing["Parameters"].hash("B").setDouble(v);
    }

    result.emplace_back(SequenceName({}, "raw_meta", "BaB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back().write().metadataReal("Description")
          .setString("Aerosol light absorption coefficient");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back()
          .write()
          .metadataReal("Processing")
          .toArray()
          .after_back()
          .set(absorptionProcessing);
    if (differenceAbsorptions) {
        result.back()
              .write()
              .metadataReal("Smoothing")
              .hash("Mode")
              .setString("BeersLawAbsorptionInitial");
        result.back().write().metadataReal("Smoothing").hash("Parameters").hash("L").setString("L");
        result.back()
              .write()
              .metadataReal("Smoothing")
              .hash("Parameters")
              .hash("Transmittance")
              .setString("IrB");
    }
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("Bap"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "BaG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light absorption coefficient");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back()
          .write()
          .metadataReal("Processing")
          .toArray()
          .after_back()
          .set(absorptionProcessing);
    if (differenceAbsorptions) {
        result.back()
              .write()
              .metadataReal("Smoothing")
              .hash("Mode")
              .setString("BeersLawAbsorptionInitial");
        result.back().write().metadataReal("Smoothing").hash("Parameters").hash("L").setString("L");
        result.back()
              .write()
              .metadataReal("Smoothing")
              .hash("Parameters")
              .hash("Transmittance")
              .setString("IrG");
    }
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("Bap"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "BaR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light absorption coefficient");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back()
          .write()
          .metadataReal("Processing")
          .toArray()
          .after_back()
          .set(absorptionProcessing);
    if (differenceAbsorptions) {
        result.back()
              .write()
              .metadataReal("Smoothing")
              .hash("Mode")
              .setString("BeersLawAbsorptionInitial");
        result.back().write().metadataReal("Smoothing").hash("Parameters").hash("L").setString("L");
        result.back()
              .write()
              .metadataReal("Smoothing")
              .hash("Parameters")
              .hash("Transmittance")
              .setString("IrR");
    }
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("Bap"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "IfB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Reference detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I reference"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "IfG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Reference detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I reference"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "IfR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Reference detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I reference"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "IpB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Sample detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I sample"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "IpG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Sample detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I sample"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "IpR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Sample detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I sample"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "Ff"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataInteger("Format").setString("0000000000");
    result.back().write().metadataInteger("GroupUnits").setString("FilterID");
    result.back().write().metadataInteger("Description").setString("Filter ID");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataInteger("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataInteger("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back().write()
          .metadataSingleFlag("STP")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");
    result.back().write().metadataSingleFlag("STP").hash("Bits").setInt64(0x0200);
    result.back().write()
          .metadataSingleFlag("STP")
          .hash("Description")
          .setString("Data reported at STP");

    result.back().write()
          .metadataSingleFlag("Weiss")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");
    result.back().write().metadataSingleFlag("Weiss").hash("Bits").setInt64(0x0080);
    result.back().write()
          .metadataSingleFlag("Weiss")
          .hash("Description")
          .setString("Weiss loading correction applied");

    result.back().write()
          .metadataSingleFlag("FlowError")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");
    result.back().write().metadataSingleFlag("FlowError").hash("Bits").setInt64(0x0010);
    result.back().write()
          .metadataSingleFlag("FlowError")
          .hash("Description")
          .setString("Flow less that 0.1 lpm was set to 0.1 lpm");

    result.back().write()
          .metadataSingleFlag("FlowOverride")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");
    result.back().write().metadataSingleFlag("FlowOverride").hash("Bits").setInt64(0x0020);
    result.back().write()
          .metadataSingleFlag("FlowOverride")
          .hash("Description")
          .setString("Flow overriden to the user specified value");

    result.back().write()
          .metadataSingleFlag("RedDetectorLow")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");
    result.back().write().metadataSingleFlag("RedDetectorLow").hash("Bits").setInt64(0x0100);
    result.back().write()
          .metadataSingleFlag("RedDetectorLow")
          .hash("Description")
          .setString("Red detector signal or reference below 150k");

    result.back().write()
          .metadataSingleFlag("GreenDetectorLow")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");
    result.back().write().metadataSingleFlag("GreenDetectorLow").hash("Bits").setInt64(0x0200);
    result.back().write()
          .metadataSingleFlag("GreenDetectorLow")
          .hash("Description")
          .setString("Green detector signal or reference below 150k");

    result.back().write()
          .metadataSingleFlag("BlueDetectorLow")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");
    result.back().write().metadataSingleFlag("BlueDetectorLow").hash("Bits").setInt64(0x0400);
    result.back().write()
          .metadataSingleFlag("BlueDetectorLow")
          .hash("Description")
          .setString("Blue detector signal or reference below 150k");

    result.back().write()
          .metadataSingleFlag("RedDetectorHigh")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");
    result.back().write().metadataSingleFlag("RedDetectorHigh").hash("Bits").setInt64(0x1000);
    result.back().write()
          .metadataSingleFlag("RedDetectorHigh")
          .hash("Description")
          .setString("Red detector signal or reference above 950k");

    result.back().write()
          .metadataSingleFlag("GreenDetectorHigh")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");
    result.back().write().metadataSingleFlag("GreenDetectorHigh").hash("Bits").setInt64(0x2000);
    result.back().write()
          .metadataSingleFlag("GreenDetectorHigh")
          .hash("Description")
          .setString("Green detector signal or reference above 950k");

    result.back().write()
          .metadataSingleFlag("BlueDetectorHigh")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");
    result.back().write().metadataSingleFlag("BlueDetectorHigh").hash("Bits").setInt64(0x4000);
    result.back().write()
          .metadataSingleFlag("BlueDetectorHigh")
          .hash("Description")
          .setString("Green detector signal or reference above 950k");

    result.back().write()
          .metadataSingleFlag("CurrentOverrange")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");
    result.back().write().metadataSingleFlag("CurrentOverrange").hash("Bits").setInt64(0x8000);
    result.back().write()
          .metadataSingleFlag("CurrentOverrange")
          .hash("Description")
          .setString(
                  "Photodiode current was overrange for any boxcar in the averaging interval, data may be bad");

    result.back().write()
          .metadataSingleFlag("FilterChanging")
          .hash("Description")
          .setString("Changing filter");
    result.back().write().metadataSingleFlag("FilterChanging").hash("Bits").setInt64(0x00010000);
    result.back().write()
          .metadataSingleFlag("FilterChanging")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");

    result.back().write()
          .metadataSingleFlag("WhiteFilterChanging")
          .hash("Description")
          .setString("Changing to a known white filter");
    result.back().write()
          .metadataSingleFlag("WhiteFilterChanging")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");

    result.back().write()
          .metadataSingleFlag("Normalizing")
          .hash("Description")
          .setString("Establishing normalization factor for filter");
    result.back().write()
          .metadataSingleFlag("Normalizing")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");

    result.back().write()
          .metadataSingleFlag("WhiteFilterNormalizing")
          .hash("Description")
          .setString("Establishing normalization factor for a white filter");
    result.back().write()
          .metadataSingleFlag("WhiteFilterNormalizing")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");

    result.back().write()
          .metadataSingleFlag("BlueTransmittanceMedium")
          .hash("Description")
          .setString("Blue transmittance less than 0.7");
    result.back().write()
          .metadataSingleFlag("BlueTransmittanceMedium")
          .hash("Bits")
          .setInt64(0x00040000);
    result.back().write()
          .metadataSingleFlag("BlueTransmittanceMedium")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");

    result.back().write()
          .metadataSingleFlag("BlueTransmittanceLow")
          .hash("Description")
          .setString("Blue transmittance less than 0.5");
    result.back().write()
          .metadataSingleFlag("BlueTransmittanceLow")
          .hash("Bits")
          .setInt64(0x00080000);
    result.back().write()
          .metadataSingleFlag("BlueTransmittanceLow")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");

    result.back().write()
          .metadataSingleFlag("GreenTransmittanceMedium")
          .hash("Description")
          .setString("Green transmittance less than 0.7");
    result.back().write()
          .metadataSingleFlag("GreenTransmittanceMedium")
          .hash("Bits")
          .setInt64(0x00100000);
    result.back().write()
          .metadataSingleFlag("GreenTransmittanceMedium")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");

    result.back().write()
          .metadataSingleFlag("GreenTransmittanceLow")
          .hash("Description")
          .setString("Green transmittance less than 0.5");
    result.back().write()
          .metadataSingleFlag("GreenTransmittanceLow")
          .hash("Bits")
          .setInt64(0x00200000);
    result.back().write()
          .metadataSingleFlag("GreenTransmittanceLow")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");

    result.back().write()
          .metadataSingleFlag("RedTransmittanceMedium")
          .hash("Description")
          .setString("Red transmittance less than 0.7");
    result.back().write()
          .metadataSingleFlag("RedTransmittanceMedium")
          .hash("Bits")
          .setInt64(0x00400000);
    result.back().write()
          .metadataSingleFlag("RedTransmittanceMedium")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");

    result.back().write()
          .metadataSingleFlag("RedTransmittanceLow")
          .hash("Description")
          .setString("Red transmittance less than 0.5");
    result.back().write()
          .metadataSingleFlag("RedTransmittanceLow")
          .hash("Bits")
          .setInt64(0x00800000);
    result.back().write()
          .metadataSingleFlag("RedTransmittanceLow")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");

    result.back().write()
          .metadataSingleFlag("NonWhiteFilter")
          .hash("Description")
          .setString("Filter did not appear to be white");
    result.back().write()
          .metadataSingleFlag("NonWhiteFilter")
          .hash("Origin").toArray().after_back().setString("acquire_rr_psap3w");

    result.emplace_back(SequenceName({}, "raw_meta", "ZFILTER"), buildFilterValueMeta(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("Filter start parameters");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");

    result.emplace_back(SequenceName({}, "raw_meta", "ZWHITE"), buildFilterValueMeta(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("White filter parameters");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");

    result.emplace_back(SequenceName({}, "raw_meta", "ZSPOT"), buildFilterValueMeta(), time,
                        FP::undefined());
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataHash("Description").setString("Spot sampling parameters");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHashChild("InB").metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[0]);
    result.back().write().metadataHashChild("InB").metadataReal("Format").setString("00.0000000");
    result.back().write().metadataHashChild("InG").metadataReal("Description")
          .setString("Spot start normalized intensity");
    result.back().write().metadataHashChild("InG").metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[1]);
    result.back().write().metadataHashChild("InG").metadataReal("Format").setString("00.0000000");
    result.back().write().metadataHashChild("InR").metadataReal("Description")
          .setString("Spot start normalized intensity");
    result.back().write().metadataHashChild("InR").metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[2]);
    result.back().write().metadataHashChild("InR").metadataReal("Format").setString("00.0000000");
    result.back().write().metadataHashChild("InBLatest").metadataReal("Description")
          .setString("Spot end normalized intensity");
    result.back().write().metadataHashChild("InBLatest").metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[0]);
    result.back().write().metadataHashChild("InBLatest").metadataReal("Format")
          .setString("00.0000000");
    result.back().write().metadataHashChild("InGLatest").metadataReal("Description")
          .setString("Spot end normalized intensity");
    result.back().write().metadataHashChild("InGLatest").metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[1]);
    result.back().write().metadataHashChild("InGLatest").metadataReal("Format")
          .setString("00.0000000");
    result.back().write().metadataHashChild("InRLatest").metadataReal("Description")
          .setString("Spot end normalized intensity");
    result.back().write().metadataHashChild("InRLatest").metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[2]);
    result.back().write().metadataHashChild("InRLatest").metadataReal("Format")
          .setString("00.0000000");
    result.back().write().metadataHashChild("InB").metadataReal("Description")
          .setString("Spot end transmittance");
    result.back().write().metadataHashChild("IrB").metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[0]);
    result.back().write().metadataHashChild("IrB").metadataReal("Format").setString("0.0000000");
    result.back().write().metadataHashChild("IrG").metadataReal("Description")
          .setString("Spot end transmittance");
    result.back().write().metadataHashChild("IrG").metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[1]);
    result.back().write().metadataHashChild("IrG").metadataReal("Format").setString("0.0000000");
    result.back().write().metadataHashChild("IrR").metadataReal("Description")
          .setString("Spot end transmittance");
    result.back().write().metadataHashChild("IrR").metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[2]);
    result.back().write().metadataHashChild("IrR").metadataReal("Format").setString("0.0000000");
    result.back().write().metadataHashChild("Ff")
          .metadataInteger("Description")
          .setString("Filter ID");
    result.back().write().metadataHashChild("Ff").metadataInteger("Format").setString("0000000000");
    result.back().write().metadataHashChild("Qt").metadataReal("Description")
          .setString("Total spot sample volume");
    result.back().write().metadataHashChild("Qt").metadataReal("Format").setString("000.00");
    result.back().write().metadataHashChild("Qt").metadataReal("Units").setString("m³");
    result.back().write().metadataHashChild("Qt").metadataReal("ReportT").setDouble(0);
    result.back().write().metadataHashChild("Qt").metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataHashChild("L").metadataReal("Description")
          .setString("Total integrated spot sample length, Qt/A");
    result.back().write().metadataHashChild("L").metadataReal("Format").setString("00000.0000");
    result.back().write().metadataHashChild("L").metadataReal("Units").setString("m");
    result.back().write().metadataHashChild("L").metadataReal("ReportT").setDouble(0);
    result.back().write().metadataHashChild("L").metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataHashChild("Area").metadataReal("Description")
          .setString("Spot area");
    result.back().write().metadataHashChild("Area").metadataReal("Format").setString("00.000");
    result.back().write().metadataHashChild("Area").metadataReal("Units").setString("mm²");
    result.back().write().metadataHashChild("SLatest").metadataReal("Description")
          .setString("Instrument time at end of spot");
    result.back().write().metadataHashChild("S").metadataReal("Description")
          .setString("Instrument time at beginning of spot");
    result.back().write().metadataHashChild("WasWhite").metadataBoolean("Description")
          .setString("Spot was determined to be white at the start of sampling");

    return result;
}

SequenceValue::Transfer AcquireRRPSAP3W::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_rr_psap3w");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);

    result.emplace_back(SequenceName({}, "raw_meta", "VQ"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0000");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Flow meter input voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Flow voltage"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(6);


    result.emplace_back(SequenceName({}, "raw_meta", "IfD"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Reference detector dark signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Dark ref"));
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);
    result.back().write().metadataReal("Realtime").hash("Eavesdropper").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "IpD"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Sample detector dark signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Dark samp"));
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);
    result.back().write().metadataReal("Realtime").hash("Eavesdropper").hash("Hide").setBool(true);


    result.emplace_back(SequenceName({}, "raw_meta", "InB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("00.0000000");
    result.back().write().metadataReal("GroupUnits").setString("NormalizedIntensity");
    result.back().write().metadataReal("Description").setString("Normalized sample spot intensity");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I normalized"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);

    result.emplace_back(SequenceName({}, "raw_meta", "InG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("00.0000000");
    result.back().write().metadataReal("GroupUnits").setString("NormalizedIntensity");
    result.back().write().metadataReal("Description").setString("Normalized sample spot intensity");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I normalized"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);

    result.emplace_back(SequenceName({}, "raw_meta", "InR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("00.0000000");
    result.back().write().metadataReal("GroupUnits").setString("NormalizedIntensity");
    result.back().write().metadataReal("Description").setString("Normalized sample spot intensity");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I normalized"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);

    result.emplace_back(SequenceName({}, "raw_meta", "IngB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0.0000000");
    result.back().write().metadataReal("GroupUnits").setString("StabilityFactor");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Normalized sample spot intensity stability factor");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("Eavesdropper").hash("Hide").setBool(true);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I norm stab"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "IngG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0.0000000");
    result.back().write().metadataReal("GroupUnits").setString("StabilityFactor");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Normalized sample spot intensity stability factor");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("Eavesdropper").hash("Hide").setBool(true);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I norm stab"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "IngR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0.0000000");
    result.back().write().metadataReal("GroupUnits").setString("StabilityFactor");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Normalized sample spot intensity stability factor");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("Eavesdropper").hash("Hide").setBool(true);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I norm stab"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(7);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Normalize")
          .setString(QObject::tr("Waiting for filter stability..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("WhiteFilterNormalize")
          .setString(QObject::tr("Waiting for white filter stability..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("FilterChange")
          .setString(QObject::tr("Changing filter..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("WhiteFilterChange")
          .setString(QObject::tr("Changing white filter..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("RequireFilterChange")
          .setString(QObject::tr("FILTER CHANGE REQUIRED"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("RequireWhiteFilterChange")
          .setString(QObject::tr("WHITE FILTER CHANGE REQUIRED"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Wait")
          .setString(QObject::tr("STARTING COMMS"));

    return result;
}

SequenceMatch::Composite AcquireRRPSAP3W::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "Ff");
    sel.append({}, {}, "ZFILTER");
    sel.append({}, {}, "ZSPOT");
    sel.append({}, {}, "ZWHITE");
    sel.append({}, {}, "ZSTATE");
    return sel;
}

static double calculateSumIntensity(double sum, double count)
{
    if (!FP::defined(sum) || !FP::defined(count))
        return FP::undefined();
    if (count <= 0.0)
        return FP::undefined();
    return sum / count;
}

static double calculateConversionSubtraction(double dI, double sI, double dC, double sC)
{
    if (!FP::defined(dI) || !FP::defined(sI) || !FP::defined(dC) || !FP::defined(sC))
        return FP::undefined();
    if (sI <= 0.0 || sC <= 0.0)
        return FP::undefined();
    dI *= sC / sI;
    dC /= 16.0; /* Counts are output as 16 times the actual */
    dI /= dC;
    dI -= 4096.0;
    return dI;
}

static double calculateIn(double Iref, double Isam)
{
    if (!FP::defined(Iref) || !FP::defined(Isam))
        return FP::undefined();
    if (Iref <= 0.0 || Isam <= 0.0)
        return FP::undefined();
    return Isam / Iref;
}

static double calculateL(double Qt, double area)
{
    if (!FP::defined(Qt) || !FP::defined(area) || area <= 0.0 || Qt <= 0.0)
        return FP::undefined();
    return Qt / (area * 1E-6);
}

static double calculateIr(double In0, double In)
{
    if (!FP::defined(In0) || !FP::defined(In))
        return FP::undefined();
    if (In0 == 0.0)
        return FP::undefined();
    return In / In0;
}

static double calculateBa(double Ir0, double Qt0, double Ir1, double Qt1, double area)
{
    if (!FP::defined(Ir0) ||
            !FP::defined(Qt0) ||
            !FP::defined(Ir1) ||
            !FP::defined(Qt1) ||
            !FP::defined(area))
        return FP::undefined();
    if (Ir0 <= 0.0 || Ir1 <= 0.0)
        return FP::undefined();
    double dQt = Qt1 - Qt0;
    if (dQt <= 0.0)
        return FP::undefined();
    return log(Ir0 / Ir1) * (area / dQt);
}

static double calculateFlow(double VQ, const double flowCalibration[3])
{
    if (!FP::defined(VQ))
        return FP::undefined();
    if (!FP::defined(flowCalibration[1]))
        return FP::undefined();
    VQ *= 1000.0;
    if (VQ < flowCalibration[1]) {
        if (!FP::defined(flowCalibration[0]))
            return FP::undefined();
        return (VQ - flowCalibration[0]) *
                ((0.3 - 0.0) / (flowCalibration[1] - flowCalibration[0]));
    } else {
        if (!FP::defined(flowCalibration[2]))
            return FP::undefined();
        return (VQ - flowCalibration[1]) *
                ((2.0 - 0.3) / (flowCalibration[2] - flowCalibration[1])) + 0.3;
    }
}

static double accumulateVolume(double startTime, double endTime, double Q)
{
    if (!FP::defined(Q) || !FP::defined(endTime))
        return FP::undefined();
    if (!FP::defined(startTime))
        startTime = endTime - 1.0;
    if (Q < 0.0)
        return FP::undefined();
    if (endTime < startTime)
        return FP::undefined();
    return (Q * (endTime - startTime)) / 60000.0;
}

static double backoutWeiss(double Ba, double Ir, double A, double B)
{
    if (!FP::defined(Ba) || !FP::defined(Ir) || !FP::defined(A) || !FP::defined(B))
        return FP::undefined();
    return Ba * (A + Ir * B);
}

static double recalibrateFlow(double Ba, double Qraw, double Qcal)
{
    if (!FP::defined(Ba) || !FP::defined(Qraw) || !FP::defined(Qcal))
        return FP::undefined();
    if (Qraw <= 0.0 || Qcal <= 0.0)
        return FP::undefined();
    return Ba * (Qraw / Qcal);
}

static double recalibrateArea(double Ba, double areaRaw, double areaTrue)
{
    if (!FP::defined(Ba) || !FP::defined(areaRaw) || !FP::defined(areaTrue))
        return FP::undefined();
    if (areaRaw <= 0.0 || areaTrue <= 0.0)
        return FP::undefined();
    return Ba * (areaTrue / areaRaw);
}

#ifndef R_RECORD_DARK_SUBTRACTED
                                                                                                                        static double calculateDarkSubtracted( double I, double ID ) {
    if (!FP::defined(I) || !FP::defined(ID))
        return FP::undefined();
    return I - ID;
}
#endif


bool AcquireRRPSAP3W::isSameNormalization(double currentTime) const
{
    if (FP::defined(currentTime) && FP::defined(normalizationLatest.startTime)) {
        if (currentTime < normalizationLatest.startTime)
            return false;
        double checkTime = acceptNormalizationTime->applyConst(normalizationLatest.startTime,
                                                               normalizationLatest.startTime, true);
        if (FP::defined(checkTime) && checkTime <= currentTime)
            return false;
    }

    if (INTEGER::defined(reportedResetTime) && INTEGER::defined(normalizationPSAPTime)) {
        if (INTEGER::defined(data.psapTime)) {
            if (data.psapTime < reportedResetTime || data.psapTime < normalizationPSAPTime)
                return false;
        }
        return reportedResetTime == normalizationPSAPTime;
    }

    double checkBand = config.first().verifyNormalizationBand;
    if (FP::defined(checkBand)) {
        for (int color = 0; color < 3; color++) {
            double v = data.If[color].read().toDouble();
            if (!FP::defined(v))
                return false;
            if (!BaselineSmoother::inRelativeBand(normalizationLatest.If[color], v, checkBand))
                return false;
        }
    }

    return true;
}

void AcquireRRPSAP3W::advanceNormalization(double startTime, double endTime)
{
    forceFilterBreak = true;
    forceRealtimeStateEmit = true;

    for (int color = 0; color < 3; color++) {
        priorIr[color] = FP::undefined();
    }

    double effectiveEnd = startTime;
    if (!FP::defined(effectiveEnd))
        effectiveEnd = endTime - config.first().reportInterval;
    double add =
            accumulateVolume(normalizationLatest.startTime, effectiveEnd, data.Q.read().toDouble());
    if (FP::defined(add)) {
        accumulatedVolume += add;
    } else {
        qCDebug(log) << "Unable to add sample volume accumulated during down time, assuming zero";
    }
}

bool AcquireRRPSAP3W::canRecoverNormalization(double currentTime) const
{
    Q_UNUSED(currentTime);

    if (!config.first().enableNormalizationRecovery)
        return false;

    for (int color = 0; color < 3; color++) {
        if (FP::defined(reportedResetIn[color])) {
            continue;
        }

        double t = data.Irc[color].read().toDouble();
        double in = data.In[color].read().toDouble();
        if (FP::defined(t) && FP::defined(in) && t > 0.0) {
            continue;
        }

        return false;
    }
    return true;
}

void AcquireRRPSAP3W::recoverNormalization(double currentTime)
{
    Q_UNUSED(currentTime);

    for (int color = 0; color < 3; color++) {
        priorIr[color] = FP::undefined();
        filterStart.Ip[color] = FP::undefined();
        filterStart.If[color] = FP::undefined();
        filterStart.In[color] = FP::undefined();

        if (FP::defined(reportedResetIn[color])) {
            filterNormalization[color] = reportedResetIn[color];
            continue;
        }

        double t = data.Irc[color].read().toDouble();
        double in = data.In[color].read().toDouble();
        if (FP::defined(t) && FP::defined(in) && t > 0.0) {
            filterNormalization[color] = in / t;
            continue;
        }

        filterNormalization[color] = FP::undefined();
    }

    forceFilterBreak = true;
    forceRealtimeStateEmit = true;
    filterStart.startTime = FP::undefined();

    lastTransmittanceCheckTime = FP::undefined();
}

void AcquireRRPSAP3W::invalidateNormalization()
{
    for (int color = 0; color < 3; color++) {
        priorIr[color] = FP::undefined();
        filterStart.Ip[color] = FP::undefined();
        filterStart.If[color] = FP::undefined();
        filterStart.In[color] = FP::undefined();
        filterNormalization[color] = FP::undefined();
    }

    forceFilterBreak = true;
    forceRealtimeStateEmit = true;
    filterStart.startTime = FP::undefined();

    lastTransmittanceCheckTime = FP::undefined();
}

bool AcquireRRPSAP3W::autodetectStart() const
{
    if (!config.first().enableAutodetectStart)
        return false;

    if (INTEGER::defined(reportedResetTime) &&
            ((INTEGER::defined(normalizationPSAPTime) &&
                    reportedResetTime > normalizationPSAPTime) ||
                    (INTEGER::defined(filterChangeStartPSAPTime) &&
                            reportedResetTime > filterChangeStartPSAPTime)))
        return true;

    for (int color = 0; color < 3; color++) {
        /* Don't have either intensities or reset time (F/S records), so try an
         * alternate method */
        if (!data.If[color].read().exists() && !INTEGER::defined(reportedResetTime)) {
            if (filterChangeIrc[color]->spike())
                return true;
            continue;
        }

        if (filterChangeIf[color]->spike())
            return true;
    }
    return false;
}

bool AcquireRRPSAP3W::autodetectEnd() const
{
    if (!config.first().enableAutodetectEnd)
        return false;

    if (INTEGER::defined(reportedResetTime) &&
            ((INTEGER::defined(normalizationPSAPTime) &&
                    reportedResetTime > normalizationPSAPTime) ||
                    (INTEGER::defined(filterChangeEndPSAPTime) &&
                            reportedResetTime > filterChangeEndPSAPTime)))
        return true;

    for (int color = 0; color < 3; color++) {
        /* Don't have either the reported transmittance or reset time
         * (R records), so try an alternate method */
        if (!data.Irc[color].read().exists() && !INTEGER::defined(reportedResetTime)) {
            if (!filterChangeIf[color]->ready())
                return false;
            if (!filterChangeIf[color]->stable())
                return false;
            continue;
        }

        if (!filterChangeIrc[color]->ready())
            return false;
        if (!filterChangeIrc[color]->stable())
            return false;
        double v = filterChangeIrc[color]->value();
        if (!FP::defined(v))
            return false;
        if (fabs(v - 1.0) > 0.02)
            return false;
    }

    return true;
}

bool AcquireRRPSAP3W::normalizationDone() const
{
    for (int color = 0; color < 3; color++) {
        if (!data.In[color].read().exists())
            continue;
        if (!filterNormalizeIn[color]->ready())
            return false;
        if (!filterNormalizeIn[color]->stable())
            return false;
    }
    return true;
}

bool AcquireRRPSAP3W::isWhiteFilter() const
{
    double checkBand = config.first().verifyWhiteBand;
    if (FP::defined(checkBand)) {
        for (int color = 0; color < 3; color++) {
            if (!FP::defined(filterStart.In[color]))
                continue;
            if (!FP::defined(filterWhite.In[color]))
                continue;
            if (!BaselineSmoother::inRelativeBand(filterWhite.In[color], filterStart.In[color],
                                                  checkBand))
                return false;
        }
    }
    return true;
}

bool AcquireRRPSAP3W::whiteFilterTimeout(double time) const
{
    if (!FP::defined(whiteFilterChangeStartTime))
        return false;
    return time > whiteFilterChangeStartTime + 3600.0;
}

void AcquireRRPSAP3W::resetSmoothers()
{
    for (int color = 0; color < 3; color++) {
        filterNormalizeIp[color]->reset();
        filterNormalizeIf[color]->reset();
        filterNormalizeIn[color]->reset();
        filterChangeIrc[color]->reset();
        filterChangeIf[color]->reset();
    }
}

Variant::Root AcquireRRPSAP3W::buildNormalizationValue() const
{
    Variant::Root result;
    auto target = result.write();

    for (int color = 0; color < 3; color++) {
        target.hash("In" + colorLookup[color]).setDouble(filterNormalization[color]);
        target.hash("In" + colorLookup[color] + "Latest").setDouble(priorIn[color]);
        target.hash("Ir" + colorLookup[color]).setDouble(priorIr[color]);
    }
    target.hash("L").setDouble(calculateL(accumulatedVolume, config.first().area));
    target.hash("Area").setDouble(config.first().area);
    if (FP::defined(normalizationStartTime))
        target.hash("Ff").setInt64((qint64) floor(normalizationStartTime));
    else
        target.hash("Ff").setInt64(INTEGER::undefined());
    target.hash("Qt").setDouble(accumulatedVolume);
    target.hash("WasWhite").setBool(!filterIsNotWhite);

    return result;
}

Variant::Root AcquireRRPSAP3W::buildFilterValue(const IntensityData &data) const
{
    Variant::Root result;
    auto target = result.write();

    for (int color = 0; color < 3; color++) {
        target.hash("Ip" + colorLookup[color]).setDouble(data.Ip[color]);
        target.hash("If" + colorLookup[color]).setDouble(data.If[color]);
        target.hash("In" + colorLookup[color]).setDouble(data.In[color]);
    }

    return result;
}

void AcquireRRPSAP3W::emitFilterEnd(double endTime)
{
    if (!FP::defined(filterStart.startTime))
        return;
    double startTime = filterStart.startTime;
    filterStart.startTime = FP::undefined();
    if (persistentEgress == NULL)
        return;

    SequenceValue result
            (SequenceName({}, "raw", "ZFILTER"), buildFilterValue(filterStart), startTime, endTime);
    persistentEgress->incomingData(result);
}

void AcquireRRPSAP3W::emitWhiteEnd(double endTime)
{
    if (!FP::defined(filterWhite.startTime))
        return;
    double startTime = filterWhite.startTime;
    filterWhite.startTime = FP::undefined();
    if (persistentEgress == NULL)
        return;

    SequenceValue result
            (SequenceName({}, "raw", "ZWHITE"), buildFilterValue(filterWhite), startTime, endTime);
    persistentEgress->incomingData(result);
}

void AcquireRRPSAP3W::emitNormalizationEnd(double endTime)
{
    if (!FP::defined(normalizationStartTime))
        return;
    double startTime = normalizationStartTime;
    normalizationStartTime = FP::undefined();
    if (persistentEgress == NULL)
        return;

    SequenceValue::Transfer result;
    result.emplace_back(SequenceName({}, "raw", "ZSPOT"), buildNormalizationValue(), startTime,
                        endTime);

    qint64 value = normalizationPSAPTime;
    if (!INTEGER::defined(value))
        value = (qint64) floor(normalizationStartTime);

    result.emplace_back(SequenceName({}, "raw", "Ff"), Variant::Root(value), startTime, endTime);

    persistentEgress->incomingData(result);
}

void AcquireRRPSAP3W::insertDiagnosticCommonValues(Variant::Write &target) const
{
    target.hash("Values").hash("Flags").set(data.baseFlags);
    target.hash("Values").hash("RawFlags").set(data.flags);
    target.hash("Values").hash("Q").set(data.Q);
    for (int color = 0; color < 3; color++) {
        target.hash("Values").hash("Irc" + colorLookup[color]).set(data.Irc[color]);
        target.hash("Values").hash("Bac" + colorLookup[color]).set(data.Irc[color]);
        target.hash("Values").hash("If" + colorLookup[color]).set(data.If[color]);
        target.hash("Values").hash("Ip" + colorLookup[color]).set(data.Ip[color]);
        target.hash("Values").hash("In" + colorLookup[color]).set(data.In[color]);
    }
    target.hash("Values").hash("IpD").set(data.IpD);
    target.hash("Values").hash("IfD").set(data.IfD);
}

void AcquireRRPSAP3W::insertDiagnosticFilterValues(Variant::Write &target,
                                                   const QString &name,
                                                   const IntensityData &source) const
{
    target.hash(name).hash("StartTime").setDouble(source.startTime);
    for (int color = 0; color < 3; color++) {
        target.hash(name).hash("Ip" + colorLookup[color]).setDouble(source.Ip[color]);
        target.hash(name).hash("If" + colorLookup[color]).setDouble(source.If[color]);
        target.hash(name).hash("In" + colorLookup[color]).setDouble(source.In[color]);
    }
}

void AcquireRRPSAP3W::insertDiagnosticNormalization(Variant::Write &target) const
{
    target.hash("Normalization").hash("StartTime").setDouble(normalizationStartTime);
    target.hash("Normalization").hash("PSAPSwitchPressedTime").setInt64(normalizationPSAPTime);
    target.hash("Normalization").hash("Volume").setDouble(accumulatedVolume);
    target.hash("Normalization").hash("LatestTime").setDouble(normalizationLatest.startTime);
    for (int color = 0; color < 3; color++) {
        target.hash("Normalization")
              .hash("In" + colorLookup[color])
              .setDouble(normalizationLatest.In[color]);
        target.hash("Normalization")
              .hash("If" + colorLookup[color])
              .setDouble(normalizationLatest.If[color]);
        target.hash("Normalization")
              .hash("Ip" + colorLookup[color])
              .setDouble(normalizationLatest.Ip[color]);
    }
}

void AcquireRRPSAP3W::insertDiagnosticFilterNormalization(Variant::Write &target) const
{
    for (int color = 0; color < 3; color++) {
        target.hash("Baseline")
              .hash("Ip" + colorLookup[color])
              .set(filterNormalizeIp[color]->describeState());
        target.hash("Baseline")
              .hash("If" + colorLookup[color])
              .set(filterNormalizeIf[color]->describeState());
        target.hash("Baseline")
              .hash("In" + colorLookup[color])
              .set(filterNormalizeIn[color]->describeState());
        target.hash("Change")
              .hash("If" + colorLookup[color])
              .set(filterChangeIf[color]->describeState());
        target.hash("Change")
              .hash("Irc" + colorLookup[color])
              .set(filterChangeIrc[color]->describeState());
    }
    target.hash("Change").hash("PSAPSwitchPressedTime").setInt64(accumulatorBinStartPSAPTime);
}


static int indexOfNextPointer(const char *data, int length, int ch)
{
    const char *ptr = (const char *) memchr(data, ch, length);
    if (ptr == NULL)
        return -1;
    return (int) (ptr - data);
}

static int indexOfNext(const Util::ByteView &ba, int offset, char ch, int limit = -1)
{
    int rc;
    if (limit < 0) {
        rc = indexOfNextPointer(ba.data<const char *>(offset), ba.size() - offset, ch);
    } else {
        rc = indexOfNextPointer(ba.data<const char *>(offset), limit - offset, ch);
    }
    if (rc >= 0)
        return rc + offset;
    return limit;
}

static std::deque<Util::ByteView> splitLine(const Util::ByteView &line)
{
    std::deque<Util::ByteView> result;
    for (int start = 0, max = line.size(); start < max;) {
        int next = indexOfNext(line, start, ',');
        next = indexOfNext(line, start, ' ', next);
        next = indexOfNext(line, start, '\t', next);

        int quote = indexOfNext(line, start, '\"', next);
        quote = indexOfNext(line, start, '\'', quote);
        if (quote >= 0 && quote < next) {
            ++quote;
            if (quote >= max)
                return result;
            next = indexOfNext(line, quote, '\"');
            next = indexOfNext(line, quote, '\'', next);

            int limit = next;
            if (limit < 0)
                limit = max;
            if (quote + 1 >= limit) {
                result.emplace_back();
                start = limit + 1;
                continue;
            }

            char ch = line[quote];
            while (ch == ' ' || ch == '\t') {
                ++quote;
                if (quote >= limit)
                    break;
                ch = line[quote];
            }
            if (quote >= limit) {
                result.emplace_back();
                start = limit - 1;
                continue;
            }

            start = quote;
            if (next < 0) {
                result.emplace_back(line.mid(start));
                break;
            }
        } else {
            if (next < 0) {
                result.emplace_back(line.mid(start));
                break;
            }
        }

        if (start != 0 || start != next)
            result.emplace_back(line.mid(start, next - start));

        char ch = line[next];
        if (ch == ',' || ch == '\"' || ch == '\'') {
            ++next;
            if (next >= max)
                return result;
            ch = line[next];
        }
        while (ch == ' ' || ch == '\t') {
            ++next;
            if (next >= max)
                return result;
            ch = line[next];
        }

        Q_ASSERT(start != next);
        start = next;
    }
    return result;
}

int AcquireRRPSAP3W::parseTime(const Util::ByteView &field,
                               qint64 &time,
                               const SequenceName::Component &parsePrefix)
{
    bool ok = false;

    qint64 iyear = field.mid(0, 2).string_trimmed().parse_i32(&ok);
    if (!ok) return 1;
    if (iyear < 0) return 2;
    if (iyear > 99) return 3;
    /* I don't think there where even any prototypes before 2000, so this
     * should be safe */
    iyear += 2000;
    Variant::Root year(iyear);
    remap(parsePrefix + "YEAR", year);
    iyear = year.read().toInt64();

    qint64 imonth = field.mid(2, 2).string_trimmed().parse_i32(&ok);
    if (!ok) return 4;
    /* It looks like the PSAP can sometimes report month zero (just in the
     * change time field?), so just fix that to month one */
    if (imonth == 0)
        imonth = 1;
    if (imonth < 1) return 5;
    if (imonth > 12) return 6;
    Variant::Root month(imonth);
    remap(parsePrefix + "MONTH", month);
    imonth = month.read().toInt64();

    qint64 iday = field.mid(4, 2).string_trimmed().parse_i32(&ok);
    if (!ok) return 7;
    if (iday < 1) return 8;
    if (iday > 31) return 9;
    Variant::Root day(iday);
    remap(parsePrefix + "DAY", day);
    iday = day.read().toInt64();

    qint64 ihour = field.mid(6, 2).string_trimmed().parse_i32(&ok);
    if (!ok) return 17;
    if (ihour < 0) return 10;
    if (ihour > 24) return 11;
    Variant::Root hour(ihour);
    remap(parsePrefix + "HOUR", hour);
    ihour = hour.read().toInt64();

    qint64 iminute = field.mid(8, 2).string_trimmed().parse_i32(&ok);
    if (!ok) return 12;
    if (iminute < 0) return 13;
    if (iminute > 59) return 14;
    Variant::Root minute(iminute);
    remap(parsePrefix + "MINUTE", minute);
    iminute = minute.read().toInt64();

    qint64 isecond = field.mid(10, 2).string_trimmed().parse_i32(&ok);
    if (!ok) return 15;
    if (isecond < 0) return 16;
    if (isecond > 60) return 17;
    Variant::Root second(isecond);
    remap(parsePrefix + "SECOND", second);
    isecond = second.read().toInt64();

    if (INTEGER::defined(iyear) &&
            iyear >= 1900 &&
            iyear <= 2999 &&
            INTEGER::defined(imonth) &&
            imonth >= 1 &&
            imonth <= 12 &&
            INTEGER::defined(iday) &&
            iday >= 1 &&
            iday <= 31 &&
            INTEGER::defined(ihour) &&
            ihour >= 0 &&
            ihour <= 23 &&
            INTEGER::defined(iminute) &&
            iminute >= 0 &&
            iminute <= 59 &&
            INTEGER::defined(isecond) &&
            isecond >= 0 &&
            isecond <= 60) {
        time = (qint64) Time::fromDateTime(QDateTime(QDate((int) iyear, (int) imonth, (int) iday),
                                                     QTime((int) ihour, (int) iminute,
                                                           (int) isecond), Qt::UTC));
    }

    return 0;
}

int AcquireRRPSAP3W::consumeTime(std::deque<Util::ByteView> &fields,
                                 double &frameTime,
                                 qint64 &psapTime)
{
    if (fields.empty())
        return 1;
    auto field = fields[0];
    while (!field.empty() && (field.back() == ' ' || field.back() == '\t')) {
        field = field.mid(0, field.size() - 1);
    }
    while (!field.empty() && (field.front() == ' ' || field.front() == '\t')) {
        field = field.mid(1);
    }
    if (field.size() != 12)
        return -1;

    for (auto check : field) {
        switch (check) {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            break;
        default:
            return -2;
        }
    }

    fields.pop_front();

    psapTime = INTEGER::undefined();
    int code = parseTime(field, psapTime);
    if (code > 0)
        return code;

    if (!FP::defined(frameTime) && INTEGER::defined(psapTime)) {
        frameTime = (double) psapTime;

        configAdvance(frameTime);
    }

    return 0;
}

static double convertI24Hex(const Util::ByteView &field)
{
    if (field.size() != 6) return FP::undefined();
    bool ok = false;
    quint32 raw = field.parse_u32(&ok, 16);
    if (!ok) return FP::undefined();
    if (raw >= 0x1000000) return FP::undefined();
    qint32 s;
    /* Sign extension from 24 to 32 bits, then just rely on the language
     * conversion from U32 to I32. */
    if (raw & 0x800000)
        raw |= 0xFF000000;
    s = (qint32) raw;
    return (double) s;
}

void AcquireRRPSAP3W::outputMetadata(double currentTime, bool haveHighPrecsionTransmittance)
{
    if (!FP::defined(currentTime))
        return;
    if (FP::defined(lastStartLogTime) && currentTime < lastStartLogTime)
        return;
    lastStartLogTime = currentTime;

    MetadataState target;
    if (haveHighPrecsionTransmittance)
        target = MetadataEmitted;
    else
        target = MetadataEmittedWithoutHighPrecsionTransmittance;


    if (logMetaState != target && FP::defined(currentTime) && loggingEgress != NULL) {
        logMetaState = target;
        loggingEgress->incomingData(buildLogMeta(currentTime, haveHighPrecsionTransmittance));
    }

    if (realtimeMetaState != target && FP::defined(currentTime) && realtimeEgress != NULL) {
        realtimeMetaState = target;
        SequenceValue::Transfer meta = buildLogMeta(currentTime, haveHighPrecsionTransmittance);
        Util::append(buildRealtimeMeta(currentTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }
}

void AcquireRRPSAP3W::outputFlags(double startTime, double endTime, bool includeWeiss)
{
    Variant::Root result(data.baseFlags);

    if (INTEGER::defined(data.flags.read().toInt64())) {
        qint64 flagsBits(data.flags.read().toInt64());
        for (int i = 0;
                i <
                        (int) (sizeof(instrumentFlagTranslation) /
                                sizeof(instrumentFlagTranslation[0]));
                i++) {
            qint64 bit = (Q_INT64_C(1) << (qint64) i);
            if (!includeWeiss && bit == (qint64) instrumentFlagWeiss)
                continue;
            if (flagsBits & bit) {
                if (instrumentFlagTranslation[i] != NULL) {
                    result.write().applyFlag(instrumentFlagTranslation[i]);
                }
            }
        }
    }
    if (filterIsNotWhite) {
        result.write().applyFlag("NonWhiteFilter");
    }

    for (int color = 0; color < 3; color++) {
        double check = data.Ir[color].read().toDouble();
        if (!FP::defined(check))
            continue;
        if (check > 0.7)
            continue;

        switch (color) {
        case 0:
            result.write().applyFlag("BlueTransmittanceMedium");
            break;
        case 1:
            result.write().applyFlag("GreenTransmittanceMedium");
            break;
        case 2:
            result.write().applyFlag("RedTransmittanceMedium");
            break;
        default:
            Q_ASSERT(false);
            break;
        }

        if (check < 0.5) {
            switch (color) {
            case 0:
                result.write().applyFlag("BlueTransmittanceLow");
                break;
            case 1:
                result.write().applyFlag("GreenTransmittanceLow");
                break;
            case 2:
                result.write().applyFlag("RedTransmittanceLow");
                break;
            default:
                Q_ASSERT(false);
                break;
            }
        }
    }

    logValue(startTime, endTime, "F1", result);

    if (realtimeEgress && forceRealtimeStateEmit) {
        forceRealtimeStateEmit = false;

        Variant::Root v(INTEGER::undefined());
        if (INTEGER::defined(normalizationPSAPTime))
            v.write().setInt64(normalizationPSAPTime);
        else if (FP::defined(normalizationStartTime))
            v.write().setInt64((qint64) std::floor(normalizationStartTime));
        realtimeEgress->emplaceData(SequenceName({}, "raw", "Ff"), std::move(v), endTime,
                                    FP::undefined());

        Variant::Root state;
        switch (sampleState) {
        case SAMPLE_RUN:
            state.write().setString("Run");
            break;
        case SAMPLE_FILTER_NORMALIZE:
            state.write().setString("Normalize");
            break;
        case SAMPLE_FILTER_CHANGING:
            state.write().setString("FilterChange");
            break;
        case SAMPLE_WHITE_FILTER_NORMALIZE:
            state.write().setString("WhiteFilterNormalize");
            break;
        case SAMPLE_WHITE_FILTER_CHANGING:
            state.write().setString("WhiteFilterChange");
            break;
        case SAMPLE_REQUIRE_FILTER_CHANGE:
            state.write().setString("RequireFilterChange");
            break;
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
            state.write().setString("RequireWhiteFilterChange");
            break;
        }
        realtimeEgress->emplaceData(SequenceName({}, "raw", "ZSTATE"), state, endTime,
                                    FP::undefined());
    }
}

void AcquireRRPSAP3W::outputIntensityData(double startTime,
                                          double endTime,
                                          double nominalTimeAdvance)
{
    if (!FP::defined(startTime))
        return;
    if (FP::defined(lastStartLogTime) && startTime < lastStartLogTime)
        return;
    lastStartLogTime = startTime;

    if (sampleState == SAMPLE_RUN) {
        double priorVolume = accumulatedVolume;

        double add;
        if (config.first().useMeasuredTime)
            add = accumulateVolume(startTime, endTime, data.Q.read().toDouble());
        else
            add = accumulateVolume(0.0, nominalTimeAdvance, data.Q.read().toDouble());
        if (FP::defined(add)) {
            if (FP::defined(accumulatedVolume))
                accumulatedVolume += add;
            else
                accumulatedVolume = add;
        }

        data.Qt.write().setReal(accumulatedVolume);
        remap("Qt", data.Qt);

        data.L.write().setReal(calculateL(data.Qt.read().toDouble(), config.first().area));

        for (int color = 0; color < 3; color++) {
            data.Ir[color].write()
                          .setReal(calculateIr(filterNormalization[color],
                                               data.In[color].read().toReal()));
            remap("Ir" + colorLookup[color], data.Ir[color]);

            data.Ba[color].write()
                          .setReal(calculateBa(priorIr[color], priorVolume,
                                               data.Ir[color].read().toDouble(),
                                               data.Qt.read().toDouble(), config.first().area));

            if (config.first().enableAbsorptionRecovery &&
                    !FP::defined(data.Ba[color].read().toReal())) {
                double A = config.first().weissA;
                double B = config.first().weissB;
                if (config.first().useReportedWeiss && FP::defined(reportedWeissA))
                    A = reportedWeissA;
                if (config.first().useReportedWeiss && FP::defined(reportedWeissB))
                    B = reportedWeissB;
                double area = config.first().instrumentArea;
                if (config.first().useReportedArea && FP::defined(reportedArea))
                    area = reportedArea;

                double Ba = backoutWeiss(data.Bac[color].read().toDouble(),
                                         data.Irc[color].read().toDouble(), A, B);
                Ba = recalibrateFlow(Ba, data.Qraw.read().toDouble(), data.Q.read().toDouble());
                Ba = recalibrateArea(Ba, area, config.first().area);
                data.Ba[color].write().setReal(Ba);
            }
            remap("Ba" + colorLookup[color], data.Ba[color]);
        }

        if (!INTEGER::defined(normalizationPSAPTime) && INTEGER::defined(reportedResetTime))
            normalizationPSAPTime = reportedResetTime;
    } else {
        data.Qt.write().setEmpty();
        remap("Qt", data.Qt);
        data.L.write().setEmpty();

        for (int color = 0; color < 3; color++) {
            data.Ir[color].write().setReal(FP::undefined());
            data.Ba[color].write().setReal(FP::undefined());

            remap("Ir" + colorLookup[color], data.Ir[color]);
            remap("Ba" + colorLookup[color], data.Ba[color]);
        }
    }
    remap("L", data.L);

    outputFlags(startTime, endTime, false);

    logValue(startTime, endTime, "Q", data.Q);
    logValue(startTime, endTime, "Qt", data.Qt);
    if (data.VQ.read().exists())
        realtimeValue(endTime, "VQ", data.VQ);
    for (int color = 0; color < 3; color++) {
        logValue(startTime, endTime, "Ba" + colorLookup[color], data.Ba[color]);

        logValue(startTime, endTime, "If" + colorLookup[color], data.If[color]);
        logValue(startTime, endTime, "Ip" + colorLookup[color], data.Ip[color]);
    }

    /* Force breaks in the data stream on filter changes */
    if (forceFilterBreak && requireFilterBreak) {
        realtimeValue(endTime, "L", Variant::Root(FP::undefined()));

        for (int color = 0; color < 3; color++) {
            realtimeValue(endTime, "Ir" + colorLookup[color],
                          Variant::Root(FP::undefined()));
        }
        requireFilterBreak = false;
    } else {
        logValue(startTime, endTime, "L", data.L);
        for (int color = 0; color < 3; color++) {
            logValue(startTime, endTime, "Ir" + colorLookup[color], data.Ir[color]);
        }
        requireFilterBreak = true;
    }
    forceFilterBreak = false;

    realtimeValue(endTime, "IfD", data.IfD);
    realtimeValue(endTime, "IpD", data.IpD);
    for (int color = 0; color < 3; color++) {
        realtimeValue(endTime, "In" + colorLookup[color], data.In[color]);
        realtimeValue(endTime, "Ing" + colorLookup[color],
                      Variant::Root(filterNormalizeIn[color]->stabilityFactor()));
    }

    for (int color = 0; color < 3; color++) {
        priorIr[color] = data.Ir[color].read().toDouble();
        priorIn[color] = data.In[color].read().toDouble();
    }

    if (!FP::defined(lastTransmittanceCheckTime) || sampleState != SAMPLE_RUN) {
        lastTransmittanceCheckTime = endTime;
        transmittanceWarningThreshold = 0.002;
    } else if (lastTransmittanceCheckTime + 3600.0 <= endTime) {
        quint32 detected = 0;
        double dIrMax = FP::undefined();
        for (int color = 0; color < 3; color++) {
            double IrInstrument = data.Irc[color].read().toDouble();
            double IrCalculated = data.Ir[color].read().toDouble();

            if (FP::defined(IrInstrument) && FP::defined(IrCalculated)) {
                double dIr = fabs(IrInstrument - IrCalculated);
                if (dIr > transmittanceWarningThreshold) {
                    detected |= (1 << color);
                    dIrMax = dIr;
                }
            }
        }

        if (detected != 0) {
            QStringList detectedNames;
            QStringList detectedIr;
            QStringList detectedIrc;
            for (int color = 0; color < 3; color++) {
                detectedIr.append(QString::number(data.Ir[color].read().toDouble()));
                detectedIrc.append(QString::number(data.Irc[color].read().toDouble()));
                if (!(detected & (1 << color)))
                    continue;
                detectedNames.append(QString::fromStdString(colorLookup[color]));
            }
            QString combinedNames(detectedNames.join(","));
            QString combinedIr(detectedIr.join(","));
            QString combinedIrc(detectedIrc.join(","));

            qCDebug(log) << "Transmittance discrepancy in" << combinedNames
                         << "channel(s) detected at" << Logging::time(endTime) << ", instrument:"
                         << combinedIrc << ", calculated:" << combinedIr << ", threshold:"
                         << transmittanceWarningThreshold;

            Variant::Write info = Variant::Write::empty();
            info.hash("State").setString("Run");
            insertDiagnosticCommonValues(info);
            insertDiagnosticFilterValues(info, "Filter", filterStart);
            insertDiagnosticFilterValues(info, "White", filterWhite);
            insertDiagnosticFilterNormalization(info);
            insertDiagnosticNormalization(info);
            for (int color = 0; color < 3; color++) {
                info.hash("Values")
                    .hash("Ir" + colorLookup[color] + "Calculated")
                    .set(data.Ir[color]);
                info.hash("InstrumentTransmittance" + colorLookup[color])
                    .set(data.Irc[color]);
                info.hash("CalculatedTransmittance" + colorLookup[color])
                    .set(data.Ir[color]);
            }
            info.hash("Threshold").setReal(transmittanceWarningThreshold);
            event(endTime,
                  QObject::tr(
                          "Discrepancy between the instrument transmittance (%2) and the calculated (%3) detected in channels: %1.")
                          .arg(combinedNames, combinedIrc, combinedIr), true, info);

            transmittanceWarningThreshold = dIrMax + 0.001;
            lastTransmittanceCheckTime = endTime;
        }
    }
}

void AcquireRRPSAP3W::outputReportedData(double startTime, double endTime)
{
    resetAccumulatorStream();
    if (!FP::defined(startTime))
        return;
    if (FP::defined(lastStartLogTime) && startTime < lastStartLogTime)
        return;
    lastStartLogTime = startTime;

    if (config.first().reverseWeiss) {
        double A = config.first().weissA;
        double B = config.first().weissB;
        if (config.first().useReportedWeiss && FP::defined(reportedWeissA))
            A = reportedWeissA;
        if (config.first().useReportedWeiss && FP::defined(reportedWeissB))
            B = reportedWeissB;

        if (!INTEGER::defined(data.flags.read().toInteger()) ||
                (data.flags.read().toInt64() & instrumentFlagWeiss)) {
            for (int color = 0; color < 3; color++) {
                data.Ba[color].write()
                              .setReal(backoutWeiss(data.Ba[color].read().toDouble(),
                                                    data.Ir[color].read().toDouble(), A, B));
            }
        }
    }

    if (sampleState == SAMPLE_RUN) {
        double add;
        if (config.first().useMeasuredTime)
            add = accumulateVolume(startTime, endTime, data.Q.read().toDouble());
        else
            add = accumulateVolume(0.0, 1.0, data.Q.read().toDouble());
        if (FP::defined(add)) {
            if (FP::defined(accumulatedVolume))
                accumulatedVolume += add;
            else
                accumulatedVolume = add;
        }

        data.Qt.write().setReal(accumulatedVolume);
        remap("Qt", data.Qt);
        data.L.write().setReal(calculateL(data.Qt.read().toDouble(), config.first().area));
    } else {
        data.Qt.write().setEmpty();
        remap("Qt", data.Qt);
        data.L.write().setEmpty();

        for (int color = 0; color < 3; color++) {
            data.Ba[color].write().setEmpty();
            data.Ir[color].write().setEmpty();
        }
    }
    remap("L", data.L);

    double area = config.first().instrumentArea;
    if (config.first().useReportedArea && FP::defined(reportedArea))
        area = reportedArea;
    for (int color = 0; color < 3; color++) {
        double Ba = data.Ba[color].read().toDouble();
        Ba = recalibrateFlow(Ba, data.Qraw.read().toDouble(), data.Q.read().toDouble());
        Ba = recalibrateArea(Ba, area, config.first().area);
        data.Ba[color].write().setReal(Ba);

        remap("Ir" + colorLookup[color], data.Ir[color]);
        remap("Ba" + colorLookup[color], data.Ba[color]);
    }


    outputFlags(startTime, endTime, !config.first().reverseWeiss);

    logValue(startTime, endTime, "Q", data.Q);
    logValue(startTime, endTime, "Qt", data.Qt);
    for (int color = 0; color < 3; color++) {
        logValue(startTime, endTime, "Ba" + colorLookup[color], data.Ba[color]);
    }

    if (forceFilterBreak && requireFilterBreak) {
        realtimeValue(endTime, "L", Variant::Root(FP::undefined()));

        for (int color = 0; color < 3; color++) {
            realtimeValue(endTime, "Ir" + colorLookup[color],
                          Variant::Root(FP::undefined()));
        }
        requireFilterBreak = false;
    } else {
        logValue(startTime, endTime, "L", data.L);

        for (int color = 0; color < 3; color++) {
            logValue(startTime, endTime, "Ir" + colorLookup[color], data.Ir[color]);
        }
        requireFilterBreak = true;
    }
}

int AcquireRRPSAP3W::processRecordR(std::deque<Util::ByteView> fields, double &frameTime)
{
    qint64 psapTime = INTEGER::undefined();
    {
        int code = consumeTime(fields, frameTime, psapTime);
        if (code > 0)
            return code + 1000;
    }

    Util::ByteView field;
    bool ok = false;

    Variant::Root Ip[4];
    Variant::Root If[4];
    for (int color = 0; color < 4; ++color) {
        SequenceName::Component code;
        if (color >= 3)
            code = "D";
        else
            code = colorLookup[color];

        qint32 i32 = 0;
        quint32 u32 = 0;
        /* Just rely on the mechanics of signed/unsigned 32-bit integers to
         * deal with our sign extension */

        if (fields.empty()) return 100 * (color + 1) + 1;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (field.size() != 8) return 100 * (color + 1) + 2;
        i32 = (qint32) field.parse_u32(&ok, 16);
        if (!ok) return 100 * (color + 1) + 3;
        Variant::Root Ipt((double) i32);
        if (!FP::defined(Ipt.read().toReal())) return 100 * (color + 1) + 4;
        remap("ZIp" + code + "Total", Ipt);

        if (fields.empty()) return 100 * (color + 1) + 5;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (field.size() != 8) return 100 * (color + 1) + 6;
        i32 = (qint32) field.parse_u32(&ok, 16);
        if (!ok) return 100 * (color + 1) + 7;
        Variant::Root Ift((double) i32);
        if (!FP::defined(Ift.read().toReal())) return 100 * (color + 1) + 8;
        remap("ZIf" + code + "Total", Ift);

        if (fields.empty()) return 100 * (color + 1) + 9;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (field.size() != 3) return 100 * (color + 1) + 10;
        u32 = field.parse_u32(&ok, 16);
        if (!ok) return 100 * (color + 1) + 11;
        Variant::Root n((double) u32);
        if (!FP::defined(n.read().toReal())) return 100 * (color + 1) + 12;
        remap("ZSamples" + code, n);

        if (fields.empty()) return 100 * (color + 1) + 13;
        field = fields.front().string_trimmed();
        fields.pop_front();
        /* Do something with the overflow? */
        /*if (field.parse_i32() != 0) {
        }*/

        Ip[color].write()
                 .setReal(calculateSumIntensity(Ipt.read().toDouble(), n.read().toDouble()));
        If[color].write()
                 .setReal(calculateSumIntensity(Ift.read().toDouble(), n.read().toDouble()));
    }

    remap("IpD", Ip[3]);
    remap("IfD", If[3]);
    for (int color = 0; color < 3; color++) {
#ifndef R_RECORD_DARK_SUBTRACTED
                                                                                                                                Ip[color].setDouble(calculateDarkSubtracted(Ip[color].toDouble(),
            Ip[3].toDouble()));
            If[color].setDouble(calculateDarkSubtracted(If[color].toDouble(),
            If[3].toDouble()));
#endif

        remap("Ip" + colorLookup[color], Ip[color]);
        remap("If" + colorLookup[color], If[color]);
    }

    if (fields.empty()) return 1;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root VQ(field.parse_real(&ok));
    if (!ok) return 2;
    if (!FP::defined(VQ.read().toReal())) return 3;
    VQ.write().setDouble(VQ.read().toDouble() / 1000.0);
    remap("VQ", VQ);

    if (fields.empty()) return 4;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Qraw(field.parse_real(&ok));
    if (!ok) return 5;
    if (!FP::defined(Qraw.read().toReal())) return 6;
    remap("QRAW", Qraw);
    Variant::Root Q(config.first().flowCalibration.apply(Qraw.read().toDouble()));
    remap("Q", Q);

    if (fields.empty()) return 7;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root flags((qint64) field.parse_u16(&ok, 16));
    if (!ok) return 8;
    remap("FRAW", flags);

    if (config.first().strictMode) {
        if (!fields.empty())
            return 99;
    }

    if (!FP::defined(frameTime))
        return -1;
    if (FP::defined(lastRecordTime) && frameTime < lastRecordTime)
        return -1;
    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;

    resetAccumulatorStream();

    data.Q = std::move(Q);
    data.Qraw = std::move(Qraw);
    data.VQ = std::move(VQ);
    data.flags = std::move(flags);
    data.IfD = std::move(If[3]);
    data.IpD = std::move(Ip[3]);
    data.psapTime = psapTime;
    for (int color = 0; color < 3; color++) {
        data.In[color].write()
                      .setReal(calculateIn(If[color].read().toDouble(),
                                           Ip[color].read().toDouble()));
        remap("In" + colorLookup[color], data.In[color]);
        data.Ip[color] = std::move(Ip[color]);
        data.If[color] = std::move(If[color]);

        data.Irc[color].write().setEmpty();
        data.Bac[color].write().setEmpty();

        filterNormalizeIp[color]->add(data.Ip[color].read().toDouble(), startTime, endTime);
        filterNormalizeIf[color]->add(data.If[color].read().toDouble(), startTime, endTime);
        filterNormalizeIn[color]->add(data.In[color].read().toDouble(), startTime, endTime);
        filterChangeIf[color]->add(data.If[color].read().toDouble(), startTime, endTime);

        filterChangeIrc[color]->reset();
    }

    if (normalizationAcceptRemaining > 0)
        normalizationAcceptRemaining = 1;

    outputMetadata(startTime);
    applyStateControl(startTime, endTime);

    outputIntensityData(startTime, endTime);

    /* Allow quicker autoprobing when we don't need a full minute for
     * metadata reasons */
    autoprobeValidRecords += 10;
    return 0;
}

int AcquireRRPSAP3W::processRecordA(std::deque<Util::ByteView> fields, double &frameTime)
{
    qint64 psapTime = INTEGER::undefined();
    {
        int code = consumeTime(fields, frameTime, psapTime);
        if (code > 0)
            return code + 1000;
    }

    Util::ByteView field;
    bool ok = false;

    Variant::Root Ba[3];
    for (int color = 0; color < 3; color++) {
        if (fields.empty()) return 100 * (color + 1) + 1;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (field == "-999.9") {
            Ba[color].write().setDouble(FP::undefined());
        } else {
            Ba[color].write().setReal(field.parse_real(&ok));
            if (!ok) return 100 * (color + 1) + 2;
            if (!FP::defined(Ba[color].read().toReal())) return 100 * (color + 1) + 3;
        }
        remap("ZBa" + colorLookup[color] + "Raw", Ba[color]);
    }

    Variant::Root Ir[3];
    for (int color = 0; color < 3; color++) {
        if (fields.empty()) return 100 * (color + 1) + 1;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Ir[color].write().setReal(field.parse_real(&ok));
        if (!ok) return 100 * (color + 1) + 2;
        if (!FP::defined(Ir[color].read().toReal())) return 100 * (color + 1) + 3;
        remap("Ir" + colorLookup[color] + "Raw", Ir[color]);
    }

    if (fields.empty()) return 1;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Qraw(field.parse_real(&ok));
    if (!ok) return 2;
    if (!FP::defined(Qraw.read().toReal())) return 3;
    remap("QRAW", Qraw);
    Variant::Root Q(config.first().flowCalibration.apply(Qraw.read().toDouble()));
    remap("Q", Q);

    if (fields.empty()) return 4;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root VQ(field.parse_real(&ok));
    if (!ok) return 5;
    if (!FP::defined(VQ.read().toReal())) return 6;
    VQ.write().setDouble(VQ.read().toDouble() / 1000.0);
    remap("VQ", VQ);

    /* Ignore averaging interval */
    if (fields.empty()) return 7;
    fields.pop_front();

    if (fields.empty()) return 8;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root flags((qint64) field.parse_u16(&ok, 16));
    if (!ok) return 9;
    remap("FRAW", flags);

    if (config.first().strictMode) {
        if (!fields.empty())
            return 99;
    }

    if (!FP::defined(frameTime))
        return -1;
    if (FP::defined(lastRecordTime) && frameTime < lastRecordTime)
        return -1;
    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;

    data.Q = Q;
    data.Qraw = Qraw;
    data.VQ = VQ;
    data.flags = flags;
    data.IfD.write().setEmpty();
    data.IpD.write().setEmpty();
    data.psapTime = psapTime;
    for (int color = 0; color < 3; color++) {
        data.Ir[color] = Ir[color];
        data.Irc[color] = std::move(Ir[color]);
        data.Ba[color] = Ba[color];
        data.Bac[color] = std::move(Ba[color]);

        data.Ip[color].write().setEmpty();
        data.If[color].write().setEmpty();
        data.In[color].write().setEmpty();

        filterChangeIrc[color]->add(data.Irc[color].read().toDouble(), startTime, endTime);

        filterNormalizeIp[color]->reset();
        filterNormalizeIf[color]->reset();
        filterNormalizeIn[color]->reset();
        filterChangeIf[color]->reset();
    }

    if (normalizationAcceptRemaining > 0)
        normalizationAcceptRemaining = 1;

    outputMetadata(startTime, false);
    applyStateControl(startTime, endTime);

    outputReportedData(startTime, endTime);

    /* Allow quicker autoprobing when we don't need a full minute for
     * metadata reasons */
    autoprobeValidRecords += 10;
    return 0;
}

void AcquireRRPSAP3W::differenceAccumulator(AccumulatorMeasurement &accumulator,
                                            double &difference,
                                            double &dT,
                                            qint64 current,
                                            double endTime,
                                            qint64 psapTime,
                                            qint64 wrap)
{
    if (!INTEGER::defined(accumulator.sum) || !INTEGER::defined(current)) {
        accumulator.sum = current;
        accumulator.endTime = endTime;
        accumulator.psapTime = psapTime;

        difference = FP::undefined();
        dT = FP::undefined();
        return;
    }

    if (current < accumulator.sum) {
        difference = (double) (wrap - accumulator.sum + current + 1);
    } else {
        difference = (double) (current - accumulator.sum);
    }

    dT = FP::undefined();
    if (INTEGER::defined(psapTime) &&
            INTEGER::defined(accumulator.psapTime) &&
            psapTime > accumulator.psapTime) {
        dT = (double) (psapTime - accumulator.psapTime);
    }
    if (config.first().useMeasuredTime &&
            FP::defined(endTime) &&
            FP::defined(accumulator.endTime) &&
            endTime > accumulator.endTime) {
        dT = endTime - accumulator.endTime;
    }

    accumulator.sum = current;
    accumulator.endTime = endTime;
    accumulator.psapTime = psapTime;
}

int AcquireRRPSAP3W::handleAccumulatorExtra(int timeIndex,
                                            std::deque<Util::ByteView> &fields,
                                            double startTime,
                                            double endTime,
                                            qint64 psapTime)
{
    Q_UNUSED(psapTime);
    Q_UNUSED(startTime);
    Q_UNUSED(endTime);

    Util::ByteView field;
    bool ok = false;

    switch (timeIndex) {
    case 0: {
        /* Current time */
        if (fields.empty()) return 1;
        qint64 time = INTEGER::undefined();
        int code = parseTime(fields.front().string_trimmed(), time);
        fields.pop_front();
        if (code > 0) {
            return code + 100;
        } else if (code < 0) {
            return code;
        }
        if (!INTEGER::defined(data.psapTime))
            data.psapTime = time;

        /* Equation in use, ignored */
        if (fields.empty()) return 2;
        fields.pop_front();
        break;
    }

    case 1:
        if (fields.empty()) return 3;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (!field.string_start("K0=") && !field.string_start("k0=")) return 4;
        reportedWeissA = field.mid(3).string_trimmed().parse_real(&ok);
        if (!ok) return 5;
        if (!FP::defined(reportedWeissA)) return 6;

        if (fields.empty()) return 7;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (!field.string_start("K1=") && !field.string_start("k1=")) return 8;
        reportedWeissB = field.mid(3).string_trimmed().parse_real(&ok);
        if (!ok) return 9;
        if (!FP::defined(reportedWeissB)) return 10;

        break;

    case 2:
        if (fields.empty()) return 11;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (!field.string_start("AREA=") && !field.string_start("area=")) return 12;
        reportedArea = field.mid(5).string_trimmed().parse_real(&ok);
        if (!ok) return 13;
        if (!FP::defined(reportedArea)) return 14;
        break;

    case 4: {
        if (fields.empty()) return 15;
        qint64 time = INTEGER::undefined();
        field = fields.front().string_trimmed();
        fields.pop_front();
        int code = parseTime(field, time, "CHANGE");
        if (code != 0) {
            /* If it's not a valid time, but is a valid hex number, just ignore
             * it, since that's what happens when there is no transmittance reset done
             * yet */
            if (field.size() > 8 && field.parse_u64(&ok, 16) > 0x10000 && ok) {
                break;
            }
        }
        if (code > 0) {
            return code + 200;
        } else if (code < 0) {
            return code;
        }

        reportedResetTime = time;
        if (!INTEGER::defined(filterChangeStartPSAPTime))
            filterChangeStartPSAPTime = time;
        if (!INTEGER::defined(filterChangeEndPSAPTime))
            filterChangeEndPSAPTime = time;
        break;
    }

    case 5:
    case 6:
    case 7: {
        if (fields.empty()) return 16;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root InRaw;
        if (field == "9.999" || field == "-9.999" || field == "0.000") {
            InRaw.write().setReal(FP::undefined());
        } else {
            InRaw.write().setReal(field.parse_real(&ok));
            if (!ok) return 17;
            if (!FP::defined(InRaw.read().toReal())) return 18;
        }

        int color = timeIndex - 5;

        remap("ZInRaw" + colorLookup[color], InRaw);
        reportedResetIn[color] = InRaw.read().toDouble();

        break;
    }

    case 16:
        /* Older firmwares do not have this */
        if (fields.empty()) break;
    case 8:
    case 12:
        /* Previous reset times, ignored */
        if (fields.empty()) return 19;
        fields.pop_front();
        break;

    case 17:
    case 18:
    case 19:
        /* Older firmwares do not have this */
        if (fields.empty()) break;
    case 9:
    case 10:
    case 11:
    case 13:
    case 14:
    case 15:
        /* Previous normalization, ignored */
        if (fields.empty()) return 20;
        fields.pop_front();
        break;

    case 21: {
        if (fields.empty()) return 21;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (!field.string_start("mf0p0=") && !field.string_start("MF0P0="))
            return 22;
        double oldCal = reportedFlowCalibration[0];
        reportedFlowCalibration[0] = field.mid(6).string_trimmed().parse_real(&ok);
        if (!ok) return 23;
        if (!FP::defined(reportedFlowCalibration[0])) return 24;

        if (!FP::equal(oldCal, reportedFlowCalibration[0])) {
            logMetaState = NotEmitted;
            realtimeMetaState = NotEmitted;
        }
        break;
    }

    case 22: {
        if (fields.empty()) return 25;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (!field.string_start("mf0p3=") && !field.string_start("MF0P3="))
            return 26;
        double oldCal = reportedFlowCalibration[1];
        reportedFlowCalibration[1] = field.mid(6).string_trimmed().parse_real(&ok);
        if (!ok) return 27;
        if (!FP::defined(reportedFlowCalibration[1])) return 28;

        if (!FP::equal(oldCal, reportedFlowCalibration[1])) {
            logMetaState = NotEmitted;
            realtimeMetaState = NotEmitted;
        }
        break;
    }

    case 23: {
        if (fields.empty()) return 29;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (!field.string_start("mf2p0=") && !field.string_start("MF2P0="))
            return 30;
        double oldCal = reportedFlowCalibration[2];
        reportedFlowCalibration[2] = field.mid(6).string_trimmed().parse_real(&ok);
        if (!ok) return 31;
        if (!FP::defined(reportedFlowCalibration[2])) return 32;

        if (!FP::equal(oldCal, reportedFlowCalibration[2])) {
            logMetaState = NotEmitted;
            realtimeMetaState = NotEmitted;
        }
        break;
    }

    case 56: {
        /* Older firmwares do not have this */
        if (fields.empty()) break;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (!field.string_start("ver=") && !field.string_start("VER=")) return 33;

        Variant::Root oldValue(instrumentMeta["FirmwareVersion"]);
        instrumentMeta["FirmwareVersion"].setString(
                QString::fromLatin1(field.mid(4).toQByteArrayRef()));

        if (oldValue.read() != instrumentMeta["FirmwareVersion"]) {
            logMetaState = NotEmitted;
            realtimeMetaState = NotEmitted;
            sourceMetadataUpdated();
        }

        break;
    }

    case 57:    /* Options */
    case 58:    /* Debug */
    case 59:    /* ROM checksum */
        /* Older firmwares do not have this */
        if (fields.empty()) break;
        fields.pop_front();
        break;

    default:
        break;
    }

    return 0;
}

static bool timeIndexElapsed(int begin, int end, int n)
{
    Q_ASSERT(n > 0 && n < 60);
    Q_ASSERT(begin >= 0 && begin <= 59);
    Q_ASSERT(end >= 0 && end <= 59);

    if (begin + n < 60) {
        if (end < begin)
            return true;
        return begin + n <= end;
    }
    if (end >= begin)
        return false;
    begin += n;
    begin %= 60;
    return end >= begin;
}

int AcquireRRPSAP3W::processRecordAccumulators(std::deque<Util::ByteView> &fields,
                                               double startTime,
                                               double endTime,
                                               qint64 psapTime)
{
    bool ok = false;

    if (fields.empty()) return 1;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.size() != 2) return 2;
    int timeIndex = field.parse_i32(&ok, 10);
    if (!ok) return 3;
    if (timeIndex < 0) return 4;
    if (timeIndex > 59) return 5;

    CurrentData nextData = data;

    int recordBaseType = timeIndex % 4;
    switch (recordBaseType) {
    case 0: {
        /* Boxcar index, ignored */
        if (fields.empty()) return 100;
        fields.pop_front();

        if (fields.empty()) return 102;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (field.size() != 8) return 103;
        qint64 sumFlow = (qint64) field.parse_u64(&ok, 16);
        if (!ok) return 104;

        if (fields.empty()) return 105;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (field.size() != 4) return 106;
        qint64 sumSeconds = (qint64) field.parse_u64(&ok, 16);
        if (!ok) return 107;

        Variant::Root IpD;
        Variant::Root IfD;
        if (timeIndex != 0) {
            if (fields.empty()) return 108;
            field = fields.front().string_trimmed();
            fields.pop_front();
            IpD.write().setReal(convertI24Hex(field));
            if (!FP::defined(IpD.read().toReal())) return 109;
            remap("IpD", IpD);

            if (fields.empty()) return 110;
            field = fields.front().string_trimmed();
            fields.pop_front();
            IfD.write().setReal(convertI24Hex(field));
            if (!FP::defined(IfD.read().toReal())) return 111;
            remap("IfD", IfD);

            /* ADC gain, ignored */
            if (fields.empty()) return 112;
            fields.pop_front();

            accumulatorDarkReference = IfD.read().toDouble();
            accumulatorDarkSample = IpD.read().toDouble();

            nextData.IpD = std::move(IpD);
            nextData.IfD = std::move(IfD);
        }

        double flowDifference;
        double flowDT;
        differenceAccumulator(accumulatorFlow, flowDifference, flowDT, sumFlow, endTime, psapTime,
                              Q_INT64_C(0xFFFFFFFF));

        double flowSecondsDifference;
        double flowSecondsDT;
        differenceAccumulator(accumulatorFlowSeconds, flowSecondsDifference, flowSecondsDT,
                              sumSeconds, endTime, psapTime, Q_INT64_C(0xFFFF));

        Variant::Root VQ;
        if (FP::defined(flowSecondsDifference) &&
                FP::defined(flowDifference) &&
                flowSecondsDifference > 0.0) {
            VQ.write().setDouble(flowDifference / flowSecondsDifference / 1000.0);
        } else {
            VQ.write().setDouble(FP::undefined());
        }
        remap("VQ", VQ);
        Variant::Root Qraw(calculateFlow(VQ.read().toDouble(), reportedFlowCalibration));
        remap("QRAW", Qraw);
        Variant::Root Q(config.first().flowCalibration.apply(Qraw.read().toDouble()));
        remap("Q", Q);

        nextData.Q = std::move(Q);
        nextData.VQ = std::move(VQ);
        if (!FP::defined(nextData.Q.read().toReal()))
            nextData.Q = data.Q;
        if (!FP::defined(nextData.VQ.read().toReal()))
            nextData.VQ = data.VQ;
        if (!FP::defined(nextData.Qraw.read().toReal()))
            nextData.Qraw = data.Qraw;

        for (int color = 0; color < 3; color++) {
            nextData.If[color].write().setEmpty();
            nextData.Ip[color].write().setEmpty();
            nextData.In[color].write().setEmpty();
            nextData.Ba[color].write().setEmpty();
        }

        break;
    }

    case 1:
    case 2:
    case 3: {
        if (fields.empty()) return 100 * (recordBaseType + 1) + 1;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (field.size() != 10) return 100 * (recordBaseType + 1) + 2;
        qint64 sumSample = (qint64) field.parse_u64(&ok, 16);
        if (!ok) return 100 * (recordBaseType + 1) + 3;

        if (fields.empty()) return 100 * (recordBaseType + 1) + 4;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (field.size() != 10) return 100 * (recordBaseType + 1) + 5;
        qint64 sumReference = (qint64) field.parse_u64(&ok, 16);
        if (!ok) return 100 * (recordBaseType + 1) + 6;

        if (fields.empty()) return 100 * (recordBaseType + 1) + 7;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (field.size() != 6) return 100 * (recordBaseType + 1) + 8;
        qint64 sumConversions = (qint64) field.parse_u64(&ok, 16);
        if (!ok) return 100 * (recordBaseType + 1) + 9;

        /* Overrange sum, ignored */
        if (fields.empty()) return 100 * (recordBaseType + 1) + 10;
        fields.pop_front();

        int color = recordBaseType - 1;

        double sampleDifference;
        double sampleDT;
        differenceAccumulator(accumulatorSample[color], sampleDifference, sampleDT, sumSample,
                              endTime, psapTime, Q_INT64_C(0xFFFFFFFFFF));

        double referenceDifference;
        double referenceDT;
        differenceAccumulator(accumulatorReference[color], referenceDifference, referenceDT,
                              sumReference, endTime, psapTime, Q_INT64_C(0xFFFFFFFFFF));

        double conversionsDifference;
        double conversionsDT;
        differenceAccumulator(accumulatorConversions[color], conversionsDifference, conversionsDT,
                              sumConversions, endTime, psapTime, Q_INT64_C(0xFFFFFF));

        Variant::Root Ipt
                (calculateConversionSubtraction(sampleDifference, sampleDT, conversionsDifference,
                                                conversionsDT));
        remap("ZIp" + colorLookup[color] + "Total", Ipt);

        Variant::Root Ift(calculateConversionSubtraction(referenceDifference, referenceDT,
                                                         conversionsDifference, conversionsDT));
        remap("ZIf" + colorLookup[color] + "Total", Ift);

#ifndef ACCUMULATOR_DARK_SUBTRACTED
        Variant::Root Ip(calculateDarkSubtracted(Ipt.toDouble(),
            accumulatorDarkSample));
        Variant::Root If(calculateDarkSubtracted(Ift.toDouble(),
            accumulatorDarkReference));
#else
        Variant::Root Ip(Ipt);
        Variant::Root If(Ift);
#endif
        remap("Ip" + colorLookup[color], Ip);
        remap("If" + colorLookup[color], If);

        Variant::Root In(calculateIn(If.read().toDouble(), Ip.read().toDouble()));
        remap("In" + colorLookup[color], In);

        nextData.Ip[color] = std::move(Ip);
        nextData.If[color] = std::move(If);
        nextData.In[color] = std::move(In);
        break;
    }

    default:
        Q_ASSERT(false);
        break;
    }

    int code = handleAccumulatorExtra(timeIndex, fields, startTime, endTime, psapTime);
    if (code > 0) {
        return code + 500;
    } else if (code < 0) {
        return code;
    }

    if (config.first().strictMode) {
        if (!fields.empty())
            return 99;
    }

    if (config.first().useReportedAbsorptions) {
        data = std::move(nextData);
        for (int color = 0; color < 3; color++) {
            data.Ba[color] = data.Bac[color];
            data.Ir[color] = data.Irc[color];
        }

        outputMetadata(startTime, false);
        applyStateControl(startTime, endTime);
        outputReportedData(startTime, endTime);
        return 0;
    }

    if (recordBaseType == 0 ||
            timeIndexElapsed(accumulatorBinStartIndex, timeIndex, 4) ||
            (INTEGER::defined(psapTime) &&
                    INTEGER::defined(accumulatorBinStartPSAPTime) &&
                    psapTime - accumulatorBinStartPSAPTime > 8)) {

        double recordStart = accumulatorBinStartTime;
        double recordEnd;
        if (recordBaseType == 0 && FP::defined(startTime) && !FP::equal(recordStart, startTime)) {
            recordEnd = startTime;
        } else {
            recordEnd = endTime;
        }
        accumulatorBinStartTime = recordEnd;

        /* Shift the output by 2 seconds because the intensities we
         * have represent the actual 4-second means of the instrument's
         * measurements (since we only get every 4'th boxcar for differences).
         * So this is the best we can do to adjust it to true time. */
        if (FP::defined(recordStart) && FP::defined(recordEnd)) {
            if (config.first().useMeasuredTime) {
                double delta = recordEnd - recordStart;
                delta /= 2.0;
                recordStart -= delta;
                recordEnd -= delta;
            } else {
                recordStart -= 2.0;
                recordEnd -= 2.0;
            }
        }

        for (int color = 0; color < 3; color++) {
            filterNormalizeIp[color]->add(data.Ip[color].read().toDouble(), recordStart, recordEnd);
            filterNormalizeIf[color]->add(data.If[color].read().toDouble(), recordStart, recordEnd);
            filterNormalizeIn[color]->add(data.In[color].read().toDouble(), recordStart, recordEnd);
            filterChangeIf[color]->add(data.If[color].read().toDouble(), recordStart, recordEnd);
        }

        outputMetadata(recordStart);
        applyStateControl(recordStart, recordEnd);
        outputIntensityData(recordStart, recordEnd, 4.0);

        accumulatorBinStartPSAPTime = psapTime;
        data = std::move(nextData);
        if (recordBaseType == 0) {
            accumulatorBinStartIndex = timeIndex;
        } else {
            int excludeColor = recordBaseType - 1;
            for (int color = 0; color < 3; color++) {
                if (color == excludeColor)
                    continue;

                data.If[color].write().setEmpty();
                data.Ip[color].write().setEmpty();
                data.In[color].write().setEmpty();
                data.Ba[color].write().setEmpty();
            }

            accumulatorBinStartIndex = timeIndex / 4;
            accumulatorBinStartIndex *= 4;
        }
    } else {
        data = std::move(nextData);
    }

    return 0;
}

int AcquireRRPSAP3W::processRecordO(std::deque<Util::ByteView> fields,
                                    double &frameTime,
                                    bool tryTime,
                                    qint64 psapTime)
{
    if (tryTime) {
        int code = consumeTime(fields, frameTime, psapTime);
        if (code > 0)
            return code + 1000;
    }
    if (!INTEGER::defined(psapTime) && FP::defined(frameTime))
        psapTime = (qint64) floor(frameTime);

    Util::ByteView field;
    bool ok = false;

    Variant::Root BaRaw[3];
    for (int color = 0; color < 3; color++) {
        if (fields.empty()) return 100 * (color + 1) + 1;
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (field == "-999.9") {
            BaRaw[color].write().setDouble(FP::undefined());
        } else {
            BaRaw[color].write().setReal(field.parse_real(&ok));
            if (!ok) return 100 * (color + 1) + 2;
            if (!FP::defined(BaRaw[color].read().toReal())) return 100 * (color + 1) + 3;
        }
        remap("ZBa" + colorLookup[color] + "Raw", BaRaw[color]);
    }

    Variant::Root IrRaw[3];
    for (int color = 0; color < 3; color++) {
        if (fields.empty()) return 100 * (color + 1) + 1;
        field = fields.front().string_trimmed();
        fields.pop_front();
        IrRaw[color].write().setReal(field.parse_real(&ok));
        if (!ok) return 100 * (color + 1) + 2;
        if (!FP::defined(IrRaw[color].read().toReal())) return 100 * (color + 1) + 3;
        remap("Ir" + colorLookup[color] + "Raw", IrRaw[color]);
    }

    if (fields.empty()) return 1;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Qraw(field.parse_real(&ok));
    if (!ok) return 2;
    if (!FP::defined(Qraw.read().toReal())) return 3;
    remap("QRAW", Qraw);
    Variant::Root Q(config.first().flowCalibration.apply(Qraw.read().toDouble()));
    remap("Q", Q);

    if (fields.empty()) return 4;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root VQ(field.parse_real(&ok));
    if (!ok) return 5;
    if (!FP::defined(VQ.read().toReal())) return 6;
    VQ.write().setDouble(VQ.read().toDouble() / 1000.0);
    remap("VQ", VQ);

    /* Ignore averaging interval */
    if (fields.empty()) return 7;
    fields.pop_front();

    if (fields.empty()) return 8;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root flags((qint64) field.parse_u16(&ok, 16));
    if (!ok) return 9;
    remap("FRAW", flags);

    if (!FP::defined(frameTime))
        return -1;
    if (FP::defined(lastRecordTime) && frameTime < lastRecordTime)
        return -1;
    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;

    data.Qraw = Qraw;
    if (!FP::defined(data.Q.read().toReal()))
        data.Q = Q;
    if (!FP::defined(data.VQ.read().toReal()))
        data.VQ = VQ;
    data.flags = flags;
    data.psapTime = psapTime;
    for (int color = 0; color < 3; color++) {
        data.Irc[color] = IrRaw[color];
        data.Bac[color] = BaRaw[color];

        if (!data.Ir[color].read().exists())
            data.Ir[color].write().setReal(FP::undefined());
        if (!data.Ip[color].read().exists())
            data.Ip[color].write().setReal(FP::undefined());
        if (!data.If[color].read().exists())
            data.If[color].write().setReal(FP::undefined());
        if (!data.In[color].read().exists())
            data.In[color].write().setReal(FP::undefined());

        filterChangeIrc[color]->add(data.Irc[color].read().toDouble(), startTime, endTime);
    }

    if (fields.size() == 1) {
        auto subfields = Util::as_deque(fields.front().string_trimmed().split(','));
        fields.pop_front();
        int code = processRecordAccumulators(subfields, startTime, endTime, psapTime);
        if (code > 0)
            return code + 2000;
        else if (code < 0)
            return -1;

        if (config.first().strictMode) {
            if (!subfields.empty())
                return 97;
        }
    } else if (fields.size() >= 4) {
        int code = processRecordAccumulators(fields, startTime, endTime, psapTime);
        if (code > 0)
            return code + 3000;
        else if (code < 0)
            return -1;
    } else {
        return 98;
    }

    autoprobeValidRecords++;
    return 0;
}

int AcquireRRPSAP3W::processRecordI(std::deque<Util::ByteView> fields, double &frameTime)
{
    qint64 psapTime = INTEGER::undefined();
    {
        int code = consumeTime(fields, frameTime, psapTime);
        if (code > 0)
            return code + 1000;
    }
    if (!INTEGER::defined(psapTime) && FP::defined(frameTime))
        psapTime = (qint64) floor(frameTime);

    if (!FP::defined(frameTime))
        return -1;
    if (FP::defined(lastRecordTime) && frameTime < lastRecordTime)
        return -1;
    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;

    data.flags.write().setEmpty();
    data.psapTime = psapTime;
    for (int color = 0; color < 3; color++) {
        data.Irc[color].write().setEmpty();
        data.Bac[color].write().setEmpty();

        if (!data.Ir[color].read().exists())
            data.Ir[color].write().setDouble(FP::undefined());
        if (!data.Ip[color].read().exists())
            data.Ip[color].write().setDouble(FP::undefined());
        if (!data.If[color].read().exists())
            data.If[color].write().setDouble(FP::undefined());
        if (!data.In[color].read().exists())
            data.In[color].write().setDouble(FP::undefined());

        filterChangeIrc[color]->reset();
    }

    if (fields.size() == 1) {
        auto subfields = Util::as_deque(fields.front().string_trimmed().split(','));
        fields.pop_front();
        int code = processRecordAccumulators(subfields, startTime, endTime, psapTime);
        if (code > 0)
            return code + 2000;
        else if (code < 0)
            return -1;

        if (config.first().strictMode) {
            if (!subfields.empty())
                return 98;
        }
    } else if (fields.size() >= 4) {
        int code = processRecordAccumulators(fields, startTime, endTime, psapTime);
        if (code > 0)
            return code + 3000;
        else if (code < 0)
            return -1;
    } else {
        return 1;
    }

    autoprobeValidRecords++;
    return 0;
}

static void removeLeadingDelimiters(Util::ByteView &line)
{
    while (!line.empty()) {
        switch (line.front()) {
        case ' ':
        case '\t':
        case ',':
        case '\n':
        case '\r':
            break;
        default:
            return;
        }
        line = line.mid(1);
    }
}

static void removeUntilDigitOrQuote(Util::ByteView &line)
{
    while (!line.empty()) {
        switch (line.front()) {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case '-':
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
        case 'A':
        case 'B':
        case 'C':
        case 'D':
        case 'E':
        case 'F':
        case '\"':
        case '\'':
            return;
        default:
            break;
        }
        line = line.mid(1);
    }
}

static int negativeOrErrorAdd(int base, int add)
{
    if (base < 0)
        return base;
    if (base > 0)
        return base + add;
    return 0;
}

int AcquireRRPSAP3W::processRecord(Util::ByteView line, double &frameTime)
{
    Q_ASSERT(!config.isEmpty());

    removeLeadingDelimiters(line);
    if (line.empty())
        return -1;
    if (line.size() < 3)
        return 100;

    switch (line.front()) {
    case 'R':
    case 'r':
        line = line.mid(1);
        removeLeadingDelimiters(line);
        return negativeOrErrorAdd(processRecordR(splitLine(line), frameTime), 10000);
    case 'F':
    case 'f':
    case 'S':
    case 's':
        line = line.mid(1);
        removeLeadingDelimiters(line);
        return negativeOrErrorAdd(processRecordA(splitLine(line), frameTime), 20000);
    case 'O':
    case 'o':
        line = line.mid(1);
        removeLeadingDelimiters(line);
        return negativeOrErrorAdd(processRecordO(splitLine(line), frameTime), 30000);
    case 'I':
    case 'i':
        line = line.mid(1);
        removeLeadingDelimiters(line);
        return negativeOrErrorAdd(processRecordI(splitLine(line), frameTime), 40000);
    }

    removeUntilDigitOrQuote(line);
    if (line.size() < 3)
        return 101;

    /* No record code, so try and resolve it base on the contents */

    /* Conclusive if it starts with one of these */
    switch (line.front()) {
    case 'a':
    case 'b':
    case 'c':
    case 'd':
    case 'e':
    case 'f':
    case 'A':
    case 'B':
    case 'C':
    case 'D':
    case 'E':
    case 'F':
        return negativeOrErrorAdd(processRecordR(splitLine(line), frameTime), 50000);
    case '\"':
    case '\'':
        return negativeOrErrorAdd(processRecordI(splitLine(line), frameTime), 60000);
    default:
        break;
    }

    std::deque<Util::ByteView> fields(splitLine(line));
    if (fields.empty())
        return 102;

    switch (fields.size()) {
    case 20:
    case 19:
        return negativeOrErrorAdd(processRecordR(fields, frameTime), 70000);
    case 10:
        return negativeOrErrorAdd(processRecordA(fields, frameTime), 80000);
    case 1:
    case 2:
        return negativeOrErrorAdd(processRecordI(fields, frameTime), 90000);
    case 12:
        return negativeOrErrorAdd(processRecordO(fields, frameTime), 100000);

    case 11: {
        /* Could be either an F/S record or an O record without a time */
        qint64 psapTime = INTEGER::undefined();
        int code = consumeTime(fields, frameTime, psapTime);
        if (code == 0) {
            return negativeOrErrorAdd(processRecordA(fields, frameTime), 110000);
        } else if (code > 0) {
            return code + 120000;
        }
        return negativeOrErrorAdd(processRecordO(fields, frameTime, false, psapTime), 130000);
    }
    }

    /* If all else fails, try it as an O record (may pass due to non-strict
     * parsing) */
    return negativeOrErrorAdd(processRecordO(fields, frameTime), 140000);
}

void AcquireRRPSAP3W::applyStateControl(double startTime, double endTime)
{
    /* Check sample state change */
    SampleState priorSampleState = sampleState;
    SampleState initialSampleState = sampleState;

    data.baseFlags.write().setType(Variant::Type::Flags);
    data.baseFlags.write().clear();
    data.baseFlags.write().applyFlag("STP"); /* Flow meter is already at STP */

    if (normalizationAcceptRemaining > 1)
        --normalizationAcceptRemaining;
    do {
        priorSampleState = sampleState;

        switch (sampleState) {
        case SAMPLE_RUN:
            if (normalizationAcceptRemaining == 1) {
                normalizationAcceptRemaining = 0;
                if (isSameNormalization(endTime)) {
                    qCDebug(log) << "Accepted pending normalization at" << Logging::time(endTime);

                    advanceNormalization(startTime, endTime);
                    break;
                } else if (canRecoverNormalization(endTime)) {
                    qCDebug(log) << "Rebuilding normalization from the reported transmittance at"
                                 << Logging::time(endTime);

                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("Run");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);
                    event(endTime,
                          QObject::tr("Recovering normalization from reported transmittance."),
                          false, info);

                    recoverNormalization(endTime);
                    break;
                } else {
                    qCDebug(log) << "Rejected pending normalization at" << Logging::time(endTime);

                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("Run");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);
                    event(endTime,
                          QObject::tr("Rejected saved normalization; filter change required."),
                          false, info);

                    invalidateNormalization();
                    resetSmoothers();

                    sampleState = SAMPLE_REQUIRE_FILTER_CHANGE;
                    break;
                }
            } else if (normalizationAcceptRemaining > 0) {
                break;
            }

            if (autodetectStart()) {
                qCDebug(log) << "Auto-detected filter change start at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("Run");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticFilterNormalization(info);
                insertDiagnosticNormalization(info);
                event(endTime, QObject::tr("Auto-detected filter change started."), true, info);
                sampleState = SAMPLE_FILTER_CHANGING;
                forceFilterBreak = true;

                emitFilterEnd(endTime);
                normalizationPSAPTime = INTEGER::undefined();
                filterChangeStartPSAPTime = INTEGER::undefined();
                normalizationLatest.startTime = FP::undefined();
                resetSmoothers();
                break;
            }

            normalizationLatest.startTime = endTime;
            for (int color = 0; color < 3; color++) {
                normalizationLatest.Ip[color] = data.Ip[color].read().toDouble();
                normalizationLatest.If[color] = data.If[color].read().toDouble();
                normalizationLatest.In[color] = data.In[color].read().toDouble();
            }

            break;

        case SAMPLE_FILTER_NORMALIZE:
            data.baseFlags.write().applyFlag("Normalizing");

            if (normalizationAcceptRemaining == 1) {
                normalizationAcceptRemaining = 0;
                if (isSameNormalization(endTime)) {
                    qCDebug(log) << "Accepted pending normalization at" << Logging::time(endTime);

                    sampleState = SAMPLE_RUN;
                    advanceNormalization(startTime, endTime);
                    break;
                } else if (canRecoverNormalization(endTime)) {
                    qCDebug(log) << "Rebuilding normalization from the reported transmittance at"
                                 << Logging::time(endTime);

                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("Normalize");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);
                    event(endTime,
                          QObject::tr("Recovering normalization from reported transmittance."),
                          false, info);

                    sampleState = SAMPLE_RUN;
                    recoverNormalization(endTime);
                    break;
                } else {
                    qCDebug(log) << "Rejected pending normalization at" << Logging::time(endTime);

                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("Normalize");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);
                    event(endTime,
                          QObject::tr("Rejected saved normalization; filter change required."),
                          false, info);

                    invalidateNormalization();
                    sampleState = SAMPLE_REQUIRE_FILTER_CHANGE;
                    forceFilterBreak = true;
                    break;
                }
            } else if (normalizationAcceptRemaining > 0) {
                break;
            }

            if (autodetectStart()) {
                qCDebug(log) << "Auto-detected filter change start at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("Normalize");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticNormalization(info);
                event(endTime, QObject::tr("Auto-detected filter change started."), true, info);
                sampleState = SAMPLE_FILTER_CHANGING;
                forceFilterBreak = true;

                emitFilterEnd(endTime);
                normalizationPSAPTime = INTEGER::undefined();
                filterChangeStartPSAPTime = INTEGER::undefined();
                normalizationLatest.startTime = FP::undefined();
                resetSmoothers();
                break;
            }

            if (!normalizationDone())
                break;
            emitFilterEnd(endTime);
            emitNormalizationEnd(endTime);
            normalizationStartTime = endTime;

            normalizationLatest.startTime = endTime;
            filterStart.startTime = endTime;

            for (int color = 0; color < 3; color++) {
                filterNormalization[color] = filterNormalizeIn[color]->value();

                normalizationLatest.Ip[color] = data.Ip[color].read().toDouble();
                normalizationLatest.If[color] = data.If[color].read().toDouble();
                normalizationLatest.In[color] = data.In[color].read().toDouble();

                filterStart.Ip[color] = filterNormalizeIp[color]->value();
                filterStart.If[color] = filterNormalizeIf[color]->value();
                filterStart.In[color] = filterNormalizeIn[color]->value();
            }

            qCDebug(log) << "Normalization established at" << Logging::time(endTime);

            filterIsNotWhite = !isWhiteFilter();
            if (filterIsNotWhite) {
                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterNormalization(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                info.hash("State").setString("Normalize");
                event(endTime, QObject::tr(
                        "Filter normalization established on what does not appear to be a white filter."),
                      true, info);
            } else {
                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterNormalization(info);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                info.hash("State").setString("Normalize");
                event(endTime, QObject::tr("Filter normalization established."), false, info);
            }

            {
                bool anyNotOne = false;
                for (int color = 0; color < 3; color++) {
                    double v = data.Irc[color].read().toDouble();
                    if (!FP::defined(v))
                        continue;
                    if (fabs(v - 1.0) > 0.1) {
                        anyNotOne = true;
                        break;
                    }
                }
                if (anyNotOne) {
                    Variant::Write info = Variant::Write::empty();
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterNormalization(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    info.hash("State").setString("Normalize");
                    event(endTime, QObject::tr(
                            "Filter beginning with an instrument transmittance not near one; please make sure the reset transmittance switch was pressed"),
                          true, info);
                }
            }

            normalizationAcceptRemaining = 0;
            forceRealtimeStateEmit = true;
            sampleState = SAMPLE_RUN;
            resetSmoothers();
            filterChangeStartPSAPTime = INTEGER::undefined();
            filterChangeEndPSAPTime = INTEGER::undefined();

            if (state != NULL)
                state->requestStateSave();

            persistentValuesUpdated();
            break;

        case SAMPLE_WHITE_FILTER_NORMALIZE:
            data.baseFlags.write().applyFlag("Normalizing");
            data.baseFlags.write().applyFlag("WhiteFilterNormalizing");

            if (normalizationAcceptRemaining == 1) {
                normalizationAcceptRemaining = 0;
                if (isSameNormalization(endTime)) {
                    qCDebug(log) << "Accepted pending normalization at" << Logging::time(endTime);

                    sampleState = SAMPLE_RUN;
                    forceFilterBreak = true;
                    advanceNormalization(startTime, endTime);
                    break;
                } else if (canRecoverNormalization(endTime)) {
                    qCDebug(log) << "Rebuilding normalization from the reported transmittance at"
                                 << Logging::time(endTime);

                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("WhiteFilterNormalize");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);
                    event(endTime,
                          QObject::tr("Recovering normalization from reported transmittance."),
                          false, info);

                    sampleState = SAMPLE_RUN;
                    recoverNormalization(endTime);
                    break;
                } else {
                    qCDebug(log) << "Rejected pending normalization at" << Logging::time(endTime);

                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("WhiteFilterNormalize");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);
                    event(endTime,
                          QObject::tr("Rejected saved normalization; filter change required."),
                          false, info);

                    forceRealtimeStateEmit = true;
                    sampleState = SAMPLE_REQUIRE_FILTER_CHANGE;
                    invalidateNormalization();
                    break;
                }
            } else if (normalizationAcceptRemaining > 0) {
                break;
            }

            if (autodetectStart()) {
                qCDebug(log) << "Auto-detected filter change start at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("WhiteFilterNormalize");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticNormalization(info);
                event(endTime, QObject::tr("Auto-detected filter change started."), true, info);
                sampleState = SAMPLE_FILTER_CHANGING;
                forceFilterBreak = true;

                emitFilterEnd(endTime);
                normalizationStartTime = FP::undefined();
                normalizationPSAPTime = INTEGER::undefined();
                filterChangeStartPSAPTime = INTEGER::undefined();
                normalizationLatest.startTime = FP::undefined();
                forceFilterBreak = true;
                resetSmoothers();
                break;
            }

            if (!normalizationDone())
                break;
            emitWhiteEnd(endTime);
            emitFilterEnd(endTime);
            emitNormalizationEnd(endTime);
            normalizationStartTime = endTime;

            normalizationLatest.startTime = endTime;
            filterStart.startTime = endTime;

            for (int color = 0; color < 3; color++) {
                filterNormalization[color] = filterNormalizeIn[color]->value();

                normalizationLatest.Ip[color] = data.Ip[color].read().toDouble();
                normalizationLatest.If[color] = data.If[color].read().toDouble();
                normalizationLatest.In[color] = data.In[color].read().toDouble();

                filterStart.Ip[color] = filterNormalizeIp[color]->value();
                filterStart.If[color] = filterNormalizeIf[color]->value();
                filterStart.In[color] = filterNormalizeIn[color]->value();
            }
            filterWhite = filterStart;

            qCDebug(log) << "White filter normalization established at" << Logging::time(endTime);

            filterIsNotWhite = false;
            {
                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterNormalization(info);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                info.hash("State").setString("WhiteFilterNormalize");
                event(endTime, QObject::tr("White filter normalization established."), false, info);
            }

            {
                bool anyNotOne = false;
                for (int color = 0; color < 3; color++) {
                    double v = data.Irc[color].read().toDouble();
                    if (!FP::defined(v))
                        continue;
                    if (fabs(v - 1.0) > 0.1) {
                        anyNotOne = true;
                        break;
                    }
                }
                if (anyNotOne) {
                    Variant::Write info = Variant::Write::empty();
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterNormalization(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    info.hash("State").setString("WhiteFilterNormalize");
                    event(endTime, QObject::tr(
                            "White filter beginning with an instrument transmittance not near one; please make sure the reset transmittance switch was pressed"),
                          true, info);
                }
            }

            normalizationAcceptRemaining = 0;
            forceRealtimeStateEmit = true;
            sampleState = SAMPLE_RUN;
            resetSmoothers();
            filterChangeStartPSAPTime = INTEGER::undefined();
            filterChangeEndPSAPTime = INTEGER::undefined();

            if (state != NULL)
                state->requestStateSave();

            persistentValuesUpdated();
            break;

        case SAMPLE_FILTER_CHANGING:
            data.baseFlags.write().applyFlag("FilterChanging");
            emitFilterEnd(endTime);

            if (autodetectEnd()) {
                qCDebug(log) << "Auto-detected filter change end at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("FilterChanging");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticFilterNormalization(info);
                event(endTime, QObject::tr("Auto-detected filter change end."), true, info);

                filterChangeStartPSAPTime = INTEGER::undefined();
                filterChangeEndPSAPTime = INTEGER::undefined();
                sampleState = SAMPLE_FILTER_NORMALIZE;
                break;
            }
            break;

        case SAMPLE_WHITE_FILTER_CHANGING:
            data.baseFlags.write().applyFlag("FilterChanging");
            data.baseFlags.write().applyFlag("WhiteFilterChanging");
            emitFilterEnd(endTime);

            if (!FP::defined(whiteFilterChangeStartTime))
                whiteFilterChangeStartTime = endTime;
            if (whiteFilterTimeout(endTime)) {
                qCDebug(log) << "White filter change timeout at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("WhiteFilterChanging");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterNormalization(info);
                event(endTime, QObject::tr("White filter change ending due to timeout."), true,
                      info);

                filterChangeStartPSAPTime = INTEGER::undefined();
                filterChangeEndPSAPTime = INTEGER::undefined();
                sampleState = SAMPLE_WHITE_FILTER_NORMALIZE;
            }

            break;

        case SAMPLE_REQUIRE_FILTER_CHANGE:
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
            emitFilterEnd(endTime);

            if (autodetectStart()) {
                qCDebug(log) << "Auto-detected filter change start at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                if (sampleState == SAMPLE_REQUIRE_FILTER_CHANGE)
                    info.hash("State").setString("RequireFilterChange");
                else
                    info.hash("State").setString("RequireWhiteFilterChange");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticFilterNormalization(info);
                event(endTime, QObject::tr("Auto-detected filter change started."), true, info);

                sampleState = SAMPLE_FILTER_CHANGING;
                forceFilterBreak = true;
                resetSmoothers();

                normalizationStartTime = FP::undefined();
                normalizationPSAPTime = INTEGER::undefined();
                filterChangeStartPSAPTime = INTEGER::undefined();
                normalizationLatest.startTime = FP::undefined();
                break;
            }

            break;
        }
    } while (priorSampleState != sampleState);

    if (initialSampleState != sampleState)
        forceRealtimeStateEmit = true;
}


void AcquireRRPSAP3W::configurationChanged()
{
    logMetaState = NotEmitted;
    realtimeMetaState = NotEmitted;
}

void AcquireRRPSAP3W::configAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

void AcquireRRPSAP3W::invalidateLogging(double frameTime)
{
    lastRecordTime = FP::undefined();
    resetAccumulatorStream();
    loggingLost(frameTime);
}


void AcquireRRPSAP3W::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (FP::defined(frameTime))
        configAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (autoprobeValidRecords > 60) {
                qCDebug(log) << "Autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                timeoutAt(frameTime + config.first().reportInterval * 2.0 + 1.0);

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("AutoprobeWait");
                event(frameTime, QObject::tr("Autoprobe succeeded."), false, info);

                forceRealtimeStateEmit = true;
            } else {
                timeoutAt(frameTime + config.first().reportInterval * 2.0 + 1.0);
            }
        } else if (code > 0) {
            qCDebug(log) << "Autoprobe failed at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            responseState = RESP_WAIT;
            autoprobeValidRecords = 0;
            autoprobeTotalFrames = 0;

            if (realtimeEgress != NULL && FP::defined(frameTime)) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            invalidateLogging(frameTime);
        } else {
            autoprobeValidRecords = 0;

            invalidateLogging(frameTime);
        }
        if (++autoprobeTotalFrames > 120) {
            qCDebug(log) << "Autoprobe failed at" << Logging::time(frameTime)
                         << "with too many rejected frames";

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            responseState = RESP_WAIT;
            autoprobeValidRecords = 0;
            autoprobeTotalFrames = 0;

            if (realtimeEgress != NULL && FP::defined(frameTime)) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            invalidateLogging(frameTime);
        }
        break;
    }

    case RESP_WAIT:
    case RESP_INITIALIZE:
        if (processRecord(frame, frameTime) == 0) {
            qCDebug(log) << "Comms established at" << Logging::time(frameTime);

            responseState = RESP_RUN;
            generalStatusUpdated();

            timeoutAt(frameTime + config.first().reportInterval * 2.0 + 1.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("Wait");
            event(frameTime, QObject::tr("Communications established."), false, info);

            forceRealtimeStateEmit = true;
        } else {
            autoprobeValidRecords = 0;
            autoprobeTotalFrames = 0;
            invalidateLogging(frameTime);
        }
        break;

    case RESP_RUN: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + config.first().reportInterval * 2.0 + 1.0);
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            responseState = RESP_WAIT;
            discardData(frameTime + config.first().reportInterval, 1);
            generalStatusUpdated();
            autoprobeValidRecords = 0;
            autoprobeTotalFrames = 0;

            Variant::Write info = Variant::Write::empty();
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            info.hash("ResponseState").setString("Run");
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            if (realtimeEgress != NULL && FP::defined(frameTime)) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            invalidateLogging(frameTime);
        }
        break;
    }

    default:
        break;
    }
}

void AcquireRRPSAP3W::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    switch (responseState) {
    case RESP_RUN: {
        qCDebug(log) << "Timeout in unpolled mode at" << Logging::time(frameTime);

        responseState = RESP_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        discardData(frameTime + config.first().reportInterval + 1.0, 1);
        generalStatusUpdated();
        autoprobeValidRecords = 0;
        autoprobeTotalFrames = 0;

        Variant::Write info = Variant::Write::empty();
        info.hash("ResponseState").setString("Run");
        event(frameTime, QObject::tr("Timeout waiting for report.  Communications dropped."), true,
              info);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        invalidateLogging(frameTime);
        break;
    }

    case RESP_AUTOPROBE_WAIT:
        qCDebug(log) << "Timeout waiting for autoprobe records at" << Logging::time(frameTime);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (controlStream != NULL)
            controlStream->resetControl();
        discardData(frameTime + config.first().reportInterval + 1.0, 1);
        responseState = RESP_WAIT;
        autoprobeValidRecords = 0;
        autoprobeTotalFrames = 0;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        invalidateLogging(frameTime);
        break;

    case RESP_INITIALIZE:
        discardData(frameTime + config.first().reportInterval, 1);
        timeoutAt(frameTime + config.first().reportInterval * 4.0 + 1.0);
        responseState = RESP_WAIT;
        break;

    case RESP_AUTOPROBE_INITIALIZE:
        discardData(frameTime + config.first().reportInterval, 1);
        timeoutAt(frameTime + config.first().reportInterval * 4.0 + 1.0);
        responseState = RESP_AUTOPROBE_WAIT;
        break;

    default:
        discardData(frameTime + 0.5, 1);
        invalidateLogging(frameTime);
        break;
    }
}

void AcquireRRPSAP3W::discardDataCompleted(double frameTime)
{
    Q_UNUSED(frameTime);
}

void AcquireRRPSAP3W::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frameTime);
    Q_UNUSED(frame);
}

SequenceValue::Transfer AcquireRRPSAP3W::getPersistentValues()
{
    PauseLock paused(*this);
    SequenceValue::Transfer result;

    if (FP::defined(filterStart.startTime)) {
        result.emplace_back(SequenceName({}, "raw", "ZFILTER"), buildFilterValue(filterStart),
                            filterStart.startTime, FP::undefined());
    }
    if (FP::defined(filterWhite.startTime)) {
        result.emplace_back(SequenceName({}, "raw", "ZWHITE"), buildFilterValue(filterWhite),
                            filterWhite.startTime, FP::undefined());
    }
    if (normalizationAcceptRemaining == 0 && FP::defined(normalizationStartTime)) {
        result.emplace_back(SequenceName({}, "raw", "ZSPOT"), buildNormalizationValue(),
                            normalizationStartTime, FP::undefined());

        qint64 value = normalizationPSAPTime;
        if (!INTEGER::defined(value))
            value = (qint64) floor(normalizationStartTime);

        result.emplace_back(SequenceName({}, "raw", "Ff"), Variant::Root(value),
                            normalizationStartTime, FP::undefined());
    }

    return result;
}

Variant::Root AcquireRRPSAP3W::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireRRPSAP3W::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

void AcquireRRPSAP3W::command(const Variant::Read &command)
{

    if (command.hash("StartFilterChange").exists()) {
        switch (sampleState) {
        case SAMPLE_RUN:
        case SAMPLE_FILTER_NORMALIZE:
        case SAMPLE_WHITE_FILTER_NORMALIZE:
        case SAMPLE_REQUIRE_FILTER_CHANGE:
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
            qCDebug(log) << "Manual filter change started in state" << sampleState;

            {
                Variant::Write info = Variant::Write::empty();
                switch (sampleState) {
                case SAMPLE_RUN:
                    info.hash("State").setString("Run");
                    break;
                case SAMPLE_FILTER_NORMALIZE:
                    info.hash("State").setString("Normalize");
                    insertDiagnosticNormalization(info);
                    break;
                case SAMPLE_WHITE_FILTER_NORMALIZE:
                    info.hash("State").setString("WhiteFilterNormalize");
                    insertDiagnosticNormalization(info);
                    break;
                case SAMPLE_REQUIRE_FILTER_CHANGE:
                    info.hash("State").setString("RequireFilterChange");
                    break;
                case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
                    info.hash("State").setString("RequireWhiteFilterChange");
                    break;
                default:
                    break;
                }
                event(Time::time(), QObject::tr("Manual filter change started."), true, info);
            }
            sampleState = SAMPLE_FILTER_CHANGING;
            forceRealtimeStateEmit = true;
            forceFilterBreak = true;
            resetSmoothers();

            for (int color = 0; color < 3; color++) {
                filterNormalization[color] = FP::undefined();
            }
            normalizationPSAPTime = INTEGER::undefined();
            normalizationStartTime = FP::undefined();
            lastTransmittanceCheckTime = FP::undefined();
            filterChangeStartPSAPTime = INTEGER::undefined();
            break;

            /* No effect in these states (already changing) */
        case SAMPLE_FILTER_CHANGING:
        case SAMPLE_WHITE_FILTER_CHANGING:
            qCDebug(log) << "Discarding filter change start in state" << sampleState;
            break;
        }
    }

    if (command.hash("StartWhiteFilterChange").exists()) {
        switch (sampleState) {
        case SAMPLE_RUN:
        case SAMPLE_FILTER_NORMALIZE:
        case SAMPLE_WHITE_FILTER_NORMALIZE:
        case SAMPLE_REQUIRE_FILTER_CHANGE:
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
            qCDebug(log) << "White filter change started in state" << sampleState;

            {
                Variant::Write info = Variant::Write::empty();
                switch (sampleState) {
                case SAMPLE_RUN:
                    info.hash("State").setString("Run");
                    break;
                case SAMPLE_FILTER_NORMALIZE:
                    info.hash("State").setString("Normalize");
                    insertDiagnosticNormalization(info);
                    break;
                case SAMPLE_WHITE_FILTER_NORMALIZE:
                    info.hash("State").setString("WhiteFilterNormalize");
                    insertDiagnosticNormalization(info);
                    break;
                case SAMPLE_REQUIRE_FILTER_CHANGE:
                    info.hash("State").setString("RequireFilterChange");
                    break;
                case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
                    info.hash("State").setString("RequireWhiteFilterChange");
                    break;
                default:
                    break;
                }
                event(Time::time(), QObject::tr("White filter change started."), true, info);
            }
            sampleState = SAMPLE_WHITE_FILTER_CHANGING;
            forceRealtimeStateEmit = true;
            whiteFilterChangeStartTime = FP::undefined();
            forceFilterBreak = true;
            resetSmoothers();
            normalizationStartTime = FP::undefined();
            lastTransmittanceCheckTime = FP::undefined();
            normalizationPSAPTime = INTEGER::undefined();
            filterChangeStartPSAPTime = INTEGER::undefined();
            for (int color = 0; color < 3; color++) {
                filterNormalization[color] = FP::undefined();
            }

            break;

            /* Already changing, to just switch a white check */
        case SAMPLE_FILTER_CHANGING:
            qCDebug(log) << "Switch to white filter change";

            {
                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("FilterChanging");
                insertDiagnosticFilterNormalization(info);
                event(Time::time(), QObject::tr("Switching to white filter change mode."), true,
                      info);
            }

            sampleState = SAMPLE_WHITE_FILTER_CHANGING;
            forceRealtimeStateEmit = true;
            whiteFilterChangeStartTime = FP::undefined();
            break;

            /* No effect */
        case SAMPLE_WHITE_FILTER_CHANGING:
            qCDebug(log) << "Discarding white filter change start";
            break;
        }
    }

    if (command.hash("StopFilterChange").exists()) {
        switch (sampleState) {
        case SAMPLE_FILTER_CHANGING:
            qCDebug(log) << "Filter change ended";
            {
                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("FilterChanging");
                insertDiagnosticFilterNormalization(info);
                event(Time::time(), QObject::tr("Filter change ended."), true, info);
            }

            sampleState = SAMPLE_FILTER_NORMALIZE;
            forceRealtimeStateEmit = true;
            filterChangeStartPSAPTime = INTEGER::undefined();
            filterChangeEndPSAPTime = INTEGER::undefined();
            resetSmoothers();
            break;

        case SAMPLE_WHITE_FILTER_CHANGING:
            qCDebug(log) << "White filter change ended";
            {
                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("WhiteFilterChanging");
                insertDiagnosticFilterNormalization(info);
                event(Time::time(), QObject::tr("White filter change ended."), true, info);
            }

            sampleState = SAMPLE_WHITE_FILTER_NORMALIZE;
            forceRealtimeStateEmit = true;
            filterChangeStartPSAPTime = INTEGER::undefined();
            filterChangeEndPSAPTime = INTEGER::undefined();
            resetSmoothers();
            break;

            /* No effect */
        case SAMPLE_RUN:
        case SAMPLE_FILTER_NORMALIZE:
        case SAMPLE_WHITE_FILTER_NORMALIZE:
        case SAMPLE_REQUIRE_FILTER_CHANGE:
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
            qCDebug(log) << "Discarding filter change stop, state: " << sampleState;
            break;
        }
    }
}

Variant::Root AcquireRRPSAP3W::getCommands()
{
    Variant::Root result;

    result["StartFilterChange"].hash("DisplayName").setString("Start &Filter Change");
    result["StartFilterChange"].hash("DisplayPriority").setInt64(-1);
    result["StartFilterChange"].hash("ToolTip")
                               .setString(
                                       "Start a filter change.  This will disable flow and allow you to change the filter in the instrument.");
    result["StartFilterChange"].hash("Exclude").array(0).hash("Type").setString("Flags");
    result["StartFilterChange"].hash("Exclude").array(0).hash("Flags").setFlags({"FilterChanging"});
    result["StartFilterChange"].hash("Exclude").array(0).hash("Variable").setString("F1");

    result["StopFilterChange"].hash("DisplayName").setString("End &Filter Change");
    result["StopFilterChange"].hash("DisplayPriority").setInt64(-1);
    result["StopFilterChange"].hash("ToolTip")
                              .setString(
                                      "This tells the instrument you are done changing the filter and it should resume sampling.");
    result["StopFilterChange"].hash("Include").array(0).hash("Type").setString("Flags");
    result["StopFilterChange"].hash("Include").array(0).hash("Flags").setFlags({"FilterChanging"});
    result["StopFilterChange"].hash("Include").array(0).hash("Variable").setString("F1");

    result["StartWhiteFilterChange"].hash("DisplayName").setString("Start a &White Filter Change");
    result["StartWhiteFilterChange"].hash("DisplayPriority").setInt64(1);
    result["StartWhiteFilterChange"].hash("ToolTip")
                                    .setString(
                                            "Start a filter change to a known to be white filter.  You should double check to make sure that only a single filter is placed in the instrument.");
    result["StartWhiteFilterChange"].hash("Exclude").array(0).hash("Type").setString("Flags");
    result["StartWhiteFilterChange"].hash("Exclude")
                                    .array(0)
                                    .hash("Flags")
                                    .setFlags({"WhiteFilterChanging"});
    result["StartWhiteFilterChange"].hash("Exclude").array(0).hash("Variable").setString("F1");

    return result;
}

AcquisitionInterface::AutoprobeStatus AcquireRRPSAP3W::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireRRPSAP3W::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_RUN:
        if (autoprobeValidRecords > 60) {
            /* Already running, just notify that we succeeded. */
            qCDebug(log) << "Interactive autoprobe requested with communications established at"
                         << Logging::time(time);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            break;
        }
        /* Fall through */

    default:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + config.first().reportInterval * 4.0 + 5.0);
        discardData(time + config.first().reportInterval, 1);
        responseState = RESP_AUTOPROBE_WAIT;
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Wait"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireRRPSAP3W::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    timeoutAt(time + config.first().reportInterval * 120.0 + 5.0);
    discardData(time + config.first().reportInterval, 1);
    responseState = RESP_AUTOPROBE_WAIT;
    generalStatusUpdated();
}

void AcquireRRPSAP3W::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + config.first().reportInterval * 2.0 + 1.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to interactive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from passive autoprobe to interactive acquisition at"
                     << Logging::time(time);

        responseState = RESP_WAIT;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Wait"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireRRPSAP3W::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + config.first().reportInterval * 2.0 + 1.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from passive autoprobe to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_WAIT;
        break;
    }
}

static const quint16 serializedStateVersion = 1;

void AcquireRRPSAP3W::serializeState(QDataStream &stream)
{
    PauseLock paused(*this);

    stream << serializedStateVersion;

    stream << accumulatedVolume << filterIsNotWhite << normalizationStartTime
           << normalizationPSAPTime << normalizationLatest << filterStart << filterWhite;

    for (int color = 0; color < 3; color++) {
        stream << filterNormalization[color];
    }
}

void AcquireRRPSAP3W::deserializeState(QDataStream &stream)
{
    quint16 version = 0;
    stream >> version;
    if (version != serializedStateVersion)
        return;

    PauseLock paused(*this);

    stream >> accumulatedVolume >> filterIsNotWhite >> normalizationStartTime
           >> normalizationPSAPTime >> normalizationLatest >> filterStart >> filterWhite;

    for (int color = 0; color < 3; color++) {
        stream >> filterNormalization[color];
    }

    normalizationAcceptRemaining = 8;
}

AcquisitionInterface::AutomaticDefaults AcquireRRPSAP3W::getDefaults()
{
    AutomaticDefaults result;
    result.name = "A$1$2";
    result.setSerialN81(9600);
    result.autoprobeLevelPriority = 20;
    return result;
}


ComponentOptions AcquireRRPSAP3WComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);
    return options;
}

ComponentOptions AcquireRRPSAP3WComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());
    return options;
}

QList<ComponentExample> AcquireRRPSAP3WComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples(true));
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireRRPSAP3WComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireRRPSAP3WComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireRRPSAP3WComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(
            ComponentExample(options, tr("Convert data with the default area and calibrations")));

    options = getBaseOptions();
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("area")))->set(18.01);
    examples.append(ComponentExample(options, tr("Explicitly defined area")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireRRPSAP3WComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                 const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireRRPSAP3W(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireRRPSAP3WComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                 const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireRRPSAP3W(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireRRPSAP3WComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                   const std::string &loggingContext)
{
    std::unique_ptr<AcquireRRPSAP3W> i(new AcquireRRPSAP3W(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireRRPSAP3WComponent::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{
    std::unique_ptr<AcquireRRPSAP3W> i(new AcquireRRPSAP3W(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
