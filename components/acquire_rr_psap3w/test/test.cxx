/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <cstdint>
#include <cmath>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"
#include "smoothing/baseline.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Smoothing;

#define R_RECORD_DARK_SUBTRACTED
#define ACCUMULATOR_DARK_SUBTRACTED

class ModelInstrument {
public:
    enum ReportMode {
        Report_R, Report_F, Report_S, Report_I, Report_O,
    };
    ReportMode reportMode;
    QByteArray outgoing;
    QByteArray incoming;
    double unpolledRemaining;
    QDateTime time;

    enum {
        Firmware_V2, Firmware_V1,
    } firmwareMode;

    std::int_fast64_t sampleCountRate[3];
    std::int_fast64_t referenceCountRate[3];
    std::int_fast64_t sampleDarkRate;
    std::int_fast64_t referenceDarkRate;

    std::int_fast64_t sampleCountRateDelta[3];
    std::int_fast64_t referenceCountRateDelta[3];
    std::int_fast64_t sampleDarkRateDelta;
    std::int_fast64_t referenceDarkRateDelta;

    double flow;
    quint16 flags;

    double weissA;
    double weissB;
    double area;
    double flowCalibration[3];
    int outputAveragingTime;
    std::int_fast64_t conversionRate;

    struct BoxcarData {
        std::int_fast64_t sample[3];
        std::int_fast64_t reference[3];
        std::int_fast64_t photoConversions[3];
        std::int_fast64_t flow;
        std::int_fast64_t flowSeconds;

        std::int_fast64_t sampleDark;
        std::int_fast64_t referenceDark;
        std::int_fast64_t seconds;
    };
    std::vector<BoxcarData> bins;
    BoxcarData current;
    BoxcarData output;
    std::int_fast64_t accumulatedSeconds;

    double In0[3];
    QDateTime InTime;

    void resetTransmittance()
    {
        for (int i = 0; i < 3; i++) {
            In0[i] = (double) sampleCountRate[i] / (double) referenceCountRate[i];
        }
        InTime = time;
    }

    ModelInstrument() : reportMode(Report_O),
                        unpolledRemaining(0),
                        time(QDate(2013, 2, 3), QTime(0, 0, 0), Qt::UTC),
                        firmwareMode(Firmware_V2)
    {
        for (int i = 0; i < 3; i++) {
            sampleCountRate[i] = 400000 - i * 10000;
            referenceCountRate[i] = 300010 - i * 500;

            sampleCountRateDelta[i] = -(3 - i) * 10;
            referenceCountRateDelta[i] = -(3 - i) * 3;
        }
        sampleDarkRate = -134;
        referenceDarkRate = -123;
        sampleDarkRateDelta = 1;
        referenceDarkRateDelta = -1;

        flow = 0.8;
        flags = 0x0001;

        weissA = 0.814;
        weissB = 1.237;
        area = 17.81;
        flowCalibration[0] = 1000;
        flowCalibration[1] = 1240;
        flowCalibration[2] = 2600;

        outputAveragingTime = 1;
        conversionRate = 200;
        accumulatedSeconds = 0;

        memset(&current, 0, sizeof(current));
        memset(&output, 0, sizeof(output));

        resetTransmittance();
    }

    int timeBinIndex() const
    {
        return time.toTime_t() % 60;
    }

    std::int_fast64_t flowVoltage() const
    {
        double raw = flow;
        if (raw < 0.3) {
            raw = raw * ((flowCalibration[1] - flowCalibration[0]) / (0.3 - 0.0)) +
                    flowCalibration[0];
        } else {
            raw = (raw - 0.3) * ((flowCalibration[2] - flowCalibration[1]) / (2.0 - 0.3)) +
                    flowCalibration[1];
        }
        return static_cast<std::int_fast64_t>(std::floor(raw + 0.5));
    }

    void accumulateData()
    {
        for (int i = 0; i < 3; i++) {
#ifdef ACCUMULATOR_DARK_SUBTRACTED
            current.sample[i] += (sampleCountRate[i] - sampleDarkRate + 4096) * conversionRate;
            current.reference[i] +=
                    (referenceCountRate[i] - referenceDarkRate + 4096) * conversionRate;
#else
            current.sample[i] += (sampleCountRate[i] +
                4096) * conversionRate;
            current.reference[i] += (referenceCountRate[i] + 
                4096) * conversionRate;
#endif

            current.photoConversions[i] += conversionRate;
        }
        current.flow += flowVoltage();
        current.flowSeconds += 1;
        current.sampleDark = sampleDarkRate;
        current.referenceDark = referenceDarkRate;
        current.seconds = accumulatedSeconds;

        bins.push_back(current);
        ++accumulatedSeconds;

        if (timeBinIndex() % 4 == 0)
            output = current;
    }

    void outputTime(const QDateTime &time)
    {
        outgoing.append(QByteArray::number(time.date().year() % 100).rightJustified(2, '0'));
        outgoing.append(QByteArray::number(time.date().month()).rightJustified(2, '0'));
        outgoing.append(QByteArray::number(time.date().day()).rightJustified(2, '0'));
        outgoing.append(QByteArray::number(time.time().hour()).rightJustified(2, '0'));
        outgoing.append(QByteArray::number(time.time().minute()).rightJustified(2, '0'));
        outgoing.append(QByteArray::number(time.time().second()).rightJustified(2, '0'));
    }

    void outputHex(std::int_fast64_t number, int digits)
    {
        if (number < 0) {
            /* Sign unextend */
            std::uint64_t mask = 0;
            for (int i = 0; i < digits / 2; i++) {
                mask <<= 8;
                mask |= 0xFF;
            }
            std::uint64_t raw = static_cast<std::uint64_t>(number);
            raw &= mask;
            number = static_cast<std::int_fast64_t>(raw);
        }
        outgoing.append(
                QByteArray::number(static_cast<qint64>(number), 16).rightJustified(digits, '0'));
    }

    void outputR()
    {
        for (int i = 0; i < 3; i++) {
#ifdef R_RECORD_DARK_SUBTRACTED
            outputHex((sampleCountRate[i] - sampleDarkRate) * conversionRate, 8);
#else
            outputHex(sampleCountRate[i] * conversionRate, 8);
#endif
            outgoing.append(' ');
#ifdef R_RECORD_DARK_SUBTRACTED
            outputHex((referenceCountRate[i] - referenceDarkRate) * conversionRate, 8);
#else
            outputHex(referenceCountRate[i] * conversionRate, 8);
#endif
            outgoing.append(' ');
            outputHex(conversionRate, 3);
            outgoing.append(' ');
            outputHex(0, 2);    /* Overflows */
            outgoing.append(' ');
        }
        outputHex(sampleDarkRate * conversionRate, 8);
        outgoing.append(' ');
        outputHex(referenceDarkRate * conversionRate, 8);
        outgoing.append(' ');
        outputHex(conversionRate, 3);
        outgoing.append(' ');
        outputHex(0, 2);    /* Overflows */
        outgoing.append(' ');

        outgoing.append(
                QByteArray::number(static_cast<int>(std::floor(flow * 1000))).rightJustified(4,
                                                                                             '0'));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(flow, 'f', 3).rightJustified(4, '0'));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(flags, 16).rightJustified(6, '0'));
    }

    static void convertBinDifference(const BoxcarData &b0,
                                     const BoxcarData &b1,
                                     int color,
                                     double &Ip,
                                     double &If)
    {
        std::int_fast64_t conversions = b1.photoConversions[color] - b0.photoConversions[color];

        std::int_fast64_t Ipt = b1.sample[color] - b0.sample[color];
        Ipt -= 4096 * conversions;
#ifndef ACCUMULATOR_DARK_SUBTRACTED
        Ipt -= b0.sampleDark * conversions;
#endif
        Ip = static_cast<double>(Ipt) / static_cast<double>(conversions);


        std::int_fast64_t Ift = b1.reference[color] - b0.reference[color];
        Ift -= 4096 * conversions;
#ifndef ACCUMULATOR_DARK_SUBTRACTED
        Ift -= b0.referenceDark * conversions;
#endif
        If = static_cast<double>(Ift) / static_cast<double>(conversions);
    }

    static double normalizedIntensity(const BoxcarData &b0, const BoxcarData &b1, int color)
    {
        double Ip;
        double If;
        convertBinDifference(b0, b1, color, Ip, If);
        if (!FP::defined(Ip) || !FP::defined(If) || If <= 0.0 || Ip <= 0.0)
            return FP::undefined();
        return Ip / If;
    }

    double absorptionAt(const BoxcarData &b0,
                        const BoxcarData &b1,
                        const BoxcarData &b2,
                        int color,
                        double area = -1,
                        double flow = -1) const
    {
        if (area <= 0.0)
            area = this->area;
        if (flow <= 0.0)
            flow = this->flow;

        double In0 = normalizedIntensity(b0, b1, color);
        double In1 = normalizedIntensity(b1, b2, color);
        if (!FP::defined(In0) || !FP::defined(In1) || In0 <= 0.0 || In1 <= 0.0)
            return FP::undefined();

        double dQt = flow * (double) (b2.seconds - b1.seconds);
        dQt /= 60000.0;

        return log(In0 / In1) * (area / dQt);
    }

    double reportedBa(int offset,
                      int color,
                      const Calibration calQ = Calibration(),
                      double area = -1) const
    {
        if (area <= 0)
            area = this->area;
        if (offset >= static_cast<int>(bins.size()))
            return FP::undefined();
        if (offset - outputAveragingTime * 2 < 0)
            return FP::undefined();
        const auto &b0 = bins[offset - outputAveragingTime * 2];
        const auto &b1 = bins[offset - outputAveragingTime];
        const auto &b2 = bins[offset];
        double Ba = absorptionAt(b0, b1, b2, color);
        if (!FP::defined(Ba))
            return FP::undefined();

        Ba = std::round(Ba * 10.0) / 10.0;
        Ba *= flow / calQ.apply(flow);
        Ba *= area / this->area;
        return Ba;
    }

    double reportedIr(int offset, int color) const
    {
        if (offset >= static_cast<int>(bins.size()))
            return FP::undefined();
        if (offset - outputAveragingTime < 0)
            return FP::undefined();
        const auto &b0 = bins[offset - outputAveragingTime];
        const auto &b1 = bins[offset];
        double In = normalizedIntensity(b0, b1, color);
        if (!FP::defined(In) || !FP::defined(In0[color]) || In0[color] <= 0.0)
            return FP::undefined();
        double Ir = In / In0[color];
        Ir = std::round(Ir * 1000.0) / 1000.0;
        return Ir;
    }

    void outputA()
    {
        if (static_cast<int>(bins.size()) < outputAveragingTime * 2) {
            outgoing.append("-999.9 -999.9 -999.9 ");
        } else {
            for (int color = 0; color < 3; color++) {
                double Ba = reportedBa(bins.size() - 1, color);
                if (!FP::defined(Ba)) {
                    outgoing.append("-999.9");
                } else {
                    outgoing.append(QByteArray::number(Ba, 'f', 1).rightJustified(6, ' '));
                }
                outgoing.append(' ');
            }
        }
        if (static_cast<int>(bins.size()) < outputAveragingTime) {
            outgoing.append("1.000 1.000 1.000 ");
        } else {
            for (int color = 0; color < 3; color++) {
                double Ir = reportedIr(bins.size() - 1, color);
                if (!FP::defined(Ir)) {
                    outgoing.append("1.000");
                } else {
                    outgoing.append(QByteArray::number(Ir, 'f', 3).rightJustified(5, ' '));
                }
                outgoing.append(' ');
            }
        }
        outgoing.append(QByteArray::number(flow, 'f', 3).rightJustified(5, ' '));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(flow, 'f', 3).rightJustified(5, ' '));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(outputAveragingTime * 2).rightJustified(2, ' '));
        outgoing.append(' ');
        outgoing.append(QByteArray::number(flags, 16).rightJustified(4, '0'));
    }

    void outputHexSum(std::int_fast64_t sum, int bytes)
    {
        std::int_fast64_t mask = 0;
        for (int add = 0; add < bytes; add++) {
            mask <<= 8;
            mask |= 0xFF;
        }
        outputHex(sum & mask, bytes * 2);
    }

    void outputAccumulators()
    {
        int index = timeBinIndex();
        outgoing.append(QByteArray::number(index).rightJustified(2, '0'));
        outgoing.append(',');

        switch (index % 4) {
        case 0:
            outputHexSum(bins.size(), 1);
            outgoing.append(',');
            outputHexSum(output.flow, 4);
            outgoing.append(',');
            outputHexSum(output.flowSeconds, 2);
            if (index != 0) {
                outgoing.append(',');
                outputHex(output.sampleDark, 6);
                outgoing.append(',');
                outputHex(output.referenceDark, 6);
                outgoing.append(',');
                outputHex(1, 6);
            }
            break;

        case 1:
        case 2:
        case 3: {
            int color = (index % 4) - 1;
            outputHexSum(output.sample[color], 5);
            outgoing.append(',');
            outputHexSum(output.reference[color], 5);
            outgoing.append(',');
            outputHexSum(output.photoConversions[color] * 16, 3);
            outgoing.append(',');
            outputHexSum(0, 1);
            break;
        }

        default:
            Q_ASSERT(false);
            break;
        }

        switch (index) {
        case 0:
            outgoing.append(',');
            outputTime(time);
            outgoing.append(",B1999:ba=b0/(k1*Tr+k0)");
            break;
        case 1:
            outgoing.append(",K0=");
            outgoing.append(QByteArray::number(weissA, 'f', 3));
            outgoing.append(",K1=");
            outgoing.append(QByteArray::number(weissB, 'f', 3));
            break;
        case 2:
            outgoing.append(",area=");
            outgoing.append(QByteArray::number(area, 'f', 2));
            break;

        case 16:
            if (firmwareMode != Firmware_V2)
                break;
        case 4:
        case 8:
        case 12:
            outgoing.append(',');
            outputTime(InTime);
            break;

        case 17:
            if (firmwareMode != Firmware_V2)
                break;
        case 5:
        case 9:
        case 13:
            outgoing.append(',');
            outgoing.append(QByteArray::number(In0[0], 'f', 3));
            break;

        case 18:
            if (firmwareMode != Firmware_V2)
                break;
        case 6:
        case 10:
        case 14:
            outgoing.append(',');
            outgoing.append(QByteArray::number(In0[1], 'f', 3));
            break;

        case 19:
            if (firmwareMode != Firmware_V2)
                break;
        case 7:
        case 11:
        case 15:
            outgoing.append(',');
            outgoing.append(QByteArray::number(In0[2], 'f', 3));
            break;

        case 21:
            outgoing.append(",mf0p0=");
            outgoing.append(QByteArray::number(flowCalibration[0], 'f', 0));
            break;
        case 22:
            outgoing.append(",mf0p3=");
            outgoing.append(QByteArray::number(flowCalibration[1], 'f', 0));
            break;
        case 23:
            outgoing.append(",mf2p0=");
            outgoing.append(QByteArray::number(flowCalibration[2], 'f', 0));
            break;

        case 56:
            if (firmwareMode != Firmware_V2)
                break;
            outgoing.append(",ver=2.03");
            break;
        case 57:
            if (firmwareMode != Firmware_V2)
                break;
            outgoing.append(",opt=c000");
            break;
        case 58:
            if (firmwareMode != Firmware_V2)
                break;
            outgoing.append(",dbg=0000");
            break;
        case 59:
            if (firmwareMode != Firmware_V2)
                break;
            outgoing.append(",romcsum=1b01");
            break;

        default:
            break;
        }
    }

    void advance(double seconds)
    {
        unpolledRemaining -= seconds;
        while (unpolledRemaining <= 0.0) {
            accumulateData();

            switch (reportMode) {
            case Report_R:
                outgoing.append("R ");
                outputTime(time);
                outgoing.append(' ');
                outputR();
                outgoing.append("\r\n");
                break;
            case Report_F:
                outgoing.append("F ");
                outputTime(time);
                outgoing.append(' ');
                outputA();
                outgoing.append("\r\n");
                break;
            case Report_S:
                outgoing.append("S ");
                outputTime(time);
                outgoing.append(' ');
                outputA();
                outgoing.append("\r\n");
                break;
            case Report_I:
                outgoing.append("I ");
                outputTime(time);
                outgoing.append(" \"");
                outputAccumulators();
                outgoing.append("\"\r\n");
                break;
            case Report_O:
                outputTime(time);
                outgoing.append(' ');
                outputA();
                outgoing.append(" \"");
                outputAccumulators();
                outgoing.append("\"\r\n");
                break;
            }


            for (int i = 0; i < 3; i++) {
                sampleCountRate[i] += sampleCountRateDelta[i];
                referenceCountRate[i] += referenceCountRateDelta[i];
            }
            sampleDarkRate += sampleDarkRateDelta;
            referenceDarkRate += referenceDarkRateDelta;

            time = time.addSecs(1);
            unpolledRemaining += 1.0;
        }
    }

    double calibratedFlow(const Calibration &cal = Calibration()) const
    {
        return cal.apply(flow);
    }

    double Qt(double totalTime, const Calibration &cal = Calibration()) const
    {
        return totalTime * calibratedFlow(cal) / 60000.0;
    }

    double L(double totalTime, double area = -1, const Calibration &cal = Calibration()) const
    {
        if (area <= 0)
            area = this->area;
        return Qt(totalTime, cal) / (area * 1E-6);
    }

    double IpD(int offset) const
    {
        if (offset < 0)
            return FP::undefined();
        if (offset >= static_cast<int>(bins.size()))
            return FP::undefined();
        const auto &b0 = bins[offset];
        return b0.sampleDark;
    }

    double IfD(int offset) const
    {
        if (offset < 0)
            return FP::undefined();
        if (offset >= static_cast<int>(bins.size()))
            return FP::undefined();
        const auto &b0 = bins[offset];
        return b0.referenceDark;
    }

    double Ip(int offset, int color, int increment = 1) const
    {
        if (offset == 0 && increment == 1 && bins.size() > 1) {
            const auto &b0 = bins[0];
            std::int_fast64_t conversions = b0.photoConversions[color];
            std::int_fast64_t Ipt = b0.sample[color];
            Ipt -= 4096 * conversions;
#ifndef ACCUMULATOR_DARK_SUBTRACTED
            Ipt -= b0.sampleDark * conversions;
#endif
            return static_cast<double>(Ipt) / static_cast<double>(conversions);
        }
        if (offset < increment)
            return FP::undefined();
        if (offset >= static_cast<int>(bins.size()))
            return FP::undefined();
        const auto &b0 = bins[offset - increment];
        const auto &b1 = bins[offset];
        double Ip = FP::undefined();
        double If = FP::undefined();
        convertBinDifference(b0, b1, color, Ip, If);
        return Ip;
    }

    double If(int offset, int color, int increment = 1) const
    {
        if (offset == 0 && increment == 1 && bins.size() > 1) {
            const auto &b0 = bins[0];
            std::int_fast64_t conversions = b0.photoConversions[color];
            std::int_fast64_t Ift = b0.reference[color];
            Ift -= 4096 * conversions;
#ifndef ACCUMULATOR_DARK_SUBTRACTED
            Ift -= b0.referenceDark * conversions;
#endif
            return static_cast<double>(Ift) / static_cast<double>(conversions);
        }
        if (offset < increment)
            return FP::undefined();
        if (offset >= static_cast<int>(bins.size()))
            return FP::undefined();
        const auto &b0 = bins[offset - increment];
        const auto &b1 = bins[offset];
        double Ip = FP::undefined();
        double If = FP::undefined();
        convertBinDifference(b0, b1, color, Ip, If);
        return If;
    }

    double In(int offset, int color, int increment = 1) const
    {
        if (offset == 0 && increment == 1 && bins.size() > 1) {
            const auto &b0 = bins[0];
            std::int_fast64_t conversions = b0.photoConversions[color];
            std::int_fast64_t Ipt = b0.sample[color];
            Ipt -= 4096 * conversions;
#ifndef ACCUMULATOR_DARK_SUBTRACTED
            Ipt -= b0.sampleDark * conversions;
#endif
            std::int_fast64_t Ift = b0.reference[color];
            Ift -= 4096 * conversions;
#ifndef ACCUMULATOR_DARK_SUBTRACTED
            Ift -= b0.referenceDark * conversions;
#endif
            double Ip = static_cast<double>(Ipt) / static_cast<double>(conversions);
            double If = static_cast<double>(Ift) / static_cast<double>(conversions);
            if (Ip <= 0.0 || If <= 0.0)
                return FP::undefined();
            return Ip / If;
        }

        if (offset < increment)
            return FP::undefined();
        if (offset >= static_cast<int>(bins.size()))
            return FP::undefined();
        const auto &b0 = bins[offset - increment];
        const auto &b1 = bins[offset];
        return normalizedIntensity(b0, b1, color);
    }

    double Ir(int offset, int normOffset, int color, int increment = 1) const
    {
        double In0 = In(normOffset, color, increment);
        if (!FP::defined(In0) || In0 == 0.0)
            return FP::undefined();
        double In1 = In(offset, color, increment);
        if (!FP::defined(In1))
            return FP::undefined();
        return In1 / In0;
    }

    double Ba(int offset,
              int color,
              double area = 17.81,
              const Calibration &cal = Calibration(),
              int increment = 1)
    {
        if (offset < increment * 2)
            return FP::undefined();
        if (offset >= static_cast<int>(bins.size()))
            return FP::undefined();
        const auto &b0 = bins[offset - increment * 2];
        const auto &b1 = bins[offset - increment];
        const auto &b2 = bins[offset];
        return absorptionAt(b0, b1, b2, color, area, cal.apply(flow));
    }
};

Q_DECLARE_METATYPE(ModelInstrument::ReportMode)

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Ff", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Qt", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("IrB", Variant::Root(467.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IrG", Variant::Root(530.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IrR", Variant::Root(660.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BaB", Variant::Root(467.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BaG", Variant::Root(530.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BaR", Variant::Root(660.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IfB", Variant::Root(467.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IfG", Variant::Root(530.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IfR", Variant::Root(660.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IpB", Variant::Root(467.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IpG", Variant::Root(530.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IpR", Variant::Root(660.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("ZSPOT", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZWHITE", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZFILTER", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("VQ", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("IfD", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("IpD", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("InB", Variant::Root(467.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("InG", Variant::Root(530.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("InR", Variant::Root(660.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IngB", Variant::Root(467.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IngG", Variant::Root(530.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IngR", Variant::Root(660.0), "^Wavelength", time))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream, ModelInstrument &instrument,
                     double logTimeBegin,
                     int binBegin,
                     double accumulateTimeBegin,
                     int n,
                     int normalizationBin = -1,
                     int increment = -1,
                     bool useReported = false,
                     bool isRealtime = false,
                     const Calibration &calQ = Calibration(),
                     double area = -1)
    {
        QList<double> times;
        QList<double> Q;
        QList<double> Qt;
        QList<double> L;
        QList<double> IrB;
        QList<double> IrG;
        QList<double> IrR;
        QList<double> BaB;
        QList<double> BaG;
        QList<double> BaR;
        QList<double> IpB;
        QList<double> IpG;
        QList<double> IpR;
        QList<double> IpD;
        QList<double> IfB;
        QList<double> IfG;
        QList<double> IfR;
        QList<double> IfD;
        QList<double> InB;
        QList<double> InG;
        QList<double> InR;

        if (normalizationBin < 0)
            normalizationBin = binBegin;

        if (increment <= 0)
            increment = useReported ? 1 : 4;
        for (int offset = 0; offset < n; offset += increment) {
            if (isRealtime) {
                times.append(logTimeBegin + offset + (double) increment);
            } else {
                times.append(logTimeBegin + offset);
            }

            double accumulatedTime = accumulateTimeBegin + offset;

            Q.append(calQ.apply(instrument.flow));
            Qt.append(instrument.Qt(accumulatedTime, calQ));
            L.append(instrument.L(accumulatedTime, area, calQ));

            int fromBin = binBegin + offset;
            if (!useReported) {
                IrB.append(instrument.Ir(fromBin, normalizationBin, 0, increment));
                IrG.append(instrument.Ir(fromBin, normalizationBin, 1, increment));
                IrR.append(instrument.Ir(fromBin, normalizationBin, 2, increment));

                BaB.append(instrument.Ba(fromBin, 0, area, calQ, increment));
                BaG.append(instrument.Ba(fromBin, 1, area, calQ, increment));
                BaR.append(instrument.Ba(fromBin, 2, area, calQ, increment));

                IpB.append(instrument.Ip(fromBin, 0, increment));
                IpG.append(instrument.Ip(fromBin, 1, increment));
                IpR.append(instrument.Ip(fromBin, 2, increment));

                IfB.append(instrument.If(fromBin, 0, increment));
                IfG.append(instrument.If(fromBin, 1, increment));
                IfR.append(instrument.If(fromBin, 2, increment));

                InB.append(instrument.In(fromBin, 0, increment));
                InG.append(instrument.In(fromBin, 1, increment));
                InR.append(instrument.In(fromBin, 2, increment));

                if (increment > 1 && fromBin % 60 == 0) {
                    /* Hack because accumulator mode doesn't output these
                     * on the minute, so the acquisition code uses the
                     * previously output ones */
                    IpD.append(instrument.IpD(fromBin - increment));
                    IfD.append(instrument.IfD(fromBin - increment));
                } else {
                    IpD.append(instrument.IpD(fromBin));
                    IfD.append(instrument.IfD(fromBin));
                }
            } else {
                IrB.append(instrument.reportedIr(fromBin, 0));
                IrG.append(instrument.reportedIr(fromBin, 1));
                IrR.append(instrument.reportedIr(fromBin, 2));

                BaB.append(instrument.reportedBa(fromBin, 0, calQ, area));
                BaG.append(instrument.reportedBa(fromBin, 1, calQ, area));
                BaR.append(instrument.reportedBa(fromBin, 2, calQ, area));
            }
        }

        if (!stream.verifyValues("Q", times, Q, !isRealtime, 1E-8))
            return false;
        if (!stream.verifyValues("Qt", times, Qt, !isRealtime, 1E-8))
            return false;
        if (!stream.verifyValues("L", times, L, !isRealtime, 1E-8))
            return false;

        if (!stream.verifyValues("IrB", times, IrB, !isRealtime, 1E-8))
            return false;
        if (!stream.verifyValues("IrG", times, IrG, !isRealtime, 1E-8))
            return false;
        if (!stream.verifyValues("IrR", times, IrR, !isRealtime, 1E-8))
            return false;

        if (!stream.verifyValues("BaB", times, BaB, !isRealtime, 1E-8))
            return false;
        if (!stream.verifyValues("BaG", times, BaG, !isRealtime, 1E-8))
            return false;
        if (!stream.verifyValues("BaR", times, BaR, !isRealtime, 1E-8))
            return false;

        if (!useReported) {
            if (!stream.verifyValues("IfB", times, IfB, !isRealtime, 1E-8))
                return false;
            if (!stream.verifyValues("IfG", times, IfG, !isRealtime, 1E-8))
                return false;
            if (!stream.verifyValues("IfR", times, IfR, !isRealtime, 1E-8))
                return false;
            if (!stream.verifyValues("IpB", times, IpB, !isRealtime, 1E-8))
                return false;
            if (!stream.verifyValues("IpG", times, IpG, !isRealtime, 1E-8))
                return false;
            if (!stream.verifyValues("IpR", times, IpR, !isRealtime, 1E-8))
                return false;
        }

        if (!isRealtime)
            return true;

        if (!useReported) {
            if (!stream.verifyValues("InB", times, InB, !isRealtime, 1E-8))
                return false;
            if (!stream.verifyValues("InG", times, InG, !isRealtime, 1E-8))
                return false;
            if (!stream.verifyValues("InR", times, InR, !isRealtime, 1E-8))
                return false;

            if (!stream.verifyValues("IfD", times, IfD, !isRealtime, 1E-8))
                return false;
            if (!stream.verifyValues("IpD", times, IpD, !isRealtime, 1E-8))
                return false;
        }

        return true;
    }

    bool checkState(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasValue("Ff", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasValue("ZSTATE", Variant::Root(), QString(), time))
            return false;

        return true;
    }

    bool checkPersistent(const SequenceValue::Transfer &values)
    {
        if (!StreamCapture::findValue(values, "raw", "Ff", Variant::Root()))
            return false;
        return true;
    }

    bool checkSpot(const SequenceValue::Transfer &values, double time, double area = 17.81)
    {
        if (!StreamCapture::findValue(values, "raw", "ZSPOT", Variant::Root(area), "Area", time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component =
                qobject_cast<AcquisitionComponent *>(ComponentLoader::create("acquire_rr_psap3w"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_rr_psap3w"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("area")));
        QVERIFY(qobject_cast<BaselineSmootherOption *>(options.get("filter-normalize")));
        QVERIFY(qobject_cast<BaselineSmootherOption *>(options.get("filter-change")));
    }


    void passiveAutoprobeF()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["StrictMode"].setBool(true);
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Smoother/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = ModelInstrument::Report_F;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 12; i++) {
            control.advance(1.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 60; i++) {
            control.advance(1.0);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 20.0, 21, 21.0, 10, 0, -1, true, false));
        QVERIFY(checkValues(realtime, instrument, 20.0, 21, 21.0, 10, 0, -1, true, true));
        QVERIFY(checkState(realtime));

        QVERIFY(checkPersistent(realtime.values()));
        QVERIFY(checkPersistent(persistentValues));
        QVERIFY(checkSpot(persistentValues, 1.0));

    }

    void passiveAutoprobeS()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["StrictMode"].setBool(true);
        cv["Area"].setDouble(17.12);
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Smoother/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = ModelInstrument::Report_S;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 12; i++) {
            control.advance(1.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 60; i++) {
            control.advance(1.0);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 20.0, 21, 21.0, 10, 0, -1, true, false,
                            Calibration(), 17.12));
        QVERIFY(checkValues(realtime, instrument, 20.0, 21, 21.0, 10, 0, -1, true, true,
                            Calibration(), 17.12));
        QVERIFY(checkState(realtime));

        QVERIFY(checkPersistent(realtime.values()));
        QVERIFY(checkPersistent(persistentValues));
        QVERIFY(checkSpot(persistentValues, 1.0, 17.12));

    }

    void passiveAutoprobeOReported()
    {
        Calibration calQ(QVector<double>() << 0.1 << 0.9);
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["StrictMode"].setBool(true);
        cv["Absorption/UseReported"].setBool(true);
        Variant::Composite::fromCalibration(cv["FlowCalibration"], calQ);
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Smoother/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = ModelInstrument::Report_O;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 120; i++) {
            control.advance(1.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 180; i++) {
            control.advance(1.0);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 140.0, 141, 141.0, 10, 0, -1, true, false, calQ));
        QVERIFY(checkValues(realtime, instrument, 140.0, 141, 141.0, 10, 0, -1, true, true, calQ));
        QVERIFY(checkState(realtime));

        QVERIFY(checkPersistent(realtime.values()));
        QVERIFY(checkPersistent(persistentValues));
        QVERIFY(checkSpot(persistentValues, 1.0));

    }

    void passiveAutoprobeR()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["StrictMode"].setBool(true);
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Smoother/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = ModelInstrument::Report_R;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 12; i++) {
            control.advance(1.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 60; i++) {
            control.advance(1.0);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 20.0, 21, 21.0, 10, 0, 1, false, false));
        QVERIFY(checkValues(realtime, instrument, 20.0, 21, 21.0, 10, 0, 1, false, true));
        QVERIFY(checkState(realtime));

        QVERIFY(checkPersistent(realtime.values()));
        QVERIFY(checkPersistent(persistentValues));
        QVERIFY(checkSpot(persistentValues, 1.0));

    }

    void passiveAutoprobeI()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["StrictMode"].setBool(true);
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Smoother/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = ModelInstrument::Report_I;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 120; i++) {
            control.advance(1.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 180; i++) {
            control.advance(1.0);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 141.0, 144, 144.0, 40, 4, -1, false, false));
        QVERIFY(checkValues(realtime, instrument, 141.0, 144, 144.0, 40, 4, -1, false, true));
        QVERIFY(checkState(realtime));

        QVERIFY(checkPersistent(realtime.values()));
        QVERIFY(checkPersistent(persistentValues));
        QVERIFY(checkSpot(persistentValues, 5.0));

    }

    void passiveAutoprobeO()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["StrictMode"].setBool(true);
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Smoother/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = ModelInstrument::Report_O;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 120; i++) {
            control.advance(1.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 180; i++) {
            control.advance(1.0);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 141.0, 144, 144.0, 40, 4, -1, false, false));
        QVERIFY(checkValues(realtime, instrument, 141.0, 144, 144.0, 40, 4, -1, false, true));
        QVERIFY(checkState(realtime));

        QVERIFY(checkPersistent(realtime.values()));
        QVERIFY(checkPersistent(persistentValues));
        QVERIFY(checkSpot(persistentValues, 5.0));

    }

    void interactiveAutoprobe()
    {
        static const Calibration calQ(QVector<double>() << 0.01 << 1.05);
        ValueSegment::Transfer config;
        Variant::Root cv;
        Variant::Composite::fromCalibration(cv["FlowCalibration"], calQ);
        cv["StrictMode"].setBool(false);
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = ModelInstrument::Report_R;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(1.0);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        double loggingStart = control.time();
        for (int i = 0; i < 60; i++) {
            control.advance(1.0);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, loggingStart + 1.0,
                            static_cast<int>(std::floor(loggingStart)) + 2, loggingStart + 1.0, 30,
                            1, 1, false, false, calQ));
        QVERIFY(checkValues(realtime, instrument, loggingStart + 1.0,
                            static_cast<int>(std::floor(loggingStart)) + 2, loggingStart + 1.0, 30,
                            1, 1, false, true, calQ));
        QVERIFY(checkState(realtime));

        QVERIFY(checkPersistent(realtime.values()));
        QVERIFY(checkPersistent(persistentValues));
        QVERIFY(checkSpot(persistentValues, 1.0));


    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 120; i++) {
            control.advance(1.0);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 1.0, 4, 4.0, 60, 4, -1, false, false));
        QVERIFY(checkValues(realtime, instrument, 1.0, 4, 4.0, 60, 4, -1, false, true));
        QVERIFY(checkState(realtime));

        QVERIFY(checkPersistent(realtime.values()));
        QVERIFY(checkPersistent(persistentValues));
        QVERIFY(checkSpot(persistentValues, 5.0));

    }

    void passiveAcquisitionOptions()
    {
        static const Calibration calQ(QVector<double>() << -0.02 << 0.9);
        ComponentOptions options(component->getPassiveOptions());
        qobject_cast<ComponentOptionSingleCalibration *>(options.get("cal-q"))->set(calQ);
        qobject_cast<BaselineSmootherOption *>(options.get("filter-normalize"))->overlay(
                new BaselineSinglePoint, FP::undefined(), FP::undefined());

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(options));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 120; i++) {
            control.advance(1.0);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 1.0, 4, 4.0, 60, 4, -1, false, false, calQ));
        QVERIFY(checkValues(realtime, instrument, 1.0, 4, 4.0, 60, 4, -1, false, true, calQ));
        QVERIFY(checkState(realtime));

        QVERIFY(checkPersistent(realtime.values()));
        QVERIFY(checkPersistent(persistentValues));
        QVERIFY(checkSpot(persistentValues, 5.0));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        for (int i = 0; i < 60; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 1.0, 4, 4.0, 40, 4, -1, false, false));
        QVERIFY(checkValues(realtime, instrument, 1.0, 4, 4.0, 40, 4, -1, false, true));
        QVERIFY(checkState(realtime));

        QVERIFY(checkPersistent(realtime.values()));
        QVERIFY(checkPersistent(persistentValues));
        QVERIFY(checkSpot(persistentValues, 5.0));

    }

    void autodetectFilterChangeO()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Autodetect/Smoother/Type"].setString("FixedTime");
        cv["Autodetect/Smoother/Time"].setDouble(5.0);
        cv["Autodetect/Smoother/RSD"].setDouble(0.01);
        cv["Autodetect/Smoother/Band"].setDouble(2.0);
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(true);
        cv["Autodetect/End/Enable"].setBool(true);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = ModelInstrument::Report_O;
        instrument.referenceCountRate[0] = 300010;
        instrument.referenceCountRateDelta[0] = 0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        control.advance(1.0);

        for (double dTE = control.time() + 6.0; control.time() <= dTE;) {
            control.advance(1.0);
        }
        instrument.referenceCountRate[0] = 5000;
        while (control.time() < 120.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        control.advance(1.0);

        double filterChangeEndTime = control.time();
        instrument.referenceCountRate[0] = 300010;
        instrument.referenceCountRateDelta[0] = -3;
        instrument.resetTransmittance();
        while (control.time() < 180.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), filterChangeEndTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkPersistent(persistentValues));
        QVERIFY(persistent.hasValue("Ff"));
        QVERIFY(StreamCapture::findValue(persistentValues, "raw", "ZFILTER", Variant::Root()));

    }

    void autodetectFilterChangeR()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Autodetect/Smoother/Type"].setString("FixedTime");
        cv["Autodetect/Smoother/Time"].setDouble(5.0);
        cv["Autodetect/Smoother/RSD"].setDouble(0.01);
        cv["Autodetect/Smoother/Band"].setDouble(2.0);
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(true);
        cv["Autodetect/End/Enable"].setBool(true);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = ModelInstrument::Report_R;
        instrument.referenceCountRate[0] = 300010;
        instrument.referenceCountRateDelta[0] = 0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        control.advance(1.0);

        for (double dTE = control.time() + 6.0; control.time() <= dTE;) {
            control.advance(1.0);
        }
        instrument.referenceCountRate[0] = 5000;
        while (control.time() < 120.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        double filterChangeEndTime = control.time();
        control.advance(1.0);

        instrument.referenceCountRate[0] = 300010;
        instrument.referenceCountRateDelta[0] = -3;
        instrument.resetTransmittance();
        while (control.time() < 240.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), filterChangeEndTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkPersistent(persistentValues));
        QVERIFY(persistent.hasValue("Ff"));
        QVERIFY(StreamCapture::findValue(persistentValues, "raw", "ZFILTER", Variant::Root()));

    }

    void autodetectFilterChangeI()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Autodetect/Smoother/Type"].setString("FixedTime");
        cv["Autodetect/Smoother/Time"].setDouble(5.0);
        cv["Autodetect/Smoother/RSD"].setDouble(0.01);
        cv["Autodetect/Smoother/Band"].setDouble(2.0);
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(true);
        cv["Autodetect/End/Enable"].setBool(true);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = ModelInstrument::Report_I;
        instrument.referenceCountRate[0] = 300010;
        instrument.referenceCountRateDelta[0] = 0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        for (int i = 0; i < 120; i++) {
            control.advance(1.0);
        }
        QTest::qSleep(50);

        instrument.referenceCountRate[0] = 5000;
        double baseTime = control.time();
        while (control.time() < baseTime + 120.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < baseTime + 120.0);
        control.advance(1.0);

        double filterChangeEndTime = control.time();
        instrument.referenceCountRate[0] = 300010;
        instrument.referenceCountRateDelta[0] = -3;
        instrument.resetTransmittance();
        while (control.time() < filterChangeEndTime + 120.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), filterChangeEndTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < filterChangeEndTime + 120.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkPersistent(persistentValues));
        QVERIFY(persistent.hasValue("Ff"));
        QVERIFY(StreamCapture::findValue(persistentValues, "raw", "ZFILTER", Variant::Root()));

    }

    void autodetectFilterChangeF()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Autodetect/Smoother/Type"].setString("FixedTime");
        cv["Autodetect/Smoother/Time"].setDouble(5.0);
        cv["Autodetect/Smoother/RSD"].setDouble(0.01);
        cv["Autodetect/Smoother/Band"].setDouble(2.0);
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(true);
        cv["Autodetect/End/Enable"].setBool(true);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = ModelInstrument::Report_F;
        instrument.referenceCountRate[0] = 300010;
        instrument.referenceCountRateDelta[0] = 0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        control.advance(1.0);

        for (double dTE = control.time() + 6.0; control.time() <= dTE;) {
            control.advance(1.0);
        }
        instrument.referenceCountRate[0] = 5000;
        while (control.time() < 120.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        control.advance(1.0);

        double filterChangeEndTime = control.time();
        instrument.referenceCountRate[0] = 300010;
        instrument.referenceCountRateDelta[0] = -3;
        instrument.resetTransmittance();
        while (control.time() < 180.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), filterChangeEndTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkPersistent(persistentValues));
        QVERIFY(persistent.hasValue("Ff"));
        QVERIFY(StreamCapture::findValue(persistentValues, "raw", "ZFILTER", Variant::Root()));

    }

    void commandIssue()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        control.advance(1.0);

        double checkTime1 = control.time();
        Variant::Write cmd = Variant::Write::empty();
        cmd["StartFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 120.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        control.advance(1.0);

        double checkTime2 = control.time();
        cmd.setEmpty();
        cmd["StopFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 180.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime1))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);
        control.advance(1.0);

        cmd.setEmpty();
        cmd["StartFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 240.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange"), checkTime2))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        control.advance(1.0);

        checkTime1 = control.time();
        cmd.setEmpty();
        cmd["StartWhiteFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 300.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("WhiteFilterChange")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 300.0);
        control.advance(1.0);

        checkTime2 = control.time();
        cmd.setEmpty();
        cmd["StopFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 360.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime1))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 360.0);
        control.advance(1.0);

        checkTime1 = control.time();
        cmd.setEmpty();
        cmd["StartWhiteFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 420.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("WhiteFilterChange"),
                                             checkTime2))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 420.0);
        control.advance(1.0);

        cmd.setEmpty();
        cmd["StopFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 480.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime1))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 480.0);
        control.advance(1.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkPersistent(persistentValues));

    }

    void stateSaveRestore()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.reportMode = ModelInstrument::Report_R;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 60; i++) {
            control.advance(1.0);
        }

        ElapsedTimer timeoutCheck;
        timeoutCheck.start();
        for (double tCheck = realtime.latestTime();
                timeoutCheck.elapsed() < 30000 && (!FP::defined(tCheck) || tCheck < 4.0);
                tCheck = realtime.latestTime()) {
            QTest::qSleep(50);
        }

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface = component->createAcquisitionPassive(config);
            control.attach(interface.get());
            interface->start();
            interface->setLoggingEgress(&logging);
            interface->setRealtimeEgress(&realtime);
            interface->setPersistentEgress(&persistent);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }
        }

        for (int i = 0; i < 60; i++) {
            control.advance(1.0);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 10.0, 11, 11.0, 45, 0, 1, false, false));
        QVERIFY(checkValues(realtime, instrument, 10.0, 11, 11.0, 45, 0, 1, false, true));
        /* Miss 1-second of time accumulation since we restore immediately, so
         * we drop one record (no end time) */
        QVERIFY(checkValues(logging, instrument, 65.0, 66, 65.0, 55, 0, 1, false, false));
        QVERIFY(checkValues(realtime, instrument, 65.0, 66, 65.0, 55, 0, 1, false, true));
        QVERIFY(checkState(realtime));

        QVERIFY(checkPersistent(realtime.values()));
        QVERIFY(checkPersistent(persistentValues));
        QVERIFY(checkSpot(persistentValues, 1.0));

    }

    void resumeLogic()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Filter/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        for (int color = 0; color < 3; color++) {
            instrument.referenceCountRateDelta[color] = 0;
            instrument.sampleCountRateDelta[color] = 0;
        }
        instrument.referenceDarkRateDelta = 0;
        instrument.sampleDarkRateDelta = 0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        for (int i = 0; i < 4; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }
        double checkTime = control.time();
        for (int i = 0; i < 4; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }

        Variant::Write cmd = Variant::Write::empty();
        cmd["StartWhiteFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 120.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("WhiteFilterChange"),
                                             checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        for (int i = 0; i < 4; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }
        checkTime = control.time();
        for (int i = 0; i < 4; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }

        cmd.setEmpty();
        cmd["StopFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 180.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);
        for (int i = 0; i < 4; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }
        checkTime = control.time();
        for (int i = 0; i < 4; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface = component->createAcquisitionInteractive(config);
            control.attach(interface.get());
            interface->setState(&control);
            interface->setControlStream(&control);
            interface->start();
            interface->setLoggingEgress(&logging);
            interface->setRealtimeEgress(&realtime);
            interface->setPersistentEgress(&persistent);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }
        }

        while (control.time() < 240.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        checkTime = control.time();
        for (int i = 0; i < 4; i++) {
            control.advance(1.0);
        }

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface = component->createAcquisitionInteractive(config);
            control.attach(interface.get());
            interface->setState(&control);
            interface->setControlStream(&control);
            interface->start();
            interface->setLoggingEgress(&logging);
            interface->setRealtimeEgress(&realtime);
            interface->setPersistentEgress(&persistent);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }
        }

        instrument.referenceCountRate[1] /= 3;
        while (control.time() < 300.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 300.0);
        checkTime = control.time();
        for (int i = 0; i < 4; i++) {
            control.advance(1.0);
        }

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface.reset();

            cv["Filter/EnableNormalizationRecovery"].setBool(false);
            config.clear();
            config.emplace_back(FP::undefined(), FP::undefined(), cv);
            interface = component->createAcquisitionInteractive(config);
            control.attach(interface.get());
            interface->setState(&control);
            interface->setControlStream(&control);
            interface->start();
            interface->setLoggingEgress(&logging);
            interface->setRealtimeEgress(&realtime);
            interface->setPersistentEgress(&persistent);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }
        }

        instrument.referenceCountRate[2] *= 3;
        while (control.time() < 360.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("RequireFilterChange"),
                                             checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 360.0);
        checkTime = control.time();
        for (int i = 0; i < 8; i++) {
            control.advance(1.0);
        }

        cmd.setEmpty();
        cmd["StartFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 420.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 420.0);
        for (int i = 0; i < 8; i++) {
            control.advance(1.0);
        }
        QTest::qSleep(50);

        cmd.setEmpty();
        cmd["StopFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 540.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 540.0);
        for (int i = 0; i < 4; i++) {
            control.advance(1.0);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkPersistent(persistentValues));

    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
