/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3STDINDEVICE_H
#define CPD3STDINDEVICE_H

#include "core/first.hxx"

#include <mutex>
#include <memory>
#include <thread>
#include <condition_variable>
#include <QtGlobal>
#include <QObject>
#include <QIODevice>
#include <QByteArray>

#include "core/core.hxx"
#include "core/waitutils.hxx"

namespace CPD3 {


/** 
 * A device that can be used to efficiently read from standard input.  It
 * reports atEnd() when standard input has received and EOF.
 */
class CPD3CORE_EXPORT StdinDevice : public QIODevice {
Q_OBJECT

    std::thread thread;
    std::mutex mutex;
    std::condition_variable external;
    bool terminated;
    QByteArray data;
    bool finished;
    int fdNotify;

    void run();
public:
    StdinDevice(QObject *parent = 0);

    ~StdinDevice();

    virtual qint64 bytesAvailable() const;

    virtual void close();

    virtual bool open(OpenMode mode);

    virtual bool atEnd() const;

    virtual bool isSequential() const;

public slots:

    virtual void signalTerminate();

protected:
    virtual qint64 readData(char *data, qint64 maxSize);

    virtual qint64 writeData(const char *data, qint64 maxSize);
};

}

#endif
