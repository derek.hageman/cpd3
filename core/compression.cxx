/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cstring>
#include <QtEndian>
#include <QLoggingCategory>

#include "core/compression.hxx"
#include "core/compress_lz4.hxx"
#include "core/waitutils.hxx"


Q_LOGGING_CATEGORY(log_core_compression, "cpd3.core.compression", QtWarningMsg)

namespace CPD3 {

/** @file core/compression.hxx
 * Provides compression routines.
 */

Compressor::~Compressor() = default;

bool Compressor::compress(const QByteArray &input, QByteArray &output, size_t maximumSize) const
{
    Util::ByteArray temp;
    bool result = this->compress(Util::ByteView(input), temp, maximumSize);
    output = temp.toQByteArray();
    return result;
}

bool Compressor::compressSkip(const Util::ByteView &input,
                              Util::ByteArray &output,
                              std::size_t skip,
                              std::size_t maximumSize) const
{
    Q_ASSERT(skip >= 0);
    if (skip == 0)
        return compress(input, output, maximumSize);
    output.resize(skip);
    Util::ByteArray temp;
    if (!compress(input, temp, maximumSize))
        return false;
    output += std::move(temp);
    return true;
}

bool Compressor::compressSkip(const QByteArray &input,
                              QByteArray &output,
                              std::size_t skip,
                              size_t maximumSize) const
{
    Util::ByteArray temp(output);
    bool result = this->compressSkip(Util::ByteView(input), temp, skip, maximumSize);
    output = temp.toQByteArray();
    return result;
}

Decompressor::~Decompressor() = default;

bool Decompressor::decompress(const QByteArray &input, QByteArray &output, size_t outputSize) const
{
    Util::ByteArray temp;
    bool result = this->decompress(Util::ByteView(input), temp, outputSize);
    output = temp.toQByteArray();
    return result;
}


CompressorZLIB::CompressorZLIB(int l) : level(l)
{ }

CompressorZLIB::~CompressorZLIB() = default;

bool CompressorZLIB::compress(const Util::ByteView &input,
                              Util::ByteArray &output,
                              std::size_t maximumSize) const
{
    if (input.empty()) {
        output.clear();
        return true;
    }
    auto temp = qCompress(input.toQByteArrayRef(), level);
    if (maximumSize > 0 && static_cast<std::size_t>(temp.size()) > maximumSize) {
        output.clear();
        return false;
    }
    output = Util::ByteArray(std::move(temp));
    return true;
}

DecompressorZLIB::DecompressorZLIB() = default;

DecompressorZLIB::~DecompressorZLIB() = default;

bool DecompressorZLIB::decompress(const Util::ByteView &input,
                                  Util::ByteArray &output,
                                  std::size_t) const
{
    if (input.empty()) {
        output.clear();
        return true;
    }
    auto temp = qUncompress(input.toQByteArrayRef());
    if (temp.isEmpty()) {
        return false;
    }
    output = Util::ByteArray(std::move(temp));
    return true;
}

namespace {
class CompressorNOOP : public Compressor {
public:
    CompressorNOOP() = default;

    virtual ~CompressorNOOP() = default;

    virtual bool compress(const Util::ByteView &input, Util::ByteArray &output, std::size_t) const
    {
        output = input;
        return true;
    }

    virtual bool compressSkip(const Util::ByteView &input,
                              Util::ByteArray &output,
                              std::size_t skip,
                              std::size_t) const
    {
        output.resize(skip);
        output += input;
        return true;
    }
};

enum {
    DecompressorID_None = 0, DecompressorID_LZ4, DecompressorID_ZLIB,
};

}

BlockCompressor::BlockCompressor(PreferredCompressionType compression) : decompressorID(
        static_cast<quint8>(DecompressorID_None)), includeUncompressedSize(false)
{
    switch (compression) {
    case None:
        backend.reset(new CompressorNOOP);
        break;
    case LZ4:
        backend.reset(new CompressorLZ4);
        decompressorID = static_cast<std::uint8_t>(DecompressorID_LZ4);
        includeUncompressedSize = true;
        break;
    case LZ4HC:
        backend.reset(new CompressorLZ4HC);
        decompressorID = static_cast<std::uint8_t>(DecompressorID_LZ4);
        includeUncompressedSize = true;
        break;
    case ZLIB:
        backend.reset(new CompressorZLIB);
        decompressorID = static_cast<std::uint8_t>(DecompressorID_ZLIB);
        break;
    }
    Q_ASSERT(backend);
}

BlockCompressor::~BlockCompressor() = default;

static void writeUncompressedBlock(const Util::ByteView &input, Util::ByteArray &output)
{
    output.push_back(static_cast<std::uint8_t>(DecompressorID_None));
    output += input;
}

void BlockCompressor::compress(const Util::ByteView &input, Util::ByteArray &output) const
{
    output.clear();
    if (input.size() < 512) {
        writeUncompressedBlock(input, output);
        return;
    }
    output.push_back(static_cast<std::uint8_t>(decompressorID));
    if (includeUncompressedSize) {
        quint32 n = static_cast<quint32>(input.size());
        Q_ASSERT(static_cast<std::size_t>(n) == input.size());
        output.appendNumber<quint32>(n);
    }
    /* Require compression to actually shrink it (output.size() is the current
     * overhead, so subtract that from the initial) */
    if (!backend->compressSkip(input, output, output.size(), input.size() - output.size())) {
        output.clear();
        writeUncompressedBlock(input, output);
        return;
    }
}

Util::ByteArray BlockCompressor::compress(const Util::ByteView &input) const
{
    Util::ByteArray result;
    compress(input, result);
    return result;
}

BlockDecompressor::BlockDecompressor()
        : backend(), lastDecompressorID(DecompressorID_None), hasUncompressedSize(false)
{ }

BlockDecompressor::~BlockDecompressor() = default;

static bool applyDecompressor(const Util::ByteView &input, Util::ByteArray &output,
                              bool hasUncompressedSize,
                              const Decompressor *dc)
{
    Q_ASSERT(dc);

    std::size_t outputSize = 0;
    std::size_t nSkip;
    if (hasUncompressedSize) {
        outputSize = input.readNumber<quint32>(1);
        if (outputSize <= 0) {
            output.clear();
            return false;
        }
        nSkip = 5;
    } else {
        nSkip = 1;
    }
    return dc->decompress(input.mid(nSkip), output, outputSize);
}

bool BlockDecompressor::decompress(const Util::ByteView &input, Util::ByteArray &output) const
{
    if (input.empty()) {
        output.clear();
        return false;
    }
    std::uint8_t decompressorID = input.front();
    if (decompressorID == DecompressorID_None) {
        output = input.mid(1);
        return true;
    }
    if (decompressorID == lastDecompressorID)
        return applyDecompressor(input, output, hasUncompressedSize, backend.get());

    switch (decompressorID) {
    case DecompressorID_None:
        Q_ASSERT(false);
        break;
    case DecompressorID_LZ4: {
        DecompressorLZ4 dc;
        return applyDecompressor(input, output, true, &dc);
    }
    case DecompressorID_ZLIB: {
        DecompressorZLIB dc;
        return applyDecompressor(input, output, false, &dc);
    }
    default:
        break;
    }
    Q_ASSERT(false);
    return false;
}

bool BlockDecompressor::decompress(const Util::ByteView &input, Util::ByteArray &output)
{
    if (input.empty()) {
        output.clear();
        return false;
    }
    std::uint8_t decompressorID = input.front();
    if (decompressorID == DecompressorID_None) {
        output = input.mid(1);
        return true;
    }
    if (decompressorID != lastDecompressorID) {
        lastDecompressorID = decompressorID;

        switch (decompressorID) {
        case DecompressorID_None:
            Q_ASSERT(false);
            break;
        case DecompressorID_LZ4:
            backend.reset(new DecompressorLZ4);
            hasUncompressedSize = true;
            break;
        case DecompressorID_ZLIB:
            backend.reset(new DecompressorZLIB);
            hasUncompressedSize = false;
            break;
        default:
            backend.reset();
            return false;
        }
    }
    return applyDecompressor(input, output, hasUncompressedSize, backend.get());
}

Util::ByteArray BlockDecompressor::decompress(const Util::ByteView &input, bool *ok) const
{
    Util::ByteArray result;
    bool s = decompress(input, result);
    if (ok)
        *ok = s;
    if (!s)
        result.clear();
    return result;
}

Util::ByteArray BlockDecompressor::decompress(const Util::ByteView &input, bool *ok)
{
    Util::ByteArray result;
    bool s = decompress(input, result);
    if (ok)
        *ok = s;
    if (!s)
        result.clear();
    return result;
}


void CompressedStream::createCompressorBackend()
{
    switch (preferredCompression) {
    case None:
        compressorBackend.reset(new CompressorNOOP);
        writeDecompressorID = static_cast<std::uint8_t>(DecompressorID_None);
        break;
    case LZ4:
        compressorBackend.reset(new CompressorLZ4);
        writeDecompressorID = static_cast<std::uint8_t>(DecompressorID_LZ4);
        break;
    case LZ4HC:
        compressorBackend.reset(new CompressorLZ4HC);
        writeDecompressorID = static_cast<std::uint8_t>(DecompressorID_LZ4);
        break;
    case ZLIB:
        compressorBackend.reset(new CompressorZLIB);
        writeDecompressorID = static_cast<std::uint8_t>(DecompressorID_ZLIB);
        break;
    }
}

void CompressedStream::initializeBackend()
{
    backend->disconnect(this);

    connect(backend, SIGNAL(aboutToClose()), this, SLOT(compressAllPending()));
    connect(backend, SIGNAL(aboutToClose()), this, SLOT(writePending()));
    connect(backend, SIGNAL(bytesWritten(qint64)), this, SLOT(writePending()));

    connect(backend, SIGNAL(readChannelFinished()), this, SLOT(completeReadChannel()));
    connect(backend, SIGNAL(readyRead()), this, SLOT(readMore()));

    bool canWrite = (openMode() & QIODevice::WriteOnly);
    bool canRead = (openMode() & QIODevice::ReadOnly);

    if (canWrite) {
        writePending();
        if (!flushCompressionTimer.isActive())
            flushCompressionTimer.start();
    }

    if (canRead) {
        readMore();
    }
}

CompressedStream::CompressedStream(QIODevice *setBackend, QObject *parent) : QIODevice(parent),
                                                                             maximumBufferTime(100),
                                                                             maximumBufferSize(
                                                                                     1073741824),
                                                                             readAheadSize(
                                                                                     16777216),
                                                                             preferredCompression(
                                                                                     LZ4),
                                                                             flushCompressionTimer(
                                                                                     this),
                                                                             backend(setBackend),
                                                                             hadWriteError(false),
                                                                             hadReadError(false),
                                                                             outstandingCompress(),
                                                                             outstandingWrite(),
                                                                             readBuffer(),
                                                                             outstandingRead(),
                                                                             writeDecompressorID(0),
                                                                             lastDecompressorID(
                                                                                     DecompressorID_None),
                                                                             compressorBackend(),
                                                                             decompressorBackend()
{
    createCompressorBackend();

    flushCompressionTimer.setSingleShot(true);
    flushCompressionTimer.setInterval(maximumBufferTime);
    connect(&flushCompressionTimer, SIGNAL(timeout()), this, SLOT(compressAllPending()));

    if (backend->isOpen()) {
        if (!QIODevice::open(backend->openMode())) {
            qCWarning(log_core_compression) << "Failed to set open mode for already open device:"
                                            << errorString();
        }
        initializeBackend();
    }
}

CompressedStream::~CompressedStream() = default;

void CompressedStream::setMaximumBufferTime(int s)
{
    maximumBufferTime = s;
    if (maximumBufferTime > 0) {
        flushCompressionTimer.setInterval(maximumBufferTime);
    } else {
        flushCompressionTimer.setInterval(0);
    }
    flushCompressionTimer.start();
}

int CompressedStream::getMaximumBufferTime() const
{ return maximumBufferTime; }

void CompressedStream::setMaximumBufferSize(int s)
{
    maximumBufferSize = s;
    if (maximumBufferSize > 1073741824)
        maximumBufferSize = 1073741824;
}

int CompressedStream::getMaximumBufferSize() const
{ return maximumBufferSize; }

void CompressedStream::setReadAheadSize(int s)
{ readAheadSize = s; }

int CompressedStream::getReadAheadSize() const
{ return readAheadSize; }

void CompressedStream::setPreferredCompression(PreferredCompressionType s)
{
    if (s == preferredCompression)
        return;
    preferredCompression = s;
    createCompressorBackend();
}

CompressedStream::PreferredCompressionType CompressedStream::getPreferredCompression() const
{ return preferredCompression; }

QIODevice *CompressedStream::getBackendDevice() const
{ return backend; }

bool CompressedStream::atEnd() const
{
    if (!backend->atEnd())
        return false;
    return outstandingRead.empty();
}

qint64 CompressedStream::bytesAvailable() const
{ return static_cast<qint64>(outstandingRead.size()); }

qint64 CompressedStream::bytesToWrite() const
{
    return backend->bytesToWrite() +
            (qint64) outstandingWrite.size() +
            (qint64) outstandingCompress.size();
}

void CompressedStream::close()
{
    flush();
    readBuffer.clear();
    outstandingRead.clear();
    backend->close();
    QIODevice::close();
}

/**
 * Immediately close the connection without flushing any data.
 */
void CompressedStream::abort()
{
    readBuffer.clear();
    outstandingRead.clear();
    backend->close();
    QIODevice::close();
}

bool CompressedStream::isSequential() const
{ return true; }

bool CompressedStream::open(QIODevice::OpenMode mode)
{
    if (!backend->open(mode))
        return false;
    if (!QIODevice::open(mode))
        return false;
    initializeBackend();
    return true;
}

bool CompressedStream::waitForBytesWritten(int msecs)
{
    ElapsedTimer to;
    to.start();

    compressAllPending();

    if (msecs > 0) {
        msecs -= to.elapsed();
        if (msecs < 0)
            msecs = 0;
    }
    return backend->waitForBytesWritten(msecs);
}

bool CompressedStream::waitForReadyRead(int msecs)
{
    ElapsedTimer to;
    to.start();

    for (;;) {
        if (!outstandingRead.empty())
            return true;

        if (msecs < 0) {
            if (!backend->waitForReadyRead(-1))
                return false;
        } else {
            int remaining = msecs - to.elapsed();
            if (remaining < 0)
                return false;
            if (!backend->waitForReadyRead(remaining))
                return false;
        }

        if (readToBlock() < 0)
            return false;
    }
}

bool CompressedStream::shouldReadMore() const
{
    /* Always read more if the read ahead buffer isn't full */
    if ((int) outstandingRead.size() < readAheadSize)
        return true;

    /* Only finish the block if the read ahead is empty (meaning nothing
     * to read) so that we ensure we don't wait forever if the read
     * ahead is smaller than the pending block. */
    if (outstandingRead.empty()) {
        /* Read until we can tell the block size */
        if (readBuffer.size() < 5)
            return true;
        /* Or until the current block is complete */
        quint32 blockSize = readBuffer.readNumber<quint32>(1);
        if (blockSize == 0 || blockSize > (quint32) 1073741824)
            return false;
        if (readBuffer.size() < blockSize + 5)
            return true;
    }
    return false;
}

void CompressedStream::decompressAllAvailable()
{
    Util::ByteArray temporaryBuffer;

    while (readBuffer.size() > 5) {
        quint32 blockSize = readBuffer.readNumber<quint32>(1);
        if (blockSize == 0 || blockSize > (quint32) 1073741824) {
            hadReadError = true;
            readBuffer.clear();
            return;
        }
        if (readBuffer.size() < blockSize + 5)
            break;

        std::uint8_t decompressorID = readBuffer.front();
        Util::ByteView blockData = readBuffer.mid(5, blockSize);
        if (decompressorID == DecompressorID_None) {
            outstandingRead += blockData;
        } else {
            if (decompressorID != lastDecompressorID) {
                lastDecompressorID = decompressorID;

                switch (decompressorID) {
                case DecompressorID_None:
                    Q_ASSERT(false);
                    break;
                case DecompressorID_LZ4:
                    decompressorBackend.reset(new DecompressorLZ4);
                    break;
                case DecompressorID_ZLIB:
                    decompressorBackend.reset(new DecompressorZLIB);
                    break;
                default:
                    decompressorBackend.reset();
                    hadReadError = true;
                    readBuffer.clear();
                    return;
                }
            }

            if (decompressorBackend->decompress(blockData, temporaryBuffer)) {
                outstandingRead += temporaryBuffer;
            }
        }

        readBuffer.pop_front(blockSize + 5);

        emit readyRead();
    }
}

qint64 CompressedStream::readToBlock()
{
    static constexpr std::size_t chunkSize = 65536;
    auto originalSize = readBuffer.size();
    qint64 n = backend->read(readBuffer.tail<char *>(chunkSize), chunkSize);
    if (n < 0) {
        hadReadError = true;
        readBuffer.resize(originalSize);
        return n;
    }
    if (n <= 0) {
        readBuffer.resize(originalSize);
        return n;
    }

    readBuffer.resize(originalSize + n);
    decompressAllAvailable();
    return n;
}

void CompressedStream::readMore()
{
    while (shouldReadMore()) {
        if (readToBlock() <= 0)
            break;
    }
}

void CompressedStream::drainBackendRead()
{
    while (!backend->atEnd()) {
        if (readToBlock() <= 0)
            break;
    }
}

void CompressedStream::completeReadChannel()
{
    while (readToBlock() > 0) { }
    decompressAllAvailable();
    readBuffer.clear();
    emit readChannelFinished();
}

qint64 CompressedStream::readData(char *data, qint64 maxSize)
{
    if (maxSize <= 0)
        return 0;

    /* Only return an error once we've read out all the non-errored data */
    if (outstandingRead.empty()) {
        if (hadReadError)
            return -1;
        return 0;
    }

    if (maxSize > static_cast<qint64>(outstandingRead.size()))
        maxSize = static_cast<qint64>(outstandingRead.size());

    std::memcpy(data, outstandingRead.data(), static_cast<std::size_t>(maxSize));
    outstandingRead.pop_front(static_cast<std::size_t>(maxSize));

    readMore();
    return maxSize;
}

void CompressedStream::compressNOOP(const Util::ByteView &input)
{
    outstandingWrite.push_back(DecompressorID_None);
    outstandingWrite.appendNumber<quint32>(static_cast<quint32>(input.size()));
    outstandingWrite += input;
}

void CompressedStream::compressBlock(const Util::ByteView &input)
{
    if (input.size() < 512) {
        compressNOOP(input);
        return;
    }

    auto originalSize = outstandingWrite.size();
    if (!compressorBackend->compressSkip(input, outstandingWrite, originalSize + 5,
                                         input.size() + 5)) {
        outstandingWrite.resize(originalSize);
        outstandingWrite.push_back(DecompressorID_None);
        outstandingWrite.appendNumber<quint32>(static_cast<quint32>(input.size()));
        outstandingWrite += input;
        return;
    }

    Q_ASSERT(outstandingWrite.size() >= originalSize + 5);
    auto *target = outstandingWrite.data<std::uint8_t *>(originalSize);
    *target = writeDecompressorID;
    ++target;
    qToLittleEndian<quint32>(static_cast<quint32>((outstandingWrite.size() - 5) - originalSize),
                             target);
}

bool CompressedStream::writePending()
{
    if (outstandingWrite.empty())
        return true;

    qint64 n = backend->write(outstandingWrite.toQByteArray());
    if (n < 0) {
        hadWriteError = true;
        return false;
    }
    if (n == 0)
        return true;

    outstandingWrite.pop_front(static_cast<std::size_t>(n));
    emit bytesWritten(n);
    return true;
}

bool CompressedStream::compressAllPending()
{
    flushCompressionTimer.stop();
    if (outstandingCompress.empty())
        return true;

    compressBlock(outstandingCompress);
    outstandingCompress.clear();
    return writePending();
}

bool CompressedStream::flush(int msecs)
{
    if (!(openMode() & QIODevice::WriteOnly))
        return false;

    ElapsedTimer to;
    to.start();

    if (!compressAllPending())
        return false;

    for (;;) {
        if (outstandingWrite.empty())
            return true;
        if (!writePending())
            return false;
        if (backend->bytesToWrite() <= 0 && outstandingWrite.empty())
            return true;

        if (msecs == 0) {
            return false;
        } else if (msecs < 0) {
            if (!backend->waitForBytesWritten(-1))
                return false;
        } else {
            int remaining = msecs - to.elapsed();
            if (remaining < 0)
                return false;
            if (!backend->waitForBytesWritten(remaining))
                return false;
        }
    }
}

qint64 CompressedStream::writeData(const char *data, qint64 maxSize)
{
    /* Return write errors immediately, since further data can't be
     * compressed anyway */
    if (hadWriteError)
        return -1;

    if (maxSize < 1)
        return 0;
    Q_ASSERT(maxSize < 1073741824);
    if (maximumBufferSize <= 1 || maximumBufferTime == 0) {
        if (!outstandingCompress.empty()) {
            if (!flush())
                return -1;
        }
        compressBlock(Util::ByteView(data, maxSize));
        if (!flush())
            return -1;
        return maxSize;
    }

    outstandingCompress += Util::ByteView(data, maxSize);

    if ((int) outstandingCompress.size() >= maximumBufferSize) {
        if (!flush())
            return -1;
    } else if (maximumBufferTime >= 0 && !flushCompressionTimer.isActive()) {
        flushCompressionTimer.start();
    }

    return maxSize;
}

}
