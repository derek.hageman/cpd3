/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3CORETIMEPARSE_H
#define CPD3CORETIMEPARSE_H

#include "core/first.hxx"

#include <QObject>
#include <QString>
#include <QStringList>

#include "core/timeutils.hxx"

namespace CPD3 {

/**
 * An exception representing an error parsing a time.
 */
class CPD3CORE_EXPORT TimeParsingException {
private:
    QString description;
public:
    TimeParsingException(const QString &d);

    /**
     * Get the localized description of what occurred.
     */
    QString getDescription() const;
};


/**
 * A reference point for time parsing.
 */
class CPD3CORE_EXPORT TimeReference {
public:
    /**
     * The time that this reference point represents.
     * 
     * @return an epoch time
     */
    virtual double getReference() noexcept(false) = 0;

    /**
     * The time that this reference point represents, defaulting to the
     * some implied time if the normal reference isn't available.  The default
     * implementation just returns getReference().
     * 
     * @return an epoch time
     */
    virtual double getReferenceImplied() noexcept(false);

    /**
     * Tests if this reference point represents the leading or trailing
     * edge of a bound set.  This controls what direction things are offset
     * from.
     * 
     * @return true if this is the leading (end) of a pair of bounds
     */
    virtual bool isLeading() noexcept(false) = 0;

    /**
     * Returns the value that should be used in the event of the bound being
     * undefined.  The default implementation throws an exception.
     * 
     * @return an epoch time
     */
    virtual double getUndefinedValue() noexcept(false);

    virtual ~TimeReference();
};

/**
 * The interface used by the list parsers to handle the components of the list
 * that don't represent times.
 */
class CPD3CORE_EXPORT TimeParseListHandler {
public:
    /**
     * Handle a leading part of the list.  Returns true if the string in 
     * question was handled by this function, false otherwise (denoting the
     * end of the arguments handled by this function).
     * 
     * @param str       the string to parse
     * @param index     the index of the argument
     * @param list      the list being parsed
     * @return          true if the string was handled
     */
    virtual bool handleLeading(const QString &str, int index, const QStringList &list)
    noexcept(false) = 0;

    /**
     * Handle a trailing part of the list.  Returns true if the string in 
     * question was handled by this function, false otherwise (denoting the
     * end of the arguments handled by this function).
     * 
     * @param str       the string to parse
     * @param index     the index of the argument
     * @param list      the list being parsed
     * @return          true if the string was handled
     */
    virtual bool handleTrailing(const QString &str, int index, const QStringList &list)
    noexcept(false) = 0;

    virtual ~TimeParseListHandler();
};

/**
 * A simple version of TimeParseListHandler that doesn't handle any arguments.
 */
class CPD3CORE_EXPORT TimeParseNOOPListHandler : public TimeParseListHandler {
public:
    virtual bool handleLeading(const QString &str, int index, const QStringList &list)
    noexcept(false);

    virtual bool handleTrailing(const QString &str, int index, const QStringList &list)
    noexcept(false);
};

/**
 * Contains time parsing functions.
 */
class CPD3CORE_EXPORT TimeParse : public QObject {
Q_OBJECT
public:
    TimeParse();

    static double parseOffset(const QString &offset,
                              TimeReference *const reference,
                              Time::LogicalTimeUnit *units = NULL)
    noexcept(false);

    static void parseOffset(const QString &offset,
                            Time::LogicalTimeUnit *const units = NULL,
                            int *const count = NULL,
                            bool *const aligned = NULL,
                            bool allowZero = false,
                            bool allowNegative = true)
    noexcept(false);

    static double parseTime(QString time,
                            TimeReference *const reference,
                            Time::LogicalTimeUnit *const units = NULL)
    noexcept(false);

    static double parseTime(QString time, Time::LogicalTimeUnit *const units = NULL)
    noexcept(false);

    static double parseListSingle(const QStringList &list,
                                  TimeReference *const reference,
                                  TimeParseListHandler *const handler,
                                  const bool allowZeroLength = false,
                                  Time::LogicalTimeUnit *const units = NULL,
                                  Time::LogicalTimeUnit defaultUnits = Time::Day)
    noexcept(false);

    static double parseListSingle(const QStringList &list,
                                  Time::LogicalTimeUnit *const units = NULL,
                                  Time::LogicalTimeUnit defaultUnits = Time::Day)
    noexcept(false);

    static Time::Bounds parseTimeBounds(const QString &start,
                                        const QString &end,
                                        bool allowUndefined = false,
                                        double defaultReference = -1.0)
    noexcept(false);

    static Time::Bounds parseListBounds(const QStringList &list,
                                        TimeParseListHandler *const handler,
                                        const Time::Bounds &defaultTimeBounds,
                                        const bool allowZeroLength = false,
                                        const bool allowUndefined = false,
                                        Time::LogicalTimeUnit defaultUnits = Time::Day,
                                        double defaultReference = -1.0)
    noexcept(false);

    static Time::Bounds parseListBounds(const QStringList &list,
                                        TimeParseListHandler *const handler,
                                        const bool allowZeroLength = false,
                                        const bool allowUndefined = false,
                                        Time::LogicalTimeUnit defaultUnits = Time::Day)
    noexcept(false);

    static Time::Bounds parseListBounds(const QStringList &list,
                                        const bool allowZeroLength = false,
                                        const bool allowUndefined = false,
                                        Time::LogicalTimeUnit defaultUnits = Time::Day)
    noexcept(false);

    static QString describeOffset(const bool allowZero = false, const bool allowNegative = true);

    static QString describeSingleTime(const bool allowInfinite = false);

    static QString describeListBoundsUsage(const bool allowZeroLength = false,
                                           const bool allowUndefined = false,
                                           Time::LogicalTimeUnit defaultUnits = Time::Day);

    static QString describeDurationUsage(const bool allowZeroLength = false,
                                         const bool allowNegative = false,
                                         Time::LogicalTimeUnit defaultUnits = Time::Day);
};

/**
 * Wrappers for legacy code.
 */
class CPD3CORE_EXPORT TimeParseLegacy {
public:
    static double parseTime(QString time,
                            TimeReference *const reference,
                            Time::LogicalTimeUnit *const units = NULL);

    static Time::Bounds parseListBounds(const QStringList &list,
                                        const bool allowZeroLength = false,
                                        const bool allowUndefined = false,
                                        Time::LogicalTimeUnit defaultUnits = Time::Day);
};

};

#endif
