/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3CORESHAREDWORK_H
#define CPD3CORESHAREDWORK_H

#include "core/first.hxx"

#include <memory>
#include <mutex>
#include <deque>
#include <functional>
#include <cstdint>
#include <condition_variable>

#include "core/core.hxx"
#include "core/waitutils.hxx"
#include "core/threading.hxx"

namespace CPD3 {

/**
 * A simple implementation that launches work handlers on a Qt thread.
 */
struct CPD3CORE_EXPORT QtThreadWorkLauncher {
public:
    void operator()(const std::function<void()> &run) const;

    void operator()(std::function<void()> &&run) const;
};

/**
 * A simple implementation that launches work on standard library threads.
 */
struct CPD3CORE_EXPORT StdThreadWorkLauncher {
public:
    void operator()(const std::function<void()> &run) const;
};

/**
 * A class that provides an interface to work with shared data in a thread
 * pool.
 * 
 * @tparam Result   the type of the result data produced
 * @tparam Data     the type of the shared data interface
 * @tparam Launch   the launcher for the workers
 */
template<typename Result, typename Data, typename Launch = QtThreadWorkLauncher>
class SharedWorkHandler {

    struct Private {
        std::mutex mutex;
        std::condition_variable cond;
        enum {
            Inactive, Running, ResultReady
        } state;
        Result result;
        Threading::Signal<> finished;

        Private() : mutex(), cond(), state(Inactive), result()
        { }

        static void run(const std::shared_ptr<Private> &p, const Data &data)
        {
            Result r(data);

            {
                std::lock_guard<std::mutex> lock(p->mutex);
                Q_ASSERT(p->state == Running);
                p->result = std::move(r);
                p->state = ResultReady;
            }

            p->finished();
            p->cond.notify_all();
        }
    };

    std::shared_ptr<Private> p;

public:
    SharedWorkHandler() : p(std::make_shared<Private>())
    {
        p->finished = finished;
    }

    SharedWorkHandler(const SharedWorkHandler &) = delete;

    SharedWorkHandler &operator=(const SharedWorkHandler &) = delete;

    /**
     * Test if the handler is available to run.  That is, test if there
     * is not a task currently running on it.
     * 
     * @param oldResult if not NULL then set to the existing result
     * @return          true if the handler is available (no task running)
     */
    bool available(Result *oldResult = nullptr) const
    {
        std::lock_guard<std::mutex> lock(p->mutex);
        if (oldResult)
            *oldResult = p->result;
        return p->state != Private::Running;
    }

    /**
     * Start the handler task.
     * 
     * @param d the input data
     */
    void start(const Data &d)
    {
        {
            std::lock_guard<std::mutex> lock(p->mutex);
            Q_ASSERT(p->state != Private::Running);
            p->state = Private::Running;
        }
        Launch()(std::bind(&Private::run, p, d));
    }

    /**
     * Wait for the result to be ready, returning whatever result was there.
     * 
     * @param time      the maximum time to wait in milliseconds, or -1 to wait forever
     * @param wasReady  if not NULL then set to to true if the handler was ready (no task running)
     * @return          the latest result
     */
    Result waitResult(int time = -1, bool *wasReady = nullptr)
    {
        ElapsedTimer to;
        to.start();

        std::unique_lock<std::mutex> lock(p->mutex);
        while (p->state != Private::ResultReady) {
            if (time == 0)
                break;
            if (time < 0) {
                p->cond.wait(lock);
            } else {
                int remaining = time - to.elapsed();
                if (remaining <= 0)
                    break;
                p->cond.wait_for(lock, std::chrono::milliseconds(remaining));
            }
        }
        if (wasReady)
            *wasReady = (p->state == Private::ResultReady);
        return p->result;
    }

    /**
     * Start the given data and wait for the result.  This is equivalent to
     * start(const Data &) and waitResult(unsigned long, bool *).
     * 
     * @param d         the input data
     * @param time      the maximum time to wait in milliseconds, or -1 to wait forever
     * @param wasReady  if not NULL then set to to true if the handler was ready (no task running)
     * @return          the latest result
     */
    Result startAndWait(const Data &d, int time = -1, bool *wasReady = NULL)
    {
        start(d);
        return waitResult(time, wasReady);
    }

    /**
     * Return the latest result.
     * 
     * @return  the latest result
     */
    Result latestResult() const
    {
        std::lock_guard<std::mutex> lock(p->mutex);
        return p->result;
    }

    /**
     * Wait for the handler to be available (no task running).
     * 
     * @param time      the maximum time to wait in milliseconds, or -1 to wait forever
     * @return          true of the handler was available
     */
    bool waitAvailable(int time = -1) const
    {
        ElapsedTimer to;
        to.start();

        std::unique_lock<std::mutex> lock(p->mutex);
        while (p->state != Private::ResultReady) {
            if (time == 0)
                break;
            if (time < 0) {
                p->cond.wait(lock);
            } else {
                int remaining = time - to.elapsed();
                if (remaining <= 0)
                    break;
                p->cond.wait_for(lock, std::chrono::milliseconds(remaining));
            }
        }
        return p->state == Private::ResultReady;
    }

    /**
     * Emitted when the executing of the data has finished.
     */
    Threading::Signal<> finished;
};

/**
 * A class that provides an interface to issue work to a shared thread pool that is
 * returned in order of issue.
 *
 * @tparam Result   the type of the result data produced
 * @tparam Data     the type of the shared data interface
 * @tparam Launch   the launcher for the workers
 */
template<typename Result, typename Data, typename Launch = QtThreadWorkLauncher>
class OrderedWorkHandler {
    class Task;

    typedef std::pair<std::uint_fast64_t, Result> CompletedData;

    struct Private {
        std::mutex mutex;
        std::condition_variable cond;
        std::size_t activeCount;
        std::deque<CompletedData> completed;
        std::uint_fast64_t nextIssue;
        std::uint_fast64_t nextComplete;
        Threading::Signal<> resultReady;

        Private() : mutex(), cond(), activeCount(0), completed(), nextIssue(0), nextComplete(0)
        { }

        static bool completedDataSorter(const CompletedData &a, const CompletedData &b)
        {
            return a.first < b.first;
        }

        static void run(const std::shared_ptr<Private> &p, std::uint_fast64_t id, const Data &d)
        {
            auto completed = std::make_pair(id, Result(d));

            bool isNext = false;
            {
                std::lock_guard<std::mutex> lock(p->mutex);
                p->completed
                 .emplace(std::lower_bound(p->completed.begin(), p->completed.end(), completed,
                                           completedDataSorter), std::move(completed));
                Q_ASSERT(p->activeCount != 0);
                p->activeCount--;
                isNext = (id == p->nextComplete);
            }

            if (isNext)
                p->resultReady();
            p->cond.notify_all();
        }
    };

    std::shared_ptr<Private> p;

public:
    OrderedWorkHandler() : p(std::make_shared<Private>())
    {
        p->resultReady = resultReady;
    }

    OrderedWorkHandler(const OrderedWorkHandler &) = delete;

    OrderedWorkHandler &operator=(const OrderedWorkHandler &) = delete;

    /**
     * Start the handler task on the global thread pool.
     *
     * @param d the input data
     */
    void start(const Data &d)
    {
        std::uint_fast64_t id;
        {
            std::lock_guard<std::mutex> lock(p->mutex);
            id = p->nextIssue++;
            p->activeCount++;
        }
        Launch()(std::bind(&Private::run, p, id, d));
    }

    /**
     * Wait for a result to be available.
     *
     * @param time      the maximum time to wait in milliseconds, or -1 to wait forever
     * @param stopOnEmpty   if set, then stop waiting and return false when there are no longer outstanding results
     * @return          true of the handler was available
     */
    bool waitAvailable(int time = -1, bool stopOnEmpty = true) const
    {
        ElapsedTimer to;
        to.start();

        std::unique_lock<std::mutex> lock(p->mutex);
        for (;;) {
            if (!p->completed.empty() && p->completed.front().first == p->nextComplete)
                return true;
            if (stopOnEmpty && p->activeCount == 0)
                break;
            if (time == 0)
                break;
            if (time < 0) {
                p->cond.wait(lock);
            } else {
                int remaining = time - to.elapsed();
                if (remaining <= 0)
                    break;
                p->cond.wait_for(lock, std::chrono::milliseconds(remaining));
            }
        }
        return false;
    }

    /**
     * Take the next available result.
     *
     * @param ok    if not null, set to true if the result is valid
     * @return      the next result
     */
    Result takeNext(bool *ok = nullptr)
    {
        if (ok)
            *ok = false;
        std::lock_guard<std::mutex> lock(p->mutex);
        if (p->completed.empty() || p->completed.front().first != p->nextComplete)
            return Result();
        if (ok)
            *ok = true;
        p->nextComplete++;
        Result r = std::move(p->completed.front().second);
        p->completed.pop_front();
        return r;
    }

    /**
     * Wait for the next result to be ready then take it
     *
     * @param time      the maximum time to wait in milliseconds, or -1 to wait forever
     * @param stopOnEmpty   if set, then stop waiting and return false when there are no longer outstanding results
     * @param ok    if not null, set to true if the result is valid
     * @return      the next result
     */
    Result waitAndTake(int time = -1, bool stopOnEmpty = true, bool *ok = nullptr)
    {
        if (ok)
            *ok = false;

        ElapsedTimer to;
        to.start();

        std::unique_lock<std::mutex> lock(p->mutex);
        for (;;) {
            if (!p->completed.empty() && p->completed.front().first == p->nextComplete) {
                if (ok)
                    *ok = true;
                p->nextComplete++;
                Result r = std::move(p->completed.front().second);
                p->completed.pop_front();
                return r;
            }
            if (stopOnEmpty && p->activeCount == 0)
                break;
            if (time == 0)
                break;
            if (time < 0) {
                p->cond.wait(lock);
            } else {
                auto remaining = time - to.elapsed();
                if (remaining <= 0)
                    break;
                p->cond.wait_for(lock, std::chrono::milliseconds(remaining));
            }
        }
        return Result();
    }

    /**
     * Get the length of the processing queue.  This does not include completed
     * but not yet retrieved requests.
     *
     * @return the number of outstanding processing requests
     */
    int processingQueueLength() const
    {
        std::lock_guard<std::mutex> lock(p->mutex);
        return static_cast<int>(p->activeCount);
    }

    /**
     * Test if the handler is still processing any data.  This does not include completed
     * but not yet retrieved requests.
     *
     * @return true if the handler is currently processing
     */
    bool isProcessing() const
    {
        return processingQueueLength() > 0;
    }

    /**
     * Wait until the processing queue is short enough.  This does not include completed
     * but not yet retrieved requests.
     *
     * @param time      the maximum time to wait in milliseconds, or -1 to wait forever
     * @param maximumActive the maximum number of active request to accept, or -1 for the default
     * @return          true if the processing queue is short enough
     */
    bool waitForQueueReady(int time = -1, int maximumActive = -1)
    {
        ElapsedTimer to;
        to.start();

        if (maximumActive < 0)
            maximumActive = std::thread::hardware_concurrency() * 2;

        std::unique_lock<std::mutex> lock(p->mutex);
        for (;;) {
            if (p->activeCount <= static_cast<std::size_t>(maximumActive))
                return true;
            if (time == 0)
                break;
            if (time < 0) {
                p->cond.wait(lock);
            } else {
                int remaining = time - to.elapsed();
                if (remaining <= 0)
                    break;
                p->cond.wait_for(lock, std::chrono::milliseconds(remaining));
            }
        }
        return false;
    }

    /**
     * Test if there are any available results.
     *
     * @return true if there are any results ready
     */
    inline bool isEmpty() const
    {
        std::lock_guard<std::mutex> lock(p->mutex);
        if (p->completed.empty())
            return true;
        return p->completed.front().first != p->nextComplete;
    }

    /**
     * Emitted when a result is ready.
     */
    Threading::Signal<> resultReady;
};

}

#endif
