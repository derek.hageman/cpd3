/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3COREWAIT_H
#define CPD3COREWAIT_H

#include "core/first.hxx"

#if defined(__cplusplus) && __cplusplus > 201100L
#include <atomic>
#elif defined(__GXX_EXPERIMENTAL_CXX0X__)

#include <cstdatomic>

#endif

#include <limits.h>
#include <mutex>
#include <QtGlobal>
#include <QMutex>
#include <QMutexLocker>
#include <QAtomicInt>

#include <QElapsedTimer>

#include "core/core.hxx"
#include "core/number.hxx"

namespace CPD3 {

typedef QElapsedTimer ElapsedTimer;

/**
 * Provides thread waiting routines.
 */
class CPD3CORE_EXPORT Wait {
public:
    static int backoffWaitTime
            (int count, double start = FP::undefined(), double timeout = FP::undefined());

    static void backoffWait
            (int count, double start = FP::undefined(), double timeout = FP::undefined());
};

/**
 * A simple indicator to test if a new thread should be spawned on a thread
 * pool or if one is already working.
 */
class CPD3CORE_EXPORT ThreadWorkingIndicator : private QAtomicInt {
    enum {
        Use_FetchAndStore =
#if defined(Q_ATOMIC_INT_FETCH_AND_STORE_IS_WAIT_FREE)
        true
#else
        false
#endif
        , Use_FetchAndAdd =
#if defined(Q_ATOMIC_INT_FETCH_AND_ADD_IS_WAIT_FREE)
        true
#else
        false
#endif
        , Use_TestAndSet =
#if defined(Q_ATOMIC_INT_TEST_AND_SET_IS_WAIT_FREE)
        true
#else
        false
#endif
        ,
    };
public:
    inline ThreadWorkingIndicator() : QAtomicInt(0)
    { }

    /**
     * Signal activation of the thread.  This sets the active indicator
     * if the thread was not active then returns true.  Returns false
     * if the thread was already active.
     * <br>
     * Generally a true return is immediately followed by queuing the handler
     * on a thread pool while a false return does nothing.
     * 
     * @return true if the thread should be started
     */
    inline bool activate()
    {
        if (Use_FetchAndStore) {
            return fetchAndStoreOrdered(1) == 0;
        } else if (Use_FetchAndAdd) {
            return fetchAndAddOrdered(1) == 0;
        } else if (Use_TestAndSet) {
            return testAndSetOrdered(0, 1);
        } else {
#ifdef Q_ATOMIC_INT_FETCH_AND_STORE_IS_SOMETIMES_NATIVE
            if (QAtomicInt::isFetchAndStoreNative()) {
                return fetchAndStoreOrdered(1) == 0;
            } else
#endif
#ifdef Q_ATOMIC_INT_FETCH_AND_ADD_IS_SOMETIMES_NATIVE
            if (QAtomicInt::isFetchAndAddNative()) {
                return fetchAndAddOrdered(1) == 0;
            } else
#endif
#ifdef Q_ATOMIC_INT_TEST_AND_SET_IS_SOMETIMES_NATIVE
            if (QAtomicInt::isTestAndSetNative()) {
                return testAndSetOrdered(0, 1);
            }
#endif
            return fetchAndStoreOrdered(1) == 0;
        }
    }

    /**
     * Release the thread from running.  This is used by the handler immediately
     * before it stops running and returns.
     */
    inline void release()
    {
        if (Use_TestAndSet) {
            testAndSetRelease(1, 0);
        } else if (Use_FetchAndStore) {
            fetchAndStoreRelease(0);
        } else {
#ifdef Q_ATOMIC_INT_FETCH_AND_STORE_IS_SOMETIMES_NATIVE
            if (QAtomicInt::isFetchAndStoreNative()) {
                fetchAndStoreRelease(0);
            } else
#endif
#ifdef Q_ATOMIC_INT_TEST_AND_SET_IS_SOMETIMES_NATIVE
            if (QAtomicInt::isTestAndSetNative()) {
                testAndSetRelease(1, 0);
            } else
#endif
            {
                fetchAndStoreRelease(0);
            }
        }
    }

    /**
     * Test if the thread is currently running.  This is only safe when also
     * protected by a mutex checking for the state.
     */
    inline bool running() const
    {
#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
        return (load() != 0);
#else
        return (loadRelaxed() != 0);
#endif
    }
};


/**
 * A class that implements blocking to limit the growth of a buffer.  This is
 * used within the functions that add to the buffer to cause their callers
 * to block when the buffer gets too large.
 */
class CPD3CORE_EXPORT BufferSizeLimiter {
    size_t thresholdLimit;
    size_t lastTriggerThreshold;
    int sleepTime;

    void executeLimiting(size_t size, QMutex *mutex);

    void executeLimiting(size_t size, std::mutex *mutex);

    void updateSleepTime(size_t size);

public:
    BufferSizeLimiter(size_t limit = 16384);

    /**
     * Apply buffer limiting, sleeping as needed.
     * 
     * @param size  the current size of the buffer
     * @param mutex the currently held mutex to the buffer
     */
    inline void run(size_t size, QMutex &mutex)
    {
        if (size < thresholdLimit)
            return;
        executeLimiting(size, &mutex);
    }

    /**
     * Apply buffer limiting, sleeping as needed.
     *
     * @param size  the current size of the buffer
     * @param mutex the currently held mutex to the buffer
     */
    inline void run(size_t size, std::mutex *mutex)
    {
        if (size < thresholdLimit)
            return;
        executeLimiting(size, mutex);
    }

    /**
     * Apply buffer limiting, sleeping as needed.
     * 
     * @param size      the current size of the buffer
     * @param locker    the currently held locker to the buffer
     */
    inline void run(size_t size, const QMutexLocker &locker)
    {
        if (size < thresholdLimit)
            return;
        executeLimiting(size, locker.mutex());
    }

    /**
     * Apply buffer limiting returning the time to sleep.
     * 
     * @param size  the current size of the buffer
     */
    inline int externalRun(size_t size)
    {
        if (size < thresholdLimit)
            return 0;
        updateSleepTime(size);
        return sleepTime;
    }
};

}

#endif
