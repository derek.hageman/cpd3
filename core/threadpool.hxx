/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3CORETHREADPOOL_H
#define CPD3CORETHREADPOOL_H

#include "core/first.hxx"

#include <functional>
#include <queue>
#include <unordered_set>
#include <mutex>
#include <condition_variable>
#include <QtGlobal>
#include <QRunnable>
#include <QThread>


#include <QQueue>

#include "core/core.hxx"

namespace CPD3 {

/**
 * A simple thread pool implementation.  This exists because of problems with Qt's implementation
 * appears to have problems related to thread reservation.
 */
class CPD3CORE_EXPORT ThreadPool {
    int threadCount;
    std::mutex mutex;
    std::condition_variable threadNotify;
    std::condition_variable poolNotify;
    bool poolEmptyInProgress;

    class Thread : public QThread {
        ThreadPool *pool;
    public:
        int blockingCount;
        std::function<void()> immediateTask;

        Thread(ThreadPool *pool);

        virtual ~Thread();

    protected:
        virtual void run();
    };

    std::unordered_set<Thread *> threads;
    std::unordered_set<Thread *> ended;
    std::unordered_set<Thread *> idle;
    std::unordered_set<Thread *> blocked;

    friend class Thread;

    std::queue<std::function<void()>> tasks;

    void reapAllThreads();

    void maybeStartThread();

    inline int totalRunningThreads() const
    {
        Q_ASSERT(threads.size() >= blocked.size());
        return threads.size() - blocked.size();
    }

    void threadExecute(Thread *thread);

public:
    /**
     * Return the default number of threads to start a pool with.
     *
     * @return  the default number of threads
     */
    static int defaultNumberOfThreads();

    /**
     * Create a thread pool.
     *
     * @param threadCount   the maximum number of threads under normal conditions.
     */
    ThreadPool(int threadCount = ThreadPool::defaultNumberOfThreads());

    ~ThreadPool();

    /**
     * Wait for all threads in the pool to be idle.
     */
    void wait();

    /**
     * Execute a functor on the thread pool
     *
     * @param f     the functor to execute
     */
    void run(const std::function<void()> &f);

    /**
     * Execute a functor on the pool launching a new thread if required for immediate execution.
     *
     * @param f     the functor to execute
     */
    void immediate(const std::function<void()> &f);

    /**
     * Execute a runnable on the pool.  The runnable is deleted after completion if
     * its autodelete is set.
     *
     * @param run   the runnable to execute
     */
    void run(QRunnable *run);

    /**
     * Execute a runnable on the pool launching a new thread if required for immediate execution.
     * The runnable is deleted after completion if its autodelete is set.
     *
     * @param run   the runnable to execute
     */
    void immediate(QRunnable *run);

    /**
     * Get a pointer to the system thread pool.
     *
     * @return  a static system thread pool
     */
    static ThreadPool *system();
};

}

#endif
