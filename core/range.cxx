/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/range.hxx"

namespace CPD3 {

/** @file core/range.hxx
 * Provides general range manipulation routines.
 */

/**
 * Test if a start is not within a given range.  Returns -1, 0, or 1 if the
 * start is before the range, within the range, or after the range.
 * 
 * @param checkStart        the start to check
 * @param start             the start of the range
 * @param end               the end of the range
 * @param inclusiveStart    true if the start of the range is inclusive
 * @param inclusiveEnd      true if the end of the range is inclusive
 * @return                  the comparison result, -1, 0, or 1
 */
int Range::notWithinStart(double checkStart,
                          double start,
                          double end,
                          bool inclusiveStart,
                          bool inclusiveEnd)
{
    if (!FP::defined(checkStart)) {
        return FP::defined(start) ? -1 : 0;
    }
    if (FP::defined(start)) {
        if (checkStart < start || (checkStart == start && !inclusiveStart))
            return -1;
    }
    if (FP::defined(end)) {
        if (checkStart > end || (checkStart == end && !inclusiveEnd))
            return 1;
    }
    return 0;
}

/**
 * Test if an end is not within a given range.  Returns -1, 0, or 1 if the
 * end is before the range, within the range, or after the range.
 * 
 * @param checkEnd          the end to check
 * @param start             the start of the range
 * @param end               the end of the range
 * @param inclusiveStart    true if the start of the range is inclusive
 * @param inclusiveEnd      true if the end of the range is inclusive
 * @return                  the comparison result, -1, 0, or 1
 */
int Range::notWithinEnd(double checkEnd,
                        double start,
                        double end,
                        bool inclusiveStart,
                        bool inclusiveEnd)
{
    if (!FP::defined(checkEnd)) {
        return FP::defined(end) ? 1 : 0;
    }
    if (FP::defined(start)) {
        if (checkEnd < start || (checkEnd == start && !inclusiveStart))
            return -1;
    }
    if (FP::defined(end)) {
        if (checkEnd > end || (checkEnd == end && !inclusiveEnd))
            return 1;
    }
    return 0;
}

}
