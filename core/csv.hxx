/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3CORECSV_H
#define CPD3CORECSV_H

#include "core/first.hxx"

#include <vector>
#include <deque>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QStringList>
#include <QByteArray>
#include <QList>
#include "core/core.hxx"
#include "util.hxx"

namespace CPD3 {
namespace CSV {

/**
 * Join a list of fields in CSV style.  Note that this does not account for
 * CSV data with embedded newlines.
 *
 * @param fields        the fields to combine
 * @param separator     the string that separates fields (the "comma")
 * @param quote         the string used to quote fields (added to the start and end)
 * @param quoteEscape   the string used to escape embedded quotes
 * @return              the combined string
 */
CPD3CORE_EXPORT QString join(const QStringList &fields,
                             const QString &separator = QString(','),
                             const QString &quote = QString('\"'),
                             const QString &quoteEscape = QString("\"\"\""));

/**
 * Split a CSV string into its component fields.
 *
 * @param data          the string to be split
 * @param separator     the string that separates fields (the "comma")
 * @param quote         the string used to quote fields (added to the start and end)
 * @param quoteEscape   the string used to escape embedded quotes
 * @return              the separated fields
 */
CPD3CORE_EXPORT QStringList split(const QString &data,
                                  const QString &separator = QString(','),
                                  const QString &quote = QString('\"'),
                                  const QString &quoteEscape = QString("\"\"\""));

/**
 * Perform generalized splitting for acquisition components.  This splits
 * the input based on either spaces, tabs or commas, as available.  Spaces
 * and tabs are compressed into single logical delimiters (so consecutive
 * spaces are interpreted as padding) while commas are not.
 *
 * @param input         the input data
 * @return              the split fields
 */
CPD3CORE_EXPORT std::deque<Util::ByteView> acquisitionSplit(const Util::ByteView &input);

}
}

#endif
