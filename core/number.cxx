/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#ifdef TIME_POSIX

#else
# ifdef TIME_WINdowS
#  include <time.h>
#  include <windows.h>
# else
#  include <time.h>
# endif
#endif

#include <cmath>
#include <vector>
#include <QRegExp>
#include <QCoreApplication>
#include <QHostInfo>
#include <QThread>

#include <QUuid>

#include "core/number.hxx"
#include "core/openssl.hxx"

/* Note, have to include this later, or it causes definition conflicts with
 * the number definitions */
#include <complex>

namespace CPD3 {

/** @file core/number.hxx
 * Provides general number manipulation routines.
 */


static std::int_fast64_t pow10(std::int_fast64_t n)
{
    Q_ASSERT(n >= 0);
    std::int_fast64_t result = 1;
    for (std::int_fast64_t i = 0; i < n; i++) {
        result *= 10;
    }
    return result;
}

/* This mostly exists because Qt's rounding behavior in formatting has changed
 * at least once, and I'd rather not deal with that again */
static QString fixedDecimalFormat(double d, int digits, bool stripTrailing = false)
{
    if (digits < 0)
        digits = 0;

    QString result;
    if (d < 0) {
        d = -d;
        result.append(QLatin1Char('-'));
    }

    std::int_fast64_t shift = pow10(digits);
    std::int_fast64_t shifted = std::lround(d * shift);
    if (shift < 0 || shifted < 0) {
        /* Overflow, so just accept whatever */
        return QString::number(d, 'f', digits);
    }
    std::int_fast64_t icmp = shifted / shift;
    std::int_fast64_t fcmp = shifted - (icmp * shift);

    result.append(QString::number(icmp));
    if (digits == 0 || (stripTrailing && fcmp == 0))
        return result;

    result.append(QLatin1Char('.'));
    for (int i = 0; i < digits; i++) {
        shift /= 10;
        std::int_fast64_t digit = (fcmp / shift) % 10;
        if (digit < 0 || digit > 9)
            digit = 0;
        result.append(QLatin1Char('0' + static_cast<char>(digit)));

        fcmp -= digit * shift;
        if (stripTrailing && fcmp == 0)
            break;
    }
    return result;
}

QString FP::decimalFormat(double d, int prec)
{
    static const QString undefinedString("Undefined");
    if (!FP::defined(d))
        return undefinedString;
    return fixedDecimalFormat(d, prec, true);
}

QString FP::scientificFormat(double d,
                             int decimals,
                             int exponents,
                             const QString &exponentSign,
                             const QString &plusSign,
                             const QString &plusExponent)
{
    static const QString undefinedString("Undefined");
    if (!FP::defined(d))
        return undefinedString;

    Q_ASSERT(exponents > 0);
    Q_ASSERT(decimals >= 0);

    QString result;

    if (d == 0.0) {
        result.append(plusSign);
        result.append(QLatin1Char('0'));
        if (decimals > 0) {
            result.append(QLatin1Char('.'));
            result.append(QString(QLatin1Char('0')).repeated(decimals));
        }
        result.append(exponentSign);
        result.append(plusExponent);
        if (exponents > 0)
            result.append(QString(QLatin1Char('0')).repeated(exponents));
        return result;
    }

    if (d < 0.0) {
        result.append(QLatin1Char('-'));
        d = -d;
    } else {
        result.append(plusSign);
    }

    auto e = static_cast<std::int_fast64_t>(std::floor(std::log10(d)));

    /* Handle the case where rounding in the fractional part would cause a
     * value >= 10, which should be shifted into the exponent */
    d /= std::pow(10.0, e - decimals);
    d = std::round(d) / pow10(decimals);
    if (d >= 10.0) {
        d /= 10.0;
        e++;
    }

    result.append(fixedDecimalFormat(d, decimals));
    result.append(exponentSign);
    if (e >= 0)
        result.append(plusExponent);
    result.append(QString::number(e).rightJustified(exponents, QLatin1Char('0')));
    return result;
}

/* From Qt's qAllocMore */
size_t INTEGER::growAlloc(size_t minimumSize)
{
    static const size_t page = 1 << 12;
    size_t nalloc;
    if (minimumSize < 1 << 6) {
        nalloc = (1 << 3) + ((minimumSize >> 3) << 3);
    } else {
        if (minimumSize >= std::numeric_limits<std::size_t>::max() / 2)
            return std::numeric_limits<std::size_t>::max();
        nalloc = (minimumSize < page) ? 1 << 3 : page;
        while (nalloc < minimumSize) {
            if (nalloc <= 0)
                return std::numeric_limits<std::size_t>::max();
            nalloc *= 2;
        }
    }
    return nalloc;
}

/**
 * Returns a random 32 bit integer.
 *
 * @return  a random number r, 0 <= r < 2^32
 */
quint32 Random::integer32()
{
    char buffer[4];
    fill(buffer, 4);
    quint32 i;
    memcpy(&i, buffer, 4);
    return i;
}

/**
 * Returns a random 64 bit integer.
 *
 * @return  a random number r, 0 <= r < 2^64
 * @see integer32()
 */
quint64 Random::integer()
{
    char buffer[8];
    fill(buffer, 8);
    quint64 i;
    memcpy(&i, buffer, 8);
    return i;
}

/**
 * Returns a random number in the range [0.0, 1.0).
 *
 * @return  a random number r, 0 <= r < 1
 * @see integer32()
 */
double Random::fp()
{
    return (double) (integer() >> 11) / (double) (Q_INT64_C(1) << 53);
}

/**
 * Generate a random string of characters in the 0-9a-uv range.
 * 
 * @param length    the number of characters
 * @return          a random string
 */
QString Random::string(int length)
{
    if (length == 0)
        return QString();
    static const char digits[33] = "0123456789abcdefghijklmnopqrstuv";

    QString result;
    QByteArray bytes(data(length).leftJustified(length, 0, true));
    for (const char *b = bytes.constData(), *endB = b + length; b != endB; ++b) {
        result.append(digits[((quint8) (*b)) >> 3]);
    }
    return result;
}

/**
 * Creates an identity calibration.
 */
Calibration::Calibration() : coefficients(2), type(Identity), defaultRootChoice(BestInRange)
{
    coefficients[0] = 0.0;
    coefficients[1] = 1.0;
}

Calibration::Calibration(const Calibration &other) = default;

Calibration &Calibration::operator=(const Calibration &other) = default;

Calibration::Calibration(Calibration &&other) = default;

Calibration &Calibration::operator=(Calibration &&other) = default;

/**
 * Creates a calibration from the given coefficients.
 * @param setCoefficients the coefficients
 */
Calibration::Calibration(QList<double> setCoefficients) : coefficients(
        QVector<double>::fromList(setCoefficients)), defaultRootChoice(BestInRange)
{
    setType();
}

/**
 * Creates a calibration from the given coefficients.
 * @param setCoefficients the coefficients
 */
Calibration::Calibration(QVector<double> setCoefficients) : coefficients(setCoefficients),
                                                            defaultRootChoice(BestInRange)
{
    setType();
}


bool Calibration::equals(const Calibration &other) const
{
    if (type != other.type) return false;
    switch (type) {
    case Invalid:
    case Identity:
        return true;
    case ThirdOrder:
        if (coefficients.at(3) != other.coefficients.at(3))
            return false;
    case SecondOrder:
        if (coefficients.at(2) != other.coefficients.at(2))
            return false;
    case FirstOrder:
        if (coefficients.at(1) != other.coefficients.at(1))
            return false;
    case Constant:
    case Offset:
        return coefficients.at(0) == other.coefficients.at(0);
    case NthOrder: {
        int tcmp = coefficients.size();
        for (tcmp--; tcmp >= 0 && coefficients.at(tcmp) == 0.0; tcmp--) { }
        int ocmp = other.coefficients.size();
        for (ocmp--; tcmp >= 0 && other.coefficients.at(ocmp) == 0.0; tcmp--) { }
        if (tcmp != ocmp)
            return false;
        for (int i = 0; i <= tcmp; i++) {
            if (coefficients.at(i) != other.coefficients.at(i))
                return false;
        }
        return true;
    }
    }
    Q_ASSERT(false);
    return false;
}

void Calibration::setType()
{
    int i = coefficients.size();
    if (i < 1) {
        type = Invalid;
        return;
    }
    if (i == 1) {
        type = Constant;
        return;
    }

    for (i--; i >= 0 && coefficients.at(i) == 0.0; i--) { }

    if (i < 1) {
        type = Constant;
    } else if (i == 1) {
        if (coefficients.at(0) == 0.0 && coefficients.at(1) == 1.0)
            type = Identity;
        else if (coefficients.at(1) == 1.0)
            type = Offset;
        else
            type = FirstOrder;
    } else if (i == 2) {
        type = SecondOrder;
    } else if (i == 3) {
        type = ThirdOrder;
    } else {
        type = NthOrder;
    }
}


/**
 * Apply the calibration to an input value.  This preserves undefined values,
 * so passing an undefined value always results in an undefined value out.
 * 
 * @param input     the input value
 * @return          the calibrated output value
 */
double Calibration::apply(double input) const
{
    if (!FP::defined(input)) return input;
    switch (type) {
    case Invalid:
        return FP::undefined();
    case Constant:
        return coefficients.at(0);
    case Identity:
        return input;
    case Offset:
        return input + coefficients.at(0);
    case FirstOrder:
        return coefficients.at(0) + input * coefficients.at(1);
    case SecondOrder:
        return coefficients.at(0) + input * (coefficients.at(1) + input * coefficients.at(2));
    case ThirdOrder:
        return coefficients.at(0) +
                input *
                        (coefficients.at(1) +
                                input * (coefficients.at(2) + input * coefficients.at(3)));
    case NthOrder: {
        double out = coefficients.at(0);
        double m = input;
        for (int i = 1, max = coefficients.size(); i < max; i++) {
            out += m * coefficients.at(i);
            m *= input;
        }
        return out;
    }
    }
    Q_ASSERT(false);
    return FP::undefined();
}

static inline double pickBetterRoot(double min,
                                    double max,
                                    double distanceBase,
                                    double picked,
                                    double r)
{
    bool dMin = FP::defined(min);
    bool dMax = FP::defined(max);
    bool dDistance = FP::defined(distanceBase);
    if (dMin && picked < min) {
        if (r >= min && (!dMax || r <= max))
            return r;
        if (dDistance && fabs(picked - distanceBase) > fabs(r - distanceBase))
            return r;
    }

    if (dMax && picked > max) {
        if (r <= max && (!dMin || r >= min))
            return r;
        if (dDistance && fabs(picked - distanceBase) > fabs(r - distanceBase))
            return r;
    }

    if ((dMin && r < min) || (dMax && r > max)) {
        if ((!dMin || picked >= min) && (!dMax || picked <= max))
            return picked;
    }

    if (dDistance && fabs(picked - distanceBase) > fabs(r - distanceBase))
        return r;

    return picked;
}

static double pickBestRoot(double min,
                           double max,
                           double r1,
                           double r2,
                           double r3 = FP::undefined())
{
    double distanceBase;
    if (FP::defined(min) && FP::defined(max))
        distanceBase = (min + max) * 0.5;
    else if (FP::defined(min))
        distanceBase = min;
    else if (FP::defined(max))
        distanceBase = max;
    else
        distanceBase = FP::undefined();

    /* Shift out any undefined ones, and take care of the trivial cases */
    if (!FP::defined(r1)) {
        if (!FP::defined(r2)) {
            return r3;
        } else {
            if (!FP::defined(r3))
                return r2;
            r1 = r2;
            r2 = r3;
            r3 = FP::undefined();
        }
    } else if (!FP::defined(r2)) {
        if (!FP::defined(r3))
            return r1;
        r2 = r3;
        r3 = FP::undefined();
    }

    double picked = pickBetterRoot(min, max, distanceBase, r1, r2);
    if (!FP::defined(r3))
        return picked;
    return pickBetterRoot(min, max, distanceBase, picked, r3);
}

/**
 * Undo the calibration on an input.  This preserves undefined values, so 
 * passing an undefined value always results in an undefined value out.  Only
 * first, second and third order calibrations can be reversed analytically.  
 * That is, reversing any higher order calibration will be done with Newton's
 * method and so will be approximate and may not converge to the desired
 * root, if more than one exists.  In the case of imaginary results the 
 * selection is only between the real roots, so the choice may be ignored if it 
 * would select an imaginary root.  If the second order has no real roots, 
 * then an undefined value is returned.
 * 
 * @param output        the value to reverse the calibration on
 * @param rootChoice    the root to chose for second order calibrations
 * @param min           the minimum desired input, or FP::undefined() for none
 * @param max           the maximum desired input, or FP::undefined() for none
 * @return              the un-calibrated value
 */
double Calibration::inverse(double output, RootChoice rootChoice, double min, double max) const
{
    if (!FP::defined(output)) return output;
    if (FP::defined(min) && FP::defined(max) && max < min) {
        double t = max;
        max = min;
        min = t;
    }

    switch (type) {
    case Invalid:
    case Constant:
        return FP::undefined();

    case Identity:
        return output;

    case Offset:
        return output - coefficients.at(0);
    case FirstOrder:
        return (output - coefficients.at(0)) / coefficients.at(1);

    case SecondOrder: {
        double d = coefficients.at(1) * coefficients.at(1) -
                4.0 * coefficients.at(2) * (coefficients.at(0) - output);
        if (d < 0.0) return FP::undefined();
        d = sqrt(d);
        if (rootChoice == AlwaysNegative)
            return (-coefficients.at(1) - d) / (2.0 * coefficients.at(2));
        if (rootChoice == AlwaysPositive || rootChoice == AlwaysThirdX1)
            return (-coefficients.at(1) + d) / (2.0 * coefficients.at(2));

        double rn = (-coefficients.at(1) - d) / (2.0 * coefficients.at(2));
        double rp = (-coefficients.at(1) + d) / (2.0 * coefficients.at(2));
        if (rootChoice == PreferNegative &&
                (!FP::defined(min) || rn >= min) &&
                (!FP::defined(max) || rn <= max))
            return rn;
        if ((rootChoice == PreferPositive || rootChoice == PreferThirdX1) &&
                (!FP::defined(min) || rp >= min) &&
                (!FP::defined(max) || rp <= max))
            return rp;
        return pickBestRoot(min, max, rn, rp);
    }

    case ThirdOrder: {
        static const double epsilon0 = 1E-8;

        std::complex<double> a(coefficients.at(3), 0.0);
        std::complex<double> b(coefficients.at(2), 0.0);
        std::complex<double> c(coefficients.at(1), 0.0);
        std::complex<double> d(coefficients.at(0) - output, 0.0);
        std::complex<double> i1 = 2.0 * b * b * b - 9.0 * a * b * c + 27.0 * a * a * d;
        std::complex<double> i2 = b * b - 3.0 * a * c;
        std::complex<double> Q = sqrt(i1 * i1 - 4.0 * i2 * i2 * i2);
        std::complex<double> C =
                pow(0.5 * (Q + 2.0 * b * b * b - 9.0 * a * b * c + 27.0 * a * a * d), 1.0 / 3.0);
        if (std::abs(C) == 0.0)
            return FP::undefined();

        std::complex<double> x1 = -b / (3.0 * a) - C / (3.0 * a) - i2 / (3.0 * a * C);
        std::complex<double> i3(1.0, sqrt(3.0));
        std::complex<double> i4(1.0, -sqrt(3.0));
        std::complex<double> x2 = -b / (3.0 * a) + (C * i3) / (6.0 * a) + (i4 * i2) / (6.0 * a * C);
        std::complex<double> x3 = -b / (3.0 * a) + (C * i4) / (6.0 * a) + (i3 * i2) / (6.0 * a * C);

        if (fabs(std::imag(x1)) > epsilon0) {
            if (fabs(std::imag(x2)) > epsilon0) {
                if (fabs(std::imag(x3)) > epsilon0)
                    return FP::undefined();
                return real(x3);
            } else if (fabs(std::imag(x3)) > epsilon0) {
                return std::real(x2);
            }

            double r1 = real(x2);
            if (rootChoice == AlwaysPositive)
                return r1;
            double r2 = real(x3);
            if (rootChoice == AlwaysNegative)
                return r2;

            if (rootChoice == PreferPositive &&
                    (!FP::defined(min) || r1 >= min) &&
                    (!FP::defined(max) || r1 <= max))
                return r1;
            if (rootChoice == PreferNegative &&
                    (!FP::defined(min) || r2 >= min) &&
                    (!FP::defined(max) || r2 <= max))
                return r2;
            return pickBestRoot(min, max, r1, r2);
        } else if (fabs(imag(x2)) > epsilon0) {
            if (fabs(imag(x3)) > epsilon0)
                return real(x1);

            double r1 = real(x1);
            if (rootChoice == AlwaysThirdX1)
                return r1;
            double r2 = real(x3);
            if (rootChoice == AlwaysNegative)
                return r2;

            if (rootChoice == PreferNegative &&
                    (!FP::defined(min) || r2 >= min) &&
                    (!FP::defined(max) || r2 <= max))
                return r2;
            if (rootChoice == PreferThirdX1 &&
                    (!FP::defined(min) || r1 >= min) &&
                    (!FP::defined(max) || r1 <= max))
                return r1;
            return pickBestRoot(min, max, r1, r2);
        } else if (fabs(imag(x3)) > epsilon0) {
            double r1 = real(x1);
            if (rootChoice == AlwaysThirdX1)
                return r1;
            double r2 = real(x2);
            if (rootChoice == AlwaysPositive)
                return r2;

            if (rootChoice == PreferThirdX1 &&
                    (!FP::defined(min) || r1 >= min) &&
                    (!FP::defined(max) || r1 <= max))
                return r1;
            if (rootChoice == PreferPositive &&
                    (!FP::defined(min) || r2 >= min) &&
                    (!FP::defined(max) || r2 <= max))
                return r2;
            return pickBestRoot(min, max, r1, r2);
        }

        double r1 = real(x1);
        if (rootChoice == AlwaysThirdX1)
            return r1;
        double r2 = real(x2);
        if (rootChoice == AlwaysPositive)
            return r2;
        double r3 = real(x3);
        if (rootChoice == AlwaysNegative)
            return r3;

        if (rootChoice == PreferThirdX1 &&
                (!FP::defined(min) || r1 >= min) &&
                (!FP::defined(max) || r1 <= max))
            return r1;
        if (rootChoice == PreferPositive &&
                (!FP::defined(min) || r2 >= min) &&
                (!FP::defined(max) || r2 <= max))
            return r2;
        if (rootChoice == PreferNegative &&
                (!FP::defined(min) || r3 >= min) &&
                (!FP::defined(max) || r3 <= max))
            return r3;
        return pickBestRoot(min, max, r1, r2, r3);
    }

    case NthOrder: {
        static const int maxItter = 500;
        static const double epsilon0 = 1E-9;

        double x0;
        if (FP::defined(min) && FP::defined(max))
            x0 = (min + max) * 0.5;
        else if (FP::defined(min))
            x0 = min;
        else if (FP::defined(max))
            x0 = max;
        else
            x0 = 0.0;

        int itter;
        for (itter = 0; itter < maxItter; itter++) {
            double dYdX = coefficients.at(1);
            double x = 1.0;
            for (int i = 1, max = coefficients.size(); i < max; i++) {
                dYdX += (double) i * x * coefficients.at(i);
                x *= x0;
            }
            double y = coefficients.at(0);
            x = x0;
            for (int i = 1, max = coefficients.size(); i < max; i++) {
                y += x * coefficients.at(i);
                x *= x0;
            }

            if (dYdX == 0.0 || fabs(dYdX) < epsilon0)
                break;

            double eeps = fabs(y) * epsilon0;
            if (eeps <= epsilon0)
                eeps = epsilon0;
            double dy = output - y;
            if (fabs(dy) <= eeps)
                break;

            x0 += dy / dYdX;
        }

        double result = apply(x0);
        double eeps = (fabs(result) + fabs(output)) * epsilon0 * 2.0;
        if (eeps <= epsilon0)
            eeps = epsilon0;
        if (fabs(output - result) > eeps)
            return FP::undefined();

        return x0;
    }
    }
    Q_ASSERT(false);
    return FP::undefined();
}

/**
 * Undo the calibration on an input.  Uses the default root choice as set by
 * setDefaultRootChoice(RootChoice). 
 * 
 * @param output        the value to reverse the calibration on
 * @param rootChoice    the root to chose for second order calibrations
 * @return              the un-calibrated value
 * @see inverse(double, RootChoice)
 * @see setDefaultRootChoice(RootChoice)
 */
double Calibration::inverse(double output, double min, double max) const
{
    return inverse(output, defaultRootChoice, min, max);
}

/**
 * Set the default root choice for calibration reversal.
 * 
 * @param rootChoice the root choice
 */
void Calibration::setDefaultRootChoice(RootChoice rootChoice)
{ defaultRootChoice = rootChoice; }

/**
 * Get the default root choice for calibration reversal.
 * 
 * @return  the choice of root
 */
Calibration::RootChoice Calibration::getDefaultRootChoice() const
{ return defaultRootChoice; }

/**
 * Returns the number of coefficients in the calibration.
 * @return  the number of coefficients
 */
int Calibration::size() const
{
    return coefficients.size();
}

/**
 * Get the Nth coefficient of the calibration.  Accessing coefficients past
 * the last one returns 0;
 * @param n     the coefficient to access
 * @return      the value of the Nth coefficient
 */
double Calibration::get(int n) const
{
    if (n >= coefficients.size()) return 0.0;
    return coefficients.at(n);
}

/**
 * Set the Nth coefficient.  This will fill any undefined preceding coefficients
 * with zero.  Setting a coefficient to undefined removes it and any
 * coefficients after it.
 * 
 * @param n     the coefficient to set
 * @param c     the value of the coefficient
 */
void Calibration::set(int n, double c)
{
    if (!FP::defined(c)) {
        int max = coefficients.size();
        if (n >= max) return;
        coefficients.erase(coefficients.begin() + n, coefficients.end());
        setType();
        return;
    }

    if (n >= coefficients.size()) {
        coefficients.reserve(n + 1);
        for (int i = coefficients.size(); i < n; i++) {
            coefficients << 0.0;
        }
        coefficients << c;
    } else {
        coefficients[n] = c;
    }
    setType();
}

/**
 * Add the given coefficient to the end of the polynomial.  Undefined values
 * are ignored.
 * 
 * @param c     the coefficient to add
 */
void Calibration::append(double c)
{
    if (!FP::defined(c)) return;
    coefficients << c;
    setType();
}

/**
 * Remove the last coefficient of the calibration.
 */
void Calibration::removeLast()
{
    if (!coefficients.empty())
        coefficients.pop_back();
    setType();
}

/**
 * Delete all coefficients.
 */
void Calibration::clear()
{
    coefficients.clear();
    setType();
}

/**
 * Set to identity.
 */
void Calibration::toIdentity()
{
    coefficients.clear();
    coefficients.reserve(2);
    coefficients << 0.0 << 1.0;
    setType();
}

/**
 * Test if the calibration is the identity.
 * 
 * @return true if this calibration has no effect
 */
bool Calibration::isIdentity() const
{
    return (type == Identity);
}

/**
 * Test if the calibration is valid.
 * 
 * @return true if the calibration is valid
 */
bool Calibration::isValid() const
{
    return (type != Invalid);
}

QDataStream &operator<<(QDataStream &stream, const Calibration &cal)
{
    stream << (quint8) cal.type;
    switch (cal.type) {
    case Calibration::Invalid:
    case Calibration::Identity:
        break;
    case Calibration::Constant:
    case Calibration::Offset:
        stream << cal.coefficients.at(0);
        break;
    case Calibration::FirstOrder:
        stream << cal.coefficients.at(0) << cal.coefficients.at(1);
        break;
    case Calibration::SecondOrder:
        stream << cal.coefficients.at(0) << cal.coefficients.at(1) << cal.coefficients.at(2);
        break;
    case Calibration::ThirdOrder:
        stream << cal.coefficients.at(0) << cal.coefficients.at(1) << cal.coefficients.at(2)
               << cal.coefficients.at(3);
        break;
    case Calibration::NthOrder:
        stream << cal.coefficients;
        break;
    default:
        Q_ASSERT(false);
    }
    return stream;
}

QDataStream &operator>>(QDataStream &stream, Calibration &cal)
{
    quint8 type;
    stream >> type;
    cal.type = (Calibration::Type) type;

    cal.coefficients.clear();
    double d;
    switch (cal.type) {
    case Calibration::Invalid:
        break;
    case Calibration::Identity:
        cal.coefficients.reserve(2);
        cal.coefficients << 0.0 << 1.0;
        break;
    case Calibration::Constant:
        stream >> d;
        cal.coefficients << d;
        break;
    case Calibration::Offset:
        cal.coefficients.reserve(2);
        stream >> d;
        cal.coefficients << d << 1.0;
        break;
    case Calibration::FirstOrder:
        cal.coefficients.reserve(2);
        stream >> d;
        cal.coefficients << d;
        stream >> d;
        cal.coefficients << d;
        break;
    case Calibration::SecondOrder:
        cal.coefficients.reserve(3);
        stream >> d;
        cal.coefficients << d;
        stream >> d;
        cal.coefficients << d;
        stream >> d;
        cal.coefficients << d;
        break;
    case Calibration::ThirdOrder:
        cal.coefficients.reserve(4);
        stream >> d;
        cal.coefficients << d;
        stream >> d;
        cal.coefficients << d;
        stream >> d;
        cal.coefficients << d;
        stream >> d;
        cal.coefficients << d;
        break;
    case Calibration::NthOrder:
        stream >> cal.coefficients;
        cal.setType();
        break;
    default:
        Q_ASSERT(false);
    }
    return stream;
}

QDebug operator<<(QDebug stream, const Calibration &cal)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "Calibration(";
    switch (cal.type) {
    case Calibration::Invalid:
        stream << "Invalid)";
        break;
    case Calibration::Identity:
        stream << "Identity)";
        break;
    case Calibration::Constant:
        stream << "Constant:" << cal.coefficients.at(0) << ")";
        break;
    case Calibration::Offset:
        stream << "Offset:" << cal.coefficients.at(0) << ")";
        break;
    case Calibration::FirstOrder:
        stream << "FirstOrder:b=" << cal.coefficients.at(0) << ",m=" << cal.coefficients.at(1)
               << ")";
        break;
    case Calibration::SecondOrder:
        stream << "SecondOrder:x0=" << cal.coefficients.at(0) << ",x1=" << cal.coefficients.at(1)
               << ",x2=" << cal.coefficients.at(2) << ")";
        break;
    case Calibration::ThirdOrder:
        stream << "ThirdOrder:x0=" << cal.coefficients.at(0) << ",x1=" << cal.coefficients.at(1)
               << ",x2=" << cal.coefficients.at(2) << ",x3=" << cal.coefficients.at(3) << ")";
        break;
    case Calibration::NthOrder:
        stream << "NthOrder:";
        for (int i = 0, max = cal.coefficients.size(); i < max; i++) {
            if (i != 0) stream << ",";
            stream << "x" << i << "=" << cal.coefficients.at(i);
        }
        stream << ")";
        break;
    default:
        Q_ASSERT(false);
    }
    return stream;
}


/**
 * Constructs a decimal format with one integer digit and three decimal 
 * digits.  The values for the scientific parameters are two digits, an
 * upper case 'E' as the exponent string and to show the exponent sign.
 * The hex case is set to uppercase.
 */
NumberFormat::NumberFormat() : mode(Decimal),
                               integerDigits(1),
                               decimalDigits(3),
                               exponentDigits(2),
                               showPositiveSign(false),
                               showPositiveExponent(true),
                               exponentSeparator(QLatin1Char('E')),
                               uppercaseHex(true),
                               basePrefix()
{ }

NumberFormat::NumberFormat(const NumberFormat &other) = default;

NumberFormat &NumberFormat::operator=(const NumberFormat &other) = default;

NumberFormat::NumberFormat(NumberFormat &&other) = default;

NumberFormat &NumberFormat::operator=(NumberFormat &&other) = default;

/**
 * Create a new format for decimal output with the given number of integer and
 * decimal places.  All other parameters are set like the default constructor.
 * 
 * @param integerWidth  the number of integer digits
 * @param decimalWidth  the number of digits after the decimal
 */
NumberFormat::NumberFormat(int integerWidth, int decimalWidth) : mode(Decimal),
                                                                 integerDigits(integerWidth),
                                                                 decimalDigits(decimalWidth),
                                                                 exponentDigits(2),
                                                                 showPositiveSign(false),
                                                                 showPositiveExponent(true),
                                                                 exponentSeparator(
                                                                         QLatin1Char('E')),
                                                                 uppercaseHex(true),
                                                                 basePrefix()
{ }

/**
 * Create a new format for scientific output with the given number of
 * decimal places.  All other parameters are set like the default constructor.
 * 
 * @param exponentFractional    the number of decimal places
 */
NumberFormat::NumberFormat(int exponentFractional) : mode(Scientific),
                                                     integerDigits(1),
                                                     decimalDigits(3),
                                                     exponentDigits(exponentFractional),
                                                     showPositiveSign(false),
                                                     showPositiveExponent(true),
                                                     exponentSeparator(QLatin1Char('E')),
                                                     uppercaseHex(true),
                                                     basePrefix()
{ }

/**
 * Constructs a format from the given string.  This string can be a set of
 * numbers in the desired form (spreadsheet-like), a printf type format,
 * or a CPD2 SFMT format.  In cases where the format specifies something
 * that is not supported, that information is coerced to something that
 * is supported.  Any unspecified or unparsable parameters are set like
 * the default constructor.
 * 
 * @param format    the format to parse
 */
NumberFormat::NumberFormat(const QString &format) : mode(Decimal),
                                                    integerDigits(1),
                                                    decimalDigits(3),
                                                    exponentDigits(2),
                                                    showPositiveSign(false),
                                                    showPositiveExponent(true),
                                                    exponentSeparator(QLatin1Char('E')),
                                                    uppercaseHex(true)
{

    QString f(format.trimmed());
    if (f.isEmpty())
        return;

    if (f.startsWith('%')) {
        QRegExp matched("%([#0+ 'I-]*)(\\d*)(?:\\.(\\d+))?(?:(?:hh?)|(?:ll?)|[Lqjzt])?(\\D)");
        if (!matched.exactMatch(f))
            return;

        showPositiveSign = false;
        if (matched.pos(1) > -1 && matched.cap(1).length() > 0) {
            QString params(matched.cap(1));
            if (params.contains('+'))
                showPositiveSign = true;
        }

        int totalLength = -1;
        if (matched.pos(2) > -1 && matched.cap(2).length() > 0) {
            bool ok;
            totalLength = matched.cap(2).toInt(&ok);
            if (totalLength <= 0 || !ok)
                totalLength = -1;
        }

        int decimalLength = -1;
        if (matched.pos(3) > -1 && matched.cap(3).length() > 0) {
            bool ok;
            decimalLength = matched.cap(3).toInt(&ok);
            if (decimalLength < 0 || !ok)
                decimalLength = -1;
        }

        if (matched.cap(4) == "x" || matched.cap(4) == "X") {
            mode = Hex;
            integerDigits = totalLength;
            uppercaseHex = (matched.cap(4) == "X");
        } else if (matched.cap(4) == "o") {
            mode = Octal;
            integerDigits = totalLength;
        } else if (matched.cap(4) == "e" || matched.cap(4) == "E") {
            mode = Scientific;
            exponentSeparator = matched.cap(4);
            integerDigits = 1;
            exponentDigits = 2;
            showPositiveExponent = true;
            if (decimalLength >= 0) {
                decimalDigits = decimalLength;
            } else {
                decimalDigits = 3;
            }
        } else {
            mode = Decimal;
            if (totalLength > 0) {
                if (decimalLength >= 0) {
                    decimalDigits = decimalLength;
                } else {
                    decimalDigits = 6;
                }
                integerDigits = totalLength;
                if (showPositiveSign)
                    integerDigits--;
                if (decimalDigits > 0)
                    integerDigits -= 1 + decimalDigits;
                if (integerDigits <= 0)
                    integerDigits = 1;
            } else if (decimalLength > 0) {
                integerDigits = 1;
                decimalDigits = decimalLength;
            } else {
                integerDigits = 1;
                decimalDigits = 6;
            }
        }
    } else if (f.startsWith('*')) {
        if (f.endsWith('e') || f.endsWith('E')) {
            mode = Scientific;
            exponentSeparator = f.right(1);

            QRegExp matched
                    ("\\*([~ +^0-]*)(\\d*)(?:\\.(\\d*)(?:\\.([~ +-^0]*)(\\d*)(?:\\.(\\d*))?)?)?[eE]");
            if (!matched.exactMatch(f))
                return;

            showPositiveSign = false;
            if (matched.pos(1) > -1 && matched.cap(1).length() > 0) {
                QString params(matched.cap(1));
                if (params.contains('+'))
                    showPositiveSign = true;
            }

            showPositiveExponent = true;
            if (matched.pos(4) > -1 && matched.cap(4).length() > 0) {
                QString params(matched.cap(4));
                if (params.contains('+'))
                    showPositiveExponent = false;
            }

            if (matched.pos(2) > -1 && matched.cap(2).length() > 0) {
                bool ok;
                integerDigits = matched.cap(2).toInt(&ok);
                if (integerDigits <= 0 || !ok)
                    integerDigits = 1;
            }

            decimalDigits = 6;
            if (matched.pos(3) > -1 && matched.cap(3).length() > 0) {
                bool ok;
                decimalDigits = matched.cap(3).toInt(&ok);
                if (decimalDigits < 0 || !ok)
                    decimalDigits = 6;
            }

            exponentDigits = 2;
            if (matched.pos(5) > -1 && matched.cap(5).length() > 0) {
                bool ok;
                exponentDigits = matched.cap(5).toInt(&ok);
                if (exponentDigits <= 0 || !ok)
                    exponentDigits = 2;
            }
        } else {
            mode = Decimal;
            QRegExp matched("\\*([ +^@0~-]*)(\\d*)(?:\\.(\\d+))?[fF]");
            if (!matched.exactMatch(f))
                return;

            showPositiveSign = false;
            if (matched.pos(1) > -1 && matched.cap(1).length() > 0) {
                QString params(matched.cap(1));
                if (params.contains('+'))
                    showPositiveSign = true;
            }

            if (matched.pos(2) > -1 && matched.cap(2).length() > 0) {
                bool ok;
                integerDigits = matched.cap(2).toInt(&ok);
                if (integerDigits <= 0 || !ok)
                    integerDigits = 1;
            }

            if (matched.pos(3) > -1 && matched.cap(3).length() > 0) {
                bool ok;
                decimalDigits = matched.cap(3).toInt(&ok);
                if (decimalDigits < 0 || !ok)
                    decimalDigits = 6;
            }
        }
    } else {
        showPositiveSign = false;
        int addIntegerDigits = 0;
        if (f.startsWith('+')) {
            showPositiveSign = true;
            f = f.mid(1);
            addIntegerDigits = 1;
        } else if (f.startsWith('-')) {
            f = f.mid(1);
            addIntegerDigits = 1;
        }
        if (f.isEmpty())
            return;

        if (f.startsWith("0x", Qt::CaseInsensitive)) {
            basePrefix = f.mid(0, 2);
            f = f.mid(2);
            if (f.isEmpty())
                return;
            mode = Hex;
            integerDigits = f.length() + addIntegerDigits;
            if (QRegExp("[A-F]").indexIn(f) >= 0) {
                uppercaseHex = true;
            } else if (QRegExp("[a-f]").indexIn(f) >= 0) {
                uppercaseHex = false;
            } else {
                uppercaseHex = true;
            }
            return;
        } else if (f.startsWith("0o", Qt::CaseInsensitive)) {
            basePrefix = f.mid(0, 2);
            f = f.mid(2);
            if (f.isEmpty())
                return;
            mode = Octal;
            integerDigits = f.length() + addIntegerDigits;
            return;
        }

        int idx = f.indexOf('e', 1, Qt::CaseInsensitive);
        if (idx == -1) {
            QRegExp hexCheck("\\d*([a-fA-F]+)[\\da-fA-F]*");
            if (hexCheck.exactMatch(f)) {
                mode = Hex;
                integerDigits = f.length() + addIntegerDigits;
                uppercaseHex = true;
                if (hexCheck.pos(1) > -1 && hexCheck.cap(1).length() > 0) {
                    if (hexCheck.cap(1).at(0).isUpper()) {
                        uppercaseHex = true;
                    } else {
                        uppercaseHex = false;
                    }
                }
                return;
            }

            mode = Decimal;
            idx = f.indexOf('.');
            if (idx == -1) {
                integerDigits = f.length() + addIntegerDigits;
                decimalDigits = 0;
            } else {
                integerDigits = idx + addIntegerDigits;
                if (integerDigits < 1)
                    integerDigits = 1;
                decimalDigits = f.length() - idx - 1;
            }
        } else {
            mode = Scientific;
            exponentSeparator = QString(f.at(idx));

            showPositiveExponent = true;
            if (idx + 1 < f.length()) {
                if (f.at(idx + 1) == '+' || f.at(idx + 1) == '-') {
                    showPositiveExponent = true;
                    exponentDigits = f.length() - idx - 2;
                } else {
                    showPositiveExponent = false;
                    exponentDigits = f.length() - idx - 1;
                }
                if (exponentDigits < 1)
                    exponentDigits = 1;
            } else {
                exponentDigits = 1;
            }
            f = f.mid(0, idx);

            idx = f.indexOf('.');
            if (idx == -1) {
                integerDigits = f.length() + addIntegerDigits;
            } else {
                integerDigits = idx + addIntegerDigits;
                if (integerDigits < 1)
                    integerDigits = 1;
                decimalDigits = f.length() - idx - 1;
            }
        }
    }
}

/**
 * Apply this format to the given value.  The result will be padded to the
 * required length.  If the padding is given as spaces, the sign will appear
 * on the first digit, otherwise it will appear  before any of the padding.
 * 
 * @param d             the value
 * @param leadingPad    the character to pad with
 * @return              the formattted string
 */
QString NumberFormat::apply(double d, const QChar &leadingPad) const
{
    if (!FP::defined(d))
        return mvc();

    switch (mode) {
    case Decimal: {
        QString result;
        if (d < 0.0) {
            d = -d;
            result.append('-');
        } else if (showPositiveSign) {
            result.append('+');
        }

        std::int_fast64_t shift = pow10(decimalDigits);
        std::int_fast64_t shifted = std::lround(d * shift);
        if (shift < 0 || shifted < 0) {
            /* Overflow, so just accept whatever */
            auto temp = QString::number(d, 'f', decimalDigits);
            auto expected = decimalDigits + integerDigits;
            if (decimalDigits > 0)
                expected++;
            if (temp.size() < expected && !leadingPad.isNull()) {
                if (leadingPad.isSpace()) {
                    result = QString(leadingPad).repeated(expected - temp.size()) + result + temp;
                } else {
                    result += temp.rightJustified(expected, leadingPad);
                }
            } else {
                result += temp;
            }
            return result;
        }
        std::int_fast64_t icmp = shifted / shift;
        std::int_fast64_t fcmp = shifted - (icmp * shift);

        if (leadingPad.isSpace()) {
            result.append(QString::number(icmp));
            result = result.rightJustified(integerDigits, leadingPad);
        } else if (leadingPad.isNull()) {
            result.append(QString::number(icmp));
        } else {
            QString nadd = QString::number(icmp);
            int toPad = integerDigits - result.length();
            if (toPad > 0) {
                nadd = nadd.rightJustified(toPad, leadingPad);
            }
            result.append(std::move(nadd));
        }

        if (decimalDigits > 0) {
            result.append(QLatin1Char('.'));

            for (int i = 0; i < decimalDigits; i++) {
                shift /= 10;
                std::int_fast64_t digit = (fcmp / shift) % 10;
                if (digit < 0 || digit > 9)
                    digit = 0;
                result.append(QLatin1Char('0' + static_cast<char>(digit)));

                fcmp -= digit * shift;
            }
        }
        return result;
    }

    case Scientific: {
        return FP::scientificFormat(d, decimalDigits, exponentDigits, exponentSeparator,
                                    showPositiveSign ? QString(QLatin1Char('+')) : QString(),
                                    showPositiveExponent ? QString(QLatin1Char('+')) : QString());
    }

    case Hex:
    case Octal:
        return apply(static_cast<qint64>(std::lround(d)), leadingPad);
    }
    Q_ASSERT(false);
    return QString();
}

QString NumberFormat::toSuperscript(const QString &input)
{
    QString result;
    result.reserve(input.size());
    for (int i = 0, max = input.size(); i < max; i++) {
        switch (input.at(i).unicode()) {
        case (ushort) '0':
            result.append(QChar(0x2070));
            break;
        case (ushort) '1':
            result.append(QChar(0x00B9));
            break;
        case (ushort) '2':
            result.append(QChar(0x00B2));
            break;
        case (ushort) '3':
            result.append(QChar(0x00B3));
            break;
        case (ushort) '4':
            result.append(QChar(0x2074));
            break;
        case (ushort) '5':
            result.append(QChar(0x2075));
            break;
        case (ushort) '6':
            result.append(QChar(0x2076));
            break;
        case (ushort) '7':
            result.append(QChar(0x2077));
            break;
        case (ushort) '8':
            result.append(QChar(0x2078));
            break;
        case (ushort) '9':
            result.append(QChar(0x2079));
            break;
        case (ushort) '+':
            result.append(QChar(0x207A));
            break;
        case (ushort) '-':
            result.append(QChar(0x207B));
            break;
        case (ushort) '=':
            result.append(QChar(0x207C));
            break;
        case (ushort) '(':
            result.append(QChar(0x207D));
            break;
        case (ushort) ')':
            result.append(QChar(0x207E));
            break;
        case (ushort) 'i':
            result.append(QChar(0x2071));
            //result.append(QChar(0x0365));
            break;
        case (ushort) 'n':
            result.append(QChar(0x207F));
            break;


        case (ushort) 'h':
            result.append(QChar(0x02B0));
            //result.append(QChar(0x036A));
            break;
        case (ushort) 'j':
            result.append(QChar(0x02B2));
            break;
        case (ushort) 'r':
            result.append(QChar(0x02B3));
            //result.append(QChar(0x036C));
            break;
        case (ushort) 'w':
            result.append(QChar(0x02B7));
            break;
        case (ushort) 'y':
            result.append(QChar(0x02B8));
            break;
        case (ushort) '<':
            result.append(QChar(0x02C2));
            break;
        case (ushort) '>':
            result.append(QChar(0x02C3));
            break;


        case (ushort) 'A':
            result.append(QChar(0x1D2C));
            break;
        case (ushort) 0x00E6:    // AE
            result.append(QChar(0x1D2D));
            break;
        case (ushort) 'B':
            result.append(QChar(0x1D2E));
            break;
        case (ushort) 'D':
            result.append(QChar(0x1D30));
            break;
        case (ushort) 'E':
            result.append(QChar(0x1D31));
            break;
        case (ushort) 'G':
            result.append(QChar(0x1D33));
            break;
        case (ushort) 'H':
            result.append(QChar(0x1D34));
            break;
        case (ushort) 'I':
            result.append(QChar(0x1D35));
            break;
        case (ushort) 'J':
            result.append(QChar(0x1D36));
            break;
        case (ushort) 'K':
            result.append(QChar(0x1D37));
            break;
        case (ushort) 'L':
            result.append(QChar(0x1D38));
            break;
        case (ushort) 'M':
            result.append(QChar(0x1D39));
            break;
        case (ushort) 'N':
            result.append(QChar(0x1D3A));
            break;
        case (ushort) 'O':
            result.append(QChar(0x1D3C));
            break;
        case (ushort) 'P':
            result.append(QChar(0x1D3E));
            break;
        case (ushort) 'R':
            result.append(QChar(0x1D3F));
            break;
        case (ushort) 'T':
            result.append(QChar(0x1D40));
            break;
        case (ushort) 'U':
            result.append(QChar(0x1D41));
            break;
        case (ushort) 'W':
            result.append(QChar(0x1D42));
            break;
        case (ushort) 'a':
            result.append(QChar(0x1D43));
            //result.append(QChar(0x0363));
            break;
        case (ushort) 'b':
            result.append(QChar(0x1D47));
            break;
        case (ushort) 'd':
            result.append(QChar(0x1D48));
            //result.append(QChar(0x0369));
            break;
        case (ushort) 'e':
            result.append(QChar(0x1D49));
            //result.append(QChar(0x0364));
            break;
        case (ushort) 'g':
            result.append(QChar(0x1D4D));
            //result.append(QChar(0x1DA2));
            break;
        case (ushort) '!':
            result.append(QChar(0x1D4E));
            break;
        case (ushort) 'k':
            result.append(QChar(0x1D4F));
            break;
        case (ushort) 'm':
            result.append(QChar(0x1D50));
            //result.append(QChar(0x036B));
            break;
        case (ushort) 'p':
            result.append(QChar(0x1D56));
            break;
        case (ushort) 't':
            result.append(QChar(0x1D57));
            //result.append(QChar(0x036D));
            break;
        case (ushort) 'u':
            result.append(QChar(0x1D58));
            //result.append(QChar(0x0367));
            break;
        case (ushort) 'v':
            result.append(QChar(0x1D5B));
            //result.append(QChar(0x036E));
            break;
        case (ushort) 0x03B2:    // small beta
            result.append(QChar(0x1D5D));
            break;
        case (ushort) 0x0263:    // small gamma
        case (ushort) 0x0264:
        case (ushort) 0x03B3:
            result.append(QChar(0x1D5E));
            break;
        case (ushort) 0x03B4:    // small delta
            result.append(QChar(0x1D5F));
            break;
        case (ushort) 0x03C6:    // small phi
        case (ushort) 0x03A6:    // capital phi
        case (ushort) 0x03D5:
        case (ushort) 0x0278:
            result.append(QChar(0x1D60));
            break;
        case (ushort) 0x03C7:    // small chi
            result.append(QChar(0x1D61));
            break;

        case (ushort) 'f':
            result.append(QChar(0x1DA0));
            break;

        case (ushort) 'o':
            result.append(QChar(0x0366));
            break;
        case (ushort) 'c':
            result.append(QChar(0x0368));
            break;
        case (ushort) 'x':
            result.append(QChar(0x036F));
            break;
        }
    }
    return result;
}

QString NumberFormat::toSubscript(const QString &input)
{
    QString result;
    result.reserve(input.size());
    for (int i = 0, max = input.size(); i < max; i++) {
        switch (input.at(i).unicode()) {
        case (ushort) '0':
            result.append(QChar(0x2080));
            break;
        case (ushort) '1':
            result.append(QChar(0x2081));
            break;
        case (ushort) '2':
            result.append(QChar(0x2082));
            break;
        case (ushort) '3':
            result.append(QChar(0x00B3));
            break;
        case (ushort) '4':
            result.append(QChar(0x2084));
            break;
        case (ushort) '5':
            result.append(QChar(0x2085));
            break;
        case (ushort) '6':
            result.append(QChar(0x2086));
            break;
        case (ushort) '7':
            result.append(QChar(0x2087));
            break;
        case (ushort) '8':
            result.append(QChar(0x2088));
            break;
        case (ushort) '9':
            result.append(QChar(0x2089));
            break;
        case (ushort) '+':
            result.append(QChar(0x208A));
            break;
        case (ushort) '-':
            result.append(QChar(0x208B));
            break;
        case (ushort) '=':
            result.append(QChar(0x208C));
            break;
        case (ushort) '(':
            result.append(QChar(0x208D));
            break;
        case (ushort) ')':
            result.append(QChar(0x208E));
            break;
        case (ushort) 'i':
            result.append(QChar(0x2081));
            //result.append(QChar(0x1D62));
            break;
        case (ushort) 'n':
            result.append(QChar(0x208F));
            break;

        case (ushort) 'a':
            result.append(QChar(0x2090));
            break;
        case (ushort) 'e':
            result.append(QChar(0x2091));
            break;
        case (ushort) 'o':
            result.append(QChar(0x2092));
            break;
        case (ushort) 'x':
            result.append(QChar(0x2093));
            break;

        case (ushort) 'r':
            result.append(QChar(0x1D63));
            break;
        case (ushort) 'u':
            result.append(QChar(0x1D64));
            break;
        case (ushort) 'v':
            result.append(QChar(0x1D65));
            break;
        case (ushort) 0x03B2:    // small beta
            result.append(QChar(0x1D66));
            break;
        case (ushort) 0x0263:    // small gamma
        case (ushort) 0x0264:
        case (ushort) 0x03B3:
            result.append(QChar(0x1D67));
            break;
        case (ushort) 0x03C6:    // small phi
        case (ushort) 0x03A6:    // capital phi
        case (ushort) 0x03D5:
        case (ushort) 0x0278:
            result.append(QChar(0x1D69));
            break;
        case (ushort) 0x03C7:    // small chi
            result.append(QChar(0x1D6A));
            break;
        }
    }
    return result;
}

/**
 * Same as apply( double , const QChar & ) const, but the result is unicode
 * superscript.
 * 
 * @see apply( double , const QChar & ) const
 */
QString NumberFormat::superscript(double d, const QChar &leadingPad) const
{ return toSuperscript(apply(d, leadingPad)); }

/**
 * Same as apply( double , const QChar & ) const, but the result is unicode
 * subscript.
 * 
 * @see apply( double , const QChar & ) const
 */
QString NumberFormat::subscript(double d, const QChar &leadingPad) const
{ return toSubscript(apply(d, leadingPad)); }

static double decimalLimitValue(int integerDigits, int decimalDigits)
{
    if (integerDigits < 1)
        integerDigits = 1;
    return (pow10(integerDigits) - 1.0) + (1.0 - 1.0 / pow10(decimalDigits));
}

/**
 * Apply this format to the given value, clipping it so it does not exceed the
 * formats width.  The result will be padded to the required length.  If the 
 * padding is given as spaces, the sign will appear on the first digit, 
 * otherwise it will appear  before any of the padding.
 * 
 * @param d             the value
 * @param leadingPad    the character to pad with
 * @return              the clipped and formatted string
 */
QString NumberFormat::applyClipped(double d, const QChar &leadingPad) const
{
    if (!FP::defined(d))
        return mvc();
    switch (mode) {
    case Decimal: {
        int positiveDigits = integerDigits;
        if (showPositiveSign)
            positiveDigits--;
        double max = decimalLimitValue(positiveDigits, decimalDigits);
        double min = -decimalLimitValue(integerDigits - 1, decimalDigits);
        if (d > max)
            d = max;
        if (d < min)
            d = min;
        break;
    }

    case Scientific: {
        double max = decimalLimitValue(1, decimalDigits);
        double min = -decimalLimitValue(1, decimalDigits);
        double multiplier = std::pow(10.0, std::pow(10.0, exponentDigits) - 1);
        max *= multiplier;
        min *= multiplier;
        if (d > max)
            d = max;
        if (d < min)
            d = min;
        break;
    }

    case Hex:
    case Octal:
        return applyClipped(static_cast<qint64>(std::lround(d)), leadingPad);
    }
    return apply(d, leadingPad);
}

/**
 * Same as applyClipped(double, const QChar &) but the result is unicode
 * superscript.
 * @see applyClipped(double, const QChar &)
 */
QString NumberFormat::superscriptClipped(double d, const QChar &leadingPad) const
{
    return toSuperscript(applyClipped(d, leadingPad));
}

/**
 * Same as applyClipped(double, const QChar &) but the result is unicode
 * subscript.
 * @see applyClipped(double, const QChar &)
 */
QString NumberFormat::subscriptClipped(double d, const QChar &leadingPad) const
{
    return toSubscript(applyClipped(d, leadingPad));
}

/**
 * Apply this format to the given value.  The result will be padded to the
 * required length.  If the padding is given as spaces, the sign will appear
 * on the first digit, otherwise it will appear  before any of the padding.
 * 
 * @param i             the value
 * @param leadingPad    the character to pad with
 * @return              the formatted string
 */
QString NumberFormat::apply(qint64 i, const QChar &leadingPad) const
{
    if (!INTEGER::defined(i))
        return mvc();

    switch (mode) {
    case Decimal:
    case Hex:
    case Octal: {
        bool isNegative = i < 0;
        if (isNegative)
            i = -i;
        QString result;
        int addDigits;
        if (mode == Decimal) {
            result = QString::number(i, 10);
            if (decimalDigits > 0) {
                result.append('.');
                result.append(QString(QLatin1Char('0')).repeated(decimalDigits));
                addDigits = (integerDigits + decimalDigits + 1) - result.length();
            } else {
                addDigits = integerDigits - result.length();
            }
        } else if (mode == Hex) {
            result = QString::number(i, 16);
            if (uppercaseHex)
                result = result.toUpper();
            else
                result = result.toLower();
            addDigits = integerDigits - result.length();
        } else if (mode == Octal) {
            result = QString::number(i, 8);
            addDigits = integerDigits - result.length();
        } else {
            Q_ASSERT(false);
            return QString();
        }

        if (isNegative || showPositiveSign)
            addDigits--;
        if (leadingPad.isSpace()) {
            if (mode != Decimal)
                result.prepend(basePrefix);
            if (isNegative)
                result.prepend('-');
            else if (showPositiveSign)
                result.prepend('+');
        }
        if (addDigits > 0 && !leadingPad.isNull())
            result.prepend(QString(leadingPad).repeated(addDigits));
        if (!leadingPad.isSpace()) {
            if (mode != Decimal)
                result.prepend(basePrefix);
            if (isNegative)
                result.prepend('-');
            else if (showPositiveSign)
                result.prepend('+');
        }
        return result;
    }

    case Scientific:
        return apply((double) i, leadingPad);
    }
    Q_ASSERT(false);
    return QString();
}

/**
 * Same as apply( qint64, const QChar & ) but the result is unicode superscript.
 * @see apply( qint64, const QChar & ) const
 */
QString NumberFormat::superscript(qint64 i, const QChar &leadingPad) const
{ return toSuperscript(apply(i, leadingPad)); }

/**
 * Same as apply( qint64, const QChar & ) but the result is unicode subscript.
 * @see apply( qint64, const QChar & ) const
 */
QString NumberFormat::subscript(qint64 i, const QChar &leadingPad) const
{ return toSubscript(apply(i, leadingPad)); }

static qint64 integerLimitValue(int integerDigits, int base)
{
    qint64 result = base - 1;
    for (int i = 1; i < integerDigits; i++) {
        result *= base;
        result += base - 1;
    }
    return result;
}

/**
 * Apply this format to the given value, clipping it so it does not exceed the
 * formats width.  The result will be padded to the required length.  If the 
 * padding is given as spaces, the sign will appear on the first digit, 
 * otherwise it will appear  before any of the padding.
 * 
 * @param i             the value
 * @param leadingPad    the character to pad with
 * @return              the clipped and formatted string
 */
QString NumberFormat::applyClipped(qint64 i, const QChar &leadingPad) const
{
    switch (mode) {
    case Decimal:
    case Hex:
    case Octal: {
        int positiveDigits = integerDigits;
        if (showPositiveSign)
            positiveDigits--;
        int base;
        QString pad;
        if (mode == Decimal) {
            base = 10;
            pad = QString(QLatin1Char('9'));
        } else if (mode == Hex) {
            base = 16;
            if (uppercaseHex)
                pad = QString(QLatin1Char('F'));
            else
                pad = QString(QLatin1Char('f'));
        } else if (mode == Octal) {
            base = 8;
            pad = QString(QLatin1Char('7'));
        } else {
            Q_ASSERT(false);
            return QString();
        }

        qint64 max = integerLimitValue(positiveDigits, base);
        if (i > max) {
            QString result;
            if (showPositiveSign)
                result.append('+');
            if (mode != Decimal)
                result.append(basePrefix);
            result.append(pad.repeated(positiveDigits));
            if (decimalDigits > 0 && mode == Decimal) {
                result.append('.');
                result.append(pad.repeated(decimalDigits));
            }
            return result;
        }
        if (i < 0 && integerDigits <= 1) {
            QString result;
            if (mode != Decimal)
                result.append(basePrefix);
            result.append('0');
            return result;
        }

        qint64 min = -integerLimitValue(integerDigits - 1, base);
        if (i < min) {
            QString result(QLatin1Char('-'));
            if (mode != Decimal)
                result.append(basePrefix);
            if (integerDigits > 1)
                result.append(pad.repeated(integerDigits - 1));
            else
                result.append(QLatin1Char('9'));
            if (decimalDigits > 0 && mode == Decimal) {
                result.append('.');
                result.append(pad.repeated(decimalDigits));
            }
            return result;
        }
        break;
    }

    case Scientific:
        return applyClipped((double) i, leadingPad);
    }

    return apply(i, leadingPad);
}

/**
 * Same as applyClipped( qint64, const QChar & ) but the result is unicode
 * superscript.
 * @see applyClipped( qint64, const QChar & ) const
 */
QString NumberFormat::superscriptClipped(qint64 i, const QChar &leadingPad) const
{ return toSuperscript(applyClipped(i, leadingPad)); }

/**
 * Same as applyClipped( qint64, const QChar & ) but the result is unicode
 * subscript.
 * @see applyClipped( qint64, const QChar & ) const
 */
QString NumberFormat::subscriptClipped(qint64 i, const QChar &leadingPad) const
{ return toSubscript(applyClipped(i, leadingPad)); }

/**
 * Get a printf(2) representation of this format.
 * 
 * @param type  the type code, will have the last character converted as needed
 * @return a string format
 */
QString NumberFormat::getFormatPrintf(const QString &type) const
{
    QString result(QLatin1Char('%'));
    if (showPositiveSign)
        result.append('+');
    result.append('0');

    switch (mode) {
    case Decimal: {
        if (decimalDigits > 0) {
            result.append(QString::number(decimalDigits + integerDigits + 1));
            result.append('.');
            result.append(QString::number(decimalDigits));
        } else {
            result.append(QString::number(integerDigits));
            result.append(".0");
        }

        if (type.endsWith('e') || type.endsWith('E')) {
            if (type.length() > 1)
                result.append(type.mid(0, type.length() - 1));
            result.append('f');
        } else if (type.endsWith('o') || type.endsWith('x') || type.endsWith('X')) {
            if (type.length() > 1)
                result.append(type.mid(0, type.length() - 1));
            result.append('d');
        } else {
            result.append(type);
        }
        return result;
    }

    case Scientific: {
        if (decimalDigits > 0) {
            result.append(QString::number(decimalDigits + 6));
            result.append('.');
            result.append(QString::number(decimalDigits));
        } else {
            result.append("4.0");
        }

        if (type.endsWith('f')) {
            if (type.length() > 1)
                result.append(type.mid(0, type.length() - 1));
        } else if (!type.endsWith('o') && !type.endsWith('x') && !type.endsWith('X')) {
            result.append(type);
        }
        if (exponentSeparator == "e")
            result.append('e');
        else
            result.append('E');
        return result;
    }

    case Hex: {
        if (basePrefix == "0x" && !uppercaseHex)
            result.append('#');
        else if (basePrefix == "0X" && uppercaseHex)
            result.append('#');
        result.append(QString::number(integerDigits));
        if (uppercaseHex)
            result.append('X');
        else
            result.append('x');
        return result;
    }

    case Octal: {
        if (basePrefix == "0")
            result.append('#');
        result.append(QString::number(integerDigits));
        result.append('o');
        return result;
    }

    }

    Q_ASSERT(false);
    return QString();
}

/**
 * Get a CPD2 sfmt representation of this format.
 * 
 * @return a string format
 */
QString NumberFormat::getFormatCPD2SFMT() const
{
    QString result(QLatin1Char('*'));
    if (showPositiveSign)
        result.append('+');
    result.append("@0");

    switch (mode) {
    case Decimal: {
        result.append(QString::number(integerDigits));
        result.append('.');
        result.append(QString::number(decimalDigits));
        result.append('f');
        return result;
    }

    case Scientific: {
        result.append("1.");
        result.append(QString::number(decimalDigits));
        result.append(".0");
        result.append(QString::number(exponentDigits));
        if (exponentSeparator == "e")
            result.append('e');
        else
            result.append('E');
        return result;
    }

    default:
        return getFormatPrintf();
    }

    Q_ASSERT(false);
    return QString();
}

/**
 * Get a string that contains a number generated by filling the format
 * with the given character.
 * 
 * @param fill  the character to fill with
 * @return      the filled formatted string
 */
QString NumberFormat::getDigitFilled(const QChar &fill) const
{
    QString result;
    if (showPositiveSign)
        result.append('+');

    switch (mode) {
    case Decimal:
        if (showPositiveSign && integerDigits > 1)
            result.append(QString(fill).repeated(integerDigits - 1));
        else
            result.append(QString(fill).repeated(integerDigits));
        if (decimalDigits > 0) {
            result.append('.');
            result.append(QString(fill).repeated(decimalDigits));
        }
        return result;

    case Scientific:
        result.append(fill);
        result.append('.');
        result.append(QString(fill).repeated(decimalDigits));
        result.append(exponentSeparator);
        if (showPositiveExponent)
            result.append('+');
        result.append(QString(fill).repeated(exponentDigits));
        return result;

    case Hex:
    case Octal:
        result.append(basePrefix);
        result.append(QString(fill).repeated(integerDigits));
        return result;
    }

    Q_ASSERT(false);
    return QString();
}

/**
 * Get the "standard" missing value code for the format.  This can also be used 
 * as the "digit based" description of the format.
 * 
 * @return the MVC
 */
QString NumberFormat::mvc() const
{
    switch (mode) {
    case Decimal:
    case Scientific:
        return getDigitFilled(QLatin1Char('9'));
    case Hex:
        if (uppercaseHex)
            return getDigitFilled(QLatin1Char('F'));
        else
            return getDigitFilled(QLatin1Char('f'));
    case Octal:
        return getDigitFilled(QLatin1Char('7'));
    }
    Q_ASSERT(false);
    return QString();
}

/**
 * Get the expected total width of the format.
 *
 * @return the width in digits
 */
int NumberFormat::width() const
{
    int result = 0;
    if (showPositiveSign)
        result++;

    switch (mode) {
    case Decimal:
        if (showPositiveSign && integerDigits > 1)
            result += integerDigits - 1;
        else
            result += integerDigits;
        if (decimalDigits > 0)
            result += 1 + decimalDigits;
        return result;

    case Scientific:
        result += 2 + decimalDigits + exponentSeparator.length() + exponentDigits;
        if (showPositiveExponent)
            result++;
        return result;

    case Hex:
    case Octal:
        result += integerDigits;
        return result;
    }

    Q_ASSERT(false);
    return 0;
}

/**
 * Get the number of digits before the decimal place.
 * 
 * @return the number of integer digits
 */
int NumberFormat::getIntegerDigits() const
{ return integerDigits; }

/**
 * Get the number of digits after the decimal place.
 * 
 * @return the number of decimal digits
 */
int NumberFormat::getDecimalDigits() const
{ return decimalDigits; }

/**
 * Get the number of digits in the exponent.
 * 
 * @return the number of exponent digits
 */
int NumberFormat::getExponentDigits() const
{ return exponentDigits; }

/**
 * Get if a '+' should be shown for positive numbers.
 * 
 * @return the show positive state
 */
bool NumberFormat::getShowPositiveSign() const
{ return showPositiveSign; }

/**
 * Get if a '+' should be shown for positive exponents.
 * 
 * @return the show positive state
 */
bool NumberFormat::getShowPositiveExponent() const
{ return showPositiveExponent; }

/**
 * Get the separator for the exponent.  This is usually 'E' or 'e'.
 * 
 * @return the exponent delimiter string
 */
QString NumberFormat::getExponentSeparator() const
{ return exponentSeparator; }

/**
 * Get the formatting mode.
 * 
 * @return the formatting mode
 */
NumberFormat::Mode NumberFormat::getMode() const
{ return mode; }

/**
 * Test if this display uppercase if it's hexadecimal.
 * 
 * @return true if this format is hexadecimal
 */
bool NumberFormat::isHexUppercase() const
{ return uppercaseHex; }

/**
 * Get the prefix used when outputting alternate base numbers.
 * 
 * @return  the base prefix
 */
QString NumberFormat::getBasePrefix() const
{ return basePrefix; }

/**
 * Set the number of integer digits in the format.
 * 
 * @param digits    the number of digits
 */
void NumberFormat::setIntegerDigits(int digits)
{ integerDigits = digits; }


/**
 * Set the number of decimal digits in the format.
 * 
 * @param digits    the number of digits
 */
void NumberFormat::setDecimalDigits(int digits)
{ decimalDigits = digits; }

/**
 * Set the number of digits in exponent.
 * 
 * @param digits    the number of digits
 */
void NumberFormat::setExpontentDigits(int digits)
{ exponentDigits = digits; }

/**
 * Set if the format should display a '+' for positive numbers.
 * 
 * @param show  true to show the '+'
 */
void NumberFormat::setShowPositiveSign(bool show)
{ showPositiveSign = show; }

/**
 * Set if the format should show a '+' for positive exponents.
 */
void NumberFormat::setShowPositiveExponent(bool show)
{ showPositiveExponent = show; }

/**
 * Set the string that delimits the exponent from the fractional part.  This
 * is usually either 'e' or 'E'.
 * 
 * @param set   the delimiter
 */
void NumberFormat::setExponentSeparator(const QString &sep)
{ exponentSeparator = sep; }

/**
 * Set the formatting mode.
 * 
 * @param m     the mode
 */
void NumberFormat::setMode(Mode m)
{ mode = m; }

/**
 * Set if hexadecimal number should use upper case.
 * 
 * @param u     true for upper case
 */
void NumberFormat::setHexUppercase(bool u)
{ uppercaseHex = u; }

/**
 * Set the prefix output with alternate base integers.
 */
void NumberFormat::setBasePrefix(const QString &h)
{ basePrefix = h; }


QDataStream &operator<<(QDataStream &stream, const NumberFormat &fmt)
{
    stream << (quint8) fmt.mode;
    stream << (quint16) fmt.integerDigits;
    stream << (quint16) fmt.decimalDigits;
    stream << (quint16) fmt.exponentDigits;
    stream << fmt.showPositiveSign;
    stream << fmt.showPositiveExponent;
    stream << fmt.exponentSeparator;
    stream << fmt.uppercaseHex;
    stream << fmt.basePrefix;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, NumberFormat &fmt)
{
    quint8 ui8;
    quint16 ui16;
    stream >> ui8;
    fmt.mode = (NumberFormat::Mode) ui8;
    stream >> ui16;
    fmt.integerDigits = (int) ui16;
    stream >> ui16;
    fmt.decimalDigits = (int) ui16;
    stream >> ui16;
    fmt.exponentDigits = (int) ui16;
    stream >> fmt.showPositiveSign;
    stream >> fmt.showPositiveExponent;
    stream >> fmt.exponentSeparator;
    stream >> fmt.uppercaseHex;
    stream >> fmt.basePrefix;
    return stream;
}

QDebug operator<<(QDebug stream, const NumberFormat &fmt)
{
    QDebugStateSaver saver(stream);
    switch (fmt.mode) {
    case NumberFormat::Decimal:
        stream.nospace() << "Decimal(" << (fmt.showPositiveSign ? "+" : "") << fmt.integerDigits
                         << "," << fmt.decimalDigits << ")";
        break;
    case NumberFormat::Scientific:
        stream.nospace() << "Scientific(" << (fmt.showPositiveSign ? "+" : "") << fmt.decimalDigits
                         << "," << fmt.exponentSeparator << ", "
                         << (fmt.showPositiveExponent ? "+" : "") << fmt.exponentDigits << ")";
        break;
    case NumberFormat::Hex:
        stream.nospace() << "Hex(" << (fmt.showPositiveSign ? "+" : "") << fmt.basePrefix
                         << fmt.integerDigits << (fmt.uppercaseHex ? " UC" : " LC") << ")";
        break;
    case NumberFormat::Octal:
        stream.nospace() << "Octal(" << (fmt.showPositiveSign ? "+" : "") << fmt.basePrefix
                         << fmt.integerDigits << ")";
        break;
    }
    return stream;
}

}

#include <openssl/rand.h>

namespace CPD3 {

/**
 * Fill the given buffer with random bytes.
 *
 * @param ptr       the output pointer
 * @param length    the number of bytes
 * @return          true on success
 */
bool Random::fill(void *ptr, int length)
{
    OpenSSL::initialize();
    return ::RAND_bytes((unsigned char *) ptr, length) > 0;
}

/**
 * Generate series of random bytes.
 *
 * @param length        the number of bytes to generate
 * @return              random data
 */
QByteArray Random::data(int length)
{
    if (length == 0)
        return QByteArray();
    OpenSSL::initialize();

    QByteArray result(length, (char) 0);
    if (::RAND_bytes((unsigned char *) result.data(), length) <= 0)
        return QByteArray();

    return result;
}

}
