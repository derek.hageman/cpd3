/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "core/actioncomponent.hxx"

namespace CPD3 {


/** @file core/actioncomponent.hxx
 * Provides the interfaces for stand alone action components.
 */


namespace ActionFeedback {

Source::Stage::Stage() : stageTitle(), stageDescription(), stageHasProgress(false)
{ }

Source::Stage::Stage(const QString &title) : stageTitle(title),
                                             stageDescription(),
                                             stageHasProgress(false)
{ }

Source::Stage::Stage(const QString &title, const QString &description, bool hasProgress)
        : stageTitle(title), stageDescription(description), stageHasProgress(hasProgress)
{ }

Source::Stage::Stage(const QString &title, bool hasProgress) : stageTitle(title),
                                                               stageDescription(),
                                                               stageHasProgress(hasProgress)
{ }

Source::State::State() : stateDescription()
{ }

Source::State::State(const QString &state) : stateDescription(state)
{ }

Source::Failure::Failure() : failureReason()
{ }

Source::Failure::Failure(const QString &reason) : failureReason(reason)
{ }

Source::Time::Time() : progressTime(FP::undefined()),
                       possibleStart(FP::undefined()),
                       possibleEnd(FP::undefined())
{ }

Source::Time::Time(double time) : progressTime(time),
                                  possibleStart(FP::undefined()),
                                  possibleEnd(FP::undefined())
{ }

Source::Time::Time(double time, double start, double end) : progressTime(time),
                                                            possibleStart(start),
                                                            possibleEnd(end)
{ }

Source::Fraction::Fraction() : progressFraction(FP::undefined())
{ }

Source::Fraction::Fraction(double fraction) : progressFraction(fraction)
{ }

Source::Source() = default;

Source::Source(const Source &) = default;

Source &Source::operator=(const Source &) = default;

Source::Source(Source &&) = default;

Source &Source::operator=(Source &&) = default;

void Source::forward(Source &receiver)
{
    stage = receiver.stage;
    state = receiver.state;
    failure = receiver.failure;
    progressTime = receiver.progressTime;
    progressFraction = receiver.progressFraction;
}


void Source::disconnect()
{
    progressTime.disconnect();
    progressFraction.disconnect();
    state.disconnect();
    failure.disconnect();
    stage.disconnect();
}

Serializer::Stage::Stage() : currentState(Spin),
                             stageTitle(),
                             stageDescription(),
                             stageStateOrReason(),
                             progressFraction(FP::undefined()),
                             progressTime(FP::undefined()),
                             progressStart(FP::undefined()),
                             progressEnd(FP::undefined())
{ }

double Serializer::Stage::toFraction(double start, double end) const
{
    if (FP::defined(progressFraction))
        return progressFraction;

    if (!FP::defined(start))
        start = progressStart;
    if (!FP::defined(end))
        end = progressEnd;

    if (!FP::defined(start))
        return FP::undefined();
    if (!FP::defined(end))
        return FP::undefined();
    if (!FP::defined(progressTime))
        return FP::undefined();
    if (start >= end)
        return FP::undefined();

    double fraction = (progressTime - start) / (end - start);
    if (fraction < 0.0)
        fraction = 0.0;
    if (fraction > 1.0)
        fraction = 1.0;
    return fraction;
}

double Serializer::Stage::getStart(double context) const
{
    if (FP::defined(context))
        return context;
    return progressStart;
}

double Serializer::Stage::getEnd(double context) const
{
    if (FP::defined(context))
        return context;
    return progressEnd;
}


Serializer::Serializer() : mutex(), active(), pending(), progressUpdated(false)
{ }

Serializer::~Serializer()
{
    disconnect();
}

void Serializer::attach(Source &source)
{
    using namespace std::placeholders;
    source.stage.connect(*this, std::bind(&Serializer::incomingStage, this, _1));
    source.state.connect(*this, std::bind(&Serializer::incomingState, this, _1));
    source.failure.connect(*this, std::bind(&Serializer::incomingFailure, this, _1));
    source.progressTime.connect(*this, std::bind(&Serializer::incomingTime, this, _1));
    source.progressFraction.connect(*this, std::bind(&Serializer::incomingFraction, this, _1));
}

Serializer::Serializer(Source &source) : Serializer()
{ attach(source); }

std::vector<Serializer::Stage> Serializer::process()
{
    std::vector<Serializer::Stage> result;
    {
        std::lock_guard<std::mutex> lock(mutex);
        result = std::move(pending);
        pending.clear();
        result.push_back(active);
        progressUpdated = false;
    }
    return result;
}

void Serializer::incomingStage(const Source::Stage &stage)
{
    auto u = updated.defer();
    auto a = advanced.defer();

    std::lock_guard<std::mutex> lock(mutex);

    if (active.currentState != Stage::Failure)
        active.currentState = Stage::Complete;
    pending.push_back(active);
    progressUpdated = false;

    active = Stage();
    active.stageTitle = stage.title();
    active.stageDescription = stage.description();
    if (stage.hasProgress())
        active.currentState = Stage::Progress;
}

void Serializer::incomingState(const Source::State &state)
{
    auto u = updated.defer();
    auto a = advanced.defer();

    std::lock_guard<std::mutex> lock(mutex);
    active.stageStateOrReason = state.description();
}

void Serializer::incomingFailure(const Source::Failure &failure)
{
    auto u = updated.defer();

    std::lock_guard<std::mutex> lock(mutex);
    active.stageStateOrReason = failure.reason();
    active.currentState = Stage::Failure;
}

void Serializer::incomingTime(const Source::Time &progress)
{
    auto u = updated.defer();

    std::lock_guard<std::mutex> lock(mutex);

    active.progressFraction = FP::undefined();
    active.progressTime = progress.time();
    active.progressStart = progress.getStart();
    active.progressEnd = progress.getEnd();

    if (progressUpdated) {
        u.never();
        return;
    }
    progressUpdated = true;
}

void Serializer::incomingFraction(const Source::Fraction &progress)
{
    auto u = updated.defer();

    std::lock_guard<std::mutex> lock(mutex);

    active.progressFraction = progress.fraction();
    active.progressTime = FP::undefined();
    active.progressStart = FP::undefined();
    active.progressEnd = FP::undefined();

    if (progressUpdated) {
        u.never();
        return;
    }
    progressUpdated = true;
}

void Serializer::reset()
{
    std::lock_guard<std::mutex> lock(mutex);
    pending.clear();
    active = Stage();
    progressUpdated = false;
}

}

CPD3Action::CPD3Action()
{ }

CPD3Action::~CPD3Action()
{ }

ComponentOptions ActionComponent::getOptions()
{ return ComponentOptions(); }

QList<ComponentExample> ActionComponent::getExamples()
{ return QList<ComponentExample>(); }

int ActionComponent::actionAllowStations()
{ return 0; }

int ActionComponent::actionRequireStations()
{ return 0; }

QString ActionComponent::promptActionContinue(const ComponentOptions &,
                                              const std::vector<std::string> &)
{ return {}; }

ComponentOptions ActionComponentTime::getOptions()
{ return ComponentOptions(); }

QList<ComponentExample> ActionComponentTime::getExamples()
{ return QList<ComponentExample>(); }

int ActionComponentTime::actionAllowStations()
{ return 0; }

int ActionComponentTime::actionRequireStations()
{ return 0; }

bool ActionComponentTime::actionRequiresTime()
{ return true; }

bool ActionComponentTime::actionAcceptsUndefinedBounds()
{ return true; }

Time::LogicalTimeUnit ActionComponentTime::actionDefaultTimeUnit()
{ return Time::Day; }

QString ActionComponentTime::promptTimeActionContinue(const ComponentOptions &,
                                                      double,
                                                      double,
                                                      const std::vector<std::string> &)
{ return {}; }

QString ActionComponentTime::promptTimeActionContinue(const ComponentOptions &options,
                                                      const std::vector<std::string> &stations)
{
    return promptTimeActionContinue(options, FP::undefined(), FP::undefined(), stations);
}

CPD3Action *ActionComponentTime::createTimeAction(const ComponentOptions &options,
                                                  const std::vector<std::string> &stations)
{
    return createTimeAction(options, FP::undefined(), FP::undefined(), stations);
}

};
