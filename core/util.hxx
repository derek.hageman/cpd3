/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3CORE_UTIL_HXX
#define CPD3CORE_UTIL_HXX

#include "core/first.hxx"

#include <utility>
#include <string>
#include <iterator>
#include <vector>
#include <deque>
#include <memory>
#include <cstdint>
#include <type_traits>
#include <unordered_set>
#include <QList>
#include <QStringList>
#include <QSet>
#include <QHash>
#include <QDebug>
#include <QByteArray>
#include <QtEndian>
#include <QIODevice>

#include "core.hxx"
#include "number.hxx"

namespace std {

template<class T1, class T2>
struct hash<std::pair<T1, T2>> {
    inline std::size_t operator()(const std::pair<T1, T2> &s) const
    {
        return std::hash<T1>()(s.first) ^ std::hash<T2>()(s.second);
    }
};

template<class T>
struct hash<std::unordered_set<T>> {
    inline std::size_t operator()(const std::unordered_set<T> &set) const
    {
        std::size_t result = 0;
        for (const auto &add : set) {
            /* No mixing, so we don't have to care about the order */
            result ^= hash<T>()(add);
        }
        return result;
    }
};

}

class QDataStream;

class QRegularExpression;

namespace CPD3 {

namespace Util {

class ByteArray;

/**
 * A simple reference to a constant sequence of bytes.  The backing storage
 * is assumed to be constant for the lifetime of the reference.
 */
class CPD3CORE_EXPORT ByteView {
    const void *ptr;
    std::size_t total;
public:
    using value_type = std::uint8_t;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using reference = const std::uint8_t &;
    using const_reference = const std::uint8_t &;
    using pointer = const void *;
    using const_pointer = const void *;
    using iterator = const std::uint8_t *;
    using const_iterator = iterator;

    static constexpr size_type npos = static_cast<size_type>(-1);

    inline ByteView() : ptr(nullptr), total(0)
    { }

    ByteView(const ByteArray &parent);

    ByteView(const ByteArray &parent, size_type offset);

    ByteView(const ByteArray &parent, size_type offset, size_type count);

    ByteView(const ByteView &parent, size_type offset);

    ByteView(const ByteView &parent, size_type offset, size_type count);

    ByteView(const QByteArray &parent);

    explicit ByteView(const std::string &parent);

    explicit ByteView(const char *parent);

    inline ByteView(const void *ptr, std::size_t count) : ptr(ptr), total(count)
    { }

    template<typename Iterator>
    ByteView(Iterator begin,
             typename std::enable_if<
                     std::is_same<typename std::iterator_traits<Iterator>::iterator_category,
                                  std::random_access_iterator_tag>::value, Iterator>::type end)
            : ptr(&(*begin)), total(static_cast<std::size_t>(
                                            reinterpret_cast<const std::uint8_t *>(&(*end)) -
                                                    reinterpret_cast<const std::uint8_t *>(&(*begin))))
    { }

    inline ByteView(const ByteView &) = default;

    inline ByteView &operator=(const ByteView &) = default;

    inline ByteView(ByteView &&) = default;

    inline ByteView &operator=(ByteView &&) = default;

    inline bool empty() const
    { return total == 0; }

    inline size_type size() const
    { return total; }

    template<typename Type = const_pointer>
    inline Type data() const
    { return reinterpret_cast<Type>(ptr); }

    template<typename Type = const_pointer>
    inline Type data(size_type offset) const
    {
        Q_ASSERT(offset < size());
        return reinterpret_cast<Type>(reinterpret_cast<const std::uint8_t *>(ptr) + offset);
    }

    inline const_iterator begin() const
    { return static_cast<const std::uint8_t *>(ptr); }

    inline const_iterator cbegin() const
    { return static_cast<const std::uint8_t *>(ptr); }

    inline const_iterator end() const
    { return static_cast<const std::uint8_t *>(ptr) + total; }

    inline const_iterator cend() const
    { return static_cast<const std::uint8_t *>(ptr) + total; }

    value_type front() const
    { return *data<const std::uint8_t *>(); }

    value_type back() const
    { return *(data<const std::uint8_t *>() + size() - 1); }

    value_type operator[](size_type offset) const
    { return *(data<const std::uint8_t *>() + offset); }

    /**
     * Test if the byte view is empty.
     *
     * @return  true if the byte view is not empty
     */
    inline operator bool() const
    { return !empty(); }

    /**
     * Make a deep copy of the data into a QByteArray.
     *
     * @return  a deep copy of the data
     */
    QByteArray toQByteArray() const;

    /**
     * Make a shallow reference to the data stored in a QByteArray.  This
     * is only valid as long as the backing data is also valid.
     *
     * @return  a shallow copy of the data
     */
    QByteArray toQByteArrayRef() const;

    /**
     * Make a string out of the raw contents.
     *
     * @return  a string without any conversion
     */
    std::string toString() const;

    /**
     * Make a Qt string out of the raw contents.
     *
     * @param utf   convert the string as UTF-8
     * @return      a converted string
     */
    QString toQString(bool utf = true) const;

    /**
     * Read a little endian number from the array.
     *
     * @tparam Numeric  the numeric type (Qt types)
     * @param offset    the offset to read at
     */
    template<typename Numeric>
    Numeric readNumber(size_type offset = 0) const
    {
        Q_ASSERT(offset + sizeof(Numeric) <= size());
        return qFromLittleEndian<Numeric>(data<const uchar *>(offset));
    }

    /**
     * Return a subsequence of bytes.
     *
     * @param offset    the offset
     * @return          the subsequence
     */
    inline ByteView mid(size_type offset) const
    { return ByteView(*this, offset); }

    /**
     * Return a subsequence of bytes.
     *
     * @param offset    the offset
     * @param count     the maximum number of bytes
     * @return          the subsequence
     */
    inline ByteView mid(size_type offset, size_type count) const
    { return ByteView(*this, offset, count); }

    /**
     * Find the first occurrence of the specified value after the given offset.
     *
     * @param find      the value to look for
     * @param offset    the minimum offset
     * @return          the index of the found value or -1 if none
     */
    size_type indexOf(value_type find, size_type offset = 0) const;

    /**
     * Get a vector of lines contained in the data.  This includes empty lines.
     *
     * @return          the contents split by line delimiters
     */
    std::vector<ByteView> split_lines() const;

    /**
     * Get a vector of the components of the data split by a delimiter.
     *
     * @oaran delimiter the delimiter
     * @return          the contents split by the delimiter
     */
    std::vector<ByteView> split(value_type delimiter) const;

    /**
     * Remove spaces from the start and end of the data.
     *
     * @return          the data
     */
    ByteView &string_trimmed();

    /**
     * Test if the bytes contents are equal to a raw string.
     *
     * @param str       the null terminated string
     * @return          true if the contents are equal
     */
    bool string_equal(const char *str) const;

    /**
     * Test if the bytes contents are equal to an ASCII string, ignoring case
     *
     * @param str       the null terminated string
     * @return          true if the contents are equal
     */
    bool string_equal_insensitive(const char *str) const;

    /**
     * Test if the bytes contents are equal to a raw string
     *
     * @param str       the string
     * @return          true if the contents are equal
     */
    bool string_equal(const std::string &str) const;

    /**
     * Test if the bytes contents are equal to an ASCII string, ignoring case
     *
     * @param str       the string
     * @return          true if the contents are equal
     */
    bool string_equal_insensitive(const std::string &str) const;

    /**
     * Test if the bytes contents start with a raw string.
     *
     * @param str       the null terminated string
     * @return          true if the contents start with the string
     */
    bool string_start(const char *str) const;

    /**
     * Test if the bytes contents start with an ASCII string, igoring case.
     *
     * @param str       the null terminated string
     * @return          true if the contents start with the string
     */
    bool string_start_insensitive(const char *str) const;

    /**
     * Test if the bytes contents start with a raw string.
     *
     * @param str       the string
     * @return          true if the contents start with the string
     */
    bool string_start(const std::string &str) const;

    /**
     * Test if the bytes contents start with an ASCII string, igoring case.
     *
     * @param str       the string
     * @return          true if the contents start with the string
     */
    bool string_start_insensitive(const std::string &str) const;

    /**
     * Test if the bytes contents end with a raw string.
     *
     * @param str       the null terminated string
     * @return          true if the contents end with the string
     */
    bool string_end(const char *str) const;

    /**
     * Test if the bytes contents end with an ASCII string, igoring case.
     *
     * @param str       the null terminated string
     * @return          true if the contents end with the string
     */
    bool string_end_insensitive(const char *str) const;

    /**
     * Test if the bytes contents end with a raw string.
     *
     * @param str       the string
     * @return          true if the contents end with the string
     */
    bool string_end(const std::string &str) const;

    /**
     * Test if the bytes contents end with an ASCII string, igoring case.
     *
     * @param str       the string
     * @return          true if the contents end with the string
     */
    bool string_end_insensitive(const std::string &str) const;

    /**
     * Parse the contents of the bytes as an integer.
     *
     * @param ok    if not null, then set depending on conversion success
     * @param base  the base to convert with
     * @return      the parsed number
     */
    std::int16_t parse_i16(bool *ok = nullptr, int base = 10) const;

    /**
     * Parse the contents of the bytes as an integer.
     *
     * @param ok    if not null, then set depending on conversion success
     * @param base  the base to convert with
     * @return      the parsed number
     */
    std::uint16_t parse_u16(bool *ok = nullptr, int base = 10) const;

    /**
     * Parse the contents of the bytes as an integer.
     *
     * @param ok    if not null, then set depending on conversion success
     * @param base  the base to convert with
     * @return      the parsed number
     */
    std::int32_t parse_i32(bool *ok = nullptr, int base = 10) const;

    /**
     * Parse the contents of the bytes as an integer.
     *
     * @param ok    if not null, then set depending on conversion success
     * @param base  the base to convert with
     * @return      the parsed number
     */
    std::uint32_t parse_u32(bool *ok = nullptr, int base = 10) const;

    /**
     * Parse the contents of the bytes as an integer.
     *
     * @param ok    if not null, then set depending on conversion success
     * @param base  the base to convert with
     * @return      the parsed number
     */
    std::int64_t parse_i64(bool *ok = nullptr, int base = 10) const;

    /**
     * Parse the contents of the bytes as an integer.
     *
     * @param ok    if not null, then set depending on conversion success
     * @param base  the base to convert with
     * @return      the parsed number
     */
    std::uint64_t parse_u64(bool *ok = nullptr, int base = 10) const;

    /**
     * Parse the contents of the bytes as a real number.
     *
     * @param ok    if not null, then set depending on conversion success
     * @return      the parsed number
     */
    double parse_real(bool *ok = nullptr) const;

    class Device;

    class LineIterator;
};

/**
 * A simple wrapper around a byte view that allows it to be used as a QIODevice.
 */
class CPD3CORE_EXPORT ByteView::Device : public QIODevice {
    ByteView ref;
public:
    explicit inline Device(const ByteView &ref) : ref(ref)
    { }

    inline Device(const Device &) = default;

    Device &operator=(const Device &) = default;

    inline Device(Device &&) = default;

    Device &operator=(Device &&) = default;

    virtual bool canReadLine() const;

    virtual bool open(OpenMode flags);

    virtual bool seek(qint64 pos);

    virtual qint64 size() const;

protected:
    virtual qint64 readData(char *data, qint64 len);

    virtual qint64 writeData(const char *data, qint64 len);
};

/**
     * A simple iterator that reads non empty lines from the array.
     */
class CPD3CORE_EXPORT ByteView::LineIterator {
    ByteView ref;
    size_type offset;
public:
    explicit inline LineIterator(const ByteView &ref) : ref(ref), offset(0)
    { }

    inline LineIterator(const LineIterator &) = default;

    LineIterator &operator=(const LineIterator &) = default;

    inline LineIterator(LineIterator &&) = default;

    LineIterator &operator=(LineIterator &&) = default;

    /**
     * Get the next line, or empty when there are no more lines.
     *
     * @param acceptFinal   accept the final line without line separator
     * @return              the next line or empty on the end
     */
    Util::ByteView next(bool acceptFinal = true);

    /**
     * Get the offset of the current start of a line.
     *
     * @return              the current start offset
     */
    inline size_type getCurrentOffset() const
    { return offset; }
};

CPD3CORE_EXPORT QDataStream &operator<<(QDataStream &stream, const ByteView &a);

CPD3CORE_EXPORT QDebug operator<<(QDebug stream, const ByteView &a);

CPD3CORE_EXPORT bool operator==(const ByteView &a, const ByteView &b);

CPD3CORE_EXPORT bool operator!=(const ByteView &a, const ByteView &b);

inline bool operator==(const ByteView &a, const std::string &b)
{ return a == ByteView(b); }

inline bool operator!=(const ByteView &a, const std::string &b)
{ return a != ByteView(b); }

inline bool operator==(const std::string &a, const ByteView &b)
{ return ByteView(a) == b; }

inline bool operator!=(const std::string &a, const ByteView &b)
{ return ByteView(a) != b; }

inline bool operator==(const ByteView &a, const char *b)
{ return a == ByteView(b); }

inline bool operator!=(const ByteView &a, const char *b)
{ return a != ByteView(b); }

inline bool operator==(const char *a, const ByteView &b)
{ return ByteView(a) == b; }

inline bool operator!=(const char *a, const ByteView &b)
{ return ByteView(a) != b; }

/**
 * A simple byte array type, used so we don't incur the overhead and
 * occasional bugs from QByteArray's CoW.
 */
class CPD3CORE_EXPORT ByteArray {
    typedef std::vector<std::uint8_t> Storage;
    Storage storage;
public:
    using value_type = Storage::value_type;
    using size_type = Storage::size_type;
    using difference_type = Storage::difference_type;
    using reference = Storage::reference;
    using const_reference = Storage::const_reference;
    using pointer = void *;
    using const_pointer = const void *;
    using iterator = Storage::iterator;
    using const_iterator = Storage::const_iterator;
    using reverse_iterator = Storage::reverse_iterator;
    using const_reverse_iterator = Storage::const_reverse_iterator;

    static constexpr size_type npos = static_cast<size_type>(-1);

    inline ByteArray() = default;

    inline ByteArray(const ByteArray &) = default;

    inline ByteArray &operator=(const ByteArray &) = default;

    inline ByteArray(ByteArray &&) = default;

    inline ByteArray &operator=(ByteArray &&) = default;

    explicit ByteArray(const ByteView &other);

    explicit ByteArray(const QByteArray &other);

    explicit ByteArray(const char *str);

    explicit ByteArray(const std::string &str);

    explicit ByteArray(const void *data, size_type n);

    static ByteArray filled(value_type contents, size_type n = 1);


    template<typename Type = pointer>
    inline Type data()
    { return reinterpret_cast<Type>(storage.data()); }

    template<typename Type = const_pointer>
    inline Type data() const
    { return reinterpret_cast<Type>(storage.data()); }

    template<typename Type = pointer>
    inline Type data(size_type offset)
    {
        Q_ASSERT(offset < size());
        return reinterpret_cast<Type>(storage.data() + offset);
    }

    template<typename Type = const_pointer>
    inline Type data(size_type offset) const
    {
        Q_ASSERT(offset < size());
        return reinterpret_cast<Type>(storage.data() + offset);
    }

    inline size_type size() const
    { return storage.size(); }

    inline bool empty() const
    { return storage.empty(); }

    inline void resize(size_type size)
    { storage.resize(size); }

    inline void resize(size_type size, value_type fill)
    { storage.resize(size, fill); }

    inline void clear()
    { storage.clear(); }

    inline reference front()
    { return storage.front(); }

    inline value_type front() const
    { return storage.front(); }

    inline reference back()
    { return storage.back(); }

    inline value_type back() const
    { return storage.back(); }

    inline reference operator[](size_type offset)
    { return storage[offset]; }

    inline value_type operator[](size_type offset) const
    { return storage[offset]; }

    inline iterator begin()
    { return storage.begin(); }

    inline const_iterator begin() const
    { return storage.begin(); }

    inline iterator erase(iterator position)
    { return storage.erase(position); }

    inline iterator erase(iterator begin, iterator end)
    { return storage.erase(begin, end); }

    inline const_iterator cbegin() const
    { return storage.cbegin(); }

    inline iterator end()
    { return storage.end(); }

    inline const_iterator end() const
    { return storage.end(); }

    inline const_iterator cend() const
    { return storage.cend(); }

    inline reverse_iterator rbegin()
    { return storage.rbegin(); }

    inline const_reverse_iterator rbegin() const
    { return storage.rbegin(); }

    inline const_reverse_iterator crbegin() const
    { return storage.crbegin(); }

    inline reverse_iterator rend()
    { return storage.rend(); }

    inline const_reverse_iterator rend() const
    { return storage.rend(); }

    inline const_reverse_iterator crend() const
    { return storage.crend(); }

    inline void push_back(value_type add)
    { storage.push_back(add); }

    void push_back(const void *data, size_type count);

    void push_front(value_type add)
    { storage.insert(storage.begin(), add); }

    void push_front(const void *data, size_type count);

    void pop_front(size_type count = 1);

    void pop_back(size_type count = 1);


    ByteArray &operator=(const QByteArray &other);

    ByteArray &operator=(const ByteView &other);

    ByteArray &operator=(const char *string);

    ByteArray &operator=(const std::string &string);


    /**
     * Append data to the byte array.
     */
    ByteArray &operator+=(const ByteArray &other);

    ByteArray &operator+=(ByteArray &&other);

    ByteArray &operator+=(const QByteArray &other);

    ByteArray &operator+=(const ByteView &other);

    ByteArray &operator+=(const char *string);

    ByteArray &operator+=(const std::string &string);


    /**
     * Prepend data to the byte array.
     */
    ByteArray &operator-=(const ByteArray &other);

    ByteArray &operator-=(ByteArray &&other);

    ByteArray &operator-=(const QByteArray &other);

    ByteArray &operator-=(const ByteView &other);

    ByteArray &operator-=(const char *string);

    ByteArray &operator-=(const std::string &string);


    /**
     * Test if the byte array is empty.
     *
     * @return  true if the byte array is not empty
     */
    inline operator bool() const
    { return !empty(); }


    /**
     * Make a deep copy of the data into a QByteArray.
     *
     * @return  a deep copy of the data
     */
    QByteArray toQByteArray() const;

    /**
     * Make a shallow reference to the data stored in a QByteArray.  This
     * is only valid as long as the ByteArray itself is not resized.
     *
     * @return  a shallow copy of the data
     */
    QByteArray toQByteArrayRef() const;

    /**
     * Make a string out of the raw contents.
     *
     * @return  a string without any conversion
     */
    std::string toString() const;

    /**
     * Make a Qt string out of the raw contents.
     *
     * @param utf   convert the string as UTF-8
     * @return      a converted string
     */
    QString toQString(bool utf = true) const;

    /**
     * Add an element to the tail.
     *
     * @param size  the size to add
     * @return      a pointer to the position where the tail starts
     */
    template<typename Type = pointer>
    Type tail(size_type size)
    {
        std::size_t offset = this->size();
        resize(offset + size);
        return data<Type>(offset);
    }

    /**
     * Append a little endian number to the array.
     *
     * @tparam Numeric  the numeric type (Qt types)
     * @param n         the number
     */
    template<typename Numeric>
    void appendNumber(Numeric n)
    {
        qToLittleEndian<Numeric>(n, tail<uchar *>(sizeof(Numeric)));
    }

    /**
     * Read a little endian number from the array.
     *
     * @tparam Numeric  the numeric type (Qt types)
     * @param offset    the offset to read at
     */
    template<typename Numeric>
    Numeric readNumber(size_type offset = 0) const
    {
        Q_ASSERT(offset + sizeof(Numeric) <= size());
        return qFromLittleEndian<Numeric>(data<const uchar *>(offset));
    }

    /**
     * Find the first occurance of the specified value after the given offset.
     *
     * @param find      the value to look for
     * @param offset    the minimum offset
     * @return          the index of the found value or -1 if none
     */
    size_type indexOf(value_type find, size_type offset = 0) const;

    /**
     * Return a subsequence of bytes.
     *
     * @param offset    the offset
     * @return          the subsequence
     */
    inline ByteView mid(size_type offset) const
    { return ByteView(*this, offset); }

    /**
     * Return a subsequence of bytes.
     *
     * @param offset    the offset
     * @param count     the maximum number of bytes
     * @return          the subsequence
     */
    inline ByteView mid(size_type offset, size_type count) const
    { return ByteView(*this, offset, count); }

    /**
     * Get a vector of lines contained in the data.  This includes empty lines.
     *
     * @return          the contents split by line delimiters
     */
    inline std::vector<ByteView> split_lines() const
    { return ByteView(*this).split_lines(); }

    /**
     * Get a vector of the components of the data split by a delimiter.
     *
     * @oaran delimiter the delimiter
     * @return          the contents split by the delimiter
     */
    inline std::vector<ByteView> split(value_type delimiter) const
    { return ByteView(*this).split(delimiter); }

    /**
     * Test if the bytes contents are equal to a raw string.
     *
     * @param str       the null terminated string
     * @return          true if the contents are equal
     */
    inline bool string_equal(const char *str) const
    { return ByteView(*this).string_equal(str); }

    /**
     * Test if the bytes contents are equal to an ASCII string, ignoring case
     *
     * @param str       the null terminated string
     * @return          true if the contents are equal
     */
    inline bool string_equal_insensitive(const char *str) const
    { return ByteView(*this).string_equal_insensitive(str); }

    /**
     * Test if the bytes contents are equal to a raw string
     *
     * @param str       the string
     * @return          true if the contents are equal
     */
    inline bool string_equal(const std::string &str) const
    { return ByteView(*this).string_equal(str); }

    /**
     * Test if the bytes contents are equal to an ASCII string, ignoring case
     *
     * @param str       the string
     * @return          true if the contents are equal
     */
    inline bool string_equal_insensitive(const std::string &str) const
    { return ByteView(*this).string_equal_insensitive(str); }

    /**
     * Test if the bytes contents start with a raw string.
     *
     * @param str       the null terminated string
     * @return          true if the contents start with the string
     */
    inline bool string_start(const char *str) const
    { return ByteView(*this).string_start(str); }

    /**
     * Test if the bytes contents start with an ASCII string, ignoring case.
     *
     * @param str       the null terminated string
     * @return          true if the contents start with the string
     */
    inline bool string_start_insensitive(const char *str) const
    { return ByteView(*this).string_start_insensitive(str); }

    /**
     * Test if the bytes contents start with a raw string.
     *
     * @param str       the string
     * @return          true if the contents start with the string
     */
    inline bool string_start(const std::string &str) const
    { return ByteView(*this).string_start(str); }

    /**
     * Test if the bytes contents start with an ASCII string, ignoring case.
     *
     * @param str       the string
     * @return          true if the contents start with the string
     */
    inline bool string_start_insensitive(const std::string &str) const
    { return ByteView(*this).string_start_insensitive(str); }

    /**
     * Test if the bytes contents end with a raw string.
     *
     * @param str       the null terminated string
     * @return          true if the contents end with the string
     */
    inline bool string_end(const char *str) const
    { return ByteView(*this).string_end(str); }

    /**
     * Test if the bytes contents end with an ASCII string, ignoring case.
     *
     * @param str       the null terminated string
     * @return          true if the contents end with the string
     */
    inline bool string_end_insensitive(const char *str) const
    { return ByteView(*this).string_end_insensitive(str); }

    /**
     * Test if the bytes contents end with a raw string.
     *
     * @param str       the string
     * @return          true if the contents end with the string
     */
    inline bool string_end(const std::string &str) const
    { return ByteView(*this).string_end(str); }

    /**
     * Test if the bytes contents end with an ASCII string, ignoring case.
     *
     * @param str       the string
     * @return          true if the contents end with the string
     */
    inline bool string_end_insensitive(const std::string &str) const
    { return ByteView(*this).string_end_insensitive(str); }

    /**
     * Convert the byte array as an ASCII string into upper case.
     *
     * @return the modified array
     */
    ByteArray &string_to_upper();

    /**
     * Convert the byte array as an ASCII string into lower case.
     *
     * @return the modified array
     */
    ByteArray &string_to_lower();

    /**
     * Remove spaces from the start and end of the array
     *
     * @return the trimmed data
     */
    ByteArray &string_trimmed();

    /**
     * Parse the contents of the bytes as an integer.
     *
     * @param ok    if not null, then set depending on conversion success
     * @param base  the base to convert with
     * @return      the parsed number
     */
    inline std::int16_t parse_i16(bool *ok = nullptr, int base = 10) const
    { return ByteView(*this).parse_i16(ok, base); }

    /**
     * Parse the contents of the bytes as an integer.
     *
     * @param ok    if not null, then set depending on conversion success
     * @param base  the base to convert with
     * @return      the parsed number
     */
    inline std::uint16_t parse_u16(bool *ok = nullptr, int base = 10) const
    { return ByteView(*this).parse_u16(ok, base); }

    /**
     * Parse the contents of the bytes as an integer.
     *
     * @param ok    if not null, then set depending on conversion success
     * @param base  the base to convert with
     * @return      the parsed number
     */
    inline std::int32_t parse_i32(bool *ok = nullptr, int base = 10) const
    { return ByteView(*this).parse_i32(ok, base); }

    /**
     * Parse the contents of the bytes as an integer.
     *
     * @param ok    if not null, then set depending on conversion success
     * @param base  the base to convert with
     * @return      the parsed number
     */
    inline std::uint32_t parse_u32(bool *ok = nullptr, int base = 10) const
    { return ByteView(*this).parse_u32(ok, base); }

    /**
     * Parse the contents of the bytes as an integer.
     *
     * @param ok    if not null, then set depending on conversion success
     * @param base  the base to convert with
     * @return      the parsed number
     */
    inline std::int64_t parse_i64(bool *ok = nullptr, int base = 10) const
    { return ByteView(*this).parse_i64(ok, base); }

    /**
     * Parse the contents of the bytes as an integer.
     *
     * @param ok    if not null, then set depending on conversion success
     * @param base  the base to convert with
     * @return      the parsed number
     */
    inline std::uint64_t parse_u64(bool *ok = nullptr, int base = 10) const
    { return ByteView(*this).parse_i64(ok, base); }

    /**
     * Parse the contents of the bytes as a real number.
     *
     * @param ok    if not null, then set depending on conversion success
     * @return      the parsed number
     */
    inline double parse_real(bool *ok = nullptr) const
    { return ByteView(*this).parse_real(ok); }

    /**
     * A simple wrapper around a byte array that allows it to be used as a QIODevice.
     */
    class CPD3CORE_EXPORT Device : public QIODevice {
        ByteArray &array;
    public:
        explicit inline Device(ByteArray &array) : array(array)
        { }

        inline Device(const Device &) = default;

        inline Device(Device &&) = default;

        bool canReadLine() const override;

        bool open(OpenMode flags) override;

        bool seek(qint64 pos) override;

        qint64 size() const override;

    protected:
        qint64 readData(char *data, qint64 len) override;

        qint64 writeData(const char *data, qint64 len) override;
    };

    /**
     * A simple iterator that reads non empty lines from the array.
     */
    class CPD3CORE_EXPORT LineIterator {
        ByteArray &array;
        size_type offset;
    public:
        explicit inline LineIterator(ByteArray &array) : array(array), offset(0)
        { }

        inline LineIterator(const LineIterator &) = default;

        inline LineIterator(LineIterator &&) = default;

        /**
         * Get the next line, or empty when there are no more lines.
         *
         * @param acceptFinal   accept the final line without line separator
         * @return              the next line or empty on the end
         */
        Util::ByteView next(bool acceptFinal = true);

        /**
         * Get the offset of the current start of a line.
         *
         * @return              the current start offset
         */
        inline size_type getCurrentOffset() const
        { return offset; }
    };
};

inline ByteView::ByteView(const ByteArray &parent) : ptr(parent.data()), total(parent.size())
{ }

CPD3CORE_EXPORT QDataStream &operator<<(QDataStream &stream, const ByteArray &a);

CPD3CORE_EXPORT QDataStream &operator>>(QDataStream &stream, ByteArray &a);

CPD3CORE_EXPORT QDebug operator<<(QDebug stream, const ByteArray &a);

inline bool operator==(const ByteArray &a, const ByteArray &b)
{ return ByteView(a) == ByteView(b); }

inline bool operator!=(const ByteArray &a, const ByteArray &b)
{ return ByteView(a) != ByteView(b); }

inline bool operator==(const ByteArray &a, const std::string &b)
{ return ByteView(a) == ByteView(b); }

inline bool operator!=(const ByteArray &a, const std::string &b)
{ return ByteView(a) != ByteView(b); }

inline bool operator==(const std::string &a, const ByteArray &b)
{ return ByteView(a) == ByteView(b); }

inline bool operator!=(const std::string &a, const ByteArray &b)
{ return ByteView(a) != ByteView(b); }

inline bool operator==(const ByteArray &a, const char *b)
{ return ByteView(a) == ByteView(b); }

inline bool operator!=(const ByteArray &a, const char *b)
{ return ByteView(a) != ByteView(b); }

inline bool operator==(const char *a, const ByteArray &b)
{ return ByteView(a) == ByteView(b); }

inline bool operator!=(const char *a, const ByteArray &b)
{ return ByteView(a) != ByteView(b); }

/**
 * A union of multiple types that does explicit construction and
 * destruction of the types stored in it.
 *
 * @tparam Args     the types
 */
template<typename...Types>
class ExplicitUnion {
    static constexpr std::size_t max2(std::size_t a, std::size_t b)
    { return a > b ? a : b; }

    template<typename First = std::uint8_t, typename... Args>
    static constexpr std::size_t maxSize()
    { return sizeof...(Args) > 0 ? max2(sizeof(First), maxSize<Args...>()) : sizeof(First); }

    static constexpr std::size_t totalSize = maxSize<Types...>();

    std::uint8_t data[totalSize];

public:
    ExplicitUnion() = default;

    ~ExplicitUnion() = default;

    ExplicitUnion(const ExplicitUnion &) = delete;

    ExplicitUnion &operator=(const ExplicitUnion &) = delete;

    /**
     * Construct a contained type in the existing data.
     *
     * @tparam T        the type to construct
     * @tparam Args     the constructor argument types
     * @param args      the constructor arguments
     */
    template<typename T, typename... Args>
    inline void construct(Args &&... args)
    {
        static_assert(sizeof(T) <= totalSize, "size exceeds capacity");
        new(&data[0]) T(std::forward<Args>(args)...);
    }

    /**
     * Destruct a type contained in the data.
     *
     * @tparam T        the type
     */
    template<typename T>
    inline void destruct()
    { ptr<T>()->~T(); }

    /**
     * Get a pointer to the contained data as a type.
     *
     * @tparam T        the type
     * @return          a pointer of the specified type
     */
    template<typename T>
    inline T *ptr()
    {
        static_assert(sizeof(T) <= totalSize, "size exceeds capacity");
        return reinterpret_cast<T *>(&data[0]);
    }

    /**
     * Get a pointer to the contained data as a type.
     *
     * @tparam T        the type
     * @return          a pointer of the specified type
     */
    template<typename T>
    inline const T *ptr() const
    {
        static_assert(sizeof(T) <= totalSize, "size exceeds capacity");
        return reinterpret_cast<const T *>(&data[0]);
    }

    /**
     * Get a reference to the contained data as a type.
     *
     * @tparam T        the type
     * @return          a reference of the specified type
     */
    template<typename T>
    inline T &ref()
    { return *ptr<T>(); }

    /**
     * Get a reference to the contained data as a type.
     *
     * @tparam T        the type
     * @return          a reference of the specified type
     */
    template<typename T>
    inline const T &ref() const
    { return *ptr<T>(); }
};

/**
 * A simple wrapper around two bounding iterators for use in range based for loops.
 *
 * @tparam ForwardIterator  the iterator type
 */
template<typename ForwardIterator>
class RangeIterator {
    ForwardIterator b;
    ForwardIterator e;
public:
    using size_type = std::size_t;
    using difference_type = typename std::iterator_traits<ForwardIterator>::difference_type;
    using value_type = typename std::iterator_traits<ForwardIterator>::value_type;
    using const_value_type = const value_type;
    using pointer = typename std::iterator_traits<ForwardIterator>::pointer;
    using reference = typename std::iterator_traits<ForwardIterator>::reference;
    using const_reference = const value_type &;
    using iterator = ForwardIterator;
    using const_iterator = ForwardIterator;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    /**
     * Create the wrapper.
     *
     * @param begin the begin iterator
     * @param end   the end iterator
     */
    RangeIterator(ForwardIterator begin, ForwardIterator end) : b(begin), e(end)
    { }

    RangeIterator() = default;

    RangeIterator(const RangeIterator &) = default;

    RangeIterator &operator=(const RangeIterator &) = default;

    RangeIterator(RangeIterator &&) = default;

    RangeIterator &operator=(RangeIterator &&) = default;

    inline ForwardIterator begin() const
    { return b; }

    inline ForwardIterator end() const
    { return e; }

    inline ForwardIterator cbegin() const
    { return b; }

    inline ForwardIterator cend() const
    { return e; }

    inline size_type size() const
    { return static_cast<size_type>(e - b); }

    inline bool empty() const
    { return b == e; }

    inline reference front()
    { return *b; }

    inline reference back()
    {
        auto end = e;
        --end;
        return *end;
    }

    inline const_reference front() const
    { return *b; }

    inline const_reference back() const
    {
        auto end = e;
        --end;
        return *end;
    }
};

/**
 * A wrapper around a const reference that provides a copy on write type
 * semantic for it.  Writing access will duplicate the value.
 *
 * @tparam T    the type, must provide a copy constructor
 */
template<typename T>
class ReferenceCopy {
    const T &original;
    std::unique_ptr<T> copy;

public:
    ReferenceCopy() = delete;

    ~ReferenceCopy() = default;

    explicit ReferenceCopy(const T &ref) : original(ref)
    { }

    ReferenceCopy(const ReferenceCopy<T> &) = delete;

    ReferenceCopy &operator=(const ReferenceCopy<T> &) = delete;

    ReferenceCopy(ReferenceCopy<T> &&) = default;

    ReferenceCopy &operator=(ReferenceCopy<T> &&) = default;

    T *operator->()
    {
        if (!copy)
            copy.reset(new T(original));
        return copy.get();
    }

    const T *operator->() const
    {
        if (copy)
            return copy.get();
        return &original;
    }

    T &operator*()
    {
        if (!copy)
            copy.reset(new T(original));
        return *copy;
    }

    const T &operator*() const
    {
        if (copy)
            return *copy;
        return original;
    }

    const T &operator()() const
    {
        if (copy)
            return *copy;
        return original;
    }
};

/**
 * A back implacing iterator, since there isn't one defined in the C++11 standard.
 * @tparam Container
 */
template<typename Container>
class BackEmplaceIterator {
    Container *container;
public:
    using self_type = BackEmplaceIterator<Container>;
    using difference_type = void;
    using value_type = void;
    using pointer = void;
    using reference = void;
    using iterator_category = std::output_iterator_tag;

    explicit BackEmplaceIterator(Container &container) : container(&container)
    { }

    BackEmplaceIterator(const BackEmplaceIterator<Container> &) = default;

    BackEmplaceIterator<Container> &operator=(const BackEmplaceIterator<Container> &) = default;

    BackEmplaceIterator(BackEmplaceIterator<Container> &&) = default;

    BackEmplaceIterator<Container> &operator=(BackEmplaceIterator<Container> &&) = default;

    class Proxy {
        friend class BackEmplaceIterator;

        Container *container;

        Proxy(const BackEmplaceIterator &from) : container(from.container)
        { }

    public:
        Proxy() = delete;

        Proxy(const Proxy &) = default;

        Proxy &operator=(const Proxy &) = default;

        Proxy(Proxy &&) = default;

        Proxy &operator=(Proxy &&) = default;

        template<typename T>
        void operator=(T &&value)
        { container->emplace_back(std::forward<T>(value)); }
    };

    friend class Proxy;

    Proxy operator*()
    { return Proxy(*this); }

    BackEmplaceIterator<Container> &operator++()
    { return *this; }

    BackEmplaceIterator<Container> &operator++(int)
    { return *this; }
};

/**
 * Create a back emplacing iterator, analagous to std::back_inserter
 *
 * @tparam Container    the container type
 * @param container     the container
 * @return              a back emplacing iterator
 */
template<typename Container>
inline BackEmplaceIterator<Container> back_emplacer(Container &container)
{ return BackEmplaceIterator<Container>(container); }

template<typename T>
inline std::back_insert_iterator<QList<T>> back_emplacer(QList<T> &container)
{ return std::back_inserter(container); }

template<typename T>
inline std::back_insert_iterator<QVector<T>> back_emplacer(QVector<T> &container)
{ return std::back_inserter(container); }

inline std::back_insert_iterator<QStringList> back_emplacer(QStringList &container)
{ return std::back_inserter(container); }

/**
 * Convert a container type into a deque by adding all the elements in the source.
 *
 * @tparam SourceType   the source list type
 * @param source        the source list
 * @return              a deque assembled from the source
 */
template<typename SourceType>
inline auto as_deque(const SourceType &source) -> std::deque<
        typename std::remove_reference<decltype(*source.begin())>::type>
{
    return std::deque<typename std::remove_reference<decltype(source.front())>::type>(
            source.begin(), source.end());
}

template<typename SourceType>
inline auto as_deque(SourceType &&source) -> std::deque<
        typename std::remove_reference<decltype(*source.begin())>::type>
{
    return std::deque<typename std::remove_reference<decltype(source.front())>::type>(
            std::make_move_iterator(source.begin()), std::make_move_iterator(source.end()));
}

/**
 * Append a source list to a destination one.
 *
 * @tparam SourceType       the source list type
 * @tparam DestinationType  the destination list type
 * @param source            the source list
 * @param destination       the destination list
 */
template<typename SourceType, typename DestinationType,
         class = typename std::enable_if<!std::is_lvalue_reference<SourceType>::value>::type>
inline void append(SourceType &&source, DestinationType &destination)
{ std::move(source.begin(), source.end(), back_emplacer(destination)); }

template<typename SourceType,
         class = typename std::enable_if<!std::is_lvalue_reference<SourceType>::value>::type>
inline void append(SourceType &&source, SourceType &destination)
{
    if (destination.empty()) {
        destination = std::move(source);
        return;
    }
    std::move(source.begin(), source.end(), back_emplacer(destination));
}

template<typename SourceType, typename DestinationType>
inline void append(const SourceType &source, DestinationType &destination)
{ std::copy(source.begin(), source.end(), back_emplacer(destination)); }

template<typename SourceType>
inline void append(const SourceType &source, SourceType &destination)
{
    if (destination.empty()) {
        destination = source;
        return;
    }
    std::copy(source.begin(), source.end(), back_emplacer(destination));
}

template<typename T>
inline void append(QList<T> &&source, QList<T> &destination)
{
    if (destination.empty()) {
        destination = std::move(source);
        return;
    }
    destination.append(source);
}

template<typename T>
inline void append(const QList<T> &source, QList<T> &destination)
{
    if (destination.empty()) {
        destination = source;
        return;
    }
    destination.append(source);
}

template<class T>
struct is_qhash_or_set : std::false_type {
};
template<class K>
struct is_qhash_or_set<QSet<K>> : std::true_type {
};
template<class K, class V>
struct is_qhash_or_set<QHash<K, V>> : std::true_type {
};

/**
 * Merge two containers together.  This is used with sets or maps to combine
 * two of them.
 *
 * @tparam SourceType       the source container type
 * @tparam DestinationType  the destination container type
 * @param source            the source container
 * @param destination       the destination container
 */
template<typename SourceType, typename DestinationType, class = typename std::enable_if<
        !std::is_lvalue_reference<SourceType>::value &&
                !is_qhash_or_set<SourceType>::value &&
                !is_qhash_or_set<DestinationType>::value>::type>
inline void merge(SourceType &&source,
                  typename std::enable_if<!std::is_const<typename std::remove_reference<
                          typename std::iterator_traits<
                                  typename SourceType::iterator>::reference>::type>::value,
                                          DestinationType>::type &destination)
{
    destination.insert(std::make_move_iterator(source.begin()),
                       std::make_move_iterator(source.end()));
}

template<typename SourceType, class = typename std::enable_if<
        !std::is_lvalue_reference<SourceType>::value && !is_qhash_or_set<SourceType>::value>::type>
inline void merge(SourceType &&source,
                  typename std::enable_if<!std::is_const<typename std::remove_reference<
                          typename std::iterator_traits<
                                  typename SourceType::iterator>::reference>::type>::value,
                                          SourceType>::type &destination)
{
    if (destination.empty()) {
        destination = std::move(source);
        return;
    }
#if CPD3_CXX >= 201700L && (!defined(__clang__) || CPD3_CXX > 201700L)
    destination.merge(std::move(source));
#else
    destination.insert(std::make_move_iterator(source.begin()),
                       std::make_move_iterator(source.end()));
#endif
}

template<typename SourceType, typename DestinationType, class = typename std::enable_if<
        !std::is_lvalue_reference<SourceType>::value &&
                !is_qhash_or_set<DestinationType>::value>::type>
inline void merge(SourceType &&source,
                  typename std::enable_if<std::is_const<typename std::remove_reference<
                          typename std::iterator_traits<
                                  typename SourceType::iterator>::reference>::type>::value,
                                          DestinationType>::type &destination)
{
    destination.insert(source.begin(), source.end());
}

template<typename SourceType, class = typename std::enable_if<
        !std::is_lvalue_reference<SourceType>::value && !is_qhash_or_set<SourceType>::value>::type>
inline void merge(SourceType &&source,
                  typename std::enable_if<std::is_const<typename std::remove_reference<
                          typename std::iterator_traits<
                                  typename SourceType::iterator>::reference>::type>::value,
                                          SourceType>::type &destination)
{
    if (destination.empty()) {
        destination = std::move(source);
        return;
    }
#if CPD3_CXX >= 201700L && (!defined(__clang__) || CPD3_CXX > 201700L)
    destination.merge(std::move(source));
#else
    destination.insert(source.begin(), source.end());
#endif
}

template<typename SourceType, typename DestinationType>
inline void merge(const SourceType &source, DestinationType &destination)
{ destination.insert(source.begin(), source.end()); }

template<typename SourceType>
inline void merge(const SourceType &source, SourceType &destination)
{
    if (destination.empty()) {
        destination = source;
        return;
    }
    destination.insert(source.begin(), source.end());
}

template<typename T>
inline void merge(QSet<T> &&source, QSet<T> &destination)
{
    if (destination.empty()) {
        destination = std::move(source);
        return;
    }
    destination.unite(std::move(source));
}

template<typename T>
inline void merge(const QSet<T> &source, QSet<T> &destination)
{
    if (destination.empty()) {
        destination = source;
        return;
    }
    destination.unite(source);
}

template<typename SourceType, typename T>
inline void merge(const SourceType &source, QSet<T> &destination)
{
    for (const auto &add : source) {
        destination.insert(add);
    }
}

template<typename K, typename V>
inline void merge(QHash<K, V> &&source, QHash<K, V> &destination)
{
    if (destination.empty()) {
        destination = std::move(source);
        return;
    }
    /* Can't use destination.unite, because it can insert duplicates */
    for (auto add = source.begin(), end = source.end(); add != end; ++add) {
        destination.insert(add.key(), std::move(add.value()));
    }
}

template<typename K, typename V>
inline void merge(const QHash<K, V> &source, QHash<K, V> &destination)
{
    if (destination.empty()) {
        destination = source;
        return;
    }
    /* Can't use destination.unite, because it can insert duplicates */
    for (auto add = source.begin(), end = source.end(); add != end; ++add) {
        destination.insert(add.key(), add.value());
    }
}

template<typename SourceType, typename K, typename V>
inline void merge(const SourceType &source, QHash<K, V> &destination)
{
    for (const auto &add : source) {
        destination.insert(add.first, add.second);
    }
}

template<typename DestinationType, typename K, typename V>
inline void merge(QHash<K, V> &&source, DestinationType &destination)
{
    for (auto add = source.begin(), end = source.end(); add != end; ++add) {
        destination.emplace(add.key(), std::move(add.value()));
    }
}

template<typename DestinationType, typename K, typename V>
inline void merge(const QHash<K, V> &source, DestinationType &destination)
{
    for (auto add = source.cbegin(), end = source.cend(); add != end; ++add) {
        destination.emplace(add.key(), add.value());
    }
}


/**
 * Insert or assign/replace an element in an associative container.
 *
 * @tparam ContainerType    the associative container type
 * @tparam K                the key type
 * @tparam V                the value type
 * @param container         the destination container
 * @param key               the key value
 * @param value             the data value
 * @return                  an iterator pointing to the inserted element
 */
template<typename ContainerType, typename K, typename V>
auto insert_or_assign(ContainerType &container, K &&key, V &&value) -> decltype(container.emplace(
        std::forward<K>(key), std::forward<V>(value)).first)
{
#if CPD3_CXX >= 201700L
    return container.insert_or_assign(std::forward<K>(key), std::forward<V>(value)).first;
#else
    auto check = container.find(key);
    if (check != container.end()) {
        check->second = std::forward<V>(value);
        return check;
    }
    return container.emplace(std::forward<K>(key), std::forward<V>(value)).first;
#endif
}

template<typename K, typename V>
auto insert_or_assign(QHash<K, V> &container,
                      const K &key,
                      const V &value) -> decltype(container.insert(key, value))
{ return container.insert(key, value); }


/**
 * A case insensitive comparison for strings.
 *
 * @param a     the first string
 * @param b     the second string
 * @return      true if the strings are equal, ignoring case
 */
CPD3CORE_EXPORT bool equal_insensitive(const std::string &a, const std::string &b);

CPD3CORE_EXPORT bool equal_insensitive(const std::string &a, const char *b);

/**
 * A case insensitive comparison for strings.
 *
 * @tparam Args additional argument types
 * @param a     the first string
 * @param b     the second string
 * @param args  any additional strings
 * @return      true if the first string is equal to any of the second, ignoring case
 */
template<typename...Args>
static bool equal_insensitive(const std::string &a, const std::string &b, Args &&... args)
{
    if (equal_insensitive(a, b))
        return true;
    return equal_insensitive(a, std::forward<Args>(args)...);
}

template<typename...Args>
static bool equal_insensitive(const std::string &a, const char *b, Args &&... args)
{
    if (equal_insensitive(a, b))
        return true;
    return equal_insensitive(a, std::forward<Args>(args)...);
}

/**
 * A case sensitive string prefix check.
 *
 * @param str       the string
 * @param prefix    the prefix to check
 * @return          true if the string starts with the prefix
 */
CPD3CORE_EXPORT bool starts_with(const std::string &str, const std::string &prefix);

CPD3CORE_EXPORT bool starts_with(const std::string &str, const char *prefix);

/**
 * A case insensitive string prefix check.
 *
 * @param str       the string
 * @param prefix    the prefix to check
 * @return          true if the string starts with the prefix, ignoring case
 */
CPD3CORE_EXPORT bool starts_with_insensitive(const std::string &str, const std::string &prefix);

CPD3CORE_EXPORT bool starts_with_insensitive(const std::string &str, const char *prefix);

/**
 * A case sensitive string suffix check.
 *
 * @param str       the string
 * @param suffix    the suffix to check
 * @return          true if the string starts with the suffix
 */
CPD3CORE_EXPORT bool ends_with(const std::string &str, const std::string &suffix);

CPD3CORE_EXPORT bool ends_with(const std::string &str, const char *suffix);

/**
 * A case insensitive string suffix check.
 *
 * @param str       the string
 * @param suffix    the suffix to check
 * @return          true if the string starts with the suffix, ignoring case
 */
CPD3CORE_EXPORT bool ends_with_insensitive(const std::string &str, const std::string &suffix);

CPD3CORE_EXPORT bool ends_with_insensitive(const std::string &str, const char *suffix);

/**
 * Replace all instances of a string with another one.
 *
 * @param s         the string to find and replace in
 * @param before    the string to search for
 * @param after     the string to replace it with
 */
CPD3CORE_EXPORT void replace_all(std::string &s,
                                 const std::string &before,
                                 const std::string &after);

CPD3CORE_EXPORT void replace_all(std::string &s, const char *before, const char *after);

/**
 * Replace all instances of a string with another one, ignoring case.
 *
 * @param s         the string to find and replace in
 * @param before    the string to search for
 * @param after     the string to replace it with
 */
CPD3CORE_EXPORT void replace_insensitive(std::string &s,
                                         const std::string &before,
                                         const std::string &after);

CPD3CORE_EXPORT void replace_insensitive(std::string &s, const char *before, const char *after);

/**
 * Perform an exact match of a string against a regular expression.
 * <br>
 * This returns true if the regular expression matches the whole string starting
 * at the first character.
 *
 * @param str       the string to match
 * @param regexp    the regular expression
 * @return          true if the regular expression is an exact match
 */
CPD3CORE_EXPORT bool exact_match(const QString &str, const QRegularExpression &regexp);

CPD3CORE_EXPORT bool exact_match(const std::string &str, const QRegularExpression &regexp);

/**
 * Check if a string contains a specific target.
 *
 * @param str       the string to match
 * @param check     the search target
 * @return          true if the string contains the check
 */
CPD3CORE_EXPORT bool contains(const std::string &str, char check);

CPD3CORE_EXPORT bool contains(const std::string &str, const std::string &check);

CPD3CORE_EXPORT bool contains(const std::string &str, const char *check);

CPD3CORE_EXPORT bool contains(const std::string &str, const QString &check);


/**
 * Return a copy of a string with spaces removed from the front and back.
 *
 * @param str       the string to trim
 * @return          a trimmed string
 */
CPD3CORE_EXPORT std::string trimmed(const std::string &str);

/**
 * Return a string with spaces removed from the front and back.
 *
 * @param str       the string to trim
 * @return          a trimmed string
 */
CPD3CORE_EXPORT std::string trimmed(std::string &&str);

/**
 * Return a copy of a string in lower case.
 *
 * @param str       the string to convert
 * @return          a lower case string
 */
CPD3CORE_EXPORT std::string to_lower(const std::string &str);

/**
 * Return a string in lower case.
 *
 * @param str       the string to convert
 * @return          a lower case string
 */
CPD3CORE_EXPORT std::string to_lower(std::string &&str);

/**
 * Return a copy of a string in upper case.
 *
 * @param str       the string to convert
 * @return          an upper case string
 */
CPD3CORE_EXPORT std::string to_upper(const std::string &str);

/**
 * Return a string in upper case.
 *
 * @param str       the string to convert
 * @return          an upper case string
 */
CPD3CORE_EXPORT std::string to_upper(std::string &&str);

/**
 * Split a string into delimited components.
 *
 * @param str           the string to split
 * @param delimiter     the delimiting character
 * @param empty         include empty components
 * @return              the split string components
 */
CPD3CORE_EXPORT std::vector<std::string> split_string(const std::string &str,
                                                      char delimiter,
                                                      bool empty = false);

/**
 * Split a string into space separated components with quoting applied.
 *
 * @param str           the string to split
 * @param escape        enable escape sequences in double quoted sections
 * @return              the split string components
 */
CPD3CORE_EXPORT std::vector<std::string> split_quoted(const std::string &str, bool escape = true);

/**
 * Split a string into line separated components with newline style handling.
 *
 * @param str           the string to split
 * @return              the split string components
 */
CPD3CORE_EXPORT std::vector<std::string> split_lines(const std::string &str);

/**
 * Split a string, getting the prefix before a delimiter.
 *
 * @param str           the string to split
 * @param delimiter     the delimiting character
 * @return              the string prefix
 */
CPD3CORE_EXPORT std::string prefix(const std::string &str, char delimiter);

CPD3CORE_EXPORT std::string prefix(std::string &&str, char delimiter);

/**
 * Split a string, getting the suffix after a delimiter.
 *
 * @param str           the string to split
 * @param delimiter     the delimiting character
 * @return              the string suffix
 */
CPD3CORE_EXPORT std::string suffix(const std::string &str, char delimiter);

CPD3CORE_EXPORT std::string suffix(std::string &&str, char delimiter);

/**
 * Split a string, getting the prefix and suffix before and after a delimiter.
 *
 * @param str           the string to split
 * @param delimiter     the delimiting character
 * @return              the string suffix
 */
CPD3CORE_EXPORT std::pair<std::string, std::string> halves(const std::string &str, char delimiter);

CPD3CORE_EXPORT std::pair<std::string, std::string> halves(std::string &&str, char delimiter);

/**
 * Create a QStringList from a source container of std::string.
 *
 * @tparam Container    the container type
 * @param source        the input values
 * @return              the contents of the container converted to QStrings
 */
template<typename Container>
QStringList to_qstringlist(const Container &source)
{
    struct Convert {
        static inline QString content_to_qstring(const std::string &str)
        { return QString::fromStdString(str); }

        static inline QString content_to_qstring(const QString &str)
        { return str; }
    };
    QStringList result;
    for (const auto &str : source) {
        result.append(Convert::content_to_qstring(str));
    }
    return result;
}

/**
 * Create a set of strings from a source container of QStrings.
 *
 * @tparam Container    the container type
 * @param source        the input values
 * @return              the contents of the container converted to QStrings
 */
template<typename Container>
std::unordered_set<std::string> set_from_qstring(const Container &source)
{
    std::unordered_set<std::string> result;
    for (const auto &str : source) {
        result.insert(str.toStdString());
    }
    return result;
}


}
}

namespace std {

template<>
struct hash<CPD3::Util::ByteArray> {
    inline std::size_t operator()(const CPD3::Util::ByteArray &data) const
    {
        std::size_t result = 0;
        std::size_t index = 0;
        std::size_t limit = data.size();
        for (; (index + sizeof(std::size_t)) <= limit; index += sizeof(std::size_t)) {
            result = CPD3::INTEGER::mix(result, *data.data<const std::size_t *>(index));
        }
        for (; index < limit; ++index) {
            result = CPD3::INTEGER::mix(result, *data.data<const std::uint8_t *>(index));
        }
        return result;
    }
};

}


#endif //CPD3CORE_UTIL_HXX
