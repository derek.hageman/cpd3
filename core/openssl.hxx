/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3COREOPENSSL_HXX
#define CPD3COREOPENSSL_HXX

#include "core/first.hxx"

#include "core/core.hxx"

namespace CPD3 {

/**
 * Basic handling functions for interfacing with OpenSSL.
 */
class CPD3CORE_EXPORT OpenSSL {
public:

    /**
     * Initialize OpenSSL if required.
     */
    static void initialize();
};

}

#endif //CPD3COREOPENSSL_HXX
