/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <mutex>
#include <thread>
#include <unordered_set>
#include <QGlobalStatic>

#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/ssl.h>

#include "core/openssl.hxx"

/** @file core/openssl.hxx
 * Provides interfaces to deal with OpenSSL.
 */

namespace CPD3 {

namespace {
struct Locks {
    std::mutex global;
    bool isInitialized;
    std::mutex *libLocks;

    Locks() : global(), isInitialized(false), libLocks(nullptr)
    { }

    ~Locks()
    {
        delete[] libLocks;
        libLocks = nullptr;
    }
};
}

Q_GLOBAL_STATIC(Locks, locks)

/*
 * OpenSSL eventually removed the callback functions and replaced the macros with
 * no-ops, so just ignore this when it's not used
 */
#pragma GCC diagnostic ignored "-Wunused-function"

static unsigned long opensslThreadID()
{
    return static_cast<unsigned long>(std::hash<std::thread::id>()(std::this_thread::get_id()));
}

static void openSSLLock(int mode, int type, const char *file, int line)
{
    Q_UNUSED(file);
    Q_UNUSED(line);
    auto l = locks();
    if (!l)
        return;
    Q_ASSERT(l->isInitialized);
    Q_ASSERT(l->libLocks);
    if (mode & CRYPTO_LOCK) {
        l->libLocks[type].lock();
    } else {
        l->libLocks[type].unlock();
    }
}

void OpenSSL::initialize()
{
    auto l = locks();
    std::lock_guard<std::mutex> lock(l->global);
    if (l->isInitialized)
        return;
    l->isInitialized = true;

    SSL_library_init();

    ERR_load_crypto_strings();
    SSL_load_error_strings();

    OpenSSL_add_all_algorithms();

    //OPENSSL_config(NULL);
    OPENSSL_no_config();

    l->libLocks = new std::mutex[static_cast<size_t>(CRYPTO_num_locks())];
    CRYPTO_set_id_callback(opensslThreadID);
    CRYPTO_set_locking_callback(openSSLLock);
}

}