/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3_THREADING_HXX
#define CPD3_THREADING_HXX

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>
#include <future>
#include <thread>
#include <atomic>
#include <functional>
#include <string>
#include <memory>
#include <thread>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <cstdint>
#include <QObject>
#include <QCoreApplication>
#include <QEvent>
#include <QTimer>
#include <QThread>
#include <QPointer>
#include <QEventLoop>

#include "core/core.hxx"
#include "number.hxx"

namespace CPD3 {
namespace Threading {


/**
 * Run a functor queued on the associated thread.
 *
 * @tparam Functor  the functor type
 * @param context   the context object to run with
 * @param f         the functor
 */
template<typename Functor>
void runQueuedFunctor(QObject *context, Functor f)
{
    class Event : public QEvent {
    public:
        typedef typename std::decay<Functor>::type FunctorStorage;
    private:
        QPointer<QObject> context;
        FunctorStorage f;
    public:
        Event(QObject *context, FunctorStorage &&in) : QEvent(QEvent::None),
                                                       context(context),
                                                       f(std::move(in))
        { }

        Event(QObject *context, const FunctorStorage &in) : QEvent(QEvent::None),
                                                            context(context),
                                                            f(in)
        { }

        virtual ~Event()
        {
            /* Since ~QObject calls QCoreApplication::removePostedEvents, which in
             * turn destroys all events, we have to catch the case where the
             * context is destroyed before the event is processed.  This does
             * rely on ~QObject setting the flag before the shared data are
             * destroyed, however. */
            if (context.isNull())
                return;
            f();
        }
    };
    QCoreApplication::postEvent(context, new Event(context, std::forward<Functor>(f)));
}

/**
 * Run a functor on the associated thread and block until it completes or the
 * associated object is destroyed.
 *
 * @tparam Functor  the functor type
 * @param context   the context object to run with
 * @param f         the functor
 */
template<typename Functor>
void runBlockingFunctor(QObject *context, Functor f)
{
    Q_ASSERT(context->thread() != QThread::currentThread());
    struct Feedback {
        std::mutex mutex;
        std::condition_variable notify;
        bool complete;

        Feedback() : complete(false)
        { }

        void release()
        {
            std::lock_guard<std::mutex> lock(mutex);
            complete = true;
            notify.notify_all();
        }

        void wait()
        {
            std::unique_lock<std::mutex> lock(mutex);
            notify.wait(lock, [this] { return complete; });
        }
    };
    class Event : public QEvent {
    public:
        typedef typename std::decay<Functor>::type FunctorStorage;
    private:
        Feedback &feedback;
        QPointer<QObject> context;
        FunctorStorage f;
    public:
        Event(Feedback &feedback, QObject *context, FunctorStorage &&in) : QEvent(QEvent::None),
                                                                           feedback(feedback),
                                                                           context(context),
                                                                           f(std::move(in))
        { }

        Event(Feedback &feedback, QObject *context, const FunctorStorage &in) : QEvent(
                QEvent::None), feedback(feedback), context(context), f(in)
        { }

        virtual ~Event()
        {
            /* Since ~QObject calls QCoreApplication::removePostedEvents, which in
             * turn destroys all events, we have to catch the case where the
             * context is destroyed before the event is processed.  This does
             * rely on ~QObject setting the flag before the shared data are
             * destroyed, however. */
            if (context.isNull()) {
                feedback.release();
                return;
            }
            f();
            feedback.release();
        }
    };
    Feedback feedback;
    QCoreApplication::postEvent(context, new Event(feedback, context, std::forward<Functor>(f)));
    return feedback.wait();
}

/**
 * Start a polling operation on the given functor, continuing until it
 * returns false.
 * <br>
 * This function can only be called from the owning thread of the context.
 *
 * @tparam Functor      the functor type
 * @param context       the context object to run with
 * @param f             the functor
 * @param resolution    the polling resolution, in milliseconds
 */
template<typename Functor>
void pollFunctor(QObject *context, Functor f, int resolution = 100)
{
    Q_ASSERT(context->thread() == QThread::currentThread());
    Q_ASSERT(resolution > 0);

    QTimer *timer = new QTimer(context);
    timer->setSingleShot(false);
    timer->setInterval(resolution);

    QObject::connect(timer, &QTimer::timeout, timer, [timer, f] {
        if (f())
            return;

        timer->disconnect();
        timer->deleteLater();
    }, Qt::DirectConnection);

    timer->start();
}

/**
 * Poll a future and call a functor when it becomes ready.
 * <br>
 * This function can only be called from the owning thread of the context.
 *
 * @tparam Future   the future type
 * @tparam Functor  the functor type
 * @param context   the context object to run with
 * @param future    the future to poll
 * @param f         the functor
 * @param resolution    the polling resolution, in milliseconds
 */
template<typename Future, typename Functor>
void pollFuture(QObject *context, Future &&future, Functor f, int resolution = 100)
{
    auto fptr = std::make_shared<Future>(std::move(future));
    return pollFunctor(context, [fptr, f] {
        if (fptr->wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
            return true;
        f(fptr->get());
        return false;
    }, resolution);
}

/**
 * Run polling on a given functor, waiting with an event loop running.  This
 * processes events until the functor returns false.
 * <br>
 * This must be called from a valid QThread.
 *
 * @tparam Functor      the functor type
 * @param f             the functor
 * @param resolution    the polling resolution, in milliseconds
 * @param timeout       maximum time to wait
 * @return              true if the functor was waited for (returned false)
 */
template<typename Functor>
bool pollInEventLoop(Functor f, int resolution = 100, int timeout = -1)
{
    Q_ASSERT(QThread::currentThread() != nullptr);
    Q_ASSERT(resolution > 0);

    if (timeout == 0)
        return !f();

    QEventLoop loop;
    bool didExit = false;

    QTimer pollTimer;
    pollTimer.setSingleShot(false);
    pollTimer.setInterval(resolution);
    pollTimer.start();

    QTimer timeoutTimer;
    if (timeout > 0) {
        timeoutTimer.setSingleShot(true);
        timeoutTimer.setInterval(resolution);
        QObject::connect(&timeoutTimer, &QTimer::timeout, std::bind(&QEventLoop::quit, &loop));
        timeoutTimer.start();
    }

    QObject::connect(&pollTimer, &QTimer::timeout, [&pollTimer, &didExit, &loop, &f] {
        if (f())
            return;

        pollTimer.disconnect();
        loop.quit();
        didExit = true;
    });

    loop.exec();
    return didExit;
}

/**
 * Start a detached function in a QThread.  This is required because Qt provides
 * no simple way to start a detached QThread (even though its internals use
 * detached system threads).
 *
 * @param call  the call to run
 */
void CPD3CORE_EXPORT detachQThread(const std::function<void()> &call);

/** @see detachQThread(const std::function<void()> &) */
void CPD3CORE_EXPORT detachQThread(std::function<void()> &&call);

/**
 * Wait for condition functor to be true within a lock an a timeout.
 *
 * @tparam Condition    the condition functor type
 * @param timeout       the maximum time to wait, in seconds
 * @param mutex         the associated mutex
 * @param notify        the notification condition variable
 * @param c             the functor test
 */
template<typename Condition>
bool waitForTimeout(double timeout, std::mutex &mutex, std::condition_variable &notify, Condition c)
{
    std::unique_lock<std::mutex> lock(mutex);
    if (!FP::defined(timeout)) {
        notify.wait(lock, c);
        return true;
    } else {
        if (timeout <= 0.0)
            return c();
        return notify.wait_for(lock, std::chrono::duration<double>(timeout), c);
    }
}

/**
 * A synchronization mechanism that allows for fast read mostly access at the cost of
 * writes possibly incurring a full copy.
 *
 * @tparam T    the synchronized type
 */
template<typename T>
class ReadCopyLock final {
    std::mutex mutex;

    struct Data final {
        std::atomic_uint_fast32_t references;
        T value;

        Data() : references(1), value()
        { }

        Data(const T &v) : references(1), value(v)
        { }

        Data(const T &v, std::int_fast32_t refs) : references(
                static_cast<decltype(references.load())>(refs)), value(v)
        { }

        Data(T &&v) : references(1), value(std::move(v))
        { }
    };

    Data *data;

public:
    class ReadReference;

private:

    class WriteBase {
        Data *ptr;

        friend class ReadReference;

    protected:
        WriteBase(Data *p) : ptr(p)
        { }

        Data *clear()
        {
            Data *old = ptr;
            ptr = nullptr;
            return old;
        }

    public:
        WriteBase() : ptr(nullptr)
        { }

        WriteBase(const WriteBase &) = delete;

        WriteBase &operator=(const WriteBase &) = delete;

        WriteBase(WriteBase &&other) : ptr(std::move(other.ptr))
        {
            other.ptr = nullptr;
        }

        WriteBase &operator=(WriteBase &&other)
        {
            if (&other == this)
                return *this;
            ptr = std::move(other.ptr);
            other.ptr = nullptr;
            return *this;
        }

        /**
         * Create a read reference that allows for reading even after the write
         * reference is released.
         *
         * @return  a readable reference
         */
        ReadReference read() const
        { return ReadReference(*this); }

        operator const T &() const
        {
            Q_ASSERT(ptr);
            return ptr->value;
        }

        operator T &()
        {
            Q_ASSERT(ptr);
            return ptr->value;
        }

        const T &operator*() const
        {
            Q_ASSERT(ptr);
            return ptr->value;
        }

        T &operator*()
        {
            Q_ASSERT(ptr);
            return ptr->value;
        }

        const T *operator->() const
        {
            Q_ASSERT(ptr);
            return &ptr->value;
        }

        T *operator->()
        {
            Q_ASSERT(ptr);
            return &ptr->value;
        }


        /**
         * Retrieve the value.
         *
         * @return  a read only reference to the value
         */
        const T &value() const
        {
            Q_ASSERT(ptr);
            return ptr->value;
        }

        /**
         * Retrieve the value.
         *
         * @return  a writable reference to the value
         */
        T &value()
        {
            Q_ASSERT(ptr);
            return ptr->value;
        }
    };

public:
    ReadCopyLock() : data(new Data)
    { }

    ReadCopyLock(const T &v) : data(new Data(v))
    { }

    ReadCopyLock(T &&v) : data(new Data(std::move(v)))
    { }

    ReadCopyLock(const ReadCopyLock &) = delete;

    ReadCopyLock &operator=(const ReadCopyLock &) = delete;

    ~ReadCopyLock()
    {
        Q_ASSERT(data);
        Q_ASSERT(data->references.load() == 1);
        delete data;
    }

    /**
     * A readable reference to the data.  This may become out of date if a
     * write occurs concurrently, but will remain valid as long as it is
     * in scope and not released.
     */
    class ReadReference {
        Data *ptr;

    public:
        ReadReference() : ptr(nullptr)
        { }

        ~ReadReference()
        { release(); }

        ReadReference(const ReadReference &other) : ptr(other.ptr)
        {
            ptr->references.fetch_add(1, std::memory_order_acquire);
        }

        ReadReference &operator=(const ReadReference &other)
        {
            if (&other == this)
                return *this;
            release();
            ptr = other.ptr;
            ptr->references.fetch_add(1, std::memory_order_acquire);
            return *this;
        }

        ReadReference(ReadReference &&other) : ptr(std::move(other.ptr))
        {
            other.ptr = nullptr;
        }

        ReadReference &operator=(ReadReference &&other)
        {
            if (&other == this)
                return *this;
            release();
            ptr = std::move(other.ptr);
            other.ptr = nullptr;
            return *this;
        }

        /**
         * Release the read reference, no further access is possible.
         */
        void release()
        {
            if (!ptr)
                return;
            /* If this is the last reference, then we need to delete it */
            if (ptr->references.fetch_sub(1, std::memory_order_acq_rel) == 1)
                delete ptr;
            ptr = nullptr;
        }

        /**
         * Acquire the read reference.
         *
         * @param origin    the originating lock
         */
        ReadReference(const ReadCopyLock<T> &origin)
        {
            ReadCopyLock<T> &o = const_cast<ReadCopyLock<T> &>(origin);
            std::lock_guard<std::mutex> lock(o.mutex);
            ptr = o.data;
            ptr->references.fetch_add(1, std::memory_order_acquire);
        }

        /**
         * Create a read reference from a locked write reference.  This allows
         * for reading even after the write reference has been released.
         *
         * @param write the originating write reference
         */
        explicit ReadReference(const WriteBase &write) : ptr(write.ptr)
        {
            if (!ptr)
                return;
            /* We know the lock is still held, so just increment the reference count */
            ptr->references.fetch_add(1, std::memory_order_acquire);
        }

        operator const T &() const
        {
            Q_ASSERT(ptr);
            return ptr->value;
        }

        const T &operator*() const
        {
            Q_ASSERT(ptr);
            return ptr->value;
        }

        const T *operator->() const
        {
            Q_ASSERT(ptr);
            return &ptr->value;
        }


        /**
         * Retrieve the value.
         *
         * @return  a read only reference to the value
         */
        const T &value() const
        {
            Q_ASSERT(ptr);
            return ptr->value;
        }
    };

    /**
     * Acquire a readable reference.
     *
     * @return a read only reference
     */
    ReadReference read() const
    { return ReadReference(*this); }


    /**
     * A writable reference, which may allow readers to continue using the out
     * of date data.
     */
    class WriteReference : public WriteBase {
        ReadCopyLock<T> *lock;

        static Data *acquire(ReadCopyLock<T> &lock)
        {
            lock.mutex.lock();

            Data *ptr = lock.data;
            if (ptr->references.fetch_add(1, std::memory_order_acq_rel) != 1) {
                /* If there was an outstanding reference, then we know there's a reader active,
                 * so we need to make a copy */
                Data *n = new Data(ptr->value, 2);
                /* Now we release the reference we had and the reference the lock had,
                 * which may (now) be the last one, so we may have to delete it */
                if (ptr->references.fetch_sub(2, std::memory_order_acq_rel) == 2)
                    delete ptr;
                ptr = n;
                lock.data = ptr;
            }

            return ptr;
        }

    public:
        WriteReference() : WriteBase(), lock(nullptr)
        { }

        ~WriteReference()
        { release(); }

        WriteReference(const WriteReference &) = delete;

        WriteReference &operator=(const WriteReference &) = delete;

        WriteReference(WriteReference &&other) : WriteBase(std::move(other)),
                                                 lock(std::move(other.lock))
        {
            other.lock = nullptr;
        }

        WriteReference &operator=(WriteReference &&other)
        {
            if (&other == this)
                return *this;
            release();
            WriteBase::operator=(std::move(other));
            lock = std::move(other.lock);
            other.lock = nullptr;
            return *this;
        }

        /**
         * Release the write reference, no further access is possible.
         */
        void release()
        {
            Data *ptr = WriteBase::clear();
            if (!ptr)
                return;
            Q_ASSERT(lock);

            /* We know there's at least one other reference to it, since the only way the
             * this->data reference can be released is from within the mutex.  So that means
             * that there cannot have been another release of it yet (we still hold the mutex).
             * That means we don't have to check this, and can use release ordering. */
            ptr->references.fetch_sub(1, std::memory_order_release);
            lock->mutex.unlock();
            lock = nullptr;
        }

        /**
         * Acquire the write reference.
         *
         * @param origin    the originating lock
         */
        WriteReference(ReadCopyLock<T> &origin) : WriteBase(acquire(origin)), lock(&origin)
        { }
    };

    /**
     * Acquire a writeable reference.
     *
     * @return a writeable reference
     */
    WriteReference write()
    { return WriteReference(*this); }

    /**
     * An exclusive write reference, ensuring no readers are active while it is held.
     */
    class WriteExclusive : public WriteBase {
        ReadCopyLock<T> *lock;

        static Data *acquire(ReadCopyLock<T> &lock)
        {
            /* Spin until there there are no active readers, further ones will be
             * excluded because we hold the lock */
            for (;;) {
                lock.mutex.lock();
                Data *ptr = lock.data;

                typedef decltype(ptr->references.load()) ExchangeType;
                ExchangeType expected;
                expected = 1;
                if (ptr->references.compare_exchange_strong(expected, 2, std::memory_order_acq_rel))
                    return ptr;
                lock.mutex.unlock();
                /* XXX: We want to tell the scheduler to wait for references == 1, but no
                 * direct (cross platform) way to do that.  This is should be rare, so probably
                 * not a huge problem. */
                std::this_thread::sleep_for(std::chrono::milliseconds(1));
            }
            Q_ASSERT(false);
            return nullptr;
        }

    public:
        WriteExclusive() : WriteBase(), lock(nullptr)
        { }

        ~WriteExclusive()
        { release(); }

        WriteExclusive(const WriteExclusive &) = delete;

        WriteExclusive &operator=(const WriteExclusive &) = delete;

        WriteExclusive(WriteExclusive &&other) : WriteBase(std::move(other)),
                                                 lock(std::move(other.lock))
        {
            other.lock = nullptr;
        }

        WriteExclusive &operator=(WriteExclusive &&other)
        {
            if (&other == this)
                return *this;
            release();
            WriteBase::operator=(std::move(other));
            lock = std::move(other.lock);
            other.lock = nullptr;
            return *this;
        }

        /**
         * Release the write reference, no further access is possible.
         */
        void release()
        {
            Data *ptr = WriteBase::clear();
            if (!ptr)
                return;
            Q_ASSERT(lock);

            /* We know there's exactly two references to it.  So that means
            * that there cannot have been another release of it yet (we still hold the mutex).
            * That means we don't have to check this, and can use release ordering. */
            ptr->references.fetch_sub(1, std::memory_order_release);
            lock->mutex.unlock();
            lock = nullptr;
        }

        /**
         * Acquire the write reference.
         *
         * @param origin    the originating lock
         */
        WriteExclusive(ReadCopyLock<T> &origin) : WriteBase(acquire(origin)), lock(&origin)
        { }
    };

    /**
     * Acquire a writeable reference with exclusive access (no readers active).
     *
     * @return a writeable reference
     */
    WriteExclusive exclusive()
    { return WriteExclusive(*this); }


    /**
     * A writeable reference that allows readers to continue with the out of date data
     * until it is released, which waits until all readers have finished with the
     * old data.
     */
    class WritePropagate : public WriteBase {
        ReadCopyLock<T> *lock;
        Data *outstanding;

        static std::pair<Data *, Data *> acquire(ReadCopyLock<T> &lock)
        {
            lock.mutex.lock();

            Data *ptr = lock.data;
            Data *prior = nullptr;
            if (ptr->references.fetch_add(1, std::memory_order_acq_rel) != 1) {
                /* If there was an outstanding reference, then we know there's a reader active,
                 * so we need to make a copy */
                Data *n = new Data(ptr->value, 2);
                /* Now release the reference the lock held, which leaves whatever had the reference
                 * (if anything) and our reference now.  So that means we don't have to check this
                 * and can use release ordering.  We can't release both references, because
                 * we need it to continue to exist so we can check it later. */
                ptr->references.fetch_sub(1, std::memory_order_release);
                prior = ptr;
                ptr = n;
                lock.data = ptr;
            }

            return std::make_pair(ptr, prior);
        }

        WritePropagate(ReadCopyLock<T> &origin, const std::pair<Data *, Data *> &acquired)
                : WriteBase(acquired.first), lock(&origin), outstanding(acquired.second)
        { }

        bool maybeReleaseOutstanding()
        {
            if (!outstanding)
                return true;
            if (outstanding->references.load(std::memory_order_acquire) != 1)
                return false;
            delete outstanding;
            outstanding = nullptr;
            return true;
        }

    public:
        WritePropagate() : WriteBase(), lock(nullptr), outstanding(nullptr)
        { }

        ~WritePropagate()
        {
            release();
            Q_ASSERT(!outstanding);
        }

        WritePropagate(const WritePropagate &) = delete;

        WritePropagate &operator=(const WritePropagate &) = delete;

        WritePropagate(WritePropagate &&other) : WriteBase(std::move(other)),
                                                 lock(std::move(other.lock)),
                                                 outstanding(std::move(other.outstanding))
        {
            other.lock = nullptr;
            other.outstanding = nullptr;
        }

        WritePropagate &operator=(WritePropagate &&other)
        {
            if (&other == this)
                return *this;
            release();
            WriteBase::operator=(std::move(other));
            lock = std::move(other.lock);
            outstanding = std::move(other.outstanding);
            other.lock = nullptr;
            other.outstanding = nullptr;
            return *this;
        }

        /**
         * Release the write reference, no further access is possible.
         */
        void release()
        {
            Data *ptr = WriteBase::clear();
            if (!ptr) {
                Q_ASSERT(!outstanding);
                return;
            }
            Q_ASSERT(lock);

            maybeReleaseOutstanding();

            /* First see if we can quickly just release, if we initially have the only
             * two references to it, that means no readers are active (any further ones
             * are still waiting on the lock) */
            if (ptr->references.fetch_sub(1, std::memory_order_acq_rel) == 1 && !outstanding) {
                lock->mutex.unlock();
                lock = nullptr;
                return;
            }

            /* There's at least one reader active now, so we wait until there is only one
             * reference to it (meaning the lock master).  It's also safe to release the
             * lock here, since we've already propagated our changes, so the semantics
             * are preserved. */
            for (;;) {
                lock->mutex.unlock();
                /* XXX: We want to tell the scheduler to wait for references == 1/0 (data deleted),
                 * but no direct (cross platform) way to do that.  This is should be
                 * rare, so probably not a huge problem. */
                std::this_thread::sleep_for(std::chrono::milliseconds(1));
                lock->mutex.lock();

                /* If there's still an outstanding one, just keep waiting for it */
                if (!maybeReleaseOutstanding())
                    continue;

                /* Re-read the data, since another write may have progressed while we
                 * where unlocked and changed it */
                ptr = lock->data;
                if (ptr->references.load(std::memory_order_acquire) == 1)
                    break;
            }

            lock->mutex.unlock();
            lock = nullptr;
            Q_ASSERT(!outstanding);
        }

        /**
         * Acquire the write reference.
         *
         * @param origin    the originating lock
         */
        WritePropagate(ReadCopyLock<T> &origin) : WritePropagate(origin, acquire(origin))
        { }
    };

    /**
     * Acquire a writeable reference that ensures no readers are active after it is released.
     *
     * @return a writeable reference
     */
    WritePropagate propagate()
    { return WritePropagate(*this); }
};

namespace Internal {
struct CPD3CORE_EXPORT Sender {

public:
    Sender();

    virtual ~Sender();

    virtual void release(uint_fast32_t id) = 0;

    virtual void releaseImmediate(uint_fast32_t id) = 0;
};
}

template<typename... Args>
class Signal;

/**
 * A signal-slot connection reference.
 */
class CPD3CORE_EXPORT Connection final {
    std::weak_ptr<Internal::Sender> sender;
    uint_fast32_t id;

    template<typename... Args> friend
    class Signal;

    template<typename T>
    Connection(const std::shared_ptr<T> &s, uint_fast32_t i) : sender(
            std::static_pointer_cast<Internal::Sender>(s)), id(i)
    { }

public:
    Connection();

    Connection(const Connection &);

    Connection &operator=(const Connection &);

    Connection(Connection &&other);

    Connection &operator=(Connection &&other);

    /**
     * Break the connection, preventing the call associated with it.  This
     * will wait for any in progress calls to complete, so it cannot
     * be used from within such a call.
     */
    void disconnect();

    /**
     * Break the connection, preventing the call associated with it.  This
     * does not wait for any any progress emissions to complete, so it it safe to
     * call from within a connection.
     */
    void disconnectImmediate();

    /**
     * Test if the connection is valid.  This is only approximate in that
     * the connection may have been broken, but it will never return true
     * while the call is still possible.
     *
     * @return  true if the connection can still be called.
     */
    bool valid() const;
};

class DeferredReceiver;

/**
 * A signal receiver.  This is normally used as a parent of a receiving class, but can
 * also be used stand alone for scoping control.
 */
class CPD3CORE_EXPORT Receiver {
    std::mutex mutex;
    std::list<Connection> connections;

    template<typename... Args> friend
    class Signal;

    friend class DeferredReceiver;

    void purgeInvalid();

public:
    Receiver();

    virtual ~Receiver();

    Receiver(const Receiver &) = delete;

    Receiver &operator=(const Receiver &) = delete;

    /**
     * Disconnect all connected signals.  After this returns, no further calls
     * into any connected contexts are possible.  As such, this cannot be
     * used from within a connection.
     */
    void disconnect();

    /**
     * Disconnect all connected signals.  This does not wait for any any progress
     * emissions to complete, so it it safe to call from within a connection.
     */
    void disconnectImmediate();
};

namespace Internal {
class CPD3CORE_EXPORT ReceiverContextQObject final : private QObject {
    Connection connection;
public:
    ReceiverContextQObject(const Connection &connection, QObject *parent);

    virtual ~ReceiverContextQObject();
};
}

/**
 * A simple signal that connects to various receiver types.
 *
 * @tparam Args     the signal arguments
 */
template<typename... Args>
class Signal final {

    friend class Receiver;

    typedef std::function<void(Args...)> Call;

    struct Shared final : public Internal::Sender {
        struct Connections {
            uint_fast32_t id;
            std::unordered_map<uint_fast32_t, Call> calls;

            Connections() : id(0), calls()
            { }
        };

        ReadCopyLock<Connections> connections;

        uint_fast32_t connect(const std::function<void(Args...)> &func)
        {
            auto lock = connections.write();
            uint_fast32_t result = ++lock->id;
            lock->calls.emplace(result, func);
            return result;
        }

        uint_fast32_t connect(std::function<void(Args...)> &&func)
        {
            auto lock = connections.write();
            uint_fast32_t result = ++lock->id;
            lock->calls.emplace(result, std::move(func));
            return result;
        }

        virtual ~Shared()
        { }

        virtual void release(uint_fast32_t id)
        {
            /* Use a propagate lock, so we know no readers remain using it
             * after we return */
            auto lock = connections.propagate();
            lock->calls.erase(id);
        }

        virtual void releaseImmediate(uint_fast32_t id)
        {
            auto lock = connections.write();
            lock->calls.erase(id);
        }

        template<typename... Refs>
        void e(Refs &&... args) const
        {
            auto current = connections.read();
            for (auto s = current->calls.cbegin(), endS = current->calls.cend(); s != endS; ++s) {
                s->second(std::forward<Refs>(args)...);
            }
        }
    };

    std::shared_ptr<Shared> shared;

public:
    Signal() : shared(std::make_shared<Shared>())
    { }

    Signal(const Signal &other) : shared(other.shared)
    { }

    Signal &operator=(const Signal &other)
    {
        shared = other.shared;
        return *this;
    }

    Signal(Signal &&other) : shared(std::move(other.shared))
    { }

    Signal &operator=(Signal &&other)
    {
        shared = std::move(other.shared);
        return *this;
    }

    /**
     * Emit the signal.
     *
     * @tparam Refs the argument types
     * @param args  the arguments
     */
    template<typename... Refs>
    inline void e(Refs &&... args) const
    { shared->e(std::forward<Refs>(args)...); }


    /**
     * Emit the signal.
     *
     * @tparam Refs the argument types
     * @param args  the arguments
     */
    template<typename... Refs>
    inline void operator()(Refs &&... args) const
    { shared->e(std::forward<Refs>(args)...); }

    /**
     * Connect the signal to a function directly.
     *
     * @param func  the function to call
     * @return      a connection reference
     */
    Connection connect(const std::function<void(Args...)> &func)
    { return Connection(shared, shared->connect(func)); }

    /** @see connect(const std::function<void(Args...)> & */
    Connection connect(std::function<void(Args...)> &&func)
    { return Connection(shared, shared->connect(std::move(func))); }


    /**
     * Connect the signal to a function with a receiver context.
     *
     * @param context   the context of the call
     * @param func      the function to call
     * @return          a connection reference
     */
    Connection connect(Receiver &context, const std::function<void(Args...)> &func)
    {
        auto c = connect(func);
        std::lock_guard<std::mutex> lock(context.mutex);
        context.purgeInvalid();
        context.connections.emplace_back(c);
        return c;
    }

    /** @see connect(Receiver &, const std::function<void(Args...)> & */
    Connection connect(Receiver &context, std::function<void(Args...)> &&func)
    {
        auto c = connect(std::move(func));
        std::lock_guard<std::mutex> lock(context.mutex);
        context.purgeInvalid();
        context.connections.emplace_back(c);
        return c;
    }

    /**
     * Connect the signal to a function, with a QObject as the context
     *
     * @param context   the context object
     * @param func      the function to call
     * @param queued    true to delay the call to the context's thread's event loop
     * @return          a connection reference
     */
    Connection connect(QObject *context,
                       const std::function<void(Args...)> &func,
                       bool queued = false)
    {
        if (!queued) {
            auto c = connect(func);
            new Internal::ReceiverContextQObject(c, context);
            return c;
        }

        auto c = connect([=](Args...args) {
            runQueuedFunctor(context, std::bind(func, std::forward<Args>(args)...));
        });
        new Internal::ReceiverContextQObject(c, context);
        return c;
    }

    /** @see connect(QObject *, const std::function<void(Args...)> &, bool) */
    Connection connect(QObject *context, std::function<void(Args...)> &&func, bool queued = false)
    {
        if (!queued) {
            auto c = connect(std::move(func));
            new Internal::ReceiverContextQObject(c, context);
            return c;
        }

        auto c = connect([=](Args...args) {
            runQueuedFunctor(context, std::bind(func, std::forward<Args>(args)...));
        });
        new Internal::ReceiverContextQObject(c, context);
        return c;
    }

    /**
     * Connect the signal to a void call of a method via Qt's introspection.  The
     * method must be available to QMetaObject::invokeMethod().
     *
     * @param context       the context object
     * @param method        the method name
     * @param type          the type of the call
     * @return              a connection reference
     */
    Connection connect(QObject *context,
                       const char *method,
                       Qt::ConnectionType type = Qt::QueuedConnection)
    {
        std::string mn(method);
        auto c = connect([=] {
            QMetaObject::invokeMethod(context, mn.data(), type);
        });
        new Internal::ReceiverContextQObject(c, context);
        return c;
    }

    /**
     * Disconnect all connections to the signal.  This will wait for any
     * any progress emissions to finish, so it cannot be used from within
     * a connected call.
     */
    void disconnect()
    {
        auto lock = shared->connections.propagate();
        lock->calls.clear();
    }

    /**
     * Disconnect all connections to the signal.  This does not wait for any any progress
     * emissions to complete, so it it safe to call from within a connection.
     */
    void disconnectImmediate()
    {
        auto lock = shared->connections.write();
        lock->calls.clear();
    }

    /**
     * A deferred emission status.  This emits the signal when it is destroyed
     * or explicitly invoked.
     * <br>
     * This also allows for destruction or replacement of the signal during
     * or before emission.  So it can be constructed and immediately destroyed
     * to handle cases where the connected functions may destroy the signal
     * being emitted (e.x. signal.defer() instead of signal() ).
     * <br>
     * Moving a deferred emission invalidates the source.  That is, only
     * a single emit will happen for every deferred construction.
     */
    class Deferred {
        typedef std::function<void()> Call;
        Call call;

        /* Can't use forwarding references here, since the binding gets messed up */
        template<typename... Refs>
        static void doCall(const std::shared_ptr<typename Signal<Args...>::Shared> &shared,
                           Refs... args)
        {
            auto connections = shared->connections.read();
            for (auto s = connections->calls.cbegin(), endS = connections->calls.cend();
                    s != endS;
                    ++s) {
                s->second(std::forward<Refs>(args)...);
            }
        }

    public:
        /**
         * Construct a deferred signal emission.
         *
         * @tparam Refs     the argument types
         * @param signal    the signal to emit
         * @param args      the signal arguments
         */
        template<typename... Refs>
        Deferred(Signal<Args...> &signal, Refs &&... args) : call(
                std::bind(&doCall<Refs...>, signal.shared, std::forward<Refs>(args)...))
        { }

        ~Deferred()
        {
            if (call)
                call();
        }

        Deferred() = default;

        Deferred(const Deferred &) = delete;

        Deferred &operator=(const Deferred &) = delete;

        Deferred(Deferred &&other) : call(std::move(other.call))
        {
            other.call = Call();
        }

        Deferred &operator=(Deferred &&other)
        {
            if (&other == this)
                return *this;
            call = std::move(other.call);
            other.call = Call();
            return *this;
        }

        /**
         * Emit the signal now.
         *
         * @param clear clear the emission status, and do not emit again
         */
        void now(bool clear = true)
        {
            if (call)
                call();
            if (clear)
                call = Call();
        }

        /**
         * Clear the emission status and do not emit the signal.
         */
        void never()
        { call = Call(); }
    };

    friend class Deferred;

    /**
     * Defer emission the signal.
     *
     * @tparam Refs the argument types
     * @param args  the arguments
     * @return      a deferred signal emitter
     */
    template<typename... Refs>
    Deferred defer(Refs &&... args)
    { return Deferred(*this, std::forward<Refs>(args)...); }



    /* When it's available in a future standard, the .then() method of futures would make these
     * trivial */

    /**
     * Emit the signal when a future is ready.
     *
     * @tparam Refs     the argument types
     * @param future    the future to wait for
     */
    template<typename... Refs>
    void future(std::future<Refs...> &&future)
    {
        auto fptr = std::make_shared<std::future<Refs...>>(std::move(future));
        auto sptr = shared;
        std::thread([fptr, sptr] {
            sptr->e(fptr->get());
        }).detach();
    }

    /** @see future(std::future<Refs...> &&) */
    void future(std::future<void> &&future)
    {
        auto fptr = std::make_shared<std::future<void>>(std::move(future));
        auto sptr = shared;
        std::thread([fptr, sptr] {
            fptr->get();
            sptr->e();
        }).detach();
    }

    /* Fails on GCC */
#if 0
    /**
     * Emit the signal when a future is ready, but using the specified arguments
     *
     * @tparam Refs     the argument types
     * @param future    the future to wait for
     */
    template<typename Future, typename... Refs>
    void future(Future &&future, Refs... args)
    {
        auto fptr = std::make_shared<Future>(std::move(future));
        auto sptr = shared;
        std::thread([fptr, sptr, args...] {
            fptr->wait();
            sptr->e(args...);
        }).detach();
    }

    /**
     * Emit the signal after a test condition has completed (in a separate thread).
     *
     * @tparam Refs     the argument types
     * @param future    the future to wait for
     */
    template<typename Condition, typename... Refs>
    void after(Condition test, Refs... args)
    {
        auto sptr = shared;
        std::thread([test, sptr, args...] {
            test();
            sptr->e(args...);
        }).detach();
    }
#else

    /**
     * Emit the signal after a test condition has completed (in a separate thread).
     *
     * @tparam Refs     the argument types
     * @param future    the future to wait for
     */
    template<typename Condition>
    void after(Condition test)
    {
        auto sptr = shared;
        std::thread([test, sptr] {
            test();
            sptr->e();
        }).detach();
    }

#endif

    /**
     * Wait for the signal to be emitted.  An optional test can be provided to stop the
     * wait if the signal has already been detected as emitted by another source.
     *
     * @param alreadyEmitted    if set, this is called before actually waiting and the wait aborted if it returns true
     * @param timeout           the maximum time to wait, in seconds
     * @return                  true if the signal was emitted before the timeout
     */
    bool wait(const std::function<bool()> &alreadyEmitted = {}, double timeout = FP::undefined())
    {
        struct Context {
            std::mutex mutex;
            std::condition_variable notify;
            bool emitted;

            Context() : emitted(false)
            { }

            void received()
            {
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    emitted = true;
                }
                notify.notify_all();
            }

            bool wait(double timeout)
            { return waitForTimeout(timeout, mutex, notify, [this] { return emitted; }); }
        };
        Context ctx;
        auto conn = connect(std::bind(&Context::received, &ctx));
        if (alreadyEmitted && alreadyEmitted()) {
            conn.disconnect();
            return true;
        }
        bool result = ctx.wait(timeout);
        conn.disconnect();
        return result;
    }

    /**
     * Wait for the signal to be emitted.  An optional test can be provided to stop the
     * wait if the signal has already been detected as emitted by another source.
     *
     * @param mutex             a mutex (unlocked) to use during waiting
     * @param notify            a condition variable to use with the mutex
     * @param alreadyEmitted    if set, this is called before actually waiting and the wait aborted if it returns true
     * @param timeout           the maximum time to wait, in seconds
     * @param lockDuringTest    hold the lock during the call to alreadyEmitted()
     * @return                  true if the signal was emitted before the timeout
     * @see wait(const std::function<bool()> &, double)
     */
    bool wait(std::mutex &mutex,
              std::condition_variable &notify,
              const std::function<bool()> &alreadyEmitted = {},
              double timeout = FP::undefined(),
              bool lockDuringTest = false)
    {
        struct Context {
            std::mutex &mutex;
            std::condition_variable &notify;
            bool emitted;

            Context(std::mutex &mutex, std::condition_variable &cv) : mutex(mutex),
                                                                      notify(cv),
                                                                      emitted(false)
            { }

            void received()
            {
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    emitted = true;
                }
                notify.notify_all();
            }

            bool wait(double timeout)
            { return waitForTimeout(timeout, mutex, notify, [this] { return emitted; }); }
        };
        Context ctx(mutex, notify);
        auto conn = connect(std::bind(&Context::received, &ctx));
        if (alreadyEmitted) {
            if (lockDuringTest) {
                std::unique_lock<std::mutex> lock(mutex);
                if (alreadyEmitted()) {
                    lock.unlock();
                    conn.disconnect();
                    return true;
                }
            } else {
                if (alreadyEmitted()) {
                    conn.disconnect();
                    return true;
                }
            }
        }
        bool result = ctx.wait(timeout);
        conn.disconnect();
        return result;
    }

    /**
     * Wait for the signal to be emitted.  An optional test can be provided to stop the
     * wait if the signal has already been detected as emitted by another source.
     * <br>
     * This version runs a Qt event loop to do the wait.
     *
     * @param alreadyEmitted    if set, this is called before actually waiting and the wait aborted if it returns true
     * @param timeout           the maximum time to wait, in seconds
     * @return                  true if the signal was emitted before the timeout
     * @see wait(const std::function<bool()> &, double)
     */
    bool waitInEventLoop(const std::function<bool()> &alreadyEmitted = {},
                         double timeout = FP::undefined())
    {
        Q_ASSERT(QThread::currentThread() != nullptr);

        struct Context {
            QEventLoop loop;
            bool emitted;

            Context() : emitted(false)
            { }

            void received()
            {
                emitted = true;
                loop.quit();
            }

            void wait(double timeout)
            {
                QTimer timeoutTimer;
                if (FP::defined(timeout) && timeout >= 0.0) {
                    timeoutTimer.setSingleShot(true);
                    timeoutTimer.setInterval(static_cast<int>(std::ceil(timeout * 1000.0)));
                    QObject::connect(&timeoutTimer, &QTimer::timeout,
                                     std::bind(&QEventLoop::quit, &loop));
                    timeoutTimer.start();
                }
                loop.exec();
            }
        };

        Context ctx;
        auto conn = connect(&ctx.loop, std::bind(&Context::received, &ctx), true);
        if (alreadyEmitted && alreadyEmitted()) {
            conn.disconnect();
            return true;
        }
        ctx.wait(timeout);
        conn.disconnect();
        return ctx.emitted;
    }
};

/**
 * A receiver that defers processing until either its destruction
 * or an explicit time.
 */
class CPD3CORE_EXPORT DeferredReceiver : public Receiver {
    typedef std::function<void()> Call;
    std::vector<Call> calls;
public:
    DeferredReceiver();

    DeferredReceiver(const DeferredReceiver &) = delete;

    DeferredReceiver &operator=(const DeferredReceiver &) = delete;

    virtual ~DeferredReceiver();

    /**
     * Call all pending functions.
     *
     * @return true if anything was called
     */
    bool call();

    /**
     * Ignore all pending functions.
     */
    void ignore();

    /**
     * Connect the deferred receiver to a signal.
     *
     * @tparam Args     the signal arguments
     * @param signal    the signal
     * @param func      the function to call
     */
    template<typename F, typename... Args>
    void connect(Signal<Args...> &signal, F func)
    {
        signal.connect(*this, [this, func](Args...args) {
            auto f = std::bind(func, std::forward<Args>(args)...);
            std::lock_guard<std::mutex> lock(mutex);
            calls.emplace_back(std::move(f));
        });
    }

    /**
     * Queue a function to be called as if it has been emitted.
     *
     * @param f the function to call
     */
    void queue(std::function<void()> f)
    {
        std::lock_guard<std::mutex> lock(mutex);
        calls.emplace_back(std::move(f));
    }
};

/**
 * A simple thread pooled runner.  Threads are preserved for re-use for a limited amount
 * of time while the container exists.  It also permits destruction of the
 * container object while continuing to run any queued tasks.
 * Tasks are executed in submission order.
 */
class CPD3CORE_EXPORT PoolRun {
    struct Shared {
        std::mutex mutex;
        std::condition_variable cv;
        std::size_t runningThreads;
        std::size_t threadShutdown;
        std::queue<std::function<void()>> tasks;

        Shared();
    };

    std::shared_ptr<Shared> shared;
    std::size_t maximumThreads;

    static void process(const std::shared_ptr<Shared> &shared);

    bool maybeLaunch();

public:

    /**
     * Create the pooled runner with the system ideal concurrency.
     */
    PoolRun();

    /**
     * Create the pooled runner with the specified number of
     * maximum threads.
     *
     * @param threadCount   the maximum number of threads or zero for unlimited
     */
    explicit PoolRun(std::size_t threadCount);

    ~PoolRun();

    /**
     * Wait for the pool to complete all tasks (all threads
     * exited).
     */
    void wait();

    /**
     * Start a task.
     *
     * @param task  the task
     */
    void run(const std::function<void()> &task);

    /**
     * Start a task.
     *
     * @param task  the task
     */
    void run(std::function<void()> &&task);
};

}
}

#endif //CPD3_THREADING_HXX
