/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QLoggingCategory>
#include <QGlobalStatic>

#include "core/waitutils.hxx"
#include "core/threadpool.hxx"


Q_LOGGING_CATEGORY(log_core_threadpool, "cpd3.core.threadpool", QtWarningMsg)

namespace CPD3 {


/** @file core/threadpool.hxx
 * A simple thread pool.
 */


ThreadPool::ThreadPool(int tc) : threadCount(tc),
                                 mutex(),
                                 threadNotify(),
                                 poolNotify(),
                                 poolEmptyInProgress(false),
                                 threads(),
                                 ended(),
                                 idle(),
                                 blocked(),
                                 tasks()
{
    if (threadCount < 1)
        threadCount = 1;
}

ThreadPool::~ThreadPool()
{
    wait();
    Q_ASSERT(threads.empty());
    Q_ASSERT(ended.empty());
}

ThreadPool::Thread::Thread(ThreadPool *p) : pool(p), blockingCount(0), immediateTask()
{ }

ThreadPool::Thread::~Thread()
{ }

void ThreadPool::Thread::run()
{ pool->threadExecute(this); }


void ThreadPool::wait()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
#ifdef Q_OS_WIN32
        {
            /* Windows weirdness seems to abort threads randomly during destruction, so we can't
             * count on consistent state */
            for (auto t = threads.begin(); t != threads.end(); ) {
                if ((*t)->wait(0)) {
                    blocked.erase(*t);
                    ended.erase(*t);
                    idle.erase(*t);
                    delete *t;
                    t = threads.erase(t);
                } else {
                    ++t;
                }
            }
        }
#endif
        reapAllThreads();
        if (threads.empty())
            break;

        poolEmptyInProgress = true;
        threadNotify.notify_all();
        poolNotify.wait(lock);
    }
    poolNotify.notify_all();
    poolEmptyInProgress = false;
}

void ThreadPool::maybeStartThread()
{
    if (totalRunningThreads() >= threadCount)
        return;
    Thread *t = new Thread(this);
    threads.insert(t);
    t->setObjectName("PooledQThread");
    t->start();

    reapAllThreads();
}

void ThreadPool::run(const std::function<void()> &f)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        tasks.push(f);

        /* See if we can just wake up an idle one, rather than trying to start a new one */
        if (!idle.empty()) {
            auto i = idle.begin();
            Q_ASSERT((*i)->immediateTask == NULL);
            Q_ASSERT(ended.count(*i) == 0);
            Q_ASSERT(blocked.count(*i) == 0);
            /* This will be reset from the wakeAll, so this is fine to "allocate" even if we
             * don't end up using this particular thread. */
            idle.erase(i);
        } else {
            maybeStartThread();
        }
    }
    threadNotify.notify_all();
}

void ThreadPool::immediate(const std::function<void()> &f)
{
    std::unique_lock<std::mutex> lock(mutex);

    /* See if we can just wake up an idle one, rather than trying to start a new one */
    if (!idle.empty()) {
        auto i = idle.begin();
        Q_ASSERT((*i)->immediateTask == NULL);
        Q_ASSERT(ended.count(*i) == 0);
        Q_ASSERT(blocked.count(*i) == 0);
        (*i)->immediateTask = f;
        idle.erase(i);
        lock.unlock();
        threadNotify.notify_all();
        return;
    }

    Thread *t = new Thread(this);
    t->immediateTask = f;
    threads.insert(t);
    t->setObjectName("PooledQThread");
    t->start();

    reapAllThreads();
}

void ThreadPool::run(QRunnable *r)
{
    run([r]() -> void {
        bool shouldDelete = r->autoDelete();
        r->run();
        /* Can't access the task directly anymore (so we can't call autoDelete() here), because
         * the caller can at this point assume there are no more references to it, if autoDelete()
         * was false.  That is, the task may have been deleted by another source by now. */
        if (shouldDelete)
            delete r;
    });
}

void ThreadPool::immediate(QRunnable *r)
{
    immediate([r]() -> void {
        bool shouldDelete = r->autoDelete();
        r->run();
        /* Can't access the task directly anymore (so we can't call autoDelete() here), because
         * the caller can at this point assume there are no more references to it, if autoDelete()
         * was false.  That is, the task may have been deleted by another source by now. */
        if (shouldDelete)
            delete r;
    });
}

Q_GLOBAL_STATIC(ThreadPool, systemInstance)

ThreadPool *ThreadPool::system()
{
    return systemInstance();
}

void ThreadPool::threadExecute(Thread *thread)
{
    ElapsedTimer sinceLastWork;
    sinceLastWork.start();

    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        if (thread->immediateTask) {
            std::function<void()> task = thread->immediateTask;
            lock.unlock();

            task();

            sinceLastWork.start();
            lock.lock();
            thread->immediateTask = std::function<void()>();

            if (thread->blockingCount > 0) {
                qCWarning(log_core_threadpool) << "Unmatched ThreadPool::block() call";
                Q_ASSERT(blocked.count(thread) > 0);
                blocked.erase(thread);
            }
#ifndef NDEBUG
            else {
                Q_ASSERT(blocked.count(thread) == 0);
            }
#endif
            thread->blockingCount = 0;
            continue;
        }

        if (totalRunningThreads() > threadCount)
            break;

        if (!tasks.empty()) {
            auto task = std::move(tasks.front());
            tasks.pop();
            lock.unlock();

            task();

            sinceLastWork.start();
            lock.lock();

            if (thread->blockingCount > 0) {
                qCWarning(log_core_threadpool) << "Unmatched ThreadPool::block() call";
                Q_ASSERT(blocked.count(thread) > 0);
                blocked.erase(thread);
            }
#ifndef NDEBUG
            else {
                Q_ASSERT(blocked.count(thread) == 0);
            }
#endif
            thread->blockingCount = 0;
            continue;
        }

        Q_ASSERT(blocked.count(thread) == 0);

        if (poolEmptyInProgress)
            break;

        int remaining = 30000 - (int) sinceLastWork.elapsed();
        if (remaining <= 0)
            break;
        idle.insert(thread);
        threadNotify.wait_for(lock, std::chrono::milliseconds(remaining));
        idle.erase(thread);
    }

    ended.insert(thread);
    threads.erase(thread);
    Q_ASSERT(idle.count(thread) == 0);
    Q_ASSERT(blocked.count(thread) == 0);
    poolNotify.notify_all();
}

void ThreadPool::reapAllThreads()
{
    if (ended.empty())
        return;
    for (const auto &t : ended) {
        Q_ASSERT(t != QThread::currentThread());
        Q_ASSERT(blocked.count(t) == 0);
        Q_ASSERT(idle.count(t) == 0);
        Q_ASSERT(threads.count(t) == 0);

        /*
         * Unfortunately there is a racey warning in QThread about a thread trying to wait
         * on itself.  This happens because Qt threads are create detached, so the
         * pthread_t identifier returned by pthread_self can freely be reused as soon as
         * the thread exits (i.e. nothing QThread actually de-allocates it).  So in cases
         * where lots of threads are spawned, there may still be QThread's with references
         * to reused ones that haven't "waited" (including destruction).  I don't
         * see any real way around this, but it doesn't actually crash things (since
         * Qt uses a reference counted data pointer), so we just live with it.  A simple
         * "real" fix would be changing the QThread::wait() function to always succeed
         * if the finished (i.e. the cleanup call is done before the pthread_t can be
         * reused).
         */

        t->wait();
        delete t;
    }
    ended.clear();
}

int ThreadPool::defaultNumberOfThreads()
{
    {
        QByteArray p(qgetenv("CPD3_POOLEDTHREADCOUNT"));
        if (!p.isEmpty()) {
            bool ok = false;
            int threadCount = p.trimmed().toInt(&ok);
            if (ok && threadCount > 0) {
                return threadCount;
            }
        }
    }

    return QThread::idealThreadCount();
}

};
