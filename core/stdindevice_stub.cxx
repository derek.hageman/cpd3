/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QIODevice>
#include <QThread>

#include "core/stdindevice.hxx"

namespace CPD3 {

StdinDevice::StdinDevice(QObject *parent) : QIODevice(parent)
{ }

StdinDevice::~StdinDevice()
{ }

qint64 StdinDevice::bytesAvailable() const
{ return QIODevice::bytesAvailable(); }

void StdinDevice::close()
{ QIODevice::close(); }

bool StdinDevice::open(OpenMode mode)
{
    Q_ASSERT(!(mode & QIODevice::QIODevice::WriteOnly));
    return QIODevice::open(mode);
}

bool StdinDevice::atEnd() const
{ return QIODevice::bytesAvailable() == 0; }

bool StdinDevice::isSequential() const
{ return true; }

qint64 StdinDevice::readData(char *, qint64)
{ return 0; }

qint64 StdinDevice::writeData(const char *, qint64)
{
    Q_ASSERT(false);
    return 0;
}

void StdinDevice::signalTerminate()
{ }

};
