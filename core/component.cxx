/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <memory>
#include <unordered_map>

#include <QCoreApplication>
#include <QDir>
#include <QLocale>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonValueRef>
#include <QJsonArray>
#include <QLoggingCategory>

#include "core/component.hxx"


Q_LOGGING_CATEGORY(log_core_component, "cpd3.core.component", QtWarningMsg)

Q_LOGGING_CATEGORY(log_core_option, "cpd3.core.option", QtWarningMsg)

namespace CPD3 {


/** @file core/component.hxx
 * Provides plugin component handling routines.
 */


ComponentOptionBase::ComponentOptionBase(const QString &setArgumentName,
                                         const QString &setDisplayName,
                                         const QString &setDescription,
                                         const QString &setDefaultBehavior,
                                         int setSortPriority) : argumentName(setArgumentName),
                                                                displayName(setDisplayName),
                                                                description(setDescription),
                                                                defaultBehavior(setDefaultBehavior),
                                                                sortPriority(setSortPriority),
                                                                optionSet(false)
{ }

ComponentOptionBase::ComponentOptionBase(const ComponentOptionBase &other)
        : QObject(),
          argumentName(other.argumentName),
          displayName(other.displayName),
          description(other.description),
          defaultBehavior(other.defaultBehavior),
          sortPriority(other.sortPriority),
          optionSet(other.optionSet)
{ }

ComponentOptionSingleString::ComponentOptionSingleString(const QString &argumentName,
                                                         const QString &displayName,
                                                         const QString &description,
                                                         const QString &defaultBehavior,
                                                         int sortPriority) : ComponentOptionBase(
        argumentName, displayName, description, defaultBehavior, sortPriority), value()
{ }

ComponentOptionSingleString::ComponentOptionSingleString(const ComponentOptionSingleString &other)
        : ComponentOptionBase(other), value(other.value)
{ }

QString ComponentOptionSingleString::get() const
{ return value; }

void ComponentOptionSingleString::set(const QString &s)
{
    optionSet = true;
    value = s;
}

void ComponentOptionSingleString::reset()
{
    optionSet = false;
    value = QString();
}

ComponentOptionBase *ComponentOptionSingleString::clone() const
{
    return new ComponentOptionSingleString(*this);
}

ComponentOptionFile::ComponentOptionFile(const QString &argumentName,
                                         const QString &displayName,
                                         const QString &description,
                                         const QString &defaultBehavior,
                                         int sortPriority) : ComponentOptionSingleString(
        argumentName, displayName, description, defaultBehavior, sortPriority),
                                                             mode(Read),
                                                             typeDescription(),
                                                             extensions()
{ }

ComponentOptionFile::ComponentOptionFile(const ComponentOptionFile &other)
        : ComponentOptionSingleString(other),
          mode(other.mode),
          typeDescription(other.typeDescription),
          extensions(other.extensions)
{ }

ComponentOptionFile::Mode ComponentOptionFile::getMode() const
{ return mode; }

void ComponentOptionFile::setMode(Mode mode)
{ this->mode = mode; }

QString ComponentOptionFile::getTypeDescription() const
{ return typeDescription; }

void ComponentOptionFile::setTypeDescription(const QString &d)
{ typeDescription = d; }

QSet<QString> ComponentOptionFile::getExtensions() const
{ return extensions; }

void ComponentOptionFile::setExtensions(const QSet<QString> &extensions)
{
    this->extensions = extensions;
}

ComponentOptionBase *ComponentOptionFile::clone() const
{ return new ComponentOptionFile(*this); }

ComponentOptionDirectory::ComponentOptionDirectory(const QString &argumentName,
                                                   const QString &displayName,
                                                   const QString &description,
                                                   const QString &defaultBehavior,
                                                   int sortPriority) : ComponentOptionSingleString(
        argumentName, displayName, description, defaultBehavior, sortPriority)
{ }

ComponentOptionDirectory::ComponentOptionDirectory(const ComponentOptionDirectory &other)
        : ComponentOptionSingleString(other)
{ }

ComponentOptionBase *ComponentOptionDirectory::clone() const
{
    return new ComponentOptionDirectory(*this);
}

ComponentOptionScript::ComponentOptionScript(const QString &argumentName,
                                             const QString &displayName,
                                             const QString &description,
                                             const QString &defaultBehavior,
                                             int sortPriority) : ComponentOptionSingleString(
        argumentName, displayName, description, defaultBehavior, sortPriority)
{ }

ComponentOptionScript::ComponentOptionScript(const ComponentOptionScript &other)
        : ComponentOptionSingleString(other)
{ }

ComponentOptionBase *ComponentOptionScript::clone() const
{
    return new ComponentOptionScript(*this);
}

ComponentOptionSequenceMatch::ComponentOptionSequenceMatch(const QString &argumentName,
                                                           const QString &displayName,
                                                           const QString &description,
                                                           const QString &defaultBehavior,
                                                           int sortPriority)
        : ComponentOptionSingleString(argumentName, displayName, description, defaultBehavior,
                                      sortPriority)
{ }

ComponentOptionSequenceMatch::ComponentOptionSequenceMatch(const ComponentOptionScript &other)
        : ComponentOptionSingleString(other)
{ }

ComponentOptionBase *ComponentOptionSequenceMatch::clone() const
{
    return new ComponentOptionSequenceMatch(*this);
}

ComponentOptionDoubleSet::ComponentOptionDoubleSet(const QString &argumentName,
                                                   const QString &displayName,
                                                   const QString &description,
                                                   const QString &defaultBehavior,
                                                   int sortPriority) : ComponentOptionBase(
        argumentName, displayName, description, defaultBehavior, sortPriority),
                                                                       values(),
                                                                       max(FP::undefined()),
                                                                       min(FP::undefined()),
                                                                       maxInclusive(true),
                                                                       minInclusive(true)
{ }

ComponentOptionDoubleSet::ComponentOptionDoubleSet(const ComponentOptionDoubleSet &other)
        : ComponentOptionBase(other),
          values(other.values),
          max(other.max),
          min(other.min),
          maxInclusive(other.maxInclusive),
          minInclusive(other.minInclusive)
{ }

QSet<double> ComponentOptionDoubleSet::get() const
{ return values; }

void ComponentOptionDoubleSet::clear()
{
    optionSet = true;
    values.clear();
}

void ComponentOptionDoubleSet::set(const QSet<double> &s)
{
    optionSet = true;
    values = s;
}

void ComponentOptionDoubleSet::add(double v)
{
    optionSet = true;
    values.insert(v);
}

void ComponentOptionDoubleSet::reset()
{
    optionSet = false;
    values.clear();
}

ComponentOptionBase *ComponentOptionDoubleSet::clone() const
{
    return new ComponentOptionDoubleSet(*this);
}

void ComponentOptionDoubleSet::setMaximum(double m, bool inclusive)
{
    max = m;
    maxInclusive = inclusive;
}

void ComponentOptionDoubleSet::setMinimum(double m, bool inclusive)
{
    min = m;
    minInclusive = inclusive;
}

double ComponentOptionDoubleSet::getMaximum() const
{ return max; }

double ComponentOptionDoubleSet::getMinimum() const
{ return min; }

bool ComponentOptionDoubleSet::getMaximumInclusive() const
{ return maxInclusive; }

bool ComponentOptionDoubleSet::getMinimumInclusive() const
{ return minInclusive; }

bool ComponentOptionDoubleSet::isValid(double check) const
{
    if (!FP::defined(check))
        return false;

    if (FP::defined(min)) {
        if (minInclusive) {
            if (check < min)
                return false;
        } else {
            if (check <= min)
                return false;
        }
    }

    if (FP::defined(max)) {
        if (maxInclusive) {
            if (check > max)
                return false;
        } else {
            if (check >= max)
                return false;
        }
    }

    return true;
}

ComponentOptionDoubleList::ComponentOptionDoubleList(const QString &argumentName,
                                                     const QString &displayName,
                                                     const QString &description,
                                                     const QString &defaultBehavior,
                                                     int sortPriority) : ComponentOptionBase(
        argumentName, displayName, description, defaultBehavior, sortPriority),
                                                                         values(),
                                                                         minimumComponents(0)
{ }

ComponentOptionDoubleList::ComponentOptionDoubleList(const ComponentOptionDoubleList &other)
        : ComponentOptionBase(other),
          values(other.values),
          minimumComponents(other.minimumComponents)
{ }

QList<double> ComponentOptionDoubleList::get() const
{ return values; }

void ComponentOptionDoubleList::clear()
{
    optionSet = true;
    values.clear();
}

void ComponentOptionDoubleList::set(const QList<double> &s)
{
    optionSet = true;
    values = s;
}

void ComponentOptionDoubleList::append(double v)
{
    optionSet = true;
    values.append(v);
}

void ComponentOptionDoubleList::reset()
{
    optionSet = false;
    values.clear();
}

ComponentOptionBase *ComponentOptionDoubleList::clone() const
{
    return new ComponentOptionDoubleList(*this);
}

void ComponentOptionDoubleList::setMinimumComponents(int n)
{ minimumComponents = n; }

int ComponentOptionDoubleList::getMinimumComponents() const
{ return minimumComponents; }

ComponentOptionSingleDouble::ComponentOptionSingleDouble(const QString &argumentName,
                                                         const QString &displayName,
                                                         const QString &description,
                                                         const QString &defaultBehavior,
                                                         int sortPriority) : ComponentOptionBase(
        argumentName, displayName, description, defaultBehavior, sortPriority),
                                                                             value(FP::undefined()),
                                                                             max(FP::undefined()),
                                                                             min(FP::undefined()),
                                                                             maxInclusive(true),
                                                                             minInclusive(true),
                                                                             allowUndefined(false)
{ }

ComponentOptionSingleDouble::ComponentOptionSingleDouble(const ComponentOptionSingleDouble &other)
        : ComponentOptionBase(other),
          value(other.value),
          max(other.max),
          min(other.min),
          maxInclusive(other.maxInclusive),
          minInclusive(other.minInclusive),
          allowUndefined(other.allowUndefined)
{ }

double ComponentOptionSingleDouble::get() const
{ return value; }

void ComponentOptionSingleDouble::set(double v)
{
    optionSet = true;
    value = v;
}

void ComponentOptionSingleDouble::reset()
{
    optionSet = false;
    if (allowUndefined)
        value = FP::undefined();
}

ComponentOptionBase *ComponentOptionSingleDouble::clone() const
{
    return new ComponentOptionSingleDouble(*this);
}

void ComponentOptionSingleDouble::setMaximum(double m, bool inclusive)
{
    max = m;
    maxInclusive = inclusive;
}

void ComponentOptionSingleDouble::setMinimum(double m, bool inclusive)
{
    min = m;
    minInclusive = inclusive;
}

double ComponentOptionSingleDouble::getMaximum() const
{ return max; }

double ComponentOptionSingleDouble::getMinimum() const
{ return min; }

bool ComponentOptionSingleDouble::getMaximumInclusive() const
{ return maxInclusive; }

bool ComponentOptionSingleDouble::getMinimumInclusive() const
{ return minInclusive; }

void ComponentOptionSingleDouble::setAllowUndefined(bool a)
{ allowUndefined = a; }

bool ComponentOptionSingleDouble::getAllowUndefined() const
{ return allowUndefined; }

bool ComponentOptionSingleDouble::isValid(double check) const
{
    if (!FP::defined(check))
        return allowUndefined;

    if (FP::defined(min)) {
        if (minInclusive) {
            if (check < min)
                return false;
        } else {
            if (check <= min)
                return false;
        }
    }

    if (FP::defined(max)) {
        if (maxInclusive) {
            if (check > max)
                return false;
        } else {
            if (check >= max)
                return false;
        }
    }

    return true;
}

ComponentOptionSingleInteger::ComponentOptionSingleInteger(const QString &argumentName,
                                                           const QString &displayName,
                                                           const QString &description,
                                                           const QString &defaultBehavior,
                                                           int sortPriority) : ComponentOptionBase(
        argumentName, displayName, description, defaultBehavior, sortPriority),
                                                                               value(INTEGER::undefined()),
                                                                               max(INTEGER::undefined()),
                                                                               min(INTEGER::undefined()),
                                                                               allowUndefined(false)
{ }

ComponentOptionSingleInteger::ComponentOptionSingleInteger(const ComponentOptionSingleInteger &other)
        : ComponentOptionBase(other),
          value(other.value),
          max(other.max),
          min(other.min),
          allowUndefined(other.allowUndefined)
{ }

qint64 ComponentOptionSingleInteger::get() const
{ return value; }

void ComponentOptionSingleInteger::set(qint64 v)
{
    optionSet = true;
    value = v;
}

void ComponentOptionSingleInteger::reset()
{
    optionSet = false;
    if (allowUndefined)
        value = INTEGER::undefined();
}

ComponentOptionBase *ComponentOptionSingleInteger::clone() const
{
    return new ComponentOptionSingleInteger(*this);
}

void ComponentOptionSingleInteger::setMaximum(qint64 m)
{ max = m; }

void ComponentOptionSingleInteger::setMinimum(qint64 m)
{ min = m; }

qint64 ComponentOptionSingleInteger::getMaximum() const
{ return max; }

qint64 ComponentOptionSingleInteger::getMinimum() const
{ return min; }

void ComponentOptionSingleInteger::setAllowUndefined(bool a)
{ allowUndefined = a; }

bool ComponentOptionSingleInteger::getAllowUndefined() const
{ return allowUndefined; }

bool ComponentOptionSingleInteger::isValid(qint64 check) const
{
    if (!INTEGER::defined(check))
        return allowUndefined;

    if (INTEGER::defined(min)) {
        if (check < min)
            return false;
    }

    if (INTEGER::defined(max)) {
        if (check > max)
            return false;
    }

    return true;
}

ComponentOptionStringSet::ComponentOptionStringSet(const QString &argumentName,
                                                   const QString &displayName,
                                                   const QString &description,
                                                   const QString &defaultBehavior,
                                                   int sortPriority) : ComponentOptionBase(
        argumentName, displayName, description, defaultBehavior, sortPriority), values()
{ }

ComponentOptionStringSet::ComponentOptionStringSet(const ComponentOptionStringSet &other)
        : ComponentOptionBase(other), values(other.values)
{ }

QSet<QString> ComponentOptionStringSet::get() const
{ return values; }

void ComponentOptionStringSet::clear()
{
    optionSet = true;
    values.clear();
}

void ComponentOptionStringSet::set(const QSet<QString> &s)
{
    optionSet = true;
    values = s;
}

void ComponentOptionStringSet::add(const QString &s)
{
    optionSet = true;
    values |= s;
}

void ComponentOptionStringSet::reset()
{
    optionSet = false;
    values.clear();
}

ComponentOptionBase *ComponentOptionStringSet::clone() const
{
    return new ComponentOptionStringSet(*this);
}

ComponentOptionInstrumentSuffixSet::ComponentOptionInstrumentSuffixSet(const QString &argumentName,
                                                                       const QString &displayName,
                                                                       const QString &description,
                                                                       const QString &defaultBehavior,
                                                                       int sortPriority)
        : ComponentOptionStringSet(argumentName, displayName, description, defaultBehavior,
                                   sortPriority)
{ }

ComponentOptionInstrumentSuffixSet::ComponentOptionInstrumentSuffixSet(const ComponentOptionInstrumentSuffixSet &other)
        : ComponentOptionStringSet(other)
{ }

ComponentOptionBase *ComponentOptionInstrumentSuffixSet::clone() const
{
    return new ComponentOptionInstrumentSuffixSet(*this);
}


ComponentOptionEnum::EnumValue::EnumValue() : id(), internalName(), name(), description()
{ }

ComponentOptionEnum::EnumValue::EnumValue(int setID,
                                          const QString &setInternalName,
                                          const QString &setName,
                                          const QString &setDescription) : id(setID),
                                                                           internalName(
                                                                                   setInternalName),
                                                                           name(setName),
                                                                           description(
                                                                                   setDescription)
{ }

ComponentOptionEnum::EnumValue::EnumValue(const EnumValue &other) : id(other.id),
                                                                    internalName(
                                                                            other.internalName),
                                                                    name(other.name),
                                                                    description(other.description)
{ }

ComponentOptionEnum::EnumValue &ComponentOptionEnum::EnumValue::operator=(const EnumValue &other)
{
    if (&other == this) return *this;
    id = other.id;
    internalName = other.internalName;
    name = other.name;
    description = other.description;
    return *this;
}

ComponentOptionEnum::ComponentOptionEnum(const QString &argumentName,
                                         const QString &displayName,
                                         const QString &description,
                                         const QString &defaultBehavior,
                                         int sortPriority) : ComponentOptionBase(argumentName,
                                                                                 displayName,
                                                                                 description,
                                                                                 defaultBehavior,
                                                                                 sortPriority),
                                                             values(),
                                                             selected()
{ }

ComponentOptionEnum::ComponentOptionEnum(const ComponentOptionEnum &other) : ComponentOptionBase(
        other), values(other.values), selected(other.selected)
{ }

void ComponentOptionEnum::set(const EnumValue &v)
{
    optionSet = true;
    selected = v;
}

void ComponentOptionEnum::set(const QString &internalName)
{
    for (QList<EnumValue>::const_iterator ev = values.constBegin(), end = values.constEnd();
            ev != end;
            ++ev) {
        if (ev->internalName == internalName) {
            selected = *ev;
            optionSet = true;
            return;
        }
    }
    qCWarning(log_core_option) << "Attempt to select an undefined internal name:" << internalName;
}

ComponentOptionEnum::EnumValue ComponentOptionEnum::get() const
{ return selected; }

QList<ComponentOptionEnum::EnumValue> ComponentOptionEnum::getAll() const
{ return values; }

QHash<QString, ComponentOptionEnum::EnumValue> ComponentOptionEnum::getAliases() const
{ return aliases; }

void ComponentOptionEnum::add(int id,
                              const QString &internalName,
                              const QString &name,
                              const QString &description)
{
    values.append(EnumValue(id, internalName, name, description));
}

void ComponentOptionEnum::alias(int id, const QString &name)
{
    for (QList<EnumValue>::const_iterator ev = values.constBegin(), end = values.constEnd();
            ev != end;
            ++ev) {
        if (ev->getID() == id) {
            aliases.insert(name, *ev);
            return;
        }
    }
    Q_ASSERT(false);
}

void ComponentOptionEnum::reset()
{ optionSet = false; }

ComponentOptionBase *ComponentOptionEnum::clone() const
{ return new ComponentOptionEnum(*this); }


ComponentOptionBoolean::ComponentOptionBoolean(const QString &argumentName,
                                               const QString &displayName,
                                               const QString &description,
                                               const QString &defaultBehavior,
                                               int sortPriority) : ComponentOptionBase(argumentName,
                                                                                       displayName,
                                                                                       description,
                                                                                       defaultBehavior,
                                                                                       sortPriority),
                                                                   enabled(false)
{ }

ComponentOptionBoolean::ComponentOptionBoolean(const ComponentOptionBoolean &other)
        : ComponentOptionBase(other), enabled(other.enabled)
{ }

void ComponentOptionBoolean::set(bool value)
{
    optionSet = true;
    enabled = value;
}

bool ComponentOptionBoolean::get() const
{ return enabled; }

void ComponentOptionBoolean::reset()
{
    optionSet = false;
    enabled = false;
}

ComponentOptionBase *ComponentOptionBoolean::clone() const
{
    return new ComponentOptionBoolean(*this);
}


ComponentOptionTimeOffset::ComponentOptionTimeOffset(const QString &argumentName,
                                                     const QString &displayName,
                                                     const QString &description,
                                                     const QString &defaultBehavior,
                                                     int sortPriority) : ComponentOptionBase(
        argumentName, displayName, description, defaultBehavior, sortPriority),
                                                                         unit(Time::Second),
                                                                         count(0),
                                                                         align(false),
                                                                         defaultUnit(Time::Second),
                                                                         defaultCount(0),
                                                                         defaultAlign(false),
                                                                         allowZero(true),
                                                                         allowNegative(true)
{ }

ComponentOptionTimeOffset::ComponentOptionTimeOffset(const ComponentOptionTimeOffset &other)
        : ComponentOptionBase(other),
          unit(other.unit),
          count(other.count),
          align(other.align),
          defaultUnit(other.defaultUnit),
          defaultCount(other.defaultCount),
          defaultAlign(other.defaultAlign),
          allowZero(other.allowZero),
          allowNegative(other.allowNegative)
{ }

void ComponentOptionTimeOffset::set(Time::LogicalTimeUnit unit, int count, bool align)
{
    optionSet = true;
    if (!getAllowZero() && count == 0 && !align)
        count = 1;
    if (!getAllowNegative() && count < 0) {
        if (allowZero || align)
            count = 0;
        else
            count = 1;
    }
    this->unit = unit;
    this->count = count;
    this->align = align;
}

void ComponentOptionTimeOffset::setDefault(Time::LogicalTimeUnit unit, int count, bool align)
{
    if (!getAllowZero() && count == 0 && !align)
        count = 1;
    if (!getAllowNegative() && count < 0) {
        if (allowZero || align)
            count = 0;
        else
            count = 1;
    }
    this->defaultUnit = unit;
    this->defaultCount = count;
    this->defaultAlign = align;
    if (!optionSet) {
        this->unit = unit;
        this->count = count;
        this->align = align;
    }
}

Time::LogicalTimeUnit ComponentOptionTimeOffset::getUnit() const
{ return unit; }

int ComponentOptionTimeOffset::getCount() const
{ return count; }

bool ComponentOptionTimeOffset::getAlign() const
{ return align; }

Time::LogicalTimeUnit ComponentOptionTimeOffset::getDefaultUnit() const
{ return unit; }

int ComponentOptionTimeOffset::getDefaultCount() const
{ return count; }

bool ComponentOptionTimeOffset::getDefaultAlign() const
{ return align; }

void ComponentOptionTimeOffset::setAllowZero(bool allow)
{
    allowZero = allow;
    if (!allowZero && count == 0 && !align) {
        count = 1;
    }
}

void ComponentOptionTimeOffset::setAllowNegative(bool allow)
{
    allowNegative = allow;
    if (!allowNegative && count < 0) {
        if (allowZero || align)
            count = 0;
        else
            count = 1;
    }
}

bool ComponentOptionTimeOffset::getAllowZero() const
{ return allowZero; }

bool ComponentOptionTimeOffset::getAllowNegative() const
{ return allowNegative; }

void ComponentOptionTimeOffset::reset()
{ optionSet = false; }

ComponentOptionBase *ComponentOptionTimeOffset::clone() const
{
    return new ComponentOptionTimeOffset(*this);
}

ComponentOptionTimeBlock::ComponentOptionTimeBlock(const QString &argumentName,
                                                   const QString &displayName,
                                                   const QString &description,
                                                   const QString &defaultBehavior,
                                                   int sortPriority) : ComponentOptionTimeOffset(
        argumentName, displayName, description, defaultBehavior, sortPriority)
{
    setDefault(Time::Hour, 1, true);
}

ComponentOptionTimeBlock::ComponentOptionTimeBlock(const ComponentOptionTimeBlock &other)
        : ComponentOptionTimeOffset(other)
{ }

ComponentOptionBase *ComponentOptionTimeBlock::clone() const
{
    return new ComponentOptionTimeBlock(*this);
}

bool ComponentOptionTimeBlock::getAllowZero() const
{ return false; }

bool ComponentOptionTimeBlock::getAllowNegative() const
{ return false; }


ComponentOptionSingleTime::ComponentOptionSingleTime(const QString &argumentName,
                                                     const QString &displayName,
                                                     const QString &description,
                                                     const QString &defaultBehavior,
                                                     int sortPriority) : ComponentOptionBase(
        argumentName, displayName, description, defaultBehavior, sortPriority),
                                                                         value(FP::undefined()),
                                                                         allowUndefined(false)
{ }

ComponentOptionSingleTime::ComponentOptionSingleTime(const ComponentOptionSingleTime &other)
        : ComponentOptionBase(other), value(other.value)
{ }

double ComponentOptionSingleTime::get() const
{ return value; }

void ComponentOptionSingleTime::set(double v)
{
    optionSet = true;
    value = v;
}

void ComponentOptionSingleTime::reset()
{
    optionSet = false;
    value = FP::undefined();
}

ComponentOptionBase *ComponentOptionSingleTime::clone() const
{
    return new ComponentOptionSingleTime(*this);
}

void ComponentOptionSingleTime::setAllowUndefined(bool a)
{ allowUndefined = a; }

bool ComponentOptionSingleTime::getAllowUndefined() const
{ return allowUndefined; }


ComponentOptionSingleCalibration::ComponentOptionSingleCalibration(const QString &argumentName,
                                                                   const QString &displayName,
                                                                   const QString &description,
                                                                   const QString &defaultBehavior,
                                                                   int sortPriority)
        : ComponentOptionBase(argumentName, displayName, description, defaultBehavior,
                              sortPriority), cal(), allowInvalid(true), allowConstant(true)
{ }

ComponentOptionSingleCalibration::ComponentOptionSingleCalibration(const ComponentOptionSingleCalibration &other)
        : ComponentOptionBase(other),
          cal(other.cal),
          allowInvalid(other.allowInvalid),
          allowConstant(other.allowConstant)
{ }

void ComponentOptionSingleCalibration::set(const Calibration &s)
{
    optionSet = true;
    cal = s;
}

Calibration ComponentOptionSingleCalibration::get() const
{ return cal; }

void ComponentOptionSingleCalibration::reset()
{
    optionSet = false;
    cal = Calibration();
}

ComponentOptionBase *ComponentOptionSingleCalibration::clone() const
{
    return new ComponentOptionSingleCalibration(*this);
}

void ComponentOptionSingleCalibration::setAllowInvalid(bool allow)
{ allowInvalid = allow; }

void ComponentOptionSingleCalibration::setAllowConstant(bool allow)
{ allowConstant = allow; }

bool ComponentOptionSingleCalibration::getAllowInvalid() const
{ return allowInvalid; }

bool ComponentOptionSingleCalibration::getAllowConstant() const
{ return allowConstant; }

bool ComponentOptionSingleCalibration::isValid(const Calibration &cal)
{
    if (!cal.isValid() && !allowInvalid)
        return false;
    if (cal.size() == 1 && !allowConstant)
        return false;
    return true;
}

ComponentOptions::ComponentOptionsData::ComponentOptionsData() : options(), exclude()
{ }

ComponentOptions::ComponentOptionsData::~ComponentOptionsData()
{
    for (QHash<QString, ComponentOptionBase *>::iterator i = options.begin();
            i != options.end();
            ++i) {
        delete i.value();
    }
}

ComponentOptions::ComponentOptionsData::ComponentOptionsData(const ComponentOptionsData &other)
        : QSharedData(other), options(), exclude(other.exclude)
{
    for (QHash<QString, ComponentOptionBase *>::const_iterator i = other.options.constBegin();
            i != other.options.constEnd();
            ++i) {
        options.insert(i.key(), i.value()->clone());
    }
}

ComponentOptions::ComponentOptions()
{
    d = new ComponentOptionsData;
}

ComponentOptions::ComponentOptions(const ComponentOptions &other) : d(other.d)
{ }

ComponentOptions &ComponentOptions::operator=(const ComponentOptions &other)
{
    if (&other == this) return *this;
    d = other.d;
    return *this;
}


/**
 * Add a new option to the list.  This takes ownership of the pointer.
 * 
 * @param id        the internal ID of this option
 * @param option    an allocated pointer to an option subtype
 */
void ComponentOptions::add(const QString &id, ComponentOptionBase *option)
{
    d->options.insert(id, option);
}

/**
 * Mark an option as excluding another.  This means that if the option is
 * set the excluded one cannot be set (for example, it will cause an error
 * on the command line, or grey out the option in the GUI).
 * 
 * @param id        the id that causes the exclusion
 * @param exclude   the other ID that is being excluded
 */
void ComponentOptions::exclude(const QString &id, const QString &exclude)
{
    QHash<QString, QSet<QString> >::iterator i = d->exclude.find(id);
    if (i != d->exclude.end()) {
        i.value().insert(exclude);
        return;
    }
    QSet<QString> add;
    add |= exclude;
    d->exclude.insert(id, add);
}

/**
 * Test if a given ID was set.  If an option was not set then the default
 * behavior should be used.
 * 
 * @param id        the internal ID of this option
 * @return          true if the option was set
 */
bool ComponentOptions::isSet(const QString &id) const
{
    QHash<QString, ComponentOptionBase *>::const_iterator i = d->options.constFind(id);
    if (i == d->options.constEnd()) return false;
    return i.value()->isSet();
}

/**
 * Get the option specified by the given ID.
 * 
 * @param id        the internal ID of this option
 * @return          the option pointer
 */
ComponentOptionBase *ComponentOptions::get(const QString &id) const
{
    QHash<QString, ComponentOptionBase *>::const_iterator i = d->options.constFind(id);
    if (i == d->options.constEnd()) return NULL;
    return i.value();
}


/**
 * Get the options that exclude one another.
 * 
 * @return the hash of ids to the set of ids they exclude
 */
QHash<QString, QSet<QString> > ComponentOptions::excluded() const
{ return d->exclude; }

/**
 * Get all possible options.
 * 
 * @return the hash from internal IDs to option objects
 */
QHash<QString, ComponentOptionBase *> ComponentOptions::getAll() const
{ return d->options; }

ComponentExample::ComponentExample()
        : options(),
          title(),
          description(),
          inputTypes(Input_Scattering),
          inputAuxiliary(),
          inputRange(1430438400, 1430611200),
          inputStation("bnd"),
          inputArchive("raw")
{ }

ComponentExample::ComponentExample(const ComponentExample &other) : options(other.options),
                                                                    title(other.title),
                                                                    description(other.description),
                                                                    inputTypes(other.inputTypes),
                                                                    inputAuxiliary(
                                                                            other.inputAuxiliary),
                                                                    inputRange(other.inputRange),
                                                                    inputStation(
                                                                            other.inputStation),
                                                                    inputArchive(other.inputArchive)
{ }

ComponentExample &ComponentExample::operator=(const ComponentExample &other)
{
    if (&other == this) return *this;
    options = other.options;
    title = other.title;
    description = other.description;
    inputTypes = other.inputTypes;
    inputAuxiliary = other.inputAuxiliary;
    inputRange = other.inputRange;
    inputStation = other.inputStation;
    inputArchive = other.inputArchive;
    return *this;
}

/**
 * Create an example with the given options, description and title.
 * 
 * @param setOptions        the options
 * @param setTitle          the title
 * @param setDescription    the description text
 */
ComponentExample::ComponentExample(const ComponentOptions &setOptions,
                                   const QString &setTitle,
                                   const QString &setDescription) : options(setOptions),
                                                                    title(setTitle),
                                                                    description(setDescription),
                                                                    inputTypes(Input_Scattering),
                                                                    inputAuxiliary(),
                                                                    inputRange(1430438400,
                                                                               1430611200),
                                                                    inputStation("bnd"),
                                                                    inputArchive("raw")
{ }

/**
 * Get the example title heading.
 * @return the title
 */
QString ComponentExample::getTitle() const
{ return title; }

/**
 * Get the example description text.
 * @return the description
 */
QString ComponentExample::getDescription() const
{ return description; }

/**
 * Get the options necessary to use this example.
 * @param a set of options
 */
ComponentOptions ComponentExample::getOptions() const
{ return options; }

/**
 * Set the example title heading.
 * @param set the new title
 */
void ComponentExample::setTitle(const QString &set)
{ title = set; }

/**
 * Set the example description text.
 * @param set the new description
 */
void ComponentExample::setDescription(const QString &set)
{ description = set; }

/**
 * Set the example options.
 * @param set the new options
 */
void ComponentExample::setOptions(const ComponentOptions &set)
{ options = set; }

/**
 * Get the mask of input types used by the example.
 * @return the mask of input types
 */
int ComponentExample::getInputTypes() const
{ return inputTypes; }

/**
 * Get the list of additional inputs used by the example.
 * @return the additional inputs
 */
QStringList ComponentExample::getInputAuxiliary() const
{ return inputAuxiliary; }

/**
 * Get the time range used by the example.
 * @return the time range
 */
Time::Bounds ComponentExample::getInputRange() const
{ return inputRange; }

/**
 * Get the default station the example operates on.
 * @return the default station
 */
QString ComponentExample::getInputStation() const
{ return inputStation; }

/**
 * Get the default archive the example operates on.
 * @return the default archive
 */
QString ComponentExample::getInputArchive() const
{ return inputArchive; }

/**
 * Set the mask of input types used by the example.
 * @param types the input types
 */
void ComponentExample::setInputTypes(int types)
{ inputTypes = types; }

/**
 * Set the list of additional inputs used by the example.
 * @param aux   the additional inputs
 */
void ComponentExample::setInputAuxiliary(const QStringList &aux)
{ inputAuxiliary = aux; }

/**
 * Set the input range used by the example.
 * @param range the input range
 */
void ComponentExample::setInputRange(const Time::Bounds &range)
{ inputRange = range; }

/**
 * Set the default input station used by the example
 * @param station   the input station
 */
void ComponentExample::setInputStation(const QString &station)
{ inputStation = station; }

/**
 * Set the input archive used by the example
 * @param archive   the input archive
 */
void ComponentExample::setInputArchive(const QString &archive)
{ inputArchive = archive; }


static QDir getComponentsDir()
{
#if defined(CPD3_COMPONENTS_DIRECTORY)
    QString dir(CPD3_COMPONENTS_DIRECTORY);
    dir.replace("@executable_path", QCoreApplication::applicationDirPath());
    return QDir(dir);
#else
    return QDir(QCoreApplication::applicationDirPath());
#endif
}

static bool candidateComponentFile(const QString &fileName)
{
    if (!fileName.contains("component_", Qt::CaseInsensitive))
        return false;

#if defined(Q_OS_LINUX)
    if (!fileName.endsWith(".so", Qt::CaseInsensitive))
        return false;
    if (!fileName.startsWith("lib", Qt::CaseInsensitive))
        return false;
#elif defined(Q_OS_OSX)
    if (!fileName.endsWith(".so", Qt::CaseInsensitive))
        return false;
    if (!fileName.startsWith("lib", Qt::CaseInsensitive))
        return false;
#elif defined(Q_OS_WIN32)
    if (!fileName.endsWith(".dll", Qt::CaseInsensitive))
        return false;
#else
    Q_UNUSED(fileName);
#endif

    return true;
}

static QString getComponentName(const QJsonObject &meta)
{
    QString first = meta.value("IID").toString();
    if (first.length() < 6 || !first.startsWith("CPD3."))
        return QString();
    first = first.mid(5);
    QString second = meta.value("MetaData").toObject().value("ComponentName").toString();
    if (first != second)
        return QString();
    return second;
}

namespace {
struct LoadedComponents {
    std::mutex mutex;
    std::unordered_map<QString, std::unique_ptr<QPluginLoader>> components;
};
}

Q_GLOBAL_STATIC(LoadedComponents, loadedComponents)

static QPluginLoader *loadRecursive(const QDir &dir, const QString &name, int depth = 1)
{
    if (depth > 40)
        return nullptr;

    QFileInfoList contents(dir.entryInfoList(QDir::NoDotAndDotDot | QDir::Files | QDir::Dirs));
    for (QFileInfoList::const_iterator file = contents.constBegin(), endFiles = contents.constEnd();
            file != endFiles;
            ++file) {
        if (file->isDir()) {
            auto check = loadRecursive(QDir(file->absoluteFilePath()), name, depth + 1);
            if (check)
                return check;
            continue;
        }

        if (!candidateComponentFile(file->fileName()))
            continue;

        if (!file->fileName().contains(name, Qt::CaseInsensitive))
            continue;

        auto l = loadedComponents();
        if (!l)
            continue;
        std::lock_guard<std::mutex> lock(l->mutex);

        {
            auto check = l->components.find(name.toLower());
            if (check != l->components.end()) {
                Q_ASSERT(getComponentName(check->second->metaData()) == name.toLower());
                return check->second.get();
            }
        }

        std::unique_ptr<QPluginLoader> loader(new QPluginLoader(file->absoluteFilePath()));
        QString loadedName = getComponentName(loader->metaData());
        if (loadedName.isEmpty())
            continue;
        loader->setLoadHints(QLibrary::PreventUnloadHint);

        /* Since we're returning the pointer, we have to make sure the current
         * instance won't be destroyed (caught above) */
        Q_ASSERT(l->components.count(loadedName) == 0);
        auto result = loader.get();

        l->components.emplace(loadedName.toLower(), std::move(loader));

        if (loadedName != name.toLower())
            continue;

        return result;
    }

    return nullptr;
}

/**
 * Load a single component by its global name.
 * 
 * @param name  the global name of the component
 * @return      the component if found or null otherwise
 */
QObject *ComponentLoader::create(const QString &name)
{

    for (const auto &check : QPluginLoader::staticPlugins()) {
        if (getComponentName(check.metaData()) != name.toLower())
            continue;
        auto result = check.instance();
        Q_ASSERT(result);
        return result;
    }

    auto l = loadedComponents();
    if (!l)
        return nullptr;

    {
        std::lock_guard<std::mutex> lock(l->mutex);
        auto check = l->components.find(name.toLower());
        if (check != l->components.end()) {
            Q_ASSERT(getComponentName(check->second->metaData()) == name.toLower());
            auto result = check->second->instance();
            if (!result) {
                qCWarning(log_core_component) << "Failed to instantiate component" << name << ":"
                                              << check->second->errorString();
            } else {
                return result;
            }
        }
    }

    QByteArray check(qgetenv("CPD3COMPONENTS"));
    if (check.isEmpty())
        check = qgetenv("CPD3_COMPONENTS");
    if (!check.isEmpty()) {
        auto loader = loadRecursive(QDir(QString::fromUtf8(check)), name);
        if (loader) {
            std::lock_guard<std::mutex> lock(l->mutex);
            auto result = loader->instance();
            if (!result) {
                qCWarning(log_core_component) << "Failed to instantiate component" << name << ":"
                                              << loader->errorString();
            } else {
                return result;
            }
        }
    }

    auto loader = loadRecursive(getComponentsDir(), name);
    if (loader) {
        std::lock_guard<std::mutex> lock(l->mutex);
        auto result = loader->instance();
        if (!result) {
            qCWarning(log_core_component) << "Failed to instantiate component" << name << ":"
                                          << loader->errorString();
        } else {
            return result;
        }
    }

    check = qgetenv("CPD3");
    if (!check.isEmpty()) {
        loader = loadRecursive(QDir(QString::fromUtf8(check)), name);
        if (loader) {
            std::lock_guard<std::mutex> lock(l->mutex);
            auto result = loader->instance();
            if (!result) {
                qCWarning(log_core_component) << "Failed to instantiate component" << name << ":"
                                              << loader->errorString();
            } else {
                return result;
            }
        }
    }

    return nullptr;
}

/**
 * Get the component information for a single component by its global name.
 *
 * @param name  the global name of the component
 * @return      the component information and an instance of it
 */
std::pair<ComponentLoader::Information, QObject *> ComponentLoader::information(const QString &name)
{
    for (const auto &check : QPluginLoader::staticPlugins()) {
        if (getComponentName(check.metaData()) != name.toLower())
            continue;
        auto result = check.instance();
        Q_ASSERT(result);
        return std::make_pair<ComponentLoader::Information, QObject *>(Information(check),
                                                                       std::move(result));
    }

    auto l = loadedComponents();
    if (!l)
        return std::pair<ComponentLoader::Information, QObject *>(ComponentLoader::Information(),
                                                                  nullptr);

    {
        std::lock_guard<std::mutex> lock(l->mutex);
        auto check = l->components.find(name.toLower());
        if (check != l->components.end()) {
            Q_ASSERT(getComponentName(check->second->metaData()) == name.toLower());
            auto result = check->second->instance();
            if (!result) {
                qCWarning(log_core_component) << "Failed to instantiate component" << name << ":"
                                              << check->second->errorString();
            } else {
                return std::make_pair<ComponentLoader::Information, QObject *>(
                        Information(*(check->second)), std::move(result));
            }
        }
    }

    QByteArray check(qgetenv("CPD3COMPONENTS"));
    if (check.isEmpty())
        check = qgetenv("CPD3_COMPONENTS");
    if (!check.isEmpty()) {
        auto loader = loadRecursive(QDir(QString::fromUtf8(check)), name);
        if (loader) {
            std::lock_guard<std::mutex> lock(l->mutex);
            auto result = loader->instance();
            if (!result) {
                qCWarning(log_core_component) << "Failed to instantiate component" << name << ":"
                                              << loader->errorString();
            } else {
                return std::make_pair<ComponentLoader::Information, QObject *>(Information(*loader),
                                                                               std::move(result));
            }
        }
    }

    auto loader = loadRecursive(getComponentsDir(), name);
    if (loader) {
        std::lock_guard<std::mutex> lock(l->mutex);
        auto result = loader->instance();
        if (!result) {
            qCWarning(log_core_component) << "Failed to instantiate component" << name << ":"
                                          << loader->errorString();
        } else {
            return std::make_pair<ComponentLoader::Information, QObject *>(Information(*loader),
                                                                           std::move(result));
        }
    }

    check = qgetenv("CPD3");
    if (!check.isEmpty()) {
        loader = loadRecursive(QDir(QString::fromUtf8(check)), name);
        if (loader) {
            std::lock_guard<std::mutex> lock(l->mutex);
            auto result = loader->instance();
            if (!result) {
                qCWarning(log_core_component) << "Failed to instantiate component" << name << ":"
                                              << loader->errorString();
            } else {
                return std::make_pair<ComponentLoader::Information, QObject *>(Information(*loader),
                                                                               std::move(result));
            }
        }
    }

    return std::pair<ComponentLoader::Information, QObject *>(ComponentLoader::Information(),
                                                              nullptr);
}

void ComponentLoader::listRecursive(const QDir &dir,
                                    std::unordered_map<QString, Information> &result,
                                    std::unordered_set<QString> &inspectedFiles,
                                    int depth)
{
    if (depth > 40)
        return;
    QFileInfoList contents(dir.entryInfoList(QDir::NoDotAndDotDot | QDir::Files | QDir::Dirs));
    for (QFileInfoList::const_iterator file = contents.constBegin(), endFiles = contents.constEnd();
            file != endFiles;
            ++file) {
        if (file->isDir()) {
            listRecursive(QDir(file->absoluteFilePath()), result, inspectedFiles, depth + 1);
            continue;
        }

        if (inspectedFiles.count(file->absoluteFilePath()) != 0)
            continue;
        inspectedFiles.insert(file->absoluteFilePath());

        if (!candidateComponentFile(file->fileName()))
            continue;

        auto l = loadedComponents();
        if (!l)
            continue;

        std::lock_guard<std::mutex> lock(l->mutex);

        std::unique_ptr<QPluginLoader> loader(new QPluginLoader(file->absoluteFilePath()));
        Information info(*loader);
        if (!info.isValid())
            continue;
        loader->setLoadHints(QLibrary::PreventUnloadHint);

        QString name = info.componentName();

        /* Even if the instance is destroyed by this call (the component was already
         * loaded) the information remains valid, so we don't need to check here */
        l->components.emplace(name, std::move(loader));

        if (info.excludeFromList())
            continue;

        result.emplace(std::move(name), std::move(info));
    }
}

/**
 * List all available components.
 *
 * @return  all available components
 */
std::unordered_map<QString, ComponentLoader::Information> ComponentLoader::list()
{
    std::unordered_map<QString, Information> result;
    for (const auto &check : QPluginLoader::staticPlugins()) {
        Information info(check);
        if (!info.isValid())
            continue;
        if (info.excludeFromList())
            continue;
        QString name = info.componentName();
        result.emplace(std::move(name), std::move(info));
    }

    auto l = loadedComponents();
    if (!l)
        return result;

    {
        std::lock_guard<std::mutex> lock(l->mutex);
        for (const auto &check : l->components) {
            Information info(*check.second);
            Q_ASSERT(info.isValid());
            if (info.excludeFromList())
                continue;
            QString name = info.componentName();
            result.emplace(std::move(name), std::move(info));
        }
    }

    std::unordered_set<QString> inspectedFiles;

    QByteArray check(qgetenv("CPD3COMPONENTS"));
    if (check.isEmpty())
        check = qgetenv("CPD3_COMPONENTS");
    if (check.isEmpty())
        check = qgetenv("CPD3");
    if (!check.isEmpty())
        listRecursive(QDir(QString::fromUtf8(check)), result, inspectedFiles);

    listRecursive(getComponentsDir(), result, inspectedFiles);

    return result;
}

std::unordered_map<QString,
                   ComponentLoader::Information> ComponentLoader::list(const std::string &type)
{
    auto all = list();
    for (auto i = all.begin(); i != all.end();) {
        if (!i->second.excludeFromList(type)) {
            ++i;
            continue;
        }
        i = all.erase(i);
    }
    return all;
}

ComponentLoader::Information::Information()
        : loadedComponentName(),
          supportedInterfaces(),
          loadedTitle(),
          loadedDescription(),
          loadedSortPriority(0),
          disableList(true),
          listExclude()
{ }

ComponentLoader::Information::Information(const ComponentLoader::Information &) = default;

ComponentLoader::Information &ComponentLoader::Information::operator=(const ComponentLoader::Information &) = default;

ComponentLoader::Information::Information(ComponentLoader::Information &&) = default;

ComponentLoader::Information &ComponentLoader::Information::operator=(ComponentLoader::Information &&) = default;

ComponentLoader::Information::Information(const QPluginLoader &loader) : Information(
        loader.metaData().value("MetaData").toObject())
{
    if (loader.metaData().value("IID").toString() !=
            QString("CPD3.%1").arg(loadedComponentName.toLower()))
        loadedComponentName.clear();
}

ComponentLoader::Information::Information(const QStaticPlugin &loader) : Information(
        loader.metaData().value("MetaData").toObject())
{
    if (loader.metaData().value("IID").toString() !=
            QString("CPD3.%1").arg(loadedComponentName.toLower()))
        loadedComponentName.clear();
}

static QString localizeMetadata(const QJsonObject &object, const QString &path)
{
    QString lname = QLocale().name();
    auto check = object.value(QString("%1_%2").arg(path, lname));
    if (check.isString())
        return check.toString();
    return object.value(path).toString();
}

ComponentLoader::Information::Information(const QJsonObject &metadata) : Information()
{
    for (auto add : metadata.value("Interfaces").toArray()) {
        supportedInterfaces.emplace(add.toString().toLower().toStdString());
    }
    supportedInterfaces.erase(std::string());
    if (supportedInterfaces.empty())
        return;

    disableList = metadata.value("List").toObject().value("Disable").toBool(false);
    for (auto add : metadata.value("List").toObject().value("Exclude").toArray()) {
        listExclude.emplace(add.toString().toLower().toStdString());
    }
    listExclude.erase(std::string());

    loadedSortPriority = metadata.value("SortPriority").toInt(0);
    loadedTitle = localizeMetadata(metadata, "Title");
    loadedDescription = localizeMetadata(metadata, "Description");

    loadedComponentName = metadata.value("ComponentName").toString().toLower();
}

bool ComponentLoader::Information::DisplayOrder::operator()(const ComponentLoader::Information &a,
                                                            const ComponentLoader::Information &b) const
{
    if (a.sortPriority() != b.sortPriority())
        return a.sortPriority() < b.sortPriority();
    return a.componentName() < b.componentName();
}

};
