/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <thread>
#include <QThread>
#include <QMutex>
#include <QWaitCondition>

#include "core/waitutils.hxx"
#include "core/timeutils.hxx"
#include "core/threadpool.hxx"
#include "core/environment.hxx"


namespace CPD3 {


/** @file core/waitutils.hxx
 * Provides simple thread waiting routines.
 */

/**
 * A simple binary exponential backoff based class for polling loops.
 * 
 * @param count     the try counter
 * @param start     the start time
 * @param timeout   the total timeout before giving up or FP::undefined for none
 * @return          the time to wait in milliseconds or zero for a simple yield
 */
int Wait::backoffWaitTime(int count, double start, double timeout)
{
    if (count < 10)
        return 0;

    int wmax = count - 10;
    if (wmax > 10) wmax = 10;
    wmax = 1 << wmax;
    int wait = (int) floor(Random::fp() * wmax * 1000.0);
    if (FP::defined(timeout)) {
        if (FP::defined(start)) {
            wmax = (int) ((timeout - (Time::time() - start)) * 1000.0);
        } else {
            wmax = (int) (timeout * 1000.0);
        }
        if (wait > wmax) wait = wmax;
    }

    if (wait < 1)
        return 0;

    return wait;
}

namespace {
/* "Fun" game to play to get access to this */
class SleepExposingThread : public QThread {
public:
    static void exposedSleep(unsigned long msecs)
    { QThread::msleep(msecs); }
};
}

/**
 * A simple binary exponential backoff based class for polling loops.
 * 
 * @param count     the try counter
 * @param start     the start time
 * @param timeout   the total timeout before giving up or FP::undefined for none
 */
void Wait::backoffWait(int count, double start, double timeout)
{
    int wait = backoffWaitTime(count, start, timeout);
    if (wait < 1) {
        QThread::yieldCurrentThread();
        return;
    }

    SleepExposingThread::exposedSleep((unsigned long) wait);
}


void BufferSizeLimiter::updateSleepTime(size_t size)
{
    if (lastTriggerThreshold < size) {
        while (sleepTime < 512 && lastTriggerThreshold < size) {
            lastTriggerThreshold <<= 1;
            sleepTime <<= 1;
        }
    } else {
        while (sleepTime > 1 && lastTriggerThreshold > size) {
            lastTriggerThreshold >>= 1;
            sleepTime >>= 1;
        }
    }

    lastTriggerThreshold = size;
}

void BufferSizeLimiter::executeLimiting(size_t size, QMutex *mutex)
{
    updateSleepTime(size);
    QWaitCondition cond;

    /* This limit is set arbitrarily, rather than on anything hard */
    if (sleepTime <= 4) {
        cond.wait(mutex, sleepTime);
        return;
    }

    cond.wait(mutex, sleepTime);
}

void BufferSizeLimiter::executeLimiting(size_t size, std::mutex *mutex)
{
    Q_ASSERT(!mutex->try_lock());
    updateSleepTime(size);

    /* This limit is set arbitrarily, rather than on anything hard */
    if (sleepTime <= 4) {
        mutex->unlock();
        std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
        mutex->lock();
        return;
    }

    mutex->unlock();
    std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
    mutex->lock();
}

BufferSizeLimiter::BufferSizeLimiter(size_t limit) : thresholdLimit(limit),
                                                     lastTriggerThreshold(limit),
                                                     sleepTime(2)
{ }

};
