/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3CORETEXTSUBSTITUTION_HXX
#define CPD3CORETEXTSUBSTITUTION_HXX

#include "core/first.hxx"

#include <memory>
#include <unordered_map>
#include <vector>
#include <QList>
#include <QHash>
#include <QString>
#include <QStringRef>
#include <QStringList>
#include <QRegExp>
#include <QRegularExpression>
#include <QFileInfo>

#include "core/core.hxx"

namespace CPD3 {

/**
 * A common base for text variable substitution.
 */
class CPD3CORE_EXPORT TextSubstitution {
public:
    TextSubstitution();

    virtual ~TextSubstitution();

    TextSubstitution(const TextSubstitution &other);

    TextSubstitution &operator=(const TextSubstitution &other);

    /**
     * Apply variable substitution to the given input.
     *
     * @param input     the input string
     * @return          the output with variables replaced
     */
    QString apply(const QString &input) const;

    /**
     * Format a time for display.
     *
     * @param time      the input time
     * @param elements  the elements of the formatting string
     * @param delimited output delimited (with special characters) by default
     * @return          the formatted time
     */
    static QString formatTime
            (double time, const QStringList &elements = QStringList(), bool delimited = true);

    /**
     * Format a double precision number for display.
     *
     * @param value     the input value
     * @param elements  the elements of the formatting string
     * @return          the formatted value
     */
    static QString formatDouble(double value, const QStringList &elements = QStringList());

    /**
     * Format an integer for display.
     *
     * @param value     the input value
     * @param elements  the elements of the formatting string
     * @return          the formatted value
     */
    static QString formatInteger(qint64 value, const QStringList &elements = QStringList());

    /**
     * Format a string for display.
     *
     * @param value     the input value
     * @param elements  the elements of the formatting string
     * @param defined   if false then use the missing value substitution
     * @return          the formatted value
     */
    static QString formatString(const QString &value,
                                const QStringList &elements = QStringList(),
                                bool defined = true);

    /**
     * Format a duration for display.
     *
     * @param duration  the duration in seconds
     * @param elements  the elements of the formatting string
     * @param relativePositive  the format to apply in relative mode when the duration is positive
     * @param relativeNegative  the format to apply in relative mode when the duration is negative
     */
    static QString formatDuration(double duration,
                                  const QStringList &elements = QStringList(),
                                  const QString &relativePositive = "%1",
                                  const QString &relativeNegative = "%1");

protected:
    /**
     * Get the string to replace the given variable key with.
     *
     * @param key       the input key
     * @return          the replacement
     */
    virtual QString substitution(const QStringList &elements) const = 0;

    /**
     * Check if the input opens a literal replacement.
     *
     * @param key       the beginning of the key, on output set to where the literal text begins
     * @return          the string that closes the literal or empty if this is not a literal
     */
    virtual QString literalCheck(QStringRef &key) const;

    /**
     * Get the string to replace literal text.
     * <br>
     * The default implementation returns substitution(const QString &).
     *
     * @param key       the input text
     * @parm close      the text used to close the literal
     * @return          the replacement
     */
    virtual QString literalSubstitution(const QStringRef &key, const QString &close) const;
};

/**
 * A simple text substitution for numbered replacements.  Replacements are of the form ${0} .. ${N},
 * with the indexing from zero.
 */
class CPD3CORE_EXPORT NumberedSubstitution : public TextSubstitution {
    QStringList replacements;
public:
    /**
     * Create the replacement operator.
     *
     * @param replacements  the replacements
     */
    NumberedSubstitution(const QStringList &replacements);

    /**
     * Create the replacement operator.  Since the regular expression captured texts begin
     * at index one (zero is the whole expression), ${1} corresponds to the first captured
     * text.
     *
     * @param expression    the matches regular expression to generate captures from
     */
    NumberedSubstitution(const QRegExp &expression);

    NumberedSubstitution(const QRegularExpressionMatch &matched);

    virtual ~NumberedSubstitution();

    NumberedSubstitution(const NumberedSubstitution &other);

    NumberedSubstitution &operator=(const NumberedSubstitution &other);

    NumberedSubstitution(NumberedSubstitution &&other);

    NumberedSubstitution &operator=(NumberedSubstitution &&other);

protected:
    virtual QString substitution(const QStringList &elements) const;
};

/**
 * A simple text substitution stack that provides basic interfaces to format
 * layers of strings, double, integers, and times.  This provides basic substitution for
 * case insensitive matching of key names.
 */
class CPD3CORE_EXPORT TextSubstitutionStack : public TextSubstitution {
public:
    /**
     * The base class for replacements done by the stack.
     */
    class CPD3CORE_EXPORT Replacer {
    public:
        Replacer();

        virtual ~Replacer();

        /**
         * Apply the replacement to the given element list.  The list has the
         * initial key removed.
         *
         * @param elements  the matched elements without the matched key
         * @return          the replacement value
         */
        virtual QString get(const QStringList &elements) const = 0;
    };

    /**
     * A simple push/pop context for the stack.  This pushes a frame on construction
     * and pops one on destruction.
     */
    class CPD3CORE_EXPORT Context { Q_DISABLE_COPY(Context);

        TextSubstitutionStack &substitutions;
    public:
        /**
         * Create the context and push a frame.
         */
        Context(TextSubstitutionStack &substitutions);

        ~Context();
    };

    /**
     * Set a replacement on the stack.  This takes ownership of the replacer object.
     *
     * @param key           the key to replace
     * @param replacer      the replacement object
     */
    void setReplacement(const QString &key, Replacer *replacer);

private:

    typedef std::shared_ptr<Replacer> ReplacerPointer;
    typedef std::unordered_map<QString, ReplacerPointer> ReplacementLevel;
    std::vector<ReplacementLevel> replacements;

public:
    TextSubstitutionStack();

    virtual ~TextSubstitutionStack();

    TextSubstitutionStack(const TextSubstitutionStack &other);

    TextSubstitutionStack &operator=(const TextSubstitutionStack &other);

    TextSubstitutionStack(TextSubstitutionStack &&other);

    TextSubstitutionStack &operator=(TextSubstitutionStack &&other);

    /**
     * Push a layer of substitutions onto the stack.  This effectively saves the state for
     * later restoration.
     */
    virtual void push();

    /**
     * Pop a previously pushed layer of substitutions.  This effectively restores a prior
     * state saved with push().
     */
    virtual void pop();

    /**
     * Clear the stack, removing all substitutions.
     */
    virtual void clear();

    /**
     * Set a key to be replaced with a given string value.
     *
     * @param key       the key to be replaced
     * @param value     the base value to replace the key variable with
     * @param exact     only return the value exactly, ignoring any modifiers
     */
    void setString(const QString &key, const QString &value, bool exact = false);

    /**
     * Set multiple string values at once.
     *
     * @param value     the values
     * @param exact     only return the value exactly, ignoring any modifiers
     */
    void setString(const QHash<QString, QString> &values, bool exact = false);

    /**
     * Set a key to be replaced with a given double value.
     *
     * @param key       the key to be replaced
     * @param value     the base value to replace the key variable with
     */
    void setDouble(const QString &key, double value);

    /**
     * Set a key to be replaced with a given integer value.
     *
     * @param key       the key to be replaced
     * @param value     the base value to replace the key variable with
     */
    void setInteger(const QString &key, qint64 value);

    /**
     * Set a key to be replaced with a given time value.
     *
     * @param key       the key to be replaced
     * @param value     the base value to replace the key variable with
     * @param delimited use delimited ISO8601 time by default
     */
    void setTime(const QString &key, double value, bool delimited = true);

    /**
     * Set a key to be replaced with a given duration in seconds.
     *
     * @param key       the key to be replaced
     * @param value     the base value to replace the key variable with
     * @param relativePositive  the format to apply in relative mode when the duration is positive
     * @param relativeNegative  the format to apply in relative mode when the duration is negative
     */
    void setDuration(const QString &key,
                     double value,
                     const QString &relativePositive = "%1",
                     const QString &relativeNegative = "%1");

    /**
     * Set a key to be replaced with file information.
     *
     * @param key       the key to be replaced
     * @param value     the base value to replace the key variable with
     */
    void setFile(const QString &key, const QFileInfo &value);

    /**
     * Set a key to be replaced with file information.
     *
     * @param key       the key to be replaced
     * @param value     the base value to replace the key variable with
     */
    void setFile(const QString &key, const QString &value);

    /**
     * Get the current evaluation of all keys with the given element parameter list.
     *
     * @param elements  the parameters
     * @return          the evaluation of all keys
     */
    QHash<QString, QString> evalulateAll(const QStringList &elements = QStringList()) const;

protected:
    virtual QString substitution(const QStringList &elements) const;

    /**
     * Test if a key is currently claimed by the stack.
     *
     * @return key      the key to check
     * @return          true if the key exists
     */
    inline bool keyClaimed(const QString &key) const
    { return replacements.back().count(key.toLower()) != 0; }
};

}

#endif //CPD3CORETEXTSUBSTITUTION_HXX
