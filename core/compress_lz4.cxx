/*
   LZ4 - Fast LZ compression algorithm
   Copyright (C) 2011-2015, Yann Collet.

   BSD 2-Clause License (http://www.opensource.org/licenses/bsd-license.php)

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:

       * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
       * Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following disclaimer
   in the documentation and/or other materials provided with the
   distribution.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

   You can contact the author at :
   - LZ4 source repository : https://github.com/Cyan4973/lz4
   - LZ4 public forum : https://groups.google.com/forum/#!forum/lz4c
*/

#include "core/first.hxx"

#include "core/compress_lz4.hxx"
#include "core/number.hxx"

namespace CPD3 {

/** @file core/compress_lz4.hxx
 * LZ4 Compression routines.
 * @see https://code.google.com/p/lz4/
 */


/**************************************
*  Tuning parameters
**************************************/
/*
 * HEAPMODE :
 * Select how default compression functions will allocate memory for their hash table,
 * in memory stack (0:default, fastest), or in memory heap (1:requires malloc()).
 */
#define HEAPMODE 0

/*
 * ACCELERATION_DEFAULT :
 * Select "acceleration" for LZ4_compress_fast() when parameter value <= 0
 */
#define ACCELERATION_DEFAULT 1

/*
 * LZ4_MEMORY_USAGE :
 * Memory usage formula : N->2^N Bytes (examples : 10 -> 1KB; 12 -> 4KB ; 16 -> 64KB; 20 -> 1MB; etc.)
 * Increasing memory usage improves compression ratio
 * Reduced memory usage can improve speed, due to cache effect
 * Default value is 14, for 16KB, which nicely fits into Intel x86 L1 cache
 */
#define LZ4_MEMORY_USAGE 14


/**************************************
*  CPU Feature Detection
**************************************/
/*
 * LZ4_FORCE_SW_BITCOUNT
 * Define this parameter if your target system or compiler does not support hardware bit count
 */
#if defined(_MSC_VER) && defined(_WIN32_WCE)   /* Visual Studio for Windows CE does not support Hardware bit count */
#  define LZ4_FORCE_SW_BITCOUNT
#endif

#define LZ4_GCC_VERSION (__GNUC__ * 100 + __GNUC_MINOR__)

#define LZ4_MAX_INPUT_SIZE        0x7E000000   /* 2 113 929 216 bytes */
#define LZ4_COMPRESSBOUND(isize)  ((unsigned)(isize) > (unsigned)LZ4_MAX_INPUT_SIZE ? 0 : (isize) + ((isize)/255) + 16)


/**************************************
*  Compiler Options
**************************************/
#ifdef _MSC_VER    /* Visual Studio */
#  define FORCE_INLINE static __forceinline
#  include <intrin.h>
#  pragma warning(disable : 4127)        /* disable: C4127: conditional expression is constant */
#  pragma warning(disable : 4293)        /* disable: C4293: too large shift (32-bits) */
#else
#  if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)   /* C99 */
#    if defined(__GNUC__) || defined(__clang__)
#      define FORCE_INLINE static inline __attribute__((always_inline))
#    else
#      define FORCE_INLINE static inline
#    endif
#  else
#    define FORCE_INLINE static
#  endif   /* __STDC_VERSION__ */
#endif  /* _MSC_VER */

/* LZ4_GCC_VERSION is defined into lz4.h */
#if (LZ4_GCC_VERSION >= 302) || (__INTEL_COMPILER >= 800) || defined(__clang__)
#  define expect(expr, value)    (__builtin_expect ((expr),(value)) )
#else
#  define expect(expr,value)    (expr)
#endif

#define likely(expr)     expect((expr) != 0, 1)
#define unlikely(expr)   expect((expr) != 0, 0)


/**************************************
*  Memory routines
**************************************/

#include <stdlib.h>   /* malloc, calloc, free */

#define ALLOCATOR(n, s) calloc(n,s)
#define FREEMEM        free

#include <string.h>   /* memset, memcpy */

#define MEM_INIT       memset


/**************************************
*  Basic Types
**************************************/
#if defined (__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)   /* C99 */
# include <stdint.h>
typedef  uint8_t BYTE;
typedef uint16_t U16;
typedef uint32_t U32;
typedef  int32_t S32;
typedef uint64_t U64;
#else
typedef unsigned char BYTE;
typedef unsigned short U16;
typedef unsigned int U32;
typedef signed int S32;
typedef unsigned long long U64;
#endif


/**************************************
*  Reading and writing into memory
**************************************/
#define STEPSIZE sizeof(size_t)

static unsigned LZ4_64bits(void)
{ return sizeof(void *) == 8; }

static unsigned LZ4_isLittleEndian(void)
{
    const union {
        U32 i;
        BYTE c[4];
    } one = {1};   /* don't use static : performance detrimental  */
    return one.c[0];
}


static U16 LZ4_read16(const void *memPtr)
{
    U16 val16;
    memcpy(&val16, memPtr, 2);
    return val16;
}

static U16 LZ4_readLE16(const void *memPtr)
{
    if (LZ4_isLittleEndian()) {
        return LZ4_read16(memPtr);
    } else {
        const BYTE *p = (const BYTE *) memPtr;
        return (U16) ((U16) p[0] + (p[1] << 8));
    }
}

static void LZ4_writeLE16(void *memPtr, U16 value)
{
    if (LZ4_isLittleEndian()) {
        memcpy(memPtr, &value, 2);
    } else {
        BYTE *p = (BYTE *) memPtr;
        p[0] = (BYTE) value;
        p[1] = (BYTE) (value >> 8);
    }
}

static U32 LZ4_read32(const void *memPtr)
{
    U32 val32;
    memcpy(&val32, memPtr, 4);
    return val32;
}

static U64 LZ4_read64(const void *memPtr)
{
    U64 val64;
    memcpy(&val64, memPtr, 8);
    return val64;
}

static size_t LZ4_read_ARCH(const void *p)
{
    if (LZ4_64bits())
        return (size_t) LZ4_read64(p);
    else
        return (size_t) LZ4_read32(p);
}


static void LZ4_copy4(void *dstPtr, const void *srcPtr)
{ memcpy(dstPtr, srcPtr, 4); }

static void LZ4_copy8(void *dstPtr, const void *srcPtr)
{ memcpy(dstPtr, srcPtr, 8); }

/* customized version of memcpy, which may overwrite up to 7 bytes beyond dstEnd */
static void LZ4_wildCopy(void *dstPtr, const void *srcPtr, void *dstEnd)
{
    BYTE *d = (BYTE *) dstPtr;
    const BYTE *s = (const BYTE *) srcPtr;
    BYTE *e = (BYTE *) dstEnd;
    do {
        LZ4_copy8(d, s);
        d += 8;
        s += 8;
    } while (d < e);
}


/**************************************
*  Common Constants
**************************************/
#define MINMATCH 4

#define COPYLENGTH 8
#define LASTLITERALS 5
#define MFLIMIT (COPYLENGTH+MINMATCH)
static const int LZ4_minLength = (MFLIMIT + 1);

#define KB *(1 <<10)
#define MB *(1 <<20)
#define GB *(1U<<30)

#define MAXD_LOG 16
#define MAX_DISTANCE ((1 << MAXD_LOG) - 1)

#define ML_BITS  4
#define ML_MASK  ((1U<<ML_BITS)-1)
#define RUN_BITS (8-ML_BITS)
#define RUN_MASK ((1U<<RUN_BITS)-1)

#define LZ4_STREAMSIZE_U64 ((1 << (LZ4_MEMORY_USAGE-3)) + 4)
#define LZ4_STREAMSIZE     (LZ4_STREAMSIZE_U64 * sizeof(long long))
typedef struct {
    long long table[LZ4_STREAMSIZE_U64];
} LZ4_stream_t;

static void LZ4_resetStream(LZ4_stream_t *LZ4_stream)
{
    MEM_INIT(LZ4_stream, 0, sizeof(LZ4_stream_t));
}


/**************************************
*  Common Utils
**************************************/
#define LZ4_STATIC_ASSERT(c)    { enum { LZ4_static_assert = 1/(int)(!!(c)) }; }   /* use only *after* variable declarations */


/**************************************
*  Common functions
**************************************/
static unsigned LZ4_NbCommonBytes(size_t val)
{
    if (LZ4_isLittleEndian()) {
        if (LZ4_64bits()) {
#       if defined(_MSC_VER) && defined(_WIN64) && !defined(LZ4_FORCE_SW_BITCOUNT)
            unsigned long r = 0;
            _BitScanForward64( &r, (U64)val );
            return (int)(r>>3);
#       elif (defined(__clang__) || (LZ4_GCC_VERSION >= 304)) && !defined(LZ4_FORCE_SW_BITCOUNT)
            return (__builtin_ctzll((U64) val) >> 3);
#       else
            static const int DeBruijnBytePos[64] = { 0, 0, 0, 0, 0, 1, 1, 2, 0, 3, 1, 3, 1, 4, 2, 7, 0, 2, 3, 6, 1, 5, 3, 5, 1, 3, 4, 4, 2, 5, 6, 7, 7, 0, 1, 2, 3, 3, 4, 6, 2, 6, 5, 5, 3, 4, 5, 6, 7, 1, 2, 4, 6, 4, 4, 5, 7, 2, 6, 5, 7, 6, 7, 7 };
            return DeBruijnBytePos[((U64)((val & -(long long)val) * 0x0218A392CDABBD3FULL)) >> 58];
#       endif
        } else /* 32 bits */
        {
#       if defined(_MSC_VER) && !defined(LZ4_FORCE_SW_BITCOUNT)
            unsigned long r;
            _BitScanForward( &r, (U32)val );
            return (int)(r>>3);
#       elif (defined(__clang__) || (LZ4_GCC_VERSION >= 304)) && !defined(LZ4_FORCE_SW_BITCOUNT)
            return (__builtin_ctz((U32) val) >> 3);
#       else
            static const int DeBruijnBytePos[32] = { 0, 0, 3, 0, 3, 1, 3, 0, 3, 2, 2, 1, 3, 2, 0, 1, 3, 3, 1, 2, 2, 2, 2, 0, 3, 1, 2, 0, 1, 0, 1, 1 };
            return DeBruijnBytePos[((U32)((val & -(S32)val) * 0x077CB531U)) >> 27];
#       endif
        }
    } else   /* Big Endian CPU */
    {
        if (LZ4_64bits()) {
#       if defined(_MSC_VER) && defined(_WIN64) && !defined(LZ4_FORCE_SW_BITCOUNT)
            unsigned long r = 0;
            _BitScanReverse64( &r, val );
            return (unsigned)(r>>3);
#       elif (defined(__clang__) || (LZ4_GCC_VERSION >= 304)) && !defined(LZ4_FORCE_SW_BITCOUNT)
            return (__builtin_clzll((U64) val) >> 3);
#       else
            unsigned r;
            if (!(val>>32)) { r=4; } else { r=0; val>>=32; }
            if (!(val>>16)) { r+=2; val>>=8; } else { val>>=24; }
            r += (!val);
            return r;
#       endif
        } else /* 32 bits */
        {
#       if defined(_MSC_VER) && !defined(LZ4_FORCE_SW_BITCOUNT)
            unsigned long r = 0;
            _BitScanReverse( &r, (unsigned long)val );
            return (unsigned)(r>>3);
#       elif (defined(__clang__) || (LZ4_GCC_VERSION >= 304)) && !defined(LZ4_FORCE_SW_BITCOUNT)
            return (__builtin_clz((U32) val) >> 3);
#       else
            unsigned r;
            if (!(val>>16)) { r=2; val>>=8; } else { r=0; val>>=24; }
            r += (!val);
            return r;
#       endif
        }
    }
}

static unsigned LZ4_count(const BYTE *pIn, const BYTE *pMatch, const BYTE *pInLimit)
{
    const BYTE *const pStart = pIn;

    while (likely(pIn < pInLimit - (STEPSIZE - 1))) {
        size_t diff = LZ4_read_ARCH(pMatch) ^LZ4_read_ARCH(pIn);
        if (!diff) {
            pIn += STEPSIZE;
            pMatch += STEPSIZE;
            continue;
        }
        pIn += LZ4_NbCommonBytes(diff);
        return (unsigned) (pIn - pStart);
    }

    if (LZ4_64bits())
        if ((pIn < (pInLimit - 3)) && (LZ4_read32(pMatch) == LZ4_read32(pIn))) {
            pIn += 4;
            pMatch += 4;
        }
    if ((pIn < (pInLimit - 1)) && (LZ4_read16(pMatch) == LZ4_read16(pIn))) {
        pIn += 2;
        pMatch += 2;
    }
    if ((pIn < pInLimit) && (*pMatch == *pIn)) pIn++;
    return (unsigned) (pIn - pStart);
}


/**************************************
*  Local Constants
**************************************/
#define LZ4_HASHLOG   (LZ4_MEMORY_USAGE-2)
#define HASHTABLESIZE (1 << LZ4_MEMORY_USAGE)
#define HASH_SIZE_U32 (1 << LZ4_HASHLOG)       /* required as macro for static allocation */

static const int LZ4_64Klimit = ((64 KB) + (MFLIMIT - 1));
static const U32 LZ4_skipTrigger =
        6;  /* Increase this value ==> compression run slower on incompressible data */


/**************************************
*  Local Structures and types
**************************************/
typedef struct {
    U32 hashTable[HASH_SIZE_U32];
    U32 currentOffset;
    U32 initCheck;
    const BYTE *dictionary;
    BYTE *bufferStart;
    /* obsolete, used for slideInputBuffer */
    U32 dictSize;
} LZ4_stream_t_internal;

typedef enum {
    notLimited = 0, limitedOutput = 1
} limitedOutput_directive;
typedef enum {
    byPtr, byU32, byU16
} tableType_t;

typedef enum {
    noDict = 0, withPrefix64k, usingExtDict
} dict_directive;
typedef enum {
    noDictIssue = 0, dictSmall
} dictIssue_directive;

typedef enum {
    endOnOutputSize = 0, endOnInputSize = 1
} endCondition_directive;
typedef enum {
    full = 0, partial = 1
} earlyEnd_directive;


/**************************************
*  Local Utils
**************************************/
static int LZ4_compressBound(std::size_t isize)
{ return LZ4_COMPRESSBOUND(isize); }


/********************************
*  Compression functions
********************************/

static U32 LZ4_hashSequence(U32 sequence, tableType_t const tableType)
{
    if (tableType == byU16)
        return (((sequence) * 2654435761U) >> ((MINMATCH * 8) - (LZ4_HASHLOG + 1)));
    else
        return (((sequence) * 2654435761U) >> ((MINMATCH * 8) - LZ4_HASHLOG));
}

static const U64 prime5bytes = 889523592379ULL;

static U32 LZ4_hashSequence64(size_t sequence, tableType_t const tableType)
{
    const U32 hashLog = (tableType == byU16) ? LZ4_HASHLOG + 1 : LZ4_HASHLOG;
    const U32 hashMask = (1 << hashLog) - 1;
    return ((sequence * prime5bytes) >> (40 - hashLog)) & hashMask;
}

static U32 LZ4_hashSequenceT(size_t sequence, tableType_t const tableType)
{
    if (LZ4_64bits())
        return LZ4_hashSequence64(sequence, tableType);
    return LZ4_hashSequence((U32) sequence, tableType);
}

static U32 LZ4_hashPosition(const void *p, tableType_t tableType)
{ return LZ4_hashSequenceT(LZ4_read_ARCH(p), tableType); }

static void LZ4_putPositionOnHash(const BYTE *p,
                                  U32 h,
                                  void *tableBase,
                                  tableType_t const tableType,
                                  const BYTE *srcBase)
{
    switch (tableType) {
    case byPtr: {
        const BYTE **hashTable = (const BYTE **) tableBase;
        hashTable[h] = p;
        return;
    }
    case byU32: {
        U32 *hashTable = (U32 *) tableBase;
        hashTable[h] = (U32) (p - srcBase);
        return;
    }
    case byU16: {
        U16 *hashTable = (U16 *) tableBase;
        hashTable[h] = (U16) (p - srcBase);
        return;
    }
    }
}

static void LZ4_putPosition(const BYTE *p,
                            void *tableBase,
                            tableType_t tableType,
                            const BYTE *srcBase)
{
    U32 h = LZ4_hashPosition(p, tableType);
    LZ4_putPositionOnHash(p, h, tableBase, tableType, srcBase);
}

static const BYTE *LZ4_getPositionOnHash(U32 h,
                                         void *tableBase,
                                         tableType_t tableType,
                                         const BYTE *srcBase)
{
    if (tableType == byPtr) {
        const BYTE **hashTable = (const BYTE **) tableBase;
        return hashTable[h];
    }
    if (tableType == byU32) {
        U32 *hashTable = (U32 *) tableBase;
        return hashTable[h] + srcBase;
    }
    {
        U16 *hashTable = (U16 *) tableBase;
        return hashTable[h] + srcBase;
    }   /* default, to ensure a return */
}

static const BYTE *LZ4_getPosition(const BYTE *p,
                                   void *tableBase,
                                   tableType_t tableType,
                                   const BYTE *srcBase)
{
    U32 h = LZ4_hashPosition(p, tableType);
    return LZ4_getPositionOnHash(h, tableBase, tableType, srcBase);
}

FORCE_INLINE int LZ4_compress_generic(void *const ctx,
                                      const char *const source,
                                      char *const dest,
                                      const int inputSize,
                                      const int maxOutputSize,
                                      const limitedOutput_directive outputLimited,
                                      const tableType_t tableType,
                                      const dict_directive dict,
                                      const dictIssue_directive dictIssue,
                                      const U32 acceleration)
{
    LZ4_stream_t_internal *const dictPtr = (LZ4_stream_t_internal *) ctx;

    const BYTE *ip = (const BYTE *) source;
    const BYTE *base = (const BYTE *) source;
    const BYTE *lowLimit = (const BYTE *) source;
    const BYTE *const lowRefLimit = ip - dictPtr->dictSize;
    const BYTE *const dictionary = dictPtr->dictionary;
    const BYTE *const dictEnd = dictionary + dictPtr->dictSize;
    const size_t dictDelta = dictEnd - (const BYTE *) source;
    const BYTE *anchor = (const BYTE *) source;
    const BYTE *const iend = ip + inputSize;
    const BYTE *const mflimit = iend - MFLIMIT;
    const BYTE *const matchlimit = iend - LASTLITERALS;

    BYTE *op = (BYTE *) dest;
    BYTE *const olimit = op + maxOutputSize;

    U32 forwardH;
    size_t refDelta = 0;

    /* Init conditions */
    if ((U32) inputSize > (U32) LZ4_MAX_INPUT_SIZE)
        return 0;   /* Unsupported input size, too large (or negative) */
    switch (dict) {
    case noDict:
    default:
        base = (const BYTE *) source;
        lowLimit = (const BYTE *) source;
        break;
    case withPrefix64k:
        base = (const BYTE *) source - dictPtr->currentOffset;
        lowLimit = (const BYTE *) source - dictPtr->dictSize;
        break;
    case usingExtDict:
        base = (const BYTE *) source - dictPtr->currentOffset;
        lowLimit = (const BYTE *) source;
        break;
    }
    if ((tableType == byU16) && (inputSize >= LZ4_64Klimit))
        return 0;   /* Size too large (not within 64K limit) */
    if (inputSize < LZ4_minLength)
        goto _last_literals;                  /* Input too small, no compression (all literals) */

    /* First Byte */
    LZ4_putPosition(ip, ctx, tableType, base);
    ip++;
    forwardH = LZ4_hashPosition(ip, tableType);

    /* Main Loop */
    for (;;) {
        const BYTE *match;
        BYTE *token;
        {
            const BYTE *forwardIp = ip;
            unsigned step = 1;
            unsigned searchMatchNb = acceleration << LZ4_skipTrigger;

            /* Find a match */
            do {
                U32 h = forwardH;
                ip = forwardIp;
                forwardIp += step;
                step = (searchMatchNb++ >> LZ4_skipTrigger);

                if (unlikely(forwardIp > mflimit)) goto _last_literals;

                match = LZ4_getPositionOnHash(h, ctx, tableType, base);
                if (dict == usingExtDict) {
                    if (match < (const BYTE *) source) {
                        refDelta = dictDelta;
                        lowLimit = dictionary;
                    } else {
                        refDelta = 0;
                        lowLimit = (const BYTE *) source;
                    }
                }
                forwardH = LZ4_hashPosition(forwardIp, tableType);
                LZ4_putPositionOnHash(ip, h, ctx, tableType, base);

            } while (((dictIssue == dictSmall) ? (match < lowRefLimit) : 0) ||
                    ((tableType == byU16) ? 0 : (match + MAX_DISTANCE < ip)) ||
                    (LZ4_read32(match + refDelta) != LZ4_read32(ip)));
        }

        /* Catch up */
        while ((ip > anchor) &&
                (match + refDelta > lowLimit) &&
                (unlikely(ip[-1] == match[refDelta - 1]))) {
            ip--;
            match--;
        }

        {
            /* Encode Literal length */
            unsigned litLength = (unsigned) (ip - anchor);
            token = op++;
            if ((outputLimited) &&
                    (unlikely(
                            op + litLength + (2 + 1 + LASTLITERALS) + (litLength / 255) > olimit)))
                return 0;   /* Check output limit */
            if (litLength >= RUN_MASK) {
                int len = (int) litLength - RUN_MASK;
                *token = (RUN_MASK << ML_BITS);
                for (; len >= 255; len -= 255) { *op++ = 255; }
                *op++ = (BYTE) len;
            } else *token = (BYTE) (litLength << ML_BITS);

            /* Copy Literals */
            LZ4_wildCopy(op, anchor, op + litLength);
            op += litLength;
        }

        _next_match:
        /* Encode Offset */
        LZ4_writeLE16(op, (U16) (ip - match));
        op += 2;

        /* Encode MatchLength */
        {
            unsigned matchLength;

            if ((dict == usingExtDict) && (lowLimit == dictionary)) {
                const BYTE *limit;
                match += refDelta;
                limit = ip + (dictEnd - match);
                if (limit > matchlimit) limit = matchlimit;
                matchLength = LZ4_count(ip + MINMATCH, match + MINMATCH, limit);
                ip += MINMATCH + matchLength;
                if (ip == limit) {
                    unsigned more = LZ4_count(ip, (const BYTE *) source, matchlimit);
                    matchLength += more;
                    ip += more;
                }
            } else {
                matchLength = LZ4_count(ip + MINMATCH, match + MINMATCH, matchlimit);
                ip += MINMATCH + matchLength;
            }

            if ((outputLimited) &&
                    (unlikely(op + (1 + LASTLITERALS) + (matchLength >> 8) > olimit)))
                return 0;    /* Check output limit */
            if (matchLength >= ML_MASK) {
                *token += ML_MASK;
                matchLength -= ML_MASK;
                for (; matchLength >= 510; matchLength -= 510) {
                    *op++ = 255;
                    *op++ = 255;
                }
                if (matchLength >= 255) {
                    matchLength -= 255;
                    *op++ = 255;
                }
                *op++ = (BYTE) matchLength;
            } else *token += (BYTE) (matchLength);
        }

        anchor = ip;

        /* Test end of chunk */
        if (ip > mflimit) break;

        /* Fill table */
        LZ4_putPosition(ip - 2, ctx, tableType, base);

        /* Test next position */
        match = LZ4_getPosition(ip, ctx, tableType, base);
        if (dict == usingExtDict) {
            if (match < (const BYTE *) source) {
                refDelta = dictDelta;
                lowLimit = dictionary;
            } else {
                refDelta = 0;
                lowLimit = (const BYTE *) source;
            }
        }
        LZ4_putPosition(ip, ctx, tableType, base);
        if (((dictIssue == dictSmall) ? (match >= lowRefLimit) : 1) &&
                (match + MAX_DISTANCE >= ip) &&
                (LZ4_read32(match + refDelta) == LZ4_read32(ip))) {
            token = op++;
            *token = 0;
            goto _next_match;
        }

        /* Prepare next loop */
        forwardH = LZ4_hashPosition(++ip, tableType);
    }

    _last_literals:
    /* Encode Last Literals */
    {
        const size_t lastRun = (size_t) (iend - anchor);
        if ((outputLimited) &&
                ((op - (BYTE *) dest) + lastRun + 1 + ((lastRun + 255 - RUN_MASK) / 255) >
                        (U32) maxOutputSize))
            return 0;   /* Check output limit */
        if (lastRun >= RUN_MASK) {
            size_t accumulator = lastRun - RUN_MASK;
            *op++ = RUN_MASK << ML_BITS;
            for (; accumulator >= 255; accumulator -= 255) { *op++ = 255; }
            *op++ = (BYTE) accumulator;
        } else {
            *op++ = (BYTE) (lastRun << ML_BITS);
        }
        memcpy(op, anchor, lastRun);
        op += lastRun;
    }

    /* End */
    return (int) (((char *) op) - dest);
}


/*******************************
*  Decompression functions
*******************************/
/*
 * This generic decompression function cover all use cases.
 * It shall be instantiated several times, using different sets of directives
 * Note that it is essential this generic function is really inlined,
 * in order to remove useless branches during compilation optimization.
 */
static bool LZ4_decompress_generic(const Util::ByteView &source, Util::ByteArray &dest)
{
    if (dest.size() < 64)
        dest.resize(64);

    /* Local Variables */
    const BYTE *ip = (const BYTE *) source.data();
    const BYTE *const iend = ip + source.size();

    BYTE *op = (BYTE *) dest.data();
    BYTE *oend = op + dest.size();
    BYTE *cpy;
    const BYTE *lowLimit = op;

    const size_t dec32table[] = {4, 1, 2, 1, 4, 4, 4, 4};
    const size_t dec64table[] = {0, 0, 0, (size_t) -1, 0, 1, 2, 3};


    /* Main Loop */
    while (1) {
        unsigned token;
        size_t length;
        const BYTE *match;

        /* get literal length */
        token = *ip++;
        if ((length = (token >> ML_BITS)) == RUN_MASK) {
            unsigned s;
            do {
                s = *ip++;
                length += s;
            } while (likely(ip < iend - RUN_MASK && (s == 255)));
            if (unlikely((size_t) (op + length) < (size_t) (op)))
                return false;   /* overflow detection */
            if (unlikely((size_t) (ip + length) < (size_t) (ip)))
                return false;   /* overflow detection */
        }

        /* copy literals */
        cpy = op + length;
        if (unlikely(cpy + MFLIMIT > oend)) {
            size_t offset = op - (BYTE *) dest.data();
            dest.resize(INTEGER::growAlloc(offset + length + MFLIMIT));
            op = (BYTE *) dest.data();
            lowLimit = op;
            oend = op + dest.size();
            op += offset;
            cpy = op + length;
        }
        if (ip + length > iend - (2 + 1 + LASTLITERALS)) {
            if (ip + length != iend) return false;   /* Error : input must be consumed */
            memcpy(op, ip, length);
            ip += length;
            op += length;
            break;     /* Necessarily EOF, due to parsing restrictions */
        }
        LZ4_wildCopy(op, ip, cpy);
        ip += length;
        op = cpy;

        /* get offset */
        match = cpy - LZ4_readLE16(ip);
        ip += 2;
        if (unlikely(match < lowLimit))
            return false;   /* Error : offset outside destination buffer */

        /* get matchlength */
        length = token & ML_MASK;
        if (length == ML_MASK) {
            unsigned s;
            do {
                if (ip > iend - LASTLITERALS)
                    return false;
                s = *ip++;
                length += s;
            } while (s == 255);
            if (unlikely((size_t) (op + length) < (size_t) op))
                return false;   /* overflow detection */
        }
        length += MINMATCH;

        /* copy repeated sequence */
        cpy = op + length;
        if (unlikely(cpy + 12 > oend)) {
            size_t offset = op - (BYTE *) dest.data();
            size_t moffset = match - (BYTE *) dest.data();
            dest.resize(INTEGER::growAlloc(offset + length + 12));
            op = (BYTE *) dest.data();
            lowLimit = op;
            match = op + moffset;
            oend = op + dest.size();
            op += offset;
            cpy = op + length;
        }
        if (unlikely((op - match) < 8)) {
            const size_t dec64 = dec64table[op - match];
            op[0] = match[0];
            op[1] = match[1];
            op[2] = match[2];
            op[3] = match[3];
            match += dec32table[op - match];
            LZ4_copy4(op + 4, match);
            op += 8;
            match -= dec64;
        } else {
            LZ4_copy8(op, match);
            op += 8;
            match += 8;
        }

        LZ4_wildCopy(op, match, cpy);
        op = cpy;   /* correction */
    }

    dest.resize(((char *) op) - dest.data<char *>());
    return true;
}


CompressorLZ4::CompressorLZ4()
{
    hashTable = malloc(sizeof(LZ4_stream_t));
    Q_ASSERT(hashTable);
}

CompressorLZ4::~CompressorLZ4()
{
    free(hashTable);
}

bool CompressorLZ4::compress(const Util::ByteView &input,
                             Util::ByteArray &output,
                             std::size_t maximumSize) const
{ return compressSkip(input, output, 0, maximumSize); }

bool CompressorLZ4::compressSkip(const Util::ByteView &input,
                                 Util::ByteArray &output,
                                 std::size_t skip,
                                 std::size_t maximumSize) const
{
    Q_ASSERT(skip >= 0);
    if (input.empty()) {
        output.resize(skip);
        return true;
    }

    Q_ASSERT(input.size() < LZ4_MAX_INPUT_SIZE);

    if (maximumSize <= 0)
        maximumSize = LZ4_compressBound(input.size());
    output.resize(skip + maximumSize);

    LZ4_stream_t *state = (LZ4_stream_t *) hashTable;
    LZ4_resetStream(state);

    int result;
    if (input.size() < LZ4_64Klimit) {
        result = LZ4_compress_generic(state, input.data<const char *>(),
                                      output.data<char *>() + skip, input.size(), maximumSize,
                                      limitedOutput, byU16, noDict, noDictIssue, 1);
    } else {
        result = LZ4_compress_generic(state, input.data<const char *>(),
                                      output.data<char *>() + skip, input.size(), maximumSize,
                                      limitedOutput, LZ4_64bits() ? byU32 : byPtr, noDict,
                                      noDictIssue, 1);
    }

    if (result <= 0) {
        output.resize(skip);
        return false;
    }
    output.resize(result + skip);
    return true;
}


DecompressorLZ4::DecompressorLZ4() = default;

DecompressorLZ4::~DecompressorLZ4() = default;

bool DecompressorLZ4::decompress(const Util::ByteView &input,
                                 Util::ByteArray &output,
                                 std::size_t outputSize) const
{
    if (input.empty()) {
        output.clear();
        return true;
    }
    if (outputSize > 0)
        output.resize(outputSize);

    if (!LZ4_decompress_generic(input, output)) {
        output.clear();
        return false;
    }

    return true;
}

/*--------------------------------
 * LZ4HC 
 */



/**************************************
*  Local Compiler Options
**************************************/
#if defined(__GNUC__)
#  pragma GCC diagnostic ignored "-Wunused-function"
#endif

#if defined (__clang__)
#  pragma clang diagnostic ignored "-Wunused-function"
#endif


/**************************************
*  Common LZ4 definition
**************************************/
#define LZ4_COMMONDEFS_ONLY


/**************************************
*  Local Constants
**************************************/
#define DICTIONARY_LOGSIZE 16
#define MAXD (1<<DICTIONARY_LOGSIZE)
#define MAXD_MASK (MAXD - 1)

#define HASH_LOG (DICTIONARY_LOGSIZE-1)
#undef HASHTABLESIZE
#define HASHTABLESIZE (1 << HASH_LOG)
#define HASH_MASK (HASHTABLESIZE - 1)

#define OPTIMAL_ML (int)((ML_MASK-1)+MINMATCH)

static const int g_maxCompressionLevel = 16;


/**************************************
*  Local Types
**************************************/
typedef struct {
    U32 hashTable[HASHTABLESIZE];
    U16 chainTable[MAXD];
    const BYTE *end;
    /* next block here to continue on current prefix */
    const BYTE *base;
    /* All index relative to this position */
    const BYTE *dictBase;
    /* alternate base for extDict */
    BYTE *inputBuffer;
    /* deprecated */
    U32 dictLimit;
    /* below that point, need extDict */
    U32 lowLimit;
    /* below that point, no more dict */
    U32 nextToUpdate;
    /* index from which to continue dictionary update */
    U32 compressionLevel;
} LZ4HC_Data_Structure;


/**************************************
*  Local Macros
**************************************/
#define HASH_FUNCTION(i)       (((i) * 2654435761U) >> ((MINMATCH*8)-HASH_LOG))
//#define DELTANEXTU16(p)        chainTable[(p) & MAXD_MASK]   /* flexible, MAXD dependent */
#define DELTANEXTU16(p)        chainTable[(U16)(p)]   /* faster */

static U32 LZ4HC_hashPtr(const void *ptr)
{ return HASH_FUNCTION(LZ4_read32(ptr)); }


/**************************************
*  HC Compression
**************************************/
static void LZ4HC_init(LZ4HC_Data_Structure *hc4, const BYTE *start)
{
    MEM_INIT((void *) hc4->hashTable, 0, sizeof(hc4->hashTable));
    MEM_INIT(hc4->chainTable, 0xFF, sizeof(hc4->chainTable));
    hc4->nextToUpdate = 64 KB;
    hc4->base = start - 64 KB;
    hc4->end = start;
    hc4->dictBase = start - 64 KB;
    hc4->dictLimit = 64 KB;
    hc4->lowLimit = 64 KB;
}


/* Update chains up to ip (excluded) */
FORCE_INLINE void LZ4HC_Insert(LZ4HC_Data_Structure *hc4, const BYTE *ip)
{
    U16 *chainTable = hc4->chainTable;
    U32 *HashTable = hc4->hashTable;
    const BYTE *const base = hc4->base;
    const U32 target = (U32) (ip - base);
    U32 idx = hc4->nextToUpdate;

    while (idx < target) {
        U32 h = LZ4HC_hashPtr(base + idx);
        size_t delta = idx - HashTable[h];
        if (delta > MAX_DISTANCE) delta = MAX_DISTANCE;
        DELTANEXTU16(idx) = (U16) delta;
        HashTable[h] = idx;
        idx++;
    }

    hc4->nextToUpdate = target;
}


FORCE_INLINE int LZ4HC_InsertAndFindBestMatch(LZ4HC_Data_Structure *hc4,   /* Index table will be updated */
                                              const BYTE *ip,
                                              const BYTE *const iLimit,
                                              const BYTE **matchpos,
                                              const int maxNbAttempts)
{
    U16 *const chainTable = hc4->chainTable;
    U32 *const HashTable = hc4->hashTable;
    const BYTE *const base = hc4->base;
    const BYTE *const dictBase = hc4->dictBase;
    const U32 dictLimit = hc4->dictLimit;
    const U32 lowLimit =
            (hc4->lowLimit + 64 KB > (U32) (ip - base)) ? hc4->lowLimit : (U32) (ip - base) -
                    (64 KB - 1);
    U32 matchIndex;
    const BYTE *match;
    int nbAttempts = maxNbAttempts;
    size_t ml = 0;

    /* HC4 match finder */
    LZ4HC_Insert(hc4, ip);
    matchIndex = HashTable[LZ4HC_hashPtr(ip)];

    while ((matchIndex >= lowLimit) && (nbAttempts)) {
        nbAttempts--;
        if (matchIndex >= dictLimit) {
            match = base + matchIndex;
            if (*(match + ml) == *(ip + ml) && (LZ4_read32(match) == LZ4_read32(ip))) {
                size_t mlt = LZ4_count(ip + MINMATCH, match + MINMATCH, iLimit) + MINMATCH;
                if (mlt > ml) {
                    ml = mlt;
                    *matchpos = match;
                }
            }
        } else {
            match = dictBase + matchIndex;
            if (LZ4_read32(match) == LZ4_read32(ip)) {
                size_t mlt;
                const BYTE *vLimit = ip + (dictLimit - matchIndex);
                if (vLimit > iLimit) vLimit = iLimit;
                mlt = LZ4_count(ip + MINMATCH, match + MINMATCH, vLimit) + MINMATCH;
                if ((ip + mlt == vLimit) && (vLimit < iLimit))
                    mlt += LZ4_count(ip + mlt, base + dictLimit, iLimit);
                if (mlt > ml) {
                    ml = mlt;
                    *matchpos = base + matchIndex;
                }   /* virtual matchpos */
            }
        }
        matchIndex -= DELTANEXTU16(matchIndex);
    }

    return (int) ml;
}


FORCE_INLINE int LZ4HC_InsertAndGetWiderMatch(LZ4HC_Data_Structure *hc4,
                                              const BYTE *const ip,
                                              const BYTE *const iLowLimit,
                                              const BYTE *const iHighLimit,
                                              int longest,
                                              const BYTE **matchpos,
                                              const BYTE **startpos,
                                              const int maxNbAttempts)
{
    U16 *const chainTable = hc4->chainTable;
    U32 *const HashTable = hc4->hashTable;
    const BYTE *const base = hc4->base;
    const U32 dictLimit = hc4->dictLimit;
    const BYTE *const lowPrefixPtr = base + dictLimit;
    const U32 lowLimit =
            (hc4->lowLimit + 64 KB > (U32) (ip - base)) ? hc4->lowLimit : (U32) (ip - base) -
                    (64 KB - 1);
    const BYTE *const dictBase = hc4->dictBase;
    U32 matchIndex;
    int nbAttempts = maxNbAttempts;
    int delta = (int) (ip - iLowLimit);


    /* First Match */
    LZ4HC_Insert(hc4, ip);
    matchIndex = HashTable[LZ4HC_hashPtr(ip)];

    while ((matchIndex >= lowLimit) && (nbAttempts)) {
        nbAttempts--;
        if (matchIndex >= dictLimit) {
            const BYTE *matchPtr = base + matchIndex;
            if (*(iLowLimit + longest) == *(matchPtr - delta + longest))
                if (LZ4_read32(matchPtr) == LZ4_read32(ip)) {
                    int mlt = MINMATCH + LZ4_count(ip + MINMATCH, matchPtr + MINMATCH, iHighLimit);
                    int back = 0;

                    while ((ip + back > iLowLimit) &&
                            (matchPtr + back > lowPrefixPtr) &&
                            (ip[back - 1] == matchPtr[back - 1])) {
                        back--;
                    }

                    mlt -= back;

                    if (mlt > longest) {
                        longest = (int) mlt;
                        *matchpos = matchPtr + back;
                        *startpos = ip + back;
                    }
                }
        } else {
            const BYTE *matchPtr = dictBase + matchIndex;
            if (LZ4_read32(matchPtr) == LZ4_read32(ip)) {
                size_t mlt;
                int back = 0;
                const BYTE *vLimit = ip + (dictLimit - matchIndex);
                if (vLimit > iHighLimit) vLimit = iHighLimit;
                mlt = LZ4_count(ip + MINMATCH, matchPtr + MINMATCH, vLimit) + MINMATCH;
                if ((ip + mlt == vLimit) && (vLimit < iHighLimit))
                    mlt += LZ4_count(ip + mlt, base + dictLimit, iHighLimit);
                while ((ip + back > iLowLimit) &&
                        (matchIndex + back > lowLimit) &&
                        (ip[back - 1] == matchPtr[back - 1])) {
                    back--;
                }
                mlt -= back;
                if ((int) mlt > longest) {
                    longest = (int) mlt;
                    *matchpos = base + matchIndex + back;
                    *startpos = ip + back;
                }
            }
        }
        matchIndex -= DELTANEXTU16(matchIndex);
    }

    return longest;
}


#define LZ4HC_DEBUG 0
#if LZ4HC_DEBUG
static unsigned debug = 0;
#endif

FORCE_INLINE int LZ4HC_encodeSequence(const BYTE **ip,
                                      BYTE **op,
                                      const BYTE **anchor,
                                      int matchLength,
                                      const BYTE *const match,
                                      limitedOutput_directive limitedOutputBuffer,
                                      BYTE *oend)
{
    int length;
    BYTE *token;

#if LZ4HC_DEBUG
    if (debug) printf("literal : %u  --  match : %u  --  offset : %u\n", (U32)(*ip - *anchor), (U32)matchLength, (U32)(*ip-match));
#endif

    /* Encode Literal length */
    length = (int) (*ip - *anchor);
    token = (*op)++;
    if ((limitedOutputBuffer) && ((*op + (length >> 8) + length + (2 + 1 + LASTLITERALS)) > oend))
        return 1;   /* Check output limit */
    if (length >= (int) RUN_MASK) {
        int len;
        *token = (RUN_MASK << ML_BITS);
        len = length - RUN_MASK;
        for (; len > 254; len -= 255) { *(*op)++ = 255; }
        *(*op)++ = (BYTE) len;
    } else *token = (BYTE) (length << ML_BITS);

    /* Copy Literals */
    LZ4_wildCopy(*op, *anchor, (*op) + length);
    *op += length;

    /* Encode Offset */
    LZ4_writeLE16(*op, (U16) (*ip - match));
    *op += 2;

    /* Encode MatchLength */
    length = (int) (matchLength - MINMATCH);
    if ((limitedOutputBuffer) && (*op + (length >> 8) + (1 + LASTLITERALS) > oend))
        return 1;   /* Check output limit */
    if (length >= (int) ML_MASK) {
        *token += ML_MASK;
        length -= ML_MASK;
        for (; length > 509; length -= 510) {
            *(*op)++ = 255;
            *(*op)++ = 255;
        }
        if (length > 254) {
            length -= 255;
            *(*op)++ = 255;
        }
        *(*op)++ = (BYTE) length;
    } else *token += (BYTE) (length);

    /* Prepare next loop */
    *ip += matchLength;
    *anchor = *ip;

    return 0;
}


static int LZ4HC_compress_generic(void *ctxvoid,
                                  const char *source,
                                  char *dest,
                                  int inputSize,
                                  int maxOutputSize,
                                  int compressionLevel,
                                  limitedOutput_directive limit)
{
    LZ4HC_Data_Structure *ctx = (LZ4HC_Data_Structure *) ctxvoid;
    const BYTE *ip = (const BYTE *) source;
    const BYTE *anchor = ip;
    const BYTE *const iend = ip + inputSize;
    const BYTE *const mflimit = iend - MFLIMIT;
    const BYTE *const matchlimit = (iend - LASTLITERALS);

    BYTE *op = (BYTE *) dest;
    BYTE *const oend = op + maxOutputSize;

    unsigned maxNbAttempts;
    int ml, ml2, ml3, ml0;
    const BYTE *ref = NULL;
    const BYTE *start2 = NULL;
    const BYTE *ref2 = NULL;
    const BYTE *start3 = NULL;
    const BYTE *ref3 = NULL;
    const BYTE *start0;
    const BYTE *ref0;


    /* init */
    if (compressionLevel > g_maxCompressionLevel) compressionLevel = g_maxCompressionLevel;
    if (compressionLevel < 1) compressionLevel = 9;
    maxNbAttempts = 1 << (compressionLevel - 1);
    ctx->end += inputSize;

    ip++;

    /* Main Loop */
    while (ip < mflimit) {
        ml = LZ4HC_InsertAndFindBestMatch(ctx, ip, matchlimit, (&ref), maxNbAttempts);
        if (!ml) {
            ip++;
            continue;
        }

        /* saved, in case we would skip too much */
        start0 = ip;
        ref0 = ref;
        ml0 = ml;

        _Search2:
        if (ip + ml < mflimit)
            ml2 = LZ4HC_InsertAndGetWiderMatch(ctx, ip + ml - 2, ip + 1, matchlimit, ml, &ref2,
                                               &start2, maxNbAttempts);
        else ml2 = ml;

        if (ml2 == ml)  /* No better match */
        {
            if (LZ4HC_encodeSequence(&ip, &op, &anchor, ml, ref, limit, oend)) return 0;
            continue;
        }

        if (start0 < ip) {
            if (start2 < ip + ml0)   /* empirical */
            {
                ip = start0;
                ref = ref0;
                ml = ml0;
            }
        }

        /* Here, start0==ip */
        if ((start2 - ip) < 3)   /* First Match too small : removed */
        {
            ml = ml2;
            ip = start2;
            ref = ref2;
            goto _Search2;
        }

        _Search3:
        /*
        * Currently we have :
        * ml2 > ml1, and
        * ip1+3 <= ip2 (usually < ip1+ml1)
        */
        if ((start2 - ip) < OPTIMAL_ML) {
            int correction;
            int new_ml = ml;
            if (new_ml > OPTIMAL_ML) new_ml = OPTIMAL_ML;
            if (ip + new_ml > start2 + ml2 - MINMATCH)
                new_ml = (int) (start2 - ip) + ml2 - MINMATCH;
            correction = new_ml - (int) (start2 - ip);
            if (correction > 0) {
                start2 += correction;
                ref2 += correction;
                ml2 -= correction;
            }
        }
        /* Now, we have start2 = ip+new_ml, with new_ml = min(ml, OPTIMAL_ML=18) */

        if (start2 + ml2 < mflimit)
            ml3 = LZ4HC_InsertAndGetWiderMatch(ctx, start2 + ml2 - 3, start2, matchlimit, ml2,
                                               &ref3, &start3, maxNbAttempts);
        else ml3 = ml2;

        if (ml3 == ml2) /* No better match : 2 sequences to encode */
        {
            /* ip & ref are known; Now for ml */
            if (start2 < ip + ml) ml = (int) (start2 - ip);
            /* Now, encode 2 sequences */
            if (LZ4HC_encodeSequence(&ip, &op, &anchor, ml, ref, limit, oend)) return 0;
            ip = start2;
            if (LZ4HC_encodeSequence(&ip, &op, &anchor, ml2, ref2, limit, oend)) return 0;
            continue;
        }

        if (start3 < ip + ml + 3) /* Not enough space for match 2 : remove it */
        {
            if (start3 >=
                    (ip +
                            ml)) /* can write Seq1 immediately ==> Seq2 is removed, so Seq3 becomes Seq1 */
            {
                if (start2 < ip + ml) {
                    int correction = (int) (ip + ml - start2);
                    start2 += correction;
                    ref2 += correction;
                    ml2 -= correction;
                    if (ml2 < MINMATCH) {
                        start2 = start3;
                        ref2 = ref3;
                        ml2 = ml3;
                    }
                }

                if (LZ4HC_encodeSequence(&ip, &op, &anchor, ml, ref, limit, oend)) return 0;
                ip = start3;
                ref = ref3;
                ml = ml3;

                start0 = start2;
                ref0 = ref2;
                ml0 = ml2;
                goto _Search2;
            }

            start2 = start3;
            ref2 = ref3;
            ml2 = ml3;
            goto _Search3;
        }

        /*
        * OK, now we have 3 ascending matches; let's write at least the first one
        * ip & ref are known; Now for ml
        */
        if (start2 < ip + ml) {
            if ((start2 - ip) < (int) ML_MASK) {
                int correction;
                if (ml > OPTIMAL_ML) ml = OPTIMAL_ML;
                if (ip + ml > start2 + ml2 - MINMATCH) ml = (int) (start2 - ip) + ml2 - MINMATCH;
                correction = ml - (int) (start2 - ip);
                if (correction > 0) {
                    start2 += correction;
                    ref2 += correction;
                    ml2 -= correction;
                }
            } else {
                ml = (int) (start2 - ip);
            }
        }
        if (LZ4HC_encodeSequence(&ip, &op, &anchor, ml, ref, limit, oend)) return 0;

        ip = start2;
        ref = ref2;
        ml = ml2;

        start2 = start3;
        ref2 = ref3;
        ml2 = ml3;

        goto _Search3;
    }

    /* Encode Last Literals */
    {
        int lastRun = (int) (iend - anchor);
        if ((limit) &&
                (((char *) op - dest) + lastRun + 1 + ((lastRun + 255 - RUN_MASK) / 255) >
                        (U32) maxOutputSize))
            return 0;  /* Check output limit */
        if (lastRun >= (int) RUN_MASK) {
            *op++ = (RUN_MASK << ML_BITS);
            lastRun -= RUN_MASK;
            for (; lastRun > 254; lastRun -= 255) { *op++ = 255; }
            *op++ = (BYTE) lastRun;
        } else *op++ = (BYTE) (lastRun << ML_BITS);
        memcpy(op, anchor, iend - anchor);
        op += iend - anchor;
    }

    /* End */
    return (int) (((char *) op) - dest);
}


static int LZ4_sizeofStateHC(void)
{ return sizeof(LZ4HC_Data_Structure); }

static int LZ4_compress_HC_extStateHC(void *state,
                                      const char *src,
                                      char *dst,
                                      int srcSize,
                                      int maxDstSize,
                                      int compressionLevel)
{
    if (((size_t) (state) & (sizeof(void *) - 1)) != 0)
        return 0;   /* Error : state is not aligned for pointers (32 or 64 bits) */
    LZ4HC_init((LZ4HC_Data_Structure *) state, (const BYTE *) src);
    if (maxDstSize < LZ4_compressBound(srcSize))
        return LZ4HC_compress_generic(state, src, dst, srcSize, maxDstSize, compressionLevel,
                                      limitedOutput);
    else
        return LZ4HC_compress_generic(state, src, dst, srcSize, maxDstSize, compressionLevel,
                                      notLimited);
}


CompressorLZ4HC::CompressorLZ4HC()
{
    internal = malloc(sizeof(LZ4HC_Data_Structure));
    Q_ASSERT(internal);
}

CompressorLZ4HC::~CompressorLZ4HC()
{
    free(internal);
}

bool CompressorLZ4HC::compress(const Util::ByteView &input,
                               Util::ByteArray &output,
                               std::size_t maximumSize) const
{
    if (input.empty()) {
        output.clear();
        return true;
    }

    Q_ASSERT(input.size() < LZ4_MAX_INPUT_SIZE);

    if (maximumSize <= 0)
        maximumSize = LZ4_compressBound(input.size());

    output.resize(maximumSize);
    auto result = LZ4_compress_HC_extStateHC((LZ4HC_Data_Structure *) internal,
                                             input.data<const char *>(), output.data<char *>(),
                                             input.size(), output.size(), 9);
    if (result <= 0) {
        output.clear();
        return false;
    }
    output.resize(result);
    return true;
}

}
