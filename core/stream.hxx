/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3CORESTREAM_H
#define CPD3CORESTREAM_H

#include "core/first.hxx"

#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <iterator>
#include <deque>

#include <QtGlobal>
#include <QtAlgorithms>
#include <QDataStream>
#include <QList>
#include <QMetaType>
#include <QDebug>

#include "core.hxx"
#include "number.hxx"
#include "range.hxx"
#include "qtcompat.hxx"
#include "util.hxx"

namespace CPD3 {

/**
 * Overlay one stream of data on top of another.  This class provides an
 * interface that takes on stream of container data and combines it
 * with an output stream of data.  The overlay stream of data must always
 * be ahead of the combined data stream.  In both cases the "position" of
 * the stream is described by the latest start time received in it.
 * <br>
 * The StreamType must provide getStart(), getEnd(), setStart(double),
 * setEnd(double) and a copy constructor.  The OverlayType must provide
 * a applyOverlay(StreamType &, bool) function to apply the overlay.  The
 * boolean parameter is set if the stream type intersects an overlay segment.  
 * This may return false to drop the value from the output.
 * 
 * @tparam StreamType   the data type of the value stream
 * @tparam OverlayType  the data type of the overlay segments
 * @tparam ResultType   the type of the returned list of results
 */
template<typename StreamType, typename OverlayType, typename ResultType = std::vector<StreamType> >
class OverlayStream {
    struct OverlaySegment {
        double start;
        double end;
        OverlayType data;

        OverlaySegment() = default;

        OverlaySegment(const OverlayType &data, double start, double end) : start(start),
                                                                            end(end),
                                                                            data(data)
        { }

        OverlaySegment(const OverlaySegment &) = default;

        OverlaySegment &operator=(const OverlaySegment &) = default;

        OverlaySegment(OverlaySegment &&) = default;

        OverlaySegment &operator=(OverlaySegment &&) = default;
    };

    std::deque<OverlaySegment> overlay;
    double overlayAdvanceTime;
    bool overlayEnded;

    struct StreamValue {
        StreamType data;
        bool intersecting;

        inline double getStart() const
        { return data.getStart(); }

        inline double getStart()
        { return data.getStart(); }

        inline double getEnd() const
        { return data.getEnd(); }

        inline double getEnd()
        { return data.getEnd(); }

        StreamValue() = default;

        explicit StreamValue(const StreamType &data) : data(data), intersecting(false)
        { }

        StreamValue(const StreamValue &) = default;

        StreamValue &operator=(const StreamValue &) = default;

        StreamValue(StreamValue &&) = default;

        StreamValue &operator=(StreamValue &&) = default;
    };

    std::deque<StreamValue> stream;
    double streamAdvanceTime;

    void appendStream(const StreamType &value)
    {
        /* Have to do this the hard way since values can be relocated
         * by the splitting on the overlay.  We can't just wait for the
         * advance to get past the value's end or we'd block forever on
         * endless values (metadata). */

        if (stream.empty()) {
            /* No possible other location, so easy */
            stream.emplace_back(value);
            return;
        }

        Q_ASSERT(!stream.empty());

        if (!FP::defined(value.getStart())) {
            /* Undefined start, so it's either at the end (all values
             * undefined) or at the end of the undefined values */

            if (!FP::defined(stream.back().getStart())) {
                stream.emplace_back(value);
            } else {
                auto target = Range::lastUndefinedStart(stream.begin(), stream.end());

                Q_ASSERT(target == stream.end() || FP::defined(target->getStart()));
                Q_ASSERT(target == stream.begin() || !FP::defined((target - 1)->getStart()));

                stream.emplace(target, value);
            }
        } else {
            /* Defined start, so check if it's after the current end, if
             * it's not, then we have to do a detailed search. */

            double checkStart = stream.back().getStart();
            if (!FP::defined(checkStart) || checkStart <= value.getStart()) {
                stream.emplace_back(value);
            } else {
                auto target = Range::upperBound(stream.begin(), stream.end(), value.getStart());

                Q_ASSERT(target == stream.end() ||
                                 (FP::defined(target->getStart()) &&
                                         target->getStart() >= value.getStart()));
                Q_ASSERT(target == stream.begin() ||
                                 !FP::defined((target - 1)->getStart()) ||
                                 (target - 1)->getStart() <= value.getStart());

                stream.emplace(target, value);
            }
        }
    }

    void terminateLastOverlay(double nextStart)
    {
        if (overlay.empty())
            return;
        Q_ASSERT(FP::defined(nextStart));
        auto &last = overlay.back();
        Q_ASSERT(!FP::defined(last.start) || last.start < nextStart);
        if (FP::defined(last.end) && last.end <= nextStart)
            return;
        last.end = nextStart;
    }

    void purgeOverlayBefore(double startTime)
    {
        if (!FP::defined(startTime))
            return;
        if (overlay.empty())
            return;
        auto endRemove = overlay.begin();
        for (auto final = overlay.end(); endRemove != final; ++endRemove) {
            if (!FP::defined(endRemove->end))
                break;
            if (endRemove->end > startTime)
                break;
        }
        overlay.erase(overlay.begin(), endRemove);
    }

    void relocateStreamValue(const typename std::deque<StreamValue>::iterator &value,
                             double newStart)
    {
        Q_ASSERT(value != stream.end());
        Q_ASSERT(Range::compareStart(newStart, value->data.getStart()) > 0);

        auto target = Range::upperBound(value, stream.end(), newStart);
        Q_ASSERT(target != value);
        Q_ASSERT(target != stream.begin());

        StreamValue save = std::move(*value);
        save.data.setStart(newStart);
        save.intersecting = true;

        /* Shift everything down into the vacated spot */
        std::move(value + 1, target, value);

        --target;
        *target = std::move(save);

#ifndef NDEBUG
        for (auto check = value + 1, endCheck = stream.end(); check != endCheck; ++check) {
            Q_ASSERT(Range::compareStart((check - 1)->getStart(), check->getStart()) <= 0);
        }
#endif
    }

    bool overlayCanBeApplied(double start, double end) const
    {
        if (overlayEnded)
            return true;
        if (!FP::defined(overlayAdvanceTime))
            return false;

        if (FP::defined(end)) {
            /* If it ends before the overlay can be changed, then we can
             * always handle it */
            if (end <= overlayAdvanceTime)
                return true;
        } else {
            /* If it has no end time, then just handle it immediately,
             * since otherwise it would stall forever.  We need to do
             * this to handle acquisition metadata, which would
             * otherwise stall forever.  We also know we're passed
             * its start already because the overlay must be
             * advanced that far.  We still check it like this
             * to require some advance (i.e. start not equal
             * to overlay), even if we can't reliably check it
             * in the general case (because the advance may be
             * implied rather than explicit) */
            if (!FP::defined(start) || start < overlayAdvanceTime)
                return true;
            return false;
        }

        auto check = overlay.begin();
        auto final = overlay.end();
        if (FP::defined(start)) {
            while (check != final && FP::defined(check->end) && start >= check->end) {
                ++check;
            }
        }

        if (check == final) {
            /* No overlay data and it ends after the overlay advance (above),
             * so we don't know enough to deal with it yet */
            return false;
        }

        /* If we don't know an end time for the first overlay segment, then
         * we don't have enough information to deal with it (unless, above,
         * it ends before the overlay could be changed) */
        if (!FP::defined(check->end))
            return false;

        /* If we have an overlay with an end, we're going to fragment
         * the value, so we can apply to it */
        return overlayAdvanceTime >= check->end;
    }

public:
    OverlayStream()
            : overlay(),
              overlayAdvanceTime(FP::undefined()),
              overlayEnded(false),
              stream(),
              streamAdvanceTime(FP::undefined())
    { }

    OverlayStream(const OverlayStream<StreamType, OverlayType, ResultType> &other) = default;

    OverlayStream &operator=(const OverlayStream<StreamType, OverlayType,
                                                 ResultType> &other) = default;

    OverlayStream(OverlayStream<StreamType, OverlayType, ResultType> &&other) = default;

    OverlayStream &operator=(OverlayStream<StreamType, OverlayType, ResultType> &&other) = default;

    /**
     * Save the state of the overlay stream.
     * 
     * @param stream the target data stream
     */
    void serialize(QDataStream &stream) const
    {
        Serialize::container(stream, overlay, [&stream](const OverlaySegment &add) {
            stream << add.data << add.start << add.end;
        });
        stream << overlayAdvanceTime << overlayEnded;

        Serialize::container(stream, this->stream, [&stream](const StreamValue &add) {
            stream << add.data << add.intersecting;
        });
        stream << streamAdvanceTime;
    }

    /**
     * Restore the state of the overlay stream.
     * 
     * @param stream the source data stream
     */
    void deserialize(QDataStream &stream)
    {
        Deserialize::container(stream, overlay, [&stream]() {
            OverlaySegment add;
            stream >> add.data >> add.start >> add.end;
            return add;
        });
        stream >> overlayAdvanceTime >> overlayEnded;

        Deserialize::container(stream, this->stream, [&stream]() {
            StreamValue add;
            stream >> add.data >> add.intersecting;
            return add;
        });
        stream >> streamAdvanceTime;
    }

    /**
     * Add a segment to the tail of the overlay list.  The previous one
     * is terminated if needed and the new one is left open ended.
     * 
     * @param data      the overlay data
     * @param start     the start that the data is effective at
     */
    void overlayTail(const OverlayType &data, double start)
    {
        Q_ASSERT(!FP::defined(overlayAdvanceTime) || start >= overlayAdvanceTime);
        Q_ASSERT(!FP::defined(streamAdvanceTime) ||
                         (FP::defined(start) && start >= streamAdvanceTime));
        Q_ASSERT(!overlayEnded);

        overlayAdvanceTime = start;
        if (!overlay.empty()) {
            auto &last = overlay.back();
            if (FP::equal(last.start, start)) {
                last.data = data;
                return;
            }
        }
        terminateLastOverlay(start);
        overlay.emplace_back(data, start, FP::undefined());
    }

    /**
     * Add a segment to the tail of the list with a fixed end time.  The
     * previous one will be terminated if needed.
     * 
     * @param data      the overlay data
     * @param start     the start time
     * @param end       the end time
     */
    void overlayInsert(const OverlayType &data, double start, double end)
    {
        Q_ASSERT(!FP::defined(overlayAdvanceTime) || start >= overlayAdvanceTime);
        Q_ASSERT(!FP::defined(streamAdvanceTime) ||
                         (FP::defined(start) && start >= streamAdvanceTime));
        Q_ASSERT(!overlayEnded);

        overlayAdvanceTime = start;
        terminateLastOverlay(start);
        overlay.emplace_back(data, start, end);
    }

    /**
     * Advance the overlay tracking to the given time (no new segments will
     * be inserted starting before the given time).
     * 
     * @param time      the time to advance until
     */
    void overlayAdvance(double time)
    {
        Q_ASSERT(FP::defined(time));
        Q_ASSERT(!FP::defined(overlayAdvanceTime) || time >= overlayAdvanceTime);
        Q_ASSERT(!FP::defined(streamAdvanceTime) || time >= streamAdvanceTime);
        Q_ASSERT(!overlayEnded);
        overlayAdvanceTime = time;
    }

    /**
     * Signal that the overlay stream has ended.  This means that no
     * further advances or data will be added to it.
     */
    void overlayEnd()
    {
        overlayEnded = true;
    }

    /**
     * Add a value to the data stream.
     * 
     * @param add       the value to add
     */
    void streamInsert(const StreamType &add)
    {
        Q_ASSERT(!FP::defined(streamAdvanceTime) ||
                         (FP::defined(add.getStart()) && add.getStart() >= streamAdvanceTime));
        streamAdvanceTime = add.getStart();
        appendStream(add);
    }

    /**
     * Advance the overlay stream to the given time, or just retrieve any
     * pending values if the time is undefined.
     * 
     * @param time      if defined, then no new data values will be inserted starting before this time
     * @param outputAdvanceEnd if not NULL then set to the time the output stream could be advanced until
     * @return          a list of stream values with the overlay applied to them
     */
    ResultType streamAdvance(double time = FP::undefined(), double *outputAdvanceEnd = nullptr)
    {
        if (FP::defined(time)) {
            Q_ASSERT(!FP::defined(streamAdvanceTime) || time >= streamAdvanceTime);
            streamAdvanceTime = time;
        }
        if (stream.empty()) {
            if (outputAdvanceEnd != NULL)
                *outputAdvanceEnd = streamAdvanceTime;
            return ResultType();
        }
        if (!FP::defined(streamAdvanceTime) ||
                (!overlayEnded && !FP::defined(overlayAdvanceTime))) {
            if (outputAdvanceEnd != NULL)
                *outputAdvanceEnd = FP::undefined();
            return ResultType();
        }

        ResultType result;

        bool finishingEqualValues = false;
        double equalInspectStart = 0;
        for (auto value = stream.begin(); value != stream.end();) {
            Q_ASSERT(value == stream.begin() ||
                             Range::compareStart((value - 1)->getStart(), value->getStart()) <= 0);

            /* We can't address this value if it intersects part of the overlay that
             * may change, so just generate all equal ones and done if that's the case */
            if (!overlayCanBeApplied(value->getStart(), value->getEnd())) {
                finishingEqualValues = true;
                equalInspectStart = value->getStart();
                ++value;
                continue;
            }

            if (FP::defined(value->getStart())) {
                if (value->getStart() >= streamAdvanceTime)
                    break;
                if (finishingEqualValues &&
                        (!FP::defined(equalInspectStart) ||
                                value->getStart() != equalInspectStart)) {
                    Q_ASSERT(value->getStart() > equalInspectStart);
                    break;
                }

                purgeOverlayBefore(value->getStart());
            }

            /* No overlay data, so just pass it through */
            if (overlay.empty()) {
                Q_ASSERT(result.empty() ||
                                 Range::compareStart(result.back().getStart(), value->getStart()) <=
                                         0);
                result.push_back(std::move(value->data));
                value = stream.erase(value);
                continue;
            }

            auto &activeOverlay = overlay.front();

            if (FP::defined(activeOverlay.start)) {
                /* Ends completely before the first one, so pass through */
                if (activeOverlay.start >= value->getEnd()) {
                    Q_ASSERT(result.empty() ||
                                     Range::compareStart(result.back().getStart(),
                                                         value->getStart()) <= 0);
                    result.push_back(std::move(value->data));
                    value = stream.erase(value);
                    continue;
                }

                /* Starts before the overlay, so output split it and re-add
                 * the intersecting one */
                if (!FP::defined(value->getStart()) || value->getStart() < activeOverlay.start) {
                    StreamType save = value->data;
                    StreamType future = save;
                    future.setStart(activeOverlay.start);
                    save.setEnd(activeOverlay.start);
                    if (activeOverlay.data.applyOverlay(future, true)) {
                        relocateStreamValue(value, activeOverlay.start);
                        Q_ASSERT(result.empty() || Range::compareStart(result.back().getStart(),
                                                             save.getStart()) <= 0);
                        result.push_back(std::move(save));
                    } else {
                        value = stream.erase(value);
                    }
                    continue;
                }
            }

            /* Ends after the end of the overlay, so it's intersecting */
            if (FP::defined(activeOverlay.end) &&
                    (!FP::defined(value->getEnd()) || value->getEnd() > activeOverlay.end)) {
                StreamType save = value->data;
                save.setEnd(activeOverlay.end);
                if (activeOverlay.data.applyOverlay(save, true)) {
                    relocateStreamValue(value, activeOverlay.end);
                    Q_ASSERT(result.empty() || Range::compareStart(result.back().getStart(),
                                                         save.getStart()) <= 0);
                    result.push_back(std::move(save));
                } else {
                    value = stream.erase(value);
                }
                continue;
            }

            Q_ASSERT(result.empty() ||
                             Range::compareStart(result.back().getStart(), value->getStart()) <= 0);

            /* Contained in the overlay, but may have intersected before, so
             * use the saved value state of intersection. */
            if (activeOverlay.data.applyOverlay(value->data, value->intersecting))
                result.push_back(std::move(value->data));
            value = stream.erase(value);
        }

        if (outputAdvanceEnd) {
            if (!stream.empty()) {
                *outputAdvanceEnd = stream.front().getStart();
                Q_ASSERT(FP::defined(streamAdvanceTime));
                if (FP::defined(*outputAdvanceEnd) && *outputAdvanceEnd > streamAdvanceTime)
                    *outputAdvanceEnd = streamAdvanceTime;
            } else {
                *outputAdvanceEnd = streamAdvanceTime;
            }
        }

        return result;
    }

    /**
     * Finish the stream.  This will emit all pending values.
     * 
     * @return a list of stream values with the overlay applied to them
     */
    ResultType finish()
    {
        ResultType result;

        for (auto value = stream.begin(); value != stream.end();) {
            if (FP::defined(value->getStart()))
                purgeOverlayBefore(value->getStart());

            /* No overlay data, so just pass all remaining ones through */
            if (overlay.empty()) {
                for (auto endStream = stream.end(); value != endStream; ++value) {
                    Q_ASSERT(result.empty() ||
                                     Range::compareStart(result.back().getStart(),
                                                         value->getStart()) <= 0);
                    result.push_back(std::move(value->data));
                }
                break;
            }

            auto &activeOverlay = overlay.front();

            if (FP::defined(activeOverlay.start)) {
                /* Ends completely before the first one, so pass through */
                if (activeOverlay.start >= value->getEnd()) {
                    Q_ASSERT(result.empty() || Range::compareStart(result.back().getStart(),
                                                         value->getStart()) <= 0);
                    result.push_back(std::move(value->data));
                    ++value;
                    continue;
                }

                /* Starts before the overlay, so output split it and re-add
                 * the intersecting one */
                if (!FP::defined(value->getStart()) || value->getStart() < activeOverlay.start) {
                    StreamType save = value->data;
                    StreamType future = save;
                    future.setStart(activeOverlay.start);
                    save.setEnd(activeOverlay.start);
                    if (activeOverlay.data.applyOverlay(future, true)) {
                        relocateStreamValue(value, activeOverlay.start);
                        Q_ASSERT(result.empty() || Range::compareStart(result.back().getStart(),
                                                             save.getStart()) <= 0);
                        result.push_back(std::move(save));
                    } else {
                        ++value;
                    }
                    continue;
                }
            }

            /* Ends after the end of the overlay, so it's intersecting */
            if (FP::defined(activeOverlay.end) &&
                    (!FP::defined(value->getEnd()) || value->getEnd() > activeOverlay.end)) {
                StreamType save = value->data;
                save.setEnd(activeOverlay.end);
                if (activeOverlay.data.applyOverlay(save, true)) {
                    relocateStreamValue(value, activeOverlay.end);
                    Q_ASSERT(result.empty() || Range::compareStart(result.back().getStart(),
                                                         save.getStart()) <= 0);
                    result.push_back(std::move(save));
                } else {
                    ++value;
                }
                continue;
            }

            Q_ASSERT(result.empty() ||
                             Range::compareStart(result.back().getStart(), value->getStart()) <= 0);

            /* Contained in the overlay, but may have intersected before, so
             * use the saved value state of intersection. */
            if (activeOverlay.data.applyOverlay(value->data, value->intersecting))
                result.push_back(std::move(value->data));
            ++value;
        }

        stream.clear();

        return result;
    }
};

template<class StreamType, class OverlayType, typename ResultType>
QDataStream &operator<<(QDataStream &stream,
                        const OverlayStream<StreamType, OverlayType, ResultType> &value)
{
    value.serialize(stream);
    return stream;
}

template<class StreamType, class OverlayType, typename ResultType>
QDataStream &operator>>(QDataStream &stream,
                        OverlayStream<StreamType, OverlayType, ResultType> &value)
{
    value.deserialize(stream);
    return stream;
}


/**
 * Generate a stream of fragmenting segments from some start time ascending
 * input stream.  This class provides the backend storage and management to
 * deal with the fragmenting of the input stream.  The derived implementation
 * must provide the storage for the active inputs as well as the conversion
 * from the active storage to the output type.  The implementation is used
 * via CRTP, that is, it should provide itself as the template argument.
 * <br>
 * The implementation must provide the following methods:
 * <ul>
 *  <li>convertActive(double, double) const - Convert the current active inputs
 *          to an output type going from the provided start to end.  This must
 *          ignore any injected that are before the start time (purging is deferred).
 *  <li>purgeBefore(double) - Remove all inputs that end entirely before the
 *          given time.  This should return true if the storage is non-empty.
 *  <li>addActive(Type) - Add the given input to the storage.
 *  <li>addInjected(Type) - Add the given input to the injected storage.
 *  <li>clearActive() - Remove all inputs.
 *  <li>mergeAll(Type) - Add all values in the other type to this one, this does
 *          not need to purge the active, as that will be done immediately 
 *          after.
 * </ul>
 */
template<typename OutputType, class Implementation, typename ResultType = std::vector<OutputType>>
class SegmentStream {
    enum State {
        /* No inputs, so the first one we see sets the state */
                Empty = 0, /* The current segment is active and accumulating, it can be
         * emitted when we get past next break point. */
        Active, /* The current segment has been emitted, so we don't emit anything
         * until we reach the next break point or something is added, then
         * we transition back to active. */
        Emitted,
    };
    State state;
    double streamPosition;
    double currentStart;
    std::vector<double> transitionPoints;


    inline OutputType convert(double start, double end) const
    {
        return static_cast<const Implementation *>(this)->convertActive(start, end);
    }

    inline bool purge(double before)
    {
        return static_cast<Implementation *>(this)->purgeBefore(before);
    }

    inline void purgeAll()
    {
        static_cast<Implementation *>(this)->clearActive();
    }

    static inline bool transitionCompare(double a, double b)
    { return b < a; }

    void purgeCompletedTransition(double before)
    {
        if (!FP::defined(before))
            return;
        while (!transitionPoints.empty()) {
            double nextEnd = transitionPoints.back();
            Q_ASSERT(FP::defined(nextEnd));
            if (before < nextEnd)
                break;

            /* If we transition past an transition then we're back to
             * normal operation if we'd already emitted that segment */
            if (state == Emitted)
                state = Active;

            transitionPoints.pop_back();
        }
    }

    template<typename InputType>
    bool initializeIfEmpty(InputType &&value)
    {
        switch (state) {
        case Empty:
            break;
        case Active:
        case Emitted:
            return false;
        }

        double start = value.getStart();
        purgeCompletedTransition(start);
        state = Active;
        currentStart = start;
        static_cast<Implementation *>(this)->addActive(std::forward<InputType>(value));
        return true;
    }

    void mergeTransition(const std::vector<double> &other)
    {
        std::vector<double> old;
        old.reserve(transitionPoints.size() + other.size());
        using std::swap;
        swap(old, transitionPoints);
        std::merge(old.begin(), old.end(), other.begin(), other.end(),
                   Util::back_emplacer(transitionPoints), transitionCompare);
        if (transitionPoints.size() > 1) {
            for (std::vector<double>::iterator check = transitionPoints.begin() + 1;
                    check != transitionPoints.end();) {
                if (*check != *(check - 1)) {
                    ++check;
                    continue;
                }
                check = transitionPoints.erase(check);
            }
        }
    }

public:
    SegmentStream() : state(Empty),
                      streamPosition(FP::undefined()),
                      currentStart(FP::undefined()),
                      transitionPoints()
    { }

    SegmentStream(QDataStream &stream) : streamPosition(FP::undefined()),
                                         currentStart(FP::undefined()),
                                         transitionPoints()
    {
        deserialize(stream);
    }

    SegmentStream(const SegmentStream<OutputType, Implementation, ResultType> &other) = default;

    SegmentStream &operator=(const SegmentStream<OutputType, Implementation, ResultType> &other)
    {
        if (&other == this)
            return *this;
        state = other.state;
        streamPosition = other.streamPosition;
        currentStart = other.currentStart;
        transitionPoints = other.transitionPoints;
        purgeAll();
        static_cast<Implementation *>(this)->mergeAll(
                *(static_cast<const Implementation *>(&other)));
        return *this;
    }

    SegmentStream(SegmentStream<OutputType, Implementation, ResultType> &&other) = default;

    SegmentStream &operator=(SegmentStream<OutputType, Implementation, ResultType> &&other)
    {
        if (&other == this)
            return *this;
        state = other.state;
        streamPosition = other.streamPosition;
        currentStart = other.currentStart;
        transitionPoints = std::move(other.transitionPoints);
        purgeAll();
        static_cast<Implementation *>(this)->mergeAll(
                *(static_cast<const Implementation *>(&other)));
        return *this;
    }

    /**
     * Save the state of the stream.
     * 
     * @param stream the target data stream
     */
    void serialize(QDataStream &stream) const
    {
        stream << (quint8) state << streamPosition << currentStart;
        stream << (quint32) transitionPoints.size();
        for (std::vector<double>::const_iterator add = transitionPoints.begin(),
                end = transitionPoints.end(); add != end; ++add) {
            stream << *add;
        }
    }

    /**
     * Restore the state of the stream.
     * 
     * @param stream the source data stream
     */
    void deserialize(QDataStream &stream)
    {
        quint8 i8;
        stream >> i8;
        state = (State) i8;
        stream >> streamPosition >> currentStart;
        quint32 i32;
        stream >> i32;
        transitionPoints.reserve(i32);
        transitionPoints.resize(0);
        for (quint32 i = 0; i < i32; i++) {
            double v;
            stream >> v;
            transitionPoints.push_back(v);
        }
    }

    /**
     * Test if the stream has future segments that might generate output.
     * 
     * @return true if there are any segments in the future
     */
    inline bool hasFutureSegments() const
    { return state != Empty; }

    /**
     * Get the start time that the stream has been advanced until.
     * 
     * @return the advanced start time
     */
    inline double getStreamTime() const
    { return streamPosition; }

    /**
     * Get the next transition time of the stream.  This is the end of
     * a complete segment or the start of the next one if a partial
     * has been emitted.
     */
    double getNextTransition() const
    {
        if (transitionPoints.empty())
            return FP::undefined();
        return transitionPoints.back();
    }

    /**
     * Advance the stream until the given time, generating all completed
     * segments until it.  This will not emit a partial segments.
     * 
     * @param time  the time to advance until
     * @return      the list of completed segments
     */
    ResultType advanceCompleted(double time)
    {
        Q_ASSERT(Range::compareStart(streamPosition, time) <= 0);
        streamPosition = time;

        ResultType result;
        if (!FP::defined(time))
            return result;

        /* This means that we're either empty or that the next start is
         * infinite.  In either case, an advance does nothing. */
        if (transitionPoints.empty())
            return result;

        Q_ASSERT(!transitionPoints.empty());
        Q_ASSERT(FP::defined(transitionPoints.back()));

        switch (state) {
        case Empty: {
            /* We might have injected breaks, so just purge those */
            do {
                double nextEnd = transitionPoints.back();
                Q_ASSERT(FP::defined(nextEnd));
                if (time < nextEnd)
                    break;
                transitionPoints.pop_back();
            } while (!transitionPoints.empty());
            break;
        }
        case Active:
        case Emitted: {
            /* Until the segment completes we can't do anything */
            double nextEnd = transitionPoints.back();
            if (time < nextEnd)
                break;

            /* Add the current segment */
            if (state == Active)
                result.push_back(convert(currentStart, nextEnd));
            currentStart = nextEnd;
            if (!purge(currentStart)) {
                state = Empty;
                break;
            }

            /* We're in a complete segment now, so set the state */
            state = Active;

            /* Remove the break and if there are no more breaks then
             * we're done (either no data or an infinite end) */
            transitionPoints.pop_back();
            if (transitionPoints.empty())
                break;

            /* Now repeat the above for any segments we complete */
            do {
                Q_ASSERT(!transitionPoints.empty());
                Q_ASSERT(FP::defined(transitionPoints.back()));

                /* Done if the next break is ahead of the time */
                nextEnd = transitionPoints.back();
                if (time < nextEnd)
                    break;

                /* Add the segment */
                result.push_back(convert(currentStart, nextEnd));
                currentStart = nextEnd;
                if (!purge(currentStart)) {
                    state = Empty;
                    break;
                }

                /* And advance the break */
                transitionPoints.pop_back();
            } while (!transitionPoints.empty());
            break;
        }
        }

        return result;
    }

    /**
     * Advance the stream until the given time, generating partial segments
     * if it moves beyond a start time.  Future segments may override
     * the partial segments generated by this.
     * 
     * @param time  the time to advance until
     * @return      the list of segments
     */
    ResultType advancePartial(double time)
    {
        /* Advance anything the time would complete */
        ResultType result = advanceCompleted(time);

        /* No time, means we can't be past the first one, so stop */
        if (!FP::defined(time))
            return result;

        switch (state) {
        case Empty:
        case Emitted:
            /* If we're empty or have already emitted, then there's nothing
             * else to do. */
            return result;
        case Active:
            break;
        }

        /* At this stage we know we're at the start of or in the middle
         * of the next segment because the above advance has emitted everything
         * before */
        Q_ASSERT(Range::compareStart(currentStart, time) <= 0);

        /* We know we're at a defined time so if we're equal then there's
         * nothing more to advance, so we're done */
        if (FP::defined(currentStart) && currentStart == time)
            return result;

        double nextEnd;
        if (transitionPoints.empty()) {
            nextEnd = FP::undefined();
        } else {
            nextEnd = transitionPoints.back();

            /* The advance makes sure there's not a possible segment after
             * this one */
            Q_ASSERT(transitionPoints.size() < 2 ||
                             Range::compareStart(transitionPoints[transitionPoints.size() - 2],
                                                 time) > 0);
        }

        /* Add the partial segment, we don't adjust the state because
         * we need completion to do that */
        result.push_back(convert(currentStart, nextEnd));
        state = Emitted;

        return result;
    }

    /**
     * Add a value to the stream, generating any output the value caused.
     * 
     * @param value     the value to add
     * @return          the list of completed segments
     */
    template<typename InputType>
    ResultType add(InputType &&value)
    {
        double start = value.getStart();

        Q_ASSERT(Range::compareStart(streamPosition, start) <= 0);
        streamPosition = start;

        /* Zero length values can't cause segments, so just ignore them */
        double end = value.getEnd();
        if (FP::defined(start) && FP::defined(end) && start >= end)
            return ResultType();

        /* If the stream is currently empty then just initialize and done */
        if (initializeIfEmpty(std::forward<InputType>(value))) {
            insertBreak(end);
            return ResultType();
        }

        /* Insert the break points the value causes before the advance */
        insertBreak(start);
        insertBreak(end);

        /* Advance anything the value completes */
        ResultType result = advanceCompleted(start);

        /* The advance may have emptied the steam, so initialize if
         * required */
        if (initializeIfEmpty(std::forward<InputType>(value)))
            return result;

        /* Finally add it to the active set */
        static_cast<Implementation *>(this)->addActive(std::forward<InputType>(value));
        return result;
    }

    /**
     * Add a value to the stream without causing fragmenting of values.  This results
     * in the value being added to the implementation storage, but not affecting
     * the points that values are generated at.
     *
     * @param value     the value to add
     * @return          the list of completed segments
     */
    template<typename InputType>
    ResultType inject(InputType &&value)
    {
        double start = value.getStart();

        Q_ASSERT(Range::compareStart(streamPosition, start) <= 0);
        streamPosition = start;

        /* Zero length values can't cause segments, so just ignore them */
        double end = value.getEnd();
        if (FP::defined(start) && FP::defined(end) && start >= end)
            return ResultType();

        /* Advance anything the value completes */
        ResultType result = advanceCompleted(start);

        /* Explicitly do this here, since we don't add a transition point, we might
         * end up buffering forever if all we get is injected values */
        if (FP::defined(start))
            purge(start);

        /* Finally add it to the active set */
        static_cast<Implementation *>(this)->addInjected(std::forward<InputType>(value));
        return result;
    }

    /**
     * Finish the stream.
     * 
     * @return          the list of completed segments
     */
    ResultType finish()
    {
        ResultType result;

        switch (state) {
        case Empty:
            break;
        case Emitted:
            /* No transition means we've already emitted the partial
             * final segment, so we're done */
            if (transitionPoints.empty())
                break;
            Q_ASSERT(FP::defined(transitionPoints.back()));

            /* Advance the time to the next transition */
            currentStart = transitionPoints.back();
            transitionPoints.pop_back();
            /* Purge the partial segment, and if there's nothing else
             * we're done */
            if (!purge(currentStart))
                break;

            /* Fall through */
        case Active:
            for (;;) {
                /* Emit the final undefined-end segment */
                if (transitionPoints.empty()) {
                    result.push_back(convert(currentStart, FP::undefined()));
                    break;
                }

                Q_ASSERT(!transitionPoints.empty());
                Q_ASSERT(FP::defined(transitionPoints.back()));

                /* Add the segment */
                double nextEnd = transitionPoints.back();
                result.push_back(convert(currentStart, nextEnd));
                currentStart = nextEnd;

                /* If the state empties, then we're done */
                if (!purge(currentStart))
                    break;

                /* And advance the break */
                transitionPoints.pop_back();
            }
            break;
        }

        reset();
        return result;
    }

    /**
     * Reset the stream discarding all current state.
     */
    void reset()
    {
        state = Empty;
        streamPosition = FP::undefined();
        currentStart = FP::undefined();
        transitionPoints.clear();
        purgeAll();
    }

    /**
     * Insert a break into the steam.  This will cause a segment to be 
     * emitted on either side of the break even if no data actually
     * causes a transition there.
     * 
     * @param time      the break time to add
     */
    void insertBreak(double time)
    {
        if (!FP::defined(time))
            return;

        /* Just ignore breaks in the past */
        if (FP::defined(streamPosition) && streamPosition > time)
            return;

        /* If this is a break at the current start, then no action is
         * required */
        if (FP::defined(currentStart) && time == currentStart)
            return;
        /* The stream position should always be ahead of or equal to the
         * start time */
        Q_ASSERT(!FP::defined(currentStart) || time > currentStart);

        /* No transitions, so this is automatically the only one */
        if (transitionPoints.empty()) {
            transitionPoints.push_back(time);
            return;
        }

        /* Handle inserts before all known transitions */
        double earliestTime = transitionPoints.back();
        Q_ASSERT(FP::defined(earliestTime));
        if (time == earliestTime) {
            return;
        } else if (time < earliestTime) {
            transitionPoints.push_back(time);
            return;
        }

        /* Handle inserts for times ahead of all known transitions */
        double latestTime = transitionPoints.front();
        Q_ASSERT(FP::defined(latestTime));
        if (time == latestTime) {
            return;
        } else if (time > latestTime) {
            size_t n = transitionPoints.size();
            transitionPoints.resize(n + 1);
            memmove(&transitionPoints[1], &transitionPoints[0], n * sizeof(double));
            transitionPoints[0] = time;
            return;
        }

        /* No we know we're inside the range of times covered by the
         * existing transitions, so find the location */
        Q_ASSERT(time < latestTime && time > earliestTime);

        std::vector<double>::iterator pos =
                std::lower_bound(transitionPoints.begin(), transitionPoints.end(), time,
                                 transitionCompare);
        Q_ASSERT(pos != transitionPoints.end());
        if (*pos == time)
            return;

        size_t n = transitionPoints.size();
        size_t i = (pos - transitionPoints.begin());
        transitionPoints.resize(n + 1);
        memmove(&transitionPoints[i + 1], &transitionPoints[i], (n - i) * sizeof(double));
        transitionPoints[i] = time;
    }

    /** @see insertBreak(double) */
    template<typename Iterator>
    void insertBreaks(Iterator begin, Iterator end)
    {
        for (; begin != end; ++begin) {
            insertBreak(*begin);
        }
    }

    /** @see insertBreak(double) */
    template<typename Container>
    void insertBreaks(const Container &container)
    { insertBreaks(container.begin(), container.end()); }

    /**
     * Get the intermediate output.  This is the output that would be
     * the next (partial) if the time was advanced to just past its
     * start.
     * 
     * @param valid     if not NULL then set to true if the output is valid
     * @return          the intermediate next
     */
    OutputType intermediate(bool *valid = NULL) const
    {
        double effectiveStart = 0;
        switch (state) {
        case Empty:
            if (valid != NULL) *valid = false;
            return OutputType();
        case Active:
            effectiveStart = currentStart;
            break;
        case Emitted:
            effectiveStart = streamPosition;
            break;
        }

        /* Get the end point */
        double nextEnd;
        if (transitionPoints.empty()) {
            nextEnd = FP::undefined();
        } else {
            nextEnd = transitionPoints.back();
        }

        /* Advances whenever we change this should ensure the next end
         * is always ahead of the stream position */
        Q_ASSERT(Range::compareStartEnd(effectiveStart, nextEnd) < 0);

        if (valid != NULL) *valid = true;
        return convert(effectiveStart, nextEnd);
    }

    /**
     * Overlay the value into the stream discarding any output it would
     * produce.  This means the stream is advanced to the value start at 
     * least and anything that it was past is discarded.  If the value is
     * before the current time then the output it would have made is discarded
     * but overlapping state is integrated into the stream.
     * 
     * @param value the value to add
     */
    template<typename InputType>
    void overlaySingle(InputType &&value)
    {
        double start = value.getStart();
        double end = value.getEnd();

        /* Zero length values can't cause segments, so just ignore them */
        if (FP::defined(start) && FP::defined(end) && start >= end)
            return;

        /* If this value is after the current start, then just do a simplified
         * version of the advance (since we don't need to emit segments) */
        int cr = Range::compareStart(start, currentStart);
        if (cr >= 0) {
            if (Range::compareStart(start, streamPosition) > 0)
                streamPosition = start;

            /* Purge everything this bulk advance has made obsolete */
            if (!purge(currentStart)) {
                /* Emptied everything, so re-intiailize as required */
                state = Empty;
                if (initializeIfEmpty(std::forward<InputType>(value))) {
                    insertBreak(end);
                    return;
                }
            } else {
                /* If this was a strictly forward move then return to
                 * active state, otherwise leave it where it was (which
                 * may have been already emitted) */
                if (cr != 0)
                    state = Active;
            }

            /* Set the new start */
            currentStart = start;
            purgeCompletedTransition(currentStart);

            /* Now we know the value goes into the currently active one,
             * so just add its end break */
            insertBreak(end);
            static_cast<Implementation *>(this)->addActive(std::forward<InputType>(value));
            return;
        }

        /* The value starts before the current start, so if it also ends
         * before it then it has no possible effect */
        if (Range::compareStartEnd(currentStart, end) >= 0)
            return;

        /* We know it has an effect on the current segment now, so just
         * insert its end break and done */

        insertBreak(end);
        static_cast<Implementation *>(this)->addActive(std::forward<InputType>(value));
    }

    /**
     * Overlay the other stream into this one.  This causes the state of
     * the other stream to be incorporated into this one.  This stream is
     * advanced to the other one's time if it is ahead, if it is behind
     * then any input behind is discarded.
     * 
     * @param value the stream to overlay
     */
    template<typename StreamType>
    void overlayStream(const StreamType &value)
    {
        /* If we're before the other one then advance to its state first */
        if (Range::compareStart(currentStart, value.currentStart) <= 0) {
            if (Range::compareStart(streamPosition, value.currentStart) < 0)
                streamPosition = value.currentStart;

            /* Advance to the new start */
            currentStart = value.currentStart;
            purgeCompletedTransition(currentStart);
            if (!purge(currentStart))
                state = Empty;

            switch (state) {
            case Empty:
                state = value.state;
                break;
            case Active:
            case Emitted:
                break;
            }

            mergeTransition(value.transitionPoints);

            /* The other should be past its current start, so the merged end
             * should be after our new start */
            Q_ASSERT(state == Empty ||
                             transitionPoints.empty() ||
                             Range::compareStartEnd(currentStart, transitionPoints.back()) < 0);

            static_cast<Implementation *>(this)->mergeAll(value);
            return;
        }

        /* We're after the other, so we need to merge its values then
         * re-advance to where we where before */

        static_cast<Implementation *>(this)->mergeAll(value);
        mergeTransition(value.transitionPoints);

        purgeCompletedTransition(currentStart);

        if (purge(currentStart)) {
            /* If there are still values then act like we've emitted them
             * if they where entirely from the other one, if they weren't
             * then we can continue being active at the new start. */
            switch (state) {
            case Empty:
            case Emitted:
                state = Emitted;
                break;
            case Active:
                break;
            }
        } else {
            state = Empty;
        }

        /* The other should be past its current start, so the merged end
         * should be after our old start */
        Q_ASSERT(state == Empty ||
                         transitionPoints.empty() ||
                         Range::compareStartEnd(currentStart, transitionPoints.back()) < 0);
    }

    /**
     * Initialize the stream from a list of inputs.  This effectively overlays
     * all inputs into the segmenter.
     * 
     * @param begin     the begin iterator
     * @param end       the end iterator
     */
    template<typename Iterator>
    void initializeFrom(Iterator begin, Iterator end)
    {
        for (; begin != end; ++begin) {
            overlaySingle(*begin);
        }
    }

    /** @see initializeFrom(Iterator, Iterator) */
    template<typename Container>
    void initializeFrom(const Container &container)
    { initializeFrom(container.begin(), container.end()); }
};

template<typename OutputType, class Implementation, typename ResultType>
QDataStream &operator<<(QDataStream &stream,
                        const SegmentStream<OutputType, Implementation, ResultType> &value)
{
    value.serialize(stream);
    return stream;
}

template<typename OutputType, class Implementation, typename ResultType>
QDataStream &operator>>(QDataStream &stream,
                        SegmentStream<OutputType, Implementation, ResultType> &value)
{
    value.deserialize(stream);
    return stream;
}

template<typename OutputType, class Implementation, typename ResultType>
QDebug operator<<(QDebug stream, const SegmentStream<OutputType, Implementation, ResultType> &value)
{
    QDebugStateSaver saver(stream);
    stream.nospace() << "SegmentStream(current=" << Logging::time(value.getStreamTime()) << ",next="
                     << Logging::time(value.getNextTransition()) << ')';
    return stream;
}


/**
 * Maintain and apply configuration segments to an incoming stream of values.
 * This assumes that the configuration stage is done once initially at the
 * start then the resulting segments are applied to incoming values in
 * some way.
 * <br>
 * The configuration segments must provide getStart() and getEnd() methods,
 * as well as any other ones required by overlay functions if used.
 */
template<class ConfigurationSegment>
class ConfigurationStream {
    std::deque<ConfigurationSegment> remaining;

public:
    ConfigurationStream() : remaining()
    { }

    ConfigurationStream(const ConfigurationStream<ConfigurationSegment> &) = default;

    ConfigurationStream &operator=(const ConfigurationStream<ConfigurationSegment> &) = default;

    ConfigurationStream(ConfigurationStream<ConfigurationSegment> &&) = default;

    ConfigurationStream &operator=(ConfigurationStream<ConfigurationSegment> &&) = default;

    /**
     * Create the stream.
     *
     * @tparam Iterator a forward iterator type
     * @param begin     the first segment to insert
     * @param end       the last segment to insert
     */
    template<typename Iterator>
    explicit ConfigurationStream(Iterator begin, Iterator end) : remaining()
    {
        std::copy(begin, end, Util::back_emplacer(remaining));
#ifndef NDEBUG
        if (!remaining.empty()) {
            Q_ASSERT(Range::compareStartEnd(remaining.front().getStart(),
                                            remaining.front().getEnd()) < 0);
            for (auto check = remaining.cbegin() + 1, endCheck = remaining.cend();
                    check != endCheck;
                    ++check) {
                Q_ASSERT(Range::compareStartEnd(check->getStart(), (check - 1)->getEnd()) >= 0);
                Q_ASSERT(Range::compareStartEnd(check->getStart(), check->getEnd()) < 0);
            }
        }
#endif
    }

    /**
     * Create the stream.
     *
     * @tparam ListType a forward iterable type
     * @param input     the list to copy
     */
    template<typename ListType>
    explicit ConfigurationStream(ListType input) : ConfigurationStream(input.begin(), input.end())
    { }

    /**
     * Overlay a single segment into the configuration.
     * <br>
     * The configuration and overlay types must provide the
     * methods for Range::overlayFragmenting(Container &, const AddType &).
     *
     * @tparam OverlayType  the type to add
     * @param add           the value to add
     */
    template<typename OverlayType>
    void overlay(const OverlayType &add)
    {
        Range::overlayFragmenting(remaining, add);
    }

    /**
     * Overlay a list of segments into the configuration.
     * <br>
     * The configuration and overlay types must provide the
     * methods for Range::overlayFragmenting(Container &, const AddType &).
     *
     * @tparam Iterator a forward iterable type
     * @param begin     the first value to add
     * @param end       the end of values to add
     */
    template<typename Iterator>
    void overlay(Iterator begin, Iterator end)
    {
        for (; begin != end; ++begin) {
            Range::overlayFragmenting(remaining, *begin);
        }
    }

    /**
     * Add a segement to the configuration.
     *
     * @param add   the segment to add
     */
    void append(const ConfigurationSegment &add)
    {
        Q_ASSERT(Range::compareStartEnd(add.getStart(), add.getEnd()) <= 0);
        Q_ASSERT(remaining.empty() ||
                         Range::compareStartEnd(add.getStart(), remaining.back().getEnd()) >= 0);
        remaining.emplace_back(add);
    }

    /** @see append(const ConfigurationSegment &) */
    void append(ConfigurationSegment &&add)
    {
        Q_ASSERT(Range::compareStartEnd(add.getStart(), add.getEnd()) <= 0);
        Q_ASSERT(remaining.empty() ||
                         Range::compareStartEnd(add.getStart(), remaining.back().getEnd()) >= 0);
        remaining.emplace_back(std::move(add));
    }

    /**
     * Serialize the configuration.
     *
     * @param stream    the target stream
     */
    void serialize(QDataStream &stream) const
    {
        stream << remaining;
    }

    /**
     * Serialize the configuration with the specified segment serializer.
     *
     * @tparam Functor  the serialization functor
     * @param stream    the target stream
     */
    template<typename Functor>
    void serialize(QDataStream &stream, Functor serializer) const
    {
        Serialize::container(stream, remaining, serializer);
    }

    /**
     * Deserialize the configuration.
     *
     * @param stream    the target stream
     */
    void deserialize(QDataStream &stream)
    {
        stream >> remaining;
    }

    /**
     * Deserialize the configuration with the specified segment serializer.
     *
     * @param stream    the target stream
     */
    template<typename Functor>
    void deserialize(QDataStream &stream, Functor serializer)
    {
        Deserialize::container(stream, remaining, serializer);
    }

    /**
     * Advance the configuration stream to the given time.
     *
     * @param time  the time to advance until
     * @return      true if the configuration advanced
     */
    bool advance(double time)
    {
        if (remaining.empty())
            return false;
        if (Range::compareStartEnd(time, remaining.front().getEnd()) < 0)
            return false;
        for (;;) {
            remaining.pop_front();
            if (remaining.empty())
                break;
            if (Range::compareStartEnd(time, remaining.front().getEnd()) < 0)
                break;
        }
        return true;
    }

    /**
     * Test if the stream is empty.
     *
     * @return  true if there are no more remaining configuration segments
     */
    inline bool empty() const
    { return remaining.empty(); }

    /**
     * Get the number of segmetns in the stream.
     *
     * @return  true if there are no more remaining configuration segments
     */
    inline std::size_t size() const
    { return remaining.size(); }

    /**
     * Get the current first configuration segment.
     *
     * @return  the current first segment
     */
    inline ConfigurationSegment &front()
    {
        Q_ASSERT(!remaining.empty());
        return remaining.front();
    }

    /** @see front() */
    inline const ConfigurationSegment &front() const
    {
        Q_ASSERT(!remaining.empty());
        return remaining.front();
    }

    /**
     * Get the segment active at a given time
     *
     * @param time  the time to find
     * @return      the active segment, if any
     */
    ConfigurationSegment *active(double time)
    {
        Q_ASSERT(FP::defined(time));

        if (!remaining.empty()) {
            auto &check = remaining.front();
            if (!FP::defined(check.getEnd()) || time < check.getEnd()) {
                if (!FP::defined(check.getStart()) || time >= check.getStart())
                    return &check;
            }
        } else {
            return nullptr;
        }

        auto check = Range::findIntersecting(remaining.begin(), remaining.end(), time);
        if (check == remaining.end())
            return nullptr;
        return &(*check);
    }

    /** @see active(double) */
    const ConfigurationSegment *active(double time) const
    {
        Q_ASSERT(FP::defined(time));

        if (!remaining.empty()) {
            auto &check = remaining.front();
            if (!FP::defined(check.getEnd()) || time < check.getEnd()) {
                if (!FP::defined(check.getStart()) || time >= check.getStart())
                    return &check;
            }
        } else {
            return nullptr;
        }

        auto check = Range::findIntersecting(remaining.cbegin(), remaining.cend(), time);
        if (check == remaining.end())
            return nullptr;
        return &(*check);
    }

    /**
     * Apply all active segments in a range with a functor.
     *
     * @tparam Functor  the functor type
     * @param start     the start of the range
     * @param end       the end of the range
     * @param f         the functor
     */
    template<typename Functor>
    void applyActive(double start, double end, Functor f)
    {
        auto range =
                Range::heuristicFindAllIntersecting(remaining.begin(), remaining.end(), start, end);
        for (auto add = range.first; add != range.second; ++add) {
            Q_ASSERT(Range::intersects(start, end, add->getStart(), add->getEnd()));
            f(*add);
        }
    }

    /** @see applyActive(double, double, Functor) */
    template<typename Functor>
    void applyActive(double start, double end, Functor f) const
    {
        auto range =
                Range::heuristicFindAllIntersecting(remaining.begin(), remaining.end(), start, end);
        for (auto add = range.first; add != range.second; ++add) {
            Q_ASSERT(Range::intersects(start, end, add->getStart(), add->getEnd()));
            f(*add);
        }
    }

    /**
     * Apply a functor to the specified range until it returns true.
     *
     * @tparam Functor  the functor type
     * @param start     the start of the range
     * @param end       the end of the range
     * @param f         the functor
     * @return          true if any call returned true
     */
    template<typename Functor>
    bool applyUntil(double start, double end, Functor f)
    {
        auto range =
                Range::heuristicFindAllIntersecting(remaining.begin(), remaining.end(), start, end);
        for (auto add = range.first; add != range.second; ++add) {
            Q_ASSERT(Range::intersects(start, end, add->getStart(), add->getEnd()));
            if (f(*add))
                return true;
        }
        return false;
    }

    /** @see applyActive(double, double, Functor) */
    template<typename Functor>
    bool applyUntil(double start, double end, Functor f) const
    {
        auto range =
                Range::heuristicFindAllIntersecting(remaining.begin(), remaining.end(), start, end);
        for (auto add = range.first; add != range.second; ++add) {
            Q_ASSERT(Range::intersects(start, end, add->getStart(), add->getEnd()));
            if (f(*add))
                return true;
        }
        return false;
    }

    /**
     * Apply any active segment that intersects a range with a functor
     *
     * @tparam Functor  the functor type
     * @param start     the start of the range
     * @param end       the end of the range
     * @param f         the functor
     * @return          true if there was a match
     */
    template<typename Functor>
    bool applySingle(double start, double end, Functor f)
    {
        if (!remaining.empty()) {
            auto &check = remaining.front();
            if (Range::intersects(start, end, check.getStart(), check.getEnd())) {
                f(check);
                return true;
            }
        } else {
            return false;
        }

        auto hit = Range::findIntersecting(remaining.begin(), remaining.end(), start, end);
        if (hit == remaining.end())
            return false;
        Q_ASSERT(Range::intersects(start, end, hit->getStart(), hit->getEnd()));
        f(*hit);
        return true;
    }

    /** @see applySingle(double, double, Functor) */
    template<typename Functor>
    bool applySingle(double start, double end, Functor f) const
    {
        if (!remaining.empty()) {
            auto &check = remaining.front();
            if (Range::intersects(start, end, check.getStart(), check.getEnd())) {
                f(check);
                return true;
            }
        } else {
            return false;
        }

        auto hit = Range::findIntersecting(remaining.begin(), remaining.end(), start, end);
        if (hit == remaining.end())
            return false;
        Q_ASSERT(Range::intersects(start, end, hit->getStart(), hit->getEnd()));
        f(*hit);
        return true;
    }

    /**
     * Apply any active segment that intersects a point with a functor.
     *
     * @tparam Functor  the functor type
     * @param time      the point to check
     * @param f         the functor
     * @return          true if there was a match
     */
    template<typename Functor>
    bool applySingle(double time, Functor f)
    {
        auto hit = active(time);
        if (!hit)
            return false;
        return f(*hit);
    }

    /** @see applySingle(double, Functor) */
    template<typename Functor>
    bool applySingle(double time, Functor f) const
    {
        auto hit = active(time);
        if (!hit)
            return false;
        return f(*hit);
    }

    /**
     * Apply a functor to all current configuration segments.
     *
     * @tparam Functor  the functor type
     * @param f         the functor to apply
     */
    template<typename Functor>
    void applyAll(Functor f)
    {
        for (auto &a : remaining) {
            f(a);
        }
    }

    /** @see applyAll(Functor) */
    template<typename Functor>
    void applyAll(Functor f) const
    {
        for (const auto &a : remaining) {
            f(a);
        }
    }

    /**
     * Apply a functor to all current configuration segments, returning
     * true when it does.
     *
     * @tparam Functor  the functor type
     * @param f         the functor to apply
     * @return          true if the functor returned true on any segment
     */
    template<typename Functor>
    bool applyUntil(Functor f)
    {
        for (auto &a : remaining) {
            if (f(a))
                return true;
        }
        return false;
    }

    /** @see applyUntil(Functor) */
    template<typename Functor>
    bool applyUntil(Functor f) const
    {
        for (auto &a : remaining) {
            if (f(a))
                return true;
        }
        return false;
    }

};

template<typename ConfigurationSegment>
QDataStream &operator<<(QDataStream &stream, const ConfigurationStream<ConfigurationSegment> &value)
{
    value.serialize(stream);
    return stream;
}

template<typename ConfigurationSegment>
QDataStream &operator>>(QDataStream &stream, ConfigurationStream<ConfigurationSegment> &value)
{
    value.deserialize(stream);
    return stream;
}

}

#endif
