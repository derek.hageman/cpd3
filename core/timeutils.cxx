/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#if defined(TIME_POSIX)

# include <sys/time.h>

#elif defined(TIME_WINDOWS)
# include <time.h>
# include <windows.h>
#elif CPD3_CXX >= 201100L
# include <chrono>
# include <time.h>
#endif

#include <cmath>
#include <QDate>
#include <QRegExp>

#include "timeutils.hxx"
#include "number.hxx"
#include "qtcompat.hxx"

namespace CPD3 {

/** @file  core/timeutils.hxx
 * Contains routines for working with Unix epoch time.
 */

/**
 * Get the current time.  Returns the number of seconds since 
 * 1970-01-01T00:00:00Z.
 * 
 * @return  the Unix epoch time
 */
double Time::time()
{
#if defined(TIME_POSIX)
    struct timeval t;
    ::gettimeofday(&t, NULL);
    return (double) t.tv_sec + (double) t.tv_usec / 1E6;
#elif defined(TIME_WINDOWS)
    FILETIME ft;
    quint64 ct;
    ::GetSystemTimeAsFileTime(&ft);
    ct = ft.dwHighDateTime;
    ct <<= 32;
    ct |= ft.dwLowDateTime;
    /* Offset from the two epochs (Windows is 1601-01-01) */
    ct -= Q_INT64_C(134774) * Q_INT64_C(86400) * Q_INT64_C(10000000);
    return (double)ct / 1E7;    /* It's in 100 nsecs */
#elif CPD3_CXX >= 201100L
    auto epoch = std::chrono::system_clock::from_time_t(0);
    auto now = std::chrono::system_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(now - epoch).
        count();
    return (double)elapsed / 1E6;
#else
    return (double)QDateTime::currentMSecsSinceEpoch() / 1E3;
#endif
}

/**
 * Convert an epoch time to a QDateTime object.
 * 
 * @param time      the time (seconds since 1970-01-01T00:00:00Z)
 * @param includeFractional if set then include fractional (milliseconds) in the result
 * @return          a QDateTime object for the input time
 */
QDateTime Time::toDateTime(double time, bool includeFractional)
{
    Q_ASSERT(FP::defined(time));

    if (includeFractional) {
        return QDateTime::fromMSecsSinceEpoch(std::round(time * 1000.0)).toUTC();
    }
    return QDateTime::fromMSecsSinceEpoch(static_cast<qint64>(std::floor(time)) * 1000).toUTC();
}

/**
 * Convert a QDateTime object into an epoch time.  The QDateTime must be in
 * UTC time.
 * 
 * @param time      the input time
 * @param includeFractional if set then include fractional (milliseconds) in the result
 * @return          the converted epoch time (seconds since 1970-01-01T00:00:00Z)
 */
double Time::fromDateTime(const QDateTime &time, bool includeFractional)
{
    Q_ASSERT(time.toUTC() == time);

    if (includeFractional)
        return time.toMSecsSinceEpoch() / 1000.0;
    return std::floor(time.toMSecsSinceEpoch() / 1000.0);
}

/**
 * Convert a calendar date to a day of week.  Zero is Sunday.
 * 
 * @param year      input year
 * @param month     input month (Jan = 1)
 * @param day       input day of month
 */
static int getDOW(int year, int month, int day)
{
    quint64 jx;
    int ix;

    ix = year + (month - 14) / 12;
    jx = (13 * ((quint64) month + 10 - ((quint64) month + 10) / 13 * 12) - 1) / 5 +
            (quint64) day +
            77 +
            (5 * ((quint64) ix - ((quint64) ix / 100) * 100)) / 4 +
            (quint64) ix / 400 -
            ((quint64) ix / 100) * 2;
    return (int) (jx % 7);
}

/**
 * Get the Julian day of a calendar date.
 *
 * @param year      input year
 * @param month     input month (Jan = 1)
 * @param day       input day of month
 */
static int julianDay(int year, int month, int day)
{
    if (month > 2) {
        month -= 3;
    } else {
        year--;
        month += 9;
    }
    int c = year / 100;
    int ya = year - 100 * c;
    return ((146097 * c) / 4 + (1461 * ya) / 4 + (153 * month + 2) / 5 + day + 1721119);
}

/**
 * Get the Julian day of a time.
 *
 * @param time  the input time
 * @return      the Julian day
 */
int Time::julianDay(double time)
{
    QDateTime dt = toDateTime(time, false);
    return CPD3::julianDay(dt.date().year(), dt.date().month(), dt.date().day());
}


/**
 * Convert a year and (fractional) DOY to Unix epoch time.
 * 
 * @param year          integer year
 * @param doy           DOY (Jan 1 = 1.0)
 * @param roundSecond   round to the nearest second
 * @return              seconds since the Unix epoch
 */
double Time::convertYearDOY(int year, double doy, bool roundSecond)
{
    double yearStart = fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));
    if (roundSecond) {
        return yearStart + ::floor(86400.0 * (doy - 1.0 + 0.000005));
    } else {
        return yearStart + 86400.0 * (doy - 1.0);
    }
}


#define QTR_1_DOY 91
#define QTR_2_DOY 182
#define QTR_3_DOY 274

/**
 * Returns the start time of a quarter in Unix epoch time.
 * 
 * @param year      the year
 * @param qtr       the quarter, 1-4
 * @return          Unix epoch time
 */
double Time::quarterStart(int year, int qtr)
{
    double yearStart = fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));
    if (qtr == 1)
        return yearStart;
    else if (qtr == 2)
        return yearStart + (QTR_1_DOY - 1) * 86400.0;
    else if (qtr == 3)
        return yearStart + (QTR_2_DOY - 1) * 86400.0;
    else
        return yearStart + (QTR_3_DOY - 1) * 86400.0;
}

/**
 * Returns the end time of a quarter in Unix epoch time.
 * 
 * @param year      the year
 * @param qtr       the quarter, 1-4
 * @return          Unix epoch time
 */
double Time::quarterEnd(int year, int qtr)
{
    if (qtr == 1)
        return fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC)) +
                (QTR_1_DOY - 1) * 86400.0;
    else if (qtr == 2)
        return fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC)) +
                (QTR_2_DOY - 1) * 86400.0;
    else if (qtr == 3)
        return fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC)) +
                (QTR_3_DOY - 1) * 86400.0;
    else
        return fromDateTime(QDateTime(QDate(year + 1, 1, 1), QTime(0, 0, 0), Qt::UTC));
}

/**
 * Get the year and quarter number that contain a given time.  Either can
 * be NULL and it will not be assigned.
 * 
 * @param time      the Unix epoch time
 * @param year      the year of the time
 * @param qtr       the quarter containing the time, 1-4
 */
void Time::containingQuarter(double time, int *year, int *qtr)
{
    QDate date(toDateTime(time).date());
    int doy = date.dayOfYear();
    if (year != NULL)
        *year = date.year();
    if (qtr == NULL)
        return;
    if (doy < QTR_1_DOY)
        *qtr = 1;
    else if (doy < QTR_2_DOY)
        *qtr = 2;
    else if (doy < QTR_3_DOY)
        *qtr = 3;
    else
        *qtr = 4;
}

static const int weekOffset[7] = {0, -1, -2, -3, 3, 2, 1};

/**
 * Get the Unix epoch time of the start of a given week.
 * 
 * @param year  integer year
 * @param week  number, starting from 1
 * @return      seconds since the Unix epoch
 */
double Time::weekStart(int year, int week)
{
    int dow = getDOW(year, 1, 1);
    dow = (dow + 6) % 7;
    int baseJD = CPD3::julianDay(year, 1, 1);
    int jd = baseJD + weekOffset[dow] + (week - 1) * 7;
    if (jd - baseJD < 0) year--;
    int count = 0;
    while ((jd - CPD3::julianDay(year + (++count), 1, 1)) > 0) { }
    year += count - 1;
    int day = jd - CPD3::julianDay(year, 1, 1) + 1;
    return ::floor(convertYearDOY(year, day));
}

/**
 * Return the ISO8601 Date-Time representation of the given time.
 * 
 * @param time              the time to output
 * @param includeFractional if true will include fractional seconds
 * @return the ISO8601 formatted time
 */
QString Time::toISO8601(double time, bool includeFractional)
{
    if (!FP::defined(time))
        return QString();
    if (includeFractional) {
        return toDateTime(time, true).toString("yyyy-MM-ddThh:mm:ss.zzzZ");
    } else {
        return toDateTime(time).toString("yyyy-MM-ddThh:mm:ssZ");
    }
}

/**
 * Return the ISO8601 Date-Time representation of the given time without
 * delimiters (":" and "-");
 * 
 * @param time              the time to output
 * @param includeFractional if true will include fractional seconds
 * @return the ISO8601 formatted time without dilimiters
 */
QString Time::toISO8601ND(double time, bool includeFractional)
{
    if (!FP::defined(time))
        return QString();
    if (includeFractional) {
        return toDateTime(time, true).toString("yyyyMMddThhmmss.zzzZ");
    } else {
        return toDateTime(time).toString("yyyyMMddThhmmssZ");
    }
}

static int decimalCountTranslator(double value, int decimals)
{
    if (decimals != 0) {
        if (value > 0.0) {
            if (value < 1.0)
                return 0;
        } else {
            if (value > -1.0)
                return 0;
        }
        return (int) value;
    }

    return (int) qRound(value);
}

/**
 * Returns a descriptive string of the given duration.  This will
 * automatically select appropriate units for the output.
 * 
 * @param seconds   the total duration in seconds
 * @param decimals  the number of digits after the decimal place to output
 */
QString Time::describeDuration(double seconds, int decimals)
{
    if (!FP::defined(seconds) || seconds == 0.0)
        return tr("0 seconds");
    if (seconds >= 86400.0 || seconds <= -86400.0) {
        return tr("%1 days", "", decimalCountTranslator(seconds / 86400.0, decimals)).arg(
                NumberFormat(1, decimals).apply(seconds / 86400.0, QChar()));
    } else if (seconds >= 3600.0 || seconds <= -3600.0) {
        return tr("%1 hours", "", decimalCountTranslator(seconds / 3600.0, decimals)).arg(
                NumberFormat(1, decimals).apply(seconds / 3600.0, QChar()));
    } else if (seconds >= 60.0 || seconds <= -60.0) {
        return tr("%1 minutes", "", decimalCountTranslator(seconds / 60.0, decimals)).arg(
                NumberFormat(1, decimals).apply(seconds / 60.0, QChar()));
    } else if (seconds >= 1.0 || seconds <= -1.0) {
        return tr("%1 seconds", "", decimalCountTranslator(seconds, decimals)).arg(
                NumberFormat(1, decimals).apply(seconds, QChar()));
    } else {
        return tr("%1 milliseconds", "", decimalCountTranslator(seconds * 1000.0, decimals)).arg(
                NumberFormat(1, decimals).apply(seconds * 1000.0, QChar()));
    }
}

/**
 * Returns the year and DOY representation of the given time.  if the rounding
 * is set to year then the it is automatically calculated based on if the time 
 * aligns with either Seconds, Hours, or Days.
 * 
 * @param time          the time to output
 * @param separator     the separator between the year and DOY
 * @param rounding      the rounding to use
 * @param padDOY        if non null, use to pad the DOY to 3 digits
 * @param formatMode    the formatting mode
 * @return the year and DOY string representing the time
 */
QString Time::toYearDOY(double time,
                        const QString &separator,
                        LogicalTimeUnit rounding,
                        const QChar &padDOY,
                        YearDOYMode formatMode)
{
    QDateTime ft(Time::toDateTime(time));
    int year = ft.date().year();
    double yearStart = fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));
    double doy = (time - yearStart) / 86400.0 + 1.0;
    long idoy = static_cast<long>(std::floor(doy));

    QString result;
    if (formatMode != DOYOnly) {
        result = QString::number(year);
        if (formatMode == DOYAsNeeded && rounding == Year && idoy == 1 && (doy - idoy) < 1E-5)
            return result;

        result.append(separator);
    }

    if (rounding == Year) {
        QTime t(ft.time());
        if (t.second() != 0 || t.minute() != 0)
            rounding = Second;
        else if (t.hour() != 0)
            rounding = Hour;
        else
            rounding = Day;
    }

    NumberFormat format;
    if (!padDOY.isNull()) {
        format.setIntegerDigits(3);
    } else {
        format.setIntegerDigits(1);
    }

    switch (rounding) {
    case Hour:
        format.setDecimalDigits(2);
        break;
    case Day:
        format.setDecimalDigits(0);
        break;
    default:
        format.setDecimalDigits(5);
        break;
    }

    result.append(format.apply(doy, padDOY));

    return result;
}

CachingYearDOYConverter::CachingYearDOYConverter()
{
    lastYear = -1;
    lastYearStart = -1;
}

CachingYearDOYConverter::CachingYearDOYConverter(const CachingYearDOYConverter &copy)
{
    this->lastYear = copy.lastYear;
    this->lastYearStart = copy.lastYearStart;
}

/**
 * Convert a year and (fractional) DOY to Unix epoch time.  This rounds to
 * the nearest second.
 * 
 * @param year          integer year
 * @param doy           DOY (Jan 1 = 1.0)
 * @return              seconds since the Unix epoch
 */
double CachingYearDOYConverter::convert(int year, double doy)
{
    if (year != lastYear) {
        lastYear = year;
        lastYearStart = Time::fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));
    }
    return lastYearStart + ::floor(86400.0 * (doy - 1.0 + 0.000005));
}

/**
 * Convert a year and (fractional) DOY to Unix epoch time
 * 
 * @param year          integer year
 * @param doy           DOY (Jan 1 = 1.0)
 * @return              seconds since the Unix epoch
 */
double CachingYearDOYConverter::convertUnrounded(int year, double doy)
{
    if (year != lastYear) {
        lastYear = year;
        lastYearStart = Time::fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));
    }
    return lastYearStart + 86400.0 * (doy - 1.0);
}

static double applyFixedInterval(double time, int count, bool roundUp, double interval, int outer)
{
    /* If it's a factor of the outer interval then we can align to the larger
     * unit and still make sense */
    if (count != 0 && ((int) (outer / count) * count) == outer)
        interval *= abs(count);

    if (roundUp)
        return ::ceil(time / interval) * interval;
    else
        return ::floor(time / interval) * interval;
}

static double offsetFixedInterval(double time,
                                  int count,
                                  bool roundUp,
                                  double interval,
                                  int outer,
                                  int multiplier)
{
    return applyFixedInterval(time, count, roundUp, interval, outer) +
            (count * multiplier) * interval;
}

/**
 * Offset a time by a given unit in either direction.  If not aligning then the
 * other components of the time are preserved, if aligning then the smaller
 * units are rounded up or down to the nearest given unit.  An undefined time
 * is unaltered.
 * <br>
 * NOTE: When the alignment would overflow a date field it is set to the largest
 * valid one in the alignment.  So one month up from Mar 31 would be on Apr 30.
 * 
 * @param time          the input time
 * @param unit          the unit of the offset
 * @param count         the number of units to offset
 * @param align         true if aligning
 * @param roundUp       if aligning, round up instead of down
 * @param multiplier    the multiplier to apply to the count
 * @return              the time modified by the offset and/or alignment
 */
double Time::logical(double time,
                     Time::LogicalTimeUnit unit,
                     int count,
                     bool align,
                     bool roundUp,
                     int multiplier)
{
    if (!FP::defined(time))
        return time;

    if (!align) {
        count *= multiplier;
        if (!count)
            return time;

        switch (unit) {
        case Millisecond:
            return time + count * 0.001;
        case Second:
            return time + count;
        case Minute:
            return time + count * 60.0;
        case Hour:
            return time + count * 3600.0;
        case Day:
            return time + count * 86400.0;
        case Week:
            return time + count * 604800.0;
        case Month: {
            double fRef = std::floor(time);
            time = fRef - time;
            QDateTime
                    ref = QDateTime::fromMSecsSinceEpoch(static_cast<qint64>(fRef) * 1000).toUTC();
            return fromDateTime(ref.addMonths(count)) + time;
        }
        case Quarter: {
            int year, qtr;
            containingQuarter(time, &year, &qtr);
            time -= quarterStart(year, qtr);
            qtr += count;
            if (count > 0)
                while (qtr > 4) {
                    qtr -= 4;
                    year++;
                }
            else
                while (qtr < 1) {
                    qtr += 4;
                    year--;
                }
            return quarterStart(year, qtr) + time;
        }
        case Year: {
            double fRef = std::floor(time);
            time = fRef - time;
            QDateTime
                    ref = QDateTime::fromMSecsSinceEpoch(static_cast<qint64>(fRef) * 1000).toUTC();
            return fromDateTime(ref.addYears(count)) + time;
        }
        }
        Q_ASSERT(false);
        return time;
    }

    switch (unit) {
    case Millisecond:
        return offsetFixedInterval(time, count, roundUp, 0.001, 1000, multiplier);
    case Second:
        return offsetFixedInterval(time, count, roundUp, 1.0, 60, multiplier);
    case Minute:
        return offsetFixedInterval(time, count, roundUp, 60.0, 60, multiplier);
    case Hour:
        return offsetFixedInterval(time, count, roundUp, 3600.0, 24, multiplier);
    case Day: {
        count *= multiplier;
        if (roundUp)
            return ::ceil(time / 86400.0) * 86400.0 + count * 86400.0;
        else
            return ::floor(time / 86400.0) * 86400.0 + count * 86400.0;
    }
    case Week: {
        count *= multiplier;
        int year;
        int week = toDateTime(time).date().weekNumber(&year);
        double start = weekStart(year, week);
        if (roundUp && start < time)
            return start + (count + 1) * 604800.0;
        return start + count * 604800.0;
    }
    case Month: {
        count *= multiplier;
        QDate date(toDateTime(time).date());
        int year = date.year();
        int month = date.month();
        QDate out = QDate(year, month, 1);
        double start = fromDateTime(QDateTime(out, QTime(0, 0, 0), Qt::UTC));
        if (roundUp && start < time)
            out = out.addMonths(1);
        if (count)
            out = out.addMonths(count);
        return fromDateTime(QDateTime(out, QTime(0, 0, 0), Qt::UTC));
    }
    case Quarter: {
        count *= multiplier;
        int year, qtr;
        containingQuarter(time, &year, &qtr);
        double start = quarterStart(year, qtr);
        qtr += count;
        if (count > 0)
            while (qtr > 4) {
                qtr -= 4;
                year++;
            }
        else
            while (qtr < 1) {
                qtr += 4;
                year--;
            }
        if (roundUp && start < time) {
            qtr++;
            if (qtr == 5) {
                year++;
                qtr = 1;
            }
        }
        return quarterStart(year, qtr);
    }
    case Year: {
        count *= multiplier;
        QDate date(toDateTime(time).date());
        int year = date.year();
        QDate out = QDate(year, 1, 1);
        double start = fromDateTime(QDateTime(out, QTime(0, 0, 0), Qt::UTC));
        if (roundUp && start < time)
            out = out.addYears(1);
        if (count)
            out = out.addYears(count);
        return fromDateTime(QDateTime(out, QTime(0, 0, 0), Qt::UTC));
    }
    }

    Q_ASSERT(false);
    return time;
}

/**
 * Apply floor-like operation to a time.  This rounds the time to the lower
 * bound of the unit-count (if applicable).  Undefined times are unaltered.
 * 
 * @param time      the base time to round
 * @param unit      the unit to round to
 * @param count     the number of units
 * @return          the aligned time
 */
double Time::floor(double time, LogicalTimeUnit unit, int count)
{
    if (!count)
        return time;
    if (!FP::defined(time))
        return time;

    switch (unit) {
    case Millisecond:
        return applyFixedInterval(time, count, false, 0.001, 1000);
    case Second:
        return applyFixedInterval(time, count, false, 1.0, 60);
    case Minute:
        return applyFixedInterval(time, count, false, 60.0, 60);
    case Hour:
        return applyFixedInterval(time, count, false, 3600.0, 24);
    case Day:
        return ::floor(time / 86400.0) * 86400.0;
    case Week: {
        int year;
        int week = toDateTime(time).date().weekNumber(&year);
        return weekStart(year, week);
    }
    case Month: {
        QDate date(toDateTime(time).date());
        int year = date.year();
        int month = date.month();
        return fromDateTime(QDateTime(QDate(year, month, 1), QTime(0, 0, 0), Qt::UTC));
    }
    case Quarter: {
        int year, qtr;
        containingQuarter(time, &year, &qtr);
        return quarterStart(year, qtr);
    }
    case Year: {
        QDate date;
        return fromDateTime(
                QDateTime(QDate(toDateTime(time).date().year(), 1, 1), QTime(0, 0, 0), Qt::UTC));
    }
    }

    Q_ASSERT(false);
    return time;
}

/**
 * Apply ceil-like operation to a time.  This rounds the time to the upper
 * bound of the unit-count (if applicable).  Undefined times are unaltered.
 * 
 * @param time      the base time to round
 * @param unit      the unit to round to
 * @param count     the number of units
 * @return          the aligned time
 */
double Time::ceil(double time, LogicalTimeUnit unit, int count)
{
    if (!count)
        return time;
    if (!FP::defined(time))
        return time;

    switch (unit) {
    case Millisecond:
        return applyFixedInterval(time, count, true, 0.001, 1000);
    case Second:
        return applyFixedInterval(time, count, true, 1.0, 60);
    case Minute:
        return applyFixedInterval(time, count, true, 60.0, 60);
    case Hour:
        return applyFixedInterval(time, count, true, 3600.0, 24);
    case Day:
        return ::ceil(time / 86400.0) * 86400.0;
    case Week: {
        int year;
        int week = toDateTime(time).date().weekNumber(&year);
        double start = weekStart(year, week);
        if (start < time)
            return start + 604800.0;
        return start;
    }
    case Month: {
        QDate date(toDateTime(time).date());
        int year = date.year();
        int month = date.month();
        QDate out = QDate(year, month, 1);
        double start = fromDateTime(QDateTime(out, QTime(0, 0, 0), Qt::UTC));
        if (start >= time)
            return start;
        return fromDateTime(QDateTime(out.addMonths(1), QTime(0, 0, 0), Qt::UTC));
    }
    case Quarter: {
        int year, qtr;
        containingQuarter(time, &year, &qtr);
        double start = quarterStart(year, qtr);
        if (start >= time)
            return start;
        qtr++;
        if (qtr == 5) {
            year++;
            qtr = 1;
        }
        return quarterStart(year, qtr);
    }
    case Year: {
        QDate date(toDateTime(time).date());
        int year = date.year();
        QDate out = QDate(year, 1, 1);
        double start = fromDateTime(QDateTime(out, QTime(0, 0, 0), Qt::UTC));
        if (start >= time)
            return start;
        return fromDateTime(QDateTime(out.addYears(1), QTime(0, 0, 0), Qt::UTC));
    }
    }

    Q_ASSERT(false);
    return time;
}

static double roundFixedInterval(double time, int count, double interval, int outer)
{
    /* If it's a factor of the outer interval then we can align to the larger
     * unit and still make sense */
    if (count != 0 && ((int) (outer / count) * count) == outer)
        interval *= abs(count);

    return ::round(time / interval) * interval;
}

static double roundToNearest(double time, double lower, double upper)
{
    if ((time - lower) < (upper - time))
        return lower;
    return upper;
}

/**
 * Apply round-like operation to a time.  This rounds the time to nearest
 * bound of the unit-count (if applicable).  Undefined times are unaltered.
 * 
 * @param time      the base time to round
 * @param unit      the unit to round to
 * @param count     the number of units
 * @return          the aligned time
 */
double Time::round(double time, LogicalTimeUnit unit, int count)
{
    if (!count)
        return time;
    if (!FP::defined(time))
        return time;

    switch (unit) {
    case Millisecond:
        return roundFixedInterval(time, count, 0.001, 1000);
    case Second:
        return roundFixedInterval(time, count, 1.0, 60);
    case Minute:
        return roundFixedInterval(time, count, 60.0, 60);
    case Hour:
        return roundFixedInterval(time, count, 3600.0, 24);
    case Day:
        return ::round(time / 86400.0) * 86400.0;
    case Week: {
        int year;
        int week = toDateTime(time).date().weekNumber(&year);
        double start = weekStart(year, week);
        return roundToNearest(time, start, start + 604800.0);
    }
    case Month: {
        QDate date(toDateTime(time).date());
        int year = date.year();
        int month = date.month();
        QDate out = QDate(year, month, 1);
        double start = fromDateTime(QDateTime(out, QTime(0, 0, 0), Qt::UTC));
        return roundToNearest(time, start,
                              fromDateTime(QDateTime(out.addMonths(1), QTime(0, 0, 0), Qt::UTC)));
    }
    case Quarter: {
        int year, qtr;
        containingQuarter(time, &year, &qtr);
        double start = quarterStart(year, qtr);
        qtr++;
        if (qtr == 5) {
            year++;
            qtr = 1;
        }
        return roundToNearest(time, start, quarterStart(year, qtr));
    }
    case Year: {
        QDate date(toDateTime(time).date());
        int year = date.year();
        QDate out = QDate(year, 1, 1);
        double start = fromDateTime(QDateTime(out, QTime(0, 0, 0), Qt::UTC));
        return roundToNearest(time, start,
                              fromDateTime(QDateTime(out.addYears(1), QTime(0, 0, 0), Qt::UTC)));
    }
    }

    Q_ASSERT(false);
    return time;
}

/* This only exists because it's unlikely that a translation will be
 * available for English for a long time, so fix it up.  Remove this
 * once a translation is actually made. */
static QString fixupOffsetPlural(const QString &input)
{
    QRegExp check("^(-?\\d+) ([^\\(]+)\\(s\\)");
    if (check.indexIn(input) != 0)
        return input;
    QString output(input.mid(check.cap(0).length()));
    int n = check.cap(1).toInt();
    if (n == 1) {
        output.prepend(QString("1 %2").arg(check.cap(2)));
    } else if (n == -1) {
        output.prepend(QString("-1 %2").arg(check.cap(2)));
    } else {
        output.prepend(QString("%1 %2s").arg(n).arg(check.cap(2)));
    }
    return output;
}

/**
 * Describe a time offset in a translated way.
 * 
 * @param unit      the unit of the offset
 * @param count     the number of units to offset
 * @param align     true if aligning
 * @see Time::logical( double, Time::LogicalTimeUnit, int, bool, bool, int )
 */
QString Time::describeOffset(Time::LogicalTimeUnit unit, int count, bool align)
{
    QString prefix;
    if (count < 0) {
        prefix = tr("-", "negative prefix");
        count = -count;
    }
    if (!align) {
        switch (unit) {
        case Millisecond:
            return fixupOffsetPlural(prefix + tr("%n millisecond(s)", "", count));
        case Second:
            return fixupOffsetPlural(prefix + tr("%n second(s)", "", count));
        case Minute:
            return fixupOffsetPlural(prefix + tr("%n minute(s)", "", count));
        case Hour:
            return fixupOffsetPlural(prefix + tr("%n hour(s)", "", count));
        case Day:
            return fixupOffsetPlural(prefix + tr("%n day(s)", "", count));
        case Week:
            return fixupOffsetPlural(prefix + tr("%n week(s)", "", count));
        case Month:
            return fixupOffsetPlural(prefix + tr("%n month(s)", "", count));
        case Quarter:
            return fixupOffsetPlural(prefix + tr("%n quarter(s)", "", count));
        case Year:
            return fixupOffsetPlural(prefix + tr("%n year(s)", "", count));
        }
    } else {
        switch (unit) {
        case Millisecond:
            return fixupOffsetPlural(prefix + tr("%n millisecond(s) aligned", "", count));
        case Second:
            return fixupOffsetPlural(prefix + tr("%n second(s) aligned", "", count));
        case Minute:
            return fixupOffsetPlural(prefix + tr("%n minute(s) aligned", "", count));
        case Hour:
            return fixupOffsetPlural(prefix + tr("%n hour(s) aligned", "", count));
        case Day:
            return fixupOffsetPlural(prefix + tr("%n day(s) aligned", "", count));
        case Week:
            return fixupOffsetPlural(prefix + tr("%n week(s) aligned", "", count));
        case Month:
            return fixupOffsetPlural(prefix + tr("%n month(s) aligned", "", count));
        case Quarter:
            return fixupOffsetPlural(prefix + tr("%n quarter(s) aligned", "", count));
        case Year:
            return fixupOffsetPlural(prefix + tr("%n year(s) aligned", "", count));
        }
    }
    return QString();
}

/**
 * Initialize both bounds to FP::undefined().
 */
Time::Bounds::Bounds() : start(FP::undefined()), end(FP::undefined())
{ }

/**
 * Initialize the bounds object.
 * 
 * @param st    the start time to set
 * @param ed    the end time to set
 */
Time::Bounds::Bounds(const double st, const double ed) : start(st), end(ed)
{ }

/**
 * Initialize the bounds object.
 * 
 * @param b     an unused parameter for range overlay
 * @param st    the start time to set
 * @param ed    the end time to set
 */
Time::Bounds::Bounds(const Bounds &b, const double st, const double ed) : start(st), end(ed)
{ Q_UNUSED(b); }

Time::Bounds::Bounds(const Time::Bounds &b)
{
    start = b.start;
    end = b.end;
}

Time::Bounds &Time::Bounds::operator=(const Time::Bounds &other)
{
    start = other.start;
    end = other.end;
    return *this;
}

bool Time::Bounds::operator==(const Time::Bounds &other) const
{
    return FP::equal(start, other.start) && FP::equal(end, other.end);
}

Time::Bounds::~Bounds()
{ }

QDebug operator<<(QDebug stream, const Time::Bounds &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace() << Logging::range(v.start, v.end);
    return stream;
}

QDebug operator<<(QDebug stream, const Time::LogicalTimeUnit &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    switch (v) {
    case Time::Millisecond:
        stream << "Millisecond";
        break;
    case Time::Second:
        stream << "Second";
        break;
    case Time::Minute:
        stream << "Minute";
        break;
    case Time::Hour:
        stream << "Hour";
        break;
    case Time::Day:
        stream << "Day";
        break;
    case Time::Week:
        stream << "Week";
        break;
    case Time::Month:
        stream << "Month";
        break;
    case Time::Quarter:
        stream << "Quarter";
        break;
    case Time::Year:
        stream << "Year";
        break;
    default:
        break;
    }
    return stream;
}

QDataStream &operator<<(QDataStream &stream, const Time::Bounds &value)
{
    stream << value.start << value.end;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, Time::Bounds &value)
{
    stream >> value.start >> value.end;
    return stream;
}

};
