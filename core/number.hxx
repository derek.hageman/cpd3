/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3CORENUMBER_H
#define CPD3CORENUMBER_H

#include "core/first.hxx"

#ifdef Q_CC_MSVC
#include <intrin.h>
#else

#include <math.h>

#endif

#if defined(Q_CC_GNU) && !defined(Q_OS_OSX)

#include <features.h>

#else
#ifndef __GNUC_PREREQ
#define __GNUC_PREREQ(maj,min)  0
#endif
#endif

#ifndef fpclassify
#include <cmath>
#endif

#include <limits.h>
#include <stdlib.h>
#include <limits>
#include <QtGlobal>
#include <QHash>
#include <QVector>
#include <QList>
#include <QString>
#include <QSet>
#include <QDataStream>
#include <QDebug>

#include "core/core.hxx"

namespace CPD3 {

/**
 * Simple routines for working with defined/undefined floating point numbers.
 */
class CPD3CORE_EXPORT FP {
public:
    /**
     * Return an undefined value.
     * @return  an undefined value
     */
    static inline double undefined()
    {
#ifdef Q_CC_MSVC
        static const double inf = DBL_MAX + DBL_MAX;
        static const double nan = inf - inf;
        return std::numeric_limits<double>::has_quiet_NaN ? std::numeric_limits<double>::quiet_NaN()
                                                          : nan;
#else
        return std::numeric_limits<double>::has_quiet_NaN ? std::numeric_limits<double>::quiet_NaN()
                                                          : (0.0 / 0.0);
#endif
    }

    /**
     * Set the given pointer to an undefined value.
     * @param d     a pointer to the number to set
     */
    static inline void undefine(double *const d)
    {
        *d = undefined();
    }

    /**
     * Set the given number to an undefined value.
     * @param d     the number to set
     */
    static inline void undefine(double &d)
    {
        d = undefined();
    }

    /**
     * Test if the given number is defined and finite.
     * @param d     the number to test
     * @return      true if the number is defined and finite
     */
    static inline bool defined(const double d)
    {
#if 0
        quint64 v;
        memcpy(&v, &d, 8);
        return (v & Q_UINT64_C(0x7FF0000000000000)) != 
            Q_UINT64_C(0x7FF0000000000000);
#endif
#ifdef Q_CC_MSVC
        return _finite(d);
#else
#ifdef isfinite
        return isfinite(d);
#else
        return ::std::isfinite(d);
#endif
#endif
    }

    /**
     * Test if two numbers are equal, respecting defined startus.
     * @param d1    the first number
     * @param d2    the second number
     * @return      true if the numbers are defined and the same, or are both undefined
     */
    static inline bool equal(double d1, double d2)
    {
        if (!defined(d1))
            return !defined(d2);
        if (!defined(d2))
            return false;
        return d1 == d2;
    }

    /**
     * Compare two (definitely defined, see Range::compareStart(double, double) and
     * Range::compareEnd(double, double) for undefined handling) numbers.
     * Returns -1, 0, or 1 if d1 is less than d2, they are equal or d1 is greater
     * than d2, respectively.
     * 
     * @param d1    the first number
     * @param d2    the second number
     * @return      the comparison result, -1, 0, or 1
     */
    static inline int compare(double d1, double d2)
    {
        Q_ASSERT(defined(d1));
        Q_ASSERT(defined(d2));
        if (d1 < d2) return -1;
        if (d1 == d2) return 0;
        return 1;
    }

    /**
     * Format a decimal number.  This returns "undefined" if the number is
     * undefined.  Does not honor translation settings, so this should not be
     * used for conventionally user visible data strings that can be undefined.
     * This strips trailing zeros.
     * 
     * @param d     the number to format
     * @param prec  the maximum number of digits after the decimal
     * @return      the formatted string
     */
    static QString decimalFormat(double d, int prec = 3);

    /**
     * Format a number in scientific (1.123E67) form.  This returns "undefined" 
     * if the number is undefined.  Does not honor translation settings, so this 
     * should not be used for conventionally user visible data strings that can 
     * be undefined.
     * 
     * @param d             the number to format
     * @param decimals      the number of digits after the decimal
     * @param exponents     the number of exponent digits
     * @param exponentSign  the string indicating the exponent has begun
     * @param plusSign      the string (or null for none) that positive values are shown with
     * @param plusExponent  the string (or null for none) that positive exponents are shown with
     */
    static QString scientificFormat(double d,
                                    int decimals = 5,
                                    int exponents = 2,
                                    const QString &exponentSign = QString(QLatin1Char('e')),
                                    const QString &plusSign = QString(),
                                    const QString &plusExponent = QString(QLatin1Char('+')));
};

/**
 * Simple routines for working with defined/undefined integers.
 */
class CPD3CORE_EXPORT INTEGER {
public:
    /**
     * Set the given pointer to an undefined value.
     * @param i     a pointer to the number to set
     */
    static inline void undefine(qint64 *const i)
    {
        *i = Q_INT64_C(-9223372036854775807);
    }

    /**
     * Set the given number to an undefined value.
     * @param i     the number to set
     */
    static inline void undefine(qint64 &i)
    {
        i = Q_INT64_C(-9223372036854775807);
    }

    /**
     * Return an undefined value.
     * @return  an undefined value
     */
    static inline qint64 undefined()
    {
        return Q_INT64_C(-9223372036854775807);
    }

    /**
     * Test if the given number is defined.
     * @param i     the number to test
     * @return      true if the number is defined
     */
    static inline bool defined(qint64 i)
    {
        return i != Q_INT64_C(-9223372036854775807);
    }

    /**
     * Test if two numbers are equal, respecting defined startus.
     * @param i1    the first number
     * @param i2    the second number
     * @return      true if the numbers are defined and the same, or are both undefined
     */
    static inline bool equal(qint64 i1, qint64 i2)
    {
        if (!defined(i1))
            return !defined(i2);
        if (!defined(i2))
            return false;
        return i1 == i2;
    }

    /**
     * Compare two (definitely defined) numbers. Returns -1, 0, or 1 if i1 is less 
     * than i2, they are equal or i1 is greater than i2, respectively.
     * 
     * @param i1    the first number
     * @param i2    the second number
     * @return      the comparison result, -1, 0, or 1
     */
    static inline int compare(qint64 i1, qint64 i2)
    {
        Q_ASSERT(defined(i1));
        Q_ASSERT(defined(i2));
        if (i1 < i2) return -1;
        if (i1 == i2) return 0;
        return 1;
    }

    /**
     * Grow an allocation to an increased size rounding up to reduce the
     * number of re-allocations used.
     */
    static size_t growAlloc(size_t minimumSize);

    /**
     * Count the number of leading zeros.
     * 
     * @param in    the integer
     * @return      the number of leading zeros
     */
    static inline quint32 clz32(quint32 in)
    {
        Q_ASSERT(in != 0);
#if defined(Q_CC_GNU)
        return __builtin_clz(in);
#elif defined(Q_CC_MSVC)
        unsigned long index;
        _BitScanReverse(&index, static_cast<unsigned long>(in));
        return 31 - index;
#else
        for (quint32 n=0; in; in <<= 1, n++) {
            if (in & 0x80000000)
                return n;
        }
        return 32;
#endif
    }

    /**
     * Count the number of leading zeros.
     * 
     * @param in    the integer
     * @return      the number of leading zeros
     */
    static inline quint64 clz(quint64 in)
    {
        Q_ASSERT(in != 0);
#if defined(Q_CC_GNU)
        return __builtin_clzll(in);
#elif defined(Q_CC_MSVC) && defined(_WIN64)
        unsigned long index;
        _BitScanReverse64(&index, static_cast<unsigned __int64>(in));
        return 63 - index;
#else
        for (quint64 n=0; in; in <<= 1, n++) {
            if (in & 0x8000000000000000)
                return n;
        }
        return 64;
#endif
    }

    /**
     * The floor of the base-2 logarithm.
     *
     * @param input     the input value
     * @return          the output floored logarithm value
     */
    static inline quint32 floorLog2_32(quint32 input)
    { return 31 - clz32(input); }

    /**
     * The floor of the base-2 logarithm.
     *
     * @param input     the input value
     * @return          the output floored logarithm value
     */
    static inline quint64 floorLog2(quint64 input)
    { return 63 - clz(input); }

    /**
     * Right circular rotate.
     * 
     * @param in    the input number
     * @param n     the number of bits
     * @return      the rotated number
     */
    template<typename Numeric>
    static inline Numeric rotr(Numeric in, Numeric n)
    {
        return (in >> n) | (in << (sizeof(Numeric) * 8 - n));
    }

    /**
     * Left circular rotate.
     * 
     * @param in    the input number
     * @param n     the number of bits
     * @return      the rotated number
     */
    template<typename Numeric>
    static inline Numeric rotl(Numeric in, Numeric n)
    {
        return (in << n) | (in >> (sizeof(Numeric) * 8 - n));
    }

    /**
     * Mix values for hashing
     * @param a     the first value
     * @param b     the second value
     * @return      the mixed value
     */
    static inline std::size_t mix(std::size_t a, std::size_t b)
    { return INTEGER::rotl<std::size_t>(a, 8) ^ b; }
};

/**
 * Provides routines to generate random numbers.
 */
class CPD3CORE_EXPORT Random {
    template<typename IntegerType>
    static inline IntegerType xorshift(IntegerType x, IntegerType a, IntegerType b, IntegerType c)
    {
        if (x == 0) x = 1;
        x ^= (x << a);
        x ^= (x >> b);
        x ^= (x << c);
        return x;
    }

public:
    static quint32 integer32();

    static quint64 integer();

    static double fp();

    static QByteArray data(int length);

    static QString string(int length = 32);

    static bool fill(void *ptr, int length);

    /**
     * Simple and fast xorshift PRNG.  This has a period of 2^16-1.
     * 
     * @param seed  the input seed
     * @return      the next number
     */
    static inline quint16 xorshift16(quint16 seed)
    { return xorshift<quint16>(seed, 4, 3, 7); }

    /**
     * Simple and fast xorshift PRNG.  This has a period of 2^32-1.
     * 
     * @param seed  the input seed
     * @return      the next number
     */
    static inline quint32 xorshift32(quint32 seed)
    { return xorshift<quint32>(seed, 13, 17, 5); }

    /**
     * Simple and fast xorshift PRNG.  This has a period of 2^64-1.
     * 
     * @param seed  the input seed
     * @return      the next number
     */
    static inline quint64 xorshift64(quint64 seed)
    { return xorshift<quint64>(seed, 13, 7, 17); }

    /**
     * Simple and fast xorshift PRNG.
     *
     * @param seed  the input seed
     * @return      the next number
     */
    template<typename IntegerType>
    static inline IntegerType xorshift(IntegerType seed)
    {
        if (sizeof(IntegerType) >= 8) {
            return (IntegerType) xorshift64((quint64) seed);
        } else if (sizeof(IntegerType) >= 4) {
            return (IntegerType) xorshift32((quint32) seed);
        } else {
            return (IntegerType) xorshift16((quint16) seed);
        }
    }
};


class Calibration;

CPD3CORE_EXPORT QDataStream &operator<<(QDataStream &stream, const Calibration &cal);

CPD3CORE_EXPORT QDataStream &operator>>(QDataStream &stream, Calibration &cal);

CPD3CORE_EXPORT QDebug operator<<(QDebug stream, const Calibration &cal);

/**
 * A calibration polynomial that can be applied to numbers.
 */
class CPD3CORE_EXPORT Calibration {
private:
    QVector<double> coefficients;

    enum Type {
        Invalid = 0, Identity, Constant, Offset, FirstOrder, SecondOrder, ThirdOrder, NthOrder
    };
    Type type;

    void setType();

    bool equals(const Calibration &other) const;

public:
    Calibration();

    Calibration(const Calibration &other);

    Calibration &operator=(const Calibration &other);

    Calibration(Calibration &&other);

    Calibration &operator=(Calibration &&other);

    Calibration(QList<double> setCoefficients);

    Calibration(QVector<double> setCoefficients);

    inline bool operator==(const Calibration &other) const
    { return equals(other); }

    inline bool operator!=(const Calibration &other) const
    { return !equals(other); }

    double apply(double input) const;

    enum RootChoice {
        /**
         * Use the root that is "best" in the range (by default all positive
         * numbers).
         */
                BestInRange, /**
         * Prefer the "positive" ((-b + sqrt(d)) / (2c)) root if the resulting
         * output value is itself in range, use the BestInRange logic.
         */
                PreferPositive, /**
         * Prefer the "negative" ((-b - sqrt(d)) / (2c)) root if the resulting
         * output value is itself in range, otherwise use the BestInRange logic.
         */
                PreferNegative, /**
         * Prefer the "x1" third order root if the resulting output value is 
         * itself in range, otherwise use the BestInRange logic.  Equivalent to 
         * AlwaysPositive for second order
         */
                PreferThirdX1, /**
         * Always use the "positive" ((-b + sqrt(d)) / (2c) or x2) root.
         */
                AlwaysPositive, /**
         * Always use "negative" ((-b - sqrt(d)) / (2c) or x3) root.
         */
                AlwaysNegative, /**
         * Always use the "x1" third order root.  Equivalent to AlwaysPositive
         * for second order
         */
                AlwaysThirdX1
    };

    double inverse
            (double output, RootChoice rootChoice, double min = 0.0, double max = FP::undefined())
            const;

    double inverse(double output, double min = 0.0, double max = FP::undefined()) const;

    void setDefaultRootChoice(RootChoice rootChoice);

    RootChoice getDefaultRootChoice() const;

    int size() const;

    double get(int n) const;

    void set(int n, double c);

    void append(double c);

    void removeLast();

    void clear();

    void toIdentity();

    bool isIdentity() const;

    bool isValid() const;

    friend CPD3CORE_EXPORT QDataStream &operator<<(QDataStream &stream, const Calibration &cal);

    friend CPD3CORE_EXPORT QDataStream &operator>>(QDataStream &stream, Calibration &cal);

    friend CPD3CORE_EXPORT QDebug operator<<(QDebug stream, const Calibration &v);

private:
    RootChoice defaultRootChoice;
};


class NumberFormat;

CPD3CORE_EXPORT QDataStream &operator<<(QDataStream &stream, const NumberFormat &fmt);

CPD3CORE_EXPORT QDataStream &operator>>(QDataStream &stream, NumberFormat &fmt);

CPD3CORE_EXPORT QDebug operator<<(QDebug stream, const NumberFormat &fmt);

/**
 * A formatter for numbers that can encode the format as a string.
 */
class CPD3CORE_EXPORT NumberFormat {
public:
    enum Mode {
        /**
         * Decimal numbers like "12.345".  The decimal point will be
         * omitted if there are no digits after it.  This uses the integer
         * and decimal widths to determine the format.
         */
                Decimal = 0,

        /**
         * Scientific notation formatted numbers like "1.234E1".  The decimal
         * point in the fractional part will be omitted if there are
         * no digits after it.  This uses the decimal and exponent widths
         * to determine the format.
         */
                Scientific,

        /**
         * Hexadecimal integers like "AB123".  This uses the integer width.
         */
                Hex,

        /**
         * Octal integers like "664".  This uses the integer width.
         */
                Octal
    };
private:
    Mode mode;
    int integerDigits;
    int decimalDigits;
    int exponentDigits;
    bool showPositiveSign;
    bool showPositiveExponent;
    QString exponentSeparator;
    bool uppercaseHex;
    QString basePrefix;

    static QString toSuperscript(const QString &input);

    static QString toSubscript(const QString &input);

public:
    NumberFormat();

    NumberFormat(const NumberFormat &other);

    NumberFormat &operator=(const NumberFormat &other);

    NumberFormat(NumberFormat &&other);

    NumberFormat &operator=(NumberFormat &&other);

    NumberFormat(int integerWidth, int decimalWidth);

    NumberFormat(int exponentFractional);

    NumberFormat(const QString &format);

    QString apply(double d, const QChar &leadingPad = QLatin1Char('0')) const;

    QString applyClipped(double d, const QChar &leadingPad = QLatin1Char('0')) const;

    QString apply(qint64 i, const QChar &leadingPad = QLatin1Char('0')) const;

    QString applyClipped(qint64 i, const QChar &leadingPad = QLatin1Char('0')) const;

    inline QString apply(int i, const QChar &leadingPad = QLatin1Char('0')) const
    { return apply((qint64) i, leadingPad); }

    inline QString applyClipped(int i, const QChar &leadingPad = QLatin1Char('0')) const
    { return applyClipped((qint64) i, leadingPad); }

    QString superscript(double d, const QChar &leadingPad = QLatin1Char('0')) const;

    QString superscriptClipped(double d, const QChar &leadingPad = QLatin1Char('0')) const;

    QString superscript(qint64 i, const QChar &leadingPad = QLatin1Char('0')) const;

    QString superscriptClipped(qint64 i, const QChar &leadingPad = QLatin1Char('0')) const;

    inline QString superscript(int i, const QChar &leadingPad = QLatin1Char('0')) const
    { return superscript((qint64) i, leadingPad); }

    inline QString superscriptClipped(int i, const QChar &leadingPad = QLatin1Char('0')) const
    { return superscriptClipped((qint64) i, leadingPad); }

    QString subscript(double d, const QChar &leadingPad = QLatin1Char('0')) const;

    QString subscriptClipped(double d, const QChar &leadingPad = QLatin1Char('0')) const;

    QString subscript(qint64 i, const QChar &leadingPad = QLatin1Char('0')) const;

    QString subscriptClipped(qint64 i, const QChar &leadingPad = QLatin1Char('0')) const;

    inline QString subscript(int i, const QChar &leadingPad = QLatin1Char('0')) const
    { return subscript((qint64) i, leadingPad); }

    inline QString subscriptClipped(int i, const QChar &leadingPad = QLatin1Char('0')) const
    { return subscriptClipped((qint64) i, leadingPad); }

    QString getFormatPrintf(const QString &type = QString(QLatin1Char('f'))) const;

    QString getFormatCPD2SFMT() const;

    QString getDigitFilled(const QChar &fill = QLatin1Char('0')) const;

    QString mvc() const;

    int width() const;

    int getIntegerDigits() const;

    int getDecimalDigits() const;

    int getExponentDigits() const;

    bool getShowPositiveSign() const;

    bool getShowPositiveExponent() const;

    QString getExponentSeparator() const;

    Mode getMode() const;

    bool isHexUppercase() const;

    QString getBasePrefix() const;

    void setIntegerDigits(int digits);

    void setDecimalDigits(int digits);

    void setExpontentDigits(int digits);

    void setShowPositiveSign(bool show);

    void setShowPositiveExponent(bool show);

    void setExponentSeparator(const QString &sep);

    void setMode(Mode m);

    void setHexUppercase(bool u);

    void setBasePrefix(const QString &h);

    friend CPD3CORE_EXPORT QDataStream &operator<<(QDataStream &stream, const NumberFormat &fmt);

    friend CPD3CORE_EXPORT QDataStream &operator>>(QDataStream &stream, NumberFormat &fmt);

    friend CPD3CORE_EXPORT QDebug operator<<(QDebug stream, const NumberFormat &fmt);
};

}

Q_DECLARE_METATYPE(CPD3::Calibration)

#endif
