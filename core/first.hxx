/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3COREFIRST_H
#define CPD3COREFIRST_H

#include <string.h>
#include <QtGlobal>
#include <QString>

/* MSVC is weird about this and setting it to 199711L */
#undef CPD3_CXX
#if __cplusplus >= 202000L
# define CPD3_CXX 202000L
#elif __cplusplus >= 201700L
# define CPD3_CXX 201700L
#elif __cplusplus >= 201400L
# define CPD3_CXX 201400L
#elif defined(Q_OS_WIN)
# if defined(_MSVC_LANG)
#  if _MSVC_LANG >= 201700L
#   define CPD3_CXX 201700L
#  elif _MSVC_LANG >= 201400L
#   define CPD3_CXX 201400L
#  endif
# endif
#endif
#ifndef CPD3_CXX
#define CPD3_CXX 201100L
#endif

#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
#include <QHash>
#include <functional>
namespace std {
template<>
struct hash<QString> {
    inline std::size_t operator()(const QString &s) const
    {
        return static_cast<std::size_t>(qHash(s));
    }
};
}
#endif

#endif
