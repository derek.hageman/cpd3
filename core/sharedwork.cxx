/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QThreadPool>

#include "core/sharedwork.hxx"
#include "core/threadpool.hxx"

namespace CPD3 {


/** @file core/sharedwork.hxx
 * Provides utilities to handle threaded working with work units that
 * can have a shared common data pool.
 */


namespace {

class QtThreadPoolRunner : public QRunnable {
    std::function<void()> task;
public:
    explicit QtThreadPoolRunner(const std::function<void()> &task) : task(task)
    {
        setAutoDelete(true);
    }

    explicit QtThreadPoolRunner(std::function<void()> &&task) : task(std::move(task))
    {
        setAutoDelete(true);
    }

    void run() override
    { task(); }
};

}


void QtThreadWorkLauncher::operator()(const std::function<void()> &run) const
{
    QThreadPool::globalInstance()->start(new QtThreadPoolRunner(run));
}

void QtThreadWorkLauncher::operator()(std::function<void()> &&run) const
{
    QThreadPool::globalInstance()->start(new QtThreadPoolRunner(std::move(run)));
}

void StdThreadWorkLauncher::operator()(const std::function<void()> &run) const
{
    ThreadPool::system()->run(run);
}

}
