/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>
#include <QAtomicInt>

#include "core/threadpool.hxx"

using namespace CPD3;

class ThreadPoolTask : public QRunnable {
    QAtomicInt *counter;
    int before;
    int after;
public:
    QThread *runOn;

    ThreadPoolTask(QAtomicInt *target = NULL, int sleepBefore = 0, int sleepAfter = 0) : counter(
            target), before(sleepBefore), after(sleepAfter), runOn(NULL)
    {
        setAutoDelete(true);
    }

    virtual ~ThreadPoolTask()
    { }

    virtual void run()
    {
        runOn = QThread::currentThread();

        if (before > 0)
            QTest::qSleep(before);
        if (counter != NULL)
            counter->ref();
        if (after > 0)
            QTest::qSleep(after);
    }
};

class TestThreadPool : public QObject {
Q_OBJECT
private slots:

    void basic()
    {
        ThreadPool pool;
        QAtomicInt counter(0);

        for (int i = 0; i < 100; i++) {
            pool.run(new ThreadPoolTask(&counter));
        }

        pool.wait();

        QCOMPARE(counter.fetchAndAddOrdered(0), 100);
    }

    void globalPool()
    {
        QAtomicInt counter(0);

        for (int i = 0; i < 100; i++) {
            ThreadPool::system()->run(new ThreadPoolTask(&counter));
        }

        ThreadPool::system()->wait();
    }

    void blocking()
    {
        ThreadPool pool(2);
        QAtomicInt counter(0);

        QAtomicInt blocking(0);
        pool.run(new ThreadPoolTask(&blocking, 200));

        for (int i = 0; i < 100; i++) {
            pool.run(new ThreadPoolTask(&counter));
        }

        QTest::qSleep(50);
        QCOMPARE(counter.fetchAndAddOrdered(0), 100);

        pool.wait();

        QCOMPARE(counter.fetchAndAddOrdered(0), 100);
        QCOMPARE(blocking.fetchAndAddOrdered(0), 1);
    }

    void blockingIdleUse()
    {
        ThreadPool pool(3);

        QAtomicInt blocking1(0);
        ThreadPoolTask taskBlock1(&blocking1, 250);
        taskBlock1.setAutoDelete(false);
        pool.run(&taskBlock1);

        QAtomicInt counter1(0);
        ThreadPoolTask taskFast1(&counter1);
        taskFast1.setAutoDelete(false);
        pool.run(&taskFast1);

        QTest::qSleep(50);
        QCOMPARE(blocking1.fetchAndAddOrdered(0), 0);
        QCOMPARE(counter1.fetchAndAddOrdered(0), 1);

        ThreadPoolTask taskFast2(&counter1);
        taskFast2.setAutoDelete(false);
        pool.run(&taskFast2);

        QTest::qSleep(50);

        for (int i = 0; i < 100; i++) {
            pool.run(new ThreadPoolTask(&counter1, 2));
        }

        QAtomicInt blocking2(0);
        pool.run(new ThreadPoolTask(&blocking2, 0, 200));

        QAtomicInt counter2(0);
        for (int i = 0; i < 100; i++) {
            pool.run(new ThreadPoolTask(&counter2));
        }
        QTest::qSleep(50);
        QCOMPARE(blocking1.fetchAndAddOrdered(0), 0);
        QCOMPARE(blocking2.fetchAndAddOrdered(0), 0);
        QCOMPARE(counter2.fetchAndAddOrdered(0), 0);
        QVERIFY(counter1.fetchAndAddOrdered(0) > 1);

        pool.wait();

        QCOMPARE(blocking1.fetchAndAddOrdered(0), 1);
        QCOMPARE(blocking2.fetchAndAddOrdered(0), 1);
        QCOMPARE(counter1.fetchAndAddOrdered(0), 102);
        QCOMPARE(counter2.fetchAndAddOrdered(0), 100);
        QVERIFY(taskFast2.runOn != NULL);
        QVERIFY(taskFast2.runOn == taskBlock1.runOn || taskFast2.runOn == taskFast1.runOn);
    }

    void immediateRun()
    {
        ThreadPool pool(1);
        QAtomicInt counter(0);

        QAtomicInt blocking(0);
        pool.run(new ThreadPoolTask(&blocking, 200));

        for (int i = 0; i < 10; i++) {
            pool.immediate(new ThreadPoolTask(&counter));
        }

        QTest::qSleep(50);
        QCOMPARE(counter.fetchAndAddOrdered(0), 10);

        pool.wait();

        QCOMPARE(counter.fetchAndAddOrdered(0), 10);
        QCOMPARE(blocking.fetchAndAddOrdered(0), 1);
    }

    void immediateExtraThreadReap()
    {
        ThreadPool pool(1);
        QAtomicInt counter(0);

        QAtomicInt blocking(0);
        pool.run(new ThreadPoolTask(&blocking, 200));

        QAtomicInt delayed(0);
        for (int i = 0; i < 100; i++) {
            pool.run(new ThreadPoolTask(&delayed));
        }

        for (int i = 0; i < 10; i++) {
            pool.immediate(new ThreadPoolTask(&counter));
        }

        QTest::qSleep(50);
        QCOMPARE(counter.fetchAndAddOrdered(0), 10);
        QCOMPARE(delayed.fetchAndAddOrdered(0), 0);

        pool.wait();

        QCOMPARE(counter.fetchAndAddOrdered(0), 10);
        QCOMPARE(blocking.fetchAndAddOrdered(0), 1);
        QCOMPARE(delayed.fetchAndAddOrdered(0), 100);
    }

    void immediateReuseThread()
    {
        ThreadPool pool(1);
        QAtomicInt counter(0);
        QAtomicInt blocking(0);

        ThreadPoolTask itask(&blocking, 200);
        itask.setAutoDelete(false);

        ThreadPoolTask ntask(&counter);
        ntask.setAutoDelete(false);

        pool.immediate(&itask);
        pool.run(&ntask);

        QTest::qSleep(50);
        QCOMPARE(counter.fetchAndAddOrdered(0), 0);
        QCOMPARE(blocking.fetchAndAddOrdered(0), 0);

        pool.wait();
        QCOMPARE(counter.fetchAndAddOrdered(0), 1);
        QCOMPARE(blocking.fetchAndAddOrdered(0), 1);

        QVERIFY(itask.runOn != NULL);
        QVERIFY(itask.runOn == ntask.runOn);
    }
};

QTEST_APPLESS_MAIN(TestThreadPool)

#include "threadpool.moc"
