/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>
#include <QString>
#include <QStringList>
#include <QList>
#include <QByteArray>

#include "core/csv.hxx"

using namespace CPD3;

Q_DECLARE_METATYPE(QList<QByteArray>);

namespace QTest {
template<>
char *toString(const QList<QByteArray> &fields)
{
    QByteArray ba = "Fields(";
    for (int i = 0, max = fields.size(); i < max; i++) {
        if (i != 0) ba += ", ";
        ba += "'" + fields.at(i) + "'";
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestCSV : public QObject {
Q_OBJECT
private slots:

    void join()
    {
        QFETCH(QStringList, fields);
        QFETCH(QString, expected);

        QString result(CSV::join(fields));
        QCOMPARE(result, expected);
    }

    void join_data()
    {
        QTest::addColumn<QStringList>("fields");
        QTest::addColumn<QString>("expected");

        QStringList fields;

        QTest::newRow("Empty") << fields << QString("");
        fields << "Field1";
        QTest::newRow("Single") << fields << QString("Field1");
        fields << "Field2";
        QTest::newRow("Two") << fields << QString("Field1,Field2");
        fields << "AB,CD";
        QTest::newRow("Quoted") << fields << QString("Field1,Field2,\"AB,CD\"");
        fields << "EF\"GH";
        QTest::newRow("Escaped") << fields << QString("Field1,Field2,\"AB,CD\",\"EF\"\"\"GH\"");
    }

    void split()
    {
        QFETCH(QString, input);
        QFETCH(QStringList, expected);

        QStringList result(CSV::split(input));
        QCOMPARE(result, expected);
    }

    void split_data()
    {
        QTest::addColumn<QString>("input");
        QTest::addColumn<QStringList>("expected");

        QStringList fields;
        fields << "";
        QTest::newRow("Empty") << QString("") << fields;

        fields.clear();
        fields << "Field1";
        QTest::newRow("Single") << QString("Field1") << fields;
        fields << "Field2";
        QTest::newRow("Two") << QString("Field1,Field2") << fields;
        fields << "Field3 and space";
        QTest::newRow("Space") << QString("Field1,Field2,Field3 and space") << fields;
        fields << "Field,4";
        QTest::newRow("Quote") << QString("Field1,Field2,Field3 and space,\"Field,4\"") << fields;
        fields << "Field\"5";
        QTest::newRow("Escape")
                << QString("Field1,Field2,Field3 and space,\"Field,4\",\"Field\"\"\"5\"") << fields;

        fields.clear();
        fields << "Leading, with \"stuff\", and such";
        QTest::newRow("First with multiple")
                << QString("\"Leading, with \"\"\"stuff\"\"\", and such\"") << fields;
        fields << "Bland";
        QTest::newRow("Multiple second")
                << QString("\"Leading, with \"\"\"stuff\"\"\", and such\",Bland") << fields;

        fields.clear();
        fields << "";
        fields << "";
        fields << "";
        QTest::newRow("Empty fields") << QString(",,") << fields;
        fields << "Blah";
        QTest::newRow("Empty fields trailing") << QString(",,,Blah") << fields;
        fields << "";
        QTest::newRow("Empty fields trailing empty") << QString(",,,Blah,") << fields;

        fields.clear();
        fields << "S11a";
        fields << "APP";
        fields << "1";
        QTest::newRow("Three no quotes") << QString("S11a,APP,1") << fields;

        fields.clear();
        fields.append("BND");
        fields.append("2004");
        fields.append("324.61183");
        fields.append(
                "USER: splitter has a cracked weld where 3/4\" Swagelok is welded to the s/s tube on one of the spare lines.  I sealed it with silicone grease");
        fields.append(" should replace splitter. /jo");
        QTest::newRow("Degenerate malformed first") << QString(
                "BND,2004,324.61183,USER: splitter has a cracked weld where 3/4\" Swagelok is welded to the s/s tube on one of the spare lines.  I sealed it with silicone grease, should replace splitter. /jo")
                                                    << fields;
        fields.clear();
        fields.append("X");
        fields.append("WLG");
        fields.append("1318058824");
        fields.append("2011-10-08T07:27:04Z");
        fields.append("USER: :Starting leak check\"");
        QTest::newRow("Degenerate malformed second") << QString(
                "X,WLG,1318058824,2011-10-08T07:27:04Z,\"USER: :Starting leak check\"\"\"")
                                                     << fields;
    }

    void acquisitionSplit()
    {
        QFETCH(QByteArray, input);
        QFETCH(QList<QByteArray>, expected);

        QList<QByteArray> result;
        for (const auto &add : CSV::acquisitionSplit(input)) {
            result.push_back(add.toQByteArray());
        }
        QCOMPARE(result, expected);
    }

    void acquisitionSplit_data()
    {
        QTest::addColumn<QByteArray>("input");
        QTest::addColumn<QList<QByteArray> >("expected");

        QList<QByteArray> fields;
        fields.append("");
        QTest::newRow("Empty") << QByteArray("") << fields;
        QTest::newRow("Empty Spaces") << QByteArray("   ") << fields;
        fields.clear();

        fields << "Field1";
        QTest::newRow("Single") << QByteArray("Field1") << fields;
        QTest::newRow("Single Before") << QByteArray("  Field1") << fields;
        QTest::newRow("Single After") << QByteArray("Field1 ") << fields;
        QTest::newRow("Single Both") << QByteArray("  Field1 ") << fields;

        fields.prepend("");
        QTest::newRow("Empty Before") << QByteArray(",Field1") << fields;
        QTest::newRow("Empty Before Padding") << QByteArray(", Field1") << fields;
        fields.append("");
        QTest::newRow("Empty Both") << QByteArray(",Field1,") << fields;
        QTest::newRow("Empty Both Padding") << QByteArray(", Field1 ,") << fields;
        fields.removeFirst();
        QTest::newRow("Empty After") << QByteArray("Field1,") << fields;
        QTest::newRow("Empty After Padding") << QByteArray("Field1 \t,") << fields;
        fields.removeLast();

        fields << "Field2";
        QTest::newRow("Two Space") << QByteArray("Field1 Field2") << fields;
        QTest::newRow("Two Space Padding") << QByteArray("  Field1   Field2 ") << fields;
        QTest::newRow("Two Comma") << QByteArray("Field1,Field2") << fields;
        QTest::newRow("Two Comma Padding") << QByteArray("Field1 ,  Field2") << fields;

        fields << "F";
        QTest::newRow("Three Space") << QByteArray("Field1 Field2 F") << fields;
        QTest::newRow("Three Space Padding") << QByteArray("Field1     Field2    F") << fields;
        QTest::newRow("Three Comma") << QByteArray("Field1,Field2,F") << fields;
        QTest::newRow("Three Comma Padding") << QByteArray("Field1,   Field2,  F") << fields;


        fields.clear();
        fields.append("");
        fields.append("");
        QTest::newRow("Empty Two") << QByteArray(",") << fields;
        QTest::newRow("Empty Two Padding") << QByteArray(" , ") << fields;
        fields.append("");
        QTest::newRow("Empty Three") << QByteArray(",,") << fields;
        QTest::newRow("Empty Three Padding") << QByteArray(",  , ") << fields;
        fields.append("");
        QTest::newRow("Empty Four") << QByteArray(",,,") << fields;
        QTest::newRow("Empty Four Padding") << QByteArray(" , ,  ,") << fields;
    }
};

QTEST_APPLESS_MAIN(TestCSV)

#include "csv.moc"
