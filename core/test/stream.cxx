/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>
#include <QString>
#include <QByteArray>
#include <QDataStream>

#include "core/stream.hxx"
#include "core/number.hxx"
#include "core/timeutils.hxx"

using namespace CPD3;

class TestRangeElement : public Time::Bounds {
public:
    unsigned int code;

    TestRangeElement() : Time::Bounds(), code(0)
    { }

    TestRangeElement(double setStart, double setEnd, unsigned int setCode = 0) : Time::Bounds(
            setStart, setEnd), code(setCode)
    {
        Q_ASSERT(!FP::defined(start) || !FP::defined(end) || start != end);
    }

    TestRangeElement(const TestRangeElement &other) : Time::Bounds(other), code(other.code)
    {
        Q_ASSERT(!FP::defined(start) || !FP::defined(end) || start != end);
    }

    TestRangeElement(const TestRangeElement &other, double setStart, double setEnd) : Time::Bounds(
            setStart, setEnd), code(other.code)
    {
        Q_ASSERT(!FP::defined(start) || !FP::defined(end) || start != end);
    }

    TestRangeElement(const TestRangeElement &o1,
                     const TestRangeElement &o2,
                     double setStart,
                     double setEnd) : Time::Bounds(setStart, setEnd), code(o1.code | o2.code)
    {
        Q_ASSERT(!FP::defined(start) || !FP::defined(end) || start != end);
    }

    TestRangeElement &operator=(const TestRangeElement &other)
    {
        if (&other == this) return *this;
        start = other.start;
        end = other.end;
        code = other.code;
        return *this;
    }

    bool operator==(const TestRangeElement &other) const
    {
        return FP::equal(start, other.start) && FP::equal(end, other.end) && code == other.code;
    }

    static bool sortFallbackCode(const TestRangeElement &a, const TestRangeElement &b)
    {
        int cr = Range::compareStart(a.getStart(), b.getStart());
        if (cr != 0)
            return cr < 0;
        return a.code < b.code;
    }
};

Q_DECLARE_METATYPE(TestRangeElement)

Q_DECLARE_METATYPE(QList<TestRangeElement>)

class TestRangeElementPOD : public TestRangeElement {
public:
    TestRangeElementPOD(const TestRangeElement &other) : TestRangeElement(other)
    { }

    TestRangeElementPOD() : TestRangeElement()
    { }

    TestRangeElementPOD(double s, double e, unsigned int c) : TestRangeElement(s, e, c)
    { }

    TestRangeElementPOD(const TestRangeElementPOD &other) : TestRangeElement(other)
    { }

    TestRangeElementPOD &operator=(const TestRangeElementPOD &other)
    {
        TestRangeElement::operator=(other);
        return *this;
    }
};

Q_DECLARE_TYPEINFO(TestRangeElementPOD, Q_PRIMITIVE_TYPE);

class TestRangeOverlay : public Time::Bounds {
public:
    unsigned int code;

    TestRangeOverlay() : Time::Bounds(), code(0)
    { }

    TestRangeOverlay(double s, double e, unsigned int c = 0) : Time::Bounds(s, e), code(c)
    {
        Q_ASSERT(!FP::defined(start) || !FP::defined(end) || start != end);
    }

    TestRangeOverlay(unsigned int c) : Time::Bounds(), code(c)
    { }

    TestRangeOverlay(const TestRangeOverlay &other) : Time::Bounds(other), code(other.code)
    { }

    TestRangeOverlay &operator=(const TestRangeOverlay &other)
    {
        if (&other == this) return *this;
        start = other.start;
        end = other.end;
        code = other.code;
        return *this;
    }

    bool applyOverlay(TestRangeElement &el, bool intersecting)
    {
        if (intersecting) {
            if (((el.code | code) & 0xF0000000) == 0xF0000000)
                return false;
            el.code |= 0x2000000;
        } else {
            if (((el.code | code) & 0xF0000000) == 0xE0000000)
                return false;
            el.code |= 0x1000000;
        }
        el.code |= code;
        return true;
    }
};

Q_DECLARE_METATYPE(TestRangeOverlay)

Q_DECLARE_METATYPE(QList<TestRangeOverlay>)

class TestRangeOverlayPOD : public TestRangeOverlay {
public:
    TestRangeOverlayPOD(const TestRangeOverlay &other) : TestRangeOverlay(other)
    { }

    TestRangeOverlayPOD() : TestRangeOverlay()
    { }

    TestRangeOverlayPOD(double s, double e, unsigned int c) : TestRangeOverlay(s, e, c)
    { }

    TestRangeOverlayPOD(const TestRangeOverlayPOD &other) : TestRangeOverlay(other)
    { }

    TestRangeOverlayPOD &operator=(const TestRangeOverlayPOD &other)
    {
        TestRangeOverlay::operator=(other);
        return *this;
    }
};

Q_DECLARE_TYPEINFO(TestRangeOverlayPOD, Q_PRIMITIVE_TYPE);

class TestSegmentStream : public SegmentStream<TestRangeElement, TestSegmentStream,
                                               QList<TestRangeElement>> {
public:
    QList<TestRangeElement> active;
    QList<TestRangeElement> injected;

    TestSegmentStream() : active(), injected()
    { }

    TestRangeElement convertActive(double start, double end) const
    {
        unsigned int sum = 0;
        for (QList<TestRangeElement>::const_iterator e = active.constBegin(),
                endA = active.constEnd(); e != endA; ++e) {
            sum |= e->code;
        }
        for (QList<TestRangeElement>::const_iterator e = injected.constBegin(),
                endA = injected.constEnd(); e != endA; ++e) {
            if (Range::compareStartEnd(start, e->getEnd()) >= 0)
                continue;
            sum |= e->code;
        }
        return TestRangeElement(start, end, sum);
    }

    void purgeInjected(double before)
    {
        if (!FP::defined(before))
            return;

    }

    bool purgeBefore(double before)
    {
        for (QList<TestRangeElement>::iterator i = active.begin(); i != active.end();) {
            if (Range::compareStartEnd(before, i->getEnd()) >= 0) {
                i = active.erase(i);
                continue;
            }
            ++i;
        }
        for (QList<TestRangeElement>::iterator i = injected.begin(); i != injected.end();) {
            if (Range::compareStartEnd(before, i->getEnd()) >= 0) {
                i = injected.erase(i);
                continue;
            }
            ++i;
        }
        return !active.isEmpty();
    }

    void addActive(const TestRangeElement &add)
    {
        active.append(add);
    }

    void addInjected(const TestRangeElement &add)
    {
        injected.append(add);
    }

    void clearActive()
    {
        active.clear();
        injected.clear();
    }

    void mergeAll(const TestSegmentStream &other)
    {
        active.append(other.active);
        injected.append(other.injected);
    }
};

class TestSegmentStreamEvent {
public:
    enum Type {
        Add, Inject, AdvanceCompleted, AdvancePartial, Finish, Check,

        OverlaySingle, OverlayStream, Intermediate, Initialize, AddBreak,
    };

    Type type;
    TestRangeElement single;
    QList<TestRangeElement> multiple;
    double time;

    TestSegmentStreamEvent(const TestRangeElement &add, Type t = Add) : type(t),
                                                                        single(add),
                                                                        multiple(),
                                                                        time(FP::undefined())
    { }

    TestSegmentStreamEvent(double start, double end, unsigned int code = 0, Type t = Add) : type(t),
                                                                                            single(start,
                                                                                                   end,
                                                                                                   code),
                                                                                            multiple(),
                                                                                            time(FP::undefined())
    { }

    TestSegmentStreamEvent(double advance, Type t = AdvanceCompleted) : type(t),
                                                                        single(),
                                                                        multiple(),
                                                                        time(advance)
    { }

    TestSegmentStreamEvent(const QList<TestRangeElement> &l, Type t = Check) : type(t),
                                                                               single(),
                                                                               multiple(l),
                                                                               time(FP::undefined())
    { }

    TestSegmentStreamEvent() : type(Finish), single(), multiple(), time(FP::undefined())
    { }

    bool apply(TestSegmentStream &stream, QList<TestRangeElement> &result)
    {
        switch (type) {
        case Add:
            result.append(stream.add(single));
            return true;
        case Inject:
            result.append(stream.inject(single));
            return true;
        case AdvanceCompleted:
            result.append(stream.advanceCompleted(time));
            return true;
        case AdvancePartial:
            result.append(stream.advancePartial(time));
            return true;
        case Finish:
            result.append(stream.finish());
            return true;
        case Check:
            return result == multiple;
        case OverlaySingle:
            stream.overlaySingle(single);
            return true;
        case OverlayStream: {
            TestSegmentStream temp;
            for (QList<TestRangeElement>::const_iterator add = multiple.constBegin(),
                    end = multiple.constEnd(); add != end; ++add) {
                temp.add(*add);
            }
            stream.overlayStream(temp);
            return true;
        }
        case Intermediate: {
            bool valid;
            return stream.intermediate(&valid) == single && valid;
        }
        case Initialize:
            stream.initializeFrom(multiple);
            return true;
        case AddBreak:
            stream.insertBreak(time);
            return true;
        }

        Q_ASSERT(false);
        return false;
    }
};

Q_DECLARE_METATYPE(TestSegmentStreamEvent)

Q_DECLARE_METATYPE(QList<TestSegmentStreamEvent>)

class TestConfigurationStreamEvent {
public:
    enum Type {
        AdvanceChange,
        AdvanceNoChange,
        Apply,
        ApplySingle,
        Active,
        NotActive,
        Front,
        NotEmpty,
        Empty
    };

    Type type;
    TestRangeElement single;
    double time;

    TestConfigurationStreamEvent(double start, double end, unsigned int code = 0, Type t = Apply)
            : type(t), single(start, end, code), time(FP::undefined())
    { }

    TestConfigurationStreamEvent(double advance, Type t = AdvanceChange) : type(t),
                                                                           single(),
                                                                           time(advance)
    { }

    TestConfigurationStreamEvent() : type(NotEmpty), single(), time(FP::undefined())
    { }

    TestConfigurationStreamEvent(Type t) : type(t), single(), time(FP::undefined())
    { }

    bool apply(ConfigurationStream<TestRangeElement> &stream) const
    {
        switch (type) {
        case AdvanceChange:
            return stream.advance(time);
        case AdvanceNoChange:
            return !stream.advance(time);
        case Apply: {
            unsigned int result = 0;
            stream.applyActive(single.getStart(), single.getEnd(),
                               [&result](TestRangeElement &add) {
                                   result |= add.code;
                               });
            return result == single.code;
        }
        case ApplySingle: {
            TestRangeElement result;
            stream.applySingle(single.getStart(), single.getEnd(),
                               [&result](TestRangeElement &add) {
                                   result = add;
                               });
            return result.code == single.code;
        }
        case Active:
            return stream.active(time) != nullptr;
        case NotActive:
            return stream.active(time) == nullptr;
        case Front:
            return stream.front() == single;
        case NotEmpty:
            return !stream.empty();
        case Empty:
            return stream.empty();
        }

        Q_ASSERT(false);
        return false;
    }
};

Q_DECLARE_METATYPE(TestConfigurationStreamEvent)

Q_DECLARE_METATYPE(QList<TestConfigurationStreamEvent>)

namespace QTest {

template<>
char *toString(const TestRangeElement &range)
{
    QByteArray ba = "Range([";
    ba += QByteArray::number(range.start) +
            ":" +
            QByteArray::number(range.end) +
            "] = 0x" +
            QByteArray::number(range.code, 16);
    ba += ")";
    return qstrdup(ba.data());
}

template<>
char *toString(const QList<TestRangeElement> &ranges)
{
    QByteArray ba = "RangeList(";
    for (int i = 0, max = ranges.size(); i < max; i++) {
        if (i != 0) ba += ", ";
        ba += "[" +
                QByteArray::number(ranges[i].start) +
                ":" +
                QByteArray::number(ranges[i].end) +
                "] = 0x" +
                QByteArray::number(ranges[i].code, 16);
    }
    ba += ")";
    return qstrdup(ba.data());
}

template<>
char *toString(const TestRangeElementPOD &range)
{
    QByteArray ba = "Range([";
    ba += QByteArray::number(range.start) +
            ":" +
            QByteArray::number(range.end) +
            "] = 0x" +
            QByteArray::number(range.code, 16);
    ba += ")";
    return qstrdup(ba.data());
}

template<>
char *toString(const QList<TestRangeElementPOD> &ranges)
{
    QByteArray ba = "RangeList(";
    for (int i = 0, max = ranges.size(); i < max; i++) {
        if (i != 0) ba += ", ";
        ba += "[" +
                QByteArray::number(ranges[i].start) +
                ":" +
                QByteArray::number(ranges[i].end) +
                "] = 0x" +
                QByteArray::number(ranges[i].code, 16);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

QDataStream &operator<<(QDataStream &stream, const TestRangeElement &value)
{
    stream << value.start << value.end << value.code;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, TestRangeElement &value)
{
    stream >> value.start >> value.end >> value.code;
    return stream;
}

QDataStream &operator<<(QDataStream &stream, const TestRangeOverlay &value)
{
    stream << value.start << value.end << value.code;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, TestRangeOverlay &value)
{
    stream >> value.start >> value.end >> value.code;
    return stream;
}

QDataStream &operator<<(QDataStream &stream, const TestSegmentStream &value)
{
    value.serialize(stream);
    stream << value.active << value.injected;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, TestSegmentStream &value)
{
    value.deserialize(stream);
    stream >> value.active >> value.injected;
    return stream;
}

class TestStream : public QObject {
Q_OBJECT
private slots:

    void overlayStream()
    {
        QFETCH(QList<TestRangeElement>, stream);
        QFETCH(QList<TestRangeOverlay>, overlay);
        QFETCH(QList<TestRangeElement>, expected);

        OverlayStream<TestRangeElement, TestRangeOverlay, QList<TestRangeElement>> ros1;
        for (QList<TestRangeOverlay>::const_iterator add = overlay.constBegin(),
                end = overlay.constEnd(); add != end; ++add) {
            ros1.overlayInsert(*add, add->start, add->end);
        }
        ros1.overlayEnd();
        for (QList<TestRangeElement>::const_iterator add = stream.constBegin(),
                end = stream.constEnd(); add != end; ++add) {
            ros1.streamInsert(*add);
        }
        QList<TestRangeElement> result(ros1.finish());
        QCOMPARE(result, expected);

        OverlayStream<TestRangeElement, TestRangeOverlay, QList<TestRangeElement>> ros2;
        for (QList<TestRangeOverlay>::const_iterator add = overlay.constBegin(),
                end = overlay.constEnd(); add != end; ++add) {
            ros2.overlayInsert(*add, add->start, add->end);
        }
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << ros2;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> ros2;
            }
        }
        for (QList<TestRangeElement>::const_iterator add = stream.constBegin(),
                end = stream.constEnd(); add != end; ++add) {
            ros2.streamInsert(*add);
        }
        result = ros2.finish();
        QCOMPARE(result, expected);

        OverlayStream<TestRangeElement, TestRangeOverlay, QList<TestRangeElement>> ros3;
        for (QList<TestRangeOverlay>::const_iterator add = overlay.constBegin(),
                end = overlay.constEnd(); add != end; ++add) {
            ros3.overlayInsert(*add, add->start, add->end);
        }
        result.clear();
        for (QList<TestRangeElement>::const_iterator add = stream.constBegin(),
                end = stream.constEnd(); add != end; ++add) {
            ros3.streamInsert(*add);
            result.append(ros3.streamAdvance());
        }
        result.append(ros3.finish());
        QCOMPARE(result, expected);

        OverlayStream<TestRangeElement, TestRangeOverlay, QList<TestRangeElement>> ros4;
        QList<TestRangeElement> addStream(stream);
        QList<TestRangeOverlay> addOverlay(overlay);
        result.clear();
        while (!addStream.isEmpty() || !addOverlay.isEmpty()) {
            if (addStream.isEmpty() ||
                    (!addOverlay.isEmpty() &&
                            Range::compareStart(addOverlay.first().start,
                                                addStream.first().getStart()) <= 0)) {
                ros4.overlayInsert(addOverlay.first(), addOverlay.first().start,
                                   addOverlay.first().end);
                addOverlay.removeFirst();
            } else {
                ros4.streamInsert(addStream.takeFirst());
            }
            result.append(ros4.streamAdvance());
        }
        result.append(ros4.finish());
        QCOMPARE(result, expected);


        QList<TestRangeElementPOD> expectedPOD;
        for (QList<TestRangeElement>::const_iterator add = expected.constBegin(),
                end = expected.constEnd(); add != end; ++add) {
            expectedPOD.append(TestRangeElementPOD(*add));
        }

        OverlayStream<TestRangeElementPOD, TestRangeOverlayPOD, QList<TestRangeElementPOD>> ros5;
        for (QList<TestRangeOverlay>::const_iterator add = overlay.constBegin(),
                end = overlay.constEnd(); add != end; ++add) {
            ros5.overlayInsert(*add, add->start, add->end);
        }
        for (QList<TestRangeElement>::const_iterator add = stream.constBegin(),
                end = stream.constEnd(); add != end; ++add) {
            ros5.streamInsert(*add);
        }
        QList<TestRangeElementPOD> resultPOD(ros5.finish());
        QCOMPARE(resultPOD, expectedPOD);

        OverlayStream<TestRangeElementPOD, TestRangeOverlayPOD, QList<TestRangeElementPOD>> ros6;
        for (QList<TestRangeOverlay>::const_iterator add = overlay.constBegin(),
                end = overlay.constEnd(); add != end; ++add) {
            ros6.overlayInsert(*add, add->start, add->end);
        }
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << ros6;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> ros6;
            }
        }
        for (QList<TestRangeElement>::const_iterator add = stream.constBegin(),
                end = stream.constEnd(); add != end; ++add) {
            ros6.streamInsert(TestRangeElementPOD(*add));
        }
        resultPOD = ros6.finish();
        QCOMPARE(resultPOD, expectedPOD);

        OverlayStream<TestRangeElementPOD, TestRangeOverlayPOD, QList<TestRangeElementPOD>> ros7;
        for (QList<TestRangeOverlay>::const_iterator add = overlay.constBegin(),
                end = overlay.constEnd(); add != end; ++add) {
            ros7.overlayInsert(*add, add->start, add->end);
        }
        resultPOD.clear();
        for (QList<TestRangeElement>::const_iterator add = stream.constBegin(),
                end = stream.constEnd(); add != end; ++add) {
            ros7.streamInsert(*add);
            resultPOD.append(ros7.streamAdvance());
        }
        resultPOD.append(ros7.finish());
        QCOMPARE(resultPOD, expectedPOD);

        OverlayStream<TestRangeElementPOD, TestRangeOverlayPOD, QList<TestRangeElementPOD>> ros8;
        addStream = stream;
        addOverlay = overlay;
        resultPOD.clear();
        while (!addStream.isEmpty() || !addOverlay.isEmpty()) {
            if (addStream.isEmpty() ||
                    (!addOverlay.isEmpty() &&
                            Range::compareStart(addOverlay.first().start,
                                                addStream.first().getStart()) <= 0)) {
                ros8.overlayInsert(addOverlay.first(), addOverlay.first().start,
                                   addOverlay.first().end);
                addOverlay.removeFirst();
            } else {
                ros8.streamInsert(addStream.takeFirst());
            }
            resultPOD.append(ros8.streamAdvance());
        }
        resultPOD.append(ros8.finish());
        QCOMPARE(resultPOD, expectedPOD);

        OverlayStream<TestRangeElement, TestRangeOverlay, QList<TestRangeElement>> ros9;
        for (QList<TestRangeOverlay>::const_iterator add = overlay.constBegin(),
                end = overlay.constEnd(); add != end; ++add) {
            ros9.overlayInsert(*add, add->start, add->end);
        }
        result.clear();
        double lastTime = FP::undefined();
        for (QList<TestRangeElement>::const_iterator add = stream.constBegin(),
                end = stream.constEnd(); add != end; ++add) {
            if (!FP::defined(lastTime) && FP::defined(add->getStart())) {
                result.append(ros9.streamAdvance(add->getStart() - 0.1));
            } else if (FP::defined(lastTime) &&
                    FP::defined(add->getStart()) &&
                    lastTime != add->getStart()) {
                Q_ASSERT(add->getStart() > lastTime);
                result.append(ros9.streamAdvance((add->getStart() + lastTime) * 0.5));
            }
            ros9.streamInsert(*add);
            if (!FP::equal(lastTime, add->getStart())) {
                result.append(ros9.streamAdvance(add->getStart()));
                lastTime = add->getStart();
            }
        }
        result.append(ros9.finish());
        QCOMPARE(result, expected);

        OverlayStream<TestRangeElementPOD, TestRangeOverlayPOD, QList<TestRangeElementPOD>> ros10;
        for (QList<TestRangeOverlay>::const_iterator add = overlay.constBegin(),
                end = overlay.constEnd(); add != end; ++add) {
            ros10.overlayInsert(*add, add->start, add->end);
        }
        resultPOD.clear();
        lastTime = FP::undefined();
        for (QList<TestRangeElement>::const_iterator add = stream.constBegin(),
                end = stream.constEnd(); add != end; ++add) {
            if (!FP::defined(lastTime) && FP::defined(add->getStart())) {
                resultPOD.append(ros10.streamAdvance(add->getStart() - 0.1));
            } else if (FP::defined(lastTime) &&
                    FP::defined(add->getStart()) &&
                    lastTime != add->getStart()) {
                Q_ASSERT(add->getStart() > lastTime);
                resultPOD.append(ros10.streamAdvance((add->getStart() + lastTime) * 0.5));
            }
            ros10.streamInsert(*add);
            if (!FP::equal(lastTime, add->getStart())) {
                resultPOD.append(ros10.streamAdvance(add->getStart()));
                lastTime = add->getStart();
            }
        }
        resultPOD.append(ros10.finish());
        QCOMPARE(resultPOD, expectedPOD);
    }

    void overlayStream_data()
    {
        QTest::addColumn<QList<TestRangeElement> >("stream");
        QTest::addColumn<QList<TestRangeOverlay> >("overlay");
        QTest::addColumn<QList<TestRangeElement> >("expected");

        QTest::newRow("Empty") << (QList<TestRangeElement>()) << (QList<TestRangeOverlay>())
                               << (QList<TestRangeElement>());
        QTest::newRow("Empty overlay A") << (QList<TestRangeElement>())
                                         << (QList<TestRangeOverlay>()
                                                 << TestRangeOverlay(1.0, 2.0, 0))
                                         << (QList<TestRangeElement>());
        QTest::newRow("Empty overlay B") << (QList<TestRangeElement>())
                                         << (QList<TestRangeOverlay>()
                                                 << TestRangeOverlay(1.0, 2.0, 0)
                                                 << TestRangeOverlay(3.0, 4.0, 1))
                                         << (QList<TestRangeElement>());
        QTest::newRow("No overlay A")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0))
                << (QList<TestRangeOverlay>())
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0));
        QTest::newRow("No overlay B") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0)
                                                                    << TestRangeElement(2.0, 3.0,
                                                                                        1))
                                      << (QList<TestRangeOverlay>())
                                      << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0)
                                                                    << TestRangeElement(2.0, 3.0,
                                                                                        1));
        QTest::newRow("No overlay C") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0)
                                                                    << TestRangeElement(1.5, 2.0, 1)
                                                                    << TestRangeElement(4.0, 5.0,
                                                                                        2))
                                      << (QList<TestRangeOverlay>())
                                      << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0)
                                                                    << TestRangeElement(1.5, 2.0, 1)
                                                                    << TestRangeElement(4.0, 5.0,
                                                                                        2));

        QTest::newRow("Single full overlay A")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x1))
                << (QList<TestRangeOverlay>()
                        << TestRangeOverlay(FP::undefined(), FP::undefined(), 0x4))
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x1000005));
        QTest::newRow("Single full overlay B")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x1)
                                              << TestRangeElement(2.0, 3.0, 0x2)
                                              << TestRangeElement(3.0, 4.0, 0x3))
                << (QList<TestRangeOverlay>()
                        << TestRangeOverlay(FP::undefined(), FP::undefined(), 0x4))
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x1000005)
                                              << TestRangeElement(2.0, 3.0, 0x1000006)
                                              << TestRangeElement(3.0, 4.0, 0x1000007));

        QTest::newRow("Single full overlay remove A")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x1))
                << (QList<TestRangeOverlay>()
                        << TestRangeOverlay(FP::undefined(), FP::undefined(), 0xE0000000))
                << (QList<TestRangeElement>());
        QTest::newRow("Single full overlay remove B")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x1)
                                              << TestRangeElement(1.0, 2.0, 0x2)
                                              << TestRangeElement(2.0, 3.0, 0x3))
                << (QList<TestRangeOverlay>()
                        << TestRangeOverlay(FP::undefined(), FP::undefined(), 0xE0000000))
                << (QList<TestRangeElement>());
        QTest::newRow("Single full overlay remove C")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0xA0000000))
                << (QList<TestRangeOverlay>()
                        << TestRangeOverlay(FP::undefined(), FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>());
        QTest::newRow("Single full overlay remove D")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0xA0000000)
                                              << TestRangeElement(2.0, 3.0, 0x1))
                << (QList<TestRangeOverlay>()
                        << TestRangeOverlay(FP::undefined(), FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x41000001));
        QTest::newRow("Single full overlay remove E")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x1)
                                              << TestRangeElement(2.0, 3.0, 0xA0000000))
                << (QList<TestRangeOverlay>()
                        << TestRangeOverlay(FP::undefined(), FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x41000001));
        QTest::newRow("Single full overlay remove F")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x1)
                                              << TestRangeElement(2.0, 3.0, 0x2)
                                              << TestRangeElement(3.0, 4.0, 0xA0000000))
                << (QList<TestRangeOverlay>()
                        << TestRangeOverlay(FP::undefined(), FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x41000001)
                                              << TestRangeElement(2.0, 3.0, 0x41000002));
        QTest::newRow("Single full overlay remove G")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x1)
                                              << TestRangeElement(2.0, 3.0, 0xA0000000)
                                              << TestRangeElement(3.0, 4.0, 0x2))
                << (QList<TestRangeOverlay>()
                        << TestRangeOverlay(FP::undefined(), FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x41000001)
                                              << TestRangeElement(3.0, 4.0, 0x41000002));
        QTest::newRow("Single full overlay remove H")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0xA0000000)
                                              << TestRangeElement(2.0, 3.0, 0x1)
                                              << TestRangeElement(3.0, 4.0, 0x2))
                << (QList<TestRangeOverlay>()
                        << TestRangeOverlay(FP::undefined(), FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x41000001)
                                              << TestRangeElement(3.0, 4.0, 0x41000002));

        QTest::newRow("Single overlay start A")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(FP::undefined(), 2.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x41000001));
        QTest::newRow("Single overlay start B")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(FP::undefined(), 2.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1));
        QTest::newRow("Single overlay start C")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x1)
                                              << TestRangeElement(2.0, 3.0, 0x2))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(FP::undefined(), 2.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x41000001)
                                              << TestRangeElement(2.0, 3.0, 0x2));

        QTest::newRow("Single overlay start remove A")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0xA0000000))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(FP::undefined(), 2.0, 0x40000000))
                << (QList<TestRangeElement>());
        QTest::newRow("Single overlay start remove B")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0xA0000000))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(FP::undefined(), 2.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0xA0000000));
        QTest::newRow("Single overlay start remove C")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0xA0000001)
                                              << TestRangeElement(2.0, 3.0, 0xA0000002))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(FP::undefined(), 2.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0xA0000002));
        QTest::newRow("Single overlay start remove D")
                << (QList<TestRangeElement>() << TestRangeElement(0.0, 1.0, 0x1)
                                              << TestRangeElement(1.0, 2.0, 0xA0000001)
                                              << TestRangeElement(2.0, 3.0, 0xA0000002)
                                              << TestRangeElement(3.0, 4.0, 0xA0000003))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(FP::undefined(), 2.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(0.0, 1.0, 0x41000001)
                                              << TestRangeElement(2.0, 3.0, 0xA0000002)
                                              << TestRangeElement(3.0, 4.0, 0xA0000003));

        QTest::newRow("Single overlay start intersecting A")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 3.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(FP::undefined(), 2.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x42000001)
                                              << TestRangeElement(2.0, 3.0, 0x1));
        QTest::newRow("Single overlay start intersecting B")
                << (QList<TestRangeElement>() << TestRangeElement(0.0, 1.0, 0x1)
                                              << TestRangeElement(1.0, 3.0, 0x2)
                                              << TestRangeElement(3.0, 4.0, 0x3))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(FP::undefined(), 2.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(0.0, 1.0, 0x41000001)
                                              << TestRangeElement(1.0, 2.0, 0x42000002)
                                              << TestRangeElement(2.0, 3.0, 0x2)
                                              << TestRangeElement(3.0, 4.0, 0x3));

        QTest::newRow("Single overlay start intersecting remove A")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 3.0, 0xB0000001))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(FP::undefined(), 2.0, 0x40000000))
                << (QList<TestRangeElement>());
        QTest::newRow("Single overlay start intersecting remove B")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 3.0, 0xB0000001)
                                              << TestRangeElement(3.0, 4.0, 0xB0000002))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(FP::undefined(), 2.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 4.0, 0xB0000002));
        QTest::newRow("Single overlay start intersecting remove C")
                << (QList<TestRangeElement>() << TestRangeElement(0.0, 0.5, 0xB0000001)
                                              << TestRangeElement(0.5, 1.0, 0xA0000002)
                                              << TestRangeElement(1.0, 3.0, 0xB0000003)
                                              << TestRangeElement(3.0, 4.0, 0x4))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(FP::undefined(), 2.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(0.0, 0.5, 0xF1000001)
                                              << TestRangeElement(3.0, 4.0, 0x4));

        QTest::newRow("Single overlay end A")
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 4.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 4.0, 0x41000001));
        QTest::newRow("Single overlay end B")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 3.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 3.0, 0x1));
        QTest::newRow("Single overlay end C")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1)
                                              << TestRangeElement(3.0, 4.0, 0x2))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1)
                                              << TestRangeElement(3.0, 4.0, 0x41000002));

        QTest::newRow("Single overlay end remove A")
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 4.0, 0xA0000000))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>());
        QTest::newRow("Single overlay end remove B")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0xA0000001))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0xA0000001));
        QTest::newRow("Single overlay end remove C")
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 4.0, 0xA0000000)
                                              << TestRangeElement(4.0, 5.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(4.0, 5.0, 0x41000001));
        QTest::newRow("Single overlay end remove D")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0xA0000001)
                                              << TestRangeElement(2.0, 3.0, 0xA0000002)
                                              << TestRangeElement(3.0, 4.0, 0xA0000003)
                                              << TestRangeElement(4.0, 5.0, 0x4))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0xA0000001)
                                              << TestRangeElement(2.0, 3.0, 0xA0000002)
                                              << TestRangeElement(4.0, 5.0, 0x41000004));

        QTest::newRow("Single overlay end intersecting A")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 4.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1)
                                              << TestRangeElement(3.0, 4.0, 0x42000001));
        QTest::newRow("Single overlay end intersecting B")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x1)
                                              << TestRangeElement(2.0, 4.0, 0x2)
                                              << TestRangeElement(4.0, 5.0, 0x3))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x1)
                                              << TestRangeElement(2.0, 3.0, 0x2)
                                              << TestRangeElement(3.0, 4.0, 0x42000002)
                                              << TestRangeElement(4.0, 5.0, 0x41000003));

        QTest::newRow("Single overlay end intersecting remove A")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 4.0, 0xB0000001))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>());
        QTest::newRow("Single overlay end intersecting remove B")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0xB0000001)
                                              << TestRangeElement(2.0, 4.0, 0xB0000002))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0xB0000001));
        QTest::newRow("Single overlay end intersecting remove C")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0xB0000001)
                                              << TestRangeElement(2.0, 4.0, 0xB0000002)
                                              << TestRangeElement(4.0, 5.0, 0xB0000003)
                                              << TestRangeElement(5.0, 6.0, 0xA0000004))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, FP::undefined(), 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0xB0000001)
                                              << TestRangeElement(4.0, 5.0, 0xF1000003));

        QTest::newRow("Single overlay outside A")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1));
        QTest::newRow("Single overlay outside B")
                << (QList<TestRangeElement>() << TestRangeElement(4.0, 5.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(4.0, 5.0, 0x1));
        QTest::newRow("Single overlay outside C")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1)
                                              << TestRangeElement(4.0, 5.0, 0x2))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1)
                                              << TestRangeElement(4.0, 5.0, 0x2));
        QTest::newRow("Single overlay outside D")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x1)
                                              << TestRangeElement(2.0, 3.0, 0x2)
                                              << TestRangeElement(4.0, 5.0, 0x3)
                                              << TestRangeElement(5.0, 6.0, 0x4))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0, 0x1)
                                              << TestRangeElement(2.0, 3.0, 0x2)
                                              << TestRangeElement(4.0, 5.0, 0x3)
                                              << TestRangeElement(5.0, 6.0, 0x4));

        QTest::newRow("Single overlay inside A")
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 5.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 5.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 5.0, 0x41000001));
        QTest::newRow("Single overlay inside B")
                << (QList<TestRangeElement>() << TestRangeElement(3.1, 4.9, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 5.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(3.1, 4.9, 0x41000001));
        QTest::newRow("Single overlay inside C")
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 4.0, 0x1)
                                              << TestRangeElement(4.0, 5.0, 0x2))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 5.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 4.0, 0x41000001)
                                              << TestRangeElement(4.0, 5.0, 0x41000002));

        QTest::newRow("Single overlay inside remove A")
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 5.0, 0xA0000000))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 5.0, 0x40000000))
                << (QList<TestRangeElement>());
        QTest::newRow("Single overlay inside remove B")
                << (QList<TestRangeElement>() << TestRangeElement(3.1, 4.9, 0xA0000000))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 5.0, 0x40000000))
                << (QList<TestRangeElement>());
        QTest::newRow("Single overlay inside remove C")
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 4.0, 0xA0000002)
                                              << TestRangeElement(4.0, 5.0, 0xA0000001))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 5.0, 0x40000000))
                << (QList<TestRangeElement>());

        QTest::newRow("Single overlay fragment start")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 4.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 5.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1)
                                              << TestRangeElement(3.0, 4.0, 0x42000001));
        QTest::newRow("Single overlay fragment end")
                << (QList<TestRangeElement>() << TestRangeElement(4.0, 6.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 5.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(4.0, 5.0, 0x42000001)
                                              << TestRangeElement(5.0, 6.0, 0x1));
        QTest::newRow("Single overlay fragment both")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 6.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 5.0, 0x40000000))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1)
                                              << TestRangeElement(3.0, 5.0, 0x42000001)
                                              << TestRangeElement(5.0, 6.0, 0x1));

        QTest::newRow("Single overlay remove start")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 4.0, 0xB0000000))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 5.0, 0x40000000))
                << (QList<TestRangeElement>());
        QTest::newRow("Single overlay remove end")
                << (QList<TestRangeElement>() << TestRangeElement(4.0, 6.0, 0xB0000000))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 5.0, 0x40000000))
                << (QList<TestRangeElement>());
        QTest::newRow("Single overlay remove both")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 6.0, 0xB0000000))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 5.0, 0x40000000))
                << (QList<TestRangeElement>());

        QTest::newRow("Two overlay before")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(4.0, 5.0, 0x40000020))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1));
        QTest::newRow("Two overlay after")
                << (QList<TestRangeElement>() << TestRangeElement(5.0, 6.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(4.0, 5.0, 0x40000020))
                << (QList<TestRangeElement>() << TestRangeElement(5.0, 6.0, 0x1));
        QTest::newRow("Two overlay middle")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(1.0, 2.0, 0x40000010)
                                              << TestRangeOverlay(3.0, 4.0, 0x40000020))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1));

        QTest::newRow("Two overlay combine first")
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 4.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(4.0, 5.0, 0x40000020))
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 4.0, 0x41000011));
        QTest::newRow("Two overlay combine second")
                << (QList<TestRangeElement>() << TestRangeElement(4.0, 5.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(4.0, 5.0, 0x40000020))
                << (QList<TestRangeElement>() << TestRangeElement(4.0, 5.0, 0x41000021));

        QTest::newRow("Two overlay fragment before one")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 4.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(4.0, 5.0, 0x40000020))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1)
                                              << TestRangeElement(3.0, 4.0, 0x42000011));
        QTest::newRow("Two overlay fragment after one")
                << (QList<TestRangeElement>() << TestRangeElement(4.0, 6.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(4.0, 5.0, 0x40000020))
                << (QList<TestRangeElement>() << TestRangeElement(4.0, 5.0, 0x42000021)
                                              << TestRangeElement(5.0, 6.0, 0x1));
        QTest::newRow("Two overlay fragment over both")
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 5.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(4.0, 5.0, 0x40000020))
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 4.0, 0x42000011)
                                              << TestRangeElement(4.0, 5.0, 0x42000021));
        QTest::newRow("Two overlay fragment both ends")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 6.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(4.0, 5.0, 0x40000020))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1)
                                              << TestRangeElement(3.0, 4.0, 0x42000011)
                                              << TestRangeElement(4.0, 5.0, 0x42000021)
                                              << TestRangeElement(5.0, 6.0, 0x1));

        QTest::newRow("Two overlay remove first")
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 4.0, 0xA0000000))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(4.0, 5.0, 0x40000020))
                << (QList<TestRangeElement>());
        QTest::newRow("Two overlay remove second")
                << (QList<TestRangeElement>() << TestRangeElement(4.0, 5.0, 0xA0000000))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(4.0, 5.0, 0x40000020))
                << (QList<TestRangeElement>());
        QTest::newRow("Two overlay remove both")
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 5.0, 0xB0000000))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(4.0, 5.0, 0x40000020))
                << (QList<TestRangeElement>());
        QTest::newRow("Two overlay remove first before")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 4.0, 0xB0000000))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(4.0, 5.0, 0x40000020))
                << (QList<TestRangeElement>());
        QTest::newRow("Two overlay remove second after")
                << (QList<TestRangeElement>() << TestRangeElement(4.0, 6.0, 0xB0000000))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(4.0, 5.0, 0x40000020))
                << (QList<TestRangeElement>());
        QTest::newRow("Two overlay remove both outside")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 6.0, 0xB0000000))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(4.0, 5.0, 0x40000020))
                << (QList<TestRangeElement>());
        QTest::newRow("Two overlay remove first valid second")
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 4.0, 0xA0000000)
                                              << TestRangeElement(4.0, 5.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(4.0, 5.0, 0x40000020))
                << (QList<TestRangeElement>() << TestRangeElement(4.0, 5.0, 0x41000021));

        QTest::newRow("Two overlay fragment both ends inject A")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 6.0, 0x1)
                                              << TestRangeElement(6.0, 7.0, 0x2))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(4.0, 5.0, 0x40000020))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1)
                                              << TestRangeElement(3.0, 4.0, 0x42000011)
                                              << TestRangeElement(4.0, 5.0, 0x42000021)
                                              << TestRangeElement(5.0, 6.0, 0x1)
                                              << TestRangeElement(6.0, 7.0, 0x2));
        QTest::newRow("Two overlay fragment both ends inject B")
                << (QList<TestRangeElement>() << TestRangeElement(0.0, 0.5, 0x3)
                                              << TestRangeElement(0.5, 1.0, 0x2)
                                              << TestRangeElement(2.0, 6.0, 0x1)
                                              << TestRangeElement(6.0, 7.0, 0x4)
                                              << TestRangeElement(7.0, 8.0, 0x5))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(4.0, 5.0, 0x40000020))
                << (QList<TestRangeElement>() << TestRangeElement(0.0, 0.5, 0x3)
                                              << TestRangeElement(0.5, 1.0, 0x2)
                                              << TestRangeElement(2.0, 3.0, 0x1)
                                              << TestRangeElement(3.0, 4.0, 0x42000011)
                                              << TestRangeElement(4.0, 5.0, 0x42000021)
                                              << TestRangeElement(5.0, 6.0, 0x1)
                                              << TestRangeElement(6.0, 7.0, 0x4)
                                              << TestRangeElement(7.0, 8.0, 0x5));

        QTest::newRow("Two gap first fragment")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 5.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(5.0, 6.0, 0x40000020))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1)
                                              << TestRangeElement(3.0, 4.0, 0x42000011)
                                              << TestRangeElement(4.0, 5.0, 0x1));
        QTest::newRow("Two gap second fragment")
                << (QList<TestRangeElement>() << TestRangeElement(4.0, 7.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(5.0, 6.0, 0x40000020))
                << (QList<TestRangeElement>() << TestRangeElement(4.0, 5.0, 0x1)
                                              << TestRangeElement(5.0, 6.0, 0x42000021)
                                              << TestRangeElement(6.0, 7.0, 0x1));
        QTest::newRow("Two gap middle fragment")
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 6.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(5.0, 6.0, 0x40000020))
                << (QList<TestRangeElement>() << TestRangeElement(3.0, 4.0, 0x42000011)
                                              << TestRangeElement(4.0, 5.0, 0x1)
                                              << TestRangeElement(5.0, 6.0, 0x42000021));
        QTest::newRow("Two gap both fragment")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 7.0, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                              << TestRangeOverlay(5.0, 6.0, 0x40000020))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1)
                                              << TestRangeElement(3.0, 4.0, 0x42000011)
                                              << TestRangeElement(4.0, 5.0, 0x1)
                                              << TestRangeElement(5.0, 6.0, 0x42000021)
                                              << TestRangeElement(6.0, 7.0, 0x1));

        QTest::newRow("Two fragment extra before")
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 5.5, 0x1)
                                              << TestRangeElement(2.1, 3.0, 0x2))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x10)
                                              << TestRangeOverlay(4.0, 5.0, 0x20))
                << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1)
                                              << TestRangeElement(2.1, 3.0, 0x2)
                                              << TestRangeElement(3.0, 4.0, 0x2000011)
                                              << TestRangeElement(4.0, 5.0, 0x2000021)
                                              << TestRangeElement(5.0, 5.5, 0x1));
        QTest::newRow("Two fragment extra after")
                << (QList<TestRangeElement>() << TestRangeElement(2.1, 5.5, 0x2)
                                              << TestRangeElement(5.1, 5.4, 0x1))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(3.0, 4.0, 0x10)
                                              << TestRangeOverlay(4.0, 5.0, 0x20))
                << (QList<TestRangeElement>() << TestRangeElement(2.1, 3.0, 0x2)
                                              << TestRangeElement(3.0, 4.0, 0x2000012)
                                              << TestRangeElement(4.0, 5.0, 0x2000022)
                                              << TestRangeElement(5.0, 5.5, 0x2)
                                              << TestRangeElement(5.1, 5.4, 0x1));

        QTest::newRow("Complex A") << (QList<TestRangeElement>() << TestRangeElement(2.0, 4.5, 0x1)
                                                                 << TestRangeElement(2.5, 3.0, 0x2)
                                                                 << TestRangeElement(2.9, 3.1,
                                                                                     0xB0000003)
                                                                 << TestRangeElement(3.0, 4.0,
                                                                                     0xA0000004)
                                                                 << TestRangeElement(3.1, 3.5, 0x5)
                                                                 << TestRangeElement(3.5, 4.0, 0x6)
                                                                 << TestRangeElement(3.6, 3.9,
                                                                                     0xA0000007)
                                                                 << TestRangeElement(4.1, 4.2, 0x8)
                                                                 << TestRangeElement(4.5, 5.0, 0x9)
                                                                 << TestRangeElement(4.9, 5.1, 0xA)
                                                                 << TestRangeElement(5.1, 8.1, 0xB)
                                                                 << TestRangeElement(6.1, 6.9, 0xC)
                                                                 << TestRangeElement(8.1, 9.0, 0xD)
                                                                 << TestRangeElement(9.0,
                                                                                     FP::undefined(),
                                                                                     0xE))
                                   << (QList<TestRangeOverlay>()
                                           << TestRangeOverlay(3.0, 4.0, 0x40000010)
                                           << TestRangeOverlay(4.0, 5.0, 0x20)
                                           << TestRangeOverlay(6.0, 7.0, 0x30)
                                           << TestRangeOverlay(7.0, 8.0, 0x40))
                                   << (QList<TestRangeElement>() << TestRangeElement(2.0, 3.0, 0x1)
                                                                 << TestRangeElement(2.5, 3.0, 0x2)
                                                                 << TestRangeElement(3.0, 4.0,
                                                                                     0x42000011)
                                                                 << TestRangeElement(3.1, 3.5,
                                                                                     0x41000015)
                                                                 << TestRangeElement(3.5, 4.0,
                                                                                     0x41000016)
                                                                 << TestRangeElement(4.0, 4.5,
                                                                                     0x2000021)
                                                                 << TestRangeElement(4.1, 4.2,
                                                                                     0x1000028)
                                                                 << TestRangeElement(4.5, 5.0,
                                                                                     0x1000029)
                                                                 << TestRangeElement(4.9, 5.0,
                                                                                     0x200002A)
                                                                 << TestRangeElement(5.0, 5.1, 0xA)
                                                                 << TestRangeElement(5.1, 6.0, 0xB)
                                                                 << TestRangeElement(6.0, 7.0,
                                                                                     0x200003B)
                                                                 << TestRangeElement(6.1, 6.9,
                                                                                     0x100003C)
                                                                 << TestRangeElement(7.0, 8.0,
                                                                                     0x200004B)
                                                                 << TestRangeElement(8.0, 8.1, 0xB)
                                                                 << TestRangeElement(8.1, 9.0, 0xD)
                                                                 << TestRangeElement(9.0,
                                                                                     FP::undefined(),
                                                                                     0xE));

        QTest::newRow("Complex B")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 100.0, 0x1)
                                              << TestRangeElement(FP::undefined(), 200.0, 0x2)
                                              << TestRangeElement(FP::undefined(), FP::undefined(),
                                                                  0x3)
                                              << TestRangeElement(120.0, FP::undefined(), 0x4)
                                              << TestRangeElement(200.0, FP::undefined(), 0x5))
                << (QList<TestRangeOverlay>() << TestRangeOverlay(FP::undefined(), 150.0, 0x10)
                                              << TestRangeOverlay(150.0, 300.0, 0x20)
                                              << TestRangeOverlay(300.0, FP::undefined(), 0x30))
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 100.0, 0x1000011)
                                              << TestRangeElement(FP::undefined(), 150.0, 0x2000012)
                                              << TestRangeElement(FP::undefined(), 150.0, 0x2000013)
                                              << TestRangeElement(120.0, 150.0, 0x2000014)
                                              << TestRangeElement(150.0, 200.0, 0x2000022)
                                              << TestRangeElement(150.0, 300.0, 0x2000023)
                                              << TestRangeElement(150.0, 300.0, 0x2000024)
                                              << TestRangeElement(200.0, 300.0, 0x2000025)
                                              << TestRangeElement(300.0, FP::undefined(), 0x2000033)
                                              << TestRangeElement(300.0, FP::undefined(), 0x2000034)
                                              << TestRangeElement(300.0, FP::undefined(),
                                                                  0x2000035));

        QList<TestRangeElement> input;
        QList<TestRangeOverlay> overlay;
        QList<TestRangeElement> result;
        for (int i = 100; i < 200; i++) {
            input << TestRangeElement((double) i, (double) (i + 1), i);
            overlay << TestRangeOverlay((double) i, (double) (i + 1), i ^ 0xDEAD);
            result << TestRangeElement((double) i, (double) (i + 1), 0x1000000 | i | (i ^ 0xDEAD));
        }
        QTest::newRow("Large") << input << overlay << result;
    }

    void overlayStreamAdvance()
    {
        OverlayStream<TestRangeElement, TestRangeOverlay, QList<TestRangeElement>> ros;

        ros.overlayTail(TestRangeOverlay(0x01), 10.0);
        ros.overlayAdvance(10.0);

        double time = FP::undefined();
        QList<TestRangeElement> result;

        result = ros.streamAdvance(10.0, &time);
        QVERIFY(result.empty());
        QCOMPARE(time, 10.0);

        time = FP::undefined();
        result = ros.streamAdvance(20.0, &time);
        QVERIFY(result.empty());
        QCOMPARE(time, 20.0);

        time = FP::undefined();
        result = ros.streamAdvance(20.0, &time);
        QVERIFY(result.empty());
        QCOMPARE(time, 20.0);

        ros.overlayTail(TestRangeOverlay(0x02), 100.0);
        ros.overlayAdvance(100.0);
        ros.streamInsert(TestRangeElement(80.0, 90.0, 0x10));
        ros.streamInsert(TestRangeElement(90.0, 130.0, 0x20));
        ros.streamInsert(TestRangeElement(100.0, 120.0, 0x40));

        time = FP::undefined();
        result = ros.streamAdvance(100.0, &time);
        QCOMPARE(time, 100.0);
        QCOMPARE(result, QList<TestRangeElement>() << TestRangeElement(80.0, 90.0, 0x1000011)
                                                   << TestRangeElement(90.0, 100.0, 0x2000021));

        time = FP::undefined();
        ros.overlayAdvance(100.0);
        result = ros.streamAdvance(100.0, &time);
        QCOMPARE(time, 100.0);
        QVERIFY(result.empty());

        time = FP::undefined();
        ros.overlayAdvance(110.0);
        result = ros.streamAdvance(110.0, &time);
        QCOMPARE(time, 100.0);
        QVERIFY(result.empty());

        ros.overlayTail(TestRangeOverlay(0x04), 120.0);
        ros.overlayAdvance(120.0);

        time = FP::undefined();
        result = ros.streamAdvance(120.0, &time);
        QCOMPARE(time, 120.0);
        std::stable_sort(result.begin(), result.end(), TestRangeElement::sortFallbackCode);
        QCOMPARE(result, QList<TestRangeElement>() << TestRangeElement(100.0, 120.0, 0x1000042)
                                                   << TestRangeElement(100.0, 120.0, 0x2000022));

        time = FP::undefined();
        ros.overlayAdvance(120.0);
        result = ros.streamAdvance(120.0, &time);
        QCOMPARE(time, 120.0);
        QVERIFY(result.empty());

        time = FP::undefined();
        ros.overlayAdvance(125.0);
        result = ros.streamAdvance(125.0, &time);
        QCOMPARE(time, 120.0);
        QVERIFY(result.empty());

        time = FP::undefined();
        ros.overlayEnd();
        result = ros.streamAdvance(130.0, &time);
        QCOMPARE(time, 130.0);
        QCOMPARE(result, QList<TestRangeElement>() << TestRangeElement(120.0, 130.0, 0x2000024));

        time = FP::undefined();
        result = ros.streamAdvance(140.0, &time);
        QCOMPARE(time, 140.0);
        QVERIFY(result.empty());

        ros.overlayEnd();
        time = FP::undefined();
        result = ros.streamAdvance(150.0, &time);
        QCOMPARE(time, 150.0);
        QVERIFY(result.empty());

        result = ros.finish();
        QVERIFY(result.empty());
    }

    void segmentStream()
    {
        QFETCH(QList<TestSegmentStreamEvent>, events);

        TestSegmentStream s1;
        QList<TestRangeElement> result;
        for (QList<TestSegmentStreamEvent>::iterator e = events.begin(), end = events.end();
                e != end;
                ++e) {
            QVERIFY(e->apply(s1, result));
        }

        s1.reset();
        result.clear();
        for (QList<TestSegmentStreamEvent>::iterator e = events.begin(), end = events.end();
                e != end;
                ++e) {
            QVERIFY(e->apply(s1, result));
        }

        s1.reset();
        result.clear();
        int half = events.size() / 2;
        for (int i = 0; i < half; i++) {
            QVERIFY(events[i].apply(s1, result));
        }

        TestSegmentStream s3(s1);
        QList<TestRangeElement> result3(result);
        TestSegmentStream s4;
        s4 = s1;
        QList<TestRangeElement> result4(result);

        TestSegmentStream s2;
        QList<TestRangeElement> result2(result);
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << s1;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> s2;
            }
        }

        for (int i = half; i < events.size(); i++) {
            QVERIFY(events[i].apply(s2, result2));
        }

        for (int i = half; i < events.size(); i++) {
            QVERIFY(events[i].apply(s1, result));
        }

        for (int i = half; i < events.size(); i++) {
            QVERIFY(events[i].apply(s3, result3));
        }

        for (int i = half; i < events.size(); i++) {
            QVERIFY(events[i].apply(s4, result4));
        }
    }

    void segmentStream_data()
    {
        QTest::addColumn<QList<TestSegmentStreamEvent> >("events");

        typedef TestSegmentStreamEvent EV;
        typedef QList<TestSegmentStreamEvent> EVL;
        typedef TestRangeElement RE;
        typedef QList<TestRangeElement> REL;

        QTest::newRow("Empty") << (EVL());
        QTest::newRow("Empty finish") << (EVL() << EV() << EV(REL()));

        QTest::newRow("Single") << (EVL() << EV(RE(100.0, 200.0, 0x1)) << EV()
                                          << EV(REL() << RE(100.0, 200.0, 0x1)));
        QTest::newRow("Single infinite start")
                << (EVL() << EV(RE(FP::undefined(), 200.0, 0x1)) << EV()
                          << EV(REL() << RE(FP::undefined(), 200.0, 0x1)));
        QTest::newRow("Single infinite end")
                << (EVL() << EV(RE(100.0, FP::undefined(), 0x1)) << EV()
                          << EV(REL() << RE(100.0, FP::undefined(), 0x1)));
        QTest::newRow("Single infinite both")
                << (EVL() << EV(RE(FP::undefined(), FP::undefined(), 0x1)) << EV()
                          << EV(REL() << RE(FP::undefined(), FP::undefined(), 0x1)));
        QTest::newRow("Single partial") << (EVL() << EV(RE(100.0, 200.0, 0x1)) << EV(REL())
                                                  << EV(100.0,
                                                        TestSegmentStreamEvent::AdvancePartial)
                                                  << EV(REL()) << EV(101.0,
                                                                     TestSegmentStreamEvent::AdvancePartial)
                                                  << EV(REL() << RE(100.0, 200.0, 0x1)) << EV(102.0,
                                                                                              TestSegmentStreamEvent::AdvancePartial)
                                                  << EV(REL() << RE(100.0, 200.0, 0x1)) << EV()
                                                  << EV(REL() << RE(100.0, 200.0, 0x1)));

        QTest::newRow("Two exact")
                << (EVL() << EV(RE(100.0, 200.0, 0x1)) << EV(REL()) << EV(RE(100.0, 200.0, 0x2))
                          << EV(REL()) << EV() << EV(REL() << RE(100.0, 200.0, 0x3)));
        QTest::newRow("Two exact infinite start")
                << (EVL() << EV(RE(FP::undefined(), 200.0, 0x1)) << EV(REL())
                          << EV(RE(FP::undefined(), 200.0, 0x2)) << EV(REL()) << EV()
                          << EV(REL() << RE(FP::undefined(), 200.0, 0x3)));
        QTest::newRow("Two exact infinite end")
                << (EVL() << EV(RE(100.0, FP::undefined(), 0x1)) << EV(REL())
                          << EV(RE(100.0, FP::undefined(), 0x2)) << EV(REL()) << EV()
                          << EV(REL() << RE(100.0, FP::undefined(), 0x3)));
        QTest::newRow("Two exact infinite both")
                << (EVL() << EV(RE(FP::undefined(), FP::undefined(), 0x1)) << EV(REL())
                          << EV(RE(FP::undefined(), FP::undefined(), 0x2)) << EV(REL()) << EV()
                          << EV(REL() << RE(FP::undefined(), FP::undefined(), 0x3)));
        QTest::newRow("Two start different")
                << (EVL() << EV(RE(100.0, 200.0, 0x1)) << EV(REL()) << EV(100.0) << EV(REL())
                          << EV(150.0) << EV(REL()) << EV(RE(150.0, 200.0, 0x2))
                          << EV(REL() << RE(100.0, 150.0, 0x1)) << EV()
                          << EV(REL() << RE(100.0, 150.0, 0x1) << RE(150.0, 200.0, 0x3)));
        QTest::newRow("Two start different infinite")
                << (EVL() << EV(RE(FP::undefined(), 200.0, 0x1)) << EV(REL())
                          << EV(RE(150.0, 200.0, 0x2))
                          << EV(REL() << RE(FP::undefined(), 150.0, 0x1)) << EV()
                          << EV(REL() << RE(FP::undefined(), 150.0, 0x1) << RE(150.0, 200.0, 0x3)));
        QTest::newRow("Two end different")
                << (EVL() << EV(RE(100.0, 150.0, 0x1)) << EV(REL()) << EV(RE(100.0, 200.0, 0x2))
                          << EV(REL()) << EV()
                          << EV(REL() << RE(100.0, 150.0, 0x3) << RE(150.0, 200.0, 0x2)));
        QTest::newRow("Two end different infinite")
                << (EVL() << EV(RE(100.0, 150.0, 0x1)) << EV(REL())
                          << EV(RE(100.0, FP::undefined(), 0x2)) << EV(REL()) << EV()
                          << EV(REL() << RE(100.0, 150.0, 0x3) << RE(150.0, FP::undefined(), 0x2)));
        QTest::newRow("Two middle different")
                << (EVL() << EV(RE(100.0, 200.0, 0x1)) << EV(REL()) << EV(RE(125.0, 175.0, 0x2))
                          << EV(REL() << RE(100.0, 125.0, 0x1)) << EV()
                          << EV(REL() << RE(100.0, 125.0, 0x1) << RE(125.0, 175.0, 0x3)
                                      << RE(175.0, 200.0, 0x1)));
        QTest::newRow("Two middle different infinite")
                << (EVL() << EV(RE(FP::undefined(), FP::undefined(), 0x1)) << EV(REL())
                          << EV(RE(125.0, 175.0, 0x2))
                          << EV(REL() << RE(FP::undefined(), 125.0, 0x1)) << EV()
                          << EV(REL() << RE(FP::undefined(), 125.0, 0x1) << RE(125.0, 175.0, 0x3)
                                      << RE(175.0, FP::undefined(), 0x1)));
        QTest::newRow("Two middle advance")
                << (EVL() << EV(RE(100.0, 200.0, 0x1)) << EV(100.0) << EV(REL()) << EV(125.0)
                          << EV(REL()) << EV(RE(125.0, 175.0, 0x2))
                          << EV(REL() << RE(100.0, 125.0, 0x1)) << EV(176.0)
                          << EV(REL() << RE(100.0, 125.0, 0x1) << RE(125.0, 175.0, 0x3)) << EV()
                          << EV(REL() << RE(100.0, 125.0, 0x1) << RE(125.0, 175.0, 0x3)
                                      << RE(175.0, 200.0, 0x1)));

        QTest::newRow("Three consecutive")
                << (EVL() << EV(RE(100.0, 200.0, 0x1)) << EV(REL()) << EV(RE(200.0, 300.0, 0x2))
                          << EV(REL() << RE(100.0, 200.0, 0x1)) << EV(RE(300.0, 400.0, 0x4))
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x2)) << EV()
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x2)
                                      << RE(300.0, 400.0, 0x4)));
        QTest::newRow("Three consecutive advance")
                << (EVL() << EV(RE(100.0, 200.0, 0x1)) << EV(REL()) << EV(200.0)
                          << EV(REL() << RE(100.0, 200.0, 0x1)) << EV(RE(200.0, 300.0, 0x2))
                          << EV(300.0)
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x2))
                          << EV(RE(300.0, 400.0, 0x4)) << EV(400.0)
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x2)
                                      << RE(300.0, 400.0, 0x4)) << EV()
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x2)
                                      << RE(300.0, 400.0, 0x4)));
        QTest::newRow("Three consecutive partial")
                << (EVL() << EV(RE(100.0, 200.0, 0x1)) << EV(REL())
                          << EV(101.0, TestSegmentStreamEvent::AdvancePartial)
                          << EV(REL() << RE(100.0, 200.0, 0x1)) << EV(RE(200.0, 300.0, 0x2))
                          << EV(200.0, TestSegmentStreamEvent::AdvancePartial)
                          << EV(REL() << RE(100.0, 200.0, 0x1))
                          << EV(201.0, TestSegmentStreamEvent::AdvancePartial)
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x2))
                          << EV(202.0, TestSegmentStreamEvent::AdvancePartial)
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x2))
                          << EV(RE(300.0, 400.0, 0x4))
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x2))
                          << EV(301.0, TestSegmentStreamEvent::AdvancePartial)
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x2)
                                      << RE(300.0, 400.0, 0x4))
                          << EV(302.0, TestSegmentStreamEvent::AdvancePartial)
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x2)
                                      << RE(300.0, 400.0, 0x4)) << EV()
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x2)
                                      << RE(300.0, 400.0, 0x4)));
        QTest::newRow("Three underlay")
                << (EVL() << EV(RE(100.0, 500.0, 0x1)) << EV(150.0) << EV(REL())
                          << EV(RE(200.0, 300.0, 0x2)) << EV(REL() << RE(100.0, 200.0, 0x1))
                          << EV(RE(300.0, 400.0, 0x4))
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x3))
                          << EV(401.0) << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x3)
                                                   << RE(300.0, 400.0, 0x5)) << EV()
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x3)
                                      << RE(300.0, 400.0, 0x5) << RE(400.0, 500.0, 0x1)));
        QTest::newRow("Three underlay infinite")
                << (EVL() << EV(RE(FP::undefined(), FP::undefined(), 0x1)) << EV(150.0) << EV(REL())
                          << EV(RE(200.0, 300.0, 0x2))
                          << EV(REL() << RE(FP::undefined(), 200.0, 0x1))
                          << EV(RE(300.0, 400.0, 0x4))
                          << EV(REL() << RE(FP::undefined(), 200.0, 0x1) << RE(200.0, 300.0, 0x3))
                          << EV(401.0)
                          << EV(REL() << RE(FP::undefined(), 200.0, 0x1) << RE(200.0, 300.0, 0x3)
                                      << RE(300.0, 400.0, 0x5)) << EV()
                          << EV(REL() << RE(FP::undefined(), 200.0, 0x1) << RE(200.0, 300.0, 0x3)
                                      << RE(300.0, 400.0, 0x5) << RE(400.0, FP::undefined(), 0x1)));
        QTest::newRow("Three underlay partial") << (EVL() << EV(RE(100.0, 500.0, 0x1)) << EV(REL())
                                                          << EV(150.0,
                                                                TestSegmentStreamEvent::AdvancePartial)
                                                          << EV(REL() << RE(100.0, 500.0, 0x1))
                                                          << EV(RE(200.0, 300.0, 0x2))
                                                          << EV(REL() << RE(100.0, 500.0, 0x1))
                                                          << EV(RE(300.0, 400.0, 0x4))
                                                          << EV(REL() << RE(100.0, 500.0, 0x1)
                                                                      << RE(200.0, 300.0, 0x3))
                                                          << EV(401.0,
                                                                TestSegmentStreamEvent::AdvancePartial)
                                                          << EV(REL() << RE(100.0, 500.0, 0x1)
                                                                      << RE(200.0, 300.0, 0x3)
                                                                      << RE(300.0, 400.0, 0x5)
                                                                      << RE(400.0, 500.0, 0x1))
                                                          << EV(402.0,
                                                                TestSegmentStreamEvent::AdvancePartial)
                                                          << EV(550.0,
                                                                TestSegmentStreamEvent::AdvancePartial)
                                                          << EV(REL() << RE(100.0, 500.0, 0x1)
                                                                      << RE(200.0, 300.0, 0x3)
                                                                      << RE(300.0, 400.0, 0x5)
                                                                      << RE(400.0, 500.0, 0x1))
                                                          << EV()
                                                          << EV(REL() << RE(100.0, 500.0, 0x1)
                                                                      << RE(200.0, 300.0, 0x3)
                                                                      << RE(300.0, 400.0, 0x5)
                                                                      << RE(400.0, 500.0, 0x1)));

        QTest::newRow("Four complex")
                << (EVL() << EV(RE(100.0, 500.0, 0x1)) << EV(101.0) << EV(REL())
                          << EV(RE(200.0, 400.0, 0x2)) << EV(REL() << RE(100.0, 200.0, 0x1))
                          << EV(RE(300.0, 450.0, 0x4))
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x3))
                          << EV(RE(450.0, 600.0, 0x8)) << EV(451.0)
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x3)
                                      << RE(300.0, 400.0, 0x7) << RE(400.0, 450.0, 0x5)) << EV()
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x3)
                                      << RE(300.0, 400.0, 0x7) << RE(400.0, 450.0, 0x5)
                                      << RE(450.0, 500.0, 0x9) << RE(500.0, 600.0, 0x8)));

        QTest::newRow("Overlay single empty")
                << (EVL() << EV(RE(100.0, 200.0, 0x1), TestSegmentStreamEvent::OverlaySingle)
                          << EV() << EV(REL() << RE(100.0, 200.0, 0x1)));
        QTest::newRow("Overlay single advance")
                << (EVL() << EV(RE(100.0, 200.0, 0x1), TestSegmentStreamEvent::OverlaySingle)
                          << EV(101.0) << EV(REL()) << EV() << EV(REL() << RE(100.0, 200.0, 0x1)));

        QTest::newRow("Overlay single two")
                << (EVL() << EV(RE(100.0, 200.0, 0x1), TestSegmentStreamEvent::OverlaySingle)
                          << EV(RE(100.0, 200.0, 0x2), TestSegmentStreamEvent::OverlaySingle)
                          << EV(REL()) << EV() << EV(REL() << RE(100.0, 200.0, 0x3)));
        QTest::newRow("Overlay single moving")
                << (EVL() << EV(RE(100.0, 200.0, 0x1), TestSegmentStreamEvent::OverlaySingle)
                          << EV(RE(150.0, 200.0, 0x2), TestSegmentStreamEvent::OverlaySingle)
                          << EV(REL()) << EV() << EV(REL() << RE(150.0, 200.0, 0x3)));
        QTest::newRow("Overlay single existing")
                << (EVL() << EV(RE(100.0, 500.0, 0x1)) << EV(REL()) << EV(RE(200.0, 400.0, 0x2))
                          << EV(REL() << RE(100.0, 200.0, 0x1))
                          << EV(RE(150.0, 450.0, 0x4), TestSegmentStreamEvent::OverlaySingle)
                          << EV(400.0)
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 400.0, 0x7)) << EV()
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 400.0, 0x7)
                                      << RE(400.0, 450.0, 0x5) << RE(450.0, 500.0, 0x1)));
        QTest::newRow("Overlay single existing partial")
                << (EVL() << EV(RE(100.0, 500.0, 0x1)) << EV(RE(200.0, 400.0, 0x2))
                          << EV(201.0, TestSegmentStreamEvent::AdvancePartial)
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 400.0, 0x3))
                          << EV(RE(200.0, 450.0, 0x4), TestSegmentStreamEvent::OverlaySingle)
                          << EV(RE(201.0, 400.0, 0x7), TestSegmentStreamEvent::Intermediate)
                          << EV(400.0)
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 400.0, 0x3))
                          << EV(RE(400.0, 450.0, 0x5), TestSegmentStreamEvent::Intermediate)
                          << EV(401.0)
                          << EV(RE(400.0, 450.0, 0x5), TestSegmentStreamEvent::Intermediate) << EV()
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 400.0, 0x3)
                                      << RE(400.0, 450.0, 0x5) << RE(450.0, 500.0, 0x1)));

        QTest::newRow("Overlay stream empty") << (EVL()
                << EV(REL() << RE(100.0, 200.0, 0x1), TestSegmentStreamEvent::OverlayStream) << EV()
                << EV(REL() << RE(100.0, 200.0, 0x1)));
        QTest::newRow("Overlay stream exact") << (EVL() << EV(RE(100.0, 200.0, 0x1))
                                                        << EV(REL() << RE(100.0, 200.0, 0x2),
                                                              TestSegmentStreamEvent::OverlayStream)
                                                        << EV()
                                                        << EV(REL() << RE(100.0, 200.0, 0x3)));
        QTest::newRow("Overlay stream before") << (EVL() << EV(RE(100.0, 300.0, 0x1))
                                                         << EV(REL() << RE(200.0, 300.0, 0x2),
                                                               TestSegmentStreamEvent::OverlayStream)
                                                         << EV()
                                                         << EV(REL() << RE(200.0, 300.0, 0x3)));
        QTest::newRow("Overlay stream after") << (EVL() << EV(RE(200.0, 300.0, 0x1))
                                                        << EV(REL() << RE(100.0, 300.0, 0x2),
                                                              TestSegmentStreamEvent::OverlayStream)
                                                        << EV()
                                                        << EV(REL() << RE(200.0, 300.0, 0x3)));
        QTest::newRow("Overlay stream outside") << (EVL() << EV(RE(200.0, 300.0, 0x1))
                                                          << EV(REL() << RE(100.0, 400.0, 0x2),
                                                                TestSegmentStreamEvent::OverlayStream)
                                                          << EV()
                                                          << EV(REL() << RE(200.0, 300.0, 0x3)
                                                                      << RE(300.0, 400.0, 0x2)));
        QTest::newRow("Overlay stream inside") << (EVL() << EV(RE(100.0, 400.0, 0x1))
                                                         << EV(REL() << RE(200.0, 300.0, 0x2),
                                                               TestSegmentStreamEvent::OverlayStream)
                                                         << EV()
                                                         << EV(REL() << RE(200.0, 300.0, 0x3)
                                                                     << RE(300.0, 400.0, 0x1)));

        QTest::newRow("Overlay stream multiple over") << (EVL() << EV(RE(200.0, 300.0, 0x1))
                                                                << EV(REL() << RE(100.0, 400.0, 0x2)
                                                                            << RE(100.0, 450.0,
                                                                                  0x4),
                                                                      TestSegmentStreamEvent::OverlayStream)
                                                                << EV()
                                                                << EV(REL() << RE(200.0, 300.0, 0x7)
                                                                            << RE(300.0, 400.0, 0x6)
                                                                            << RE(400.0, 450.0,
                                                                                  0x4)));
        QTest::newRow("Overlay stream multiple under")
                << (EVL() << EV(RE(100.0, 300.0, 0x1)) << EV(RE(200.0, 300.0, 0x2))
                          << EV(REL() << RE(100.0, 200.0, 0x1))
                          << EV(REL() << RE(100.0, 400.0, 0x4),
                                TestSegmentStreamEvent::OverlayStream) << EV()
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0x7)
                                      << RE(300.0, 400.0, 0x4)));
        QTest::newRow("Overlay stream multiple both")
                << (EVL() << EV(RE(100.0, 300.0, 0x1)) << EV(RE(200.0, 300.0, 0x2))
                          << EV(REL() << RE(100.0, 200.0, 0x1))
                          << EV(REL() << RE(100.0, 400.0, 0x4) << RE(150.0, 450.0, 0x8),
                                TestSegmentStreamEvent::OverlayStream) << EV(401.0)
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0xF)
                                      << RE(300.0, 400.0, 0xC)) << EV()
                          << EV(REL() << RE(100.0, 200.0, 0x1) << RE(200.0, 300.0, 0xF)
                                      << RE(300.0, 400.0, 0xC) << RE(400.0, 450.0, 0x8)));

        QTest::newRow("Initialize") << (EVL()
                << EV(REL() << RE(100.0, 300.0, 0x1) << RE(200.0, 300.0, 0x2)
                            << RE(100.0, 400.0, 0x4), TestSegmentStreamEvent::Initialize) << EV()
                << EV(REL() << RE(200.0, 300.0, 0x7) << RE(300.0, 400.0, 0x4)));


        QTest::newRow("Add break before") << (EVL() << EV(150.0, TestSegmentStreamEvent::AddBreak)
                                                    << EV(RE(100.0, 200.0, 0x1)) << EV()
                                                    << EV(REL() << RE(100.0, 150.0, 0x1)
                                                                << RE(150.0, 200.0, 0x1)));
        QTest::newRow("Add break after") << (EVL() << EV(RE(100.0, 200.0, 0x1))
                                                   << EV(150.0, TestSegmentStreamEvent::AddBreak)
                                                   << EV() << EV(REL() << RE(100.0, 150.0, 0x1)
                                                                       << RE(150.0, 200.0, 0x1)));
        QTest::newRow("Add break unaffected")
                << (EVL() << EV(300.0, TestSegmentStreamEvent::AddBreak)
                          << EV(RE(100.0, 200.0, 0x1))
                          << EV(200.0, TestSegmentStreamEvent::AddBreak) << EV()
                          << EV(REL() << RE(100.0, 200.0, 0x1)));
        QTest::newRow("Add break before advance")
                << (EVL() << EV(150.0, TestSegmentStreamEvent::AddBreak)
                          << EV(RE(100.0, 200.0, 0x1)) << EV(160.0)
                          << EV(REL() << RE(100.0, 150.0, 0x1)) << EV()
                          << EV(REL() << RE(100.0, 150.0, 0x1) << RE(150.0, 200.0, 0x1)));
        QTest::newRow("Add break final advance")
                << (EVL() << EV(150.0, TestSegmentStreamEvent::AddBreak) << EV(100.0)
                          << EV(RE(100.0, 200.0, 0x1)) << EV()
                          << EV(REL() << RE(100.0, 150.0, 0x1) << RE(150.0, 200.0, 0x1)));


        QTest::newRow("Inject middle") << (EVL() << EV(RE(100.0, 200.0, 0x1)) << EV(REL())
                                                 << EV(RE(150.0, 200.0, 0x2),
                                                       TestSegmentStreamEvent::Inject) << EV(REL())
                                                 << EV(RE(300.0, 400.0, 0x4))
                                                 << EV(REL() << RE(100.0, 200.0, 0x3)) << EV()
                                                 << EV(REL() << RE(100.0, 200.0, 0x3)
                                                             << RE(300.0, 400.0, 0x4)));
        QTest::newRow("Inject tail") << (EVL() << EV(RE(100.0, 200.0, 0x1)) << EV(REL())
                                               << EV(RE(150.0, 500.0, 0x2),
                                                     TestSegmentStreamEvent::Inject) << EV(REL())
                                               << EV(RE(300.0, 400.0, 0x4))
                                               << EV(REL() << RE(100.0, 200.0, 0x3)) << EV()
                                               << EV(REL() << RE(100.0, 200.0, 0x3)
                                                           << RE(300.0, 400.0, 0x6)));
        QTest::newRow("Inject head")
                << (EVL() << EV(RE(100.0, 300.0, 0x1), TestSegmentStreamEvent::Inject) << EV(REL())
                          << EV(RE(200.0, 300.0, 0x2)) << EV(REL()) << EV(RE(300.0, 400.0, 0x4))
                          << EV(REL() << RE(200.0, 300.0, 0x3)) << EV()
                          << EV(REL() << RE(200.0, 300.0, 0x3) << RE(300.0, 400.0, 0x4)));
        QTest::newRow("Inject ignored") << (EVL() << EV(RE(100.0, 200.0, 0x1)) << EV(REL())
                                                  << EV(RE(200.0, 250.0, 0x2),
                                                        TestSegmentStreamEvent::Inject)
                                                  << EV(REL() << RE(100.0, 200.0, 0x1))
                                                  << EV(RE(300.0, 400.0, 0x4))
                                                  << EV(REL() << RE(100.0, 200.0, 0x1)) << EV()
                                                  << EV(REL() << RE(100.0, 200.0, 0x1)
                                                              << RE(300.0, 400.0, 0x4)));
        QTest::newRow("Inject ignored middle") << (EVL() << EV(RE(100.0, 200.0, 0x1)) << EV(REL())
                                                         << EV(RE(200.0, 250.0, 0x2),
                                                               TestSegmentStreamEvent::Inject)
                                                         << EV(REL() << RE(100.0, 200.0, 0x1))
                                                         << EV(RE(300.0, 400.0, 0x4))
                                                         << EV(REL() << RE(100.0, 200.0, 0x1))
                                                         << EV(RE(400.0, 500.0, 0x8))
                                                         << EV(REL() << RE(100.0, 200.0, 0x1)
                                                                     << RE(300.0, 400.0, 0x4))
                                                         << EV()
                                                         << EV(REL() << RE(100.0, 200.0, 0x1)
                                                                     << RE(300.0, 400.0, 0x4)
                                                                     << RE(400.0, 500.0, 0x8)));
    }

    void configurationStream()
    {
        QFETCH(QList<TestRangeElement>, input);
        QFETCH(QList<TestConfigurationStreamEvent>, events);

        {
            ConfigurationStream<TestRangeElement> stream(input);
            for (const auto &e : events) {
                QVERIFY(e.apply(stream));
            }
        }

        ConfigurationStream<TestRangeElement> s1(input);
        ConfigurationStream<TestRangeElement> s3;
        s3 = s1;

        for (const auto &e : events) {
            QVERIFY(e.apply(s3));
        }

        int half = events.size() / 2;
        for (int i = 0; i < half; i++) {
            QVERIFY(events[i].apply(s1));
        }

        ConfigurationStream<TestRangeElement> s2;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << s1;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> s2;
            }
        }

        for (int i = half; i < events.size(); i++) {
            QVERIFY(events[i].apply(s2));
        }

        for (int i = half; i < events.size(); i++) {
            QVERIFY(events[i].apply(s1));
        }
    }

    void configurationStream_data()
    {
        QTest::addColumn<QList<TestRangeElement> >("input");
        QTest::addColumn<QList<TestConfigurationStreamEvent> >("events");

        typedef QList<TestConfigurationStreamEvent> EVL;
        typedef QList<TestRangeElement> REL;

        QTest::newRow("Empty") << (REL()) << (EVL());
        QTest::newRow("Empty advance") << (REL())
                                       << (EVL{{0.0, TestConfigurationStreamEvent::AdvanceNoChange},
                                               {TestConfigurationStreamEvent::Empty},});

        QTest::newRow("Single") << (REL{{100, 200, 0x01},})
                                << (EVL{{TestConfigurationStreamEvent::NotEmpty},
                                        {0.0,   TestConfigurationStreamEvent::AdvanceNoChange},
                                        {50.0,  75.0,  0x00},
                                        {50.0,  300.0, 0x01},
                                        {50.0,  150.0, 0x01},
                                        {150.0, 175.0, 0x01},
                                        {150.0, 300.0, 0x01},
                                        {300.0, 400.0, 0x00},
                                        {150.0, 200.0, 0x01, TestConfigurationStreamEvent::ApplySingle},
                                        {150.0, TestConfigurationStreamEvent::AdvanceNoChange},
                                        {350.0},
                                        {TestConfigurationStreamEvent::Empty},
                                        {350.0, 400.0, 0x00},});

        QTest::newRow("Multiple") << (REL{{100, 200, 0x01},
                                          {200, 300, 0x02},
                                          {300, 400, 0x04},
                                          {500, 600, 0x10},
                                          {600, 700, 0x20},
                                          {700, 800, 0x40},})
                                  << (EVL{{TestConfigurationStreamEvent::NotEmpty},
                                          {0.0,    TestConfigurationStreamEvent::AdvanceNoChange},
                                          {50.0,   800.0,           0x77},
                                          {50.0,   100.0,           0x00},
                                          {50.0,   250.0,           0x03},
                                          {50.0,   150.0,           0x01, TestConfigurationStreamEvent::ApplySingle},
                                          {150.0,  TestConfigurationStreamEvent::AdvanceNoChange},
                                          {150.0,  250.0,           0x03},
                                          {350.0},
                                          {350.0,  FP::undefined(), 0x74},
                                          {400.0,  500.0,           0x00},
                                          {600.0,  650.0,           0x20},
                                          {1000.0, 2000.0,          0x00},
                                          {360.0,  TestConfigurationStreamEvent::AdvanceNoChange},
                                          {650.0},
                                          {600.0,  700.0,           0x20, TestConfigurationStreamEvent::Front},
                                          {675.0,  750.0,           0x60},
                                          {750.0,  TestConfigurationStreamEvent::Active},
                                          {2000.0, TestConfigurationStreamEvent::NotActive},});
    }
};

QTEST_APPLESS_MAIN(TestStream)

#include "stream.moc"
