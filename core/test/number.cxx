/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <vector>
#include <time.h>
#include <math.h>
#include <QtGlobal>
#include <QTest>
#include <QString>
#include <QDataStream>

#include "core/number.hxx"

using namespace CPD3;

class TestNumber : public QObject {
Q_OBJECT
private slots:

    void defined()
    {
        QVERIFY(FP::defined(1.0));
        QVERIFY(INTEGER::defined(15));
    }

    void undefined()
    {
        QVERIFY(!FP::defined(FP::undefined()));
        QVERIFY(!INTEGER::defined(INTEGER::undefined()));
    }

    void undefine()
    {
        double d = 23.0;
        QVERIFY(FP::defined(d));
        FP::undefine(&d);
        QVERIFY(!FP::defined(d));
        d = -32435.343;
        QVERIFY(FP::defined(d));
        FP::undefine(d);
        QVERIFY(!FP::defined(d));

        qint64 i = -40;
        QVERIFY(INTEGER::defined(i));
        INTEGER::undefine(&i);
        QVERIFY(!INTEGER::defined(i));
        i = 1234545;
        QVERIFY(INTEGER::defined(i));
        INTEGER::undefine(i);
        QVERIFY(!INTEGER::defined(i));
    }

    void ieee754defined()
    {
        double d1 = FP::undefined();
        double d2 = 5.0;
        QVERIFY(!(d1 < d2));
        QVERIFY(!(d1 > d2));
        QVERIFY(!(d1 == d2));
        QVERIFY(d1 != d2);
#ifdef isunordered
        QVERIFY(!isunordered(4.0, d2));
        QVERIFY(isunordered(d1, d2));
        QVERIFY(isunordered(d2, d1));
#endif

        d2 = FP::undefined();
        QVERIFY(!(d1 < d2));
        QVERIFY(!(d1 > d2));
        QVERIFY(!(d1 == d2));
        QVERIFY(d1 != d2);
#ifdef isunordered
        QVERIFY(isunordered(d1, d2));
        QVERIFY(isunordered(d2, d1));
#endif
    }

    void equal()
    {
        double d1 = 12.0;
        double d2 = 12.0;
        QVERIFY(FP::equal(d1, d2));
        QVERIFY(FP::equal(d2, d1));
        d1 = 13.0;
        QVERIFY(!FP::equal(d1, d2));
        QVERIFY(!FP::equal(d2, d1));
        d1 = FP::undefined();
        QVERIFY(!FP::equal(d1, d2));
        QVERIFY(!FP::equal(d2, d1));
        d2 = FP::undefined();
        QVERIFY(FP::equal(d1, d2));
        QVERIFY(FP::equal(d2, d1));

        qint64 i1 = 12;
        qint64 i2 = 12;
        QVERIFY(INTEGER::equal(i1, i2));
        QVERIFY(INTEGER::equal(i2, i1));
        i1 = 13;
        QVERIFY(!INTEGER::equal(i1, i2));
        QVERIFY(!INTEGER::equal(i2, i1));
        i1 = INTEGER::undefined();
        QVERIFY(!INTEGER::equal(i1, i2));
        QVERIFY(!INTEGER::equal(i2, i1));
        i2 = INTEGER::undefined();
        QVERIFY(INTEGER::equal(i1, i2));
        QVERIFY(INTEGER::equal(i2, i1));
    }

    void compare()
    {
        QVERIFY(FP::compare(12.0, 12.0) == 0);
        QVERIFY(FP::compare(12.0, 13.0) < 0);
        QVERIFY(FP::compare(13.0, 12.0) > 0);

        QVERIFY(INTEGER::compare(Q_INT64_C(12), Q_INT64_C(12)) == 0);
        QVERIFY(INTEGER::compare(Q_INT64_C(12), Q_INT64_C(13)) < 0);
        QVERIFY(INTEGER::compare(Q_INT64_C(13), Q_INT64_C(12)) > 0);
    }

    void format()
    {
        QCOMPARE(FP::decimalFormat(0.0, 3), QString("0"));
        QCOMPARE(FP::decimalFormat(1.0, 3), QString("1"));
        QCOMPARE(FP::decimalFormat(-1.0, 3), QString("-1"));
        QCOMPARE(FP::decimalFormat(12.0, 3), QString("12"));
        QCOMPARE(FP::decimalFormat(-12.0, 3), QString("-12"));
        QCOMPARE(FP::decimalFormat(12.1, 0), QString("12"));
        QCOMPARE(FP::decimalFormat(-12.1, 0), QString("-12"));
        QCOMPARE(FP::decimalFormat(12.9, 0), QString("13"));
        QCOMPARE(FP::decimalFormat(-12.9, 0), QString("-13"));
        QCOMPARE(FP::decimalFormat(12.9, 1), QString("12.9"));
        QCOMPARE(FP::decimalFormat(-12.9, 1), QString("-12.9"));
        QCOMPARE(FP::decimalFormat(12.9, 3), QString("12.9"));
        QCOMPARE(FP::decimalFormat(-12.9, 3), QString("-12.9"));
        QCOMPARE(FP::decimalFormat(12.9001, 3), QString("12.9"));
        QCOMPARE(FP::decimalFormat(-12.9001, 3), QString("-12.9"));
        QCOMPARE(FP::decimalFormat(12.9009, 3), QString("12.901"));
        QCOMPARE(FP::decimalFormat(-12.9009, 3), QString("-12.901"));
        QCOMPARE(FP::decimalFormat(273.15, 3), QString("273.15"));
        QCOMPARE(FP::decimalFormat(-273.15, 3), QString("-273.15"));
        QCOMPARE(FP::decimalFormat(FP::undefined(), 3), QString("Undefined"));
    }

    void scientific()
    {
        QFETCH(double, value);
        QFETCH(int, decimals);
        QFETCH(int, exponents);
        QFETCH(QString, expected);

        QString result(FP::scientificFormat(value, decimals, exponents));
        QCOMPARE(result, expected);
    }

    void scientific_data()
    {
        QTest::addColumn<double>("value");
        QTest::addColumn<int>("decimals");
        QTest::addColumn<int>("exponents");
        QTest::addColumn<QString>("expected");

        QTest::newRow("Zero") << 0.0 << 3 << 2 << QString("0.000e+00");
        QTest::newRow("Zero no decimals") << 0.0 << 0 << 3 << QString("0e+000");
        QTest::newRow("Positive no exponent") << 1.123 << 3 << 1 << QString("1.123e+0");
        QTest::newRow("Negative no exponent") << -1.123 << 3 << 1 << QString("-1.123e+0");
        QTest::newRow("Positive rounded exponent") << 9.9999 << 3 << 1 << QString("1.000e+1");
        QTest::newRow("Negative rounded exponent") << -9.9999 << 3 << 1 << QString("-1.000e+1");
        QTest::newRow("Positive and positive exponent") <<
                4.56E1 <<
                4 <<
                2 <<
                QString("4.5600e+01");
        QTest::newRow("Negative and positive exponent") <<
                -4.56E1 <<
                4 <<
                2 <<
                QString("-4.5600e+01");
        QTest::newRow("Positive and negative exponent") <<
                7.8912E-12 <<
                3 <<
                2 <<
                QString("7.891e-12");
        QTest::newRow("Negative and negative exponent") <<
                -7.8912E-12 <<
                3 <<
                2 <<
                QString("-7.891e-12");
        QTest::newRow("Positive and overflow exponent") << 3.45E15 << 2 << 1 << QString("3.45e+15");
        QTest::newRow("Negative and overflow exponent") <<
                -3.45E15 <<
                2 <<
                1 <<
                QString("-3.45e+15");
        QTest::newRow("Positive and underflow exponent") <<
                6.7E-13 <<
                2 <<
                1 <<
                QString("6.70e-13");
        QTest::newRow("Negative and underflow exponent") <<
                -6.7E-13 <<
                2 <<
                1 <<
                QString("-6.70e-13");
        QTest::newRow("Positive no decimal") << 6.0E2 << 0 << 1 << QString("6e+2");
        QTest::newRow("Negative no decimal") << -6.0E2 << 0 << 1 << QString("-6e+2");
        QTest::newRow("Positive no decimal rounded") << 6.3E-2 << 0 << 1 << QString("6e-2");
        QTest::newRow("Negative no decimal rounded") << -6.3E-2 << 0 << 1 << QString("-6e-2");
        QTest::newRow("Positive no decimal rounded overflow") <<
                9.99E-4 <<
                0 <<
                1 <<
                QString("1e-3");
        QTest::newRow("Negative no decimal rounded overflow") <<
                -9.99E-4 <<
                0 <<
                1 <<
                QString("-1e-3");
    }

    void scientificOverrides()
    {
        QCOMPARE(FP::scientificFormat(0.0, 3, 2, "E", "+", QString()), QString("+0.000E00"));
        QCOMPARE(FP::scientificFormat(1.123, 3, 2, "E", "+", QString()), QString("+1.123E00"));
        QCOMPARE(FP::scientificFormat(1.123E2, 3, 2, "E", "+", QString()), QString("+1.123E02"));
    }

    void random()
    {
        Random::integer32();
        Random::integer();

        for (int i = 0; i < 10000; i++) {
            double d = Random::fp();
            QVERIFY(d >= 0.0);
            QVERIFY(d < 1.0);
        }

        QString str(Random::string(16));
        QCOMPARE(str.length(), 16);

        QByteArray data(Random::data(32));
        QCOMPARE(data.length(), 32);

        char bfr[16];
        QVERIFY(Random::fill(bfr, 16));
        QVERIFY(Random::fill(bfr, 15));
    }

    void xorshift()
    {
        quint16 u16a = Random::xorshift16(1234);
        QVERIFY(u16a != 1234);
        quint16 u16b = Random::xorshift16(u16a);
        QVERIFY(u16a != u16b);
        QVERIFY(u16b != 1234);

        quint32 u32a = Random::xorshift32(1234);
        QVERIFY(u32a != 1234);
        quint32 u32b = Random::xorshift32(u32a);
        QVERIFY(u32a != u32b);
        QVERIFY(u32b != 1234);

        quint64 u64a = Random::xorshift64(1234);
        QVERIFY(u64a != 1234);
        quint64 u64b = Random::xorshift64(u64a);
        QVERIFY(u64a != u64b);
        QVERIFY(u64b != 1234);
    }

    void clz()
    {
        QCOMPARE(INTEGER::clz32(0x80000000), (quint32) 0);
        QCOMPARE(INTEGER::clz32(0x40000000), (quint32) 1);
        QCOMPARE(INTEGER::clz32(0x10000000), (quint32) 3);
        QCOMPARE(INTEGER::clz32(0x00010000), (quint32) 15);
        QCOMPARE(INTEGER::clz32(0x0000FFFF), (quint32) 16);
        QCOMPARE(INTEGER::clz32(0x00000002), (quint32) 30);
        QCOMPARE(INTEGER::clz32(0x00000001), (quint32) 31);

        QCOMPARE(INTEGER::clz(Q_UINT64_C(0x8000000000000000)), Q_UINT64_C(0));
        QCOMPARE(INTEGER::clz(Q_UINT64_C(0x4000000000000000)), Q_UINT64_C(1));
        QCOMPARE(INTEGER::clz(Q_UINT64_C(0x1000000000000000)), Q_UINT64_C(3));
        QCOMPARE(INTEGER::clz(Q_UINT64_C(0x0000000100000000)), Q_UINT64_C(31));
        QCOMPARE(INTEGER::clz(Q_UINT64_C(0x00000000FFFFFFFF)), Q_UINT64_C(32));
        QCOMPARE(INTEGER::clz(Q_UINT64_C(0x0000000000000002)), Q_UINT64_C(62));
        QCOMPARE(INTEGER::clz(Q_UINT64_C(0x0000000000000001)), Q_UINT64_C(63));
    }

    void log2()
    {
        QCOMPARE(INTEGER::floorLog2_32(1), (quint32) 0);
        QCOMPARE(INTEGER::floorLog2_32(2), (quint32) 1);
        QCOMPARE(INTEGER::floorLog2_32(3), (quint32) 1);
        QCOMPARE(INTEGER::floorLog2_32(4), (quint32) 2);
        QCOMPARE(INTEGER::floorLog2_32(31), (quint32) 4);
        QCOMPARE(INTEGER::floorLog2_32(32), (quint32) 5);
        QCOMPARE(INTEGER::floorLog2_32(33), (quint32) 5);
        QCOMPARE(INTEGER::floorLog2_32(123456), (quint32) 16);
        QCOMPARE(INTEGER::floorLog2_32(0xFFFFFFFF), (quint32) 31);

        QCOMPARE(INTEGER::floorLog2(Q_UINT64_C(1)), Q_UINT64_C(0));
        QCOMPARE(INTEGER::floorLog2(Q_UINT64_C(2)), Q_UINT64_C(1));
        QCOMPARE(INTEGER::floorLog2(Q_UINT64_C(3)), Q_UINT64_C(1));
        QCOMPARE(INTEGER::floorLog2(Q_UINT64_C(4)), Q_UINT64_C(2));
        QCOMPARE(INTEGER::floorLog2(Q_UINT64_C(31)), Q_UINT64_C(4));
        QCOMPARE(INTEGER::floorLog2(Q_UINT64_C(32)), Q_UINT64_C(5));
        QCOMPARE(INTEGER::floorLog2(Q_UINT64_C(12345678890)), Q_UINT64_C(33));
        QCOMPARE(INTEGER::floorLog2(Q_UINT64_C(0xFFFFFFFFFFFFFFFF)), Q_UINT64_C(63));
    }

    void rotate()
    {
        QCOMPARE(INTEGER::rotl<quint32>(0x80000000, 2), (quint32) 0x00000002);
        QCOMPARE(INTEGER::rotl<quint32>(0x80000001, 2), (quint32) 0x00000006);
        QCOMPARE(INTEGER::rotl<quint32>(0x81000000, 2), (quint32) 0x04000002);

        QCOMPARE(INTEGER::rotr<quint32>(0x00000001, 2), (quint32) 0x40000000);
        QCOMPARE(INTEGER::rotr<quint32>(0x80000001, 2), (quint32) 0x60000000);
        QCOMPARE(INTEGER::rotr<quint32>(0x00000005, 2), (quint32) 0x40000001);
    }

    void calibration()
    {
        Calibration c;
        QCOMPARE(c.apply(123.0), 123.0);
        QCOMPARE(c.inverse(123.0), 123.0);
        QVERIFY(!FP::defined(c.apply(FP::undefined())));
        QVERIFY(!FP::defined(c.inverse(FP::undefined())));
        QVERIFY(c.isIdentity());

        Calibration o;
        o.toIdentity();
        QVERIFY(c == o);
        QVERIFY(!(c != o));

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << c;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> o;
            }
            QVERIFY(c == o);
        }

        c.set(1, 2.0);
        QCOMPARE(c.apply(30.0), 60.0);
        QCOMPARE(c.inverse(60.0), 30.0);
        QVERIFY(!c.isIdentity());
        QVERIFY(!(c == o));
        QVERIFY(c != o);
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << c;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> o;
            }
            QVERIFY(c == o);
        }

        c.set(0, 5.0);
        QCOMPARE(c.apply(30.0), 65.0);
        QCOMPARE(c.inverse(65.0), 30.0);
        QVERIFY(!(c == o));
        QVERIFY(c != o);
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << c;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> o;
            }
            QVERIFY(c == o);
        }

        c.set(1, 1.0);
        c.set(0, 10.0);
        QCOMPARE(c.apply(30.0), 40.0);
        QCOMPARE(c.inverse(40.0), 30.0);
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << c;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> o;
            }
            QVERIFY(c == o);
        }

        c.set(1, 0.0);
        QCOMPARE(c.apply(30.0), 10.0);
        QVERIFY(!FP::defined(c.inverse(10.0)));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << c;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> o;
            }
            QVERIFY(c == o);
        }

        c.clear();
        c.set(2, 1.0);
        c.set(1, -1.0);
        c.set(0, -2.0);
        QCOMPARE(c.apply(1.5), -1.25);
        QCOMPARE(c.inverse(-1.25, Calibration::PreferPositive), 1.5);
        QCOMPARE(c.inverse(-1.25, Calibration::PreferThirdX1), 1.5);
        QCOMPARE(c.inverse(-1.25, Calibration::PreferNegative), 1.5);
        QCOMPARE(c.inverse(-1.25, Calibration::AlwaysPositive), 1.5);
        QCOMPARE(c.inverse(-1.25, Calibration::AlwaysThirdX1), 1.5);
        QCOMPARE(c.inverse(-1.25, Calibration::AlwaysNegative), -0.5);
        QCOMPARE(c.inverse(-2, Calibration::PreferPositive), 1.0);
        QCOMPARE(c.inverse(-2, Calibration::PreferThirdX1), 1.0);
        QCOMPARE(c.inverse(-2, Calibration::PreferNegative), 0.0);
        QVERIFY(!FP::defined(c.inverse(-4.0)));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << c;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> o;
            }
            QVERIFY(c == o);
        }

        c.set(3, 2.0);
        c.set(2, -3.0);
        c.set(1, -3.0);
        c.set(0, 2.0);
        QCOMPARE(c.apply(3.0), 20.0);
        QCOMPARE(c.inverse(20.0), 3.0);
        QCOMPARE(c.inverse(0.0, -1.1, -0.9), -1.0);
        QCOMPARE(c.inverse(0.0, 0.4, 0.6), 0.5);
        QCOMPARE(c.inverse(0.0, 1.9, 2.1), 2.0);
        QCOMPARE(c.inverse(0.0, Calibration::PreferThirdX1), 0.5);
        QCOMPARE(c.inverse(0.0, Calibration::PreferThirdX1, FP::undefined()), -1.0);
        QCOMPARE(c.inverse(0.0, Calibration::AlwaysThirdX1), -1.0);
        QCOMPARE(c.inverse(0.0, Calibration::PreferPositive), 0.5);
        QCOMPARE(c.inverse(0.0, Calibration::AlwaysPositive, 2.0), 0.5);
        QCOMPARE(c.inverse(0.0, Calibration::PreferNegative), 2.0);
        QCOMPARE(c.inverse(0.0, Calibration::PreferNegative, 0.0, 1.0), 0.5);
        QCOMPARE(c.inverse(0.0, Calibration::AlwaysNegative, 0.0, 1.0), 2.0);
        QVERIFY(fabs(c.inverse(2.59807621135331, Calibration::BestInRange, FP::undefined(), 0.0) +
                             3.66025369123435972973e-01) < 1E-9);
        QVERIFY(fabs(
                c.inverse(2.59807621135331, Calibration::BestInRange) - 2.23205080756887674909) <
                        1E-9);
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << c;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> o;
            }
            QVERIFY(c == o);
        }


        c.append(-1.0);
        QCOMPARE(c.apply(2.0), -16.0);
        QVERIFY(fabs(c.inverse(-16.0) - 2.0) < 1E-9);
        QVERIFY(fabs(c.inverse(0.0, 0.2, 0.8) - 0.487452765111963) < 1E-9);
        QVERIFY(fabs(c.inverse(0.0, -1.1, -0.8) + 0.915024347822024) < 1E-9);
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << c;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> o;
            }
            QVERIFY(c == o);
        }
    }

    void numberFormat()
    {
        QFETCH(QString, format);
        QFETCH(double, doubleValue);
        QFETCH(QString, doubleResult);
        QFETCH(QString, doubleClipped);
        QFETCH(int, intValue);
        QFETCH(QString, intResult);
        QFETCH(QString, intClipped);

        NumberFormat fmt(format);
        QCOMPARE(fmt.apply(doubleValue), doubleResult);
        QCOMPARE(fmt.applyClipped(doubleValue), doubleClipped);
        QCOMPARE(fmt.apply(intValue), intResult);
        QCOMPARE(fmt.applyClipped(intValue), intClipped);

        NumberFormat fmt2(fmt);
        QCOMPARE(fmt2.apply(doubleValue), doubleResult);
        QCOMPARE(fmt2.applyClipped(doubleValue), doubleClipped);
        QCOMPARE(fmt2.apply(intValue), intResult);
        QCOMPARE(fmt2.applyClipped(intValue), intClipped);

        NumberFormat fmt3;
        fmt3 = fmt;
        QCOMPARE(fmt3.apply(doubleValue), doubleResult);
        QCOMPARE(fmt3.applyClipped(doubleValue), doubleClipped);
        QCOMPARE(fmt3.apply(intValue), intResult);
        QCOMPARE(fmt3.applyClipped(intValue), intClipped);

        NumberFormat fmt4;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << fmt;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> fmt4;
            }
        }
        QCOMPARE(fmt4.apply(doubleValue), doubleResult);
        QCOMPARE(fmt4.applyClipped(doubleValue), doubleClipped);
        QCOMPARE(fmt4.apply(intValue), intResult);
        QCOMPARE(fmt4.applyClipped(intValue), intClipped);
    }

    void numberFormat_data()
    {
        QTest::addColumn<QString>("format");
        QTest::addColumn<double>("doubleValue");
        QTest::addColumn<QString>("doubleResult");
        QTest::addColumn<QString>("doubleClipped");
        QTest::addColumn<int>("intValue");
        QTest::addColumn<QString>("intResult");
        QTest::addColumn<QString>("intClipped");

        QTest::newRow("Digit decimal") <<
                QString("00.000") <<
                34.56 <<
                QString("34.560") <<
                QString("34.560") <<
                34 <<
                QString("34.000") <<
                QString("34.000");
        QTest::newRow("Digit decimal clip max") <<
                QString("00.000") <<
                102.1234 <<
                QString("102.123") <<
                QString("99.999") <<
                102 <<
                QString("102.000") <<
                QString("99.999");
        QTest::newRow("Digit decimal clip min") <<
                QString("00.000") <<
                -50.3 <<
                QString("-50.300") <<
                QString("-9.999") <<
                -50 <<
                QString("-50.000") <<
                QString("-9.999");
        QTest::newRow("Digit decimal plus sign") <<
                QString("+00.0") <<
                34.4 <<
                QString("+34.4") <<
                QString("+34.4") <<
                500 <<
                QString("+500.0") <<
                QString("+99.9");
        QTest::newRow("Digit decimal plus sign minus") <<
                QString("+00.0") <<
                -34.4 <<
                QString("-34.4") <<
                QString("-34.4") <<
                -500 <<
                QString("-500.0") <<
                QString("-99.9");

        QTest::newRow("Digit scientific") <<
                QString("0.0E+00") <<
                1.23E3 <<
                QString("1.2E+03") <<
                QString("1.2E+03") <<
                3 <<
                QString("3.0E+00") <<
                QString("3.0E+00");
        QTest::newRow("Digit scientific lower") <<
                QString("0.0e+00") <<
                1.23E3 <<
                QString("1.2e+03") <<
                QString("1.2e+03") <<
                3 <<
                QString("3.0e+00") <<
                QString("3.0e+00");
        QTest::newRow("Digit scientific clip max") <<
                QString("0.00E+0") <<
                1.5E10 <<
                QString("1.50E+10") <<
                QString("9.99E+9") <<
                0 <<
                QString("0.00E+0") <<
                QString("0.00E+0");
        QTest::newRow("Digit scientific clip min") <<
                QString("0.00E+0") <<
                -1E10 <<
                QString("-1.00E+10") <<
                QString("-9.99E+9") <<
                0 <<
                QString("0.00E+0") <<
                QString("0.00E+0");
        QTest::newRow("Digit scientific plus sign") <<
                QString("+0.0E+00") <<
                1.23E3 <<
                QString("+1.2E+03") <<
                QString("+1.2E+03") <<
                3 <<
                QString("+3.0E+00") <<
                QString("+3.0E+00");
        QTest::newRow("Digit scientific plus sign minus") <<
                QString("+0.0E+00") <<
                -1.23E3 <<
                QString("-1.2E+03") <<
                QString("-1.2E+03") <<
                -3 <<
                QString("-3.0E+00") <<
                QString("-3.0E+00");
        QTest::newRow("Digit scientific exponent sign") <<
                QString("0.00E00") <<
                1.23E3 <<
                QString("1.23E03") <<
                QString("1.23E03") <<
                3 <<
                QString("3.00E00") <<
                QString("3.00E00");
        QTest::newRow("Digit scientific exponent sign minus") <<
                QString("0.00E00") <<
                1.23E-3 <<
                QString("1.23E-3") <<
                QString("1.23E-3") <<
                3 <<
                QString("3.00E00") <<
                QString("3.00E00");

        QTest::newRow("Digit integer") <<
                QString("000") <<
                34.12 <<
                QString("034") <<
                QString("034") <<
                34 <<
                QString("034") <<
                QString("034");
        QTest::newRow("Digit integer clip max") <<
                QString("000") <<
                12345.0 <<
                QString("12345") <<
                QString("999") <<
                12345 <<
                QString("12345") <<
                QString("999");
        QTest::newRow("Digit integer clip min") <<
                QString("000") <<
                -1234.0 <<
                QString("-1234") <<
                QString("-99") <<
                -1234 <<
                QString("-1234") <<
                QString("-99");

        QTest::newRow("Digit hex") <<
                QString("FFFF") <<
                (double) (0x123A) <<
                QString("123A") <<
                QString("123A") <<
                0x123A <<
                QString("123A") <<
                QString("123A");
        QTest::newRow("Digit hex case") <<
                QString("ffff") <<
                (double) (0x123A) <<
                QString("123a") <<
                QString("123a") <<
                0x123A <<
                QString("123a") <<
                QString("123a");
        QTest::newRow("Digit hex clip max") <<
                QString("FFFF") <<
                (double) (0x1FFFF) <<
                QString("1FFFF") <<
                QString("FFFF") <<
                0x1FFFF <<
                QString("1FFFF") <<
                QString("FFFF");
        QTest::newRow("Digit hex clip min") <<
                QString("FFFF") <<
                -(double) (0x1FFFF) <<
                QString("-1FFFF") <<
                QString("-FFF") <<
                -0x1FFFF <<
                QString("-1FFFF") <<
                QString("-FFF");

        QTest::newRow("Printf decimal") <<
                QString("%06.3f") <<
                34.56 <<
                QString("34.560") <<
                QString("34.560") <<
                34 <<
                QString("34.000") <<
                QString("34.000");
        QTest::newRow("Printf decimal no integer") <<
                QString("%.2f") <<
                34.56 <<
                QString("34.56") <<
                QString("9.99") <<
                34 <<
                QString("34.00") <<
                QString("9.99");
        QTest::newRow("Printf decimal plus sign") <<
                QString("%+05.1f") <<
                34.4 <<
                QString("+34.4") <<
                QString("+9.9") <<
                34 <<
                QString("+34.0") <<
                QString("+9.9");

        QTest::newRow("Printf scientific") <<
                QString("%.1E") <<
                1.23E3 <<
                QString("1.2E+03") <<
                QString("1.2E+03") <<
                3 <<
                QString("3.0E+00") <<
                QString("3.0E+00");
        QTest::newRow("Printf scientific case") <<
                QString("%.1e") <<
                1.23E3 <<
                QString("1.2e+03") <<
                QString("1.2e+03") <<
                3 <<
                QString("3.0e+00") <<
                QString("3.0e+00");
        QTest::newRow("Printf scientific width") <<
                QString("%7.1E") <<
                1.23E3 <<
                QString("1.2E+03") <<
                QString("1.2E+03") <<
                3 <<
                QString("3.0E+00") <<
                QString("3.0E+00");
        QTest::newRow("Printf scientific plus sign") <<
                QString("%+8.1E") <<
                1.23E3 <<
                QString("+1.2E+03") <<
                QString("+1.2E+03") <<
                3 <<
                QString("+3.0E+00") <<
                QString("+3.0E+00");

        QTest::newRow("Printf hex") <<
                QString("%4X") <<
                (double) (0x123A) <<
                QString("123A") <<
                QString("123A") <<
                0x123A <<
                QString("123A") <<
                QString("123A");
        QTest::newRow("Printf hex case") <<
                QString("%4x") <<
                (double) (0x123A) <<
                QString("123a") <<
                QString("123a") <<
                0x123A <<
                QString("123a") <<
                QString("123a");

        QTest::newRow("Printf octal") <<
                QString("%4o") <<
                (double) (0123) <<
                QString("0123") <<
                QString("0123") <<
                0123 <<
                QString("0123") <<
                QString("0123");

        QTest::newRow("CPD2 decimal") <<
                QString("*3.2f") <<
                34.56 <<
                QString("034.56") <<
                QString("034.56") <<
                34 <<
                QString("034.00") <<
                QString("034.00");
        QTest::newRow("CPD2 decimal plus") <<
                QString("*+03.2f") <<
                34.56 <<
                QString("+34.56") <<
                QString("+34.56") <<
                34 <<
                QString("+34.00") <<
                QString("+34.00");

        QTest::newRow("CPD2 scientific") <<
                QString("*01.3.02E") <<
                1.23E3 <<
                QString("1.230E+03") <<
                QString("1.230E+03") <<
                3 <<
                QString("3.000E+00") <<
                QString("3.000E+00");
        QTest::newRow("CPD2 scientific case") <<
                QString("*01.3.02e") <<
                1.23E3 <<
                QString("1.230e+03") <<
                QString("1.230e+03") <<
                3 <<
                QString("3.000e+00") <<
                QString("3.000e+00");
        QTest::newRow("CPD2 scientific plus exponent") <<
                QString("*01.3.+02E") <<
                1.23E3 <<
                QString("1.230E03") <<
                QString("1.230E03") <<
                3 <<
                QString("3.000E00") <<
                QString("3.000E00");
        QTest::newRow("CPD2 scientific plus sign") <<
                QString("*0+1.4.02E") <<
                1.23E3 <<
                QString("+1.2300E+03") <<
                QString("+1.2300E+03") <<
                3 <<
                QString("+3.0000E+00") <<
                QString("+3.0000E+00");

        QTest::newRow("0x upper") <<
                QString("0xFFFF") <<
                (double) (0x123A) <<
                QString("0x123A") <<
                QString("0x123A") <<
                0x123A <<
                QString("0x123A") <<
                QString("0x123A");
        QTest::newRow("0x lower") <<
                QString("0Xffff") <<
                (double) (0x123A) <<
                QString("0X123a") <<
                QString("0X123a") <<
                0x123A <<
                QString("0X123a") <<
                QString("0X123a");
        QTest::newRow("0x mvc") <<
                QString("0x9999") <<
                (double) (0x123A) <<
                QString("0x123A") <<
                QString("0x123A") <<
                0x123A <<
                QString("0x123A") <<
                QString("0x123A");
    }

    void numberFormatSpecial()
    {
        NumberFormat fmt;

        fmt = NumberFormat("000.0");
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << fmt;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> fmt;
            }
        }
        QCOMPARE(fmt.apply(9.93), QString("009.9"));
        QCOMPARE(fmt.getIntegerDigits(), 3);
        QCOMPARE(fmt.getDecimalDigits(), 1);
        QCOMPARE(fmt.getMode(), NumberFormat::Decimal);
        QCOMPARE(fmt.getFormatPrintf(), QString("%05.1f"));
        QCOMPARE(fmt.getFormatCPD2SFMT(), QString("*@03.1f"));
        QCOMPARE(fmt.mvc(), QString("999.9"));
        QCOMPARE(fmt.getDigitFilled(' '), QString("   . "));
        fmt.setIntegerDigits(4);
        fmt.setDecimalDigits(0);
        fmt.setShowPositiveSign(true);
        QCOMPARE(fmt.apply(23.0), QString("+023"));

        fmt = NumberFormat("0.0000E+000");
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << fmt;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> fmt;
            }
        }
        QCOMPARE(fmt.apply(1.23E12), QString("1.2300E+012"));
        QCOMPARE(fmt.getDecimalDigits(), 4);
        QCOMPARE(fmt.getExponentDigits(), 3);
        QCOMPARE(fmt.getExponentSeparator(), QString("E"));
        QCOMPARE(fmt.getMode(), NumberFormat::Scientific);
        QCOMPARE(fmt.getFormatPrintf(), QString("%010.4E"));
        QCOMPARE(fmt.getFormatCPD2SFMT(), QString("*@01.4.03E"));
        QCOMPARE(fmt.mvc(), QString("9.9999E+999"));
        QCOMPARE(fmt.getDigitFilled(' '), QString(" .    E+   "));
        fmt.setDecimalDigits(2);
        fmt.setExpontentDigits(1);
        fmt.setShowPositiveSign(true);
        fmt.setShowPositiveExponent(false);
        fmt.setExponentSeparator("e");
        QCOMPARE(fmt.apply(23.0), QString("+2.30e1"));
        QCOMPARE(fmt.mvc(), QString("+9.99e9"));

        fmt = NumberFormat("+000.0");
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << fmt;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> fmt;
            }
        }
        QCOMPARE(fmt.apply(1.2, ' '), QString("  +1.2"));
        QCOMPARE(fmt.apply(-1.2, ' '), QString("  -1.2"));

        fmt = NumberFormat("FFF");
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << fmt;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> fmt;
            }
        }
        QCOMPARE(fmt.apply(0xABC), QString("ABC"));
        QCOMPARE(fmt.getIntegerDigits(), 3);
        QCOMPARE(fmt.getMode(), NumberFormat::Hex);
        QCOMPARE(fmt.getFormatPrintf(), QString("%03X"));
        QCOMPARE(fmt.mvc(), QString("FFF"));
        fmt.setHexUppercase(false);
        QCOMPARE(fmt.apply(0xABC), QString("abc"));
        QCOMPARE(fmt.getFormatPrintf(), QString("%03x"));
        QCOMPARE(fmt.mvc(), QString("fff"));
    }

    void numberFormatOverflow()
    {
        auto result = FP::decimalFormat(-6.94453673803748e+36, 6);

        bool haveDecimal = false;
        bool canHaveSign = true;
        for (auto digit : result) {
            if (digit == '-' && canHaveSign) {
                canHaveSign = false;
                continue;
            }
            canHaveSign = false;

            if (digit == '0')
                continue;
            if (digit == '1')
                continue;
            if (digit == '2')
                continue;
            if (digit == '3')
                continue;
            if (digit == '4')
                continue;
            if (digit == '5')
                continue;
            if (digit == '6')
                continue;
            if (digit == '7')
                continue;
            if (digit == '8')
                continue;
            if (digit == '9')
                continue;
            if (digit == '.' && !haveDecimal) {
                haveDecimal = true;
                continue;
            }

            QFAIL("Invalid digit");
        }

        NumberFormat fmt;

        fmt = NumberFormat("00.000000");
        result = fmt.apply(-6.94453673803748e+36);

        haveDecimal = false;
        canHaveSign = true;
        for (auto digit : result) {
            if (digit == '-' && canHaveSign) {
                canHaveSign = false;
                continue;
            }
            canHaveSign = false;

            if (digit == '0')
                continue;
            if (digit == '1')
                continue;
            if (digit == '2')
                continue;
            if (digit == '3')
                continue;
            if (digit == '4')
                continue;
            if (digit == '5')
                continue;
            if (digit == '6')
                continue;
            if (digit == '7')
                continue;
            if (digit == '8')
                continue;
            if (digit == '9')
                continue;
            if (digit == '.' && !haveDecimal) {
                haveDecimal = true;
                continue;
            }

            QFAIL("Invalid digit");
        }
    }

#if 0
    void definedBenchmark() {
        static const size_t n = 10000;
        std::vector<double> values(n, 0.0);
        for (size_t i=0; i<n; i++) {
            if (i % 27 == 0) {
                values[i] = FP::undefined();
            } else {
                values[i] = 12.0 + (double)(i % 200) / 27.0;
            }
        }
        
        std::vector<int> result(n, false);
        QBENCHMARK {
            for (size_t i=0; i<n; i++) {
                result[i] = FP::defined(values[i]);
            }
        }
    }
#endif
};

QTEST_APPLESS_MAIN(TestNumber)

#include "number.moc"
