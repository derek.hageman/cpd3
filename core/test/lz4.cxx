/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QtGlobal>
#include <QTest>
#include <QByteArray>
#include <QDataStream>

#include "core/compress_lz4.hxx"

using namespace CPD3;

class TestLZ4 : public QObject {
Q_OBJECT

    QByteArray makeCompressibleData()
    {
        QByteArray result;

        QDataStream stream(&result, QIODevice::WriteOnly);
        while (result.size() < 1048576) {
            stream << (quint32) (result.size() ^ 0xDEADBEEF);
            stream << sin(result.size() / 10.0);
            stream << (quint32) 1;
        }

        return result;
    }

private slots:

    void lz4()
    {
        QByteArray data(makeCompressibleData());

        QByteArray compressed;
        bool ok = CompressorLZ4().compress(data, compressed);
        QVERIFY(ok);
        QVERIFY(!compressed.isEmpty());
        QVERIFY(compressed.size() < data.size());

        QByteArray recovered;
        ok = CompressorLZ4().compress(data, recovered, compressed.size() + 1);
        QVERIFY(ok);
        QCOMPARE(recovered, compressed);

        recovered.clear();
        ok = DecompressorLZ4().decompress(compressed, recovered);
        QVERIFY(ok);
        QCOMPARE(recovered, data);

        recovered.clear();
        ok = DecompressorLZ4().decompress(compressed, recovered, data.size());
        QVERIFY(ok);
        QCOMPARE(recovered, data);

        ok = CompressorLZ4().compress(data, recovered, compressed.size() - 4);
        QVERIFY(!ok);
    }

    void lz4_64k()
    {
        QByteArray data(makeCompressibleData());
        data.resize(32768);

        QByteArray compressed;
        bool ok = CompressorLZ4().compress(data, compressed);
        QVERIFY(ok);
        QVERIFY(!compressed.isEmpty());
        QVERIFY(compressed.size() < data.size());

        QByteArray recovered;
        ok = CompressorLZ4().compress(data, recovered, compressed.size() + 1);
        QVERIFY(ok);
        QCOMPARE(recovered, compressed);

        recovered.clear();
        ok = DecompressorLZ4().decompress(compressed, recovered);
        QVERIFY(ok);
        QCOMPARE(recovered, data);

        recovered.clear();
        ok = DecompressorLZ4().decompress(compressed, recovered, data.size());
        QVERIFY(ok);
        QCOMPARE(recovered, data);

        ok = CompressorLZ4().compress(data, recovered, compressed.size() - 4);
        QVERIFY(!ok);
    }

    void lz4hc()
    {
        QByteArray data(makeCompressibleData());

        QByteArray compressed;
        bool ok = CompressorLZ4HC().compress(data, compressed);
        QVERIFY(ok);
        QVERIFY(!compressed.isEmpty());
        QVERIFY(compressed.size() < data.size());

        QByteArray recovered;
        ok = CompressorLZ4HC().compress(data, recovered, compressed.size() + 1);
        QVERIFY(ok);
        QCOMPARE(recovered, compressed);

        recovered.clear();
        ok = DecompressorLZ4().decompress(compressed, recovered);
        QVERIFY(ok);
        QCOMPARE(recovered, data);

        recovered.clear();
        ok = DecompressorLZ4().decompress(compressed, recovered, data.size());
        QVERIFY(ok);
        QCOMPARE(recovered, data);

        ok = CompressorLZ4HC().compress(data, recovered, compressed.size() - 4);
        QVERIFY(!ok);

        data = QByteArray::fromHex(
                "00c4d3da4a00ff030000027331ff040000026131ff050000027531ff06000000ff"
                        "070000000000000000000000000000000000244000000000000034400000000000"
                        "000000000000408f40019a9999999999f13f000000000000003440000000000000"
                        "3e400000000000000000000000408f4001cdcccccccccc00400000000000000044"
                        "4000000000000049400000000000000000000000409f40016666666666661040ff"
                        "0100000000000049400000000000004e40000000000000000000000000409f40");
        ok = CompressorLZ4HC().compress(data, compressed);
        QVERIFY(ok);
        QVERIFY(!compressed.isEmpty());
        QVERIFY(compressed.size() < data.size());
        recovered.clear();
        ok = DecompressorLZ4().decompress(compressed, recovered);
        QVERIFY(ok);
        QCOMPARE(recovered, data);
    }
};

QTEST_APPLESS_MAIN(TestLZ4)

#include "lz4.moc"
