/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>

#include "core/sharedwork.hxx"

using namespace CPD3;

class TestData {
public:
    int value;
};

class TestResult {
public:
    int value;

    TestResult() : value(-1)
    { }

    TestResult(const TestData &data) : value(data.value)
    { }
};

class TestSharedWork : public QObject {
Q_OBJECT
private slots:

    void basic()
    {
        TestData d;
        d.value = 42;

        SharedWorkHandler<TestResult, TestData> handler;

        QVERIFY(handler.available());

        bool ok = false;
        TestResult result(handler.startAndWait(d, -1, &ok));
        QVERIFY(ok);
        QCOMPARE(result.value, 42);

        result = handler.latestResult();
        QCOMPARE(result.value, 42);

        result = handler.waitResult();
        QCOMPARE(result.value, 42);

        QVERIFY(handler.waitAvailable());
    }

    void ordered()
    {
        TestData d;
        d.value = 42;

        OrderedWorkHandler<TestResult, TestData> handler;
        QVERIFY(handler.isEmpty());
        QCOMPARE(handler.processingQueueLength(), 0);

        for (int i = 0; i < 10; i++) {
            handler.start(d);
            d.value++;
        }

        int r = 42;
        for (int i = 0; i < 5; i++) {
            QVERIFY(handler.waitAvailable(-1, false));
            QVERIFY(!handler.isEmpty());
            bool ok = false;
            QCOMPARE(handler.takeNext(&ok).value, r);
            QVERIFY(ok);
            r++;
        }

        QVERIFY(handler.waitForQueueReady());

        for (int i = 0; i < 5; i++) {
            bool ok = false;
            QCOMPARE(handler.waitAndTake(-1, false, &ok).value, r);
            QVERIFY(ok);
            r++;
        }

        QVERIFY(!handler.waitAvailable(-1, true));
        bool ok = true;
        handler.waitAndTake(-1, true, &ok);
        QVERIFY(!ok);

        QVERIFY(handler.waitForQueueReady());
    }
};

QTEST_APPLESS_MAIN(TestSharedWork)

#include "sharedwork.moc"
