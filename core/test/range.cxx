/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>
#include <QString>
#include <QByteArray>
#include <QDataStream>

#include "core/range.hxx"
#include "core/number.hxx"
#include "core/timeutils.hxx"

using namespace CPD3;

class TestRangeElement : public Time::Bounds {
public:
    unsigned int code;

    TestRangeElement() : Time::Bounds(), code(0)
    { }

    TestRangeElement(double setStart, double setEnd, unsigned int setCode = 0) : Time::Bounds(
            setStart, setEnd), code(setCode)
    {
        Q_ASSERT(!FP::defined(start) || !FP::defined(end) || start != end);
    }

    TestRangeElement(const TestRangeElement &other) : Time::Bounds(other), code(other.code)
    {
        Q_ASSERT(!FP::defined(start) || !FP::defined(end) || start != end);
    }

    TestRangeElement(const TestRangeElement &other, double setStart, double setEnd) : Time::Bounds(
            setStart, setEnd), code(other.code)
    {
        Q_ASSERT(!FP::defined(start) || !FP::defined(end) || start != end);
    }

    TestRangeElement(const TestRangeElement &o1,
                     const TestRangeElement &o2,
                     double setStart,
                     double setEnd) : Time::Bounds(setStart, setEnd), code(o1.code | o2.code)
    {
        Q_ASSERT(!FP::defined(start) || !FP::defined(end) || start != end);
    }

    TestRangeElement &operator=(const TestRangeElement &other)
    {
        if (&other == this) return *this;
        start = other.start;
        end = other.end;
        code = other.code;
        return *this;
    }

    bool operator==(const TestRangeElement &other) const
    {
        return FP::equal(start, other.start) && FP::equal(end, other.end) && code == other.code;
    }
};

Q_DECLARE_METATYPE(TestRangeElement)

Q_DECLARE_METATYPE(QList<TestRangeElement>)

class TestEventElement : public Time::Bounds {
public:
    unsigned int code;

    TestEventElement() : Time::Bounds(), code(0)
    { }

    TestEventElement(double setStart, double setEnd, unsigned int setCode) : Time::Bounds(setStart,
                                                                                          setEnd),
                                                                             code(setCode)
    { }

    TestEventElement(double setStart, unsigned int setCode) : Time::Bounds(setStart, setStart),
                                                              code(setCode)
    { }

    TestEventElement(const TestEventElement &other) : Time::Bounds(other), code(other.code)
    { }

    TestEventElement(const TestEventElement &other, double setStart, double setEnd) : Time::Bounds(
            setStart, setEnd), code(other.code)
    { }

    TestEventElement(const TestEventElement &o1,
                     const TestEventElement &o2,
                     double setStart,
                     double setEnd) : Time::Bounds(setStart, setEnd), code(o1.code | o2.code)
    { }

    TestEventElement &operator=(const TestEventElement &other)
    {
        if (&other == this) return *this;
        start = other.start;
        end = other.end;
        code = other.code;
        return *this;
    }

    double getTime() const
    { return start; }

    bool operator==(const TestEventElement &other) const
    {
        return FP::equal(start, other.start) && FP::equal(end, other.end) && code == other.code;
    }
};

Q_DECLARE_METATYPE(TestEventElement)

Q_DECLARE_METATYPE(QList<TestEventElement>)


namespace QTest {

template<>
char *toString(const TestRangeElement &range)
{
    QByteArray ba = "Range([";
    ba += QByteArray::number(range.start) +
            ":" +
            QByteArray::number(range.end) +
            "] = 0x" +
            QByteArray::number(range.code, 16);
    ba += ")";
    return qstrdup(ba.data());
}

template<>
char *toString(const QList<TestRangeElement> &ranges)
{
    QByteArray ba = "RangeList(";
    for (int i = 0, max = ranges.size(); i < max; i++) {
        if (i != 0) ba += ", ";
        ba += "[" +
                QByteArray::number(ranges[i].start) +
                ":" +
                QByteArray::number(ranges[i].end) +
                "] = 0x" +
                QByteArray::number(ranges[i].code, 16);
    }
    ba += ")";
    return qstrdup(ba.data());
}

template<>
char *toString(const TestEventElement &range)
{
    QByteArray ba = "Range([";
    ba += QByteArray::number(range.start) +
            ":" +
            QByteArray::number(range.end) +
            "] = 0x" +
            QByteArray::number(range.code, 16);
    ba += ")";
    return qstrdup(ba.data());
}

template<>
char *toString(const QList<TestEventElement> &ranges)
{
    QByteArray ba = "RangeList(";
    for (int i = 0, max = ranges.size(); i < max; i++) {
        if (i != 0) ba += ", ";
        ba += "[" +
                QByteArray::number(ranges[i].start) +
                ":" +
                QByteArray::number(ranges[i].end) +
                "] = 0x" +
                QByteArray::number(ranges[i].code, 16);
    }
    ba += ")";
    return qstrdup(ba.data());
}

}

QDataStream &operator<<(QDataStream &stream, const TestRangeElement &value)
{
    stream << value.start << value.end << value.code;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, TestRangeElement &value)
{
    stream >> value.start >> value.end >> value.code;
    return stream;
}

class TestRange : public QObject {
Q_OBJECT
private slots:

    void compare()
    {
        QVERIFY(Range::compareStart(12.0, 12.0) == 0);
        QVERIFY(Range::compareStart(12.0, 13.0) < 0);
        QVERIFY(Range::compareStart(13.0, 12.0) > 0);
        QVERIFY(Range::compareStart(FP::undefined(), FP::undefined()) == 0);
        QVERIFY(Range::compareStart(FP::undefined(), 13.0) < 0);
        QVERIFY(Range::compareStart(13.0, FP::undefined()) > 0);

        QVERIFY(Range::compareEnd(12.0, 12.0) == 0);
        QVERIFY(Range::compareEnd(12.0, 13.0) < 0);
        QVERIFY(Range::compareEnd(13.0, 12.0) > 0);
        QVERIFY(Range::compareEnd(FP::undefined(), FP::undefined()) == 0);
        QVERIFY(Range::compareEnd(FP::undefined(), 13.0) > 0);
        QVERIFY(Range::compareEnd(13.0, FP::undefined()) < 0);

        QVERIFY(Range::compareStartEnd(12.0, 12.0) == 0);
        QVERIFY(Range::compareStartEnd(13.0, 12.0) > 0);
        QVERIFY(Range::compareStartEnd(12.0, 13.0) < 0);
        QVERIFY(Range::compareStartEnd(FP::undefined(), FP::undefined()) < 0);
        QVERIFY(Range::compareStartEnd(FP::undefined(), 13.0) < 0);
        QVERIFY(Range::compareStartEnd(13.0, FP::undefined()) < 0);
    }

    void intersects()
    {
        QVERIFY(Range::intersects(1.0, 2.0, 1.0, 2.0));

        QVERIFY(Range::intersects(1.5, 2.0, 1.0, 2.0));
        QVERIFY(Range::intersects(1.0, 2.0, 1.5, 2.0));
        QVERIFY(Range::intersects(1.0, 1.5, 1.0, 2.0));
        QVERIFY(Range::intersects(1.0, 2.0, 1.0, 1.5));
        QVERIFY(Range::intersects(1.0, 3.0, 1.0, 2.0));
        QVERIFY(Range::intersects(1.0, 2.0, 1.0, 3.0));
        QVERIFY(Range::intersects(0.5, 2.0, 1.0, 2.0));
        QVERIFY(Range::intersects(1.0, 2.0, 0.5, 2.0));

        QVERIFY(Range::intersects(1.5, 3.0, 1.0, 2.0));
        QVERIFY(Range::intersects(1.0, 2.0, 1.5, 3.0));
        QVERIFY(Range::intersects(0.5, 1.5, 1.0, 2.0));
        QVERIFY(Range::intersects(1.0, 2.0, 0.5, 1.5));

        QVERIFY(!Range::intersects(1.0, 2.0, 2.0, 3.0));
        QVERIFY(!Range::intersects(2.0, 3.0, 1.0, 2.0));
        QVERIFY(!Range::intersects(1.0, 2.0, 3.0, 4.0));
        QVERIFY(!Range::intersects(3.0, 4.0, 1.0, 2.0));
    }

    void notWithin()
    {
        QVERIFY(Range::notWithinStart(1.0, 2.0, 3.0, true, true) < 0);
        QVERIFY(Range::notWithinStart(2.0, 2.0, 3.0, true, true) == 0);
        QVERIFY(Range::notWithinStart(2.0, 2.0, 3.0, false, true) < 0);
        QVERIFY(Range::notWithinStart(4.0, 2.0, 3.0, true, true) > 0);
        QVERIFY(Range::notWithinStart(3.0, 2.0, 3.0, true, true) == 0);
        QVERIFY(Range::notWithinStart(3.0, 2.0, 3.0, true, false) > 0);
        QVERIFY(Range::notWithinStart(FP::undefined(), 2.0, 3.0, true, true) < 0);
        QVERIFY(Range::notWithinStart(FP::undefined(), FP::undefined(), 3.0, true, true) == 0);

        QVERIFY(Range::notWithinEnd(1.0, 2.0, 3.0, true, true) < 0);
        QVERIFY(Range::notWithinEnd(2.0, 2.0, 3.0, true, true) == 0);
        QVERIFY(Range::notWithinEnd(2.0, 2.0, 3.0, false, true) < 0);
        QVERIFY(Range::notWithinEnd(4.0, 2.0, 3.0, true, true) > 0);
        QVERIFY(Range::notWithinEnd(3.0, 2.0, 3.0, true, true) == 0);
        QVERIFY(Range::notWithinEnd(3.0, 2.0, 3.0, true, false) > 0);
        QVERIFY(Range::notWithinEnd(FP::undefined(), 2.0, 3.0, true, true) > 0);
        QVERIFY(Range::notWithinEnd(FP::undefined(), 2.0, FP::undefined(), true, true) == 0);
    }

    void findLowerBound()
    {
        QFETCH(QList<TestRangeElement>, input);
        QFETCH(double, start);
        QFETCH(int, expected);

        QList<TestRangeElement>::const_iterator result = Range::findLowerBound(input, start);
        if (expected == -1) {
            QVERIFY(result == input.constEnd());
        } else {
            QCOMPARE((result - input.constBegin()), expected);
        }
    }

    void findLowerBound_data()
    {
        QTest::addColumn<QList<TestRangeElement> >("input");
        QTest::addColumn<double>("start");
        QTest::addColumn<int>("expected");

        QList<TestRangeElement> list;

        QTest::newRow("Empty") << list << 12.0 << -1;
        QTest::newRow("Empty infinite") << list << FP::undefined() << -1;

        list << TestRangeElement(5.0, 6.0);
        QTest::newRow("One before infinite") << list << FP::undefined() << 0;
        QTest::newRow("One after") << list << 12.0 << -1;
        QTest::newRow("One before all") << list << 1.0 << 0;
        QTest::newRow("One exact end") << list << 5.0 << 0;
        QTest::newRow("One after") << list << 5.5 << -1;

        list << TestRangeElement(7.0, 8.0);
        QTest::newRow("Two before infinite") << list << FP::undefined() << 0;
        QTest::newRow("Two after") << list << 12.0 << -1;
        QTest::newRow("Two before all") << list << 1.0 << 0;
        QTest::newRow("Two exact first") << list << 5.0 << 0;
        QTest::newRow("Two after first") << list << 5.5 << 0;
        QTest::newRow("Two after first end") << list << 6.5 << 0;
        QTest::newRow("Two exact end") << list << 7.0 << 1;

        list << TestRangeElement(8.0, 9.0);
        QTest::newRow("Three before infinite") << list << FP::undefined() << 0;
        QTest::newRow("Three after") << list << 12.0 << -1;
        QTest::newRow("Three before all") << list << 1.0 << 0;
        QTest::newRow("Three exact first") << list << 5.0 << 0;
        QTest::newRow("Three after first") << list << 5.5 << 0;
        QTest::newRow("Three after first end") << list << 6.5 << 0;
        QTest::newRow("Three exact second") << list << 7.0 << 1;
        QTest::newRow("Three after second") << list << 7.5 << 1;
        QTest::newRow("Three exact end") << list << 8.0 << 2;

        list << TestRangeElement(9.0, 10.0);
        QTest::newRow("Four before infinite") << list << FP::undefined() << 0;
        QTest::newRow("Four after") << list << 12.0 << -1;
        QTest::newRow("Four before all") << list << 1.0 << 0;
        QTest::newRow("Four exact first") << list << 5.0 << 0;
        QTest::newRow("Four after first") << list << 5.5 << 0;
        QTest::newRow("Four after first end") << list << 6.5 << 0;
        QTest::newRow("Four exact second") << list << 7.0 << 1;
        QTest::newRow("Four after second") << list << 7.5 << 1;
        QTest::newRow("Four exact third") << list << 8.0 << 2;
        QTest::newRow("Four third middle") << list << 8.5 << 2;
        QTest::newRow("Four exact end") << list << 9.0 << 3;

        list << TestRangeElement(10.0, 11.0);
        QTest::newRow("Five before infinite") << list << FP::undefined() << 0;
        QTest::newRow("Five after") << list << 12.0 << -1;
        QTest::newRow("Five before all") << list << 1.0 << 0;
        QTest::newRow("Five exact first") << list << 5.0 << 0;
        QTest::newRow("Five after first") << list << 5.5 << 0;
        QTest::newRow("Five after first end") << list << 6.5 << 0;
        QTest::newRow("Five exact second") << list << 7.0 << 1;
        QTest::newRow("Five after second") << list << 7.5 << 1;
        QTest::newRow("Five exact third") << list << 8.0 << 2;
        QTest::newRow("Five third middle") << list << 8.5 << 2;
        QTest::newRow("Five exact fourth") << list << 9.0 << 3;
        QTest::newRow("Five fourth middle") << list << 9.5 << 3;
        QTest::newRow("Five exact end") << list << 10.0 << 4;

        list.prepend(TestRangeElement(FP::undefined(), 4.0));
        QTest::newRow("Infinite start before infinite") << list << FP::undefined() << 0;
        QTest::newRow("Infinite start after") << list << 12.0 << -1;
        QTest::newRow("Infinite start before all") << list << 1.0 << 0;
        QTest::newRow("Infinite start exact first") << list << 5.0 << 1;
        QTest::newRow("Infinite start after first") << list << 5.5 << 1;
        QTest::newRow("Infinite start after first end") << list << 6.5 << 1;
        QTest::newRow("Infinite start exact second") << list << 7.0 << 2;
        QTest::newRow("Infinite start after second") << list << 7.5 << 2;
        QTest::newRow("Infinite start exact third") << list << 8.0 << 3;
        QTest::newRow("Infinite start third middle") << list << 8.5 << 3;
        QTest::newRow("Infinite start exact fourth") << list << 9.0 << 4;
        QTest::newRow("Infinite start fourth middle") << list << 9.5 << 4;
        QTest::newRow("Infinite start exact end") << list << 10.0 << 5;

        list << TestRangeElement(12.0, FP::undefined());
        QTest::newRow("Infinite end before infinite") << list << FP::undefined() << 0;
        QTest::newRow("Infinite end before all") << list << 1.0 << 0;
        QTest::newRow("Infinite end exact first") << list << 5.0 << 1;
        QTest::newRow("Infinite end after first") << list << 5.5 << 1;
        QTest::newRow("Infinite end after first end") << list << 6.5 << 1;
        QTest::newRow("Infinite end exact second") << list << 7.0 << 2;
        QTest::newRow("Infinite end after second") << list << 7.5 << 2;
        QTest::newRow("Infinite end exact third") << list << 8.0 << 3;
        QTest::newRow("Infinite end third middle") << list << 8.5 << 3;
        QTest::newRow("Infinite end exact fourth") << list << 9.0 << 4;
        QTest::newRow("Infinite end fourth middle") << list << 9.5 << 4;
        QTest::newRow("Infinite end exact end") << list << 10.0 << 5;
        QTest::newRow("Infinite end after exact") << list << 12.0 << 6;
        QTest::newRow("Infinite end after") << list << 123.0 << -1;
    }

    void intersectShift()
    {
        QList<TestRangeElement> list;

        QVERIFY(!Range::intersectShift(list, 3.0, 4.0));
        QVERIFY(!Range::intersectShift(list, 3.0));

        list << TestRangeElement(2.0, 4.0, 1);
        QVERIFY(!Range::intersectShift(list, 1.0, 1.5));
        QVERIFY(!Range::intersectShift(list, 1.5, 2.0));
        QVERIFY(Range::intersectShift(list, 3.0, 4.0));
        QCOMPARE(list.at(0).code, (unsigned int) 1);
        QVERIFY(!Range::intersectShift(list, 1.0));
        QVERIFY(Range::intersectShift(list, 2.0));
        QVERIFY(Range::intersectShift(list, 3.0));
        QCOMPARE(list.at(0).code, (unsigned int) 1);

        QVERIFY(!Range::intersectShift(list, 4.5, 5.0));
        QCOMPARE(list.size(), 0);
        list << TestRangeElement(2.0, 4.0, 1);
        QVERIFY(!Range::intersectShift(list, 4.0, 5.0));
        QCOMPARE(list.size(), 0);
        list << TestRangeElement(2.0, 4.0, 1);
        QVERIFY(!Range::intersectShift(list, 4.5));
        QCOMPARE(list.size(), 0);
        list << TestRangeElement(2.0, 4.0, 1);
        QVERIFY(!Range::intersectShift(list, 4.0));
        QCOMPARE(list.size(), 0);

        list << TestRangeElement(2.0, 4.0, 1);
        list << TestRangeElement(4.0, 5.0, 2);
        QVERIFY(!Range::intersectShift(list, 1.0, 1.5));
        QVERIFY(!Range::intersectShift(list, 1.5, 2.0));
        QVERIFY(Range::intersectShift(list, 3.0, 4.0));
        QCOMPARE(list.at(0).code, (unsigned int) 1);
        QVERIFY(!Range::intersectShift(list, 1.0));
        QVERIFY(Range::intersectShift(list, 2.0));
        QVERIFY(Range::intersectShift(list, 3.0));
        QCOMPARE(list.at(0).code, (unsigned int) 1);

        QVERIFY(Range::intersectShift(list, 4.1, 4.5));
        QCOMPARE(list.size(), 1);
        QCOMPARE(list.at(0).code, (unsigned int) 2);
        list.prepend(TestRangeElement(2.0, 4.0, 1));
        QVERIFY(Range::intersectShift(list, 4.1));
        QCOMPARE(list.size(), 1);
        QCOMPARE(list.at(0).code, (unsigned int) 2);

        list.clear();
        list << TestRangeElement(2.0, 4.0, 1);
        list << TestRangeElement(4.5, 5.0, 2);
        list << TestRangeElement(6.0, 7.0, 3);
        list << TestRangeElement(7.0, 8.0, 4);
        list << TestRangeElement(9.0, 10.0, 5);
        QVERIFY(!Range::intersectShift(list, 1.0, 1.5));
        QCOMPARE(list.size(), 5);
        QVERIFY(!Range::intersectShift(list, 1.0));
        QCOMPARE(list.size(), 5);
        QVERIFY(!Range::intersectShift(list, 4.1, 4.2));
        QCOMPARE(list.size(), 4);
        QVERIFY(Range::intersectShift(list, 4.5, 4.9));
        QCOMPARE(list.size(), 4);
        QCOMPARE(list.at(0).code, (unsigned int) 2);
        QVERIFY(Range::intersectShift(list, 7.0, 7.9));
        QCOMPARE(list.size(), 2);
        QCOMPARE(list.at(0).code, (unsigned int) 4);
        QVERIFY(!Range::intersectShift(list, 10.0, FP::undefined()));
        QCOMPARE(list.size(), 0);

        list.clear();
        list << TestRangeElement(2.0, 4.0, 1);
        list << TestRangeElement(4.5, 5.0, 2);
        list << TestRangeElement(6.0, 7.0, 3);
        list << TestRangeElement(7.0, 8.0, 4);
        list << TestRangeElement(9.0, 10.0, 5);
        QVERIFY(!Range::intersectShift(list, 1.0));
        QCOMPARE(list.size(), 5);
        QVERIFY(!Range::intersectShift(list, 4.1));
        QCOMPARE(list.size(), 4);
        QVERIFY(Range::intersectShift(list, 4.5));
        QCOMPARE(list.size(), 4);
        QCOMPARE(list.at(0).code, (unsigned int) 2);
        QVERIFY(Range::intersectShift(list, 7.0, 7.9));
        QCOMPARE(list.size(), 2);
        QCOMPARE(list.at(0).code, (unsigned int) 4);
        QVERIFY(!Range::intersectShift(list, 10.0));
        QCOMPARE(list.size(), 0);
    }

    void intersectFindRange()
    {
        QFETCH(QList<TestRangeElement>, input);
        QFETCH(double, start);
        QFETCH(double, end);
        QFETCH(int, expected);
        QFETCH(int, expectedPoint);

        QList<TestRangeElement>::const_iterator result = Range::findIntersecting(input, start, end);
        if (expected == -1) {
            QVERIFY(result == input.constEnd());
        } else {
            QVERIFY(result == (input.constBegin() + expected));
        }

        result = Range::findIntersecting(input, start);
        if (expectedPoint == -1) {
            QVERIFY(result == input.constEnd());
        } else {
            QVERIFY(result == (input.constBegin() + expectedPoint));
        }
    }

    void intersectFindRange_data()
    {
        QTest::addColumn<QList<TestRangeElement> >("input");
        QTest::addColumn<double>("start");
        QTest::addColumn<double>("end");
        QTest::addColumn<int>("expected");
        QTest::addColumn<int>("expectedPoint");

        QList<TestRangeElement> list;

        QTest::newRow("Empty") << list << 3.0 << 4.0 << -1 << -1;

        list << TestRangeElement(2.0, 4.0);
        QTest::newRow("One before") << list << 1.0 << 1.9 << -1 << -1;
        QTest::newRow("One before exact") << list << 1.0 << 2.0 << -1 << -1;
        QTest::newRow("One before infinite") << list << FP::undefined() << 1.9 << -1 << -1;
        QTest::newRow("One first intersect") << list << 1.0 << 2.5 << 0 << -1;
        QTest::newRow("One first exact") << list << 2.0 << 4.0 << 0 << 0;
        QTest::newRow("One first exact start") << list << 2.0 << 3.9 << 0 << 0;
        QTest::newRow("One first exact end") << list << 2.1 << 4.0 << 0 << 0;
        QTest::newRow("One first middle") << list << 2.1 << 3.9 << 0 << 0;
        QTest::newRow("One last intersect") << list << 3.5 << 5.0 << 0 << 0;
        QTest::newRow("One after") << list << 5.0 << 6.0 << -1 << -1;
        QTest::newRow("One after exact") << list << 4.0 << 5.0 << -1 << -1;
        QTest::newRow("One after infinite") << list << 5.0 << FP::undefined() << -1 << -1;

        list << TestRangeElement(4.0, 5.0);
        QTest::newRow("Two before") << list << 1.0 << 1.9 << -1 << -1;
        QTest::newRow("Two before exact") << list << 1.0 << 2.0 << -1 << -1;
        QTest::newRow("Two before infinite") << list << FP::undefined() << 1.9 << -1 << -1;
        QTest::newRow("Two first intersect") << list << 1.0 << 2.5 << 0 << -1;
        QTest::newRow("Two first exact") << list << 2.0 << 4.0 << 0 << 0;
        QTest::newRow("Two first exact start") << list << 2.0 << 3.9 << 0 << 0;
        QTest::newRow("Two first exact end") << list << 2.1 << 4.0 << 0 << 0;
        QTest::newRow("Two first middle") << list << 2.1 << 3.9 << 0 << 0;
        QTest::newRow("Two second exact") << list << 4.0 << 5.0 << 1 << 1;
        QTest::newRow("Two second exact start") << list << 4.0 << 4.9 << 1 << 1;
        QTest::newRow("Two second exact end") << list << 4.1 << 5.0 << 1 << 1;
        QTest::newRow("Two second middle") << list << 4.1 << 4.9 << 1 << 1;
        QTest::newRow("Two last intersect") << list << 4.5 << 6.0 << 1 << 1;
        QTest::newRow("Two after") << list << 5.0 << 6.0 << -1 << -1;
        QTest::newRow("Two after exact") << list << 5.0 << 6.0 << -1 << -1;
        QTest::newRow("Two after infinite") << list << 5.0 << FP::undefined() << -1 << -1;

        list << TestRangeElement(6.0, 7.0);
        QTest::newRow("Three before") << list << 1.0 << 1.9 << -1 << -1;
        QTest::newRow("Three before exact") << list << 1.0 << 2.0 << -1 << -1;
        QTest::newRow("Three before infinite") << list << FP::undefined() << 1.9 << -1 << -1;
        QTest::newRow("Two first intersect") << list << 1.0 << 2.5 << 0 << -1;
        QTest::newRow("Three first exact") << list << 2.0 << 4.0 << 0 << 0;
        QTest::newRow("Three first exact start") << list << 2.0 << 3.9 << 0 << 0;
        QTest::newRow("Three first exact end") << list << 2.1 << 4.0 << 0 << 0;
        QTest::newRow("Three first middle") << list << 2.1 << 3.9 << 0 << 0;
        QTest::newRow("Three second exact") << list << 4.0 << 5.0 << 1 << 1;
        QTest::newRow("Three second exact start") << list << 4.0 << 4.9 << 1 << 1;
        QTest::newRow("Three second exact end") << list << 4.1 << 5.0 << 1 << 1;
        QTest::newRow("Three second middle") << list << 4.1 << 4.9 << 1 << 1;
        QTest::newRow("Three third exact") << list << 6.0 << 7.0 << 2 << 2;
        QTest::newRow("Three third exact start") << list << 6.0 << 6.9 << 2 << 2;
        QTest::newRow("Three third exact end") << list << 6.1 << 7.0 << 2 << 2;
        QTest::newRow("Three third middle") << list << 6.1 << 6.9 << 2 << 2;
        QTest::newRow("Three middle second intersect") << list << 4.5 << 5.5 << 1 << 1;
        QTest::newRow("Three middle third intersect") << list << 5.5 << 6.1 << 2 << -1;
        QTest::newRow("Three middle third miss exact") << list << 5.0 << 6.0 << -1 << -1;
        QTest::newRow("Three middle third miss exact start") << list << 5.0 << 5.9 << -1 << -1;
        QTest::newRow("Three middle third miss exact end") << list << 5.1 << 6.0 << -1 << -1;
        QTest::newRow("Three middle third miss") << list << 5.1 << 5.9 << -1 << -1;
        QTest::newRow("Three last intersect") << list << 6.5 << 8.0 << 2 << 2;
        QTest::newRow("Three after") << list << 8.0 << 9.0 << -1 << -1;
        QTest::newRow("Three after exact") << list << 7.0 << 9.0 << -1 << -1;
        QTest::newRow("Three after infinite") << list << 8.0 << FP::undefined() << -1 << -1;

        list.prepend(TestRangeElement(FP::undefined(), 2.0));
        QTest::newRow("Infinite start hit exact") << list << FP::undefined() << 2.0 << 0 << 0;
        QTest::newRow("Infinite start hit exact start") << list << FP::undefined() << 1.9 << 0 << 0;
        QTest::newRow("Infinite start hit exact end") << list << 1.0 << 2.0 << 0 << 0;
        QTest::newRow("Infinite start hit finite") << list << 1.0 << 1.9 << 0 << 0;
        QTest::newRow("Infinite start first exact") << list << 2.0 << 4.0 << 1 << 1;
        QTest::newRow("Infinite start first exact start") << list << 2.0 << 3.9 << 1 << 1;
        QTest::newRow("Infinite start first exact end") << list << 2.1 << 4.0 << 1 << 1;
        QTest::newRow("Infinite start first middle") << list << 2.1 << 3.9 << 1 << 1;
        QTest::newRow("Infinite start second exact") << list << 4.0 << 5.0 << 2 << 2;
        QTest::newRow("Infinite start second exact start") << list << 4.0 << 4.9 << 2 << 2;
        QTest::newRow("Infinite start second exact end") << list << 4.1 << 5.0 << 2 << 2;
        QTest::newRow("Infinite start second middle") << list << 4.1 << 4.9 << 2 << 2;
        QTest::newRow("Infinite start third exact") << list << 6.0 << 7.0 << 3 << 3;
        QTest::newRow("Infinite start third exact start") << list << 6.0 << 6.9 << 3 << 3;
        QTest::newRow("Infinite start third exact end") << list << 6.1 << 7.0 << 3 << 3;
        QTest::newRow("Infinite start third middle") << list << 6.1 << 6.9 << 3 << 3;
        QTest::newRow("Infinite start middle second intersect") << list << 4.5 << 5.5 << 2 << 2;
        QTest::newRow("Infinite start middle third intersect") << list << 5.5 << 6.1 << 3 << -1;
        QTest::newRow("Infinite start middle third miss exact") << list << 5.0 << 6.0 << -1 << -1;
        QTest::newRow("Infinite start middle third miss exact start") << list << 5.0 << 5.9 << -1
                                                                      << -1;
        QTest::newRow("Infinite start middle third miss exact end") << list << 5.1 << 6.0 << -1
                                                                    << -1;
        QTest::newRow("Infinite start middle third miss") << list << 5.1 << 5.9 << -1 << -1;
        QTest::newRow("Infinite start last intersect") << list << 6.5 << 8.0 << 3 << 3;
        QTest::newRow("Infinite start after") << list << 8.0 << 9.0 << -1 << -1;
        QTest::newRow("Infinite start after exact") << list << 7.0 << 9.0 << -1 << -1;
        QTest::newRow("Infinite start after infinite") << list << 8.0 << FP::undefined() << -1
                                                       << -1;


        list << TestRangeElement(8.0, FP::undefined());
        QTest::newRow("Infinite end hit start exact") << list << FP::undefined() << 2.0 << 0 << 0;
        QTest::newRow("Infinite end hit start exact start") << list << FP::undefined() << 1.9 << 0
                                                            << 0;
        QTest::newRow("Infinite end hit start exact end") << list << 1.0 << 2.0 << 0 << 0;
        QTest::newRow("Infinite end hit start finite") << list << 1.0 << 1.9 << 0 << 0;
        QTest::newRow("Infinite end first exact") << list << 2.0 << 4.0 << 1 << 1;
        QTest::newRow("Infinite end first exact start") << list << 2.0 << 3.9 << 1 << 1;
        QTest::newRow("Infinite end first exact end") << list << 2.1 << 4.0 << 1 << 1;
        QTest::newRow("Infinite end first middle") << list << 2.1 << 3.9 << 1 << 1;
        QTest::newRow("Infinite end second exact") << list << 4.0 << 5.0 << 2 << 2;
        QTest::newRow("Infinite end second exact start") << list << 4.0 << 4.9 << 2 << 2;
        QTest::newRow("Infinite end second exact end") << list << 4.1 << 5.0 << 2 << 2;
        QTest::newRow("Infinite end second middle") << list << 4.1 << 4.9 << 2 << 2;
        QTest::newRow("Infinite end third exact") << list << 6.0 << 7.0 << 3 << 3;
        QTest::newRow("Infinite end third exact start") << list << 6.0 << 6.9 << 3 << 3;
        QTest::newRow("Infinite end third exact end") << list << 6.1 << 7.0 << 3 << 3;
        QTest::newRow("Infinite end third middle") << list << 6.1 << 6.9 << 3 << 3;
        QTest::newRow("Infinite end middle second intersect") << list << 4.5 << 5.5 << 2 << 2;
        QTest::newRow("Infinite end middle third intersect") << list << 5.5 << 6.1 << 3 << -1;
        QTest::newRow("Infinite end middle third miss exact") << list << 5.0 << 6.0 << -1 << -1;
        QTest::newRow("Infinite end middle third miss exact start") << list << 5.0 << 5.9 << -1
                                                                    << -1;
        QTest::newRow("Infinite end middle third miss exact end") << list << 5.1 << 6.0 << -1 << -1;
        QTest::newRow("Infinite end middle third miss") << list << 5.1 << 5.9 << -1 << -1;
        QTest::newRow("Infinite end last intersect exact end") << list << 6.5 << 8.0 << 3 << 3;
        QTest::newRow("Infinite end last intersect") << list << 6.5 << 7.9 << 3 << 3;
        QTest::newRow("Infinite end miss") << list << 7.5 << 7.9 << -1 << -1;
        QTest::newRow("Infinite end miss exact") << list << 7.0 << 8.0 << -1 << -1;
        QTest::newRow("Infinite end miss exact start") << list << 7.0 << 7.9 << -1 << -1;
        QTest::newRow("Infinite end miss exact end") << list << 7.1 << 8.0 << -1 << -1;
        QTest::newRow("Infinite end intersect") << list << 7.5 << 8.5 << 4 << -1;
        QTest::newRow("Infinite end exact end") << list << 8.1 << FP::undefined() << 4 << 4;
        QTest::newRow("Infinite end exact start") << list << 8.0 << 9.0 << 4 << 4;
        QTest::newRow("Infinite end hit") << list << 8.1 << 9.0 << 4 << 4;
    }

    void insertNotOverlapping()
    {
        QFETCH(QList<TestRangeElement>, input);
        QFETCH(QList<TestRangeElement>, expected);
        QFETCH(QList<bool>, status);

        QList<TestRangeElement> result;
        QList<bool> truth;
        for (QList<TestRangeElement>::const_iterator in = input.constBegin(),
                end = input.constEnd(); in != end; ++in) {
            truth.append(Range::insertNotOverlapping(result, *in));
        }

        QCOMPARE(result, expected);
        QCOMPARE(truth, status);
    }

    void insertNotOverlapping_data()
    {
        QTest::addColumn<QList<TestRangeElement> >("input");
        QTest::addColumn<QList<TestRangeElement> >("expected");
        QTest::addColumn<QList<bool> >("status");

        QList<TestRangeElement> input;
        QList<TestRangeElement> expected;
        QList<bool> status;

        QTest::newRow("Empty") << input << expected << status;

        input << TestRangeElement(1.0, 2.0, 0x01);
        expected << TestRangeElement(1.0, 2.0, 0x01);
        status << true;
        QTest::newRow("Single") << input << expected << status;

        input << TestRangeElement(3.0, 4.0, 0x02);
        expected << TestRangeElement(3.0, 4.0, 0x02);
        status << true;
        QTest::newRow("Two non-overlapping") << input << expected << status;

        input << TestRangeElement(4.0, 5.0, 0x04);
        expected << TestRangeElement(4.0, 5.0, 0x04);
        status << true;
        QTest::newRow("Three non-overlapping") << input << expected << status;

        input << TestRangeElement(4.5, 5.0, 0x08);
        status << false;
        QTest::newRow("Tail intersection") << input << expected << status;

        input << TestRangeElement(5.0, FP::undefined(), 0x10);
        expected << TestRangeElement(5.0, FP::undefined(), 0x10);
        status << true;
        QTest::newRow("Infinite tail") << input << expected << status;

        input << TestRangeElement(7.0, 9.0, 0x20);
        status << false;
        QTest::newRow("Infinite tail intersection") << input << expected << status;

        input.clear();
        expected.clear();
        status.clear();
        input << TestRangeElement(1.0, 2.0, 0x01);
        input << TestRangeElement(FP::undefined(), FP::undefined(), 0x02);
        expected << TestRangeElement(1.0, 2.0, 0x01);
        status << true << false;
        QTest::newRow("Infinite overlay") << input << expected << status;

        input.clear();
        expected.clear();
        status.clear();
        input << TestRangeElement(5.0, 6.0, 0x01);
        input << TestRangeElement(1.0, 2.0, 0x02);
        expected << TestRangeElement(1.0, 2.0, 0x02);
        expected << TestRangeElement(5.0, 6.0, 0x01);
        status << true;
        status << true;
        QTest::newRow("Before add") << input << expected << status;
        input << TestRangeElement(5.0, 6.0, 0x04);
        status << false;
        QTest::newRow("Tail duplicate") << input << expected << status;

        input << TestRangeElement(5.1, 5.9, 0x08);
        status << false;
        QTest::newRow("Tail contained overlap") << input << expected << status;


        input.clear();
        expected.clear();
        status.clear();
        input << TestRangeElement(3.0, 4.0, 0x01);
        input << TestRangeElement(5.0, 6.0, 0x02);
        input << TestRangeElement(4.5, 6.0, 0x04);
        expected << TestRangeElement(3.0, 4.0, 0x01);
        expected << TestRangeElement(5.0, 6.0, 0x02);
        status << true;
        status << true;
        status << false;
        QTest::newRow("Middle start intersection") << input << expected << status;

        input.clear();
        expected.clear();
        status.clear();
        input << TestRangeElement(1.0, 2.0, 0x01);
        input << TestRangeElement(5.0, 6.0, 0x02);
        input << TestRangeElement(3.0, 4.0, 0x04);
        expected << TestRangeElement(1.0, 2.0, 0x01);
        expected << TestRangeElement(3.0, 4.0, 0x04);
        expected << TestRangeElement(5.0, 6.0, 0x02);
        status << true;
        status << true;
        status << true;
        QTest::newRow("Middle insert") << input << expected << status;

        input.clear();
        expected.clear();
        status.clear();
        input << TestRangeElement(1.0, 2.0, 0x01);
        input << TestRangeElement(5.0, 6.0, 0x02);
        input << TestRangeElement(1.5, 1.9, 0x04);
        expected << TestRangeElement(1.0, 2.0, 0x01);
        expected << TestRangeElement(5.0, 6.0, 0x02);
        status << true;
        status << true;
        status << false;
        QTest::newRow("Start contained") << input << expected << status;

        input.clear();
        expected.clear();
        status.clear();
        input << TestRangeElement(1.0, 2.0, 0x01);
        input << TestRangeElement(5.0, 6.0, 0x02);
        input << TestRangeElement(1.5, 2.0, 0x04);
        expected << TestRangeElement(1.0, 2.0, 0x01);
        expected << TestRangeElement(5.0, 6.0, 0x02);
        status << true;
        status << true;
        status << false;
        QTest::newRow("Start contained no tail") << input << expected << status;

        input.clear();
        expected.clear();
        status.clear();
        input << TestRangeElement(1.0, 2.0, 0x01);
        input << TestRangeElement(5.0, 6.0, 0x02);
        input << TestRangeElement(1.5, 3.0, 0x04);
        expected << TestRangeElement(1.0, 2.0, 0x01);
        expected << TestRangeElement(5.0, 6.0, 0x02);
        status << true;
        status << true;
        status << false;
        QTest::newRow("Start contained tail") << input << expected << status;

        input.clear();
        expected.clear();
        status.clear();
        input << TestRangeElement(1.0, 2.0, 0x01);
        input << TestRangeElement(5.0, 6.0, 0x02);
        input << TestRangeElement(2.0, 3.0, 0x04);
        expected << TestRangeElement(1.0, 2.0, 0x01);
        expected << TestRangeElement(2.0, 3.0, 0x04);
        expected << TestRangeElement(5.0, 6.0, 0x02);
        status << true;
        status << true;
        status << true;
        QTest::newRow("Start contained tail end exact") << input << expected << status;

        input.clear();
        expected.clear();
        status.clear();
        input << TestRangeElement(1.0, 2.0, 0x01);
        input << TestRangeElement(5.0, 6.0, 0x02);
        input << TestRangeElement(1.5, 5.5, 0x04);
        expected << TestRangeElement(1.0, 2.0, 0x01);
        expected << TestRangeElement(5.0, 6.0, 0x02);
        status << true;
        status << true;
        status << false;
        QTest::newRow("Start contained tail overlap") << input << expected << status;

        input.clear();
        expected.clear();
        status.clear();
        input << TestRangeElement(1.0, 2.0, 0x01);
        input << TestRangeElement(3.0, 4.0, 0x02);
        input << TestRangeElement(5.0, 6.0, 0x04);
        input << TestRangeElement(8.0, 9.0, 0x08);
        input << TestRangeElement(1.0, 9.0, 0x10);
        expected << TestRangeElement(1.0, 2.0, 0x01);
        expected << TestRangeElement(3.0, 4.0, 0x02);
        expected << TestRangeElement(5.0, 6.0, 0x04);
        expected << TestRangeElement(8.0, 9.0, 0x08);
        status << true;
        status << true;
        status << true;
        status << true;
        status << false;
        QTest::newRow("Middle gap intersect") << input << expected << status;

        input.clear();
        expected.clear();
        status.clear();
        input << TestRangeElement(2.0, 3.0, 0x01);
        input << TestRangeElement(1.0, 2.1, 0x02);
        expected << TestRangeElement(2.0, 3.0, 0x01);
        status << true;
        status << false;
        QTest::newRow("Start fill tail intersect") << input << expected << status;
    }

    void fragmentingOverlay()
    {
        QFETCH(QList<TestRangeElement>, input);
        QFETCH(QList<TestRangeElement>, expected);

        QList<TestRangeElement> result;
        for (QList<TestRangeElement>::const_iterator in = input.constBegin(),
                end = input.constEnd(); in != end; ++in) {
            Range::overlayFragmenting(result, *in);
        }

        QCOMPARE(result, expected);
    }

    void fragmentingOverlay_data()
    {
        QTest::addColumn<QList<TestRangeElement> >("input");
        QTest::addColumn<QList<TestRangeElement> >("expected");

        QList<TestRangeElement> input;
        QList<TestRangeElement> expected;

        QTest::newRow("Empty") << input << expected;

        input << TestRangeElement(1.0, 2.0, 0x01);
        expected << TestRangeElement(1.0, 2.0, 0x01);
        QTest::newRow("Single") << input << expected;

        input << TestRangeElement(3.0, 4.0, 0x02);
        expected << TestRangeElement(3.0, 4.0, 0x02);
        QTest::newRow("Two non-overlapping") << input << expected;

        input << TestRangeElement(4.0, 5.0, 0x04);
        expected << TestRangeElement(4.0, 5.0, 0x04);
        QTest::newRow("Three non-overlapping") << input << expected;

        input << TestRangeElement(4.5, 5.0, 0x08);
        expected.pop_back();
        expected << TestRangeElement(4.0, 4.5, 0x04);
        expected << TestRangeElement(4.5, 5.0, 0x0C);
        QTest::newRow("Tail split") << input << expected;

        input << TestRangeElement(5.0, FP::undefined(), 0x10);
        expected << TestRangeElement(5.0, FP::undefined(), 0x10);
        QTest::newRow("Infinite tail") << input << expected;

        input << TestRangeElement(7.0, 9.0, 0x20);
        expected.pop_back();
        expected << TestRangeElement(5.0, 7.0, 0x10);
        expected << TestRangeElement(7.0, 9.0, 0x30);
        expected << TestRangeElement(9.0, FP::undefined(), 0x10);
        QTest::newRow("Infinite tail split") << input << expected;

        input.clear();
        expected.clear();
        input << TestRangeElement(1.0, 2.0, 0x01);
        input << TestRangeElement(FP::undefined(), FP::undefined(), 0x02);
        expected << TestRangeElement(FP::undefined(), 1.0, 0x02);
        expected << TestRangeElement(1.0, 2.0, 0x03);
        expected << TestRangeElement(2.0, FP::undefined(), 0x02);
        QTest::newRow("Infinite overlay") << input << expected;
        input << TestRangeElement(FP::undefined(), FP::undefined(), 0x04);
        expected[0].code |= 0x04;
        expected[1].code |= 0x04;
        expected[2].code |= 0x04;
        QTest::newRow("Infinite re-overlay") << input << expected;
        input << TestRangeElement(1.0, 2.0, 0x08);
        expected[1].code |= 0x08;
        QTest::newRow("Infinite middle reoverlay") << input << expected;

        input.clear();
        expected.clear();
        input << TestRangeElement(5.0, 6.0, 0x01);
        input << TestRangeElement(1.0, 2.0, 0x02);
        expected << TestRangeElement(1.0, 2.0, 0x02);
        expected << TestRangeElement(5.0, 6.0, 0x01);
        QTest::newRow("Before add") << input << expected;
        input << TestRangeElement(5.0, 6.0, 0x04);
        expected[1].code |= 0x04;
        QTest::newRow("Tail duplicate") << input << expected;

        input << TestRangeElement(5.1, 5.9, 0x08);
        expected.pop_back();
        expected << TestRangeElement(5.0, 5.1, 0x05);
        expected << TestRangeElement(5.1, 5.9, 0x0D);
        expected << TestRangeElement(5.9, 6.0, 0x05);
        QTest::newRow("Tail fragment") << input << expected;

        input << TestRangeElement(5.95, 7.0, 0x10);
        expected.pop_back();
        expected << TestRangeElement(5.9, 5.95, 0x05);
        expected << TestRangeElement(5.95, 6.0, 0x15);
        expected << TestRangeElement(6.0, 7.0, 0x10);
        QTest::newRow("Tail extend") << input << expected;

        input.clear();
        expected.clear();
        input << TestRangeElement(5.0, 6.0, 0x01);
        input << TestRangeElement(4.0, 7.0, 0x02);
        expected << TestRangeElement(4.0, 5.0, 0x02);
        expected << TestRangeElement(5.0, 6.0, 0x03);
        expected << TestRangeElement(6.0, 7.0, 0x02);
        QTest::newRow("Middle end fragment") << input << expected;

        input.clear();
        expected.clear();
        input << TestRangeElement(3.0, 4.0, 0x01);
        input << TestRangeElement(5.0, 6.0, 0x02);
        input << TestRangeElement(4.5, 6.0, 0x04);
        expected << TestRangeElement(3.0, 4.0, 0x01);
        expected << TestRangeElement(4.5, 5.0, 0x04);
        expected << TestRangeElement(5.0, 6.0, 0x06);
        QTest::newRow("Middle start fill") << input << expected;

        input.clear();
        expected.clear();
        input << TestRangeElement(1.0, 2.0, 0x01);
        input << TestRangeElement(5.0, 6.0, 0x02);
        input << TestRangeElement(3.0, 4.0, 0x04);
        expected << TestRangeElement(1.0, 2.0, 0x01);
        expected << TestRangeElement(3.0, 4.0, 0x04);
        expected << TestRangeElement(5.0, 6.0, 0x02);
        QTest::newRow("Middle insert") << input << expected;

        input.clear();
        expected.clear();
        input << TestRangeElement(1.0, 2.0, 0x01);
        input << TestRangeElement(5.0, 6.0, 0x02);
        input << TestRangeElement(1.5, 1.9, 0x04);
        expected << TestRangeElement(1.0, 1.5, 0x01);
        expected << TestRangeElement(1.5, 1.9, 0x05);
        expected << TestRangeElement(1.9, 2.0, 0x01);
        expected << TestRangeElement(5.0, 6.0, 0x02);
        QTest::newRow("Start contained fragment") << input << expected;

        input.clear();
        expected.clear();
        input << TestRangeElement(1.0, 2.0, 0x01);
        input << TestRangeElement(5.0, 6.0, 0x02);
        input << TestRangeElement(1.5, 2.0, 0x04);
        expected << TestRangeElement(1.0, 1.5, 0x01);
        expected << TestRangeElement(1.5, 2.0, 0x05);
        expected << TestRangeElement(5.0, 6.0, 0x02);
        QTest::newRow("Start contained no tail") << input << expected;

        input.clear();
        expected.clear();
        input << TestRangeElement(1.0, 2.0, 0x01);
        input << TestRangeElement(5.0, 6.0, 0x02);
        input << TestRangeElement(1.5, 3.0, 0x04);
        expected << TestRangeElement(1.0, 1.5, 0x01);
        expected << TestRangeElement(1.5, 2.0, 0x05);
        expected << TestRangeElement(2.0, 3.0, 0x04);
        expected << TestRangeElement(5.0, 6.0, 0x02);
        QTest::newRow("Start contained tail") << input << expected;

        input.clear();
        expected.clear();
        input << TestRangeElement(1.0, 2.0, 0x01);
        input << TestRangeElement(5.0, 6.0, 0x02);
        input << TestRangeElement(2.0, 3.0, 0x04);
        expected << TestRangeElement(1.0, 2.0, 0x01);
        expected << TestRangeElement(2.0, 3.0, 0x04);
        expected << TestRangeElement(5.0, 6.0, 0x02);
        QTest::newRow("Start contained tail end exact") << input << expected;

        input.clear();
        expected.clear();
        input << TestRangeElement(1.0, 2.0, 0x01);
        input << TestRangeElement(5.0, 6.0, 0x02);
        input << TestRangeElement(1.5, 5.5, 0x04);
        expected << TestRangeElement(1.0, 1.5, 0x01);
        expected << TestRangeElement(1.5, 2.0, 0x05);
        expected << TestRangeElement(2.0, 5.0, 0x04);
        expected << TestRangeElement(5.0, 5.5, 0x06);
        expected << TestRangeElement(5.5, 6.0, 0x02);
        QTest::newRow("Start contained tail overlap") << input << expected;

        input.clear();
        expected.clear();
        input << TestRangeElement(1.0, 2.0, 0x01);
        input << TestRangeElement(3.0, 4.0, 0x02);
        input << TestRangeElement(5.0, 6.0, 0x04);
        input << TestRangeElement(8.0, 9.0, 0x08);
        input << TestRangeElement(1.0, 9.0, 0x10);
        expected << TestRangeElement(1.0, 2.0, 0x11);
        expected << TestRangeElement(2.0, 3.0, 0x10);
        expected << TestRangeElement(3.0, 4.0, 0x12);
        expected << TestRangeElement(4.0, 5.0, 0x10);
        expected << TestRangeElement(5.0, 6.0, 0x14);
        expected << TestRangeElement(6.0, 8.0, 0x10);
        expected << TestRangeElement(8.0, 9.0, 0x18);
        QTest::newRow("Middle gap fill") << input << expected;

        input.clear();
        expected.clear();
        input << TestRangeElement(2.0, 3.0, 0x01);
        input << TestRangeElement(1.0, 2.1, 0x02);
        expected << TestRangeElement(1.0, 2.0, 0x02);
        expected << TestRangeElement(2.0, 2.1, 0x03);
        expected << TestRangeElement(2.1, 3.0, 0x01);
        QTest::newRow("Start fill tail overlap") << input << expected;

        input = expected;
        input.append(TestRangeElement(1.5, 2.5, 0x04));
        expected.clear();
        expected << TestRangeElement(1.0, 1.5, 0x02);
        expected << TestRangeElement(1.5, 2.0, 0x06);
        expected << TestRangeElement(2.0, 2.1, 0x07);
        expected << TestRangeElement(2.1, 2.5, 0x05);
        expected << TestRangeElement(2.5, 3.0, 0x01);
        QTest::newRow("Tail multiple overlap") << input << expected;

        input.clear();
        input << TestRangeElement(2.0, 3.0, 0x01);
        input << TestRangeElement(1.0, 2.1, 0x02);
        input << TestRangeElement(1.5, 2.5, 0x04);
        QTest::newRow("Start fill tail multiple overlap") << input << expected;

        input.clear();
        input.append(TestRangeElement(2.0, 3.0, 0x01));
        input.append(TestRangeElement(4.0, 5.0, 0x01));
        input.append(TestRangeElement(2.0, 3.1, 0x02));
        expected.clear();
        expected.append(TestRangeElement(2.0, 3.0, 0x03));
        expected.append(TestRangeElement(3.0, 3.1, 0x02));
        expected.append(TestRangeElement(4.0, 5.0, 0x01));
        QTest::newRow("Middle gap extend") << input << expected;
    }

    void eventOverlay()
    {
        QFETCH(bool, merge);
        QFETCH(QList<TestEventElement>, input);
        QFETCH(QList<TestEventElement>, events);
        QFETCH(QList<TestEventElement>, expected);

        QList<TestEventElement> result(input);
        for (QList<TestEventElement>::const_iterator in = events.constBegin(),
                end = events.constEnd(); in != end; ++in) {
            Range::overlayEvent(result, *in, merge);
        }

        QCOMPARE(result, expected);
    }

    void eventOverlay_data()
    {
        QTest::addColumn<bool>("merge");
        QTest::addColumn<QList<TestEventElement> >("input");
        QTest::addColumn<QList<TestEventElement> >("events");
        QTest::addColumn<QList<TestEventElement> >("expected");

        QTest::newRow("Empty") << true << (QList<TestEventElement>()) << (QList<TestEventElement>())
                               << (QList<TestEventElement>());

        QTest::newRow("Single no event") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01)) << (QList<TestEventElement>())
                                         << (QList<TestEventElement>()
                                                 << TestEventElement(1.0, 2.0, 0x01));

        QTest::newRow("Single event before") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01)) << (QList<TestEventElement>()
                << TestEventElement(0.5, 0x02)) << (QList<TestEventElement>()
                << TestEventElement(0.5, 0.5, 0x02) << TestEventElement(1.0, 2.0, 0x01));
        QTest::newRow("Single event after") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01)) << (QList<TestEventElement>()
                << TestEventElement(2.5, 0x02)) << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(2.5, 2.5, 0x02));
        QTest::newRow("Single middle merge") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01)) << (QList<TestEventElement>()
                << TestEventElement(1.5, 0x02)) << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x03));
        QTest::newRow("Single middle fragment") << false << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01)) << (QList<TestEventElement>()
                << TestEventElement(1.5, 0x02)) << (QList<TestEventElement>()
                << TestEventElement(1.0, 1.5, 0x01) << TestEventElement(1.5, 1.5, 0x03)
                << TestEventElement(1.5, 2.0, 0x01));
        QTest::newRow("Single start merge") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01)) << (QList<TestEventElement>()
                << TestEventElement(1.0, 0x02)) << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x03));
        QTest::newRow("Single start fragment") << false << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01)) << (QList<TestEventElement>()
                << TestEventElement(1.0, 0x02)) << (QList<TestEventElement>()
                << TestEventElement(1.0, 1.0, 0x03) << TestEventElement(1.0, 2.0, 0x01));
        QTest::newRow("Single end merge") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01)) << (QList<TestEventElement>()
                << TestEventElement(2.0, 0x02)) << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(2.0, 2.0, 0x02));
        QTest::newRow("Single end fragment") << false << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01)) << (QList<TestEventElement>()
                << TestEventElement(2.0, 0x02)) << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(2.0, 2.0, 0x02));


        QTest::newRow("Single two event before") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01)) << (QList<TestEventElement>()
                << TestEventElement(0.5, 0x02) << TestEventElement(0.5, 0x04))
                                                 << (QList<TestEventElement>()
                                                         << TestEventElement(0.5, 0.5, 0x06)
                                                         << TestEventElement(1.0, 2.0, 0x01));
        QTest::newRow("Single two event after") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01)) << (QList<TestEventElement>()
                << TestEventElement(2.5, 0x02) << TestEventElement(2.5, 0x04))
                                                << (QList<TestEventElement>()
                                                        << TestEventElement(1.0, 2.0, 0x01)
                                                        << TestEventElement(2.5, 2.5, 0x06));
        QTest::newRow("Single two event middle merge") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01)) << (QList<TestEventElement>()
                << TestEventElement(1.5, 0x02) << TestEventElement(1.5, 0x04))
                                                       << (QList<TestEventElement>()
                                                               << TestEventElement(1.0, 2.0, 0x07));
        QTest::newRow("Single two event middle fragment") << false << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01)) << (QList<TestEventElement>()
                << TestEventElement(1.5, 0x02) << TestEventElement(1.5, 0x04))
                                                          << (QList<TestEventElement>()
                                                                  << TestEventElement(1.0, 1.5,
                                                                                      0x01)
                                                                  << TestEventElement(1.5, 1.5,
                                                                                      0x07)
                                                                  << TestEventElement(1.5, 2.0,
                                                                                      0x01));
        QTest::newRow("Single two event start merge") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01)) << (QList<TestEventElement>()
                << TestEventElement(1.0, 0x02) << TestEventElement(1.0, 0x04))
                                                      << (QList<TestEventElement>()
                                                              << TestEventElement(1.0, 2.0, 0x07));
        QTest::newRow("Single two event start fragment") << false << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01)) << (QList<TestEventElement>()
                << TestEventElement(1.0, 0x02) << TestEventElement(1.0, 0x04))
                                                         << (QList<TestEventElement>()
                                                                 << TestEventElement(1.0, 1.0, 0x07)
                                                                 << TestEventElement(1.0, 2.0,
                                                                                     0x01));
        QTest::newRow("Single two event end merge") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01)) << (QList<TestEventElement>()
                << TestEventElement(2.0, 0x02) << TestEventElement(2.0, 0x04))
                                                    << (QList<TestEventElement>()
                                                            << TestEventElement(1.0, 2.0, 0x01)
                                                            << TestEventElement(2.0, 2.0, 0x06));
        QTest::newRow("Single two event end fragment") << false << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01)) << (QList<TestEventElement>()
                << TestEventElement(2.0, 0x02) << TestEventElement(2.0, 0x04))
                                                       << (QList<TestEventElement>()
                                                               << TestEventElement(1.0, 2.0, 0x01)
                                                               << TestEventElement(2.0, 2.0, 0x06));

        QTest::newRow("Two event before") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(2.0, 3.0, 0x02))
                                          << (QList<TestEventElement>()
                                                  << TestEventElement(0.5, 0x04))
                                          << (QList<TestEventElement>()
                                                  << TestEventElement(0.5, 0.5, 0x04)
                                                  << TestEventElement(1.0, 2.0, 0x01)
                                                  << TestEventElement(2.0, 3.0, 0x02));
        QTest::newRow("Two event after") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(2.0, 3.0, 0x02))
                                         << (QList<TestEventElement>()
                                                 << TestEventElement(3.5, 0x04))
                                         << (QList<TestEventElement>()
                                                 << TestEventElement(1.0, 2.0, 0x01)
                                                 << TestEventElement(2.0, 3.0, 0x02)
                                                 << TestEventElement(3.5, 3.5, 0x04));
        QTest::newRow("Two event first merge") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(2.0, 3.0, 0x02))
                                               << (QList<TestEventElement>()
                                                       << TestEventElement(1.5, 0x04))
                                               << (QList<TestEventElement>()
                                                       << TestEventElement(1.0, 2.0, 0x05)
                                                       << TestEventElement(2.0, 3.0, 0x02));
        QTest::newRow("Two event first fragment") << false << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(2.0, 3.0, 0x02))
                                                  << (QList<TestEventElement>()
                                                          << TestEventElement(1.5, 0x04))
                                                  << (QList<TestEventElement>()
                                                          << TestEventElement(1.0, 1.5, 0x01)
                                                          << TestEventElement(1.5, 1.5, 0x05)
                                                          << TestEventElement(1.5, 2.0, 0x01)
                                                          << TestEventElement(2.0, 3.0, 0x02));
        QTest::newRow("Two event second merge") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(2.0, 3.0, 0x02))
                                                << (QList<TestEventElement>()
                                                        << TestEventElement(2.5, 0x04))
                                                << (QList<TestEventElement>()
                                                        << TestEventElement(1.0, 2.0, 0x01)
                                                        << TestEventElement(2.0, 3.0, 0x06));
        QTest::newRow("Two event second fragment") << false << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(2.0, 3.0, 0x02))
                                                   << (QList<TestEventElement>()
                                                           << TestEventElement(2.5, 0x04))
                                                   << (QList<TestEventElement>()
                                                           << TestEventElement(1.0, 2.0, 0x01)
                                                           << TestEventElement(2.0, 2.5, 0x02)
                                                           << TestEventElement(2.5, 2.5, 0x06)
                                                           << TestEventElement(2.5, 3.0, 0x02));
        QTest::newRow("Two event first start merge") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(2.0, 3.0, 0x02))
                                                     << (QList<TestEventElement>()
                                                             << TestEventElement(1.0, 0x04))
                                                     << (QList<TestEventElement>()
                                                             << TestEventElement(1.0, 2.0, 0x05)
                                                             << TestEventElement(2.0, 3.0, 0x02));
        QTest::newRow("Two event first start fragment") << false << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(2.0, 3.0, 0x02))
                                                        << (QList<TestEventElement>()
                                                                << TestEventElement(1.0, 0x04))
                                                        << (QList<TestEventElement>()
                                                                << TestEventElement(1.0, 1.0, 0x05)
                                                                << TestEventElement(1.0, 2.0, 0x01)
                                                                << TestEventElement(2.0, 3.0,
                                                                                    0x02));
        QTest::newRow("Two event second start merge") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(2.0, 3.0, 0x02))
                                                      << (QList<TestEventElement>()
                                                              << TestEventElement(2.0, 0x04))
                                                      << (QList<TestEventElement>()
                                                              << TestEventElement(1.0, 2.0, 0x01)
                                                              << TestEventElement(2.0, 3.0, 0x06));
        QTest::newRow("Two event second start fragment") << false << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(2.0, 3.0, 0x02))
                                                         << (QList<TestEventElement>()
                                                                 << TestEventElement(2.0, 0x04))
                                                         << (QList<TestEventElement>()
                                                                 << TestEventElement(1.0, 2.0, 0x01)
                                                                 << TestEventElement(2.0, 2.0, 0x06)
                                                                 << TestEventElement(2.0, 3.0,
                                                                                     0x02));
        QTest::newRow("Two event second end merge") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(2.0, 3.0, 0x02))
                                                    << (QList<TestEventElement>()
                                                            << TestEventElement(3.0, 0x04))
                                                    << (QList<TestEventElement>()
                                                            << TestEventElement(1.0, 2.0, 0x01)
                                                            << TestEventElement(2.0, 3.0, 0x02)
                                                            << TestEventElement(3.0, 3.0, 0x04));
        QTest::newRow("Two event second end fragment") << false << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(2.0, 3.0, 0x02))
                                                       << (QList<TestEventElement>()
                                                               << TestEventElement(3.0, 0x04))
                                                       << (QList<TestEventElement>()
                                                               << TestEventElement(1.0, 2.0, 0x01)
                                                               << TestEventElement(2.0, 3.0, 0x02)
                                                               << TestEventElement(3.0, 3.0, 0x04));

        QTest::newRow("Gap two event before") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(3.0, 4.0, 0x02))
                                              << (QList<TestEventElement>()
                                                      << TestEventElement(0.5, 0x04))
                                              << (QList<TestEventElement>()
                                                      << TestEventElement(0.5, 0.5, 0x04)
                                                      << TestEventElement(1.0, 2.0, 0x01)
                                                      << TestEventElement(3.0, 4.0, 0x02));
        QTest::newRow("Gap two event after") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(3.0, 4.0, 0x02))
                                             << (QList<TestEventElement>()
                                                     << TestEventElement(4.5, 0x04))
                                             << (QList<TestEventElement>()
                                                     << TestEventElement(1.0, 2.0, 0x01)
                                                     << TestEventElement(3.0, 4.0, 0x02)
                                                     << TestEventElement(4.5, 4.5, 0x04));
        QTest::newRow("Gap two event first merge") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(3.0, 4.0, 0x02))
                                                   << (QList<TestEventElement>()
                                                           << TestEventElement(1.5, 0x04))
                                                   << (QList<TestEventElement>()
                                                           << TestEventElement(1.0, 2.0, 0x05)
                                                           << TestEventElement(3.0, 4.0, 0x02));
        QTest::newRow("Gap two event first fragment") << false << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(3.0, 4.0, 0x02))
                                                      << (QList<TestEventElement>()
                                                              << TestEventElement(1.5, 0x04))
                                                      << (QList<TestEventElement>()
                                                              << TestEventElement(1.0, 1.5, 0x01)
                                                              << TestEventElement(1.5, 1.5, 0x05)
                                                              << TestEventElement(1.5, 2.0, 0x01)
                                                              << TestEventElement(3.0, 4.0, 0x02));
        QTest::newRow("Gap two event second merge") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(3.0, 4.0, 0x02))
                                                    << (QList<TestEventElement>()
                                                            << TestEventElement(3.5, 0x04))
                                                    << (QList<TestEventElement>()
                                                            << TestEventElement(1.0, 2.0, 0x01)
                                                            << TestEventElement(3.0, 4.0, 0x06));
        QTest::newRow("Gap two event second fragment") << false << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(3.0, 4.0, 0x02))
                                                       << (QList<TestEventElement>()
                                                               << TestEventElement(3.5, 0x04))
                                                       << (QList<TestEventElement>()
                                                               << TestEventElement(1.0, 2.0, 0x01)
                                                               << TestEventElement(3.0, 3.5, 0x02)
                                                               << TestEventElement(3.5, 3.5, 0x06)
                                                               << TestEventElement(3.5, 4.0, 0x02));
        QTest::newRow("Gap two event first start merge") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(3.0, 4.0, 0x02))
                                                         << (QList<TestEventElement>()
                                                                 << TestEventElement(1.0, 0x04))
                                                         << (QList<TestEventElement>()
                                                                 << TestEventElement(1.0, 2.0, 0x05)
                                                                 << TestEventElement(3.0, 4.0,
                                                                                     0x02));
        QTest::newRow("Gap two event first start fragment") << false << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(3.0, 4.0, 0x02))
                                                            << (QList<TestEventElement>()
                                                                    << TestEventElement(1.0, 0x04))
                                                            << (QList<TestEventElement>()
                                                                    << TestEventElement(1.0, 1.0,
                                                                                        0x05)
                                                                    << TestEventElement(1.0, 2.0,
                                                                                        0x01)
                                                                    << TestEventElement(3.0, 4.0,
                                                                                        0x02));
        QTest::newRow("Gap two event second start merge") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(3.0, 4.0, 0x02))
                                                          << (QList<TestEventElement>()
                                                                  << TestEventElement(3.0, 0x04))
                                                          << (QList<TestEventElement>()
                                                                  << TestEventElement(1.0, 2.0,
                                                                                      0x01)
                                                                  << TestEventElement(3.0, 4.0,
                                                                                      0x06));
        QTest::newRow("Gap two event second start fragment") << false << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(3.0, 4.0, 0x02))
                                                             << (QList<TestEventElement>()
                                                                     << TestEventElement(3.0, 0x04))
                                                             << (QList<TestEventElement>()
                                                                     << TestEventElement(1.0, 2.0,
                                                                                         0x01)
                                                                     << TestEventElement(3.0, 3.0,
                                                                                         0x06)
                                                                     << TestEventElement(3.0, 4.0,
                                                                                         0x02));
        QTest::newRow("Gap two event second end merge") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(3.0, 4.0, 0x02))
                                                        << (QList<TestEventElement>()
                                                                << TestEventElement(4.0, 0x04))
                                                        << (QList<TestEventElement>()
                                                                << TestEventElement(1.0, 2.0, 0x01)
                                                                << TestEventElement(3.0, 4.0, 0x02)
                                                                << TestEventElement(4.0, 4.0,
                                                                                    0x04));
        QTest::newRow("Gap two event second end fragment") << false << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(3.0, 4.0, 0x02))
                                                           << (QList<TestEventElement>()
                                                                   << TestEventElement(4.0, 0x04))
                                                           << (QList<TestEventElement>()
                                                                   << TestEventElement(1.0, 2.0,
                                                                                       0x01)
                                                                   << TestEventElement(3.0, 4.0,
                                                                                       0x02)
                                                                   << TestEventElement(4.0, 4.0,
                                                                                       0x04));
        QTest::newRow("Gap two event into middle merge") << true << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(3.0, 4.0, 0x02))
                                                         << (QList<TestEventElement>()
                                                                 << TestEventElement(2.5, 0x04))
                                                         << (QList<TestEventElement>()
                                                                 << TestEventElement(1.0, 2.0, 0x01)
                                                                 << TestEventElement(2.5, 2.5, 0x04)
                                                                 << TestEventElement(3.0, 4.0,
                                                                                     0x02));
        QTest::newRow("Gap two event into middle fragment") << false << (QList<TestEventElement>()
                << TestEventElement(1.0, 2.0, 0x01) << TestEventElement(3.0, 4.0, 0x02))
                                                            << (QList<TestEventElement>()
                                                                    << TestEventElement(2.5, 0x04))
                                                            << (QList<TestEventElement>()
                                                                    << TestEventElement(1.0, 2.0,
                                                                                        0x01)
                                                                    << TestEventElement(2.5, 2.5,
                                                                                        0x04)
                                                                    << TestEventElement(3.0, 4.0,
                                                                                        0x02));
    }

    void lastUndefinedStart()
    {
        QFETCH(QList<TestRangeElement>, input);

        QList<TestRangeElement>::const_iterator result = Range::lastUndefinedStart(input);

        QList<TestRangeElement>::const_iterator expected = input.constBegin();
        for (; expected != input.constEnd(); ++expected) {
            if (FP::defined(expected->getStart()))
                break;
        }

        QVERIFY(expected == result);
    }

    void lastUndefinedStart_data()
    {
        QTest::addColumn<QList<TestRangeElement> >("input");

        QTest::newRow("Empty") << QList<TestRangeElement>();
        QTest::newRow("Single undefined")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0));
        QTest::newRow("Two undefined")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0));
        QTest::newRow("Three undefined")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0)
                                              << TestRangeElement(FP::undefined(), 3.0));
        QTest::newRow("Three then defined")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0)
                                              << TestRangeElement(FP::undefined(), 3.0)
                                              << TestRangeElement(3.0, 4.0));
        QTest::newRow("Three then two defined")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0)
                                              << TestRangeElement(FP::undefined(), 3.0)
                                              << TestRangeElement(3.0, 4.0)
                                              << TestRangeElement(4.0, 5.0));
        QTest::newRow("Single defined")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0));
        QTest::newRow("Two defined") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                   << TestRangeElement(2.0, 3.0));
        QTest::newRow("Three defined") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(2.0, 3.0)
                                                                     << TestRangeElement(3.0, 4.0));
    }

    void boundSearch()
    {
        QFETCH(QList<TestRangeElement>, input);
        QFETCH(double, value);

        QList<TestRangeElement>::const_iterator begin = input.constBegin();
        QList<TestRangeElement>::const_iterator end = input.constEnd();
        QList<TestRangeElement>::const_iterator expected = begin;
        for (; expected != end; ++expected) {
            if (!FP::defined(value)) {
                if (FP::defined(expected->getStart()))
                    break;
            } else {
                if (FP::defined(expected->getStart()) && expected->getStart() > value)
                    break;
            }
        }

        QVERIFY(Range::upperBound(begin, end, value) == expected);
        QVERIFY(Range::heuristicUpperBound(begin, end, value) == expected);

        if (FP::defined(value) && (input.isEmpty() || FP::defined(input.first().getStart()))) {
            QVERIFY(Range::upperBoundDefined(begin, end, value) == expected);
            QVERIFY(Range::heuristicUpperBoundDefined(begin, end, value) == expected);
        }


        expected = begin;
        if (FP::defined(value)) {
            for (; expected != end &&
                    (!FP::defined(expected->getStart()) || expected->getStart() < value);
                    ++expected) { }
        }

        QVERIFY(Range::lowerBound(begin, end, value) == expected);
        QVERIFY(Range::heuristicLowerBound(begin, end, value) == expected);

        if (FP::defined(value) && (input.isEmpty() || FP::defined(input.first().getStart()))) {
            QVERIFY(Range::lowerBoundDefined(begin, end, value) == expected);
            QVERIFY(Range::heuristicLowerBoundDefined(begin, end, value) == expected);
        }
    }

    void boundSearch_data()
    {
        QTest::addColumn<QList<TestRangeElement> >("input");
        QTest::addColumn<double>("value");

        QTest::newRow("Empty") << QList<TestRangeElement>() << 1.0;
        QTest::newRow("Single undefined A")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0))
                << FP::undefined();
        QTest::newRow("Single undefined B")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)) << 1.0;
        QTest::newRow("Two undefined A")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0))
                << FP::undefined();
        QTest::newRow("Two undefined B")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0)) << 1.0;
        QTest::newRow("Three undefined A")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0)
                                              << TestRangeElement(FP::undefined(), 3.0))
                << FP::undefined();
        QTest::newRow("Three undefined B")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0)
                                              << TestRangeElement(FP::undefined(), 3.0)) << 1.0;
        QTest::newRow("Three then defined A")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0)
                                              << TestRangeElement(FP::undefined(), 3.0)
                                              << TestRangeElement(3.0, 4.0)) << FP::undefined();
        QTest::newRow("Three then defined B")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0)
                                              << TestRangeElement(FP::undefined(), 3.0)
                                              << TestRangeElement(3.0, 4.0)) << 1.0;
        QTest::newRow("Three then defined C")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0)
                                              << TestRangeElement(FP::undefined(), 3.0)
                                              << TestRangeElement(3.0, 4.0)) << 3.0;
        QTest::newRow("Three then defined D")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0)
                                              << TestRangeElement(FP::undefined(), 3.0)
                                              << TestRangeElement(3.0, 4.0)) << 4.0;
        QTest::newRow("Three then two A")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0)
                                              << TestRangeElement(FP::undefined(), 3.0)
                                              << TestRangeElement(3.0, 4.0)
                                              << TestRangeElement(4.0, 5.0)) << FP::undefined();
        QTest::newRow("Three then two B")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0)
                                              << TestRangeElement(FP::undefined(), 3.0)
                                              << TestRangeElement(3.0, 4.0)
                                              << TestRangeElement(4.0, 5.0)) << 2.0;
        QTest::newRow("Three then two C")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0)
                                              << TestRangeElement(FP::undefined(), 3.0)
                                              << TestRangeElement(3.0, 4.0)
                                              << TestRangeElement(4.0, 5.0)) << 3.0;
        QTest::newRow("Three then two D")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0)
                                              << TestRangeElement(FP::undefined(), 3.0)
                                              << TestRangeElement(3.0, 4.0)
                                              << TestRangeElement(4.0, 5.0)) << 3.5;
        QTest::newRow("Three then two E")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0)
                                              << TestRangeElement(FP::undefined(), 3.0)
                                              << TestRangeElement(3.0, 4.0)
                                              << TestRangeElement(4.0, 5.0)) << 4.0;
        QTest::newRow("Three then two F")
                << (QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 1.0)
                                              << TestRangeElement(FP::undefined(), 2.0)
                                              << TestRangeElement(FP::undefined(), 3.0)
                                              << TestRangeElement(3.0, 4.0)
                                              << TestRangeElement(4.0, 5.0)) << 5.0;
        QTest::newRow("Single defined A")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)) << FP::undefined();
        QTest::newRow("Single defined B")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)) << 0.5;
        QTest::newRow("Single defined C")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)) << 1.0;
        QTest::newRow("Single defined D")
                << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)) << 1.5;
        QTest::newRow("Two defined A") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(2.0, 3.0))
                                       << FP::undefined();
        QTest::newRow("Two defined B") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(2.0, 3.0))
                                       << 0.5;
        QTest::newRow("Two defined C") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(2.0, 3.0))
                                       << 1.0;
        QTest::newRow("Two defined D") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(2.0, 3.0))
                                       << 1.5;
        QTest::newRow("Two defined E") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(2.0, 3.0))
                                       << 2.0;
        QTest::newRow("Two defined F") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(2.0, 3.0))
                                       << 2.5;
        QTest::newRow("Three defined A") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                       << TestRangeElement(2.0, 3.0)
                                                                       << TestRangeElement(3.0,
                                                                                           4.0))
                                         << FP::undefined();
        QTest::newRow("Three defined B") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                       << TestRangeElement(2.0, 3.0)
                                                                       << TestRangeElement(3.0,
                                                                                           4.0))
                                         << 0.5;
        QTest::newRow("Three defined C") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                       << TestRangeElement(2.0, 3.0)
                                                                       << TestRangeElement(3.0,
                                                                                           4.0))
                                         << 1.0;
        QTest::newRow("Three defined D") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                       << TestRangeElement(2.0, 3.0)
                                                                       << TestRangeElement(3.0,
                                                                                           4.0))
                                         << 2.0;
        QTest::newRow("Three defined E") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                       << TestRangeElement(2.0, 3.0)
                                                                       << TestRangeElement(3.0,
                                                                                           4.0))
                                         << 2.5;
        QTest::newRow("Three defined F") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                       << TestRangeElement(2.0, 3.0)
                                                                       << TestRangeElement(3.0,
                                                                                           4.0))
                                         << 3.0;
        QTest::newRow("Three defined G") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                       << TestRangeElement(2.0, 3.0)
                                                                       << TestRangeElement(3.0,
                                                                                           4.0))
                                         << 3.5;
        QTest::newRow("Start two A") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                   << TestRangeElement(1.0, 2.0)
                                                                   << TestRangeElement(2.0, 3.0)
                                                                   << TestRangeElement(3.0, 4.0))
                                     << FP::undefined();
        QTest::newRow("Start two B") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                   << TestRangeElement(1.0, 2.0)
                                                                   << TestRangeElement(2.0, 3.0)
                                                                   << TestRangeElement(3.0, 4.0))
                                     << 0.5;
        QTest::newRow("Start two C") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                   << TestRangeElement(1.0, 2.0)
                                                                   << TestRangeElement(2.0, 3.0)
                                                                   << TestRangeElement(3.0, 4.0))
                                     << 1.0;
        QTest::newRow("Start two D") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                   << TestRangeElement(1.0, 2.0)
                                                                   << TestRangeElement(2.0, 3.0)
                                                                   << TestRangeElement(3.0, 4.0))
                                     << 1.5;
        QTest::newRow("Start two E") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                   << TestRangeElement(1.0, 2.0)
                                                                   << TestRangeElement(2.0, 3.0)
                                                                   << TestRangeElement(3.0, 4.0))
                                     << 2.0;
        QTest::newRow("Start three A") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(2.0, 3.0)
                                                                     << TestRangeElement(3.0, 4.0))
                                       << FP::undefined();
        QTest::newRow("Start three B") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(2.0, 3.0)
                                                                     << TestRangeElement(3.0, 4.0))
                                       << 0.5;
        QTest::newRow("Start three C") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(2.0, 3.0)
                                                                     << TestRangeElement(3.0, 4.0))
                                       << 1.0;
        QTest::newRow("Start three D") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(2.0, 3.0)
                                                                     << TestRangeElement(3.0, 4.0))
                                       << 1.5;
        QTest::newRow("Start three E") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(1.0, 2.0)
                                                                     << TestRangeElement(2.0, 3.0)
                                                                     << TestRangeElement(3.0, 4.0))
                                       << 2.0;
        QTest::newRow("End two A") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                 << TestRangeElement(2.0, 3.0)
                                                                 << TestRangeElement(3.0, 4.0)
                                                                 << TestRangeElement(3.0, 4.0))
                                   << FP::undefined();
        QTest::newRow("End two B") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                 << TestRangeElement(2.0, 3.0)
                                                                 << TestRangeElement(3.0, 4.0)
                                                                 << TestRangeElement(3.0, 4.0))
                                   << 0.5;
        QTest::newRow("End two C") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                 << TestRangeElement(2.0, 3.0)
                                                                 << TestRangeElement(3.0, 4.0)
                                                                 << TestRangeElement(3.0, 4.0))
                                   << 1.0;
        QTest::newRow("End two D") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                 << TestRangeElement(2.0, 3.0)
                                                                 << TestRangeElement(3.0, 4.0)
                                                                 << TestRangeElement(3.0, 4.0))
                                   << 2.5;
        QTest::newRow("End two E") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                 << TestRangeElement(2.0, 3.0)
                                                                 << TestRangeElement(3.0, 4.0)
                                                                 << TestRangeElement(3.0, 4.0))
                                   << 3.0;
        QTest::newRow("End two F") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                 << TestRangeElement(2.0, 3.0)
                                                                 << TestRangeElement(3.0, 4.0)
                                                                 << TestRangeElement(3.0, 4.0))
                                   << 3.5;
        QTest::newRow("End three A") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                   << TestRangeElement(2.0, 3.0)
                                                                   << TestRangeElement(3.0, 4.0)
                                                                   << TestRangeElement(3.0, 4.0)
                                                                   << TestRangeElement(3.0, 4.0))
                                     << FP::undefined();
        QTest::newRow("End three B") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                   << TestRangeElement(2.0, 3.0)
                                                                   << TestRangeElement(3.0, 4.0)
                                                                   << TestRangeElement(3.0, 4.0)
                                                                   << TestRangeElement(3.0, 4.0))
                                     << 0.5;
        QTest::newRow("End three C") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                   << TestRangeElement(2.0, 3.0)
                                                                   << TestRangeElement(3.0, 4.0)
                                                                   << TestRangeElement(3.0, 4.0)
                                                                   << TestRangeElement(3.0, 4.0))
                                     << 1.0;
        QTest::newRow("End three D") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                   << TestRangeElement(2.0, 3.0)
                                                                   << TestRangeElement(3.0, 4.0)
                                                                   << TestRangeElement(3.0, 4.0)
                                                                   << TestRangeElement(3.0, 4.0))
                                     << 2.5;
        QTest::newRow("End three E") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                   << TestRangeElement(2.0, 3.0)
                                                                   << TestRangeElement(3.0, 4.0)
                                                                   << TestRangeElement(3.0, 4.0)
                                                                   << TestRangeElement(3.0, 4.0))
                                     << 3.0;
        QTest::newRow("End three F") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                   << TestRangeElement(2.0, 3.0)
                                                                   << TestRangeElement(3.0, 4.0)
                                                                   << TestRangeElement(3.0, 4.0)
                                                                   << TestRangeElement(3.0, 4.0))
                                     << 3.5;
        QTest::newRow("Middle two A") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                    << TestRangeElement(2.0, 3.0)
                                                                    << TestRangeElement(2.0, 3.0)
                                                                    << TestRangeElement(3.0, 4.0))
                                      << 1.5;
        QTest::newRow("Middle two B") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                    << TestRangeElement(2.0, 3.0)
                                                                    << TestRangeElement(2.0, 3.0)
                                                                    << TestRangeElement(3.0, 4.0))
                                      << 2.0;
        QTest::newRow("Middle two C") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                    << TestRangeElement(2.0, 3.0)
                                                                    << TestRangeElement(2.0, 3.0)
                                                                    << TestRangeElement(3.0, 4.0))
                                      << 2.5;
        QTest::newRow("Middle three A") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                      << TestRangeElement(2.0, 3.0)
                                                                      << TestRangeElement(2.0, 3.0)
                                                                      << TestRangeElement(2.0, 3.0)
                                                                      << TestRangeElement(3.0, 4.0))
                                        << 1.5;
        QTest::newRow("Middle three B") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                      << TestRangeElement(2.0, 3.0)
                                                                      << TestRangeElement(2.0, 3.0)
                                                                      << TestRangeElement(2.0, 3.0)
                                                                      << TestRangeElement(3.0, 4.0))
                                        << 2.0;
        QTest::newRow("Middle three C") << (QList<TestRangeElement>() << TestRangeElement(1.0, 2.0)
                                                                      << TestRangeElement(2.0, 3.0)
                                                                      << TestRangeElement(2.0, 3.0)
                                                                      << TestRangeElement(2.0, 3.0)
                                                                      << TestRangeElement(3.0, 4.0))
                                        << 2.5;

        QList<TestRangeElement> big;
        for (int i = 0; i < 50.0; i++) {
            if (i == 20 || i == 39) {
                for (int j = 0; j < 3; j++) {
                    big << TestRangeElement(1.0 + i, 2.0 + i);
                }
            }
            big << TestRangeElement(1.0 + i, 2.0 + i);
        }
        QTest::newRow("Big undefined") << big << FP::undefined();
        QTest::newRow("Big before first") << big << 0.5;
        QTest::newRow("Big first") << big << 1.0;
        QTest::newRow("Big after first") << big << 1.5;
        QTest::newRow("Big second") << big << 2.0;
        QTest::newRow("Big after second") << big << 2.5;
        QTest::newRow("Big third") << big << 3.0;
        QTest::newRow("Big after third") << big << 3.5;
        QTest::newRow("Big after last") << big << 51.0;
        QTest::newRow("Big last") << big << 50.0;
        QTest::newRow("Big after second to last") << big << 49.5;
        QTest::newRow("Big second to last") << big << 49.0;
        QTest::newRow("Big after third to last") << big << 48.5;
        QTest::newRow("Big third to last") << big << 48.0;
        QTest::newRow("Big before third to last") << big << 47.5;
        QTest::newRow("Big middle exact A") << big << 15.0;
        QTest::newRow("Big middle exact B") << big << 35.0;
        QTest::newRow("Big middle exact C") << big << 25.0;
        QTest::newRow("Big middle inexact A") << big << 15.5;
        QTest::newRow("Big middle inexact B") << big << 35.5;
        QTest::newRow("Big middle inexact C") << big << 25.5;
    }

    void intersectFindAll()
    {
        QFETCH(QList<TestRangeElement>, input);
        QFETCH(double, rangeStart);
        QFETCH(double, rangeEnd);

        QList<TestRangeElement>::const_iterator begin = input.cbegin();
        QList<TestRangeElement>::const_iterator end = input.cend();
        QList<TestRangeElement>::const_iterator expectedBegin = end;
        QList<TestRangeElement>::const_iterator expectedEnd = end;
        for (auto check = begin; check != end; ++check) {
            if (!Range::intersects(check->getStart(), check->getEnd(), rangeStart, rangeEnd))
                continue;
            if (expectedBegin == end) {
                expectedBegin = check;
            }
            expectedEnd = check + 1;
        }

        auto result = Range::findAllIntersecting(begin, end, rangeStart, rangeEnd);
        if (expectedBegin == expectedEnd) {
            QVERIFY(result.first == result.second);
        } else {
            QVERIFY(result.first == expectedBegin);
            QVERIFY(result.second == expectedEnd);
        }

        result = Range::heuristicFindAllIntersecting(begin, end, rangeStart, rangeEnd);
        if (expectedBegin == expectedEnd) {
            QVERIFY(result.first == result.second);
        } else {
            QVERIFY(result.first == expectedBegin);
            QVERIFY(result.second == expectedEnd);
        }
    }

    void intersectFindAll_data()
    {
        QTest::addColumn<QList<TestRangeElement> >("input");
        QTest::addColumn<double>("rangeStart");
        QTest::addColumn<double>("rangeEnd");

        typedef QList<TestRangeElement> REL;

        QTest::newRow("Empty") << REL() << 100.0 << 200.0;

        QTest::newRow("Single before") << REL{{100.0, 200.0}} << 50.0 << 190.0;
        QTest::newRow("Single before exact") << REL{{100.0, 200.0}} << 50.0 << 200.0;
        QTest::newRow("Single after") << REL{{100.0, 200.0}} << 150.0 << 250.0;
        QTest::newRow("Single before exact") << REL{{100.0, 200.0}} << 150.0 << 200.0;
        QTest::newRow("Single exact") << REL{{100.0, 200.0}} << 200.0 << 200.0;
        QTest::newRow("Single miss before") << REL{{100.0, 200.0}} << 50.0 << 75.0;
        QTest::newRow("Single miss after") << REL{{100.0, 200.0}} << 250.0 << 300.0;
        QTest::newRow("Single infinite start") << REL{{100.0, 200.0}} << FP::undefined() << 150.0;
        QTest::newRow("Single infinite start exact") << REL{{100.0, 200.0}} << FP::undefined()
                                                     << 200.0;
        QTest::newRow("Single infinite start after") << REL{{100.0, 200.0}} << FP::undefined()
                                                     << 250.0;
        QTest::newRow("Single infinite start miss") << REL{{100.0, 200.0}} << FP::undefined()
                                                    << 50.0;
        QTest::newRow("Single infinite start miss exact") << REL{{100.0, 200.0}} << FP::undefined()
                                                          << 100.0;
        QTest::newRow("Single infinite end") << REL{{100.0, 200.0}} << 150.0 << FP::undefined();
        QTest::newRow("Single infinite end exact") << REL{{100.0, 200.0}} << 100.0
                                                   << FP::undefined();
        QTest::newRow("Single infinite end before") << REL{{100.0, 200.0}} << 50.0
                                                    << FP::undefined();
        QTest::newRow("Single infinite end miss") << REL{{100.0, 200.0}} << 250.0
                                                  << FP::undefined();
        QTest::newRow("Single infinite end miss exact") << REL{{100.0, 200.0}} << 200.0
                                                        << FP::undefined();
        QTest::newRow("Single infinite both") << REL{{100.0, 200.0}} << FP::undefined()
                                              << FP::undefined();
        QTest::newRow("Single negative") << REL{{FP::undefined(), 200.0}} << 100.0 << 150.0;
        QTest::newRow("Single negative exact") << REL{{FP::undefined(), 200.0}} << 100.0 << 150.0;
        QTest::newRow("Single negative after") << REL{{FP::undefined(), 200.0}} << 250.0 << 300.0;
        QTest::newRow("Single negative after exact") << REL{{FP::undefined(), 200.0}} << 200.0
                                                     << 300.0;
        QTest::newRow("Single positive") << REL{{100.0, FP::undefined()}} << 150.0 << 200.0;
        QTest::newRow("Single positive exact") << REL{{100.0, FP::undefined()}} << 100.0 << 200.0;
        QTest::newRow("Single positive before") << REL{{100.0, FP::undefined()}} << 50.0 << 75.0;
        QTest::newRow("Single positive before exact") << REL{{100.0, FP::undefined()}} << 50.0
                                                      << 100.0;
        QTest::newRow("Single dual") << REL{{FP::undefined(), FP::undefined()}} << 100.0 << 200.0;
        QTest::newRow("Single dual infinite") << REL{{FP::undefined(), FP::undefined()}}
                                              << FP::undefined() << FP::undefined();

        QTest::newRow("Two before") << REL{{100.0, 200.0},
                                           {200.0, 300.0},} << 50.0 << 250.0;
        QTest::newRow("Two miss before") << REL{{100.0, 200.0},
                                                {200.0, 300.0},} << 50.0 << 75.0;
        QTest::newRow("Two miss after") << REL{{100.0, 200.0},
                                               {200.0, 300.0},} << 400.0 << 500.0;
        QTest::newRow("Two before exact") << REL{{100.0, 200.0},
                                                 {200.0, 300.0},} << 50.0 << 300.0;
        QTest::newRow("Two before first") << REL{{100.0, 200.0},
                                                 {200.0, 300.0},} << 50.0 << 150.0;
        QTest::newRow("Two before first exact") << REL{{100.0, 200.0},
                                                       {200.0, 300.0},} << 50.0 << 200.0;
        QTest::newRow("Two after") << REL{{100.0, 200.0},
                                          {200.0, 300.0},} << 150.0 << 400.0;
        QTest::newRow("Two after exact") << REL{{100.0, 200.0},
                                                {200.0, 300.0},} << 150.0 << 300.0;
        QTest::newRow("Two after second") << REL{{100.0, 200.0},
                                                 {200.0, 300.0},} << 250.0 << 400.0;
        QTest::newRow("Two after second exact") << REL{{100.0, 200.0},
                                                       {200.0, 300.0},} << 250.0 << 300.0;
        QTest::newRow("Two infinite start") << REL{{100.0, 200.0},
                                                   {200.0, 300.0},} << FP::undefined() << 250.0;
        QTest::newRow("Two infinite start single") << REL{{100.0, 200.0},
                                                          {200.0, 300.0},} << FP::undefined()
                                                   << 150.0;
        QTest::newRow("Two infinite start exact") << REL{{100.0, 200.0},
                                                         {200.0, 300.0},} << FP::undefined()
                                                  << 300.0;
        QTest::newRow("Two infinite start exact single") << REL{{100.0, 200.0},
                                                                {200.0, 300.0},} << FP::undefined()
                                                         << 200.0;
        QTest::newRow("Two infinite end") << REL{{100.0, 200.0},
                                                 {200.0, 300.0},} << 150.0 << FP::undefined();
        QTest::newRow("Two infinite end single") << REL{{100.0, 200.0},
                                                        {200.0, 300.0},} << 250.0
                                                 << FP::undefined();
        QTest::newRow("Two infinite end exact") << REL{{100.0, 200.0},
                                                       {200.0, 300.0},} << 100.0 << FP::undefined();
        QTest::newRow("Two infinite end exact single") << REL{{100.0, 200.0},
                                                              {200.0, 300.0},} << 200.0
                                                       << FP::undefined();
        QTest::newRow("Two infinite") << REL{{100.0, 200.0},
                                             {200.0, 300.0},} << FP::undefined() << FP::undefined();
        QTest::newRow("Two middle gap") << REL{{100.0, 200.0},
                                               {300.0, 400.0},} << 210.0 << 290.0;
        QTest::newRow("Two middle gap exact") << REL{{100.0, 200.0},
                                                     {300.0, 400.0},} << 200.0 << 300.0;
        QTest::newRow("Two first unlimited") << REL{{FP::undefined(), 200.0},
                                                    {200.0,           300.0},} << 150.0 << 400.0;
        QTest::newRow("Two first unlimited after") << REL{{FP::undefined(), 200.0},
                                                          {200.0,           300.0},} << 300.0
                                                   << 400.0;
        QTest::newRow("Two first unlimited single") << REL{{FP::undefined(), 200.0},
                                                           {200.0,           300.0},} << 150.0
                                                    << 190.0;
        QTest::newRow("Two first unlimited exact") << REL{{FP::undefined(), 200.0},
                                                          {200.0,           300.0},} << 150.0
                                                   << 300.0;
        QTest::newRow("Two first unlimited exact single") << REL{{FP::undefined(), 200.0},
                                                                 {200.0,           300.0},} << 150.0
                                                          << 200.0;
        QTest::newRow("Two second unlimited") << REL{{100.0, 200.0},
                                                     {200.0, FP::undefined()},} << 150.0 << 400.0;
        QTest::newRow("Two second unlimited before") << REL{{100.0, 200.0},
                                                            {200.0, FP::undefined()},} << 50.0
                                                     << 75.0;
        QTest::newRow("Two second unlimited single") << REL{{100.0, 200.0},
                                                            {200.0, FP::undefined()},} << 250.0
                                                     << 400.0;
        QTest::newRow("Two second unlimited exact") << REL{{100.0, 200.0},
                                                           {200.0, FP::undefined()},} << 100.0
                                                    << 400.0;
        QTest::newRow("Two second unlimited exact single") << REL{{100.0, 200.0},
                                                                  {200.0, FP::undefined()},}
                                                           << 200.0 << 400.0;
        QTest::newRow("Two both unlimited") << REL{{FP::undefined(), 200.0},
                                                   {200.0,           FP::undefined()},} << 100.0
                                            << 400.0;
        QTest::newRow("Two both unlimited single") << REL{{FP::undefined(), 200.0},
                                                          {200.0,           FP::undefined()},}
                                                   << 300.0 << 400.0;
        QTest::newRow("Two both unlimited single exact") << REL{{FP::undefined(), 200.0},
                                                                {200.0,           FP::undefined()},}
                                                         << 100.0 << 200.0;
        QTest::newRow("Two both unlimited gap") << REL{{FP::undefined(), 200.0},
                                                       {300.0,           FP::undefined()},} << 250.0
                                                << 275.0;

        QTest::newRow("Three all") << REL{{100.0, 200.0},
                                          {300.0, 400.0},
                                          {400.0, 500.0},} << 150.0 << 450.0;
        QTest::newRow("Three all outside") << REL{{100.0, 200.0},
                                                  {300.0, 400.0},
                                                  {400.0, 500.0},} << 50.0 << 600.0;
        QTest::newRow("Three all exact") << REL{{100.0, 200.0},
                                                {300.0, 400.0},
                                                {400.0, 500.0},} << 100.0 << 500.0;
        QTest::newRow("Three first") << REL{{100.0, 200.0},
                                            {300.0, 400.0},
                                            {400.0, 500.0},} << 50.0 << 150.0;
        QTest::newRow("Three last") << REL{{100.0, 200.0},
                                           {300.0, 400.0},
                                           {400.0, 500.0},} << 450.0 << 600.0;
        QTest::newRow("Three gap first") << REL{{100.0, 200.0},
                                                {300.0, 400.0},
                                                {400.0, 500.0},} << 250.0 << 275.0;
        QTest::newRow("Three gap second") << REL{{100.0, 200.0},
                                                 {300.0, 400.0},
                                                 {500.0, 600.0},} << 450.0 << 475.0;
        QTest::newRow("Three before") << REL{{100.0, 200.0},
                                             {300.0, 400.0},
                                             {400.0, 500.0},} << 50.0 << 75.0;
        QTest::newRow("Three after") << REL{{100.0, 200.0},
                                            {300.0, 400.0},
                                            {400.0, 500.0},} << 600.0 << 700.0;
    }
};

QTEST_APPLESS_MAIN(TestRange)

#include "range.moc"
