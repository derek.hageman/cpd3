/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QtGlobal>
#include <QTest>
#include <QLocalSocket>
#include <QLocalServer>
#include <QCryptographicHash>
#include <QHostInfo>

#include "core/compression.hxx"
#include "core/number.hxx"

using namespace CPD3;

class TestCompression : public QObject {
Q_OBJECT

    static QByteArray makeCompressibleData()
    {
        QByteArray result;

        QDataStream stream(&result, QIODevice::WriteOnly);
        while (result.size() < 1048576) {
            stream << (quint32) (result.size() ^ 0xDEADBEEF);
            stream << sin(result.size() / 10.0);
            stream << (quint32) 1;
        }

        return result;
    }

    QString uid;

    static QByteArray streamReceive(CompressedStream &rx, CompressedStream &tx, int n)
    {
        QByteArray received;
        received.resize(n);
        int offset = 0;
        int itter = 0;
        while (itter < 5000 && n > 0) {
            if (!rx.waitForReadyRead(100)) {
                ++itter;
                tx.waitForBytesWritten(10);
                continue;
            }
            qint64 nrx = rx.read(received.data() + offset, n);
            if (nrx < 0)
                return QByteArray();
            if (nrx > 0) {
                Q_ASSERT((int) nrx <= n);
                n -= (int) nrx;
                offset += (int) nrx;
            }
        }
        return received;
    }

private slots:

    void initTestCase()
    {
        uid = Random::string();
    }

    void zlib()
    {
        QByteArray data(makeCompressibleData());

        QByteArray compressed;
        bool ok = CompressorZLIB().compress(data, compressed);
        QVERIFY(ok);
        QVERIFY(!compressed.isEmpty());
        QVERIFY(compressed.size() < data.size());

        QByteArray recovered;
        ok = CompressorZLIB().compress(data, recovered, compressed.size() + 1);
        QVERIFY(ok);
        QVERIFY(!recovered.isEmpty());
        QVERIFY(recovered.size() < data.size());

        recovered.clear();
        ok = DecompressorZLIB().decompress(compressed, recovered);
        QVERIFY(ok);
        QCOMPARE(recovered, data);

        recovered.clear();
        ok = DecompressorZLIB().decompress(compressed, recovered, data.size());
        QVERIFY(ok);
        QCOMPARE(recovered, data);
    }

    void blockCompressor()
    {
        BlockDecompressor decompressor;

        Util::ByteArray input(makeCompressibleData());
        Util::ByteArray compressed;
        Util::ByteArray recovered;

        BlockCompressor().compress(input, compressed);
        QVERIFY(compressed.size() < input.size());
        QVERIFY(decompressor.decompress(compressed, recovered));
        QCOMPARE(recovered, input);

        BlockCompressor(BlockCompressor::ZLIB).compress(input, compressed);
        QVERIFY(compressed.size() < input.size());
        QVERIFY(decompressor.decompress(compressed, recovered));
        QCOMPARE(recovered, input);

        BlockCompressor(BlockCompressor::LZ4HC).compress(input, compressed);
        QVERIFY(compressed.size() < input.size());
        QVERIFY(decompressor.decompress(compressed, recovered));
        QCOMPARE(recovered, input);

        input.resize(256);
        BlockCompressor().compress(input, compressed);
        QVERIFY(decompressor.decompress(compressed, recovered));
        QCOMPARE(recovered, input);
    }

    void compressedStream()
    {
        QLocalServer localSocketServer;
        QString socketName(QString("CPD3CompressionTest-%1").arg(uid));
        QLocalServer::removeServer(socketName);
        QVERIFY(localSocketServer.listen(socketName));

        QLocalSocket localSocket;
        localSocket.connectToServer(socketName, QIODevice::ReadWrite | QIODevice::Unbuffered);

        QVERIFY(localSocketServer.waitForNewConnection(30000));
        QLocalSocket *serverSocket = localSocketServer.nextPendingConnection();
        serverSocket->open(QIODevice::ReadWrite | QIODevice::Unbuffered);
        QVERIFY(serverSocket != NULL);

        QVERIFY(localSocket.waitForConnected(30000));

        {
            CompressedStream streamA(&localSocket);
            CompressedStream streamB(serverSocket);

            QByteArray data;
            QByteArray check;

            data = "ABCDEF";
            QCOMPARE((int) streamA.write(data), data.size());
            QVERIFY(streamA.waitForBytesWritten(30000));
            check = streamReceive(streamB, streamA, data.size());
            QCOMPARE(check, data);

            check = "QWERT";
            QCOMPARE((int) streamB.write(data), data.size());
            QCOMPARE((int) streamB.write(check), check.size());
            QVERIFY(streamB.waitForBytesWritten(30000));
            data.append(check);
            check = streamReceive(streamA, streamB, data.size());
            QCOMPARE(check, data);

            data = makeCompressibleData();
            QCOMPARE((int) streamA.write(data), data.size());
            QVERIFY(streamA.flush(30000));
            check = streamReceive(streamB, streamA, data.size());
            QCOMPARE(check, data);

            QEventLoop readyA;
            connect(&streamA, SIGNAL(readyRead()), &readyA, SLOT(quit()));
            QTimer::singleShot(30000, &readyA, SLOT(quit()));
            QCOMPARE((int) streamB.write(data), data.size());
            readyA.exec();
            QVERIFY(streamA.bytesAvailable() > 0);

            data = "QWERTYASDFG";
            QEventLoop readyB;
            connect(&streamB, SIGNAL(readyRead()), &readyB, SLOT(quit()));
            QTimer::singleShot(30000, &readyB, SLOT(quit()));
            QCOMPARE((int) streamA.write(data), data.size());
            readyB.exec();
            QVERIFY(streamB.bytesAvailable() > 0);
        }

        localSocket.close();
        serverSocket->close();
        delete serverSocket;
    }
};

QTEST_MAIN(TestCompression)

#include "compression.moc"
