/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <QtGlobal>
#include <QTest>
#include <QSet>
#include <QHash>

#include "core/util.hxx"

using namespace CPD3;

class TestUtil : public QObject {
Q_OBJECT
private slots:

    void append()
    {
        std::vector<int> output;
        Util::append(std::vector<int>{1}, output);
        Util::append(std::vector<int>{2}, output);
        std::vector<int> input{3};
        Util::append(input, output);
        QCOMPARE(output, (std::vector<int>{1, 2, 3}));

        QList<int> output2;
        Util::append(output, output2);
        Util::append(QList<int>{4}, output2);
        QList<int> input2{5};
        Util::append(input2, output2);
        QCOMPARE(output2, (QList<int>{1, 2, 3, 4, 5}));

        Util::append(std::vector<unsigned int>{4}, output);
        Util::append(input2, output);
        QCOMPARE(output, (std::vector<int>{1, 2, 3, 4, 5}));
    }

    void appendNoCopy()
    {
        std::vector<std::unique_ptr<int>> list;

        {
            std::vector<std::unique_ptr<int>> add;
            add.emplace_back(new int(1));
            add.emplace_back(new int(2));
            Util::append(std::move(add), list);
        }
        {
            std::list<std::unique_ptr<int>> add;
            add.emplace_back(new int(3));
            add.emplace_back(new int(4));
            Util::append(std::move(add), list);
        }

        QCOMPARE(list.size(), static_cast<std::size_t>(4));
        QCOMPARE(*list[0], 1);
        QCOMPARE(*list[1], 2);
        QCOMPARE(*list[2], 3);
        QCOMPARE(*list[3], 4);


        static int counter = 0;
        struct Tracking {
            Tracking() = default;

            Tracking(const Tracking &other)
            {
                ++counter;
            }

            Tracking &operator=(const Tracking &other)
            {
                ++counter;
                return *this;
            }
        };

        QList<Tracking> qListA;
        QList<Tracking> qListB;
        qListA.append(Tracking());
        qListA.append(Tracking());
        int original = counter;
        Util::append(std::move(qListA), qListB);
        QCOMPARE(counter, original);
        QCOMPARE(qListB.size(), 2);

        qListA.clear();
        qListB.clear();
        qListA.append(Tracking());
        original = counter;
        Util::append(qListA, qListB);
        QCOMPARE(counter, original);
        QCOMPARE(qListA.size(), 1);
        QCOMPARE(qListB.size(), 1);
    }

    void mergeSet()
    {
        {
            QSet<int> output;
            Util::merge(QSet<int>{1, 2}, output);
            QSet<int> input{1, 3};
            Util::merge(input, output);
            Util::merge(QSet<unsigned int>{1, 2}, output);
            QCOMPARE(output, (QSet<int>{1, 2, 3}));

            std::unordered_set<int> output2;
            Util::merge(output, output2);
            Util::merge(QSet<int>{4}, output2);
            Util::merge(QSet<unsigned int>{1}, output2);
            QCOMPARE(output2, (std::unordered_set<int>{1, 2, 3, 4}));
        }
        {
            std::unordered_set<int> output;
            Util::merge(std::unordered_set<int>{1, 2}, output);
            std::unordered_set<int> input{1, 3};
            Util::merge(input, output);
            Util::merge(std::unordered_set<unsigned int>{1, 2}, output);
            QCOMPARE(output, (std::unordered_set<int>{1, 2, 3}));

            QSet<int> output2;
            Util::merge(output, output2);
            Util::merge(std::unordered_set<int>{4}, output2);
            Util::merge(std::unordered_set<unsigned int>{1}, output2);
            QCOMPARE(output2, (QSet<int>{1, 2, 3, 4}));
        }
    }

    void mergeMap()
    {
        {
            QHash<int, int> output;
            Util::merge(QHash<int, int>{{1, 10},
                                        {2, 20}}, output);
            QHash<int, int> input{{3, 30},
                                  {4, 40}};
            Util::merge(input, output);
            Util::merge(QHash<int, int>{{1, 10},
                                        {2, 20}}, output);
            QCOMPARE(output, (QHash<int, int>{{1, 10},
                                              {2, 20},
                                              {3, 30},
                                              {4, 40}}));

            std::unordered_map<int, int> output2;
            Util::merge(output, output2);
            Util::merge(QHash<int, int>{{4, 40}}, output2);
            QHash<unsigned int, unsigned int> input2{{1, 10}};
            Util::merge(input2, output2);
            QCOMPARE(output2, (std::unordered_map<int, int>{{1, 10},
                                                            {2, 20},
                                                            {3, 30},
                                                            {4, 40}}));
        }
        {
            std::unordered_map<int, int> output;
            Util::merge(std::unordered_map<int, int>{{1, 10},
                                                     {2, 20}}, output);
            std::unordered_map<int, int> input{{3, 30},
                                               {4, 40}};
            Util::merge(input, output);
            Util::merge(std::unordered_map<unsigned int, unsigned int>{{1, 10},
                                                                       {2, 20}}, output);
            QCOMPARE(output, (std::unordered_map<int, int>{{1, 10},
                                                           {2, 20},
                                                           {3, 30},
                                                           {4, 40}}));

            QHash<int, int> output2;
            Util::merge(output, output2);
            Util::merge(std::unordered_map<int, int>{{4, 40}}, output2);
            std::unordered_map<unsigned int, unsigned int> input2{{1, 10}};
            Util::merge(input2, output2);
            QCOMPARE(output2, (QHash<int, int>{{1, 10},
                                               {2, 20},
                                               {3, 30},
                                               {4, 40}}));
        }
    }

    void mergeMapNoCopy()
    {
        std::unordered_map<int, std::unique_ptr<int>> map;

        {
            std::unordered_map<int, std::unique_ptr<int>> add;
            add.emplace(1, std::unique_ptr<int>(new int(1)));
            add.emplace(2, std::unique_ptr<int>(new int(2)));
            Util::merge(std::move(add), map);
        }
        {
            std::unordered_map<int, std::unique_ptr<int>> add;
            add.emplace(3, std::unique_ptr<int>(new int(3)));
            add.emplace(4, std::unique_ptr<int>(new int(4)));
            Util::merge(std::move(add), map);
        }

        QCOMPARE(map.size(), static_cast<std::size_t>(4));
        QCOMPARE(*map[1], 1);
        QCOMPARE(*map[2], 2);
        QCOMPARE(*map[3], 3);
        QCOMPARE(*map[4], 4);


        static int counter = 0;
        struct Tracking {
            Tracking() = default;

            Tracking(const Tracking &other)
            {
                ++counter;
            }

            Tracking &operator=(const Tracking &other)
            {
                ++counter;
                return *this;
            }
        };

        QHash<int, Tracking> qHashA;
        QHash<int, Tracking> qHashB;
        qHashA.insert(1, Tracking());
        qHashA.insert(2, Tracking());
        int original = counter;
        Util::merge(std::move(qHashA), qHashB);
        QCOMPARE(counter, original);
        QCOMPARE(qHashB.size(), 2);

        qHashA.clear();
        qHashB.clear();
        qHashA.insert(3, Tracking());
        original = counter;
        Util::merge(qHashA, qHashB);
        QCOMPARE(counter, original);
        QCOMPARE(qHashA.size(), 1);
        QCOMPARE(qHashB.size(), 1);
    }

    void explicitUnion()
    {
        struct Tracker {
            int &ctor;
            int &dtor;

            Tracker(int &ctor, int &dtor) : ctor(ctor), dtor(dtor)
            {
                this->ctor++;
            }

            virtual ~Tracker()
            {
                this->dtor++;
            }
        };
        struct Larger : public Tracker {
            int filler;

            Larger(int &ctor, int &dtor) : Tracker(ctor, dtor), filler(1)
            { }

            virtual ~Larger() = default;
        };

        Util::ExplicitUnion<Tracker, Larger> u;

        int ctor1 = 0;
        int dtor1 = 0;
        int ctor2 = 0;
        int dtor2 = 0;

        u.construct<Tracker>(ctor1, dtor1);
        QCOMPARE(ctor1, 1);
        QCOMPARE(dtor1, 0);
        QCOMPARE(&(u.ref<Tracker>().ctor), &ctor1);
        u.destruct<Tracker>();
        QCOMPARE(dtor1, 1);

        u.construct<Tracker>(ctor1, dtor1);
        QCOMPARE(ctor1, 2);
        QCOMPARE(dtor1, 1);
        QCOMPARE(&(u.ref<Tracker>().ctor), &ctor1);
        u.destruct<Tracker>();
        QCOMPARE(dtor1, 2);

        u.construct<Larger>(ctor2, dtor2);
        QCOMPARE(ctor1, 2);
        QCOMPARE(dtor1, 2);
        QCOMPARE(ctor2, 1);
        QCOMPARE(dtor2, 0);
        QCOMPARE(u.ptr<Larger>()->filler, 1);
        u.destruct<Larger>();
        QCOMPARE(dtor2, 1);

        u.construct<Larger>(ctor2, dtor2);
        QCOMPARE(ctor1, 2);
        QCOMPARE(dtor1, 2);
        QCOMPARE(ctor2, 2);
        QCOMPARE(dtor2, 1);
        u.destruct<Tracker>();
        QCOMPARE(ctor1, 2);
        QCOMPARE(dtor1, 2);
        QCOMPARE(ctor2, 2);
        QCOMPARE(dtor2, 2);
    }

    void byteView()
    {
        Util::ByteView view("Abcd", 4);

        QVERIFY(view.string_equal("Abcd"));
        QVERIFY(!view.string_equal("ABcd"));
        QVERIFY(!view.string_equal("zzzz"));
        QVERIFY(!view.string_equal("Abcde"));
        QVERIFY(!view.string_equal("Abc"));

        QVERIFY(view.string_start("Abcd"));
        QVERIFY(view.string_start("Abc"));
        QVERIFY(!view.string_start("ABc"));
        QVERIFY(!view.string_start("Abcde"));
        QVERIFY(!view.string_start("zzzz"));

        QVERIFY(view.string_equal(std::string("Abcd")));
        QVERIFY(!view.string_equal(std::string("ABcd")));
        QVERIFY(!view.string_equal(std::string("zzzz")));
        QVERIFY(!view.string_equal(std::string("Abcde")));
        QVERIFY(!view.string_equal(std::string("Abc")));

        QVERIFY(view.string_start(std::string("Abcd")));
        QVERIFY(view.string_start(std::string("Abc")));
        QVERIFY(!view.string_start(std::string("ABc")));
        QVERIFY(!view.string_start(std::string("Abcde")));
        QVERIFY(!view.string_start(std::string("zzzz")));

        QVERIFY(view.string_equal_insensitive("Abcd"));
        QVERIFY(view.string_equal_insensitive("abcd"));
        QVERIFY(view.string_equal_insensitive("ABcd"));
        QVERIFY(!view.string_equal_insensitive("zzzz"));
        QVERIFY(!view.string_equal_insensitive("Abcde"));
        QVERIFY(!view.string_equal_insensitive("Abc"));

        QVERIFY(view.string_start_insensitive("Abcd"));
        QVERIFY(view.string_start_insensitive("Abc"));
        QVERIFY(view.string_start_insensitive("ABc"));
        QVERIFY(view.string_start_insensitive("ABcd"));
        QVERIFY(!view.string_start_insensitive("Abcde"));
        QVERIFY(!view.string_start_insensitive("zzzz"));

        QVERIFY(view.string_equal_insensitive(std::string("Abcd")));
        QVERIFY(view.string_equal_insensitive(std::string("abcd")));
        QVERIFY(view.string_equal_insensitive(std::string("ABcd")));
        QVERIFY(!view.string_equal_insensitive(std::string("zzzz")));
        QVERIFY(!view.string_equal_insensitive(std::string("Abcde")));
        QVERIFY(!view.string_equal_insensitive(std::string("Abc")));

        QVERIFY(view.string_start_insensitive(std::string("Abcd")));
        QVERIFY(view.string_start_insensitive(std::string("Abc")));
        QVERIFY(view.string_start_insensitive(std::string("ABc")));
        QVERIFY(view.string_start_insensitive(std::string("ABcd")));
        QVERIFY(!view.string_start_insensitive(std::string("Abcde")));
        QVERIFY(!view.string_start_insensitive(std::string("zzzz")));

        Util::ByteView zero;
        QVERIFY(!zero.string_equal("Abcd"));
        QVERIFY(zero.string_equal(""));
        QVERIFY(!zero.string_start("Abcd"));
        QVERIFY(zero.string_start(""));
        QVERIFY(!zero.string_equal(std::string("Abcd")));
        QVERIFY(zero.string_equal(std::string()));
        QVERIFY(!zero.string_start(std::string("Abcd")));
        QVERIFY(zero.string_start(std::string()));
        QVERIFY(!zero.string_equal_insensitive("Abcd"));
        QVERIFY(zero.string_equal_insensitive(""));
        QVERIFY(!zero.string_start_insensitive("Abcd"));
        QVERIFY(zero.string_start_insensitive(""));
        QVERIFY(!zero.string_equal_insensitive(std::string("Abcd")));
        QVERIFY(zero.string_equal_insensitive(std::string()));
        QVERIFY(!zero.string_start_insensitive(std::string("Abcd")));
        QVERIFY(zero.string_start_insensitive(std::string()));
    }

    void stringSplit()
    {
        QCOMPARE(Util::split_string("a string  that splits ", ' '),
                 (std::vector<std::string>{"a", "string", "that", "splits"}));
        QCOMPARE(Util::split_string(" a string  that splits", ' '),
                 (std::vector<std::string>{"a", "string", "that", "splits"}));
        QCOMPARE(Util::split_string("a,string,,that,splits,", ',', true),
                 (std::vector<std::string>{"a", "string", "", "that", "splits", ""}));
        QCOMPARE(Util::split_string(",a,string,,that,splits", ',', true),
                 (std::vector<std::string>{"", "a", "string", "", "that", "splits"}));

        QCOMPARE(Util::split_quoted("a string  that splits "),
                 (std::vector<std::string>{"a", "string", "that", "splits"}));
        QCOMPARE(Util::split_quoted(" a string  that splits"),
                 (std::vector<std::string>{"a", "string", "that", "splits"}));
        QCOMPARE(Util::split_quoted(R"(a 'string ' "that splits " )"),
                 (std::vector<std::string>{"a", "string ", "that splits "}));
        QCOMPARE(Util::split_quoted(R"(\n "es\nc")"), (std::vector<std::string>{"\\n", "es\nc"}));

        QCOMPARE(Util::split_lines("a string\nthat splits\n"),
                 (std::vector<std::string>{"a string", "that splits",}));
        QCOMPARE(Util::split_lines("a string\nthat splits"),
                 (std::vector<std::string>{"a string", "that splits",}));
        QCOMPARE(Util::split_lines("\r\na string\n\rthat splits\n\n"),
                 (std::vector<std::string>{"", "a string", "that splits", "",}));
        QCOMPARE(Util::split_lines("a string\n\nthat splits"),
                 (std::vector<std::string>{"a string", "", "that splits",}));
        QCOMPARE(Util::split_lines("a string"), (std::vector<std::string>{"a string"}));
        QCOMPARE(Util::split_lines(""), (std::vector<std::string>()));
    }
};

QTEST_APPLESS_MAIN(TestUtil)

#include "util.moc"
