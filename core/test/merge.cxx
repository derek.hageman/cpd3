/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>
#include <QString>
#include <QByteArray>
#include <QDataStream>

#include "core/merge.hxx"
#include "core/number.hxx"
#include "core/timeutils.hxx"

using namespace CPD3;

class TestRangeElement : public Time::Bounds {
public:
    unsigned int code;

    TestRangeElement() : Time::Bounds(), code(0)
    { }

    TestRangeElement(double setStart, double setEnd, unsigned int setCode = 0) : Time::Bounds(
            setStart, setEnd), code(setCode)
    {
        Q_ASSERT(!FP::defined(start) || !FP::defined(end) || start != end);
    }

    TestRangeElement(const TestRangeElement &other) : Time::Bounds(other), code(other.code)
    { }

    TestRangeElement &operator=(const TestRangeElement &other)
    {
        if (&other == this) return *this;
        start = other.start;
        end = other.end;
        code = other.code;
        return *this;
    }

    bool operator==(const TestRangeElement &other) const
    {
        return FP::equal(start, other.start) && FP::equal(end, other.end) && code == other.code;
    }
};

QDebug operator<<(QDebug debug, const TestRangeElement &v)
{
    QDebugStateSaver saver(debug);
    debug.nospace() << Logging::range(v.start, v.end) << hex << v.code;
    return debug;
}

Q_DECLARE_METATYPE(TestRangeElement)

Q_DECLARE_METATYPE(QList<TestRangeElement>)

Q_DECLARE_METATYPE(QList<QList<TestRangeElement> >)

namespace QTest {

template<>
char *toString(const TestRangeElement &range)
{
    QByteArray ba = "Range([";
    ba += QByteArray::number(range.start) +
            ":" +
            QByteArray::number(range.end) +
            "] = 0x" +
            QByteArray::number(range.code, 16);
    ba += ")";
    return qstrdup(ba.data());
}

}

QDataStream &operator<<(QDataStream &stream, const TestRangeElement &value)
{
    stream << value.start << value.end << value.code;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, TestRangeElement &value)
{
    stream >> value.start >> value.end >> value.code;
    return stream;
}


static bool deterministicSortCompare(const TestRangeElement &a, const TestRangeElement &b)
{
    int cr = Range::compareStart(a.getStart(), b.getStart());
    if (cr != 0)
        return cr < 0;
    cr = Range::compareEnd(a.getEnd(), b.getEnd());
    if (cr != 0)
        return cr < 0;
    return a.code < b.code;
}

static QList<TestRangeElement> deterministicSort(QList<TestRangeElement> input)
{
    std::sort(input.begin(), input.end(), deterministicSortCompare);
    return input;
}

class TestMerge : public QObject {
Q_OBJECT
private slots:

    void simpleMerge()
    {
        QFETCH(QList<QList<TestRangeElement> >, input);
        QFETCH(QList<TestRangeElement>, expected);

        bool haveUndefined = false;
        for (QList<QList<TestRangeElement>>::const_iterator i = input.constBegin(),
                endI = input.constEnd(); i != endI; ++i) {
            if (i->isEmpty())
                continue;
            if (!FP::defined(i->front().getStart())) {
                haveUndefined = true;
                break;
            }
        }

        typedef std::pair<QList<TestRangeElement>::const_iterator,
                          QList<TestRangeElement>::const_iterator> MergePair;

        {
            std::vector<MergePair> merge;
            for (QList<QList<TestRangeElement>>::const_iterator i = input.constBegin(),
                    endI = input.constEnd(); i != endI; ++i) {
                merge.push_back(MergePair(i->constBegin(), i->constEnd()));
            }
            QList<TestRangeElement> output;
            Merge::nWay(merge, output);
            QCOMPARE(output, expected);

            output.clear();
            Merge::nWaySequential(merge, output);
            QCOMPARE(output, expected);
        }

        if (!haveUndefined) {
            std::vector<MergePair> merge;
            for (QList<QList<TestRangeElement>>::const_iterator i = input.constBegin(),
                    endI = input.constEnd(); i != endI; ++i) {
                merge.push_back(MergePair(i->constBegin(), i->constEnd()));
            }
            QList<TestRangeElement> output;
            Merge::nWayDefined(merge, output);
            QCOMPARE(output, expected);

            output.clear();
            Merge::nWaySequentialDefined(merge, output);
            QCOMPARE(output, expected);
        }
    }

    void simpleMerge_data()
    {
        QTest::addColumn<QList<QList<TestRangeElement> > >("input");
        QTest::addColumn<QList<TestRangeElement> >("expected");

        typedef TestRangeElement E;
        typedef QList<E> EL;
        typedef QList<EL> ELL;

        QTest::newRow("Empty") << (ELL()) << (EL());
        QTest::newRow("One") << (ELL() << (EL() << E(100, 200) << E(200, 300)))
                             << (EL() << E(100, 200) << E(200, 300));
        QTest::newRow("One empty") << (ELL() << (EL())) << (EL());

        QTest::newRow("Two sequential") << (ELL() << (EL() << E(100, 200) << E(200, 300))
                                                  << (EL() << E(300, 400) << E(400, 500)))
                                        << (EL() << E(100, 200) << E(200, 300) << E(300, 400)
                                                 << E(400, 500));

        QTest::newRow("Two interweave") << (ELL() << (EL() << E(100, 200) << E(300, 400))
                                                  << (EL() << E(200, 300) << E(400, 500)))
                                        << (EL() << E(100, 200) << E(200, 300) << E(300, 400)
                                                 << E(400, 500));

        QTest::newRow("Two multiple")
                << (ELL() << (EL() << E(100, 200) << E(300, 400) << E(300, 400))
                          << (EL() << E(200, 300) << E(200, 300) << E(400, 500)))
                << (EL() << E(100, 200) << E(200, 300) << E(200, 300) << E(300, 400) << E(300, 400)
                         << E(400, 500));

        QTest::newRow("Two empty")
                << (ELL() << (EL() << E(100, 200) << E(200, 300) << E(300, 400) << E(400, 500))
                          << (EL()))
                << (EL() << E(100, 200) << E(200, 300) << E(300, 400) << E(400, 500));

        QTest::newRow("Two undefined") << (ELL() << (EL() << E(FP::undefined(), 200) << E(300, 400))
                                                 << (EL() << E(FP::undefined(), 300)
                                                          << E(400, 500)))
                                       << (EL() << E(FP::undefined(), 200)
                                                << E(FP::undefined(), 300) << E(300, 400)
                                                << E(400, 500));

        QTest::newRow("Two undefined multiple") << (ELL()
                << (EL() << E(FP::undefined(), 200) << E(FP::undefined(), 200) << E(300, 400))
                << (EL() << E(FP::undefined(), 300) << E(FP::undefined(), 300) << E(400, 500)))
                                                << (EL() << E(FP::undefined(), 200)
                                                         << E(FP::undefined(), 200)
                                                         << E(FP::undefined(), 300)
                                                         << E(FP::undefined(), 300) << E(300, 400)
                                                         << E(400, 500));

        QTest::newRow("Three mixed") << (ELL() << (EL() << E(100, 200) << E(200, 300))
                                               << (EL() << E(100, 150) << E(300, 400))
                                               << (EL() << E(300, 350) << E(400, 500)))
                                     << (EL() << E(100, 200) << E(100, 150) << E(200, 300)
                                              << E(300, 400) << E(300, 350) << E(400, 500));

        EL result;
        ELL input;
        for (int i = 0; i < 10; i++) {
            input.push_back(EL());
        }
        for (int i = 0; i < 1000; i++) {
            for (int j = 0; j < 10; j++) {
                for (int m = 0; m < ((i % 3) + 1); ++m) {
                    E e(i, i + 1, (unsigned int) j);
                    input[j].push_back(e);
                    result.push_back(e);
                }
            }
        }
        QTest::newRow("Bulk") << input << result;
    }

    void multiplexerEmpty()
    {

        StreamMultiplexer<TestRangeElement> mux;

        auto *simple = mux.createSimple();
        auto *segmented = mux.createSegmented();
        auto *unsorted = mux.createUnsorted();
        auto *placeholder = mux.createPlaceholder();

        mux.inject(QList<TestRangeElement>());
        mux.injectUnsorted(QList<TestRangeElement>());

        QVERIFY(!FP::defined(mux.getCurrentAdvance()));
        QVERIFY(mux.output<QList<TestRangeElement>>().empty());
        QVERIFY(!FP::defined(mux.getCurrentAdvance()));

        mux.creationComplete();

        QVERIFY(mux.output<QList<TestRangeElement>>().empty());

        simple->end();
        segmented->end();
        unsorted->end();
        placeholder->end();

        QVERIFY(mux.output<QList<TestRangeElement>>().empty());

        QVERIFY(mux.isComplete());
    }

    void multiplexerSingle()
    {
        StreamMultiplexer<TestRangeElement> mux;
        auto *simple = mux.createSimple();
        mux.creationComplete();

        QVERIFY(mux.output<QList<TestRangeElement>>().empty());
        QVERIFY(!FP::defined(mux.getCurrentAdvance()));

        QVERIFY(simple->incomingValue(TestRangeElement(100, 200)));

        QCOMPARE(mux.output<QList<TestRangeElement>>(),
                 QList<TestRangeElement>() << TestRangeElement(100, 200));

        QVERIFY(simple->incomingValue(TestRangeElement(200, 300)));
        simple->incoming(QList<TestRangeElement>() << TestRangeElement(300, 400)
                                                   << TestRangeElement(301, 401));

        QCOMPARE(mux.output<QList<TestRangeElement>>(),
                 QList<TestRangeElement>() << TestRangeElement(200, 300)
                                           << TestRangeElement(300, 400)
                                           << TestRangeElement(301, 401));
        QCOMPARE(mux.getCurrentAdvance(), 301.0);

        QVERIFY(simple->advance(500));
        QVERIFY(mux.output<QList<TestRangeElement>>().empty());

        QVERIFY(simple->incoming(QList<TestRangeElement>() << TestRangeElement(500, 600)
                                                           << TestRangeElement(501, 601)));

        simple->end();

        QVERIFY(!mux.isComplete());
        QCOMPARE(mux.output<QList<TestRangeElement>>(),
                 QList<TestRangeElement>() << TestRangeElement(500, 600)
                                           << TestRangeElement(501, 601));


        QVERIFY(mux.output<QList<TestRangeElement>>().empty());
        QVERIFY(mux.isComplete());
    }

    void multiplexerTwo()
    {
        StreamMultiplexer<TestRangeElement> mux;
        auto *a = mux.createSimple();
        auto *b = mux.createSimple();
        mux.creationComplete();

        QVERIFY(a->incomingValue(TestRangeElement(100, 200)));
        QVERIFY(b->incomingValue(TestRangeElement(101, 201)));

        QCOMPARE(mux.output<QList<TestRangeElement>>(),
                 QList<TestRangeElement>() << TestRangeElement(100, 200));
        QVERIFY(!b->advance(101));
        QVERIFY(a->advance(200));
        QCOMPARE(mux.output<QList<TestRangeElement>>(),
                 QList<TestRangeElement>() << TestRangeElement(101, 201));

        QVERIFY(!a->incomingValue(TestRangeElement(200, 300)));
        QVERIFY(mux.output().empty());

        QCOMPARE(mux.getCurrentAdvance(), 101.0);

        b->end();

        QCOMPARE(mux.output<QList<TestRangeElement>>(),
                 QList<TestRangeElement>() << TestRangeElement(200, 300));

        QVERIFY(a->incomingValue(TestRangeElement(400, 500)));

        QCOMPARE(mux.output<QList<TestRangeElement>>(),
                 QList<TestRangeElement>() << TestRangeElement(400, 500));

        QCOMPARE(mux.getCurrentAdvance(), 400.0);

        a->end();

        QVERIFY(mux.output().empty());

        QVERIFY(mux.isComplete());
    }

    void multiplexerInject()
    {
        StreamMultiplexer<TestRangeElement> mux;
        auto *simple = mux.createSimple();
        mux.creationComplete();

        mux.injectUnsorted(QList<TestRangeElement>() << TestRangeElement(100, 200)
                                                     << TestRangeElement(50, 200));
        mux.injectValue(TestRangeElement(150, 200));

        QVERIFY(mux.output().empty());

        QVERIFY(simple->incomingValue(TestRangeElement(FP::undefined(), 400)));

        QCOMPARE(mux.output<QList<TestRangeElement>>(),
                 QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 400));

        mux.injectValueUnsorted(TestRangeElement(FP::undefined(), 200));

        QCOMPARE(mux.output<QList<TestRangeElement>>(),
                 QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 200));

        QVERIFY(mux.output().empty());

        QVERIFY(simple->incomingValue(TestRangeElement(75, 400)));
        simple->advance(125);

        QCOMPARE(mux.output<QList<TestRangeElement>>(),
                 QList<TestRangeElement>() << TestRangeElement(50, 200) << TestRangeElement(75, 400)
                                           << TestRangeElement(100, 200));

        QVERIFY(simple->incomingValue(TestRangeElement(125, 200)));
        QCOMPARE(mux.output<QList<TestRangeElement>>(),
                 QList<TestRangeElement>() << TestRangeElement(125, 200));

        QVERIFY(simple->advance(150));
        QCOMPARE(mux.output<QList<TestRangeElement>>(),
                 QList<TestRangeElement>() << TestRangeElement(150, 200));

        mux.injectValue(TestRangeElement(200, 400));
        QVERIFY(mux.output().empty());

        simple->end();

        QVERIFY(!mux.isComplete());
        QCOMPARE(mux.output<QList<TestRangeElement>>(),
                 QList<TestRangeElement>() << TestRangeElement(200, 400));

        mux.injectValue(TestRangeElement(300, 400));
        QVERIFY(!mux.isComplete());
        QCOMPARE(mux.output<QList<TestRangeElement>>(),
                 QList<TestRangeElement>() << TestRangeElement(300, 400));

        QVERIFY(mux.isComplete());
    }

    void multiplexerEmplace()
    {
        StreamMultiplexer<TestRangeElement, std::vector<TestRangeElement>> mux;
        auto *simple = mux.createSimple();
        auto *seg = mux.createSegmented();
        auto *us = mux.createUnsorted();
        mux.creationComplete();

        simple->emplaceValue(100, 200);
        seg->emplaceValue(200, 300);
        us->emplaceValue(50, 100);

        mux.emplaceValue(300, 400, 0x1);
        mux.emplaceValueUnsorted(400, 500);

        mux.finishAll();

        QVERIFY(!mux.isComplete());

        std::vector<TestRangeElement> expected;
        expected.push_back(TestRangeElement(50, 100));
        expected.push_back(TestRangeElement(100, 200));
        expected.push_back(TestRangeElement(200, 300));
        expected.push_back(TestRangeElement(300, 400, 0x1));
        expected.push_back(TestRangeElement(400, 500));

        QCOMPARE(mux.output<std::vector<TestRangeElement>>(), expected);
    }

    void multiplexerUndefined()
    {
        StreamMultiplexer<TestRangeElement> mux;
        auto *a = mux.createSimple();
        auto *b = mux.createSimple();
        auto *c = mux.createSimple();
        auto *seg = mux.createSegmented();
        auto *us = mux.createUnsorted();
        auto *ph = mux.createPlaceholder();
        mux.creationComplete();

        {
            QList<TestRangeElement> v;
            v << TestRangeElement(FP::undefined(), 100);
            v << TestRangeElement(FP::undefined(), 200);
            v << TestRangeElement(FP::undefined(), 300);
            a->incoming(v.begin(), v.end());
        }

        b->incomingValue(TestRangeElement(FP::undefined(), 400));
        b->incomingValue(TestRangeElement(500, 600));

        c->incomingValue(TestRangeElement(FP::undefined(), 500));

        seg->incomingValue(TestRangeElement(1000, 2000));

        us->incomingValue(TestRangeElement(FP::undefined(), 600));

        QCOMPARE(deterministicSort(mux.output<QList<TestRangeElement>>()),
                 QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 100)
                                           << TestRangeElement(FP::undefined(), 200)
                                           << TestRangeElement(FP::undefined(), 300)
                                           << TestRangeElement(FP::undefined(), 400)
                                           << TestRangeElement(FP::undefined(), 500)
                                           << TestRangeElement(FP::undefined(), 600));


        a->incomingValue(TestRangeElement(FP::undefined(), 100, 0x1));
        a->incomingValue(TestRangeElement(FP::undefined(), 200, 0x1));
        a->incomingValue(TestRangeElement(FP::undefined(), 300, 0x1));

        c->incomingValue(TestRangeElement(FP::undefined(), 400, 0x1));
        c->incomingValue(TestRangeElement(FP::undefined(), 500, 0x1));

        mux.injectValue(TestRangeElement(FP::undefined(), 600, 0x1));
        mux.injectValue(TestRangeElement(2000, 3000));

        QVERIFY(!b->incomingValue(TestRangeElement(701, 800)));
        QVERIFY(!FP::defined(mux.getCurrentAdvance()));

        QCOMPARE(deterministicSort(mux.output<QList<TestRangeElement>>()),
                 QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 100, 0x1)
                                           << TestRangeElement(FP::undefined(), 200, 0x1)
                                           << TestRangeElement(FP::undefined(), 300, 0x1)
                                           << TestRangeElement(FP::undefined(), 400, 0x1)
                                           << TestRangeElement(FP::undefined(), 500, 0x1)
                                           << TestRangeElement(FP::undefined(), 600, 0x1));


        a->incomingValue(TestRangeElement(FP::undefined(), 100, 0x2));
        a->incomingValue(TestRangeElement(FP::undefined(), 200, 0x2));
        a->end();

        QCOMPARE(deterministicSort(mux.output<QList<TestRangeElement>>()),
                 QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 100, 0x2)
                                           << TestRangeElement(FP::undefined(), 200, 0x2));

        QVERIFY(mux.output().empty());

        ph->advance(600);

        QVERIFY(mux.output().empty());

        us->end();

        c->incomingValue(TestRangeElement(FP::undefined(), 100, 0x3));
        c->incomingValue(TestRangeElement(200, 300, 0x3));
        c->advance(600);

        mux.injectValueUnsorted(TestRangeElement(FP::undefined(), 400, 0x3));
        mux.injectValueUnsorted(TestRangeElement(450, 500));

        QCOMPARE(deterministicSort(mux.output<QList<TestRangeElement>>()),
                 QList<TestRangeElement>() << TestRangeElement(FP::undefined(), 100, 0x3)
                                           << TestRangeElement(FP::undefined(), 400, 0x3)
                                           << TestRangeElement(200, 300, 0x3)
                                           << TestRangeElement(450, 500)
                                           << TestRangeElement(500, 600));

        c->end();
        b->advance(4000);
        ph->end();

        QCOMPARE(deterministicSort(mux.output<QList<TestRangeElement>>()),
                 QList<TestRangeElement>() << TestRangeElement(701, 800)
                                           << TestRangeElement(1000, 2000)
                                           << TestRangeElement(2000, 3000));

        mux.finishAll();

        QVERIFY(mux.output().empty());
        QVERIFY(mux.isComplete());
    }

    void multiplexerSerialize()
    {
        StreamMultiplexer<TestRangeElement> mux;
        auto *simple = mux.createSimple();
        auto *seg = mux.createSegmented();
        auto *us = mux.createUnsorted();
        auto *ph = mux.createPlaceholder();

        QVERIFY(!mux.isComplete());

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                mux.serialize(stream);
                simple->serialize(stream);
                seg->serialize(stream);
                us->serialize(stream);
                ph->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                mux.deserialize(stream);
                simple = mux.deserializeSimple(stream);
                seg = mux.deserializeSegmented(stream);
                us = mux.deserializeUnsorted(stream);
                ph = mux.deserializePlaceholder(stream);
            }
        }

        QVERIFY(!mux.isComplete());
        mux.creationComplete();
        QVERIFY(!mux.isComplete());

        mux.injectValue(TestRangeElement(50, 1000));
        mux.injectValue(TestRangeElement(500, 600));

        simple->incomingValue(TestRangeElement(100, 200));
        seg->incomingValue(TestRangeElement(200, 300));
        us->incomingValue(TestRangeElement(300, 400));
        ph->advance(200);

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                mux.serialize(stream);
                simple->serialize(stream);
                seg->serialize(stream);
                us->serialize(stream);
                ph->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                mux.deserialize(stream);
                simple = mux.deserializeSimple(stream);
                seg = mux.deserializeSegmented(stream);
                us = mux.deserializeUnsorted(stream);
                ph = mux.deserializePlaceholder(stream);
            }
        }

        us->advance(200);
        simple->advance(200);

        QVERIFY(!mux.isComplete());
        QCOMPARE(mux.output<QList<TestRangeElement>>(),
                 QList<TestRangeElement>() << TestRangeElement(50, 1000)
                                           << TestRangeElement(100, 200)
                                           << TestRangeElement(200, 300));
        QVERIFY(mux.output().empty());
        QVERIFY(!mux.isComplete());

        simple->end();

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                mux.serialize(stream);
                seg->serialize(stream);
                us->serialize(stream);
                ph->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                mux.deserialize(stream);
                seg = mux.deserializeSegmented(stream);
                us = mux.deserializeUnsorted(stream);
                ph = mux.deserializePlaceholder(stream);
            }
        }

        seg->end();
        ph->end();
        us->advance(300);

        QCOMPARE(mux.output<QList<TestRangeElement>>(),
                 QList<TestRangeElement>() << TestRangeElement(300, 400));

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                mux.serialize(stream);
                us->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                mux.deserialize(stream);
                us = mux.deserializeUnsorted(stream);
            }
        }

        mux.finishAll();

        QVERIFY(!mux.isComplete());
        QCOMPARE(mux.output<QList<TestRangeElement>>(),
                 QList<TestRangeElement>() << TestRangeElement(500, 600));
        QVERIFY(mux.output().empty());

        QVERIFY(mux.isComplete());

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                mux.serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                mux.deserialize(stream);
            }
        }

        QVERIFY(mux.isComplete());
        QVERIFY(mux.output().empty());
    }
};

QTEST_APPLESS_MAIN(TestMerge)

#include "merge.moc"
