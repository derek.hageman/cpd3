/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <time.h>
#include <math.h>
#include <functional>
#include <QtGlobal>
#include <QTest>
#include <QString>
#include <QStringList>

#include "core/search.hxx"

using namespace CPD3;

Q_DECLARE_METATYPE(QList<double>);

class TestSearch : public QObject {
Q_OBJECT
private slots:

    void lowerBound()
    {
        QFETCH(QList<double>, data);

        QList<double>::const_iterator result =
                Search::heuristicLowerBound(data.constBegin(), data.constEnd(), data.constBegin(),
                                            100.0);
        QList<double>::const_iterator
                expected = std::lower_bound(data.constBegin(), data.constEnd(), 100.0);
        QCOMPARE(result - data.constBegin(), expected - data.constBegin());

        if (data.isEmpty())
            return;

        result = Search::heuristicLowerBound(data.constBegin(), data.constEnd(),
                                             data.constBegin() + 1, 100.0);
        QCOMPARE(result - data.constBegin(), expected - data.constBegin());
        result =
                Search::heuristicLowerBound(data.constBegin(), data.constEnd(), data.constEnd() - 1,
                                            100.0);
        QCOMPARE(result - data.constBegin(), expected - data.constBegin());

        expected = std::lower_bound(data.constBegin(), data.constEnd(), -100.0);
        result = Search::heuristicLowerBound(data.constBegin(), data.constEnd(), data.constBegin(),
                                             -100.0);
        QCOMPARE(result - data.constBegin(), expected - data.constBegin());
        result = Search::heuristicLowerBound(data.constBegin(), data.constEnd(),
                                             data.constBegin() + 1, -100.0);
        QCOMPARE(result - data.constBegin(), expected - data.constBegin());
        result =
                Search::heuristicLowerBound(data.constBegin(), data.constEnd(), data.constEnd() - 1,
                                            -100.0);
        QCOMPARE(result - data.constBegin(), expected - data.constBegin());

        for (QList<double>::const_iterator targetBase = data.constBegin(),
                endTarget = data.constEnd(); targetBase != endTarget; ++targetBase) {
            expected = std::lower_bound(data.constBegin(), data.constEnd(), *targetBase);

            result = Search::heuristicLowerBound(data.constBegin(), data.constEnd(),
                                                 data.constBegin(), *targetBase);
            QCOMPARE(result - data.constBegin(), expected - data.constBegin());
            result = Search::heuristicLowerBound(data.constBegin(), data.constEnd(),
                                                 data.constBegin() + 1, *targetBase);
            QCOMPARE(result - data.constBegin(), expected - data.constBegin());
            result = Search::heuristicLowerBound(data.constBegin(), data.constEnd(),
                                                 data.constEnd() - 1, *targetBase);
            QCOMPARE(result - data.constBegin(), expected - data.constBegin());
            result = Search::heuristicLowerBound(data.constBegin(), data.constEnd(), expected,
                                                 *targetBase);
            QCOMPARE(result - data.constBegin(), expected - data.constBegin());

            if (targetBase + 1 != data.constEnd()) {
                double sv = (*(targetBase + 1) + *targetBase) * 0.5;
                expected = std::lower_bound(data.constBegin(), data.constEnd(), sv);

                result = Search::heuristicLowerBound(data.constBegin(), data.constEnd(),
                                                     data.constBegin(), sv);
                QCOMPARE(result - data.constBegin(), expected - data.constBegin());
                result = Search::heuristicLowerBound(data.constBegin(), data.constEnd(),
                                                     data.constBegin() + 1, sv);
                QCOMPARE(result - data.constBegin(), expected - data.constBegin());
                result = Search::heuristicLowerBound(data.constBegin(), data.constEnd(),
                                                     data.constEnd() - 1, sv);
                QCOMPARE(result - data.constBegin(), expected - data.constBegin());
                result = Search::heuristicLowerBound(data.constBegin(), data.constEnd(), expected,
                                                     sv);
                QCOMPARE(result - data.constBegin(), expected - data.constBegin());
            }
        }
    }

    void lowerBound_data()
    {
        QTest::addColumn<QList<double> >("data");

        QStringList fields;

        QTest::newRow("Empty") << (QList<double>());
        QTest::newRow("One") << (QList<double>() << 1.0);
        QTest::newRow("Two") << (QList<double>() << 1.0 << 2.0);
        QTest::newRow("Two duplicate") << (QList<double>() << 1.0 << 1.0);
        QTest::newRow("Three") << (QList<double>() << 1.0 << 2.0 << 3.0);
        QTest::newRow("Three duplicate A") << (QList<double>() << 1.0 << 1.0 << 2.0);
        QTest::newRow("Three duplicate B") << (QList<double>() << 1.0 << 2.0 << 2.0);
        QTest::newRow("Three duplicate C") << (QList<double>() << 2.0 << 2.0 << 2.0);
        QTest::newRow("Four") << (QList<double>() << 1.0 << 2.0 << 3.0 << 4.0);
        QTest::newRow("Four duplicate A") << (QList<double>() << 1.0 << 2.0 << 4.0 << 4.0);
        QTest::newRow("Four duplicate B") << (QList<double>() << 1.0 << 4.0 << 4.0 << 4.0);
        QTest::newRow("Four duplicate C") << (QList<double>() << 4.0 << 4.0 << 4.0 << 4.0);
        QTest::newRow("Four duplicate D") << (QList<double>() << 2.0 << 2.0 << 4.0 << 4.0);
        QTest::newRow("Four duplicate E") << (QList<double>() << 1.0 << 3.0 << 3.0 << 4.0);
        QTest::newRow("Four duplicate F") << (QList<double>() << 3.0 << 3.0 << 3.0 << 4.0);
        QTest::newRow("Four duplicate G") << (QList<double>() << 2.0 << 2.0 << 3.0 << 4.0);
        QTest::newRow("Five") << (QList<double>() << 1.0 << 2.0 << 3.0 << 4.0 << 5.0);
        QTest::newRow("Five duplicate A") << (QList<double>() << 1.0 << 2.0 << 3.0 << 5.0 << 5.0);
        QTest::newRow("Five duplicate B") << (QList<double>() << 2.0 << 2.0 << 3.0 << 5.0 << 5.0);
        QTest::newRow("Five duplicate C") << (QList<double>() << 2.0 << 2.0 << 5.0 << 5.0 << 5.0);
        QTest::newRow("Five duplicate D") << (QList<double>() << 1.0 << 2.0 << 3.0 << 3.0 << 5.0);
        QTest::newRow("Five duplicate E") << (QList<double>() << 1.0 << 3.0 << 3.0 << 5.0 << 5.0);

        QTest::newRow("Complex A") <<
                (QList<double>() <<
                        1.0 <<
                        2.0 <<
                        3.0 <<
                        5.0 <<
                        5.0 <<
                        6.0 <<
                        7.0 <<
                        7.0 <<
                        9.0 <<
                        10.0 <<
                        12.0 <<
                        13.0 <<
                        13.0 <<
                        13.0 <<
                        13.0 <<
                        14.0);
    }
};

QTEST_APPLESS_MAIN(TestSearch)

#include "search.moc"
