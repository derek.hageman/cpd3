/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QTest>

#include "core/textsubstitution.hxx"
#include "core/number.hxx"

using namespace CPD3;

class TestSubstitution : public TextSubstitution {
public:
    TestSubstitution()
    { }

    virtual ~TestSubstitution()
    { }

    QHash<QString, QString> subs;

protected:
    virtual QString substitution(const QStringList &elements) const
    {
        return subs.value(elements.at(0)) + QStringList(elements.mid(1)).join("*").toUpper();
    }

    virtual QString literalCheck(QStringRef &key) const
    {
        if (key.at(0) == '<') {
            key = QStringRef(key.string(), key.position() + 1, key.length() - 1);
            return QString('>');
        } else if (key.at(0) == 'E' && key.at(1) == 'O' && key.at(2) == 'F') {
            key = QStringRef(key.string(), key.position() + 3, key.length() - 3);
            return QString("EOF");
        }
        return QString();
    }
};

typedef QHash<QString, QString> SubsType;

Q_DECLARE_METATYPE(SubsType);

class TestTextSubstitution : public QObject {
Q_OBJECT
private slots:

    void basicSubsitution()
    {
        QFETCH(QString, input);
        QFETCH(QString, expected);
        QFETCH(SubsType, subs);

        TestSubstitution ts;
        ts.subs = subs;
        QString result(ts.apply(input));

        QCOMPARE(result, expected);
    }

    void basicSubsitution_data()
    {
        QTest::addColumn<QString>("input");
        QTest::addColumn<QString>("expected");
        QTest::addColumn<SubsType>("subs");

        QHash<QString, QString> subs;

        QTest::newRow("Empty") << "" << "" << subs;
        QTest::newRow("No replacement one") << "1" << "1" << subs;
        QTest::newRow("No replacement two") << "12" << "12" << subs;
        QTest::newRow("No replacement three") << "123" << "123" << subs;
        QTest::newRow("No replacement long") << "123456789" << "123456789" << subs;
        QTest::newRow("Invalid single variable") << "$" << "$" << subs;
        QTest::newRow("Escape single") << "$$" << "$" << subs;
        QTest::newRow("Escape first") << "$$1" << "$1" << subs;
        QTest::newRow("Escape first multiple") << "$$1234" << "$1234" << subs;
        QTest::newRow("Escape last") << "1$$" << "1$" << subs;
        QTest::newRow("Escape last multiple") << "1234$$" << "1234$" << subs;
        QTest::newRow("Escape middle") << "1$$2" << "1$2" << subs;
        QTest::newRow("Escape middle multiple begin") << "12$$3" << "12$3" << subs;
        QTest::newRow("Escape middle multiple end") << "1$$23" << "1$23" << subs;
        QTest::newRow("Escape middle multiple both") << "123$$456" << "123$456" << subs;
        QTest::newRow("Escape multiple single") << "$$$$" << "$$" << subs;
        QTest::newRow("Escape multiple middle") << "$$1$$" << "$1$" << subs;
        QTest::newRow("Escape multiple long") << "123$$456$$789" << "123$456$789" << subs;
        QTest::newRow("Direct single") << "${$}" << "$" << subs;
        QTest::newRow("Direct first") << "${$}1" << "$1" << subs;
        QTest::newRow("Direct first multiple") << "${$}1234" << "$1234" << subs;
        QTest::newRow("Direct last") << "1${$}" << "1$" << subs;
        QTest::newRow("Direct last multiple") << "1234${$}" << "1234$" << subs;
        QTest::newRow("Direct middle") << "1${$}2" << "1$2" << subs;
        QTest::newRow("Direct middle multiple begin") << "12${$}3" << "12$3" << subs;
        QTest::newRow("Direct middle multiple end") << "1${$}23" << "1$23" << subs;
        QTest::newRow("Direct middle multiple both") << "123${$}456" << "123$456" << subs;
        QTest::newRow("Direct multiple single") << "${$}${$}" << "$$" << subs;
        QTest::newRow("Direct multiple middle") << "${$}1${$}" << "$1$" << subs;
        QTest::newRow("Direct multiple long") << "123${$}456${$}789" << "123$456$789" << subs;
        QTest::newRow("Unterminated end") << "123${" << "123${" << subs;

        QTest::newRow("Not found single") << "${z}" << "" << subs;
        QTest::newRow("Not found single longer") << "${zzzz}" << "" << subs;
        QTest::newRow("Not found first") << "${z}1" << "1" << subs;
        QTest::newRow("Not found first multiple") << "${z}123" << "123" << subs;
        QTest::newRow("Not found first multiple longer") << "${zzzz}123" << "123" << subs;
        QTest::newRow("Not found last") << "${z}1" << "1" << subs;
        QTest::newRow("Not found last multiple") << "123${z}" << "123" << subs;
        QTest::newRow("Not found last multiple longer") << "123${zzzz}" << "123" << subs;
        QTest::newRow("Not found middle") << "1${z}2" << "12" << subs;
        QTest::newRow("Not found middle multiple") << "123${z}456" << "123456" << subs;
        QTest::newRow("Not found middle multiple longer") << "123${zzzz}456" << "123456" << subs;

        subs.insert("a", "BB");
        subs.insert("bb", "AAAA");
        subs.insert("c", "");
        subs.insert("d", "CCCCC");
        subs.insert("}E", "QWERTY");
        subs.insert("AA}E", "QWERTY");
        subs.insert("\\A", "A");
        subs.insert("|c", "DD");

        QTest::newRow("Shorter single") << "${a}" << "BB" << subs;
        QTest::newRow("Shorter first") << "${a}1" << "BB1" << subs;
        QTest::newRow("Shorter first multiple") << "${a}123" << "BB123" << subs;
        QTest::newRow("Shorter last") << "1${a}" << "1BB" << subs;
        QTest::newRow("Shorter last multiple") << "123${a}" << "123BB" << subs;
        QTest::newRow("Shorter middle") << "1${a}2" << "1BB2" << subs;
        QTest::newRow("Shorter middle multiple") << "123${a}456" << "123BB456" << subs;
        QTest::newRow("Shorter repeated") << "${a}${a}" << "BBBB" << subs;
        QTest::newRow("Shorter multiple middle") << "123${a}456${a}789" << "123BB456BB789" << subs;

        QTest::newRow("Same single") << "${bb}" << "AAAA" << subs;
        QTest::newRow("Same first") << "${bb}1" << "AAAA1" << subs;
        QTest::newRow("Same first multiple") << "${bb}123" << "AAAA123" << subs;
        QTest::newRow("Same last") << "1${bb}" << "1AAAA" << subs;
        QTest::newRow("Same last multiple") << "123${bb}" << "123AAAA" << subs;
        QTest::newRow("Same middle") << "1${bb}2" << "1AAAA2" << subs;
        QTest::newRow("Same middle multiple") << "123${bb}456" << "123AAAA456" << subs;
        QTest::newRow("Same repeated") << "${bb}${bb}" << "AAAAAAAA" << subs;
        QTest::newRow("Same multiple middle") <<
                "123${bb}456${bb}789" <<
                "123AAAA456AAAA789" <<
                subs;

        QTest::newRow("Longer single") << "${d}" << "CCCCC" << subs;
        QTest::newRow("Longer first") << "${d}1" << "CCCCC1" << subs;
        QTest::newRow("Longer first multiple") << "${d}123" << "CCCCC123" << subs;
        QTest::newRow("Longer last") << "1${d}" << "1CCCCC" << subs;
        QTest::newRow("Longer last multiple") << "123${d}" << "123CCCCC" << subs;
        QTest::newRow("Longer middle") << "1${d}2" << "1CCCCC2" << subs;
        QTest::newRow("Longer middle multiple") << "123${d}456" << "123CCCCC456" << subs;
        QTest::newRow("Longer repeated") << "${d}${d}" << "CCCCCCCCCC" << subs;
        QTest::newRow("Longer multiple middle") <<
                "123${d}456${d}789" <<
                "123CCCCC456CCCCC789" <<
                subs;

        QTest::newRow("Empty single") << "${c}" << "" << subs;
        QTest::newRow("Empty first") << "${c}1" << "1" << subs;
        QTest::newRow("Empty first multiple") << "${c}123" << "123" << subs;
        QTest::newRow("Empty last") << "1${c}" << "1" << subs;
        QTest::newRow("Empty last multiple") << "123${c}" << "123" << subs;
        QTest::newRow("Empty middle") << "1${c}2" << "12" << subs;
        QTest::newRow("Empty middle multiple") << "123${c}456" << "123456" << subs;
        QTest::newRow("Empty repeated") << "${c}${c}" << "" << subs;
        QTest::newRow("Empty multiple middle") << "123${c}456${c}789" << "123456789" << subs;

        QTest::newRow("Escape bracket single") << "${\\}E}" << "QWERTY" << subs;
        QTest::newRow("Escape bracket first") << "${\\}E}1" << "QWERTY1" << subs;
        QTest::newRow("Escape bracket first multiple") << "${\\}E}123" << "QWERTY123" << subs;
        QTest::newRow("Escape bracket last") << "1${\\}E}" << "1QWERTY" << subs;
        QTest::newRow("Escape bracket last multiple") << "123${\\}E}" << "123QWERTY" << subs;
        QTest::newRow("Escape bracket middle") << "1${\\}E}2" << "1QWERTY2" << subs;
        QTest::newRow("Escape bracket middle multiple") <<
                "123${\\}E}456" <<
                "123QWERTY456" <<
                subs;
        QTest::newRow("Escape bracket repeated") << "${\\}E}${\\}E}" << "QWERTYQWERTY" << subs;
        QTest::newRow("Escape bracket multiple middle") <<
                "123${\\}E}456${\\}E}789" <<
                "123QWERTY456QWERTY789" <<
                subs;

        QTest::newRow("Escape slash single") << "${\\\\A}" << "A" << subs;
        QTest::newRow("Escape slash first") << "${\\\\A}1" << "A1" << subs;
        QTest::newRow("Escape slash first multiple") << "${\\\\A}123" << "A123" << subs;
        QTest::newRow("Escape slash last") << "1${\\\\A}" << "1A" << subs;
        QTest::newRow("Escape slash last multiple") << "123${\\\\A}" << "123A" << subs;
        QTest::newRow("Escape slash middle") << "1${\\\\A}2" << "1A2" << subs;
        QTest::newRow("Escape slash middle multiple") << "123${\\\\A}456" << "123A456" << subs;
        QTest::newRow("Escape slash repeated") << "${\\\\A}${\\\\A}" << "AA" << subs;
        QTest::newRow("Escape slash multiple middle") <<
                "123${\\\\A}456${\\\\A}789" <<
                "123A456A789" <<
                subs;

        QTest::newRow("Escape other single") << "${\\bb}" << "AAAA" << subs;
        QTest::newRow("Escape other first") << "${\\bb}1" << "AAAA1" << subs;
        QTest::newRow("Escape other first multiple") << "${\\bb}123" << "AAAA123" << subs;
        QTest::newRow("Escape other last") << "1${\\bb}" << "1AAAA" << subs;
        QTest::newRow("Escape other last multiple") << "123${\\bb}" << "123AAAA" << subs;
        QTest::newRow("Escape other middle") << "1${\\bb}2" << "1AAAA2" << subs;
        QTest::newRow("Escape other middle multiple") << "123${\\bb}456" << "123AAAA456" << subs;
        QTest::newRow("Escape other repeated") << "${\\bb}${\\bb}" << "AAAAAAAA" << subs;
        QTest::newRow("Escape other multiple middle") <<
                "123${\\bb}456${\\bb}789" <<
                "123AAAA456AAAA789" <<
                subs;

        QTest::newRow("Escape pipe single") << "${\\|c}" << "DD" << subs;
        QTest::newRow("Escape pipe first") << "${\\|c}1" << "DD1" << subs;
        QTest::newRow("Escape pipe first multiple") << "${\\|c}123" << "DD123" << subs;
        QTest::newRow("Escape pipe last") << "1${\\|c}" << "1DD" << subs;
        QTest::newRow("Escape pipe last multiple") << "123${\\|c}" << "123DD" << subs;
        QTest::newRow("Escape pipe middle") << "1${\\|c}2" << "1DD2" << subs;
        QTest::newRow("Escape pipe middle multiple") << "123${\\|c}456" << "123DD456" << subs;
        QTest::newRow("Escape pipe repeated") << "${\\|c}${\\|c}" << "DDDD" << subs;
        QTest::newRow("Escape pipe multiple middle") <<
                "123${\\|c}456${\\|c}789" <<
                "123DD456DD789" <<
                subs;

        QTest::newRow("Literal one single") << "${<bb>}" << "AAAA" << subs;
        QTest::newRow("Literal one first") << "${<bb>}1" << "AAAA1" << subs;
        QTest::newRow("Literal one first multiple") << "${<bb>}123" << "AAAA123" << subs;
        QTest::newRow("Literal one last") << "1${<bb>}" << "1AAAA" << subs;
        QTest::newRow("Literal one last multiple") << "123${<bb>}" << "123AAAA" << subs;
        QTest::newRow("Literal one middle") << "1${<bb>}2" << "1AAAA2" << subs;
        QTest::newRow("Literal one middle multiple") << "123${<bb>}456" << "123AAAA456" << subs;
        QTest::newRow("Literal one repeated") << "${<bb>}${<bb>}" << "AAAAAAAA" << subs;
        QTest::newRow("Literal one multiple middle") <<
                "123${<bb>}456${<bb>}789" <<
                "123AAAA456AAAA789" <<
                subs;

        QTest::newRow("Literal three single") << "${EOFbbEOF}" << "AAAA" << subs;
        QTest::newRow("Literal three first") << "${EOFbbEOF}1" << "AAAA1" << subs;
        QTest::newRow("Literal three first multiple") << "${EOFbbEOF}123" << "AAAA123" << subs;
        QTest::newRow("Literal three last") << "1${EOFbbEOF}" << "1AAAA" << subs;
        QTest::newRow("Literal three last multiple") << "123${EOFbbEOF}" << "123AAAA" << subs;
        QTest::newRow("Literal three middle") << "1${EOFbbEOF}2" << "1AAAA2" << subs;
        QTest::newRow("Literal three middle multiple") <<
                "123${EOFbbEOF}456" <<
                "123AAAA456" <<
                subs;
        QTest::newRow("Literal three repeated") << "${EOFbbEOF}${EOFbbEOF}" << "AAAAAAAA" << subs;
        QTest::newRow("Literal three multiple middle") <<
                "123${EOFbbEOF}456${EOFbbEOF}789" <<
                "123AAAA456AAAA789" <<
                subs;

        QTest::newRow("Literal bracket single") << "${<}E>}" << "QWERTY" << subs;
        QTest::newRow("Literal bracket first") << "${<}E>}1" << "QWERTY1" << subs;
        QTest::newRow("Literal bracket first multiple") << "${<}E>}123" << "QWERTY123" << subs;
        QTest::newRow("Literal bracket last") << "1${<}E>}" << "1QWERTY" << subs;
        QTest::newRow("Literal bracket last multiple") << "123${<}E>}" << "123QWERTY" << subs;
        QTest::newRow("Literal bracket middle") << "1${<}E>}2" << "1QWERTY2" << subs;
        QTest::newRow("Literal bracket middle multiple") <<
                "123${<}E>}456" <<
                "123QWERTY456" <<
                subs;
        QTest::newRow("Literal bracket repeated") << "${<}E>}${<}E>}" << "QWERTYQWERTY" << subs;
        QTest::newRow("Literal bracket multiple middle") <<
                "123${<}E>}456${<}E>}789" <<
                "123QWERTY456QWERTY789" <<
                subs;

        QTest::newRow("Literal offset bracket single") << "${<AA}E>}" << "QWERTY" << subs;
        QTest::newRow("Literal offset bracket first") << "${<AA}E>}1" << "QWERTY1" << subs;
        QTest::newRow("Literal offset bracket first multiple") <<
                "${<AA}E>}123" <<
                "QWERTY123" <<
                subs;
        QTest::newRow("Literal offset bracket last") << "1${<AA}E>}" << "1QWERTY" << subs;
        QTest::newRow("Literal offset bracket last multiple") <<
                "123${<AA}E>}" <<
                "123QWERTY" <<
                subs;
        QTest::newRow("Literal offset bracket middle") << "1${<AA}E>}2" << "1QWERTY2" << subs;
        QTest::newRow("Literal offset bracket middle multiple") <<
                "123${<AA}E>}456" <<
                "123QWERTY456" <<
                subs;
        QTest::newRow("Literal offset bracket repeated") <<
                "${<AA}E>}${<AA}E>}" <<
                "QWERTYQWERTY" <<
                subs;
        QTest::newRow("Literal offset bracket multiple middle") <<
                "123${<AA}E>}456${<AA}E>}789" <<
                "123QWERTY456QWERTY789" <<
                subs;

        QTest::newRow("Literal text single") << "${EOF}EEOF}" << "QWERTY" << subs;
        QTest::newRow("Literal text first") << "${EOF}EEOF}1" << "QWERTY1" << subs;
        QTest::newRow("Literal text first multiple") << "${EOF}EEOF}123" << "QWERTY123" << subs;
        QTest::newRow("Literal text last") << "1${EOF}EEOF}" << "1QWERTY" << subs;
        QTest::newRow("Literal text last multiple") << "123${EOF}EEOF}" << "123QWERTY" << subs;
        QTest::newRow("Literal text middle") << "1${EOF}EEOF}2" << "1QWERTY2" << subs;
        QTest::newRow("Literal text middle multiple") <<
                "123${EOF}EEOF}456" <<
                "123QWERTY456" <<
                subs;
        QTest::newRow("Literal text repeated") <<
                "${EOF}EEOF}${EOF}EEOF}" <<
                "QWERTYQWERTY" <<
                subs;
        QTest::newRow("Literal text multiple middle") <<
                "123${EOF}EEOF}456${EOF}EEOF}789" <<
                "123QWERTY456QWERTY789" <<
                subs;

        QTest::newRow("Literal offset text single") << "${EOFAA}EEOF}" << "QWERTY" << subs;
        QTest::newRow("Literal offset text first") << "${EOFAA}EEOF}1" << "QWERTY1" << subs;
        QTest::newRow("Literal offset text first multiple") <<
                "${EOFAA}EEOF}123" <<
                "QWERTY123" <<
                subs;
        QTest::newRow("Literal offset text last") << "1${EOFAA}EEOF}" << "1QWERTY" << subs;
        QTest::newRow("Literal offset text last multiple") <<
                "123${EOFAA}EEOF}" <<
                "123QWERTY" <<
                subs;
        QTest::newRow("Literal offset text middle") << "1${EOFAA}EEOF}2" << "1QWERTY2" << subs;
        QTest::newRow("Literal offset text middle multiple") <<
                "123${EOFAA}EEOF}456" <<
                "123QWERTY456" <<
                subs;
        QTest::newRow("Literal offset text repeated") <<
                "${EOFAA}EEOF}${EOFAA}EEOF}" <<
                "QWERTYQWERTY" <<
                subs;
        QTest::newRow("Literal offset text multiple middle") <<
                "123${EOFAA}EEOF}456${EOFAA}EEOF}789" <<
                "123QWERTY456QWERTY789" <<
                subs;

        QTest::newRow("Multipart empty single") << "${a|}" << "BB" << subs;
        QTest::newRow("Multipart empty multiple") << "${a|||}" << "BB**" << subs;
        QTest::newRow("Multipart empty middle") << "${a|q||w}" << "BBQ**W" << subs;
        QTest::newRow("Multipart single") << "${a|qwerty}" << "BBQWERTY" << subs;
        QTest::newRow("Multipart contents") << "${a|qwe|rty}" << "BBQWE*RTY" << subs;
        QTest::newRow("Multipart escape") << "${a|q\\|we|rty}" << "BBQ|WE*RTY" << subs;
    }

    void timeFormat()
    {
        QFETCH(double, time);
        QFETCH(QStringList, elements);
        QFETCH(QString, expected);

        QCOMPARE(TextSubstitution::formatTime(time, elements), expected);
    }

    void timeFormat_data()
    {
        QTest::addColumn<double>("time");
        QTest::addColumn<QStringList>("elements");
        QTest::addColumn<QString>("expected");

        QTest::newRow("Empty") << FP::undefined() << (QStringList()) << "";
        QTest::newRow("MVC") << FP::undefined() << (QStringList("default") << "MVC") << "MVC";
        QTest::newRow("Basic") << 1458321591.0 << (QStringList()) << "2016-03-18T17:19:51Z";

        QTest::newRow("ISO") << 1458321591.0 << (QStringList()) << "2016-03-18T17:19:51Z";
        QTest::newRow("ISO no delimiter") <<
                1458321591.0 <<
                (QStringList("nd")) <<
                "20160318T171951Z";

        QTest::newRow("Year") << 1458321591.0 << (QStringList("year")) << "2016";
        QTest::newRow("Year format") << 1458321591.0 << (QStringList("year") << "00000") << "02016";
        QTest::newRow("Year implicit MVC") << FP::undefined() << (QStringList("year")) << "";
        QTest::newRow("Year explicit MVC") <<
                FP::undefined() <<
                (QStringList("year") << "" << "MVC") <<
                "MVC";
        QTest::newRow("Year format MVC") <<
                FP::undefined() <<
                (QStringList("year") << "0000") <<
                "9999";

        QTest::newRow("Month") << 1458321591.0 << (QStringList("month")) << "03";
        QTest::newRow("Month fill") <<
                1458321591.0 <<
                (QStringList("month") << "" << "" << " ") <<
                " 3";
        QTest::newRow("Month format") << 1458321591.0 << (QStringList("month") << "000") << "003";
        QTest::newRow("Month implicit MVC") << FP::undefined() << (QStringList("month")) << "";
        QTest::newRow("Month explicit MVC") <<
                FP::undefined() <<
                (QStringList("month") << "" << "MVC") <<
                "MVC";
        QTest::newRow("Month format MVC") <<
                FP::undefined() <<
                (QStringList("month") << "00") <<
                "99";

        QTest::newRow("Day") << 1458321591.0 << (QStringList("day")) << "18";
        QTest::newRow("Day format") << 1458321591.0 << (QStringList("day") << "000") << "018";
        QTest::newRow("Day implicit MVC") << FP::undefined() << (QStringList("day")) << "";
        QTest::newRow("Day explicit MVC") <<
                FP::undefined() <<
                (QStringList("day") << "" << "MVC") <<
                "MVC";
        QTest::newRow("Day format MVC") << FP::undefined() << (QStringList("day") << "00") << "99";

        QTest::newRow("Hour") << 1458321591.0 << (QStringList("Hour")) << "17";
        QTest::newRow("Hour format") << 1458321591.0 << (QStringList("Hour") << "000") << "017";
        QTest::newRow("Hour implicit MVC") << FP::undefined() << (QStringList("Hour")) << "";
        QTest::newRow("Hour explicit MVC") <<
                FP::undefined() <<
                (QStringList("Hour") << "" << "MVC") <<
                "MVC";
        QTest::newRow("Day format MVC") << FP::undefined() << (QStringList("Hour") << "00") << "99";

        QTest::newRow("Minute") << 1458321591.0 << (QStringList("MINUTE")) << "19";
        QTest::newRow("Minute format") << 1458321591.0 << (QStringList("MINUTE") << "000") << "019";
        QTest::newRow("Minute implicit MVC") << FP::undefined() << (QStringList("MINUTE")) << "";
        QTest::newRow("Minute explicit MVC") <<
                FP::undefined() <<
                (QStringList("MINUTE") << "" << "MVC") <<
                "MVC";
        QTest::newRow("Minute format MVC") <<
                FP::undefined() <<
                (QStringList("MINUTE") << "00") <<
                "99";

        QTest::newRow("Second") << 1458321591.0 << (QStringList("second")) << "51";
        QTest::newRow("Second format") << 1458321591.0 << (QStringList("second") << "000") << "051";
        QTest::newRow("Second implicit MVC") << FP::undefined() << (QStringList("second")) << "";
        QTest::newRow("Second explicit MVC") <<
                FP::undefined() <<
                (QStringList("second") << "" << "MVC") <<
                "MVC";
        QTest::newRow("Second format MVC") <<
                FP::undefined() <<
                (QStringList("second") << "00") <<
                "99";

        QTest::newRow("Millisecond") << 1458321591.021 << (QStringList("msec")) << "021";
        QTest::newRow("Millisecond format") <<
                1458321591.021 <<
                (QStringList("Millisecond") << "0000") <<
                "0021";
        QTest::newRow("Millisecond implicit MVC") << FP::undefined() << (QStringList("msec")) << "";
        QTest::newRow("Millisecond explicit MVC") <<
                FP::undefined() <<
                (QStringList("msec") << "" << "MVC") <<
                "MVC";
        QTest::newRow("Millisecond format MVC") <<
                FP::undefined() <<
                (QStringList("msec") << "00") <<
                "99";

        QTest::newRow("DOY") << 1458321591.0 << (QStringList("doy")) << "078.72212";
        QTest::newRow("DOY format") << 1458321591.0 << (QStringList("doy") << "00.0") << "78.7";
        QTest::newRow("DOY implicit MVC") << FP::undefined() << (QStringList("doy")) << "";
        QTest::newRow("DOY explicit MVC") <<
                FP::undefined() <<
                (QStringList("doy") << "" << "MVC") <<
                "MVC";
        QTest::newRow("DOY format MVC") <<
                FP::undefined() <<
                (QStringList("doy") << "000.0") <<
                "999.9";

        QTest::newRow("Date format") <<
                1458321591.0 <<
                (QStringList("format") << "yyyy:dd") <<
                "2016:18";
        QTest::newRow("Date format implicit MVC") <<
                FP::undefined() <<
                (QStringList("format")) <<
                "";
        QTest::newRow("Date format MVC") <<
                FP::undefined() <<
                (QStringList("format") << "" << "MVC") <<
                "MVC";
    }

    void doubleFormat()
    {
        QFETCH(double, value);
        QFETCH(QStringList, elements);
        QFETCH(QString, expected);

        QCOMPARE(TextSubstitution::formatDouble(value, elements), expected);
    }

    void doubleFormat_data()
    {
        QTest::addColumn<double>("value");
        QTest::addColumn<QStringList>("elements");
        QTest::addColumn<QString>("expected");

        QTest::newRow("Empty") << FP::undefined() << (QStringList()) << "";
        QTest::newRow("MVC") << FP::undefined() << (QStringList("") << "MVC") << "MVC";
        QTest::newRow("Basic") << 1234.0 << (QStringList()) << "1234.000";
        QTest::newRow("Format") << 1234.0 << (QStringList("00000.0")) << "01234.0";
        QTest::newRow("Fill") << 1234.0 << (QStringList("00000.0") << "" << " ") << " 1234.0";

        QTest::newRow("Scale") <<
                1234.0 <<
                (QStringList("00000.0") << "" << "" << "scale" << "0.1") <<
                "123.4";
        QTest::newRow("Offset") <<
                1234.0 <<
                (QStringList("00000.0") << "" << "" << "Offset" << "1") <<
                "1235.0";
        QTest::newRow("Divide") <<
                1234.0 <<
                (QStringList("00000.0") << "" << "" << "divide" << "10") <<
                "123.4";
        QTest::newRow("Denominator") <<
                1234.0 <<
                (QStringList("00000.0") << "" << "" << "denominator" << "123400") <<
                "100.0";
        QTest::newRow("Subtract") <<
                1234.0 <<
                (QStringList("00000.0") << "" << "" << "SUBTRACT" << "1") <<
                "1233.0";
        QTest::newRow("Countdown") <<
                1234.0 <<
                (QStringList("00000.0") << "" << "" << "Countdown" << "1236") <<
                "2.0";
        QTest::newRow("Minimum apply") <<
                1234.0 <<
                (QStringList("00000.0") << "" << "" << "Min" << "123") <<
                "123.0";
        QTest::newRow("Minimum pass") <<
                1234.0 <<
                (QStringList("00000.0") << "" << "" << "minimum" << "1235") <<
                "1234.0";
        QTest::newRow("Maximum apply") <<
                1234.0 <<
                (QStringList("00000.0") << "" << "" << "Max" << "1235") <<
                "1235.0";
        QTest::newRow("Maximum pass") <<
                1234.0 <<
                (QStringList("00000.0") << "" << "" << "Maximum" << "1231") <<
                "1234.0";
        QTest::newRow("Modulus") <<
                1234.0 <<
                (QStringList("00000.0") << "" << "" << "Modulus" << "100") <<
                "34.0";
        QTest::newRow("Range inside") <<
                8.0 <<
                (QStringList("00000.0") << "" << "" << "Range" << "1" << "10") <<
                "8.0";
        QTest::newRow("Range above") <<
                12.0 <<
                (QStringList("00000.0") << "" << "" << "Range" << "1" << "10") <<
                "3.0";
        QTest::newRow("Range below") <<
                0.0 <<
                (QStringList("00000.0") << "" << "" << "Range" << "1" << "10") <<
                "9.0";

        QTest::newRow("Inverse") <<
                5.0 <<
                (QStringList("00000.0") << "" << "" << "inverse") <<
                "0.2";
        QTest::newRow("Polynomial") <<
                3.0 <<
                (QStringList("00000.0") << "" << "" << "poly" << "0.5;2.0") <<
                "6.5";
        QTest::newRow("Inverse Polynomial") <<
                6.5 <<
                (QStringList("00000.0") << "" << "" << "invpoly" << "0.5;2.0") <<
                "3.0";
    }

    void integerFormat()
    {
        QFETCH(qint64, value);
        QFETCH(QStringList, elements);
        QFETCH(QString, expected);

        QCOMPARE(TextSubstitution::formatInteger(value, elements), expected);
    }

    void integerFormat_data()
    {
        QTest::addColumn<qint64>("value");
        QTest::addColumn<QStringList>("elements");
        QTest::addColumn<QString>("expected");

        QTest::newRow("Empty") << INTEGER::undefined() << (QStringList()) << "";
        QTest::newRow("MVC") << INTEGER::undefined() << (QStringList("") << "MVC") << "MVC";
        QTest::newRow("Basic") << Q_INT64_C(1234) << (QStringList()) << "1234";
        QTest::newRow("Format") << Q_INT64_C(1234) << (QStringList("00000")) << "01234";
        QTest::newRow("Fill") << Q_INT64_C(1234) << (QStringList("00000") << "" << " ") << " 1234";

        QTest::newRow("Scale") <<
                Q_INT64_C(1234) <<
                (QStringList("00000") << "" << "" << "scale" << "2") <<
                "2468";
        QTest::newRow("Offset") <<
                Q_INT64_C(1234) <<
                (QStringList("00000") << "" << "" << "Offset" << "1") <<
                "1235";
        QTest::newRow("Divide") <<
                Q_INT64_C(1234) <<
                (QStringList("00000") << "" << "" << "divide" << "10") <<
                "123";
        QTest::newRow("Denominator") <<
                Q_INT64_C(1234) <<
                (QStringList("00000") << "" << "" << "denominator" << "123400") <<
                "100";
        QTest::newRow("Subtract") <<
                Q_INT64_C(1234) <<
                (QStringList("00000") << "" << "" << "SUBTRACT" << "1") <<
                "1233";
        QTest::newRow("Countdown") <<
                Q_INT64_C(1234) <<
                (QStringList("00000") << "" << "" << "Countdown" << "1236") <<
                "2";
        QTest::newRow("Minimum apply") <<
                Q_INT64_C(1234) <<
                (QStringList("00000") << "" << "" << "Min" << "123") <<
                "123";
        QTest::newRow("Minimum pass") <<
                Q_INT64_C(1234) <<
                (QStringList("00000") << "" << "" << "minimum" << "1235") <<
                "1234";
        QTest::newRow("Maximum apply") <<
                Q_INT64_C(1234) <<
                (QStringList("00000") << "" << "" << "Max" << "1235") <<
                "1235";
        QTest::newRow("Maximum pass") <<
                Q_INT64_C(1234) <<
                (QStringList("00000") << "" << "" << "Maximum" << "1231") <<
                "1234";
        QTest::newRow("Modulus") <<
                Q_INT64_C(1234) <<
                (QStringList("00000") << "" << "" << "Modulus" << "100") <<
                "34";
        QTest::newRow("Range inside") <<
                Q_INT64_C(8) <<
                (QStringList("00000") << "" << "" << "Range" << "1" << "10") <<
                "8";
        QTest::newRow("Range above") <<
                Q_INT64_C(12) <<
                (QStringList("00000") << "" << "" << "Range" << "1" << "10") <<
                "3";
        QTest::newRow("Range below") <<
                Q_INT64_C(0) <<
                (QStringList("00000") << "" << "" << "Range" << "1" << "10") <<
                "9";

        QTest::newRow("OR") <<
                Q_INT64_C(0x2) <<
                (QStringList("00000") << "" << "" << "OR" << "0x1") <<
                "3";
        QTest::newRow("AND") <<
                Q_INT64_C(0x13) <<
                (QStringList("00000") << "" << "" << "AND" << "0x7") <<
                "3";
        QTest::newRow("XOR") <<
                Q_INT64_C(0x2) <<
                (QStringList("00000") << "" << "" << "xor" << "0x3") <<
                "1";
        QTest::newRow("Mask") <<
                Q_INT64_C(0x7) <<
                (QStringList("00000") << "" << "" << "mask" << "0x3") <<
                "4";
        QTest::newRow("Left shift") <<
                Q_INT64_C(0x1) <<
                (QStringList("00000") << "" << "" << "lshift" << "2") <<
                "4";
        QTest::newRow("Right shift") <<
                Q_INT64_C(0x7) <<
                (QStringList("00000") << "" << "" << "rshift" << "2") <<
                "1";
        QTest::newRow("CLZ") <<
                Q_INT64_C(0x5) <<
                (QStringList("00000") << "" << "" << "CLZ") <<
                "61";
    }

    void stringFormat()
    {
        QFETCH(QString, value);
        QFETCH(QStringList, elements);
        QFETCH(QString, expected);

        QCOMPARE(TextSubstitution::formatString(value, elements), expected);
    }

    void stringFormat_data()
    {
        QTest::addColumn<QString>("value");
        QTest::addColumn<QStringList>("elements");
        QTest::addColumn<QString>("expected");

        QTest::newRow("Empty") << "" << (QStringList()) << "";
        QTest::newRow("Empty with MVC") << "" << (QStringList("") << "MVC") << "";
        QTest::newRow("Upper") << "foO" << (QStringList("uc")) << "FOO";
        QTest::newRow("Lower") << "foO" << (QStringList("lc")) << "foo";
        QTest::newRow("Quote") << "foo,bar" << (QStringList("quote")) << "\"foo,bar\"";
        QTest::newRow("Quote none") <<
                "foobar" <<
                (QStringList("quote") << "MVC" << "\"" << ",") <<
                "foobar";
        QTest::newRow("Quote escape") <<
                "foo,\"bar" <<
                (QStringList("quote") << "MVC" << "\"" << ",") <<
                "\"foo,\"\"\"bar\"";
        QTest::newRow("Trim") << "  foo " << (QStringList("trim")) << "foo";
        QTest::newRow("Simplified") << "  f o  o" << (QStringList("simplified")) << "f o o";
        QTest::newRow("Truncate") << "foobar" << (QStringList("truncate") << "MVC" << "3") << "foo";
        QTest::newRow("Mid") << "foobar" << (QStringList("mid") << "MVC" << "3" << "2") << "ba";
        QTest::newRow("Right justified") <<
                "foo" <<
                (QStringList("right") << "MVC" << "5" << "-") <<
                "--foo";
        QTest::newRow("Left justified") <<
                "foo" <<
                (QStringList("left") << "MVC" << "5" << "-") <<
                "foo--";
        QTest::newRow("To double") <<
                "123.5" <<
                (QStringList("double") << "MVC" << "0000.00") <<
                "0123.50";
        QTest::newRow("To integer") <<
                "123" <<
                (QStringList("integer") << "MVC" << "0000") <<
                "0123";
        QTest::newRow("To time") <<
                "2015:210.5" <<
                (QStringList("time") << "MVC") <<
                "2015-07-29T12:00:00Z";
        QTest::newRow("Empty replace") << "" << (QStringList("empty") << "MVC") << "MVC";

        QTest::newRow("Regexp basic") <<
                "foobar" <<
                (QStringList("regexp") << "MVC" << "foo" << "bazz") <<
                "bazzbar";
        QTest::newRow("Regexp case insensitive") <<
                "FOObar" <<
                (QStringList("regexp") << "MVC" << "foo" << "baz" << "i") <<
                "bazbar";
        QTest::newRow("Regexp miss") <<
                "foobar" <<
                (QStringList("regexp") << "MVC" << "baz" << "blag" << "r") <<
                "MVC";
        QTest::newRow("Regexp multiple") <<
                "foofoobarfoo" <<
                (QStringList("regexp") << "MVC" << "foo" << "a" << "g") <<
                "aabara";
        QTest::newRow("Regexp multiple unflagged") <<
                "foofoobarfoo" <<
                (QStringList("regexp") << "MVC" << "foo" << "a") <<
                "afoobarfoo";
        QTest::newRow("Regexp capture") <<
                "foobar" <<
                (QStringList("regexp") << "MVC" << "^foo(bar)$" << "${1}baz${1}") <<
                "barbazbar";

        QTest::newRow("Multiple operation") <<
                "  foo " <<
                (QStringList("trim,lower,upper")) <<
                "FOO";
    }

    void stringFormatUndefined()
    {
        QCOMPARE(TextSubstitution::formatString("", QStringList(), false), QString(""));
        QCOMPARE(TextSubstitution::formatString("", QStringList("") << "MVC", false),
                 QString("MVC"));
    }

    void durationFormat()
    {
        QFETCH(double, value);
        QFETCH(QStringList, elements);
        QFETCH(QString, expected);

        QCOMPARE(TextSubstitution::formatDuration(value, elements, "r%1", "n%1"), expected);
    }

    void durationFormat_data()
    {
        QTest::addColumn<double>("value");
        QTest::addColumn<QStringList>("elements");
        QTest::addColumn<QString>("expected");

        QTest::newRow("Empty") << FP::undefined() << (QStringList()) << "";
        QTest::newRow("MVC") << FP::undefined() << (QStringList("") << "MVC") << "MVC";
        QTest::newRow("Basic") << 30.0 << (QStringList("relative")) << "r30";

        QTest::newRow("Auto milliseconds") <<
                0.5 <<
                (QStringList("relative") << "MVC" << "auto") <<
                "r500";
        QTest::newRow("Auto seconds") <<
                30.0 <<
                (QStringList("relative") << "MVC" << "auto") <<
                "r30";
        QTest::newRow("Auto minutes") <<
                300.0 <<
                (QStringList("relative") << "MVC" << "auto") <<
                "r5";
        QTest::newRow("Auto hours") <<
                7200.0 <<
                (QStringList("relative") << "MVC" << "auto") <<
                "r2";
        QTest::newRow("Auto days") <<
                172800.0 <<
                (QStringList("relative") << "MVC" << "auto") <<
                "r2";
        QTest::newRow("Auto weeks") <<
                1209600.0 <<
                (QStringList("relative") << "MVC" << "auto") <<
                "r2";

        QTest::newRow("Auto units milliseconds") <<
                0.5 <<
                (QStringList("relative") << "MVC" << "autounits") <<
                "r500 millisecond(s)";
        QTest::newRow("Auto units seconds") <<
                30.0 <<
                (QStringList("relative") << "MVC" << "autounits") <<
                "r30 second(s)";
        QTest::newRow("Auto units minutes") <<
                300.0 <<
                (QStringList("relative") << "MVC" << "autounits") <<
                "r5 minute(s)";
        QTest::newRow("Auto units hours") <<
                7200.0 <<
                (QStringList("relative") << "MVC" << "autounits") <<
                "r2 hour(s)";
        QTest::newRow("Auto units days") <<
                172800.0 <<
                (QStringList("relative") << "MVC" << "autounits") <<
                "r2 day(s)";
        QTest::newRow("Auto units weeks") <<
                1209600.0 <<
                (QStringList("relative") << "MVC" << "autounits") <<
                "r2 week(s)";

        QTest::newRow("Explicit milliseconds") <<
                0.5 <<
                (QStringList("relative") << "MVC" << "millisecond") <<
                "r500";
        QTest::newRow("Explicit seconds") <<
                30.0 <<
                (QStringList("relative") << "MVC" << "second") <<
                "r30";
        QTest::newRow("Explicit minutes") <<
                300.0 <<
                (QStringList("relative") << "MVC" << "minute") <<
                "r5";
        QTest::newRow("Explicit hours") <<
                7200.0 <<
                (QStringList("relative") << "MVC" << "hour") <<
                "r2";
        QTest::newRow("Explicit days") <<
                172800.0 <<
                (QStringList("relative") << "MVC" << "day") <<
                "r2";
        QTest::newRow("Explicit weeks") <<
                1209600.0 <<
                (QStringList("relative") << "MVC" << "week") <<
                "r2";

        QTest::newRow("Explicit units milliseconds") <<
                0.5 <<
                (QStringList("relative") << "MVC" << "millisecondsunit") <<
                "r500 millisecond(s)";
        QTest::newRow("Explicit units seconds") <<
                30.0 <<
                (QStringList("relative") << "MVC" << "secondsunit") <<
                "r30 second(s)";
        QTest::newRow("Explicit units minutes") <<
                300.0 <<
                (QStringList("relative") << "MVC" << "minutesunit") <<
                "r5 minute(s)";
        QTest::newRow("Explicit units hours") <<
                7200.0 <<
                (QStringList("relative") << "MVC" << "hoursunit") <<
                "r2 hour(s)";
        QTest::newRow("Explicit units days") <<
                172800.0 <<
                (QStringList("relative") << "MVC" << "daysunit") <<
                "r2 day(s)";
        QTest::newRow("Explicit units weeks") <<
                1209600.0 <<
                (QStringList("relative") << "MVC" << "weeksunit") <<
                "r2 week(s)";

        QTest::newRow("Format minutes") <<
                30.0 <<
                (QStringList("relative") << "MVC" << "minutes" << "0.0") <<
                "r0.5";
        QTest::newRow("Format hours padding") <<
                7200.0 <<
                (QStringList("relative") << "MVC" << "hour" << "00" << "0") <<
                "r02";

        QTest::newRow("Relative negative") << -30.0 << (QStringList("relative") << "MVC") << "n30";
        QTest::newRow("Absolute positive") << 30.0 << (QStringList("absolute") << "MVC") << "30";
        QTest::newRow("Absolute negative") << -30.0 << (QStringList("absolute") << "MVC") << "30";
        QTest::newRow("Signed positive") << 30.0 << (QStringList("signed") << "MVC") << "30";
        QTest::newRow("Signed negative") << -30.0 << (QStringList("signed") << "MVC") << "-30";

        QTest::newRow("Countdown") << 320.0 << (QStringList("countdown")) << "05:20";
        QTest::newRow("Relative countdown") <<
                320.0 <<
                (QStringList("relative") << "MVC" << "countdown") <<
                "r05:20";
    }

    void numberedReplacer()
    {
        NumberedSubstitution rep(QStringList() << "zero" << "One" << "TWO");

        QCOMPARE(rep.apply("${0}"), QString("zero"));
        QCOMPARE(rep.apply("${1}"), QString("One"));
        QCOMPARE(rep.apply("${2}"), QString("TWO"));
        QCOMPARE(rep.apply("${3}"), QString(""));
        QCOMPARE(rep.apply("${-1}"), QString(""));
        QCOMPARE(rep.apply("${2}${0}"), QString("TWOzero"));
        QCOMPARE(rep.apply("${2|lc}"), QString("two"));
    }

    void textSubstitutionStack()
    {
        TextSubstitutionStack rep;

        rep.setDouble("double", 1.0);
        rep.setInteger("integer", 2);
        rep.setString("string", "Striny");
        rep.setTime("time", 1458321591.0);
        rep.setDuration("duration", 30.0);

        QCOMPARE(rep.apply("${double}"), QString("1.000"));
        QCOMPARE(rep.apply("${double|00.0}"), QString("01.0"));
        QCOMPARE(rep.apply("${integer}"), QString("2"));
        QCOMPARE(rep.apply("${integer|000}"), QString("002"));
        QCOMPARE(rep.apply("${string}"), QString("Striny"));
        QCOMPARE(rep.apply("${string|uc}"), QString("STRINY"));
        QCOMPARE(rep.apply("${string|lower}"), QString("striny"));
        QCOMPARE(rep.apply("${string|regexp||Str(i)ny|Stuff\\$\\{1\\}}"), QString("Stuffi"));
        QCOMPARE(rep.apply("${string|regexp|FOO|NOMATCH|Blah|r}"), QString("FOO"));
        QCOMPARE(rep.apply("${TIME}"), QString("2016-03-18T17:19:51Z"));
        QCOMPARE(rep.apply("${TIME|year}"), QString("2016"));
        QCOMPARE(rep.apply("${duration|relative||autounits}"), QString("30 second(s)"));

        rep.push();

        rep.setDouble("double", 3.0);
        rep.setInteger("integer", 4);
        rep.setDouble("string", -1.0);

        QCOMPARE(rep.apply("${double}"), QString("3.000"));
        QCOMPARE(rep.apply("${double|00.0}"), QString("03.0"));
        QCOMPARE(rep.apply("${integer}"), QString("4"));
        QCOMPARE(rep.apply("${integer|000}"), QString("004"));
        QCOMPARE(rep.apply("${string}"), QString("-1.000"));
        QCOMPARE(rep.apply("${string|000.0}"), QString("-01.0"));
        QCOMPARE(rep.apply("${TIME}"), QString("2016-03-18T17:19:51Z"));
        QCOMPARE(rep.apply("${TIME|year}"), QString("2016"));
        QCOMPARE(rep.apply("${duration|relative||autounits}"), QString("30 second(s)"));

        rep.pop();

        {
            TextSubstitutionStack::Context ctx(rep);

            rep.setDouble("double", 9.0);
            rep.setInteger("integer", 4);
            rep.setDouble("string", -1.0);

            QCOMPARE(rep.apply("${double}"), QString("9.000"));
            QCOMPARE(rep.apply("${double|00.0}"), QString("09.0"));
            QCOMPARE(rep.apply("${integer}"), QString("4"));
            QCOMPARE(rep.apply("${integer|000}"), QString("004"));
            QCOMPARE(rep.apply("${string}"), QString("-1.000"));
            QCOMPARE(rep.apply("${string|000.0}"), QString("-01.0"));
            QCOMPARE(rep.apply("${TIME}"), QString("2016-03-18T17:19:51Z"));
            QCOMPARE(rep.apply("${TIME|year}"), QString("2016"));
            QCOMPARE(rep.apply("${duration|relative||autounits}"), QString("30 second(s)"));
        }

        QCOMPARE(rep.apply("${double}"), QString("1.000"));
        QCOMPARE(rep.apply("${double|00.0}"), QString("01.0"));
        QCOMPARE(rep.apply("${integer}"), QString("2"));
        QCOMPARE(rep.apply("${integer|000}"), QString("002"));
        QCOMPARE(rep.apply("${string}"), QString("Striny"));
        QCOMPARE(rep.apply("${string|uc}"), QString("STRINY"));
        QCOMPARE(rep.apply("${string|lower}"), QString("striny"));
        QCOMPARE(rep.apply("${string|regexp||Str(i)ny|Stuff\\$\\{1\\}}"), QString("Stuffi"));
        QCOMPARE(rep.apply("${string|regexp|FOO|NOMATCH|Blah|r}"), QString("FOO"));
        QCOMPARE(rep.apply("${TIME}"), QString("2016-03-18T17:19:51Z"));
        QCOMPARE(rep.apply("${TIME|year}"), QString("2016"));
        QCOMPARE(rep.apply("${duration|relative||autounits}"), QString("30 second(s)"));

        rep.clear();

        rep.setDouble("foo", 1.0);

        QCOMPARE(rep.apply("${double}"), QString(""));
        QCOMPARE(rep.apply("${double|00.0}"), QString(""));
        QCOMPARE(rep.apply("${foo}"), QString("1.000"));
        QCOMPARE(rep.apply("${foo|00.0}"), QString("01.0"));
    }

};

QTEST_APPLESS_MAIN(TestTextSubstitution)

#include "textsubstitution.moc"

