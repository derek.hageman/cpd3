/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <time.h>
#include <math.h>
#include <stdint.h>
#include <QTest>
#include <QString>

#include "core/timeparse.hxx"
#include "core/timeutils.hxx"
#include "core/number.hxx"

using namespace CPD3;

class TestTimeParseReference : public TimeReference {
private:
    double reference;
    bool leading;
    bool allowUndefined;
public:
    TestTimeParseReference()
    {
        reference = 0;
        leading = false;
        allowUndefined = false;
    }

    TestTimeParseReference(double reference, bool leading, bool allowUndefined = false)
    {
        this->reference = reference;
        this->leading = leading;
        this->allowUndefined = allowUndefined;
    }

    TestTimeParseReference(const TestTimeParseReference &copy) : TimeReference()
    {
        reference = copy.reference;
        leading = copy.leading;
        allowUndefined = copy.allowUndefined;
    }

    virtual double getReference() noexcept(false)
    {
        return reference;
    }

    virtual bool isLeading() noexcept(false)
    {
        return leading;
    }

    virtual double getUndefinedValue() noexcept(false)
    {
        if (allowUndefined)
            return FP::undefined();
        throw TimeParsingException(TimeParse::tr("Undefined/infinite bounds are not valid."));
    }
};

Q_DECLARE_METATYPE(TestTimeParseReference)

class TestTimeParseHandler : public TimeParseListHandler {
private:
    int ignoreStart;
    int ignoreEnd;
public:
    TestTimeParseHandler()
    {
        ignoreStart = 0;
        ignoreEnd = 0;
    }

    TestTimeParseHandler(int ignoreStart, int ignoreEnd)
    {
        this->ignoreStart = ignoreStart;
        this->ignoreEnd = ignoreEnd;
    }

    TestTimeParseHandler(const TestTimeParseHandler &copy) : TimeParseListHandler()
    {
        ignoreStart = copy.ignoreStart;
        ignoreEnd = copy.ignoreEnd;
    }

    virtual bool handleLeading(const QString &str, int index, const QStringList &list)
    noexcept(false)
    {
        (void) str;
        (void) list;
        return index < ignoreStart;
    }

    virtual bool handleTrailing(const QString &str, int index, const QStringList &list)
    noexcept(false)
    {
        (void) str;
        return (list.size() - 1) - index < ignoreEnd;
    }
};

Q_DECLARE_METATYPE(TestTimeParseHandler)

class TestTimeParse : public QObject {
Q_OBJECT
private slots:

    void parseCurrentTime()
    {
        double tStart = Time::time();
        double tEnd;
        try { tEnd = TimeParse::parseTime("now"); } catch (TimeParsingException tpe) {
            QFAIL(tpe.getDescription().toLatin1().data());
        }
        QVERIFY(tEnd >= tStart);
    }

    void parseOffset()
    {
        QFETCH(QString, string);
        QFETCH(TestTimeParseReference, reference);
        QFETCH(double, result);
        QFETCH(Time::LogicalTimeUnit, unit);

        double tv;
        Time::LogicalTimeUnit uv;
        try {
            tv = TimeParse::parseOffset(string, &reference, &uv);
        } catch (TimeParsingException tpe) {
            QFAIL(tpe.getDescription().toLatin1().data());
        }
        if (fabs(tv - result) >= 0.001) {
            QFAIL((QString("Parsed: ") +
                    QString::number(tv, 'f', 3) +
                    QString(" Expected: ") +
                    QString::number(result, 'f', 3)).toLatin1().data());
        }
        QCOMPARE(uv, unit);

        try { tv = TimeParse::parseOffset(string, &reference); } catch (TimeParsingException tpe) {
            QFAIL(tpe.getDescription().toLatin1().data());
        }
        if (fabs(tv - result) >= 0.001) {
            QFAIL((QString("Parsed: ") +
                    QString::number(tv, 'f', 3) +
                    QString(" Expected: ") +
                    QString::number(result, 'f', 3)).toLatin1().data());
        }


        int count = -1;
        bool aligned = false;
        try {
            TimeParse::parseOffset(string, &uv, &count, &aligned, false, false);
        } catch (TimeParsingException tpe) {
            QFAIL(tpe.getDescription().toLatin1().data());
        }
        QCOMPARE(uv, unit);

        if (reference.isLeading())
            tv = Time::logical(reference.getReference(), uv, -count, aligned, false);
        else
            tv = Time::logical(reference.getReference(), uv, count, aligned, true);
        if (fabs(tv - result) >= 0.001) {
            QFAIL((QString("Parsed: ") +
                    QString::number(tv, 'f', 3) +
                    QString(" Expected: ") +
                    QString::number(result, 'f', 3)).toLatin1().data());
        }
    }

    void parseOffset_data()
    {
        QTest::addColumn<QString>("string");
        QTest::addColumn<TestTimeParseReference>("reference");
        QTest::addColumn<double>("result");
        QTest::addColumn<Time::LogicalTimeUnit>("unit");

        /* 2010-05-13T18:17:26Z */
        TestTimeParseReference leading = TestTimeParseReference(1273774646.0, true);
        TestTimeParseReference trailing = TestTimeParseReference(1273774646.0, false);

        /* Relative offsets */
        QTest::newRow("Offset millisecond leading") <<
                "1msec" <<
                leading <<
                1273774645.999 <<
                Time::Millisecond;
        QTest::newRow("Offset millisecond trailing") <<
                "843. milliseconds" <<
                trailing <<
                1273774646.843 <<
                Time::Millisecond;
        QTest::newRow("Offset second leading") <<
                "10 seconds" <<
                leading <<
                1273774636.0 <<
                Time::Second;
        QTest::newRow("Offset second leading fractional") <<
                "10.5 seconds" <<
                leading <<
                1273774635.5 <<
                Time::Millisecond;
        QTest::newRow("Offset second trailing") <<
                "542s" <<
                trailing <<
                1273775188.0 <<
                Time::Second;
        QTest::newRow("Offset second trailing fractional") <<
                "542.1 s" <<
                trailing <<
                1273775188.1 <<
                Time::Millisecond;
        QTest::newRow("Offset hour leading") << "1.0 hour" << leading << 1273771046.0 << Time::Hour;
        QTest::newRow("Offset hour leading fractional") <<
                "0.5 hour" <<
                leading <<
                1273772846.0 <<
                Time::Minute;
        QTest::newRow("Offset hour trailing") << "5h" << trailing << 1273792646.0 << Time::Hour;
        QTest::newRow("Offset hour trailing fractional") <<
                "2.34h" <<
                trailing <<
                1273783070.0 <<
                Time::Second;
        QTest::newRow("Offset day leading") << "10 days" << leading << 1272910646.0 << Time::Day;
        QTest::newRow("Offset day leading fractional") <<
                "1.25day" <<
                leading <<
                1273666646.0 <<
                Time::Hour;
        QTest::newRow("Offset day trailing") << "365d" << trailing << 1305310646.0 << Time::Day;
        QTest::newRow("Offset day trailing fractional") <<
                "5.1d" <<
                trailing <<
                1274215286.0 <<
                Time::Minute;
        QTest::newRow("Offset week leading") << "3 weeks" << leading << 1271960246.0 << Time::Week;
        QTest::newRow("Offset week leading fractional") <<
                "2.1week" <<
                leading <<
                1272504566.0 <<
                Time::Minute;
        QTest::newRow("Offset week trailing") << "1w" << trailing << 1274379446.0 << Time::Week;
        QTest::newRow("Offset week trailing") << "2.5w" << trailing << 1275286646.0 << Time::Hour;
        QTest::newRow("Offset month leading") <<
                "7 months" <<
                leading <<
                1255457846.0 <<
                Time::Month;
        QTest::newRow("Offset month trailing") << "2mon" << trailing << 1279045046.0 << Time::Month;
        QTest::newRow("Offset quarter leading") << "1q" << leading << 1265998646.0 << Time::Quarter;
        QTest::newRow("Offset quarter trailing") <<
                "5 quarters" <<
                trailing <<
                1313173046.0 <<
                Time::Quarter;
        QTest::newRow("Offset year leading") << "3 years" << leading << 1179080246.0 << Time::Year;
        QTest::newRow("Offset year trailing") << "1y" << trailing << 1305310646.0 << Time::Year;

        /* No count */
        QTest::newRow("Offset no count millisecond leading") <<
                "msec" <<
                leading <<
                1273774645.999 <<
                Time::Millisecond;
        QTest::newRow("Offset no count millisecond trailing") <<
                "millisecond" <<
                trailing <<
                1273774646.001 <<
                Time::Millisecond;
        QTest::newRow("Offset no count second leading") <<
                "s" <<
                leading <<
                1273774645.0 <<
                Time::Second;
        QTest::newRow("Offset no count second trailing") <<
                "second" <<
                trailing <<
                1273774647.0 <<
                Time::Second;
        QTest::newRow("Offset no count minute leading") <<
                "m" <<
                leading <<
                1273774586.0 <<
                Time::Minute;
        QTest::newRow("Offset no count minute trailing") <<
                "minutes" <<
                trailing <<
                1273774706.0 <<
                Time::Minute;
        QTest::newRow("Offset no count hour leading") <<
                "h" <<
                leading <<
                1273771046.0 <<
                Time::Hour;
        QTest::newRow("Offset no count hour trailing") <<
                "hour" <<
                trailing <<
                1273778246.0 <<
                Time::Hour;
        QTest::newRow("Offset no count day leading") << "d" << leading << 1273688246.0 << Time::Day;
        QTest::newRow("Offset no count day trailing") <<
                "days" <<
                trailing <<
                1273861046.0 <<
                Time::Day;
        QTest::newRow("Offset no count week leading") <<
                "w" <<
                leading <<
                1273169846.0 <<
                Time::Week;
        QTest::newRow("Offset no count week trailing") <<
                "week" <<
                trailing <<
                1274379446.0 <<
                Time::Week;
        QTest::newRow("Offset no count month leading") <<
                "mo" <<
                leading <<
                1271182646.0 <<
                Time::Month;
        QTest::newRow("Offset no count month trailing") <<
                "month" <<
                trailing <<
                1276453046.0 <<
                Time::Month;
        QTest::newRow("Offset no count quarter leading") <<
                "q" <<
                leading <<
                1265998646.0 <<
                Time::Quarter;
        QTest::newRow("Offset no count quarter trailing") <<
                "quarter" <<
                trailing <<
                1281637046.0 <<
                Time::Quarter;
        QTest::newRow("Offset no count year leading") <<
                "y" <<
                leading <<
                1242238646.0 <<
                Time::Year;
        QTest::newRow("Offset no count year trailing") <<
                "year" <<
                trailing <<
                1305310646.0 <<
                Time::Year;

        /* Explicit alignment disable */
        QTest::newRow("Offset millisecond leading no align") <<
                "1msecna" <<
                leading <<
                1273774645.999 <<
                Time::Millisecond;
        QTest::newRow("Offset millisecond trailing no align") <<
                "843 milliseconds no aligned" <<
                trailing <<
                1273774646.843 <<
                Time::Millisecond;
        QTest::newRow("Offset second leading no align") <<
                "10 seconds noalign" <<
                leading <<
                1273774636.0 <<
                Time::Second;
        QTest::newRow("Offset second trailing no align") <<
                "542sna" <<
                trailing <<
                1273775188.0 <<
                Time::Second;
        QTest::newRow("Offset hour leading no align") <<
                "1 hour no align" <<
                leading <<
                1273771046.0 <<
                Time::Hour;
        QTest::newRow("Offset hour trailing no align") <<
                "5h noalign" <<
                trailing <<
                1273792646.0 <<
                Time::Hour;
        QTest::newRow("Offset day leading no align") <<
                "10 days na" <<
                leading <<
                1272910646.0 <<
                Time::Day;
        QTest::newRow("Offset day trailing no align") <<
                "365d noalign" <<
                trailing <<
                1305310646.0 <<
                Time::Day;
        QTest::newRow("Offset week leading no align") <<
                "3 weeks no aligned" <<
                leading <<
                1271960246.0 <<
                Time::Week;
        QTest::newRow("Offset week trailing no align") <<
                "1w na" <<
                trailing <<
                1274379446.0 <<
                Time::Week;
        QTest::newRow("Offset month leading no align") <<
                "7 months noaligned" <<
                leading <<
                1255457846.0 <<
                Time::Month;
        QTest::newRow("Offset month trailing no align") <<
                "2mon noaligned" <<
                trailing <<
                1279045046.0 <<
                Time::Month;
        QTest::newRow("Offset quarter leading no align") <<
                "1qna" <<
                leading <<
                1265998646.0 <<
                Time::Quarter;
        QTest::newRow("Offset quarter trailing no align") <<
                "5 quartersna" <<
                trailing <<
                1313173046.0 <<
                Time::Quarter;
        QTest::newRow("Offset year leading no align") <<
                "3 years noalign" <<
                leading <<
                1179080246.0 <<
                Time::Year;
        QTest::newRow("Offset year trailing no align") <<
                "1yna" <<
                trailing <<
                1305310646.0 <<
                Time::Year;

        /* No count explicit alignment disabled */
        QTest::newRow("Offset no count millisecond leading no align") <<
                "msecna" <<
                leading <<
                1273774645.999 <<
                Time::Millisecond;
        QTest::newRow("Offset no count millisecond trailing no align") <<
                "millisecond no align" <<
                trailing <<
                1273774646.001 <<
                Time::Millisecond;
        QTest::newRow("Offset no count second leading no align") <<
                "sna" <<
                leading <<
                1273774645.0 <<
                Time::Second;
        QTest::newRow("Offset no count second trailing no align") <<
                "secondnoalign" <<
                trailing <<
                1273774647.0 <<
                Time::Second;
        QTest::newRow("Offset no count minute leading no align") <<
                "mnoalign" <<
                leading <<
                1273774586.0 <<
                Time::Minute;
        QTest::newRow("Offset no count minute trailing no align") <<
                "minutesna" <<
                trailing <<
                1273774706.0 <<
                Time::Minute;
        QTest::newRow("Offset no count hour leading no align") <<
                "hna" <<
                leading <<
                1273771046.0 <<
                Time::Hour;
        QTest::newRow("Offset no count hour trailing no align") <<
                "hour noalign" <<
                trailing <<
                1273778246.0 <<
                Time::Hour;
        QTest::newRow("Offset no count day leading no align") <<
                "dna" <<
                leading <<
                1273688246.0 <<
                Time::Day;
        QTest::newRow("Offset no count day trailing no align") <<
                "days no align" <<
                trailing <<
                1273861046.0 <<
                Time::Day;
        QTest::newRow("Offset no count week leading no align") <<
                "w no align" <<
                leading <<
                1273169846.0 <<
                Time::Week;
        QTest::newRow("Offset no count week trailing no align") <<
                "weekna" <<
                trailing <<
                1274379446.0 <<
                Time::Week;
        QTest::newRow("Offset no count month leading no align") <<
                "mo noalign" <<
                leading <<
                1271182646.0 <<
                Time::Month;
        QTest::newRow("Offset no count month trailing no align") <<
                "monthna" <<
                trailing <<
                1276453046.0 <<
                Time::Month;
        QTest::newRow("Offset no count quarter leading no align") <<
                "qna" <<
                leading <<
                1265998646.0 <<
                Time::Quarter;
        QTest::newRow("Offset no count quarter trailing no align") <<
                "quarter no align" <<
                trailing <<
                1281637046.0 <<
                Time::Quarter;
        QTest::newRow("Offset no count year leading no align") <<
                "yna" <<
                leading <<
                1242238646.0 <<
                Time::Year;
        QTest::newRow("Offset no count year trailing no align") <<
                "year no align" <<
                trailing <<
                1305310646.0 <<
                Time::Year;

        /* Relative offsets aligned */
        QTest::newRow("Offset millisecond leading aligned") <<
                "1mseca" <<
                TestTimeParseReference(1273774646.0001, true) <<
                1273774645.999 <<
                Time::Millisecond;
        QTest::newRow("Offset millisecond trailing aligned") <<
                "843 milliseconds aligned" <<
                TestTimeParseReference(1273774646.0001, false) <<
                1273774646.844 <<
                Time::Millisecond;
        QTest::newRow("Offset second leading aligned") <<
                "10 seconds aligned" <<
                TestTimeParseReference(1273774646.2, true) <<
                1273774630.0 <<
                Time::Second;
        QTest::newRow("Offset second trailing aligned") <<
                "542sa" <<
                TestTimeParseReference(1273774646.2, false) <<
                1273775189.0 <<
                Time::Second;
        QTest::newRow("Offset hour leading aligned") <<
                "1 hour aligned" <<
                leading <<
                1273770000.0 <<
                Time::Hour;
        QTest::newRow("Offset hour trailing aligned") <<
                "5ha" <<
                trailing <<
                1273795200.0 <<
                Time::Hour;
        QTest::newRow("Offset day leading aligned") <<
                "10 days aligned" <<
                leading <<
                1272844800.0 <<
                Time::Day;
        QTest::newRow("Offset day trailing aligned") <<
                "365da" <<
                trailing <<
                1305331200.0 <<
                Time::Day;
        QTest::newRow("Offset week leading aligned") <<
                "3 weeks a" <<
                leading <<
                1271635200.0 <<
                Time::Week;
        QTest::newRow("Offset week trailing aligned") <<
                "1waligned" <<
                trailing <<
                1274659200.0 <<
                Time::Week;
        QTest::newRow("Offset month leading aligned") <<
                "7 months aligned" <<
                leading <<
                1254355200.0 <<
                Time::Month;
        QTest::newRow("Offset month trailing aligned") <<
                "2monsa" <<
                trailing <<
                1280620800.0 <<
                Time::Month;
        QTest::newRow("Offset quarter leading aligned") <<
                "0qa" <<
                leading <<
                1270080000.0 <<
                Time::Quarter;
        QTest::newRow("Offset quarter trailing aligned") <<
                "5 quarters aligned" <<
                trailing <<
                1317427200.0 <<
                Time::Quarter;
        QTest::newRow("Offset year leading aligned") <<
                "3 years aligned" <<
                leading <<
                1167609600.0 <<
                Time::Year;
        QTest::newRow("Offset year trailing aligned") <<
                "1ya" <<
                trailing <<
                1325376000.0 <<
                Time::Year;

        /* Relative offsets aligned with no count */
        QTest::newRow("Offset no count millisecond leading aligned") <<
                "mseca" <<
                TestTimeParseReference(1273774646.0001, true) <<
                1273774645.999 <<
                Time::Millisecond;
        QTest::newRow("Offset no count millisecond trailing aligned") <<
                "msecalign" <<
                TestTimeParseReference(1273774646.0001, false) <<
                1273774646.002 <<
                Time::Millisecond;
        QTest::newRow("Offset no count second leading aligned") <<
                "sa" <<
                TestTimeParseReference(1273774646.2, true) <<
                1273774645.0 <<
                Time::Second;
        QTest::newRow("Offset no count second trailing aligned") <<
                "seconds align" <<
                TestTimeParseReference(1273774646.2, false) <<
                1273774648.0 <<
                Time::Second;
        QTest::newRow("Offset no count minute leading aligned") <<
                "min aligned" <<
                leading <<
                1273774560.0 <<
                Time::Minute;
        QTest::newRow("Offset no count minute trailing aligned") <<
                "minute aligned" <<
                trailing <<
                1273774740.0 <<
                Time::Minute;
        QTest::newRow("Offset no count hour leading aligned") <<
                "hour aligned" <<
                leading <<
                1273770000.0 <<
                Time::Hour;
        QTest::newRow("Offset no count hour trailing aligned") <<
                "ha" <<
                trailing <<
                1273780800.0 <<
                Time::Hour;
        QTest::newRow("Offset no count day leading aligned") <<
                "d aligned" <<
                leading <<
                1273622400.0 <<
                Time::Day;
        QTest::newRow("Offset no count day trailing aligned") <<
                "da" <<
                trailing <<
                1273881600.0 <<
                Time::Day;
        QTest::newRow("Offset no count week leading aligned") <<
                "weeka" <<
                leading <<
                1272844800.0 <<
                Time::Week;
        QTest::newRow("Offset no count week trailing aligned") <<
                "wa" <<
                trailing <<
                1274659200.0 <<
                Time::Week;
        QTest::newRow("Offset no count quarter leading aligned") <<
                "quarter aligned" <<
                leading <<
                1262304000.0 <<
                Time::Quarter;
        QTest::newRow("Offset no count quarter trailing aligned") <<
                "qa" <<
                trailing <<
                1285891200.0 <<
                Time::Quarter;
        QTest::newRow("Offset no count quarter leading aligned") <<
                "y aligned" <<
                leading <<
                1230768000.0 <<
                Time::Year;
        QTest::newRow("Offset no count quarter trailing aligned") <<
                "yeara" <<
                trailing <<
                1325376000.0 <<
                Time::Year;
    }

    void validateOffset()
    {
        QFETCH(QString, string);
        QFETCH(TestTimeParseReference, reference);

        bool failed = false;
        try { TimeParse::parseOffset(string, &reference); } catch (TimeParsingException tpe) {
            failed = true;
        }
        if (!failed)
            QFAIL("Accepted invalid result");

        failed = false;
        try { TimeParse::parseOffset(string, NULL, NULL, NULL, false, false); } catch (
                TimeParsingException tpe) { failed = true; }
        if (!failed)
            QFAIL("Accepted invalid result");
    }

    void validateOffset_data()
    {
        QTest::addColumn<QString>("string");
        QTest::addColumn<TestTimeParseReference>("reference");

        TestTimeParseReference ref = TestTimeParseReference(1273774646.0, true);

        QTest::newRow("Zero millisecond") << "0msec" << ref;
        QTest::newRow("Zero second") << "0s" << ref;
        QTest::newRow("Zero minute") << "0m" << ref;
        QTest::newRow("Zero hour") << "0h" << ref;
        QTest::newRow("Zero day") << "0d" << ref;
        QTest::newRow("Zero week") << "0w" << ref;
        QTest::newRow("Zero month") << "0mo" << ref;
        QTest::newRow("Zero quarter") << "0q" << ref;
        QTest::newRow("Zero year") << "0y" << ref;
    }

    void parseOffsetComponents()
    {
        QFETCH(QString, string);
        QFETCH(bool, allowZero);
        QFETCH(bool, allowNegative);
        QFETCH(Time::LogicalTimeUnit, units);
        QFETCH(int, count);
        QFETCH(bool, aligned);

        Time::LogicalTimeUnit resultUnits = (Time::LogicalTimeUnit) -1;
        int resultCount = count + 1;
        bool resultAlign = !aligned;
        try {
            TimeParse::parseOffset(string, &resultUnits, &resultCount, &resultAlign, allowZero,
                                   allowNegative);
        } catch (TimeParsingException tpe) {
            QFAIL(tpe.getDescription().toLatin1().data());
        }
        QCOMPARE(resultUnits, units);
        QCOMPARE(resultCount, count);
        if (string.endsWith("a") || string.endsWith("na") || string.contains("align")) {
            QCOMPARE(resultAlign, aligned);
        }
    }

    void parseOffsetComponents_data()
    {
        QTest::addColumn<QString>("string");
        QTest::addColumn<bool>("allowZero");
        QTest::addColumn<bool>("allowNegative");
        QTest::addColumn<Time::LogicalTimeUnit>("units");
        QTest::addColumn<int>("count");
        QTest::addColumn<bool>("aligned");

        QTest::newRow("Zero") << "zero" << true << false << Time::Second << 0 << false;
        QTest::newRow("Zero literal") << "0" << true << false << Time::Second << 0 << false;
        QTest::newRow("Zero literal decimals") <<
                "0.00" <<
                true <<
                false <<
                Time::Second <<
                0 <<
                false;
        QTest::newRow("None") << "none" << true << false << Time::Second << 0 << false;
        QTest::newRow("Zero millisecond") <<
                "0msec" <<
                true <<
                false <<
                Time::Millisecond <<
                0 <<
                false;
        QTest::newRow("Zero millisecond decimal") <<
                "0.millisecond" <<
                true <<
                false <<
                Time::Millisecond <<
                0 <<
                false;
        QTest::newRow("Zero second") << "0s" << true << false << Time::Second << 0 << false;
        QTest::newRow("Zero second decimal") <<
                "0.00seconds" <<
                true <<
                false <<
                Time::Second <<
                0 <<
                false;
        QTest::newRow("Zero minute") << "0m" << true << false << Time::Minute << 0 << false;
        QTest::newRow("Zero minute decimal") <<
                "0.0min" <<
                true <<
                false <<
                Time::Minute <<
                0 <<
                false;
        QTest::newRow("Zero hour") << "0h" << true << false << Time::Hour << 0 << false;
        QTest::newRow("Zero hour decimal") << "0.hour" << true << false << Time::Hour << 0 << false;
        QTest::newRow("Zero day") << "0d" << true << false << Time::Day << 0 << false;
        QTest::newRow("Zero day decimal") << "0.0days" << true << false << Time::Day << 0 << false;
        QTest::newRow("Zero week") << "0w" << true << false << Time::Week << 0 << false;
        QTest::newRow("Zero week decimal") <<
                "0.000week" <<
                true <<
                false <<
                Time::Week <<
                0 <<
                false;
        QTest::newRow("Zero quarter") << "0q" << true << false << Time::Quarter << 0 << false;
        QTest::newRow("Zero quarter decimal") <<
                "0.quarter" <<
                true <<
                false <<
                Time::Quarter <<
                0 <<
                false;
        QTest::newRow("Zero year") << "0y" << true << false << Time::Year << 0 << false;
        QTest::newRow("Zero year decimal") <<
                "0.0years" <<
                true <<
                false <<
                Time::Year <<
                0 <<
                false;

        QTest::newRow("Zero millisecond aligned") <<
                "0mseca" <<
                true <<
                false <<
                Time::Millisecond <<
                0 <<
                true;
        QTest::newRow("Zero second aligned") <<
                "0 seconds aligned" <<
                true <<
                false <<
                Time::Second <<
                0 <<
                true;
        QTest::newRow("Zero minute aligned") <<
                "0 min aligned" <<
                true <<
                false <<
                Time::Minute <<
                0 <<
                true;
        QTest::newRow("Zero hour aligned") << "0ha" << true << false << Time::Hour << 0 << true;
        QTest::newRow("Zero day aligned") <<
                "0 days aligned" <<
                true <<
                false <<
                Time::Day <<
                0 <<
                true;
        QTest::newRow("Zero week aligned") << "0wa" << true << false << Time::Week << 0 << true;
        QTest::newRow("Zero quarter aligned") <<
                "0qa" <<
                true <<
                false <<
                Time::Quarter <<
                0 <<
                true;
        QTest::newRow("Zero year aligned") <<
                "0 years aligned" <<
                true <<
                false <<
                Time::Year <<
                0 <<
                true;

        QTest::newRow("Positive") <<
                "10" <<
                false <<
                false <<
                ((Time::LogicalTimeUnit) -1) <<
                10 <<
                false;
        QTest::newRow("Negative") <<
                "-10" <<
                false <<
                true <<
                ((Time::LogicalTimeUnit) -1) <<
                -10 <<
                false;
        QTest::newRow("Positive millisecond") <<
                "10msec" <<
                false <<
                false <<
                Time::Millisecond <<
                10 <<
                false;
        QTest::newRow("Negative millisecond") <<
                "-10msec" <<
                false <<
                true <<
                Time::Millisecond <<
                -10 <<
                false;
        QTest::newRow("Positive second") << "10s" << false << false << Time::Second << 10 << false;
        QTest::newRow("Positive second fractional") <<
                "1.5s" <<
                false <<
                false <<
                Time::Millisecond <<
                1500 <<
                false;
        QTest::newRow("Negative second") <<
                "-10 seconds" <<
                false <<
                true <<
                Time::Second <<
                -10 <<
                false;
        QTest::newRow("Negative second fractional") <<
                "-4.125 sec" <<
                false <<
                true <<
                Time::Millisecond <<
                -4125 <<
                false;
        QTest::newRow("Positive minute") << "10m" << false << false << Time::Minute << 10 << false;
        QTest::newRow("Positive minute fractional") <<
                "1.5m" <<
                false <<
                false <<
                Time::Second <<
                90 <<
                false;
        QTest::newRow("Negative minute") <<
                "-10minutes" <<
                false <<
                true <<
                Time::Minute <<
                -10 <<
                false;
        QTest::newRow("Negative minute fractional") <<
                "-5.11minutes" <<
                false <<
                true <<
                Time::Millisecond <<
                -306600 <<
                false;
        QTest::newRow("Positive hour") << "10 hour" << false << false << Time::Hour << 10 << false;
        QTest::newRow("Positive hour fractional") <<
                "2.5 hour" <<
                false <<
                false <<
                Time::Minute <<
                150 <<
                false;
        QTest::newRow("Negative hour") << "-10h" << false << true << Time::Hour << -10 << false;
        QTest::newRow("Negative hour fractional") <<
                "-5.25h" <<
                false <<
                true <<
                Time::Minute <<
                -315 <<
                false;
        QTest::newRow("Positive day") << "10d" << false << false << Time::Day << 10 << false;
        QTest::newRow("Positive day fractional") <<
                "0.25 days" <<
                false <<
                false <<
                Time::Hour <<
                6 <<
                false;
        QTest::newRow("Negative day") << "-10d" << false << true << Time::Day << -10 << false;
        QTest::newRow("Negative day fractional") <<
                "-1.23day" <<
                false <<
                true <<
                Time::Second <<
                -106272 <<
                false;
        QTest::newRow("Positive week") << "10w" << false << false << Time::Week << 10 << false;
        QTest::newRow("Positive week fractional") <<
                "2.5week" <<
                false <<
                false <<
                Time::Hour <<
                420 <<
                false;
        QTest::newRow("Negative week") << "-10w" << false << true << Time::Week << -10 << false;
        QTest::newRow("Negative week fractional") <<
                "-5.25w" <<
                false <<
                true <<
                Time::Hour <<
                -882 <<
                false;
        QTest::newRow("Positive quarter") << "1q" << false << false << Time::Quarter << 1 << false;
        QTest::newRow("Negative quarter") <<
                "-3quarter" <<
                false <<
                true <<
                Time::Quarter <<
                -3 <<
                false;
        QTest::newRow("Positive year") << "10y" << false << false << Time::Year << 10 << false;
        QTest::newRow("Negative year") << "-10y" << false << true << Time::Year << -10 << false;

        QTest::newRow("Positive millisecond aligned") <<
                "10mseca" <<
                false <<
                false <<
                Time::Millisecond <<
                10 <<
                true;
        QTest::newRow("Negative millisecond aligned") <<
                "-10mseca" <<
                false <<
                true <<
                Time::Millisecond <<
                -10 <<
                true;
        QTest::newRow("Positive second aligned") <<
                "10s aligned" <<
                false <<
                false <<
                Time::Second <<
                10 <<
                true;
        QTest::newRow("Negative second aligned") <<
                "-10 secondsa" <<
                false <<
                true <<
                Time::Second <<
                -10 <<
                true;
        QTest::newRow("Positive minute aligned") <<
                "10ma" <<
                false <<
                false <<
                Time::Minute <<
                10 <<
                true;
        QTest::newRow("Negative minute aligned") <<
                "-10minutesa" <<
                false <<
                true <<
                Time::Minute <<
                -10 <<
                true;
        QTest::newRow("Positive hour aligned") <<
                "10 hour aligned" <<
                false <<
                false <<
                Time::Hour <<
                10 <<
                true;
        QTest::newRow("Negative hour aligned") <<
                "-10h aligned" <<
                false <<
                true <<
                Time::Hour <<
                -10 <<
                true;
        QTest::newRow("Positive day aligned") <<
                "10d aligned" <<
                false <<
                false <<
                Time::Day <<
                10 <<
                true;
        QTest::newRow("Negative day aligned") <<
                "-10da" <<
                false <<
                true <<
                Time::Day <<
                -10 <<
                true;
        QTest::newRow("Positive week aligned") <<
                "10wa" <<
                false <<
                false <<
                Time::Week <<
                10 <<
                true;
        QTest::newRow("Negative week aligned") <<
                "-10weeks aligned" <<
                false <<
                true <<
                Time::Week <<
                -10 <<
                true;
        QTest::newRow("Positive quarter aligned") <<
                "1 q a" <<
                false <<
                false <<
                Time::Quarter <<
                1 <<
                true;
        QTest::newRow("Negative quarter aligned") <<
                "-3 qtr aligned" <<
                false <<
                true <<
                Time::Quarter <<
                -3 <<
                true;
        QTest::newRow("Positive year aligned") <<
                "10y aligned" <<
                false <<
                false <<
                Time::Year <<
                10 <<
                true;
        QTest::newRow("Negative year aligned") <<
                "-10y aligned" <<
                false <<
                true <<
                Time::Year <<
                -10 <<
                true;

        QTest::newRow("Positive not aligned") <<
                "10na" <<
                false <<
                false <<
                ((Time::LogicalTimeUnit) -1) <<
                10 <<
                false;
        QTest::newRow("Negative not aligned") <<
                "-10noalign" <<
                false <<
                true <<
                ((Time::LogicalTimeUnit) -1) <<
                -10 <<
                false;
        QTest::newRow("Positive millisecond not aligned") <<
                "10msecna" <<
                false <<
                false <<
                Time::Millisecond <<
                10 <<
                false;
        QTest::newRow("Negative millisecond not aligned") <<
                "-10msecna" <<
                false <<
                true <<
                Time::Millisecond <<
                -10 <<
                false;
        QTest::newRow("Positive second not aligned") <<
                "10s no align" <<
                false <<
                false <<
                Time::Second <<
                10 <<
                false;
        QTest::newRow("Positive second fractional not aligned") <<
                "1.5sna" <<
                false <<
                false <<
                Time::Millisecond <<
                1500 <<
                false;
        QTest::newRow("Negative second not aligned") <<
                "-10 seconds na" <<
                false <<
                true <<
                Time::Second <<
                -10 <<
                false;
        QTest::newRow("Negative second fractional not aligned") <<
                "-4.125 secna" <<
                false <<
                true <<
                Time::Millisecond <<
                -4125 <<
                false;
        QTest::newRow("Positive minute not aligned") <<
                "10mnoalign" <<
                false <<
                false <<
                Time::Minute <<
                10 <<
                false;
        QTest::newRow("Positive minute fractional not aligned") <<
                "1.5mna" <<
                false <<
                false <<
                Time::Second <<
                90 <<
                false;
        QTest::newRow("Negative minute not aligned") <<
                "-10minutes no align" <<
                false <<
                true <<
                Time::Minute <<
                -10 <<
                false;
        QTest::newRow("Negative minute fractional not aligned") <<
                "-5.11minutes na" <<
                false <<
                true <<
                Time::Millisecond <<
                -306600 <<
                false;
        QTest::newRow("Positive hour not aligned") <<
                "10 hour no align" <<
                false <<
                false <<
                Time::Hour <<
                10 <<
                false;
        QTest::newRow("Positive hour fractional not aligned") <<
                "2.5 hourna" <<
                false <<
                false <<
                Time::Minute <<
                150 <<
                false;
        QTest::newRow("Negative hour not aligned") <<
                "-10hna" <<
                false <<
                true <<
                Time::Hour <<
                -10 <<
                false;
        QTest::newRow("Negative hour fractional not aligned") <<
                "-5.25hnoalign" <<
                false <<
                true <<
                Time::Minute <<
                -315 <<
                false;
        QTest::newRow("Positive day not aligned") <<
                "10dna" <<
                false <<
                false <<
                Time::Day <<
                10 <<
                false;
        QTest::newRow("Positive day fractional not aligned") <<
                "0.25 daysna" <<
                false <<
                false <<
                Time::Hour <<
                6 <<
                false;
        QTest::newRow("Negative day not aligned") <<
                "-10d no align" <<
                false <<
                true <<
                Time::Day <<
                -10 <<
                false;
        QTest::newRow("Negative day fractional not aligned") <<
                "-1.23daynoalign" <<
                false <<
                true <<
                Time::Second <<
                -106272 <<
                false;
        QTest::newRow("Positive week not aligned") <<
                "10wnoalign" <<
                false <<
                false <<
                Time::Week <<
                10 <<
                false;
        QTest::newRow("Positive week fractional not aligned") <<
                "2.5weekna" <<
                false <<
                false <<
                Time::Hour <<
                420 <<
                false;
        QTest::newRow("Negative week not aligned") <<
                "-10wna" <<
                false <<
                true <<
                Time::Week <<
                -10 <<
                false;
        QTest::newRow("Negative week fractional not aligned") <<
                "-5.25w no align" <<
                false <<
                true <<
                Time::Hour <<
                -882 <<
                false;
        QTest::newRow("Positive quarter not aligned") <<
                "1qna" <<
                false <<
                false <<
                Time::Quarter <<
                1 <<
                false;
        QTest::newRow("Negative quarter not aligned") <<
                "-3quarterna" <<
                false <<
                true <<
                Time::Quarter <<
                -3 <<
                false;
        QTest::newRow("Positive year not aligned") <<
                "10ynoalign" <<
                false <<
                false <<
                Time::Year <<
                10 <<
                false;
        QTest::newRow("Negative year not aligned") <<
                "-10yna" <<
                false <<
                true <<
                Time::Year <<
                -10 <<
                false;
    }


    void validateOffsetComponents()
    {
        QFETCH(QString, string);
        try { TimeParse::parseOffset(string, NULL, NULL, NULL, false, false); } catch (
                TimeParsingException tpe) { return; }
        QFAIL("Accepted invalid result");
    }

    void validateOffsetComponents_data()
    {
        QTest::addColumn<QString>("string");

        QTest::newRow("Garbage") << "asdasfds";
        QTest::newRow("Empty") << "";
        QTest::newRow("Zero") << "0";
        QTest::newRow("Zero millisecond") << "0msec";
        QTest::newRow("Zero second") << "0s";
        QTest::newRow("Zero minute") << "0m";
        QTest::newRow("Zero hour") << "0h";
        QTest::newRow("Zero day") << "0d";
        QTest::newRow("Zero week") << "0w";
        QTest::newRow("Zero month") << "0mo";
        QTest::newRow("Zero quarter") << "0q";
        QTest::newRow("Zero year") << "0y";
        QTest::newRow("Negative") << "-1";
        QTest::newRow("Negative millisecond") << "-1msec";
        QTest::newRow("Negative second") << "-1s";
        QTest::newRow("Negative minute") << "-1m";
        QTest::newRow("Negative hour") << "-1h";
        QTest::newRow("Negative day") << "-1d";
        QTest::newRow("Negative week") << "-1w";
        QTest::newRow("Negative month") << "-1mo";
        QTest::newRow("Negative quarter") << "-1q";
        QTest::newRow("Negative year") << "-1y";
        QTest::newRow("Fractional millisecond") << "2.5milliseconds";
        QTest::newRow("Fractional month") << "0.5mon";
        QTest::newRow("Fractional quarter") << "2.1quarter";
        QTest::newRow("Fractional year") << "32.1years";
        QTest::newRow("Aligned fractional second") << "1.1sa";
        QTest::newRow("Aligned fractional minute") << "2.6ma";
        QTest::newRow("Aligned fractional hour") << "0.5hour aligned";
        QTest::newRow("Aligned fractional day") << "3.1daya";
        QTest::newRow("Aligned fractional week") << "2.5wa";
    }

    void parseSingle()
    {
        QFETCH(QString, string);
        QFETCH(double, result);
        QFETCH(bool, testOffset);

        double tv;
        try { tv = TimeParse::parseTime(string); } catch (TimeParsingException tpe) {
            QFAIL(tpe.getDescription().toLatin1().data());
        }
        if (fabs(tv - result) >= 0.001) {
            QFAIL((QString("Parsed: ") +
                    QString::number(tv, 'f', 3) +
                    QString(" Expected: ") +
                    QString::number(result, 'f', 3)).toLatin1().data());
        }

        /* Check that offset modifiers function */
        if (testOffset) {
            try { tv = TimeParse::parseTime(string + " + 12s"); } catch (TimeParsingException tpe) {
                QFAIL(tpe.getDescription().toLatin1().data());
            }
            if (fabs(tv - (result + 12.0)) >= 0.001) {
                QFAIL((QString("Offset parsed: ") +
                        QString::number(tv + 12.0, 'f', 3) +
                        QString(" Expected: ") +
                        QString::number(result + 12.0, 'f', 3)).toLatin1().data());
            }
        }
    }

    void parseSingle_data()
    {
        QTest::addColumn<QString>("string");
        QTest::addColumn<double>("result");
        QTest::addColumn<bool>("testOffset");

        QTest::newRow("Epoch time") << "123456789" << 123456789.0 << true;
        QTest::newRow("Epoch time explicit") << "E:1995" << 1995.0 << true;
        QTest::newRow("Date-like epoch time") << "200101019" << 200101019.0 << true;
        QTest::newRow("Year-like epoch time") << "200188800" << 200188800.0 << true;

        /* ISO Datetime */
        QTest::newRow("Full ISO datetime") << "2010-03-04T02:06:01Z" << 1267668361.0 << true;
        QTest::newRow("Full ISO datetime with msecs") <<
                "2010-03-04T02:06:01.154Z" <<
                1267668361.154 <<
                true;
        QTest::newRow("Full datetime non-delimited") << "20100304T020601Z" << 1267668361.0 << true;
        QTest::newRow("Date only") << "1998-12-01" << 912470400.0 << true;
        QTest::newRow("Date with partial time") << "2003/01/01 23:15" << 1041462900.0 << true;
        QTest::newRow("Date with time no zero padding") << "2001/4/3 6:7:3" << 986278023.0 << true;
        QTest::newRow("Date only on leap year") << " 2004-02-29" << 1078012800.0 << true;

        /* ISO Weeks */
        QTest::newRow("Week") << "2009W4" << 1232323200.0 << true;
        QTest::newRow("Week and day of week") << "1995W23-3" << 802483200.0 << true;

        /* Quarter */
        QTest::newRow("Quarter") << "2003Q3" << 1057017600.0 << true;

        /* Year and DOY */
        QTest::newRow("Year and DOY") << "2010:123.12356" << 1272855476.0 << true;
        QTest::newRow("Year and DOY no separator") << "2010123.12356" << 1272855476.0 << true;
        QTest::newRow("Year and DOY integer") << "2003,1" << 1041379200.0 << true;
        QTest::newRow("Year and DOY hour round up") << "2003  1.12" << 1041390000.0 << true;
        QTest::newRow("Year and DOY hour round down") << "1997 1.21" << 852094800.0 << true;
        QTest::newRow("Year and DOY fractional") << "2003-001.123" << 1041389827.2 << true;
        QTest::newRow("Year and DOY second rounding lower") <<
                "2005,45.65348" <<
                1108395661.0 <<
                true;
        QTest::newRow("Year and DOY second rounding upper") <<
                "2005;45.65349" <<
                1108395661.0 <<
                true;

        /* Time offsets */
        QTest::newRow("Offset base") << "1998-11-15T14:39:10Z" << 911140750.0 << false;
        QTest::newRow("Offset positive milliseconds") <<
                "1998-11-15T14:39:10Z + 931msec" <<
                911140750.931 <<
                false;
        QTest::newRow("Offset positive millisecond single") <<
                "1998-11-15T14:39:10Z + msec" <<
                911140750.001 <<
                false;
        QTest::newRow("Offset negative milliseconds") <<
                "1998-11-15T14:39:10Z - 200 milliseconds" <<
                911140749.800 <<
                false;
        QTest::newRow("Offset negative millisecond single") <<
                "1998-11-15T14:39:10Z - millisecond" <<
                911140749.999 <<
                false;
        QTest::newRow("Offset positive seconds") <<
                "1998-11-15T14:39:10Z + 1second" <<
                911140751.0 <<
                false;
        QTest::newRow("Offset positive second single") <<
                "1998-11-15T14:39:10Z + sec" <<
                911140751.0 <<
                false;
        QTest::newRow("Offset positive seconds fractional") <<
                "1998-11-15T14:39:10Z + 1.5seconds" <<
                911140751.5 <<
                false;
        QTest::newRow("Offset negative seconds") <<
                "1998-11-15T14:39:10Z-3963s" <<
                911136787.0 <<
                false;
        QTest::newRow("Offset negative second single") <<
                "1998-11-15T14:39:10Z - second" <<
                911140749.0 <<
                false;
        QTest::newRow("Offset negative seconds fractional") <<
                "1998-11-15T14:39:10Z-3962.25s" <<
                911136787.75 <<
                false;
        QTest::newRow("Offset positive minutes") <<
                "1998-11-15T14:39:10Z + 2min" <<
                911140870.0 <<
                false;
        QTest::newRow("Offset positive minute single") <<
                "1998-11-15T14:39:10Z + m" <<
                911140810.0 <<
                false;
        QTest::newRow("Offset positive minutes fractional") <<
                "1998-11-15T14:39:10Z + 3.5minutes" <<
                911140960.0 <<
                false;
        QTest::newRow("Offset negative minutes") <<
                "1998-11-15T14:39:10Z-10m" <<
                911140150.0 <<
                false;
        QTest::newRow("Offset negative minute single") <<
                "1998-11-15T14:39:10Z - minute" <<
                911140690.0 <<
                false;
        QTest::newRow("Offset negative minutes fractional") <<
                "1998-11-15T14:39:10Z-2.5min" <<
                911140600.0 <<
                false;
        QTest::newRow("Offset positive hours") <<
                "1998-11-15T14:39:10Z + 100h" <<
                911500750.0 <<
                false;
        QTest::newRow("Offset positive hour single") <<
                "1998-11-15T14:39:10Z + hour" <<
                911144350.0 <<
                false;
        QTest::newRow("Offset positive hours fractional") <<
                "1998-11-15T14:39:10Z + 0.5hours" <<
                911142550.0 <<
                false;
        QTest::newRow("Offset negative hours") << "1998-11-15T14:39:10Z-2h" << 911133550.0 << false;
        QTest::newRow("Offset negative hour single") <<
                "1998-11-15T14:39:10Z-h" <<
                911137150.0 <<
                false;
        QTest::newRow("Offset negative hours fractional") <<
                "1998-11-15T14:39:10Z - 1.25hour" <<
                911136250.0 <<
                false;
        QTest::newRow("Offset positive days") <<
                "1998-11-15T14:39:10Z + 1000d" <<
                997540750.0 <<
                false;
        QTest::newRow("Offset positive day single") <<
                "1998-11-15T14:39:10Z+day" <<
                911227150.0 <<
                false;
        QTest::newRow("Offset positive days fractional") <<
                "1998-11-15T14:39:10Z + 1.75 d" <<
                911291950.0 <<
                false;
        QTest::newRow("Offset negative days") <<
                "1998-11-15T14:39:10Z - 12days" <<
                910103950.0 <<
                false;
        QTest::newRow("Offset negative day single") <<
                "1998-11-15T14:39:10Z - d" <<
                911054350.0 <<
                false;
        QTest::newRow("Offset negative days frational") <<
                "1998-11-15T14:39:10Z - 10.5 day" <<
                910233550.0 <<
                false;
        QTest::newRow("Offset positive weeks") <<
                "1998-11-15T14:39:10Z + 1 week" <<
                911745550.0 <<
                false;
        QTest::newRow("Offset positive week single") <<
                "1998-11-15T14:39:10Z + week" <<
                911745550.0 <<
                false;
        QTest::newRow("Offset positive weeks fractional") <<
                "1998-11-15T14:39:10Z + 2.5 w" <<
                912652750.0 <<
                false;
        QTest::newRow("Offset negative weeks") <<
                "1998-11-15T14:39:10Z-10w" <<
                905092750.0 <<
                false;
        QTest::newRow("Offset negative week single") <<
                "1998-11-15T14:39:10Z - w" <<
                910535950.0 <<
                false;
        QTest::newRow("Offset negative weeks fractional") <<
                "1998-11-15T14:39:10Z-4.25week" <<
                908570350.0 <<
                false;
        QTest::newRow("Offset positive months") <<
                "1998-11-15T14:39:10Z + 1 month" <<
                913732750.0 <<
                false;
        QTest::newRow("Offset positive month single") <<
                "1998-11-15T14:39:10Z + mo" <<
                913732750.0 <<
                false;
        QTest::newRow("Offset negative months") <<
                "1998-11-15T14:39:10Z - 14 mons" <<
                874334350.0 <<
                false;
        QTest::newRow("Offset negative month single") <<
                "1998-11-15T14:39:10Z - month" <<
                908462350.0 <<
                false;
        QTest::newRow("Offset positive quarters") <<
                "1998-11-15T14:39:10Z + 3q" <<
                934727950.0 <<
                false;
        QTest::newRow("Offset positive quarter single") <<
                "1998-11-15T14:39:10Z+q" <<
                919089550.0 <<
                false;
        QTest::newRow("Offset negative quarters") <<
                "1998-11-15T14:39:10Z - 1 quarter" <<
                903191950.0 <<
                false;
        QTest::newRow("Offset negative quarter single") <<
                "1998-11-15T14:39:10Z - quarter" <<
                903191950.0 <<
                false;
        QTest::newRow("Offset positive years") <<
                "1998-11-15T14:39:10Z + 10y" <<
                1226759950.0 <<
                false;
        QTest::newRow("Offset positive year single") <<
                "1998-11-15T14:39:10Z + y" <<
                942676750.0 <<
                false;
        QTest::newRow("Offset negative years") <<
                "1998-11-15T14:39:10Z - 2 years" <<
                848068750.0 <<
                false;
        QTest::newRow("Offset negative year single") <<
                "1998-11-15T14:39:10Z-year" <<
                879604750.0 <<
                false;

        QTest::newRow("ISO8601 Timezone") << "2014-01-01T00:00:00+01" << 1388530800.0 << false;
        QTest::newRow("ISO8601 Timezone Minute") <<
                "2014-01-01T00:00:00-0230" <<
                1388543400.0 <<
                false;
    }

    void parseSingleUndefined()
    {
        QFETCH(QString, string);

        double tv;
        try {
            TestTimeParseReference ref(0, true, true);
            tv = TimeParse::parseTime(string, &ref);
        } catch (TimeParsingException tpe) {
            QFAIL(tpe.getDescription().toLatin1().data());
        }
        QVERIFY(!FP::defined(tv));
    }

    void parseSingleUndefined_data()
    {
        QTest::addColumn<QString>("string");

        QTest::newRow("Zero length") << "";
        QTest::newRow("Spaces") << "  ";
        QTest::newRow("0") << "0";
        QTest::newRow("None") << " none   ";
        QTest::newRow("Undef") << "undef ";
        QTest::newRow("Undefined") << " undefined";
        QTest::newRow("Inf") << "inf";
        QTest::newRow("Infinity") << "Infinity";
        QTest::newRow("All") << "ALL";
        QTest::newRow("Forever") << "forever";
    }

    void validateSingle()
    {
        QFETCH(QString, string);

        try { TimeParse::parseTime(string); } catch (TimeParsingException tpe) { return; }
        QFAIL("Accepted invalid result");
    }

    void validateSingle_data()
    {
        QTest::addColumn<QString>("string");

        /* Infinity without the allow flag set */
        QTest::newRow("Infinity without flag 1") << "none";
        QTest::newRow("Infinity without flag 2") << "";

        /* Datetime */
        QTest::newRow("Date year too small") << "1800-05-08";
        QTest::newRow("Date year too large") << "4324-05-08";
        QTest::newRow("Date month too large") << "2010-13-08";
        QTest::newRow("Date day of month too large") << "2010-01-32";
        QTest::newRow("Date day of month invalid (non-leap)") << "2010-02-29";
        QTest::newRow("Date day of month invalid (leap)") << "2004-02-30";
        QTest::newRow("Datetime hour too large") << "2010-02-08T24:00:00Z";
        QTest::newRow("Datetime minute too large") << "2010-02-08T15:60:00Z";
        QTest::newRow("Datetime second too large") << "2010-02-08T15:32:61Z";
        QTest::newRow("Datetime invalid msec") << "2010-02-08T15:32:15.Z";
        QTest::newRow("Date with trailing garbage") << "2010-01-08 Blarg";

        /* Week */
        QTest::newRow("Week year too small") << "1500W1";
        QTest::newRow("Week year too large") << "9423W32";
        QTest::newRow("Week too small") << "W0";
        QTest::newRow("Week too large") << "2010W54";
        QTest::newRow("Week day of week too small") << "2010W12-0";
        QTest::newRow("Week day of week too large") << "2010W12-8";

        /* Quarter */
        QTest::newRow("Quarter year too small") << "1300Q1";
        QTest::newRow("Quarter year too large") << "8123Q3";
        QTest::newRow("Quarter too small") << "2010Q0";
        QTest::newRow("Quarter too large") << "2011Q5";
        QTest::newRow("Quarter multiple digit") << "2011Q10";

        /* Year and DOY */
        QTest::newRow("Year and DOY year too large") << "5343:12";
        QTest::newRow("Year and DOY year too small") << "1790:65";
        QTest::newRow("Year and DOY doy too small") << "2001:0";
        QTest::newRow("Year and DOY doy too large") << "2001:367";

        /* Unavailable fractions */
        QTest::newRow("Fractional milliseconds") << "1998-11-15T14:39:10Z+1.5msec";
        QTest::newRow("Fractional months") << "1998-11-15T14:39:10Z-0.5mo";
        QTest::newRow("Fractional quarters") << "1998-11-15T14:39:10Z-5.3q";
        QTest::newRow("Fractional years") << "1998-11-15T14:39:10Z+1.5y";

        QTest::newRow("Garbage") << "Blarg";
    }

    void parseRelative()
    {
        QFETCH(QString, string);
        QFETCH(TestTimeParseReference, reference);
        QFETCH(double, result);
        QFETCH(Time::LogicalTimeUnit, unit);

        double tv;
        Time::LogicalTimeUnit uv = Time::Second;
        try {
            tv = TimeParse::parseTime(string, &reference, &uv);
        } catch (TimeParsingException tpe) {
            QFAIL(tpe.getDescription().toLatin1().data());
        }
        if (fabs(tv - result) >= 0.001) {
            QFAIL((QString("Parsed: ") +
                    QString::number(tv, 'f', 3) +
                    QString(" Expected: ") +
                    QString::number(result, 'f', 3)).toLatin1().data());
        }
        QCOMPARE(uv, unit);

        try { tv = TimeParse::parseTime(string, &reference); } catch (TimeParsingException tpe) {
            QFAIL(tpe.getDescription().toLatin1().data());
        }
        if (fabs(tv - result) >= 0.001) {
            QFAIL((QString("Parsed: ") +
                    QString::number(tv, 'f', 3) +
                    QString(" Expected: ") +
                    QString::number(result, 'f', 3)).toLatin1().data());
        }
    }

    void parseRelative_data()
    {
        QTest::addColumn<QString>("string");
        QTest::addColumn<TestTimeParseReference>("reference");
        QTest::addColumn<double>("result");
        QTest::addColumn<Time::LogicalTimeUnit>("unit");

        /* 2010-05-13T18:17:26Z */
        TestTimeParseReference leading = TestTimeParseReference(1273774646.0, true);
        TestTimeParseReference trailing = TestTimeParseReference(1273774646.0, false);

        /* Offsets, extensive testing taken care of above */
        QTest::newRow("Offset second trailing") <<
                "542s" <<
                trailing <<
                1273775188.0 <<
                Time::Second;
        QTest::newRow("Offset hour leading") << "1 hour" << leading << 1273771046.0 << Time::Hour;

        /* Time */
        QTest::newRow("Time leading before") <<
                "05:03:23" <<
                leading <<
                1273727003.0 <<
                Time::Second;
        QTest::newRow("Time leading after") <<
                "  20:32:01" <<
                leading <<
                1273696321.0 <<
                Time::Second;
        QTest::newRow("Time trailing before") <<
                "01:46:12Z" <<
                trailing <<
                1273801572.0 <<
                Time::Second;
        QTest::newRow("Time trailing after") <<
                "21:15:19.34" <<
                trailing <<
                1273785319.34 <<
                Time::Millisecond;
        QTest::newRow("Time minute") << "02:15" << leading << 1273716900.0 << Time::Minute;

        /* Week */
        QTest::newRow("Week leading") << "2010W03" << leading << 1263772800.0 << Time::Week;
        QTest::newRow("Week trailing") << "2010W20" << trailing << 1274659200.0 << Time::Week;
        QTest::newRow("Week day leading") << "2010W02-2" << leading << 1263254400.0 << Time::Week;
        QTest::newRow("Week day trailing") << "2010W45-5" << trailing << 1289606400.0 << Time::Week;
        QTest::newRow("Week leading before") << "W19" << leading << 1273449600.0 << Time::Week;
        QTest::newRow("Week leading after") << "W20" << leading << 1242000000.0 << Time::Week;
        QTest::newRow("Week trailing before") << "W18" << trailing << 1304899200.0 << Time::Week;
        QTest::newRow("Week trailing after") << "W19" << trailing << 1274054400.0 << Time::Week;

        /* Quarter */
        QTest::newRow("Quarter leading") << "2003Q1" << leading << 1041379200.0 << Time::Quarter;
        QTest::newRow("Quarter trailing") << "2010Q2" << trailing << 1277942400.0 << Time::Quarter;
        QTest::newRow("Quarter leading before") << "Q2" << leading << 1270080000.0 << Time::Quarter;
        QTest::newRow("Quarter leading after") << "Q3" << leading << 1246406400.0 << Time::Quarter;
        QTest::newRow("Quarter trailing before") <<
                "Q1" <<
                trailing <<
                1301616000.0 <<
                Time::Quarter;
        QTest::newRow("Quarter trailing after") <<
                "Q2" <<
                trailing <<
                1277942400.0 <<
                Time::Quarter;

        /* DOY */
        QTest::newRow("DOY leading before") <<
                "133.76210" <<
                leading <<
                1273774645.0 <<
                Time::Second;
        QTest::newRow("DOY leading after") << "134" << leading << 1242259200.0 << Time::Day;
        QTest::newRow("DOY trailing before") << "133.04" << trailing << 1305248400.0 << Time::Hour;
        QTest::newRow("DOY trailing after") << "134" << trailing << 1273795200.0 << Time::Day;
    }

    void validateRelative()
    {
        QFETCH(QString, string);
        QFETCH(TestTimeParseReference, reference);

        try {
            TimeParse::parseTime(string, &reference);
        } catch (TimeParsingException tpe) { return; }
        QFAIL("Accepted invalid result");
    }

    void validateRelative_data()
    {
        QTest::addColumn<QString>("string");
        QTest::addColumn<TestTimeParseReference>("reference");

        TestTimeParseReference leading = TestTimeParseReference(1273774646.0, true);
        TestTimeParseReference trailing = TestTimeParseReference(1273774646.0, false);

        /* Relative offsets, detailed handlers above */
        QTest::newRow("Zero year relative") << "0y" << leading;

        /* Time */
        QTest::newRow("Time hour too large") << "24:01:02" << leading;
        QTest::newRow("Time minute too large") << "10:60:02" << leading;
        QTest::newRow("Time second too large") << "10:20:61" << leading;
    }


    void parseSingleList()
    {
        QFETCH(QStringList, list);
        QFETCH(TestTimeParseReference, reference);
        QFETCH(TestTimeParseHandler, handler);
        QFETCH(double, result);
        QFETCH(Time::LogicalTimeUnit, unit);
        QFETCH(Time::LogicalTimeUnit, defaultUnit);

        double tv;
        Time::LogicalTimeUnit uv = Time::Second;
        try {
            tv = TimeParse::parseListSingle(list, &reference, &handler, false, &uv, defaultUnit);
        } catch (TimeParsingException tpe) {
            QFAIL(tpe.getDescription().toLatin1().data());
        }
        if (fabs(tv - result) >= 0.001) {
            QFAIL((QString("Parsed: ") +
                    QString::number(tv, 'f', 3) +
                    QString(" Expected: ") +
                    QString::number(result, 'f', 3)).toLatin1().data());
        }
        QCOMPARE(uv, unit);
    }

    void parseSingleList_data()
    {
        QTest::addColumn<QStringList>("list");
        QTest::addColumn<TestTimeParseReference>("reference");
        QTest::addColumn<TestTimeParseHandler>("handler");
        QTest::addColumn<double>("result");
        QTest::addColumn<Time::LogicalTimeUnit>("unit");
        QTest::addColumn<Time::LogicalTimeUnit>("defaultUnit");

        TestTimeParseReference reference = TestTimeParseReference(1042502400.0, true);

        QStringList list;

        list.clear();
        list.append("Arg1");
        list.append("2Arg");
        list.append("2015:123");
        list.append("3Tail");
        QTest::newRow("Leading and trailing") <<
                list <<
                reference <<
                TestTimeParseHandler(2, 1) <<
                1430611200.0 <<
                Time::Day <<
                Time::Day;

        list.clear();
        list.append("2015:123");
        list.append("3Tail");
        list.append("Blarg");
        QTest::newRow("Trailing only") <<
                list <<
                reference <<
                TestTimeParseHandler(0, 2) <<
                1430611200.0 <<
                Time::Day <<
                Time::Day;

        list.clear();
        list.append("Start");
        list.append("2015:123");
        QTest::newRow("Leading only") <<
                list <<
                reference <<
                TestTimeParseHandler(1, 0) <<
                1430611200.0 <<
                Time::Day <<
                Time::Day;

        list.clear();
        list.append("2015:123");
        QTest::newRow("Time only") <<
                list <<
                reference <<
                TestTimeParseHandler(0, 0) <<
                1430611200.0 <<
                Time::Day <<
                Time::Day;

        list.clear();
        list.append("1997");
        list.append("65");
        QTest::newRow("Year DOY only") <<
                list <<
                reference <<
                TestTimeParseHandler(0, 0) <<
                857606400.0 <<
                Time::Day <<
                Time::Day;

        list.clear();
        list.append("Foo");
        list.append("Bar");
        list.append("1997");
        list.append("65");
        list.append("Baz");
        list.append("Zap");
        QTest::newRow("Year DOY leading and trailing") <<
                list <<
                reference <<
                TestTimeParseHandler(2, 2) <<
                857606400.0 <<
                Time::Day <<
                Time::Day;

        list.clear();
        list.append("2005");
        list.append("13");
        QTest::newRow("Year week") <<
                list <<
                reference <<
                TestTimeParseHandler(0, 0) <<
                1111968000.0 <<
                Time::Week <<
                Time::Week;

        list.clear();
        list.append("2005");
        list.append("w13");
        QTest::newRow("Year week manual") <<
                list <<
                reference <<
                TestTimeParseHandler(0, 0) <<
                1111968000.0 <<
                Time::Week <<
                Time::Day;

        list.clear();
        list.append("2001");
        list.append("2");
        QTest::newRow("Year quarter") <<
                list <<
                reference <<
                TestTimeParseHandler(0, 0) <<
                986083200.0 <<
                Time::Quarter <<
                Time::Quarter;

        list.clear();
        list.append("2001");
        list.append("Q2");
        QTest::newRow("Year quarter manual") <<
                list <<
                reference <<
                TestTimeParseHandler(0, 0) <<
                986083200.0 <<
                Time::Quarter <<
                Time::Day;

        list.clear();
        list.append("2013");
        list.append("01");
        list.append("2");
        list.append("11");
        list.append("50");
        list.append("29");
        QTest::newRow("Datetime only") <<
                list <<
                reference <<
                TestTimeParseHandler(0, 0) <<
                1357127429.0 <<
                Time::Second <<
                Time::Day;

        list.clear();
        list.append("13");
        list.append("01");
        list.append("2");
        list.append("11");
        list.append("50");
        list.append("29");
        QTest::newRow("Datetime short year") <<
                list <<
                reference <<
                TestTimeParseHandler(0, 0) <<
                1357127429.0 <<
                Time::Second <<
                Time::Day;
    }

    void parseSingleListZero()
    {
        QFETCH(QStringList, list);
        QFETCH(TestTimeParseReference, reference);
        QFETCH(TestTimeParseHandler, handler);

        double tv;
        try {
            tv = TimeParse::parseListSingle(list, &reference, &handler, true);
        } catch (TimeParsingException tpe) {
            QFAIL(tpe.getDescription().toLatin1().data());
        }
        if (FP::defined(tv))
            QFAIL("Expected undefined result.");
    }

    void parseSingleListZero_data()
    {
        QTest::addColumn<QStringList>("list");
        QTest::addColumn<TestTimeParseReference>("reference");
        QTest::addColumn<TestTimeParseHandler>("handler");

        TestTimeParseReference reference = TestTimeParseReference(1042502400.0, true, true);

        QStringList list;

        list.clear();
        QTest::newRow("Empty list") << list << reference << TestTimeParseHandler(0, 0);

        list.clear();
        list.append("Arg1");
        list.append("2Arg");
        list.append("3Tail");

        QTest::newRow("Leading and trailing") << list << reference << TestTimeParseHandler(2, 1);
        QTest::newRow("Leading only") << list << reference << TestTimeParseHandler(3, 0);
        QTest::newRow("Trailing only") << list << reference << TestTimeParseHandler(0, 3);

        list.clear();
        list.append("Arg1");
        QTest::newRow("Single leading") << list << reference << TestTimeParseHandler(1, 0);
        QTest::newRow("Single trailing") << list << reference << TestTimeParseHandler(0, 1);
    }

    void validateSingleList()
    {
        QFETCH(QStringList, list);
        QFETCH(TestTimeParseReference, reference);
        QFETCH(TestTimeParseHandler, handler);

        try { TimeParse::parseListSingle(list, &reference, &handler, false); } catch (
                TimeParsingException tpe) { return; }
        QFAIL("Accepted invalid result");
    }

    void validateSingleList_data()
    {
        QTest::addColumn<QStringList>("list");
        QTest::addColumn<TestTimeParseReference>("reference");
        QTest::addColumn<TestTimeParseHandler>("handler");

        TestTimeParseReference reference = TestTimeParseReference(1042502400.0, true, false);

        QStringList list;

        list.clear();
        QTest::newRow("Empty list") << list << reference << TestTimeParseHandler(0, 0);

        list.clear();
        list.append("Arg1");
        list.append("2Arg");
        list.append("3Tail");

        QTest::newRow("Leading and trailing") << list << reference << TestTimeParseHandler(2, 1);
        QTest::newRow("Leading only") << list << reference << TestTimeParseHandler(3, 0);
        QTest::newRow("Trailing only") << list << reference << TestTimeParseHandler(0, 3);

        list.clear();
        list.append("Arg1");
        QTest::newRow("Single leading") << list << reference << TestTimeParseHandler(1, 0);
        QTest::newRow("Single trailing") << list << reference << TestTimeParseHandler(0, 1);
        QTest::newRow("Invalid time") << list << reference << TestTimeParseHandler(0, 0);

        list.clear();
        list.append("1235");
        list.append("12");
        QTest::newRow("Year too small") << list << reference << TestTimeParseHandler(0, 0);

        list.clear();
        list.append("8654");
        list.append("89");
        QTest::newRow("Year too large") << list << reference << TestTimeParseHandler(0, 0);

        list.clear();
        list.append("2004");
        list.append("0");
        QTest::newRow("DOY too small") << list << reference << TestTimeParseHandler(0, 0);

        list.clear();
        list.append("2004");
        list.append("368");
        QTest::newRow("DOY too large") << list << reference << TestTimeParseHandler(0, 0);

        list.clear();
        list.append("2004");
        list.append("Q0");
        QTest::newRow("Quarter too small") << list << reference << TestTimeParseHandler(0, 0);

        list.clear();
        list.append("2004");
        list.append("Q5");
        QTest::newRow("Quarter too large") << list << reference << TestTimeParseHandler(0, 0);

        list.clear();
        list.append("2004");
        list.append("w0");
        QTest::newRow("Week too small") << list << reference << TestTimeParseHandler(0, 0);

        list.clear();
        list.append("2004");
        list.append("W55");
        QTest::newRow("Week too large") << list << reference << TestTimeParseHandler(0, 0);
    }


    void parseBound()
    {
        QFETCH(QString, start);
        QFETCH(QString, end);
        QFETCH(bool, allowUndefined);
        QFETCH(Time::Bounds, result);

        Time::Bounds b;
        try {
            b = TimeParse::parseTimeBounds(start, end, allowUndefined);
        } catch (TimeParsingException tpe) {
            QFAIL(tpe.getDescription().toLatin1().data());
        }
        if (!FP::defined(result.start)) {
            if (FP::defined(b.start))
                QFAIL("Unexpected defined start");
        } else if (!FP::defined(b.start)) {
            QFAIL("Unexpected undefined start");
        } else if (fabs(result.start - b.start) >= 0.001) {
            QFAIL((QString("Start Parsed: ") +
                    QString::number(b.start, 'f', 3) +
                    QString(" Expected: ") +
                    QString::number(result.start, 'f', 3)).toLatin1().data());
        }
        if (!FP::defined(result.end)) {
            if (FP::defined(b.end))
                QFAIL("Unexpected defined end");
        } else if (!FP::defined(b.end)) {
            QFAIL("Unexpected undefined end");
        } else if (fabs(result.end - b.end) >= 0.001) {
            QFAIL((QString("End Parsed: ") +
                    QString::number(b.end, 'f', 3) +
                    QString(" Expected: ") +
                    QString::number(result.end, 'f', 3)).toLatin1().data());
        }
    }

    void parseBound_data()
    {
        QTest::addColumn<QString>("start");
        QTest::addColumn<QString>("end");
        QTest::addColumn<bool>("allowUndefined");
        QTest::addColumn<Time::Bounds>("result");

        QTest::newRow("Two year/doy") <<
                "2010:1" <<
                "2010:2" <<
                false <<
                Time::Bounds(1262304000, 1262390400);
        QTest::newRow("End relative") <<
                "2010:1" <<
                "1d" <<
                false <<
                Time::Bounds(1262304000, 1262390400);
        QTest::newRow("Start relative") <<
                "1d" <<
                "2010:2" <<
                false <<
                Time::Bounds(1262304000, 1262390400);

        QTest::newRow("Implied year DOY") <<
                "2003:12" <<
                "54" <<
                false <<
                Time::Bounds(1042329600, 1045958400);
        QTest::newRow("Implied date") <<
                "1995-10-13T03:12:15Z" <<
                "12:54:16" <<
                false <<
                Time::Bounds(813553935, 813588856);

        QTest::newRow("Both undefined") <<
                "0" <<
                "inf" <<
                true <<
                Time::Bounds(FP::undefined(), FP::undefined());
        QTest::newRow("Start undefined") <<
                "all" <<
                "2010:2" <<
                true <<
                Time::Bounds(FP::undefined(), 1262390400);
        QTest::newRow("End undefined") <<
                "2010:1" <<
                "none" <<
                true <<
                Time::Bounds(1262304000.0, FP::undefined());
    }

    void validateBound()
    {
        QFETCH(QString, start);
        QFETCH(QString, end);
        QFETCH(bool, allowUndefined);

        try {
            TimeParse::parseTimeBounds(start, end, allowUndefined);
        } catch (TimeParsingException tpe) { return; }
        QFAIL("Accepted invalid result");
    }

    void validateBound_data()
    {
        QTest::addColumn<QString>("start");
        QTest::addColumn<QString>("end");
        QTest::addColumn<bool>("allowUndefined");

        QTest::newRow("Both relative") << "32d" << "4d" << false;
        QTest::newRow("End after start") << "2010:10" << "2010:8" << false;
        QTest::newRow("Undefined start") << "none" << "2010:8" << false;
        QTest::newRow("Undefined end") << "1999:129" << "none" << false;
        QTest::newRow("End relative to undefined start") << "0" << "1h" << true;
        QTest::newRow("Start relative to undefined end") << "1m" << "0" << true;
    }

    void parseBoundList()
    {
        QFETCH(QStringList, list);
        QFETCH(TestTimeParseHandler, handler);
        QFETCH(bool, allowZeroLength);
        QFETCH(bool, allowUndefined);
        QFETCH(Time::LogicalTimeUnit, defaultUnit);
        QFETCH(Time::Bounds, result);

        Time::Bounds b;
        try {
            b = TimeParse::parseListBounds(list, &handler,
                                           Time::Bounds(FP::undefined(), FP::undefined()),
                                           allowZeroLength, allowUndefined, defaultUnit,
                                           1276091074.0);
        } catch (TimeParsingException tpe) {
            QFAIL(tpe.getDescription().toLatin1().data());
        }
        if (!FP::defined(result.start)) {
            if (FP::defined(b.start))
                QFAIL("Unexpected defined start");
        } else if (!FP::defined(b.start)) {
            QFAIL("Unexpected undefined start");
        } else if (fabs(result.start - b.start) >= 0.001) {
            QFAIL((QString("Start Parsed: ") +
                    QString::number(b.start, 'f', 3) +
                    QString(" Expected: ") +
                    QString::number(result.start, 'f', 3)).toLatin1().data());
        }
        if (!FP::defined(result.end)) {
            if (FP::defined(b.end))
                QFAIL("Unexpected defined end");
        } else if (!FP::defined(b.end)) {
            QFAIL("Unexpected undefined end");
        } else if (fabs(result.end - b.end) >= 0.001) {
            QFAIL((QString("End Parsed: ") +
                    QString::number(b.end, 'f', 3) +
                    QString(" Expected: ") +
                    QString::number(result.end, 'f', 3)).toLatin1().data());
        }
    }

    void parseBoundList_data()
    {
        QTest::addColumn<QStringList>("list");
        QTest::addColumn<TestTimeParseHandler>("handler");
        QTest::addColumn<bool>("allowZeroLength");
        QTest::addColumn<bool>("allowUndefined");
        QTest::addColumn<Time::LogicalTimeUnit>("defaultUnit");
        QTest::addColumn<Time::Bounds>("result");

        QStringList list;

        list.clear();
        list.append("Arg1");
        list.append("2Arg");
        list.append("2011:123");
        list.append("2011:125");
        list.append("3Tail");
        QTest::newRow("Leading and trailing") <<
                list <<
                TestTimeParseHandler(2, 1) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1304380800, 1304553600);
        QTest::newRow("Leading and trailing single") <<
                list <<
                TestTimeParseHandler(2, 2) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1304380800, 1304467200);
        QTest::newRow("Leading and trailing zero") <<
                list <<
                TestTimeParseHandler(3, 2) <<
                true <<
                true <<
                Time::Day <<
                Time::Bounds(FP::undefined(), FP::undefined());
        QTest::newRow("Leading only zero") <<
                list <<
                TestTimeParseHandler(5, 0) <<
                true <<
                true <<
                Time::Day <<
                Time::Bounds(FP::undefined(), FP::undefined());
        QTest::newRow("Trailing only zero") <<
                list <<
                TestTimeParseHandler(0, 5) <<
                true <<
                true <<
                Time::Day <<
                Time::Bounds(FP::undefined(), FP::undefined());

        list.clear();
        list.append("2001W3");
        list.append("12");
        list.append("3Tail");
        QTest::newRow("Trailing week") <<
                list <<
                TestTimeParseHandler(0, 1) <<
                false <<
                false <<
                Time::Week <<
                Time::Bounds(979516800, 985564800);
        QTest::newRow("Trailing week single") <<
                list <<
                TestTimeParseHandler(0, 2) <<
                false <<
                false <<
                Time::Week <<
                Time::Bounds(979516800, 980121600);
        QTest::newRow("Week only") <<
                list <<
                TestTimeParseHandler(1, 2) <<
                false <<
                false <<
                Time::Week <<
                Time::Bounds(1269216000, 1269820800);
        list.replace(1, "W12");
        QTest::newRow("Trailing week day arg") <<
                list <<
                TestTimeParseHandler(0, 1) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(979516800, 985564800);

        list.clear();
        list.append("17");
        list.append("1990W31");
        QTest::newRow("Start week") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Week <<
                Time::Bounds(640828800, 649900800);
        list.replace(0, "W17");
        QTest::newRow("Start week day arg") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(640828800, 649900800);

        list.clear();
        list.append("Blarg");
        list.append("2004Q1");
        list.append("2");
        QTest::newRow("Leading quarter") <<
                list <<
                TestTimeParseHandler(1, 0) <<
                false <<
                false <<
                Time::Quarter <<
                Time::Bounds(1072915200, 1088553600);
        QTest::newRow("Year and quarter only") <<
                list <<
                TestTimeParseHandler(1, 1) <<
                false <<
                false <<
                Time::Quarter <<
                Time::Bounds(1072915200, 1080691200);
        QTest::newRow("Leading quarter only") <<
                list <<
                TestTimeParseHandler(2, 0) <<
                false <<
                false <<
                Time::Quarter <<
                Time::Bounds(1270080000, 1277942400);
        list.replace(2, "Q2");
        QTest::newRow("Leading quarter only day arg") <<
                list <<
                TestTimeParseHandler(2, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1270080000, 1277942400);

        list.clear();
        list.append("2");
        list.append("1995Q3");
        QTest::newRow("Start quarter") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Quarter <<
                Time::Bounds(796694400, 812505600);
        list.replace(0, "Q2");
        QTest::newRow("Start quarter day arg") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(796694400, 812505600);

        list.clear();
        list.append("Blarg");
        list.append("14");
        QTest::newRow("Leading week only") <<
                list <<
                TestTimeParseHandler(1, 0) <<
                false <<
                false <<
                Time::Week <<
                Time::Bounds(1270425600, 1271030400);
        list.replace(1, "W14");
        QTest::newRow("Leading week only day arg") <<
                list <<
                TestTimeParseHandler(1, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1270425600, 1271030400);

        list.clear();
        list.append("4");
        list.append("Foo");
        list.append("Bar");
        QTest::newRow("Trailing quarter only") <<
                list <<
                TestTimeParseHandler(0, 2) <<
                false <<
                false <<
                Time::Quarter <<
                Time::Bounds(1285891200, 1293840000);
        list.replace(0, "Q4");
        QTest::newRow("Trailing quarter only day arg") <<
                list <<
                TestTimeParseHandler(0, 2) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1285891200, 1293840000);

        list.clear();
        list.append("120");
        QTest::newRow("Single DOY") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1272585600, 1272672000);

        list.clear();
        list.append("90");
        list.append("100");
        QTest::newRow("Two DOY") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1269993600, 1270857600);

        list.clear();
        list.append("2d");
        QTest::newRow("Relative from reference") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1275918274, 1276004674);

        list.clear();
        list.append("95+2h");
        QTest::newRow("Single DOY and offset") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1270432800, 1270519200);

        list.clear();
        list.append("Q3");
        list.append("Q1");
        QTest::newRow("Quarter prior year") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1246406400, 1270080000);

        list.clear();
        list.append("W23");
        list.append("W11");
        QTest::newRow("Week prior year") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1243814400, 1269216000);

        list.clear();
        list.append("2009");
        list.append("3");
        QTest::newRow("Year DOY two arg") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1230940800, 1231027200);
        QTest::newRow("Year week two arg") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Week <<
                Time::Bounds(1231718400, 1232323200);
        QTest::newRow("Year quarter two arg") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Quarter <<
                Time::Bounds(1246406400, 1254355200);
        list.replace(1, "Q2");
        QTest::newRow("Year quarter two arg day") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1238544000, 1246406400);
        list.replace(1, "W15");
        QTest::newRow("Year week two arg day") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1238976000, 1239580800);
        list.append("16");
        QTest::newRow("Year week three arg") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1238976000, 1240185600);
        list.replace(2, "W16");
        QTest::newRow("Year week three arg week") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1238976000, 1240185600);
        list.replace(2, "W3");
        QTest::newRow("Year week three arg week year advance") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1238976000, 1264377600);
        list.replace(2, "Q3");
        QTest::newRow("Year week three arg quarter") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1238976000, 1254355200);
        list.replace(2, "Q1");
        QTest::newRow("Year week three arg quarter year advance") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1238976000, 1270080000);
        list.replace(2, "125.0");
        QTest::newRow("Year week three arg DOY") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1238976000, 1241481600);
        list.replace(2, "16.0");
        QTest::newRow("Year week three arg DOY year advance") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1238976000, 1263600000);
        list.replace(2, "2011");
        list.append("W25");
        QTest::newRow("Four arg year and week") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1238976000, 1309132800);

        list.clear();
        list.append("3");
        list.append("2009");
        list.append("4");
        QTest::newRow("End year DOY") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1230940800, 1231027200);
        QTest::newRow("End year week") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Week <<
                Time::Bounds(1231718400, 1232928000);
        QTest::newRow("End year quarter") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Quarter <<
                Time::Bounds(1246406400, 1262304000);
        list.replace(2, "W4");
        QTest::newRow("End year week day arg") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1231718400, 1232928000);
        list.replace(2, "Q4");
        QTest::newRow("End year quarter day arg") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1246406400, 1262304000);
        list.replace(0, "W15");
        list.replace(2, "W10");
        QTest::newRow("End year week decrease start year") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1207526400, 1236556800);
        list.replace(0, "Q4");
        list.replace(2, "Q2");
        QTest::newRow("End year week decrease start year") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1222732800, 1246406400);
        list.replace(0, "27");
        list.replace(2, "15");
        QTest::newRow("End year DOY decrease start year") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1201392000, 1231977600);


        list.clear();
        list.append("2010");
        list.append("1");
        list.append("3d");
        QTest::newRow("Split first and offset") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1262304000, 1262563200);

        list.clear();
        list.append("3d");
        list.append("2010");
        list.append("4");
        QTest::newRow("Split last and offset") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1262304000, 1262563200);

        list.clear();
        list.append("2011-04-09T20:56:31Z,2011-04-10T04:59:00Z");
        QTest::newRow("Comma separated times") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1302382591, 1302411540);

        list.clear();
        list.append("2011-04-09T20:56:31Z;2011-04-10T04:59:00Z");
        QTest::newRow("Semicolon separated times") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1302382591, 1302411540);

        list.clear();
        list.append("2010.1");
        list.append("2010.2");
        QTest::newRow("Two fractional years") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day <<
                Time::Bounds(1265457600, 1268611200);

        list.clear();
        list.append("20");
        list.append("70");
        QTest::newRow("Two DOY with week default") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Week <<
                Time::Bounds(1263945600, 1268265600);
    }

    void validateBoundList()
    {
        QFETCH(QStringList, list);
        QFETCH(TestTimeParseHandler, handler);
        QFETCH(bool, allowZeroLength);
        QFETCH(bool, allowUndefined);
        QFETCH(Time::LogicalTimeUnit, defaultUnit);

        try {
            TimeParse::parseListBounds(list, &handler,
                                       Time::Bounds(FP::undefined(), FP::undefined()),
                                       allowZeroLength, allowUndefined, defaultUnit, 1276091074.0);
        } catch (TimeParsingException tpe) { return; }
        QFAIL("Accepted invalid result");
    }

    void validateBoundList_data()
    {
        QTest::addColumn<QStringList>("list");
        QTest::addColumn<TestTimeParseHandler>("handler");
        QTest::addColumn<bool>("allowZeroLength");
        QTest::addColumn<bool>("allowUndefined");
        QTest::addColumn<Time::LogicalTimeUnit>("defaultUnit");
        QTest::addColumn<Time::Bounds>("result");

        QStringList list;

        list.clear();
        list.append("Arg1");
        list.append("2Arg");
        list.append("Faz");
        list.append("Zab");
        list.append("3Tail");
        QTest::newRow("Accept zero length") <<
                list <<
                TestTimeParseHandler(2, 3) <<
                false <<
                false <<
                Time::Day;
        QTest::newRow("Accept zero length start") <<
                list <<
                TestTimeParseHandler(5, 0) <<
                false <<
                false <<
                Time::Day;
        QTest::newRow("Accept zero length end") <<
                list <<
                TestTimeParseHandler(0, 5) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("none");
        list.append("2009:123");
        QTest::newRow("Infinite start") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        using std::swap;
        swap(list[0], list[1]);
        QTest::newRow("Infinite end") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("Q5");
        QTest::newRow("Start quarter too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(0, "Q0");
        QTest::newRow("Start quarter too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(0, "W55");
        QTest::newRow("Start week too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(0, "W0");
        QTest::newRow("Start week too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.append("Tail arg");
        QTest::newRow("End week too small") <<
                list <<
                TestTimeParseHandler(0, 1) <<
                false <<
                false <<
                Time::Day;
        list.replace(0, "W55");
        QTest::newRow("End week too large") <<
                list <<
                TestTimeParseHandler(0, 1) <<
                false <<
                false <<
                Time::Day;
        list.replace(0, "Q5");
        QTest::newRow("Start quarter too large") <<
                list <<
                TestTimeParseHandler(0, 1) <<
                false <<
                false <<
                Time::Day;
        list.replace(0, "Q0");
        QTest::newRow("Start quarter too small") <<
                list <<
                TestTimeParseHandler(0, 1) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("1300");
        list.append("W5");
        QTest::newRow("Start year too small week") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(1, "Q3");
        QTest::newRow("Start year too small quarter") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(0, "4332");
        QTest::newRow("Start year too large quarter") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(1, "W15");
        QTest::newRow("Start year too large week") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(0, "2001");
        list.append("1100");
        list.append("W12");
        QTest::newRow("Four arg end year too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(2, "6533");
        QTest::newRow("Four arg end year too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("2004");
        list.append("Q0");
        QTest::newRow("Start year start quarter too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(1, "Q5");
        QTest::newRow("Start year start quarter too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(1, "W0");
        QTest::newRow("Start year start week too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(1, "W55");
        QTest::newRow("Start year start week too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(1, "0.12345");
        QTest::newRow("Start year start DOY too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(1, "377");
        QTest::newRow("Start year start DOY too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(1, "120");
        list.append("Q0");
        QTest::newRow("Start year end quarter too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(2, "Q5");
        QTest::newRow("Start year end quarter too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(2, "W0");
        QTest::newRow("Start year end week too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(2, "W55");
        QTest::newRow("Start year end week too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(2, "0.1");
        QTest::newRow("Start year end DOY too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(2, "370");
        QTest::newRow("Start year end DOY too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("Q0");
        list.append("Q2");
        QTest::newRow("Start quarter too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(0, "Q5");
        QTest::newRow("Start quarter too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(0, "Q3");
        list.replace(1, "Q0");
        QTest::newRow("End quarter too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(1, "Q5");
        QTest::newRow("End quarter too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("W0");
        list.append("W10");
        QTest::newRow("Start week too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(0, "W55");
        QTest::newRow("Start week too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(0, "W16");
        list.replace(1, "W0");
        QTest::newRow("End week too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(1, "W55");
        QTest::newRow("End week too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("2010:150");
        list.append("W2");
        QTest::newRow("End week before start") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("W45");
        list.append("2010:150");
        QTest::newRow("Start week after end") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("2010:160");
        list.append("2010:150");
        QTest::newRow("Start after end") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("W10");
        list.append("1500");
        list.append("W11");
        QTest::newRow("End year too small week") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(1, "4542");
        QTest::newRow("End year too large week") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("Q1");
        list.append("1500");
        list.append("Q2");
        QTest::newRow("End year too small quarter") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(1, "4542");
        QTest::newRow("End year too large quarter") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("Q1");
        list.append("1980");
        list.append("Q0");
        QTest::newRow("End year end quarter too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(2, "Q5");
        QTest::newRow("End year end quarter too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(2, "Q2");
        list.replace(0, "Q0");
        QTest::newRow("End year start quarter too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(0, "Q5");
        QTest::newRow("End year start quarter too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("W10");
        list.append("1980");
        list.append("W0");
        QTest::newRow("End year end week too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(2, "W54");
        QTest::newRow("End year end week too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(2, "W15");
        list.replace(0, "W0");
        QTest::newRow("End year start week too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(0, "W55");
        QTest::newRow("End year start week too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("149");
        list.append("1980");
        list.append("0.5");
        QTest::newRow("End year end DOY too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(2, "367.42");
        QTest::newRow("End year end DOY too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(2, "180");
        list.replace(0, "0.75");
        QTest::newRow("End year start DOY too small") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.replace(0, "390");
        QTest::newRow("End year start DOY too large") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("2001");
        list.append("120");
        list.append("2001");
        list.append("110");
        QTest::newRow("Four arg start after end DOY") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("2001");
        list.append("Q4");
        list.append("2001");
        list.append("Q1");
        QTest::newRow("Four arg start after end quarter") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("2001");
        list.append("W40");
        list.append("2001");
        list.append("W10");
        QTest::newRow("Four arg start after end week") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("Bad1");
        QTest::newRow("Single bad argument") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.append("Bad2");
        QTest::newRow("Two bad arguments") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.append("Bad3");
        QTest::newRow("Three bad arguments") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.append("Bad4");
        QTest::newRow("Four bad arguments") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
        list.append("Bad5");
        QTest::newRow("Five bad arguments") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;

        list.clear();
        list.append("2001");
        list.append("120");
        list.append("2001");
        list.append("180");
        list.append("Bad");
        QTest::newRow("Valid time with extra argument") <<
                list <<
                TestTimeParseHandler(0, 0) <<
                false <<
                false <<
                Time::Day;
    }
};

QTEST_APPLESS_MAIN(TestTimeParse)

#include "timeparse.moc"
