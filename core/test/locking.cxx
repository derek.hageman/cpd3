/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "core/first.hxx"


#include <QTest>
#include <thread>

#include "core/threading.hxx"

using namespace CPD3;
using namespace CPD3::Threading;

class TestLocking : public QObject {
Q_OBJECT
private slots:

    void readCopyBasic()
    {
        ReadCopyLock<int> lock;

        {
            auto w = lock.write();
            *w = 1;
        }

        {
            ReadCopyLock<int>::ReadReference r(lock);
            QCOMPARE(*r, 1);
        }
        {
            auto r = lock.read();
            QCOMPARE(*r, 1);
        }

        {
            auto r = lock.read();
            QCOMPARE(*r, 1);
            ReadCopyLock<int>::WriteReference w(lock);
            *w = 2;
            QCOMPARE(*r, 1);
            QCOMPARE(*w, 2);
        }

        {
            auto r = lock.read();
            QCOMPARE(*r, 2);
        }

        {
            auto w = lock.exclusive();
            *w = 3;
        }

        {
            auto r = lock.read();
            QCOMPARE(*r, 3);
        }

        {
            auto w = lock.propagate();
            *w = 4;
        }

        {
            auto r = lock.read();
            QCOMPARE(*r, 4);
        }

        {
            auto before = lock.read();
            QCOMPARE(*before, 4);

            {
                auto w = lock.write();
                *w = 5;
                auto r = w.read();
                *w = 6;
                w.release();
                QCOMPARE(*r, 6);
            }

            QCOMPARE(*before, 4);
        }

        {
            auto r = lock.read();
            QCOMPARE(*r, 6);
        }
    }

    void readyCopyThreaded()
    {
        ReadCopyLock<int> lock(0);
        bool ok = true;

        {
            std::vector<std::thread> threads;
            for (int i = 0; i < 20; i++) {
                threads.emplace_back([&lock, i] {
                    for (int j = 0; j < 100; j++) {
                        auto w = lock.write();
                        *w = -1;
                        std::this_thread::yield();
                        *w = i;
                    }
                });

                threads.emplace_back([&] {
                    for (int j = 0; j < 100; j++) {
                        auto r = lock.read();
                        std::this_thread::yield();
                        if (*r < 0 || *r >= 20)
                            ok = false;
                    }
                });
            }
            for (auto &t : threads) {
                t.join();
            }
        }
        QVERIFY(ok);

        {
            std::vector<std::thread> threads;
            for (int i = 0; i < 20; i++) {
                threads.emplace_back([&lock, i] {
                    for (int j = 0; j < 100; j++) {
                        auto w = lock.exclusive();
                        *w = -1;
                        std::this_thread::yield();
                        *w = i;
                    }
                });

                threads.emplace_back([&] {
                    for (int j = 0; j < 100; j++) {
                        auto r = lock.read();
                        std::this_thread::yield();
                        if (*r < 0 || *r >= 20)
                            ok = false;
                    }
                });
            }
            for (auto &t : threads) {
                t.join();
            }
        }
        QVERIFY(ok);

        {
            std::vector<std::thread> threads;
            for (int i = 0; i < 20; i++) {
                threads.emplace_back([&lock, i] {
                    for (int j = 0; j < 100; j++) {
                        auto w = lock.propagate();
                        *w = -1;
                        std::this_thread::yield();
                        *w = i;
                    }
                });

                threads.emplace_back([&] {
                    for (int j = 0; j < 100; j++) {
                        auto r = lock.read();
                        std::this_thread::yield();
                        if (*r < 0 || *r >= 20)
                            ok = false;
                    }
                });
            }
            for (auto &t : threads) {
                t.join();
            }
        }
        QVERIFY(ok);
    }
};

QTEST_APPLESS_MAIN(TestLocking)

#include "locking.moc"