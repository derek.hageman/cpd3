/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"


#include <condition_variable>
#include <QtGlobal>
#include <QTest>
#include <QAtomicInt>
#include <QDebug>

#include "core/threading.hxx"

using namespace CPD3;
using namespace CPD3::Threading;

class Uncopyable {
public:
    int value;

    Uncopyable() = delete;

    Uncopyable(const Uncopyable &) = delete;

    Uncopyable &operator=(const Uncopyable &) = delete;

    explicit Uncopyable(int v) : value(v)
    { }
};

class CopyTrack {
public:
    int value;

    CopyTrack(const CopyTrack &other) : value(other.value)
    {
        qDebug() << "COPY";
    }

    CopyTrack &operator=(const CopyTrack &other)
    {
        qDebug() << "ASSIGN";
        value = other.value;
        return *this;
    }

    CopyTrack(CopyTrack &&other) : value(other.value)
    { }

    CopyTrack &operator=(CopyTrack &&other)
    {
        value = other.value;
        return *this;
    }

    explicit CopyTrack(int v) : value(v)
    { }
};

class TestSignal : public QObject {
Q_OBJECT
private slots:

    void basic()
    {
        Signal<> signal;
        int counter = 0;

        signal.connect([&] {
            ++counter;
        });

        signal();
        QCOMPARE(counter, 1);
        signal();
        signal();
        QCOMPARE(counter, 3);
    }

    void forwarding()
    {
        Signal<const Uncopyable &, const Uncopyable &> signal;
        int counter = 0;

        signal.connect([&](const Uncopyable &i, const Uncopyable &j) {
            counter += i.value;
            counter -= j.value;
        });

        signal(Uncopyable(1), Uncopyable(0));
        QCOMPARE(counter, 1);
        signal(Uncopyable(2), Uncopyable(1));
        signal(Uncopyable(4), Uncopyable(0));
        QCOMPARE(counter, 6);
    }

    void disconnections()
    {
        Signal<> signal;
        int counter = 0;

        signal.connect([&] {
            ++counter;
        });

        signal();
        QCOMPARE(counter, 1);
        signal.disconnect();
        signal();
        QCOMPARE(counter, 1);

        signal.connect([&] {
            signal.disconnectImmediate();
            ++counter;
        });
        signal.e();
        QCOMPARE(counter, 2);
        signal();
        QCOMPARE(counter, 2);
    }

    void deferredEmit()
    {
        Signal<int> signal;
        int counter = 0;

        signal.connect([&](int i) {
            counter += i;
        });

        {
            auto d = signal.defer(1);
            QCOMPARE(counter, 0);
        }
        QCOMPARE(counter, 1);

        signal(1);
        QCOMPARE(counter, 2);
        {
            Signal<int>::Deferred d(signal, 1);
            QCOMPARE(counter, 2);
            signal(2);
            QCOMPARE(counter, 4);
        }
        QCOMPARE(counter, 5);

        {
            Signal<const int &> ref;

            ref.connect([&](const int &i) {
                counter += i;
            });

            {
                int ir = 1;
                auto d = signal.defer(static_cast<const int &>(ir));
                QCOMPARE(counter, 5);
            }
            QCOMPARE(counter, 6);
        }
    }

    void receiverContext()
    {
        Receiver outer;
        int counter = 0;

        {
            Signal<int> signal;

            signal.connect(outer, [&](int i) {
                counter += i;
            });
            signal(1);
            QCOMPARE(counter, 1);

            {
                Receiver inner;
                signal.connect(inner, [&](int i) {
                    counter += i;
                });
                signal(1);
                QCOMPARE(counter, 3);
            }

            signal(1);
            QCOMPARE(counter, 4);
        }

        QCOMPARE(counter, 4);
        outer.disconnect();
    }

    void deferredReceiver()
    {
        Signal<int> signal;
        int counter = 0;

        {
            DeferredReceiver receiver;
            receiver.connect(signal, [&](int i) {
                counter += i;
            });

            signal(1);
            QCOMPARE(counter, 0);
            receiver.call();
            QCOMPARE(counter, 1);

            signal(1);
            QCOMPARE(counter, 1);
            receiver.ignore();
            receiver.call();
            QCOMPARE(counter, 1);

            signal(1);
        }

        QCOMPARE(counter, 2);
        signal(1);
        QCOMPARE(counter, 2);

        {
            DeferredReceiver receiver;
            {
                Signal<const int &> ref;
                receiver.connect(signal, [&](const int &i) {
                    counter += i;
                });

                int ir = 1;
                signal(static_cast<const int &>(ir));
                QCOMPARE(counter, 2);
            }
            receiver.call();
            QCOMPARE(counter, 3);
        }
    }

    void qobjectReceiver()
    {
        QObject outer;
        int counter = 0;

        {
            Signal<int> signal;

            signal.connect(&outer, [&](int i) {
                counter += i;
            });
            signal(1);
            QCOMPARE(counter, 1);

            {
                QObject inner;
                signal.connect(&inner, [&](int i) {
                    counter += i;
                });
                signal(1);
                QCOMPARE(counter, 3);
            }

            signal(1);
            QCOMPARE(counter, 4);
        }

        QCOMPARE(counter, 4);
        outer.disconnect();
    }

    void qobjectDeferred()
    {
        Signal<int> signal;
        int counter = 0;

        {
            QObject receiver;
            signal.connect(&receiver, [&](int i) {
                counter += i;
            }, true);

            signal(1);
            QCOMPARE(counter, 0);
            QCoreApplication::processEvents();
            QCOMPARE(counter, 1);

            signal(1);
            QCOMPARE(counter, 1);
            QThread::yieldCurrentThread();
            QCOMPARE(counter, 1);
        }

        /* No signal process means the queued event shouldn't run
         * since the object will be deleted */
        QCOMPARE(counter, 1);
        QCoreApplication::processEvents();
        QCOMPARE(counter, 1);
    }

    void future()
    {
        {
            Signal<int> signal;
            std::mutex mutex;
            std::condition_variable cv;
            int counter = 0;

            signal.connect([&](int add) {
                std::lock_guard<std::mutex> lock(mutex);
                counter += add;
                cv.notify_all();
            });

            auto f1 = std::async(std::launch::async, [] {
                return 1;
            });
            signal.future(std::move(f1));

            auto f2 = std::async(std::launch::async, [] {
                return 2;
            });
            signal.future(std::move(f2));

#if 0
            auto f3 = std::async(std::launch::async, [] {
                return 5;
            });
            signal.future(std::move(f2), 2);
#endif

            {

                std::unique_lock<std::mutex> lock(mutex);
#if 0
                cv.wait_for(lock, std::chrono::milliseconds(1000), [&] { return counter == 5; });
                QCOMPARE(counter, 5);
#else
                cv.wait_for(lock, std::chrono::milliseconds(1000), [&] { return counter == 3; });
                QCOMPARE(counter, 3);
#endif
            }
        }
        {
            Signal<> signal;
            std::mutex mutex;
            std::condition_variable cv;
            int counter = 0;

            signal.connect([&]() {
                std::lock_guard<std::mutex> lock(mutex);
                ++counter;
                cv.notify_all();
            });

            auto f1 = std::async(std::launch::async, [] { });
            signal.future(std::move(f1));

            auto f2 = std::async(std::launch::async, [] { });
            signal.future(std::move(f2));

            {
                std::unique_lock<std::mutex> lock(mutex);
                cv.wait_for(lock, std::chrono::milliseconds(1000), [&] { return counter == 2; });
                QCOMPARE(counter, 2);
            }
        }
    }

    void after()
    {
        Signal<> signal;
        std::mutex mutex;
        std::condition_variable cv;
        int counter = 0;

        signal.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            ++counter;
            cv.notify_all();
        });

        auto f1 = std::shared_future<void>(std::async(std::launch::async, [] { }));
        signal.after([=] { f1.wait(); });

        auto f2 = std::shared_future<void>(std::async(std::launch::async, [] { }));
        signal.after([=] { f2.wait(); });

        f1.wait();
        f2.wait();

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::milliseconds(1000), [&] { return counter == 2; });
            QCOMPARE(counter, 2);
        }
    }

    void replaceDuringCall()
    {
        Signal<> signal;

        int counter = 0;
        signal.connect([&]() {
            signal = Signal<>();
            ++counter;
        });

#if 0
        signal();
        signal();
#else
        signal.defer();
        signal.defer();
#endif

        QCOMPARE(counter, 1);
    }

    void disconnectionOrdering()
    {
        for (int retry = 0; retry < 10; ++retry) {
            Signal<> signal;

            int target = 1;
            int source = 1;
            auto c = signal.connect([&]() {
                std::this_thread::yield();
                target = source;
                if (target != 1)
                    abort();
            });

            std::mutex mutex;
            std::condition_variable cv;
            bool guard = false;

            auto t = std::thread([&] {
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    guard = true;
                    cv.notify_all();
                }
                std::this_thread::yield();
                c.disconnect();
                source = 0;
            });

            {
                std::unique_lock<std::mutex> lock(mutex);
                cv.wait(lock, [&] { return guard; });
            }

            std::vector<std::thread> workers;
            for (int n = 0; n < 8; n++) {
                workers.emplace_back([&] {
                    for (int i = 0; i < 1000; i++) {
                        signal();
                    }
                });
            }

            t.join();
            for (auto &w : workers) {
                w.join();
            }

            QCOMPARE(target, 1);
        }
    }
};

QTEST_MAIN(TestSignal)

#include "signal.moc"
