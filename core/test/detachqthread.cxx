/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"


#include <condition_variable>
#include <QtGlobal>
#include <QTest>
#include <QAtomicInt>

#include "core/threading.hxx"

using namespace CPD3;
using namespace CPD3::Threading;

class TestDetachQThread : public QObject {
Q_OBJECT
private slots:

    void basic()
    {
        std::mutex mutex;
        std::condition_variable notify;
        bool first = false;
        bool second = false;

        detachQThread([&] {
            std::lock_guard<std::mutex> lock(mutex);
            first = true;
            notify.notify_all();
        });

        std::function<void()> call = [&] {
            std::lock_guard<std::mutex> lock(mutex);
            second = true;
            notify.notify_all();
        };
        detachQThread(call);

        std::unique_lock<std::mutex> lock(mutex);
        notify.wait_for(lock, std::chrono::milliseconds(5000), [&] { return first && second; });
        QVERIFY(first);
        QVERIFY(second);
    }
};

QTEST_MAIN(TestDetachQThread)

#include "detachqthread.moc"
