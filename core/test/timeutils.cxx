/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <time.h>
#include <math.h>
#include <QTest>

#include "core/timeutils.hxx"

using namespace CPD3;

class TestTimeUtils : public QObject {
Q_OBJECT
private slots:

    void timeAccurateEpoch()
    {
        time_t test = (time_t) floor(Time::time());
        time_t base = time(NULL);
        QVERIFY(test == base || test + 1 == base);
    }

    void timeSecondPrecision()
    {
        double t1 = Time::time();
        QTest::qSleep(1250);
        double t2 = Time::time();
        QVERIFY(t2 > t1);
    }

    void timeSubsecondPrecision()
    {
        double t1 = Time::time();
        QTest::qSleep(250);
        double t2 = Time::time();
        QVERIFY(t2 >= t1);
        if (t2 != t1) return;
        QTest::qSleep(250);
        t1 = Time::time();
        QVERIFY(t1 >= t2);
        if (t1 == t2)
            QWARN("No sub-second precision on time available");
    }

    void yearDOYConvert()
    {
        QFETCH(int, year);
        QFETCH(double, doy);
        QFETCH(double, result);

        double tv = Time::convertYearDOY(year, doy);
        if (fabs(tv - result) >= 0.001) {
            QFAIL((QString("Converted: ") +
                    QString::number(tv, 'f', 3) +
                    QString(" Expected: ") +
                    QString::number(result, 'f', 3)).toLatin1().data());
        }
    }

    void yearDOYConvert_data()
    {
        QTest::addColumn<int>("year");
        QTest::addColumn<double>("doy");
        QTest::addColumn<double>("result");

        QTest::newRow("2010:1") << 2010 << 1.0 << 1262304000.0;
        QTest::newRow("2010:1.25") << 2010 << 1.25 << 1262325600.0;
        QTest::newRow("2010:1.34129") << 2010 << 1.34129 << 1262333487.0;
        QTest::newRow("2010:1.34128") << 2010 << 1.34128 << 1262333487.0;
        QTest::newRow("2010:366") << 2010 << 366.0 << 1293840000.0;
    }

    void cachingYearDOYConvert()
    {
        CachingYearDOYConverter cv;
        QCOMPARE(cv.convert(2010, 1), 1262304000.0);
        QCOMPARE(cv.convert(2010, 1.25), 1262325600.0);
        QCOMPARE(cv.convert(2010, 1.25), 1262325600.0);
        QCOMPARE(cv.convert(2010, 1.34129), 1262333487.0);
        QCOMPARE(cv.convert(2010, 1.34128), 1262333487.0);
        QCOMPARE(cv.convert(2007, 366), 1199145600.0);
        QCOMPARE(cv.convert(2008, 3.123), 1199329027.0);
        QCOMPARE(cv.convert(2009, 3.123), 1230951427.0);
    }

    void quarterBoundaries()
    {
        QCOMPARE(Time::quarterStart(2010, 1), 1262304000.0);
        QCOMPARE(Time::quarterEnd(2010, 1), 1270080000.0);
        QCOMPARE(Time::quarterStart(2010, 2), 1270080000.0);
        QCOMPARE(Time::quarterEnd(2010, 2), 1277942400.0);
        QCOMPARE(Time::quarterStart(2010, 3), 1277942400.0);
        QCOMPARE(Time::quarterEnd(2010, 3), 1285891200.0);
        QCOMPARE(Time::quarterStart(2010, 4), 1285891200.0);
        QCOMPARE(Time::quarterEnd(2010, 4), 1293840000.0);
    }

    void containingQuarter()
    {
        QFETCH(double, time);
        QFETCH(int, expectedYear);
        QFETCH(int, expectedQuarter);

        int year, qtr;
        Time::containingQuarter(time, &year, &qtr);
        QCOMPARE(year, expectedYear);
        QCOMPARE(qtr, expectedQuarter);
    }

    void containingQuarter_data()
    {
        QTest::addColumn<double>("time");
        QTest::addColumn<int>("expectedYear");
        QTest::addColumn<int>("expectedQuarter");

        QTest::newRow("2009:365.9...") << 1262303999.999 << 2009 << 4;
        QTest::newRow("2010:1") << 1262304000.0 << 2010 << 1;
        QTest::newRow("2010:35") << 1265241600.0 << 2010 << 1;
        QTest::newRow("2010:90.9...") << 1270079999.999 << 2010 << 1;
        QTest::newRow("2010:91") << 1270080000.0 << 2010 << 2;
        QTest::newRow("2010:160") << 1276041600.0 << 2010 << 2;
        QTest::newRow("2010:181.9...") << 1277942399.999 << 2010 << 2;
        QTest::newRow("2010:182") << 1277942400.0 << 2010 << 3;
        QTest::newRow("2010:200") << 1279497600.0 << 2010 << 3;
        QTest::newRow("2010:273.9...") << 1285891199.999 << 2010 << 3;
        QTest::newRow("2010:274") << 1285891200.0 << 2010 << 4;
        QTest::newRow("2010:365.9...") << 1293839999.999 << 2010 << 4;
        QTest::newRow("2011:1") << 1293840000.0 << 2011 << 1;
    }

    void weekStart()
    {
        QFETCH(int, year);
        QFETCH(int, week);
        QFETCH(double, result);

        double tv = Time::weekStart(year, week);
        if (fabs(tv - result) >= 0.001) {
            QFAIL((QString("Converted: ") +
                    QString::number(tv, 'f', 3) +
                    QString(" Expected: ") +
                    QString::number(result, 'f', 3)).toLatin1().data());
        }
    }

    void weekStart_data()
    {
        QTest::addColumn<int>("year");
        QTest::addColumn<int>("week");
        QTest::addColumn<double>("result");

        QTest::newRow("2010W1") << 2010 << 1 << 1262563200.0;
        QTest::newRow("2010W2") << 2010 << 2 << 1263168000.0;
        QTest::newRow("2010W52") << 2010 << 52 << 1293408000.0;
        QTest::newRow("2009W53") << 2009 << 53 << 1261958400.0;
        QTest::newRow("2008W13") << 2008 << 13 << 1206316800.0;
        QTest::newRow("2004W24") << 2004 << 24 << 1086566400.0;
    }

    void logical()
    {
        QFETCH(double, time);
        QFETCH(Time::LogicalTimeUnit, unit);
        QFETCH(int, count);
        QFETCH(bool, align);
        QFETCH(bool, rounding);
        QFETCH(int, multiplier);
        QFETCH(double, result);

        double tv = Time::logical(time, unit, count, align, rounding, multiplier);
        if (fabs(tv - result) >= 0.001) {
            QFAIL((QString("Offset: ") +
                    QString::number(tv, 'f', 3) +
                    QString(" Expected: ") +
                    QString::number(result, 'f', 3)).toLatin1().data());
        }
    }

    void logical_data()
    {
        QTest::addColumn<double>("time");
        QTest::addColumn<Time::LogicalTimeUnit>("unit");
        QTest::addColumn<int>("count");
        QTest::addColumn<bool>("align");
        QTest::addColumn<bool>("rounding");
        QTest::addColumn<int>("multiplier");
        QTest::addColumn<double>("result");

        QTest::newRow("NOOP") << 331108992.0 << Time::Second << 0 << false << false << 1
                              << 331108992.0;
        QTest::newRow("NOOP multplier") << 331108992.0 << Time::Second << 10 << false << false << 0
                                        << 331108992.0;

        QTest::newRow("Unaligned milliseconds") << 331108992.0 << Time::Millisecond << 150 << false
                                                << false << 1 << 331108992.150;
        QTest::newRow("Unaligned milliseconds multiplier") << 331108992.0 << Time::Millisecond << 50
                                                           << false << false << 3 << 331108992.150;
        QTest::newRow("Unaligned seconds") << 331108992.0 << Time::Second << -3200 << false << false
                                           << 1 << 331105792.0;
        QTest::newRow("Unaligned seconds multiplier") << 331108992.0 << Time::Second << 1600
                                                      << false << false << -2 << 331105792.0;
        QTest::newRow("Unaligned minutes") << 331108992.0 << Time::Minute << 2 << false << false
                                           << 1 << 331109112.0;
        QTest::newRow("Unaligned minutes multiplier") << 331108992.0 << Time::Minute << 1 << false
                                                      << false << 2 << 331109112.0;
        QTest::newRow("Unaligned hours") << 331108992.0 << Time::Hour << 1 << false << false << 1
                                         << 331112592.0;
        QTest::newRow("Unaligned hours multiplier") << 331108992.0 << Time::Hour << -2 << false
                                                    << false << 3 << 331087392.0;
        QTest::newRow("Unaligned days") << 331108992.0 << Time::Day << -100 << false << false << 1
                                        << 322468992.0;
        QTest::newRow("Unaligned days multiplier") << 331108992.0 << Time::Day << -50 << false
                                                   << false << 2 << 322468992.0;
        QTest::newRow("Unaligned weeks") << 331108992.0 << Time::Week << 13 << false << false << 1
                                         << 338971392.0;
        QTest::newRow("Unaligned months") << 1078067950.0 << Time::Month << 13 << false << false
                                          << 1 << 1112109550.0;
        QTest::newRow("Unaligned quarters") << 1078067950.0 << Time::Quarter << -2 << false << false
                                            << 1 << 1062170350.0;
        QTest::newRow("Unaligned years") << 1078067950.0 << Time::Year << -2 << false << false << 1
                                         << 1014909550.0;

        QTest::newRow("Aligned milliseconds down") << 331108992.00032 << Time::Millisecond << 150
                                                   << true << false << 1 << 331108992.150;
        QTest::newRow("Aligned milliseconds up") << 331108992.00032 << Time::Millisecond << 150
                                                 << true << true << 1 << 331108992.151;
        QTest::newRow("Aligned milliseconds down multiplier") << 331108992.001 << Time::Millisecond
                                                              << 50 << true << false << 4
                                                              << 331108992.200;
        QTest::newRow("Aligned milliseconds up multiplier") << 331108992.00032 << Time::Millisecond
                                                            << 50 << true << true << -4
                                                            << 331108991.850;
        QTest::newRow("Aligned seconds down") << 331108992.5 << Time::Second << -3200 << true
                                              << false << 1 << 331105792.0;
        QTest::newRow("Aligned seconds up") << 331108992.5 << Time::Second << -3200 << true << true
                                            << 1 << 331105793.0;
        QTest::newRow("Aligned seconds down multiplier") << 331108992.5 << Time::Second << -15
                                                         << true << false << 2 << 331108950.0;
        QTest::newRow("Aligned seconds up multiplier") << 331108992.5 << Time::Second << -15 << true
                                                       << true << 2 << 331108965.0;
        QTest::newRow("Aligned minutes down") << 331108992.0 << Time::Minute << 1 << true << false
                                              << 2 << 331109100.0;
        QTest::newRow("Aligned minutes up") << 331108992.0 << Time::Minute << 1 << true << true << 2
                                            << 331109160.0;
        QTest::newRow("Aligned minutes down multiplier") << 331108992.0 << Time::Minute << 10
                                                         << true << false << -4 << 331106400.0;
        QTest::newRow("Aligned minutes up multiplier") << 331108992.0 << Time::Minute << 10 << true
                                                       << true << -4 << 331107000.0;
        QTest::newRow("Aligned hours down") << 331108992.0 << Time::Hour << 1 << true << false << 1
                                            << 331110000.0;
        QTest::newRow("Aligned hours up") << 331108992.0 << Time::Hour << 1 << true << true << 1
                                          << 331113600.0;
        QTest::newRow("Aligned hours down multiplier") << 331108992.0 << Time::Hour << 6 << true
                                                       << false << 1 << 331128000.0;
        QTest::newRow("Aligned hours up multiplier") << 331108992.0 << Time::Hour << 6 << true
                                                     << true << 1 << 331149600.0;
        QTest::newRow("Aligned days down") << 331108992.0 << Time::Day << -3 << true << false << 1
                                           << 330825600.0;
        QTest::newRow("Aligned days up") << 331108992.0 << Time::Day << -3 << true << true << 1
                                         << 330912000.0;
        QTest::newRow("Aligned days down multiplier") << 331108992.0 << Time::Day << -1 << true
                                                      << false << 3 << 330825600.0;
        QTest::newRow("Aligned days up multiplier") << 331108992.0 << Time::Day << -1 << true
                                                    << true << 3 << 330912000.0;
        QTest::newRow("Aligned weeks down") << 331108992.0 << Time::Week << -50 << true << false
                                            << 1 << 300326400.0;
        QTest::newRow("Aligned weeks up") << 331108992.0 << Time::Week << 3 << true << true << 1
                                          << 332985600.0;
        QTest::newRow("Aligned weeks down multiplier") << 331108992.0 << Time::Week << 25 << true
                                                       << false << -2 << 300326400.0;
        QTest::newRow("Aligned weeks up multiplier") << 331108992.0 << Time::Week << 2 << true
                                                     << true << 2 << 333590400.0;
        QTest::newRow("Aligned months down") << 331108992.0 << Time::Month << 2 << true << false
                                             << 1 << 333936000.0;
        QTest::newRow("Aligned months up") << 331108992.0 << Time::Month << -7 << true << true << 1
                                           << 312854400.0;
        QTest::newRow("Aligned months down multiplier") << 331108992.0 << Time::Month << 1 << true
                                                        << false << 2 << 333936000.0;
        QTest::newRow("Aligned months up multiplier") << 331108992.0 << Time::Month << -1 << true
                                                      << true << 2 << 325987200.0;
        QTest::newRow("Aligned quarters down") << 331108992.0 << Time::Quarter << 1 << true << false
                                               << 1 << 331171200.0;
        QTest::newRow("Aligned quarters up") << 331108992.0 << Time::Quarter << -3 << true << true
                                             << 1 << 307584000.0;
        QTest::newRow("Aligned quarters down mulitplier") << 331108992.0 << Time::Quarter << 1
                                                          << true << false << 0 << 323308800.0;
        QTest::newRow("Aligned quarters up multiplier") << 331108992.0 << Time::Quarter << -1
                                                        << true << true << 3 << 307584000.0;
        QTest::newRow("Aligned years down") << 331108992.0 << Time::Year << 2 << true << false << 1
                                            << 378691200.0;
        QTest::newRow("Aligned years up") << 331108992.0 << Time::Year << 2 << true << true << 1
                                          << 410227200.0;
        QTest::newRow("Aligned years down multiplier") << 331108992.0 << Time::Year << 1 << true
                                                       << false << 2 << 378691200.0;
        QTest::newRow("Aligned years up multiplier") << 331108992.0 << Time::Year << 1 << true
                                                     << true << 2 << 410227200.0;

        QTest::newRow("Aligned seconds into minute") << 1462060835.0 << Time::Second << 10 << true
                                                     << false << 0 << 1462060830.0;

        QTest::newRow("Degenerate Week One") << 1231027200.0 << Time::Week << 1 << true << false
                                             << 1 << 1231113600.0;
    }

    void timeFloor()
    {
        QFETCH(double, time);
        QFETCH(Time::LogicalTimeUnit, unit);
        QFETCH(int, count);
        QFETCH(double, result);

        double tv = Time::floor(time, unit, count);
        if (fabs(tv - result) >= 0.001) {
            QFAIL((QString("Offset: ") +
                    QString::number(tv, 'f', 3) +
                    QString(" Expected: ") +
                    QString::number(result, 'f', 3)).toLatin1().data());
        }
    }

    void timeFloor_data()
    {
        QTest::addColumn<double>("time");
        QTest::addColumn<Time::LogicalTimeUnit>("unit");
        QTest::addColumn<int>("count");
        QTest::addColumn<double>("result");

        QTest::newRow("NOOP") << 331108992.5 << Time::Second << 0 << 331108992.5;

        QTest::newRow("Milliseconds") << 331108992.00032 << Time::Millisecond << 150
                                      << 331108992.000;
        QTest::newRow("Seconds") << 331108992.5 << Time::Second << 5 << 331108990.000;
        QTest::newRow("Minutes") << 331108992.0 << Time::Minute << 1 << 331108980.0;
        QTest::newRow("Hours") << 331108992.0 << Time::Hour << 6 << 331106400.0;
        QTest::newRow("Days") << 331108992.0 << Time::Day << 2 << 331084800.0;
        QTest::newRow("Weeks") << 331108992.0 << Time::Week << 3 << 330566400.0;
        QTest::newRow("Months") << 331108992.0 << Time::Month << 1 << 328665600.0;
        QTest::newRow("Quarters") << 331108992.0 << Time::Quarter << 2 << 323308800.0;
        QTest::newRow("Years") << 331108992.0 << Time::Year << 1 << 315532800.0;

        QTest::newRow("Degenerate Week One") << 1231027200.0 << Time::Week << 1 << 1230508800.0;
    }

    void timeCeil()
    {
        QFETCH(double, time);
        QFETCH(Time::LogicalTimeUnit, unit);
        QFETCH(int, count);
        QFETCH(double, result);

        double tv = Time::ceil(time, unit, count);
        if (fabs(tv - result) >= 0.001) {
            QFAIL((QString("Offset: ") +
                    QString::number(tv, 'f', 3) +
                    QString(" Expected: ") +
                    QString::number(result, 'f', 3)).toLatin1().data());
        }
    }

    void timeCeil_data()
    {
        QTest::addColumn<double>("time");
        QTest::addColumn<Time::LogicalTimeUnit>("unit");
        QTest::addColumn<int>("count");
        QTest::addColumn<double>("result");

        QTest::newRow("NOOP") << 331108992.5 << Time::Second << 0 << 331108992.5;

        QTest::newRow("Milliseconds") << 331108992.00032 << Time::Millisecond << -100
                                      << 331108992.100;
        QTest::newRow("Seconds") << 331108992.5 << Time::Second << 1 << 331108993.0;
        QTest::newRow("Minutes") << 331108992.0 << Time::Minute << 30 << 331110000.0;
        QTest::newRow("Hours") << 331108992.0 << Time::Hour << 1 << 331110000.0;
        QTest::newRow("Days") << 331108992.0 << Time::Day << 2 << 331171200.0;
        QTest::newRow("Weeks") << 331108992.0 << Time::Week << 1 << 331171200.0;
        QTest::newRow("Months") << 331108992.0 << Time::Month << 2 << 331257600.0;
        QTest::newRow("Quarters") << 331108992.0 << Time::Quarter << 1 << 331171200.0;
        QTest::newRow("Years") << 331108992.0 << Time::Year << 4 << 347155200.0;

        QTest::newRow("Degenerate Week One") << 1231027200.0 << Time::Week << 1 << 1231113600.0;
    }

    void timeRound()
    {
        QFETCH(double, time);
        QFETCH(Time::LogicalTimeUnit, unit);
        QFETCH(int, count);
        QFETCH(double, result);

        double tv = Time::round(time, unit, count);
        if (fabs(tv - result) >= 0.001) {
            QFAIL((QString("Offset: ") +
                    QString::number(tv, 'f', 3) +
                    QString(" Expected: ") +
                    QString::number(result, 'f', 3)).toLatin1().data());
        }
    }

    void timeRound_data()
    {
        QTest::addColumn<double>("time");
        QTest::addColumn<Time::LogicalTimeUnit>("unit");
        QTest::addColumn<int>("count");
        QTest::addColumn<double>("result");

        QTest::newRow("NOOP") << 331108992.5 << Time::Second << 0 << 331108992.5;

        QTest::newRow("Milliseconds down") << 331108992.00032 << Time::Millisecond << 1
                                           << 331108992.0;
        QTest::newRow("Milliseconds up") << 331108992.0007 << Time::Millisecond << 1
                                         << 331108992.001;
        QTest::newRow("Milliseconds down multiple") << 331108992.023 << Time::Millisecond << 100
                                                    << 331108992.000;
        QTest::newRow("Milliseconds up multiple") << 331108992.070 << Time::Millisecond << -100
                                                  << 331108992.100;
        QTest::newRow("Seconds down") << 331108992.1 << Time::Second << 1 << 331108992.0;
        QTest::newRow("Seconds up") << 331108992.9 << Time::Second << 1 << 331108993.0;
        QTest::newRow("Seconds down multiple") << 331108983.0 << Time::Second << -15 << 331108980.0;
        QTest::newRow("Seconds up multiple") << 331108992.0 << Time::Second << 15 << 331108995.0;
        QTest::newRow("Minutes down") << 331108992.0 << Time::Minute << 1 << 331108980.0;
        QTest::newRow("Minutes up") << 331109030.0 << Time::Minute << 1 << 331109040.0;
        QTest::newRow("Minutes down multiple") << 331108992.0 << Time::Minute << 3 << 331108920.0;
        QTest::newRow("Minutes up multiple") << 331109160.0 << Time::Minute << 30 << 331110000.0;
        QTest::newRow("Hours down") << 331106700.0 << Time::Hour << 1 << 331106400.0;
        QTest::newRow("Hours up") << 331108992.0 << Time::Hour << 1 << 331110000.0;
        QTest::newRow("Hours down multiple") << 331108992.0 << Time::Hour << 6 << 331106400.0;
        QTest::newRow("Hours up multiple") << 331124399.0 << Time::Hour << 6 << 331128000.0;
        QTest::newRow("Days down") << 331106700.0 << Time::Day << 1 << 331084800.0;
        QTest::newRow("Days up") << 331167600.0 << Time::Day << 3 << 331171200.0;
        QTest::newRow("Weeks down") << 330566400.0 << Time::Week << 2 << 330566400.0;
        QTest::newRow("Weeks up") << 331167600.0 << Time::Week << 1 << 331171200.0;
        QTest::newRow("Months down") << 329054400.0 << Time::Month << 1 << 328665600.0;
        QTest::newRow("Months up") << 331171200.0 << Time::Month << 2 << 331257600.0;
        QTest::newRow("Quarters down") << 323403840.0 << Time::Quarter << 2 << 323308800.0;
        QTest::newRow("Quarters up") << 331108992.0 << Time::Quarter << 1 << 331171200.0;
        QTest::newRow("Years down") << 319118400.0 << Time::Year << -1 << 315532800.0;
        QTest::newRow("Years up") << 341431200.0 << Time::Year << -2 << 347155200.0;

        QTest::newRow("Degenerate Week One Up") << 1231027200.0 << Time::Week << 1 << 1231113600.0;
        QTest::newRow("Degenerate Week One Down") << 1230595200.0 << Time::Week << 1
                                                  << 1230508800.0;
    }

    void ISO8601()
    {
        QCOMPARE(Time::toISO8601(1283185923.0), QString("2010-08-30T16:32:03Z"));
        QCOMPARE(Time::toISO8601(1283185923.123, true), QString("2010-08-30T16:32:03.123Z"));
        QCOMPARE(Time::toISO8601ND(1283185923.0), QString("20100830T163203Z"));
        QCOMPARE(Time::toISO8601ND(1283185923.123, true), QString("20100830T163203.123Z"));
    }

    void yearDOY()
    {
        QCOMPARE(Time::toYearDOY(1292428852.0), QString("2010:349.66727"));
        QCOMPARE(Time::toYearDOY(1292428800.0), QString("2010:349.67"));
        QCOMPARE(Time::toYearDOY(1292371200.0), QString("2010:349"));
        QCOMPARE(Time::toYearDOY(1262390400.0, ",", Time::Second, QChar()),
                 QString("2010,2.00000"));
        QCOMPARE(Time::toYearDOY(1262390400.0, ",", Time::Hour, QChar('0'), Time::DOYOnly),
                 QString("002.00"));
        QCOMPARE(Time::toYearDOY(1262399040.0, "!", Time::Day, QChar('0')), QString("2010!002"));
        QCOMPARE(Time::toYearDOY(1262304000.0, ",", Time::Year, QChar(), Time::DOYAsNeeded),
                 QString("2010"));
        QCOMPARE(Time::toYearDOY(1262304000.0, ",", Time::Hour, QChar(), Time::DOYAsNeeded),
                 QString("2010,1.00"));
        QCOMPARE(Time::toYearDOY(1262304001.0, ",", Time::Year, QChar(), Time::DOYAsNeeded),
                 QString("2010,1.00001"));
        QCOMPARE(Time::toYearDOY(1262390400.0, QString(), Time::Second, QChar(), Time::DOYOnly),
                 QString("2.00000"));
    }

    void describeOffset()
    {
        QVERIFY(Time::describeOffset(Time::Second, 1).length() > 0);
    }
};

QTEST_APPLESS_MAIN(TestTimeUtils)

#include "timeutils.moc"
