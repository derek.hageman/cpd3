/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <string.h>
#include <string>
#include <cctype>
#include <cstring>
#include <QDataStream>
#include <QLoggingCategory>
#include <QRegularExpression>

#include "core/util.hxx"


Q_LOGGING_CATEGORY(log_core_util, "cpd3.core.util", QtWarningMsg)

namespace CPD3 {
namespace Util {

bool equal_insensitive(const std::string &a, const std::string &b)
{
    if (a.length() != b.length())
        return false;
    return std::equal(a.begin(), a.end(), b.begin(), [](char cA, char cB) -> bool {
        return std::tolower(cA) == std::tolower(cB);
    });
}

bool equal_insensitive(const std::string &a, const char *b)
{
    auto blen = std::strlen(b);
    if (a.length() != blen)
        return false;
    return std::equal(a.begin(), a.end(), b, [](char cA, char cB) -> bool {
        return std::tolower(cA) == std::tolower(cB);
    });
}

bool starts_with(const std::string &str, const std::string &prefix)
{
#if CPD3_CXX >= 202000L
    return str.starts_with(prefix);
#else
    if (str.length() < prefix.length())
        return false;
    return std::equal(prefix.begin(), prefix.end(), str.begin());
#endif
}

bool starts_with(const std::string &str, const char *prefix)
{
#if CPD3_CXX >= 202000L
    return str.starts_with(prefix);
#else
    auto plen = std::strlen(prefix);
    if (str.length() < plen)
        return false;
    return std::equal(prefix, prefix + plen, str.begin());
#endif
}

bool starts_with_insensitive(const std::string &str, const std::string &prefix)
{
    if (str.length() < prefix.length())
        return false;
    return std::equal(prefix.begin(), prefix.end(), str.begin(), [](char cA, char cB) -> bool {
        return std::tolower(cA) == std::tolower(cB);
    });
}

bool starts_with_insensitive(const std::string &str, const char *prefix)
{
    auto plen = std::strlen(prefix);
    if (str.length() < plen)
        return false;
    return std::equal(prefix, prefix + plen, str.begin(), [](char cA, char cB) -> bool {
        return std::tolower(cA) == std::tolower(cB);
    });
}

bool ends_with(const std::string &str, const std::string &suffix)
{
#if CPD3_CXX >= 202000L
    return str.ends_with(suffix);
#else
    if (str.length() < suffix.length())
        return false;
    return std::equal(suffix.begin(), suffix.end(), str.end() - suffix.length());
#endif
}

bool ends_with(const std::string &str, const char *suffix)
{
#if CPD3_CXX >= 202000L
    return str.ends_with(suffix);
#else
    auto plen = std::strlen(suffix);
    if (str.length() < plen)
        return false;
    return std::equal(suffix, suffix + plen, str.end() - plen);
#endif
}

bool ends_with_insensitive(const std::string &str, const std::string &suffix)
{
    if (str.length() < suffix.length())
        return false;
    return std::equal(suffix.begin(), suffix.end(), str.end() - suffix.length(),
                      [](char cA, char cB) -> bool {
                          return std::tolower(cA) == std::tolower(cB);
                      });
}

bool ends_with_insensitive(const std::string &str, const char *suffix)
{
    auto plen = std::strlen(suffix);
    if (str.length() < plen)
        return false;
    return std::equal(suffix, suffix + plen, str.end() - plen, [](char cA, char cB) -> bool {
        return std::tolower(cA) == std::tolower(cB);
    });
}

void replace_all(std::string &s, const std::string &before, const std::string &after)
{
    for (std::size_t index = 0;;) {
        index = s.find(before, index);
        if (index == std::string::npos)
            break;

        s.replace(index, before.length(), after);
        index += after.length();
    }
}

void replace_all(std::string &s, const char *before, const char *after)
{
    std::size_t blen = std::strlen(before);
    std::size_t alen = std::strlen(after);
    for (std::size_t index = 0;;) {
        index = s.find(before, index);
        if (index == std::string::npos)
            break;

        s.replace(index, blen, after, alen);
        index += alen;
    }
}

void replace_insensitive(std::string &s, const std::string &before, const std::string &after)
{
    for (std::size_t index = 0;;) {
        auto pos = std::search(s.begin() + index, s.end(), before.begin(), before.end(),
                               [](char cA, char cB) -> bool {
                                   return std::tolower(cA) == std::tolower(cB);
                               });
        if (pos == s.end())
            break;
        index = static_cast<std::size_t>(pos - s.begin());

        s.replace(index, before.length(), after);
        index += after.length();
    }
}

void replace_insensitive(std::string &s, const char *before, const char *after)
{
    std::size_t blen = std::strlen(before);
    std::size_t alen = std::strlen(after);
    for (std::size_t index = 0;;) {
        auto pos = std::search(s.begin() + index, s.end(), before, before + blen,
                               [](char cA, char cB) -> bool {
                                   return std::tolower(cA) == std::tolower(cB);
                               });
        if (pos == s.end())
            break;
        index = static_cast<std::size_t>(pos - s.begin());

        s.replace(index, blen, after, alen);
        index += alen;
    }
}

bool exact_match(const QString &str, const QRegularExpression &regexp)
{
    auto r = regexp.match(str, 0, QRegularExpression::NormalMatch,
                          QRegularExpression::AnchoredMatchOption);
    if (!r.hasMatch())
        return false;
    return r.capturedLength() == str.length();
}

bool exact_match(const std::string &str, const QRegularExpression &regexp)
{ return exact_match(QString::fromStdString(str), regexp); }

bool contains(const std::string &str, char check)
{ return str.find(check) != str.npos; }

bool contains(const std::string &str, const std::string &check)
{ return str.find(check) != str.npos; }

bool contains(const std::string &str, const char *check)
{ return str.find(check) != str.npos; }

bool contains(const std::string &str, const QString &check)
{ return str.find(check.toStdString()) != str.npos; }

static inline bool is_space(char c)
{
    switch (c) {
    case ' ':
    case '\f':
    case '\n':
    case '\r':
    case '\t':
    case '\v':
        return true;
    default:
        break;
    }
    return false;
}

std::string trimmed(const std::string &str)
{
    auto begin = str.begin();
    auto end = str.end();
    for (; begin != end && is_space(*begin); ++begin) { }
    if (begin == end)
        return {};
    --end;
    for (; end != begin && is_space(*end); --end) { }
    ++end;
    return std::string(begin, end);
}

std::string trimmed(std::string &&str)
{
    auto begin = str.begin();
    auto first = begin;
    auto end = str.end();
    for (; begin != end && is_space(*begin); ++begin) { }
    if (begin == end)
        return {};
    --end;
    for (; end != begin && is_space(*end); --end) { }
    ++end;

    auto clearStart = static_cast<std::size_t>(begin - first);
    str.resize(static_cast<std::size_t>(end - first));
    str.erase(0, clearStart);
    return std::string(std::move(str));
}

std::string to_lower(const std::string &str)
{
    auto result = str;
    std::transform(result.begin(), result.end(), result.begin(), [](char c) -> char {
        return static_cast<char>(std::tolower(c));
    });
    return result;
}

std::string to_lower(std::string &&str)
{
    std::transform(str.begin(), str.end(), str.begin(), [](char c) -> char {
        return static_cast<char>(std::tolower(c));
    });
    return std::string(std::move(str));
}

std::string to_upper(const std::string &str)
{
    auto result = str;
    std::transform(result.begin(), result.end(), result.begin(), [](char c) -> char {
        return static_cast<char>(std::toupper(c));
    });
    return result;
}

std::string to_upper(std::string &&str)
{
    std::transform(str.begin(), str.end(), str.begin(), [](char c) -> char {
        return static_cast<char>(std::toupper(c));
    });
    return std::string(std::move(str));
}

std::vector<std::string> split_string(const std::string &str, char delimiter, bool empty)
{
    std::vector<std::string> result;
    for (auto begin = str.begin(), end = str.end(); begin != end;) {
        auto next = std::find(begin, end, delimiter);
        if (next == end) {
            result.emplace_back(begin, end);
            break;
        }

        if (!empty) {
            if (next == begin) {
                ++begin;
                continue;
            }
        }

        result.emplace_back(begin, next);
        begin = next;
        ++begin;

        if (empty) {
            /* Ended on a delimiter */
            if (begin == end) {
                result.emplace_back();
                break;
            }
        }
    }
    return result;
}

static int to_digit(char c, int base = 10)
{
    if (c >= '0' && (c <= '9' - 10 - base))
        return static_cast<int>(c - '0');
    if (base <= 10)
        return -1;
    base -= 10;
    Q_ASSERT(base <= 26);
    if (c >= 'a' && c <= 'a' + base)
        return static_cast<int>(c - 'a') + 10;
    if (c >= 'A' && c <= 'A' + base)
        return static_cast<int>(c - 'A') + 10;
    return -1;
}

std::vector<std::string> split_quoted(const std::string &str, bool escape)
{
    std::vector<std::string> result;
    for (auto begin = str.begin(), end = str.end(); begin != end;) {
        char c = *begin;
        if (is_space(c)) {
            ++begin;
            continue;
        }

        switch (c) {
        case '"': {
            result.emplace_back();
            auto &target = result.back();
            for (++begin; begin != end; ++begin) {
                if (*begin == '\\' && escape) {
                    ++begin;
                    if (begin == end)
                        break;
                    int dig = to_digit(*begin, 8);
                    if (dig != -1) {
                        std::uint_fast16_t octal = dig * 8 * 8;

                        ++begin;
                        if (begin == end)
                            break;
                        dig = to_digit(*begin, 8);
                        if (dig == -1)
                            break;
                        octal += dig * 8;

                        ++begin;
                        if (begin == end)
                            break;
                        dig = to_digit(*begin, 8);
                        if (dig == -1)
                            break;
                        octal += dig;

                        if (octal > 0xFF)
                            continue;

                        target.push_back(static_cast<char>(octal));
                        continue;
                    }
                    switch (*begin) {
                    case 'a':
                        target.push_back('\a');
                        break;
                    case 'b':
                        target.push_back('\b');
                        break;
                    case 'f':
                        target.push_back('\f');
                        break;
                    case 'n':
                        target.push_back('\n');
                        break;
                    case 'r':
                        target.push_back('\r');
                        break;
                    case 't':
                        target.push_back('\t');
                        break;
                    case 'v':
                        target.push_back('\v');
                        break;

                    case 'x': {
                        std::uint_fast8_t hex = 0;
                        int i;
                        for (i = 0; i < 2; i++) {
                            ++begin;
                            if (begin == end)
                                break;
                            dig = to_digit(*begin, 16);
                            if (dig == -1)
                                break;
                            hex += (0x10U >> (i * 4U)) * dig;
                        }
                        if (i != 2)
                            break;
                        target.push_back(static_cast<char>(hex));
                        break;
                    }

                    case 'u': {
                        quint16 hex = 0;
                        int i;
                        for (i = 0; i < 4; i++) {
                            ++begin;
                            if (begin == end)
                                break;
                            dig = to_digit(*begin, 16);
                            if (dig == -1)
                                break;
                            hex += (0x1000U >> (i * 4U)) * dig;
                        }
                        if (i != 4)
                            break;
                        target += QString(QChar(hex)).toUtf8().toStdString();
                        break;
                    }

                    case 'U': {
                        quint32 hex = 0;
                        int i;
                        for (i = 0; i < 8; i++) {
                            ++begin;
                            if (begin == end)
                                break;
                            dig = to_digit(*begin, 16);
                            if (dig == -1)
                                break;
                            hex += (0x10000000U >> (i * 4U)) * dig;
                        }
                        if (i != 4)
                            break;
                        target += QString(QChar(hex)).toUtf8().toStdString();
                        break;
                    }

                    case '\"':
                    case '\'':
                    case '?':
                    case '\\':
                    default:
                        target.push_back(*begin);
                        break;
                    }

                    continue;
                }
                if (*begin == '\"')
                    break;

                target.push_back(*begin);
            }
            if (begin == end)
                return result;
            ++begin;
            break;
        }

        case '\'': {
            ++begin;
            if (begin == end)
                break;
            auto next = begin;
            for (; next != end && *next != '\''; ++next) { }
            result.emplace_back(begin, next);
            begin = next;
            if (begin == end)
                return result;
            ++begin;
            break;
        }

        default: {
            auto next = begin;
            for (++next; next != end && !is_space(*next); ++next) { }
            result.emplace_back(begin, next);
            begin = next;
            if (begin == end)
                return result;
            break;
        }
        }
    }
    return result;
}

std::vector<std::string> split_lines(const std::string &str)
{
    std::vector<std::string> result;
    for (const auto &line : Util::ByteView(str).split_lines()) {
        result.emplace_back(line.data<const char *>(), line.size());
    }
    return result;
}

std::string prefix(const std::string &str, char delimiter)
{
    auto idx = str.find(delimiter);
    if (idx == str.npos) {
        return str;
    }
    return str.substr(0, idx);
}

std::string prefix(std::string &&str, char delimiter)
{
    auto idx = str.find(delimiter);
    if (idx == str.npos) {
        return std::string(std::move(str));
    }
    str.erase(idx);
    return std::string(std::move(str));
}

std::string suffix(const std::string &str, char delimiter)
{
    auto idx = str.find(delimiter);
    if (idx == str.npos) {
        return {};
    }
    return str.substr(idx + 1);
}

std::string suffix(std::string &&str, char delimiter)
{
    auto idx = str.find(delimiter);
    if (idx == str.npos) {
        return {};
    }
    str.erase(0, idx + 1);
    return std::string(std::move(str));
}

std::pair<std::string, std::string> halves(const std::string &str, char delimiter)
{
    auto idx = str.find(delimiter);
    if (idx == str.npos) {
        return {str, {}};
    }
    return {str.substr(0, idx), str.substr(idx + 1)};
}

std::pair<std::string, std::string> halves(std::string &&str, char delimiter)
{
    auto idx = str.find(delimiter);
    if (idx == str.npos) {
        return {std::move(str), {}};
    }
    auto suffix = str.substr(idx + 1);
    str.erase(idx);
    return {std::move(str), std::move(suffix)};
}


ByteArray::ByteArray(const QByteArray &other) : storage(static_cast<size_type>(other.size()))
{
    std::memcpy(data(), other.data(), size());
}

ByteArray::ByteArray(const ByteView &other) : storage(other.size())
{
    std::memcpy(data(), other.data(), size());
}

ByteArray::ByteArray(const char *str)
{
    std::size_t n = std::strlen(str);
    storage.resize(n);
    std::memcpy(data(), str, size());
}

ByteArray::ByteArray(const std::string &other) : storage(other.size())
{
    std::memcpy(data(), other.data(), size());
}

ByteArray::ByteArray(const void *data, size_type n) : storage(n)
{
    std::memcpy(this->data(), data, size());
}

ByteArray ByteArray::filled(value_type contents, size_type n)
{
    ByteArray result;
    result.resize(n, contents);
    return result;
}

void ByteArray::push_back(const void *data, size_type count)
{
    if (!count)
        return;
    Q_ASSERT(reinterpret_cast<const uint8_t *>(data) + count <= this->data<const uint8_t *>() ||
                     reinterpret_cast<const uint8_t *>(data) >=
                             this->data<const uint8_t *>() + size());
    std::memcpy(tail(count), data, count);
}

void ByteArray::push_front(const void *data, size_type count)
{
    if (!count)
        return;
    Q_ASSERT(reinterpret_cast<const uint8_t *>(data) + count <= this->data<const uint8_t *>() ||
                     reinterpret_cast<const uint8_t *>(data) >=
                             this->data<const uint8_t *>() + size());
    auto original = size();
    resize(original + count);
    std::memmove(this->data(count), this->data(), original);
    std::memcpy(this->data(), data, count);
}

void ByteArray::pop_front(size_type count)
{
    if (count >= size())
        return clear();
    auto remaining = size() - count;
    std::memmove(data(), data(count), remaining);
    resize(remaining);
}

void ByteArray::pop_back(size_type count)
{
    if (count >= size())
        return clear();
    resize(size() - count);
}

ByteArray &ByteArray::operator+=(const ByteArray &other)
{
    if (&other == this) {
        if (empty())
            return *this;
        auto original = size();
        resize(original * 2);
        std::memcpy(data(original), data(), original);
        return *this;
    }
    push_back(other.data(), other.size());
    return *this;
}

ByteArray &ByteArray::operator+=(ByteArray &&other)
{
    Q_ASSERT(&other != this);
    if (empty()) {
        *this = std::move(other);
        return *this;
    }
    push_back(other.data(), other.size());
    return *this;
}

ByteArray &ByteArray::operator+=(const QByteArray &other)
{
    push_back(other.data(), static_cast<size_type>(other.size()));
    return *this;
}

ByteArray &ByteArray::operator+=(const ByteView &other)
{
    push_back(other.data(), other.size());
    return *this;
}

ByteArray &ByteArray::operator+=(const char *string)
{
    if (!string)
        return *this;
    push_back(string, std::strlen(string));
    return *this;
}

ByteArray &ByteArray::operator+=(const std::string &other)
{
    push_back(other.data(), other.size());
    return *this;
}

ByteArray &ByteArray::operator-=(const ByteArray &other)
{
    if (&other == this) {
        if (empty())
            return *this;
        auto original = size();
        resize(original * 2);
        std::memcpy(data(original), data(), original);
        return *this;
    }
    push_front(other.data(), other.size());
    return *this;
}

ByteArray &ByteArray::operator-=(ByteArray &&other)
{
    Q_ASSERT(&other != this);
    if (empty()) {
        *this = std::move(other);
        return *this;
    }
    push_front(other.data(), other.size());
    return *this;
}

ByteArray &ByteArray::operator-=(const QByteArray &other)
{
    push_front(other.data(), static_cast<size_type>(other.size()));
    return *this;
}

ByteArray &ByteArray::operator-=(const ByteView &other)
{
    push_front(other.data(), other.size());
    return *this;
}

ByteArray &ByteArray::operator-=(const std::string &other)
{
    push_front(other.data(), other.size());
    return *this;
}

ByteArray &ByteArray::operator-=(const char *string)
{
    if (!string)
        return *this;
    push_front(string, std::strlen(string));
    return *this;
}

ByteArray &ByteArray::operator=(const QByteArray &other)
{
    auto total = static_cast<size_type>(other.size());
    resize(total);
    std::memcpy(data(), other.data(), total);
    return *this;
}

ByteArray &ByteArray::operator=(const ByteView &other)
{
    if (other.empty()) {
        resize(0);
        return *this;
    }
    Q_ASSERT(other.data<const std::uint8_t *>() < data<const std::uint8_t *>() ||
                     other.data<const std::uint8_t *>() > data<const std::uint8_t *>() + size());
    auto total = other.size();
    resize(total);
    std::memcpy(data(), other.data(), total);
    return *this;
}

ByteArray &ByteArray::operator=(const char *string)
{
    Q_ASSERT(string < data<const char *>() || string > data<const char *>() + size());
    auto total = std::strlen(string);
    resize(total);
    std::memcpy(data(), string, total);
    return *this;
}

ByteArray &ByteArray::operator=(const std::string &other)
{
    if (other.empty()) {
        resize(0);
        return *this;
    }
    auto total = other.size();
    resize(total);
    std::memcpy(data(), other.data(), total);
    return *this;
}


QByteArray ByteArray::toQByteArray() const
{
    if (empty())
        return {};
    Q_ASSERT(static_cast<int>(size()) > 0);
    Q_ASSERT(static_cast<size_type>(static_cast<int>(size())) == size());
    return QByteArray(data<const char *>(), static_cast<int>(size()));
}

QByteArray ByteArray::toQByteArrayRef() const
{
    if (empty())
        return {};
    Q_ASSERT(static_cast<int>(size()) > 0);
    Q_ASSERT(static_cast<size_type>(static_cast<int>(size())) == size());
    return QByteArray::fromRawData(data<const char *>(), static_cast<int>(size()));
}

std::string ByteArray::toString() const
{
    if (empty())
        return {};
    return std::string(data<const char *>(), size());
}

QString ByteArray::toQString(bool utf) const
{
    if (empty())
        return {};
    Q_ASSERT(static_cast<int>(size()) > 0);
    Q_ASSERT(static_cast<size_type>(static_cast<int>(size())) == size());
    if (utf) {
        return QString::fromUtf8(data<const char *>(), static_cast<int>(size()));
    } else {
        return QString::fromLatin1(data<const char *>(), static_cast<int>(size()));
    }
}

ByteArray::size_type ByteArray::indexOf(ByteArray::value_type find,
                                        ByteArray::size_type offset) const
{
    if (offset >= size())
        return npos;
    auto begin = this->begin();
    auto end = this->end();
    auto v = std::find(begin + offset, end, find);
    if (v == end)
        return npos;
    return static_cast<size_type>(v - begin);
}

ByteArray &ByteArray::string_to_upper()
{
    std::transform(begin(), end(), begin(), [](ByteArray::value_type c) -> ByteArray::value_type {
        return static_cast<ByteArray::value_type>(std::toupper(static_cast<char>(c)));
    });
    return *this;
}

ByteArray &ByteArray::string_to_lower()
{
    std::transform(begin(), end(), begin(), [](ByteArray::value_type c) -> ByteArray::value_type {
        return static_cast<ByteArray::value_type>(std::tolower(static_cast<char>(c)));
    });
    return *this;
}

ByteArray &ByteArray::string_trimmed()
{
    while (!empty() && std::isspace(back())) {
        pop_back();
    }
    if (empty())
        return *this;
    Q_ASSERT(!std::isspace(back()));
    size_type spaces = 0;
    while (std::isspace((*this)[spaces])) {
        ++spaces;
    }
    if (spaces) {
        Q_ASSERT(spaces < size());
        pop_front(spaces);
    }
    return *this;
}

bool ByteArray::Device::canReadLine() const
{
    if (!isOpen())
        return false;
    if (array.indexOf('\n', static_cast<size_type>(pos())) != npos)
        return true;
    return QIODevice::canReadLine();
}

bool ByteArray::Device::open(QIODevice::OpenMode flags)
{
    if ((flags & (Append | Truncate)) != 0)
        flags |= WriteOnly;

    if ((flags & (ReadOnly | WriteOnly)) == 0) {
        qCWarning(log_core_util) << "Device open mode not specified";
        return false;
    }

    if ((flags & Truncate) == Truncate)
        array.resize(0);

    return QIODevice::open(flags | QIODevice::Unbuffered);
}

bool ByteArray::Device::seek(qint64 pos)
{
    if (pos > static_cast<qint64>(array.size())) {
        if (isWritable()) {
            auto adding = pos - array.size();
            auto *target = array.tail(adding);
            std::memset(target, 0, adding);
        } else {
            return false;
        }
    } else if (pos < 0) {
        return false;
    }
    return QIODevice::seek(pos);
}

qint64 ByteArray::Device::size() const
{ return static_cast<qint64>(array.size()); }

qint64 ByteArray::Device::readData(char *data, qint64 len)
{
    qint64 offset = pos();
    qint64 available = static_cast<qint64>(array.size()) - offset;
    if (len > available)
        len = available;
    if (len <= 0)
        return 0;
    std::memcpy(data, array.data(static_cast<std::size_t>(offset)), static_cast<std::size_t>(len));
    return len;
}

qint64 ByteArray::Device::writeData(const char *data, qint64 len)
{
    if (len <= 0)
        return 0;

    auto finalOffset = static_cast<std::size_t>(pos() + len);
    if (finalOffset > array.size()) {
        array.resize(finalOffset);
    }

    std::memcpy(array.data(static_cast<std::size_t>(pos())), data, static_cast<std::size_t>(len));
    return len;
}

static ByteView readLine(bool acceptFinal, const ByteView &input, std::size_t &offset)
{
    while (offset < input.size()) {
        auto n = input.indexOf('\n', offset);
        if (n == ByteView::npos) {
            n = input.indexOf('\r', offset);
            if (n == ByteView::npos) {
                if (!acceptFinal)
                    return {};
                auto result = input.mid(offset);
                offset = input.size();
                return result;
            }
        } else if (n != offset) {
            auto r = input.mid(offset, n - offset).indexOf('\r');
            if (r != ByteView::npos) {
                n = r + offset;
            }
        }
        if (n == offset) {
            ++offset;
            continue;
        }

        auto result = input.mid(offset, n - offset);
        offset = n + 1;
        return result;
    }
    return {};
}

Util::ByteView ByteArray::LineIterator::next(bool acceptFinal)
{ return readLine(acceptFinal, array, offset); }


ByteView::ByteView(const ByteArray &parent, ByteArray::size_type offset)
{
    if (offset >= parent.size()) {
        ptr = nullptr;
        total = 0;
        return;
    }
    ptr = parent.data(offset);
    total = parent.size() - offset;
}

ByteView::ByteView(const ByteArray &parent, size_type offset, size_type count)
{
    if (offset >= parent.size()) {
        ptr = nullptr;
        total = 0;
        return;
    }
    ptr = parent.data(offset);
    total = parent.size() - offset;
    if (total > count)
        total = count;
}

ByteView::ByteView(const ByteView &parent, size_type offset)
{
    if (offset >= parent.size()) {
        ptr = nullptr;
        total = 0;
        return;
    }
    ptr = parent.data(offset);
    total = parent.size() - offset;
}

ByteView::ByteView(const ByteView &parent, size_type offset, size_type count)
{
    if (offset >= parent.size()) {
        ptr = nullptr;
        total = 0;
        return;
    }
    ptr = parent.data(offset);
    total = parent.size() - offset;
    if (total > count)
        total = count;
}

ByteView::ByteView(const QByteArray &parent) : ptr(
        reinterpret_cast<ByteArray::const_pointer>(parent.data())),
                                               total(static_cast<ByteArray::size_type>(parent.size()))
{ }

ByteView::ByteView(const std::string &parent) : ptr(
        reinterpret_cast<ByteArray::const_pointer>(parent.data())),
                                                total(static_cast<ByteArray::size_type>(parent.size()))
{ }

ByteView::ByteView(const char *parent) : ptr(reinterpret_cast<ByteArray::const_pointer>(parent)),
                                         total(0)
{
    if (parent)
        total = std::strlen(parent);
}

QByteArray ByteView::toQByteArray() const
{
    if (empty())
        return {};
    Q_ASSERT(static_cast<int>(size()) > 0);
    Q_ASSERT(static_cast<size_type>(static_cast<int>(size())) == size());
    return QByteArray(data<const char *>(), static_cast<int>(size()));
}

QByteArray ByteView::toQByteArrayRef() const
{
    if (empty())
        return {};
    Q_ASSERT(static_cast<int>(size()) > 0);
    Q_ASSERT(static_cast<size_type>(static_cast<int>(size())) == size());
    return QByteArray::fromRawData(data<const char *>(), static_cast<int>(size()));
}

std::string ByteView::toString() const
{
    if (empty())
        return {};
    return std::string(data<const char *>(), size());
}

QString ByteView::toQString(bool utf) const
{
    if (empty())
        return {};
    Q_ASSERT(static_cast<int>(size()) > 0);
    Q_ASSERT(static_cast<size_type>(static_cast<int>(size())) == size());
    if (utf) {
        return QString::fromUtf8(data<const char *>(), static_cast<int>(size()));
    } else {
        return QString::fromLatin1(data<const char *>(), static_cast<int>(size()));
    }
}

ByteView::size_type ByteView::indexOf(ByteView::value_type find, ByteView::size_type offset) const
{
    if (offset >= size())
        return npos;
    auto begin = this->begin();
    auto end = this->end();
    auto v = std::find(begin + offset, end, find);
    if (v == end)
        return npos;
    return static_cast<size_type>(v - begin);
}

std::vector<ByteView> ByteView::split_lines() const
{
    std::vector<ByteView> lines;
    size_type offset = 0;
    while (offset < size()) {
        auto n = indexOf('\n', offset);
        if (n == npos) {
            n = indexOf('\r', offset);
            if (n == npos) {
                lines.emplace_back(mid(offset));
                break;
            }
            lines.emplace_back(mid(offset, n - offset));
            offset = n + 1;
        } else if (n != offset) {
            auto lineData = mid(offset, n - offset);
            auto r = lineData.indexOf('\r');
            if (r != npos) {
                lineData = mid(offset, r);
                lines.emplace_back(std::move(lineData));
                if (n == offset + r + 1) {
                    offset = n + 1;
                } else {
                    offset = offset + r + 1;
                }
            } else {
                lines.emplace_back(std::move(lineData));
                offset = n + 1;
                if (offset < size() && (*this)[offset] == '\r')
                    ++offset;
            }
        } else {
            lines.emplace_back();
            offset = n + 1;
            if (offset < size() && (*this)[offset] == '\r')
                ++offset;
        }
    }
    return lines;
}

std::vector<ByteView> ByteView::split(value_type delimiter) const
{
    std::vector<ByteView> result;
    size_type offset = 0;
    while (offset < size()) {
        auto n = indexOf(delimiter, offset);
        if (n == npos) {
            result.emplace_back(mid(offset));
            break;
        }
        result.emplace_back(mid(offset, n - offset));
        offset = n + 1;
    }
    return result;
}

ByteView &ByteView::string_trimmed()
{
    for (const char *start = data<const char *>(), *end = start + size();
            start != end && std::isspace(*start);
            ++start, --total, ptr = start) { }
    if (empty())
        return *this;
    Q_ASSERT(!std::isspace(front()));
    for (const char *start = data<const char *>(), *end = start + size() - 1;
            start != end && std::isspace(*end);
            --end, --total) { }
    return *this;
}

bool ByteView::string_equal(const char *str) const
{
    const char *c = data<const char *>();
    const char *endC = c + size();
    for (; c != endC && *str; ++c, ++str) {
        if (*c != *str)
            return false;
    }
    return c == endC && !(*str);
}

bool ByteView::string_equal_insensitive(const char *str) const
{
    const char *c = data<const char *>();
    const char *endC = c + size();
    for (; c != endC && *str; ++c, ++str) {
        if (std::tolower(*c) != std::tolower(*str))
            return false;
    }
    return c == endC && !(*str);
}

bool ByteView::string_equal(const std::string &str) const
{
    const char *c = data<const char *>();
    const char *endC = c + size();
    auto s = str.cbegin();
    auto endS = str.cend();
    for (; c != endC && s != endS; ++c, ++s) {
        if (*c != *s)
            return false;
    }
    return c == endC && s == endS;
}

bool ByteView::string_equal_insensitive(const std::string &str) const
{
    const char *c = data<const char *>();
    const char *endC = c + size();
    auto s = str.cbegin();
    auto endS = str.cend();
    for (; c != endC && s != endS; ++c, ++s) {
        if (std::tolower(*c) != std::tolower(*s))
            return false;
    }
    return c == endC && s == endS;
}

bool ByteView::string_start(const char *str) const
{
    const char *c = data<const char *>();
    const char *endC = c + size();
    for (; *str; ++c, ++str) {
        if (c == endC)
            return false;
        if (*c != *str)
            return false;
    }
    return !(*str);
}

bool ByteView::string_start_insensitive(const char *str) const
{
    const char *c = data<const char *>();
    const char *endC = c + size();
    for (; *str; ++c, ++str) {
        if (c == endC)
            return false;
        if (std::tolower(*c) != std::tolower(*str))
            return false;
    }
    return !(*str);
}

bool ByteView::string_start(const std::string &str) const
{
    const char *c = data<const char *>();
    const char *endC = c + size();
    auto s = str.cbegin();
    auto endS = str.cend();
    for (; s != endS; ++c, ++s) {
        if (c == endC)
            return false;
        if (*c != *s)
            return false;
    }
    return s == endS;
}

bool ByteView::string_start_insensitive(const std::string &str) const
{
    const char *c = data<const char *>();
    const char *endC = c + size();
    auto s = str.cbegin();
    auto endS = str.cend();
    for (; s != endS; ++c, ++s) {
        if (c == endC)
            return false;
        if (std::tolower(*c) != std::tolower(*s))
            return false;
    }
    return s == endS;
}

bool ByteView::string_end(const char *str) const
{
    auto elen = std::strlen(str);
    if (!elen)
        return true;
    if (elen > size())
        return false;
    const char *c = data<const char *>(size() - elen);
    for (; *str; ++c, ++str) {
        if (*c != *str)
            return false;
    }
    return true;
}

bool ByteView::string_end_insensitive(const char *str) const
{
    auto elen = std::strlen(str);
    if (!elen)
        return true;
    if (elen > size())
        return false;
    const char *c = data<const char *>(size() - elen);
    for (; *str; ++c, ++str) {
        if (std::tolower(*c) != std::tolower(*str))
            return false;
    }
    return true;
}

bool ByteView::string_end(const std::string &str) const
{
    if (str.empty())
        return true;
    if (str.size() > size())
        return false;
    const char *c = data<const char *>(size() - str.size());
    auto s = str.cbegin();
    auto endS = str.cend();
    for (; s != endS; ++c, ++s) {
        if (*c != *s)
            return false;
    }
    return true;
}

bool ByteView::string_end_insensitive(const std::string &str) const
{
    if (str.empty())
        return true;
    if (str.size() > size())
        return false;
    const char *c = data<const char *>(size() - str.size());
    auto s = str.cbegin();
    auto endS = str.cend();
    for (; s != endS; ++c, ++s) {
        if (std::tolower(*c) != std::tolower(*s))
            return false;
    }
    return true;
}

std::int16_t ByteView::parse_i16(bool *ok, int base) const
{ return toQByteArrayRef().toShort(ok, base); }

std::uint16_t ByteView::parse_u16(bool *ok, int base) const
{ return toQByteArrayRef().toUShort(ok, base); }

std::int32_t ByteView::parse_i32(bool *ok, int base) const
{ return toQByteArrayRef().toInt(ok, base); }

std::uint32_t ByteView::parse_u32(bool *ok, int base) const
{ return toQByteArrayRef().toUInt(ok, base); }

std::int64_t ByteView::parse_i64(bool *ok, int base) const
{ return toQByteArrayRef().toLongLong(ok, base); }

std::uint64_t ByteView::parse_u64(bool *ok, int base) const
{ return toQByteArrayRef().toULongLong(ok, base); }

double ByteView::parse_real(bool *ok) const
{ return toQByteArrayRef().toDouble(ok); }

bool ByteView::Device::canReadLine() const
{
    if (!isOpen())
        return false;
    if (ref.indexOf('\n', static_cast<size_type>(pos())) != npos)
        return true;
    return QIODevice::canReadLine();
}

bool ByteView::Device::open(QIODevice::OpenMode flags)
{
    if ((flags & WriteOnly) != 0) {
        qCWarning(log_core_util) << "Cannot open byte reference devices in write mode";
        return false;
    }
    if ((flags & (ReadOnly)) == 0) {
        qCWarning(log_core_util) << "Device open mode not specified";
        return false;
    }

    return QIODevice::open(flags | QIODevice::Unbuffered);
}

bool ByteView::Device::seek(qint64 pos)
{
    if (pos > static_cast<qint64>(ref.size())) {
        return false;
    } else if (pos < 0) {
        return false;
    }
    return QIODevice::seek(pos);
}

qint64 ByteView::Device::size() const
{ return static_cast<qint64>(ref.size()); }

qint64 ByteView::Device::readData(char *data, qint64 len)
{
    qint64 offset = pos();
    qint64 available = static_cast<qint64>(ref.size()) - offset;
    if (len > available)
        len = available;
    if (len <= 0)
        return 0;
    std::memcpy(data, ref.data(static_cast<std::size_t>(offset)), static_cast<std::size_t>(len));
    return len;
}

qint64 ByteView::Device::writeData(const char *, qint64)
{ return -1; }

Util::ByteView ByteView::LineIterator::next(bool acceptFinal)
{ return readLine(acceptFinal, ref, offset); }


/* Compliant with QByteArray's serialization, so limited to int32 size */
QDataStream &operator<<(QDataStream &stream, const ByteArray &a)
{
    quint32 n = static_cast<quint32>(a.size());
    Q_ASSERT(static_cast<ByteArray::size_type>(n) == a.size());
    Q_ASSERT(n < 0x7FFFFFFF);
    stream << n;
    stream.writeRawData(a.data<const char *>(), static_cast<int>(n));
    return stream;
}

QDataStream &operator<<(QDataStream &stream, const ByteView &a)
{
    quint32 n = static_cast<quint32>(a.size());
    Q_ASSERT(static_cast<ByteArray::size_type>(n) == a.size());
    Q_ASSERT(n < 0x7FFFFFFF);
    stream << n;
    stream.writeRawData(a.data<const char *>(), static_cast<int>(n));
    return stream;
}

QDataStream &operator>>(QDataStream &stream, ByteArray &a)
{
    quint32 n = 0;
    stream >> n;
    if (n == 0xFFFFFFFF) {
        a.clear();
        return stream;
    }
    Q_ASSERT(n < 0x7FFFFFFF);

    a.resize(n);
    if (stream.readRawData(a.data<char *>(), static_cast<int>(n)) != static_cast<int>(n)) {
        a.clear();
        stream.setStatus(QDataStream::ReadPastEnd);
    }
    return stream;
}

enum class ByteOutputMode {
    ASCII, ASCII_CR, ASCII_LF, ASCII_CRLF, ASCII_LFCR, Hex,
};

template<typename T>
static ByteOutputMode getByteOutputMode(const T &input)
{
    for (auto c = input.begin(), endC = input.end(); c != endC; ++c) {
        if (std::isprint(static_cast<char>(*c)))
            continue;

        if (*c == '\r') {
            auto next = c + 1;
            if (next == endC)
                return ByteOutputMode::ASCII_CR;
            if (*next == '\n') {
                if (next + 1 == endC)
                    return ByteOutputMode::ASCII_CRLF;
            }
            return ByteOutputMode::Hex;
        }

        if (*c == '\n') {
            auto next = c + 1;
            if (next == endC)
                return ByteOutputMode::ASCII_LF;
            if (*next == '\r') {
                if (next + 1 == endC)
                    return ByteOutputMode::ASCII_LFCR;
            }
            return ByteOutputMode::Hex;
        }

        return ByteOutputMode::Hex;
    }
    return ByteOutputMode::ASCII;
}

static void hexPrint(QDebug &stream, std::uint8_t n)
{
    QDebugStateSaver saver(stream);
    stream << hex << qSetFieldWidth(2) << qSetPadChar('0') << n;
}

template<typename T>
static void byteSequenceOutput(QDebug &stream, const T &input, const char *name)
{
    QDebugStateSaver saver(stream);
    stream.nospace();

    ByteView output;
    switch (getByteOutputMode(input)) {
    case ByteOutputMode::ASCII:
        stream << name << "(";
        output = input;
        break;
    case ByteOutputMode::ASCII_CR:
        stream << name << "CR(";
        output = input.mid(0, input.size() - 1);
        break;
    case ByteOutputMode::ASCII_LF:
        stream << name << "LF(";
        output = input.mid(0, input.size() - 1);
        break;
    case ByteOutputMode::ASCII_CRLF:
        stream << name << "CRLF(";
        output = input.mid(0, input.size() - 2);
        break;
    case ByteOutputMode::ASCII_LFCR:
        stream << name << "LFCR(";
        output = input.mid(0, input.size() - 2);
        break;
    case ByteOutputMode::Hex: {
        stream << name << "Raw(";
        bool first = true;
        for (auto p : input) {
            if (!first)
                stream << ' ';
            first = false;
            hexPrint(stream, p);
        }
        stream << ')';
        return;
    }
    }

    for (auto c : output) {
        stream << static_cast<char>(c);
    }
    stream << ')';
}

QDebug operator<<(QDebug stream, const ByteArray &a)
{
    byteSequenceOutput(stream, a, "ByteArray");
    return stream;
}

QDebug operator<<(QDebug stream, const ByteView &a)
{
    byteSequenceOutput(stream, a, "ByteReference");
    return stream;
}

bool operator==(const ByteView &a, const ByteView &b)
{
    if (a.size() != b.size())
        return false;
    return ::memcmp(a.data(), b.data(), a.size()) == 0;
}

bool operator!=(const ByteView &a, const ByteView &b)
{
    if (a.size() != b.size())
        return true;
    return ::memcmp(a.data(), b.data(), a.size()) != 0;
}


}
}