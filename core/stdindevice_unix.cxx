/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/select.h>
#include <errno.h>

#include <QLoggingCategory>

#include "core/stdindevice.hxx"
#include "core/range.hxx"

Q_LOGGING_CATEGORY(log_core_stdindevice, "cpd3.core.stdindevice", QtWarningMsg)

namespace CPD3 {

StdinDevice::StdinDevice(QObject *parent) : QIODevice(parent),
                                            thread(),
                                            mutex(),
                                            terminated(false),
                                            data(),
                                            finished(false),
                                            fdNotify(-1)
{ }

StdinDevice::~StdinDevice()
{
    if (thread.joinable()) {
        signalTerminate();
        thread.join();
    }
}

qint64 StdinDevice::bytesAvailable() const
{
    qint64 n = 0;
    {
        std::lock_guard<std::mutex> lock(const_cast<std::mutex &>(mutex));
        n = data.size();
    }
    return n + QIODevice::bytesAvailable();
}

void StdinDevice::close()
{
    if (thread.joinable()) {
        signalTerminate();
        thread.join();
    }
    QIODevice::close();
}

bool StdinDevice::open(OpenMode mode)
{
    Q_ASSERT(!(mode & QIODevice::QIODevice::WriteOnly));

    Q_ASSERT(!thread.joinable());
    thread = std::thread(&StdinDevice::run, this);

    return QIODevice::open(mode);
}

bool StdinDevice::atEnd() const
{
    if (QIODevice::bytesAvailable() != 0)
        return false;
    std::lock_guard<std::mutex> lock(const_cast<std::mutex &>(mutex));
    return data.size() == 0 && finished;
}

bool StdinDevice::isSequential() const
{ return true; }

qint64 StdinDevice::readData(char *data, qint64 maxSize)
{
    qint64 n = -1;

    std::unique_lock<std::mutex> lock(mutex);
    int length = this->data.length();
    if (length == 0) {
        if (!finished) {
            n = 0;
        } else {
            lock.unlock();
            if (thread.joinable())
                thread.join();
            return -1;
        }
    } else if (length <= maxSize) {
        ::memcpy(data, this->data.constData(), static_cast<size_t>(length));
        n = length;
        if (finished) {
            this->data.clear();
            lock.unlock();
            if (thread.joinable())
                thread.join();
            return n;
        }
    } else {
        ::memcpy(data, this->data.constData(), static_cast<size_t>(maxSize));
        n = maxSize;
    }
    if (n > 0) {
        this->data.remove(0, n);
        external.notify_all();
    }

    return n;
}

void StdinDevice::run()
{
    static const int fdStdin = 0;

    int flags;
    if ((flags = ::fcntl(fdStdin, F_GETFL)) == -1) {
        qCWarning(log_core_stdindevice) << "Can't get stdin flags:" << ::strerror(errno);
    } else {
        flags |= O_NONBLOCK;
        if (::fcntl(fdStdin, F_SETFL, flags) == -1) {
            qCWarning(log_core_stdindevice) << "Can't set stdin flags:" << ::strerror(errno);
        }
    }

    int pipeFDs[2];
    if (::pipe(pipeFDs) == -1) {
        qCWarning(log_core_stdindevice) << "Can't create signal pipe:" << ::strerror(errno);

        {
            std::lock_guard<std::mutex> lock(mutex);
            finished = true;
        }
        ::close(fdStdin);

        emit readChannelFinished();
        return;
    }

    {
        std::unique_lock<std::mutex> lock(mutex);
        fdNotify = pipeFDs[1];
    }

    int wakeFD = pipeFDs[0];

    int nFDs = std::max(fdStdin, wakeFD) + 1;

    std::vector<char> buffer;
    buffer.resize(32752);
    for (;;) {
        {
            std::unique_lock<std::mutex> lock(mutex);
            if (terminated) {
                fdNotify = -1;
                finished = true;
                lock.unlock();
                ::close(fdStdin);
                ::close(pipeFDs[0]);
                ::close(pipeFDs[1]);

                emit readChannelFinished();
                return;
            }
        }

        fd_set rd;
        fd_set ex;
        FD_ZERO(&rd);
        FD_ZERO(&ex);
        FD_SET(fdStdin, &rd);
        FD_SET(fdStdin, &ex);
        if (wakeFD != -1) {
            FD_SET(wakeFD, &rd);
            FD_SET(wakeFD, &ex);
        }

        struct timeval tv;
        tv.tv_sec = 0;
        tv.tv_usec = 100000;
        int rv = ::select(nFDs, &rd, NULL, &ex, &tv);

        if (::fcntl(fdStdin, F_GETFD) != 0) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                finished = true;
                fdNotify = -1;
            }

            ::close(pipeFDs[0]);
            ::close(pipeFDs[1]);

            emit readChannelFinished();
            return;
        }

        if (rv == 0)
            continue;
        if (rv == -1 && errno == EINTR)
            continue;

        if (wakeFD != -1 && (FD_ISSET(wakeFD, &rd) || FD_ISSET(wakeFD, &ex))) {
            char discard[8];
            ssize_t n = ::read(wakeFD, discard, sizeof(discard));
            if ((n == -1 && errno != EAGAIN) || n == 0) {
                qCWarning(log_core_stdindevice) << "Error reading from notification:"
                                                << ::strerror(errno);

                wakeFD = -1;
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    fdNotify = -1;
                }
                ::close(pipeFDs[0]);
                ::close(pipeFDs[1]);
            }
        }

        if (!FD_ISSET(fdStdin, &rd) && !FD_ISSET(fdStdin, &ex))
            continue;

        ssize_t n = ::read(fdStdin, buffer.data(), buffer.size());
        if (n == -1) {
            if (errno == EAGAIN)
                continue;
            {
                std::lock_guard<std::mutex> lock(mutex);
                finished = true;
                fdNotify = -1;
            }

            ::close(pipeFDs[0]);
            ::close(pipeFDs[1]);
            ::close(fdStdin);

            emit readChannelFinished();
            return;
        } else if (n == 0) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                finished = true;
                fdNotify = -1;
            }

            ::close(pipeFDs[0]);
            ::close(pipeFDs[1]);
            ::close(fdStdin);

            emit readChannelFinished();
            return;
        }

        bool wasBufferEmpty;
        {
            std::unique_lock<std::mutex> lock(mutex);
            while (data.size() > 16 * 1024 * 1024) {
                if (terminated)
                    break;
                external.wait(lock);
            }
            wasBufferEmpty = data.isEmpty();
            data.append(buffer.data(), (int) n);
        }

        if (wasBufferEmpty) {
            emit readyRead();
        }

    }
}

void StdinDevice::signalTerminate()
{
    std::unique_lock<std::mutex> lock(mutex);
    terminated = true;
    external.notify_all();
    if (fdNotify != -1) {
        char b = 0;
        ::write(fdNotify, &b, 1);
    }
}

qint64 StdinDevice::writeData(const char *, qint64)
{
    Q_ASSERT(false);
    return 0;
}

}
