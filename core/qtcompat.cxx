/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <mutex>
#include <QCoreApplication>
#include <QLoggingCategory>
#include <QFile>

#ifdef Q_OS_UNIX

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#endif

#include "qtcompat.hxx"
#include "number.hxx"
#include "timeutils.hxx"

namespace CPD3 {
namespace Logging {

time::time(double value) : value(value)
{ }

QDebug operator<<(QDebug stream, const time &value)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream.noquote();
    if (FP::defined(value.value)) {
        if (value.value < 1E6) {
            stream << fixed << qSetRealNumberPrecision(3) << value.value;
        } else {
            stream << Time::toISO8601(value.value, true);
        }
    } else {
        stream << "UNDEFINED";
    }
    return stream;
}


range::range(double start, double end) : start(start), end(end)
{ }

QDebug operator<<(QDebug stream, const range &value)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream.noquote();
    if (FP::defined(value.start)) {
        stream << '(';
        if (value.start < 1E6) {
            stream << fixed << qSetRealNumberPrecision(3) << value.start;
        } else {
            stream << Time::toISO8601(value.start, true);
        }
        stream << ',';
    } else {
        stream << "(-INFINITY,";
    }
    if (FP::defined(value.end)) {
        if (value.end < 1E6) {
            stream << fixed << qSetRealNumberPrecision(3) << value.end;
        } else {
            stream << Time::toISO8601(value.end, true);
        }
        stream << ')';
    } else {
        stream << "+INFINITY)";
    }
    return stream;
}


elapsed::elapsed(double seconds) : seconds(seconds)
{ }

QDebug operator<<(QDebug stream, const elapsed &value)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    if (FP::defined(value.seconds)) {
        stream.noquote() << Time::describeDuration(value.seconds);
    } else {
        stream << "UNDEFINED";
    }
    return stream;
}

static void testSuppressCategoryFilter(QLoggingCategory *category)
{
    if (qstrncmp(category->categoryName(), "cpd3.", 4) == 0) {
        category->setEnabled(QtDebugMsg, false);
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
        category->setEnabled(QtInfoMsg, false);
#endif
    }
}

void suppressForTesting()
{
    {
        auto value = ::qgetenv("CPD3_TESTVERBOSE");
        bool ok = false;
        if (!value.isEmpty() && value.trimmed().toInt(&ok) && ok)
            return;
    }

    QLoggingCategory::installFilter(testSuppressCategoryFilter);
}

static void testGlobalSuppressCategoryFilter(QLoggingCategory *category)
{
    if (qstrncmp(category->categoryName(), "cpd3.", 4) == 0) {
        category->setEnabled(QtDebugMsg, false);
        category->setEnabled(QtWarningMsg, false);
        category->setEnabled(QtCriticalMsg, false);
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
        category->setEnabled(QtInfoMsg, false);
#endif
    }
}

void suppressGlobalForTesting()
{
    QLoggingCategory::installFilter(testGlobalSuppressCategoryFilter);
}

static void consoleSuppressCategoryFilter(QLoggingCategory *category)
{
    category->setEnabled(QtDebugMsg, false);
    category->setEnabled(QtWarningMsg, false);
    category->setEnabled(QtCriticalMsg, false);
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
    category->setEnabled(QtInfoMsg, false);
#endif
}

void suppressForConsole()
{
    {
        auto value = ::qgetenv("CPD3_LOG");
        if (!value.isEmpty())
            return;
    }
    {
        auto value = ::qgetenv("QT_LOGGING_TO_CONSOLE");
        bool ok = false;
        if (!value.isEmpty() && !value.trimmed().toInt(&ok) && ok)
            return;
    }

    QLoggingCategory::installFilter(consoleSuppressCategoryFilter);
    /* So child processes don't generate output */
    ::qputenv("QT_LOGGING_RULES", "*=false");
}

}
}


QDataStream &operator<<(QDataStream &stream, const std::string &v)
{
    quint32 n = static_cast<quint32>(v.length());
    Q_ASSERT(static_cast<std::size_t>(static_cast<int>(n)) == v.length());
    stream << n;
    stream.writeRawData(reinterpret_cast<const char *>(v.data()), n);
    return stream;
}

QDataStream &operator>>(QDataStream &stream, std::string &v)
{
    v.clear();
    quint32 n = 0;
    stream >> n;
    if (n <= 0)
        return stream;

    std::vector<char> raw;
    while (n > 0) {
        if (stream.status() != QDataStream::Ok) {
            v.clear();
            break;
        }
        raw.resize(std::min<std::size_t>(n, 65536));
        stream.readRawData(reinterpret_cast<char *>(raw.data()), static_cast<int>(raw.size()));
        v.append(reinterpret_cast<const char *>(raw.data()), raw.size());
        n -= raw.size();
    }
    return stream;
}

QDebug operator<<(QDebug stream, const std::string &value)
{
    QDebugStateSaver saver(stream);
    stream << QString::fromStdString(value);
    return stream;
}


namespace {

class RedirectHandler final {
    QString fileName;
    QtMessageHandler oldHandler;
public:
    RedirectHandler()
    {
        auto value = ::qgetenv("CPD3_LOG");
        if (value.isEmpty())
            return;
        fileName = QString::fromUtf8(value);
        if (fileName.isEmpty())
            return;

        oldHandler = qInstallMessageHandler(messageHandler);
    }

    ~RedirectHandler()
    {
        qInstallMessageHandler(0);
    }

private:

    static void messageHandler(QtMsgType type,
                               const QMessageLogContext &context,
                               const QString &msg);
};

static RedirectHandler redirectHandler;

void RedirectHandler::messageHandler(QtMsgType type,
                                     const QMessageLogContext &context,
                                     const QString &msg)
{
    QString line;

    QDateTime currentTime = QDateTime::currentDateTimeUtc();
    line.append(currentTime.toString("yyyy-MM-ddThh:mm:ss.zzzZ"));
    line.append(' ');
    line.append(context.category);
    line.append(' ');
    if (context.function) {
        line.append(context.function);
        line.append(' ');
    }
    if (context.file && context.line) {
        line.append(context.file);
        line.append(':');
        line.append(QString::number(context.line));
        line.append(' ');
    }

    switch (type) {
    case QtDebugMsg:
        line.append('D');
        break;
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
    case QtInfoMsg:
        line.append('I');
        break;
#endif
    case QtWarningMsg:
        line.append('W');
        break;
    case QtCriticalMsg:
        line.append('C');
        break;
    case QtFatalMsg:
        line.append('F');
        break;
    }

    line.append('(');
    line.append(QString::number(static_cast<int>(QCoreApplication::applicationPid())));
    line.append("): ");
    line.append(msg);
    line.append('\n');

#ifdef Q_OS_UNIX
    int flags = O_NOCTTY | O_APPEND | O_CREAT | O_WRONLY;
#ifdef O_CLOEXEC
    flags |= O_CLOEXEC;
#endif
    int fd = ::open(redirectHandler.fileName.toUtf8().data(), flags, 0666);
    if (fd < 0) {
        redirectHandler.oldHandler(type, context, msg);
        return;
    }
    auto contents = line.toUtf8();
    ::write(fd, contents.data(), static_cast<::size_t>(contents.size()));
    ::close(fd);
#else
    static std::mutex mutex;
    std::lock_guard<std::mutex> lock(mutex);
    QFile file(redirectHandler.fileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
        redirectHandler.oldHandler(type, context, msg);
        return;
    }

    file.write(line.toUtf8());
    file.flush();
    file.close();
#endif
}

}