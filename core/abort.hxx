/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3COREABORT_H
#define CPD3COREABORT_H

#include "core/first.hxx"

#include <limits.h>
#include <QtGlobal>
#include <QObject>
#include <QTimer>
#include "core/core.hxx"

namespace CPD3 {

/**
 * Provides quick utilities to poll for forced external abort (e.g. CTRL-C).
 */
class CPD3CORE_EXPORT Abort {
public:
    /**
     * Install the appropriate abort handlers if needed. 
     * 
     * @param abortOnHangup if true then trigger an abort on hangup (terminal closed)
     */
    static void installAbortHandler(bool abortOnHangup = false);

    /**
     * Poll if an abort was detected.
     * 
     * @return true if the program had an abort requested.
     */
    static bool aborted();
};

/**
 * A utility class to poll for aborts and emit a signal when one is detected.
 */
class CPD3CORE_EXPORT AbortPoller : public QObject {
Q_OBJECT

private:
    QTimer *timer;

public:
    /**
     * Create (but do not start) the abort polling.
     */
    AbortPoller(QObject *parent = 0);

    ~AbortPoller();

    /**
     * Start or restart the abort polling.
     */
    void start();

    /**
     * Stop the abort polling.
     */
    void stop();

signals:

    /**
     * Emitted when an abort is detected.  The polling is stopped so this will
     * only be emitted once unless another start is issued.
     */
    void aborted();

private slots:

    void checkAborted();
};

}

#endif
