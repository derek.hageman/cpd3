/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "memory.hxx"

namespace CPD3 {
namespace Memory {

static ReleaseFunction release_function = nullptr;

void install_release_function(ReleaseFunction release)
{
    release_function = release;
}

void release_unused()
{
    if (!release_function)
        return;
    release_function();
}

}
}