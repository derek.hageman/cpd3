/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3CORETIMEUTILS_H
#define CPD3CORETIMEUTILS_H

#include "core/first.hxx"

#include <QString>
#include <QObject>
#include <QMetaType>
#include <QDataStream>
#include <QDateTime>
#include <QDebug>

#include "core/core.hxx"

namespace CPD3 {

/**
 * Contains general time utilities.
 */
class CPD3CORE_EXPORT Time : public QObject {
Q_OBJECT
public:
    /**
     * Logical time units.
     */
    enum LogicalTimeUnit {
        Millisecond = 0, Second, Minute, Hour, Day, Week, Month, Quarter, Year
    };

    /**
     * A simple class containing a time bound (start and end).  The 
     * overlay functions do nothing.
     */
    class CPD3CORE_EXPORT Bounds {
    public:
        /**
         * The start (inclusive) bound.
         */
        double start;
        /**
         * The end (exclusive) bound.
         */
        double end;

        Bounds();

        Bounds(const double start, const double end);

        Bounds(const Bounds &b);

        Bounds(const Bounds &b, const double start, const double end);

        Bounds &operator=(const Bounds &other);

        ~Bounds();

        bool operator==(const Bounds &other) const;

        /**
         * Initialize the bounds object.
         * 
         * @param o     an unused parameter for range overlay
         * @param st    the start time to set
         * @param ed    the end time to set
         */
        template<typename T>
        Bounds(const T &o, double st, double ed) : start(st), end(ed)
        { Q_UNUSED(o); }

        /**
         * Initialize the bounds object.
         * 
         * @param o     an unused parameter for range overlay
         * @param u     an unused parameter for range overlay
         * @param st    the start time to set
         * @param ed    the end time to set
         */
        template<typename T>
        Bounds(const Bounds &u, const T &o, double st, double ed) : start(st), end(ed)
        {
            Q_UNUSED(u);
            Q_UNUSED(o);
        }

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double s)
        { start = s; }

        inline void setEnd(double e)
        { end = e; }
    };

    static double time();

    static QDateTime toDateTime(double time, bool includeFractional = false);

    static double fromDateTime(const QDateTime &time, bool includeFractional = false);

    static double convertYearDOY(int year, double doy, bool roundSecond = true);

    static double quarterStart(int year, int qtr);

    static double quarterEnd(int year, int qtr);

    static void containingQuarter(double time, int *year, int *qtr);

    static int julianDay(double time);

    static double weekStart(int year, int week);

    static double logical(double time,
                          LogicalTimeUnit unit,
                          int count,
                          bool align = false,
                          bool roundUp = false,
                          int multiplier = 1);

    static double ceil(double time, LogicalTimeUnit unit, int count = 1);

    static double floor(double time, LogicalTimeUnit unit, int count = 1);

    static double round(double time, LogicalTimeUnit unit, int count = 1);

    static QString toISO8601(double time, bool includeFractional = false);

    static QString toISO8601ND(double time, bool includeFractional = false);

    static QString describeDuration(double seconds, int decimals = 1);

    enum YearDOYMode {
        /** Always include both the year and DOY. */
                YearAndDOY, /** Never include the year. */
                DOYOnly, /** Include the DOY only if it is not exactly 1 and the rounding
         * mode is years. */
                DOYAsNeeded
    };

    static QString toYearDOY(double time,
                             const QString &separator = QString(':'),
                             LogicalTimeUnit rounding = Year,
                             const QChar &padDOY = QLatin1Char('0'),
                             YearDOYMode formatMode = YearAndDOY);

    static QString describeOffset(LogicalTimeUnit unit, int count, bool align = false);
};

/**
 * A caching converter for year and DOY pairs.  This is slightly faster than
 * repeating the year start conversion each call on data streams that are 
 * mostly localized in a year.  Reentrant.
 */
class CPD3CORE_EXPORT CachingYearDOYConverter {
private:
    int lastYear;
    double lastYearStart;
public:
    CachingYearDOYConverter();

    CachingYearDOYConverter(const CachingYearDOYConverter &copy);

    double convert(int year, double doy);

    double convertUnrounded(int year, double doy);
};

CPD3CORE_EXPORT QDebug operator<<(QDebug dbg, const Time::Bounds &v);

CPD3CORE_EXPORT QDebug operator<<(QDebug dbg, const Time::LogicalTimeUnit &v);

CPD3CORE_EXPORT QDataStream &operator<<(QDataStream &stream, const Time::Bounds &value);

CPD3CORE_EXPORT QDataStream &operator>>(QDataStream &stream, Time::Bounds &value);

};

Q_DECLARE_METATYPE(CPD3::Time::LogicalTimeUnit)

Q_DECLARE_TYPEINFO(CPD3::Time::LogicalTimeUnit, Q_PRIMITIVE_TYPE);

Q_DECLARE_TYPEINFO(CPD3::Time::Bounds, Q_PRIMITIVE_TYPE);

Q_DECLARE_METATYPE(CPD3::Time::Bounds)

#endif
