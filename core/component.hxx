/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3CORECOMPONENT_H
#define CPD3CORECOMPONENT_H

#include "core/first.hxx"

#include <unordered_set>
#include <unordered_map>
#include <QtGlobal>
#include <QObject>
#include <QtPlugin>
#include <QPluginLoader>
#include <QStaticPlugin>
#include <QHash>
#include <QSharedData>
#include <QSet>
#include <QStringList>
#include <QDir>

#include "core/core.hxx"
#include "core/timeutils.hxx"
#include "core/number.hxx"

class QJsonObject;

namespace CPD3 {

class CPD3CORE_EXPORT ComponentOptionBase : public QObject {
Q_OBJECT

private:

    QString argumentName;
    QString displayName;
    QString description;
    QString defaultBehavior;
    int sortPriority;

protected:
    bool optionSet;

public:
    ComponentOptionBase(const QString &setArgumentName,
                        const QString &setDisplayName,
                        const QString &setDescription,
                        const QString &setDefaultBehavior,
                        int setSortPriority = 0);

    ComponentOptionBase(const ComponentOptionBase &other);

    inline QString getArgumentName() const
    { return argumentName; }

    inline QString getDisplayName() const
    { return displayName; }

    inline QString getDescription() const
    { return description; }

    inline QString getDefaultBehavior() const
    { return defaultBehavior; }

    inline int getSortPriority() const
    { return sortPriority; }

    inline bool isSet() const
    { return optionSet; }

    virtual void reset() = 0;

    virtual ComponentOptionBase *clone() const = 0;
};

class CPD3CORE_EXPORT ComponentOptionSingleString : public ComponentOptionBase {
Q_OBJECT

private:
    QString value;
public:
    ComponentOptionSingleString(const QString &argumentName,
                                const QString &displayName,
                                const QString &description,
                                const QString &defaultBehavior,
                                int sortPriority = 0);

    ComponentOptionSingleString(const ComponentOptionSingleString &other);

    QString get() const;

    void set(const QString &s);

    virtual void reset();

    virtual ComponentOptionBase *clone() const;
};

class CPD3CORE_EXPORT ComponentOptionFile : public ComponentOptionSingleString {
Q_OBJECT

public:
    enum Mode {
        Read, Write, ReadWrite
    };

private:
    Mode mode;
    QString typeDescription;
    QSet<QString> extensions;
public:
    ComponentOptionFile(const QString &argumentName,
                        const QString &displayName,
                        const QString &description,
                        const QString &defaultBehavior,
                        int sortPriority = 0);

    ComponentOptionFile(const ComponentOptionFile &other);

    Mode getMode() const;

    void setMode(Mode mode);

    QString getTypeDescription() const;

    void setTypeDescription(const QString &d);

    QSet<QString> getExtensions() const;

    void setExtensions(const QSet<QString> &extensions);

    virtual ComponentOptionBase *clone() const;
};

class CPD3CORE_EXPORT ComponentOptionDirectory : public ComponentOptionSingleString {
Q_OBJECT

public:
    ComponentOptionDirectory(const QString &argumentName,
                             const QString &displayName,
                             const QString &description,
                             const QString &defaultBehavior,
                             int sortPriority = 0);

    ComponentOptionDirectory(const ComponentOptionDirectory &other);

    virtual ComponentOptionBase *clone() const;
};


class CPD3CORE_EXPORT ComponentOptionScript : public ComponentOptionSingleString {
Q_OBJECT

public:
    ComponentOptionScript(const QString &argumentName,
                          const QString &displayName,
                          const QString &description,
                          const QString &defaultBehavior,
                          int sortPriority = 0);

    ComponentOptionScript(const ComponentOptionScript &other);

    virtual ComponentOptionBase *clone() const;
};

class CPD3CORE_EXPORT ComponentOptionSequenceMatch : public ComponentOptionSingleString {
Q_OBJECT

public:
    ComponentOptionSequenceMatch(const QString &argumentName,
                          const QString &displayName,
                          const QString &description,
                          const QString &defaultBehavior,
                          int sortPriority = 0);

    ComponentOptionSequenceMatch(const ComponentOptionScript &other);

    virtual ComponentOptionBase *clone() const;
};

class CPD3CORE_EXPORT ComponentOptionSingleDouble : public ComponentOptionBase {
Q_OBJECT

private:
    double value;
    double max;
    double min;
    bool maxInclusive;
    bool minInclusive;
    bool allowUndefined;
public:
    ComponentOptionSingleDouble(const QString &argumentName,
                                const QString &displayName,
                                const QString &description,
                                const QString &defaultBehavior,
                                int sortPriority = 0);

    ComponentOptionSingleDouble(const ComponentOptionSingleDouble &other);

    double get() const;

    void set(double v);

    virtual void reset();

    virtual ComponentOptionBase *clone() const;

    void setMaximum(double m, bool inclusive = true);

    void setMinimum(double m, bool inclusive = true);

    double getMaximum() const;

    double getMinimum() const;

    bool getMaximumInclusive() const;

    bool getMinimumInclusive() const;

    void setAllowUndefined(bool a);

    bool getAllowUndefined() const;

    bool isValid(double check) const;
};

class CPD3CORE_EXPORT ComponentOptionSingleInteger : public ComponentOptionBase {
Q_OBJECT

private:
    qint64 value;
    qint64 max;
    qint64 min;
    bool allowUndefined;
public:
    ComponentOptionSingleInteger(const QString &argumentName,
                                 const QString &displayName,
                                 const QString &description,
                                 const QString &defaultBehavior,
                                 int sortPriority = 0);

    ComponentOptionSingleInteger(const ComponentOptionSingleInteger &other);

    qint64 get() const;

    void set(qint64 v);

    virtual void reset();

    virtual ComponentOptionBase *clone() const;

    void setMaximum(qint64 m);

    void setMinimum(qint64 m);

    qint64 getMaximum() const;

    qint64 getMinimum() const;

    void setAllowUndefined(bool a);

    bool getAllowUndefined() const;

    bool isValid(qint64 check) const;
};

class CPD3CORE_EXPORT ComponentOptionDoubleSet : public ComponentOptionBase {
Q_OBJECT

private:
    QSet<double> values;
    double max;
    double min;
    bool maxInclusive;
    bool minInclusive;
public:
    ComponentOptionDoubleSet(const QString &argumentName,
                             const QString &displayName,
                             const QString &description,
                             const QString &defaultBehavior,
                             int sortPriority = 0);

    ComponentOptionDoubleSet(const ComponentOptionDoubleSet &other);

    QSet<double> get() const;

    void clear();

    void set(const QSet<double> &s);

    void add(double v);

    virtual void reset();

    virtual ComponentOptionBase *clone() const;

    void setMaximum(double m, bool inclusive = true);

    void setMinimum(double m, bool inclusive = true);

    double getMaximum() const;

    double getMinimum() const;

    bool getMaximumInclusive() const;

    bool getMinimumInclusive() const;

    bool isValid(double check) const;
};

class CPD3CORE_EXPORT ComponentOptionDoubleList : public ComponentOptionBase {
Q_OBJECT

private:
    QList<double> values;
    int minimumComponents;
public:
    ComponentOptionDoubleList(const QString &argumentName,
                              const QString &displayName,
                              const QString &description,
                              const QString &defaultBehavior,
                              int sortPriority = 0);

    ComponentOptionDoubleList(const ComponentOptionDoubleList &other);

    QList<double> get() const;

    void clear();

    void set(const QList<double> &s);

    void append(double v);

    virtual void reset();

    virtual ComponentOptionBase *clone() const;

    void setMinimumComponents(int n);

    int getMinimumComponents() const;
};

class CPD3CORE_EXPORT ComponentOptionStringSet : public ComponentOptionBase {
Q_OBJECT

private:
    QSet<QString> values;
public:
    ComponentOptionStringSet(const QString &argumentName,
                             const QString &displayName,
                             const QString &description,
                             const QString &defaultBehavior,
                             int sortPriority = 0);

    ComponentOptionStringSet(const ComponentOptionStringSet &other);

    QSet<QString> get() const;

    void clear();

    void set(const QSet<QString> &s);

    void add(const QString &s);

    virtual void reset();

    virtual ComponentOptionBase *clone() const;
};

class CPD3CORE_EXPORT ComponentOptionInstrumentSuffixSet : public ComponentOptionStringSet {
Q_OBJECT
public:
    ComponentOptionInstrumentSuffixSet(const QString &argumentName,
                                       const QString &displayName,
                                       const QString &description,
                                       const QString &defaultBehavior,
                                       int sortPriority = 0);

    ComponentOptionInstrumentSuffixSet(const ComponentOptionInstrumentSuffixSet &other);

    virtual ComponentOptionBase *clone() const;
};

class CPD3CORE_EXPORT ComponentOptionEnum : public ComponentOptionBase {
Q_OBJECT

public:

    class CPD3CORE_EXPORT EnumValue {
    private:
        int id;
        QString internalName;
        QString name;
        QString description;
    public:
        EnumValue();

        EnumValue(int setID,
                  const QString &internalName,
                  const QString &setName,
                  const QString &setDescription);

        EnumValue(const EnumValue &other);

        EnumValue &operator=(const EnumValue &other);

        inline int getID() const
        { return id; }

        inline QString getName() const
        { return name; }

        inline QString getDescription() const
        { return description; }

        inline QString getInternalName() const
        { return internalName; }

        friend class ComponentOptionEnum;
    };

private:
    QList<EnumValue> values;
    QHash<QString, EnumValue> aliases;
    EnumValue selected;
public:
    ComponentOptionEnum(const QString &argumentName,
                        const QString &displayName,
                        const QString &description,
                        const QString &defaultBehavior,
                        int sortPriority = 0);

    ComponentOptionEnum(const ComponentOptionEnum &other);

    EnumValue get() const;

    void set(const EnumValue &v);

    void set(const QString &internalName);

    QList<EnumValue> getAll() const;

    QHash<QString, EnumValue> getAliases() const;

    void add(int id, const QString &internalName, const QString &name, const QString &description);

    void alias(int id, const QString &name);

    virtual void reset();

    virtual ComponentOptionBase *clone() const;
};

class CPD3CORE_EXPORT ComponentOptionBoolean : public ComponentOptionBase {
Q_OBJECT

    bool enabled;
public:
    ComponentOptionBoolean(const QString &argumentName,
                           const QString &displayName,
                           const QString &description,
                           const QString &defaultBehavior,
                           int sortPriority = 0);

    ComponentOptionBoolean(const ComponentOptionBoolean &other);

    void set(bool value);

    bool get() const;

    virtual void reset();

    virtual ComponentOptionBase *clone() const;
};

class CPD3CORE_EXPORT ComponentOptionTimeOffset : public ComponentOptionBase {
Q_OBJECT

    Time::LogicalTimeUnit unit;
    int count;
    bool align;

    Time::LogicalTimeUnit defaultUnit;
    int defaultCount;
    bool defaultAlign;

    bool allowZero;
    bool allowNegative;
public:
    ComponentOptionTimeOffset(const QString &argumentName,
                              const QString &displayName,
                              const QString &description,
                              const QString &defaultBehavior,
                              int sortPriority = 0);

    ComponentOptionTimeOffset(const ComponentOptionTimeOffset &other);

    void set(Time::LogicalTimeUnit unit, int count, bool align);

    void setDefault(Time::LogicalTimeUnit unit, int count, bool align);

    Time::LogicalTimeUnit getUnit() const;

    int getCount() const;

    bool getAlign() const;

    Time::LogicalTimeUnit getDefaultUnit() const;

    int getDefaultCount() const;

    bool getDefaultAlign() const;

    virtual void reset();

    virtual ComponentOptionBase *clone() const;

    void setAllowZero(bool allow);

    void setAllowNegative(bool allow);

    virtual bool getAllowZero() const;

    virtual bool getAllowNegative() const;
};

class CPD3CORE_EXPORT ComponentOptionTimeBlock : public ComponentOptionTimeOffset {
Q_OBJECT
public:
    ComponentOptionTimeBlock(const QString &argumentName,
                             const QString &displayName,
                             const QString &description,
                             const QString &defaultBehavior,
                             int sortPriority = 0);

    ComponentOptionTimeBlock(const ComponentOptionTimeBlock &other);

    virtual ComponentOptionBase *clone() const;

    virtual bool getAllowZero() const;

    virtual bool getAllowNegative() const;
};

class CPD3CORE_EXPORT ComponentOptionSingleTime : public ComponentOptionBase {
Q_OBJECT

    double value;
    bool allowUndefined;
public:
    ComponentOptionSingleTime(const QString &argumentName,
                              const QString &displayName,
                              const QString &description,
                              const QString &defaultBehavior,
                              int sortPriority = 0);

    ComponentOptionSingleTime(const ComponentOptionSingleTime &other);

    double get() const;

    void set(double v);

    virtual void reset();

    virtual ComponentOptionBase *clone() const;

    void setAllowUndefined(bool a);

    bool getAllowUndefined() const;
};

class CPD3CORE_EXPORT ComponentOptionSingleCalibration : public ComponentOptionBase {
Q_OBJECT

    Calibration cal;
    bool allowInvalid;
    bool allowConstant;
public:
    ComponentOptionSingleCalibration(const QString &argumentName,
                                     const QString &displayName,
                                     const QString &description,
                                     const QString &defaultBehavior,
                                     int sortPriority = 0);

    ComponentOptionSingleCalibration(const ComponentOptionSingleCalibration &other);

    void set(const Calibration &s);

    Calibration get() const;

    virtual void reset();

    virtual ComponentOptionBase *clone() const;

    void setAllowInvalid(bool allow);

    void setAllowConstant(bool allow);

    bool getAllowInvalid() const;

    bool getAllowConstant() const;

    bool isValid(const Calibration &cal);
};

/**
 * A container for a list of options that can be set to control a component.
 * This class is intended to be configured with actual values for the options
 * either by processing command line arguments or through the GUI interface.
 */
class CPD3CORE_EXPORT ComponentOptions {
private:
    class ComponentOptionsData : public QSharedData {
    public:
        ComponentOptionsData();

        ComponentOptionsData(const ComponentOptionsData &other);

        ~ComponentOptionsData();

        QHash<QString, ComponentOptionBase *> options;
        QHash<QString, QSet<QString> > exclude;
    };

    QSharedDataPointer<ComponentOptionsData> d;

public:
    ComponentOptions();

    ComponentOptions(const ComponentOptions &other);

    ComponentOptions &operator=(const ComponentOptions &other);

    void add(const QString &id, ComponentOptionBase *option);

    void exclude(const QString &id, const QString &exclude);

    bool isSet(const QString &id) const;

    ComponentOptionBase *get(const QString &id) const;

    QHash<QString, QSet<QString> > excluded() const;

    QHash<QString, ComponentOptionBase *> getAll() const;
};

/**
 * A container the describes an example usage of a component.  The usage
 * consists of a set of options and a description of what the usage does.
 */
class CPD3CORE_EXPORT ComponentExample {
    ComponentOptions options;
    QString title;
    QString description;

    int inputTypes;
    QStringList inputAuxiliary;
    Time::Bounds inputRange;
    QString inputStation;
    QString inputArchive;
public:
    enum {
        Input_Scattering = 0x01,
        Input_Absorption = 0x02,
        Input_Counts = 0x04,
        Input_FileOnly = 0x08,
    };

    ComponentExample();

    ComponentExample(const ComponentExample &other);

    ComponentExample &operator=(const ComponentExample &other);

    ComponentExample(const ComponentOptions &setOptions,
                     const QString &title,
                     const QString &description = QString());

    QString getTitle() const;

    QString getDescription() const;

    ComponentOptions getOptions() const;

    int getInputTypes() const;

    QStringList getInputAuxiliary() const;

    Time::Bounds getInputRange() const;

    QString getInputStation() const;

    QString getInputArchive() const;

    void setTitle(const QString &set);

    void setDescription(const QString &set);

    void setOptions(const ComponentOptions &set);

    void setInputTypes(int types);

    void setInputAuxiliary(const QStringList &aux);

    void setInputRange(const Time::Bounds &range);

    void setInputStation(const QString &station);

    void setInputArchive(const QString &archive);
};

/**
 * Provides routines to load component plugins.
 */
class CPD3CORE_EXPORT ComponentLoader {
public:
    static QObject *create(const QString &name);

    class CPD3CORE_EXPORT Information final {
        QString loadedComponentName;
        std::unordered_set<std::string> supportedInterfaces;
        QString loadedTitle;
        QString loadedDescription;
        int loadedSortPriority;
        bool disableList;
        std::unordered_set<std::string> listExclude;

        Information(const QPluginLoader &loader);

        Information(const QStaticPlugin &loader);

        Information(const QJsonObject &metadata);

        friend class ComponentLoader;

    public:
        Information();

        Information(const Information &);

        Information &operator=(const Information &);

        Information(Information &&);

        Information &operator=(Information &&);

        inline bool isValid() const
        { return !loadedComponentName.isEmpty(); }

        inline const QString &componentName() const
        { return loadedComponentName; }

        inline const std::unordered_set<std::string> &interfaces() const
        { return supportedInterfaces; }

        inline bool interface(const std::string &type) const
        { return interfaces().count(type) != 0; }

        inline const QString &title() const
        { return loadedTitle; }

        inline const QString &description() const
        { return loadedDescription; }

        inline int sortPriority() const
        { return loadedSortPriority; }

        inline bool excludeFromList() const
        { return disableList; }

        inline bool excludeFromList(const std::string &type) const
        { return disableList || listExclude.count(type) != 0; }

        struct CPD3CORE_EXPORT DisplayOrder {
            bool operator()(const Information &a, const Information &b) const;
        };
    };

    static std::pair<Information, QObject *> information(const QString &name);

    static std::unordered_map<QString, Information> list();

    static std::unordered_map<QString, Information> list(const std::string &type);

private:
    static void listRecursive(const QDir &dir,
                              std::unordered_map<QString, Information> &result,
                              std::unordered_set<QString> &inspectedFiles,
                              int depth = 1);
};

}

#endif
