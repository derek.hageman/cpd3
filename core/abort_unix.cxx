/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <signal.h>

#include <QTimer>
#include <QLoggingCategory>

#include "core/abort.hxx"


Q_LOGGING_CATEGORY(log_core_abort, "cpd3.core.abort", QtWarningMsg)

namespace CPD3 {

static QAtomicInt abortedSignal;
static QAtomicInt handlerInstalled;
static sigset_t maskAllSignals;

static void abort_signal_handler(int sig)
{
    sigset_t oldmask;
    sigprocmask(SIG_SETMASK, &maskAllSignals, &oldmask);
    abortedSignal.ref();
    sigprocmask(SIG_SETMASK, &oldmask, NULL);
    Q_UNUSED(sig);
}

void Abort::installAbortHandler(bool abortOnHangup)
{
    if (handlerInstalled.testAndSetAcquire(0, 1))
        return;

    memset(&maskAllSignals, 0, sizeof(maskAllSignals));
    sigfillset(&maskAllSignals);

    struct sigaction sa;
    sigset_t ss;

    memset(&ss, 0, sizeof(ss));
    sigfillset(&ss);
    /* Ignore all signals until we're done setting up */
    sigprocmask(SIG_SETMASK, &ss, NULL);

    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = abort_signal_handler;
    sa.sa_mask = ss;
    if (sigaction(SIGINT, &sa, NULL) != 0) {
        qCCritical(log_core_abort) << "Can't install SIGINT handler";
    }
    if (sigaction(SIGTERM, &sa, NULL) != 0) {
        qCCritical(log_core_abort) << "Can't install SIGTERM handler";
    }
    if (abortOnHangup && sigaction(SIGHUP, &sa, NULL) != 0) {
        qCCritical(log_core_abort) << "Can't install SIGHUP handler";
    }

    /* Ignore this because a disconnected unix socket generates it, and 
       we handle it elsewhere */
    sa.sa_handler = SIG_IGN;
    if (sigaction(SIGPIPE, &sa, NULL) != 0) {
        qCCritical(log_core_abort) << "Can't ignore SIGPIPE";
    }

    /* Now that we're done, un-block the signals */
    sigdelset(&ss, SIGINT);
    sigdelset(&ss, SIGTERM);

    /* Pass this through because Qt's QProcess needs it for proper reaping */
    sigdelset(&ss, SIGCHLD);

    if (abortOnHangup) sigdelset(&ss, SIGHUP);
    sigprocmask(SIG_SETMASK, &ss, NULL);
}

bool Abort::aborted()
{
#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
    return abortedSignal.load() != 0;
#else
    return abortedSignal.loadRelaxed() != 0;
#endif
}

AbortPoller::AbortPoller(QObject *parent) : QObject(parent)
{
    Abort::installAbortHandler();

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(checkAborted()));
}

AbortPoller::~AbortPoller()
{
    timer->stop();
    delete timer;
}

void AbortPoller::start()
{
    timer->start(250);
}

void AbortPoller::stop()
{
    timer->stop();
}

void AbortPoller::checkAborted()
{
    if (!Abort::aborted())
        return;
    timer->stop();
    emit aborted();
}

};
