/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#if defined(Q_OS_WIN32)
#include <windows.h>
#include <lmcons.h>
#include <tchar.h>
#else

#endif

#if defined(Q_OS_UNIX)

#include <unistd.h>

#endif

#if defined(Q_OS_LINUX)

#include <sys/sysinfo.h>

#endif

#include <QCoreApplication>
#include <QStringList>
#include <QHostInfo>
#include <QFile>

#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)

#include <QSysInfo>

#endif

#include "core/environment.hxx"

#include "version.h"

namespace CPD3 {


/** @file core/environment.hxx
 * Provides simple routines to query information about the environment.
 */

/**
 * Returns a short description of the calling environment.
 * 
 * @return a description string
 */
QString Environment::describe()
{
    QString user(Environment::user());
    QString applicationName(QCoreApplication::applicationName());
    QString arguments(QCoreApplication::arguments().join(" "));
    QString hostname(QHostInfo::localHostName());

    if (!applicationName.isEmpty()) {
        if (!arguments.isEmpty()) {
            return tr("%5 (%1) run by %2 on %3 via %4",
                      "full specification").arg(QCoreApplication::applicationPid())
                                           .arg(user, hostname, arguments, applicationName);
        } else {
            return tr("%4 (%1) run by %2 on %3",
                      "version with no arguments").arg(QCoreApplication::applicationPid())
                                                  .arg(user, hostname, applicationName);
        }
    } else {
        if (!arguments.isEmpty()) {
            return tr("PID %1 run by %2 on %3 via %4", "no program name").arg(
                    QCoreApplication::applicationPid()).arg(user, hostname, arguments);
        } else {
            return tr("PID %1 run by %2 on %3",
                      "no program and no arguments").arg(QCoreApplication::applicationPid())
                                                    .arg(user, hostname);
        }
    }
}

/**
 * Get the total system memory in bytes.
 * 
 * @return the total system memory in bytes or 0 on error
 */
quint64 Environment::totalMemory()
{
#if defined(Q_OS_WIN32)
    MEMORYSTATUSEX status;
    ZeroMemory(&status, sizeof(MEMORYSTATUSEX));
    status.dwLength = sizeof(MEMORYSTATUSEX);
    if (!GlobalMemoryStatusEx(&status))
        return 0;
    return (quint64)status.ullTotalPhys;
#elif defined(Q_OS_LINUX)
    struct sysinfo info;
    if (sysinfo(&info))
        return 0;
    return (quint64) info.totalram * (quint64) info.mem_unit;
#elif defined(Q_OS_UNIX) && defined(_SC_PHYS_PAGES) && defined(_SC_PAGESIZE)
    int pages = sysconf(_SC_PHYS_PAGES);
    if (pages == -1)
        return 0;
    int pageSize = sysconf(_SC_PAGESIZE);
    if (pageSize == -1)
        return 0;
    return (quint64)pages * (quint64)pageSize;
#else
    return 0;
#endif
}

/**
 * Returns a minimal description of the calling environment.  This is
 * intended primarily for debugging.
 * 
 * @return a description string
 */
QString Environment::minimal()
{
    QString user(Environment::user());

    QString result;
    result.append(QString::number(QCoreApplication::applicationPid()));
    result.append(':');
    result.append(QCoreApplication::applicationName());
    result.append(':');
    result.append(QCoreApplication::applicationVersion());
    result.append(':');
    result.append(user);
    result.append(':');
    result.append(QHostInfo::localHostName());
    result.append(':');
    result.append(QCoreApplication::arguments().join("#"));

    return result;
}

/**
 * Get a string representing the current user.  Generally this is the
 * symbolic user name.
 * 
 * @return the user
 */
QString Environment::user()
{
    QString user;

#if defined(Q_OS_WIN32)
# if defined(UNICODE)
    if (QSysInfo::windowsVersion() & QSysInfo::WV_NT_based) {
        TCHAR winUserName[UNLEN + 1];
        DWORD winUserNameSize = sizeof(winUserName);
        GetUserName( winUserName, &winUserNameSize );
        for (; winUserNameSize>0 && winUserName[winUserNameSize-1] == 0; --winUserNameSize) { }
        user = QString::fromWCharArray( winUserName, winUserNameSize );
    } else
# endif
    {
        char winUserName[UNLEN + 1];
        DWORD winUserNameSize = sizeof(winUserName);
        GetUserNameA( winUserName, &winUserNameSize );
        user = QString::fromLocal8Bit( winUserName );
    }
#else
    char *check = getenv("LOGNAME");
    if (check == NULL) {
        check = getenv("USER");
    }

    if (check != NULL && *check != '\0') {
        user = QString::fromUtf8(check);
    } else {
        user = QString::number((quint64) getuid());
    }
#endif

    return user;
}

/**
 * Return a description of the current revision of the code.
 *
 * @return a description string
 */
QString Environment::revision()
{
    QString check(CPD3_VERSION_NAME);
    check = check.section('-', 0, 0);
    if (check.startsWith('v'))
        check = check.mid(1);
    bool ok = false;
    if (check.toInt(&ok) == CPD3_VERSION_NUMBER && ok)
        return tr("%1", "revision").arg(CPD3_VERSION_NAME);
    return tr("%2 (%1)", "revision description").arg(CPD3_VERSION_NUMBER).arg(CPD3_VERSION_NAME);
}

QString Environment::architecture()
{
    return tr("%2-%1MiB", "architecture description").arg(totalMemory() / (1024 * 1024))
                                                     .arg(QSysInfo::buildAbi());
}

};
