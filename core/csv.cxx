/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>
#include <QStringList>

#include "core/csv.hxx"

namespace CPD3 {
namespace CSV {


QString join(const QStringList &fields,
             const QString &separator,
             const QString &quote,
             const QString &quoteEscape)
{
    QStringList working(fields);
    if (quote.length() > 0) {
        for (int i = 0, max = fields.size(); i < max; i++) {
            if (working.at(i).contains(separator) || working.at(i).contains(quote)) {
                QString f(working.at(i));
                f.replace(quote, quoteEscape);
                f.prepend(quote);
                f.append(quote);
                working[i] = f;
            }
        }
    } else if (separator.length() > 0) {
        for (int i = 0, max = fields.size(); i < max; i++) {
            if (working.at(i).contains(separator)) {
                working[i].remove(separator);
            }
        }
    }
    return working.join(separator);
}

QStringList split(const QString &data,
                  const QString &separator,
                  const QString &quote,
                  const QString &quoteEscape)
{
    QStringList fields;

    int length = data.length();
    int lengthC = separator.length();
    int lengthQ = quote.length();
    int lengthE = quoteEscape.length();
    int cIdx = data.indexOf(separator);
    int qIdx = data.indexOf(quote);
    int start = 0;
    for (;;) {
        if (cIdx == -1) {
            if (start < length) {
                if (qIdx == start) {
                    /* Handle escapes as below */
                    cIdx = length;
                } else {
                    fields.append(data.mid(start));
                    break;
                }
            } else {
                fields.append(QString());
                break;
            }
        }
        if (start >= length)
            break;

        /* No quotes or only quotes after the comma */
        if (qIdx == -1 || qIdx >= cIdx) {
            if (cIdx == start) {
                fields.append(QString());
            } else {
                fields.append(data.mid(start, cIdx - start));
            }
            start = cIdx + lengthC;
            cIdx = data.indexOf(separator, start);
            continue;
        }
        /* A quote before the comma */

        /* Discard anything before the first quote (leading whitespace, 
         * usually */
        int eIdx = qIdx + lengthQ;
        qIdx = data.indexOf(quote, eIdx);

        /* No end quote, so just assume it's degenerate and add the string */
        if (qIdx == -1) {
            if (cIdx == start) {
                fields.append(QString());
            } else {
                fields.append(data.mid(start, cIdx - start));
            }
            start = cIdx + lengthC;
            cIdx = data.indexOf(separator, start);
            continue;
        }

        /* Advance after the check for degeneracy */
        start = eIdx;
        eIdx = data.indexOf(quoteEscape, start);

        /* No escapes */
        if (eIdx == -1 || eIdx > qIdx) {
            if (qIdx == start) {
                fields.append(QString());
            } else {
                fields.append(data.mid(start, qIdx - start));
            }
            start = qIdx + lengthQ;
            start = data.indexOf(separator, start);
            if (start == -1)
                break;
            start += lengthC;
            cIdx = data.indexOf(separator, start);
            qIdx = data.indexOf(quote, start);
            continue;
        }

        QString working;
        for (;;) {
            if (eIdx != start)
                working.append(data.mid(start, eIdx - start));
            working.append(quote);
            start = eIdx + lengthE;

            eIdx = data.indexOf(quoteEscape, start);
            //cIdx = data.indexOf(separator, start);
            qIdx = data.indexOf(quote, start);
            if (eIdx == -1 || qIdx == -1 || eIdx > qIdx)
                break;
        }

        /* No end quote after escapes so it's degenerate, so end the string */
        if (qIdx == -1) {
            fields.append(working);
            break;
        }

        if (qIdx != start)
            working.append(data.mid(start, qIdx - start));
        fields.append(working);

        start = qIdx + lengthQ;
        start = data.indexOf(separator, start);
        if (start == -1)
            break;
        start += lengthC;
        cIdx = data.indexOf(separator, start);
        qIdx = data.indexOf(quote, start);
    }

    return fields;
}

static Util::ByteView::size_type indexOfNext(const Util::ByteView &data,
                                             Util::ByteView::size_type offset,
                                             char ch,
                                             Util::ByteView::size_type limit = Util::ByteView::npos)
{
    Util::ByteView::size_type pos;
    if (limit == data.npos) {
        pos = data.mid(offset).indexOf(ch);
    } else {
        pos = data.mid(offset, limit - offset).indexOf(ch);
    }
    if (pos != data.npos)
        return pos + offset;
    return limit;
}

static bool isSpaceDelimiter(char c)
{
    return (c == ' ' || c == '\t' || c == '\r' || c == '\n' || c == '\v');
}

std::deque<Util::ByteView> acquisitionSplit(const Util::ByteView &input)
{
    std::deque<Util::ByteView> result;

    Util::ByteView::size_type start = 0;
    Util::ByteView::size_type max = input.size();
    while (start < max && isSpaceDelimiter(input[start])) {
        ++start;
    }
    if (start >= max) {
        result.emplace_back();
        return result;
    }

    while (start < max) {
        auto next = indexOfNext(input, start, ',');
        next = indexOfNext(input, start, ' ', next);
        next = indexOfNext(input, start, '\t', next);
        next = indexOfNext(input, start, '\r', next);
        next = indexOfNext(input, start, '\n', next);
        next = indexOfNext(input, start, '\v', next);

        if (next == Util::ByteView::npos) {
            result.emplace_back(input.mid(start));
            break;
        }

        char ch = input[next];
        if (start != 0 || start != next || ch == ',') {
            result.emplace_back(input.mid(start, next - start));
        }

        while (isSpaceDelimiter(ch)) {
            ++next;
            if (next >= max)
                break;
            ch = input[next];
        }

        if (ch == ',') {
            ++next;
            if (next >= max) {
                result.emplace_back();
                return result;
            }
            ch = input[next];

            while (isSpaceDelimiter(ch)) {
                ++next;
                if (next >= max)
                    break;
                ch = input[next];
            }

            if (next >= max) {
                result.emplace_back();
                return result;
            }
        }

        Q_ASSERT(start != next);
        start = next;
    }
    return result;
}

}
}
