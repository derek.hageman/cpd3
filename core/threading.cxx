/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <condition_variable>
#include <list>
#include <QGlobalStatic>
#include <QThread>
#include <QCoreApplication>

#include "threading.hxx"

namespace CPD3 {
namespace Threading {

namespace {

class DetachedQThreads {
    std::mutex mutex;
    std::condition_variable notify;

    static DetachedQThreads *container();

    class DetachedThread : public QThread {
        enum {
            Waiting, Running, Complete, Terminating
        } state;
        std::function<void()> call;
    public:
        DetachedThread() : state(Waiting)
        { }

        virtual ~DetachedThread() = default;

        bool invoke(std::function<void()> &&c)
        {
            if (state != Waiting)
                return false;
            call = std::move(c);
            state = Running;
            return true;
        }

        bool reap()
        {
            if (state != Complete)
                return false;
            wait();
            return true;
        }

        void requestTerminate()
        {
            if (state == Complete)
                return;
            state = Terminating;
        }

    protected:
        virtual void run()
        {
            setTerminationEnabled(true);
            for (;;) {
                std::function<void()> c;

                {
                    auto l = container();
                    if (!l)
                        return;
                    std::unique_lock<std::mutex> lock(l->mutex);
                    switch (state) {
                    case Waiting:
                        l->notify.wait_for(lock, std::chrono::minutes(10), [this] {
                            return state != Waiting;
                        });
                        if (state == Waiting) {
                            state = Complete;
                            l->notify.notify_all();
                            return;
                        }
                        continue;
                    case Running:
                        if (!call) {
                            state = Waiting;
                            l->notify.notify_all();
                            continue;
                        }
                        c = std::move(call);
                        call = std::function<void()>();
                        break;
                    case Complete:
                        return;
                    case Terminating:
                        state = Complete;
                        l->notify.notify_all();
                        return;
                    }
                }

                Q_ASSERT(c);
                c();
            }
        }
    };

    friend class DetachedThread;

    std::list<std::unique_ptr<DetachedThread>> threads;

    void reap()
    {
        for (auto r = threads.begin(); r != threads.end();) {
            if (!(*r)->reap()) {
                ++r;
                continue;
            }
            r = threads.erase(r);
        }
    }

public:
    DetachedQThreads() = default;

    ~DetachedQThreads()
    {
        std::unique_lock<std::mutex> lock(mutex);
        for (auto &t : threads) {
            t->requestTerminate();
        }
        notify.notify_all();
        notify.wait_for(lock, std::chrono::seconds(5), [this] {
            reap();
            return threads.empty();
        });
        auto remaining = std::move(threads);
        threads.clear();
        lock.unlock();

        /* By now it's going to leak regardless, so just terminate it and hope for the
         * best */
        for (auto &t : remaining) {
            t->terminate();
            if (t->wait(5000)) {
                t.reset();
            } else {
                /* Didn't even finish, so just drop the pointer */
                t.release();
            }
        }
    }

    void launch(std::function<void()> call)
    {
        {
            std::unique_lock<std::mutex> lock(mutex);
            reap();
            for (auto &t : threads) {
                if (t->invoke(std::move(call))) {
                    lock.unlock();
                    notify.notify_all();
                    return;
                }
            }
        }

        std::unique_ptr<DetachedThread> thread(new DetachedThread());
        thread->setObjectName("DetatchedQThread");
        thread->invoke(std::move(call));

        {
            std::lock_guard<std::mutex> lock(mutex);
            thread->start();
            threads.emplace_back(std::move(thread));
        }
        notify.notify_all();
    }
};

Q_GLOBAL_STATIC(DetachedQThreads, detachedQThreads)

DetachedQThreads *DetachedQThreads::container()
{ return detachedQThreads(); }

}

void detachQThread(const std::function<void()> &call)
{
    auto l = detachedQThreads();
    if (!l)
        return;
    l->launch(call);
}

void detachQThread(std::function<void()> &&call)
{
    auto l = detachedQThreads();
    if (!l)
        return;
    l->launch(std::move(call));
}


Internal::Sender::Sender() = default;

Internal::Sender::~Sender() = default;

Connection::Connection() = default;

Connection::Connection(const Connection &) = default;

Connection &Connection::operator=(const Connection &) = default;

Connection::Connection(Connection &&other) = default;

Connection &Connection::operator=(Connection &&other) = default;

void Connection::disconnect()
{
    if (auto ref = sender.lock()) {
        ref->release(id);
    }
    sender.reset();
    id = 0;
}

void Connection::disconnectImmediate()
{
    if (auto ref = sender.lock()) {
        ref->releaseImmediate(id);
    }
    sender.reset();
    id = 0;
}

bool Connection::valid() const
{
    if (id == 0)
        return false;
    return !sender.expired();
}

void Receiver::disconnect()
{
    std::list<Connection> dc;
    {
        std::lock_guard<std::mutex> lock(mutex);
        dc = std::move(connections);
        connections.clear();
    }
    for (auto &c : dc) {
        c.disconnect();
    }
}

void Receiver::disconnectImmediate()
{
    std::list<Connection> dc;
    {
        std::lock_guard<std::mutex> lock(mutex);
        dc = std::move(connections);
        connections.clear();
    }
    for (auto &c : dc) {
        c.disconnectImmediate();
    }
}

void Receiver::purgeInvalid()
{
    for (auto c = connections.begin(); c != connections.end();) {
        if (c->valid()) {
            ++c;
            continue;
        }
        c = connections.erase(c);
    }
}


Receiver::Receiver() = default;

Receiver::~Receiver()
{ disconnect(); }

Internal::ReceiverContextQObject::ReceiverContextQObject(const Connection &c, QObject *parent)
        : QObject(parent), connection(c)
{ }

Internal::ReceiverContextQObject::~ReceiverContextQObject()
{
    connection.disconnect();
}

DeferredReceiver::DeferredReceiver() = default;

DeferredReceiver::~DeferredReceiver()
{
    Receiver::disconnect();
    call();
}

bool DeferredReceiver::call()
{
    std::vector<Call> pending;
    {
        std::lock_guard<std::mutex> lock(mutex);
        pending = std::move(calls);
        calls.clear();
    }
    for (const auto &c : pending) {
        c();
    }
    return !pending.empty();
}

void DeferredReceiver::ignore()
{
    std::lock_guard<std::mutex> lock(mutex);
    calls.clear();
}


PoolRun::PoolRun() : PoolRun(std::thread::hardware_concurrency())
{ }

PoolRun::PoolRun(std::size_t threadCount) : shared(std::make_shared<Shared>()),
                                            maximumThreads(threadCount)
{
    if (!maximumThreads) {
        /* Unlimited threads always launch, so just have them exit when there's no work */
        shared->threadShutdown = 1;
    }
}

PoolRun::~PoolRun()
{
    {
        std::lock_guard<std::mutex> lock(shared->mutex);
        shared->threadShutdown++;
    }
    shared->cv.notify_all();
}

void PoolRun::wait()
{
    {
        std::unique_lock<std::mutex> lock(shared->mutex);
        shared->threadShutdown++;
    }
    shared->cv.notify_all();

    std::unique_lock<std::mutex> lock(shared->mutex);
    shared->cv.wait(lock, [this] { return shared->runningThreads == 0; });
    shared->threadShutdown--;
}

void PoolRun::run(const std::function<void()> &task)
{
    {
        std::lock_guard<std::mutex> lock(shared->mutex);
        shared->tasks.emplace(task);
        if (maybeLaunch())
            return;
    }
}

void PoolRun::run(std::function<void()> &&task)
{
    {
        std::lock_guard<std::mutex> lock(shared->mutex);
        shared->tasks.emplace(std::move(task));
        if (maybeLaunch())
            return;
    }
    shared->cv.notify_all();
}

bool PoolRun::maybeLaunch()
{
    if (maximumThreads > 0 && shared->runningThreads >= maximumThreads)
        return false;
    shared->runningThreads++;
    std::thread(std::bind(&PoolRun::process, shared)).detach();
    return true;
}

void PoolRun::process(const std::shared_ptr<PoolRun::Shared> &shared)
{
    for (;;) {
        std::function<void()> task;
        {
            std::unique_lock<std::mutex> lock(shared->mutex);
            for (;;) {
                if (!shared->tasks.empty())
                    break;
                if (shared->threadShutdown) {
                    shared->runningThreads--;
                    if (!shared->runningThreads)
                        shared->cv.notify_all();
                    return;
                }
                if (!shared->cv.wait_for(lock, std::chrono::milliseconds(500), [&shared] {
                    return !shared->tasks.empty() || shared->threadShutdown != 0;
                })) {
                    shared->runningThreads--;
                    if (!shared->runningThreads)
                        shared->cv.notify_all();
                    return;
                }
            }
            task = std::move(shared->tasks.front());
            shared->tasks.pop();
        }
        task();
    }
}

PoolRun::Shared::Shared() : runningThreads(0), threadShutdown(0)
{ }

}
}