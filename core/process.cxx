/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QFileInfo>

#include "core/process.hxx"

namespace CPD3 {


/** @file core/process.hxx
 * Provides routines for working with external processes.
 */

void Process::forwardError(QProcess *process)
{
    process->setProcessChannelMode(QProcess::ForwardedErrorChannel);
    process->setReadChannel(QProcess::StandardOutput);
}

#ifdef Q_OS_UNIX

namespace Internal {

ProcessErrorFoward::ProcessErrorFoward(QProcess *p) : QObject(p), process(p), file(), buffer()
{ }

bool ProcessErrorFoward::attach()
{
    if (!file.open(2, QIODevice::WriteOnly | QIODevice::Unbuffered))
        return false;

    connect(process, SIGNAL(readyReadStandardError()), this, SLOT(readError()));
    connect(&file, SIGNAL(bytesWritten(qint64)), this, SLOT(writeError()), Qt::QueuedConnection);

    QMetaObject::invokeMethod(this, "readError", Qt::QueuedConnection);

    return true;
}

void ProcessErrorFoward::readError()
{
    QByteArray data(process->readAllStandardError());
    if (data.isEmpty())
        return;

    if (buffer.isEmpty()) {
        buffer = data;
        writeError();
    } else {
        buffer.append(data);
    }
}

void ProcessErrorFoward::writeError()
{
    if (buffer.isEmpty())
        return;
    qint64 n = file.write(buffer);
    if (n == -1) {
        buffer.clear();
        deleteLater();
        return;
    }

    if ((int) n >= buffer.size()) {
        buffer.clear();
        return;
    }

    int remaining = buffer.size() - n;
    memmove(buffer.data(), buffer.constData() + n, remaining);
    buffer.resize(remaining);
}

}

#endif

}
