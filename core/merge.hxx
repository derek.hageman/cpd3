/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3_MERGE_HXX
#define CPD3_MERGE_HXX

#include "core/first.hxx"

#if CPD3_CXX >= 201700L && defined(HAVE_CXX_PARALLEL)
#include <execution>
#elif defined(__GLIBC__)

#include <parallel/algorithm>

#endif

#include <iterator>
#include <algorithm>
#include <utility>
#include <vector>
#include <list>
#include <deque>
#include <QList>
#include <QDataStream>
#include <QDebug>

#include "core.hxx"
#include "range.hxx"
#include "timeutils.hxx"
#include "qtcompat.hxx"
#include "util.hxx"

#ifndef NDEBUG
#define MERGE_TIME_DEBUGGING
#endif

namespace CPD3 {

/**
 * Routines for general purpose merging.
 */
class CPD3CORE_EXPORT Merge {

#ifdef MERGE_TIME_DEBUGGING

    template<typename Iterator>
    static void verifyIteratorSorted(Iterator begin, Iterator end)
    {
        if (begin == end)
            return;
        for (Iterator prior = begin++; begin != end; prior = begin, ++begin) {
            Q_ASSERT(Range::compareStart(prior->getStart(), begin->getStart()) <= 0);
        }
    }

#endif

    template<typename InputList, typename AdvanceList>
    static void mergeDefined(InputList &working, InputList &copySet, AdvanceList &advanceSet)
    {
        for (typename InputList::iterator stream = working.begin(), endStream = working.end();
                stream != endStream;
                ++stream) {
            if (stream->first == stream->second)
                continue;
            double streamTime = stream->first->getStart();
            Q_ASSERT(FP::defined(streamTime));
            if (!copySet.empty()) {
                double bound = copySet.front().first->getStart();
                if (streamTime != bound) {
                    if (bound < streamTime) {
                        continue;
                    } else {
                        copySet.clear();
                        advanceSet.clear();
                    }
                }
            }

            auto endCopy =
                    Range::heuristicUpperBoundDefined(stream->first, stream->second, streamTime);

            Q_ASSERT(endCopy != stream->first);
#ifdef MERGE_TIME_DEBUGGING
            Q_ASSERT((endCopy - 1)->getStart() == streamTime);
#endif

            copySet.emplace_back(stream->first, endCopy);
            advanceSet.emplace_back(stream);
        }
    }

    template<typename InputList, typename AdvanceList>
    static void mergeUndefined(InputList &working, InputList &copySet, AdvanceList &advanceSet)
    {
        for (typename InputList::iterator stream = working.begin(), endStream = working.end();
                stream != endStream;
                ++stream) {
            if (stream->first == stream->second)
                continue;
            double streamTime = stream->first->getStart();
            if (FP::defined(streamTime))
                continue;

            auto endCopy = Range::lastUndefinedStart(stream->first, stream->second);

            Q_ASSERT(endCopy != stream->first);
#ifdef MERGE_TIME_DEBUGGING
            Q_ASSERT(!FP::defined((endCopy - 1)->getStart()));
#endif

            copySet.emplace_back(stream->first, endCopy);
            advanceSet.emplace_back(stream);
        }
    }

    template<bool haveUndefined, typename InputList, typename OutputType>
    static void nWayExecute(const InputList &inputs, OutputType &output)
    {
        InputList working(inputs);
        InputList copySet;
        std::vector<typename InputList::iterator> advanceSet;

        if (haveUndefined) {
            mergeUndefined(working, copySet, advanceSet);
            Q_ASSERT(copySet.size() == advanceSet.size());

            auto advance = advanceSet.begin();
            for (auto copy = copySet.begin(), endCopy = copySet.end();
                    copy != endCopy;
                    ++copy, ++advance) {
                std::copy(copy->first, copy->second, Util::back_emplacer(output));
                (*advance)->first = copy->second;
            }

            copySet.clear();
            advanceSet.clear();
        }

        for (;;) {
            mergeDefined(working, copySet, advanceSet);
            Q_ASSERT(copySet.size() == advanceSet.size());
            if (copySet.empty())
                break;

            auto advance = advanceSet.begin();
            for (auto copy = copySet.begin(), endCopy = copySet.end();
                    copy != endCopy;
                    ++copy, ++advance) {
                std::copy(copy->first, copy->second, Util::back_emplacer(output));
                (*advance)->first = copy->second;
            }

            copySet.clear();
            advanceSet.clear();
        }

#ifdef MERGE_TIME_DEBUGGING
        verifyIteratorSorted(output.begin(), output.end());
#endif
    }

    /*
     * Parallel merge requires a forward iterator (not just an output iterator), so allocate
     * elements and use a normal iterator, if available.
     */
    template<bool haveUndefined, typename ForwardIt1, typename ForwardIt2, typename OutputType>
    static inline auto dispatchOptimizedMerge(ForwardIt1 first1,
                                              ForwardIt1 last1,
                                              ForwardIt2 first2,
                                              ForwardIt2 last2,
                                              OutputType &output) -> typename std::enable_if<
            !std::is_default_constructible<typename std::iterator_traits<
                    typename OutputType::iterator>::value_type>::value>::type
    {
        typedef typename std::iterator_traits<typename OutputType::iterator>::value_type ValueType;
        if (haveUndefined) {
            optimizedMerge(first1, last1, first2, last2, Util::back_emplacer(output),
                           Range::startLessThan<ValueType>);
        } else {
            optimizedMerge(first1, last1, first2, last2, Util::back_emplacer(output),
                           Range::startLessThanDefined<ValueType>);
        }
    }

    template<bool haveUndefined, typename ForwardIt1, typename ForwardIt2, typename OutputType>
    static inline auto dispatchOptimizedMerge(ForwardIt1 first1,
                                              ForwardIt1 last1,
                                              ForwardIt2 first2,
                                              ForwardIt2 last2,
                                              OutputType &output) -> typename std::enable_if<
            std::is_default_constructible<typename std::iterator_traits<
                    typename OutputType::iterator>::value_type>::value>::type
    {
        typedef typename std::iterator_traits<typename OutputType::iterator>::value_type ValueType;

#if CPD3_CXX >= 201700L && defined(HAVE_CXX_PARALLEL)
        auto offset = output.size();
        output.resize(offset + std::distance(first1, last1) + std::distance(first2, last2));
        if (haveUndefined) {
            optimizedMerge(first1, last1, first2, last2, output.begin() + offset,
                           Range::startLessThan<ValueType>);
        } else {
            optimizedMerge(first1, last1, first2, last2, output.begin() + offset,
                           Range::startLessThanDefined<ValueType>);
        }
#else
        if (haveUndefined) {
            optimizedMerge(first1, last1, first2, last2, Util::back_emplacer(output),
                           Range::startLessThan<ValueType>);
        } else {
            optimizedMerge(first1, last1, first2, last2, Util::back_emplacer(output),
                           Range::startLessThanDefined<ValueType>);
        }
#endif
    }

    template<bool haveUndefined, typename ForwardIt1, typename ForwardIt2, typename ValueType>
    static inline void dispatchOptimizedMerge(ForwardIt1 first1,
                                              ForwardIt1 last1,
                                              ForwardIt2 first2,
                                              ForwardIt2 last2,
                                              QList<ValueType> &output)
    {
        if (haveUndefined) {
            std::merge(first1, last1, first2, last2, Util::back_emplacer(output),
                       Range::startLessThan<ValueType>);
        } else {
            std::merge(first1, last1, first2, last2, Util::back_emplacer(output),
                       Range::startLessThanDefined<ValueType>);
        }
    }

    template<bool haveUndefined, typename InputList, typename OutputType>
    static void nWayInternal(const InputList &inputs, OutputType &output)
    {
        typedef typename std::iterator_traits<typename OutputType::iterator>::value_type ValueType;

#ifdef MERGE_TIME_DEBUGGING
        for (const auto &add : inputs) {
            verifyIteratorSorted(add.first, add.second);
        }
#endif

        /* Sort circuit the trivial cases */
        switch (inputs.size()) {
        case 0:
            return;
        case 1:
            std::copy(inputs.front().first, inputs.front().second, Util::back_emplacer(output));
#ifdef MERGE_TIME_DEBUGGING
            verifyIteratorSorted(output.begin(), output.end());
#endif
            return;
        case 2: {
            auto a = inputs.begin(), b = a;
            ++b;
            dispatchOptimizedMerge<haveUndefined>(a->first, a->second, b->first, b->second, output);
#ifdef MERGE_TIME_DEBUGGING
            verifyIteratorSorted(output.begin(), output.end());
#endif
            return;
        }
        default:
            break;
        }

        /* Check to see if it's going to be faster to just sort everything, rather than
         * trying a full merge */
        {
            std::size_t m = inputs.size();
            std::size_t n = 0;
            for (const auto &add : inputs) {
                n += std::distance(add.first, add.second);
            }
            if (n == 0)
                return;
            /* Approximate the merge as n * m, and the sort as n * log2(n), so:
             * n * m > n * log2(n) -> m > log2(n) */
            if (m > static_cast<std::size_t>(INTEGER::floorLog2(n))) {
                std::size_t offset = output.size();
                auto outputWriter = Util::back_emplacer(output);
                for (const auto &add : inputs) {
                    std::copy(add.first, add.second, outputWriter);
                }
                if (haveUndefined) {
                    Merge::optimizedSort(output.begin() + offset, output.end(),
                                         Range::startLessThan<ValueType>);
                } else {
                    Merge::optimizedSort(output.begin() + offset, output.end(),
                                         Range::startLessThanDefined<ValueType>);
                }
#ifdef MERGE_TIME_DEBUGGING
                verifyIteratorSorted(output.begin(), output.end());
#endif
                return;
            }
        }

        nWayExecute<haveUndefined>(inputs, output);
    }

public:

    /**
     * A wrapper around std::sort that uses an optimized (parallel, if avaiable) implementation.
     */
    template<typename Iterator, typename Compare>
    static inline auto optimizedSort(Iterator begin,
                                     Iterator end,
                                     Compare comp) -> typename std::enable_if<
            !std::is_copy_constructible<typename Iterator::value_type>::value>::type
    {
#if CPD3_CXX >= 201700L && defined(HAVE_CXX_PARALLEL)
        std::sort(std::execution::par_unseq, begin, end, comp);
#else
        std::sort(begin, end, comp);
#endif
    }

    template<typename Iterator, typename Compare>
    static inline auto optimizedSort(Iterator begin,
                                     Iterator end,
                                     Compare comp) -> typename std::enable_if<
            std::is_copy_constructible<typename Iterator::value_type>::value>::type
    {
        /* Required because glibc's parallel implementation doesn't compile correctly with
         * move only types */
#if CPD3_CXX >= 201700L && defined(HAVE_CXX_PARALLEL)
        std::sort(std::execution::par_unseq, begin, end, comp);
#elif defined(__GLIBC__)
        __gnu_parallel::sort(begin, end, comp);
#else
        std::sort(begin, end, comp);
#endif
    }

    /**
     * A wrapper around std::merge that uses an optimized (parallel, if available) implementation.
     */
    template<typename ForwardIt1, typename ForwardIt2, typename ForwardIt3, typename Compare>
    static inline auto optimizedMerge(ForwardIt1 first1,
                                      ForwardIt1 last1,
                                      ForwardIt2 first2,
                                      ForwardIt2 last2,
                                      ForwardIt3 d_first,
                                      Compare comp) -> typename std::enable_if<
            std::is_same<typename std::iterator_traits<ForwardIt3>::iterator_category,
                         std::forward_iterator_tag>::value>::type
    {
#if CPD3_CXX >= 201700L && defined(HAVE_CXX_PARALLEL)
        std::merge(std::execution::par_unseq, first1, last1, first2, last2, d_first, comp);
#elif defined(__GLIBC__)
        __gnu_parallel::merge(first1, last1, first2, last2, d_first, comp);
#else
        std::merge(first1, last1, first2, last2, d_first, comp);
#endif
    }

    template<typename ForwardIt1, typename ForwardIt2, typename OutputIt, typename Compare>
    static inline auto optimizedMerge(ForwardIt1 first1,
                                      ForwardIt1 last1,
                                      ForwardIt2 first2,
                                      ForwardIt2 last2,
                                      OutputIt d_first,
                                      Compare comp) -> typename std::enable_if<
            !std::is_same<typename std::iterator_traits<OutputIt>::iterator_category,
                          std::forward_iterator_tag>::value>::type
    {
        std::merge(first1, last1, first2, last2, d_first, comp);
    }

    /**
     * A simple two-way merge.
     *
     * @tparam InputType    the input type, a pair of start and end iterators
     * @tparam OutputTpe    the output container type
     * @param a             the first input
     * @param b             the second input
     * @param output        the output list
     */
    template<typename InputType, typename OutputType>
    static inline void twoWay(InputType a, InputType b, OutputType &output)
    {
#ifdef MERGE_TIME_DEBUGGING
        verifyIteratorSorted(a.first, a.second);
        verifyIteratorSorted(b.first, b.second);
#endif

        optimizedMerge(a.first, a.second, b.first, b.second, Util::back_emplacer(output),
                       Range::startLessThan);

#ifdef MERGE_TIME_DEBUGGING
        verifyIteratorSorted(output.begin(), output.end());
#endif
    }

    /** @see twoWay(InputType, InputType, OutputType) */
    template<typename Container>
    static inline void twoWay(const Container &a, const Container &b, Container &output)
    {
        typedef typename Container::const_iterator Iterator;
        typedef std::pair<Iterator, Iterator> MergeBounds;
        twoWay(MergeBounds(a.begin(), a.end()), MergeBounds(a.begin(), a.end()), output);
    }

    /** @see twoWay(InputType, InputType, OutputType) */
    template<typename Container>
    static inline Container twoWay(const Container &a, const Container &b)
    {
        Container output;
        output.reserve(a.size() + b.size());
        twoWay(a, b, output);
        return output;
    }


    /**
     * A simple two-way merge of only defined values.
     *
     * @tparam InputType    the input type, a pair of start and end iterators
     * @tparam OutputTpe    the output container type
     * @param a             the first input
     * @param b             the second input
     * @param output        the output list
     */
    template<typename InputType, typename OutputTpe>
    static inline void twoWayDefined(InputType a, InputType b, OutputTpe &output)
    {
#ifdef MERGE_TIME_DEBUGGING
        verifyIteratorSorted(a.first, a.second);
        verifyIteratorSorted(b.first, b.second);
#endif

        optimizedMerge(a.first, a.second, b.first, b.second, Util::back_emplacer(output),
                       Range::startLessThan);

#ifdef MERGE_TIME_DEBUGGING
        verifyIteratorSorted(output.begin(), output.end());
#endif
    }

    /** @see twoWayDefined(InputType, InputType, OutputType) */
    template<typename Container>
    static inline void twoWayDefined(const Container &a, const Container &b, Container &output)
    {
        typedef typename Container::const_iterator Iterator;
        typedef std::pair<Iterator, Iterator> MergeBounds;
        twoWayDefined(MergeBounds(a.begin(), a.end()), MergeBounds(a.begin(), a.end()), output);
    }

    /** @see twoWay(InputType, InputType, OutputType) */
    template<typename Container>
    static inline Container twoWayDefined(const Container &a, const Container &b)
    {
        Container output;
        output.reserve(a.size() + b.size());
        twoWayDefined(a, b, output);
        return output;
    }

    /**
     * Perform an n-way merge of pairs of iterators.
     *
     * @tparam InputContainer   a list of pairs of start and end iterators to merge from
     * @tparam OutputType       the output list type
     * @param inputs            the inputs to merge
     * @param output            the output
     */
    template<typename InputContainer, typename OutputType>
    static inline void nWay(const InputContainer &inputs, OutputType &output)
    { return nWayInternal<true>(inputs, output); }

    /**
     * Perform an n-way merge of pairs of iterators of only defined values.
     *
     * @tparam InputContainer   a list of pairs of start and end iterators to merge from
     * @tparam OutputType       the output list type
     * @param inputs            the inputs to merge
     * @param output            the output
     */
    template<typename InputContainer, typename OutputType>
    static inline void nWayDefined(const InputContainer &inputs, OutputType &output)
    { return nWayInternal<false>(inputs, output); }

    /**
     * Perform an n-way merge of pairs of iterators avoiding any random access.
     *
     * @tparam InputContainer   a list of pairs of start and end iterators to merge from
     * @tparam OutputType       the output list type
     * @param inputs            the inputs to merge
     * @param output            the output
     */
    template<typename InputContainer, typename OutputType>
    static inline void nWaySequential(const InputContainer &inputs, OutputType &output)
    { return nWayExecute<true>(inputs, output); }

    /**
     * Perform an n-way merge of pairs of iterators of only defined values avoiding any
     * random access.
     *
     * @tparam InputContainer   a list of pairs of start and end iterators to merge from
     * @tparam OutputType       the output list type
     * @param inputs            the inputs to merge
     * @param output            the output
     */
    template<typename InputContainer, typename OutputType>
    static inline void nWaySequentialDefined(const InputContainer &inputs, OutputType &output)
    { return nWayExecute<false>(inputs, output); }
};


/**
 * A multiplexer of streams of start time sorted values.
 *
 * @tparam T            the data type
 * @tparam Container    the storage list type
 */
template<typename T, typename Container = std::deque<T> >
class StreamMultiplexer {

    typedef std::move_iterator<typename Container::iterator> MergeIterator;
    typedef std::pair<MergeIterator, MergeIterator> MergeInput;
    typedef std::vector<MergeInput> MergeList;

#ifdef MERGE_TIME_DEBUGGING

    template<typename Iterator>
    static void verifyTimeSorted(Iterator begin, Iterator end)
    {
        if (begin == end)
            return;
        for (Iterator prior = begin++; begin != end; prior = begin++) {
            Q_ASSERT(Range::compareStart(prior->getStart(), begin->getStart()) <= 0);
        }
    }

    template<typename Input>
    static void verifyTimeSorted(const Input &container)
    { verifyTimeSorted(container.begin(), container.end()); }

    template<typename Iterator>
    void verifyAllAfterAdvance(Iterator begin, Iterator end) const
    {
        for (; begin != end; ++begin) {
            Q_ASSERT(Range::compareStart(lastOutputAdvance, begin->getStart()) <= 0);
        }
    }

    template<typename Input>
    void verifyAllAfterAdvance(const Input &container) const
    { verifyAllAfterAdvance(container.begin(), container.end()); }

#endif

    class Input {
        friend class StreamMultiplexer;

        double streamTime;
        bool isCurrentTime;

    protected:
        StreamMultiplexer *mux;

        enum {
            /* Waiting for the first defined time */
                    IncomingUndefined = 0,

            /* We've seen a defined time, but we haven't yet processed all
             * undefined ones */
                    UndefinedPending,

            /* All undefined values have been processed so only defined ones
             * are possible now */
                    AllDefined,

            /* The input has ended and is waiting deletion */
                    Ended
        } state;

#ifndef NDEBUG
        int inputUID;
#endif

        Input(StreamMultiplexer *m) : streamTime(m->lastOutputAdvance),
                                      isCurrentTime(true),
                                      mux(m),
                                      state(IncomingUndefined)
        {
            Q_ASSERT(mux->moreInputsPossible);
            mux->inputs.push_back(this);

            if (FP::defined(streamTime))
                state = AllDefined;

#ifndef NDEBUG
            this->inputUID = ++mux->inputUID;
#endif
        }

        Input(StreamMultiplexer *m, QDataStream &stream) : streamTime(FP::undefined()),
                                                           isCurrentTime(true),
                                                           mux(m),
                                                           state(IncomingUndefined)
        {
            mux->inputs.push_back(this);

#ifndef NDEBUG
            {
                quint32 i = 0;
                stream >> i;
                this->inputUID = (int) i;
            }
#endif

            stream >> streamTime;
            stream >> isCurrentTime;
            {
                quint8 i = 0;
                stream >> i;
                state = static_cast<decltype(state)>(i);
            }

            Q_ASSERT(state != Ended);
        }

        inline double currentAdvanceTime() const
        { return streamTime; }

        virtual void completeOutput(double time)
        {
            switch (state) {
            case IncomingUndefined:
                /* Still waiting for the end of undefined values, so nothing
                 * further to do */
                Q_ASSERT(!FP::defined(time));
                Q_ASSERT(isCurrentTime);
                break;

            case UndefinedPending:
                Q_ASSERT(FP::defined(streamTime));

                /* We've now seen and output all undefined values, so as soon
                 * as the global moves out of the undefined region, transition
                 * to all defined */
                if (!FP::defined(time)) {
                    Q_ASSERT(!isCurrentTime);
                    break;
                }
                state = AllDefined;
                /* Fall through */
            case AllDefined:
                Q_ASSERT(FP::defined(streamTime));
                if (!FP::defined(time)) {
                    Q_ASSERT(!isCurrentTime);
                    return;
                }
                Q_ASSERT(FP::defined(time));
                Q_ASSERT(time <= streamTime);

                isCurrentTime = (time == streamTime);
                break;

            case Ended:
                /* The end has been handled, so the input can be deleted */
                Q_ASSERT(!isCurrentTime);
                break;
            }
        }

        virtual void prepareOutputUndefined(MergeList &merge) = 0;

        virtual void prepareOutput(double time, MergeList &merge) = 0;

        virtual void prepareFinal(MergeList &merge) = 0;

        virtual size_t bufferSize() const = 0;

        virtual size_t serializeAuxiliarySize() const = 0;

        virtual void serializeAuxiliary(QDataStream &stream) const = 0;

    public:
        virtual ~Input()
        { }

        /**
         * Test if the input is blocking the multiplexer.
         *
         * @return          true if the input is holding the multiplexer back
         */
        inline bool isBlocking() const
        { return isCurrentTime; }

        /**
         * Advance the input.
         *
         * @param time      the time to advance to
         * @return          true if the change may have made values ready
         */
        bool advance(double time)
        {
            switch (state) {
            case IncomingUndefined:
                /* Waiting for the end of undefined, so all values can still
                 * be output immediately */
                if (!FP::defined(time)) {
                    Q_ASSERT(isCurrentTime);
                    return true;
                }
                isCurrentTime = false;
                streamTime = time;
                state = UndefinedPending;
                return true;

            case UndefinedPending:
                Q_ASSERT(FP::defined(time));
                Q_ASSERT(FP::defined(streamTime));
                /* Still waiting for processing to complete all undefined values, so
                 * we don't need to do anything until then */
                Q_ASSERT(!isCurrentTime);
                Q_ASSERT(time >= streamTime);
                streamTime = time;
                return false;

            case AllDefined:
                Q_ASSERT(FP::defined(time));
                Q_ASSERT(FP::defined(streamTime));
                Q_ASSERT(time >= streamTime);
                streamTime = time;
                if (isCurrentTime) {
                    isCurrentTime = false;
                    return true;
                }
                return false;

            case Ended:
                Q_ASSERT(false);
                break;
            }
            Q_ASSERT(false);
            return false;
        }

        /**
         * End the input.  No more data is permitted and the input may
         * be deleted by the multiplexer.
         *
         * @return           true if the change may have made values ready
         */
        bool end()
        {
            switch (state) {
            case IncomingUndefined:
            case UndefinedPending:
            case AllDefined:
                state = Ended;
                if (isCurrentTime) {
                    isCurrentTime = false;
                    return true;
                }
                return false;
            case Ended:
                return false;
            }
            Q_ASSERT(false);
            return false;
        }

        /**
         * Serialize the input into a data stream.
         *
         * @param stream    the target stream
         */
        virtual void serialize(QDataStream &stream) const
        {
            Q_ASSERT(state != Ended);

#ifndef NDEBUG
            stream << (quint32) inputUID;
#endif
            stream << streamTime;
            stream << isCurrentTime;
            stream << (quint8) state;
        }

        /**
         * Describe the input state.
         *
         * @param detailed      output full detailed state
         * @return              a description of the input
         */
        virtual void describe(QDebug debug, bool detailed = false) const
        {
            Q_UNUSED(detailed);
            QDebugStateSaver saver(debug);
            debug.nospace();
            if (state != Ended) {
                debug << Logging::time(streamTime);
            } else {
                debug << "ENDED";
            }
        }
    };

    friend class Input;

    std::list<Input *> inputs;
    bool moreInputsPossible;

    Container pendingOutput;
    bool pendingUnsorted;
    double lastOutputAdvance;

    class SortedBuffer : public Input {
        Container buffer;
        typename Container::iterator mergeEnd;

#ifdef MERGE_TIME_DEBUGGING

        void verifyValueInsert(const T &value) const
        {
            Q_ASSERT(buffer.empty() ||
                             Range::compareStart(buffer.back().getStart(), value.getStart()) <= 0);
            if (this->state == Input::UndefinedPending || this->state == Input::AllDefined) {
                Q_ASSERT(FP::defined(value.getStart()));
                Q_ASSERT(value.getStart() >= this->currentAdvanceTime());
            }
        }

        template<typename Iterator>
        void verifyInsert(Iterator begin, Iterator end)
        {
            verifyTimeSorted(begin, end);
            for (; begin != end; ++begin) {
                verifyValueInsert(*begin);
            }
        }

        template<typename Input>
        void verifyInsert(const Input &container)
        {
            verifyInsert(container.begin(), container.end());
        }

#endif

    protected:
        SortedBuffer(StreamMultiplexer *manager) : Input(manager), buffer(), mergeEnd()
        { }

        SortedBuffer(StreamMultiplexer *manager, QDataStream &stream) : Input(manager, stream),
                                                                        buffer(),
                                                                        mergeEnd()
        {
            quint32 n = 0;
            stream >> n;
            for (quint32 i = 0; i < n; ++i) {
                T v;
                stream >> v;
                buffer.push_back(std::move(v));
            }
#ifdef MERGE_TIME_DEBUGGING
            verifyTimeSorted(buffer);
#endif
        }

        virtual void prepareOutputUndefined(MergeList &merge)
        {
            /* If we have no values, we never need to do anything */
            if (buffer.empty()) {
                mergeEnd = buffer.begin();
                return;
            }

            auto first = buffer.begin();
            auto last = buffer.end();

            switch (this->state) {
            case Input::IncomingUndefined:
                mergeEnd = last;
                break;
            case Input::UndefinedPending:
            case Input::Ended:
                mergeEnd = Range::lastUndefinedStart(first, last);
                break;
            case Input::AllDefined:
                mergeEnd = first;
                return;
            }

            if (this->state == Input::Ended) {
                if (mergeEnd == first) {
                    this->mux->inject(std::move(buffer));
                    buffer.clear();
                    return;
                }

                this->mux->inject(std::make_move_iterator(mergeEnd), std::make_move_iterator(last));
            }

            merge.emplace_back(std::make_move_iterator(first), std::make_move_iterator(mergeEnd));
        }

        virtual void prepareOutput(double time, MergeList &merge)
        {
            Q_ASSERT(FP::defined(time));

            /* If we have no values, we never need to do anything */
            if (buffer.empty()) {
                mergeEnd = buffer.begin();
                return;
            }

            auto first = buffer.begin();
            auto last = buffer.end();

            switch (this->state) {
            case Input::IncomingUndefined:
                Q_ASSERT(false);
                break;

            case Input::UndefinedPending:
                mergeEnd = Range::heuristicUpperBound(first, last, time);
                if (mergeEnd == first)
                    return;
                break;

            case Input::AllDefined:
                mergeEnd = Range::heuristicUpperBoundDefined(first, last, time);
                if (mergeEnd == first)
                    return;
                break;

            case Input::Ended:

                /* If we're ended, then put the appropriate values into the merge list, but
                * dump the rest into the common pool */
                mergeEnd = Range::heuristicUpperBound(first, last, time);

                /* Nothing to merge, dump it all */
                if (mergeEnd == first) {
                    this->mux->inject(std::move(buffer));
                    buffer.clear();
                    mergeEnd = buffer.end();
                    return;
                }

                this->mux->inject(std::make_move_iterator(mergeEnd), std::make_move_iterator(last));
                break;
            }

            merge.emplace_back(std::make_move_iterator(first), std::make_move_iterator(mergeEnd));
        }

        virtual void prepareFinal(MergeList &merge)
        {
            Q_ASSERT(this->state == Input::Ended);
            if (buffer.empty())
                return;
            auto first = buffer.begin();
            mergeEnd = buffer.end();
            merge.emplace_back(std::make_move_iterator(first), std::make_move_iterator(mergeEnd));
        }

        virtual void completeOutput(double time)
        {
            Input::completeOutput(time);

            /* If we've ended, then the buffer is handled, so just clear it */
            if (this->state == Input::Ended) {
                buffer.clear();
                return;
            }

            buffer.erase(buffer.begin(), mergeEnd);
        }

        template<typename AddType>
        inline void incomingInputValue(AddType &&add)
        {
            Q_ASSERT(this->state != Input::Ended);
#ifdef MERGE_TIME_DEBUGGING
            verifyValueInsert(add);
#endif
            buffer.push_back(std::forward<AddType>(add));
        }

        template<typename... Args>
        inline const T &emplaceInputValue(Args &&... args)
        {
            buffer.emplace_back(std::forward<Args>(args)...);
#ifdef MERGE_TIME_DEBUGGING
            if (buffer.size() >= 2) {
                auto first = buffer.cend() - 1;
                auto second = first--;
                Q_ASSERT(Range::compareStart(first->getStart(), second->getStart()) <= 0);
                if (this->state == Input::UndefinedPending || this->state == Input::AllDefined) {
                    Q_ASSERT(FP::defined(second->getStart()));
                    Q_ASSERT(second->getStart() >= this->currentAdvanceTime());
                }
            }
#endif
            return buffer.back();
        }

        template<typename Iterator>
        inline void incomingInput(Iterator begin, Iterator end)
        {
            Q_ASSERT(this->state != Input::Ended);
#ifdef MERGE_TIME_DEBUGGING
            verifyInsert(begin, end);
#endif
            std::copy(begin, end, Util::back_emplacer(buffer));
        }

        template<typename Input>
        inline void incomingInput(Input &&container)
        {
            Q_ASSERT(this->state != (StreamMultiplexer<T, Container>::Input::Ended));
            if (container.empty())
                return;
#ifdef MERGE_TIME_DEBUGGING
            verifyInsert(container);
#endif

            Util::append(std::forward<Input>(container), buffer);
        }

        virtual size_t serializeAuxiliarySize() const
        {
            if (this->state != Input::Ended)
                return 0;
            return (size_t) buffer.size();
        }

        virtual void serializeAuxiliary(QDataStream &stream) const
        {
            if (this->state != Input::Ended)
                return;
            for (const auto &i : buffer) {
                stream << i;
            }
        }

    public:
        virtual ~SortedBuffer()
        { }

        virtual void serialize(QDataStream &stream) const
        {
            Input::serialize(stream);

            stream << (quint32) buffer.size();
            for (const auto &i : buffer) {
                stream << i;
            }
        }

        virtual void describe(QDebug debug, bool detailed = false) const
        {
            QDebugStateSaver saver(debug);
            debug.nospace();
            if (this->state != Input::Ended) {
                debug << Logging::time(this->streamTime) << ',';
            } else {
                debug << "ENDED,";
            }
            if (detailed) {
                debug << "[" << buffer << "]";
            } else {
                debug << buffer.size();
            }
        }

        /**
         * Get the number of values the input currently has in its buffer.
         * @return
         */
        virtual size_t bufferSize() const
        { return static_cast<size_t>(buffer.size()); }
    };


#ifndef NDEBUG
    int inputUID;
#endif

    void checkUnsortSingle()
    {
        if (pendingUnsorted)
            return;
        if (pendingOutput.size() < 2)
            return;

        auto first = pendingOutput.cend() - 1;
        auto second = first--;
        if (Range::compareStart(first->getStart(), second->getStart()) > 0)
            pendingUnsorted = true;
    }

    template<typename AddType>
    void checkUnsortAdd(const AddType &add)
    {
        if (pendingUnsorted)
            return;

        Q_ASSERT(!pendingOutput.empty());
        if (Range::compareStart(pendingOutput.back().getStart(), add.getStart()) > 0)
            pendingUnsorted = true;
    }

public:
    StreamMultiplexer()
            : inputs(),
              moreInputsPossible(true),
              pendingOutput(),
              pendingUnsorted(false),
              lastOutputAdvance(FP::undefined())
#ifndef NDEBUG
            ,
              inputUID(0)
#endif
    { }

    ~StreamMultiplexer()
    {
        qDeleteAll(inputs);
    }


    /**
     * Inject a value into the multiplexer.  Injected values will be emitted
     * on the next output when all inputs have advanced past them.
     *
     * @tparam AddType  the type of the value being added
     * @param add       the value to add
     */
    template<typename AddType>
    inline void injectValue(AddType &&add)
    {
        pendingOutput.push_back(std::forward<AddType>(add));
        checkUnsortSingle();
#ifdef MERGE_TIME_DEBUGGING
        Q_ASSERT(Range::compareStart(lastOutputAdvance, pendingOutput.back().getStart()) <= 0);
#endif
    }

    /***
     * Inject a value into the multiplexer, calling the constructor directly.
     * Injected values will be emitted on the next output when all inputs have
     * advanced past them.
     *
     * @tparam Args     the constructor argument types
     * @param args      the constructor arguments
     */
    template<typename... Args>
    inline void emplaceValue(Args &&... args)
    {
        pendingOutput.emplace_back(std::forward<Args>(args)...);
        checkUnsortSingle();
#ifdef MERGE_TIME_DEBUGGING
        Q_ASSERT(Range::compareStart(lastOutputAdvance, pendingOutput.back().getStart()) <= 0);
#endif
    }

    /**
     * Inject values into the multiplexer.  Injected values will be emitted
     * on the next output when all inputs have advanced past them.
     *
     * @tparam Iterator     a sequential iterator
     * @param begin         the start to add
     * @param end           the end to add
     */
    template<typename Iterator>
    inline void inject(Iterator begin, Iterator end)
    {
        if (begin == end)
            return;
#ifdef MERGE_TIME_DEBUGGING
        verifyTimeSorted(begin, end);
        Q_ASSERT(Range::compareStart(lastOutputAdvance, begin->getStart()) <= 0);
#endif
        if (!pendingOutput.empty()) {
            checkUnsortAdd(*begin);
        } else {
            Q_ASSERT(!pendingUnsorted);
        }
        std::copy(begin, end, Util::back_emplacer(pendingOutput));
    }

    /**
     * Inject values into the multiplexer.  Injected values will be emitted
     * on the next output when all inputs have advanced past them.
     *
     * @tparam Input        the container type
     * @param list          the list of values
     */
    template<typename Input>
    inline void inject(Input &&container)
    {
        if (container.empty())
            return;
#ifdef MERGE_TIME_DEBUGGING
        verifyTimeSorted(container.begin(), container.end());
        Q_ASSERT(Range::compareStart(lastOutputAdvance, container.front().getStart()) <= 0);
#endif
        if (!pendingOutput.empty()) {
            checkUnsortAdd(container.front());
        } else {
            Q_ASSERT(!pendingUnsorted);
        }
        Util::append(std::forward<Input>(container), pendingOutput);
    }


    /**
     * Inject a value into the multiplexer.  Injected values will be emitted
     * on the next output when all inputs have advanced past them.
     * <br>
     * The values do not have to be sorted but must be after the latest
     * emit time.
     *
     * @tparam AddType  the type of the value being added
     * @param add       the value to add
     */
    template<typename AddType>
    void injectValueUnsorted(AddType &&add)
    { injectValue(std::forward<AddType>(add)); }

    /***
     * Inject a value into the multiplexer, calling the constructor directly.
     * Injected values will be emitted on the next output when all inputs have
     * advanced past them.
     * <br>
     * The values do not have to be sorted but must be after the latest
     * emit time.
     *
     * @tparam Args     the constructor argument types
     * @param args      the constructor arguments
     */
    template<typename... Args>
    void emplaceValueUnsorted(Args &&... args)
    { emplaceValue(std::forward<Args>(args)...); }

    /**
     * Inject values into the multiplexer.  Injected values will be emitted
     * on the next output when all inputs have advanced past them.
     * <br>
     * The values do not have to be sorted but must be after the latest
     * emit time.
     *
     * @tparam Iterator     a sequential iterator
     * @param begin         the start to add
     * @param end           the end to add
     */
    template<typename Iterator>
    void injectUnsorted(Iterator begin, Iterator end)
    {
        if (begin == end)
            return;
#ifdef MERGE_TIME_DEBUGGING
        verifyAllAfterAdvance(begin, end);
#endif
        pendingUnsorted = true;
        std::copy(begin, end, Util::back_emplacer(pendingOutput));
    }

    /**
     * Inject values into the multiplexer.  Injected values will be emitted
     * on the next output when all inputs have advanced past them.
     * <br>
     * The values do not have to be sorted but must be after the latest
     * emit time.
     *
     * @tparam Input        the container type
     * @param container     the list of values
     */
    template<typename Input>
    inline void injectUnsorted(Input &&container)
    {
        if (container.empty())
            return;
#ifdef MERGE_TIME_DEBUGGING
        verifyAllAfterAdvance(container);
#endif
        pendingUnsorted = true;
        Util::append(std::forward<Input>(container), pendingOutput);
    }


    /**
     * Reset the multiplexer.  This releases all inputs.
     *
     * @param resetPending  reset pending data, not just inputs
     */
    void reset(bool resetPending = true)
    {
        if (resetPending) {
            pendingOutput.clear();
            pendingUnsorted = false;
            lastOutputAdvance = FP::undefined();
        }

        moreInputsPossible = true;
        qDeleteAll(inputs);
        inputs.clear();
    }

    /**
     * Inform the multiplexer that all inputs have created and only the existing
     * ones will receive data.
     */
    void creationComplete()
    {
        this->moreInputsPossible = false;
    }

    /**
     * Finish all existing inputs, they will not accept any more data.
     *
     * @param moreCreationPossible  if false then no more inputs can be created
     */
    void finishAll(bool moreCreationPossible = false)
    {
        for (auto i : inputs) {
            i->end();
        }
        if (!moreCreationPossible)
            this->moreInputsPossible = false;
    }


    /**
     * A simple input stream.  The values added to this must be in start
     * time ascending order with the advance changing the start time.
     */
    class Simple : public SortedBuffer {
        friend class StreamMultiplexer;

        Simple(StreamMultiplexer *manager) : SortedBuffer(manager)
        { }

        Simple(StreamMultiplexer *manager, QDataStream &stream) : SortedBuffer(manager, stream)
        { }

    public:
        virtual ~Simple()
        { }

#ifndef NDEBUG

        int uid() const
        { return (this->inputUID << 2) | 0x0; }

#endif

        /**
         * Add a value to the input.
         *
         * @tparam AddType  the type of the value
         * @param add       the value to add
         * @return          true if the change may have made values ready
         */
        template<typename AddType>
        bool incomingValue(AddType &&add)
        {
            double adv = add.getStart();
            this->incomingInputValue(std::forward<AddType>(add));
            return this->advance(adv);
        }

        /***
         * Add a value to the input.
         *
         * @tparam Args     the constructor argument types
         * @param args      the constructor arguments
         * @return          true if the change may have made values ready
         */
        template<typename... Args>
        inline bool emplaceValue(Args &&... args)
        { return this->advance(this->emplaceInputValue(std::forward<Args>(args)...).getStart()); }

        /**
         * Add values to the input.
         *
         * @tparam Iterator a sequential access iterator
         * @param add       the values to add
         * @return          true if the change may have made values ready
         */
        template<typename Iterator>
        bool incoming(Iterator begin, Iterator end)
        {
            if (begin == end)
                return false;
            double adv = (end - 1)->getStart();
            this->incomingInput(begin, end);
            return this->advance(adv);
        }

        /**
         * Add values to the input.
         *
         * @tparam Input        the container type
         * @param add           the values to add
         * @return              true if the change may have made values ready
         */
        template<typename Input>
        bool incoming(Input &&container)
        {
            if (container.empty())
                return false;
            double adv = container.back().getStart();
            this->incomingInput(std::forward<Input>(container));
            return this->advance(adv);
        }
    };

    /**
     * Create a simple input.  Simple input accept a normal stream of
     * values sorted by the start time.
     * <br>
     * The input is implicitly advanced to the multiplexer's global advance time.
     *
     * @return              a simple input, owned by the multiplexer
     */
    Simple *createSimple()
    { return new Simple(this); }

    /**
     * Restore an input from a serialized stream
     *
     * @param stream        the input stream
     * @return              the restored input
     */
    Simple *deserializeSimple(QDataStream &stream)
    { return new Simple(this, stream); }


    /**
     * A segmented input.  This accepts values with a start time that is strictly
     * after the end of the last one or the advance.
     */
    class Segmented : public SortedBuffer {
        friend class StreamMultiplexer;

        Segmented(StreamMultiplexer *manager) : SortedBuffer(manager)
        { }

        Segmented(StreamMultiplexer *manager, QDataStream &stream) : SortedBuffer(manager, stream)
        { }

    public:
        virtual ~Segmented()
        { }

#ifndef NDEBUG

        int uid() const
        { return (this->inputUID << 2) | 0x1; }

#endif

        /**
         * Add a value to the input.
         *
         * @tparam AddType  the type of the value
         * @param add       the value to add
         * @return          true if the change may have made values ready
         */
        template<typename AddType>
        bool incomingValue(AddType &&add)
        {
            double adv = add.getEnd();
            this->incomingInputValue(std::forward<AddType>(add));
            if (FP::defined(adv))
                return this->advance(adv);
            return this->end();
        }

        /***
         * Add a value to the input.
         *
         * @tparam Args     the constructor argument types
         * @param args      the constructor arguments
         * @return          true if the change may have made values ready
         */
        template<typename... Args>
        inline bool emplaceValue(Args &&... args)
        { return this->advance(this->emplaceInputValue(std::forward<Args>(args)...).getEnd()); }

        /**
         * Add values to the input.
         *
         * @tparam Iterator a sequential access iterator
         * @param add       the values to add
         * @return          true if the change may have made values ready
         */
        template<typename Iterator>
        bool incoming(Iterator begin, Iterator end)
        {
            if (begin == end)
                return false;
            double adv = (end - 1)->getEnd();
            this->incomingInput(begin, end);
            if (FP::defined(adv))
                return this->advance(adv);
            return this->end();
        }

        /**
         * Add values to the input.
         *
         * @tparam Input        the container type
         * @param add           the values to add
         * @return              true if the change may have made values ready
         */
        template<typename Input>
        bool incoming(Input &&container)
        {
            if (container.empty())
                return false;
            double adv = container.back().getEnd();
            this->incomingInput(std::forward<Input>(container));
            if (FP::defined(adv))
                return this->advance(adv);
            return this->end();
        }
    };

    /**
     * Create a segmented input.  Simple inputs accept a stream non-overlapping
     * values.  So the start of a value must always be greater than or equal to the
     * end of the previous one.
     * <br>
     * The input is implicitly advanced to the multiplexer's global advance time.
     *
     * @return              a segmented input, owned by the multiplexer
     */
    Segmented *createSegmented()
    { return new Segmented(this); }

    /**
     * Restore an input from a serialized stream
     *
     * @param stream        the input stream
     * @return              the restored input
     */
    Segmented *deserializeSegmented(QDataStream &stream)
    { return new Segmented(this, stream); }


    /**
     * An unsorted input.  This accepts unsorted values as long as they
     * are start after or equal it its last advance.
     */
    class Unsorted : public Input {
        friend class StreamMultiplexer;

        Unsorted(StreamMultiplexer *manager) : Input(manager)
        { }

        Unsorted(StreamMultiplexer *manager, QDataStream &stream) : Input(manager, stream)
        { }

    protected:
        virtual void prepareOutputUndefined(MergeList &merge)
        { }

        virtual void prepareOutput(double time, MergeList &merge)
        { }

        virtual void prepareFinal(MergeList &merge)
        { }

        virtual size_t bufferSize() const
        { return 0; }

        virtual size_t serializeAuxiliarySize() const
        { return 0; }

        virtual void serializeAuxiliary(QDataStream &) const
        { }

    public:
        virtual ~Unsorted()
        { }

#ifndef NDEBUG

        int uid() const
        { return (this->inputUID << 2) | 0x2; }

#endif

        /**
         * Add a value to the input.
         *
         * @tparam AddType  the type of the value
         * @param add       the value to add
         * @return          true if the change may have made values ready
         */
        template<typename AddType>
        inline bool incomingValue(AddType &&add)
        {
            this->mux->injectValueUnsorted(std::forward<AddType>(add));
            return false;
        }

        /***
         * Add a value to the input.
         *
         * @tparam Args     the constructor argument types
         * @param args      the constructor arguments
         * @return          true if the change may have made values ready
         */
        template<typename... Args>
        inline bool emplaceValue(Args &&... args)
        {
            this->mux->emplaceValueUnsorted(std::forward<Args>(args)...);
            return false;
        }

        /**
         * Add values to the input.
         *
         * @tparam Iterator a sequential access iterator
         * @param add       the values to add
         * @return          true if the change may have made values ready
         */
        template<typename Iterator>
        inline bool incoming(Iterator begin, Iterator end)
        {
            this->mux->injectUnsorted(begin, end);
            return false;
        }

        /**
         * Add values to the input.
         *
         * @tparam Input        the container type
         * @param add           the values to add
         * @return              true if the change may have made values ready
         */
        template<typename Input>
        inline bool incoming(Input &&container)
        {
            this->mux->injectUnsorted(std::forward<Input>(container));
            return false;
        }
    };

    /**
     * Create an unsorted input.  Unsorted inputs accept values that have not yet
     * been sorted, as long as all their starts are greater than or equal to the
     * latest advance.
     * <br>
     * The input is implicitly advanced to the multiplexer's global advance time.
     *
     * @return              an unsorted input, owned by the multiplexer
     */
    Unsorted *createUnsorted()
    { return new Unsorted(this); }

    /**
     * Restore an input from a serialized stream
     *
     * @param stream        the input stream
     * @return              the restored input
     */
    Unsorted *deserializeUnsorted(QDataStream &stream)
    { return new Unsorted(this, stream); }


    /**
     * A placeholder input.  This holds a point in time until it is advanced.
     */
    class Placeholder : public Input {
        friend class StreamMultiplexer;

        Placeholder(StreamMultiplexer *manager) : Input(manager)
        { }

        Placeholder(StreamMultiplexer *manager, QDataStream &stream) : Input(manager, stream)
        { }

    protected:
        virtual void prepareOutputUndefined(MergeList &)
        { }

        virtual void prepareOutput(double, MergeList &)
        { }

        virtual void prepareFinal(MergeList &)
        { }

        virtual size_t bufferSize() const
        { return 0; }

        virtual size_t serializeAuxiliarySize() const
        { return 0; }

        virtual void serializeAuxiliary(QDataStream &) const
        { }

    public:
        virtual ~Placeholder()
        { }

#ifndef NDEBUG

        int uid() const
        { return (this->inputUID << 2) | 0x03; }

#endif
    };

    /**
     * Create an placeholder input.  Placeholder inputs simply hold back
     * the merger until they are advanced.
     * <br>
     * The input is implicitly advanced to the multiplexer's global advance time.
     *
     * @return              a placeholder input, owned by the multiplexer
     */
    Placeholder *createPlaceholder()
    { return new Placeholder(this); }

    /**
     * Restore an input from a serialized stream
     *
     * @param stream        the input stream
     * @return              the restored input
     */
    Placeholder *deserializePlaceholder(QDataStream &stream)
    { return new Placeholder(this, stream); }

    /**
     * Take the current output of the multiplexer.
     *
     * @tparam Destination  the output list type
     * @param output        the output the data are appended to
     */
    template<typename Destination>
    void output(Destination &output)
    {
        double advanceTime = FP::undefined();
        enum {
            /* No inputs to merge */
                    Invalid,

            /* At least one input is still in the undefined region */
                    Undefined,

            /* All inputs in the defined region */
                    Defined,

            /* All inputs ended, but the time is valid (the next output will be Invalid */
                    Ended
        } outputType = Invalid;
        bool definedTransition = false;

        /* First stage, figure out the type and bounds we're merging with */
        for (auto in : inputs) {
            switch (in->state) {
            case Input::IncomingUndefined:
                outputType = Undefined;
                advanceTime = FP::undefined();
                Q_ASSERT(!FP::defined(in->currentAdvanceTime()));
                break;

            case Input::UndefinedPending:
                definedTransition = true;
            case Input::AllDefined: {
                if (outputType == Undefined)
                    break;

                double time = in->currentAdvanceTime();
                Q_ASSERT(FP::defined(time));
                Q_ASSERT(Range::compareStart(lastOutputAdvance, time) <= 0);

                if (outputType != Defined) {
                    advanceTime = time;
                    outputType = Defined;
                    break;
                }

                Q_ASSERT(FP::defined(advanceTime));

                if (time >= advanceTime)
                    continue;
                advanceTime = time;
                break;
            }

            case Input::Ended:
                double time = in->currentAdvanceTime();
                Q_ASSERT(Range::compareStart(lastOutputAdvance, time) <= 0);
                definedTransition = true;

                /* Track the advance time so that we can output something from the
                 * global pool initially */
                if (outputType == Invalid) {
                    outputType = Ended;
                    advanceTime = time;
                } else if (outputType == Ended) {
                    if (!FP::defined(time) || time < advanceTime)
                        advanceTime = time;
                }
                break;
            }

            Q_ASSERT(outputType != Invalid);
        }

        /* No merging possible, so just short circuit */
        if (outputType == Invalid) {
            Q_ASSERT(inputs.empty());

            /* Can get more data, so don't output anything */
            if (moreInputsPossible)
                return;

            /* No pending, so can't possibly get anything else */
            if (pendingOutput.empty())
                return;

            /* Sort if required */
            if (pendingUnsorted) {
                if (FP::defined(lastOutputAdvance)) {
                    Merge::optimizedSort(pendingOutput.begin(), pendingOutput.end(),
                                         Range::startLessThanDefined<T>);
                } else {
                    Merge::optimizedSort(pendingOutput.begin(), pendingOutput.end(),
                                         Range::startLessThan<T>);
                }
                pendingUnsorted = false;
            }

#ifdef MERGE_TIME_DEBUGGING
            verifyTimeSorted(pendingOutput);
            Q_ASSERT(Range::compareStart(pendingOutput.front().getStart(), lastOutputAdvance) >= 0);
#endif

            lastOutputAdvance = pendingOutput.back().getStart();

            /* Dump it to the output */
            Util::append(std::move(pendingOutput), output);
            pendingOutput.clear();
            return;
        }

        MergeList toMerge;

        /* Now that we have a merge limit, get the data to merge from all the inputs */
        for (auto in : inputs) {
            switch (outputType) {
            case Invalid:
                Q_ASSERT(false);
                break;
            case Undefined:
                in->prepareOutputUndefined(toMerge);
                break;
            case Defined:
                in->prepareOutput(advanceTime, toMerge);
                break;
            case Ended:
                if (FP::defined(advanceTime)) {
                    in->prepareOutput(advanceTime, toMerge);
                } else {
                    in->prepareOutputUndefined(toMerge);
                }
                break;
            }
        }

        /* See if we need to put anything from the pending in */
        typename Container::iterator pendingMergeEnd;
        if (!pendingOutput.empty()) {
            /* Sort the pending if we need to */
            if (pendingUnsorted) {
                /* If we're going to sort anyway, just throw all the data into one big one */
                if (!toMerge.empty()) {
                    auto target = Util::back_emplacer(pendingOutput);
                    for (const auto &m : toMerge) {
                        std::copy(m.first, m.second, target);
                    }
                    toMerge.clear();
                }

                if (FP::defined(lastOutputAdvance)) {
                    Merge::optimizedSort(pendingOutput.begin(), pendingOutput.end(),
                                         Range::startLessThanDefined<T>);
                } else {
                    Merge::optimizedSort(pendingOutput.begin(), pendingOutput.end(),
                                         Range::startLessThan<T>);
                }
                pendingUnsorted = false;
            } else {
#ifdef MERGE_TIME_DEBUGGING
                verifyTimeSorted(pendingOutput);
#endif
            }

            Q_ASSERT(!pendingUnsorted);

            auto first = pendingOutput.begin();
            auto last = pendingOutput.end();

            /* Get the bound for what we're taking out of the pending */
            if (outputType == Ended) {
                /* All inputs ended, so output as much as possible */
                if (!moreInputsPossible) {
                    pendingMergeEnd = last;

                    double check = pendingOutput.back().getStart();
                    Q_ASSERT(Range::compareStart(lastOutputAdvance, advanceTime) <= 0);
                    if (FP::defined(check)) {
                        if (!FP::defined(advanceTime) || check > advanceTime) {
                            advanceTime = check;
                        }
                    }
                } else if (FP::defined(advanceTime)) {
                    pendingMergeEnd = Range::heuristicUpperBound(first, last, advanceTime);
                } else {
                    pendingMergeEnd = Range::lastUndefinedStart(first, last);
                }
            } else if (FP::defined(lastOutputAdvance)) {
                Q_ASSERT(FP::defined(advanceTime));
                Q_ASSERT(outputType != Undefined);
#ifdef MERGE_TIME_DEBUGGING
                Q_ASSERT(FP::defined(first->getStart()));
#endif
                pendingMergeEnd = Range::heuristicUpperBoundDefined(first, last, advanceTime);
            } else if (outputType == Undefined) {
                pendingMergeEnd = Range::lastUndefinedStart(first, last);
            } else {
                pendingMergeEnd = Range::heuristicUpperBound(first, last, advanceTime);
            }

            /* Now add it to the merge */
            if (pendingMergeEnd != first) {
                /* If this is the only thing to merge and we're taking the whole thing,
                 * just dump it in */
                if (toMerge.empty() && output.empty() && pendingMergeEnd == last) {
                    Util::append(std::move(pendingOutput), output);
                    pendingOutput.clear();
                    pendingMergeEnd = pendingOutput.begin();
                } else {
                    toMerge.emplace_back(std::make_move_iterator(first),
                                         std::make_move_iterator(pendingMergeEnd));
                }
            }
        } else {
            pendingMergeEnd = pendingOutput.begin();
            pendingUnsorted = false;
        }

        Q_ASSERT(!pendingOutput.empty() || !pendingUnsorted);


        /* Do the actual merge */
        switch (outputType) {
        case Invalid:
            Q_ASSERT(false);
            break;
        case Undefined: {
            auto target = Util::back_emplacer(output);
            for (const auto &m : toMerge) {
#ifdef MERGE_TIME_DEBUGGING
                for (auto check = m.first, endC = m.second; check != endC; ++check) {
                    Q_ASSERT(!FP::defined(check->getStart()));
                }
#endif
                std::copy(m.first, m.second, target);
            }
            break;
        }
        case Defined:
            if (!definedTransition) {
                Merge::nWayDefined(toMerge, output);
            } else {
                Merge::nWay(toMerge, output);
            }
            break;
        case Ended:
            Merge::nWay(toMerge, output);
            break;
        }

        /* Complete the merge, removing any ended outputs */
        for (auto in = inputs.begin(); in != inputs.end();) {
            (*in)->completeOutput(advanceTime);

            switch ((*in)->state) {
            case Input::IncomingUndefined:
            case Input::UndefinedPending:
            case Input::AllDefined:
                ++in;
                break;

            case Input::Ended:
                delete *in;
                in = inputs.erase(in);
                break;
            }
        }

        /* Remove anything required from the pending */
        pendingOutput.erase(pendingOutput.begin(), pendingMergeEnd);

        /* Now set the advance time for tracking */
        switch (outputType) {
        case Invalid:
            Q_ASSERT(false);
            break;
        case Undefined:
            Q_ASSERT(!FP::defined(lastOutputAdvance));
            break;
        case Defined:
            Q_ASSERT(Range::compareStart(lastOutputAdvance, advanceTime) <= 0);
            lastOutputAdvance = advanceTime;
            break;
        case Ended:
            Q_ASSERT(Range::compareStart(lastOutputAdvance, advanceTime) <= 0);
            lastOutputAdvance = advanceTime;
            break;
        }
    }

    /**
     * Take the current output of the multiplexer.
     *
     * @tparam Destination  the output list type
     * @return              the output
     */
    template<typename Destination=std::vector<T>>
    Destination output()
    {
        Destination result;
        output(result);
        return std::move(result);
    }

    /**
     * Get the time the multiplexer is currently advanced to.  Any new
     * input will only accept values after or equal to this time.
     *
     * @return      the current advance time
     */
    inline double getCurrentAdvance() const
    { return lastOutputAdvance; }

    /**
     * Get the number of values the multiplexer currently has in the buffer.
     *
     * @return      the number of values
     */
    size_t totalBufferSize() const
    {
        size_t size = pendingOutput.size();
        for (auto in : inputs) {
            size += in->bufferSize();
        }
        return size;
    }

    /**
     * Test if the multiplexer is complete: no more ingresses possible and
     * all output generated.
     *
     * @return      true if the multiplexer is complete
     */
    inline bool isComplete() const
    {
        if (moreInputsPossible)
            return false;
        if (!inputs.empty())
            return false;
        return pendingOutput.empty();
    }

    /**
     * Describe the global state of the multiplexer.
     *
     * @param detailed      output a detailed description
     */
    void describe(QDebug debug, bool detailed = false) const
    {
        QDebugStateSaver saver(debug);
        debug.nospace();
        debug << Logging::time(lastOutputAdvance) << ",";
        if (detailed) {
            debug << "[" << pendingOutput << "]";
        } else {
            debug << pendingOutput.size();
        }
    }

    /**
     * Write the multiplexer state to a serialization stream.  This does
     * not save any inputs.
     *
     * @param stream        the output stream
     */
    void serialize(QDataStream &stream) const
    {
#ifndef NDEBUG
        stream << static_cast<quint32>(inputUID);
#endif

        stream << lastOutputAdvance;
        stream << moreInputsPossible;

        /* For consistency, we any ended inputs that haven't yet been free'd need
         * to be handled somehow, so just dump them all into the global buffer
         * when we deserialize */
        size_t n = 0;
        for (auto in : inputs) {
            n += in->serializeAuxiliarySize();
        }
        if (n != 0) {
            stream << false;
        } else {
            stream << pendingUnsorted;
        }

        n += pendingOutput.size();
        stream << static_cast<quint32>(n);
        for (const auto &v : pendingOutput) {
            stream << v;
        }
        for (auto in : inputs) {
            in->serializeAuxiliary(stream);
        }
    }

    /**
     * Restore the multiplexer state from a serialization stream.  This
     * does not restore any inputs.
     * <br>
     * The external creation flag is used to allow a handler to perform input
     * creation on its own, before doing the complete again, if requried.
     * This value effectively contains the deserialized end state, while the
     * current end state is set to false.  After full deserialization, the
     * multiplexer should have creationComplete() called if it is true
     *
     * @param stream            the input stream
     * @param externalCreation  the external creation flag
     */
    void deserialize(QDataStream &stream, bool *externalCreation = nullptr)
    {
        qDeleteAll(inputs);
        inputs.clear();

#ifndef NDEBUG
        {
            quint32 n = 0;
            stream >> n;
            inputUID = (int) n;
        }
#endif

        stream >> lastOutputAdvance;
        stream >> moreInputsPossible;
        stream >> pendingUnsorted;

        pendingOutput.clear();
        {
            quint32 n = 0;
            stream >> n;
            for (quint32 i = 0; i < n; ++i) {
                T v;
                stream >> v;
                pendingOutput.push_back(std::move(v));
            }
        }

#ifdef MERGE_TIME_DEBUGGING
        if (!pendingUnsorted) {
            verifyTimeSorted(pendingOutput);
        }
#endif

        if (externalCreation) {
            *externalCreation = !moreInputsPossible;
            moreInputsPossible = true;
        }
    }
};

}

#endif //CPD3_MERGE_HXX
