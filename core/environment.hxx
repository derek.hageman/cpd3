/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3COREENVIRONMENT_H
#define CPD3COREENVIRONMENT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include "core/core.hxx"

namespace CPD3 {

/**
 * Provides routines to get information about the environment.
 */
class CPD3CORE_EXPORT Environment : public QObject {
Q_OBJECT

public:
    static QString describe();

    static QString minimal();

    static QString user();

    static quint64 totalMemory();

    static QString revision();

    static QString architecture();
};

}

#endif
