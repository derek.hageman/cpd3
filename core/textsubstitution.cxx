/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <math.h>
#include <QDir>
#include <QRegularExpressionMatch>

#include "core/textsubstitution.hxx"
#include "core/timeutils.hxx"
#include "core/timeparse.hxx"
#include "core/number.hxx"

namespace CPD3 {

TextSubstitution::TextSubstitution() = default;

TextSubstitution::~TextSubstitution() = default;

TextSubstitution::TextSubstitution(const TextSubstitution &) = default;

TextSubstitution &TextSubstitution::operator=(const TextSubstitution &) = default;

QString TextSubstitution::apply(const QString &input) const
{
    QString result(input);
    for (int offset = 0; offset < result.length();) {
        int index = result.indexOf('$', offset);
        if (index == -1)
            break;
        int length = result.length();
        if (index + 1 >= length)
            break;
        if (result.at(index + 1) == '$') {
            result.remove(index + 1, 1);
            offset = index + 1;
            continue;
        }
        if (result.at(index + 1) != '{') {
            offset = index + 1;
            continue;
        }
        if (index + 3 >= length)
            break;
        QStringRef litCheck(result.midRef(index + 2));
        QString litEnd(literalCheck(litCheck));
        int litEndLength = litEnd.length();
        if (litEndLength > 0) {
            int litBeginIndex = litCheck.position();
            int end = result.indexOf(litEnd, litBeginIndex + 1);
            while (end != -1) {
                if (end + litEndLength >= length) {
                    end = -1;
                    break;
                }
                if (result.at(end + litEndLength) == '}')
                    break;
                end = result.indexOf(litEnd, end + 1);
            }
            if (end == -1)
                break;

            QString replacement
                    (literalSubstitution(result.midRef(litBeginIndex, end - litBeginIndex),
                                         litEnd));
            result.replace(index, ((end + 1) + litEndLength) - index, replacement);
            offset = index + replacement.length();
            continue;
        }

        int end = index + 2;
        int elementBegin = index + 2;
        QStringList elements;
        for (; end < length; ++end) {
            QChar ch(result.at(end));
            if (ch == '\\') {
                if (end + 1 > length) {
                    end = -1;
                    break;
                }
                result.remove(end, 1);
                --length;
                continue;
            } else if (ch == '}') {
                break;
            } else if (ch == '|') {
                elements.append(result.mid(elementBegin, end - elementBegin));
                elementBegin = end + 1;
            }
        }
        if (end == -1)
            break;
        elements.append(result.mid(elementBegin, end - elementBegin));

        if (elements.length() == 1 &&
                elements.first().length() == 1 &&
                elements.first().at(0) == '$') {
            result.remove(index + 1, 3);
            offset = index + 1;
            continue;
        }

        QString replacement(substitution(elements));
        result.replace(index, (end + 1) - index, replacement);
        offset = index + replacement.length();
    }
    return result;
}

QString TextSubstitution::literalCheck(QStringRef &key) const
{
    Q_UNUSED(key);
    return QString();
}

QString TextSubstitution::literalSubstitution(const QStringRef &key, const QString &close) const
{
    Q_UNUSED(close);
    return substitution(QStringList(key.toString()));
}

template<typename T>
static void applyModulus(T &value, T lower, T upper)
{
    T range = upper - lower;
    if (range <= 0)
        return;
    if (value < lower) {
        value += (T) ::qAbs((qint64) ::floor((value - lower) / (double) range)) * range;
    }
    if (value >= upper) {
        value -= (T) ::qAbs((qint64) ::floor((value - lower) / (double) range)) * range;
    }
}

template<typename ValueType, typename FromString, typename IsDefined>
static bool standardNumberOperation(const QString &operation,
                                    const QStringList &elements,
                                    int &index,
                                    ValueType &value,
                                    FromString fromString,
                                    IsDefined defined)
{
    if (operation == "scale" || operation == "multiply" || operation == "*" || operation == "x") {
        if (index < elements.size()) {
            ValueType v = fromString(elements.at(index++));
            if (defined(v) && defined(value))
                value *= v;
        }
        return true;
    } else if (operation == "offset" || operation == "add" || operation == "+") {
        if (index < elements.size()) {
            ValueType v = fromString(elements.at(index++));
            if (defined(v) && defined(value))
                value += v;
        }
        return true;
    } else if (operation == "divide") {
        if (index < elements.size()) {
            ValueType v = fromString(elements.at(index++));
            if (defined(v) && defined(value))
                value /= v;
        }
        return true;
    } else if (operation == "denominator") {
        if (index < elements.size()) {
            ValueType v = fromString(elements.at(index++));
            if (defined(v) && defined(value))
                value = v / value;
        }
        return true;
    } else if (operation == "subtract" || operation == "sub" || operation == "-") {
        if (index < elements.size()) {
            ValueType v = fromString(elements.at(index++));
            if (defined(v) && defined(value))
                value -= v;
        }
        return true;
    } else if (operation == "subtractfrom" || operation == "countdown") {
        if (index < elements.size()) {
            ValueType v = fromString(elements.at(index++));
            if (defined(v) && defined(value))
                value = v - value;
        }
        return true;
    } else if (operation == "minimum" || operation == "min") {
        if (index < elements.size()) {
            ValueType v = fromString(elements.at(index++));
            if (defined(v) && defined(value) && v < value)
                value = v;
        }
        return true;
    } else if (operation == "maximum" || operation == "max") {
        if (index < elements.size()) {
            ValueType v = fromString(elements.at(index++));
            if (defined(v) && defined(value) && v > value)
                value = v;
        }
        return true;
    } else if (operation == "modulus" || operation == "mod" || operation == "%") {
        if (index < elements.size()) {
            ValueType v = fromString(elements.at(index++));
            if (defined(v) && defined(value)) {
                applyModulus(value, (ValueType) 0, v);
            }
        }
        return true;
    } else if (operation == "range") {
        if (index + 1 < elements.size()) {
            ValueType l = fromString(elements.at(index++));
            ValueType u = fromString(elements.at(index++));
            if (defined(l) && defined(u) && defined(value)) {
                applyModulus(value, l, u);
            }
        }
        return true;
    }

    return false;
}


static double stringToDouble(const QString &input)
{
    bool ok = false;
    double v = input.toDouble(&ok);
    if (!ok)
        return FP::undefined();
    return v;
}

static qint64 stringToInteger(const QString &input)
{
    bool ok = false;
    qint64 v = input.toLongLong(&ok, 0);
    if (!ok)
        return INTEGER::undefined();
    return v;
}

static QString applyIntegerFormat(qint64 value, const QStringList &elements, int index = 0,
                                  int width = -1)
{
    QString format;
    if (index < elements.size())
        format = elements.at(index++);

    QString mvc;
    if (index < elements.size()) {
        mvc = elements.at(index++);
        if (!INTEGER::defined(value))
            return mvc;
    }

    QChar fill('0');
    if (index < elements.size()) {
        QString check(elements.at(index++));
        if (!check.isEmpty())
            fill = check.at(0);
        else
            fill = QChar();
    }

    QStringList operations;
    if (index < elements.size())
        operations = elements.at(index++).toLower().split(QRegExp("[,;:]+"));

    for (QStringList::const_iterator operation = operations.constBegin(),
            endOperations = operations.constEnd(); operation != endOperations; ++operation) {
        if (standardNumberOperation(*operation, elements, index, value, stringToInteger,
                                    INTEGER::defined))
            continue;
        if (*operation == "and" ||
                *operation == "bitand" ||
                *operation == "bitwiseand" ||
                *operation == "&") {
            if (index < elements.size()) {
                qint64 v = stringToInteger(elements.at(index++));
                if (INTEGER::defined(v) && INTEGER::defined(value))
                    value &= v;
            }
        } else if (*operation == "or" || *operation == "bitor" || *operation == "bitwiseor") {
            if (index < elements.size()) {
                qint64 v = stringToInteger(elements.at(index++));
                if (INTEGER::defined(v) && INTEGER::defined(value))
                    value |= v;
            }
        } else if (*operation == "xor" ||
                *operation == "bitxor" ||
                *operation == "bitwisexor" ||
                *operation == "^") {
            if (index < elements.size()) {
                qint64 v = stringToInteger(elements.at(index++));
                if (INTEGER::defined(v) && INTEGER::defined(value))
                    value ^= v;
            }
        } else if (*operation == "mod" || *operation == "modulus" || *operation == "%") {
            if (index < elements.size()) {
                qint64 v = stringToInteger(elements.at(index++));
                if (INTEGER::defined(v) && INTEGER::defined(value) && v > 0)
                    value %= v;
            }
        } else if (*operation == "mask") {
            if (index < elements.size()) {
                qint64 v = stringToInteger(elements.at(index++));
                if (INTEGER::defined(v) && INTEGER::defined(value))
                    value &= ~v;
            }
        } else if (*operation == "lshift" || *operation == "leftshift" || *operation == "<<") {
            if (index < elements.size()) {
                qint64 v = stringToInteger(elements.at(index++));
                if (INTEGER::defined(v) && INTEGER::defined(value))
                    value <<= v;
            }
        } else if (*operation == "rshift" || *operation == "rightshift" || *operation == ">>") {
            if (index < elements.size()) {
                qint64 v = stringToInteger(elements.at(index++));
                if (INTEGER::defined(v) && INTEGER::defined(value))
                    value >>= v;
            }
        } else if (*operation == "clz") {
            if (INTEGER::defined(value))
                value = (qint64) INTEGER::clz((quint64) value);
        } else if (*operation == "rotr") {
            if (index < elements.size()) {
                qint64 v = stringToInteger(elements.at(index++));
                if (INTEGER::defined(v) && INTEGER::defined(value))
                    value = INTEGER::rotr(value, v);
            }
        } else if (*operation == "rotl") {
            if (index < elements.size()) {
                qint64 v = stringToInteger(elements.at(index++));
                if (INTEGER::defined(v) && INTEGER::defined(value))
                    value = INTEGER::rotl(value, v);
            }
        } else if (*operation == "xorshift" || *operation == "random" || *operation == "rand") {
            if (INTEGER::defined(value)) {
                value = (qint64) Random::xorshift64((quint64) value);
                if (!INTEGER::defined(value))
                    value = 1;
            }
        } else if (*operation == "hex") {
            if (!format.isEmpty()) {
                NumberFormat fmt(format);
                fmt.setMode(NumberFormat::Hex);
                return fmt.apply(value);
            }
        } else if (*operation == "hexupper") {
            if (!format.isEmpty()) {
                NumberFormat fmt(format);
                fmt.setMode(NumberFormat::Hex);
                fmt.setHexUppercase(true);
                return fmt.apply(value);
            }
        } else if (*operation == "hexlower") {
            if (!format.isEmpty()) {
                NumberFormat fmt(format);
                fmt.setMode(NumberFormat::Hex);
                fmt.setHexUppercase(false);
                return fmt.apply(value);
            }
        } else if (*operation == "octal") {
            if (!format.isEmpty()) {
                NumberFormat fmt(format);
                fmt.setMode(NumberFormat::Octal);
                return fmt.apply(value);
            }
        } else if (*operation == "super" || *operation == "superscript") {
            if (!format.isEmpty())
                return NumberFormat(format).superscript(value, fill);
        } else if (*operation == "subscript") {
            if (!format.isEmpty())
                return NumberFormat(format).subscript(value, fill);
        } else if (*operation == "clipped") {
            if (!format.isEmpty())
                return NumberFormat(format).applyClipped(value, fill);
        }
    }

    if (!format.isEmpty())
        return NumberFormat(format).apply(value, fill);

    if (!INTEGER::defined(value))
        return mvc;

    QString result(QString::number(value));
    if (width > 0)
        result = result.rightJustified(width, fill);
    return result;
}

static QString applyDoubleFormat(double value,
                                 const QStringList &elements,
                                 int index = 0,
                                 int decimals = 3,
                                 int width = -1)
{
    QString format;
    if (index < elements.size())
        format = elements.at(index++);

    QString mvc;
    if (index < elements.size()) {
        mvc = elements.at(index++);
        if (!FP::defined(value))
            return mvc;
    }

    QChar fill('0');
    if (index < elements.size()) {
        QString check(elements.at(index++));
        if (!check.isEmpty())
            fill = check.at(0);
        else
            fill = QChar();
    }

    QStringList operations;
    if (index < elements.size())
        operations = elements.at(index++).toLower().split(QRegExp("[,;:]+"));

    for (QStringList::const_iterator operation = operations.constBegin(),
            endOperations = operations.constEnd(); operation != endOperations; ++operation) {
        if (standardNumberOperation(*operation, elements, index, value, stringToDouble,
                                    FP::defined))
            continue;
        if (*operation == "inverse") {
            if (FP::defined(value) && value != 0.0)
                value = 1.0 / value;
        } else if (*operation == "poly" ||
                *operation == "polynomial" ||
                *operation == "cal" ||
                *operation == "calibrate") {
            if (index < elements.size()) {
                QStringList raw(elements.at(index++).split(QRegExp("[,;:]+")));
                QVector<double> coefficients;
                bool ok = true;
                for (QStringList::const_iterator add = raw.constBegin(), endAdd = raw.constEnd();
                        add != endAdd;
                        ++add) {
                    double v = stringToDouble(*add);
                    if (!FP::defined(v)) {
                        ok = false;
                        break;
                    }
                    coefficients.append(v);
                }
                if (ok && !coefficients.isEmpty())
                    value = Calibration(coefficients).apply(value);
            }
        } else if (*operation == "invpoly" ||
                *operation == "inversepolynomial" ||
                *operation == "revcal" ||
                *operation == "reversecalibrate") {
            if (index < elements.size()) {
                QStringList raw(elements.at(index++).split(QRegExp("[,;:]+")));
                QVector<double> coefficients;
                bool ok = true;
                for (QStringList::const_iterator add = raw.constBegin(), endAdd = raw.constEnd();
                        add != endAdd;
                        ++add) {
                    double v = stringToDouble(*add);
                    if (!FP::defined(v)) {
                        ok = false;
                        break;
                    }
                    coefficients.append(v);
                }
                if (ok && !coefficients.isEmpty())
                    value = Calibration(coefficients).inverse(value);
            }
        } else if (*operation == "super" || *operation == "superscript") {
            if (!format.isEmpty())
                return NumberFormat(format).superscript(value, fill);
        } else if (*operation == "subscript") {
            if (!format.isEmpty())
                return NumberFormat(format).subscript(value, fill);
        } else if (*operation == "clipped") {
            if (!format.isEmpty())
                return NumberFormat(format).applyClipped(value, fill);
        }
    }

    if (index < elements.size()) {
        QString check(elements.at(index++));
        bool ok = false;
        double scale = check.toDouble(&ok);
        if (ok && FP::defined(scale))
            value *= scale;
    }

    if (!format.isEmpty())
        return NumberFormat(format).apply(value, fill);

    if (!FP::defined(value))
        return mvc;

    QString result;
    if (decimals > 0)
        result = NumberFormat(1, decimals).apply(value, QChar());
    else
        result = QString::number(value);
    if (width > 0)
        result = result.rightJustified(width, fill);
    return result;
}

static double toFractionalYear(double time)
{
    int year = Time::toDateTime(time).date().year();
    QDateTime dt(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC);
    double yearStart = Time::fromDateTime(dt);
    dt = dt.addYears(1);
    double yearEnd = Time::fromDateTime(dt);
    return (time - yearStart) / (yearEnd - yearStart) + (double) year;
}

QString TextSubstitution::formatTime(double time, const QStringList &elements, bool delimited)
{
    int index = 0;
    QString type;
    if (index < elements.size())
        type = elements.at(index++).toLower();

    if (type == "year") {
        if (!FP::defined(time))
            return applyIntegerFormat(INTEGER::undefined(), elements, index, 4);
        return applyIntegerFormat(Time::toDateTime(time).date().year(), elements, index, 4);
    } else if (type == "shortyear") {
        if (!FP::defined(time))
            return applyIntegerFormat(INTEGER::undefined(), elements, index, 2);
        return applyIntegerFormat(Time::toDateTime(time).date().year() % 100, elements, index, 2);
    } else if (type == "month" || type == "mon") {
        if (!FP::defined(time))
            return applyIntegerFormat(INTEGER::undefined(), elements, index, 2);
        return applyIntegerFormat(Time::toDateTime(time).date().month(), elements, index, 2);
    } else if (type == "day" || type == "mday") {
        if (!FP::defined(time))
            return applyIntegerFormat(INTEGER::undefined(), elements, index, 2);
        return applyIntegerFormat(Time::toDateTime(time).date().day(), elements, index, 2);
    } else if (type == "hour") {
        if (!FP::defined(time))
            return applyIntegerFormat(INTEGER::undefined(), elements, index, 2);
        return applyIntegerFormat(Time::toDateTime(time).time().hour(), elements, index, 2);
    } else if (type == "minute") {
        if (!FP::defined(time))
            return applyIntegerFormat(INTEGER::undefined(), elements, index, 2);
        return applyIntegerFormat(Time::toDateTime(time).time().minute(), elements, index, 2);
    } else if (type == "second") {
        if (!FP::defined(time))
            return applyIntegerFormat(INTEGER::undefined(), elements, index, 2);
        return applyIntegerFormat(Time::toDateTime(time).time().second(), elements, index, 2);
    } else if (type == "millisecond" || type == "msec") {
        if (!FP::defined(time))
            return applyIntegerFormat(INTEGER::undefined(), elements, index, 3);
        return applyIntegerFormat(Time::toDateTime(time, true).time().msec(), elements, index, 3);
    } else if (type == "doy") {
        if (!FP::defined(time))
            return applyDoubleFormat(FP::undefined(), elements, index, 5, 9);
        int year = Time::toDateTime(time).date().year();
        double yearStart = Time::fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));
        double doy = (time - yearStart) / 86400.0 + 1.0;
        return applyDoubleFormat(doy, elements, index, 5, 9);
    } else if (type == "fyear" || type == "fractionalyear") {
        if (!FP::defined(time))
            return applyDoubleFormat(FP::undefined(), elements, index, 3, 8);
        return applyDoubleFormat(toFractionalYear(time), elements, index, 3, 8);
    } else if (type == "format") {
        QString format;
        if (index < elements.size())
            format = elements.at(index++);

        if (!FP::defined(time)) {
            QString mvc;
            if (index < elements.size())
                mvc = elements.at(index++);
            return mvc;
        }

        if (!format.isEmpty()) {
            return Time::toDateTime(time, true).toString(format);
        }
    } else if (type == "iso" || type == "strict") {
        QString mvc;
        if (index < elements.size())
            mvc = elements.at(index++);
        if (!FP::defined(time))
            return mvc;

        return Time::toISO8601(time);
    } else if (type == "nd" || type == "nodelimiters") {
        QString mvc;
        if (index < elements.size())
            mvc = elements.at(index++);
        if (!FP::defined(time))
            return mvc;

        return Time::toISO8601ND(time);
    }

    QString mvc;
    if (index < elements.size())
        mvc = elements.at(index++);
    if (!FP::defined(time))
        return mvc;

    if (delimited)
        return Time::toISO8601(time);
    else
        return Time::toISO8601ND(time);
}

QString TextSubstitution::formatDouble(double value, const QStringList &elements)
{
    return applyDoubleFormat(value, elements);
}

QString TextSubstitution::formatInteger(qint64 value, const QStringList &elements)
{
    return applyIntegerFormat(value, elements);
}

QString TextSubstitution::formatString(const QString &value,
                                       const QStringList &elements,
                                       bool defined)
{
    int index = 0;
    QStringList operations;
    if (index < elements.size())
        operations = elements.at(index++).toLower().split(QRegExp("[,;:]+"));

    QString mvc;
    if (index < elements.size())
        mvc = elements.at(index++);
    if (!defined)
        return mvc;

    QString result(value);
    for (QStringList::const_iterator operation = operations.constBegin(),
            endOperations = operations.constEnd(); operation != endOperations; ++operation) {
        if (*operation == "lc" || *operation == "lower" || *operation == "lowercase") {
            result = result.toLower();
        } else if (*operation == "uc" || *operation == "upper" || *operation == "uppercase") {
            result = result.toUpper();
        } else if (*operation == "quote") {
            QString quote('\"');
            if (index < elements.size())
                quote = elements.at(index++);
            QString trigger;
            if (index < elements.size())
                trigger = elements.at(index++);
            if (!result.isEmpty() && (trigger.isEmpty() || result.contains(QRegExp(trigger)))) {
                result.replace(quote, quote.repeated(3));
                result = quote + result + quote;
            }
        } else if (*operation == "trim" || *operation == "trimmed") {
            result = result.trimmed();
        } else if (*operation == "simplified") {
            result = result.simplified();
        } else if (*operation == "truncate") {
            int length = 0;
            if (index < elements.size()) {
                bool ok = false;
                length = elements.at(index++).toInt(&ok);
                if (!ok)
                    length = 0;
            }
            result.truncate(length);
        } else if (*operation == "mid" || *operation == "substring") {
            int position = -1;
            if (index < elements.size()) {
                bool ok = false;
                position = elements.at(index++).toInt(&ok);
                if (!ok)
                    position = 0;
            }
            int n = -1;
            if (index < elements.size()) {
                bool ok = false;
                n = elements.at(index++).toInt(&ok);
                if (!ok)
                    n = -1;
            }
            result = result.mid(position, n);
        } else if (*operation == "right" || *operation == "rightjustified") {
            int length = 0;
            if (index < elements.size()) {
                bool ok = false;
                length = elements.at(index++).toInt(&ok);
                if (!ok)
                    length = 0;
            }
            QChar fill(' ');
            if (index < elements.size()) {
                QString check(elements.at(index++));
                if (!check.isEmpty())
                    fill = check.at(0);
            }
            result = result.rightJustified(length, fill);
        } else if (*operation == "left" || *operation == "leftjustified") {
            int length = 0;
            if (index < elements.size()) {
                bool ok = false;
                length = elements.at(index++).toInt(&ok);
                if (!ok)
                    length = 0;
            }
            QChar fill(' ');
            if (index < elements.size()) {
                QString check(elements.at(index++));
                if (!check.isEmpty())
                    fill = check.at(0);
            }
            result = result.leftJustified(length, fill);
        } else if (*operation == "double" || *operation == "real" || *operation == "number") {
            return formatDouble(stringToDouble(result), elements.mid(index));
        } else if (*operation == "integer" || *operation == "int") {
            return formatInteger(stringToInteger(result), elements.mid(index));
        } else if (*operation == "time") {
            double time;
            try {
                time = TimeParse::parseTime(result);
            } catch (TimeParsingException tpe) {
                time = FP::undefined();
            }
            return formatTime(time, elements);
        } else if (*operation == "empty" || *operation == "mvc") {
            if (result.isEmpty())
                result = mvc;
        } else if (*operation == "regexp" ||
                *operation == "regularexpression" ||
                *operation == "s") {
            QString pattern;
            if (index < elements.size())
                pattern = elements.at(index++);

            QString replacement;
            if (index < elements.size())
                replacement = elements.at(index++);

            QString flags;
            if (index < elements.size())
                flags = elements.at(index++);

            if (pattern.isEmpty()) {
                if (flags.contains('r', Qt::CaseInsensitive))
                    return mvc;
                continue;
            }

            QRegExp re(pattern, flags.contains('i', Qt::CaseInsensitive) ? Qt::CaseInsensitive
                                                                         : Qt::CaseSensitive);
            if (!re.isValid()) {
                if (flags.contains('r', Qt::CaseInsensitive))
                    return mvc;
                continue;
            }

            int idx = re.indexIn(result);
            if (idx < 0) {
                if (flags.contains('r', Qt::CaseInsensitive))
                    return mvc;
                continue;
            }

            bool globalMatch = flags.contains('g', Qt::CaseInsensitive);
            do {
                QString output(NumberedSubstitution(re).apply(replacement));
                result.replace(idx, re.matchedLength(), output);
                if (!globalMatch)
                    break;
                idx = re.indexIn(result, idx + output.length());
            } while (idx >= 0);
        }
    }
    return result;
}

static QString applyCountdown(double duration, const QStringList &elements, int index = 0)
{
    Q_UNUSED(elements);
    Q_UNUSED(index);

    QString result;
    if (duration < 0.0) {
        result.append('-');
        duration = -duration;
    }
    if (duration > 5999.0)
        duration = 5999.0;
    result.append(QString::number((qint64) ::floor(duration / 60.0)).rightJustified(2, '0'));
    result.append(':');
    result.append(QString::number((qint64) ::floor(duration) % 60).rightJustified(2, '0'));
    return result;
}

static QString applyDurationFormat(double duration, const QStringList &elements, int index = 0)
{
    QByteArray output("%1");
    if (index < elements.size()) {
        QString type(elements.at(index++).toLower());
        QByteArray units("second(s)");

        if (type.startsWith("auto", Qt::CaseInsensitive)) {
            if (fabs(duration) > 604800.0) {
                units = "week(s)";
                duration /= 604800.0;
            } else if (fabs(duration) > 86400.0) {
                units = "day(s)";
                duration /= 86400.0;
            } else if (fabs(duration) > 3600.0) {
                units = "hour(s)";
                duration /= 3600.0;
            } else if (fabs(duration) > 60.0) {
                units = "minute(s)";
                duration /= 60.0;
            } else if (fabs(duration) < 1.0) {
                units = "millisecond(s)";
                duration *= 1000.0;
            }
        } else if (type.startsWith("week", Qt::CaseInsensitive)) {
            units = "week(s)";
            duration /= 604800.0;
        } else if (type.startsWith("day", Qt::CaseInsensitive)) {
            units = "day(s)";
            duration /= 86400.0;
        } else if (type.startsWith("hour", Qt::CaseInsensitive)) {
            units = "hour(s)";
            duration /= 3600.0;
        } else if (type.startsWith("minute", Qt::CaseInsensitive)) {
            units = "minute(s)";
            duration /= 60.0;
        } else if (type.startsWith("millisecond", Qt::CaseInsensitive)) {
            units = "millisecond(s)";
            duration *= 1000.0;
        } else if (type == "countdown") {
            return applyCountdown(duration, elements, index);
        }

        if (type.endsWith("unit", Qt::CaseInsensitive) ||
                type.endsWith("units", Qt::CaseInsensitive)) {
            output.append(' ');
            output.append(units);
        }
    }

    NumberFormat format("0");
    if (index < elements.size())
        format = NumberFormat(elements.at(index++));

    QChar fill;
    if (index < elements.size()) {
        QString check(elements.at(index++));
        if (!check.isEmpty())
            fill = check.at(0);
    }
    return QObject::tr(output.constData(), "output duration format", (int) qRound(duration)).arg(
            format.apply(duration, fill));
}

QString TextSubstitution::formatDuration(double duration,
                                         const QStringList &elements,
                                         const QString &relativePositive,
                                         const QString &relativeNegative)
{
    int index = 0;
    QString type;
    if (index < elements.size())
        type = elements.at(index++).toLower();

    QString mvc;
    if (index < elements.size())
        mvc = elements.at(index++);
    if (!FP::defined(duration))
        return mvc;

    if (type == "absolute") {
        return applyDurationFormat(::fabs(duration), elements, index);
    } else if (type == "signed") {
        return applyDurationFormat(duration, elements, index);
    } else if (type == "countdown") {
        return applyCountdown(duration, elements, index);
    }

    if (duration < 0.0) {
        return relativeNegative.arg(applyDurationFormat(-duration, elements, index));
    } else {
        return relativePositive.arg(applyDurationFormat(duration, elements, index));
    }
}


NumberedSubstitution::NumberedSubstitution(const QRegExp &expression) : replacements(
        expression.capturedTexts())
{ }

NumberedSubstitution::NumberedSubstitution(const QRegularExpressionMatch &matched) : replacements(
        matched.capturedTexts())
{ }

NumberedSubstitution::NumberedSubstitution(const QStringList &rep) : replacements(rep)
{ }

NumberedSubstitution::~NumberedSubstitution() = default;

NumberedSubstitution::NumberedSubstitution(const NumberedSubstitution &) = default;

NumberedSubstitution &NumberedSubstitution::operator=(const NumberedSubstitution &) = default;

NumberedSubstitution::NumberedSubstitution(NumberedSubstitution &&) = default;

NumberedSubstitution &NumberedSubstitution::operator=(NumberedSubstitution &&) = default;

QString NumberedSubstitution::substitution(const QStringList &elements) const
{
    bool ok = false;
    int index = elements.first().toInt(&ok);
    if (!ok || index < 0 || index >= replacements.size())
        return formatString(QString(), elements.mid(1), false);
    return formatString(replacements.at(index), elements.mid(1));
}


TextSubstitutionStack::TextSubstitutionStack() : replacements()
{ replacements.emplace_back(); }

TextSubstitutionStack::~TextSubstitutionStack() = default;

TextSubstitutionStack::TextSubstitutionStack(const TextSubstitutionStack &) = default;

TextSubstitutionStack &TextSubstitutionStack::operator=(const TextSubstitutionStack &) = default;

TextSubstitutionStack::TextSubstitutionStack(TextSubstitutionStack &&) = default;

TextSubstitutionStack &TextSubstitutionStack::operator=(TextSubstitutionStack &&) = default;

void TextSubstitutionStack::push()
{
    replacements.push_back(replacements.back());
}

void TextSubstitutionStack::pop()
{
    Q_ASSERT(replacements.size() > 1);
    replacements.pop_back();
}

void TextSubstitutionStack::clear()
{
    replacements.clear();
    replacements.emplace_back();
}

namespace {
class StringReplacer : public TextSubstitutionStack::Replacer {
    QString value;
    bool exact;
public:
    StringReplacer(const QString &v, bool e) : value(v), exact(e)
    { }

    virtual ~StringReplacer()
    { }

    virtual QString get(const QStringList &elements) const
    {
        if (exact)
            return value;
        return TextSubstitutionStack::formatString(value, elements);
    }
};

class DoubleReplacer : public TextSubstitutionStack::Replacer {
    double value;
public:
    DoubleReplacer(double v) : value(v)
    { }

    virtual ~DoubleReplacer()
    { }

    virtual QString get(const QStringList &elements) const
    { return TextSubstitutionStack::formatDouble(value, elements); }
};

class IntegerReplacer : public TextSubstitutionStack::Replacer {
    qint64 value;
public:
    IntegerReplacer(qint64 v) : value(v)
    { }

    virtual ~IntegerReplacer()
    { }

    virtual QString get(const QStringList &elements) const
    { return TextSubstitutionStack::formatInteger(value, elements); }
};

class TimeReplacer : public TextSubstitutionStack::Replacer {
    double time;
    bool delimited;
public:
    TimeReplacer(double t, bool d) : time(t), delimited(d)
    { }

    virtual ~TimeReplacer()
    { }

    virtual QString get(const QStringList &elements) const
    { return TextSubstitutionStack::formatTime(time, elements, delimited); }
};

class DurationReplacer : public TextSubstitutionStack::Replacer {
    double duration;
    QString relativePositive;
    QString relativeNegative;
public:
    DurationReplacer(double d, const QString &rp, const QString &rn) : duration(d),
                                                                       relativePositive(rp),
                                                                       relativeNegative(rn)
    { }

    virtual ~DurationReplacer()
    { }

    virtual QString get(const QStringList &elements) const
    {
        return TextSubstitutionStack::formatDuration(duration, elements, relativePositive,
                                                     relativeNegative);
    }
};

class FileReplacer : public TextSubstitutionStack::Replacer {
    QFileInfo info;
    QString original;

    static QString apply(const QStringList &elements,
                         const QFileInfo &info,
                         const QString &original = QString())
    {
        int index = 0;
        QString operation;
        if (index < elements.size())
            operation = elements.at(index++).toLower();

        if (operation == "filename" || operation == "file") {
            return info.fileName();
        } else if (operation == "canonical") {
            return info.canonicalFilePath();
        } else if (operation == "path") {
            return info.filePath();
        } else if (operation == "canonicalpath") {
            return info.canonicalPath();
        } else if (operation == "subfile" || operation == "lookup" || operation == "within") {
            QString name;
            if (index < elements.size())
                name = elements.at(index++);
            return apply(elements.mid(index), QFileInfo(info.dir().filePath(name)));
        } else if (operation == "modified") {
            QDateTime dt(info.lastModified().toUTC());
            double time = FP::undefined();
            if (dt.isValid())
                time = Time::fromDateTime(dt);
            return TextSubstitutionStack::formatTime(time, elements.mid(index));
        } else if (operation == "size") {
            if (!info.exists())
                return applyIntegerFormat(INTEGER::undefined(), elements, index);
            return applyIntegerFormat(info.size(), elements, index);
        }

        if (!original.isEmpty())
            return original;
        return info.absoluteFilePath();
    }

public:
    FileReplacer(const QFileInfo &i) : info(i), original()
    { }

    FileReplacer(const QString &f) : info(f), original(f)
    { }

    virtual ~FileReplacer()
    { }

    virtual QString get(const QStringList &elements) const
    { return apply(elements, info, original); }
};
}

void TextSubstitutionStack::setString(const QString &key, const QString &value, bool exact)
{
    setReplacement(key, new StringReplacer(value, exact));
}

void TextSubstitutionStack::setDouble(const QString &key, double value)
{
    setReplacement(key, new DoubleReplacer(value));
}

void TextSubstitutionStack::setInteger(const QString &key, qint64 value)
{
    setReplacement(key, new IntegerReplacer(value));
}

void TextSubstitutionStack::setTime(const QString &key, double value, bool delimited)
{
    setReplacement(key, new TimeReplacer(value, delimited));
}

void TextSubstitutionStack::setDuration(const QString &key,
                                        double value,
                                        const QString &relativePositive,
                                        const QString &relativeNegative)
{
    setReplacement(key, new DurationReplacer(value, relativePositive, relativeNegative));
}

void TextSubstitutionStack::setFile(const QString &key, const QFileInfo &value)
{
    setReplacement(key, new FileReplacer(value));
}

void TextSubstitutionStack::setFile(const QString &key, const QString &value)
{
    setReplacement(key, new FileReplacer(value));
}

QString TextSubstitutionStack::substitution(const QStringList &elements) const
{
    auto check = replacements.back().find(elements.first().toLower());
    if (check != replacements.back().end())
        return check->second->get(elements.mid(1));
    return QString();
}

void TextSubstitutionStack::setReplacement(const QString &key, Replacer *replacer)
{
    replacements.back()[key.toLower()] = ReplacerPointer(replacer);
}

void TextSubstitutionStack::setString(const QHash<QString, QString> &values, bool exact)
{
    for (QHash<QString, QString>::const_iterator add = values.constBegin(),
            endAdd = values.constEnd(); add != endAdd; ++add) {
        setString(add.key(), add.value(), exact);
    }
}

QHash<QString, QString> TextSubstitutionStack::evalulateAll(const QStringList &elements) const
{
    QHash<QString, QString> result;
    for (const auto &add : replacements.back()) {
        result.insert(add.first, add.second->get(elements));
    }
    return result;
}


TextSubstitutionStack::Replacer::Replacer() = default;

TextSubstitutionStack::Replacer::~Replacer() = default;

TextSubstitutionStack::Context::Context(TextSubstitutionStack &substitutions) : substitutions(
        substitutions)
{ substitutions.push(); }

TextSubstitutionStack::Context::~Context()
{ substitutions.pop(); }

}
