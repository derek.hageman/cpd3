/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3CORECOMPRESSION_H
#define CPD3CORECOMPRESSION_H

#include "core/first.hxx"

#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QIODevice>
#include <QTimer>

#include "core/core.hxx"
#include "util.hxx"

namespace CPD3 {

/* Compression is in core instead of algorithms because it's needed
 * for the archive, and data (containing the archive) is needed by
 * algorithms itself. */

/**
 * A general wrapper for a compressor.
 */
class CPD3CORE_EXPORT Compressor {
public:
    virtual ~Compressor();

    /**
     * Compress data.  This will resize the output to contain the compressed
     * data.
     * 
     * @param input         the data to compress
     * @param output        the output compressed data
     * @param maximumSize   the maximum size before aborting compression
     * @return              true on success or false on failure (incompressible data, etc)
     */
    virtual bool compress(const Util::ByteView &input,
                          Util::ByteArray &output,
                          std::size_t maximumSize = 0) const = 0;

    bool compress(const QByteArray &input, QByteArray &output, std::size_t maximumSize = 0) const;

    /**
     * Compress data but skip some number of bytes after the output buffer 
     * start.  This will resize the output to contain the compressed
     * data and the seeked size.
     * 
     * @param input         the data to compress
     * @param output        the output compressed data
     * @param skip          the number of bytes to reserve at the start of the output
     * @param maximumSize   the maximum size before aborting compression (of compressed data only, this does not include the skip)
     * @return              true on success or false on failure (incompressible data, etc)
     */
    virtual bool compressSkip(const Util::ByteView &input,
                              Util::ByteArray &output,
                              std::size_t skip,
                              std::size_t maximumSize = 0) const;

    virtual bool compressSkip(const QByteArray &input,
                              QByteArray &output,
                              std::size_t skip,
                              std::size_t maximumSize = 0) const;
};

/**
 * A general wrapper for a compressor.
 */
class CPD3CORE_EXPORT Decompressor {
public:
    virtual ~Decompressor();

    /**
     * Decompress data.  This will resize the output to contain the decompressed
     * data.
     * 
     * @param input         the data to decompress
     * @param output        the output decompressed data
     * @param outputSize    the known output size (this is NOT approximate), or zero for unknown
     * @return              true on success or false on failure
     */
    virtual bool decompress(const Util::ByteView &input,
                            Util::ByteArray &output,
                            std::size_t outputSize = 0) const = 0;

    bool decompress(const QByteArray &input, QByteArray &output, std::size_t outputSize = 0) const;
};


/**
 * A simple wrapper around Qt's built in ZLIB compressor.
 */
class CPD3CORE_EXPORT CompressorZLIB : public Compressor {
    int level;
public:
    explicit CompressorZLIB(int level = -1);

    virtual ~CompressorZLIB();

    virtual bool compress(const Util::ByteView &input,
                          Util::ByteArray &output,
                          std::size_t maximumSize = 0) const;

    using Compressor::compress;
};

/**
 * A simple wrapper around Qt's built in ZLIB decompressor.
 */
class CPD3CORE_EXPORT DecompressorZLIB : public Decompressor {
public:
    DecompressorZLIB();

    virtual ~DecompressorZLIB();

    virtual bool decompress(const Util::ByteView &input,
                            Util::ByteArray &output,
                            std::size_t outputSize = 0) const;

    using Decompressor::decompress;
};


/**
 * A general wrapper around compressors that handles compressing a block
 * of data.  This includes handling for incompressible data and encoding
 * the compression type used.
 */
class CPD3CORE_EXPORT BlockCompressor {
    std::unique_ptr<Compressor> backend;
    std::uint8_t decompressorID;
    bool includeUncompressedSize;
public:
    /** The preferred type of compression */
    enum PreferredCompressionType {
        /** No compression */
                None,

        /** LZ4 */
                LZ4,

        /** LZ4 High compression mode */
                LZ4HC,

        /** ZLIB */
                ZLIB,
    };

    BlockCompressor(PreferredCompressionType compression = LZ4);

    ~BlockCompressor();

    /**
     * Apply the compressor to a block of data.
     * 
     * @param input     the input data
     * @param output    the output buffer
     */
    void compress(const Util::ByteView &input, Util::ByteArray &output) const;

    /**
     * Apply the compressor to a block of data.
     * 
     * @param input     the input data
     * @return          the compressed block
     */
    Util::ByteArray compress(const Util::ByteView &input) const;
};

/**
 * A general wrapper around decompressors that handles decompressing a block
 * of data.  This automatically selects the same compressor used by the
 * compressor that generated the block.
 * 
 * @see BlockCompressor
 */
class CPD3CORE_EXPORT BlockDecompressor {
    std::unique_ptr<Decompressor> backend;
    std::uint8_t lastDecompressorID;
    bool hasUncompressedSize;
public:
    BlockDecompressor();

    ~BlockDecompressor();

    /**
     * Decompress a block of data.
     * 
     * @param input     the compressed block
     * @param output    the decompressed result
     * @return          true on success
     */
    bool decompress(const Util::ByteView &input, Util::ByteArray &output) const;

    /**
     * Decompress a block of data.
     * 
     * @param input     the compressed block
     * @param output    the decompressed result
     * @return          true on success
     */
    bool decompress(const Util::ByteView &input, Util::ByteArray &output);

    /**
     * Apply the decompressor to a block of data.
     * 
     * @param input     the compressed block
     * @param ok        set to true if not NULL on success
     * @return          the decompressed data
     */
    Util::ByteArray decompress(const Util::ByteView &input, bool *ok = NULL) const;

    /**
     * Apply the decompressor to a block of data.
     * 
     * @param input     the compressed block
     * @param ok        set to true if not NULL on success
     * @return          the decompressed data
     */
    Util::ByteArray decompress(const Util::ByteView &input, bool *ok = NULL);
};


/**
 * A general compressed stream of data.  This implements real time compression
 * wrapping around another QIODevice.  Generally, this is used on network
 * sockets to reduce the volume of data transmitted.
 */
class CPD3CORE_EXPORT CompressedStream : public QIODevice {
Q_OBJECT

public:
    /** The preferred type of compression */
    enum PreferredCompressionType {
        /** No compression */
                None,

        /** LZ4 */
                LZ4,

        /** LZ4 High compression mode */
                LZ4HC,

        /** ZLIB */
                ZLIB,
    };
private:

    /**
     * This property holds the maximum real time in milliseconds to wait
     * before compressing existing data and sending it to the backend.  Setting
     * this to zero will write data the next time control returns to the event
     * loop while setting it to -1 will wait forever (until a flush or
     * close).  The default is 100ms.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setMaximumBufferTime(int)
     *  <li> int getMaximumBufferTime() const
     * </ul>
     */
    Q_PROPERTY(int maximumBufferTime
                       READ getMaximumBufferTime
                       WRITE
                       setMaximumBufferTime)
    int maximumBufferTime;

    /**
     * This property holds the maximum size the buffer is allowed to grow to
     * before writing is done immediately.  The allowed range is 1 byte to 
     * 1 GiB.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setMaximumBufferSize(int)
     *  <li> int getMaximumBufferSize() const
     * </ul>
     */
    Q_PROPERTY(int maximumBufferSize
                       READ getMaximumBufferSize
                       WRITE
                       setMaximumBufferSize)
    int maximumBufferSize;

    /**
     * This property holds the maximum amount of data allowed to be read and
     * decompressed ahead of the actual buffer being emptied.  The default
     * is 16MiB.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setReadAheadSize(int)
     *  <li> int getReadAheadSize() const
     * </ul>
     */
    Q_PROPERTY(int readAheadSize
                       READ getReadAheadSize
                       WRITE
                       setReadAheadSize)
    int readAheadSize;

    /**
     * This property holds the preferred compression mode to use when writing
     * data.  The default is LZ4.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setPreferredCompression(PreferredCompressionType)
     *  <li> PreferredCompressionType getPreferredCompression() const
     * </ul>
     */
    Q_PROPERTY(PreferredCompressionType preferredCompression
                       READ getPreferredCompression
                       WRITE
                       setPreferredCompression)
    PreferredCompressionType preferredCompression;

    QTimer flushCompressionTimer;

    QIODevice *backend;
    bool hadWriteError;
    bool hadReadError;

    Util::ByteArray outstandingCompress;
    Util::ByteArray outstandingWrite;

    Util::ByteArray readBuffer;
    Util::ByteArray outstandingRead;

    std::uint8_t writeDecompressorID;
    std::uint8_t lastDecompressorID;
    std::unique_ptr<Compressor> compressorBackend;
    std::unique_ptr<Decompressor> decompressorBackend;

    void createCompressorBackend();

    void initializeBackend();

    void compressNOOP(const Util::ByteView &input);

    void compressBlock(const Util::ByteView &input);

    bool shouldReadMore() const;

    void decompressAllAvailable();

    qint64 readToBlock();

public:
    /**
     * Create a new compression stream from the given backend.  If the backend
     * is already open then the compression stream is also opened with the
     * same mode.
     * 
     * @param setBackend    the backend IO device to wrap
     * @param parent        the parent
     */
    CompressedStream(QIODevice *setBackend, QObject *parent = 0);

    virtual ~CompressedStream();

    void setMaximumBufferTime(int s);

    int getMaximumBufferTime() const;

    void setMaximumBufferSize(int s);

    int getMaximumBufferSize() const;

    void setReadAheadSize(int s);

    int getReadAheadSize() const;

    void setPreferredCompression(PreferredCompressionType s);

    PreferredCompressionType getPreferredCompression() const;

    virtual bool atEnd() const;

    virtual qint64 bytesAvailable() const;

    virtual qint64 bytesToWrite() const;

    virtual void close();

    void abort();

    virtual bool isSequential() const;

    virtual bool open(QIODevice::OpenMode mode);

    virtual bool waitForBytesWritten(int msecs);

    virtual bool waitForReadyRead(int msecs);

    /**
     * Get the backend device used by the compression wrapper.
     * 
     * @return  the underlying device
     */
    QIODevice *getBackendDevice() const;

public slots:

    /**
     * Flush all outstanding data to be compressed to the backend device.
     * This does not force the device to write, it just makes sure that
     * the compression layer has no data buffered waiting for compression and
     * that the compressed data has been handed off to the device.
     * 
     * @param msecs the maximum time to wait, or -1 to wait forever
     * @return      true on success
     */
    bool flush(int msecs = -1);

    /**
     * Drain the backend of all readable bytes until it returns atEnd().
     */
    void drainBackendRead();

private slots:

    bool compressAllPending();

    bool writePending();

    void readMore();

    void completeReadChannel();

protected:
    virtual qint64 readData(char *data, qint64 maxSize);

    virtual qint64 writeData(const char *data, qint64 maxSize);
};

}


#endif
