/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3CORESEARCH_H
#define CPD3CORESEARCH_H

#include "core/first.hxx"

#include <functional>
#include <algorithm>
#include <QtGlobal>
#include <QtAlgorithms>

#include "core/core.hxx"
#include "core/number.hxx"

namespace CPD3 {

/**
 * Routines for simple searching.
 */
class CPD3CORE_EXPORT Search {
public:

    /**
     * A lower bound search that takes an initial guess to expand outward from.
     * <br>
     * This returns the first value less than or equal to the given one or the
     * position it should be inserted.
     * 
     * @param begin     the start of the search range
     * @param end       the end of the search range
     * @param guess     the initial guess
     * @param value     the value to search for
     * @param lessThan  the comparison functional object
     */
    template<typename RandomAccessItertor, typename T, typename LessThan>
    static RandomAccessItertor heuristicLowerBound(RandomAccessItertor begin,
                                                   RandomAccessItertor end,
                                                   RandomAccessItertor guess,
                                                   const T &value,
                                                   LessThan lessThan)
    {
        if (guess == end)
            return std::lower_bound(begin, end, value, lessThan);
        if (lessThan(*guess, value)) {
            begin = guess;

            RandomAccessItertor middle;
            int n = (int) (end - begin);
            int half = 1;
            while (half < n) {
                middle = begin + half;
                if (!lessThan(*middle, value))
                    return std::lower_bound(guess, middle, value, lessThan);
                half <<= 1;
                guess = middle;
            }
            return std::lower_bound(guess, end, value, lessThan);
        } else {
            end = guess;

            RandomAccessItertor middle;
            int n = (int) (end - begin);
            int half = 2;
            while (half <= n) {
                middle = end - half;
                if (lessThan(*middle, value))
                    return std::lower_bound(middle, guess, value, lessThan);
                half <<= 1;
                guess = middle;
            }
            return std::lower_bound(begin, guess, value, lessThan);
        }
    }

    /**
     * A lower bound search that takes an initial guess to expand outward from.
     * <br>
     * This returns the first value less than or equal to the given one or the
     * position it should be inserted.
     * 
     * @param begin     the start of the search range
     * @param end       the end of the search range
     * @param guess     the initial guess
     * @param value     the value to search for
     */
    template<typename RandomAccessItertor, typename T>
    static inline RandomAccessItertor heuristicLowerBound(RandomAccessItertor begin,
                                                          RandomAccessItertor end,
                                                          RandomAccessItertor guess,
                                                          const T &value)
    { return heuristicLowerBound(begin, end, guess, value, std::less<T>()); }

};

}


#endif
