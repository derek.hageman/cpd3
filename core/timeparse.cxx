/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QRegExp>
#include <QDate>

#include "core/timeparse.hxx"
#include "core/number.hxx"

namespace CPD3 {

/** @file core/timeparse.hxx
 * Provides general time parsing routines.  These routines include
 * utilities to convert from human readable to time in an undefined format
 * to a Unix epoch time.
 */

TimeParsingException::TimeParsingException(const QString &d)
{
    description = d;
}

QString TimeParsingException::getDescription() const
{
    return description;
}

double TimeReference::getReferenceImplied() noexcept(false)
{
    return getReference();
}

double TimeReference::getUndefinedValue() noexcept(false)
{
    throw TimeParsingException(TimeParse::tr("Undefined/infinite bounds are not valid."));
}

TimeReference::~TimeReference()
{ }

static int matchDate(const QString &time, QRegExp &match)
{
    match =
            QRegExp("^\\s*(\\d{4})(\\d{2})(\\d{2})\\s*Z?\\s*(?:([+-])\\s*(\\d{2})(?:[:]?(\\d{2}))?)?\\s*",
                    Qt::CaseInsensitive);
    int m = match.indexIn(time);
    if (m > -1) {
        int year = match.cap(1).toInt();
        if (year >= 1900 && year <= 2999) {
            int month = match.cap(2).toInt();
            if (month >= 1 && month <= 12) {
                int mday = match.cap(3).toInt();
                if (mday >= 1 && mday <= 31) {
                    /* If it doesn't completely match require a T separator
                     * before a time. */
                    int len = match.matchedLength();
                    if (len < time.length()) {
                        if (QRegExp("\\s*T", Qt::CaseInsensitive).indexIn(time, m) == len)
                            return m;
                    } else {
                        return m;
                    }
                }
            }
        }
    }

    match =
            QRegExp("^\\s*(\\d{4})(?:[/\\-](\\d{1,2})(?:[/\\-](\\d{1,2}))?)?\\s*Z?\\s*(?:([+-])\\s*(\\d{2})(?:[:]?(\\d{2}))?)?\\s*",
                    Qt::CaseInsensitive);
    return match.indexIn(time);
}

static int matchTime(const QString &time,
                     QRegExp &match,
                     int offset = 0,
                     bool requireSeparator = false)
{
    if (requireSeparator) {
        match =
                QRegExp("\\s*T\\s*(\\d{2})(\\d{2})(\\d{2})(?:[\\.,](\\d+))?\\s*Z?\\s*(?:([+-])\\s*(\\d{2})(?:[:]?(\\d{2}))?)?\\s*$",
                        Qt::CaseInsensitive);
    } else {
        match =
                QRegExp("\\s*T?\\s*(\\d{2})(\\d{2})(\\d{2})(?:[\\.,](\\d+))?\\s*Z?\\s*(?:([+-])\\s*(\\d{2})(?:[:]?(\\d{2}))?)?\\s*$",
                        Qt::CaseInsensitive);
    }
    int m = match.indexIn(time, offset);
    if (m > -1) {
        int hour = match.cap(1).toInt();
        if (hour >= 0 && hour <= 23) {
            int minute = match.cap(2).toInt();
            if (minute >= 0 && minute <= 59) {
                int second = match.cap(3).toInt();
                if (second >= 0 && second <= 60) {
                    return m;
                }
            }
        }
    }

    match =
            QRegExp("\\s*T?\\s*(\\d{1,2})[:](\\d{1,2})(?:[:](\\d{1,2})(?:[\\.,](\\d+))?)?\\s*Z?\\s*(?:([+-])\\s*(\\d{2})(?:[:]?(\\d{2}))?)?\\s*$",
                    Qt::CaseInsensitive);
    return match.indexIn(time, offset);
}

static int matchYearDOY(const QString &time, QRegExp &match)
{
    match = QRegExp("^\\s*(\\d{4})[-]?(\\d{3})(?:[\\.,](\\d+))\\s*$", Qt::CaseInsensitive);
    int m = match.indexIn(time);
    if (m > -1) {
        int year = match.cap(1).toInt();
        if (year >= 1900 && year <= 2999) {
            int doy = match.cap(2).toInt();
            if (doy >= 1 && doy <= 366) {
                return m;
            }
        }
    }

    match = QRegExp("^\\s*(?:(\\d{4})(?:[:;,]|\\s+))?(\\d{1,3})(?:[\\.,](\\d+))?\\s*$",
                    Qt::CaseInsensitive);
    return match.indexIn(time);
}

static bool validFractionalOffset(double total, int &outputCount, double millisecondMultiplier)
{
    if (total <= INT_MIN) {
        outputCount = INT_MIN;
        return false;
    }
    if (total >= INT_MAX) {
        outputCount = INT_MAX;
        return false;
    }
    outputCount = (int) floor(total + 0.5);
    return (fabs(total - outputCount) * millisecondMultiplier < 1.0);
}

static void interpretFractionalDecimalUnits(const QString &integer,
                                            const QString &decimal,
                                            Time::LogicalTimeUnit baseUnit,
                                            int &outputCount,
                                            Time::LogicalTimeUnit &outputUnit)
noexcept(false)
{
    if (integer.isEmpty() || decimal.isEmpty())
        throw TimeParsingException(TimeParse::tr("Invalid decimal number in fractional offset"));
    bool ok = false;
    double count = (double) integer.toInt(&ok);
    if (!ok)
        throw TimeParsingException(
                TimeParse::tr("Error parsing integer component of fractional offset"));
    if (integer.startsWith('-') || count < 0) {
        count -= decimal.toInt(&ok) / pow(10.0, decimal.length());
    } else {
        count += decimal.toInt(&ok) / pow(10.0, decimal.length());
    }
    if (!ok)
        throw TimeParsingException(TimeParse::tr("Error parsing fractional component of offset"));

    /* Count on the fact that the intervals are all built on one another and
     * fall through. */
    switch (baseUnit) {
    case Time::Week:
        count *= 7.0;
        if (validFractionalOffset(count, outputCount, 604800000.0)) {
            outputUnit = Time::Day;
            return;
        }

    case Time::Day:
        count *= 24.0;
        if (validFractionalOffset(count, outputCount, 86400000.0)) {
            outputUnit = Time::Hour;
            return;
        }

    case Time::Hour:
        count *= 60.0;
        if (validFractionalOffset(count, outputCount, 3600000.0)) {
            outputUnit = Time::Minute;
            return;
        }

    case Time::Minute:
        count *= 60.0;
        if (validFractionalOffset(count, outputCount, 60000.0)) {
            outputUnit = Time::Second;
            return;
        }

    case Time::Second:
        count *= 1000.0;
        if (validFractionalOffset(count, outputCount, 1000.0)) {
            outputUnit = Time::Millisecond;
            return;
        }
        break;

    case Time::Millisecond:
    case Time::Month:
    case Time::Quarter:
    case Time::Year:
        throw TimeParsingException(TimeParse::tr("Cannot offset fractions of the given unit"));
        break;
    }
}

static bool setTimeParseOffsetNoFractional(const QString &pattern,
                                           Time::LogicalTimeUnit unit,
                                           QString &time,
                                           Time::LogicalTimeUnit &offsetUnit,
                                           int &offsetCount) noexcept(false)
{
    QString construct(pattern);
    construct.prepend("\\s*([+-])\\s*(\\d+)(?:[\\.,]0*)?\\s*(?:");
    construct.append(")\\s*$");
    QRegExp matched(construct, Qt::CaseInsensitive);
    if (matched.indexIn(time) > -1) {
        offsetCount = matched.cap(2).toInt();
        offsetUnit = unit;
        if (matched.cap(1) == "-") offsetCount *= -1;
        time.chop(matched.matchedLength());
        return true;
    }

    construct = pattern;
    construct.prepend("\\s*([+-])\\s*(?:");
    construct.append(")\\s*$");
    matched = QRegExp(construct, Qt::CaseInsensitive);
    if (matched.indexIn(time) > -1) {
        offsetCount = 1;
        offsetUnit = unit;
        if (matched.cap(1) == "-") offsetCount *= -1;
        time.chop(matched.matchedLength());
        return true;
    }

    return false;
}

static bool setTimeParseOffsetFractional(const QString &pattern,
                                         Time::LogicalTimeUnit unit,
                                         QString &time,
                                         Time::LogicalTimeUnit &offsetUnit,
                                         int &offsetCount) noexcept(false)
{
    QString construct(pattern);
    construct.prepend("\\s*([+-])\\s*(\\d+)(?:[\\.,](\\d*))?\\s*(?:");
    construct.append(")\\s*$");
    QRegExp matched(construct, Qt::CaseInsensitive);
    if (matched.indexIn(time) > -1) {
        if (matched.pos(3) > 0 && matched.cap(3).length() > 0) {
            if (matched.cap(3).length() > 0 && matched.cap(3).toInt() != 0) {
                interpretFractionalDecimalUnits(matched.cap(2), matched.cap(3), unit, offsetCount,
                                                offsetUnit);
                if (matched.cap(1) == "-") offsetCount *= -1;
                time.chop(matched.matchedLength());
                return true;
            }
        }

        offsetCount = matched.cap(2).toInt();
        offsetUnit = unit;
        if (matched.cap(1) == "-") offsetCount *= -1;
        time.chop(matched.matchedLength());
        return true;
    }

    construct = pattern;
    construct.prepend("\\s*([+-])\\s*(?:");
    construct.append(")\\s*$");
    matched = QRegExp(construct, Qt::CaseInsensitive);
    if (matched.indexIn(time) > -1) {
        offsetCount = 1;
        offsetUnit = unit;
        if (matched.cap(1) == "-") offsetCount *= -1;
        time.chop(matched.matchedLength());
        return true;
    }

    return false;
}

static void setTimeParseOffset(QString &time,
                               Time::LogicalTimeUnit &offsetUnit, int &offsetCount) noexcept(false)
{
    if (setTimeParseOffsetNoFractional("msec|(?:milliseconds?)", Time::Millisecond, time,
                                       offsetUnit, offsetCount))
        return;
    if (setTimeParseOffsetFractional("s|(?:secs?)|(?:seconds?)", Time::Second, time, offsetUnit,
                                     offsetCount))
        return;
    if (setTimeParseOffsetFractional("m|(?:mins?)|(?:minutes?)", Time::Minute, time, offsetUnit,
                                     offsetCount))
        return;
    if (setTimeParseOffsetFractional("h|(?:hours?)", Time::Hour, time, offsetUnit, offsetCount))
        return;
    if (setTimeParseOffsetFractional("d|(?:days?)", Time::Day, time, offsetUnit, offsetCount))
        return;
    if (setTimeParseOffsetFractional("w|(?:weeks?)", Time::Week, time, offsetUnit, offsetCount))
        return;
    if (setTimeParseOffsetNoFractional("mo|(?:mons?)|(?:months?)", Time::Month, time, offsetUnit,
                                       offsetCount))
        return;
    if (setTimeParseOffsetNoFractional("q|(?:qtrs?)|(?:quarters?)", Time::Quarter, time, offsetUnit,
                                       offsetCount))
        return;
    if (setTimeParseOffsetNoFractional("y|(?:years?)", Time::Year, time, offsetUnit, offsetCount))
        return;
}

TimeParse::TimeParse()
{
}

static bool parseOffsetSimpleNoFractional(const QString &pattern,
                                          Time::LogicalTimeUnit parseUnit,
                                          double &result,
                                          const QString &offset,
                                          TimeReference *const reference,
                                          Time::LogicalTimeUnit *const units)
noexcept(false)
{
    QString construct(pattern);
    construct.prepend("^\\s*(\\d+)(?:[\\.,]0*)?\\s*(?:");
    construct.append(")\\s*(a|(?:align(?:ed)?)|na|(?:no\\s*align(?:ed)?))?\\s*$");
    QRegExp matched(construct, Qt::CaseInsensitive);
    if (matched.indexIn(offset) > -1) {
        int add = matched.cap(1).toInt();
        bool aligned = matched.pos(2) > 0 &&
                matched.cap(2).length() > 0 &&
                !matched.cap(2).startsWith('n', Qt::CaseInsensitive);
        if (add < 0 || (add == 0 && !aligned))
            throw TimeParsingException(TimeParse::tr("Invalid offset."));
        if (units != NULL) *units = parseUnit;
        if (reference->isLeading())
            result = Time::logical(reference->getReference(), parseUnit, add * -1, aligned, false);
        else
            result = Time::logical(reference->getReference(), parseUnit, add, aligned, true);
        return true;
    }

    construct = pattern;
    construct.prepend("^\\s*(?:");
    construct.append(")\\s*(a|(?:align(?:ed)?)|na|(?:no\\s*align(?:ed)?))?\\s*$");
    matched = QRegExp(construct, Qt::CaseInsensitive);
    if (matched.indexIn(offset) > -1) {
        bool aligned = matched.pos(1) > 0 &&
                matched.cap(1).length() > 0 &&
                !matched.cap(1).startsWith('n', Qt::CaseInsensitive);
        if (units != NULL) *units = parseUnit;
        if (reference->isLeading())
            result = Time::logical(reference->getReference(), parseUnit, -1, aligned, false);
        else
            result = Time::logical(reference->getReference(), parseUnit, 1, aligned, true);
        return true;
    }

    return false;
}

static bool parseOffsetSimpleFractional(const QString &pattern,
                                        Time::LogicalTimeUnit parseUnit,
                                        double &result,
                                        const QString &offset,
                                        TimeReference *const reference,
                                        Time::LogicalTimeUnit *const units)
noexcept(false)
{
    QString construct(pattern);
    construct.prepend("^\\s*(\\d+)(?:[\\.,](\\d*))?\\s*(?:");
    construct.append(")\\s*(a|(?:align(?:ed)?)|na|(?:no\\s*align(?:ed)?))?\\s*$");
    QRegExp matched(construct, Qt::CaseInsensitive);
    if (matched.indexIn(offset) > -1) {
        if (matched.pos(2) > 0 && matched.cap(2).length() > 0) {
            if (matched.cap(2).length() > 0 && matched.cap(2).toInt() != 0) {
                if (matched.pos(3) > 0 &&
                        matched.cap(3).length() > 0 &&
                        !matched.cap(3).startsWith('n', Qt::CaseInsensitive)) {
                    throw TimeParsingException(
                            TimeParse::tr("Alignment not allowed with fractional offsets."));
                }

                Time::LogicalTimeUnit offsetUnit = parseUnit;
                int offsetCount = 0;
                interpretFractionalDecimalUnits(matched.cap(1), matched.cap(2), parseUnit,
                                                offsetCount, offsetUnit);

                if (offsetCount <= 0)
                    throw TimeParsingException(TimeParse::tr("Invalid offset."));
                if (units != NULL) *units = offsetUnit;
                if (reference->isLeading())
                    result = Time::logical(reference->getReference(), offsetUnit, offsetCount * -1,
                                           false, false);
                else
                    result =
                            Time::logical(reference->getReference(), offsetUnit, offsetCount, false,
                                          true);
                return true;
            }
        }

        int add = matched.cap(1).toInt();
        bool aligned = matched.pos(3) > 0 &&
                matched.cap(3).length() > 0 &&
                !matched.cap(3).startsWith('n', Qt::CaseInsensitive);
        if (add < 0 || (add == 0 && !aligned))
            throw TimeParsingException(TimeParse::tr("Invalid offset."));
        if (units != NULL) *units = parseUnit;
        if (reference->isLeading())
            result = Time::logical(reference->getReference(), parseUnit, add * -1, aligned, false);
        else
            result = Time::logical(reference->getReference(), parseUnit, add, aligned, true);
        return true;
    }

    construct = pattern;
    construct.prepend("^\\s*(?:");
    construct.append(")\\s*(a|(?:align(?:ed)?)|na|(?:no\\s*align(?:ed)?))?\\s*$");
    matched = QRegExp(construct, Qt::CaseInsensitive);
    if (matched.indexIn(offset) > -1) {
        bool aligned = matched.pos(1) > 0 &&
                matched.cap(1).length() > 0 &&
                !matched.cap(1).startsWith('n', Qt::CaseInsensitive);
        if (units != NULL) *units = parseUnit;
        if (reference->isLeading())
            result = Time::logical(reference->getReference(), parseUnit, -1, aligned, false);
        else
            result = Time::logical(reference->getReference(), parseUnit, 1, aligned, true);
        return true;
    }

    return false;
}

/**
 * Parse a time offset from a reference point.  The offset is one of the
 * of the form "X<unit>" optionally followed by "a" or "aligned" to round to
 * the appropriate unit of time or explicitly disabled by with "na" or 
 * "no aligned".  The following units are valid:
 * <ul>
 *  <li> msec or milliseconds
 *  <li> s, sec, or seconds
 *  <li> m, min, or minutes - Exactly X * 60 seconds
 *  <li> h or hours - Exactly X * 3600 seconds
 *  <li> d or days - Exactly X * 86400 seconds
 *  <li> w or weeks - Exactly X * 604800 seconds
 *  <li> mo, mon, or months - Preserves the time of day and day of month
 *  <li> q, qtr, or quarter - Preserves the time after the start of the quarter
 *  <li> y or years - Preserves the month, day of month, and time of day
 * </ul>
 * Fractional numbers of units are allowed only for the exact length intervals,
 * and alignment is not allowed when that is used.  The count may be omitted to
 * specify a single unit.
 * 
 * @param offset        the offset to parse
 * @param reference     the reference point to parse around
 * @param units         the output parsed unit or NULL
 * @return              the time after the offset or FP::undefined() if not 
 *                      a valid offset
 */
double TimeParse::parseOffset(const QString &offset,
                              TimeReference *const reference,
                              Time::LogicalTimeUnit *const units)
noexcept(false)
{
    double result = 0;
    if (parseOffsetSimpleNoFractional("msec|(?:milliseconds?)", Time::Millisecond, result, offset,
                                      reference, units))
        return result;
    if (parseOffsetSimpleFractional("s|(?:secs?)|(?:seconds?)", Time::Second, result, offset,
                                    reference, units))
        return result;
    if (parseOffsetSimpleFractional("m|min|(?:minutes?)", Time::Minute, result, offset, reference,
                                    units))
        return result;
    if (parseOffsetSimpleFractional("h|(?:hours?)", Time::Hour, result, offset, reference, units))
        return result;
    if (parseOffsetSimpleFractional("d|(?:days?)", Time::Day, result, offset, reference, units))
        return result;
    if (parseOffsetSimpleFractional("w|(?:weeks?)", Time::Week, result, offset, reference, units))
        return result;
    if (parseOffsetSimpleNoFractional("mo|(?:mons?)|(?:months?)", Time::Month, result, offset,
                                      reference, units))
        return result;
    if (parseOffsetSimpleNoFractional("q|qtrs?|(?:quarters?)", Time::Quarter, result, offset,
                                      reference, units))
        return result;
    if (parseOffsetSimpleNoFractional("y|(?:years?)", Time::Year, result, offset, reference, units))
        return result;
    return FP::undefined();
}

static bool parseOffsetBreakdownNoFractional(const QString &pattern,
                                             Time::LogicalTimeUnit parseUnit,
                                             const QString &offset,
                                             Time::LogicalTimeUnit *const units,
                                             int *const count,
                                             bool *const aligned,
                                             bool allowNegative)
noexcept(false)
{
    QString construct(pattern);
    construct.prepend("^\\s*(-?\\d+)(?:[\\.,]0*)?\\s*(?:");
    construct.append(")\\s*(a|(?:align(?:ed)?)|na|(?:no\\s*align(?:ed)?))?\\s*$");
    QRegExp matched(construct, Qt::CaseInsensitive);
    if (matched.indexIn(offset) > -1) {
        int add = matched.cap(1).toInt();
        if (count != NULL) *count = add;
        if (aligned != NULL && matched.pos(2) > 0 && matched.cap(2).length() > 0)
            *aligned = !matched.cap(2).startsWith('n', Qt::CaseInsensitive);
        if (units != NULL) *units = parseUnit;
        if ((!allowNegative && add < 0) || (add == 0 && (aligned == NULL || !(*aligned))))
            throw TimeParsingException(TimeParse::tr("Invalid offset."));
        return true;
    }

    construct = pattern;
    construct.prepend("^\\s*(-)?\\s*(?:");
    construct.append(")\\s*(a|(?:align(?:ed)?)|na|(?:no\\s*align(?:ed)?))?\\s*$");
    matched = QRegExp(construct, Qt::CaseInsensitive);
    if (matched.indexIn(offset) > -1) {
        if (matched.pos(1) > 0 && matched.cap(1).length() > 0) {
            if (!allowNegative)
                throw TimeParsingException(TimeParse::tr("Invalid offset."));
            if (count != NULL) *count = -1;
        } else {
            if (count != NULL) *count = 1;
        }
        if (aligned != NULL && matched.pos(2) > 0 && matched.cap(2).length() > 0)
            *aligned = !matched.cap(2).startsWith('n', Qt::CaseInsensitive);
        if (units != NULL) *units = parseUnit;
        return true;
    }

    return false;
}

static bool parseOffsetBreakdownFractional(const QString &pattern,
                                           Time::LogicalTimeUnit parseUnit,
                                           const QString &offset,
                                           Time::LogicalTimeUnit *const units,
                                           int *const count,
                                           bool *const aligned,
                                           bool allowNegative)
noexcept(false)
{
    QString construct(pattern);
    construct.prepend("^\\s*(-?\\d+)(?:[\\.,](\\d*))?\\s*(?:");
    construct.append(")\\s*(a|(?:align(?:ed)?)|na|(?:no\\s*align(?:ed)?))?\\s*$");
    QRegExp matched(construct, Qt::CaseInsensitive);
    if (matched.indexIn(offset) > -1) {
        if (matched.pos(2) > 0 && matched.cap(2).length() > 0) {
            if (matched.cap(2).length() > 0 && matched.cap(2).toInt() != 0) {
                if (matched.pos(3) > 0 &&
                        matched.cap(3).length() > 0 &&
                        !matched.cap(3).startsWith('n', Qt::CaseInsensitive)) {
                    throw TimeParsingException(
                            TimeParse::tr("Alignment not allowed with fractional offsets."));
                }

                Time::LogicalTimeUnit offsetUnit = parseUnit;
                int offsetCount = 0;
                interpretFractionalDecimalUnits(matched.cap(1), matched.cap(2), parseUnit,
                                                offsetCount, offsetUnit);

                if (count != NULL) *count = offsetCount;
                if (aligned != NULL) *aligned = false;
                if (units != NULL) *units = offsetUnit;
                if (offsetCount == 0)
                    throw TimeParsingException(TimeParse::tr("Invalid zero offset specification."));
                if (!allowNegative && offsetCount < 0)
                    throw TimeParsingException(TimeParse::tr("Invalid offset."));
                return true;
            }
        }

        int add = matched.cap(1).toInt();
        if (count != NULL) *count = add;
        if (aligned != NULL && matched.pos(3) > 0 && matched.cap(3).length() > 0)
            *aligned = !matched.cap(3).startsWith('n', Qt::CaseInsensitive);
        if (units != NULL) *units = parseUnit;
        if ((!allowNegative && add < 0) || (add == 0 && (aligned == NULL || !(*aligned))))
            throw TimeParsingException(TimeParse::tr("Invalid offset."));
        return true;
    }

    construct = pattern;
    construct.prepend("^\\s*(-)?\\s*(?:");
    construct.append(")\\s*(a|(?:align(?:ed)?)|na|(?:no\\s*align(?:ed)?))?\\s*$");
    matched = QRegExp(construct, Qt::CaseInsensitive);
    if (matched.indexIn(offset) > -1) {
        if (matched.pos(1) > 0 && matched.cap(1).length() > 0) {
            if (!allowNegative)
                throw TimeParsingException(TimeParse::tr("Invalid offset."));
            if (count != NULL) *count = -1;
        } else {
            if (count != NULL) *count = 1;
        }
        if (aligned != NULL && matched.pos(2) > 0 && matched.cap(2).length() > 0)
            *aligned = !matched.cap(2).startsWith('n', Qt::CaseInsensitive);
        if (units != NULL) *units = parseUnit;
        return true;
    }

    return false;
}

/**
 * Parse a time offset.  
 * 
 * @param offset        the offset to parse
 * @param units         the output parsed unit or NULL
 * @param count         the output parsed count or NULL
 * @param aligned       the output parsed alignment or NULL
 * @param allowZero     true if zero results are allowed
 * @param allowNegative true if negative offsets are allowed
 * @see parseOffset( const QString &, TimeReference * const, 
 * Time::LogicalTimeUnit * const )
 */
void TimeParse::parseOffset(const QString &offset,
                            Time::LogicalTimeUnit *const units,
                            int *const count,
                            bool *const aligned,
                            bool allowZero,
                            bool allowNegative)
noexcept(false)
{
    if (allowZero) {
        QRegExp matched("^\\s*(?:(?:0+(?:[\\.,]0*)?)|none|zero)\\s*$", Qt::CaseInsensitive);
        if (matched.indexIn(offset) > -1) {
            if (count != NULL) *count = 0;
            if (aligned != NULL) *aligned = false;
            if (units != NULL) *units = Time::Second;
            return;
        }
    }

    if (parseOffsetBreakdownNoFractional("msec|(?:milliseconds?)", Time::Millisecond, offset, units,
                                         count, aligned, allowNegative))
        return;
    if (parseOffsetBreakdownFractional("s|(?:secs?)|(?:seconds?)", Time::Second, offset, units,
                                       count, aligned, allowNegative))
        return;
    if (parseOffsetBreakdownFractional("m|min|(?:minutes?)", Time::Minute, offset, units, count,
                                       aligned, allowNegative))
        return;
    if (parseOffsetBreakdownFractional("h|(?:hours?)", Time::Hour, offset, units, count, aligned,
                                       allowNegative))
        return;
    if (parseOffsetBreakdownFractional("d|(?:days?)", Time::Day, offset, units, count, aligned,
                                       allowNegative))
        return;
    if (parseOffsetBreakdownFractional("w|(?:weeks?)", Time::Week, offset, units, count, aligned,
                                       allowNegative))
        return;
    if (parseOffsetBreakdownNoFractional("mo|(?:mons?)|(?:months?)", Time::Month, offset, units,
                                         count, aligned, allowNegative))
        return;
    if (parseOffsetBreakdownNoFractional("q|qtrs?|(?:quarters?)", Time::Quarter, offset, units,
                                         count, aligned, allowNegative))
        return;
    if (parseOffsetBreakdownNoFractional("y|(?:years?)", Time::Year, offset, units, count, aligned,
                                         allowNegative))
        return;

    QRegExp matched("^\\s*(-?\\d+)\\s*(a|(?:align(?:ed)?)|na|(?:no\\s*align(?:ed)?))?\\s*$",
                    Qt::CaseInsensitive);
    if (matched.indexIn(offset) > -1) {
        int add = matched.cap(1).toInt();
        if (count != NULL) *count = add;
        if (aligned != NULL && matched.pos(2) > 0 && matched.cap(2).length() > 0)
            *aligned = !matched.cap(2).startsWith('n', Qt::CaseInsensitive);
        if ((!allowNegative && add < 0) || (add == 0 && (aligned == NULL || !(*aligned))))
            throw TimeParsingException(tr("Invalid second offset."));
        return;
    }

    throw TimeParsingException(tr("Invalid time offset."));
}

static double roundFractionalDOY(int digits, int length, Time::LogicalTimeUnit *const units = NULL)
{
    /* Some common specifications */
    if (length == 2) {
        if (units != NULL) *units = Time::Hour;
        return floor(digits * 0.24 + 0.5) * 3600.0;
    } else if (length == 5) {
        if (units != NULL) *units = Time::Second;
        return floor(((double) digits + 0.5) * 0.864);
    } else {
        return (digits / pow(10.0, length)) * 86400.0;
    }
}

/**
 * Parse a single time specification.  In general this accepts ISO 8601 times
 * and additional extensions and inference as possible.  Accepted formats are:
 * <ul>
 *  <li>now - The current time.
 *  <li>An integer and an offset multiplier - This offsets the time in the
 *      reasonable direction from its bound.  For example if the bound is the
 *      leading edge then the offset is backwards from it.  The form is of 
 *      "X<unit>" where "X" is a non-negative number, and "<unit>" is one of:
 *   <ul>
 *     <li> msec or milliseconds
 *     <li> s, sec, or seconds
 *     <li> m, min, or minutes - Exactly X * 60 seconds
 *     <li> h or hours - Exactly X * 3600 seconds
 *     <li> d or days - Exactly X * 86400 seconds
 *     <li> w or weeks - Exactly X * 604800 seconds
 *     <li> mo, mon, or months - Preserves the time of day and day of month
 *     <li> q, qtr, or quarter - Preserves the time after the start of the quarter
 *     <li> y or years - Preserves the month, day of month, and time of day
 *   </ul>
 *      For example "1w" is one week from the reference point.  This may
 *      optionally be followed by "a" or "aligned" to align to the boundary
 *      the multiplier represents.  For example "0qa" from a leading bound
 *      rounds the time down to the start of the quarter.  Without the alignment
 *      enabled, the count integer must be postive (non-zero).  Alignment may
 *      be explicitly disabled with "na" or "no aligned" suffixes.  In general
 *      the count must be an integer but decimal numbers are accepted for
 *      the exact specifications and are interpreted as the largest whole unit
 *      below the specification that gives the exact time.  For example "1.5d"
 *      is interpreted to mean 36 hours.  The count may also be omitted to 
 *      specify a single of the given unit.  Alignment is not allowed on 
 *      fractional specifications.
 *  <li>Date and/or time - If present the date must come first, it must start 
 *      with a four digit year, followed by an optional month and day of month.
 *      Each field must be separated by either a dash or a slash.  The time
 *      then follows either a space separator or a "T".  Time is specified on
 *      a 24 hour clock, with the hours first, seconds being optional.  Each 
 *      field must be separated by a ":".  The specification may optionally 
 *      end in a "Z".  Some valid examples include:
 *   <ul>
 *    <li> 2010-03-10T00:15:00Z
 *    <li> 2010/03/10 00:15:00
 *    <li> 2010-03-10
 *    <li> 20100310T001500Z
 *    <li> 15:00:12.123Z
 *    <li> 2010
 *   </ul>
 *      If only a time is specified, the date is inferred from the given
 *      reference point such that it would be reasonable relative to it.  For
 *      example, if the reference point is leading and the time is after the
 *      reference time, then it is assumed to be in the prior day.  A date
 *      and/or time may optionally be followed by a time zone offset, this
 *      takes the form of + or - a two or four digit integer or "hh:mm".  This
 *      number is subtracted or added to the given time to give UTC.
 *   <li>Year and week or week - One of the following forms:
 *    <ul>
 *     <li> YYYYwWW
 *     <li> YYYYwWW-D
 *     <li> wWW
 *     <li> wWW-D
 *    </ul>
 *    Note that the "WW" component may be a single digit.  The exact time that
 *    the week specifies depends on the reference point: if the reference point
 *    is leading, then time specified is the start of that week, otherwise it
 *    is the end.  If the year is omitted then it is inferred from the reference
 *    point as with only a time above.
 *   <li>Year and quarter or quarter - One of the following forms:
 *    <ul>
 *     <li> YYYYqQ
 *     <li> qQ
 *    </ul>
 *    The exact time that the quarter specifies depends on the reference point
 *    as above with weeks.
 *   <li>Year and DOY or DOY - A four digit year separated from a DOY by any 
 *       of ":", ";", "," or space(s).  The DOY starts with 1.0 being midnight
 *       January 1st of that year.  The separator may also be omitted or
 *       replaced with "-" if there are exactly three digits in the integer
 *       component of the DOY.  Some examples include:
 *     <ul>
 *      <li> 2010:1
 *      <li> 2010,1.2345
 *      <li> 2010;023.12456
 *      <li> 2010023.1234
 *      <li> 2010-023
 *     </ul>
 *       The year may also be omitted in which case it is inferred from the
 *       reference point.
 *   <li>A fractional year.  This is a number with a decimal component between
 *       1900.0 and 2999.0.  The integer part is interpreted as being the
 *       year in question and the fractional part is the fraction of the year
 *       though it.
 *   <li>An epoch time.  This is a decimal number of seconds since
 *       1900-01-01T00:00:00Z.  This may also be begun with "E:" to 
 *       disambiguate from ISO time strings and fractional years.
 *   <li>An infinite bound.  This results in the value of the reference's
 *       TimeReference::getUndefinedValue()
 *    <ul>
 *     <li> Empty - All space (or zero length)
 *     <li> 0 - The number zero
 *     <li> none
 *     <li> undef or undefined
 *     <li> inf or infinity
 *     <li> all
 *     <li> forever
 *    </ul>
 * </ul>
 * The time format specification may be followed by an offset, this consists of
 * a + or -, an integer, and a unit.  The integer number of the given time
 * unit are added or subtracted to the time specified before.  The following
 * time units are valid:
 * <ul>
 *  <li> msec or milliseconds
 *  <li> s, sec, or seconds
 *  <li> m, min, or minutes - Exactly X * 60 seconds
 *  <li> h or hours - Exactly X * 3600 seconds
 *  <li> d or days - Exactly X * 86400 seconds
 *  <li> w or weeks - Exactly X * 604800 seconds
 *  <li> mo, mon, or months - Preserves the time of day and day of month
 *  <li> q, qtr, or quarter - Preserves the time after the start of the quarter
 *  <li> y or years - Preserves the time of day and month and time of day
 * </ul>
 * For example "2010-03-10T00:15:00Z+5d" results in "2010-03-15T00:15:00Z".
 * 
 * <br>
 * NOTE: When the alignment would overflow a date field it is set to the largest
 * valid one in the alignment.  So one month up from Mar 31 would be on Apr 30.
 * 
 * @param time              the time string
 * @param reference         the reference point for parsing
 * @param units             the output natural units or NULL
 * @return                  the parsed time
 * @see TimeParse::parseOffset( const QString &, TimeReference * const,
        Time::LogicalTimeUnit * const )
 */
double TimeParse::parseTime(QString time,
                            TimeReference *const reference,
                            Time::LogicalTimeUnit *const units) noexcept(false)
{
    int offsetCount = 0;
    Time::LogicalTimeUnit offsetUnit = Time::Second;

    QRegExp matched("^\\s*(?:(?:0+)|none|(?:undef(?:ined)?)|(?:inf(?:inity)?)|all|forever)?\\s*$",
                    Qt::CaseInsensitive);
    if (matched.indexIn(time) > -1) {
        return reference->getUndefinedValue();
    }

    setTimeParseOffset(time, offsetUnit, offsetCount);

    matched = QRegExp("^\\s*now\\s*$", Qt::CaseInsensitive);
    if (matched.indexIn(time) > -1) {
        if (units != NULL) *units = Time::Week;
        return Time::logical(Time::time(), offsetUnit, offsetCount);
    }

    {
        double relativeTime = parseOffset(time, reference, units);
        if (FP::defined(relativeTime))
            return Time::logical(relativeTime, offsetUnit, offsetCount);
    }

    matched =
            QRegExp("^\\s*(\\d{4})?\\s*w\\s*(\\d{1,2})(?:[-d+:](\\d))?\\s*$", Qt::CaseInsensitive);
    if (matched.indexIn(time) > -1) {
        if (units != NULL) *units = Time::Week;

        int week = matched.cap(2).toInt();
        if (week < 1 || week > 53)
            throw TimeParsingException(tr("Week is out of range."));
        int weekDay = -1;
        if (matched.pos(3) > 0) {
            weekDay = matched.cap(3).toInt();
            if (weekDay < 1 || weekDay > 7)
                throw TimeParsingException(tr("Day of week is out of range."));
        }

        if (matched.pos(1) > -1 && matched.cap(1).length() > 0) {
            int year = matched.cap(1).toInt();
            if (year < 1900 || year > 2999)
                throw TimeParsingException(tr("Year is out of range."));
            if (reference->isLeading()) {
                if (weekDay == -1)
                    return Time::logical(Time::weekStart(year, week), offsetUnit, offsetCount);
                return Time::logical(Time::weekStart(year, week) + (weekDay - 1) * 86400.0,
                                     offsetUnit, offsetCount);
            } else {
                if (weekDay == -1)
                    return Time::logical(Time::weekStart(year, week) + 604800.0, offsetUnit,
                                         offsetCount);
                return Time::logical(Time::weekStart(year, week) + weekDay * 86400.0, offsetUnit,
                                     offsetCount);
            }
        } else {
            double refT = reference->getReferenceImplied();
            int year = Time::toDateTime(refT).date().year();
            if (reference->isLeading()) {
                double out = Time::weekStart(year, week);
                if (weekDay == -1) {
                    if (out >= refT)
                        return Time::logical(Time::weekStart(year - 1, week), offsetUnit,
                                             offsetCount);
                    return Time::logical(out, offsetUnit, offsetCount);
                } else {
                    out += (weekDay - 1) * 86400.0;
                    if (out >= refT)
                        return Time::logical(
                                Time::weekStart(year - 1, week) + (weekDay - 1) * 86400.0,
                                offsetUnit, offsetCount);
                    return Time::logical(out, offsetUnit, offsetCount);
                }
            } else {
                if (weekDay == -1) {
                    double out = Time::weekStart(year, week) + 604800.0;
                    if (out <= refT)
                        return Time::logical(Time::weekStart(year + 1, week) + 604800.0, offsetUnit,
                                             offsetCount);
                    return Time::logical(out, offsetUnit, offsetCount);
                } else {
                    double out = Time::weekStart(year, week) + (weekDay + 1) * 86400.0;
                    if (out <= refT)
                        return Time::logical(Time::weekStart(year + 1, week) + weekDay * 86400.0,
                                             offsetUnit, offsetCount);
                    return Time::logical(out, offsetUnit, offsetCount);
                }
            }
        }
    }

    matched = QRegExp("^\\s*(\\d{4})?\\s*Q\\s*(\\d)\\s*$", Qt::CaseInsensitive);
    if (matched.indexIn(time) > -1) {
        if (units != NULL) *units = Time::Quarter;

        int qtr = matched.cap(2).toInt();
        if (qtr < 1 || qtr > 4)
            throw TimeParsingException(tr("Quarter is out of range."));

        if (matched.pos(1) > -1 && matched.cap(1).length() > 0) {
            int year = matched.cap(1).toInt();
            if (year < 1900 || year > 2999)
                throw TimeParsingException(tr("Year is out of range."));
            if (reference->isLeading()) {
                return Time::logical(Time::quarterStart(year, qtr), offsetUnit, offsetCount);
            } else {
                return Time::logical(Time::quarterEnd(year, qtr), offsetUnit, offsetCount);
            }
        } else {
            double refT = reference->getReferenceImplied();
            int year = Time::toDateTime(refT).date().year();
            if (reference->isLeading()) {
                double out = Time::quarterStart(year, qtr);
                if (out >= refT)
                    return Time::logical(Time::quarterStart(year - 1, qtr), offsetUnit,
                                         offsetCount);
                return Time::logical(out, offsetUnit, offsetCount);
            } else {
                double out = Time::quarterEnd(year, qtr);
                if (out <= refT)
                    return Time::logical(Time::quarterEnd(year + 1, qtr), offsetUnit, offsetCount);
                return Time::logical(out, offsetUnit, offsetCount);
            }
        }
    }

    if (matchYearDOY(time, matched) > -1) {
        if (units != NULL) *units = Time::Day;

        int doy = matched.cap(2).toInt();
        int fdoy = 0;
        int fdoyDigits = 0;
        if (matched.pos(3) > -1 && matched.cap(3).length() > 0) {
            QString f = matched.cap(3);
            fdoy = f.toInt();
            fdoyDigits = f.length();
        }

        if (doy < 1 || doy > 366)
            throw TimeParsingException(tr("Day of year is out of range."));

        double base = (double) (doy - 1) * 86400.0;
        if (fdoyDigits != 0) {
            base += roundFractionalDOY(fdoy, fdoyDigits, units);
        }

        if (matched.pos(1) > -1 && matched.cap(1).length() > 0) {
            int year = matched.cap(1).toInt();
            if (year < 1900 || year > 2999)
                throw TimeParsingException(tr("Year is out of range."));
            base += Time::fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));

            return Time::logical(base, offsetUnit, offsetCount);
        } else {
            double refT = reference->getReferenceImplied();
            int year = Time::toDateTime(refT).date().year();
            double out = base +
                    Time::fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));

            if (reference->isLeading()) {
                if (out < refT)
                    return Time::logical(out, offsetUnit, offsetCount);
                base += Time::fromDateTime(
                        QDateTime(QDate(year - 1, 1, 1), QTime(0, 0, 0), Qt::UTC));
                return Time::logical(base, offsetUnit, offsetCount);
            } else {
                if (out > refT)
                    return Time::logical(out, offsetUnit, offsetCount);
                base += Time::fromDateTime(
                        QDateTime(QDate(year + 1, 1, 1), QTime(0, 0, 0), Qt::UTC));
                return Time::logical(base, offsetUnit, offsetCount);
            }
        }
    }

    if (matchDate(time, matched) > -1) {
        int len = matched.matchedLength();
        QRegExp timeMatched;
        if (len >= time.length() || matchTime(time, timeMatched, len, true) == len) {
            if (units != NULL) *units = Time::Year;

            int year = matched.cap(1).toInt();
            int month = 1;
            int mday = 1;
            double tzOffset = 0.0;
            bool hadTZ = false;
            if (matched.pos(2) > 0) {
                if (units != NULL) *units = Time::Month;
                month = matched.cap(2).toInt();
                if (matched.pos(3) > 0) {
                    if (units != NULL) *units = Time::Day;
                    mday = matched.cap(3).toInt();
                }
            }
            if (matched.pos(4) > 0) {
                hadTZ = true;
                int tzHour = matched.cap(5).toInt();
                if (tzHour < 0 || tzHour > 23)
                    throw TimeParsingException(tr("Time zone hour is out of range."));
                tzOffset = 3600.0 * tzHour;
                if (matched.pos(6) > 0) {
                    int tzMinute = matched.cap(6).toInt();
                    if (tzMinute < 0 || tzMinute > 59)
                        throw TimeParsingException(tr("Time zone minute is out of range."));
                    tzOffset += 60.0 * tzMinute;
                }
                if (matched.cap(4) == "+")
                    tzOffset *= -1;
            }

            if (year < 1900 || year > 2999)
                throw TimeParsingException(tr("Year is out of range."));
            if (month < 1 || month > 12)
                throw TimeParsingException(tr("Month is out of range."));

            QDate date(year, month, 1);
            if (mday < 1 || mday > date.daysInMonth())
                throw TimeParsingException(tr("Day of month is out of range."));
            double base = Time::fromDateTime(
                    QDateTime(QDate(year, month, mday), QTime(0, 0, 0), Qt::UTC));

            if (len >= time.length()) {
                return Time::logical(base + tzOffset, offsetUnit, offsetCount);
            }

            if (units != NULL) *units = Time::Minute;

            if (hadTZ)
                throw TimeParsingException(tr("Time zone specified with date in datetime."));

            int hour = timeMatched.cap(1).toInt();
            if (hour < 0 || hour > 23)
                throw TimeParsingException(tr("Hour is out of range."));
            int minute = timeMatched.cap(2).toInt();
            if (minute < 0 || minute > 59)
                throw TimeParsingException(tr("Minute is out of range."));
            double second = 0;
            if (timeMatched.pos(3) > 0) {
                if (units != NULL) *units = Time::Second;
                second = timeMatched.cap(3).toInt();
                if (timeMatched.pos(4) > 0) {
                    if (units != NULL) *units = Time::Millisecond;
                    QString fsec = timeMatched.cap(4);
                    second += fsec.toInt() / pow(10.0, fsec.length());
                }
            }
            if (second < 0.0 || second > 60.0) /* Leap second */
                throw TimeParsingException(tr("Second is out of range."));

            if (timeMatched.pos(5) > 0) {
                int tzHour = timeMatched.cap(6).toInt();
                if (tzHour < 0 || tzHour > 23)
                    throw TimeParsingException(tr("Time zone hour is out of range."));
                tzOffset = 3600.0 * tzHour;
                if (timeMatched.pos(7) > 0) {
                    int tzMinute = timeMatched.cap(7).toInt();
                    if (tzMinute < 0 || tzMinute > 59)
                        throw TimeParsingException(tr("Time zone minute is out of range."));
                    tzOffset += 60.0 * tzMinute;
                }
                if (timeMatched.cap(5) == "+")
                    tzOffset *= -1;
            }

            return Time::logical(base + tzOffset + hour * 3600 + minute * 60 + second, offsetUnit,
                                 offsetCount);
        }
    }

    if (matchTime(time, matched) == 0) {
        if (units != NULL) *units = Time::Minute;
        int hour = matched.cap(1).toInt();
        if (hour < 0 || hour > 23)
            throw TimeParsingException(tr("Hour is out of range."));
        int minute = matched.cap(2).toInt();
        if (minute < 0 || minute > 59)
            throw TimeParsingException(tr("Minute is out of range."));
        double second = 0;
        if (matched.pos(3) > 0) {
            if (units != NULL) *units = Time::Second;
            second = matched.cap(3).toInt();
            if (matched.pos(4) > 0) {
                if (units != NULL) *units = Time::Millisecond;
                QString fsec = matched.cap(4);
                second += fsec.toInt() / pow(10.0, fsec.length());
            }
        }
        if (second < 0.0 || second > 60.0) /* Leap second */
            throw TimeParsingException(tr("Second is out of range."));

        double tzOffset = 0.0;
        if (matched.pos(5) > 0) {
            int tzHour = matched.cap(6).toInt();
            if (tzHour < 0 || tzHour > 23)
                throw TimeParsingException(tr("Time zone hour is out of range."));
            tzOffset = 3600.0 * tzHour;
            if (matched.pos(7) > 0) {
                int tzMinute = matched.cap(7).toInt();
                if (tzMinute < 0 || tzMinute > 59)
                    throw TimeParsingException(tr("Time zone minute is out of range."));
                tzOffset += 60.0 * tzMinute;
            }
            if (matched.cap(5) == "+")
                tzOffset *= -1;
        }

        second += hour * 3600 + minute * 60;
        double refT = reference->getReference();
        second += Time::fromDateTime(
                QDateTime(Time::toDateTime(refT).date(), QTime(0, 0, 0), Qt::UTC));
        second += tzOffset;
        if (reference->isLeading()) {
            if (second >= refT)
                return Time::logical(second - 86400.0, offsetUnit, offsetCount);
            return Time::logical(second, offsetUnit, offsetCount);
        } else {
            if (second <= refT)
                return Time::logical(second + 86400.0, offsetUnit, offsetCount);
            return Time::logical(second, offsetUnit, offsetCount);
        }
    }

    matched = QRegExp("^\\s*(\\d{4})[\\.,](\\d+)\\s*$", Qt::CaseInsensitive);
    if (matched.indexIn(time) > -1) {
        if (units != NULL) *units = Time::Year;
        int year = matched.cap(1).toInt();
        if (year < 1900 || year > 2999)
            throw TimeParsingException(
                    tr("Fractional year out of range or wrongly interpreting an epoch time."));
        QString fyearStr = matched.cap(2);
        double fyear = fyearStr.toInt() / pow(10.0, fyearStr.length());
        double start = Time::fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));
        double end = Time::fromDateTime(QDateTime(QDate(year + 1, 1, 1), QTime(0, 0, 0), Qt::UTC));
        return Time::logical((end - start) * fyear + start, offsetUnit, offsetCount);
    }

    matched = QRegExp("^\\s*(E:?\\s*)?(\\d+(?:[\\.,]\\d+)?)\\s*$", Qt::CaseInsensitive);
    if (matched.indexIn(time) > -1) {
        if (units != NULL) *units = Time::Second;
        double epoch = matched.cap(2).toDouble();
        if (epoch == 0.0)
            return reference->getUndefinedValue();
        if (epoch <= 1.0)
            throw TimeParsingException(tr("Epoch time is out of range."));
        if (matched.pos(1) < 0 || matched.cap(1).length() < 1) {
            if (epoch <= 29991231.0)
                throw TimeParsingException(tr("Epoch time is too similar to a date."));
        }
        if (!FP::defined(epoch))
            throw TimeParsingException(tr("Resulting epoch time was non-fininite."));
        return Time::logical(epoch, offsetUnit, offsetCount);
    }

    throw TimeParsingException(tr("Unrecognized time format."));
}

namespace {

class ReferenceDefaultLeading : public TimeReference {
private:
    bool allowUndefined;
    double ref;
public:
    ReferenceDefaultLeading()
    {
        allowUndefined = false;
        ref = Time::time();
    }

    ReferenceDefaultLeading(const bool allowUndefined)
    {
        this->allowUndefined = allowUndefined;
        ref = Time::time();
    }

    ReferenceDefaultLeading(const bool allowUndefined, const double ref)
    {
        this->allowUndefined = allowUndefined;
        this->ref = ref;
    }

    ReferenceDefaultLeading(const ReferenceDefaultLeading &c) : TimeReference()
    {
        allowUndefined = c.allowUndefined;
        ref = c.ref;
    }

    virtual double getReference() noexcept(false)
    {
        return ref;
    }

    virtual bool isLeading() noexcept(false)
    {
        return true;
    }

    virtual double getUndefinedValue() noexcept(false)
    {
        if (allowUndefined)
            return FP::undefined();
        throw TimeParsingException(TimeParse::tr("Undefined/infinite bounds are not valid."));
    }
};

class ReferenceDefaultTrailing : public TimeReference {
private:
    bool allowUndefined;
    double ref;
public:
    ReferenceDefaultTrailing()
    {
        allowUndefined = false;
        ref = Time::time();
    }

    ReferenceDefaultTrailing(const bool allowUndefined)
    {
        this->allowUndefined = allowUndefined;
        ref = Time::time();
    }

    ReferenceDefaultTrailing(const bool allowUndefined, const double ref)
    {
        this->allowUndefined = allowUndefined;
        this->ref = ref;
    }

    ReferenceDefaultTrailing(const ReferenceDefaultTrailing &c) : TimeReference()
    {
        allowUndefined = c.allowUndefined;
        ref = c.ref;
    }

    virtual double getReference() noexcept(false)
    {
        return ref;
    }

    virtual bool isLeading() noexcept(false)
    {
        return false;
    }

    virtual double getUndefinedValue() noexcept(false)
    {
        if (allowUndefined)
            return FP::undefined();
        throw TimeParsingException(TimeParse::tr("Undefined/infinite bounds are not valid."));
    }
};

}

/**
 * Parse a time relative to the current time.  The current time is treated as
 * the leading bound, so relative offsets go backwards in time.
 * 
 * @param time              the time string
 * @param units             the output natural units or NULL
 * @return                  the parsed time
 * @see  TimeParse::parseTime(QString, TimeReference * const,  Time::LogicalTimeUnit * const)
 */
double TimeParse::parseTime(QString time, Time::LogicalTimeUnit *const units)
noexcept(false)
{
    ReferenceDefaultLeading ref;
    return parseTime(time, &ref, units);
}

TimeParseListHandler::~TimeParseListHandler()
{ }

/**
 * Handle a list of strings that contains a single time specifier in the
 * middle.  It calls the appropriate handler methods on the start and end of
 * the list until it isolates what should be the time specification.  If the
 * result is a single item in the list then TimeParse::parseTime(QString, TimeReference * const,  Time::LogicalTimeUnit * const)
 * is called on it.  If set to allow for zero length time specifications and 
 * either the start or end consumption methods take the entire list then
 * the results is reference's TimeReference::getUndefinedValue().  If the
 * result is multiple items then they are handled as follows:
 * <ul>
 *  <li>The first is a four digit number that looks like a year and there are
 *      exactly two items left over:
 *  <ul>
 *   <li>If the second is an integer that starts with "w" or the default
 *       unit is set to week, then then it is treated as a year
 *       and week specification.
 *   <li>If the second is an integer that starts with "Q" or the default
 *       unit is set to quarter, then then it is treated as a year
 *       and quarter specification.
 *   <li>If the second looks like a decimal DOY, then it is treated as a year
 *       and DOY specification.
 *  </ul>
 * </ul>
 * 
 * @param list              the list to parse
 * @param reference         the time reference point
 * @param handler           the handler to invoke on the other parts of the list
 * @param allowZeroLength   if true then the entire list is allowed to be consumed by the handler
 * @param units             the output natural units or NULL
 * @param defaultUnits      the default time unit for cases of ambiguity
 * @return                  the parsed time
 * @see TimeParse::parseTime(QString, TimeReference * const,  Time::LogicalTimeUnit * const)
 */
double TimeParse::parseListSingle(const QStringList &list,
                                  TimeReference *const reference,
                                  TimeParseListHandler *const handler,
                                  const bool allowZeroLength,
                                  Time::LogicalTimeUnit *const units,
                                  Time::LogicalTimeUnit defaultUnits)
noexcept(false)
{
    int size = list.size();
    if (size == 0) {
        if (allowZeroLength)
            return reference->getUndefinedValue();
        throw TimeParsingException(tr("No possible time in list."));
    }
    int i;
    for (i = 0; i < size - 1; i++) {
        if (!handler->handleLeading(list[i], i, list))
            break;
    }
    if (i == size - 1) {
        if (allowZeroLength) {
            if (handler->handleLeading(list[i], i, list))
                return reference->getUndefinedValue();
        } else {
            return parseTime(list[i], reference, units);
        }
    }

    int j;
    for (j = size - 1; j > i; j--) {
        if (!handler->handleTrailing(list[j], j, list))
            break;
    }
    if (j == i) {
        if (allowZeroLength && handler->handleTrailing(list[j], j, list))
            return reference->getUndefinedValue();
        return parseTime(list[i], reference, units);
    }

    /* Multiple strings, so see if they match any common forms */

    QRegExp yearMatch("^\\s*(\\d{4})\\s*$", Qt::CaseInsensitive);
    QRegExp shortYearMatch("^\\s*(\\d{1,2})\\s*$", Qt::CaseInsensitive);
    if (j == i + 1 && yearMatch.indexIn(list[i]) > -1) {
        int year = yearMatch.cap(1).toInt();
        if (year < 1900 || year > 2999)
            throw TimeParsingException(tr("Year is out of range."));

        /* First one looks like a year */

        QRegExp arg2("^\\s*(?:([WQ])\\s*)?(\\d{1,2})\\s*$", Qt::CaseInsensitive);
        if (arg2.indexIn(list[j]) > -1) {
            if (arg2.pos(1) > -1 && arg2.cap(1).length() > 0) {
                char mc = arg2.cap(1).at(0).toLower().toLatin1();
                if (mc == 'q') {
                    defaultUnits = Time::Quarter;
                } else if (mc == 'w') {
                    defaultUnits = Time::Week;
                }
            }

            if (defaultUnits == Time::Quarter) {
                if (units != NULL) *units = Time::Quarter;

                int qtr = arg2.cap(2).toInt();
                if (qtr < 1 || qtr > 4)
                    throw TimeParsingException(tr("Quarter is out of range."));

                if (reference->isLeading()) {
                    return Time::quarterStart(year, qtr);
                } else {
                    return Time::quarterEnd(year, qtr);
                }
            } else if (defaultUnits == Time::Week) {
                if (units != NULL) *units = Time::Week;

                int week = arg2.cap(2).toInt();
                if (week < 1 || week > 53)
                    throw TimeParsingException(tr("Week is out of range."));

                if (reference->isLeading()) {
                    return Time::weekStart(year, week);
                } else {
                    return Time::weekStart(year, week) + 604800.0;
                }
            }
        }


        QRegExp doyMatch("^\\s*(\\d{1,3})(?:[\\.,](\\d+))?\\s*$", Qt::CaseInsensitive);
        if (doyMatch.indexIn(list[j]) > -1) {
            if (units != NULL) *units = Time::Day;

            int doy = doyMatch.cap(1).toInt();
            int fdoy = 0;
            int fdoyDigits = 0;
            if (doyMatch.pos(2) > -1 && doyMatch.cap(2).length() > 0) {
                QString f = doyMatch.cap(2);
                fdoy = f.toInt();
                fdoyDigits = f.length();
            }

            if (doy < 1 || doy > 366)
                throw TimeParsingException(tr("Day of year is out of range."));

            double base = (double) (doy - 1) * 86400.0;
            if (fdoyDigits != 0)
                base += roundFractionalDOY(fdoy, fdoyDigits, units);
            base += Time::fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));

            return base;
        }
    } else if (j == i + 5 &&
            (yearMatch.indexIn(list[i]) > -1 || shortYearMatch.indexIn(list[i]) > -1)) {
        int year;
        if (!yearMatch.cap(1).isEmpty()) {
            year = yearMatch.cap(1).toInt();
        } else {
            double refT = reference->getReferenceImplied();
            int century = Time::toDateTime(refT).date().year();
            int modularYear = century / 100;
            century = modularYear * 100;
            year = shortYearMatch.cap(1).toInt();
            if (year >= 0 && year <= 99) {
                if (abs(year - modularYear) <= 50)
                    year += century;
                else
                    year += century - 100;
            }
        }
        if (year < 1900 || year > 2999)
            throw TimeParsingException(tr("Year is out of range."));

        QRegExp arg2("^\\s*(\\d{1,2})\\s*$", Qt::CaseInsensitive);
        if (arg2.indexIn(list[i + 1]) <= -1)
            throw TimeParsingException(tr("Unrecognized time format."));
        int month = arg2.cap(1).toInt();
        if (month < 1 || month > 12)
            throw TimeParsingException(tr("Month is out of range."));

        if (arg2.indexIn(list[i + 2]) <= -1)
            throw TimeParsingException(tr("Unrecognized time format."));
        int mday = arg2.cap(1).toInt();
        QDate date(year, month, 1);
        if (mday < 1 || mday > date.daysInMonth())
            throw TimeParsingException(tr("Day of month is out of range."));

        double base =
                Time::fromDateTime(QDateTime(QDate(year, month, mday), QTime(0, 0, 0), Qt::UTC));

        if (arg2.indexIn(list[i + 3]) <= -1)
            throw TimeParsingException(tr("Unrecognized time format."));
        int hour = arg2.cap(1).toInt();
        if (hour < 0 || hour > 23)
            throw TimeParsingException(tr("Hour is out of range."));
        base += (double) hour * 3600.0;

        if (arg2.indexIn(list[i + 4]) <= -1)
            throw TimeParsingException(tr("Unrecognized time format."));
        int minute = arg2.cap(1).toInt();
        if (minute < 0 || minute > 59)
            throw TimeParsingException(tr("Minute is out of range."));
        base += (double) minute * 60.0;

        QRegExp secondMatch("^\\s*(\\d{1,2})(\\.\\d*)?\\s*$", Qt::CaseInsensitive);
        if (secondMatch.indexIn(list[i + 5]) <= -1)
            throw TimeParsingException(tr("Unrecognized time format."));
        double second = (double) secondMatch.cap(1).toInt();
        if (secondMatch.pos(2) > 0) {
            QString fsec(secondMatch.cap(2));
            second += fsec.toInt() / pow(10.0, fsec.length());
        }
        if (second < 0.0 || second > 60.0) /* Leap second */
            throw TimeParsingException(tr("Second is out of range."));
        base += second;

        if (units != NULL) *units = Time::Second;
        return base;
    }

    throw TimeParsingException(tr("Unrecognized time format."));
}

bool TimeParseNOOPListHandler::handleLeading(const QString &str,
                                             int index, const QStringList &list) noexcept(false)
{
    Q_UNUSED(str);
    Q_UNUSED(index);
    Q_UNUSED(list);
    return false;
}

bool TimeParseNOOPListHandler::handleTrailing(const QString &str,
                                              int index, const QStringList &list) noexcept(false)
{
    Q_UNUSED(str);
    Q_UNUSED(index);
    Q_UNUSED(list);
    return false;
}

/**
 * Parse an entire list as a single time specifier relative to the current
 * time.
 * 
 * @param list              the list to parse
 * @param units             the output natural units or NULL
 * @param defaultUnits      the default time unit for cases of ambiguity
 * @return                  the parsed time
 * @see TimeParse::parseListSingle( const QStringList &, TimeReference * const, TimeParseListHandler * const, const bool, Time::LogicalTimeUnit * const, Time::LogicalTimeUnit) 
 */
double TimeParse::parseListSingle(const QStringList &list,
                                  Time::LogicalTimeUnit *const units,
                                  Time::LogicalTimeUnit defaultUnits)
noexcept(false)
{
    ReferenceDefaultLeading ref;
    TimeParseNOOPListHandler handler;
    return parseListSingle(list, &ref, &handler, false, units, defaultUnits);
}


namespace {

class ReferenceBoundEndAbsolute : public TimeReference {
private:
    bool allowUndefined;
    double impliedReference;
public:
    ReferenceBoundEndAbsolute(bool allowUndefined, double impliedReference)
    {
        this->allowUndefined = allowUndefined;
        this->impliedReference = impliedReference;
    }

    virtual double getReference() noexcept(false)
    {
        throw TimeParsingException(TimeParse::tr("Both bounds cannot be relative."));
    }

    virtual double getReferenceImplied() noexcept(false)
    {
        return impliedReference;
    }

    virtual bool isLeading() noexcept(false)
    {
        return false;
    }

    virtual double getUndefinedValue() noexcept(false)
    {
        if (allowUndefined)
            return FP::undefined();
        throw TimeParsingException(TimeParse::tr("Undefined/infinite bounds are not valid."));
    }
};

class ReferenceBoundEndRelative : public TimeReference {
private:
    bool allowUndefined;
    double start;
    double impliedReference;
public:
    ReferenceBoundEndRelative(bool allowUndefined, double start, double impliedReference)
    {
        this->allowUndefined = allowUndefined;
        this->start = start;
        this->impliedReference = impliedReference;
    }

    virtual double getReference() noexcept(false)
    {
        if (FP::defined(start))
            return start;
        throw TimeParsingException(TimeParse::tr("Bound end cannot be relative to infinity."));
    }

    virtual double getReferenceImplied() noexcept(false)
    {
        if (FP::defined(start))
            return start;
        return impliedReference;
    }

    virtual bool isLeading() noexcept(false)
    {
        return false;
    }

    virtual double getUndefinedValue() noexcept(false)
    {
        if (allowUndefined)
            return FP::undefined();
        throw TimeParsingException(TimeParse::tr("Undefined/infinite bounds are not valid."));
    }
};

class ReferenceBoundStartRelative : public TimeReference {
private:
    bool allowUndefined;
    double end;
    double impliedReference;
public:
    ReferenceBoundStartRelative(bool allowUndefined, double end, double impliedReference)
    {
        this->allowUndefined = allowUndefined;
        this->end = end;
        this->impliedReference = impliedReference;
    }

    virtual double getReference() noexcept(false)
    {
        if (FP::defined(end))
            return end;
        throw TimeParsingException(TimeParse::tr("Bound start cannot be relative to infinity."));
    }

    virtual double getReferenceImplied() noexcept(false)
    {
        if (FP::defined(end))
            return end;
        return impliedReference;
    }

    virtual bool isLeading() noexcept(false)
    {
        return true;
    }

    virtual double getUndefinedValue() noexcept(false)
    {
        if (allowUndefined)
            return FP::undefined();
        throw TimeParsingException(TimeParse::tr("Undefined/infinite bounds are not valid."));
    }
};

class ReferenceBoundStart : public TimeReference {
private:
    bool allowUndefined;
    QString end;
    bool parsedEnd;
    double endResult;
    double ref;

public:
    ReferenceBoundStart(bool allowUndefined, const QString &end, const double ref)
    {
        this->allowUndefined = allowUndefined;
        this->end = end;
        this->ref = ref;
        parsedEnd = false;
    }

    virtual double getReference() noexcept(false)
    {
        if (parsedEnd) return endResult;
        ReferenceBoundEndAbsolute ra(allowUndefined, ref);
        endResult = TimeParse::parseTime(end, &ra, NULL);
        parsedEnd = true;
        if (FP::defined(endResult))
            return endResult;
        throw TimeParsingException(TimeParse::tr("Bound start cannot be relative to infinity."));
    }

    virtual double getReferenceImplied() noexcept(false)
    {
        if (!parsedEnd) {
            try {
                ReferenceBoundEndAbsolute ra(allowUndefined, ref);
                endResult = TimeParse::parseTime(end, &ra, NULL);
                parsedEnd = true;
            } catch (TimeParsingException e) {
                parsedEnd = false;
                return ref;
            }
        }
        if (FP::defined(endResult) && endResult < ref)
            return endResult;
        return ref;
    }

    virtual bool isLeading() noexcept(false)
    {
        return true;
    }

    virtual double getUndefinedValue() noexcept(false)
    {
        if (allowUndefined)
            return FP::undefined();
        throw TimeParsingException(TimeParse::tr("Undefined/infinite bounds are not valid."));
    }
};

}

/**
 * Parse two time strings as two bounds in time.  Either bound may be relative
 * to the other (but not both) with the reference point being the other bound
 * as applicable.
 * 
 * @param start             the start time string to parse
 * @param end               the end time string to parse
 * @param allowUndefined    allow undefined/infinite bounds
 * @param defaultReference  the default reference time, or invalid for the current time
 * @return                  the parsed time range
 * @see TimeParse::parseTime( const QString &, TimeReference * const,
        Time::LogicalTimeUnit * const )
 */
Time::Bounds TimeParse::parseTimeBounds(const QString &start,
                                        const QString &end,
                                        bool allowUndefined,
                                        double defaultReference)
noexcept(false)
{
    if (!FP::defined(defaultReference) || defaultReference <= 0.0)
        defaultReference = Time::time();
    Time::Bounds b;
    ReferenceBoundStart rbs(allowUndefined, end, defaultReference);
    b.start = parseTime(start, &rbs, NULL);
    ReferenceBoundEndRelative rer(allowUndefined, b.start, defaultReference);
    b.end = parseTime(end, &rer, NULL);
    if (FP::defined(b.start) && FP::defined(b.end)) {
        if (b.start >= b.end)
            throw TimeParsingException(TimeParse::tr("Start time must be before end time."));
    }
    return b;
}


/**
 * Handle a list of strings that may contain a bound specifier in the middle.
 * It calls the appropriate handler methods on the start and end of
 * the list until it isolates what should be the time specification.  The 
 * general form is that the start precedes the end in the list ordering.  
 * First as much as the start and end of the list as possible is consumed by
 * the handler->  Then the remaining items are checked against the following
 * forms:
 * <ul>
 *  <li>The entire list consumed results in the defaults being returned.
 *  <li>A single item parsed by TimeParse::parseTime(QString, TimeReference &,  Time::LogicalTimeUnit * const)
 *      results in an interval starting at the parsed time end ending one of
 *      the "most logical" time unit after it.  For example, "2003W12" results
 *      in a single week while "2001Q2" results in the whole quarter.
 *  <li>A single integer possibly starting with "W" or "Q" remaining: if
 *      the default unit is either weeks or quarters or the item starts in the
 *      appropriate prefix, then the result is that week or quarter in the
 *      year specified by the default reference point.
 *  <li>Two items parsed by TimeParse::parseTime(QString, TimeReference &,  Time::LogicalTimeUnit * const)
 *      are considered the resultant bounds.  Either one may be relative to
 *      the other if the other is finite.  Either or obth may be a single 
 *      integer (optionally starting with "Q" or "W") that is the week
 *      or quarter of that bound.  The year will be inferred from the other
 *      argument if possible, otherwise using the default reference point.
 *  <li>A four digit year followed by an integer number of the week or quarter
 *      (either beginning with "W" or "Q" or with the default unit set) or
 *      a DOY.  The start is set to either the start of the week or quarter as
 *      appropriate or the Year/DOY specified.  The end is set as follows:
 *      <ul>
 *       <li>No more items remaining in the list results in the end being set
 *           to the end of the week or quarter if one was specified otherwise
 *           the appropriate offset from the DOY.  For example a list of
 *           "2001 321" results in all of day 321 of 2001.  "2001 Q2" results
 *           in all of quarter two of 2001.
 *       <li>A single item remaining that is either a week or quarter as above
 *           or a DOY.  If a prefix "W" or "Q" is used on the first (start)
 *           specifier then it may be omitted here.  The result is an end time
 *           of the end of the week or quarter or the time of the given DOY
 *           as appropriate.  If the resulting end time would be before the
 *           start in the current year, then it is treated as an end time in
 *           the next year.
 *       <li>Two items remaining with the first being a four digit year and the
 *           second being a week, quarter, or DOY as above.  This is identical
 *           to the above case except that the year is not carried over from
 *           the initial specification and that it results in an exception if
 *           the start is after the end.
 *      </ul>
 *   <li>Two specifications of a week, quarter or DOY at the start and end of
 *       three items with a four digit year in the middle.  The year is treated
 *       as the year of the end specifier.  If the start would be after the end
 *       in that year then it is assumed to be in the prior year.
 * </ul>
 * 
 * @param list              the list to parse
 * @param handler           the handler to invoke on the other parts of the list
 * @param defaultTimeBounds the default bounds
 * @param allowZeroLength   if true then the entire list is allowed to be consumed by the handler
 * @param allowUndefined    allow undefined bounds
 * @param defaultUnits      the default time unit for cases of ambiguity
 * @param defaultReference  the default reference time, or invalid for the current time
 * @return                  the parsed time range
 * @see TimeParse::parseTime( const QString &, TimeReference * const,
        Time::LogicalTimeUnit * const )
 */
Time::Bounds TimeParse::parseListBounds(const QStringList &list,
                                        TimeParseListHandler *const handler,
                                        const Time::Bounds &defaultTimeBounds,
                                        const bool allowZeroLength,
                                        const bool allowUndefined,
                                        Time::LogicalTimeUnit defaultUnits,
                                        double defaultReference)
noexcept(false)
{
    int size = list.size();
    if (size == 0) {
        if (allowZeroLength)
            return defaultTimeBounds;
        throw TimeParsingException(tr("No possible time in list."));
    }

    if (!FP::defined(defaultReference) || defaultReference <= 0.0)
        defaultReference = Time::time();

    int i;
    for (i = 0; i < size - 1; i++) {
        if (!handler->handleLeading(list[i], i, list))
            break;
    }

    QRegExp intMatch("^\\s*(?:([WQ])\\s*)?(\\d{1,2})\\s*$", Qt::CaseInsensitive);

    if (i == size - 1) {
        if (allowZeroLength) {
            if (handler->handleLeading(list[i], i, list))
                return defaultTimeBounds;
        } else {
            if (intMatch.indexIn(list[i]) > -1) {
                if (intMatch.pos(1) > -1 && intMatch.cap(1).length() > 0) {
                    char mc = intMatch.cap(1).at(0).toLower().toLatin1();
                    if (mc == 'q') {
                        defaultUnits = Time::Quarter;
                    } else if (mc == 'w') {
                        defaultUnits = Time::Week;
                    }
                }

                if (defaultUnits == Time::Quarter) {
                    int qtr = intMatch.cap(2).toInt();
                    if (qtr < 1 || qtr > 4)
                        throw TimeParsingException(tr("Quarter is out of range."));
                    int year = Time::toDateTime(defaultReference).date().year();
                    return Time::Bounds(Time::quarterStart(year, qtr), Time::quarterEnd(year, qtr));
                } else if (defaultUnits == Time::Week) {
                    int week = intMatch.cap(2).toInt();
                    if (week < 1 || week > 53)
                        throw TimeParsingException(tr("Week is out of range."));
                    int year = Time::toDateTime(defaultReference).date().year();
                    double start = Time::weekStart(year, week);
                    return Time::Bounds(start, start + 604800.0);
                }
            }

            try {
                Time::LogicalTimeUnit units = defaultUnits;
                ReferenceDefaultLeading ref(allowUndefined, defaultReference);
                double start = parseTime(list[i], &ref, &units);
                if (!FP::defined(start))
                    return Time::Bounds(start, defaultTimeBounds.end);
                double end = Time::logical(start, units, 1);
                return Time::Bounds(start, end);
            } catch (TimeParsingException tpe) {
                /* See if the is a comma separated list of fully qualified times.
                 * This is to handle copy-pastes from CSV files, mostly. */
                QStringList split(list[i].split(','));
                if (split.size() != 2) {
                    split = list[i].split(';');
                    if (split.size() != 2) {
                        split = list[i].split(':');
                        if (split.size() != 2) {
                            throw tpe;
                        }
                    }
                }
                Time::Bounds b;
                ReferenceBoundStart rbs(allowUndefined, split[1], defaultReference);
                b.start = parseTime(split[0], &rbs, NULL);
                ReferenceBoundEndRelative rer(allowUndefined, b.start, defaultReference);
                b.end = parseTime(split[1], &rer, NULL);
                if (FP::defined(b.start) && FP::defined(b.end)) {
                    if (b.start >= b.end)
                        throw TimeParsingException(
                                TimeParse::tr("Start time must be before end time."));
                }
                return b;
            }
        }
    }

    int j;
    for (j = size - 1; j > i; j--) {
        if (!handler->handleTrailing(list[j], j, list))
            break;
    }

    if (j == i) {
        if (allowZeroLength && handler->handleTrailing(list[j], j, list))
            return defaultTimeBounds;

        if (intMatch.indexIn(list[i]) > -1) {
            if (intMatch.pos(1) > -1 && intMatch.cap(1).length() > 0) {
                char mc = intMatch.cap(1).at(0).toLower().toLatin1();
                if (mc == 'q') {
                    defaultUnits = Time::Quarter;
                } else if (mc == 'w') {
                    defaultUnits = Time::Week;
                }
            }

            if (defaultUnits == Time::Quarter) {
                int qtr = intMatch.cap(2).toInt();
                if (qtr < 1 || qtr > 4)
                    throw TimeParsingException(tr("Quarter is out of range."));
                int year = Time::toDateTime(defaultReference).date().year();
                return Time::Bounds(Time::quarterStart(year, qtr), Time::quarterEnd(year, qtr));
            } else if (defaultUnits == Time::Week) {
                int week = intMatch.cap(2).toInt();
                if (week < 1 || week > 53)
                    throw TimeParsingException(tr("Week is out of range."));
                int year = Time::toDateTime(defaultReference).date().year();
                double start = Time::weekStart(year, week);
                return Time::Bounds(start, start + 604800.0);
            }
        }

        try {
            Time::LogicalTimeUnit units = defaultUnits;
            ReferenceDefaultLeading ref(allowUndefined, defaultReference);
            double start = parseTime(list[i], &ref, &units);
            if (!FP::defined(start))
                return Time::Bounds(start, defaultTimeBounds.end);
            double end = Time::logical(start, units, 1);
            return Time::Bounds(start, end);
        } catch (TimeParsingException tpe) {
            /* See if the is a comma separated list of fully qualified times.
             * This is to handle copy-pastes from CSV files, mostly. */
            QStringList split(list[i].split(','));
            if (split.size() != 2) {
                split = list[i].split(';');
                if (split.size() != 2) {
                    split = list[i].split(':');
                    if (split.size() != 2) {
                        throw tpe;
                    }
                }
            }
            Time::Bounds b;
            ReferenceBoundStart rbs(allowUndefined, split[1], defaultReference);
            b.start = parseTime(split[0], &rbs, NULL);
            ReferenceBoundEndRelative rer(allowUndefined, b.start, defaultReference);
            b.end = parseTime(split[1], &rer, NULL);
            if (FP::defined(b.start) && FP::defined(b.end)) {
                if (b.start >= b.end)
                    throw TimeParsingException(
                            TimeParse::tr("Start time must be before end time."));
            }
            return b;
        }
    }

    /* Multiple strings, so see if they match any common forms */

    QRegExp yearMatch("^\\s*(\\d{4})\\s*$", Qt::CaseInsensitive);
    QRegExp doyMatch("^\\s*(\\d{1,3})(?:[\\.,](\\d+))?\\s*$", Qt::CaseInsensitive);
    if ((i + 1 == j || i + 2 == j || i + 3 == j) && yearMatch.indexIn(list[i]) > -1) {
        double start = FP::undefined();
        int year = -1;

        if (intMatch.indexIn(list[i + 1]) > -1) {
            if (intMatch.pos(1) > -1 && intMatch.cap(1).length() > 0) {
                char mc = intMatch.cap(1).at(0).toLower().toLatin1();
                if (mc == 'q') {
                    defaultUnits = Time::Quarter;
                } else if (mc == 'w') {
                    defaultUnits = Time::Week;
                }
            }

            if (defaultUnits == Time::Quarter) {
                year = yearMatch.cap(1).toInt();
                if (year < 1900 || year > 2999)
                    throw TimeParsingException(tr("Year is out of range."));
                int qtr = intMatch.cap(2).toInt();
                if (qtr < 1 || qtr > 4)
                    throw TimeParsingException(tr("Quarter is out of range."));
                if (i + 1 == j) {
                    return Time::Bounds(Time::quarterStart(year, qtr), Time::quarterEnd(year, qtr));
                } else {
                    start = Time::quarterStart(year, qtr);
                }
            } else if (defaultUnits == Time::Week) {
                year = yearMatch.cap(1).toInt();
                if (year < 1900 || year > 2999)
                    throw TimeParsingException(tr("Year is out of range."));
                int week = intMatch.cap(2).toInt();
                if (week < 1 || week > 53)
                    throw TimeParsingException(tr("Week is out of range."));
                start = Time::weekStart(year, week);
                if (i + 1 == j)
                    return Time::Bounds(start, start + 604800.0);
            }
        }

        if (!FP::defined(start) && doyMatch.indexIn(list[i + 1]) > -1) {
            year = yearMatch.cap(1).toInt();
            if (year < 1900 || year > 2999)
                throw TimeParsingException(tr("Year is out of range."));
            int doy = doyMatch.cap(1).toInt();
            int fdoy = 0;
            int fdoyDigits = 0;
            if (doyMatch.pos(2) > -1 && doyMatch.cap(2).length() > 0) {
                QString f = doyMatch.cap(2);
                fdoy = f.toInt();
                fdoyDigits = f.length();
            }

            if (doy < 1 || doy > 366)
                throw TimeParsingException(tr("Day of year is out of range."));

            start = (double) (doy - 1) * 86400.0;
            Time::LogicalTimeUnit units = Time::Day;
            if (fdoyDigits != 0)
                start += roundFractionalDOY(fdoy, fdoyDigits, &units);
            start += Time::fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));

            if (i + 1 == j) {
                double end = Time::logical(start, units, 1);
                return Time::Bounds(start, end);
            }
        }

        if (FP::defined(start)) {
            int parseField = -1;
            if (i + 2 == j) {
                parseField = i + 2;
            } else if (yearMatch.indexIn(list[i + 2]) > -1) {
                year = yearMatch.cap(1).toInt();
                if (year < 1900 || year > 2999)
                    throw TimeParsingException(tr("Year is out of range."));
                parseField = i + 3;
            }
            if (parseField != -1) {
                if (intMatch.indexIn(list[parseField]) > -1) {
                    if (intMatch.pos(1) > -1 && intMatch.cap(1).length() > 0) {
                        char mc = intMatch.cap(1).at(0).toLower().toLatin1();
                        if (mc == 'q') {
                            defaultUnits = Time::Quarter;
                        } else if (mc == 'w') {
                            defaultUnits = Time::Week;
                        }
                    }

                    if (defaultUnits == Time::Quarter) {
                        int qtr = intMatch.cap(2).toInt();
                        if (qtr < 1 || qtr > 4)
                            throw TimeParsingException(tr("Quarter is out of range."));
                        double end = Time::quarterEnd(year, qtr);
                        if (start >= end) {
                            if (parseField == i + 3)
                                throw TimeParsingException(
                                        TimeParse::tr("Start time must be before end time."));
                            end = Time::quarterEnd(year + 1, qtr);
                        }
                        return Time::Bounds(start, end);
                    } else if (defaultUnits == Time::Week) {
                        int week = intMatch.cap(2).toInt();
                        if (week < 1 || week > 53)
                            throw TimeParsingException(tr("Week is out of range."));
                        double end = Time::weekStart(year, week) + 604800.0;
                        if (start >= end) {
                            if (parseField == i + 3)
                                throw TimeParsingException(
                                        TimeParse::tr("Start time must be before end time."));
                            end = Time::weekStart(year + 1, week) + 604800.0;
                        }
                        return Time::Bounds(start, end);
                    }
                }
                if (doyMatch.indexIn(list[parseField]) > -1) {
                    int doy = doyMatch.cap(1).toInt();
                    int fdoy = 0;
                    int fdoyDigits = 0;
                    if (doyMatch.pos(2) > -1 && doyMatch.cap(2).length() > 0) {
                        QString f = doyMatch.cap(2);
                        fdoy = f.toInt();
                        fdoyDigits = f.length();
                    }

                    if (doy < 1 || doy > 366)
                        throw TimeParsingException(tr("Day of year is out of range."));

                    double end = (double) (doy - 1) * 86400.0;
                    if (fdoyDigits != 0)
                        end += roundFractionalDOY(fdoy, fdoyDigits, NULL);
                    double proposedYear = Time::fromDateTime(
                            QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));
                    if (start >= end + proposedYear) {
                        if (parseField == i + 3)
                            throw TimeParsingException(
                                    TimeParse::tr("Start time must be before end time."));
                        end += Time::fromDateTime(
                                QDateTime(QDate(year + 1, 1, 1), QTime(0, 0, 0), Qt::UTC));
                    } else {
                        end += proposedYear;
                    }
                    return Time::Bounds(start, end);
                }
            }

            /* Handle cases like "2010 1 1d" */
            if (i + 2 == j) {
                QRegExp checkNumber("^\\s*(\\d+)(?:[\\.,](\\d+))?\\s*$", Qt::CaseInsensitive);
                if (checkNumber.indexIn(list[j]) < 0) {
                    ReferenceBoundEndRelative rer(allowUndefined, start, defaultReference);
                    try {
                        double end = parseTime(list[j], &rer, NULL);
                        if (FP::defined(end) || allowUndefined) {
                            if (!FP::defined(end) || start < end)
                                return Time::Bounds(start, end);
                        }
                    } catch (TimeParsingException tpe) { }
                }
            }
        }
    }

    if (i + 1 == j) {
        /* Special case for things like "20 70" without units, interpret this as a DOY range
         * regardless of the default unit specification. */
        if (doyMatch.indexIn(list[i]) > -1) {
            int year = Time::toDateTime(defaultReference).date().year();

            int sdoy = doyMatch.cap(1).toInt();
            int sfdoy = 0;
            int sfdoyDigits = 0;
            if (doyMatch.pos(2) > -1 && doyMatch.cap(2).length() > 0) {
                QString f = doyMatch.cap(2);
                sfdoy = f.toInt();
                sfdoyDigits = f.length();
            }

            if (sdoy >= 1 && sdoy <= 366 && doyMatch.indexIn(list[j]) > -1) {
                int edoy = doyMatch.cap(1).toInt();
                int efdoy = 0;
                int efdoyDigits = 0;
                if (doyMatch.pos(2) > -1 && doyMatch.cap(2).length() > 0) {
                    QString f = doyMatch.cap(2);
                    efdoy = f.toInt();
                    efdoyDigits = f.length();
                }

                if (edoy >= 1 && edoy <= 366) {
                    double start = (double) (sdoy - 1) * 86400.0;
                    if (sfdoyDigits != 0)
                        start += roundFractionalDOY(sfdoy, sfdoyDigits, NULL);
                    start += Time::fromDateTime(
                            QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));

                    double end = (double) (edoy - 1) * 86400.0;
                    if (efdoyDigits != 0)
                        end += roundFractionalDOY(efdoy, efdoyDigits, NULL);
                    end += Time::fromDateTime(
                            QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));
                    if (end > start) {
                        return Time::Bounds(start, end);
                    }
                }
            }

        }

        double endReference = defaultReference;
        if (intMatch.indexIn(list[i]) == -1 && intMatch.indexIn(list[j]) > -1) {
            try {
                ReferenceDefaultLeading ref(false, defaultReference);
                endReference = parseTime(list[i], &ref, NULL);
            } catch (TimeParsingException tpe) {
                endReference = defaultReference;
            }
        }

        double end = FP::undefined();
        double startReference = defaultReference;
        if (intMatch.indexIn(list[j]) > -1) {
            if (intMatch.pos(1) > -1 && intMatch.cap(1).length() > 0) {
                char mc = intMatch.cap(1).at(0).toLower().toLatin1();
                if (mc == 'q') {
                    defaultUnits = Time::Quarter;
                } else if (mc == 'w') {
                    defaultUnits = Time::Week;
                }
            }

            if (defaultUnits == Time::Quarter) {
                int qtr = intMatch.cap(2).toInt();
                if (qtr < 1 || qtr > 4)
                    throw TimeParsingException(tr("Quarter is out of range."));
                int year = Time::toDateTime(endReference).date().year();
                end = Time::quarterEnd(year, qtr);
            } else if (defaultUnits == Time::Week) {
                int week = intMatch.cap(2).toInt();
                if (week < 1 || week > 53)
                    throw TimeParsingException(tr("Week is out of range."));
                int year = Time::toDateTime(endReference).date().year();
                end = Time::weekStart(year, week) + 604800.0;
            }
        } else if (intMatch.indexIn(list[i]) > -1) {
            try {
                ReferenceDefaultTrailing ref(false, defaultReference);
                startReference = parseTime(list[j], &ref, NULL);
            } catch (TimeParsingException tpe) {
                startReference = defaultReference;
            }
        }

        double start = FP::undefined();
        if (intMatch.indexIn(list[i]) > -1) {
            if (intMatch.pos(1) > -1 && intMatch.cap(1).length() > 0) {
                char mc = intMatch.cap(1).at(0).toLower().toLatin1();
                if (mc == 'q') {
                    defaultUnits = Time::Quarter;
                } else if (mc == 'w') {
                    defaultUnits = Time::Week;
                }
            }

            if (defaultUnits == Time::Quarter) {
                int qtr = intMatch.cap(2).toInt();
                if (qtr < 1 || qtr > 4)
                    throw TimeParsingException(tr("Quarter is out of range."));
                int year = Time::toDateTime(startReference).date().year();
                start = Time::quarterStart(year, qtr);
                if (FP::defined(end) && start >= end)
                    start = Time::quarterStart(year - 1, qtr);
            } else if (defaultUnits == Time::Week) {
                int week = intMatch.cap(2).toInt();
                if (week < 1 || week > 53)
                    throw TimeParsingException(tr("Week is out of range."));
                int year = Time::toDateTime(startReference).date().year();
                start = Time::weekStart(year, week);
                if (FP::defined(end) && start >= end)
                    start = Time::weekStart(year - 1, week);
            }
        }

        if (FP::defined(start) && FP::defined(end))
            return Time::Bounds(start, end);

        if (FP::defined(start)) {
            ReferenceBoundEndRelative ref(allowUndefined, start, defaultReference);
            Time::Bounds b;
            b.start = start;
            b.end = parseTime(list[j], &ref, NULL);
            if (FP::defined(b.start) && FP::defined(b.end)) {
                if (b.start >= b.end)
                    throw TimeParsingException(
                            TimeParse::tr("Start time must be before end time."));
            }
            return b;
        } else if (FP::defined(end)) {
            ReferenceBoundStartRelative ref(allowUndefined, end, defaultReference);
            Time::Bounds b;
            b.end = end;
            b.start = parseTime(list[i], &ref, NULL);
            if (FP::defined(b.start) && FP::defined(b.end)) {
                if (b.start >= b.end)
                    throw TimeParsingException(
                            TimeParse::tr("Start time must be before end time."));
            }
            return b;
        }


        Time::Bounds b;
        ReferenceBoundStart rbs(allowUndefined, list[j], defaultReference);
        b.start = parseTime(list[i], &rbs, NULL);
        ReferenceBoundEndRelative rer(allowUndefined, b.start, defaultReference);
        b.end = parseTime(list[j], &rer, NULL);
        if (FP::defined(b.start) && FP::defined(b.end)) {
            if (b.start >= b.end)
                throw TimeParsingException(TimeParse::tr("Start time must be before end time."));
        }
        return b;
    }

    if ((i + 2 == j) && yearMatch.indexIn(list[i + 1]) > -1) {
        double end = FP::undefined();
        int year = -1;
        if (intMatch.indexIn(list[j]) > -1) {
            if (intMatch.pos(1) > -1 && intMatch.cap(1).length() > 0) {
                char mc = intMatch.cap(1).at(0).toLower().toLatin1();
                if (mc == 'q') {
                    defaultUnits = Time::Quarter;
                } else if (mc == 'w') {
                    defaultUnits = Time::Week;
                }
            }

            if (defaultUnits == Time::Quarter) {
                year = yearMatch.cap(1).toInt();
                if (year < 1900 || year > 2999)
                    throw TimeParsingException(tr("Year is out of range."));
                int qtr = intMatch.cap(2).toInt();
                if (qtr < 1 || qtr > 4)
                    throw TimeParsingException(tr("Quarter is out of range."));
                end = Time::quarterEnd(year, qtr);
            } else if (defaultUnits == Time::Week) {
                year = yearMatch.cap(1).toInt();
                if (year < 1900 || year > 2999)
                    throw TimeParsingException(tr("Year is out of range."));
                int week = intMatch.cap(2).toInt();
                if (week < 1 || week > 53)
                    throw TimeParsingException(tr("Week is out of range."));
                end = Time::weekStart(year, week) + 604800.0;
            }
        }
        if (!FP::defined(end) && doyMatch.indexIn(list[j]) > -1) {
            year = yearMatch.cap(1).toInt();
            if (year < 1900 || year > 2999)
                throw TimeParsingException(tr("Year is out of range."));
            int doy = doyMatch.cap(1).toInt();
            int fdoy = 0;
            int fdoyDigits = 0;
            if (doyMatch.pos(2) > -1 && doyMatch.cap(2).length() > 0) {
                QString f = doyMatch.cap(2);
                fdoy = f.toInt();
                fdoyDigits = f.length();
            }

            if (doy < 1 || doy > 366)
                throw TimeParsingException(tr("Day of year is out of range."));

            end = (double) (doy - 1) * 86400.0;
            if (fdoyDigits != 0)
                end += roundFractionalDOY(fdoy, fdoyDigits, NULL);
            end += Time::fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));
        }

        if (FP::defined(end)) {
            if (intMatch.indexIn(list[i]) > -1) {
                if (intMatch.pos(1) > -1 && intMatch.cap(1).length() > 0) {
                    char mc = intMatch.cap(1).at(0).toLower().toLatin1();
                    if (mc == 'q') {
                        defaultUnits = Time::Quarter;
                    } else if (mc == 'w') {
                        defaultUnits = Time::Week;
                    }
                }

                if (defaultUnits == Time::Quarter) {
                    int qtr = intMatch.cap(2).toInt();
                    if (qtr < 1 || qtr > 4)
                        throw TimeParsingException(tr("Quarter is out of range."));
                    double start = Time::quarterStart(year, qtr);
                    if (start >= end)
                        start = Time::quarterStart(year - 1, qtr);
                    return Time::Bounds(start, end);
                } else if (defaultUnits == Time::Week) {
                    int week = intMatch.cap(2).toInt();
                    if (week < 1 || week > 53)
                        throw TimeParsingException(tr("Week is out of range."));
                    double start = Time::weekStart(year, week);
                    if (start >= end)
                        start = Time::weekStart(year - 1, week);
                    return Time::Bounds(start, end);
                }
            }
            if (doyMatch.indexIn(list[i]) > -1) {
                int doy = doyMatch.cap(1).toInt();
                int fdoy = 0;
                int fdoyDigits = 0;
                if (doyMatch.pos(2) > -1 && doyMatch.cap(2).length() > 0) {
                    QString f = doyMatch.cap(2);
                    fdoy = f.toInt();
                    fdoyDigits = f.length();
                }

                if (doy < 1 || doy > 366)
                    throw TimeParsingException(tr("Day of year is out of range."));

                double start = (double) (doy - 1) * 86400.0;
                if (fdoyDigits != 0)
                    start += roundFractionalDOY(fdoy, fdoyDigits, NULL);
                double proposedYear =
                        Time::fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));
                if (start + proposedYear >= end)
                    start += Time::fromDateTime(
                            QDateTime(QDate(year - 1, 1, 1), QTime(0, 0, 0), Qt::UTC));
                else
                    start += proposedYear;
                return Time::Bounds(start, end);
            }

            /* Handle cases like "1d 2010 2" */
            if (i + 2 == j) {
                QRegExp checkNumber("^\\s*(\\d+)(?:[\\.,](\\d+))?\\s*$", Qt::CaseInsensitive);
                if (checkNumber.indexIn(list[i]) < 0) {
                    ReferenceBoundStartRelative ref(allowUndefined, end, defaultReference);
                    try {
                        double start = parseTime(list[i], &ref, NULL);
                        if (FP::defined(start) || allowUndefined) {
                            if (!FP::defined(start) || start < end)
                                return Time::Bounds(start, end);
                        }
                    } catch (TimeParsingException tpe) { }
                }
            }
        }
    }

    throw TimeParsingException(tr("Unrecognized time format."));
};

/**
 * Parse a list of bounds with the default times being undefined.
 * 
 * @param list              the list to parse
 * @param handler           the handler to invoke on the other parts of the list
 * @param allowZeroLength   if true then the entire list is allowed to be consumed by the handler
 * @param allowUndefined    allow undefined bounds
 * @param defaultUnits      the default time unit for cases of ambiguity
 * @return                  the parsed time range
 * @see TimeParse::parseListBounds( const QStringList &, 
        TimeParseListHandler * const, const Time::Bounds &,
        const bool, const bool, Time::LogicalTimeUnit )
 */
Time::Bounds TimeParse::parseListBounds(const QStringList &list,
                                        TimeParseListHandler *const handler,
                                        const bool allowZeroLength,
                                        const bool allowUndefined,
                                        Time::LogicalTimeUnit defaultUnits) noexcept(false)
{
    return parseListBounds(list, handler, Time::Bounds(FP::undefined(), FP::undefined()),
                           allowZeroLength, allowUndefined, defaultUnits);
}

/**
 * Parse a list of only bounds with the default times being undefined.
 * 
 * @param list              the list to parse
 * @param allowZeroLength   if true then the entire list is allowed to be consumed by the handler
 * @param allowUndefined    allow undefined bounds
 * @param defaultUnits      the default time unit for cases of ambiguity
 * @return                  the parsed time range
 * @see TimeParse::parseListBounds( const QStringList &, 
        TimeParseListHandler * const, const Time::Bounds &,
        const bool, const bool,
        Time::LogicalTimeUnit )
 */
Time::Bounds TimeParse::parseListBounds(const QStringList &list,
                                        const bool allowZeroLength,
                                        const bool allowUndefined,
                                        Time::LogicalTimeUnit defaultUnits) noexcept(false)
{
    TimeParseNOOPListHandler handler;
    return parseListBounds(list, &handler, Time::Bounds(FP::undefined(), FP::undefined()),
                           allowZeroLength, allowUndefined, defaultUnits);
}

/**
 * Return a description of how offsets are parsed.
 * 
 * @param allowZero     true if zero is an acceptable offset
 * @param allowNegative true if negative offsets are allowed
 * @return              a translated description
 */
QString TimeParse::describeOffset(const bool allowZero, const bool allowNegative)
{
    QString result;

    result += tr("A time interval or offset consists of an integer and a unit specifier.  "
                         "The form is of \"X<unit>\" where \"X\" is a number, and \"<unit>\" is one "
                         "of:\n"
                         "    msec or milliseconds\n"
                         "    s, sec, or seconds\n"
                         "    m, min, or minutes - Exactly X * 60 seconds\n"
                         "    h or hours - Exactly X * 3600 seconds\n"
                         "    d or days - Exactly X * 86400 seconds\n"
                         "    w or weeks - Exactly X * 604800 seconds\n"
                         "    mo, mon, or months - Preserves the time of day and day of month\n"
                         "    q, qtr, or quarter - Preserves the time after the start of the quarter\n"
                         "    y or years - Preserves the month, day of month, and time of day\n"
                         "  For example \"1w\" is one week from the reference point.  This may "
                         "optionally be followed by \"a\" or \"aligned\" to align to the boundary "
                         "the multiplier represents.  For example \"0qa\" for a start bound rounds the "
                         "time down to the start of the quarter.  Rounding can be explicitly disabled "
                         "with \"na\" or \"no aligned\" suffixes.  In general the count must be an "
                         "integer but decimal numbers are accepted for the exact fixed length "
                         "specifications and are interpreted as the largest whole unit below the "
                         "specification that gives the exact time.  For example \"1.5d\" is "
                         "interpreted to mean 36 hours.  Alignment is not allowed on fractional "
                         "specifications.  The units may also be omitted to specify an offset in the "
                         "default units.  If the count is omitted but a unit given then the "
                         "specification is for a single of the unit.");

    if (allowNegative) {
        result += tr("Without the alignment enabled, the count integer must be non-zero.");
    } else {
        result +=
                tr("The count integer must be positive.  Without alignment enabled it must also be non-zero.");
    }

    if (allowZero) {
        result +=
                tr("A value of \"none\", \"zero\", \"0\" specifies that no offset should take place.");
    }

    return result;
}

/**
 * Return a description of how a single time is parsed.
 * 
 * @param allowInfinite     true if infinite/undefined times are allowed
 * @return                  a translated description
 */
QString TimeParse::describeSingleTime(const bool allowInfinite)
{
    QString result;

    result += tr("A single time specification consists of an ISO 8601 time and/or date "
                         "specification with additional extensions and inference as possible.  Accepted "
                         "formats are:\n\n"
                         "The literal \"now\" - The current time.\n\n"
                         "An integer and an offset multiplier - This offsets the time in the "
                         "other reference point (and so is only valid when a reference point exists, "
                         "such as valid other bound in a pair of times).  The form is of \"X<unit>\" "
                         "where \"X\" is a non-negative number, and \"<unit>\" is one of:\n"
                         "    msec or milliseconds\n"
                         "    s, sec, or seconds\n"
                         "    m, min, or minutes - Exactly X * 60 seconds\n"
                         "    h or hours - Exactly X * 3600 seconds\n"
                         "    d or days - Exactly X * 86400 seconds\n"
                         "    w or weeks - Exactly X * 604800 seconds\n"
                         "    mo, mon, or months - Preserves the time of day and day of month\n"
                         "    q, qtr, or quarter - Preserves the time after the start of the quarter\n"
                         "    y or years - Preserves the month, day of month, and time of day\n"
                         "  For example \"1w\" is one week from the reference point.  This may "
                         "optionally be followed by \"a\" or \"aligned\" to align to the boundary "
                         "the multiplier represents.  For example \"0qa\" for a start bound rounds the "
                         "time down to the start of the quarter.  In general the count must be an "
                         "integer but decimal numbers are accepted for the exact fixed length "
                         "specifications and are interpreted as the largest whole unit below the "
                         "specification that gives the exact time.  For example \"1.5d\" is "
                         "interpreted to mean 36 hours.  Alignment is not allowed on fractional "
                         "specifications.  Without the alignment enabled, the count number must be "
                         "positive (non-zero).  If the count is omitted then the specification is for "
                         "a single of the unit.\n\n"
                         "Date and/or time - If present the date must come first, it must start with a "
                         "four digit year, followed by an optional month and day of month.  Each field "
                         "must be separated by either a dash or a slash.  The time then follows either "
                         "a space separator or a \"T\".  Time is specified on a 24 hour clock, with "
                         "the hours first, seconds being optional.  Each field must be separated by a "
                         "\":\".  The specification may optionally end in a \"Z\".  Some valid examples "
                         "include:\n"
                         "    2010-03-10T00:15:00Z\n"
                         "    2010/03/10 00:15:00\n"
                         "    2010-03-10\n"
                         "    20100310T001500Z\n"
                         "    15:00:12.123Z\n"
                         "    2010\n"
                         "  If only a time is specified, the date is inferred from the reference point\n"
                         "such that it would be reasonable relative to it.  For example, if the \n"
                         "if this is the start bound and the time is after the reference end bound, \n"
                         "then it is assumed to be in the prior day.  A date and/or time may optionally "
                         "be followed by a time zone offset, this takes the form of + or - a two or "
                         "four digit integer or \"hh:mm\".  This number is subtracted or added to the "
                         "given time to give UTC.\n\n"
                         "Year and week or week - One of the following forms:\n"
                         "    YYYYwWW\n"
                         "    YYYYwWW-D\n"
                         "    wWW\n"
                         "    wWW-D\n"
                         "  Note that the \"WW\" component may be a single digit.  The exact time that "
                         "the week specifies depends on the reference point: if this is a start bound, "
                         "then time specified is the start of that week, otherwise it is the end.  If "
                         "the year is omitted then it is inferred from the reference point as with only "
                         "a time above.\n\n"
                         "Year and quarter or quarter - One of the following forms:\n"
                         "    YYYYqQ\n"
                         "    qQ\n"
                         "  The exact time that the quarter specifies depends on the reference point as "
                         "above with weeks.\n\n"
                         "Year and DOY or DOY - A four digit year separated from a DOY by any of \":\", "
                         "\";\", \",\" or space(s).  The DOY starts with 1.0 being midnight January 1st "
                         "of that year.  The separator may also be omitted or replaced with \"-\" if "
                         "there are exactly three digits in the integer component of the DOY.  Some "
                         "examples include:\n"
                         "    2010:1\n"
                         "    2010,1.2345\n"
                         "    2010;023.12456\n"
                         "    2010023.1234\n"
                         "    2010-023\n"
                         "  The year may also be omitted in which case it is inferred from the reference "
                         "point.\n\n"
                         "A fractional year.  This is a number with a fractional component between "
                         "1900.0 and 2999.0.  The integer part is interpreted as a year and the "
                         "fractional part is the fraction of the way through the year.  For example, "
                         "2010.0 is 2010-01-01T00:00:00Z while 2010.5 is 2010-07-02T12:00:00Z.\n\n"
                         "An epoch time.  This is a decimal number of seconds since "
                         "1900-01-01T00:00:00Z.  This may also be begun with \"E:\" to disambiguate from "
                         "ISO time strings.\n\n");
    if (allowInfinite) {
        result += tr("An infinite bound.  This is one of the following forms:\n"
                             "    Empty - All space (or zero length)\n"
                             "    0 - The number zero\n"
                             "    none\n"
                             "    undef or undefined\n"
                             "    inf or infinity\n"
                             "    all\n"
                             "    forever\n\n");
    }
    result += tr("\nThe time specification may be followed by an offset, this consists of a + "
                         "or -, an integer, and a unit.  The integer number of the given time unit are "
                         "added or subtracted to the time specified before.  The following time units "
                         "are valid:\n"
                         "    msec or milliseconds\n"
                         "    s, sec, or seconds\n"
                         "    m, min, or minutes - Exactly X * 60 seconds\n"
                         "    h or hours - Exactly X * 3600 seconds\n"
                         "    d or days - Exactly X * 86400 seconds\n"
                         "    w or weeks - Exactly X * 604800 seconds\n"
                         "    mo, mon, or months - Preserves the time of day and day of month\n"
                         "    q, qtr, or quarter - Preserves the time after the start of the quarter\n"
                         "    y or years - Preserves the time of day and month and time of day\n"
                         "  For example \"2010-03-10T00:15:00Z+5d\" results in \"2010-03-15T00:15:00Z\".\n\n"
                         "NOTE: When the alignment would overflow a date field it is set to the largest "
                         "valid one in the alignment.  So one month up from Mar 31 would be on Apr 30.");
    return result;
}

/**
 * Return a translated description of how a list is parsed into a set of
 * time bounds.
 * 
 * @param allowZeroLength   true if the parsing accepts zero length bounds
 * @param allowUndefined    true if the parsing accepts undefined bounds
 * @param defaultUnits      the default parsing units
 * @return                  a translated description
 */
QString TimeParse::describeListBoundsUsage(const bool allowZeroLength,
                                           const bool allowUndefined,
                                           Time::LogicalTimeUnit defaultUnits)
{
    QString result;
    if (allowZeroLength) {
        result = tr("The specification of time bounds consists of zero or more arguments that "
                            "specify start and end time bounds.  If there are no arguments then both the "
                            "start and end are treated as infinite.  The start time is defined by the "
                            "arguments towards the start (left) side.");
    } else {
        result = tr("The specification of time bounds consists of one or more arguments that "
                            "specify start and end time bounds.  The start time is defined by the "
                            "arguments towards the start (left) side.");
    }

    result += describeSingleTime(allowUndefined);
    result += tr("\n\n\nThe following forms of time bounds are accepted:\n"
                         "A single item as described above to give the start time, with the being one "
                         "unit of the \"most logical\" time unit after it.  For example, \"2003W12\" "
                         "results in a single week while \"2001Q2\" results in the whole quarter."
                         "\n\n"
                         "A single integer possibly starting with \"W\" or \"Q\" then if it has the "
                         "appropriate prefix, then the result is that week or quarter in the year "
                         "specified by the default reference point (the current year by default).  ");
    if (defaultUnits == Time::Quarter) {
        result += tr("If no unit is given then it is assumed it is a quarter specifier.");
    } else if (defaultUnits == Time::Week) {
        result += tr("If no unit is given then it is assumed it is a week of year.");
    } else {
        result += tr("If no unit is given then it is assumed it is a day of year.");
    }
    result += tr("\n"
                         "Two fully qualified single times as defined above.  Either one may be relative "
                         "to the other if the other is finite.  Either or both may be also single "
                         "integer (optionally starting with \"Q\" or \"W\") that is the start or end of "
                         "that week or quarter, depending on which bound it specified.  The year will be "
                         "inferred from the other argument if possible, otherwise using the default "
                         "reference point (the current year)."
                         "\n\n"
                         "A four digit year followed by an integer number of the week or quarter ");
    if (defaultUnits == Time::Quarter) {
        result += tr("(either beginning with \"W\" or \"Q\" or assumed to be a quarter");
    } else if (defaultUnits == Time::Week) {
        result += tr("(either beginning with \"W\" or \"Q\" or assumed to be a week");
    } else {
        result += tr("(either beginning with \"W\" or \"Q\")");
    }
    result += tr("or a DOY.  The start is set to either the start of the week or quarter as "
                         "appropriate or the Year/DOY specified.  The end is set as follows:\n"
                         "  No arguments remaining results in the end being set to the end of the week or "
                         "quarter if one was specified otherwise the appropriate offset from the DOY.  "
                         "For \"2001 321\" results in all of day 321 of 2001.  \"2001 Q2\" results in "
                         "all of quarter two of 2001.\n"
                         "  A single item remaining that is either a week or quarter as above or a DOY.  "
                         "If a prefix \"W\" or \"Q\" is used on the first (start)  specifier then it may "
                         "be omitted here.  The result is an end time of the end of the week or quarter "
                         "or the time of the given DOY as appropriate.  If the resulting end time would "
                         "be before the start in the current year, then it is treated as an end time "
                         "in the next year.\n"
                         "  Two items remaining with the first being a four digit year and the second "
                         "being a week, quarter, or DOY as above.  This is identical to the above case "
                         "except that the year is not carried over from the initial specification and "
                         "that it results in an error if the start is after the end.\n"
                         "Two specifications of a week, quarter or DOY at the start and end of three "
                         "items with a four digit year in the middle.  The year is treated as the year "
                         "of the end specifier.  If the start would be after the end in that year then "
                         "it is assumed to be in the prior year.");
    return result;
}

/**
 * Return a translated description of how a duration is parsed into logical
 * units.
 * 
 * @param allowZeroLength   true if the parsing accepts zero length durations
 * @param allowNegative     true if the parsing accepts negative durations
 * @param defaultUnits      the default parsing units
 * @return                  a translated description
 * @return                  a translated description
 */
QString TimeParse::describeDurationUsage(const bool allowZeroLength,
                                         const bool allowNegative,
                                         Time::LogicalTimeUnit defaultUnits)
{
    QString result
            (tr("The specification of a duration consists of a single argument that specifies "
                        "a time offset from the current time.  "));
    result += describeOffset(allowZeroLength, allowNegative);

    switch (defaultUnits) {
    case Time::Millisecond:
        result += tr("  If no time unit is specified then the duration is assumed to be in "
                             "milliseconds.");
        break;
    case Time::Second:
        result += tr("  If no time unit is specified then the duration is assumed to be in "
                             "second.");
        break;
    case Time::Minute:
        result += tr("  If no time unit is specified then the duration is assumed to be in "
                             "minutes.");
        break;
    case Time::Hour:
        result += tr("  If no time unit is specified then the duration is assumed to be in "
                             "hours.");
        break;
    case Time::Day:
        result += tr("  If no time unit is specified then the duration is assumed to be in "
                             "days.");
        break;
    case Time::Week:
        result += tr("  If no time unit is specified then the duration is assumed to be in "
                             "weeks.");
        break;
    case Time::Month:
        result += tr("  If no time unit is specified then the duration is assumed to be in "
                             "months.");
        break;
    case Time::Quarter:
        result += tr("  If no time unit is specified then the duration is assumed to be in "
                             "quarters.");
        break;
    case Time::Year:
        result += tr("  If no time unit is specified then the duration is assumed to be in "
                             "years.");
        break;
    }

    return result;
}


/**
 * Doesn't throw exceptions.  This is for JNI code which has issues not allowing
 * the exception to be caught at all.
 * 
 * @see TimeParse::parseTime( QString, TimeReference * const, 
 *          Time::LogicalTimeUnit * const )
 */
double TimeParseLegacy::parseTime(QString time,
                                  TimeReference *const reference,
                                  Time::LogicalTimeUnit *const units)
{
    try {
        return TimeParse::parseTime(time, reference, units);
    } catch (TimeParsingException tpe) {
        return FP::undefined();
    }
}

/**
 * Doesn't throw exceptions.  This is for JNI code which has issues not allowing
 * the exception to be caught at all.
 * 
 * @see TimeParse::parseListBounds( const QStringList &, const bool, const bool,
 *          Time::LogicalTimeUnit )
 */
Time::Bounds TimeParseLegacy::parseListBounds(const QStringList &list,
                                              const bool allowZeroLength,
                                              const bool allowUndefined,
                                              Time::LogicalTimeUnit defaultUnits)
{
    try {
        return TimeParse::parseListBounds(list, allowZeroLength, allowUndefined, defaultUnits);
    } catch (TimeParsingException tpe) {
        return Time::Bounds();
    }
}

};
