/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/stream.hxx"

namespace CPD3 {

/** @file core/stream.hxx
 * Provides general stream handling routines.
 */

}
