/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3COREACTIONCOMPONENT_H
#define CPD3COREACTIONCOMPONENT_H

#include "core/first.hxx"

#include <functional>
#include <vector>
#include <string>

#include <QtGlobal>
#include <QObject>
#include <QtPlugin>
#include <QThread>
#include <QList>

#include "core.hxx"
#include "component.hxx"
#include "timeutils.hxx"
#include "threading.hxx"

namespace CPD3 {

namespace ActionFeedback {

/**
 * A handler class for action feedback.  This provides user feedback related to
 * the progress of an action.
 */
struct CPD3CORE_EXPORT Source {
    /**
     * A stage of feedback is the top level information of feedback.  This is usually
     * represented as a progress bar or spinner indicator.
     */
    class CPD3CORE_EXPORT Stage final {
        QString stageTitle;
        QString stageDescription;
        bool stageHasProgress;
    public:
        Stage();

        Stage(const Stage &) = default;

        Stage &operator=(const Stage &) = default;

        Stage(Stage &&) = default;

        Stage &operator=(Stage &&) = default;

        /**
         * Crate the stage.
         *
         * @param title the title of the stage
         */
        Stage(const QString &title);

        /**
         * Create the stage.
         *
         * @param title         the title of the stage
         * @param description   the detailed description of the stage
         * @param hasProgress   true if the stage expects to generate progress indicators
         */
        Stage(const QString &title, const QString &description, bool hasProgress = false);

        /**
         * Create the stage.
         * 
         * @param title         the title of the stage
         * @param description   the detailed description of the stage
         * @param hasProgress   true if the stage expects to generate progress indicators
         */
        Stage(const QString &title, bool hasProgress);

        /**
         * Get the title of the stage.
         *
         * @return  the stage title
         */
        inline const QString &title() const
        { return stageTitle; }

        /**
         * Get the detailed description of the stage.
         *
         * @return  the description
         */
        inline const QString &description() const
        { return stageDescription; }

        /**
         * Test if the stage has progress.
         *
         * @return  true if the stage will generate progress updates.
         */
        inline bool hasProgress() const
        { return stageHasProgress; }
    };

    /**
     * The signal generated when a new stage advances.  If the failure signal has not
     * been emitted then the previous stage is assumed to have suceeded.
     */
    Threading::Signal<Stage> stage;

    /** @see Stage::Stage() */
    template<typename...Args>
    inline void emitStage(Args &&... args) const
    { stage(Stage(std::forward<Args>(args)...)); }

    /**
     * The state of the currently active stage.  This provides further information
     * about what is occurring in the stage.
     */
    class CPD3CORE_EXPORT State final {
        QString stateDescription;
    public:
        State();

        State(const State &) = default;

        State &operator=(const State &) = default;

        State(State &&) = default;

        State &operator=(State &&) = default;

        State(const QString &state);

        inline const QString &description() const
        { return stateDescription; }
    };

    /**
     * The signal generated on a stage state update.  This is used to provide
     * details about the stage that are not directly connected to a fraction of
     * completion.
     */
    Threading::Signal<State> state;

    /** @see State::State() */
    template<typename...Args>
    inline void emitState(Args &&... args) const
    { state(State(std::forward<Args>(args)...)); }

    /**
     * A failure in the current stage.
     */
    class CPD3CORE_EXPORT Failure final {
        QString failureReason;
    public:
        Failure();

        Failure(const Failure &) = default;

        Failure &operator=(const Failure &) = default;

        Failure(Failure &&) = default;

        Failure &operator=(Failure &&) = default;

        /**
         * Create the failure.
         *
         * @param reason    the reason for the failure
         */
        Failure(const QString &reason);

        /**
         * Get the reason for the failure.
         *
         * @return  the failure reason
         */
        inline const QString &reason() const
        { return failureReason; }
    };

    /**
     * The signal generated when the current stage has experienced a failure.  This is
     * used to indicate an error and display a reason for the failure, if possible.
     */
    Threading::Signal<Failure> failure;

    /** @see Failure::Failure() */
    template<typename...Args>
    inline void emitFailure(Args &&... args) const
    { failure(Failure(std::forward<Args>(args)...)); }

    /**
     * An update to the progress of the current stage in time.
     */
    class CPD3CORE_EXPORT Time final {
        double progressTime;
        double possibleStart;
        double possibleEnd;
    public:
        Time();

        Time(const Time &) = default;

        Time &operator=(const Time &) = default;

        Time(Time &&) = default;

        Time &operator=(Time &&) = default;

        /**
         * Create the progress indicator.
         *
         * @param time          the time of the progress
         */
        Time(double time);

        /**
         * Create the progress indicator.
         *
         * @param time          the time of the progress
         * @param start         the expected start time
         * @param end           the expected end time
         */
        Time(double time, double start, double end);

        /**
         * Get the time of progress.
         *
         * @return              the time
         */
        inline double time() const
        { return progressTime; }

        inline double getStart() const
        { return possibleStart; }

        inline double getEnd() const
        { return possibleEnd; }
    };

    Threading::Signal<Time> progressTime;

    /** @see Time::Time() */
    template<typename...Args>
    inline void emitProgressTime(Args &&... args) const
    { progressTime(Time(std::forward<Args>(args)...)); }

    /**
     * An update to the progress of the current stage as a fraction of completion.
     */
    class CPD3CORE_EXPORT Fraction final {
        double progressFraction;
    public:
        Fraction();

        Fraction(const Fraction &) = default;

        Fraction &operator=(const Fraction &) = default;

        Fraction(Fraction &&) = default;

        Fraction &operator=(Fraction &&) = default;

        /**
         * Create the progress indicator.
         *
         * @param fraction  the fraction [0,1] of completion
         */
        Fraction(double fraction);

        /**
         * Get the fraction of progress.
         *
         * @return              the fraction of completion [0,1]
         */
        inline double fraction() const
        { return progressFraction; }
    };

    Threading::Signal<Fraction> progressFraction;

    /** @see Fraction::Fraction() */
    template<typename...Args>
    inline void emitProgressFraction(Args &&... args) const
    { progressFraction(Fraction(std::forward<Args>(args)...)); }

    Source();

    Source(const Source &);

    Source &operator=(const Source &);

    Source(Source &&);

    Source &operator=(Source &&);

    /**
     * Forward all future signals to the targets of an existing receiver.
     *
     * @param receiver  the receiver
     */
    void forward(Source &receiver);

    /**
     * Disconnect all signals.
     */
    void disconnect();
};

/**
 * A serializing endpoint for action progress indicators.
 */
class CPD3CORE_EXPORT Serializer : public Threading::Receiver {
public:
    /**
     * A progress stage.
     */
    class CPD3CORE_EXPORT Stage {
        friend class Serializer;

    public:
        enum State {
            /** Progress currently spinning (i.e. no completion indicator available). */
                    Spin,

            /** Progress currently active. */
                    Progress,

            /** Stage completed */
                    Complete,

            /** Stage failed */
                    Failure
        };
    private:
        State currentState;

        QString stageTitle;
        QString stageDescription;
        QString stageStateOrReason;

        double progressFraction;

        double progressTime;
        double progressStart;
        double progressEnd;

    public:
        Stage();

        Stage(const Stage &) = default;

        Stage &operator=(const Stage &) = default;

        Stage(Stage &&) = default;

        Stage &operator=(Stage &&) = default;

        /**
         * Get the state of the stage.
         *
         * @return  the current state
         */
        inline State state() const
        { return currentState; }

        /**
         * Get the title of the stage.
         *
         * @return  the title
         */
        inline const QString &title() const
        { return stageTitle; }

        /**
         * Get the description of the stage.
         *
         * @return  the stage description
         */
        inline const QString &description() const
        { return stageDescription; }

        /**
         * Get the status text of the stage.
         *
         * @return  the current status text
         */
        inline const QString &stageStatus() const
        { return stageStateOrReason; }

        /**
         * Get the reason for the failure of the stage.
         *
         * @return  the failure reason
         */
        inline const QString &failureReason() const
        { return stageStateOrReason; }

        /**
         * Get the progress fraction of the stage.
         *
         * @return  the current progress fraction [0,1]
         */
        inline double fraction() const
        { return progressFraction; }

        /**
         * Convert the stage progress to a fraction even if it is a time.
         *
         * @param start the contextual start time
         * @param end   the contextual end time
         * @return      a progress fraction [0,1] or undefined
         */
        double toFraction(double start = FP::undefined(), double end = FP::undefined()) const;

        /**
         * Get the progress time of the stage.
         *
         * @return  the progress time
         */
        inline double time() const
        { return progressTime; }

        /**
         * Get the start time of the stage.
         *
         * @param context   the contextual start time
         * @return          a start time
         */
        double getStart(double context = FP::undefined()) const;

        /**
         * Get the end time of the stage.
         *
         * @param context   the contextual end time
         * @return          an end time
         */
        double getEnd(double context = FP::undefined()) const;
    };

private:

    std::mutex mutex;
    Stage active;
    std::vector<Stage> pending;
    bool progressUpdated;

    void incomingStage(const Source::Stage &stage);

    void incomingState(const Source::State &state);

    void incomingFailure(const Source::Failure &failure);

    void incomingTime(const Source::Time &progress);

    void incomingFraction(const Source::Fraction &progress);

public:
    Serializer();

    virtual ~Serializer();

    Serializer(const Serializer &) = delete;

    Serializer &operator=(const Serializer &) = delete;

    /**
     * Create the serializing and attach to a source.
     *
     * @param source    the source to attach to
     */
    Serializer(Source &source);

    /**
     * Attach the serializer to a source of feedback.
     *
     * @param source    the feedback source
     */
    void attach(Source &source);

    /**
     * Reset the serializer.
     */
    void reset();

    /**
     * Process all current feedback.  The first elements in the result are
     * the completed stages and the last is the active one.
     *
     * @return  the stages to process
     */
    std::vector<Stage> process();

    /**
     * Emitted whenever any update occurs.
     */
    Threading::Signal<> updated;

    /**
     * Emitted when a stage advances (either in a full stage or as the status component of one)
     */
    Threading::Signal<> advanced;
};

}

/**
 * The base from which actions are derived.  This exists to define the 
 * signals used to notify the parent of the action's progress.
 */
class CPD3CORE_EXPORT CPD3Action : public QThread {
Q_OBJECT
public:
    CPD3Action();

    virtual ~CPD3Action();

public:

    ActionFeedback::Source feedback;

public slots:

    virtual void signalTerminate() = 0;
};

/**
 * The interface for a single shot stand alone action.
 */
class CPD3CORE_EXPORT ActionComponent {
public:
    virtual ~ActionComponent() = default;

    /**
     * Get an options object (set to all defaults) for this action type.
     * 
     * @return an options object for this action type
     */
    virtual ComponentOptions getOptions();

    /**
     * Gets a list of examples that show usage of the component.  The caller
     * will use the specified options to construct usage in the context it's
     * called (e.x. command lines).
     * 
     * @return a list in display order of examples
     */
    virtual QList<ComponentExample> getExamples();

    /**
     * Get the number of stations allowed to be specified.  The default 
     * implementation returns zero.  Return INT_MAX to disable the maximum.
     * 
     * @return the number of allowed stations to be specified
     */
    virtual int actionAllowStations();

    /**
     * Get the number of stations required to be specified.  The default 
     * implementation returns zero.
     * 
     * @return the number of required stations to be specified
     */
    virtual int actionRequireStations();

    /**
     * Get the prompt string for the given options.  If the prompt string
     * is non-zero length it is displayed with a question to continue.
     * The default implementation returns an empty string.
     * 
     * @param options   the options to use
     * @param stations  the stations specified, only valid if the mode allows it
     */
    virtual QString promptActionContinue(const ComponentOptions &options = {},
                                         const std::vector<std::string> &stations = {});

    /**
     * Create a action object from the given options.
     * <br>
     * This allocates the object with new, so the caller must delete it when 
     * finished.
     * 
     * @param options   the options to use
     * @param stations  the stations specified, only valid if the mode allows it
     */
    virtual CPD3Action *createAction(const ComponentOptions &options = {},
                                     const std::vector<std::string> &stations = {}) = 0;
};

/**
 * The interface for a single shot stand alone action that may accept a time
 * range.
 */
class CPD3CORE_EXPORT ActionComponentTime {
public:
    virtual ~ActionComponentTime() = default;

    /**
     * Get an options object (set to all defaults) for this action type.
     * 
     * @return an options object for this action type
     */
    virtual ComponentOptions getOptions();

    /**
     * Gets a list of examples that show usage of the component.  The caller
     * will use the specified options to construct usage in the context it's
     * called (e.x. command lines).
     * 
     * @return a list in display order of examples
     */
    virtual QList<ComponentExample> getExamples();

    /**
     * Get the number of stations allowed to be specified.  The default 
     * implementation returns zero.  Return INT_MAX to disable the maximum.
     * 
     * @return the number of allowed stations to be specified
     */
    virtual int actionAllowStations();

    /**
     * Get the number of stations required to be specified.  The default 
     * implementation returns zero.
     * 
     * @return the number of required stations to be specified
     */
    virtual int actionRequireStations();

    /**
     * This should return true if the action always requires time bounds.
     * The default implementation returns true.
     * 
     * @return true if the action requires a time range
     */
    virtual bool actionRequiresTime();

    /**
     * This should return true if the action can accept undefined/infinite
     * time bounds.  The default implementation returns true.
     * 
     * @return true if the action accepts infinite bounds
     */
    virtual bool actionAcceptsUndefinedBounds();

    /**
     * This should return the default time unit for parsing times.  For example
     * setting this to a week allows a string like "2010 2" to specify the 
     * second week of 2010.  The default implementation returns Time::Day.
     * 
     * @return the default unit for time parsing
     * @see parseListBounds( const QStringList &, const bool, const bool,
     *          Time::LogicalTimeUnit ) 
     */
    virtual Time::LogicalTimeUnit actionDefaultTimeUnit();

    /**
     * Get the prompt string for the given options and time range.  If the 
     * prompt string is non-zero length it is displayed with a question to 
     * continue.  The default implementation returns an empty string.
     * 
     * @param options   the options to use
     * @param start     the start time
     * @param end       the end time
     * @param stations  the stations specified, only valid if the mode allows it
     */
    virtual QString promptTimeActionContinue(const ComponentOptions &options,
                                             double start,
                                             double end,
                                             const std::vector<std::string> &stations = {});

    /**
     * Get the prompt string for the given options.  If the 
     * prompt string is non-zero length it is displayed with a question to 
     * continue.  This is called when no time range was specified and only if 
     * actionRequiresTime() returns false.  The default implementation calls
     * promptTimeActionContinue( const ComponentOptions &, double, double ) 
     * with both bounds set to FP::undefined().
     * 
     * @param options   the options to use
     * @param stations  the stations specified, only valid if the mode allows it
     */
    virtual QString promptTimeActionContinue(const ComponentOptions &options = {},
                                             const std::vector<std::string> &stations = {});

    /**
     * Create a action object from the given options and start time range.
     * <br>
     * This allocates the object with new, so the caller must delete it when 
     * finished.
     * 
     * @param options   the options to use
     * @param start     the start time
     * @param end       the end time
     * @param stations  the stations specified, only valid if the mode allows it
     */
    virtual CPD3Action *createTimeAction(const ComponentOptions &options,
                                         double start,
                                         double end,
                                         const std::vector<std::string> &stations = {}) = 0;

    /**
     * Create a action object from the given options.  This is called when no
     * time range was specified and only if actionRequiresTime() returns false.
     * The default implementation calls createTimeAction(
     * const ComponentOptions &, double, double ) with both bounds set to
     * FP::undefined().
     * <br>
     * This allocates the object with new, so the caller must delete it when 
     * finished.
     * 
     * @param options   the options to use
     * @param stations  the stations specified, only valid if the mode allows it
     */
    virtual CPD3Action *createTimeAction(const ComponentOptions &options = {},
                                         const std::vector<std::string> &stations = {});
};

}

Q_DECLARE_INTERFACE(CPD3::ActionComponent, "CPD3.ActionComponent/1.0");

Q_DECLARE_INTERFACE(CPD3::ActionComponentTime, "CPD3.ActionComponentTime/1.0");

#endif
