/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3_QTCOMPAT_HXX
#define CPD3_QTCOMPAT_HXX

#include "core/first.hxx"

#include <vector>
#include <list>
#include <map>
#include <deque>
#include <unordered_set>
#include <unordered_map>
#include <string>
#include <QDebug>
#include <QDebugStateSaver>
#include <QTextStream>
#include <QDataStream>

#include "core.hxx"
#include "number.hxx"

template<class T, class Allocator>
QDataStream &operator<<(QDataStream &stream, const std::vector<T, Allocator> &v)
{
    Q_ASSERT(static_cast<quint32>(v.size()) == v.size());
    stream << static_cast<quint32>(v.size());
    for (const auto &add : v) {
        stream << add;
    }
    return stream;
}

template<class T, class Allocator>
QDataStream &operator>>(QDataStream &stream, std::vector<T, Allocator> &v)
{
    quint32 n = 0;
    stream >> n;
    v.clear();
    v.reserve(n);
    for (quint32 i = 0; i < n; ++i) {
        v.emplace_back();
        stream >> v.back();
    }
    return stream;
}

template<class T, class Allocator>
QDataStream &operator<<(QDataStream &stream, const std::deque<T, Allocator> &v)
{
    Q_ASSERT(static_cast<quint32>(v.size()) == v.size());
    stream << static_cast<quint32>(v.size());
    for (const auto &add : v) {
        stream << add;
    }
    return stream;
}

template<class T, class Allocator>
QDataStream &operator>>(QDataStream &stream, std::deque<T, Allocator> &v)
{
    quint32 n = 0;
    stream >> n;
    v.clear();
    for (quint32 i = 0; i < n; ++i) {
        v.emplace_back();
        stream >> v.back();
    }
    return stream;
}

template<class T, class Allocator>
QDataStream &operator<<(QDataStream &stream, const std::list<T, Allocator> &v)
{
    Q_ASSERT(static_cast<quint32>(v.size()) == v.size());
    stream << static_cast<quint32>(v.size());
    for (const auto &add : v) {
        stream << add;
    }
    return stream;
}

template<class T, class Allocator>
QDataStream &operator>>(QDataStream &stream, std::list<T, Allocator> &v)
{
    quint32 n = 0;
    stream >> n;
    v.clear();
    for (quint32 i = 0; i < n; ++i) {
        v.emplace_back();
        stream >> v.back();
    }
    return stream;
}

template<class T, class Hash, class KeyEqual, class Allocator>
QDataStream &operator<<(QDataStream &stream,
                        const std::unordered_set<T, Hash, KeyEqual, Allocator> &v)
{
    Q_ASSERT(static_cast<quint32>(v.size()) == v.size());
    stream << static_cast<quint32>(v.size());
    for (const auto &add : v) {
        stream << add;
    }
    return stream;
}

template<class T, class Hash, class KeyEqual, class Allocator>
QDataStream &operator>>(QDataStream &stream, std::unordered_set<T, Hash, KeyEqual, Allocator> &v)
{
    quint32 n = 0;
    stream >> n;
    v.clear();
    v.reserve(n);
    for (quint32 i = 0; i < n; ++i) {
        T add;
        stream >> add;
        v.emplace(std::move(add));
    }
    return stream;
}

template<class Key, class T, class Hash, class KeyEqual, class Allocator>
QDataStream &operator<<(QDataStream &stream,
                        const std::unordered_map<Key, T, Hash, KeyEqual, Allocator> &v)
{
    Q_ASSERT(static_cast<quint32>(v.size()) == v.size());
    stream << static_cast<quint32>(v.size());
    for (const auto &add : v) {
        stream << add.first << add.second;
    }
    return stream;
}

template<class Key, class T, class Hash, class KeyEqual, class Allocator>
QDataStream &operator>>(QDataStream &stream,
                        std::unordered_map<Key, T, Hash, KeyEqual, Allocator> &v)
{
    quint32 n = 0;
    stream >> n;
    v.clear();
    v.reserve(n);
    for (quint32 i = 0; i < n; ++i) {
        std::pair<Key, T> add;
        stream >> add.first >> add.second;
        v.emplace(std::move(add));
    }
    return stream;
}

template<class Key, class T, class Compare, class Allocator>
QDataStream &operator<<(QDataStream &stream, const std::map<Key, T, Compare, Allocator> &v)
{
    Q_ASSERT(static_cast<quint32>(v.size()) == v.size());
    stream << static_cast<quint32>(v.size());
    for (const auto &add : v) {
        stream << add.first << add.second;
    }
    return stream;
}

template<class Key, class T, class Compare, class Allocator>
QDataStream &operator>>(QDataStream &stream, std::map<Key, T, Compare, Allocator> &v)
{
    quint32 n = 0;
    stream >> n;
    v.clear();
    for (quint32 i = 0; i < n; ++i) {
        std::pair<Key, T> add;
        stream >> add.first >> add.second;
        v.emplace(std::move(add));
    }
    return stream;
}


template<class T, class Hash, class KeyEqual, class Allocator>
QDebug operator<<(QDebug stream, const std::unordered_set<T, Hash, KeyEqual, Allocator> &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    bool first = true;
    stream << "std::unordered_set(";
    for (const auto &add : v) {
        if (!first)
            stream << ',';
        stream << add;
        first = false;
    }
    stream << ')';
    return stream;
}

template<class Key, class T, class Hash, class KeyEqual, class Allocator>
QDebug operator<<(QDebug stream, const std::unordered_map<Key, T, Hash, KeyEqual, Allocator> &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    bool first = true;
    stream << "std::unordered_map(";
    for (const auto &add : v) {
        if (!first)
            stream << ',';
        stream << '{' << add.first << ',' << add.second << '}';
        first = false;
    }
    stream << ')';
    return stream;
}

CPD3CORE_EXPORT QDataStream &operator<<(QDataStream &stream, const std::string &v);

CPD3CORE_EXPORT QDataStream &operator>>(QDataStream &stream, std::string &v);

CPD3CORE_EXPORT QDebug operator<<(QDebug stream, const std::string &value);

Q_DECLARE_METATYPE(std::string);


template<class T, class Allocator>
QDebug operator<<(QDebug stream, const std::deque<T, Allocator> &v)
{
	QDebugStateSaver saver(stream);
	stream.nospace();
	bool first = true;
	stream << "std::deque(";
	for (const auto &add : v) {
		if (!first)
			stream << ',';
		stream << add;
		first = false;
	}
	stream << ')';
	return stream;
}

#if QT_VERSION < QT_VERSION_CHECK(5, 7, 0)

template<class T, class Allocator>
QDebug operator<<(QDebug stream, const std::vector<T, Allocator> &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    bool first = true;
    stream << "std::vector(";
    for (const auto &add : v) {
        if (!first)
            stream << ',';
        stream << add;
        first = false;
    }
    stream << ')';
    return stream;
}

template<class T, class Allocator>
QDebug operator<<(QDebug stream, const std::list<T, Allocator> &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    bool first = true;
    stream << "std::list(";
    for (const auto &add : v) {
        if (!first)
            stream << ',';
        stream << add;
        first = false;
    }
    stream << ')';
    return stream;
}

template<class Key, class T, class Compare, class Allocator>
QDebug operator<<(QDebug stream, const std::map<Key, T, Compare, Allocator> &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    bool first = true;
    stream << "std::map(";
    for (const auto &add : v) {
        if (!first)
            stream << ',';
        stream << '{' << add.first << ',' << add.second << '}';
        first = false;
    }
    stream << ')';
    return stream;
}

#endif

#if QT_VERSION < QT_VERSION_CHECK(5, 5, 0)
#define qCInfo(x)   qCDebug(x)
#endif

namespace CPD3 {

namespace Logging {

class time;

CPD3CORE_EXPORT QDebug operator<<(QDebug stream, const time &value);

/**
 * A wrapper that causes a time to be output with formatting in a debug stream.
 */
class CPD3CORE_EXPORT time final {
    double value;
public:
    explicit time(double value);

    friend CPD3CORE_EXPORT QDebug operator<<(QDebug stream, const time &value);
};


class range;

CPD3CORE_EXPORT QDebug operator<<(QDebug stream, const range &value);

/**
 * A wrapper that causes a time range to be output with formatting in a debug stream.
 */
class CPD3CORE_EXPORT range final {
    double start;
    double end;
public:
    explicit range(double start, double end);

    friend CPD3CORE_EXPORT QDebug operator<<(QDebug stream, const range &value);
};


class elapsed;

CPD3CORE_EXPORT QDebug operator<<(QDebug stream, const elapsed &value);

/**
 * A wrapper that causes an elapsed time to be output in a human readable fashion.
 */
class CPD3CORE_EXPORT elapsed final {
    double seconds;
public:
    explicit elapsed(double seconds);

    friend CPD3CORE_EXPORT QDebug operator<<(QDebug stream, const elapsed &value);
};

/**
 * Suppress debug output for test cases.
 */
CPD3CORE_EXPORT void suppressForTesting();

/**
 * Suppress all output for test cases.
 */
CPD3CORE_EXPORT void suppressGlobalForTesting();

/**
 * Suppress output for console (curses) applications.
 */
CPD3CORE_EXPORT void suppressForConsole();

}

namespace Serialize {

/**
 * Serialize a container by calling a functor on every element of it.
 *
 * @tparam Container    the container type
 * @tparam Serializer   the functor type
 * @param stream        the stream
 * @param container     the container to serialize
 * @param serializer    the serialization functor
 * @return              the stream
 */
template<typename Container, typename Serializer>
QDataStream &container(QDataStream &stream, const Container &container, Serializer serializer)
{
    Q_ASSERT(static_cast<quint32>(container.size() == container.size()));
    stream << static_cast<quint32>(container.size());
    for (const auto &add : container) {
        serializer(add);
    }
    return stream;
}

}

namespace Deserialize {

/**
 * Deserialize a container by calling a functor for every serialize element.
 *
 * @tparam Container    the container type
 * @tparam Deserializer the functor type
 * @param stream        the stream
 * @param container     the container to write into
 * @param deserializer  the deserizliation functor
 * @return              the stream
 */
template<typename Container, typename Deserializer>
QDataStream &container(QDataStream &stream, Container &container, Deserializer deserializer)
{
    quint32 n = 0;
    stream >> n;
    container.clear();
    for (quint32 i = 0; i < n; ++i) {
        container.push_back(deserializer());
    }
    return stream;
}

}

}

#ifdef QTEST_H

#define CPD3_QTEST

namespace CPD3 {

inline void appendTime(QByteArray &data, double time)
{
    if (!CPD3::FP::defined(time)) {
        data += "UNDEFINED";
        return;
    }
    data += QByteArray::number(time, 'f', 3);
}

inline void appendRange(QByteArray &data, double start, double end)
{
    data += '(';
    if (!CPD3::FP::defined(start)) {
        data += "-INFINITY";
    } else {
        data += QByteArray::number(start, 'f', 3);
    }
    data += ',';
    if (!CPD3::FP::defined(start)) {
        data += "+INFINITY";
    } else {
        data += QByteArray::number(end, 'f', 3);
    }
    data += ')';
}

}

#endif


#endif //CPD3_QTCOMPAT_HXX
