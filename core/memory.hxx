/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3_MEMORY_HXX
#define CPD3_MEMORY_HXX

#include "core/first.hxx"

#include "core/core.hxx"

namespace CPD3 {
namespace Memory {

typedef void (*ReleaseFunction)();

CPD3CORE_EXPORT void install_release_function(ReleaseFunction release);

CPD3CORE_EXPORT void release_unused();

}
}

#endif //CPD3_THREADING_HXX
