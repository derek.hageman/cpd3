/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include "core/abort.hxx"

namespace CPD3 {

void Abort::installAbortHandler(bool abortOnHangup)
{ Q_UNUSED(abortOnHangup); }

bool Abort::aborted()
{ return false; }

AbortPoller::AbortPoller(QObject *parent) : QObject(parent)
{ }

AbortPoller::~AbortPoller()
{ }

void AbortPoller::start()
{ }

void AbortPoller::stop()
{ }

void AbortPoller::checkAborted()
{ }

};
