/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3CORERANGE_H
#define CPD3CORERANGE_H

#include "core/first.hxx"

#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <iterator>
#include <type_traits>

#include <QtGlobal>
#include <QtAlgorithms>
#include <QDataStream>
#include <QList>
#include <QMetaType>

#include "core/core.hxx"
#include "core/number.hxx"

namespace CPD3 {

/**
 * Routines for working with half open ranges.  The start is inclusive while
 * the end is exclusive.  An undefined bound indicated infinity.
 * <br>
 * This complies with the standard conventions used for time ranges in CPD3.
 */
class CPD3CORE_EXPORT Range {
    template<typename RangeType>
    static inline bool undefinedLessThan(double value, const RangeType &r)
    {
        if (!FP::defined(r.getStart()))
            return false;
        return value < r.getStart();
    }

    template<typename RangeType>
    static inline bool undefinedLessThan(const RangeType &r, double value)
    {
        if (!FP::defined(r.getStart()))
            return true;
        return r.getStart() < value;
    }

    template<typename RandomAccessIterator, bool possibleUndefined>
    static RandomAccessIterator upperBoundInternal(RandomAccessIterator begin,
                                                   RandomAccessIterator end,
                                                   double time)
    {
        if (possibleUndefined && !FP::defined(time))
            return lastUndefinedStart(begin, end);

        RandomAccessIterator middle;
        int n = end - begin;
        int half;
        while (n > 0) {
            half = n >> 1;
            middle = begin + half;

            if (possibleUndefined) {
                if (undefinedLessThan(time, *middle)) {
                    n = half;
                } else {
                    begin = middle + 1;
                    n -= half + 1;
                }
            } else {
                if (time < middle->getStart()) {
                    n = half;
                } else {
                    begin = middle + 1;
                    n -= half + 1;
                }
            }
        }
        return begin;
    }

    template<typename RandomAccessIterator, bool possibleUndefined>
    static RandomAccessIterator lowerBoundInternal(RandomAccessIterator begin,
                                                   RandomAccessIterator end,
                                                   double time)
    {
        if (possibleUndefined && !FP::defined(time))
            return begin;

        RandomAccessIterator middle;
        int n = end - begin;
        int half;

        while (n > 0) {
            half = n >> 1;
            middle = begin + half;
            if (possibleUndefined) {
                if (undefinedLessThan(*middle, time)) {
                    begin = middle + 1;
                    n -= half + 1;
                } else {
                    n = half;
                }
            } else {
                if (middle->getStart() < time) {
                    begin = middle + 1;
                    n -= half + 1;
                } else {
                    n = half;
                }
            }
        }
        return begin;
    }

    template<typename RandomAccessIterator, bool possibleUndefined>
    static RandomAccessIterator heuristicUpperBoundInternal(RandomAccessIterator begin,
                                                            RandomAccessIterator end,
                                                            double time)
    {
        if (begin == end)
            return end;
        /* Common case: the entire list is after the start time */
        if (possibleUndefined) {
            if (FP::defined(begin->getStart()) &&
                    (!FP::defined(time) || begin->getStart() > time)) {
                return begin;
            }
        } else {
            if (begin->getStart() > time)
                return begin;
        }

        RandomAccessIterator middle = begin + 1;
        /* Only one possible value. */
        if (middle == end)
            return end;

        /* Common case: one value that matches */
        if (possibleUndefined) {
            if (FP::defined(middle->getStart()) &&
                    (!FP::defined(time) || middle->getStart() > time))
                return middle;
        } else {
            if (middle->getStart() > time)
                return middle;
        }
        /* Common case: check the end of the list */
        if (possibleUndefined) {
            if (!FP::defined((end - 1)->getStart()))
                return end;
            if (FP::defined(time) && (end - 1)->getStart() <= time)
                return end;
        } else {
            if ((end - 1)->getStart() <= time)
                return end;
        }

        /* Otherwise do binary expansion from the start of the list to the
         * first value that's greater than the time.  Then do a normal
         * upper bound within that bracket */

        int n = 1;
        int size = end - begin;
        for (;;) {
            n <<= 1;
            if (n >= size) {
                return upperBoundInternal<RandomAccessIterator, possibleUndefined>(middle, end,
                                                                                   time);
            }
            middle = begin + n;
            if (possibleUndefined) {
                if (FP::defined(middle->getStart())) {
                    if (!FP::defined(time)) {
                        return lastUndefinedStart(begin + (n >> 1), middle);
                    } else if (middle->getStart() > time) {
                        return upperBoundInternal<RandomAccessIterator, possibleUndefined>(
                                begin + (n >> 1), middle, time);
                    }
                }
            } else {
                if (middle->getStart() > time) {
                    return upperBoundInternal<RandomAccessIterator, possibleUndefined>(
                            begin + (n >> 1), middle, time);
                }
            }
        }
    }

    template<typename RandomAccessIterator, bool possibleUndefined>
    static RandomAccessIterator heuristicLowerBoundInternal(RandomAccessIterator begin,
                                                            RandomAccessIterator end,
                                                            double time)
    {
        if (begin == end)
            return end;
        /* Short circuit this here, this is always the start of the list */
        if (possibleUndefined && !FP::defined(time))
            return begin;

        /* Common case: time is after the whole list */
        RandomAccessIterator middle = end - 1;
        if (possibleUndefined) {
            if (!FP::defined(middle->getStart()) || middle->getStart() < time)
                return end;
        } else {
            if (middle->getStart() < time)
                return end;
        }

        /* Common case: only a single value in the list */
        if (middle == begin)
            return begin;

        /* Common case: one value matches */
        --middle;
        if (possibleUndefined) {
            if (!FP::defined(middle->getStart()) || middle->getStart() < time)
                return middle + 1;
        } else {
            if (middle->getStart() < time)
                return middle + 1;
        }

        /* Common case: check the start of the list */
        if (possibleUndefined) {
            if (FP::defined(begin->getStart()) && begin->getStart() >= time)
                return begin;
        } else {
            if (begin->getStart() >= time)
                return begin;
        }

        /* Otherwise do a boundary expansion until we bracket the block
         * containing the target, then just binary search on that */

        int n = 1;
        int size = end - begin;
        for (;;) {
            n <<= 1;
            if (n >= size) {
                return lowerBoundInternal<RandomAccessIterator, possibleUndefined>(begin, middle,
                                                                                   time);
            }
            middle = end - (n + 1);
            if (possibleUndefined) {
                if (!FP::defined(middle->getStart()) || middle->getStart() < time) {
                    return lowerBoundInternal<RandomAccessIterator, possibleUndefined>(middle, end -
                            ((n >> 1) + 1), time);
                }
            } else {
                if (middle->getStart() < time) {
                    return lowerBoundInternal<RandomAccessIterator, possibleUndefined>(middle, end -
                            ((n >> 1) + 1), time);
                }
            }
        }
    }

    template<typename RandomAccessIterator, typename BoundFunctor>
    static inline std::pair<RandomAccessIterator, RandomAccessIterator> findAllIntersectingInternal(
            RandomAccessIterator begin,
            RandomAccessIterator end,
            double intersectBegin,
            double intersectEnd,
            BoundFunctor findUpperBound,
            BoundFunctor findLowerBound)
    {
        Q_ASSERT(compareStartEnd(intersectBegin, intersectEnd) <= 0);
        if (begin == end)
            return std::make_pair(begin, end);

        /* Locate last possible segment that could intersect: the last one
         * with a start time after or equal to then end of the range */
        auto effectiveEnd = end;
        if (FP::defined(intersectEnd)) {
            effectiveEnd = findUpperBound(begin, end, intersectEnd);

            /* If we hit the end exactly, move back one, since the
             * upper bound is past the one that can't intersect */
            if (effectiveEnd != begin) {
                auto check = effectiveEnd - 1;
                if (check->getStart() == intersectEnd) {
                    effectiveEnd = check;
                }
            }
        }
        /* The first segment has a start time after or equal to the end
         * intersecting time, so no intersection is possible */
        if (effectiveEnd == begin) {
            Q_ASSERT(!intersects(intersectBegin, intersectEnd, begin->getStart(), begin->getEnd()));
            return std::make_pair(begin, begin);
        }

        /* Locate the first segment with a start time before or equal
         * to the intersection start */
        auto effectiveStart = findLowerBound(begin, effectiveEnd, intersectBegin);

        /* If we're not already at the start, we might need to step back one, if
         * we didn't hit exactly.  The bound will be the one after, but we need the
         * one before, since we'll still intersect it. */
        if (effectiveStart != begin) {
            auto check = effectiveStart - 1;
            if (compareStartEnd(intersectBegin, check->getEnd()) < 0)
                effectiveStart = check;
        }

        /* If we're not already at the end, we might need to step forward one,
         * since the end is exclusive. */
        if (effectiveEnd != end) {
            if (compareStartEnd(effectiveEnd->getStart(), intersectEnd) < 0) {
                ++effectiveEnd;
            }
        }

        /* Between segments entirely */
        if (effectiveStart == effectiveEnd) {
            Q_ASSERT(effectiveStart == end ||
                             !intersects(intersectBegin, intersectEnd, effectiveStart->getStart(),
                                         effectiveStart->getEnd()));
            return std::make_pair(effectiveStart, effectiveEnd);
        }

        Q_ASSERT(intersects(intersectBegin, intersectEnd, effectiveStart->getStart(),
                            effectiveStart->getEnd()));
        Q_ASSERT(intersects(intersectBegin, intersectEnd, (effectiveEnd - 1)->getStart(),
                            (effectiveEnd - 1)->getEnd()));

#ifndef NDEBUG
        for (auto check = effectiveStart; check != effectiveEnd; ++check) {
            Q_ASSERT(intersects(intersectBegin, intersectEnd, check->getStart(), check->getEnd()));
        }
#endif

        return std::make_pair(effectiveStart, effectiveEnd);
    }

public:
    /**
     * Compare two (possibly undefined) numbers as the start of a time range.
     * Returns -1, 0, or 1 if d1 is less than d2, they are equal or d1 is greater
     * than d2, respectively.
     * 
     * @param d1    the first start bound
     * @param d2    the second start bound
     * @return      the comparison result, -1, 0, or 1
     */
    static inline int compareStart(double d1, double d2)
    {
        if (!FP::defined(d1)) {
            if (FP::defined(d2))
                return -1;
            return 0;
        }
        if (!FP::defined(d2))
            return 1;
        if (d1 < d2) return -1;
        if (d1 == d2) return 0;
        return 1;
    }

    /**
     * Compare two (possibly undefined) numbers as the end of a time range.
     * Returns -1, 0, or 1 if d1 is less than d2, they are equal or d1 is greater
     * than d2, respectively.
     * 
     * @param d1    the first start bound
     * @param d2    the second start bound
     * @return      the comparison result, -1, 0, or 1
     */
    static inline int compareEnd(double d1, double d2)
    {
        if (!FP::defined(d1)) {
            if (FP::defined(d2))
                return 1;
            return 0;
        }
        if (!FP::defined(d2))
            return -1;
        if (d1 < d2) return -1;
        if (d1 == d2) return 0;
        return 1;
    }

    /**
     * Compare a start time with an end time (both possibly undefined).  Returns
     * -1, 0, or 1 if the start is before, equal to, or after the end.
     * 
     * @param start     the start bound
     * @param end       the end bound
     * @return          the comparison result, -1, 0, or 1
     */
    static inline int compareStartEnd(double start, double end)
    {
        if (!FP::defined(end) || !FP::defined(start))
            return -1;
        if (start < end) return -1;
        if (start == end) return 0;
        return 1;
    }

    /**
     * Test if two ranges intersect one another.
     * 
     * @param s1        the start of the first range
     * @param e1        the end of the first range
     * @param s2        the start of the second range
     * @param e2        the end of the second range
     */
    static inline bool intersects(double s1, double e1, double s2, double e2)
    {
        if (FP::defined(e1) && FP::defined(s2)) {
            if (s2 >= e1)
                return false;
        }
        if (FP::defined(e2) && FP::defined(s1)) {
            if (s1 >= e2)
                return false;
        }
        return true;
        /*if (compareStartEnd(s2, e1) >= 0)
            return false;
        if (compareStartEnd(s1, e2) >= 0)
            return false;
        return true;*/
    }

    static int notWithinStart(double checkStart,
                              double start,
                              double end,
                              bool inclusiveStart = true,
                              bool inclusiveEnd = false);

    static int notWithinEnd(double checkEnd,
                            double start,
                            double end,
                            bool inclusiveStart = true,
                            bool inclusiveEnd = false);

    /**
     * A simple comparison of start time for sorting.
     *
     * @param a     the first element
     * @param b     the second element
     * @return      true if the first is less than the second
     */
    template<typename T>
    static inline double startLessThan(const T &a, const T &b)
    {
        double sa = a.getStart();
        double sb = b.getStart();
        if (!FP::defined(sa))
            return FP::defined(sb);
        else if (!FP::defined(sb))
            return false;
        return sa < sb;
    }

    /**
     * A simple comparison of start time for sorting of only defined start times.
     *
     * @param a     the first element
     * @param b     the second element
     * @return      true if the first is less than the second
     */
    template<typename T>
    static inline double startLessThanDefined(const T &a, const T &b)
    { return a.getStart() < b.getStart(); }


    /**
     * Find the lower bound for a given start in a list of sorted segments.
     * If there is an exact match in the search this will always return that,
     * overriding the conditions below.  Will return the end iterator if the 
     * start is after the last (real) segment, even if that segment would 
     * "really" be the lower bound.  Similarly will return the start iterator 
     * if the start is  before the first (real) segment.
     * <br>
     * These segments must be non-zero length or the results will be 
     * undefined.
     * 
     * @param start     the start of the list to search
     * @param end       the end of the list to search
     * @param s         the start to search for
     * @return          the lower bounding segment
     */
    template<typename RandomAccessIterator>
    static RandomAccessIterator findLowerBound(RandomAccessIterator start,
                                               RandomAccessIterator end,
                                               double s)
    {
        if (start == end) return end;

        int cr = Range::compareStart(s, start->getStart());
        if (cr <= 0) return start;

        --end;
        if (start == end) return end + 1;

        cr = Range::compareStart(s, end->getStart());
        if (cr > 0) return end + 1;
        if (cr == 0) return end;

        while (start != end) {
            int off = ((end - start) + 1) >> 1;
            if (off == 0) break;
            RandomAccessIterator mid = start + off;

            cr = Range::compareStart(s, mid->getStart());

            if (cr == 0) return mid;
            if (cr < 0) end = mid - 1; else start = mid + 1;
        }
        cr = Range::compareStart(s, start->getStart());
        if (cr < 0) return start - 1;
        return start;
    }

    /**
     * Same as findLowerBound(container.begin(), container.end(), start)
     * @see findLowerBound( RandomAccessIterator, RandomAccessIterator, double )
     */
    template<typename Container>
    static inline typename Container::const_iterator findLowerBound(const Container &container,
                                                                    double start)
    {
        return findLowerBound(container.begin(), container.end(), start);
    }


    /**
     * Shift a list of sorted segments until the first one either intersects the
     * given range or is before it.  If this function returns true then the
     * first segment intersects the range, if it returns false then it does not.
     * 
     * @param list      the list to shift
     * @param start     the start of the range to check
     * @param end       the end of the range to check
     * @return          true if the first segment was made to intersect
     */
    template<typename ListType>
    static bool intersectShift(ListType &list, double start, double end)
    {
        while (!list.empty() && Range::compareStartEnd(start, list.front().getEnd()) >= 0) {
            list.pop_front();
        }
        if (list.empty())
            return false;
        if (Range::compareStartEnd(list.front().getStart(), end) >= 0)
            return false;
        return true;
    }

    /**
     * Same as intersectShift(list, intersect.getStart(), intersect.getEnd())
     * @see intersectShift(ListType &, double, double)
     */
    template<typename ListType, typename RangeType>
    static bool intersectShift(ListType &list, const RangeType &intersect)
    {
        return intersectShift(list, intersect.getStart(), intersect.getEnd());
    }

    /**
     * Shift a sorted list of segments until the first one is either after the
     * given point or contains it.  If this function returns true then the point
     * is in the first segment.
     * 
     * @param list      the list to shift
     * @param point     the point to intersect
     * @return          true if the first segment was made to intersect
     */
    template<typename ListType>
    static bool intersectShift(ListType &list, double point)
    {
        while (!list.empty() && Range::compareStartEnd(point, list.front().getEnd()) >= 0) {
            list.pop_front();
        }
        if (list.empty())
            return false;
        if (Range::compareStart(point, list.front().getStart()) < 0)
            return false;
        return true;
    }
    template<typename ElementType>
    static bool intersectShift(std::vector<ElementType> &list, double point)
    {
        while (!list.empty() && Range::compareStartEnd(point, list.front().getEnd()) >= 0) {
            list.erase(list.begin());
        }
        if (list.empty())
            return false;
        if (Range::compareStart(point, list.front().getStart()) < 0)
            return false;
        return true;
    }

    /**
     * Find the segment in a sorted list that intersects the given range.
     * 
     * @param begin             the start to search
     * @param end               the end to search
     * @param intersectStart    the start of the range to check
     * @param intersectEnd      the end of the range to check
     * @return                  the intersecting position or end if none
     */
    template<typename RandomAccessIterator>
    static RandomAccessIterator findIntersecting(RandomAccessIterator begin,
                                                 RandomAccessIterator end,
                                                 double intersectStart,
                                                 double intersectEnd)
    {
        if (begin == end) return end;

        RandomAccessIterator lb = findLowerBound(begin, end, intersectStart);
        if (lb == end) {
            --lb;
            if (Range::compareStartEnd(intersectStart, lb->getEnd()) >= 0)
                return end;
            return lb;
        }
        if (lb == begin) {
            if (Range::compareStartEnd(lb->getStart(), intersectEnd) >= 0)
                return end;
        }
        if (Range::compareStartEnd(intersectStart, lb->getEnd()) < 0)
            return lb;
        ++lb;
        if (lb == end)
            return end;
        if (Range::compareStartEnd(lb->getStart(), intersectEnd) < 0)
            return lb;
        return end;
    }

    /**
     * Same as findIntersecting(begin, end, intersect.getStart(), intersect.getEnd())
     * @see findIntersecting(RandomAccessIterator, RandomAccessIterator, double, double)
     */
    template<typename RandomAccessIterator, typename RangeType>
    static RandomAccessIterator findIntersecting(RandomAccessIterator begin,
                                                 RandomAccessIterator end,
                                                 const RangeType &intersect)
    {
        return findIntersecting(begin, end, intersect.getStart(), intersect.getEnd());
    }

    /**
     * Same as findIntersecting(container.begin(), container.end(), intersect.getStart(), intersect.getEnd())
     * @see findIntersecting(RandomAccessIterator, RandomAccessIterator, double, double)
     */
    template<typename Container, typename RangeType>
    static inline typename Container::const_iterator findIntersecting(const Container &container,
                                                                      const RangeType &intersect)
    {
        return findIntersecting(container.begin(), container.end(), intersect.getStart(),
                                intersect.getEnd());
    }

    /**
     * Same as findIntersecting(container.begin(), container.end(), start, end)
     * @see findIntersecting(RandomAccessIterator, RandomAccessIterator, double, double)
     */
    template<typename Container>
    static typename Container::const_iterator findIntersecting(const Container &container,
                                                               double start,
                                                               double end)
    {
        return findIntersecting(container.begin(), container.end(), start, end);
    }

    /**
     * Find the segment in a sorted list that contains the given point.
     * 
     * @param begin     the start to search
     * @param end       the end to search
     * @param point     the point to find
     * @return          the intersecting position or end if none
     */
    template<typename RandomAccessIterator>
    static RandomAccessIterator findIntersecting(RandomAccessIterator begin,
                                                 RandomAccessIterator end,
                                                 double point)
    {
        if (begin == end) return end;

        RandomAccessIterator lb = findLowerBound(begin, end, point);
        if (lb == begin) {
            if (Range::compareStart(point, lb->getStart()) < 0)
                return end;
        }
        if (lb == end)
            --lb;
        if (Range::compareStartEnd(point, lb->getEnd()) >= 0)
            return end;
        return lb;
    }

    /**
     * Same as findIntersecting(container.begin(), container.end(), point)
     * @see angeFindIntersecting( RandomAccessIterator, RandomAccessIterator, double )
     */
    template<typename Container>
    static inline typename Container::const_iterator findIntersecting(const Container &container,
                                                                      double point)
    {
        return findIntersecting(container.begin(), container.end(), point);
    }

    /**
     * Get the insertion position into a sorted segment list for a range
     * such that it does not intersect any existing segments.
     *
     * @param begin             the start of the list
     * @param end               the end of the list
     * @param segmentStart      the start of the insertion range
     * @param segmentEnd        the end of the insertion range
     * @param position          the output position to insert before
     * @return true             if position is valid and the segment can be inserted
     */
    template<typename RandomAccessIterator>
    static bool insertionPosition(RandomAccessIterator begin,
                                  RandomAccessIterator end,
                                  double segmentStart,
                                  double segmentEnd,
                                  RandomAccessIterator *position = NULL)
    {
        Q_ASSERT(Range::compareStartEnd(segmentStart, segmentEnd) < 0);

        if (begin == end) {
            if (position != NULL)
                *position = end;
            return true;
        }

        /* Ends completely before the first one */
        if (Range::compareStartEnd(begin->getStart(), segmentEnd) >= 0) {
            if (position != NULL)
                *position = begin;
            return true;
        }

        RandomAccessIterator lb = findLowerBound(begin, end, segmentStart);
        if (lb == end) {
            /* At the end, so check if it intersects with the last and if not, insert
             * it on the tail */
            --lb;
            if (Range::compareStartEnd(segmentStart, lb->getEnd()) < 0)
                return false;
            if (position != NULL)
                *position = end;
            return true;
        }

        Q_ASSERT(lb != begin || Range::compareStartEnd(lb->getStart(), segmentEnd) < 0);

        /* The position is equal to or before the current start, so check if the
         * incoming start is before its end and fail if it is */
        if (Range::compareStartEnd(segmentStart, lb->getEnd()) < 0)
            return false;

        /* Now check the next one (we need to be able to fit before it) */
        ++lb;
        if (lb == end) {
            if (position != NULL)
                *position = end;
            return true;
        }
        if (Range::compareStartEnd(lb->getStart(), segmentEnd) < 0)
            return false;

        if (position != NULL)
            *position = lb;
        return true;
    }

    /**
     * Insert a segment into a sorted segment list only if it does not
     * intersect any of the existing contents.  Preserves the sorting of the
     * list, so this function can be called multiple times to insert multiple
     * segments.
     *
     * @param list          the input and output list to add the new segment to
     * @param n             the value to add
     * @return              true if the insertion was performed
     */
    template<typename Container, typename AddType>
    static bool insertNotOverlapping(Container &list, const AddType &n)
    {
        typename Container::iterator position;
        if (!insertionPosition(list.begin(), list.end(), n.getStart(), n.getEnd(), &position))
            return false;
        list.insert(position, n);
        return true;
    }

    /**
     * Overlay a range on top of a sorted list.  Preserves the sorting of the
     * list, so this function can be called multiple times to overlay multiple
     * segments.
     * <br>
     * The type in the list must provide the following functions/constructors:
     * <ul>
     *  <li> Constructor: ListType(const ListType &other, double start, double end) -
     *       To create a new item with the same contents as the other item but 
     *       with the given start and end.
     *  <li> Constructor: ListType(const AddType &other, double start, double end) -
     *       To create a new item with the same contents as the other item but 
     *       with the given start and end.
     *  <li> Constructor: ListType(const ListType &under, const AddType &over, double start, double end) -
     *       Create a new item with the contents of "over" overlaid on top of 
     *       the contents of "under" with the given start and end.
     *  <li> Function: setStart(double start) - Set the start of the item.
     *  <li> Function: setEnd(double end) - Set the end of the item.
     * </ul>
     * Note that ListType and AddType may be the same, in which case the first 
     * two constructors are identical (and so two are not needed).
     * <br>
     * Both types must provide the functions "getStart()" and "getEnd()" to 
     * access their start and end.
     * 
     * @param list      the input and output list to add the new segment to
     * @param n         the new segment to overlay on top of the list
     */
    template<typename Container, typename AddType>
    static void overlayFragmenting(Container &list, const AddType &n)
    {
        typedef typename std::remove_reference<decltype(list.front())>::type ListType;

        Q_ASSERT(Range::compareStartEnd(n.getStart(), n.getEnd()) < 0);
        if (list.empty()) {
            list.push_back(ListType(n, n.getStart(), n.getEnd()));
            return;
        }

        /* Ends completely before the first one */
        if (Range::compareStartEnd(list.front().getStart(), n.getEnd()) >= 0) {
            list.push_front(ListType(n, n.getStart(), n.getEnd()));
            return;
        }


        auto lb = findLowerBound(list.begin(), list.end(), n.getStart());
        if (lb == list.end()) {
            double le = list.back().getEnd();

            /* Starts completely after the back */
            if (Range::compareStartEnd(n.getStart(), le) >= 0) {
                list.push_back(ListType(n, n.getStart(), n.getEnd()));
                return;
            }

            /* Starts in the middle of the back */
            int cr = Range::compareEnd(le, n.getEnd());
            list.back().setEnd(n.getStart());
            if (cr == 0) {
                list.push_back(ListType(list.back(), n, n.getStart(), le));
            } else if (cr > 0) {
                list.push_back(ListType(list.back(), n, n.getStart(), n.getEnd()));
                list.push_back(ListType(list.at(list.size() - 2), n.getEnd(), le));
            } else {
                list.push_back(ListType(list.back(), n, n.getStart(), le));
                list.push_back(ListType(n, le, n.getEnd()));
            }
            return;
        }

        double es = n.getStart();
        double ee = n.getEnd();
        double lbs = lb->getStart();
        double lbe = lb->getEnd();
        int cr = Range::compareStart(es, lbs);

        /* Has a part before */
        if (cr < 0) {
            lb = list.insert(lb, ListType(n, es, lbs));
            ++lb;
            es = lbs;
        } else if (cr > 0) {
            cr = Range::compareStartEnd(es, lbe);
            if (cr < 0) {
                /* Starts in the middle of the first, so split */
                lb->setEnd(es);
                ++lb;

                cr = Range::compareEnd(ee, lbe);
                if (cr < 0) {
                    /* Ends entirely in this segment, so fragment it and be 
                     * done */
                    lb = list.insert(lb, ListType(*(lb - 1), n, es, ee));
                    list.insert(lb + 1, ListType(*(lb - 1), ee, lbe));
                    return;
                }

                /* Ends after this segment */
                lb = list.insert(lb, ListType(*(lb - 1), n, es, lbe));
                if (cr == 0) {
                    /* Ends at the same time, so no further possible fragments 
                     */
                    return;
                }
                ++lb;
                es = lbe;
                lbs = lb->getStart();

                if (Range::compareStartEnd(lbs, ee) >= 0) {
                    /* Fits completely between, so don't fragment anything */
                    lb = list.insert(lb, ListType(n, es, ee));
                    return;
                }

                if (Range::compareStartEnd(es, lbs) < 0) {
                    /* Ends sometime after the segment, so add a filler to the 
                     * start */
                    lb = list.insert(lb, ListType(n, es, lbs));
                    ++lb;
                    es = lbs;
                    lbs = lb->getStart();
                }
            } else {
                /* Starts after the end of the first, so add a filler */
                ++lb;
                lbs = lb->getStart();

                if (Range::compareStartEnd(lbs, ee) >= 0) {
                    /* Fits completely between, so don't fragment anything */
                    lb = list.insert(lb, ListType(n, es, ee));
                    return;
                }

                /* Ends sometime after the segment, so add a filler to the 
                 * start */
                lb = list.insert(lb, ListType(n, es, lbs));
                ++lb;
                es = lbs;
                lbs = lb->getStart();
            }
        }

        for (;;) {
            lbe = lb->getEnd();

            cr = Range::compareEnd(ee, lbe);

            /* Ends in the middle of this segment */
            if (cr < 0) {
                lb->setStart(ee);
                list.insert(lb, ListType(*lb, n, es, ee));
                return;
            } else if (cr == 0) {
                *lb = ListType(*lb, n, lbs, lbe);
                return;
            }

            *lb = ListType(*lb, n, es, lbe);
            ++lb;
            es = lbe;

            /* Finished the list, so this segment has a tail */
            if (lb == list.end()) {
                list.push_back(ListType(n, es, n.getEnd()));
                return;
            }
            lbs = lb->getStart();

            cr = Range::compareStart(es, lbs);

            /* Has a gap before the next, so fill it */
            if (cr < 0) {
                cr = Range::compareStartEnd(lbs, ee);

                /* Ends before the start of the next */
                if (cr >= 0) {
                    list.insert(lb, ListType(n, es, ee));
                    return;
                }

                /* Otherwise fill until the start of the next */
                lb = list.insert(lb, ListType(n, es, lbs));
                ++lb;
            }
            es = lbs;
        }
    }

    /**
     * Overlay an instantaneous event on top of a sorted segment list.  
     * Preserves the sorting of the list, so this function can be called 
     * multiple times to overlay multiple segments.
     * <br>
     * The type in the list must provide the following functions/constructors:
     * <ul>
     *  <li> Constructor: ListType(const ListType &other, double start, double end) -
     *       To create a new item with the same contents as the other item but 
     *       with the given start and end.
     *  <li> Constructor: ListType(const AddType &other, double start, double end) -
     *       To create a new item with the same contents as the other item but 
     *       with the given start and end.
     *  <li> Constructor: ListType(ListType &under, const AddType &over, double start, double end) -
     *       Create a new item with the contents of "over" overlaid on top of 
     *       the contents of "under" with the given start and end.
     *  <li> Function: setStart(double start) - Set the start of the item.
     *  <li> Function: setEnd(double end) - Set the end of the item.
     * </ul>
     * Note that ListType and AddType may be the same, in which case the first 
     * two constructors are identical (and so two are not needed).
     * <br>
     * The list type must provide the functions "getStart()" and "getEnd()" to 
     * access its start and end.  The AddType must provide the function 
     * "getTime()" to access its time (must be defined).
     * <br>
     * NOTE: After using this function it is no longer safe to use normal
     * segment fragmenting, so event merging must be done after all
     * fragmenting.  This is because this function can produce zero length
     * segments which normal segmenting does not understand.
     * 
     * @param list      the input and output list to add the new segment to
     * @param n         the new event to overlay on top of the list
     * @param merge     if set then the event will be merged into existing segments instead of always fragmenting them
     */
    template<typename Container, typename AddType>
    static void overlayEvent(Container &list, const AddType &n, bool merge = true)
    {
        typedef typename std::remove_reference<decltype(list.front())>::type ListType;

        double time = n.getTime();
        Q_ASSERT(FP::defined(time));
        if (list.empty()) {
            list.push_back(ListType(n, time, time));
            return;
        }

        /* Before the first */
        if (Range::compareStart(list.front().getStart(), time) > 0) {
            list.push_front(ListType(n, time, time));
            return;
        }

        /* After the last */
        int cr = Range::compareEnd(list.back().getEnd(), time);
        if (cr <= 0) {
            /* Exactly equal to the last, so merge */
            if (cr == 0 && FP::equal(list.back().getStart(), time)) {
                list.back() = ListType(list.back(), n, time, time);
                return;
            }
            list.push_back(ListType(n, time, time));
            return;
        }

        auto lb = lowerBound(list.begin(), list.end(), time);
        if (lb != list.begin() && lb == list.end())
            --lb;

        cr = Range::compareStart(lb->getStart(), time);
        if (cr == 0) {
            /* Equal start time, so merge into the existing if it's
             * already an event or insert a new event and done */
            if (merge || FP::equal(lb->getEnd(), time)) {
                *lb = ListType(*lb, n, lb->getStart(), lb->getEnd());
            } else {
                list.insert(lb, ListType(*lb, n, time, time));
            }
            return;
        } else if (cr > 0) {
            Q_ASSERT(lb != list.begin());
            /* Walk back one, so we're pointing at the segment that
             * might overlap with this event */
            --lb;
        }
        Q_ASSERT(!FP::equal(lb->getStart(), time));

        cr = Range::compareEnd(lb->getEnd(), time);
        if (cr <= 0) {
            /* In a space between segments, so insert a new one */
            Q_ASSERT((lb + 1) == list.end() || Range::compareStart((lb + 1)->getStart(), time) > 0);
            list.insert(lb + 1, ListType(n, time, time));
            return;
        }
        Q_ASSERT(!FP::equal(lb->getEnd(), time));

        /* We have the pointer to the segment that the event is in the middle
         * of now */

        if (merge) {
            *lb = ListType(*lb, n, lb->getStart(), lb->getEnd());
            return;
        }

        /* Insert a new second half after the event */
        lb = list.insert(lb + 1, ListType(*lb, time, lb->getEnd()));
        /* Insert the new overlapped event in the middle */
        lb = list.insert(lb, ListType(*lb, n, time, time));
        /* Update the original segment's end time */
        (lb - 1)->setEnd(time);
    }

    /**
     * Find the upper bound of the undefined start times in the given
     * sorted list.  The input list is required to be sorted by start time
     * (meaning all undefined starts are at the start of the list).
     * <br>
     * This returns the item after the last undefined start, or the start of
     * the list if there are none.
     * <br>
     * The list items are required to implement getStart().
     * 
     * @param start the start of the list
     * @param end   the end of the list
     * @return      the item after the last undefined start time
     */
    template<typename RandomAccessIterator>
    static RandomAccessIterator lastUndefinedStart(RandomAccessIterator begin,
                                                   RandomAccessIterator end)
    {
        /* Short circuit the trivial cases */
        if (begin == end)
            return end;
        if (FP::defined(begin->getStart()))
            return begin;
        if (!FP::defined((end - 1)->getStart()))
            return end;

        RandomAccessIterator middle;
        int n = end - begin;
        int half;

        while (n > 0) {
            half = n >> 1;
            middle = begin + half;
            if (FP::defined(middle->getStart())) {
                n = half;
            } else {
                begin = middle + 1;
                n -= half + 1;
            }
        }
        return begin;
    }

    /**
     * Same as lastUndefinedStart(container.begin(), container.end())
     * @see lastUndefinedStart( RandomAccessIterator, RandomAccessIterator )
     */
    template<typename Container>
    static inline typename Container::const_iterator lastUndefinedStart(const Container &container)
    {
        return lastUndefinedStart(container.begin(), container.end());
    }

    /**
     * Find the upper bound of the given time within a start time sorted
     * list of items.
     * <br>
     * This returns the item after the last one in the list with a start time 
     * equal to the given time or the position it should be inserted.
     * <br>
     * The list items are required to implement getStart().
     * 
     * @param start the start of the list
     * @param end   the end of the list
     * @param time  the time to search for
     */
    template<typename RandomAccessIterator>
    static inline RandomAccessIterator upperBound(RandomAccessIterator begin,
                                                  RandomAccessIterator end,
                                                  double time)
    {
        return upperBoundInternal<RandomAccessIterator, true>(begin, end, time);
    }

    /**
     * Same as upperBound(container.begin(), container.end(), time)
     * @see upperBound( RandomAccessIterator, RandomAccessIterator, double )
     */
    template<typename Container>
    static inline typename Container::const_iterator upperBound(const Container &container,
                                                                double time)
    {
        return upperBound(container.begin(), container.end(), time);
    }

    /**
     * Find the upper bound of the given time within a start time sorted
     * list of items.  This requires all start times to be defined but is
     * slightly faster.
     * <br>
     * This returns the item after the last one in the list with a start time 
     * equal to the given time or the position it should be inserted.
     * <br>
     * The list items are required to implement getStart().
     * 
     * @param possibleUndefined if set then undefined values are accepted for any times which is slightly slower
     * @param start the start of the list
     * @param end   the end of the list
     * @param time  the time to search for
     */
    template<typename RandomAccessIterator>
    static inline RandomAccessIterator upperBoundDefined(RandomAccessIterator begin,
                                                         RandomAccessIterator end,
                                                         double time)
    {
        return upperBoundInternal<RandomAccessIterator, false>(begin, end, time);
    }

    /**
     * Same as upperBoundDefined(container.begin(), container.end(), time)
     * @see upperBoundDefined( RandomAccessIterator, RandomAccessIterator, double )
     */
    template<typename Container>
    static inline typename Container::const_iterator upperBoundDefined(const Container &container,
                                                                       double time)
    {
        return upperBoundDefined(container.begin(), container.end(), time);
    }


    /**
     * Find the lower bound of the given time within a start time sorted
     * list of items.
     * <br>
     * This returns the first item in the list with a start time equal to the
     * given time or the position it should be inserted.
     * <br>
     * The list items are required to implement getStart().
     * 
     * @param start the start of the list
     * @param end   the end of the list
     * @param time  the time to search for
     */
    template<typename RandomAccessIterator>
    static inline RandomAccessIterator lowerBound(RandomAccessIterator begin,
                                                  RandomAccessIterator end,
                                                  double time)
    {
        return lowerBoundInternal<RandomAccessIterator, true>(begin, end, time);
    }

    /**
     * Same as lowerBound(container.begin(), container.end(), time)
     * @see lowerBound( RandomAccessIterator, RandomAccessIterator, double )
     */
    template<typename Container>
    static inline typename Container::const_iterator lowerBound(const Container &container,
                                                                double time)
    { return lowerBound(container.begin(), container.end(), time); }

    /**
     * Find the lower bound of the given time within a start time sorted
     * list of items.  This requires all start times to be defined but is
     * slightly faster.
     * <br>
     * This returns the first item in the list with a start time equal to the
     * given time or the position it should be inserted.
     * <br>
     * The list items are required to implement getStart().
     * 
     * @param start the start of the list
     * @param end   the end of the list
     * @param time  the time to search for
     */
    template<typename RandomAccessIterator>
    static inline RandomAccessIterator lowerBoundDefined(RandomAccessIterator begin,
                                                         RandomAccessIterator end,
                                                         double time)
    {
        return lowerBoundInternal<RandomAccessIterator, false>(begin, end, time);
    }

    /**
     * Same as lowerBoundDefined(container.begin(), container.end(), time)
     * @see lowerBoundDefined( RandomAccessIterator, RandomAccessIterator, double )
     */
    template<typename Container>
    static inline typename Container::const_iterator lowerBoundDefined(const Container &container,
                                                                       double time)
    { return lowerBoundDefined(container.begin(), container.end(), time); }


    /**
     * Find the upper bound of the given time within a start time sorted
     * list of items.  This is distinct from upperBound(RandomAccessIterator,
     * RandomAccessIterator, double) in that it that it assumes that the
     * target value is near to the start of the list and will do better than
     * O(log N) if that is true.  The worst case algorithmic complexity is 
     * still O(log N), but it is more complex in logic so it will run
     * slower than a simple upper bound if the objective is not near the start
     * of the list.
     * <br>
     * This returns the item after the last one in the list with a start time 
     * equal to the given time or the position it should be inserted.
     * <br>
     * The list items are required to implement getStart().
     * 
     * @param start the start of the list
     * @param end   the end of the list
     * @param time  the time to search for
     */
    template<typename RandomAccessIterator>
    static inline RandomAccessIterator heuristicUpperBound(RandomAccessIterator begin,
                                                           RandomAccessIterator end,
                                                           double time)
    {
        return heuristicUpperBoundInternal<RandomAccessIterator, true>(begin, end, time);
    }

    /**
     * Same as upperBound(container.begin(), container.end(), time)
     * @see upperBound( RandomAccessIterator, RandomAccessIterator, double )
     */
    template<typename Container>
    static inline typename Container::const_iterator heuristicUpperBound(const Container &container,
                                                                         double time)
    {
        return heuristicUpperBound(container.begin(), container.end(), time);
    }

    /**
     * Find the upper bound of the given time within a start time sorted
     * list of items.  This is distinct from upperBound(RandomAccessIterator,
     * RandomAccessIterator, double) in that it that it assumes that the
     * target value is near to the start of the list and will do better than
     * O(log N) if that is true.  The worst case algorithmic complexity is 
     * still O(log N), but it is more complex in logic so it will run
     * slower than a simple upper bound if the objective is not near the start
     * of the list.  This requires all start times to be defined but is
     * slightly faster.
     * <br>
     * This returns the item after the last one in the list with a start time 
     * equal to the given time or the position it should be inserted.
     * <br>
     * The list items are required to implement getStart().
     * 
     * @param start the start of the list
     * @param end   the end of the list
     * @param time  the time to search for
     */
    template<typename RandomAccessIterator>
    static inline RandomAccessIterator heuristicUpperBoundDefined(RandomAccessIterator begin,
                                                                  RandomAccessIterator end,
                                                                  double time)
    {
        return heuristicUpperBoundInternal<RandomAccessIterator, false>(begin, end, time);
    }

    /**
     * Same as heuristicUpperBoundDefined(container.begin(), container.end(), time)
     * @see heuristicUpperBoundDefined( RandomAccessIterator, RandomAccessIterator, double )
     */
    template<typename Container>
    static inline typename Container::const_iterator heuristicUpperBoundDefined(const Container &container,
                                                                                double time)
    {
        return heuristicUpperBoundDefined(container.begin(), container.end(), time);
    }

    /**
     * Find the lower bound of the given time within a start time sorted
     * list of items.  This is distinct from lowerBound(RandomAccessIterator,
     * RandomAccessIterator, double) in that it that it assumes that the
     * target value is near to the end of the list and will do better than
     * O(log N) if that is true.  The worst case algorithmic complexity is 
     * still O(log N), but it is more complex in logic so it will run
     * slower than a simple lower bound if the objective is not near the end
     * of the list.
     * <br>
     * This returns the first item matching the given start time or the position
     * it should be inserted.
     * <br>
     * The list items are required to implement getStart().
     * 
     * @param start the start of the list
     * @param end   the end of the list
     * @param time  the time to search for
     */
    template<typename RandomAccessIterator>
    static inline RandomAccessIterator heuristicLowerBound(RandomAccessIterator begin,
                                                           RandomAccessIterator end,
                                                           double time)
    {
        return heuristicLowerBoundInternal<RandomAccessIterator, true>(begin, end, time);
    }

    /**
     * Same as lowerBound(container.begin(), container.end(), time)
     * @see lowerBound( RandomAccessIterator, RandomAccessIterator, double )
     */
    template<typename Container>
    static inline typename Container::const_iterator heuristicLowerBound(const Container &container,
                                                                         double time)
    {
        return heuristicLowerBound(container.begin(), container.end(), time);
    }

    /**
     * Find the lower bound of the given time within a start time sorted
     * list of items.  This is distinct from lowerBound(RandomAccessIterator,
     * RandomAccessIterator, double) in that it that it assumes that the
     * target value is near to the end of the list and will do better than
     * O(log N) if that is true.  The worst case algorithmic complexity is 
     * still O(log N), but it is more complex in logic so it will run
     * slower than a simple lower bound if the objective is not near the end
     * of the list.  This requires all start times to be defined but is
     * slightly faster.
     * <br>
     * This returns the first item matching the given start time or the position
     * it should be inserted.
     * <br>
     * The list items are required to implement getStart().
     * 
     * @param start the start of the list
     * @param end   the end of the list
     * @param time  the time to search for
     */
    template<typename RandomAccessIterator>
    static inline RandomAccessIterator heuristicLowerBoundDefined(RandomAccessIterator begin,
                                                                  RandomAccessIterator end,
                                                                  double time)
    {
        return heuristicLowerBoundInternal<RandomAccessIterator, false>(begin, end, time);
    }

    /**
     * Same as heuristicLowerBoundDefined(container.begin(), container.end(), time)
     * @see heuristicLowerBoundDefined( RandomAccessIterator, RandomAccessIterator, double )
     */
    template<typename Container>
    static inline typename Container::const_iterator heuristicLowerBoundDefined(const Container &container,
                                                                                double time)
    {
        return heuristicLowerBoundDefined(container.begin(), container.end(), time);
    }


    /**
     * Find the range of segments in a sorted list that intersect the specified
     * range.
     *
     * @param begin             the start of the list
     * @param end               the end of the list
     * @param intersectBegin    the start of the time range to find
     * @param intersectEnd      the end of the time range to find
     * @return                  the beginning and end of the intersecting segments
     */
    template<typename RandomAccessIterator>
    static std::pair<RandomAccessIterator, RandomAccessIterator> findAllIntersecting(
            RandomAccessIterator begin,
            RandomAccessIterator end,
            double intersectBegin,
            double intersectEnd)
    {
        return findAllIntersectingInternal(begin, end, intersectBegin, intersectEnd,
                                           upperBoundInternal<RandomAccessIterator, true>,
                                           lowerBoundInternal<RandomAccessIterator, true>);
    }

    /** @see findAllIntersecting(RandomAccessIterator, RandomAccessIterator, double, double) */
    template<typename Container>
    static inline std::pair<typename Container::const_iterator,
                            typename Container::const_iterator> findAllIntersecting(const Container &container,
                                                                                    double intersectBegin,
                                                                                    double intersectEnd)
    {
        return findAllIntersecting(container.begin(), container.end(), intersectBegin,
                                   intersectEnd);
    }

    /** @see findAllIntersecting(RandomAccessIterator, RandomAccessIterator, double, double) */
    template<typename Container, typename RangeType>
    static inline std::pair<typename Container::const_iterator,
                            typename Container::const_iterator> findAllIntersecting(const Container &container,
                                                                                    const RangeType &check)
    {
        return findAllIntersecting(container.begin(), container.end(), check.getStart(),
                                   check.getEnd());
    }

    /**
     * Find the range of segments in a sorted list that intersect the specified
     * range.  This assumes the target range is near the start of the possible segments
     *
     * @param begin             the start of the list
     * @param end               the end of the list
     * @param intersectBegin    the start of the time range to find
     * @param intersectEnd      the end of the time range to find
     * @return                  the beginning and end of the intersecting segments
     * @see findAllIntersecting(RandomAccessIterator, RandomAccessIterator, double, double)
     */
    template<typename RandomAccessIterator>
    static std::pair<RandomAccessIterator, RandomAccessIterator> heuristicFindAllIntersecting(
            RandomAccessIterator begin,
            RandomAccessIterator end,
            double intersectBegin,
            double intersectEnd)
    {
        return findAllIntersectingInternal(begin, end, intersectBegin, intersectEnd,
                                           heuristicUpperBoundInternal<RandomAccessIterator, true>,
                                           heuristicLowerBoundInternal<RandomAccessIterator, true>);
    }

    /** @see heuristicFindAllIntersecting(RandomAccessIterator, RandomAccessIterator, double, double) */
    template<typename Container>
    static inline std::pair<typename Container::const_iterator,
                            typename Container::const_iterator> heuristicFindAllIntersecting(const Container &container,
                                                                                             double intersectBegin,
                                                                                             double intersectEnd)
    {
        return heuristicFindAllIntersecting(container.begin(), container.end(), intersectBegin,
                                            intersectEnd);
    }

    /** @see heuristicFindAllIntersecting(RandomAccessIterator, RandomAccessIterator, double, double) */
    template<typename Container, typename RangeType>
    static inline std::pair<typename Container::const_iterator,
                            typename Container::const_iterator> heuristicFindAllIntersecting(const Container &container,
                                                                                             const RangeType &check)
    {
        return heuristicFindAllIntersecting(container.begin(), container.end(), check.getStart(),
                                            check.getEnd());
    }
};

}

#endif
