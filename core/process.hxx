/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3COREPROCESS_H
#define CPD3COREPROCESS_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QProcess>
#include <QByteArray>
#include <QFile>
#include "core/core.hxx"

namespace CPD3 {

/**
 * Provides routines for working with external processes.
 */
class CPD3CORE_EXPORT Process : public QObject {
public:
    /**
     * Forward standard error to the callers standard error.
     *
     * @param process   the process to forward from
     */
    static void forwardError(QProcess *process);
};

#ifdef Q_OS_UNIX
namespace Internal {

class ProcessErrorFoward : public QObject {
Q_OBJECT

    QProcess *process;
    QFile file;
    QByteArray buffer;

public:
    ProcessErrorFoward(QProcess *process);

    bool attach();

private slots:

    void readError();

    void writeError();
};

}
#endif

}

#endif
