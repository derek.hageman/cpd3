/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGAXISCOMMON_H
#define CPD3GRAPHINGAXISCOMMON_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QDialog>
#include <QComboBox>
#include <QLineEdit>

#include "graphing/graphing.hxx"
#include "graphing/display.hxx"
#include "graphing/axistransformer.hxx"
#include "graphing/axistickgenerator.hxx"

namespace CPD3 {
namespace Graphing {

/**
 * The axis sides in a two dimensional graph.
 */
enum Axis2DSide {
    /** Above the trace area. */
    Axis2DTop = 0,

    /** Below the trace area. */
    Axis2DBottom,

    /** To the left of the trace area. */
    Axis2DLeft,

    /** To the right of the trace area. */
    Axis2DRight,
};

/**
 * A simple indicator for two dimensional axes.
 */
class CPD3GRAPHING_EXPORT AxisIndicator2D {
public:
    /** 
     * The available types for drawing, ordered in the auto-selection
     * priority order.
     */
    enum Type {
        /** A filled rectangular bar. */
        RectangleFilled, /** A filled, possibly stretched, triangle pointing away from the axis. */
        TriangleAwayFilled, /** A filled, possibly stretched, triangle pointing towards the axis. */
        TriangleTowardsFilled, /** A filled, possibly stretched, diamond. */
        DiamondFilled,

        /** A rectangular outline bar. */
        Rectangle, /** A possibly stretched, triangle outline pointing away from the axis. */
        TriangleAway, /** A possibly stretched, triangle outline pointing towards the axis. */
        TriangleTowards, /** A possibly stretched, diamond outline. */
        Diamond,

        /** Type currently unselected, will not draw anything. */
        Unselected,

        /* After unselected so these are never auto-chosen. */
        /** Lines that span the entire area at the start and end. */
        SpanningLine,

        /** Fill the entire area. */
        AreaFilled,
    };

    /* C++ < 14 does not provide specialization for enums */
    struct TypeHash {
        inline std::size_t operator()(Type t) const
        { return std::hash<std::size_t>()(static_cast<std::size_t>(t)); }
    };

    AxisIndicator2D();

    AxisIndicator2D(const AxisIndicator2D &other);

    AxisIndicator2D &operator=(const AxisIndicator2D &other);

    AxisIndicator2D(Axis2DSide pos, Type t = Unselected, qreal s = 1.0, qreal inset = 1.0);

    AxisIndicator2D(const AxisIndicator2D &other, Axis2DSide pos);

    AxisIndicator2D(const Data::Variant::Read &configuration);

    Data::Variant::Root toConfiguration() const;

    Type getType() const;

    void setType(Type t);

    void selectTypeIfNeeded(std::unordered_set<Type, TypeHash> &usedTypes);

    Axis2DSide getPosition() const;

    void paint(qreal start,
               qreal end,
               QPainter *painter,
               qreal baseline,
               qreal totalDepth = 0.0) const;

    void getInsets(qreal baseline,
                   qreal totalDepth,
                   qreal &begin,
                   qreal &end,
                   QPaintDevice *paintdevice = NULL) const;

    QMenu *customizeMenu(const std::function<void(const AxisIndicator2D &)> &update,
                         QWidget *parent) const;

private:
    Axis2DSide position;
    Type type;
    qreal size;
    qreal inset;
};


/**
 * A dialog that can be used to configure the bounds of an axis transformer.
 */
class CPD3GRAPHING_EXPORT AxisTransformerBoundDialog : public QDialog {
    QComboBox *mode;

    QWidget *displayFixed;
    QWidget *displayExtendFraction;
    QWidget *displayExtendAbsolute;
    QWidget *displayExtendPower;

    QLineEdit *fixedValue;

    QLineEdit *extendFraction;
    QLineEdit *extendFractionAtLeast;
    QLineEdit *extendFractionAtMost;

    QLineEdit *extendAbsoluteAmount;

    QLineEdit *extendPowerBase;

    QLineEdit *absoluteLimit;
    QLineEdit *requiredLimit;

    void modeChanged();

public:
    AxisTransformerBoundDialog(QWidget *parent = nullptr);

    virtual ~AxisTransformerBoundDialog();

    /**
     * Configure the dialog parameters from the current minimum settings of
     * an existing transformer.
     *
     * @param transformer   the axis transformer
     */
    void setFromMinimum(const AxisTransformer &transformer);

    /**
     * Configure the dialog parameters from the current maximum settings of
     * an existing transformer.
     *
     * @param transformer   the axis transformer
     */
    void setFromMaximum(const AxisTransformer &transformer);

    /**
     * Configure the dialog parameters from an existing configuration
     *
     * @param configuration the bound configuration
     */
    void setFromConfiguration(const Data::Variant::Read &configuration);

    /**
     * Apply the dialog results to the the minimum bound of a transformer.
     *
     * @param transformer   the axis transformer
     */
    void applyToMinimum(AxisTransformer &transformer);

    /**
     * Apply the dialog results to the maximum bound of a transformer.
     *
     * @param transformer   the axis transformer
     */
    void applyToMaximum(AxisTransformer &transformer);

    /**
     * Get the dialog results as a configuration output.
     *
     * @return  the configuration
     */
    Data::Variant::Root toConfiguration() const;
};

}
}

Q_DECLARE_METATYPE(CPD3::Graphing::Axis2DSide);

#endif
