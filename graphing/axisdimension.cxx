/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>

#include "graphing/axisdimension.hxx"

using namespace CPD3::Data;
using namespace CPD3::Graphing::Internal;

namespace CPD3 {
namespace Graphing {

/** @file graphing/axisdimension.hxx
 * A set of associated axes for a single dimension.
 */


AxisParameters::AxisParameters() : components(0),
                                   transformer(),
                                   alwaysShow(false),
                                   units(),
                                   groupUnits(),
                                   tickGenerator(),
                                   tickLogBase(FP::undefined()),
                                   tickLevels(2),
                                   sortPriority(0)
{
    tickGenerator = std::shared_ptr<AxisTickGenerator>(new AxisTickGeneratorDecimal);
}

AxisParameters::~AxisParameters() = default;

AxisParameters::AxisParameters(const AxisParameters &) = default;

AxisParameters &AxisParameters::operator=(const AxisParameters &) = default;

AxisParameters::AxisParameters(AxisParameters &&) = default;

AxisParameters &AxisParameters::operator=(AxisParameters &&) = default;

AxisParameters::AxisParameters(const Variant::Read &value, QSettings *settings) : components(0),
                                                                                  transformer(
                                                                                          value["Range"]),
                                                                                  alwaysShow(false),
                                                                                  units(),
                                                                                  groupUnits(),
                                                                                  tickGenerator(),
                                                                                  tickLogBase(
                                                                                          FP::undefined()),
                                                                                  tickLevels(2),
                                                                                  sortPriority(0)
{

    if (value["Range"].exists()) {
        components |= Component_Transformer;
    }

    if (value["AlwaysShow"].exists()) {
        components |= Component_AlwaysShow;
        alwaysShow = value["AlwaysShow"].toBool();
    }

    if (value["Units"].exists()) {
        switch (value["Units"].getType()) {
        case Variant::Type::Array:
        case Variant::Type::Matrix: {
            components |= Component_Units;
            for (auto add : value["Units"].toChildren()) {
                auto str = add.toDisplayString();
                if (str.isEmpty())
                    continue;
                units.insert(str);
            }
            break;
        }
        case Variant::Type::String: {
            auto add = value["Units"].toDisplayString();
            if (!add.isEmpty()) {
                units.insert(add);
                components |= Component_Units;
            }
            break;
        }
        default: {
            for (const auto &add : value["Units"].toChildren().keys()) {
                units.insert(QString::fromStdString(add));
                components |= Component_Units;
            }
            break;
        }
        }
    }

    if (value["GroupUnits"].exists()) {
        for (const auto &add : value["GroupUnits"].toChildren().keys()) {
            groupUnits.insert(QString::fromStdString(add));
            components |= Component_GroupUnits;
        }
    }

    if (value["Ticks/Mode"].exists()) {
        components |= Component_TickGenerator;
        const auto &mode = value["Ticks/Mode"].toString();
        if (!transformer.isLogarithmic()) {
            if (Util::equal_insensitive(mode, "decimal")) {
                if (INTEGER::defined(value["Ticks/Digits"].toInt64()) &&
                        (int) value["Ticks/Digits"].toInt64() > 0) {
                    tickGenerator = std::shared_ptr<AxisTickGenerator>(
                            new AxisTickGeneratorDecimal((int) value["Ticks/Digits"].toInt64()));
                } else {
                    tickGenerator =
                            std::shared_ptr<AxisTickGenerator>(new AxisTickGeneratorDecimal);
                }
            } else if (Util::equal_insensitive(mode, "fixed", "constant")) {
                double interval = value["Ticks/Interval"].toDouble();
                double origin = value["Ticks/Origin"].toDouble();
                if (FP::defined(interval) || FP::defined(origin)) {
                    if (!FP::defined(interval))
                        interval = 1.0;
                    tickGenerator = std::shared_ptr<AxisTickGenerator>(
                            new AxisTickGeneratorConstant(interval, origin));
                } else {
                    tickGenerator =
                            std::shared_ptr<AxisTickGenerator>(new AxisTickGeneratorConstant);
                }
            } else if (Util::equal_insensitive(mode, "time")) {
                tickGenerator = std::shared_ptr<AxisTickGenerator>(
                        new AxisTickGeneratorTime(value["Ticks/TimeFormat"].toDisplayString(),
                                                  settings));
            } else if (Util::equal_insensitive(mode, "cdf", "normalcdf")) {
                tickGenerator =
                        std::shared_ptr<AxisTickGenerator>(new AxisTickGeneratorNormalQuantiles);
            } else if (mode == "allan" || mode == "timeinterval") {
                tickGenerator =
                        std::shared_ptr<AxisTickGenerator>(new AxisTickGeneratorTimeInterval);
            } else if (Util::equal_insensitive(mode, "disable", "none")) {
            } else if (Util::equal_insensitive(mode, "log", "logarithmic")) {
            }
        } else {
            if (Util::equal_insensitive(mode, "allan", "timeinterval")) {
                tickGenerator =
                        std::shared_ptr<AxisTickGenerator>(new AxisTickGeneratorTimeInterval);
            } else if (Util::equal_insensitive(mode, "disable", "none")) {
            } else {
                double base = value["Ticks/Base"].toDouble();
                if (FP::defined(base) && base > 0.0) {
                    tickGenerator =
                            std::shared_ptr<AxisTickGenerator>(new AxisTickGeneratorLog(base));
                    tickLogBase = base;
                } else {
                    tickGenerator = std::shared_ptr<AxisTickGenerator>(new AxisTickGeneratorLog);
                    tickLogBase = 10.0;
                }
            }
        }
    } else {
        if (!transformer.isLogarithmic()) {
            tickGenerator = std::shared_ptr<AxisTickGenerator>(new AxisTickGeneratorDecimal);
        } else {
            tickGenerator = std::shared_ptr<AxisTickGenerator>(new AxisTickGeneratorLog);
            tickLogBase = 10.0;
        }
    }

    if (INTEGER::defined(value["Ticks/TotalLevels"].toInt64())) {
        auto n = value["Ticks/TotalLevels"].toInteger();
        if (n < 0)
            n = 0;
        components |= Component_TickLevels;
        tickLevels = n;
    }

    if (INTEGER::defined(value["SortPriority"].toInt64())) {
        components |= Component_SortPriority;
        sortPriority = (int) value["SortPriority"].toInt64();
    }
}

/**
 * Save the parameters to the given value.  Derived classes must call
 * the base implementation.
 * 
 * @param value the target value
 */
void AxisParameters::save(Variant::Write &value) const
{
    if (components & Component_Transformer) {
        value["Range"].set(transformer.toConfiguration());
    }

    if (components & Component_AlwaysShow) {
        value["AlwaysShow"].setBool(alwaysShow);
    }

    if (components & Component_Units) {
        for (const auto &add : units) {
            value["Units"].toArray().after_back().setString(add);
        }
    }

    if (components & Component_GroupUnits) {
        for (const auto &add : groupUnits) {
            value["GroupUnits"].toArray().after_back().setString(add);
        }
    }

    if (components & Component_TickGenerator) {
        if (auto decimal = qobject_cast<const AxisTickGeneratorDecimal *>(tickGenerator.get())) {
            value["Ticks/Mode"].setString("Decimal");
            value["Ticks/Digits"].setInt64(decimal->getMaxSignificantDigits());
        }

        if (auto constant = qobject_cast<const AxisTickGeneratorConstant *>(tickGenerator.get())) {
            value["Ticks/Mode"].setString("Fixed");
            value["Ticks/Interval"].setDouble(constant->getInterval());
            value["Ticks/Origin"].setDouble(constant->getOrigin());
        }

        if (qobject_cast<const AxisTickGeneratorTime *>(tickGenerator.get())) {
            value["Ticks/Mode"].setString("Time");
        }

        if (auto log = qobject_cast<const AxisTickGeneratorLog *>(tickGenerator.get())) {
            value["Ticks/Mode"].setString("Log");
            value["Ticks/Base"].setDouble(log->getBase());
        }

        if (qobject_cast<const AxisTickGeneratorTimeInterval *>(tickGenerator.get())) {
            value["Ticks/Mode"].setString("TimeInterval");
        }

        if (!tickGenerator) {
            value["Ticks/Mode"].setString("Disable");
        }
    }

    if (components & Component_TickLevels) {
        value["Ticks/TotalLevels"].setInteger(tickLevels);
    }

    if (components & Component_SortPriority) {
        value["SortPriority"].setInteger(sortPriority);
    }

}

/**
 * Replace these parameters with the given ones, including all polymorphism.
 * <br>
 * This is equivalent to: *this = *(static_cast<const CLASS *>(from))
 * 
 * @param from  the parameters to copy 
 */
void AxisParameters::copy(const AxisParameters *from)
{ *this = *from; }

/**
 * Clear all "set" flags from the parameters.  Derived implementations must
 * call the base one.
 */
void AxisParameters::clear()
{ components = 0; }

std::unique_ptr<AxisParameters> AxisParameters::clone() const
{ return std::unique_ptr<AxisParameters>(new AxisParameters(*this)); }

void AxisParameters::overlay(const AxisParameters *over)
{
    if (over->components & Component_Transformer) {
        components |= Component_Transformer;
        if (transformer.isLogarithmic() != over->transformer.isLogarithmic()) {
            tickGenerator = over->tickGenerator;
            tickLogBase = over->tickLogBase;
        }
        transformer = over->transformer;
    }

    if (over->components & Component_AlwaysShow) {
        components |= Component_AlwaysShow;
        alwaysShow = over->alwaysShow;
    }

    if (over->components & Component_Units) {
        components |= Component_Units;
        units = over->units;
    }

    if (over->components & Component_GroupUnits) {
        components |= Component_GroupUnits;
        groupUnits = over->groupUnits;
    }

    if (over->components & Component_TickGenerator) {
        components |= Component_TickGenerator;
        tickGenerator = over->tickGenerator;
        tickLogBase = over->tickLogBase;
    }

    if (over->components & Component_TickLevels) {
        components |= Component_TickLevels;
        tickLevels = over->tickLevels;
    }

    if (over->components & Component_SortPriority) {
        components |= Component_SortPriority;
        sortPriority = over->sortPriority;
    }
}


void AxisParameters::forceAxisTransformer(const AxisTransformer &setTransformer, bool defined)
{
    transformer = setTransformer;
    components &= ~Component_Transformer;
    if (defined)
        components |= Component_Transformer;
}

void AxisParameters::forceTickGenerator(const std::shared_ptr<AxisTickGenerator> &setTickGenerator,
                                        double setTickLogBase,
                                        bool defined)
{
    tickGenerator = setTickGenerator;
    tickLogBase = setTickLogBase;
    components &= ~Component_TickGenerator;
    if (defined)
        components |= Component_TickGenerator;
}

AxisDimension::AxisDimension(std::unique_ptr<AxisParameters> &&params) : parameters(
        std::move(params)),
                                                                         originalParameters(),
                                                                         overridenParameters(),
                                                                         transformer(
                                                                                 parameters->getTransformer()),
                                                                         rawMin(FP::undefined()),
                                                                         rawMax(FP::undefined()),
                                                                         dataMin(FP::undefined()),
                                                                         dataMax(FP::undefined()),
                                                                         zoomMin(FP::undefined()),
                                                                         zoomMax(FP::undefined()),
                                                                         units(),
                                                                         groupUnits(),
                                                                         dynamic(false),
                                                                         attachedReferenceCount(0),
                                                                         externalReferenceCount(0)
{ }

AxisDimension::~AxisDimension() = default;

AxisDimension::AxisDimension(const AxisDimension &other, std::unique_ptr<AxisParameters> &&params)
        : parameters(std::move(params)),
          originalParameters(),
          overridenParameters(),
          transformer(other.transformer),
          rawMin(other.rawMin),
          rawMax(other.rawMax),
          dataMin(other.dataMin),
          dataMax(other.dataMax),
          zoomMin(other.zoomMin),
          zoomMax(other.zoomMax),
          units(other.units),
          groupUnits(other.groupUnits),
          dynamic(other.dynamic),
          attachedReferenceCount(other.attachedReferenceCount)
{ }

std::unique_ptr<AxisDimension> AxisDimension::clone() const
{ return std::unique_ptr<AxisDimension>(new AxisDimension(*this, parameters->clone())); }

void AxisDimension::beginUpdate()
{
    attachedReferenceCount = 0;
    rawMin = FP::undefined();
    rawMax = FP::undefined();
    dataMin = FP::undefined();
    dataMax = FP::undefined();
}

std::vector<std::vector<AxisTickGenerator::Tick>> AxisDimension::getTicks() const
{
    if (parameters->getTickLevels() <= 0)
        return {};
    auto generator = parameters->getTickGenerator();
    if (!generator)
        return {};
    if (!FP::defined(dataMin) || !FP::defined(dataMax))
        return {};

    std::vector<std::vector<AxisTickGenerator::Tick>> result;
    for (std::size_t i = 0; i < parameters->getTickLevels(); i++) {
        std::vector<AxisTickGenerator::Tick> add;
        if (i == 0) {
            add = generator->generate(dataMin, dataMax, AxisTickGenerator::MajorTick);
        } else {
            auto prior = result.back();
            for (auto it = prior.begin() + 1, end = prior.end(); it != end; ++it) {
                double min = (it - 1)->point;
                double max = it->point;
                if (!FP::defined(min) || !FP::defined(max) || min >= max)
                    continue;
                Util::append(generator->generate(min, max, AxisTickGenerator::MinorTick), add);
            }
        }
        if (add.empty())
            break;
        result.emplace_back(std::move(add));
    }
    return result;
}

void AxisDimension::registerDataLimits(double min, double max)
{
    if (FP::defined(rawMin) && FP::defined(rawMax)) {
        if (FP::defined(min)) {
            if (rawMin > min) rawMin = min;
            if (rawMax < min) rawMax = min;
        }
        if (FP::defined(max)) {
            if (rawMin > max) rawMin = max;
            if (rawMax < max) rawMax = max;
        }
        return;
    }
    if (!FP::defined(rawMin)) {
        if (FP::defined(min))
            rawMin = min;
        else
            rawMin = max;
    }
    if (!FP::defined(rawMax)) {
        if (FP::defined(max))
            rawMax = max;
        else
            rawMax = min;
    }
}

void AxisDimension::updateTransformer(double &min, double &max, double zoomMin, double zoomMax)
{
    transformer.setReal(min, max, zoomMin, zoomMax);
}

void AxisDimension::allDataRegistered()
{
    dataMin = rawMin;
    dataMax = rawMax;
    updateTransformer(dataMin, dataMax, zoomMin, zoomMax);
}

void AxisDimension::setScreen(double min, double max)
{
    transformer.setScreen(min, max);
}

void AxisDimension::setZoom(double min, double max)
{
    zoomMin = min;
    zoomMax = max;
}

void AxisDimension::couplingUpdated(double min,
                                    double max,
                                    DisplayCoupling::CouplingPosition position)
{
    Q_UNUSED(position);
    dataMin = min;
    dataMax = max;
    updateTransformer(dataMin, dataMax, zoomMin, zoomMax);
}

std::vector<std::shared_ptr<
        DisplayCoupling>> AxisDimension::createRangeCoupling(DisplayCoupling::CoupleMode mode)
{
    if (units.empty() && groupUnits.empty())
        return {};
    return std::vector<std::shared_ptr<DisplayCoupling>>{
            std::make_shared<AxisUnitCoupling>(mode, this)};
}

/**
 * Get the override parameters.  This also flags the dimension as having
 * had something overriden.
 * 
 * @return the override parameters
 */
AxisParameters *AxisDimension::overrideParameters()
{
    if (!overridenParameters) {
        overridenParameters = parameters->clone();
        overridenParameters->clear();
        Q_ASSERT(!originalParameters);
        originalParameters = parameters->clone();
    }
    return overridenParameters.get();
}

/**
 * Merge all overrides into the main parameters.
 */
void AxisDimension::mergeOverrideParameters()
{
    Q_ASSERT(overridenParameters);
    parameters->overlay(overridenParameters.get());
    transformer = parameters->getTransformer();
}

/**
 * Reset all parameters to the original state, reverting all overrides.
 */
void AxisDimension::revertOverrideParameters()
{
    if (!originalParameters)
        return;
    parameters->copy(originalParameters.get());
    originalParameters.reset();
    Q_ASSERT(overridenParameters);
    overridenParameters.reset();
    transformer = parameters->getTransformer();
}

/**
 * Save the override parameters to the given target value.
 * 
 * @param target    the target of the override parameters
 */
void AxisDimension::saveOverrideParameters(Variant::Write &target) const
{
    Q_ASSERT(overridenParameters);
    overridenParameters->save(target);
}

namespace Internal {

AxisUnitCoupling::AxisUnitCoupling() = default;

/**
 * Create a coupling with the given mode and parent.
 * @param setMode   the mode
 * @param setParent the parent
 */
AxisUnitCoupling::AxisUnitCoupling(DisplayCoupling::CoupleMode mode, AxisDimension *setParent)
        : DisplayRangeCoupling(mode), parent(setParent)
{ }

AxisUnitCoupling::~AxisUnitCoupling()
{ }

double AxisUnitCoupling::getMin() const
{
    if (parent.isNull()) return FP::undefined();
    return parent->getRawMin();
}

double AxisUnitCoupling::getMax() const
{
    if (parent.isNull()) return FP::undefined();
    return parent->getRawMax();
}

bool AxisUnitCoupling::canCouple(const std::shared_ptr<DisplayCoupling> &other) const
{
    if (parent.isNull())
        return false;
    if (!other)
        return false;
    auto sa = qobject_cast<AxisUnitCoupling *>(other.get());
    if (!sa)
        return false;
    if (sa->parent.isNull())
        return false;
    if (!sa->parent->getGroupUnits().empty())
        return sa->parent->getGroupUnits() == parent->getGroupUnits();
    if (!parent->getGroupUnits().empty())
        return false;
    return sa->parent->getUnits() == parent->getUnits();
}

void AxisUnitCoupling::applyCoupling(const QVariant &data,
                                     DisplayCoupling::CouplingPosition position)
{
    if (parent.isNull())
        return;
    double setMin = FP::undefined();
    double setMax = FP::undefined();
    QList<QVariant> list(data.toList());
    Q_ASSERT(list.size() > 1);
    if (!list.at(0).isNull())
        setMin = list.at(0).toDouble();
    if (!list.at(1).isNull())
        setMax = list.at(1).toDouble();
    parent->couplingUpdated(setMin, setMax, position);
}

}

AxisDimensionZoom::AxisDimensionZoom()
{ }

/**
 * Create a zoom axis.
 * 
 * @param setDimension  the dimension to zoom for
 * @param setParent     the parent dimension set
 */
AxisDimensionZoom::AxisDimensionZoom(AxisDimension *setDimension, AxisDimensionSet *setParent)
        : dimension(setDimension), parent(setParent)
{
    if (!dimension.isNull())
        dimension->externalReferenceCount.ref();
}

AxisDimensionZoom::~AxisDimensionZoom()
{
    if (!dimension.isNull())
        dimension->externalReferenceCount.deref();
}

QString AxisDimensionZoom::getTitle() const
{
    if (dimension.isNull()) return QString();
    QStringList sorted;
    Util::append(dimension->getUnits(), sorted);
    if (sorted.isEmpty()) {
        Util::append(dimension->getGroupUnits(), sorted);
    }
    std::sort(sorted.begin(), sorted.end());
    return sorted.join(" ");
}

double AxisDimensionZoom::getMin() const
{
    if (dimension.isNull())
        return FP::undefined();
    return dimension->getMin();
}

double AxisDimensionZoom::getMax() const
{
    if (dimension.isNull())
        return FP::undefined();
    return dimension->getMax();
}

double AxisDimensionZoom::getZoomMin() const
{
    if (dimension.isNull())
        return FP::undefined();
    return dimension->getZoomMin();
}

double AxisDimensionZoom::getZoomMax() const
{
    if (dimension.isNull())
        return FP::undefined();
    return dimension->getZoomMax();
}

std::vector<std::shared_ptr<DisplayZoomAxis>> AxisDimensionZoom::getCoupledAxes() const
{
    std::vector<std::shared_ptr<DisplayZoomAxis>> result;
    for (const auto &it : siblings) {
        auto ref = it.lock();
        if (!ref)
            continue;
        if (static_cast<AxisDimensionZoom *>(ref.get()) == this)
            continue;
        result.emplace_back(std::move(ref));
    }
    return result;
}

void AxisDimensionZoom::setZoom(double min, double max)
{
    if (dimension.isNull())
        return;
    dimension->setZoom(min, max);
    if (parent.isNull())
        return;
    parent->externalZoomNotify();
}

void AxisDimensionSet::externalZoomNotify()
{ emit zoomUpdated(); }

bool AxisDimensionZoom::sharedZoom(const std::shared_ptr<DisplayZoomAxis> &other) const
{
    if (parent.isNull())
        return false;
    if (dimension.isNull())
        return false;
    auto sa = qobject_cast<AxisDimensionZoom *>(other.get());
    if (!sa)
        return false;
    if (!sa->parent)
        return false;
    if (!sa->dimension)
        return false;
    if (!dimension->getGroupUnits().empty()) {
        return dimension->getGroupUnits() == sa->dimension->getGroupUnits();
    } else if (!sa->dimension->getGroupUnits().empty()) {
        return false;
    }
    if (dimension->getUnits().empty())
        return false;
    return dimension->getUnits() == sa->dimension->getUnits();
}

AxisDimensionSet::OverrideBound::~OverrideBound() = default;

void AxisDimensionSet::OverrideBound::apply(AxisParameters *target, const QString &name) const
{
    if (!Util::exact_match(name, matcher))
        return;
    target->overlay(parameters.get());
}

AxisDimensionSet::OverrideUnbound::~OverrideUnbound() = default;

void AxisDimensionSet::OverrideUnbound::apply(AxisParameters *target,
                                              const std::unordered_set<QString> &units,
                                              const std::unordered_set<QString> &groupUnits) const
{
    std::unordered_set<QString> remaining;
    if (useGroup)
        remaining = groupUnits;
    else
        remaining = units;
    if (remaining.empty())
        return;

    for (const auto &match : matchers) {
        for (auto check = remaining.begin(); check != remaining.end();) {
            if (Util::exact_match(*check, match)) {
                check = remaining.erase(check);
                continue;
            }
            ++check;
        }
        if (remaining.empty())
            break;
    }
    if (!remaining.empty())
        return;
    target->overlay(parameters.get());
}

AxisDimensionSet::AxisDimensionSet() : defaults(nullptr)
{ }

AxisDimensionSet::~AxisDimensionSet() = default;

AxisDimensionSet::AxisDimensionSet(const AxisDimensionSet &other)
{
    if (other.defaults != NULL)
        defaults = other.defaults->clone();
    for (const auto &it : other.bound) {
        bound.emplace(it.first, it.second->clone());
    }
    for (const auto &it : other.unbound) {
        unbound.emplace_back(it->clone());
    }
    for (const auto &it : other.overrideBound) {
        std::unique_ptr<OverrideBound> add(new OverrideBound);
        add->matcher = it->matcher;
        add->parameters = it->parameters->clone();
        overrideBound.emplace_back(std::move(add));
    }
    for (const auto &it : other.overrideUnbound) {
        std::unique_ptr<OverrideUnbound> add(new OverrideUnbound);
        add->matchers = it->matchers;
        add->useGroup = it->useGroup;
        add->parameters = it->parameters->clone();
        overrideUnbound.emplace_back(std::move(add));
    }
}

std::unique_ptr<AxisDimensionSet> AxisDimensionSet::clone() const
{ return std::unique_ptr<AxisDimensionSet>(new AxisDimensionSet(*this)); }

bool AxisDimensionSet::mergable(const Variant::Read &, const Variant::Read &)
{ return true; }

void AxisDimensionSet::initialize(const ValueSegment::Transfer &config,
                                  const DisplayDefaults &defaults)
{
    this->defaults.reset();
    std::unordered_map<std::string, std::unique_ptr<AxisParameters>> bindings;
    overrideBound.clear();
    overrideUnbound.clear();
    for (const auto &cseg : config) {
        auto segment = cseg.read();

        if (segment["Defaults"].exists()) {
            this->defaults = parseParameters(segment["Defaults"], defaults.getSettings());
            Q_ASSERT(this->defaults != NULL);
        }

        if (segment["Static"].exists()) {
            /* Force these to hide, even if they where set otherwise. */
            for (const auto &binding : bindings) {
                binding.second->components |= AxisParameters::Component_AlwaysShow;
                binding.second->alwaysShow = false;
            }
            for (auto it : segment["Static"].toHash()) {
                if (it.first.empty())
                    continue;
                auto check = bindings.find(it.first);
                auto add = parseParameters(it.second, defaults.getSettings());
                Q_ASSERT(add != NULL);
                if (check != bindings.end()) {
                    check->second = std::move(add);
                } else {
                    bindings.emplace(it.first, std::move(add));
                }
            }
        }

        if (segment["Override"].exists()) {
            overrideBound.clear();
            for (auto it : segment["Override"].toHash()) {
                if (it.first.empty())
                    continue;
                QString re(it.second["Match"].toQString());
                if (re.isEmpty())
                    continue;
                std::unique_ptr<OverrideBound> add(new OverrideBound);
                add->matcher = QRegularExpression(re);
                add->parameters = parseParameters(it.second["Settings"], defaults.getSettings());
                Q_ASSERT(add->parameters.get() != nullptr);
                overrideBound.emplace_back(std::move(add));
            }
        }

        if (segment["OverrideUnits"].exists()) {
            overrideUnbound.clear();
            for (auto it : segment["OverrideUnits"].toHash()) {
                if (it.first.empty())
                    continue;
                std::vector<QRegularExpression> re;
                for (auto a : it.second["Match"].toArray()) {
                    QString str(a.toDisplayString());
                    if (str.isEmpty())
                        continue;
                    re.emplace_back(str);
                }
                if (re.empty())
                    continue;
                std::unique_ptr<OverrideUnbound> add(new OverrideUnbound);
                add->matchers = std::move(re);
                add->useGroup = it.second["GroupUnits"].toBool();
                add->parameters = parseParameters(it.second["Settings"], defaults.getSettings());
                Q_ASSERT(add->parameters.get() != nullptr);
                overrideUnbound.emplace_back(std::move(add));
            }
        }
    }

    if (this->defaults == NULL) {
        this->defaults = parseParameters(Variant::Read::empty(), defaults.getSettings());
        Q_ASSERT(this->defaults != NULL);
    }

    for (auto &it : bindings) {
        auto params = it.second.get();
        for (const auto &override : overrideBound) {
            override->apply(params, QString::fromStdString(it.first));
        }

        auto add = createDimension(std::move(it.second));
        Q_ASSERT(add.get() != nullptr);
        add->dynamic = false;
        add->units = params->getUnits();
        add->groupUnits = params->getGroupUnits();
        bound.emplace(it.first, std::move(add));
    }
}

void AxisDimensionSet::beginUpdate()
{
    for (const auto &it : bound) {
        it.second->beginUpdate();
        it.second->units = it.second->parameters->getUnits();
        it.second->groupUnits = it.second->parameters->getGroupUnits();
    }
    for (const auto &it : unbound) {
        it->beginUpdate();
    }
    for (const auto &it : unreferencedBound) {
        it.second->beginUpdate();
        it.second->units = it.second->parameters->getUnits();
        it.second->groupUnits = it.second->parameters->getGroupUnits();
    }
    for (const auto &it : unreferencedUnbound) {
        it->beginUpdate();
    }
}

void AxisDimensionSet::finishUpdate()
{
    for (auto it = bound.begin(); it != bound.end();) {
        if (it->second->dynamic && it->second->attachedReferenceCount <= 0) {
            if (it->second->hasExternalReferences())
                unreferencedBound.emplace(it->first, std::move(it->second));
            it = bound.erase(it);
            continue;
        }
        it->second->allDataRegistered();
        ++it;
    }
    for (auto it = unbound.begin(); it != unbound.end();) {
        if ((*it)->dynamic && (*it)->attachedReferenceCount <= 0) {
            if ((*it)->hasExternalReferences())
                unreferencedUnbound.emplace_back(std::move(*it));
            it = unbound.erase(it);
            continue;
        }
        (*it)->allDataRegistered();
        ++it;
    }
    for (auto it = unreferencedBound.begin(); it != unreferencedBound.end();) {
        if (!it->second->hasExternalReferences()) {
            it = unreferencedBound.erase(it);
            continue;
        }
        ++it;
    }
    for (auto it = unreferencedUnbound.begin(); it != unreferencedUnbound.end();) {
        if (!(*it)->hasExternalReferences()) {
            it = unreferencedUnbound.erase(it);
            continue;
        }
        ++it;
    }
}


AxisDimension *AxisDimensionSet::get(const std::unordered_set<QString> &units,
                                     const std::unordered_set<QString> &groupUnits)
{
    for (const auto &it : bound) {
        if (!groupUnits.empty()) {
            if (it.second->getGroupUnits() == groupUnits) {
                it.second->attachedReferenceCount++;
                Util::merge(units, it.second->units);
                return it.second.get();
            }
        } else if (it.second->getGroupUnits().empty()) {
            if (it.second->getUnits() == units) {
                it.second->attachedReferenceCount++;
                return it.second.get();
            }
        }
    }
    for (const auto &it : unbound) {
        if (!groupUnits.empty()) {
            if (it->getGroupUnits() == groupUnits) {
                it->attachedReferenceCount++;
                Util::merge(units, it->units);
                return it.get();
            }
        } else if (it->getGroupUnits().empty()) {
            if (it->getUnits() == units) {
                it->attachedReferenceCount++;
                return it.get();
            }
        }
    }
    for (auto it = unreferencedUnbound.begin(); it != unreferencedUnbound.end();) {
        if (!(*it)->hasExternalReferences()) {
            it = unreferencedUnbound.erase(it);
            continue;
        }
        if (!groupUnits.empty()) {
            if ((*it)->getGroupUnits() == groupUnits) {
                auto dim = std::move(*it);
                unreferencedUnbound.erase(it);
                dim->attachedReferenceCount++;
                Util::merge(units, dim->units);
                unbound.emplace_back(std::move(dim));
                return unbound.back().get();
            }
        } else if ((*it)->getGroupUnits().empty()) {
            if ((*it)->getUnits() == units) {
                auto dim = std::move(*it);
                unreferencedUnbound.erase(it);
                dim->attachedReferenceCount++;
                Util::merge(units, dim->units);
                unbound.emplace_back(std::move(dim));
                return unbound.back().get();
            }
        }
        ++it;
    }

    std::unique_ptr<AxisParameters> params;
    if (defaults)
        params = defaults->clone();
    else
        params = parseParameters(Variant::Read::empty());
    Q_ASSERT(params.get() != nullptr);

    for (const auto &it : overrideUnbound) {
        it->apply(params.get(), units, groupUnits);
    }

    auto add = createDimension(std::move(params));
    Q_ASSERT(add.get() != nullptr);
    add->dynamic = true;
    add->units = units;
    add->groupUnits = groupUnits;
    add->attachedReferenceCount = 1;
    unbound.emplace_back(std::move(add));
    return unbound.back().get();
}

AxisDimension *AxisDimensionSet::get(const std::unordered_set<QString> &units,
                                     const std::unordered_set<QString> &groupUnits,
                                     const QString &binding)
{
    {
        auto check = bound.find(binding.toStdString());
        if (check != bound.end()) {
            Util::merge(units, check->second->units);
            Util::merge(groupUnits, check->second->groupUnits);
            check->second->attachedReferenceCount++;
            return check->second.get();
        }
    }
    {
        auto check = unreferencedBound.find(binding.toStdString());
        if (check != unreferencedBound.end()) {
            auto dim = std::move(check->second);
            unreferencedBound.erase(check);
            if (dim->hasExternalReferences()) {
                Util::merge(units, dim->units);
                Util::merge(groupUnits, dim->groupUnits);
                dim->attachedReferenceCount++;
                auto result = dim.get();
                bound.emplace(binding.toStdString(), std::move(dim));
                return result;
            }
        }
    }

    std::unique_ptr<AxisParameters> params;
    if (defaults)
        params = defaults->clone();
    else
        params = parseParameters(Variant::Read::empty());
    Q_ASSERT(params.get() != nullptr);

    for (const auto &it : overrideBound) {
        it->apply(params.get(), binding);
    }

    auto add = createDimension(std::move(params));
    Q_ASSERT(add.get() != nullptr);
    add->dynamic = true;
    add->units = units;
    add->groupUnits = groupUnits;
    add->attachedReferenceCount = 1;
    auto result = add.get();
    bound.emplace(binding.toStdString(), std::move(add));
    return result;
}

static QString staticSortUnitPriority[] =
        {QString::fromUtf8("Mm\xE2\x81\xBB¹"), QString::fromLatin1("\xC2\xB0\x43"),
         QString::fromLatin1("hPa"), QString::fromLatin1("%")};
static QString staticSortGroupUnitPriority[] = {QString::fromLatin1("Transmittance"),};

static bool sortUnbound(const AxisDimension *a, const AxisDimension *b)
{
    for (std::size_t i = 0;
            i < sizeof(staticSortUnitPriority) / sizeof(staticSortUnitPriority[0]);
            i++) {
        bool checkA = a->getUnits().count(staticSortUnitPriority[i]);
        bool checkB = b->getUnits().count(staticSortUnitPriority[i]);
        if (checkA != checkB)
            return checkA;
    }
    for (std::size_t i = 0;
            i < sizeof(staticSortGroupUnitPriority) / sizeof(staticSortGroupUnitPriority[0]);
            i++) {
        bool checkA = a->getGroupUnits().count(staticSortGroupUnitPriority[i]);
        bool checkB = b->getGroupUnits().count(staticSortGroupUnitPriority[i]);
        if (checkA != checkB)
            return checkA;
    }
    return false;
}

static bool sortAllAxes(const AxisDimension *a, const AxisDimension *b)
{
    return a->getSortPriority() < b->getSortPriority();
}

std::vector<AxisDimension *> AxisDimensionSet::getDisplayed() const
{
    std::vector<AxisDimension *> result;

    std::vector<std::string> sortedBound;
    for (const auto &b : bound) {
        sortedBound.emplace_back(b.first);
    }
    std::sort(sortedBound.begin(), sortedBound.end());
    for (const auto &it : sortedBound) {
        auto check = bound.find(it);
        Q_ASSERT(check != bound.end());
        if (!check->second->parameters->getAlwaysShow() &&
                check->second->attachedReferenceCount <= 0)
            continue;
        result.emplace_back(check->second.get());
    }

    auto lastAdd = result.size();
    for (const auto &it : unbound) {
        if (!it->parameters->getAlwaysShow() && it->attachedReferenceCount <= 0)
            continue;
        result.emplace_back(it.get());
    }
    if (lastAdd != result.size())
        std::stable_sort(result.begin() + lastAdd, result.end(), sortUnbound);

    std::stable_sort(result.begin(), result.end(), sortAllAxes);
    return result;
}

std::size_t AxisDimensionSet::countDisplayed() const
{
    std::size_t result = 0;
    for (const auto &it : bound) {
        if (!it.second->parameters->getAlwaysShow() && it.second->attachedReferenceCount <= 0)
            continue;
        ++result;
    }
    for (const auto &it : unbound) {
        if (!it->parameters->getAlwaysShow() && it->attachedReferenceCount <= 0)
            continue;
        ++result;
    }
    return result;
}

std::vector<AxisDimension *> AxisDimensionSet::getAllDimensions() const
{
    std::vector<AxisDimension *> result;
    for (const auto &it : unbound) {
        result.emplace_back(it.get());
    }
    for (const auto &it : bound) {
        result.emplace_back(it.second.get());
    }
    return result;
}

std::vector<std::shared_ptr<DisplayZoomAxis>> AxisDimensionSet::getZoom()
{
    std::vector<std::shared_ptr<DisplayZoomAxis>> result;
    std::vector<std::weak_ptr<DisplayZoomAxis>> siblings;
    for (const auto &it : bound) {
        if (!it.second->parameters->getAlwaysShow() && it.second->attachedReferenceCount <= 0)
            continue;
        auto add = createZoom(it.second.get());
        if (!add)
            continue;
        siblings.emplace_back(add);
        result.emplace_back(std::move(add));
    }
    for (const auto &it : unbound) {
        if (!it->parameters->getAlwaysShow() && it->attachedReferenceCount <= 0)
            continue;
        auto add = createZoom(it.get());
        if (!add)
            continue;
        siblings.emplace_back(add);
        result.emplace_back(std::move(add));
    }
    for (const auto &it : result) {
        (static_cast<AxisDimensionZoom *>(it.get()))->siblings = siblings;
    }
    return result;
}

DisplayZoomCommand AxisDimensionSet::convertMouseZoom(qreal min, qreal max)
{
    std::vector<DisplayZoomCommand::Axis> zoomAxes;
    for (const auto &it : bound) {
        if (!it.second->parameters->getAlwaysShow() && it.second->attachedReferenceCount <= 0)
            continue;
        auto add = createZoom(it.second.get());
        if (!add)
            continue;
        zoomAxes.emplace_back(std::move(add), it.second->getTransformer().toReal(min),
                              it.second->getTransformer().toReal(max));
    }
    for (const auto &it : unbound) {
        if (!it->parameters->getAlwaysShow() && it->attachedReferenceCount <= 0)
            continue;
        auto add = createZoom(it.get());
        if (!add)
            continue;
        zoomAxes.emplace_back(std::move(add), it->getTransformer().toReal(min),
                              it->getTransformer().toReal(max));
    }
    return DisplayZoomCommand(std::move(zoomAxes));
}

std::vector<std::shared_ptr<
        DisplayCoupling>> AxisDimensionSet::createRangeCoupling(DisplayCoupling::CoupleMode mode)
{
    std::vector<std::shared_ptr<DisplayCoupling>> result;
    for (const auto &it : bound) {
        if (!it.second->parameters->getAlwaysShow() && it.second->attachedReferenceCount <= 0)
            continue;
        Util::append(it.second->createRangeCoupling(mode), result);
    }
    for (const auto &it : unbound) {
        if (!it->parameters->getAlwaysShow() && it->attachedReferenceCount <= 0)
            continue;
        Util::append(it->createRangeCoupling(mode), result);
    }
    return result;
}

/**
 * Get the list of segments that define any manual overrides in effect
 * for the dimension set.
 * 
 * @param overrideName      the override name hash to put values in
 * @return                  the list of override segments
 */
ValueSegment::Transfer AxisDimensionSet::getManualOverrides(const QString &overrideName)
{
    ValueSegment::Transfer result;
    for (const auto &it : bound) {
        if (!it.second->hasOverrideParameters())
            continue;
        QString targetName;
        if (overrideName.contains('%'))
            targetName = overrideName.arg(QString::fromStdString(it.first));
        else
            targetName = overrideName;

        Variant::Root base;
        auto target = base.write().hash("Override").hash(targetName);
        auto overrideTarget = target["Settings"];
        it.second->saveOverrideParameters(overrideTarget);
        target["Match"].setString(QRegularExpression::escape(QString::fromStdString(it.first)));

        result.emplace_back(FP::undefined(), FP::undefined(), std::move(base));
    }
    for (const auto &it : unbound) {
        if (!it->hasOverrideParameters())
            continue;
        QString targetName;
        if (overrideName.contains('%')) {
            QStringList sorted;
            Util::append(it->getGroupUnits(), sorted);
            if (sorted.empty())
                Util::append(it->getUnits(), sorted);
            std::sort(sorted.begin(), sorted.end());
            targetName = overrideName.arg(sorted.join(","));
        } else {
            targetName = overrideName;
        }

        Variant::Root base;
        auto target = base.write().hash("OverrideUnits").hash(targetName);
        auto overrideTarget = target["Settings"];
        it->saveOverrideParameters(overrideTarget);

        auto units = it->getGroupUnits();
        if (units.empty()) {
            units = it->getUnits();
            target["GroupUnits"].setBool(false);
        } else {
            target["GroupUnits"].setBool(true);
        }
        if (units.empty())
            continue;

        for (const auto &u : units) {
            target["Match"].toArray().after_back().setString(QRegularExpression::escape(u));
        }

        result.emplace_back(FP::undefined(), FP::undefined(), std::move(base));
    }

    if (result.size() < 2)
        return result;
    return ValueSegment::merge(result);
}

/**
 * Reset all manual overrides for the set.
 */
void AxisDimensionSet::resetManualOverrides()
{
    for (auto &it : bound) {
        it.second->revertOverrideParameters();
    }
    for (auto &it : unbound) {
        it->revertOverrideParameters();
    }
    for (auto it = unreferencedBound.begin(); it != unreferencedBound.end();) {
        if (!it->second->hasExternalReferences()) {
            it = unreferencedBound.erase(it);
            continue;
        }
        it->second->revertOverrideParameters();
        ++it;
    }
    for (auto it = unreferencedUnbound.begin(); it != unreferencedUnbound.end();) {
        if (!(*it)->hasExternalReferences()) {
            it = unreferencedUnbound.erase(it);
            continue;
        }
        (*it)->revertOverrideParameters();
        ++it;
    }
}

std::unique_ptr<AxisParameters> AxisDimensionSet::parseParameters(const Variant::Read &value,
                                                                  QSettings *settings) const
{ return std::unique_ptr<AxisParameters>(new AxisParameters(value, settings)); }

std::unique_ptr<AxisDimension> AxisDimensionSet::createDimension(std::unique_ptr<
        AxisParameters> &&parameters) const
{ return std::unique_ptr<AxisDimension>(new AxisDimension(std::move(parameters))); }

std::shared_ptr<AxisDimensionZoom> AxisDimensionSet::createZoom(AxisDimension *dimension)
{ return std::make_shared<AxisDimensionZoom>(dimension, this); }

}
}
