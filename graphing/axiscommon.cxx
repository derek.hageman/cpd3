/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>
#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QGroupBox>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QDialog>
#include <QIconEngine>
#include <QPainterPath>

#include "graphing/display.hxx"
#include "graphing/axiscommon.hxx"
#include "axiscommon.hxx"


using namespace CPD3::Data;

namespace CPD3 {
namespace Graphing {

/** @file graphing/axiscommon.hxx
 * Provides general utilities related to normal axes.
 */


AxisIndicator2D::AxisIndicator2D() : position(Axis2DBottom), type(Unselected), size(1.0), inset(1.0)
{ }

AxisIndicator2D::AxisIndicator2D(const AxisIndicator2D &other) = default;

AxisIndicator2D &AxisIndicator2D::operator=(const AxisIndicator2D &other) = default;

/**
 * Create a new indicator with the given parameters.  If a size is
 * specified as a negative value, it is treated as a device dependent absolute
 * size.  Otherwise it is a scalar based off the font size of the device.
 * 
 * @param pos       the axis position
 * @param t         the indicator type
 * @param s         the indicator size
 * @param i         the indicator axis inset size
 */
AxisIndicator2D::AxisIndicator2D(Axis2DSide pos, Type t, double s, double i) : position(pos),
                                                                               type(t),
                                                                               size(s),
                                                                               inset(i)
{ }

/**
 * Create a duplicate of the indicator on the given side.
 * 
 * @param other     the indicator to copy
 * @param pos       the axis position
 */
AxisIndicator2D::AxisIndicator2D(const AxisIndicator2D &other, Axis2DSide pos) : position(pos),
                                                                                 type(other.type),
                                                                                 size(other.size),
                                                                                 inset(other.inset)
{ }

/**
 * Create a new indicator from the given configuration.
 * 
 * @param configuration the configuration
 */
AxisIndicator2D::AxisIndicator2D(const Variant::Read &configuration) : position(Axis2DBottom),
                                                                       type(Unselected),
                                                                       size(1.0),
                                                                       inset(1.0)
{
    if (configuration["Type"].exists()) {
        const auto &t = configuration["Type"].toString();
        if (Util::equal_insensitive(t, "rectanglefilled", "filledrectangle"))
            type = RectangleFilled;
        else if (Util::equal_insensitive(t, "triangleawayfilled", "filledtriangleaway",
                                         "trianglefilled", "filledtriangle"))
            type = TriangleAwayFilled;
        else if (Util::equal_insensitive(t, "triangletowardsfilled", "filledtriangletowards"))
            type = TriangleTowardsFilled;
        else if (Util::equal_insensitive(t, "diamondfilled", "filleddiamond"))
            type = DiamondFilled;
        else if (Util::equal_insensitive(t, "rectangle"))
            type = Rectangle;
        else if (Util::equal_insensitive(t, "triangleaway", "triangle"))
            type = TriangleAway;
        else if (Util::equal_insensitive(t, "triangletowards"))
            type = TriangleTowards;
        else if (Util::equal_insensitive(t, "diamond"))
            type = Diamond;
        else if (Util::equal_insensitive(t, "spanningline"))
            type = SpanningLine;
        else if (Util::equal_insensitive(t, "fillarea", "areafill", "area"))
            type = AreaFilled;
    }

    double checkSize = configuration["Size"].toDouble();
    if (!FP::defined(checkSize))
        size = 1.0;
    else
        size = checkSize;

    double checkInset = configuration["Inset"].toDouble();
    if (!FP::defined(checkInset))
        inset = 1.0;
    else
        inset = checkInset;
}

/**
 * Save the axis indicator configuration.
 * 
 * @return the configuration for the indicator
 */
Variant::Root AxisIndicator2D::toConfiguration() const
{
    Variant::Root configuration;

    switch (type) {
    case RectangleFilled:
        configuration["Type"].setString("RectangleFilled");
        break;
    case TriangleAwayFilled:
        configuration["Type"].setString("TriangleAwayFilled");
        break;
    case TriangleTowardsFilled:
        configuration["Type"].setString("TriangleTowardsFilled");
        break;
    case DiamondFilled:
        configuration["Type"].setString("DiamondFilled");
        break;
    case Rectangle:
        configuration["Type"].setString("Rectangle");
        break;
    case TriangleAway:
        configuration["Type"].setString("TriangleAway");
        break;
    case TriangleTowards:
        configuration["Type"].setString("TriangleTowards");
        break;
    case Diamond:
        configuration["Type"].setString("Diamond");
        break;
    case Unselected:
        break;
    case SpanningLine:
        configuration["Type"].setString("SpanningLine");
        break;
    case AreaFilled:
        configuration["Type"].setString("FillArea");
        break;
    }

    configuration["Size"].setDouble(size);
    configuration["Inset"].setDouble(inset);

    return configuration;
}

/**
 * Get the indicator type.
 * 
 * @return  the indicator type
 */
AxisIndicator2D::Type AxisIndicator2D::getType() const
{ return type; }

/**
 * Set the indicator type.
 * 
 * @param t the indicator type
 */
void AxisIndicator2D::setType(Type t)
{ type = t; }

/**
 * Select a indicator type, if this symbol is currently unselected.  This also
 * inserts the type into the used list.
 * 
 * @param usedTypes the symbol types that are already in use
 */
void AxisIndicator2D::selectTypeIfNeeded(std::unordered_set<Type, TypeHash> &usedTypes)
{
    if (type != Unselected)
        return;
    for (int i = 0; i < Unselected; i++) {
        if (usedTypes.count(static_cast<Type>(i)))
            continue;
        type = static_cast<Type>(i);
        usedTypes.insert(type);
        return;
    }
}

/**
 * Get the position of the indicator.
 * 
 * @return the indicator position side
 */
Axis2DSide AxisIndicator2D::getPosition() const
{ return position; }

/**
 * Paint an indicator section.   The position to paint must be transformed into
 * valid coordinate space for the painter.  The baseline is the outer edge
 * of the axis (e.x. the top of the area for top axes).  Returns the total
 * depth like predictSize(const std::vector<AxisTickGenerator::Tick> &,
 * QPaintDevice *).
 * 
 * @param start         the start of the indicated section
 * @param end           the end of the indicated section
 * @param painter       the painter to draw with
 * @param baseline      the axis baseline coordinate
 * @param totalDepth    the maximum total depth (for spanning components)
 */
void AxisIndicator2D::paint(qreal start,
                            qreal end,
                            QPainter *painter,
                            qreal baseline,
                            qreal totalDepth) const
{
    Q_ASSERT(FP::defined(start) && FP::defined(end));

    qreal effectiveSize;
    if (size > 0.0) {
        QFont baseFont(QApplication::font());
        baseFont.setPointSizeF(baseFont.pointSizeF() * size);
        QFontMetrics fm(baseFont, painter->device());
        effectiveSize = (qreal) fm.xHeight() * (qreal) 0.5;
    } else {
        effectiveSize = (qreal) (-size);
    }

    qreal effectiveInset;
    if (inset > 0.0) {
        QFont baseFont(QApplication::font());
        baseFont.setPointSizeF(baseFont.pointSizeF() * inset);
        QFontMetrics fm(baseFont, painter->device());
        effectiveInset = (qreal) fm.xHeight() * (qreal) 0.5;
    } else {
        effectiveInset = (qreal) (-inset);
    }

    QPainterPath path;

    switch (type) {
    case RectangleFilled:
    case Rectangle:
        switch (position) {
        case Axis2DTop:
            path.moveTo(start, baseline + effectiveInset);
            path.lineTo(start, baseline + effectiveInset + effectiveSize);
            path.lineTo(end, baseline + effectiveInset + effectiveSize);
            path.lineTo(end, baseline + effectiveInset);
            path.closeSubpath();
            break;
        case Axis2DBottom:
            path.moveTo(start, baseline - effectiveInset);
            path.lineTo(start, baseline - effectiveInset - effectiveSize);
            path.lineTo(end, baseline - effectiveInset - effectiveSize);
            path.lineTo(end, baseline - effectiveInset);
            path.closeSubpath();
            break;
        case Axis2DLeft:
            path.moveTo(baseline + effectiveInset, start);
            path.lineTo(baseline + effectiveInset + effectiveSize, start);
            path.lineTo(baseline + effectiveInset + effectiveSize, end);
            path.lineTo(baseline + effectiveInset, end);
            path.closeSubpath();
            break;
        case Axis2DRight:
            path.moveTo(baseline - effectiveInset, start);
            path.lineTo(baseline - effectiveInset - effectiveSize, start);
            path.lineTo(baseline - effectiveInset - effectiveSize, end);
            path.lineTo(baseline - effectiveInset, end);
            path.closeSubpath();
            break;
        }
        break;
    case TriangleAwayFilled:
    case TriangleAway:
        switch (position) {
        case Axis2DTop:
            path.moveTo(start - 0.43301 * effectiveSize, baseline + effectiveInset);
            path.lineTo(start, baseline + effectiveInset + effectiveSize);
            path.lineTo(end, baseline + effectiveInset + effectiveSize);
            path.lineTo(end + 0.43301 * effectiveSize, baseline + effectiveInset);
            path.closeSubpath();
            break;
        case Axis2DBottom:
            path.moveTo(start - 0.43301 * effectiveSize, baseline - effectiveInset);
            path.lineTo(start, baseline - effectiveInset - effectiveSize);
            path.lineTo(end, baseline - effectiveInset - effectiveSize);
            path.lineTo(end + 0.43301 * effectiveSize, baseline - effectiveInset);
            path.closeSubpath();
            break;
        case Axis2DLeft:
            path.moveTo(baseline + effectiveInset, start - 0.43301 * effectiveSize);
            path.lineTo(baseline + effectiveInset + effectiveSize, start);
            path.lineTo(baseline + effectiveInset + effectiveSize, end);
            path.lineTo(baseline + effectiveInset, end + 0.43301 * effectiveSize);
            path.closeSubpath();
            break;
        case Axis2DRight:
            path.moveTo(baseline - effectiveInset, start - 0.43301 * effectiveSize);
            path.lineTo(baseline - effectiveInset - effectiveSize, start);
            path.lineTo(baseline - effectiveInset - effectiveSize, end);
            path.lineTo(baseline - effectiveInset, end + 0.43301 * effectiveSize);
            path.closeSubpath();
            break;
        }
        break;
    case TriangleTowardsFilled:
    case TriangleTowards:
        switch (position) {
        case Axis2DTop:
            path.moveTo(start - 0.43301 * effectiveSize, baseline + effectiveInset + effectiveSize);
            path.lineTo(start, baseline + effectiveInset);
            path.lineTo(end, baseline + effectiveInset);
            path.lineTo(end + 0.43301 * effectiveSize, baseline + effectiveInset + effectiveSize);
            path.closeSubpath();
            break;
        case Axis2DBottom:
            path.moveTo(start - 0.43301 * effectiveSize, baseline - effectiveInset - effectiveSize);
            path.lineTo(start, baseline - effectiveInset);
            path.lineTo(end, baseline - effectiveInset);
            path.lineTo(end + 0.43301 * effectiveSize, baseline - effectiveInset - effectiveSize);
            path.closeSubpath();
            break;
        case Axis2DLeft:
            path.moveTo(baseline + effectiveInset + effectiveSize, start - 0.43301 * effectiveSize);
            path.lineTo(baseline + effectiveInset, start);
            path.lineTo(baseline + effectiveInset, end);
            path.lineTo(baseline + effectiveInset + effectiveSize, end + 0.43301 * effectiveSize);
            path.closeSubpath();
            break;
        case Axis2DRight:
            path.moveTo(baseline - effectiveInset - effectiveSize, start - 0.43301 * effectiveSize);
            path.lineTo(baseline - effectiveInset, start);
            path.lineTo(baseline - effectiveInset, end);
            path.lineTo(baseline - effectiveInset - effectiveSize, end + 0.43301 * effectiveSize);
            path.closeSubpath();
            break;
        }
        break;
    case DiamondFilled:
    case Diamond:
        switch (position) {
        case Axis2DTop:
            path.moveTo(start, baseline + effectiveInset);
            path.lineTo(start - effectiveSize * 0.5,
                        baseline + effectiveInset + effectiveSize * 0.5);
            path.lineTo(start, baseline + effectiveInset + effectiveSize);
            path.lineTo(end, baseline + effectiveInset + effectiveSize);
            path.lineTo(end + effectiveSize * 0.5, baseline + effectiveInset + effectiveSize * 0.5);
            path.lineTo(end, baseline + effectiveInset);
            path.closeSubpath();
            break;
        case Axis2DBottom:
            path.moveTo(start, baseline - effectiveInset);
            path.lineTo(start - effectiveSize * 0.5,
                        baseline - effectiveInset - effectiveSize * 0.5);
            path.lineTo(start, baseline - effectiveInset - effectiveSize);
            path.lineTo(end, baseline - effectiveInset - effectiveSize);
            path.lineTo(end + effectiveSize * 0.5, baseline - effectiveInset - effectiveSize * 0.5);
            path.lineTo(end, baseline - effectiveInset);
            path.closeSubpath();
            break;
        case Axis2DLeft:
            path.moveTo(baseline + effectiveInset, start);
            path.lineTo(baseline + effectiveInset + effectiveSize * 0.5,
                        start - effectiveSize * 0.5);
            path.lineTo(baseline + effectiveInset + effectiveSize, start);
            path.lineTo(baseline + effectiveInset + effectiveSize, end);
            path.lineTo(baseline + effectiveInset + effectiveSize * 0.5, end + effectiveSize * 0.5);
            path.lineTo(baseline + effectiveInset, end);
            path.closeSubpath();
            break;
        case Axis2DRight:
            path.moveTo(baseline - effectiveInset, start);
            path.lineTo(baseline - effectiveInset - effectiveSize * 0.5,
                        start - effectiveSize * 0.5);
            path.lineTo(baseline - effectiveInset - effectiveSize, start);
            path.lineTo(baseline - effectiveInset - effectiveSize, end);
            path.lineTo(baseline - effectiveInset - effectiveSize * 0.5, end + effectiveSize * 0.5);
            path.lineTo(baseline - effectiveInset, end);
            path.closeSubpath();
            break;
        }
        break;

    case SpanningLine:
        switch (position) {
        case Axis2DTop:
            path.moveTo(start, baseline);
            path.lineTo(start, baseline + totalDepth);
            if (start != end) {
                path.moveTo(end, baseline);
                path.lineTo(end, baseline + totalDepth);
            }
            break;
        case Axis2DBottom:
            path.moveTo(start, baseline);
            path.lineTo(start, baseline - totalDepth);
            if (start != end) {
                path.moveTo(end, baseline);
                path.lineTo(end, baseline - totalDepth);
            }
            break;
        case Axis2DLeft:
            path.moveTo(baseline, start);
            path.lineTo(baseline + totalDepth, start);
            if (start != end) {
                path.moveTo(baseline, end);
                path.lineTo(baseline + totalDepth, end);
            }
            break;
        case Axis2DRight:
            path.moveTo(baseline, start);
            path.lineTo(baseline - totalDepth, start);
            if (start != end) {
                path.moveTo(baseline, end);
                path.lineTo(baseline - totalDepth, end);
            }
            break;
        }
        break;

    case AreaFilled:
        switch (position) {
        case Axis2DTop:
            path.moveTo(start, baseline);
            path.lineTo(start, baseline + totalDepth);
            path.lineTo(end, baseline + totalDepth);
            path.lineTo(end, baseline);
            path.closeSubpath();
            break;
        case Axis2DBottom:
            path.moveTo(start, baseline);
            path.lineTo(start, baseline - totalDepth);
            path.lineTo(end, baseline - totalDepth);
            path.lineTo(end, baseline);
            path.closeSubpath();
            break;
        case Axis2DLeft:
            path.moveTo(baseline, start);
            path.lineTo(baseline + totalDepth, start);
            path.lineTo(baseline + totalDepth, end);
            path.lineTo(baseline, end);
            path.closeSubpath();
            break;
        case Axis2DRight:
            path.moveTo(baseline, start);
            path.lineTo(baseline - totalDepth, start);
            path.lineTo(baseline - totalDepth, end);
            path.lineTo(baseline, end);
            path.closeSubpath();
            break;
        }
        break;

    case Unselected:
        break;
    }

    switch (type) {
    case RectangleFilled:
    case TriangleAwayFilled:
    case TriangleTowardsFilled:
    case DiamondFilled:
    case AreaFilled:
        painter->drawPath(path);
        break;

    case Rectangle:
    case TriangleAway:
    case TriangleTowards:
    case Diamond:
    case SpanningLine:
        painter->strokePath(path, painter->pen());
        break;

    case Unselected:
        break;
    }
}

/**
 * Get the insets that the indicator spans.
 * 
 * @param start         the start of the indicated section
 * @param end           the end of the indicated section
 * @param begin         the output first edge
 * @param end           the output second edge
 * @param paintdevice   the paint device in use or NULL for screen space
 */
void AxisIndicator2D::getInsets(qreal baseline,
                                qreal totalDepth,
                                qreal &begin,
                                qreal &end,
                                QPaintDevice *paintdevice) const
{
    qreal effectiveSize;
    if (size > 0.0) {
        QFont baseFont(QApplication::font());
        baseFont.setPointSizeF(baseFont.pointSizeF() * size);
        QFontMetrics fm(baseFont);
        if (paintdevice)
            fm = QFontMetrics(baseFont, paintdevice);
        effectiveSize = (qreal) fm.xHeight() * (qreal) 0.5;
    } else {
        effectiveSize = (qreal) (-size);
    }

    qreal effectiveInset;
    if (inset > 0.0) {
        QFont baseFont(QApplication::font());
        baseFont.setPointSizeF(baseFont.pointSizeF() * inset);
        QFontMetrics fm(baseFont);
        if (paintdevice)
            fm = QFontMetrics(baseFont, paintdevice);
        effectiveInset = (qreal) fm.xHeight() * (qreal) 0.5;
    } else {
        effectiveInset = (qreal) (-inset);
    }

    begin = baseline;
    end = baseline;

    switch (type) {
    case RectangleFilled:
    case Rectangle:
    case TriangleAwayFilled:
    case TriangleAway:
    case TriangleTowardsFilled:
    case TriangleTowards:
    case DiamondFilled:
    case Diamond:
        switch (position) {
        case Axis2DTop:
        case Axis2DLeft:
            begin = baseline + effectiveInset;
            end = baseline + effectiveInset + effectiveSize;
            break;
        case Axis2DBottom:
        case Axis2DRight:
            begin = baseline - effectiveInset;
            end = baseline - effectiveInset - effectiveSize;
            break;
        }
        break;

    case SpanningLine:
    case AreaFilled:
        switch (position) {
        case Axis2DTop:
        case Axis2DLeft:
            begin = baseline;
            end = baseline + totalDepth;
            break;
        case Axis2DBottom:
        case Axis2DRight:
            begin = baseline;
            end = baseline - totalDepth;
            break;
        }
        break;

    case Unselected:
        break;
    }
}

/**
 * Return a menu used to customize the indicator.
 *
 * @param update    the callback when the indicator is updated
 * @param parent    the parent of the menu and dialogs
 * @return          a menu used to customize the indicator
 */
QMenu *AxisIndicator2D::customizeMenu(const std::function<void(const AxisIndicator2D &)> &update,
                                      QWidget *parent) const
{
    QAction *act;
    QActionGroup *group;
    QMenu *menu;

    AxisIndicator2D output(*this);

    menu = new QMenu(QObject::tr("&Indicator", "Context|SetIndicatorDisplay"), parent);

    act = menu->addAction(QObject::tr("Si&ze", "Context|AxisIndicatorSize"));
    act->setToolTip(QObject::tr("Change the size of the indicator."));
    act->setStatusTip(QObject::tr("Set the indicator size"));
    act->setWhatsThis(QObject::tr("Set the size or scale of the indicator."));
    QObject::connect(act, &QAction::triggered, parent, [update, output, parent] {
        QDialog dialog(parent);
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(QObject::tr("Set Indicator Size"));

        auto layout = new QVBoxLayout(&dialog);
        dialog.setLayout(layout);

        auto mode = new QComboBox(&dialog);
        layout->addWidget(mode);
        auto scale = new QDoubleSpinBox(&dialog);
        layout->addWidget(scale);

        auto configureScale = [scale, mode]() {
            if (mode->currentIndex() == 1) {
                scale->setDecimals(0);
                scale->setRange(1, 1000.0);
                scale->setSingleStep(2);
            } else {
                scale->setDecimals(2);
                scale->setRange(0.01, 100.0);
                scale->setSingleStep(0.1);
            }
        };

        mode->setToolTip(QObject::tr("Select the indicator scaling mode."));
        mode->setStatusTip(QObject::tr("Scaling mode"));
        mode->setWhatsThis(QObject::tr("Select the scaling mode used for the indicator."));
        mode->addItem(QObject::tr("Scale factor"));
        mode->addItem(QObject::tr("Exact pixel size"));
        mode->setCurrentIndex(output.size < 0 ? 1 : 0);
        QObject::connect(mode,
                         static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
                         scale, configureScale);

        scale->setToolTip(QObject::tr("Scale factor or pixel size."));
        scale->setStatusTip(QObject::tr("Indicator size"));
        scale->setWhatsThis(
                QObject::tr("The scale factor fraction or the absolute size in pixels."));
        configureScale();
        scale->setValue(std::fabs(output.size));

        layout->addStretch(1);

        auto buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                                            Qt::Horizontal, &dialog);
        layout->addWidget(buttons);
        QObject::connect(buttons, &QDialogButtonBox::accepted, &dialog, &QDialog::accept);
        QObject::connect(buttons, &QDialogButtonBox::rejected, &dialog, &QDialog::reject);

        if (dialog.exec() != QDialog::Accepted)
            return;
        auto result = output;
        result.size = scale->value();
        if (mode->currentIndex() == 1)
            result.size = -result.size;
        update(result);
    });

    act = menu->addAction(QObject::tr("Inse&t", "Context|AxisIndicatorInset"));
    act->setToolTip(QObject::tr("Change the inset of the indicator."));
    act->setStatusTip(QObject::tr("Set the indicator inset"));
    act->setWhatsThis(QObject::tr("Set the distance the indicator is inset from its axis."));
    QObject::connect(act, &QAction::triggered, parent, [update, output, parent] {
        QDialog dialog(parent);
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(QObject::tr("Set Indicator Inset"));

        auto layout = new QVBoxLayout(&dialog);
        dialog.setLayout(layout);

        auto mode = new QComboBox(&dialog);
        layout->addWidget(mode);
        auto scale = new QDoubleSpinBox(&dialog);
        layout->addWidget(scale);

        auto configureScale = [scale, mode]() {
            if (mode->currentIndex() == 1) {
                scale->setDecimals(0);
                scale->setRange(1, 1000.0);
                scale->setSingleStep(2);
            } else {
                scale->setDecimals(2);
                scale->setRange(0.01, 100.0);
                scale->setSingleStep(0.1);
            }
        };

        mode->setToolTip(QObject::tr("Select the indicator inset scaling mode."));
        mode->setStatusTip(QObject::tr("Scaling mode"));
        mode->setWhatsThis(QObject::tr("Select the scaling mode used for the indicator inset."));
        mode->addItem(QObject::tr("Scale factor"));
        mode->addItem(QObject::tr("Exact pixel size"));
        mode->setCurrentIndex(output.size < 0 ? 1 : 0);
        QObject::connect(mode,
                         static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
                         scale, configureScale);

        scale->setToolTip(QObject::tr("Scale factor or pixel size."));
        scale->setStatusTip(QObject::tr("Inset size"));
        scale->setWhatsThis(
                QObject::tr("The scale factor fraction or the absolute size in pixels."));
        configureScale();
        scale->setValue(std::fabs(output.inset));

        layout->addStretch(1);

        auto buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                                            Qt::Horizontal, &dialog);
        layout->addWidget(buttons);
        QObject::connect(buttons, &QDialogButtonBox::accepted, &dialog, &QDialog::accept);
        QObject::connect(buttons, &QDialogButtonBox::rejected, &dialog, &QDialog::reject);

        if (dialog.exec() != QDialog::Accepted)
            return;
        auto result = output;
        result.inset = scale->value();
        if (mode->currentIndex() == 1)
            result.inset = -result.inset;
        update(result);
    });


    menu->addSeparator();

    group = new QActionGroup(menu);

    class IndicatorEngine : public QIconEngine {
        AxisIndicator2D indicator;
    public:
        explicit IndicatorEngine(AxisIndicator2D::Type type)
        {
            indicator.type = type;
            indicator.inset = 0;
            indicator.position = Axis2DBottom;
        }

        virtual ~IndicatorEngine() = default;

        QIconEngine *clone() const override
        { return new IndicatorEngine(indicator.getType()); }

        void paint(QPainter *painter,
                   const QRect &rect,
                   QIcon::Mode mode,
                   QIcon::State state) override
        {
            painter->save();

            painter->translate(QPoint(rect.topLeft()) + QPoint(rect.width() / 2, rect.height()));

            auto size = std::min(rect.width() - 6, rect.height());
            size /= 2;
            --size;
            if (size < 1)
                size = 1;
            indicator.size = -size;

            auto color = QApplication::palette().text().color();
            painter->setBrush(QBrush(color));
            painter->setPen(color);
            indicator.paint(-3, 3, painter, -3, rect.height());

            painter->restore();
        }

        QPixmap pixmap(const QSize &size, QIcon::Mode mode, QIcon::State state) override
        {
            QImage img(size, QImage::Format_ARGB32);
            img.fill(0x00FFFFFF);
            QPixmap result = QPixmap::fromImage(img, Qt::NoFormatConversion);
            {
                QPainter painter(&result);
                this->paint(&painter, QRect(QPoint(0, 0), size), mode, state);
            }
            return result;
        }
    };

    act = menu->addAction(QObject::QObject::tr("&Automatic", "Context|AxisIndicatorAutomatic"));
    act->setToolTip(QObject::tr("Automatically select the indicator type to use."));
    act->setStatusTip(QObject::tr("Set indicator type"));
    act->setWhatsThis(
            QObject::tr("Set the indicator to automatically select an unused display type."));
    act->setCheckable(true);
    act->setChecked(type == AxisIndicator2D::Unselected);
    act->setActionGroup(group);
    output.setType(AxisIndicator2D::Unselected);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Filled &Rectangle", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A solid colored rectangle."));
    act->setStatusTip(QObject::tr("Set indicator display"));
    act->setWhatsThis(QObject::tr(
            "Set the indicator to display a filled rectangle from the start to the end."));
    act->setCheckable(true);
    act->setChecked(type == AxisIndicator2D::RectangleFilled);
    act->setActionGroup(group);
    act->setIcon(QIcon(new IndicatorEngine(AxisIndicator2D::RectangleFilled)));
    output.setType(AxisIndicator2D::RectangleFilled);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Filled &Triangle (away)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A solid colored triangle pointing away from the axis."));
    act->setStatusTip(QObject::tr("Set indicator display"));
    act->setWhatsThis(QObject::tr(
            "Set the indicator to display a filled triangle pointing away from the axis, distorted into a isosceles trapezoid from the start to the end."));
    act->setCheckable(true);
    act->setChecked(type == AxisIndicator2D::TriangleAwayFilled);
    act->setActionGroup(group);
    act->setIcon(QIcon(new IndicatorEngine(AxisIndicator2D::TriangleAwayFilled)));
    output.setType(AxisIndicator2D::TriangleAwayFilled);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Filled Tr&iangle (towards)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A solid colored triangle pointing towards from the axis."));
    act->setStatusTip(QObject::tr("Set indicator display"));
    act->setWhatsThis(QObject::tr(
            "Set the indicator to display a filled triangle pointing towards from the axis, distorted into a isosceles trapezoid from the start to the end."));
    act->setCheckable(true);
    act->setChecked(type == AxisIndicator2D::TriangleTowardsFilled);
    act->setActionGroup(group);
    act->setIcon(QIcon(new IndicatorEngine(AxisIndicator2D::TriangleTowardsFilled)));
    output.setType(AxisIndicator2D::TriangleTowardsFilled);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Filled &Diamond", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A solid colored diamond."));
    act->setStatusTip(QObject::tr("Set indicator display"));
    act->setWhatsThis(QObject::tr(
            "Set the indicator to display a filled diamond, distorted into a hexagon from the start to the end."));
    act->setCheckable(true);
    act->setChecked(type == AxisIndicator2D::DiamondFilled);
    act->setActionGroup(group);
    act->setIcon(QIcon(new IndicatorEngine(AxisIndicator2D::DiamondFilled)));
    output.setType(AxisIndicator2D::DiamondFilled);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Rectangle", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("An outlined rectangle."));
    act->setStatusTip(QObject::tr("Set indicator display"));
    act->setWhatsThis(QObject::tr(
            "Set the indicator to display the outline of a rectangle from the start to the end."));
    act->setCheckable(true);
    act->setChecked(type == AxisIndicator2D::Rectangle);
    act->setActionGroup(group);
    act->setIcon(QIcon(new IndicatorEngine(AxisIndicator2D::Rectangle)));
    output.setType(AxisIndicator2D::Rectangle);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Triangle (away)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("The outline of a triangle pointing away from the axis."));
    act->setStatusTip(QObject::tr("Set indicator display"));
    act->setWhatsThis(QObject::tr(
            "Set the indicator to display the outline of a triangle pointing away from the axis, distorted into a isosceles trapezoid from the start to the end."));
    act->setCheckable(true);
    act->setChecked(type == AxisIndicator2D::TriangleAway);
    act->setActionGroup(group);
    act->setIcon(QIcon(new IndicatorEngine(AxisIndicator2D::TriangleAway)));
    output.setType(AxisIndicator2D::TriangleAway);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Triangle (towards)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("The outline of a triangle pointing towards from the axis."));
    act->setStatusTip(QObject::tr("Set indicator display"));
    act->setWhatsThis(QObject::tr(
            "Set the indicator to display the outline of a triangle pointing towards from the axis, distorted into a isosceles trapezoid from the start to the end."));
    act->setCheckable(true);
    act->setChecked(type == AxisIndicator2D::TriangleTowards);
    act->setActionGroup(group);
    act->setIcon(QIcon(new IndicatorEngine(AxisIndicator2D::TriangleTowards)));
    output.setType(AxisIndicator2D::TriangleTowards);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Diamond", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("The outline of a diamond."));
    act->setStatusTip(QObject::tr("Set indicator display"));
    act->setWhatsThis(QObject::tr(
            "Set the indicator to display the outline of a diamond, distorted into a hexagon from the start to the end."));
    act->setCheckable(true);
    act->setChecked(type == AxisIndicator2D::Diamond);
    act->setActionGroup(group);
    act->setIcon(QIcon(new IndicatorEngine(AxisIndicator2D::Diamond)));
    output.setType(AxisIndicator2D::Diamond);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Spanning Line", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A line spanning the whole graph."));
    act->setStatusTip(QObject::tr("Set indicator display"));
    act->setWhatsThis(QObject::tr(
            "Set the indicator to display a line spanning the whole graph at the start and end of each indicated region.  This is most useful for event indicators, rather than continuous ones."));
    act->setCheckable(true);
    act->setChecked(type == AxisIndicator2D::SpanningLine);
    act->setActionGroup(group);
    act->setIcon(QIcon(new IndicatorEngine(AxisIndicator2D::SpanningLine)));
    output.setType(AxisIndicator2D::SpanningLine);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Filled Area", "Context|TraceSymbolSelect"));
    act->setToolTip(
            QObject::tr("A solid filled area across the whole graph from the start to the end."));
    act->setStatusTip(QObject::tr("Set indicator display"));
    act->setWhatsThis(QObject::tr(
            "Set the indicator to fill the entire area from the start to the end across the whole graph.  This is most useful when the indicator is set to draw below the data or the color selected is partially transparent."));
    act->setCheckable(true);
    act->setChecked(type == AxisIndicator2D::AreaFilled);
    act->setActionGroup(group);
    act->setIcon(QIcon(new IndicatorEngine(AxisIndicator2D::AreaFilled)));
    output.setType(AxisIndicator2D::AreaFilled);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    return menu;
}


AxisTransformerBoundDialog::AxisTransformerBoundDialog(QWidget *parent) : QDialog(parent)
{
    auto layout = new QVBoxLayout(this);
    setLayout(layout);

    mode = new QComboBox(this);
    layout->addWidget(mode);
    mode->setToolTip(tr("Select the axis bound mode."));
    mode->setStatusTip(tr("Axis bound mode"));
    mode->setWhatsThis(
            tr("Select how the axis transforms the bound to the final range displayed."));
    mode->addItem(tr("Fixed"), static_cast<int>(AxisTransformer::Fixed));
    mode->addItem(tr("Fractional extension"), static_cast<int>(AxisTransformer::ExtendFraction));
    mode->addItem(tr("Absolute extension"), static_cast<int>(AxisTransformer::ExtendAbsolute));
    mode->addItem(tr("Next base-X (logarithmic)"), static_cast<int>(AxisTransformer::ExtendPower));
    connect(mode, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this,
            &AxisTransformerBoundDialog::modeChanged);


    displayFixed = new QWidget(this);
    layout->addWidget(displayFixed);
    auto form = new QFormLayout(displayFixed);
    form->setContentsMargins(0, 0, 0, 0);
    displayFixed->setLayout(form);

    fixedValue = new QLineEdit(displayFixed);
    form->addRow(tr("Value:"), fixedValue);
    fixedValue->setToolTip(tr("The fixed axis bound."));
    fixedValue->setStatusTip(tr("Axis bound"));
    fixedValue->setWhatsThis(tr("This sets the axis bound to an exact and fixed value."));
    fixedValue->setValidator(new QDoubleValidator(fixedValue));


    displayExtendFraction = new QWidget(this);
    layout->addWidget(displayExtendFraction);
    form = new QFormLayout(displayExtendFraction);
    form->setContentsMargins(0, 0, 0, 0);
    displayExtendFraction->setLayout(form);

    extendFraction = new QLineEdit(displayExtendFraction);
    form->addRow(tr("Extension:"), extendFraction);
    extendFraction->setToolTip(tr("The fraction of the base range the axis is extended by."));
    extendFraction->setStatusTip(tr("Axis extension"));
    extendFraction->setWhatsThis(
            tr("This sets the fraction of the input data range that the axis bound is extended outwards by."));
    extendFraction->setValidator(new QDoubleValidator(extendFraction));
    extendFraction->setPlaceholderText(tr("0.1"));

    extendFractionAtLeast = new QLineEdit(displayExtendFraction);
    form->addRow(tr("At least:"), extendFractionAtLeast);
    extendFractionAtLeast->setToolTip(tr("The minimum amount of extension performed."));
    extendFractionAtLeast->setStatusTip(tr("Minimum extension"));
    extendFractionAtLeast->setWhatsThis(
            tr("This sets the minimum amount of extension performed.  If the range multiplied by the fraction is less than this, this value is used instead."));
    extendFractionAtLeast->setValidator(new QDoubleValidator(extendFractionAtLeast));
    extendFractionAtLeast->setPlaceholderText(tr("None"));

    extendFractionAtMost = new QLineEdit(displayExtendFraction);
    form->addRow(tr("At most:"), extendFractionAtMost);
    extendFractionAtMost->setToolTip(tr("The maximum amount of extension performed."));
    extendFractionAtMost->setStatusTip(tr("Maximum extension"));
    extendFractionAtMost->setWhatsThis(
            tr("This sets the maximum amount of extension performed.  If the range multiplied by the fraction is greater than this, this value is used instead."));
    extendFractionAtMost->setValidator(new QDoubleValidator(extendFractionAtMost));
    extendFractionAtMost->setPlaceholderText(tr("Unlimited"));


    displayExtendAbsolute = new QWidget(this);
    layout->addWidget(displayExtendAbsolute);
    form = new QFormLayout(displayExtendAbsolute);
    form->setContentsMargins(0, 0, 0, 0);
    displayExtendAbsolute->setLayout(form);

    extendAbsoluteAmount = new QLineEdit(displayExtendAbsolute);
    form->addRow(tr("Amount:"), extendAbsoluteAmount);
    extendAbsoluteAmount->setToolTip(tr("The amount the axis bound is extended by."));
    extendAbsoluteAmount->setStatusTip(tr("Axis extension"));
    extendAbsoluteAmount->setWhatsThis(
            tr("This sets the fixed amount over the input range that the axis bound is extended by."));
    extendAbsoluteAmount->setValidator(new QDoubleValidator(extendAbsoluteAmount));
    extendAbsoluteAmount->setPlaceholderText(tr("0"));


    displayExtendPower = new QWidget(this);
    layout->addWidget(displayExtendPower);
    form = new QFormLayout(displayExtendPower);
    form->setContentsMargins(0, 0, 0, 0);
    displayExtendPower->setLayout(form);

    extendPowerBase = new QLineEdit(displayExtendPower);
    form->addRow(tr("Amount:"), extendPowerBase);
    extendPowerBase->setToolTip(tr("The numeric base the axis is extended to."));
    extendPowerBase->setStatusTip(tr("Axis extension base"));
    extendPowerBase->setWhatsThis(
            tr("This sets the numeric base the axis is extended to.  For example, setting this to 10 will extend to 0.1, 1, 10, 100, etc."));
    extendPowerBase->setValidator(new QDoubleValidator(extendAbsoluteAmount));
    extendPowerBase->setPlaceholderText(tr("10"));


    auto box = new QGroupBox(this);
    layout->addWidget(box);
    box->setTitle(tr("Extension limits"));
    form = new QFormLayout(box);
    box->setLayout(form);

    absoluteLimit = new QLineEdit(box);
    form->addRow(tr("Absolute:"), absoluteLimit);
    absoluteLimit->setToolTip(tr("The absolute limit of the axis."));
    absoluteLimit->setStatusTip(tr("Absolute limit"));
    absoluteLimit->setWhatsThis(
            tr("This sets the absolute limit of the axis.  Extension will not cause the displayed range to exceed this value."));
    absoluteLimit->setValidator(new QDoubleValidator(absoluteLimit));
    absoluteLimit->setPlaceholderText(tr("None"));

    requiredLimit = new QLineEdit(box);
    form->addRow(tr("Required:"), requiredLimit);
    requiredLimit->setToolTip(tr("The required value of the axis."));
    requiredLimit->setStatusTip(tr("Required value"));
    requiredLimit->setWhatsThis(
            tr("This sets the required value limit of the axis.  The axis will always include this value, regardless of the extension or input range."));
    requiredLimit->setValidator(new QDoubleValidator(requiredLimit));
    requiredLimit->setPlaceholderText(tr("None"));


    layout->addStretch(1);

    QDialogButtonBox *buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 this);
    layout->addWidget(buttons);
    connect(buttons, &QDialogButtonBox::accepted, this, &AxisTransformerBoundDialog::accept);
    connect(buttons, &QDialogButtonBox::rejected, this, &AxisTransformerBoundDialog::reject);

    modeChanged();
}

AxisTransformerBoundDialog::~AxisTransformerBoundDialog() = default;

void AxisTransformerBoundDialog::setFromMinimum(const AxisTransformer &transformer)
{
    mode->setCurrentIndex(mode->findData(static_cast<int>(transformer.minMode)));
    switch (transformer.minMode) {
    case AxisTransformer::Fixed:
        fixedValue->setText(QString::number(transformer.boundMin));
        break;
    case AxisTransformer::ExtendFraction:
        extendFraction->setText(QString::number(transformer.boundMin));
        extendFraction->setText(QString::number(transformer.boundMin));
        if (FP::defined(transformer.boundMinFractionAtLeast))
            extendFractionAtLeast->setText(QString::number(transformer.boundMinFractionAtLeast));
        else
            extendFractionAtLeast->setText({});
        if (FP::defined(transformer.boundMinFractionAtMost))
            extendFractionAtMost->setText(QString::number(transformer.boundMinFractionAtMost));
        else
            extendFractionAtMost->setText({});
        break;
    case AxisTransformer::ExtendAbsolute:
        extendAbsoluteAmount->setText(QString::number(transformer.boundMin));
        break;
    case AxisTransformer::ExtendPower:
        extendPowerBase->setText(QString::number(transformer.boundMin));
        break;
    }

    if (FP::defined(transformer.absoluteMin)) {
        absoluteLimit->setText(QString::number(transformer.absoluteMin));
    } else {
        absoluteLimit->setText({});
    }
    if (FP::defined(transformer.requiredMin)) {
        requiredLimit->setText(QString::number(transformer.requiredMin));
    } else {
        requiredLimit->setText({});
    }
}

void AxisTransformerBoundDialog::setFromMaximum(const AxisTransformer &transformer)
{
    mode->setCurrentIndex(mode->findData(static_cast<int>(transformer.maxMode)));
    switch (transformer.maxMode) {
    case AxisTransformer::Fixed:
        fixedValue->setText(QString::number(transformer.boundMax));
        break;
    case AxisTransformer::ExtendFraction:
        extendFraction->setText(QString::number(transformer.boundMax));
        if (FP::defined(transformer.boundMaxFractionAtLeast))
            extendFractionAtLeast->setText(QString::number(transformer.boundMaxFractionAtLeast));
        else
            extendFractionAtLeast->setText({});
        if (FP::defined(transformer.boundMaxFractionAtMost))
            extendFractionAtMost->setText(QString::number(transformer.boundMaxFractionAtMost));
        else
            extendFractionAtMost->setText({});
        break;
    case AxisTransformer::ExtendAbsolute:
        extendAbsoluteAmount->setText(QString::number(transformer.boundMax));
        break;
    case AxisTransformer::ExtendPower:
        extendPowerBase->setText(QString::number(transformer.boundMax));
        break;
    }

    if (FP::defined(transformer.absoluteMax)) {
        absoluteLimit->setText(QString::number(transformer.absoluteMax));
    } else {
        absoluteLimit->setText({});
    }
    if (FP::defined(transformer.requiredMax)) {
        requiredLimit->setText(QString::number(transformer.requiredMax));
    } else {
        requiredLimit->setText({});
    }
}

void AxisTransformerBoundDialog::setFromConfiguration(const Data::Variant::Read &configuration)
{
    const auto &type = configuration["Type"].toString();
    if (Util::equal_insensitive(type, "fixed") && configuration["Value"].exists()) {
        mode->setCurrentIndex(mode->findData(static_cast<int>(AxisTransformer::Fixed)));
        double v = configuration["Value"].toDouble();
        if (!FP::defined(v))
            v = 0.0;
        fixedValue->setText(QString::number(v));
    } else if (Util::equal_insensitive(type, "extendabsolute")) {
        mode->setCurrentIndex(mode->findData(static_cast<int>(AxisTransformer::ExtendAbsolute)));
        double v = configuration["Amount"].toDouble();
        if (!FP::defined(v))
            v = 0.0;
        extendAbsoluteAmount->setText(QString::number(v));
    } else if (Util::equal_insensitive(type, "powerround")) {
        mode->setCurrentIndex(mode->findData(static_cast<int>(AxisTransformer::ExtendPower)));
        double v = configuration["Base"].toDouble();
        if (!FP::defined(v) || v <= 1.0)
            v = 10.0;
        extendPowerBase->setText(QString::number(v));
    } else {
        mode->setCurrentIndex(mode->findData(static_cast<int>(AxisTransformer::ExtendFraction)));
        double v = configuration["Fraction"].toDouble();
        if (!FP::defined(v))
            v = 0.1;
        extendFraction->setText(QString::number(v));
        v = configuration["AtLeast"].toDouble();
        if (FP::defined(v))
            extendFractionAtLeast->setText(QString::number(v));
        else
            extendFractionAtLeast->setText({});
        v = configuration["AtMost"].toDouble();
        if (FP::defined(v))
            extendFractionAtMost->setText(QString::number(v));
        else
            extendFractionAtMost->setText({});
    }

    double v = configuration["Absolute"].toDouble();
    if (FP::defined(v)) {
        absoluteLimit->setText(QString::number(v));
    } else {
        absoluteLimit->setText({});
    }
    v = configuration["Required"].toDouble();
    if (FP::defined(v)) {
        requiredLimit->setText(QString::number(v));
    } else {
        requiredLimit->setText({});
    }
}

void AxisTransformerBoundDialog::applyToMinimum(AxisTransformer &transformer)
{
    switch (static_cast<AxisTransformer::BoundMode>(mode->currentData().toInt())) {
    case AxisTransformer::Fixed: {
        bool ok = false;
        double v = fixedValue->text().toDouble(&ok);
        if (!ok)
            v = 0.0;
        transformer.setMinFixed(v);
        break;
    }
    case AxisTransformer::ExtendFraction: {
        bool ok = false;
        double fraction = extendFraction->text().toDouble(&ok);
        if (!ok)
            fraction = 0.1;
        double atLeast = extendFractionAtLeast->text().toDouble(&ok);
        if (!ok)
            atLeast = FP::undefined();
        double atMost = extendFractionAtMost->text().toDouble(&ok);
        if (!ok)
            atMost = FP::undefined();
        transformer.setMinExtendFraction(fraction, atLeast, atMost);
        break;
    }
    case AxisTransformer::ExtendAbsolute: {
        bool ok = false;
        double v = extendAbsoluteAmount->text().toDouble(&ok);
        if (!ok)
            v = 0.0;
        transformer.setMinExtendAbsolute(v);
        break;
    }
    case AxisTransformer::ExtendPower: {
        bool ok = false;
        double v = extendPowerBase->text().toDouble(&ok);
        if (!ok || !FP::defined(v) || v <= 1.0)
            v = 10.0;
        transformer.setMinExtendPowerRound(v);
        break;
    }
    }

    bool ok = false;
    double v = absoluteLimit->text().toDouble(&ok);
    if (!ok)
        v = FP::undefined();
    transformer.setAbsoluteMin(v);

    ok = false;
    v = requiredLimit->text().toDouble(&ok);
    if (!ok)
        v = FP::undefined();
    transformer.setRequiredMin(v);
}

void AxisTransformerBoundDialog::applyToMaximum(AxisTransformer &transformer)
{
    switch (static_cast<AxisTransformer::BoundMode>(mode->currentData().toInt())) {
    case AxisTransformer::Fixed: {
        bool ok = false;
        double v = fixedValue->text().toDouble(&ok);
        if (!ok)
            v = 0.0;
        transformer.setMaxFixed(v);
        break;
    }
    case AxisTransformer::ExtendFraction: {
        bool ok = false;
        double fraction = extendFraction->text().toDouble(&ok);
        if (!ok)
            fraction = 0.1;
        double atLeast = extendFractionAtLeast->text().toDouble(&ok);
        if (!ok)
            atLeast = FP::undefined();
        double atMost = extendFractionAtMost->text().toDouble(&ok);
        if (!ok)
            atMost = FP::undefined();
        transformer.setMaxExtendFraction(fraction, atLeast, atMost);
        break;
    }
    case AxisTransformer::ExtendAbsolute: {
        bool ok = false;
        double v = extendAbsoluteAmount->text().toDouble(&ok);
        if (!ok)
            v = 0.0;
        transformer.setMaxExtendAbsolute(v);
        break;
    }
    case AxisTransformer::ExtendPower: {
        bool ok = false;
        double v = extendPowerBase->text().toDouble(&ok);
        if (!ok || !FP::defined(v) || v <= 1.0)
            v = 10.0;
        transformer.setMaxExtendPowerRound(v);
        break;
    }
    }

    bool ok = false;
    double v = absoluteLimit->text().toDouble(&ok);
    if (!ok)
        v = FP::undefined();
    transformer.setAbsoluteMax(v);

    ok = false;
    v = requiredLimit->text().toDouble(&ok);
    if (!ok)
        v = FP::undefined();
    transformer.setRequiredMax(v);
}

Data::Variant::Root AxisTransformerBoundDialog::toConfiguration() const
{
    Variant::Root result;

    switch (static_cast<AxisTransformer::BoundMode>(mode->currentData().toInt())) {
    case AxisTransformer::Fixed: {
        result["Type"] = "Fixed";
        bool ok = false;
        double v = fixedValue->text().toDouble(&ok);
        if (!ok)
            v = 0.0;
        result["Value"] = v;
        break;
    }
    case AxisTransformer::ExtendFraction: {
        result["Type"] = "ExtendFraction";
        bool ok = false;
        double v = extendFraction->text().toDouble(&ok);
        if (!ok)
            v = 0.1;
        result["Fraction"] = v;
        v = extendFractionAtLeast->text().toDouble(&ok);
        if (!ok)
            result["AtLeast"].setEmpty();
        else
            result["AtLeast"] = v;
        v = extendFractionAtMost->text().toDouble(&ok);
        if (!ok)
            result["AtMost"].setEmpty();
        else
            result["AtMost"] = v;
        break;
    }
    case AxisTransformer::ExtendAbsolute: {
        result["Type"] = "ExtendAbsolute";
        bool ok = false;
        double v = extendAbsoluteAmount->text().toDouble(&ok);
        if (!ok)
            v = 0.0;
        result["Amount"] = v;
        break;
    }
    case AxisTransformer::ExtendPower: {
        result["Type"] = "PowerRound";
        bool ok = false;
        double v = extendPowerBase->text().toDouble(&ok);
        if (!ok || !FP::defined(v) || v <= 1.0)
            v = 10.0;
        result["Base"] = v;
        break;
    }
    }

    bool ok = false;
    double v = absoluteLimit->text().toDouble(&ok);
    if (!ok || !FP::defined(v))
        result["Absolute"].setEmpty();
    else
        result["Absolute"] = v;

    ok = false;
    v = requiredLimit->text().toDouble(&ok);
    if (!ok || !FP::defined(v))
        result["Required"].setEmpty();
    else
        result["Required"] = v;

    return result;
}

void AxisTransformerBoundDialog::modeChanged()
{
    auto active = static_cast<AxisTransformer::BoundMode>(mode->currentData().toInt());
    displayFixed->setVisible(active == AxisTransformer::Fixed);
    displayExtendFraction->setVisible(active == AxisTransformer::ExtendFraction);
    displayExtendAbsolute->setVisible(active == AxisTransformer::ExtendAbsolute);
    displayExtendPower->setVisible(active == AxisTransformer::ExtendPower);
}

}
}
