/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>
#include <QGridLayout>
#include <QCheckBox>
#include <QLineEdit>
#include <QDialogButtonBox>

#include "graphing/graphcommon2d.hxx"
#include "graphing/graph2d.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Graphing::Internal;

namespace CPD3 {
namespace Graphing {

static const qreal defaultLegendBoxSize = 0.25;
static const qreal defaultLegendBoxInset = 0.5;
static const int defaultHighlightAlpha = 64;
static const qreal defaultTitleScale = 1.5;

/** @file graphing/graphcommon2d.hxx
 * The shared components of two dimensional graphs.
 */

/**
 * Create a new primitive with the given priority.
 * 
 * @param setSortPriority   the draw sorting priority
 */
Graph2DDrawPrimitive::Graph2DDrawPrimitive(int setSortPriority) : sortPriority(setSortPriority)
{ }

Graph2DDrawPrimitive::~Graph2DDrawPrimitive() = default;

void Graph2DDrawPrimitive::updateMouseover(const QPointF &,
                                           std::shared_ptr<Graph2DMouseoverComponent> &)
{ }

void Graph2DDrawPrimitive::updateMouseClick(const QPointF &point,
                                            std::shared_ptr<Graph2DMouseoverComponent> &output)
{ updateMouseover(point, output); }

/**
 * Compare this primitive with the other one.
 * 
 * @return true if this one should be drawn before the other.
 */
bool Graph2DDrawPrimitive::drawLessThan(const Graph2DDrawPrimitive *other) const
{
    return sortPriority < other->sortPriority;
}

GraphLegendItem2D::GraphLegendItem2D(const QString &setText,
                                     const QColor &setColor,
                                     const QFont &setFont,
                                     qreal setLineWidth,
                                     Qt::PenStyle setStyle,
                                     const TraceSymbol &setSymbol,
                                     bool setDrawSymbol,
                                     bool setDrawSwatch) : LegendItem(setText, setColor, setFont,
                                                                      setLineWidth, setStyle,
                                                                      setSymbol, setDrawSymbol,
                                                                      setDrawSwatch)
{ }

/**
 * Create a modification component for the legend item.
 * <br>
 * The default implementation returns NULL (no component).
 * 
 * @return a new modification component or NULL for none
 */
DisplayModificationComponent *GraphLegendItem2D::createModificationComponent(Graph2D *)
{ return nullptr; }

/**
 * Get the legend descriptive tooltip for the item.
 * 
 * @return  the tooltip description
 */
QString GraphLegendItem2D::getLegendTooltip() const
{ return {}; }

Graph2DMouseoverComponent::Graph2DMouseoverComponent() = default;

Graph2DMouseoverComponent::~Graph2DMouseoverComponent() = default;

/**
 * Create a new trace component.
 * 
 * @param setCenter     the screen space center
 * @param setTracePtr   the trace pointer ID
 */
Graph2DMouseoverComponentTrace::Graph2DMouseoverComponentTrace(const QPointF &setCenter,
                                                               quintptr setTracePtr) : center(
        setCenter), tracePtr(setTracePtr)
{ }

Graph2DMouseoverComponentTrace::~Graph2DMouseoverComponentTrace() = default;

/**
 * Create a new standard trace component.
 * 
 * @param setCenter     the screen space center
 * @param setTracePtr   the trace pointer ID
 * @param setPoint      the real trace point
 */
Graph2DMouseoverComponentTraceStandard::Graph2DMouseoverComponentTraceStandard(const QPointF &setCenter,
                                                                               quintptr setTracePtr,
                                                                               const TracePoint<
                                                                                       3> &setPoint)
        : Graph2DMouseoverComponentTrace(setCenter, setTracePtr), point(setPoint)
{ }

Graph2DMouseoverComponentTraceStandard::~Graph2DMouseoverComponentTraceStandard() = default;

/**
 * Create a new trace fit component.
 * 
 * @param setCenter     the screen space center
 * @param setTracePtr   the trace pointer ID
 * @param sX            the real X coordinate
 * @param sY            the real Y coordinate
 * @param sZ            the real Z coordinate (if any)
 * @param sI            the real I coordinate (if any)
 */
Graph2DMouseoverComponentTraceFit::Graph2DMouseoverComponentTraceFit(const QPointF &setCenter,
                                                                     quintptr setTracePtr,
                                                                     double sX,
                                                                     double sY,
                                                                     double sZ,
                                                                     double sI)
        : Graph2DMouseoverComponentTrace(setCenter, setTracePtr), x(sX), y(sY), z(sZ), i(sI)
{ }

Graph2DMouseoverComponentTraceFit::~Graph2DMouseoverComponentTraceFit() = default;

/**
 * Create a new trace fill model scan component.
 * 
 * @param setCenter     the screen space center
 * @param setTracePtr   the trace pointer ID
 * @param sX            the real X coordinate
 * @param sY            the real Y coordinate
 * @param sZ            the real Z coordinate
 */
Graph2DMouseoverComponentTraceFillModelScan::Graph2DMouseoverComponentTraceFillModelScan(const QPointF &setCenter,
                                                                                         quintptr setTracePtr,
                                                                                         double sX,
                                                                                         double sY,
                                                                                         double sZ)
        : Graph2DMouseoverComponentTrace(setCenter, setTracePtr), x(sX), y(sY), z(sZ)
{ }

Graph2DMouseoverComponentTraceFillModelScan::~Graph2DMouseoverComponentTraceFillModelScan() = default;

/**
 * Create a new box whisker hit component.
 * 
 * @param setCenter     the screen space center
 * @param setTracePtr   the trace pointer ID
 * @param setPoint      the bin point hit
 * @param setLocation   the box location
 */
Graph2DMouseoverComponentTraceBox::Graph2DMouseoverComponentTraceBox(const QPointF &setCenter,
                                                                     quintptr setTracePtr,
                                                                     const BinPoint<1, 1> &setPoint,
                                                                     Location setLocation)
        : Graph2DMouseoverComponentTrace(setCenter, setTracePtr),
          point(setPoint),
          location(setLocation)
{ }

Graph2DMouseoverComponentTraceBox::~Graph2DMouseoverComponentTraceBox() = default;

/**
 * Create a new indicator hit component.
 * 
 * @param setCenter     the screen space center
 * @param setTracePtr   the trace pointer ID
 * @param setPoint      the indicator point hit
 */
Graph2DMouseoverComponentIndicator::Graph2DMouseoverComponentIndicator(const QPointF &setCenter,
                                                                       quintptr setTracePtr,
                                                                       const IndicatorPoint<
                                                                               1> &setPoint,
                                                                       Axis2DSide setSide)
        : Graph2DMouseoverComponentTrace(setCenter, setTracePtr), point(setPoint), side(setSide)
{ }

Graph2DMouseoverComponentIndicator::~Graph2DMouseoverComponentIndicator() = default;

Graph2DMouseoverTitle::Graph2DMouseoverTitle() = default;

Graph2DMouseoverTitle::~Graph2DMouseoverTitle() = default;

Graph2DMouseoverLegend::Graph2DMouseoverLegend() = default;

Graph2DMouseoverLegend::~Graph2DMouseoverLegend() = default;

Graph2DMouseoverLegendItem::Graph2DMouseoverLegendItem(const std::shared_ptr<
        GraphLegendItem2D> &setItem) : item(setItem)
{ }

Graph2DMouseoverLegendItem::~Graph2DMouseoverLegendItem() = default;

Graph2DMouseoverAxis::Graph2DMouseoverAxis(quintptr setAxisPtr) : axisPtr(setAxisPtr)
{ }

Graph2DMouseoverAxis::~Graph2DMouseoverAxis() = default;

Graph2DParameters::~Graph2DParameters() = default;

Graph2DParameters::Graph2DParameters() : components(0),
                                         titleText(),
                                         titleFont(),
                                         titleColor(0, 0, 0),
                                         legendEnable(true),
                                         legendPosition(Inside_TopLeft),
                                         legendBoxXInset(defaultLegendBoxInset),
                                         legendBoxYInset(defaultLegendBoxInset),
                                         legendBoxSize(defaultLegendBoxSize),
                                         legendBoxOutlineWidth(1.0),
                                         legendBoxOutlineStyle(Qt::SolidLine),
                                         legendBoxOutlineColor(0, 0, 0),
                                         legendBoxFillColor(255, 255, 255),
                                         legendDrawPriority(Graph2DDrawPrimitive::Priority_Legend),
                                         highlightColor(0, 0, 0, defaultHighlightAlpha),
                                         highlightDrawPriority(
                                                 Graph2DDrawPrimitive::Priority_Highlight),
                                         insetTop(1.0),
                                         insetBottom(1.0),
                                         insetLeft(1.0),
                                         insetRight(1.0)
{
    titleFont.setBold(true);
    titleFont.setPointSizeF(titleFont.pointSizeF() * defaultTitleScale);
}

Graph2DParameters::Graph2DParameters(const Graph2DParameters &) = default;

Graph2DParameters &Graph2DParameters::operator=(const Graph2DParameters &) = default;

Graph2DParameters::Graph2DParameters(Graph2DParameters &&) = default;

Graph2DParameters &Graph2DParameters::operator=(Graph2DParameters &&) = default;

/**
 * Initialize the parameters from the given configuration.
 * 
 * @param value     the base value to configure from
 * @param defaults  the display defaults in use
 */
Graph2DParameters::Graph2DParameters(const Variant::Read &value, const DisplayDefaults &defaults)
        : components(0),
          titleText(),
          titleFont(),
          titleColor(0, 0, 0),
          legendEnable(true),
          legendPosition(Inside_TopLeft),
          legendBoxXInset(defaultLegendBoxInset),
          legendBoxYInset(defaultLegendBoxInset),
          legendBoxSize(defaultLegendBoxSize),
          legendBoxOutlineWidth(1.0),
          legendBoxOutlineStyle(Qt::SolidLine),
          legendBoxOutlineColor(0, 0, 0),
          legendBoxFillColor(255, 255, 255),
          legendDrawPriority(Graph2DDrawPrimitive::Priority_Legend),
          highlightColor(0, 0, 0, defaultHighlightAlpha),
          highlightDrawPriority(Graph2DDrawPrimitive::Priority_Highlight),
          insetTop(1.0),
          insetBottom(1.0),
          insetLeft(1.0),
          insetRight(1.0)
{
    Q_UNUSED(defaults);
    titleFont.setBold(true);
    titleFont.setPointSizeF(titleFont.pointSizeF() * defaultTitleScale);

    if (value["Title/Text"].exists()) {
        components |= Component_TitleText;
        titleText = value["Title/Text"].toDisplayString();
    }

    if (value["Title/Font"].exists()) {
        components |= Component_TitleFont;
        titleFont = Display::parseFont(value["Title/Font"], titleFont);
    }

    if (value["Title/Color"].exists()) {
        components |= Component_TitleColor;
        titleColor = Display::parseColor(value["Title/Color"], titleColor);
    }

    if (value["Legend/Enable"].exists()) {
        components |= Component_LegendEnable;
        legendEnable = value["Legend/Enable"].toBool();
    }

    if (value["Legend/Position"].exists()) {
        const auto &position = value["Legend/Position"].toString();
        if (Util::equal_insensitive(position, "inside", "topleft", "insidetopleft")) {
            components |= Component_LegendPosition;
            legendPosition = Inside_TopLeft;
        } else if (Util::equal_insensitive(position, "topright", "insidetopright")) {
            components |= Component_LegendPosition;
            legendPosition = Inside_TopRight;
        } else if (Util::equal_insensitive(position, "bottomleft", "insidebottomleft")) {
            components |= Component_LegendPosition;
            legendPosition = Inside_BottomLeft;
        } else if (Util::equal_insensitive(position, "bottomright", "insidebottomright")) {
            components |= Component_LegendPosition;
            legendPosition = Inside_BottomRight;
        } else if (Util::equal_insensitive(position, "left")) {
            components |= Component_LegendPosition;
            legendPosition = Outside_Left;
        } else if (Util::equal_insensitive(position, "right", "outside")) {
            components |= Component_LegendPosition;
            legendPosition = Outside_Right;
        }
    }

    if (value["Legend/Box/XInset"].exists()) {
        double v = value["Legend/Box/XInset"].toDouble();
        if (FP::defined(v) && v >= 0.0) {
            components |= Component_LegendBoxXInset;
            legendBoxXInset = v;
        }
    }

    if (value["Legend/Box/YInset"].exists()) {
        double v = value["Legend/Box/YInset"].toDouble();
        if (FP::defined(v) && v >= 0.0) {
            components |= Component_LegendBoxYInset;
            legendBoxYInset = v;
        }
    }

    if (FP::defined(value["Legend/Box/Size"].toDouble())) {
        components |= Component_LegendBoxSize;
        legendBoxSize = value["Legend/Box/Size"].toDouble();
    }

    if (value["Legend/Box/Outline/Width"].exists()) {
        double v = value["Legend/Outline/Width"].toDouble();
        if (FP::defined(v) && v >= 0.0) {
            components |= Component_LegendBoxOutlineWidth;
            legendBoxOutlineWidth = v;
        }
    }

    if (value["Legend/Box/Outline/Style"].exists()) {
        legendBoxOutlineStyle =
                Display::parsePenStyle(value["Legend/Box/Outline/Style"], legendBoxOutlineStyle);
        components |= Component_LegendBoxOutlineStyle;
    }

    if (value["Legend/Box/Outline/Color"].exists()) {
        components |= Component_LegendBoxOutlineColor;
        legendBoxOutlineColor =
                Display::parseColor(value["Legend/Box/Outline/Color"], legendBoxOutlineColor);
    }

    if (value["Legend/Box/Fill/Color"].exists()) {
        components |= Component_LegendBoxFillColor;
        legendBoxFillColor =
                Display::parseColor(value["Legend/Box/Fill/Color"], legendBoxFillColor);
    }

    if (INTEGER::defined(value["Legend/DrawPriority"].toInt64())) {
        components |= Component_LegendDrawPriority;
        legendDrawPriority = (int) value["Legend/DrawPriority"].toInt64();
    }

    if (value["Highlight/Color"].exists()) {
        components |= Component_HighlightColor;
        highlightColor = Display::parseColor(value["Highlight/Color"], highlightColor);
    }

    if (INTEGER::defined(value["Highlight/DrawPriority"].toInt64())) {
        components |= Component_HighlightDrawPriority;
        highlightDrawPriority = (int) value["Highlight/DrawPriority"].toInt64();
    }

    if (value["Inset/Top"].exists()) {
        double v = value["Inset/Top"].toDouble();
        if (FP::defined(v) && v >= 0.0) {
            components |= Component_InsetTop;
            insetTop = v;
        }
    }
    if (value["Inset/Bottom"].exists()) {
        double v = value["Inset/Bottom"].toDouble();
        if (FP::defined(v) && v >= 0.0) {
            components |= Component_InsetBottom;
            insetBottom = v;
        }
    }
    if (value["Inset/Left"].exists()) {
        double v = value["Inset/Left"].toDouble();
        if (FP::defined(v) && v >= 0.0) {
            components |= Component_InsetLeft;
            insetLeft = v;
        }
    }
    if (value["Inset/Right"].exists()) {
        double v = value["Inset/Right"].toDouble();
        if (FP::defined(v) && v >= 0.0) {
            components |= Component_InsetRight;
            insetRight = v;
        }
    }
}

void Graph2DParameters::save(Variant::Write &value) const
{
    if (components & Component_TitleText) {
        value["Title/Text"].setString(titleText);
    }

    if (components & Component_TitleFont) {
        value["Title/Font"].set(Display::formatFont(titleFont));
    }

    if (components & Component_TitleColor) {
        value["Title/Color"].set(Display::formatColor(titleColor));
    }

    if (components & Component_LegendEnable) {
        value["Legend/Enable"].setBool(legendEnable);
    }

    if (components & Component_LegendPosition) {
        switch (legendPosition) {
        case Inside_TopLeft:
            value["Legend/Position"].setString("TopLeft");
            break;
        case Inside_TopRight:
            value["Legend/Position"].setString("TopRight");
            break;
        case Inside_BottomLeft:
            value["Legend/Position"].setString("BottomLeft");
            break;
        case Inside_BottomRight:
            value["Legend/Position"].setString("BottomRight");
            break;
        case Outside_Left:
            value["Legend/Position"].setString("Left");
            break;
        case Outside_Right:
            value["Legend/Position"].setString("Right");
            break;
        }
    }

    if (components & Component_LegendBoxXInset) {
        value["Legend/Box/XInset"].setDouble(legendBoxXInset);
    }

    if (components & Component_LegendBoxYInset) {
        value["Legend/Box/YInset"].setDouble(legendBoxYInset);
    }

    if (components & Component_LegendBoxSize) {
        value["Legend/Box/Size"].setDouble(legendBoxSize);
    }

    if (components & Component_LegendBoxOutlineWidth) {
        value["Legend/Box/Outline/Width"].setDouble(legendBoxOutlineWidth);
    }

    if (components & Component_LegendBoxOutlineStyle) {
        value["Legend/Box/Outline/Style"].set(Display::formatPenStyle(legendBoxOutlineStyle));
    }

    if (components & Component_LegendBoxOutlineColor) {
        value["Legend/Box/Outline/Color"].set(Display::formatColor(legendBoxOutlineColor));
    }

    if (components & Component_LegendBoxFillColor) {
        value["Legend/Box/Fill/Color"].set(Display::formatColor(legendBoxFillColor));
    }

    if (components & Component_LegendDrawPriority) {
        value["Legend/DrawPriority"].setInt64(legendDrawPriority);
    }

    if (components & Component_HighlightColor) {
        value["Highlight/Color"].set(Display::formatColor(highlightColor));
    }

    if (components & Component_HighlightDrawPriority) {
        value["Highlight/DrawPriority"].setInt64(highlightDrawPriority);
    }

    if (components & Component_InsetTop) {
        value["Inset/Top"].setDouble(insetTop);
    }
    if (components & Component_InsetBottom) {
        value["Inset/Bottom"].setDouble(insetBottom);
    }
    if (components & Component_InsetLeft) {
        value["Inset/Left"].setDouble(insetLeft);
    }
    if (components & Component_InsetRight) {
        value["Inset/Right"].setDouble(insetRight);
    }
}

void Graph2DParameters::copy(const Graph2DParameters *from)
{ *this = *from; }

void Graph2DParameters::clear()
{ components = 0; }

std::unique_ptr<Graph2DParameters> Graph2DParameters::clone() const
{ return std::unique_ptr<Graph2DParameters>(new Graph2DParameters(*this)); }

void Graph2DParameters::overlay(const Graph2DParameters *over)
{
    if (over->components & Component_TitleText) {
        components |= Component_TitleText;
        titleText = over->titleText;
    }

    if (over->components & Component_TitleFont) {
        components |= Component_TitleFont;
        titleFont = over->titleFont;
    }

    if (over->components & Component_TitleColor) {
        components |= Component_TitleColor;
        titleColor = over->titleColor;
    }

    if (over->components & Component_LegendEnable) {
        components |= Component_LegendEnable;
        legendEnable = over->legendEnable;
    }

    if (over->components & Component_LegendPosition) {
        components |= Component_LegendPosition;
        legendPosition = over->legendPosition;
    }

    if (over->components & Component_LegendBoxXInset) {
        components |= Component_LegendBoxXInset;
        legendBoxXInset = over->legendBoxXInset;
    }

    if (over->components & Component_LegendBoxYInset) {
        components |= Component_LegendBoxYInset;
        legendBoxYInset = over->legendBoxYInset;
    }

    if (over->components & Component_LegendBoxSize) {
        components |= Component_LegendBoxSize;
        legendBoxSize = over->legendBoxSize;
    }

    if (over->components & Component_LegendBoxOutlineWidth) {
        components |= Component_LegendBoxOutlineWidth;
        legendBoxOutlineWidth = over->legendBoxOutlineWidth;
    }

    if (over->components & Component_LegendBoxOutlineStyle) {
        components |= Component_LegendBoxOutlineStyle;
        legendBoxOutlineStyle = over->legendBoxOutlineStyle;
    }

    if (over->components & Component_LegendBoxOutlineColor) {
        components |= Component_LegendBoxOutlineColor;
        legendBoxOutlineColor = over->legendBoxOutlineColor;
    }

    if (over->components & Component_LegendBoxFillColor) {
        components |= Component_LegendBoxFillColor;
        legendBoxFillColor = over->legendBoxFillColor;
    }

    if (over->components & Component_LegendDrawPriority) {
        components |= Component_LegendDrawPriority;
        legendDrawPriority = over->legendDrawPriority;
    }

    if (over->components & Component_HighlightColor) {
        components |= Component_HighlightColor;
        highlightColor = over->highlightColor;
    }

    if (over->components & Component_HighlightDrawPriority) {
        components |= Component_HighlightDrawPriority;
        highlightDrawPriority = over->highlightDrawPriority;
    }

    if (over->components & Component_InsetTop) {
        components |= Component_InsetTop;
        insetTop = over->insetTop;
    }

    if (over->components & Component_InsetBottom) {
        components |= Component_InsetBottom;
        insetBottom = over->insetBottom;
    }

    if (over->components & Component_InsetLeft) {
        components |= Component_InsetLeft;
        insetLeft = over->insetLeft;
    }

    if (over->components & Component_InsetRight) {
        components |= Component_InsetRight;
        insetRight = over->insetRight;
    }
}

void Graph2DParameters::initializeOverlay(const Graph2DParameters *over)
{
    Graph2DParameters::operator=(*over);
}

}
}
