/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <unordered_set>
#include <tuple>
#include <QApplication>
#include <QSettings>

#include "graphing/displayenable.hxx"
#include "datacore/sequencematch.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Graphing {

/** @file graphing/displayenable.hxx
 * Display enable management.
 */


DisplayEnable::EnableResult::EnableResult(DisplayEnable &enable) : enable(enable)
{ }

DisplayEnable::EnableResult::EnableResult(const EnableResult &) = default;

DisplayEnable::EnableResult::~EnableResult() = default;

bool DisplayEnable::EnableResult::evaluate(const std::unique_ptr<EnableResult> &other)
{
    auto check = enable.resultCache.find(other);
    if (check != enable.resultCache.end())
        return check->second;
    bool result = other->evaluate();
    enable.resultCache.emplace(other->clone(), result);
    return result;
}

class DisplayEnable::Enable_Constant : public EnableResult {
    bool value;
public:
    Enable_Constant(DisplayEnable &enable, bool value) : EnableResult(enable), value(value)
    { }

    virtual ~Enable_Constant() = default;

    bool evaluate() override
    { return value; }

    std::size_t hash() const override
    {
        std::size_t result = static_cast<std::size_t>(type());
        result = INTEGER::mix(result, value ? 1 : 0);
        return result;
    }

    bool equalTo(const EnableResult &other) override
    {
        if (other.type() != type())
            return false;
        return static_cast<const Enable_Constant &>(other).value == value;
    }

    EnableType type() const override
    { return EnableType::Constant; }

    std::unique_ptr<EnableResult> clone() const override
    { return std::unique_ptr<EnableResult>(new Enable_Constant(*this)); }

protected:
    Enable_Constant(const Enable_Constant &) = default;
};

class DisplayEnable::Enable_AtLeast : public EnableResult {
    SequenceMatch::Composite selection;
    std::size_t n;
    Variant::Root selectionConfig;

    std::size_t existingMatches() const
    {
        std::size_t n = 0;
        for (const auto &check : existingNames()) {
            if (selection.matches(check))
                n++;
        }
        return n;
    }

public:
    Enable_AtLeast(DisplayEnable &enable, const Variant::Read &selection, std::size_t n)
            : EnableResult(enable),
              selection(toSelection(selection)),
              n(n),
              selectionConfig(selection)
    { }

    virtual ~Enable_AtLeast() = default;

    bool evaluate() override
    {
        auto check = existingMatches();
        if (check >= n)
            return true;
        integrateSelection(selection);
        return existingMatches() >= n;
    }

    std::size_t hash() const override
    {
        std::size_t result = static_cast<std::size_t>(type());
        result = INTEGER::mix(result, n);
        result = INTEGER::mix(result, Variant::Read::ValueHash()(selectionConfig.read()));
        return result;
    }

    bool equalTo(const EnableResult &other) override
    {
        if (other.type() != type())
            return false;
        const auto &v = static_cast<const Enable_AtLeast &>(other);
        return v.n == n && v.selectionConfig.read() == selectionConfig.read();
    }

    EnableType type() const override
    { return EnableType::AtLeast; }

    std::unique_ptr<EnableResult> clone() const override
    { return std::unique_ptr<EnableResult>(new Enable_AtLeast(*this)); }

protected:
    Enable_AtLeast(const Enable_AtLeast &) = default;
};

class DisplayEnable::Enable_AtMost : public EnableResult {
    SequenceMatch::Composite selection;
    std::size_t n;
    Variant::Root selectionConfig;

    std::size_t existingMatches() const
    {
        std::size_t n = 0;
        for (const auto &check : existingNames()) {
            if (selection.matches(check))
                n++;
        }
        return n;
    }

public:
    Enable_AtMost(DisplayEnable &enable, const Variant::Read &selection, std::size_t n)
            : EnableResult(enable),
              selection(toSelection(selection)),
              n(n),
              selectionConfig(selection)
    { }

    virtual ~Enable_AtMost() = default;

    bool evaluate() override
    {
        auto check = existingMatches();
        if (check > n)
            return false;
        integrateSelection(selection);
        return existingMatches() <= n;
    }

    std::size_t hash() const override
    {
        std::size_t result = static_cast<std::size_t>(type());
        result = INTEGER::mix(result, n);
        result = INTEGER::mix(result, Variant::Read::ValueHash()(selectionConfig.read()));
        return result;
    }

    bool equalTo(const EnableResult &other) override
    {
        if (other.type() != type())
            return false;
        const auto &v = static_cast<const Enable_AtMost &>(other);
        return v.n == n && v.selectionConfig.read() == selectionConfig.read();
    }

    EnableType type() const override
    { return EnableType::AtMost; }

    std::unique_ptr<EnableResult> clone() const override
    { return std::unique_ptr<EnableResult>(new Enable_AtMost(*this)); }

protected:
    Enable_AtMost(const Enable_AtMost &) = default;
};

class DisplayEnable::Enable_AtLeastInstrument : public EnableResult {
    SequenceMatch::Composite selection;
    std::size_t n;
    Variant::Root selectionConfig;

    std::size_t existingMatches() const
    {
        SequenceName::Set seen;
        for (const auto &check : existingNames()) {
            if (selection.matches(check)) {
                SequenceName add = check;
                add.setVariable(Util::suffix(add.getVariable(), '_'));
                seen.insert(std::move(add));
            }
        }
        return seen.size();
    }

public:
    Enable_AtLeastInstrument(DisplayEnable &enable, const Variant::Read &selection, std::size_t n)
            : EnableResult(enable),
              selection(toSelection(selection)),
              n(n),
              selectionConfig(selection)
    { }

    virtual ~Enable_AtLeastInstrument() = default;

    bool evaluate() override
    {
        auto check = existingMatches();
        if (check >= n)
            return true;
        integrateSelection(selection);
        return existingMatches() >= n;
    }

    std::size_t hash() const override
    {
        std::size_t result = static_cast<std::size_t>(type());
        result = INTEGER::mix(result, n);
        result = INTEGER::mix(result, Variant::Read::ValueHash()(selectionConfig.read()));
        return result;
    }

    bool equalTo(const EnableResult &other) override
    {
        if (other.type() != type())
            return false;
        const auto &v = static_cast<const Enable_AtLeastInstrument &>(other);
        return v.n == n && v.selectionConfig.read() == selectionConfig.read();
    }

    EnableType type() const override
    { return EnableType::AtLeastInstrument; }

    std::unique_ptr<EnableResult> clone() const override
    { return std::unique_ptr<EnableResult>(new Enable_AtLeastInstrument(*this)); }

protected:
    Enable_AtLeastInstrument(const Enable_AtLeastInstrument &) = default;
};

class DisplayEnable::Enable_AtMostInstrument : public EnableResult {
    SequenceMatch::Composite selection;
    std::size_t n;
    Variant::Root selectionConfig;

    std::size_t existingMatches() const
    {
        SequenceName::Set seen;
        for (const auto &check : existingNames()) {
            if (selection.matches(check)) {
                SequenceName add = check;
                add.setVariable(Util::suffix(add.getVariable(), '_'));
                seen.insert(std::move(add));
            }
        }
        return seen.size();
    }

public:
    Enable_AtMostInstrument(DisplayEnable &enable, const Variant::Read &selection, std::size_t n)
            : EnableResult(enable),
              selection(toSelection(selection)),
              n(n),
              selectionConfig(selection)
    { }

    virtual ~Enable_AtMostInstrument() = default;

    bool evaluate() override
    {
        auto check = existingMatches();
        if (check > n)
            return false;
        integrateSelection(selection);
        return existingMatches() <= n;
    }

    std::size_t hash() const override
    {
        std::size_t result = static_cast<std::size_t>(type());
        result = INTEGER::mix(result, n);
        result = INTEGER::mix(result, Variant::Read::ValueHash()(selectionConfig.read()));
        return result;
    }

    bool equalTo(const EnableResult &other) override
    {
        if (other.type() != type())
            return false;
        const auto &v = static_cast<const Enable_AtMostInstrument &>(other);
        return v.n == n && v.selectionConfig.read() == selectionConfig.read();
    }

    EnableType type() const override
    { return EnableType::AtMostInstrument; }

    std::unique_ptr<EnableResult> clone() const override
    { return std::unique_ptr<EnableResult>(new Enable_AtMostInstrument(*this)); }

protected:
    Enable_AtMostInstrument(const Enable_AtMostInstrument &) = default;
};

class DisplayEnable::Enable_And : public EnableResult {
    std::vector<std::unique_ptr<EnableResult>> components;

public:
    Enable_And(DisplayEnable &enable, std::vector<std::unique_ptr<EnableResult>> &&components)
            : EnableResult(enable), components(std::move(components))
    { }

    virtual ~Enable_And() = default;

    bool evaluate() override
    {
        for (const auto &c : components) {
            if (!EnableResult::evaluate(c))
                return false;
        }
        return true;
    }

    std::size_t hash() const override
    {
        std::size_t result = static_cast<std::size_t>(type());
        result = INTEGER::mix(result, components.size());
        for (const auto &add : components) {
            result = INTEGER::mix(result, add->hash());
        }
        return result;
    }

    bool equalTo(const EnableResult &other) override
    {
        if (other.type() != type())
            return false;
        const auto &v = static_cast<const Enable_And &>(other);
        if (v.components.size() != components.size())
            return false;
        return std::equal(components.begin(), components.end(), v.components.begin(),
                          [](const std::unique_ptr<EnableResult> &ca,
                             const std::unique_ptr<EnableResult> &cb) {
                              return ca->equalTo(*cb);
                          });
    }

    EnableType type() const override
    { return EnableType::And; }

    std::unique_ptr<EnableResult> clone() const override
    { return std::unique_ptr<EnableResult>(new Enable_And(*this)); }

protected:
    Enable_And(const Enable_And &other) : EnableResult(other)
    {
        for (const auto &c : other.components) {
            components.emplace_back(c->clone());
        }
    }
};

class DisplayEnable::Enable_Or : public EnableResult {
    std::vector<std::unique_ptr<EnableResult>> components;

public:
    Enable_Or(DisplayEnable &enable, std::vector<std::unique_ptr<EnableResult>> &&components)
            : EnableResult(enable), components(std::move(components))
    { }

    virtual ~Enable_Or() = default;

    bool evaluate() override
    {
        for (const auto &c : components) {
            if (EnableResult::evaluate(c))
                return true;
        }
        return false;
    }

    std::size_t hash() const override
    {
        std::size_t result = static_cast<std::size_t>(type());
        result = INTEGER::mix(result, components.size());
        for (const auto &add : components) {
            result = INTEGER::mix(result, add->hash());
        }
        return result;
    }

    bool equalTo(const EnableResult &other) override
    {
        if (other.type() != type())
            return false;
        const auto &v = static_cast<const Enable_Or &>(other);
        if (v.components.size() != components.size())
            return false;
        return std::equal(components.begin(), components.end(), v.components.begin(),
                          [](const std::unique_ptr<EnableResult> &ca,
                             const std::unique_ptr<EnableResult> &cb) {
                              return ca->equalTo(*cb);
                          });
    }

    EnableType type() const override
    { return EnableType::Or; }

    std::unique_ptr<EnableResult> clone() const override
    { return std::unique_ptr<EnableResult>(new Enable_Or(*this)); }

protected:
    Enable_Or(const Enable_Or &other) : EnableResult(other)
    {
        for (const auto &c : other.components) {
            components.emplace_back(c->clone());
        }
    }
};

class DisplayEnable::Enable_Not : public EnableResult {
    std::unique_ptr<EnableResult> component;

public:
    Enable_Not(DisplayEnable &enable, std::unique_ptr<EnableResult> &&component) : EnableResult(
            enable), component(std::move(component))
    { }

    virtual ~Enable_Not() = default;

    bool evaluate() override
    {
        return !EnableResult::evaluate(component);
    }

    std::size_t hash() const override
    {
        std::size_t result = static_cast<std::size_t>(type());
        result = INTEGER::mix(result, component->hash());
        return result;
    }

    bool equalTo(const EnableResult &other) override
    {
        if (other.type() != type())
            return false;
        return component->equalTo(*(static_cast<const Enable_Not &>(other).component));
    }

    EnableType type() const override
    { return EnableType::Not; }

    std::unique_ptr<EnableResult> clone() const override
    { return std::unique_ptr<EnableResult>(new Enable_Not(*this)); }

protected:
    Enable_Not(const Enable_Not &other) : EnableResult(other), component(other.component->clone())
    { }
};


DisplayEnable::DisplayEnable(double start,
                             double end,
                             const SequenceName::Component &station,
                             const SequenceName::Component &archive,
                             const SequenceName::Component &variable,
                             Archive::Access *access) : reader(access),
                                                        start(start),
                                                        end(end),
                                                        station(station),
                                                        archive(archive),
                                                        variable(variable),
                                                        knownToExist(),
                                                        resultCache()
{ }

DisplayEnable::~DisplayEnable() = default;

static Archive::Selection::Match listIfNotEmpty(const SequenceName::Component &input)
{
    if (input.empty())
        return {};
    return {input};
}

SequenceName::Set DisplayEnable::evaluateSelection(const SequenceMatch::Composite &selection)
{
    auto selections =
            selection.toArchiveSelections(listIfNotEmpty(station), listIfNotEmpty(archive),
                                          listIfNotEmpty(variable));
    if (selections.empty())
        return SequenceName::Set();

    for (auto &sel : selections) {
        sel.start = start;
        sel.end = end;
    }

    if (!reader) {
        localArchive.reset(new Archive::Access);
        reader = localArchive.get();
        localLock.reset(new Archive::Access::ReadLock(*reader));
    }

    SequenceName::Set result;
    for (auto add : reader->availableNames(selections)) {
        add.clearFlavors();
        add.clearMeta();
        if (add.isDefaultStation() && !station.empty())
            add.setStation(station);
        if (!selection.matches(add))
            continue;
        result.emplace(std::move(add));
    }
    return result;
}

void DisplayEnable::integrateSelection(const SequenceMatch::Composite &selection)
{ Util::merge(evaluateSelection(selection), knownToExist); }

SequenceMatch::Composite DisplayEnable::toSelection(const Data::Variant::Read &present) const
{
    return SequenceMatch::Composite(present, {QString::fromStdString(station)},
                                    {QString::fromStdString(archive)},
                                    {QString::fromStdString(variable)});
}

std::unique_ptr<
        DisplayEnable::EnableResult> DisplayEnable::constructEnable(const Variant::Read &configuration)
{
    switch (configuration.getType()) {
    case Variant::Type::Boolean:
        return std::unique_ptr<EnableResult>(new Enable_Constant(*this, configuration.toBool()));

    case Variant::Type::Hash: {
        const auto &type = configuration["Type"].toString();
        if (Util::equal_insensitive(type, "always")) {
            return std::unique_ptr<EnableResult>(new Enable_Constant(*this, true));
        } else if (Util::equal_insensitive(type, "never")) {
            return std::unique_ptr<EnableResult>(new Enable_Constant(*this, false));
        } else if (Util::equal_insensitive(type, "atleast")) {
            auto n = configuration["Count"].toInteger();
            if (!INTEGER::defined(n) || n < 0)
                return std::unique_ptr<EnableResult>(new Enable_Constant(*this, true));
            auto selection = configuration["Selection"];
            if (!selection.exists())
                selection = configuration;
            return std::unique_ptr<EnableResult>(
                    new Enable_AtLeast(*this, selection, static_cast<std::size_t>(n)));
        } else if (Util::equal_insensitive(type, "atmost")) {
            auto n = configuration["Count"].toInteger();
            if (!INTEGER::defined(n) || n < 0)
                return std::unique_ptr<EnableResult>(new Enable_Constant(*this, true));
            auto selection = configuration["Selection"];
            if (!selection.exists())
                selection = configuration;
            return std::unique_ptr<EnableResult>(
                    new Enable_AtMost(*this, selection, static_cast<std::size_t>(n)));
        } else if (Util::equal_insensitive(type, "atleastinstrument")) {
            auto n = configuration["Count"].toInteger();
            if (!INTEGER::defined(n) || n < 0)
                return std::unique_ptr<EnableResult>(new Enable_Constant(*this, true));
            auto selection = configuration["Selection"];
            if (!selection.exists())
                selection = configuration;
            return std::unique_ptr<EnableResult>(
                    new Enable_AtLeastInstrument(*this, selection, static_cast<std::size_t>(n)));
        } else if (Util::equal_insensitive(type, "atmostinstrument")) {
            auto n = configuration["Count"].toInteger();
            if (!INTEGER::defined(n) || n < 0)
                return std::unique_ptr<EnableResult>(new Enable_Constant(*this, true));
            auto selection = configuration["Selection"];
            if (!selection.exists())
                selection = configuration;
            return std::unique_ptr<EnableResult>(
                    new Enable_AtMostInstrument(*this, selection, static_cast<std::size_t>(n)));
        } else if (Util::equal_insensitive(type, "and")) {
            std::vector<std::unique_ptr<EnableResult>> components;
            for (auto add : configuration["Components"].toChildren()) {
                components.emplace_back(constructEnable(add));
            }
            return std::unique_ptr<EnableResult>(new Enable_And(*this, std::move(components)));
        } else if (Util::equal_insensitive(type, "or")) {
            std::vector<std::unique_ptr<EnableResult>> components;
            for (auto add : configuration["Components"].toChildren()) {
                components.emplace_back(constructEnable(add));
            }
            return std::unique_ptr<EnableResult>(new Enable_Or(*this, std::move(components)));
        } else if (Util::equal_insensitive(type, "not", "invert")) {
            return std::unique_ptr<EnableResult>(
                    new Enable_Not(*this, constructEnable(configuration["Component"])));
        }

        std::vector<std::unique_ptr<EnableResult>> result;

        if (configuration.hash("Any").exists()) {
            result.emplace_back(new Enable_AtLeast(*this, configuration["Any"], 1));
        }
        switch (configuration["All"].getType()) {
        case Variant::Type::Empty:
            break;
        case Variant::Type::Array: {
            std::vector<std::unique_ptr<EnableResult>> components;
            for (auto add : configuration["All"].toChildren()) {
                components.emplace_back(new Enable_AtLeast(*this, add, 1));
            }
            if (!components.empty())
                result.emplace_back(new Enable_And(*this, std::move(components)));
            break;
        }
        default:
            result.emplace_back(new Enable_AtLeast(*this, configuration["All"], 1));
            break;
        }
        if (configuration["None"].exists()) {
            result.emplace_back(new Enable_AtMost(*this, configuration["None"], 0));
        }

        switch (result.size()) {
        case 0:
            return std::unique_ptr<EnableResult>(new Enable_Constant(*this, true));
        case 1:
        default:
            return std::unique_ptr<EnableResult>(new Enable_And(*this, std::move(result)));
        }
        Q_ASSERT(false);
        break;
    }

    default:
        break;
    }

    return std::unique_ptr<EnableResult>(new Enable_Constant(*this, true));
}

bool DisplayEnable::enabled(const Variant::Read &value)
{
    auto e = constructEnable(value);
    auto check = resultCache.find(e);
    if (check != resultCache.end())
        return check->second;
    bool result = e->evaluate();
    resultCache.emplace(std::move(e), result);
    return result;
}

}
}
