/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGTRACESET_H
#define CPD3GRAPHINGTRACESET_H

#include "core/first.hxx"

#include <array>
#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QVector>
#include <QDebug>
#include <QDebugStateSaver>
#include <datacore/sequencematch.hxx>

#include "graphing/graphing.hxx"
#include "graphing/tracedispatch.hxx"
#include "core/range.hxx"
#include "core/number.hxx"
#include "core/qtcompat.hxx"
#include "algorithms/transformer.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/stream.hxx"
#include "datacore/segment.hxx"
#include "datacore/sequencematch.hxx"
#include "datacore/variant/composite.hxx"

namespace CPD3 {
namespace Graphing {

/**
 * A single point on a trace.
 * 
 * @param N the number of dimensions
 */
template<std::size_t N>
struct CPD3GRAPHING_EXPORT TracePoint {
    /**
     * The start time of the point.
     */
    double start;
    /**
     * The end time of the point.
     */
    double end;
    /**
     * The dimension values.
     */
    std::array<double, N> d;

    bool operator==(const TracePoint<N> &other) const
    {
        if (!FP::equal(start, other.start))
            return false;
        if (!FP::equal(end, other.end))
            return false;
        for (std::size_t i = 0; i < N; i++) {
            if (!FP::equal(d[i], other.d[i]))
                return false;
        }
        return true;
    }

    bool operator!=(const TracePoint<N> &other) const
    {
        return !(*this == other);
    }
};

template<std::size_t N>
QDebug operator<<(QDebug stream, const TracePoint<N> &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "TracePoint<" << N << ">(" << Logging::range(v.start, v.end);
    for (std::size_t i = 0; i < N; i++) {
        stream << ',' << v.d[i];
    }
    stream << ")";
    return stream;
}

struct CPD3GRAPHING_EXPORT TraceDimensionMetadata {
    QString format;
    QString description;
    QString origin;
    double wavelength;

    inline TraceDimensionMetadata() : format(), description(), origin(), wavelength(FP::undefined())
    { }

    inline void clear()
    {
        format.clear();
        description.clear();
        origin.clear();
        wavelength = FP::undefined();
    }
};

/**
 * The limits of data that a trace occupies.
 * 
 * @param N the total number of dimensions
 */
template<std::size_t N>
struct TraceLimits {
    /**
     * The minimum values.
     */
    double min[N];
    /**
     * The maximum values.
     */
    double max[N];

    void reset()
    {
        for (std::size_t i = 0; i < N; i++) {
            min[i] = FP::undefined();
            max[i] = FP::undefined();
        }
    }

    void add(const TracePoint<N> &p)
    {
        for (std::size_t i = 0; i < N; i++) {
            if (!FP::defined(min[i])) {
                min[i] = max[i] = p.d[i];
            } else if (FP::defined(p.d[i])) {
                if (min[i] > p.d[i]) min[i] = p.d[i];
                if (max[i] < p.d[i]) max[i] = p.d[i];
            }
        }
    }

    bool operator==(const TraceLimits<N> &other) const
    {
        for (std::size_t i = 0; i < N; i++) {
            if (!FP::equal(min[i], other.min[i]))
                return false;
            if (!FP::equal(max[i], other.max[i]))
                return false;
        }
        return true;
    }

    bool operator!=(const TraceLimits<N> &other) const
    {
        return !(*this == other);
    }
};

template<std::size_t N>
QDebug operator<<(QDebug stream, const TraceLimits<N> &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "TraceLimits<" << N << ">(";
    for (std::size_t i = 0; i < N; i++) {
        if (i != 0) stream << ',';
        stream << '[' << v.min[i] << ',' << v.max[i] << ']';
    }
    stream << ')';
    return stream;
}

/**
 * The base class for all trace parameters.
 */
class CPD3GRAPHING_EXPORT TraceParameters {
public:
    /**
     * The way values are passed to the transformer.
     */
    enum TransformerInputMode {
        /** Only pass values to the transformer. */
        Transformer_ValuesOnly,
        /** Pass the start time then the values to the transformer. */
        Transformer_StartThenValues,
        /** Pass the end time then the values to the transformer. */
        Transformer_EndThenValues,
        /** Pass the middle time then the values to the transformer. */
        Transformer_MiddleThenValues,
        /** Pass the start time, end time, then the values to the transformer. */
        Transformer_StartEndValues,
    };
private:
    enum {
        Component_Transformer = 0x0001, Component_TransformerInputMode = 0x0002,
    };
    int components;
    Data::Variant::Root transformer;
    TransformerInputMode transformerInputMode;
public:
    TraceParameters();

    virtual ~TraceParameters();

    TraceParameters(const TraceParameters &);

    TraceParameters &operator=(const TraceParameters &);

    TraceParameters(TraceParameters &&);

    TraceParameters &operator=(TraceParameters &&);

    TraceParameters(const Data::Variant::Read &value);

    virtual void save(Data::Variant::Write &value) const;

    virtual std::unique_ptr<TraceParameters> clone() const;

    virtual void overlay(const TraceParameters *over);

    virtual void copy(const TraceParameters *from);

    virtual void clear();

    /**
     * Test if the parameters specify a value transformer.
     * @return true if the parameters specify a transformer
     */
    inline bool hasTransformer() const
    { return components & Component_Transformer; }

    /**
     * Get the transformer.
     * @return the transformer
     */
    inline const Data::Variant::Root &getTransformer() const
    { return transformer; }

    /**
     * Test if the parameters specify a transformer input mode.
     * @return true if the parameters specify a transformer input mode
     */
    inline bool hasTransformerInputMode() const
    { return components & Component_TransformerInputMode; }

    /**
     * Get the transformer input mode
     * @return the transformer input mode
     */
    inline TransformerInputMode getTransformerInputMode() const
    { return transformerInputMode; }
};

/**
 * The base class for value handlers.  This should not be instantiated directly,
 * it exists only to provide a fixed base for Qt.
 */
class CPD3GRAPHING_EXPORT TraceValueHandlerBase : public QObject {
Q_OBJECT

public:
    TraceValueHandlerBase();

signals:

    /**
     * Emitted when the state of the deferred processing has changed
     * and the handler needs to be processed again.
     */
    void deferredUpdate();
};

/**
 * A handler for a single final dispatch trace.  This converts dimension
 * outputs of the dispatch into "real" points, possibly passing them through
 * a transform function.  It also tracks basic metadata for the trace.
 * 
 * @param N         the total number of dimensions
 * @param PointType the type of the points to store
 */
template<std::size_t N, typename PointType>
class TraceValueHandlerComplex : public TraceValueHandlerBase {
    std::unique_ptr<TraceParameters> parameters;
    std::unique_ptr<TraceParameters> originalParameters;
    std::unique_ptr<TraceParameters> overridenParameters;

    double start;
    double end;
    bool dataRangeInconsistent;

    std::vector<TraceDispatch::OutputValue> untransformedValues;
    bool needTransformRerun;
    Algorithms::DeferredTransformerWrapper *transform;

    std::vector<PointType> points;
    std::vector<PointType> rangeTrimmedPoints;
    TraceLimits<N> limits;

    std::unordered_set<QString> units[N];
    std::unordered_set<QString> groupUnits[N];
    TraceDimensionMetadata metadata[N];

    bool handlePointsUpdate(typename std::vector<PointType>::const_iterator begin,
                            typename std::vector<PointType>::const_iterator end)
    {
        if (begin == end)
            return false;
        bool anyPassed = false;
        if (dataRangeInconsistent) {
            rangeTrimmedPoints.reserve(rangeTrimmedPoints.size() + (end - begin));
            for (; begin != end; ++begin) {
                if (!Range::intersects(begin->start, begin->end, this->start, this->end))
                    continue;
                rangeTrimmedPoints.emplace_back(*begin);
                limits.add(*begin);
                anyPassed = true;
            }
            return anyPassed;
        }
        for (; begin != end; ++begin) {
            if (Range::intersects(begin->start, begin->end, this->start, this->end)) {
                limits.add(*begin);
                anyPassed = true;
                continue;
            }

            dataRangeInconsistent = true;
            rangeTrimmedPoints = points;
            rangeTrimmedPoints.resize(begin - points.cbegin());
            for (++begin; begin != end; ++begin) {
                if (!Range::intersects(begin->start, begin->end, this->start, this->end)) {
                    continue;
                }
                rangeTrimmedPoints.emplace_back(*begin);
                limits.add(*begin);
                anyPassed = true;
            }
            break;
        }
        return anyPassed;
    }

protected:
    template<std::size_t M>
    class TransformValue : public Algorithms::TransformerPointStatic<M> {
        TraceDispatch::OutputValue value;
    public:
        TransformValue(const TraceDispatch::OutputValue &inputValue)
                : Algorithms::TransformerPointStatic<M>(), value(inputValue)
        { }

        TransformValue(const TraceDispatch::OutputValue &inputValue, double v1)
                : Algorithms::TransformerPointStatic<M>(v1), value(inputValue)
        { }

        TransformValue(const TraceDispatch::OutputValue &inputValue, double v1, double v2)
                : Algorithms::TransformerPointStatic<M>(v1, v2), value(inputValue)
        { }

        TransformValue(const TraceDispatch::OutputValue &inputValue,
                       double v1,
                       double v2,
                       double v3) : Algorithms::TransformerPointStatic<M>(v1, v2, v3),
                                    value(inputValue)
        { }

        TransformValue(const TraceDispatch::OutputValue &inputValue, const QVector<double> &values)
                : Algorithms::TransformerPointStatic<M>(values), value(inputValue)
        { }

        TransformValue(const TraceDispatch::OutputValue &inputValue,
                       const Algorithms::TransformerPointStatic<M> &other)
                : Algorithms::TransformerPointStatic<M>(other), value(inputValue)
        { }

        /**
         * Get the input value.
         * @return the input value
         */
        inline const TraceDispatch::OutputValue &getValue() const
        { return value; }

        /**
         * Get the input value.
         * @return the input value
         */
        inline TraceDispatch::OutputValue &getValue()
        { return value; }


        /**
         * The simple implementation of a transformer allocator.
         */
        class Allocator : public Algorithms::TransformerAllocator {
        public:
            Allocator()
            { }

            virtual ~Allocator()
            { }

            virtual Algorithms::TransformerPoint *create(const Algorithms::Transformer *tr)
            {
                Q_UNUSED(tr);
                return new TransformValue<M>(TraceDispatch::OutputValue());
            }

            virtual Algorithms::TransformerPoint *copy(const Algorithms::TransformerPoint *origin)
            {
                const TransformValue<M> *other = static_cast<const TransformValue<M> *>(origin);
                return new TransformValue(other->value, *other);
            }
        };

        static Allocator allocator;
    };

public:
    TraceValueHandlerComplex(std::unique_ptr<TraceParameters> &&params) : parameters(
            std::move(params)),
                                                                          start(FP::undefined()),
                                                                          end(FP::undefined()),
                                                                          dataRangeInconsistent(
                                                                                  true),
                                                                          needTransformRerun(false),
                                                                          transform(NULL)
    { }

    virtual ~TraceValueHandlerComplex()
    {
        if (transform) {
            transform->disconnect(this);
            transform->deleteWhenFinished();
        }
    }

    /**
     * Get the trace handler parameters.
     * 
     * @return the handler parameters
     */
    inline TraceParameters *getParameters() const
    { return parameters.get(); }

    using Cloned = std::unique_ptr<TraceValueHandlerComplex<N, PointType>>;

    /**
     * Create a polymorphic copy of the handler.
     * 
     * @return a copy
     */
    virtual Cloned clone() const
    { return Cloned(new TraceValueHandlerComplex<N, PointType>(*this, parameters->clone())); }

    /**
     * Get the trace points.
     * 
     * @return the trace points
     */
    inline const std::vector<PointType> &getPoints() const
    {
        if (!dataRangeInconsistent)
            return points;
        return rangeTrimmedPoints;
    }

    /**
     * Get the trace limits.
     * 
     * @return the trace limits.
     */
    inline const TraceLimits<N> &getLimits() const
    { return limits; }

    /**
     * Reset all data, clearing everything existing out.
     */
    virtual void clear()
    {
        untransformedValues = std::vector<TraceDispatch::OutputValue>();
        needTransformRerun = false;
        if (transform) {
            transform->deleteWhenFinished();
            transform = nullptr;
        }
        points = std::vector<PointType>();
        rangeTrimmedPoints = std::vector<PointType>();
        dataRangeInconsistent = false;
        limits.reset();
        for (std::size_t i = 0; i < N; i++) {
            units[i].clear();
            groupUnits[i].clear();
            metadata[i].clear();
        }
    }

    /**
     * Process data, adding anything needed.  If a time range is specified
     * then the incoming data is filtered to that range.
     * 
     * @param incoming      the new incoming values to add
     * @param deferrable    if set then processing may be completed later if it takes a long time
     * @return              true if the underlying data was updated
     */
    virtual bool process(const std::vector<TraceDispatch::OutputValue> &incoming,
                         bool deferrable = false)
    {
        bool changed = false;

        if (parameters->hasTransformer()) {
            if (!incoming.empty()) {
                Util::append(incoming, untransformedValues);
                needTransformRerun = true;
            }
            if (deferrable) {
                bool ready = false;
                QVector<Algorithms::TransformerPoint *> transformedPoints;
                if (transform) {
                    transformedPoints = transform->takePoints(&ready);
                    if (ready) {
                        delete transform;
                        transform = nullptr;
                    }
                }

                if (needTransformRerun && !transform) {
                    needTransformRerun = false;

                    Algorithms::TransformerParameters transformParameters;
                    QVector<Algorithms::TransformerPoint *> deferrPoints
                            (prepareTransformer(untransformedValues, transformParameters));
                    transform = new Algorithms::DeferredTransformerWrapper(deferrPoints,
                                                                           parameters->getTransformer(),
                                                                           transformParameters,
                                                                           getTransformerAllocator());
                    QObject::connect(transform,
                                     &Algorithms::DeferredTransformerWrapper::pointsReady, this,
                                     &TraceValueHandlerBase::deferredUpdate);
                    bool testReady = false;
                    deferrPoints = transform->takePoints(&testReady);
                    if (testReady) {
                        for (auto it : transformedPoints) {
                            delete it;
                        }
                        transformedPoints = deferrPoints;
                        ready = true;

                        delete transform;
                        transform = nullptr;
                    }
                }

                if (ready) {
                    points = convertTransformerResult(transformedPoints);
                    dataRangeInconsistent = false;
                    limits.reset();
                    rangeTrimmedPoints.clear();
                    handlePointsUpdate(points.cbegin(), points.cend());
                    changed = true;
                } else if (!transformedPoints.isEmpty()) {
                    Q_ASSERT(false);
                    for (auto it : transformedPoints) {
                        delete it;
                    }
                }
            } else if (needTransformRerun) {
                if (transform) {
                    delete transform;
                    transform = nullptr;
                }

                Algorithms::TransformerParameters transformParameters;
                QVector<Algorithms::TransformerPoint *> transformPoints
                        (prepareTransformer(untransformedValues, transformParameters));
                Algorithms::Transformer *tr =
                        Algorithms::Transformer::fromConfiguration(parameters->getTransformer(),
                                                                   transformParameters);
                tr->apply(transformPoints, getTransformerAllocator());
                delete tr;

                points = convertTransformerResult(transformPoints);
                dataRangeInconsistent = false;
                limits.reset();
                rangeTrimmedPoints.clear();
                handlePointsUpdate(points.cbegin(), points.cend());
                changed = true;
            } else if (transform) {
                while (!transform->waitForReady()) { }
                QVector<Algorithms::TransformerPoint *> transformedPoints(transform->takePoints());
                delete transform;
                transform = NULL;

                points = convertTransformerResult(transformedPoints);
                dataRangeInconsistent = false;
                limits.reset();
                rangeTrimmedPoints.clear();
                handlePointsUpdate(points.cbegin(), points.cend());
                changed = true;
            }
        } else {
            auto n = points.size();
            for (const auto &it : incoming) {
                convertValue(it, points);
            }
            if (n != points.size()) {
                if (handlePointsUpdate(points.cbegin() + n, points.cend()))
                    changed = true;
            }
        }

        return changed;
    }

    /**
     * Trim everything except the final result value.
     */
    virtual void trimToLast()
    {
        if (points.size() < 2) return;
        dataRangeInconsistent = false;
        points.erase(points.begin(), points.end() - 1);
        for (std::size_t i = 0; i < N; i++) {
            limits.min[i] = points.front().d[i];
            limits.max[i] = points.front().d[i];
        }
        rangeTrimmedPoints.clear();
        rangeTrimmedPoints.shrink_to_fit();
    }

    /**
     * Trim data to the given range.
     * 
     * @param start the start of the range to include
     * @param end   the end of the range to include
     */
    virtual void trimData(double start, double end)
    {
        bool dataRemoved = false;
        for (auto it = points.begin(); it != points.end();) {
            if (Range::intersects(it->start, it->end, start, end)) {
                ++it;
                continue;
            }
            dataRemoved = true;
            it = points.erase(it);
        }

        for (auto it = untransformedValues.begin(); it != untransformedValues.end();) {
            if (Range::intersects(it->getStart(), it->getEnd(), start, end)) {
                ++it;
                continue;
            }
            it = untransformedValues.erase(it);
            needTransformRerun = true;
        }

        if (dataRemoved) {
            dataRangeInconsistent = false;
            limits.reset();
            rangeTrimmedPoints.clear();
            handlePointsUpdate(points.cbegin(), points.cend());
            rangeTrimmedPoints.shrink_to_fit();
        }
    }

    /**
     * Get the current visible start.
     * 
     * @return the start time
     */
    inline double getStart() const
    { return start; }

    /**
     * Get the current visible end.
     * 
     * @return the end time
     */
    inline double getEnd() const
    { return end; }

    /**
     * Set the visible time range of data.
     * 
     * @param start the start time
     * @param end   the end time
     */
    virtual void setVisibleTimeRange(double start, double end)
    {
        if (!needTransformRerun) {
            for (const auto &it : untransformedValues) {
                if (!Range::intersects(it.getStart(), it.getEnd(), start, end)) {
                    needTransformRerun = true;
                    break;
                }
                if (!Range::intersects(it.getStart(), it.getEnd(), this->start, this->end)) {
                    needTransformRerun = true;
                    break;
                }
            }
        }

        this->start = start;
        this->end = end;

        dataRangeInconsistent = false;
        limits.reset();
        rangeTrimmedPoints.clear();
        handlePointsUpdate(points.cbegin(), points.cend());
    }

    /**
     * Get the set of units in effect for a given dimension.
     * 
     * @param dimension the dimension index
     * @return          the units in effect
     */
    inline const std::unordered_set<QString> &getUnits(std::size_t dimension) const
    {
        Q_ASSERT(dimension >= 0 && dimension < N);
        return units[dimension];
    }

    /**
     * Get the set of group units in effect for a given dimension.
     * 
     * @param dimension the dimension index
     * @return          the units in effect
     */
    inline const std::unordered_set<QString> &getGroupUnits(std::size_t dimension) const
    {
        Q_ASSERT(dimension >= 0 && dimension < N);
        return groupUnits[dimension];
    }

    /**
     * Get the number format for a given dimension.
     * 
     * @param dimension the dimension index
     * @return          the number format
     */
    QString getFormat(std::size_t dimension) const
    {
        Q_ASSERT(dimension >= 0 && dimension < N);
        return metadata[dimension].format;
    }

    /**
     * Get the wavelength for a given dimension.
     * 
     * @param dimension the dimension index
     * @return          the wavelength
     */
    double getWavelength(std::size_t dimension) const
    {
        Q_ASSERT(dimension >= 0 && dimension < N);
        return metadata[dimension].wavelength;
    }

    /**
     * Get the default legend text for the handler.  This incorporates the given
     * units (usually from dispatch) and the metadata from this specific
     * handler.
     * 
     * @param inputUnits        the units that went into this handler
     * @param showStation       if set then show the station
     * @param showArchive       if set then show the archive
     * @return                  a legend text
     */
    QString getLegendText(const std::vector<Data::SequenceName::Set> &inputUnits = {},
                          bool showStation = false,
                          bool showArchive = false) const
    {
        QStringList dimensionNames;

        for (std::ptrdiff_t i = inputUnits.size() - 1; i >= 0; --i) {
            const auto &units = inputUnits[i];
            if (units.empty())
                continue;

            bool multipleStation = false;
            if (showStation) {
                auto current = units.begin();
                auto prior = current;
                ++current;
                for (auto last = units.end(); current != last; prior = current, ++current) {
                    if (prior->getStation() != current->getStation()) {
                        multipleStation = true;
                        break;
                    }
                }
            }

            bool multipleArchive = false;
            if (showArchive) {
                auto current = units.begin();
                auto prior = current;
                ++current;
                for (auto last = units.end(); current != last; prior = current, ++current) {
                    if (prior->getArchive() != current->getArchive()) {
                        multipleArchive = true;
                        break;
                    }
                }
            }

            std::vector<Data::SequenceName> sorted(units.begin(), units.end());
            std::sort(sorted.begin(), sorted.end(), Data::SequenceName::OrderDisplayCaching());

            QString add;
            if (showStation && !multipleStation) {
                add.append(sorted.front().getStationQString().toUpper());
                add.append(' ');
            }
            if (showArchive && !multipleArchive) {
                add.append(sorted.front().getArchiveQString().toUpper());
                add.append(' ');
            }
            for (std::size_t j = 0, max = sorted.size(); j < max; j++) {
                if (j != 0) {
                    add.append(", ");
                }
                if (multipleStation) {
                    add.append(sorted[j].getStationQString().toUpper());
                    add.append(' ');
                }
                if (multipleArchive) {
                    add.append(sorted[j].getArchiveQString().toUpper());
                    add.append(' ');
                }
                add.append(sorted[j].getVariableQString());
                auto flavors = sorted[j].getFlavors();
                if (!flavors.empty()) {
                    add.append(" (");
                    QStringList sortedFlavors(Util::to_qstringlist(flavors));
                    std::sort(sortedFlavors.begin(), sortedFlavors.end());
                    add.append(sortedFlavors.join(",").toUpper());
                    add.append(')');
                }
            }

            dimensionNames.append(add);
        }

        if (dimensionNames.isEmpty()) {
            for (std::ptrdiff_t i = N - 1; i >= 0; i--) {
                const auto &d = getUnits(i);
                if (d.empty())
                    continue;
                QStringList sorted;
                Util::append(d, sorted);
                std::sort(sorted.begin(), sorted.end());
                dimensionNames.append(sorted.join(tr(",", "dimension parameter join")));
            }
        }

        return dimensionNames.join(tr(" vs ", "dimension join"));
    }

    /**
     * Get the default tool tip for the legend item.  This incorporates the
     * metadata and generates a default description for the trace.
     * 
     * @return the legend tooltip
     */
    QString getLegendTooltip() const
    {
        QStringList dimensionNames;

        for (std::ptrdiff_t i = N - 1; i >= 0; --i) {
            if (!metadata[i].description.isEmpty()) {
                QString text(metadata[i].description);
                if (FP::defined(metadata[i].wavelength)) {
                    if (!metadata[i].origin.isEmpty()) {
                        text = tr("%1 (%2 at %3 nm)", "description tooltip wavelength origin").arg(
                                text, metadata[i].origin,
                                FP::decimalFormat(metadata[i].wavelength, 0));
                    } else {
                        text = tr("%1 (%2 nm)", "description tooltip wavelength").arg(text,
                                                                                      QString::number(
                                                                                              metadata[i]
                                                                                                      .wavelength,
                                                                                              'f',
                                                                                              0));
                    }
                } else if (!metadata[i].origin.isEmpty()) {
                    text = tr("%1 (%2)", "description tooltip origin").arg(text,
                                                                           metadata[i].origin);
                }
                dimensionNames.append(text);
            } else if (FP::defined(metadata[i].wavelength)) {
                QString text(metadata[i].origin);
                if (!text.isEmpty()) {
                    text = tr("%1 (%2 nm)", "tooltip wavelength origin").arg(text, QString::number(
                            metadata[i].wavelength, 'f', 0));
                } else {
                    text = tr("%1 nm", "tooltip wavelength").arg(
                            FP::decimalFormat(metadata[i].wavelength, 0));
                }
                dimensionNames.append(text);
            } else if (!metadata[i].origin.isEmpty()) {
                dimensionNames.append(metadata[i].origin);
            }
        }

        return dimensionNames.join(tr("<br>", "tooltip join"));
    }

    /**
     * Get the default origin text for the handler.
     * 
     * @return                  the origin text
     */
    QString getOriginText() const
    {
        QStringList dimensionNames;

        for (std::ptrdiff_t i = N - 1; i >= 0; --i) {
            QString text(metadata[i].origin);
            if (text.isEmpty())
                continue;
            dimensionNames.append(text);
        }

        return dimensionNames.join(tr(", ", "origin join"));
    }


    /**
     * Get the override parameters.  This also flags the handler as having
     * had something overriden.
     * 
     * @return the override parameters
     */
    TraceParameters *overrideParameters()
    {
        if (!overridenParameters) {
            overridenParameters = parameters->clone();
            overridenParameters->clear();
            Q_ASSERT(!originalParameters);
            originalParameters = parameters->clone();
        }
        return overridenParameters.get();
    }

    /**
     * Merge all overrides into the main parameters.
     */
    void mergeOverrideParameters()
    {
        Q_ASSERT(overridenParameters.get() != nullptr);
        parameters->overlay(overridenParameters.get());
    }

    /**
     * Reset all parameters to the original state, reverting all overrides.
     */
    void revertOverrideParameters()
    {
        if (!originalParameters)
            return;
        parameters->copy(originalParameters.get());
        originalParameters.reset();
        overridenParameters.reset();
    }

    /**
     * Test if the handler has any overrides set.
     * 
     * @return true if there are any overrides set
     */
    bool hasOverrideParameters() const
    { return overridenParameters.get() != nullptr; }

    /**
     * Save the override parameters to the given target value.
     * 
     * @param target    the target of the override parameters
     */
    void saveOverrideParameters(Data::Variant::Write &target) const
    {
        Q_ASSERT(overridenParameters.get() != nullptr);
        overridenParameters->save(target);
    }

protected:
    /**
     * Copy the handler with the given parameters
     * 
     * @param other         the handler to copy
     * @param parameters    the parameters to use
     */
    TraceValueHandlerComplex(const TraceValueHandlerComplex<N, PointType> &other,
                             std::unique_ptr<TraceParameters> &&params)
            : TraceValueHandlerBase(),
              parameters(std::move(params)),
              start(other.start),
              end(other.end),
              dataRangeInconsistent(other.dataRangeInconsistent),
              untransformedValues(other.untransformedValues),
              needTransformRerun(other.transform != nullptr ? true : other.needTransformRerun),
              transform(nullptr),
              points(other.points),
              rangeTrimmedPoints(other.rangeTrimmedPoints),
              limits(other.limits)
    {
        for (std::size_t i = 0; i < N; i++) {
            units[i] = other.units[i];
            groupUnits[i] = other.groupUnits[i];
            metadata[i] = other.metadata[i];
        }
    }

    /**
     * Handle the fan out for values that expand to more than one point.  This
     * is called when appending a value that would fan out (e.x. an array).
     * The resulting values are created from the master template with the
     * other dimensions set based on the fan out.
     * 
     * @param points    the target points, all values are added to the end
     * @param master    the master template to add points from
     * @param idxAddStart the start index before any of the fanned out values where added
     * @param value     the value of the dimension
     * @param targetDim the target dimension index
     */
    void handleValueFanout(std::vector<PointType> &points,
                           const PointType &master,
                           std::size_t idxAddStart,
                           const Data::Variant::Read &value,
                           std::size_t targetDim) const
    {
        switch (value.getType()) {
        case Data::Variant::Type::Array:
        case Data::Variant::Type::Matrix: {
            std::size_t addSize = value.toArray().size();
            std::size_t nAvailable = points.size() - idxAddStart;
            for (std::size_t n = nAvailable; n < addSize; n++) {
                points.emplace_back(master);
            }
            for (std::size_t i = 0; i < addSize; i++) {
                points[idxAddStart + i].d[targetDim] =
                        Data::Variant::Composite::toNumber(value.array(i));
            }
            break;
        }

        default: {
            double v = Data::Variant::Composite::toNumber(value);
            for (auto set = points.begin() + idxAddStart, end = points.end(); set != end; ++set) {
                set->d[targetDim] = v;
            }
            break;
        }
        }
    }

    /**
     * Convert a value to points.  This is only called if there is no
     * transform in effect.  The implementation should append values to
     * the points list.
     * <br>
     * The default implementation adds a single point, based on the
     * dimension values.  Units metadata is also inspected and set.
     * 
     * @param value     the input value
     * @param points    the output list
     */
    virtual void convertValue(const TraceDispatch::OutputValue &value,
                              std::vector<PointType> &points)
    {
        if (value.isMetadata()) {
            for (std::size_t i = 0; i < N; i++) {
                updateMetadata(i, value.get(i));
            }
            return;
        }
        PointType add;
        add.start = value.getStart();
        add.end = value.getEnd();
        for (std::size_t i = 0; i < N; i++) {
            switch (value.getType(i)) {
            case Data::Variant::Type::Array:
            case Data::Variant::Type::Matrix: {
                int idxStart = points.size();
                for (std::size_t j = i; j < N; j++) {
                    add.d[j] = FP::undefined();
                }
                for (; i < N; i++) {
                    handleValueFanout(points, add, idxStart, value.get(i), i);
                }
                return;
            }
            default:
                add.d[i] = Data::Variant::Composite::toNumber(value.get(i));
                break;
            }
        }
        points.emplace_back(std::move(add));
    }

    /**
     * Handle the value fanout for transformer values.  This takes care of
     * expanding array values into multiple points.
     * 
     * @param points    the target points, all values are added to the end
     * @param value     the the source value
     * @param dimensionOffset the offset from the start of the dimension to insert at
     * @param M         the number of output dimensions
     */
    template<std::size_t M>
    static void handleTranformerFanout(QVector<Algorithms::TransformerPoint *> &points,
                                       const TraceDispatch::OutputValue &value,
                                       std::size_t dimensionOffset)
    {
        auto idxAddStart = points.size();
        points.push_back(new TransformValue<M>(value));

        for (std::size_t targetDim = dimensionOffset; targetDim < M; targetDim++) {
            switch (value.get(targetDim - dimensionOffset).getType()) {
            case Data::Variant::Type::Array:
            case Data::Variant::Type::Matrix: {
                auto source = value.get(targetDim - dimensionOffset);
                std::size_t addSize = source.toArray().size();
                std::size_t nAvailable = points.size() - idxAddStart;
                for (std::size_t n = nAvailable; n < addSize; n++) {
                    points.push_back(new TransformValue<M>(value));
                }
                for (std::size_t i = 0; i < addSize; i++) {
                    points[idxAddStart + i]->set(targetDim, Data::Variant::Composite::toNumber(
                            source.array(i)));
                }
                break;
            }

            default: {
                for (std::size_t i = idxAddStart;
                        i < static_cast<std::size_t>(points.size());
                        i++) {
                    points[i]->set(targetDim, Data::Variant::Composite::toNumber(
                            value.get(targetDim - dimensionOffset)));
                }
                break;
            }
            }
        }
    }

    /**
     * Convert a single trace point into transformer points, including array
     * fanout.
     * <br>
     * This calls handleTranformerFanout( 
     * QVector<Algorithms::TransformerPoint *> &,
     * const TraceDispatch::OutputValue &, int) for the input point based
     * on the transformer input mode.  The dimensions of the data are offset
     * by the mode as needed.
     * 
     * @param mode      the transformer mode
     * @param result    the result array
     * @param point     the input point
     */
    template<std::size_t M>
    static void handleTransformerPoint(TraceParameters::TransformerInputMode mode,
                                       QVector<Algorithms::TransformerPoint *> &result,
                                       const TraceDispatch::OutputValue &point)
    {
        switch (mode) {
        case TraceParameters::Transformer_ValuesOnly:
            handleTranformerFanout<M>(result, point, 0);
            break;
        case TraceParameters::Transformer_StartThenValues: {
            std::size_t idxStart = result.size();
            handleTranformerFanout<M>(result, point, 1);
            for (std::size_t i = idxStart; i < static_cast<std::size_t>(result.size()); i++) {
                result[i]->set(0, point.getStart());
            }
            break;
        }
        case TraceParameters::Transformer_EndThenValues: {
            std::size_t idxStart = result.size();
            handleTranformerFanout<M>(result, point, 1);
            for (std::size_t i = idxStart; i < static_cast<std::size_t>(result.size()); i++) {
                result[i]->set(0, point.getEnd());
            }
            break;
        }
        case TraceParameters::Transformer_MiddleThenValues: {
            std::size_t idxStart = result.size();
            handleTranformerFanout<M>(result, point, 1);
            for (std::size_t i = idxStart; i < static_cast<std::size_t>(result.size()); i++) {
                if (!FP::defined(point.getStart()))
                    result[i]->set(0, point.getEnd());
                else if (!FP::defined(point.getEnd()))
                    result[i]->set(0, point.getStart());
                else
                    result[i]->set(0, (point.getEnd() + point.getStart()) * 0.5);
            }
            break;
        }
        case TraceParameters::Transformer_StartEndValues: {
            std::size_t idxStart = result.size();
            handleTranformerFanout<M>(result, point, 2);
            for (std::size_t i = idxStart; i < static_cast<std::size_t>(result.size()); i++) {
                result[i]->set(0, point.getStart());
                if (M > 1)
                    result[i]->set(1, point.getEnd());
            }
            break;
        }
        }
    }

    /**
     * Prepare the parameters to run the transformer  This should allocate
     * the list of input points to the transformer.
     * <br>
     * The default implementation generates calls handleTransformerPoint( 
     * TransformerParameters::TransformerInputMode,
     * QVector<Algorithms::TransformerPoint *> &,
     * const TraceDispatch::OutputValue & ) for each input point, which takes
     * care of the fanout and input mode handling.
     * 
     * @param points        the input points (not filtered for visible times)
     * @param parameters    the transformer parameters
     */
    virtual QVector<Algorithms::TransformerPoint *> prepareTransformer(const std::vector<
            TraceDispatch::OutputValue> &points, Algorithms::TransformerParameters &parameters)
    {
        Q_UNUSED(parameters);

        QVector<Algorithms::TransformerPoint *> result;
        result.reserve(points.size());
        for (const auto &it : points) {
            if (!Range::intersects(it.getStart(), it.getEnd(), start, end))
                continue;

            if (it.isMetadata()) {
                switch (this->parameters->getTransformerInputMode()) {
                case TraceParameters::Transformer_ValuesOnly:
                    for (std::size_t i = 0; i < N; i++) {
                        updateMetadata(i, it.get(i));
                    }
                    break;
                case TraceParameters::Transformer_StartThenValues:
                case TraceParameters::Transformer_EndThenValues:
                case TraceParameters::Transformer_MiddleThenValues:
                    for (std::size_t i = 1; i < N; i++) {
                        updateMetadata(i, it.get(i - 1));
                    }
                    break;
                case TraceParameters::Transformer_StartEndValues:
                    for (std::size_t i = 2; i < N; i++) {
                        updateMetadata(i, it.get(i - 2));
                    }
                    break;
                }
                continue;
            }

            handleTransformerPoint<N>(this->parameters->getTransformerInputMode(), result, it);
        }
        return result;
    }

    /**
     * Convert the result of the transform.  This must take ownership
     * of any points returned (normally by deleting them).
     * <br>
     * The default implementation calls sets the dimension values
     * assuming the points are of type TransformValue<N>.  Unit metadata is
     * also inspected and set.
     * 
     * @param points        the result points
     * @return              a list of points
     */
    virtual std::vector<PointType> convertTransformerResult(const QVector<
            Algorithms::TransformerPoint *> &points)
    {
        std::vector<PointType> result;
        result.reserve(points.size());
        for (auto it : points) {
            const TransformValue<N> *point = static_cast<const TransformValue<N> *>(it);
            PointType add;
            add.start = point->getValue().getStart();
            add.end = point->getValue().getEnd();
            for (std::size_t i = 0; i < N; i++) {
                add.d[i] = point->get(i);
            }
            result.emplace_back(std::move(add));
            delete it;
        }
        return result;
    }

    /**
     * Return the allocator to be used with the transformer.
     * <br>
     * The default implementation just returns TransformValue<N>::allocator
     */
    virtual Algorithms::TransformerAllocator *getTransformerAllocator()
    { return &TransformValue<N>::allocator; }


    /**
     * Update metadata for a given dimension.
     * 
     * @param dimension     the dimension index
     * @param units         the units of the dimension (generally used in axis labels)
     * @param groupUnits    the units to group the dimension on (generally used in axis grouping, e.x. delta P no grouped with absolute pressures)
     * @param meta          the metadata for the dimension
     */
    void updateMetadata(std::size_t dimension,
                        const QString &units,
                        const QString &groupUnits,
                        const TraceDimensionMetadata &meta)
    {
        Q_ASSERT(dimension >= 0 && dimension < N);
        if (!units.isEmpty()) {
            this->units[dimension].insert(units);
        }
        if (!groupUnits.isEmpty()) {
            this->groupUnits[dimension].insert(groupUnits);
        }
        if (!meta.format.isEmpty()) {
            this->metadata[dimension].format = meta.format;
        }
        if (!meta.description.isEmpty()) {
            this->metadata[dimension].description = meta.description;
        }
        if (!meta.origin.isEmpty()) {
            this->metadata[dimension].origin = meta.origin;
        }
        if (FP::defined(meta.wavelength)) {
            this->metadata[dimension].wavelength = meta.wavelength;
        }
    }

    /**
     * Build the origin string from the source component of a metadata value.
     * 
     * @param source    the source metadata component
     * @return          the origin string
     */
    QString buildOriginString(const Data::Variant::Read &source)
    {
        QString result(source.hash("Manufacturer").toDisplayString());

        QString model(source.hash("Model").toDisplayString());
        if (!model.isEmpty()) {
            if (!result.isEmpty())
                result.append(tr(" ", "origin model join"));
            result.append(model);
        }

        QString serial(source.hash("SerialNumber").toDisplayString());
        if (serial.isEmpty()) {
            qint64 i = source.hash("SerialNumber").toInt64();
            if (INTEGER::defined(i))
                serial = QString::number(i);
        }
        if (!serial.isEmpty()) {
            if (!result.isEmpty()) {
                if (serial[0].isNumber()) {
                    result.append(tr(" #", "origin serial numeric join"));
                } else {
                    result.append(tr(" ", "origin serial join"));
                }
            }
            result.append(serial);
        }
        return result;
    }

    /**
     * Update the metadata for a given dimension from the base metadata
     * value.  This interprets the value and calls updateMetadata(int,
     * const QString &, const QString &, const QString &) with the
     * appropriate values.
     * 
     * @param dimension     the dimension index
     * @param value         the value to interpret
     */
    void updateMetadata(std::size_t dimension, const Data::Variant::Read &value)
    {
        switch (value.getType()) {
        case Data::Variant::Type::MetadataArray: {
            auto childUnits = value.metadata("Children").metadata("Units");
            if (!childUnits.exists())
                childUnits = value.metadata("Units");
            auto childGroupUnits = value.metadata("Children").metadata("GroupUnits");
            if (!childGroupUnits.exists())
                childGroupUnits = value.metadata("GroupUnits");
            auto childFormat = value.metadata("Children").metadata("Format");
            if (!childFormat.exists())
                childFormat = value.metadata("Format");
            auto childDescription = value.metadata("Children").metadata("Description");
            if (!childDescription.exists())
                childDescription = value.metadata("Description");
            auto source = value.metadata("Children").metadata("Source");
            if (!source.exists())
                source = value.metadata("Source");

            TraceDimensionMetadata meta;
            meta.format = childFormat.toDisplayString();
            meta.description = childDescription.toDisplayString();
            meta.origin = buildOriginString(source);
            meta.wavelength = value.metadata("Children").metadata("Wavelength").toDouble();
            if (!FP::defined(meta.wavelength))
                meta.wavelength = value.metadata("Wavelength").toDouble();

            updateMetadata(dimension, childUnits.toDisplayString(), childGroupUnits.toQString(),
                           meta);
            break;
        }
        default: {
            TraceDimensionMetadata meta;
            meta.format = value.metadata("Format").toDisplayString();
            meta.description = value.metadata("Description").toDisplayString();
            meta.origin = buildOriginString(value.metadata("Source"));
            meta.wavelength = value.metadata("Wavelength").toDouble();

            updateMetadata(dimension, value.metadata("Units").toDisplayString(),
                           value.metadata("GroupUnits").toQString(), meta);
            break;
        }
        }
    }
};

/* Well this is arcane... (initialize the static allocator) */
template<std::size_t N, typename PointType>
template<std::size_t M> typename TraceValueHandlerComplex<N, PointType>::template TransformValue<
        M>::Allocator
        TraceValueHandlerComplex<N, PointType>::TransformValue<M>::allocator;

/**
 * A trace handler that only supports simple data.
 */
template<std::size_t N>
class TraceValueHandler : public TraceValueHandlerComplex<N, TracePoint<N>> {
public:
    TraceValueHandler(std::unique_ptr<TraceParameters> &&params) : TraceValueHandlerComplex<N,
                                                                                            TracePoint<
                                                                                                    N> >(
            std::move(params))
    { }

    virtual ~TraceValueHandler()
    { }

    std::unique_ptr<TraceValueHandlerComplex<N, TracePoint<N>>> clone() const override
    {
        return std::unique_ptr<TraceValueHandlerComplex<N, TracePoint<N>>>(
                new TraceValueHandler<N>(*this,
                                         TraceValueHandlerComplex<N, TracePoint<N>>::getParameters()
                                                 ->clone()));
    }

protected:
    /**
     * Copy the handler with the given parameters
     * 
     * @param other         the handler to copy
     * @param parameters    the parameters to use
     */
    TraceValueHandler(const TraceValueHandler<N> &other, std::unique_ptr<TraceParameters> &&params)
            : TraceValueHandlerComplex<N, TracePoint<N> >(other, std::move(params))
    { }
};

/**
 * The base class for dispatch sets.  This should not be instantiated directly,
 * it exists only to provide a fixed base for Qt.
 */
class CPD3GRAPHING_EXPORT TraceDispatchSetBase : public QObject {
Q_OBJECT

protected:
    void handlersUpdated();

    /**
     * Test if all dispatchers have ended.  Once this returns true
     * then readyForFinalProcessing() is emitted.
     */
    virtual bool haveAllDispatchersEnded() = 0;

public:
    TraceDispatchSetBase();

    void dispatcherFinished();

signals:

    /**
     * Emitted when the state of the deferred processing has changed
     * and the handler needs to be processed again.
     */
    void deferredUpdate();

    /**
     * Emitted when all chain inputs have finished and the next (non-deferrable)
     * call to process will result in a final output.
     */
    void readyForFinalProcessing();

    /**
     * Emitted when a handler is added or removed.
     */
    void handlersChanged();
};

/**
 * A handler for a set of multiple trace dispatches.  This produces a number
 * of trace value handlers that represent the actual trace data.
 * 
 * @param N             the number of dimensions
 * @param HandlerType   the handler type
 */
template<std::size_t N, typename HandlerType>
class TraceDispatchSetComplex : public TraceDispatchSetBase {
    struct Override {
        std::array<Data::SequenceMatch::Composite, N> matchers;
        std::unique_ptr<TraceParameters> parameters;

        Override(const Override &other) : matchers(other.matchers),
                                          parameters(other.parameters->clone())
        { }

        Override &operator=(const Override &other)
        {
            matchers = other.matchers;
            parameters = other.parameters->clone();
            return *this;
        }

        Override(Override &&other) = default;

        Override &operator=(Override &&other) = default;

        Override() = default;

        ~Override() = default;

        void apply(TraceParameters *target, const std::vector<Data::SequenceName::Set> &units) const
        {
            for (std::size_t i = 0, max = std::min<std::size_t>(N, units.size()); i < max; i++) {
                bool hit = false;
                for (const auto &it : units[i]) {
                    if (matchers[i].matches(it)) {
                        hit = true;
                        break;
                    }
                }
                if (!hit)
                    return;
            }
            target->overlay(parameters.get());
        }
    };

    struct Group {
        ~Group() = default;

        Group(std::unique_ptr<TraceDispatch> &&sd,
              std::unique_ptr<TraceParameters> &&sp,
              std::vector<std::unique_ptr<Override>> &&sOverride,
              const QString &sName) : parameters(std::move(sp)),
                                      overrides(std::move(sOverride)),
                                      dispatch(std::move(sd)),
                                      name(sName)
        { }

        std::unique_ptr<TraceParameters> parameters;
        std::vector<std::unique_ptr<Override>> overrides;
        std::unique_ptr<TraceDispatch> dispatch;

        QString name;
        std::vector<std::unique_ptr<HandlerType>> handlers;

        void reset()
        {
            handlers.clear();
        }
    };

    std::vector<std::unique_ptr<Group>> groups;
    std::vector<HandlerType *> handlers;
    bool pendingGroupClear;

    double start;
    double end;

    void clearAllGroups()
    {
        pendingGroupClear = false;
        for (const auto &group : groups) {
            group->reset();
        }
        handlers.clear();
    }

public:
    TraceDispatchSetComplex() : pendingGroupClear(false),
                                start(FP::undefined()),
                                end(FP::undefined())
    { }

    virtual ~TraceDispatchSetComplex() = default;

    using Cloned = typename std::unique_ptr<TraceDispatchSetComplex<N, HandlerType>>;

    /**
     * Create a polymorphic copy of the set.  Note that this does NOT
     * connect the dispatch.  That is, the new group will be completely
     * unconnected from the data source.
     * 
     * @return a copy
     */
    virtual Cloned clone() const = 0;

    /**
     * Test if two values representing configurations for the trace set
     * set could be merged together into a single set.
     * 
     * @param a the first configuration
     * @param b the second configuration
     */
    static bool mergable(const Data::Variant::Read &a, const Data::Variant::Read &b)
    {
        std::unordered_set<Data::Variant::PathElement::HashIndex> indices;
        Util::merge(a.toHash().keys(), indices);
        Util::merge(b.toHash().keys(), indices);
        for (const auto &check : indices) {
            if (check.empty())
                continue;
            auto traceA = a.hash(check);
            auto traceB = b.hash(check);
            if (!traceA.exists() || !traceB.exists())
                continue;

            if (!TraceDispatch::mergable(traceA["Data"], traceB["Data"]))
                return false;
        }
        return true;
    }

    /**
     * Initialize the trace dispatch set from the given configuration.
     * 
     * @param config        the configuration
     * @param defaults      the defaults to use
     * @param asRealtime    true if the trace set is for realtime data
     */
    virtual void initialize(const Data::ValueSegment::Transfer &config,
                            const DisplayDefaults &defaults = {},
                            bool asRealtime = false)
    {
        groups.clear();

        std::unordered_map<Data::Variant::PathElement::HashIndex, Data::ValueSegment::Transfer>
                dispatchConfig;
        std::unordered_map<Data::Variant::PathElement::HashIndex, std::unique_ptr<TraceParameters>>
                parameters;
        std::unordered_map<Data::Variant::PathElement::HashIndex,
                           std::vector<std::unique_ptr<Override>>> overrides;
        for (const auto &cseg : config) {
            for (auto trace : cseg.getValue().toHash()) {
                if (trace.first.empty())
                    continue;
                if (!trace.second.exists())
                    continue;
                dispatchConfig[trace.first].emplace_back(cseg.getStart(), cseg.getEnd(),
                                                         Data::Variant::Root(trace.second["Data"]));

                if (trace.second["Settings"].exists()) {
                    Util::insert_or_assign(parameters, trace.first,
                                           std::unique_ptr<TraceParameters>(
                                                   parseParameters(trace.second["Settings"])));
                }

                if (trace.second["Override"].exists()) {
                    overrides[trace.first].clear();
                    for (auto over : trace.second["Override"].toChildren()) {
                        auto ml = over["Match"].toArray();
                        std::unique_ptr<Override> add(new Override);
                        for (std::size_t i = 0; i < N; i++) {
                            if (i >= ml.size() || !ml[i].exists()) {
                                add->matchers[i] = Data::SequenceMatch::Composite(
                                        Data::SequenceMatch::Element::SpecialMatch::All);
                            } else {
                                add->matchers[i] = Data::SequenceMatch::Composite(ml[i]);
                            }
                        }
                        add->parameters = parseParameters(over["Settings"]);
                        Q_ASSERT(add->parameters.get() != nullptr);
                        overrides[trace.first].emplace_back(std::move(add));
                    }
                }
            }
        }

        std::vector<Data::Variant::PathElement::HashIndex> sorted;
        for (const auto &add : dispatchConfig) {
            sorted.emplace_back(add.first);
        }
        std::sort(sorted.begin(), sorted.end());

        for (const auto &name : sorted) {
            std::unique_ptr<TraceDispatch>
                    d(new TraceDispatch(dispatchConfig[name], defaults, asRealtime));
            QObject::connect(d.get(), &TraceDispatch::allStreamsEnded,
                             static_cast<TraceDispatchSetBase *>(this),
                             &TraceDispatchSetBase::dispatcherFinished, Qt::QueuedConnection);
            auto p = std::move(parameters[name]);
            if (!p)
                p = parseParameters(Data::Variant::Read::empty());
            std::vector<std::unique_ptr<Override>> over;
            for (auto &add : overrides[name]) {
                over.emplace_back(std::move(add));
            }
            groups.emplace_back(new Group(std::move(d), std::move(p), std::move(over),
                                          QString::fromStdString(name)));
        }
    }

    /**
     * Get the list of segments that define any manual overrides in effect
     * for the trace set.
     * 
     * @param dispatchOverride  the override for the dispatch
     * @param overrideName      the override name hash to put values in
     * @return                  the list of override segments
     */
    Data::ValueSegment::Transfer getManualOverrides(const TraceDispatchOverride &dispatchOverride = {},
                                                    const QString &overrideName = QString::fromLatin1(
                                                            "Saved")) const
    {
        Data::ValueSegment::Transfer result;
        QList<QString> paths;
        for (const auto &group : groups) {
            for (std::size_t handlerIdx = 0, nHandlers = group->handlers.size();
                    handlerIdx < nHandlers;
                    handlerIdx++) {
                const auto &handler = group->handlers[handlerIdx];
                if (!handler->hasOverrideParameters())
                    continue;

                Data::Variant::Root base;
                auto target = base.write().hash(group->name).hash("Override").hash(overrideName);
                auto overrideTarget = target["Settings"];
                handler->saveOverrideParameters(overrideTarget);
                for (std::size_t i = 0; i < N; i++) {
                    const auto &units = group->dispatch->getUnits(handlerIdx, i);
                    if (units.empty())
                        continue;

                    auto vs = target["Match"].array(i);
                    for (const auto &unit : units) {
                        Data::SequenceMatch::Composite::addToConfiguration(vs, unit);
                    }
                }

                result.emplace_back(FP::undefined(), FP::undefined(), base);
                paths.append(QString());
            }

            auto add = dispatchOverride.get(group->dispatch.get());
            if (!add.empty()) {
                QString path(group->name);
                path.append("/Data");
                for (int i = 0, max = add.size(); i < max; i++) {
                    paths.append(path);
                }
                Util::append(std::move(add), result);
            }
        }
        if (result.size() < 2 && (paths.isEmpty() || paths.front().isEmpty()))
            return result;
        return Data::ValueSegment::merge(result, paths);
    }

    /**
     * Reset all manual overrides for the set.
     */
    void resetManualOverrides()
    {
        for (auto &handler : handlers) {
            handler->revertOverrideParameters();
        }
    }

    /**
     * Process any updates needed.
     * 
     * @param asRealtime    do the realtime post processing (remove everything except the last value)
     * @param deferrable    if set then defer long processing as needed
     * @return              true if the underlying data was updated
     */
    virtual bool process(bool asRealtime = false, bool deferrable = false)
    {
        bool changed = false;
        bool handlersChangedEmitted = false;
        for (const auto &group : groups) {
            if (!group->dispatch)
                continue;
            auto incoming = group->dispatch->getPending();
            for (std::size_t i = 0, max = incoming.size(); i < max; i++) {
                if (pendingGroupClear && !incoming[i].empty()) {
                    clearAllGroups();
                    changed = true;
                    if (!handlersChangedEmitted) {
                        handlersUpdated();
                        handlersChangedEmitted = true;
                    }
                }

                while (i >= group->handlers.size()) {
                    auto p = group->parameters->clone();
                    const auto &units = group->dispatch->getUnits(group->handlers.size());
                    for (const auto &it : group->overrides) {
                        it->apply(p.get(), units);
                    }
                    auto h = createHandler(std::move(p));
                    Q_ASSERT(h.get() != nullptr);
                    QObject::connect(h.get(), &HandlerType::deferredUpdate, this,
                                     &TraceDispatchSetBase::deferredUpdate, Qt::QueuedConnection);
                    h->setVisibleTimeRange(start, end);
                    handlers.emplace_back(h.get());
                    group->handlers.emplace_back(std::move(h));
                    changed = true;
                    if (!handlersChangedEmitted) {
                        handlersUpdated();
                        handlersChangedEmitted = true;
                    }
                }

                if (group->handlers[i]->process(incoming[i], deferrable))
                    changed = true;
                if (asRealtime)
                    group->handlers[i]->trimToLast();
            }
        }
        return changed;
    }

    /**
     * Get all active handlers.
     * 
     * @return a list of handlers
     */
    const std::vector<HandlerType *> &getHandlers() const
    { return handlers; }

    /**
     * Register all handlers with the given chain.
     * 
     * @param chain         the chain to register with
     * @param deferClear    if set then the actual trace clear is deferred until data is received
     */
    virtual void registerChain(Data::ProcessingTapChain *chain, bool deferClear = true)
    {
        if (!deferClear) {
            clearAllGroups();
        } else {
            pendingGroupClear = true;
        }

        for (const auto &group : groups) {
            group->dispatch->registerChain(chain);
        }

        if (groups.empty())
            dispatcherFinished();
    }

    /**
     * Trim data to the given range.
     * 
     * @param start the start of the range to include
     * @param end   the end of the range to include
     */
    void trimData(double start, double end)
    {
        for (auto handler : handlers) {
            handler->trimData(start, end);
        }
    }

    /**
     * Get the current visible start.
     * 
     * @return the start time
     */
    inline double getStart() const
    { return start; }

    /**
     * Get the current visible end.
     * 
     * @return the end time
     */
    inline double getEnd() const
    { return end; }

    /**
     * Set the visible time range of data.
     * 
     * @param start the start time
     * @param end   the end time
     */
    void setVisibleTimeRange(double start, double end)
    {
        this->start = start;
        this->end = end;
        for (auto handler : handlers) {
            handler->setVisibleTimeRange(start, end);
        }
    }

    /**
     * Get all handlers affected by the given variable selection.
     * 
     * @param selection     the selection to match
     * @param dimensions    the output matched dimensions if not NULL
     * @return              the list of matched handlers
     */
    std::vector<HandlerType *> getAffectedHandlers(const Data::SequenceMatch::Composite &selection,
                                                   std::vector<std::unordered_set<
                                                           std::size_t>> *dimensions = nullptr) const
    {
        std::vector<HandlerType *> result;
        for (const auto &group : groups) {
            for (std::size_t hIdx = 0, hMax = group->handlers.size(); hIdx < hMax; hIdx++) {
                const auto &units = group->dispatch->getUnits(hIdx);
                bool first = true;
                for (std::size_t dIdx = 0, dMax = units.size(); dIdx < dMax; dIdx++) {
                    bool hit = false;
                    for (const auto &it : units[dIdx]) {
                        if (!selection.matches(it))
                            continue;
                        hit = true;
                        break;
                    }
                    if (!hit)
                        continue;

                    if (!dimensions) {
                        result.emplace_back(group->handlers[hIdx].get());
                        break;
                    }

                    if (first) {
                        result.emplace_back(group->handlers[hIdx].get());
                        dimensions->emplace_back();
                        first = false;
                    }
                    dimensions->back().insert(dIdx);
                }
            }
        }
        return result;
    }

    /**
     * Get the total number of groups in the trace set.
     * 
     * @param the total number of groups
     */
    std::size_t getTotalGroups() const
    { return groups.size(); }

    /**
     * Get the handler associated with a given group.
     * 
     * @param i     the group to query
     * @return      the handlers for the given group
     */
    inline const std::vector<std::unique_ptr<HandlerType>> &getGroupHandlers(std::size_t i) const
    { return groups[i]->handlers; }

    /**
     * Get the dispatch for a given group.
     * 
     * @param i     the group to query
     * @return      the dispatch for the given group
     */
    inline const TraceDispatch *getGroupDispatch(std::size_t i) const
    { return groups[i]->dispatch.get(); }

protected:
    TraceDispatchSetComplex(const TraceDispatchSetComplex<N, HandlerType> &other)
            : TraceDispatchSetBase(),
              pendingGroupClear(other.pendingGroupClear),
              start(other.start),
              end(other.end)
    {
        for (const auto &group : other.groups) {
            std::vector<std::unique_ptr<Override>> overrides;
            overrides.reserve(group->overrides.size());
            for (const auto &over : group->overrides) {
                overrides.emplace_back(new Override(*over));
            }

            groups.emplace_back(
                    new Group(std::unique_ptr<TraceDispatch>(new TraceDispatch(*group->dispatch)),
                              group->parameters->clone(), std::move(overrides), group->name));

            for (const auto &handler : group->handlers) {
                groups.back()
                      ->handlers
                      .emplace_back(static_cast<HandlerType *>(handler->clone().release()));
                handlers.emplace_back(groups.back()->handlers.back().get());
            }
        }
    }

    bool haveAllDispatchersEnded() override
    {
        for (const auto &group : groups) {
            if (!group->dispatch->haveAllStreamsEnded())
                return false;
        }
        return true;
    }

    /**
     * Parse the given value into a new parameters object.
     * 
     * @param value     the value to parse
     * @return          a newly allocated parameters object
     */
    virtual std::unique_ptr<TraceParameters> parseParameters(const Data::Variant::Read &value) const
    { return std::unique_ptr<TraceParameters>(new TraceParameters(value)); }

    using CreatedHandler = typename std::unique_ptr<HandlerType>;

    /**
     * Create a new handler object, given a set of parameters.
     * 
     * @param parameters    the parameters to create the handler with
     * @return              a newly allocated handler
     */
    virtual CreatedHandler createHandler(std::unique_ptr<TraceParameters> &&parameters) const = 0;
};

/**
 * A trace dispatch set that only supports simple data.
 */
template<std::size_t N>
class TraceDispatchSet : public TraceDispatchSetComplex<N, TraceValueHandler<N>> {
public:
    TraceDispatchSet() = default;

    virtual ~TraceDispatchSet() = default;

    using Cloned = typename TraceDispatchSetComplex<N, TraceValueHandler<N>>::Cloned;

    using CreatedHandler = typename TraceDispatchSetComplex<N,
                                                            TraceValueHandler<N>>::CreatedHandler;

    Cloned clone() const override
    { return Cloned(new TraceDispatchSet<N>(*this)); }

protected:
    TraceDispatchSet(const TraceDispatchSet<N> &other) : TraceDispatchSetComplex<N,
                                                                                 TraceValueHandler<
                                                                                         N> >(other)
    { }

    CreatedHandler createHandler(std::unique_ptr<TraceParameters> &&parameters) const override
    { return CreatedHandler(new TraceValueHandler<N>(std::move(parameters))); }
};

}
}

#endif
