/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGCYCLECOMMON_H
#define CPD3GRAPHINGCYCLECOMMON_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "graphing/graphing.hxx"
#include "graphing/axistickgenerator.hxx"
#include "core/timeutils.hxx"
#include "core/number.hxx"
#include "datacore/variant/root.hxx"

namespace CPD3 {
namespace Graphing {

/**
 * A class to manage the outer interval division for time cycle axes.  This
 * does not handle the actual transform, instead it is used by the conversion
 * of times to values.
 */
class CPD3GRAPHING_EXPORT TimeCycleInterval {
    Time::LogicalTimeUnit units;
    int count;
    bool align;

    double lastStart;
    double lastEnd;
    double lastBase;
public:
    TimeCycleInterval();

    TimeCycleInterval(const Data::Variant::Read &config);

    TimeCycleInterval(Time::LogicalTimeUnit setUnits, int setCount, bool setAlign = true);

    TimeCycleInterval(const TimeCycleInterval &);

    TimeCycleInterval &operator=(const TimeCycleInterval &);

    TimeCycleInterval(TimeCycleInterval &&);

    TimeCycleInterval &operator=(TimeCycleInterval &&);

    /**
     * Get the interval units.
     * @return the interval time units
     */
    inline Time::LogicalTimeUnit getUnits() const
    { return units; }

    /**
     * Get the interval unit count.
     * @return the interval unit count
     */
    inline int getCount() const
    { return count; }

    /**
     * Test if the interval should be aligned.
     * @return true if the interval should be aligned
     */
    inline bool getAlign() const
    { return align; }

    /**
     * Write the interval out to a value.
     * @return the configuration value
     */
    Data::Variant::Root save() const;

    void intervalBounds(double base, double time, double &start, double &end);

    void intervalBounds(double base, double time, double &start, double &end) const;
};

/**
 * The inner division transformer for a time cycle axis.  This does not
 * handle the point to screen space transformation, instead it it used to
 * convert times into numbers that can be used for plotting.
 */
class CPD3GRAPHING_EXPORT TimeCycleDivision {
    Time::LogicalTimeUnit units;
    int count;

    double lastStart;
    double lastEnd;
    double lastBase;
    int lastOffset;
public:
    TimeCycleDivision();

    TimeCycleDivision(const Data::Variant::Read &config);

    TimeCycleDivision(Time::LogicalTimeUnit setUnits, int setCount);

    TimeCycleDivision(const TimeCycleDivision &);

    TimeCycleDivision &operator=(const TimeCycleDivision &);

    TimeCycleDivision(TimeCycleDivision &&);

    TimeCycleDivision &operator=(TimeCycleDivision &&);

    /**
     * Get the division units.
     * @return the division time units
     */
    inline Time::LogicalTimeUnit getUnits() const
    { return units; }

    /**
     * Get the division unit count.
     * @return the division unit count
     */
    inline int getCount() const
    { return count; }

    /**
     * Write the division out to a value.
     * @return the configuration value
     */
    Data::Variant::Root save() const;

    /**
     * Rectify the division with the interval.  If the division doesn't make
     * sense with the interval (for example, if it's longer) then this will
     * set the division to something that makes sense.
     */
    void rectifyWithInterval(const TimeCycleInterval &interval);

    double convertTime(double base, double time, TimeCycleInterval &interval);

    double convertTime(double base, double time, const TimeCycleInterval &interval) const;

    void getTotalBounds(double &lower, double &upper, const TimeCycleInterval &interval) const;
};

/**
 * Provides divisions for an axis representing a cycle.
 */
class CPD3GRAPHING_EXPORT AxisTickGeneratorTimeCycle : public AxisTickGenerator {
Q_OBJECT

    TimeCycleInterval interval;
    TimeCycleDivision division;

    AxisTickGeneratorDecimal decimal;
public:
    AxisTickGeneratorTimeCycle(QObject *parent = 0);

    AxisTickGeneratorTimeCycle(const TimeCycleInterval &setInterval,
                               const TimeCycleDivision &setDivision,
                               QObject *parent = 0);

    virtual ~AxisTickGeneratorTimeCycle();

    virtual std::vector<AxisTickGenerator::Tick> generate(double min,
                                                          double max,
                                                          int flags = AxisTickGenerator::DefaultFlags) const;

    static AxisTickGenerator::Tick getTotalTick(const TimeCycleInterval &interval,
                                                const TimeCycleDivision &division);

    /** The types of titles for the divisions */
    enum TitleType {
        /** On an axis. */
        Title_Axis, /** As a suffix to a fit. */
        Title_Fit, /** As part of a tooltip unit display. */
        Title_Units,
    };

    static QString getTitle(const TimeCycleInterval &interval,
                            const TimeCycleDivision &division,
                            TitleType type);
};


}
}


#endif
