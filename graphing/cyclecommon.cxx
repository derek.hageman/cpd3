/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "graphing/cyclecommon.hxx"
#include "datacore/variant/composite.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Graphing {

/** @file graphing/cyclecommon.hxx
 * Provides tools to deal with the common aspects of plotting cyclic data.
 */

TimeCycleInterval::TimeCycleInterval() : units(Time::Year),
                                         count(1),
                                         align(true),
                                         lastStart(FP::undefined()),
                                         lastEnd(FP::undefined()),
                                         lastBase(FP::undefined())
{ }

TimeCycleInterval::TimeCycleInterval(Time::LogicalTimeUnit setUnits, int setCount, bool setAlign)
        : units(setUnits),
          count(setCount),
          align(setAlign),
          lastStart(FP::undefined()),
          lastEnd(FP::undefined()),
          lastBase(FP::undefined())
{ }

TimeCycleInterval::TimeCycleInterval(const Variant::Read &config) : units(Time::Year),
                                                                    count(1),
                                                                    align(true),
                                                                    lastStart(FP::undefined()),
                                                                    lastEnd(FP::undefined()),
                                                                    lastBase(FP::undefined())
{
    units = Variant::Composite::toTimeInterval(config, &count, &align);
    if (count < 1)
        count = 1;
}

TimeCycleInterval::TimeCycleInterval(const TimeCycleInterval &) = default;

TimeCycleInterval &TimeCycleInterval::operator=(const TimeCycleInterval &) = default;

TimeCycleInterval::TimeCycleInterval(TimeCycleInterval &&) = default;

TimeCycleInterval &TimeCycleInterval::operator=(TimeCycleInterval &&) = default;

Variant::Root TimeCycleInterval::save() const
{
    Variant::Root value;

    value["Count"].setInt64(count);
    value["Align"].setBool(align);
    switch (units) {
    case Time::Millisecond:
        value["Units"].setString("Millisecond");
        break;
    case Time::Second:
        value["Units"].setString("Second");
        break;
    case Time::Minute:
        value["Units"].setString("Minute");
        break;
    case Time::Hour:
        value["Units"].setString("Hour");
        break;
    case Time::Day:
        value["Units"].setString("Day");
        break;
    case Time::Week:
        value["Units"].setString("Week");
        break;
    case Time::Month:
        value["Units"].setString("Month");
        break;
    case Time::Quarter:
        value["Units"].setString("Quarter");
        break;
    case Time::Year:
        value["Units"].setString("Year");
        break;
    }

    return value;
}

/**
 * Get the bounds based on an input time.
 * 
 * @param base  the base time to use (e.x. the graph start time)
 * @param time  the time to convert
 * @param start the output start of the interval
 * @param end   the output end of the interval
 */
void TimeCycleInterval::intervalBounds(double base, double time, double &start, double &end)
{
    Q_ASSERT(FP::defined(time));
    if (FP::defined(lastStart) &&
            time >= lastStart &&
            time < lastEnd &&
            (!FP::defined(lastBase) || (FP::defined(base) && base == lastBase))) {
        start = lastStart;
        end = lastEnd;
        return;
    }

    double originalBase = base;

    if (!FP::defined(base)) {
        base = time;
    } else if (align && FP::defined(lastEnd) && FP::defined(lastBase) && base == lastBase) {
        base = lastEnd;
    }

    lastStart = base;
    if (align)
        lastStart = Time::floor(lastStart, units, count);
    lastEnd = Time::logical(lastStart, units, count, align, true);

    int offset = 0;
    while (lastEnd + 0.0005 < time) {
        ++offset;
        lastStart = Time::logical(base, units, count, align, false, offset);
        lastEnd = Time::logical(lastStart, units, count, align, true);
    }
    while (lastStart - 0.0005 > time) {
        --offset;
        lastStart = Time::logical(base, units, count, align, false, offset);
        lastEnd = Time::logical(lastStart, units, count, align, true);
    }

    start = lastStart;
    end = lastEnd;
    lastBase = originalBase;
}

/**
 * Get the bounds based on an input time.
 * 
 * @param base  the base time to use (e.x. the graph start time)
 * @param time  the time to convert
 * @param start the output start of the interval
 * @param end   the output end of the interval
 */
void TimeCycleInterval::intervalBounds(double base, double time, double &start, double &end) const
{
    Q_ASSERT(FP::defined(time));

    if (!FP::defined(base)) {
        base = time;
    } else if (align && FP::defined(lastEnd) && FP::defined(lastBase) && base == lastBase) {
        base = lastEnd;
    }

    start = base;
    if (align)
        start = Time::floor(start, units, count);
    end = Time::logical(start, units, count, align, true);

    int offset = 0;
    while (end + 0.0005 < time) {
        ++offset;
        start = Time::logical(base, units, count, align, false, offset);
        end = Time::logical(start, units, count, align, true);
    }
    while (start - 0.0005 > time) {
        --offset;
        start = Time::logical(base, units, count, align, false, offset);
        end = Time::logical(start, units, count, align, true);
    }
}

TimeCycleDivision::TimeCycleDivision() : units(Time::Month),
                                         count(1),
                                         lastStart(FP::undefined()),
                                         lastEnd(FP::undefined()),
                                         lastBase(FP::undefined()),
                                         lastOffset(-1)
{ }

TimeCycleDivision::TimeCycleDivision(Time::LogicalTimeUnit setUnits, int setCount) : units(
        setUnits),
                                                                                     count(setCount),
                                                                                     lastStart(
                                                                                             FP::undefined()),
                                                                                     lastEnd(FP::undefined()),
                                                                                     lastBase(
                                                                                             FP::undefined()),
                                                                                     lastOffset(-1)
{ }

TimeCycleDivision::TimeCycleDivision(const Variant::Read &config) : units(Time::Month),
                                                                    count(1),
                                                                    lastStart(FP::undefined()),
                                                                    lastEnd(FP::undefined()),
                                                                    lastBase(FP::undefined()),
                                                                    lastOffset(-1)
{
    units = Variant::Composite::toTimeInterval(config, &count);
    if (count < 1)
        count = 1;
}

TimeCycleDivision::TimeCycleDivision(const TimeCycleDivision &) = default;

TimeCycleDivision &TimeCycleDivision::operator=(const TimeCycleDivision &) = default;

TimeCycleDivision::TimeCycleDivision(TimeCycleDivision &&) = default;

TimeCycleDivision &TimeCycleDivision::operator=(TimeCycleDivision &&) = default;

Variant::Root TimeCycleDivision::save() const
{
    Variant::Root value;

    value["Count"].setInt64(count);
    switch (units) {
    case Time::Millisecond:
        value["Units"].setString("Millisecond");
        break;
    case Time::Second:
        value["Units"].setString("Second");
        break;
    case Time::Minute:
        value["Units"].setString("Minute");
        break;
    case Time::Hour:
        value["Units"].setString("Hour");
        break;
    case Time::Day:
        value["Units"].setString("Day");
        break;
    case Time::Week:
        value["Units"].setString("Week");
        break;
    case Time::Month:
        value["Units"].setString("Month");
        break;
    case Time::Quarter:
        value["Units"].setString("Quarter");
        break;
    case Time::Year:
        value["Units"].setString("Year");
        break;
    }

    return value;
}

void TimeCycleDivision::rectifyWithInterval(const TimeCycleInterval &interval)
{
    if (units > interval.getUnits()) {
        units = interval.getUnits();
    }
    if (units == interval.getUnits() && count >= interval.getCount()) {
        count = 1;
    }

    qint64 max = -1;
    switch (interval.getUnits()) {
    case Time::Year:
        switch (units) {
        case Time::Millisecond:
            max = (qint64) interval.getCount() * Q_INT64_C(31536000000);
            break;
        case Time::Second:
            max = (qint64) interval.getCount() * Q_INT64_C(31536000);
            break;
        case Time::Minute:
            max = (qint64) interval.getCount() * Q_INT64_C(525600);
            break;
        case Time::Hour:
            max = (qint64) interval.getCount() * Q_INT64_C(8760);
            break;
        case Time::Day:
            max = (qint64) interval.getCount() * Q_INT64_C(365);
            break;
        case Time::Week:
            max = (qint64) interval.getCount() * Q_INT64_C(52);
            break;
        case Time::Month:
            max = (qint64) interval.getCount() * Q_INT64_C(24);
            break;
        case Time::Quarter:
            max = (qint64) interval.getCount() * Q_INT64_C(4);
            break;
        default:
            break;
        }
        break;
    case Time::Quarter:
        switch (units) {
        case Time::Millisecond:
            max = (qint64) interval.getCount() * Q_INT64_C(7948800000);
            break;
        case Time::Second:
            max = (qint64) interval.getCount() * Q_INT64_C(7948800);
            break;
        case Time::Minute:
            max = (qint64) interval.getCount() * Q_INT64_C(132480);
            break;
        case Time::Hour:
            max = (qint64) interval.getCount() * Q_INT64_C(2208);
            break;
        case Time::Day:
            max = (qint64) interval.getCount() * Q_INT64_C(92);
            break;
        case Time::Week:
            max = (qint64) interval.getCount() * Q_INT64_C(13);
            break;
        case Time::Month:
            max = (qint64) interval.getCount() * Q_INT64_C(3);
            break;
        default:
            break;
        }
        break;
    case Time::Month:
        switch (units) {
        case Time::Millisecond:
            max = (qint64) interval.getCount() * Q_INT64_C(2678400000);
            break;
        case Time::Second:
            max = (qint64) interval.getCount() * Q_INT64_C(2678400);
            break;
        case Time::Minute:
            max = (qint64) interval.getCount() * Q_INT64_C(44640);
            break;
        case Time::Hour:
            max = (qint64) interval.getCount() * Q_INT64_C(744);
            break;
        case Time::Day:
            max = (qint64) interval.getCount() * Q_INT64_C(31);
            break;
        case Time::Week:
            max = (qint64) interval.getCount() * Q_INT64_C(4);
            break;
        default:
            break;
        }
        break;
    case Time::Week:
        switch (units) {
        case Time::Millisecond:
            max = (qint64) interval.getCount() * Q_INT64_C(604800000);
            break;
        case Time::Second:
            max = (qint64) interval.getCount() * Q_INT64_C(604800);
            break;
        case Time::Minute:
            max = (qint64) interval.getCount() * Q_INT64_C(10080);
            break;
        case Time::Hour:
            max = (qint64) interval.getCount() * Q_INT64_C(168);
            break;
        case Time::Day:
            max = (qint64) interval.getCount() * Q_INT64_C(7);
            break;
        default:
            break;
        }
        break;
    case Time::Day:
        switch (units) {
        case Time::Millisecond:
            max = (qint64) interval.getCount() * Q_INT64_C(86400000);
            break;
        case Time::Second:
            max = (qint64) interval.getCount() * Q_INT64_C(86400);
            break;
        case Time::Minute:
            max = (qint64) interval.getCount() * Q_INT64_C(1440);
            break;
        case Time::Hour:
            max = (qint64) interval.getCount() * Q_INT64_C(24);
            break;
        default:
            break;
        }
        break;
    case Time::Hour:
        switch (units) {
        case Time::Millisecond:
            max = (qint64) interval.getCount() * Q_INT64_C(3600000);
            break;
        case Time::Second:
            max = (qint64) interval.getCount() * Q_INT64_C(3600);
            break;
        case Time::Minute:
            max = (qint64) interval.getCount() * Q_INT64_C(60);
            break;
        default:
            break;
        }
        break;
    case Time::Minute:
        switch (units) {
        case Time::Millisecond:
            max = (qint64) interval.getCount() * Q_INT64_C(60000);
            break;
        case Time::Second:
            max = (qint64) interval.getCount() * Q_INT64_C(60);
            break;
        default:
            break;
        }
        break;
    case Time::Second:
        switch (units) {
        case Time::Millisecond:
            max = (qint64) interval.getCount() * Q_INT64_C(1000);
            break;
        default:
            break;
        }
        break;
    case Time::Millisecond:
        break;
    }

    if (max != -1 && count >= max)
        count = 1;
}

/**
 * Convert a time to a position on the graph.
 * 
 * @param base      the base time to use
 * @param time      the time to convert
 * @param interval  the graph interval
 */
double TimeCycleDivision::convertTime(double base, double time, TimeCycleInterval &interval)
{
    if (!FP::defined(time))
        return time;
    if (lastOffset >= 0 && FP::equal(base, lastBase) && time >= lastStart && time < lastEnd) {
        return (time - lastStart) / (lastEnd - lastStart) + (double) lastOffset;
    }
    double start = 0;
    double end = 0;
    interval.intervalBounds(base, time, start, end);

    int offset = 0;
    end = Time::logical(start, units, count, false, true);
    for (int n = 2; end + 0.0005 < time; n++, offset++) {
        end = Time::logical(start, units, count, false, true, n);
    }
    start = Time::logical(end, units, count, false, false, -1);

    if (units == Time::Day &&
            count == 1 &&
            interval.getUnits() == Time::Year &&
            interval.getCount() == 1)
        offset++;

    lastStart = start;
    lastEnd = end;
    lastBase = base;
    lastOffset = offset;
    return (time - start) / (end - start) + (double) offset;
}

/**
 * Convert a time to a position on the graph.
 * 
 * @param base      the base time to use
 * @param time      the time to convert
 * @param interval  the graph interval
 */
double TimeCycleDivision::convertTime(double base,
                                      double time,
                                      const TimeCycleInterval &interval) const
{
    if (!FP::defined(time))
        return time;

    double start = 0;
    double end = 0;
    interval.intervalBounds(base, time, start, end);

    int offset = 0;
    end = Time::logical(start, units, count, false, true);
    for (int n = 2; end + 0.0005 < time; n++, offset++) {
        end = Time::logical(start, units, count, false, true, n);
    }
    start = Time::logical(end, units, count, false, false, -1);

    if (count == 1 && interval.getCount() == 1 && interval.getAlign()) {
        if (units == Time::Day && interval.getUnits() == Time::Year)
            offset++;
        else if (units == Time::Day && interval.getUnits() == Time::Month)
            offset++;
    }

    return (time - start) / (end - start) + (double) offset;
}

/**
 * Get the total possible bounds for the divisions.
 * 
 * @param lower     the lower possible bound
 * @param upper     the upper possible bound
 * @param interval  the graph interval
 */
void TimeCycleDivision::getTotalBounds(double &lower,
                                       double &upper,
                                       const TimeCycleInterval &interval) const
{
    lower = 0;
    upper = -1;
    switch (interval.getUnits()) {
    case Time::Year:
        switch (units) {
        case Time::Year:
            upper = (double) interval.getCount() / (double) count;
            break;
        case Time::Quarter:
            upper = (double) interval.getCount() * (4.0 / (double) count);
            break;
        case Time::Month:
            upper = (double) interval.getCount() * (12.0 / (double) count);
            break;
        case Time::Week:
            upper = (double) interval.getCount() * (53.0 / (double) count);
            break;
        case Time::Day:
            if (count == 1 && interval.getCount() == 1 && interval.getAlign()) {
                lower = 1.0;
                upper = 367.0;
            } else {
                upper = (double) interval.getCount() * (366.0 / (double) count);
            }
            break;
        case Time::Hour:
            upper = (double) interval.getCount() * (8784.0 / (double) count);
            break;
        case Time::Minute:
            upper = (double) interval.getCount() * (527040.0 / (double) count);
            break;
        case Time::Second:
            upper = (double) interval.getCount() * (31622400.0 / (double) count);
            break;
        case Time::Millisecond:
            upper = (double) interval.getCount() * (31622400000.0 / (double) count);
            break;
        }
        break;
    case Time::Quarter:
        switch (units) {
        case Time::Quarter:
            upper = (double) interval.getCount() / (double) count;
            break;
        case Time::Month:
            upper = (double) interval.getCount() * (3.0 / (double) count);
            break;
        case Time::Week:
            upper = (double) interval.getCount() * (14.0 / (double) count);
            break;
        case Time::Day:
            upper = (double) interval.getCount() * (92.0 / (double) count);
            break;
        case Time::Hour:
            upper = (double) interval.getCount() * (2208.0 / (double) count);
            break;
        case Time::Minute:
            upper = (double) interval.getCount() * (132480.0 / (double) count);
            break;
        case Time::Second:
            upper = (double) interval.getCount() * (7948800.0 / (double) count);
            break;
        case Time::Millisecond:
            upper = (double) interval.getCount() * (7948800000.0 / (double) count);
            break;
        default:
            Q_ASSERT(false);
            break;
        }
        break;
    case Time::Month:
        switch (units) {
        case Time::Month:
            upper = (double) interval.getCount() / (double) count;
            break;
        case Time::Week:
            upper = (double) interval.getCount() * (4.43 / (double) count);
            break;
        case Time::Day:
            if (count == 1 && interval.getCount() == 1 && interval.getAlign()) {
                lower = 1.0;
                upper = 32.0;
            } else {
                upper = (double) interval.getCount() * (31.0 / (double) count);
            }
            break;
        case Time::Hour:
            upper = (double) interval.getCount() * (744.0 / (double) count);
            break;
        case Time::Minute:
            upper = (double) interval.getCount() * (44640.0 / (double) count);
            break;
        case Time::Second:
            upper = (double) interval.getCount() * (2678400.0 / (double) count);
            break;
        case Time::Millisecond:
            upper = (double) interval.getCount() * (2678400000.0 / (double) count);
            break;
        default:
            Q_ASSERT(false);
            break;
        }
        break;
    case Time::Week:
        switch (units) {
        case Time::Week:
            upper = (double) interval.getCount() / (double) count;
            break;
        case Time::Day:
            upper = (double) interval.getCount() * (7.0 / (double) count);
            break;
        case Time::Hour:
            upper = (double) interval.getCount() * (168.0 / (double) count);
            break;
        case Time::Minute:
            upper = (double) interval.getCount() * (10080.0 / (double) count);
            break;
        case Time::Second:
            upper = (double) interval.getCount() * (604800.0 / (double) count);
            break;
        case Time::Millisecond:
            upper = (double) interval.getCount() * (604800000.0 / (double) count);
            break;
        default:
            Q_ASSERT(false);
            break;
        }
        break;
    case Time::Day:
        switch (units) {
        case Time::Day:
            upper = (double) interval.getCount() / (double) count;
            break;
        case Time::Hour:
            upper = (double) interval.getCount() * (24.0 / (double) count);
            break;
        case Time::Minute:
            upper = (double) interval.getCount() * (1440.0 / (double) count);
            break;
        case Time::Second:
            upper = (double) interval.getCount() * (86400.0 / (double) count);
            break;
        case Time::Millisecond:
            upper = (double) interval.getCount() * (86400000.0 / (double) count);
            break;
        default:
            Q_ASSERT(false);
            break;
        }
        break;
    case Time::Hour:
        switch (units) {
        case Time::Hour:
            upper = (double) interval.getCount() / (double) count;
            break;
        case Time::Minute:
            upper = (double) interval.getCount() * (60.0 / (double) count);
            break;
        case Time::Second:
            upper = (double) interval.getCount() * (3600.0 / (double) count);
            break;
        case Time::Millisecond:
            upper = (double) interval.getCount() * (3600000.0 / (double) count);
            break;
        default:
            Q_ASSERT(false);
            break;
        }
        break;
    case Time::Minute:
        switch (units) {
        case Time::Minute:
            upper = (double) interval.getCount() / (double) count;
            break;
        case Time::Second:
            upper = (double) interval.getCount() * (60.0 / (double) count);
            break;
        case Time::Millisecond:
            upper = (double) interval.getCount() * (60000.0 / (double) count);
            break;
        default:
            Q_ASSERT(false);
            break;
        }
        break;
    case Time::Second:
        switch (units) {
        case Time::Second:
            upper = (double) interval.getCount() / (double) count;
            break;
        case Time::Millisecond:
            upper = (double) interval.getCount() * (1000.0 / (double) count);
            break;
        default:
            Q_ASSERT(false);
            break;
        }
        break;
    case Time::Millisecond:
        Q_ASSERT(units == Time::Millisecond);
        upper = (double) interval.getCount() / (double) count;
        break;
    }

    Q_ASSERT(upper > 0.0);
    Q_ASSERT(upper > lower);
}


AxisTickGeneratorTimeCycle::AxisTickGeneratorTimeCycle(QObject *parent) : AxisTickGenerator(parent)
{ }

AxisTickGeneratorTimeCycle::AxisTickGeneratorTimeCycle(const TimeCycleInterval &setInterval,
                                                       const TimeCycleDivision &setDivision,
                                                       QObject *parent) : AxisTickGenerator(parent),
                                                                          interval(setInterval),
                                                                          division(setDivision)
{ }

AxisTickGeneratorTimeCycle::~AxisTickGeneratorTimeCycle()
{ }

static QString translateMonth(int idx)
{
    switch (idx) {
    case 0:
        return AxisTickGeneratorTimeCycle::tr("JAN");
    case 1:
        return AxisTickGeneratorTimeCycle::tr("FEB");
    case 2:
        return AxisTickGeneratorTimeCycle::tr("MAR");
    case 3:
        return AxisTickGeneratorTimeCycle::tr("APR");
    case 4:
        return AxisTickGeneratorTimeCycle::tr("MAY");
    case 5:
        return AxisTickGeneratorTimeCycle::tr("JUN");
    case 6:
        return AxisTickGeneratorTimeCycle::tr("JUL");
    case 7:
        return AxisTickGeneratorTimeCycle::tr("AUG");
    case 8:
        return AxisTickGeneratorTimeCycle::tr("SEP");
    case 9:
        return AxisTickGeneratorTimeCycle::tr("OCT");
    case 10:
        return AxisTickGeneratorTimeCycle::tr("NOV");
    case 11:
        return AxisTickGeneratorTimeCycle::tr("DEC");
    default:
        break;
    }
    return QString();
}

static QString translateWeekday(int idx)
{
    switch (idx) {
    case 0:
        return AxisTickGeneratorTimeCycle::tr("MON");
    case 1:
        return AxisTickGeneratorTimeCycle::tr("TUE");
    case 2:
        return AxisTickGeneratorTimeCycle::tr("WED");
    case 3:
        return AxisTickGeneratorTimeCycle::tr("THU");
    case 4:
        return AxisTickGeneratorTimeCycle::tr("FRI");
    case 5:
        return AxisTickGeneratorTimeCycle::tr("SAT");
    case 6:
        return AxisTickGeneratorTimeCycle::tr("SUN");
    default:
        break;
    }
    return QString();
}

static QString translateQuarter(int idx)
{
    switch (idx) {
    case 0:
        return AxisTickGeneratorTimeCycle::tr("Q1");
    case 1:
        return AxisTickGeneratorTimeCycle::tr("Q2");
    case 2:
        return AxisTickGeneratorTimeCycle::tr("Q3");
    case 3:
        return AxisTickGeneratorTimeCycle::tr("Q4");
    default:
        break;
    }
    return QString();
}

static std::vector<AxisTickGenerator::Tick> generateNamed(double min,
                                                          double max,
                                                          int flags,
                                                          double boundMin,
                                                          double boundMax,
                                                          QString (*translate)(int),
                                                          int advance = -1)
{
    if (boundMin <= (double) INT_MIN ||
            boundMax >= (double) INT_MAX ||
            boundMax - boundMin > 65535.0)
        return {};
    std::vector<AxisTickGenerator::Tick> result;
    int start = (int) floor(qMax(min, boundMin));
    int end = (int) ceil(qMin(max, boundMax));
    if (advance <= 0) {
        advance = 1;
        if (end - start >= 8)
            advance = 2;
    }

    double epsilon = qMax(fabs(boundMin), fabs(boundMax)) * 1E-6;

    for (int idx = start; idx <= end; idx += advance) {
        double position = (double) idx;

        if (position < (min - epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeBeforeMin))
                continue;
        } else if (position <= (min + epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeMin))
                continue;
            position = min;
        }

        if (position > (max + epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeAfterMax))
                break;
        } else if (position >= (max - epsilon)) {
            if (!(flags & AxisTickGenerator::IncludeMax))
                continue;
            position = max;
        }

        AxisTickGenerator::Tick add;
        add.point = position;
        if (translate != NULL)
            add.label = (*translate)(idx);
        else
            add.label = QString::number(idx);

        result.emplace_back(std::move(add));
    }

    return result;
}


std::vector<AxisTickGenerator::Tick> AxisTickGeneratorTimeCycle::generate(double min,
                                                                          double max,
                                                                          int flags) const
{
    Q_ASSERT(FP::defined(min) && FP::defined(max));
    if (max <= min)
        return {};

    if (!(flags & AxisTickGenerator::FirstLevelHandling))
        return decimal.generate(min, max, flags);

    double boundLower = 0;
    double boundUpper = -1;
    division.getTotalBounds(boundLower, boundUpper, interval);

    if (min < boundLower) {
        min = boundLower;
        if (min >= max)
            return {};
    }
    if (max > boundUpper) {
        max = boundUpper;
        if (min >= max)
            return {};
    }

    switch (interval.getUnits()) {
    case Time::Year:
        if (interval.getCount() != 1 || !interval.getAlign() || division.getCount() != 1)
            break;
        if (max - min < 2.0)
            break;
        switch (division.getUnits()) {
        case Time::Quarter:
            return generateNamed(min, max, flags, boundLower, boundUpper, translateQuarter);
        case Time::Month:
            return generateNamed(min, max, flags, boundLower, boundUpper, translateMonth);
        default:
            break;
        }
        break;
    case Time::Quarter:
        break;
    case Time::Month:
        break;
    case Time::Week:
        if (interval.getCount() != 1 || !interval.getAlign() || division.getCount() != 1)
            break;
        if (max - min < 2.0)
            break;
        switch (division.getUnits()) {
        case Time::Day:
            return generateNamed(min, max, flags, boundLower, boundUpper, translateWeekday);
        default:
            break;
        }
        break;
    case Time::Day:
        if (interval.getCount() != 1 || !interval.getAlign() || division.getCount() != 1)
            break;
        switch (division.getUnits()) {
        case Time::Hour:
            if (max - min < 23.9)
                break;
            return generateNamed(min, max, flags, boundLower, boundUpper, NULL, 3);
        default:
            break;
        }
        break;
    case Time::Hour:
    case Time::Minute:
    case Time::Second:
    case Time::Millisecond:
        break;
    }

    return decimal.generate(min, max, flags);
}

/**
 * Get the tick with labels set for the total index with the given
 * divisions.
 * 
 * @param interval  the axis interval
 * @param division  the axis division
 * @return          a tick with label set (but no position)
 */
AxisTickGenerator::Tick AxisTickGeneratorTimeCycle::getTotalTick(const TimeCycleInterval &interval,
                                                                 const TimeCycleDivision &division)
{
    Q_UNUSED(division);

    AxisTickGenerator::Tick result;
    result.label = tr("ALL", "total tick label");

    switch (interval.getUnits()) {
    case Time::Year:
        if (interval.getCount() != 1)
            break;
        result.label = tr("ANN", "total tick label");
        break;
    default:
        break;
    }

    return result;
}

/**
 * Get title for a the interval and division combination.
 * 
 * @param interval  the axis interval
 * @param division  the axis division
 * @param type      the type of title
 * @return          the axis title
 */
QString AxisTickGeneratorTimeCycle::getTitle(const TimeCycleInterval &interval,
                                             const TimeCycleDivision &division,
                                             TitleType type)
{

    if (interval.getCount() == 1 && interval.getAlign()) {
        switch (interval.getUnits()) {
        case Time::Year:
            if (division.getCount() == 1) {
                switch (division.getUnits()) {
                case Time::Year:
                    switch (type) {
                    case Title_Axis:
                        return tr("Fractional Year");
                    default:
                        break;
                    }
                    break;
                case Time::Quarter:
                    switch (type) {
                    case Title_Axis:
                        return QString();
                    default:
                        break;
                    }
                    break;
                case Time::Month:
                    switch (type) {
                    case Title_Axis:
                        return QString();
                    default:
                        break;
                    }
                    break;
                case Time::Week:
                    switch (type) {
                    case Title_Axis:
                        return tr("Week of Year");
                    default:
                        break;
                    }
                    break;
                case Time::Day:
                    switch (type) {
                    case Title_Axis:
                        return tr("Day of Year");
                    case Title_Units:
                        return tr("DOY");
                    default:
                        break;
                    }
                    break;
                case Time::Hour:
                    switch (type) {
                    case Title_Axis:
                        return tr("Hours after year start");
                    default:
                        break;
                    }
                    break;
                case Time::Minute:
                    switch (type) {
                    case Title_Axis:
                        return tr("Minutes after year start");
                    default:
                        break;
                    }
                    break;
                case Time::Second:
                    switch (type) {
                    case Title_Axis:
                        return tr("Seconds after year start");
                    default:
                        break;
                    }
                    break;
                case Time::Millisecond:
                    switch (type) {
                    case Title_Axis:
                        return tr("Milliseconds after year start");
                    default:
                        break;
                    }
                    break;
                }
            } else {
                switch (division.getUnits()) {
                case Time::Quarter:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n quarter intervals after year start", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Month:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n month intervals after year start", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Week:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n week intervals after year start", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Day:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n day intervals after year start", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Hour:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n hour intervals after year start", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Minute:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n minute intervals after year start", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Second:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n second intervals after year start", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Millisecond:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n millisecond intervals after year start", "",
                                  division.getCount());
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
            }
            break;
        case Time::Quarter:
            if (division.getCount() == 1) {
                switch (division.getUnits()) {
                case Time::Quarter:
                    switch (type) {
                    case Title_Axis:
                        return tr("Fractional Quarter");
                    default:
                        break;
                    }
                    break;
                case Time::Month:
                    switch (type) {
                    case Title_Axis:
                        return tr("Months after quarter start");
                    default:
                        break;
                    }
                    break;
                case Time::Week:
                    switch (type) {
                    case Title_Axis:
                        return tr("Weeks after quarter start");
                    default:
                        break;
                    }
                    break;
                case Time::Day:
                    switch (type) {
                    case Title_Axis:
                        return tr("Days after quarter start");
                    default:
                        break;
                    }
                    break;
                case Time::Hour:
                    switch (type) {
                    case Title_Axis:
                        return tr("Hours after quarter start");
                    default:
                        break;
                    }
                    break;
                case Time::Minute:
                    switch (type) {
                    case Title_Axis:
                        return tr("Minutes after quarter start");
                    default:
                        break;
                    }
                    break;
                case Time::Second:
                    switch (type) {
                    case Title_Axis:
                        return tr("Seconds after quarter start");
                    default:
                        break;
                    }
                    break;
                case Time::Millisecond:
                    switch (type) {
                    case Title_Axis:
                        return tr("Milliseconds after quarter start");
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
            } else {
                switch (division.getUnits()) {
                case Time::Month:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n month intervals after quarter start", "",
                                  division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Week:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n week intervals after quarter start", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Day:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n day intervals after quarter start", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Hour:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n hour intervals after quarter start", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Minute:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n minute intervals after quarter start", "",
                                  division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Second:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n second intervals after quarter start", "",
                                  division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Millisecond:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n millisecond intervals after quarter start", "",
                                  division.getCount());
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
            }
            break;
        case Time::Month:
            if (division.getCount() == 1) {
                switch (division.getUnits()) {
                case Time::Month:
                    switch (type) {
                    case Title_Axis:
                        return tr("Fractional Month");
                    default:
                        break;
                    }
                    break;
                case Time::Week:
                    switch (type) {
                    case Title_Axis:
                        return tr("Weeks after months start");
                    default:
                        break;
                    }
                    break;
                case Time::Day:
                    switch (type) {
                    case Title_Axis:
                        return tr("Day of Month");
                    case Title_Units:
                        return tr("DOM");
                    default:
                        break;
                    }
                    break;
                case Time::Hour:
                    switch (type) {
                    case Title_Axis:
                        return tr("Hours after month start");
                    default:
                        break;
                    }
                    break;
                case Time::Minute:
                    switch (type) {
                    case Title_Axis:
                        return tr("Minutes after month start");
                    default:
                        break;
                    }
                    break;
                case Time::Second:
                    switch (type) {
                    case Title_Axis:
                        return tr("Seconds after month start");
                    default:
                        break;
                    }
                    break;
                case Time::Millisecond:
                    switch (type) {
                    case Title_Axis:
                        return tr("Milliseconds after month start");
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
            } else {
                switch (division.getUnits()) {
                case Time::Week:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n week intervals after month start", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Day:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n day intervals after month start", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Hour:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n hour intervals after month start", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Minute:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n minute intervals after month start", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Second:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n second intervals after month start", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Millisecond:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n millisecond intervals after month start", "",
                                  division.getCount());
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
            }
            break;
        case Time::Week:
            if (division.getCount() == 1) {
                switch (division.getUnits()) {
                case Time::Week:
                    switch (type) {
                    case Title_Axis:
                        return tr("Fractional Week");
                    default:
                        break;
                    }
                    break;
                case Time::Day:
                    switch (type) {
                    case Title_Axis:
                        return QString();
                    case Title_Units:
                        return tr("DOW (0Z MON=1.0)");
                    default:
                        break;
                    }
                    break;
                case Time::Hour:
                    switch (type) {
                    case Title_Axis:
                        return tr("Hours after midnight UTC Monday");
                    default:
                        break;
                    }
                    break;
                case Time::Minute:
                    switch (type) {
                    case Title_Axis:
                        return tr("Minutes after midnight UTC Monday");
                    default:
                        break;
                    }
                    break;
                case Time::Second:
                    switch (type) {
                    case Title_Axis:
                        return tr("Seconds after midnight UTC Monday");
                    default:
                        break;
                    }
                    break;
                case Time::Millisecond:
                    switch (type) {
                    case Title_Axis:
                        return tr("Milliseconds after midnight UTC Monday");
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
            } else {
                switch (division.getUnits()) {
                case Time::Day:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n day intervals after Monday", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Hour:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n hour intervals after midnight UTC Monday", "",
                                  division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Minute:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n minute intervals after midnight UTC Monday", "",
                                  division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Second:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n second intervals after midnight UTC Monday", "",
                                  division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Millisecond:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n millisecond intervals after midnight UTC Monday", "",
                                  division.getCount());
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
            }
            break;
        case Time::Day:
            if (division.getCount() == 1) {
                switch (division.getUnits()) {
                case Time::Day:
                    switch (type) {
                    case Title_Axis:
                        return tr("Hours after midnight UTC");
                    default:
                        break;
                    }
                    break;
                case Time::Hour:
                    switch (type) {
                    case Title_Axis:
                        return tr("Hours after midnight UTC");
                    default:
                        break;
                    }
                    break;
                case Time::Minute:
                    switch (type) {
                    case Title_Axis:
                        return tr("Minutes after midnight UTC");
                    default:
                        break;
                    }
                    break;
                case Time::Second:
                    switch (type) {
                    case Title_Axis:
                        return tr("Seconds after midnight UTC");
                    default:
                        break;
                    }
                    break;
                case Time::Millisecond:
                    switch (type) {
                    case Title_Axis:
                        return tr("Milliseconds after midnight UTC");
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
            } else {
                switch (division.getUnits()) {
                case Time::Hour:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n hour intervals after midnight UTC", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Minute:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n minute intervals after midnight UTC", "",
                                  division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Second:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n second intervals after midnight UTC", "",
                                  division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Millisecond:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n millisecond intervals after midnight UTC", "",
                                  division.getCount());
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
            }
            break;
        case Time::Hour:
            if (division.getCount() == 1) {
                switch (division.getUnits()) {
                case Time::Hour:
                    switch (type) {
                    case Title_Axis:
                        return tr("Fractional Hour");
                    default:
                        break;
                    }
                    break;
                case Time::Minute:
                    switch (type) {
                    case Title_Axis:
                        return tr("Minutes after the hour");
                    default:
                        break;
                    }
                    break;
                case Time::Second:
                    switch (type) {
                    case Title_Axis:
                        return tr("Seconds after the hour");
                    default:
                        break;
                    }
                    break;
                case Time::Millisecond:
                    switch (type) {
                    case Title_Axis:
                        return tr("Milliseconds after the hour");
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
            } else {
                switch (division.getUnits()) {
                case Time::Minute:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n minute intervals after the hour", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Second:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n second intervals after the hour", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Millisecond:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n millisecond intervals after the hour", "",
                                  division.getCount());
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
            }
            break;
        case Time::Minute:
            if (division.getCount() == 1) {
                switch (division.getUnits()) {
                case Time::Minute:
                    switch (type) {
                    case Title_Axis:
                        return tr("Fractional Minute");
                    default:
                        break;
                    }
                    break;
                case Time::Second:
                    switch (type) {
                    case Title_Axis:
                        return tr("Seconds after the minute");
                    default:
                        break;
                    }
                    break;
                case Time::Millisecond:
                    switch (type) {
                    case Title_Axis:
                        return tr("Milliseconds after the minute");
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
            } else {
                switch (division.getUnits()) {
                case Time::Second:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n second intervals after the minute", "", division.getCount());
                    default:
                        break;
                    }
                    break;
                case Time::Millisecond:
                    switch (type) {
                    case Title_Axis:
                        return tr("%n millisecond intervals after the minute", "",
                                  division.getCount());
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
            }
            break;
        case Time::Second:
            if (division.getCount() == 1) {
                switch (division.getUnits()) {
                case Time::Second:
                    switch (type) {
                    case Title_Axis:
                        return tr("Fractional Second");
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
            }
            break;
        case Time::Millisecond:
            if (division.getCount() == 1) {
                switch (division.getUnits()) {
                case Time::Second:
                    switch (type) {
                    case Title_Axis:
                        return tr("Fractional Millisecond");
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
            }
            break;
        }
    }


    if (division.getCount() == 1) {
        switch (division.getUnits()) {
        case Time::Year:
            switch (type) {
            case Title_Axis:
                return tr("Years after start");
            default:
                return tr("Years", "year default title");
            }
            break;
        case Time::Quarter:
            switch (type) {
            case Title_Axis:
                return tr("Quarters after start");
            default:
                return tr("Quarters", "quarter default title");
            }
            break;
        case Time::Month:
            switch (type) {
            case Title_Axis:
                return tr("Months after start");
            default:
                return tr("Months", "month default title");
            }
            break;
        case Time::Week:
            switch (type) {
            case Title_Axis:
                return tr("Weeks after start");
            default:
                return tr("Weeks", "week default title");
            }
            break;
        case Time::Day:
            switch (type) {
            case Title_Axis:
                return tr("Days after start");
            default:
                return tr("Days", "day default title");
            }
            break;
        case Time::Hour:
            switch (type) {
            case Title_Axis:
                return tr("Hours after start");
            default:
                return tr("Hours", "hour default title");
            }
            break;
        case Time::Minute:
            switch (type) {
            case Title_Axis:
                return tr("Minutes after start");
            default:
                return tr("Minutes", "minute default title");
            }
            break;
        case Time::Second:
            switch (type) {
            case Title_Axis:
                return tr("Seconds after start");
            default:
                return tr("Seconds", "second default title");
            }
            break;
        case Time::Millisecond:
            switch (type) {
            case Title_Axis:
                return tr("Milliseconds after start");
            default:
                return tr("Milliseconds", "millisecond default title");
            }
            break;
        }
    } else {
        switch (division.getUnits()) {
        case Time::Year:
            switch (type) {
            case Title_Axis:
                return tr("%n year intervals after start", "year division multiple",
                          division.getCount());
            default:
                return tr("Years\xC3\x97%n", "year division multiple", division.getCount());
            }
            break;
        case Time::Quarter:
            switch (type) {
            case Title_Axis:
                return tr("%n quarter intervals after start", "quarter division multiple",
                          division.getCount());
            default:
                return tr("Quarters\xC3\x97%n", "quarter division multiple", division.getCount());
            }
            break;
        case Time::Month:
            switch (type) {
            case Title_Axis:
                return tr("%n month intervals after start", "month division multiple",
                          division.getCount());
            default:
                return tr("Months\xC3\x97%n", "month division multiple", division.getCount());
            }
            break;
        case Time::Week:
            switch (type) {
            case Title_Axis:
                return tr("%n week intervals after start", "week division multiple",
                          division.getCount());
            default:
                return tr("Week\xC3\x97%n", "week division multiple", division.getCount());
            }
            break;
        case Time::Day:
            switch (type) {
            case Title_Axis:
                return tr("%n day intervals after start", "day division multiple",
                          division.getCount());
            default:
                return tr("Days\xC3\x97%n", "day division multiple", division.getCount());
            }
            break;
        case Time::Hour:
            switch (type) {
            case Title_Axis:
                return tr("%n hour intervals after start", "hour division multiple",
                          division.getCount());
            default:
                return tr("Hours\xC3\x97%n", "hour division multiple", division.getCount());
            }
            break;
        case Time::Minute:
            switch (type) {
            case Title_Axis:
                return tr("%n minute intervals after start", "minute division multiple",
                          division.getCount());
            default:
                return tr("Minutes\xC3\x97%n", "minute division multiple", division.getCount());
            }
            break;
        case Time::Second:
            switch (type) {
            case Title_Axis:
                return tr("%n second intervals after start", "second division multiple",
                          division.getCount());
            default:
                return tr("Seconds\xC3\x97%n", "second division multiple", division.getCount());
            }
            break;
        case Time::Millisecond:
            switch (type) {
            case Title_Axis:
                return tr("%n milliseconds intervals after start", "millisecond division multiple",
                          division.getCount());
            default:
                return tr("Milliseconds\xC3\x97%n", "millisecond division multiple",
                          division.getCount());
            }
            break;
        }
    }

    return QString();
}

}
}
