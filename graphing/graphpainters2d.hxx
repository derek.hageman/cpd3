/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGGRAPHPAINTERS2D_H
#define CPD3GRAPHINGGRAPHPAINTERS2D_H

#include "core/first.hxx"

#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QColor>
#include <QPen>

#include "graphing/graphing.hxx"
#include "graphing/display.hxx"
#include "graphing/traceset.hxx"
#include "graphing/binset.hxx"
#include "graphing/indicator.hxx"
#include "graphing/tracecommon.hxx"
#include "graphing/axistransformer.hxx"
#include "graphing/axiscommon.hxx"
#include "graphing/graphcommon2d.hxx"
#include "graphing/bin2d.hxx"
#include "algorithms/model.hxx"


namespace CPD3 {
namespace Graphing {

template<std::size_t N>
struct Graph2DSimpleDrawPoint {
    std::array<double, N> d;

    Graph2DSimpleDrawPoint() = default;

    inline Graph2DSimpleDrawPoint(const TracePoint<N> &other) : d(other.d)
    { }

    inline Graph2DSimpleDrawPoint &operator=(const TracePoint<N> &other)
    {
        d = other.d;
        return *this;
    }

    inline Graph2DSimpleDrawPoint(const TracePoint<N + 1> &other)
    { for (std::size_t i = 0; i < N; i++) { d[i] = other.d[i]; }}

    inline void clear()
    { d.fill(FP::undefined()); }
};

/**
 * A painter that draws lines between a set of points.  This implementation
 * does not consider the "Z" (color) of the points.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DLines : public Graph2DDrawPrimitive {
    std::vector<TracePoint<3>> points;
    AxisTransformer tX;
    AxisTransformer tY;
    qreal lineWidth;
    Qt::PenStyle style;
    QColor color;
    TraceSymbol symbol;
    double continuity;
    bool skipUndefined;
    quintptr tracePtr;
public:
    GraphPainter2DLines(std::vector<TracePoint<3>> setPoints,
                        const AxisTransformer &setX,
                        const AxisTransformer &setY,
                        qreal setLineWidth,
                        Qt::PenStyle setStyle,
                        const QColor &setColor,
                        const TraceSymbol &setIsolatedPointSymbol,
                        double setContinuity = 1.0,
                        bool setSkipUndefined = false,
                        quintptr setTracePtr = 0,
                        int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DLines();

    virtual void paint(QPainter *painter);

    virtual void updateMouseover(const QPointF &point,
                                 std::shared_ptr<Graph2DMouseoverComponent> &output);

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output);
};

/**
 * A painter that draws lines between a set of points.  This implementation
 * uses a gradient to color the "Z" axis of the points.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DLinesGradient : public Graph2DDrawPrimitive {
    std::vector<TracePoint<3>> points;
    AxisTransformer tX;
    AxisTransformer tY;
    AxisTransformer tZ;
    qreal lineWidth;
    Qt::PenStyle style;
    TraceGradient gradient;
    TraceSymbol symbol;
    double continuity;
    bool skipUndefined;
    quintptr tracePtr;
public:
    GraphPainter2DLinesGradient(std::vector<TracePoint<3>> setPoints,
                                const AxisTransformer &setX,
                                const AxisTransformer &setY,
                                const AxisTransformer &setZ,
                                qreal setLineWidth,
                                Qt::PenStyle setStyle,
                                const TraceGradient &setGradient,
                                const TraceSymbol &setIsolatedPointSymbol,
                                double setContinuity = 1.0,
                                bool setSkipUndefined = false,
                                quintptr setTracePtr = 0,
                                int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DLinesGradient();

    virtual void paint(QPainter *painter);

    virtual void updateMouseover(const QPointF &point,
                                 std::shared_ptr<Graph2DMouseoverComponent> &output);

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output);
};

/**
 * A painter that draws symbols at points.  This implementation
 * does not consider the "Z" (color) of the points.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DPoints : public Graph2DDrawPrimitive {
    std::vector<TracePoint<3>> points;
    AxisTransformer tX;
    AxisTransformer tY;
    TraceSymbol symbol;
    QColor color;
    quintptr tracePtr;
public:
    GraphPainter2DPoints(std::vector<TracePoint<3>> setPoints,
                         const AxisTransformer &setX,
                         const AxisTransformer &setY,
                         const TraceSymbol &setSymbol,
                         const QColor &setColor,
                         quintptr setTracePtr = 0,
                         int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DPoints();

    virtual void paint(QPainter *painter);

    virtual void updateMouseover(const QPointF &point,
                                 std::shared_ptr<Graph2DMouseoverComponent> &output);

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output);
};

/**
 * A painter that draws symbols at points.  This implementation
 * uses a gradient to color the "Z" axis of the points.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DPointsGradient : public Graph2DDrawPrimitive {
    std::vector<TracePoint<3>> points;
    AxisTransformer tX;
    AxisTransformer tY;
    AxisTransformer tZ;
    TraceSymbol symbol;
    TraceGradient gradient;
    quintptr tracePtr;
public:
    GraphPainter2DPointsGradient(std::vector<TracePoint<3>> setPoints,
                                 const AxisTransformer &setX,
                                 const AxisTransformer &setY,
                                 const AxisTransformer &setZ,
                                 const TraceSymbol &setSymbol,
                                 const TraceGradient &setGradient,
                                 quintptr setTracePtr = 0,
                                 int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DPointsGradient();

    virtual void paint(QPainter *painter);

    virtual void updateMouseover(const QPointF &point,
                                 std::shared_ptr<Graph2DMouseoverComponent> &output);

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output);
};

/**
 * A painter that draws a line for a model, with the X axis being the
 * independent variable.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DFitX : public Graph2DDrawPrimitive {
    std::shared_ptr<Algorithms::Model> model;
    AxisTransformer tX;
    AxisTransformer tY;
    qreal lineWidth;
    Qt::PenStyle style;
    QColor color;
    double scale;
    quintptr tracePtr;

    std::vector<Graph2DSimpleDrawPoint<4>> generatePoints();

public:
    GraphPainter2DFitX(std::shared_ptr<Algorithms::Model> setModel,
                       const AxisTransformer &setX,
                       const AxisTransformer &setY,
                       qreal setLineWidth,
                       Qt::PenStyle setStyle,
                       const QColor &setColor,
                       double setScale,
                       quintptr setTracePtr = 0,
                       int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DFitX();

    virtual void paint(QPainter *painter);

    virtual void updateMouseover(const QPointF &point,
                                 std::shared_ptr<Graph2DMouseoverComponent> &output);

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output);
};

/**
 * A painter that draws a line for a model, with the Y axis being the
 * independent variable.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DFitY : public Graph2DDrawPrimitive {
    std::shared_ptr<Algorithms::Model> model;
    AxisTransformer tX;
    AxisTransformer tY;
    qreal lineWidth;
    Qt::PenStyle style;
    QColor color;
    double scale;
    quintptr tracePtr;

    std::vector<Graph2DSimpleDrawPoint<4>> generatePoints();

public:
    GraphPainter2DFitY(std::shared_ptr<Algorithms::Model> setModel,
                       const AxisTransformer &setX,
                       const AxisTransformer &setY,
                       qreal setLineWidth,
                       Qt::PenStyle setStyle,
                       const QColor &setColor,
                       double setScale,
                       quintptr tracePtr = 0,
                       int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DFitY();

    virtual void paint(QPainter *painter);

    virtual void updateMouseover(const QPointF &point,
                                 std::shared_ptr<Graph2DMouseoverComponent> &output);

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output);
};

/**
 * A painter that draws a line for a model, with a virtual "I" axis being the
 * independent variable.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DFitI : public Graph2DDrawPrimitive {
    std::shared_ptr<Algorithms::Model> model;
    AxisTransformer tX;
    AxisTransformer tY;
    qreal lineWidth;
    Qt::PenStyle style;
    QColor color;
    double minI;
    double maxI;
    quintptr tracePtr;

    std::vector<Graph2DSimpleDrawPoint<5>> generatePoints();

public:
    GraphPainter2DFitI(std::shared_ptr<Algorithms::Model> setModel,
                       const AxisTransformer &setX,
                       const AxisTransformer &setY,
                       qreal setLineWidth,
                       Qt::PenStyle setStyle,
                       const QColor &setColor,
                       double setMinI,
                       double setMaxI,
                       quintptr tracePtr = 0,
                       int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DFitI();

    virtual void paint(QPainter *painter);

    virtual void updateMouseover(const QPointF &point,
                                 std::shared_ptr<Graph2DMouseoverComponent> &output);

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output);
};


/**
 * A painter that draws a line for a model, with the X axis being the
 * independent variable.  This implementation uses a gradient to color the "Z" 
 * axis of the fit.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DFitXGradient : public Graph2DDrawPrimitive {
    std::shared_ptr<Algorithms::Model> model;
    AxisTransformer tX;
    AxisTransformer tY;
    AxisTransformer tZ;
    qreal lineWidth;
    Qt::PenStyle style;
    TraceGradient gradient;
    double scale;
    quintptr tracePtr;

    std::vector<Graph2DSimpleDrawPoint<6> > generatePoints();

public:
    GraphPainter2DFitXGradient(std::shared_ptr<Algorithms::Model> setModel,
                               const AxisTransformer &setX,
                               const AxisTransformer &setY,
                               const AxisTransformer &setZ,
                               qreal setLineWidth,
                               Qt::PenStyle setStyle,
                               const TraceGradient &setGradient,
                               double setScale,
                               quintptr setTracePtr = 0,
                               int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DFitXGradient();

    virtual void paint(QPainter *painter);

    virtual void updateMouseover(const QPointF &point,
                                 std::shared_ptr<Graph2DMouseoverComponent> &output);

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output);
};

/**
 * A painter that draws a line for a model, with the Y axis being the
 * independent variable.  This implementation uses a gradient to color the "Z" 
 * axis of the points.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DFitYGradient : public Graph2DDrawPrimitive {
    std::shared_ptr<Algorithms::Model> model;
    AxisTransformer tX;
    AxisTransformer tY;
    AxisTransformer tZ;
    qreal lineWidth;
    Qt::PenStyle style;
    TraceGradient gradient;
    double scale;
    quintptr tracePtr;

    std::vector<Graph2DSimpleDrawPoint<6> > generatePoints();

public:
    GraphPainter2DFitYGradient(std::shared_ptr<Algorithms::Model> setModel,
                               const AxisTransformer &setX,
                               const AxisTransformer &setY,
                               const AxisTransformer &setZ,
                               qreal setLineWidth,
                               Qt::PenStyle setStyle,
                               const TraceGradient &setGradient,
                               double setScale,
                               quintptr setTracePtr = 0,
                               int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DFitYGradient();

    virtual void paint(QPainter *painter);

    virtual void updateMouseover(const QPointF &point,
                                 std::shared_ptr<Graph2DMouseoverComponent> &output);

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output);
};

/**
 * A painter that draws a line for a model, with a virtual "I" axis being the
 * independent variable.  This implementation uses a gradient to color the "Z" 
 * axis of the points.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DFitIGradient : public Graph2DDrawPrimitive {
    std::shared_ptr<Algorithms::Model> model;
    AxisTransformer tX;
    AxisTransformer tY;
    AxisTransformer tZ;
    qreal lineWidth;
    Qt::PenStyle style;
    TraceGradient gradient;
    double minI;
    double maxI;
    quintptr tracePtr;

    std::vector<Graph2DSimpleDrawPoint<7> > generatePoints();

public:
    GraphPainter2DFitIGradient(std::shared_ptr<Algorithms::Model> setModel,
                               const AxisTransformer &setX,
                               const AxisTransformer &setY,
                               const AxisTransformer &setZ,
                               qreal setLineWidth,
                               Qt::PenStyle setStyle,
                               const TraceGradient &setGradient,
                               double setMinI,
                               double setMaxI,
                               quintptr setTracePtr = 0,
                               int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DFitIGradient();

    virtual void paint(QPainter *painter);

    virtual void updateMouseover(const QPointF &point,
                                 std::shared_ptr<Graph2DMouseoverComponent> &output);

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output);
};

/**
 * A painter that fills the area with the evaluation of a model at all points
 * in it.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DFillModelScan : public Graph2DDrawPrimitive {
    std::shared_ptr<Algorithms::Model> model;
    AxisTransformer tX;
    AxisTransformer tY;
    AxisTransformer tZ;
    TraceGradient gradient;
    quintptr tracePtr;
    bool hadZRange;
public:
    GraphPainter2DFillModelScan(std::shared_ptr<Algorithms::Model> setModel,
                                const AxisTransformer &setX,
                                const AxisTransformer &setY,
                                const AxisTransformer &setZ,
                                const TraceGradient &setGradient,
                                quintptr setTracePtr = 0,
                                int priority = Graph2DDrawPrimitive::Priority_Default);

    GraphPainter2DFillModelScan(std::shared_ptr<Algorithms::Model> setModel,
                                const AxisTransformer &setX,
                                const AxisTransformer &setY,
                                const TraceGradient &setGradient,
                                quintptr setTracePtr = 0,
                                int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DFillModelScan();

    virtual void paint(QPainter *painter);

    virtual void updateMouseover(const QPointF &point,
                                 std::shared_ptr<Graph2DMouseoverComponent> &output);

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output);
};

/**
 * A painter that takes the input points and forms them into a grid then
 * does bilinear interpolation on that grid.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DFillBilinearGrid : public Graph2DDrawPrimitive {
    std::vector<TracePoint<3>> points;
    AxisTransformer tX;
    AxisTransformer tY;
    AxisTransformer tZ;
    TraceGradient gradient;
    quintptr tracePtr;

    std::vector<Graph2DSimpleDrawPoint<3>> gridPoints(int &pitch);

public:
    GraphPainter2DFillBilinearGrid(std::vector<TracePoint<3>> setPoints,
                                   const AxisTransformer &setX,
                                   const AxisTransformer &setY,
                                   const AxisTransformer &setZ,
                                   const TraceGradient &setGradient,
                                   quintptr setTracePtr = 0,
                                   int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DFillBilinearGrid();

    virtual void paint(QPainter *painter);

    virtual void updateMouseover(const QPointF &point,
                                 std::shared_ptr<Graph2DMouseoverComponent> &output);

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output);
};

/**
 * A painter that draws lines to an axis for a set of points.  
 * This implementation does not consider the "Z" (color) of the points.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DFillShade : public Graph2DDrawPrimitive {
    std::vector<TracePoint<3>> points;
    AxisTransformer tX;
    AxisTransformer tY;
    Axis2DSide side;
    QColor color;
    double continuity;
    bool skipUndefined;
public:
    GraphPainter2DFillShade(std::vector<TracePoint<3>> setPoints,
                            const AxisTransformer &setX,
                            const AxisTransformer &setY,
                            Axis2DSide setSide,
                            const QColor &setColor,
                            double setContinuity = 1.0,
                            bool setSkipUndefined = false,
                            int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DFillShade();

    virtual void paint(QPainter *painter);
};

/**
 * A painter that draws lines to an axis for a set of points.  
 * This implementation does not consider the "Z" (color) of the points.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DFillShadeGradient : public Graph2DDrawPrimitive {
    std::vector<TracePoint<3>> points;
    AxisTransformer tX;
    AxisTransformer tY;
    AxisTransformer tZ;
    Axis2DSide side;
    TraceGradient gradient;
    double continuity;
    bool skipUndefined;
public:
    GraphPainter2DFillShadeGradient(std::vector<TracePoint<3>> setPoints,
                                    const AxisTransformer &setX,
                                    const AxisTransformer &setY,
                                    const AxisTransformer &setZ,
                                    Axis2DSide setSide,
                                    const TraceGradient &setGradient,
                                    double setContinuity = 1.0,
                                    bool setSkipUndefined = false,
                                    int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DFillShadeGradient();

    virtual void paint(QPainter *painter);
};

/**
 * A painter that draws a shaded area for a model, with the X axis being the
 * independent variable.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DShadeFitX : public Graph2DDrawPrimitive {
    std::shared_ptr<Algorithms::Model> model;
    AxisTransformer tX;
    AxisTransformer tY;
    Axis2DSide side;
    QColor color;
    double scale;
public:
    GraphPainter2DShadeFitX(std::shared_ptr<Algorithms::Model> setModel,
                            const AxisTransformer &setX,
                            const AxisTransformer &setY,
                            Axis2DSide setSide,
                            const QColor &setColor,
                            double setScale,
                            int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DShadeFitX();

    virtual void paint(QPainter *painter);
};

/**
 * A painter that draws a shaded area for a model, with the Y axis being the
 * independent variable.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DShadeFitY : public Graph2DDrawPrimitive {
    std::shared_ptr<Algorithms::Model> model;
    AxisTransformer tX;
    AxisTransformer tY;
    Axis2DSide side;
    QColor color;
    double scale;
public:
    GraphPainter2DShadeFitY(std::shared_ptr<Algorithms::Model> setModel,
                            const AxisTransformer &setX,
                            const AxisTransformer &setY,
                            Axis2DSide setSide,
                            const QColor &setColor,
                            double setScale,
                            int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DShadeFitY();

    virtual void paint(QPainter *painter);
};

/**
 * A painter that draws a shaded area for a model, with a virtual "I" axis 
 * being the independent variable.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DShadeFitI : public Graph2DDrawPrimitive {
    std::shared_ptr<Algorithms::Model> model;
    AxisTransformer tX;
    AxisTransformer tY;
    Axis2DSide side;
    QColor color;
    double minI;
    double maxI;
public:
    GraphPainter2DShadeFitI(std::shared_ptr<Algorithms::Model> setModel,
                            const AxisTransformer &setX,
                            const AxisTransformer &setY,
                            Axis2DSide setSide,
                            const QColor &setColor,
                            double setMinI,
                            double setMaxI,
                            int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DShadeFitI();

    virtual void paint(QPainter *painter);
};

/**
 * A painter that draws a shades area for a model, with the X axis being the
 * independent variable.  This implementation uses a gradient to color the "Z" 
 * axis of the fit.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DShadeFitXGradient : public Graph2DDrawPrimitive {
    std::shared_ptr<Algorithms::Model> model;
    AxisTransformer tX;
    AxisTransformer tY;
    AxisTransformer tZ;
    Axis2DSide side;
    TraceGradient gradient;
    double scale;
public:
    GraphPainter2DShadeFitXGradient(std::shared_ptr<Algorithms::Model> setModel,
                                    const AxisTransformer &setX,
                                    const AxisTransformer &setY,
                                    const AxisTransformer &setZ,
                                    Axis2DSide setSide,
                                    const TraceGradient &setGradient,
                                    double setScale,
                                    int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DShadeFitXGradient();

    virtual void paint(QPainter *painter);
};

/**
 * A painter that draws a shaded area for a model, with the Y axis being the
 * independent variable.  This implementation uses a gradient to color the "Z" 
 * axis of the points.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DShadeFitYGradient : public Graph2DDrawPrimitive {
    std::shared_ptr<Algorithms::Model> model;
    AxisTransformer tX;
    AxisTransformer tY;
    AxisTransformer tZ;
    Axis2DSide side;
    TraceGradient gradient;
    double scale;
public:
    GraphPainter2DShadeFitYGradient(std::shared_ptr<Algorithms::Model> setModel,
                                    const AxisTransformer &setX,
                                    const AxisTransformer &setY,
                                    const AxisTransformer &setZ,
                                    Axis2DSide setSide,
                                    const TraceGradient &setGradient,
                                    double setScale,
                                    int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DShadeFitYGradient();

    virtual void paint(QPainter *painter);
};

/**
 * A painter that draws a shaded area for a model, with a virtual "I" axis 
 * being the independent variable.  This implementation uses a gradient to 
 * color the "Z"  axis of the points.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DShadeFitIGradient : public Graph2DDrawPrimitive {
    std::shared_ptr<Algorithms::Model> model;
    AxisTransformer tX;
    AxisTransformer tY;
    AxisTransformer tZ;
    Axis2DSide side;
    TraceGradient gradient;
    double minI;
    double maxI;
public:
    GraphPainter2DShadeFitIGradient(std::shared_ptr<Algorithms::Model> setModel,
                                    const AxisTransformer &setX,
                                    const AxisTransformer &setY,
                                    const AxisTransformer &setZ,
                                    Axis2DSide setSide,
                                    const TraceGradient &setGradient,
                                    double setMinI,
                                    double setMaxI,
                                    int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DShadeFitIGradient();

    virtual void paint(QPainter *painter);
};

/**
 * A painter that draws box and whisker bins.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DBin : public Graph2DDrawPrimitive {
    std::vector<Bin2DPaintPoint> points;
    AxisTransformer tX;
    AxisTransformer tY;
    QColor fill;
    qreal whiskerLineWidth;
    Qt::PenStyle whiskerStyle;
    QColor whiskerColor;
    qreal whiskerCapWidth;
    qreal boxLineWidth;
    Qt::PenStyle boxStyle;
    QColor boxColor;
    qreal middleLineWidth;
    Qt::PenStyle middleStyle;
    QColor middleColor;
    bool vertical;
    quintptr tracePtr;

    double pointDistance(double x1,
                         double x2,
                         double y1,
                         double y2,
                         const QPointF &point,
                         double &cx,
                         double &cy);

    void checkMouseoverPoint(double x1,
                             double x2,
                             double y1,
                             double y2,
                             const Bin2DPaintPoint &binPoint,
                             Graph2DMouseoverComponentTraceBox::Location location,
                             const QPointF &point,
                             std::shared_ptr<Graph2DMouseoverComponent> &output);

public:
    GraphPainter2DBin(std::vector<Bin2DPaintPoint> setPoints,
                      const AxisTransformer &setX,
                      const AxisTransformer &setY,
                      const QColor &setFill,
                      qreal setWhiskerLineWidth,
                      Qt::PenStyle setWhiskerStyle,
                      const QColor &setWhiskerColor,
                      qreal setWhiskerCapWidth,
                      qreal setBoxLineWidth,
                      Qt::PenStyle setBoxStyle,
                      const QColor &setBoxColor,
                      qreal setMiddleLineWidth,
                      Qt::PenStyle setMiddleStyle,
                      const QColor &setMiddleColor,
                      bool setVertical = true,
                      quintptr setTracePtr = 0,
                      int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DBin();

    virtual void paint(QPainter *painter);

    virtual void updateMouseover(const QPointF &point,
                                 std::shared_ptr<Graph2DMouseoverComponent> &output);

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output);
};

/**
 * A painter that draws an indicator along the inside of the trace area.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DIndicator : public Graph2DDrawPrimitive {
    std::vector<IndicatorPoint<1> > points;
    AxisTransformer transformer;
    qreal baseline;
    qreal totalDepth;
    AxisIndicator2D indicator;
    Axis2DSide position;
    QColor color;
    double continuity;
    quintptr tracePtr;
public:
    GraphPainter2DIndicator(std::vector<IndicatorPoint<1>> setPoints,
                            const AxisTransformer &setTransformer,
                            qreal setBaseline,
                            qreal setTotalDepth,
                            const AxisIndicator2D &setIndicator,
                            const QColor &setColor,
                            double setContinuity,
                            quintptr setTracePtr = 0,
                            int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DIndicator();

    virtual void paint(QPainter *painter);

    virtual void updateMouseover(const QPointF &point,
                                 std::shared_ptr<Graph2DMouseoverComponent> &output);
};

/**
 * A painter that draws a single line.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DSimpleLine : public Graph2DDrawPrimitive {
    QPointF p1;
    QPointF p2;
    qreal lineWidth;
    Qt::PenStyle lineStyle;
    QColor color;
public:
    GraphPainter2DSimpleLine(const QPointF &s1,
                             const QPointF &s2,
                             qreal setLineWidth,
                             Qt::PenStyle setLineStyle,
                             const QColor &setColor,
                             int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DSimpleLine();

    virtual void paint(QPainter *painter);
};

/**
 * A painter that draws the title text.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DTitle : public Graph2DDrawPrimitive {
    QString text;
    QPointF top;
    QFont font;
    QColor color;
public:
    GraphPainter2DTitle(const QString &setText,
                        const QPointF &setTop,
                        const QFont &setFont,
                        const QColor &setColor,
                        int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DTitle();

    virtual void paint(QPainter *painter);

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output);
};

/**
 * A painter that draws the legend text.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DLegend : public Graph2DDrawPrimitive {
    Legend legend;
    QPointF legendOrigin;
    QRectF background;
    qreal backgroundLineWidth;
    Qt::PenStyle backgroundLineStyle;
    QColor backgroundFill;
    QColor backgroundOutline;
public:
    GraphPainter2DLegend(const Legend &setLegend,
                         const QPointF &setLegendOrigin,
                         const QRectF &setBackground,
                         qreal setBackgroundLineWidth,
                         Qt::PenStyle setBackgroundLineStyle,
                         const QColor &setBackgroundFill,
                         const QColor &setBackgroundOutline,
                         int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DLegend();

    virtual void paint(QPainter *painter);

    virtual void updateMouseover(const QPointF &point,
                                 std::shared_ptr<Graph2DMouseoverComponent> &output);

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output);
};

/**
 * A painter that draws highlight regions.
 */
class CPD3GRAPHING_EXPORT GraphPainter2DHighlight : public Graph2DDrawPrimitive {
public:
    class CPD3GRAPHING_EXPORT Region {
        AxisTransformer tX;
        AxisTransformer tY;
        double minX;
        double maxX;
        double minY;
        double maxY;
        QColor color;

        friend class GraphPainter2DHighlight;

    public:
        Region();

        Region(const Region &other);

        Region &operator=(const Region &other);

        Region(Region &&other);

        Region &operator=(Region &&other);

        Region(const AxisTransformer &x,
               const AxisTransformer &y,
               double setMinX,
               double setMaxX,
               double setMinY,
               double setMaxY,
               const QColor &setColor);
    };

private:
    std::vector<Region> regions;
    QColor fill;
public:
    GraphPainter2DHighlight(std::vector<Region> setRegions,
                            int priority = Graph2DDrawPrimitive::Priority_Default);

    virtual ~GraphPainter2DHighlight();

    void paint(QPainter *painter) override;
};

}
}


#endif
