/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGCYCLEPLOT2D_H
#define CPD3GRAPHINGCYCLEPLOT2D_H

#include "core/first.hxx"

#include <vector>
#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QList>

#include "graphing/graphing.hxx"
#include "graphing/graph2d.hxx"
#include "graphing/cyclecommon.hxx"

namespace CPD3 {
namespace Graphing {

class CyclePlot2D;

/**
 * The parameters for a 2D cycle plot graph.
 */
class CPD3GRAPHING_EXPORT CyclePlot2DParameters : public Graph2DParameters {
    enum {
        Component_Interval = 0x0001, Component_Division = 0x0002,
    };

    int components;

    TimeCycleInterval interval;
    TimeCycleDivision division;
public:
    CyclePlot2DParameters();

    virtual ~CyclePlot2DParameters();

    CyclePlot2DParameters(const CyclePlot2DParameters &);

    CyclePlot2DParameters &operator=(const CyclePlot2DParameters &);

    CyclePlot2DParameters(CyclePlot2DParameters &&);

    CyclePlot2DParameters &operator=(CyclePlot2DParameters &&);

    CyclePlot2DParameters(const Data::Variant::Read &value,
                          const DisplayDefaults &defaults = DisplayDefaults());

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<Graph2DParameters> clone() const override;

    void overlay(const Graph2DParameters *over) override;

    void initializeOverlay(const Graph2DParameters *over) override;

    void copy(const Graph2DParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify an interval.
     * @return true if the parameters specify an interval
     */
    inline bool hasInterval() const
    { return components & Component_Interval; }

    /**
     * Get the interval.
     * @return the interval
     */
    inline TimeCycleInterval getInterval() const
    { return interval; }

    /**
     * Test if the parameters specify a division.
     * @return true if the parameters specify a division
     */
    inline bool hasDivision() const
    { return components & Component_Division; }

    /**
     * Get the division.
     * @return the division
     */
    inline TimeCycleDivision getDivision() const
    { return division; }
};

/**
 * The parameters for a 2D cycle plot trace
 */
class CPD3GRAPHING_EXPORT CyclePlotTraceParameters2D : public TraceParameters2D {
public:
    enum PointOrigin {
        Origin_Start, Origin_End, Origin_Center
    };
private:
    enum {
        Component_Origin = 0x0001, Component_BreakCycle = 0x0002,
    };
    int components;
    PointOrigin origin;
    bool breakCycle;
public:
    CyclePlotTraceParameters2D();

    virtual ~CyclePlotTraceParameters2D();

    CyclePlotTraceParameters2D(const CyclePlotTraceParameters2D &);

    CyclePlotTraceParameters2D &operator=(const CyclePlotTraceParameters2D &);

    CyclePlotTraceParameters2D(CyclePlotTraceParameters2D &&);

    CyclePlotTraceParameters2D &operator=(CyclePlotTraceParameters2D &&);

    CyclePlotTraceParameters2D(const Data::Variant::Read &value);

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<TraceParameters> clone() const override;

    void overlay(const TraceParameters *over) override;

    void copy(const TraceParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify the point origin.
     * @return true if the parameters specify the point origin
     */
    inline bool hasOrigin() const
    { return components & Component_Origin; }

    /**
     * Get the point origin.
     * @return the point origin
     */
    inline PointOrigin getOrigin() const
    { return origin; };

    /**
     * Test if the parameters specify a break cycle state.
     * @return true if the parameters specify a break cycle state
     */
    inline bool hasBreakCycle() const
    { return components & Component_BreakCycle; }

    /**
     * Get the break cycle state.
     * @return true if a break should be inserted whenever the cycle resets
     */
    inline bool getBreakCycle() const
    { return breakCycle; }
};

/**
 * The base class for a cycle plot trace.
 */
class CPD3GRAPHING_EXPORT CyclePlotTraceHandler2D : public TraceHandler2D {
    CyclePlotTraceParameters2D *parameters;

    TimeCycleInterval interval;
    TimeCycleDivision division;
public:
    CyclePlotTraceHandler2D(std::unique_ptr<CyclePlotTraceParameters2D> &&params);

    virtual ~CyclePlotTraceHandler2D();

    using Cloned = typename TraceHandler2D::Cloned;

    Cloned clone() const override;

    /**
     * Set the plot parameters.  Must be called after creation.
     * @param interval  the plot interval
     * @param division  the plot division
     */
    void setPlotParameters(const TimeCycleInterval &interval, const TimeCycleDivision &division);

protected:
    CyclePlotTraceHandler2D(const CyclePlotTraceHandler2D &other,
                            std::unique_ptr<TraceParameters> &&params);

    QString getFitLegend(const std::shared_ptr<Algorithms::Model> &fit) const override;

    void convertValue(const TraceDispatch::OutputValue &value,
                      std::vector<TracePoint<3> > &points) override;

    QVector<Algorithms::TransformerPoint *> prepareTransformer(const std::vector<
            TraceDispatch::OutputValue> &points,
                                                               Algorithms::TransformerParameters &parameters) override;

    std::vector<TracePoint<3> > convertTransformerResult(const QVector<
            Algorithms::TransformerPoint *> &points) override;

    Algorithms::TransformerAllocator *getTransformerAllocator() override;
};

/**
 * The base class for two dimensional cycle plot trace sets.
 */
class CPD3GRAPHING_EXPORT CyclePlotTraceSet2D : public TraceSet2D {
    TimeCycleInterval interval;
    TimeCycleDivision division;
public:
    CyclePlotTraceSet2D();

    virtual ~CyclePlotTraceSet2D();

    using Cloned = typename TraceSet2D::Cloned;

    Cloned clone() const override;

    /**
     * Set the plot parameters.  Must be called after creation.
     * @param interval  the plot interval
     * @param division  the plot division
     */
    void setPlotParameters(const TimeCycleInterval &interval, const TimeCycleDivision &division);

protected:
    CyclePlotTraceSet2D(const CyclePlotTraceSet2D &other);

    std::unique_ptr<
            TraceParameters> parseParameters(const Data::Variant::Read &value) const override;

    using CreatedHandler = typename TraceSet2D::CreatedHandler;

    CreatedHandler createHandler(std::unique_ptr<TraceParameters> &&parameters) const override;
};

/**
 * The parameters for bins on a cycle plot graph.
 */
class CPD3GRAPHING_EXPORT CyclePlotBinParameters2D : public BinParameters2D {
private:
    enum {
        Component_IntervalBins = 0x0001,
        Component_TotalBefore = 0x0002,
        Component_TotalAfter = 0x0004,
        Component_OnlyExtendTotals = 0x0008,
    };
    int components;

    bool intervalBins;
    bool totalBefore;
    bool totalAfter;
    bool onlyExtendTotals;
public:
    CyclePlotBinParameters2D();

    virtual ~CyclePlotBinParameters2D();

    CyclePlotBinParameters2D(const CyclePlotBinParameters2D &);

    CyclePlotBinParameters2D &operator=(const CyclePlotBinParameters2D &);

    CyclePlotBinParameters2D(CyclePlotBinParameters2D &&);

    CyclePlotBinParameters2D &operator=(CyclePlotBinParameters2D &&);

    CyclePlotBinParameters2D(const Data::Variant::Read &value);

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<TraceParameters> clone() const override;

    void overlay(const TraceParameters *over) override;

    void copy(const TraceParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters a interval bins enable state.
     * @return true if the parameters a interval bins enable state
     */
    inline bool hasIntervalBins() const
    { return components & Component_IntervalBins; }

    /**
     * Get the interval bins enable state
     * @return      true if normal bins should be shown
     */
    inline bool getIntervalBins() const
    { return intervalBins; }

    /**
     * Test if the parameters a total before enable state.
     * @return true if the parameters a total before enable state
     */
    inline bool hasTotalBefore() const
    { return components & Component_TotalBefore; }

    /**
     * Get the total before enable state
     * @return      true if a total bin is enabled before any data.
     */
    inline bool getTotalBefore() const
    { return totalBefore; }

    /**
     * Test if the parameters a total after enable state.
     * @return true if the parameters a total after enable state
     */
    inline bool hasTotalAfter() const
    { return components & Component_TotalAfter; }

    /**
     * Get the total after enable state
     * @return      true if a total bin is enabled after any data.
     */
    inline bool getTotalAfter() const
    { return totalAfter; }

    /**
     * Test if the parameters if only total bins should be extended.
     * @return true if the parameters if only total bins should be extended
     */
    inline bool hasOnlyExtendTotals() const
    { return components & Component_OnlyExtendTotals; }

    /**
     * Test only total bins should have their middles extended.
     * @return      true if only total bins should have their middles extended
     */
    inline bool getOnlyExtendTotals() const
    { return onlyExtendTotals; }
};

/**
 * The base class for two dimensional cycle plot bins.
 */
class CPD3GRAPHING_EXPORT CyclePlotBinHandler2D : public BinHandler2D {
    CyclePlotBinParameters2D *parameters;

    TimeCycleInterval interval;
    TimeCycleDivision division;
public:
    CyclePlotBinHandler2D(std::unique_ptr<CyclePlotBinParameters2D> &&params);

    virtual ~CyclePlotBinHandler2D();

    using Cloned = typename BinHandler2D::Cloned;

    Cloned clone() const override;

    /**
     * Set the plot parameters.  Must be called after creation.
     * @param interval  the plot interval
     * @param division  the plot division
     */
    void setPlotParameters(const TimeCycleInterval &interval, const TimeCycleDivision &division);

protected:
    CyclePlotBinHandler2D(const CyclePlotBinHandler2D &other,
                          std::unique_ptr<TraceParameters> &&params);

    void convertValue(const TraceDispatch::OutputValue &value,
                      std::vector<TracePoint<2> > &points) override;

    QVector<Algorithms::TransformerPoint *> prepareTransformer(const std::vector<
            TraceDispatch::OutputValue> &points,
                                                               Algorithms::TransformerParameters &parameters) override;

    std::vector<TracePoint<2>> convertTransformerResult(const QVector<
            Algorithms::TransformerPoint *> &points) override;

    Algorithms::TransformerAllocator *getTransformerAllocator() override;

    void buildBins(std::size_t dim,
                   std::vector<double> &starts,
                   std::vector<double> &ends,
                   std::vector<double> &centers) override;

    std::vector<TracePoint<2>> applyFilter(std::vector<TracePoint<2>> input,
                                           double start,
                                           double end,
                                           std::size_t dim) override;

    void convertBin(const std::vector<TracePoint<2> > &points, BinPoint<1, 1> &output) override;

    void convertHistogram(const std::vector<TracePoint<2> > &points,
                          const double totalTime[1],
                          double effectiveStart,
                          double effectiveEnd,
                          BinPoint<1, 1> &output) override;

    std::vector<BinPoint<1, 1>> getMiddleExtendPoints(const std::vector<
            BinPoint<1, 1> > &input) const override;
};

/**
 * The base class for two dimensional cycle plot box whisker sets.
 */
class CPD3GRAPHING_EXPORT CyclePlotBinSet2D : public BinSet2D {
    TimeCycleInterval interval;
    TimeCycleDivision division;

public:
    CyclePlotBinSet2D();

    virtual ~CyclePlotBinSet2D();

    using Cloned = typename BinSet2D::Cloned;

    Cloned clone() const override;

    /**
     * Set the plot parameters.  Must be called after creation.
     * @param interval  the plot interval
     * @param division  the plot division
     */
    void setPlotParameters(const TimeCycleInterval &interval, const TimeCycleDivision &division);

protected:
    CyclePlotBinSet2D(const CyclePlotBinSet2D &other);

    std::unique_ptr<
            TraceParameters> parseParameters(const Data::Variant::Read &value) const override;

    using CreatedHandler = typename BinSet2D::CreatedHandler;

    CreatedHandler createHandler(std::unique_ptr<TraceParameters> &&parameters) const override;
};

/**
 * Parameters for an indicator on a two dimensional cycle plot graph.
 */
class CPD3GRAPHING_EXPORT CyclePlotIndicatorParameters2D : public IndicatorParameters2D {
public:
    enum PointOrigin {
        Origin_Start, Origin_End, Origin_Center
    };
private:
    enum {
        Component_Origin = 0x0001,
    };
    int components;
    PointOrigin origin;
public:
    CyclePlotIndicatorParameters2D();

    virtual ~CyclePlotIndicatorParameters2D();

    CyclePlotIndicatorParameters2D(const CyclePlotIndicatorParameters2D &);

    CyclePlotIndicatorParameters2D &operator=(const CyclePlotIndicatorParameters2D &);

    CyclePlotIndicatorParameters2D(CyclePlotIndicatorParameters2D &&);

    CyclePlotIndicatorParameters2D &operator=(CyclePlotIndicatorParameters2D &&);

    CyclePlotIndicatorParameters2D(const Data::Variant::Read &value);

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<TraceParameters> clone() const override;

    void overlay(const TraceParameters *over) override;

    void copy(const TraceParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify the point origin.
     * @return true if the parameters specify the point origin
     */
    inline bool hasOrigin() const
    { return components & Component_Origin; }

    /**
     * Get the point origin.
     * @return the point origin
     */
    inline PointOrigin getOrigin() const
    { return origin; };
};

/**
 * A handler for a single indicator on a two dimensional cycle plot graph.
 */
class CPD3GRAPHING_EXPORT CyclePlotIndicatorHandler2D : public IndicatorHandler2D {
    CyclePlotIndicatorParameters2D *parameters;

    TimeCycleInterval interval;
    TimeCycleDivision division;
public:
    CyclePlotIndicatorHandler2D(std::unique_ptr<CyclePlotIndicatorParameters2D> &&params);

    virtual ~CyclePlotIndicatorHandler2D();

    using Cloned = IndicatorHandler2D::Cloned;

    Cloned clone() const override;

    /**
     * Set the plot parameters.  Must be called after creation.
     * @param interval  the plot interval
     * @param division  the plot division
     */
    void setPlotParameters(const TimeCycleInterval &interval, const TimeCycleDivision &division);

protected:
    void convertIndicatorValue(const TraceDispatch::OutputValue &value,
                               IndicatorPoint<1> &add) override;

    CyclePlotIndicatorHandler2D(const CyclePlotIndicatorHandler2D &other,
                                std::unique_ptr<TraceParameters> &&params);
};

/**
 * A handler for a set of indicators on a two dimensional cycle plot graph.
 */
class CPD3GRAPHING_EXPORT CyclePlotIndicatorSet2D : public IndicatorSet2D {
    TimeCycleInterval interval;
    TimeCycleDivision division;
public:
    CyclePlotIndicatorSet2D();

    virtual ~CyclePlotIndicatorSet2D();

    using Cloned = IndicatorSet2D::Cloned;

    Cloned clone() const override;

    /**
     * Set the plot parameters.  Must be called after creation.
     * @param interval  the plot interval
     * @param division  the plot division
     */
    void setPlotParameters(const TimeCycleInterval &interval, const TimeCycleDivision &division);

protected:
    CyclePlotIndicatorSet2D(const CyclePlotIndicatorSet2D &other);

    std::unique_ptr<
            TraceParameters> parseParameters(const Data::Variant::Read &value) const override;

    using CreatedHandler = typename IndicatorSet2D::CreatedHandler;

    CreatedHandler createHandler(std::unique_ptr<TraceParameters> &&parameters) const override;
};

/**
 * The common parameters for the time axis on a cycle plot graph.
 */
class CPD3GRAPHING_EXPORT AxisParametersCyclePlot2D : public AxisParameters2DSide {
    enum {
        Component_TickOffset = 0x0001,
    };
    int components;
    double tickOffset;
public:
    AxisParametersCyclePlot2D();

    virtual ~AxisParametersCyclePlot2D();

    AxisParametersCyclePlot2D(const AxisParametersCyclePlot2D &);

    AxisParametersCyclePlot2D &operator=(const AxisParametersCyclePlot2D &);

    AxisParametersCyclePlot2D(AxisParametersCyclePlot2D &&);

    AxisParametersCyclePlot2D &operator=(AxisParametersCyclePlot2D &&);

    AxisParametersCyclePlot2D(const Data::Variant::Read &value, QSettings *settings = 0);

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<AxisParameters> clone() const override;

    void overlay(const AxisParameters *over) override;

    void copy(const AxisParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify a tick offset.
     * @return true if the parameters specify a tick offset
     */
    inline bool hasTickOffset() const
    { return components & Component_TickOffset; }

    /**
     * Get the tick offset, added to all (non-total) tick positions.
     * @return the tick offset
     */
    inline double getTickOffset() const
    { return tickOffset; }
};

namespace Internal {
class CyclePlotAxisZoom;

class CyclePlotAxisCoupling;
}

/**
 * The set of axis representing the time on a cycle plot 2D graph.
 */
class CPD3GRAPHING_EXPORT AxisDimensionCyclePlot2D : public AxisDimension2DSide {
    AxisParametersCyclePlot2D *parameters;

    TimeCycleInterval interval;
    TimeCycleDivision division;

    friend class Internal::CyclePlotAxisZoom;

    friend class Internal::CyclePlotAxisCoupling;

public:
    AxisDimensionCyclePlot2D(std::unique_ptr<AxisParametersCyclePlot2D> &&params);

    virtual ~AxisDimensionCyclePlot2D();

    std::unique_ptr<AxisDimension> clone() const override;

    std::vector<std::shared_ptr<
            DisplayCoupling>> createRangeCoupling(DisplayCoupling::CoupleMode mode = DisplayCoupling::Always) override;

    /**
     * Set the plot parameters.  Must be called after creation.
     * @param interval  the plot interval
     * @param division  the plot division
     */
    void setPlotParameters(const TimeCycleInterval &interval, const TimeCycleDivision &division);

    std::vector<std::vector<AxisTickGenerator::Tick>> getTicks() const override;

    bool enableBoundsModification() const override;

protected:
    AxisDimensionCyclePlot2D(const AxisDimensionCyclePlot2D &other,
                             std::unique_ptr<AxisParameters> &&params);

    void updateTransformer(double &min, double &max, double zoomMin, double zoomMax) override;
};

namespace Internal {

/**
 * A coupling for a cycle plot axis
 */
class CyclePlotAxisCoupling : public DisplayRangeCoupling {
Q_OBJECT

    QPointer<AxisDimensionCyclePlot2D> parent;
public:
    CyclePlotAxisCoupling();

    CyclePlotAxisCoupling(DisplayCoupling::CoupleMode mode, AxisDimensionCyclePlot2D *setParent);

    virtual ~CyclePlotAxisCoupling();

    double getMin() const override;

    double getMax() const override;

    bool canCouple(const std::shared_ptr<DisplayCoupling> &other) const override;

    void applyCoupling(const QVariant &data, DisplayCoupling::CouplingPosition position) override;
};

}

class AxisDimensionSetCyclePlot2D;

namespace Internal {

/**
 * The zoom for a cycle plot axis.
 */
class CyclePlotAxisZoom : public AxisDimensionZoom {
Q_OBJECT

    QPointer<AxisDimensionCyclePlot2D> dimension;
    QPointer<AxisDimensionSetCyclePlot2D> parent;
public:
    CyclePlotAxisZoom();

    CyclePlotAxisZoom(AxisDimensionCyclePlot2D *setDimension,
                      AxisDimensionSetCyclePlot2D *setParent);

    QString getTitle() const override;

    bool sharedZoom(const std::shared_ptr<DisplayZoomAxis> &other) const override;

    DisplayZoomAxis::DisplayMode getDisplayMode() const override;
};

}

/**
 * A set of side for the time axis on a 2D cycle plot graph.
 */
class CPD3GRAPHING_EXPORT AxisDimensionSetCyclePlot2D : public AxisDimensionSet2DSide {
    TimeCycleInterval interval;
    TimeCycleDivision division;
public:
    AxisDimensionSetCyclePlot2D();

    virtual ~AxisDimensionSetCyclePlot2D();

    std::unique_ptr<AxisDimensionSet> clone() const override;

    AxisDimension *get(const std::unordered_set<QString> &units,
                       const std::unordered_set<QString> &groupUnits) override;

    AxisDimension *get(const std::unordered_set<QString> &units,
                       const std::unordered_set<QString> &groupUnits,
                       const QString &binding) override;

    /**
     * Set the plot parameters.  Must be called after creation.
     * @param interval  the plot interval
     * @param division  the plot division
     */
    void setPlotParameters(const TimeCycleInterval &interval, const TimeCycleDivision &division);

protected:
    AxisDimensionSetCyclePlot2D(const AxisDimensionSetCyclePlot2D &other);

    std::unique_ptr<AxisParameters> parseParameters(const Data::Variant::Read &value,
                                                    QSettings *settings = 0) const override;

    std::unique_ptr<AxisDimension> createDimension(std::unique_ptr<
            AxisParameters> &&parameters) const override;

    std::shared_ptr<AxisDimensionZoom> createZoom(AxisDimension *dimension) override;
};

/**
 * A cycle plot style two dimensional graph.  Cycle position is on the X axis
 * and values are on the Y axis.
 */
class CPD3GRAPHING_EXPORT CyclePlot2D : public Graph2D {
Q_OBJECT

    QSettings *settings;

public:
    virtual ~CyclePlot2D();

    CyclePlot2D(const DisplayDefaults &defaults = DisplayDefaults(), QObject *parent = 0);

    CyclePlot2D(const CyclePlot2D &other);

    std::unique_ptr<Display> clone() const override;

protected:
    std::unique_ptr<TraceSet2D> createTraces() override;

    std::unique_ptr<BinSet2D> createXBins() override;

    std::unique_ptr<BinSet2D> createYBins() override;

    std::unique_ptr<IndicatorSet2D> createXIndicators() override;

    std::unique_ptr<AxisDimensionSet2DSide> createXAxes() override;

    std::unique_ptr<Graph2DParameters> parseParameters(const Data::Variant::Read &config,
                                                       const DisplayDefaults &defaults = {}) override;

    std::vector<
            GraphPainter2DHighlight::Region> translateHighlightTrace(DisplayHighlightController::HighlightType type,
                                                                     double start,
                                                                     double end,
                                                                     double min,
                                                                     double max,
                                                                     TraceHandler2D *trace,
                                                                     AxisDimension2DSide *x,
                                                                     AxisDimension2DSide *y,
                                                                     AxisDimension2DColor *z,
                                                                     const std::unordered_set<
                                                                             std::size_t> &affectedDimensions) override;

    std::vector<
            GraphPainter2DHighlight::Region> translateHighlightXBin(DisplayHighlightController::HighlightType type,
                                                                    double start,
                                                                    double end,
                                                                    double min,
                                                                    double max,
                                                                    BinHandler2D *bins,
                                                                    AxisDimension2DSide *x,
                                                                    AxisDimension2DSide *y,
                                                                    const std::unordered_set<
                                                                            std::size_t> &affectedDimensions) override;

    QString convertMouseoverToolTipTrace(TraceHandler2D *trace,
                                         TraceSet2D *traces,
                                         AxisDimension2DSide *x,
                                         AxisDimension2DSide *y,
                                         AxisDimension2DColor *z,
                                         const TracePoint<3> &point,
                                         const DisplayDynamicContext &context) override;

    QString convertMouseoverToolTipBins(BinHandler2D *bins,
                                        BinSet2D *binSet,
                                        bool vertical,
                                        AxisDimension2DSide *x,
                                        AxisDimension2DSide *y,
                                        const BinPoint<1, 1> &point,
                                        Graph2DMouseoverComponentTraceBox::Location location,
                                        const DisplayDynamicContext &context) override;

    QString convertMouseoverToolTipIndicator(IndicatorHandler2D *indicator,
                                             IndicatorSet2D *indicatorSet,
                                             AxisDimension2DSide *axis,
                                             const IndicatorPoint<1> &point,
                                             Axis2DSide side,
                                             const DisplayDynamicContext &context) override;

    QString convertMouseoverToolTipFit(TraceHandler2D *trace,
                                       TraceSet2D *traces,
                                       AxisDimension2DSide *x,
                                       AxisDimension2DSide *y,
                                       AxisDimension2DColor *z,
                                       double fx,
                                       double fy,
                                       double fz,
                                       double fi,
                                       const DisplayDynamicContext &context) override;

    QString convertMouseoverToolTipModelScan(TraceHandler2D *trace,
                                             TraceSet2D *traces,
                                             AxisDimension2DSide *x,
                                             AxisDimension2DSide *y,
                                             AxisDimension2DColor *z,
                                             double mx,
                                             double my,
                                             double mz,
                                             const DisplayDynamicContext &context) override;

    QString convertMouseoverToolTipFit(BinHandler2D *bins,
                                       BinSet2D *binSet,
                                       bool vertical,
                                       AxisDimension2DSide *x,
                                       AxisDimension2DSide *y,
                                       BinParameters2D::FitOrigin origin,
                                       double fx,
                                       double fy,
                                       const DisplayDynamicContext &context) override;

    /**
     * Get the time interval used in the plot.
     * @return the time interval
     */
    inline TimeCycleInterval getInterval() const
    {
        return static_cast<const CyclePlot2DParameters *>(getParameters())->getInterval();
    }

    /**
     * Get the time division used in the plot.
     * @return the time division
     */
    inline TimeCycleDivision getDivision() const
    {
        return static_cast<const CyclePlot2DParameters *>(getParameters())->getDivision();
    }
};

}
}

#endif
