/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGLEGEND_H
#define CPD3GRAPHINGLEGEND_H

#include "core/first.hxx"

#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QPen>
#include <QColor>
#include <QFont>
#include <QVector>

#include "graphing/graphing.hxx"
#include "graphing/tracecommon.hxx"

namespace CPD3 {
namespace Graphing {

class Legend;

/**
 * A single item in the legend.
 */
class CPD3GRAPHING_EXPORT LegendItem {
    qreal lineWidth;
    Qt::PenStyle lineStyle;
    bool drawSwatch;

    TraceSymbol symbol;
    bool drawSymbol;

    QString text;
    QColor color;
    QFont font;

    int nGroup;

    friend class Legend;

    LegendItem();

public:
    virtual ~LegendItem();

    LegendItem(const QString &setText,
               const QColor &setColor,
               const QFont &setFont = QFont(),
               qreal setLineWidth = -1.0,
               Qt::PenStyle setStyle = Qt::NoPen,
               const TraceSymbol &setSymbol = TraceSymbol(),
               bool setDrawSymbol = false,
               bool setDrawSwatch = false);

    /**
     * Get the color of this item
     * @return the item color
     */
    inline QColor getColor() const
    { return color; }

    /**
     * Get the text of this item
     * @return the item text
     */
    inline QString getText() const
    { return text; }

    /**
     * Test if any part of the legend entry is drawable.
     * @return true if the legend entry has a symbol, swatch or line
     */
    inline bool hasAnyDrawComponents() const
    {
        return (lineStyle != Qt::NoPen && lineWidth >= 0.0) ||
                drawSwatch ||
                (drawSymbol && symbol.getType() != TraceSymbol::Unselected);
    }
};

/**
 * The base class 
 */
class CPD3GRAPHING_EXPORT Legend {
    std::vector<std::shared_ptr<LegendItem>> items;
    std::vector<std::vector<std::shared_ptr<LegendItem>>> columns;
    QSizeF predictedSize;
    std::vector<qreal> rowHeights;
    std::vector<qreal> columnWidths;
    std::vector<qreal> columnGraphicWidths;

public:
    Legend();

    Legend(const Legend &other);

    Legend &operator=(const Legend &other);

    Legend(Legend &&other);

    Legend &operator=(Legend &&other);

    /**
     * Test if the legend is empty.
     * 
     * @return true if there are no items
     */
    inline bool isEmpty() const
    { return items.empty(); }

    /**
     * Get the number of entries in the legend.
     * 
     * @return the number of entries in the legend
     */
    inline int size() const
    { return items.size(); }

    /**
     * Get the item at the given index.
     * 
     * @param index the index to get
     * @return a legend item
     */
    inline std::shared_ptr<LegendItem> at(int index) const
    { return items.at(index); }

    void clear();

    void append(const std::shared_ptr<LegendItem> &add);

    void append(std::shared_ptr<LegendItem> &&add);

    void append(const std::vector<std::shared_ptr<LegendItem>> &add);

    void append(std::vector<std::shared_ptr<LegendItem>> &&add);

    void layout(qreal maximumWidth = -1,
                qreal maximumHeight = -1,
                QPaintDevice *paintdevice = NULL);

    QSizeF getSize() const;

    void paint(QPainter *painter) const;

    void paint(QPainter *painter, qreal x, qreal y) const;

    std::shared_ptr<LegendItem> itemAt(qreal x, qreal y, QPaintDevice *paintdevice = NULL) const;
};

}
}


#endif
