/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGINDICATOR2D_H
#define CPD3GRAPHINGINDICATOR2D_H

#include "core/first.hxx"

#include <memory>
#include <QtGlobal>
#include <QObject>

#include "graphing/graphing.hxx"
#include "graphing/indicator.hxx"
#include "graphing/graphcommon2d.hxx"
#include "graphing/axiscommon.hxx"

namespace CPD3 {
namespace Graphing {

/**
 * Parameters for an indicator on a two dimensional graph.
 */
class CPD3GRAPHING_EXPORT IndicatorParameters2D : public IndicatorParameters {
public:
    /** The side selection mode. */
    enum SideMode {
        /** Automatically select the drawing side. */
        Side_Auto, /** Always draw on the primary (bottom/left) side. */
        Side_Primary, /** Always draw on the secondary (top/right) side. */
        Side_Secondary,
    };
private:
    enum {
        Component_Side = 0x00000001,
        Component_AxisIndicator = 0x00000002,
        Component_Enable = 0x00000004,
        Component_Color = 0x00000008,
        Component_LegendText = 0x00000010,
        Component_DrawPriority = 0x00000020,
        Component_Binding = 0x00000040,
    };
    int components;

    SideMode side;
    AxisIndicator2D indicator;
    bool enable;
    QColor color;
    QString legendText;
    int drawPriority;
    QString binding;
public:
    IndicatorParameters2D();

    virtual ~IndicatorParameters2D();

    IndicatorParameters2D(const IndicatorParameters2D &other);

    IndicatorParameters2D &operator=(const IndicatorParameters2D &other);

    IndicatorParameters2D(IndicatorParameters2D &&other);

    IndicatorParameters2D &operator=(IndicatorParameters2D &&other);

    IndicatorParameters2D(const Data::Variant::Read &value);

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<TraceParameters> clone() const override;

    void overlay(const TraceParameters *over) override;

    void copy(const TraceParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify the draw side.
     * @return true if the parameters specify the draw side
     */
    inline bool hasSide() const
    { return components & Component_Side; }

    /**
     * Get the draw side.
     * @return the draw side
     */
    inline SideMode getSide() const
    { return side; };

    /**
     * Test if the parameters specify an axis indicator.
     * @return true if the parameters specify an axis indicator
     */
    inline bool hasAxisIndicator() const
    { return components & Component_AxisIndicator; }

    /**
     * Get the axis indicator.
     * @return a copy of the axis indicator
     */
    inline AxisIndicator2D getAxisIndicator() const
    { return indicator; };

    /**
     * Get a modifiable reference to the axis indicator
     * @return a reference to the axis indicator
     */
    inline AxisIndicator2D &getAxisIndicatorReference()
    { return indicator; }

    /**
     * Override the axis indicator.  This sets the defined flag.
     * @param indicator the new indicator
     */
    inline void overrideAxisIndicator(const AxisIndicator2D &indicator)
    {
        this->indicator = indicator;
        components |= Component_AxisIndicator;
    }

    /**
     * Test if the parameters specify the enable state.
     * @return true if the parameters specify the enable state
     */
    inline bool hasEnable() const
    { return components & Component_Enable; }

    /**
     * Get the enabled state.
     * @return the enable state
     */
    inline bool getEnable() const
    { return enable; };

    /**
     * Override the enable state.  This sets the defined flag.
     * @param e the new enable state
     */
    inline void overrideEnable(bool e)
    {
        enable = e;
        components |= Component_Enable;
    }

    /**
     * Test if the parameters specify the draw color.
     * @return true if the parameters specify the draw color
     */
    inline bool hasColor() const
    { return components & Component_Color; }

    /**
     * Get the draw color.
     * @return the draw color
     */
    inline QColor getColor() const
    { return color; };

    /**
     * Override the color.  This sets the defined flag.
     * @param color the new color
     */
    inline void overrideColor(const QColor &color)
    {
        this->color = color;
        components |= Component_Color;
    }

    /**
     * Test if the parameters specify the legend text.
     * @return true if the parameters specify a display name
     */
    inline bool hasLegendText() const
    { return components & Component_LegendText; }

    /**
     * Get the legend text.
     * @return the displayed name
     */
    inline QString getLegendText() const
    { return legendText; };

    /**
     * Test if the parameters specify the draw sort priority.
     * @return true if the parameters specify the draw sort priority
     */
    inline bool hasDrawPriority() const
    { return components & Component_DrawPriority; }

    /**
     * Get the draw sort priority.
     * @return the draw sort priority
     */
    inline int getDrawPriority() const
    { return drawPriority; };

    /**
     * Test if the parameters specify the binding.
     * @return true if the parameters specify the binding
     */
    inline bool hasBinding() const
    { return components & Component_Binding; }

    /**
     * Get the binding.
     * @return the binding
     */
    inline QString getBinding() const
    { return binding; };
};

namespace Internal {
class IndicatorOutput2D;
}

/**
 * A handler for a single indicator on a two dimensional graph.
 */
class CPD3GRAPHING_EXPORT IndicatorHandler2D : public IndicatorValueHandler<1> {
    IndicatorParameters2D *parameters;

    friend class Internal::IndicatorOutput2D;

public:
    IndicatorHandler2D(std::unique_ptr<IndicatorParameters2D> &&params);

    virtual ~IndicatorHandler2D();

    using Cloned = typename IndicatorValueHandler<1>::Cloned;

    Cloned clone() const override;

    /**
     * Get the side mode for the indicator handler.
     * @return the side mode
     */
    inline IndicatorParameters2D::SideMode getSideMode() const
    { return parameters->getSide(); }

    /**
     * Test if the handler specified an axis binding.
     * @return true if the handler is bound to a specific axis
     */
    inline bool hasBinding() const
    { return parameters->hasBinding(); }

    /**
     * Get the specific axis the indicator is bound to
     * @return the axis binding
     */
    inline QString getBinding() const
    { return parameters->getBinding(); }

    /**
     * Get the color of the indicator.
     * @return the indicator color
     */
    inline QColor getColor() const
    { return parameters->getColor(); }

    void insertUsedTypes(std::unordered_set<AxisIndicator2D::Type,
                                            AxisIndicator2D::TypeHash> &types) const;

    void bindType(std::unordered_set<AxisIndicator2D::Type,
                                     AxisIndicator2D::TypeHash> &types) const;

    QString getDisplayTitle(const std::vector<Data::SequenceName::Set> &inputUnits = {},
                            bool showStation = false,
                            bool showArchive = false,
                            const DisplayDynamicContext &context = {}) const;

    std::vector<
            std::unique_ptr<Graph2DDrawPrimitive>> createDraw(const AxisTransformer &transformer,
                                                              qreal primary,
                                                              qreal secondary,
                                                              Axis2DSide side) const;

    std::vector<std::shared_ptr<DisplayOutput>> getOutputs(const std::vector<
            Data::SequenceName::Set> &inputUnits = {},
                                                           bool showStation = false,
                                                           bool showArchive = false,
                                                           const DisplayDynamicContext &context = {});

protected:
    IndicatorHandler2D(const IndicatorHandler2D &other, std::unique_ptr<TraceParameters> &&params);
};

/**
 * A handler for a set of indicators on a two dimensional graph.
 */
class CPD3GRAPHING_EXPORT IndicatorSet2D : public IndicatorDispatchSet<1> {
public:
    IndicatorSet2D();

    virtual ~IndicatorSet2D();

    using Cloned = typename IndicatorDispatchSet<1>::Cloned;

    using CreatedHandler = typename IndicatorDispatchSet<1>::CreatedHandler;

    Cloned clone() const override;

    std::vector<IndicatorHandler2D *> getSortedHandlers() const;

    QString handlerTitle(IndicatorHandler2D *indicator, const DisplayDynamicContext &context) const;

    QString handlerOrigin(IndicatorHandler2D *indicator,
                          const DisplayDynamicContext &context) const;

    std::vector<
            std::shared_ptr<DisplayOutput> > getOutputs(const DisplayDynamicContext &context = {});

protected:
    IndicatorSet2D(const IndicatorSet2D &other);

    std::unique_ptr<
            TraceParameters> parseParameters(const Data::Variant::Read &value) const override;

    CreatedHandler createHandler(std::unique_ptr<TraceParameters> &&parameters) const override;
};

namespace Internal {

/**
 * The output type for 2-D indicators.
 */
class IndicatorOutput2D : public DisplayOutput {
Q_OBJECT
    QPointer<IndicatorHandler2D> handler;
    QString title;
public:
    IndicatorOutput2D(IndicatorHandler2D *setHandler, const QString &setTitle);

    virtual ~IndicatorOutput2D();

    QString getTitle() const override;

    QList<QAction *> getMenuActions(QWidget *parent = nullptr) override;
};

}

}
}

#endif
