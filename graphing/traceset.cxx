/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>

#include "graphing/traceset.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Graphing {

/** @file graphing/traceset.hxx
 * Provides a class to handle trace set dispatch.
 */

TraceParameters::TraceParameters() : components(0), transformerInputMode(Transformer_ValuesOnly)
{ }

TraceParameters::~TraceParameters() = default;

TraceParameters::TraceParameters(const TraceParameters &) = default;

TraceParameters &TraceParameters::operator=(const TraceParameters &) = default;

TraceParameters::TraceParameters(TraceParameters &&) = default;

TraceParameters &TraceParameters::operator=(TraceParameters &&) = default;

/**
 * Replace these parameters with the given ones, including all polymorphism.
 * <br>
 * This is equivalent to: *this = *(static_cast<const CLASS *>(from))
 * 
 * @param from  the parameters to copy 
 */
void TraceParameters::copy(const TraceParameters *from)
{ *this = *from; }

/**
 * Clear all "set" flags from the parameters.  Derived implementations must
 * call the base one.
 */
void TraceParameters::clear()
{ components = 0; }

TraceValueHandlerBase::TraceValueHandlerBase() = default;

TraceDispatchSetBase::TraceDispatchSetBase() = default;

void TraceDispatchSetBase::dispatcherFinished()
{
    if (!haveAllDispatchersEnded())
        return;
    emit readyForFinalProcessing();
}

void TraceDispatchSetBase::handlersUpdated()
{
    emit handlersChanged();
}

/**
 * Parse the given value into parameters.
 */
TraceParameters::TraceParameters(const Variant::Read &value) : components(0),
                                                               transformer(),
                                                               transformerInputMode(
                                                                       Transformer_ValuesOnly)
{
    if (value["Transformer"].exists()) {
        components |= Component_Transformer;
        transformer = Variant::Root(value["Transformer"]);
    }

    if (value["TransformerMode"].exists()) {
        const auto &type = value["TransformerMode"].toString();
        if (Util::equal_insensitive(type, "simple", "values", "valuesonly")) {
            transformerInputMode = Transformer_ValuesOnly;
            components |= Component_TransformerInputMode;
        } else if (Util::equal_insensitive(type, "start", "withstart", "startthenvalues")) {
            transformerInputMode = Transformer_StartThenValues;
            components |= Component_TransformerInputMode;
        } else if (Util::equal_insensitive(type, "end", "withend", "endthenvalues")) {
            transformerInputMode = Transformer_EndThenValues;
            components |= Component_TransformerInputMode;
        } else if (Util::equal_insensitive(type, "middle", "withmiddle", "middlethenvalues")) {
            transformerInputMode = Transformer_MiddleThenValues;
            components |= Component_TransformerInputMode;
        } else if (Util::equal_insensitive(type, "startend", "withstartandend",
                                           "startendthenvalues")) {
            transformerInputMode = Transformer_StartEndValues;
            components |= Component_TransformerInputMode;
        }
    }
}

/**
 * Save the parameters to the given value.  Derived classes must call
 * the base implementation.
 * 
 * @param value the target value
 */
void TraceParameters::save(Variant::Write &value) const
{
    if (components & Component_Transformer) {
        value["Transformer"].set(transformer);
    }
    if (components & Component_TransformerInputMode) {
        switch (transformerInputMode) {
        case Transformer_ValuesOnly:
            value["TransformerMode"].setString("Values");
            break;
        case Transformer_StartThenValues:
            value["TransformerMode"].setString("StartThenValues");
            break;
        case Transformer_EndThenValues:
            value["TransformerMode"].setString("EndThenValues");
            break;
        case Transformer_MiddleThenValues:
            value["TransformerMode"].setString("MiddleThenValues");
            break;
        case Transformer_StartEndValues:
            value["TransformerMode"].setString("StartEndThenValues");
            break;
        }
    }
}

/**
 * Create a copy of the parameters, including polymorphism.
 * 
 * @return a copy of the parameters
 */
std::unique_ptr<TraceParameters> TraceParameters::clone() const
{ return std::unique_ptr<TraceParameters>(new TraceParameters(*this)); }

/**
 * Overlay the given parameters on top of these.
 * 
 * @param over  the parameters to overlay on top.
 */
void TraceParameters::overlay(const TraceParameters *over)
{
    if (over->components & Component_Transformer) {
        components |= Component_Transformer;
        transformer = over->transformer;
    }
    if (over->components & Component_TransformerInputMode) {
        components |= Component_TransformerInputMode;
        transformerInputMode = over->transformerInputMode;
    }
}

}
}
