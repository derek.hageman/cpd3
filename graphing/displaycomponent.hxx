/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGDISPLAYCOMPONENT_H
#define CPD3GRAPHINGDISPLAYCOMPONENT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QList>

#include "graphing/graphing.hxx"
#include "graphing/display.hxx"
#include "core/component.hxx"
#include "datacore/variant/root.hxx"

namespace CPD3 {
namespace Graphing {

/**
 * A class of utilities to handle displays as discrete components.
 */
class CPD3GRAPHING_EXPORT DisplayComponent : public QObject {
Q_OBJECT
public:
    /**
     * The type of display being made.
     */
    enum DisplayType {
        /** A scatter plot. */
        Display_Scatter2D,

        /** A time series plot. */
        Display_TimeSeries2D,

        /** A cyle display plot. */
        Display_Cycle2D,

        /** A CDF plot. */
        Display_CDF,

        /** A PDF/Histogram plot. */
        Display_PDF,

        /** An Allan plot. */
        Display_Allan,

        /** A scatter plot of density. */
        Display_Density2D,

        /** A configuration loaded from a file. */
        Display_Layout,
    };

    static ComponentOptions options(DisplayType type);

    static QList<ComponentExample> examples(DisplayType type);

    static Display *create(DisplayType type,
                           const ComponentOptions &options = ComponentOptions(),
                           const DisplayDefaults &defaults = DisplayDefaults());

    static Data::Variant::Root baselineConfiguration(DisplayType type,
                                                     const ComponentOptions &options = ComponentOptions());
};

}
}

#endif
