/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtAlgorithms>
#include <QObject>
#include <QString>
#include <QTest>

#include "graphing/cyclecommon.hxx"
#include "core/timeutils.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Graphing;

Q_DECLARE_METATYPE(TimeCycleInterval);

Q_DECLARE_METATYPE(TimeCycleDivision);

Q_DECLARE_METATYPE(AxisTickGenerator::GenerateFlags);

Q_DECLARE_METATYPE(std::vector<AxisTickGenerator::Tick>);

class TestCycleCommon : public QObject {
Q_OBJECT
private slots:

    void intervalBounds()
    {
        QFETCH(Time::LogicalTimeUnit, unit);
        QFETCH(int, count);
        QFETCH(bool, align);
        QFETCH(double, base);
        QFETCH(double, timeFor);
        QFETCH(double, expectedStart);
        QFETCH(double, expectedEnd);

        double start = 0;
        double end = 0;

        TimeCycleInterval in(unit, count, align);
        const_cast<const TimeCycleInterval *>(&in)->intervalBounds(base, timeFor, start, end);
        QCOMPARE(start, expectedStart);
        QCOMPARE(end, expectedEnd);

        start = end = 0;


        in.intervalBounds(base, timeFor, start, end);
        QCOMPARE(start, expectedStart);
        QCOMPARE(end, expectedEnd);

        in.intervalBounds(base, timeFor, start, end);
        QCOMPARE(start, expectedStart);
        QCOMPARE(end, expectedEnd);
    }

    void intervalBounds_data()
    {
        QTest::addColumn<Time::LogicalTimeUnit>("unit");
        QTest::addColumn<int>("count");
        QTest::addColumn<bool>("align");
        QTest::addColumn<double>("base");
        QTest::addColumn<double>("timeFor");
        QTest::addColumn<double>("expectedStart");
        QTest::addColumn<double>("expectedEnd");

        QTest::newRow("Single year aligned start") <<
                Time::Year <<
                1 <<
                true <<
                1262304000.0 <<
                1262304000.0 <<
                1262304000.0 <<
                1293840000.0;
        QTest::newRow("Single year aligned mid") <<
                Time::Year <<
                1 <<
                true <<
                1262304000.0 <<
                1262304020.0 <<
                1262304000.0 <<
                1293840000.0;
        QTest::newRow("Single year aligned mid base") <<
                Time::Year <<
                1 <<
                true <<
                1262304020.0 <<
                1262304020.0 <<
                1262304000.0 <<
                1293840000.0;
        QTest::newRow("Two year aligned start") <<
                Time::Year <<
                2 <<
                true <<
                1262304000.0 <<
                1262304000.0 <<
                1262304000.0 <<
                1325376000.0;
        QTest::newRow("Two year aligned mid") <<
                Time::Year <<
                2 <<
                true <<
                1262304000.0 <<
                1262304020.0 <<
                1262304000.0 <<
                1325376000.0;
        QTest::newRow("Single year unaligned start same") <<
                Time::Year <<
                1 <<
                false <<
                1262304000.0 <<
                1262304000.0 <<
                1262304000.0 <<
                1293840000.0;
        QTest::newRow("Two year unaligned start same") <<
                Time::Year <<
                2 <<
                false <<
                1262304000.0 <<
                1262304000.0 <<
                1262304000.0 <<
                1325376000.0;
        QTest::newRow("Single year unaligned start offset same") <<
                Time::Year <<
                1 <<
                false <<
                1262304020.0 <<
                1262304040.0 <<
                1262304020.0 <<
                1293840020.0;
        QTest::newRow("Single year unaligned start offset before") <<
                Time::Year <<
                1 <<
                false <<
                1262304020.0 <<
                1230768040.0 <<
                1230768020.0 <<
                1262304020.0;
        QTest::newRow("Single year unaligned start offset advance") <<
                Time::Year <<
                1 <<
                false <<
                1230768020.0 <<
                1262304050.0 <<
                1262304020.0 <<
                1293840020.0;
    }

    void totalBounds()
    {
        QFETCH(TimeCycleInterval, interval);
        QFETCH(TimeCycleDivision, division);
        QFETCH(double, expectedLower);
        QFETCH(double, expectedUpper);

        double lower = -1;
        double upper = -2;
        division.getTotalBounds(lower, upper, interval);
        QCOMPARE(lower, expectedLower);
        QCOMPARE(upper, expectedUpper);
    }

    void totalBounds_data()
    {
        QTest::addColumn<TimeCycleInterval>("interval");
        QTest::addColumn<TimeCycleDivision>("division");
        QTest::addColumn<double>("expectedLower");
        QTest::addColumn<double>("expectedUpper");

        QTest::newRow("Year-Year") <<
                TimeCycleInterval(Time::Year, 1) <<
                TimeCycleDivision(Time::Year, 1) <<
                0.0 <<
                1.0;
        QTest::newRow("Year-Quarter") <<
                TimeCycleInterval(Time::Year, 1) <<
                TimeCycleDivision(Time::Quarter, 1) <<
                0.0 <<
                4.0;
        QTest::newRow("Year-2Quarter") <<
                TimeCycleInterval(Time::Year, 1) <<
                TimeCycleDivision(Time::Quarter, 2) <<
                0.0 <<
                2.0;
        QTest::newRow("Year-Month") <<
                TimeCycleInterval(Time::Year, 1) <<
                TimeCycleDivision(Time::Month, 1) <<
                0.0 <<
                12.0;
        QTest::newRow("Year-2Month") <<
                TimeCycleInterval(Time::Year, 1) <<
                TimeCycleDivision(Time::Month, 2) <<
                0.0 <<
                6.0;
        QTest::newRow("Year-Week") <<
                TimeCycleInterval(Time::Year, 1) <<
                TimeCycleDivision(Time::Week, 1) <<
                0.0 <<
                53.0;
        QTest::newRow("Year-2Week") <<
                TimeCycleInterval(Time::Year, 1) <<
                TimeCycleDivision(Time::Week, 2) <<
                0.0 <<
                26.5;
        QTest::newRow("Year-Day") <<
                TimeCycleInterval(Time::Year, 1) <<
                TimeCycleDivision(Time::Day, 1) <<
                1.0 <<
                367.0;
        QTest::newRow("Year-2Day") <<
                TimeCycleInterval(Time::Year, 1) <<
                TimeCycleDivision(Time::Day, 2) <<
                0.0 <<
                183.0;
        QTest::newRow("Year-Hour") <<
                TimeCycleInterval(Time::Year, 1) <<
                TimeCycleDivision(Time::Hour, 1) <<
                0.0 <<
                8784.0;
        QTest::newRow("Year-2Hour") <<
                TimeCycleInterval(Time::Year, 1) <<
                TimeCycleDivision(Time::Hour, 2) <<
                0.0 <<
                4392.0;
        QTest::newRow("Year-Minute") <<
                TimeCycleInterval(Time::Year, 1) <<
                TimeCycleDivision(Time::Minute, 1) <<
                0.0 <<
                527040.0;
        QTest::newRow("Year-2Minute") <<
                TimeCycleInterval(Time::Year, 1) <<
                TimeCycleDivision(Time::Minute, 2) <<
                0.0 <<
                263520.0;
        QTest::newRow("Year-Second") <<
                TimeCycleInterval(Time::Year, 1) <<
                TimeCycleDivision(Time::Second, 1) <<
                0.0 <<
                31622400.0;
        QTest::newRow("Year-2Second") <<
                TimeCycleInterval(Time::Year, 1) <<
                TimeCycleDivision(Time::Second, 2) <<
                0.0 <<
                15811200.0;
        QTest::newRow("Year-Millisecond") <<
                TimeCycleInterval(Time::Year, 1) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                31622400000.0;
        QTest::newRow("Year-2Millisecond") <<
                TimeCycleInterval(Time::Year, 1) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                15811200000.0;

        QTest::newRow("2Year-2Year") <<
                TimeCycleInterval(Time::Year, 2) <<
                TimeCycleDivision(Time::Year, 2) <<
                0.0 <<
                1.0;
        QTest::newRow("4Year-2Year") <<
                TimeCycleInterval(Time::Year, 4) <<
                TimeCycleDivision(Time::Year, 2) <<
                0.0 <<
                2.0;
        QTest::newRow("2Year-Quarter") <<
                TimeCycleInterval(Time::Year, 2) <<
                TimeCycleDivision(Time::Quarter, 1) <<
                0.0 <<
                8.0;
        QTest::newRow("2Year-2Quarter") <<
                TimeCycleInterval(Time::Year, 2) <<
                TimeCycleDivision(Time::Quarter, 2) <<
                0.0 <<
                4.0;
        QTest::newRow("2Year-Month") <<
                TimeCycleInterval(Time::Year, 2) <<
                TimeCycleDivision(Time::Month, 1) <<
                0.0 <<
                24.0;
        QTest::newRow("2Year-2Month") <<
                TimeCycleInterval(Time::Year, 2) <<
                TimeCycleDivision(Time::Month, 2) <<
                0.0 <<
                12.0;
        QTest::newRow("2Year-Week") <<
                TimeCycleInterval(Time::Year, 2) <<
                TimeCycleDivision(Time::Week, 1) <<
                0.0 <<
                106.0;
        QTest::newRow("2Year-2Week") <<
                TimeCycleInterval(Time::Year, 2) <<
                TimeCycleDivision(Time::Week, 2) <<
                0.0 <<
                53.0;
        QTest::newRow("2Year-Day") <<
                TimeCycleInterval(Time::Year, 2) <<
                TimeCycleDivision(Time::Day, 1) <<
                0.0 <<
                732.0;
        QTest::newRow("2Year-2Day") <<
                TimeCycleInterval(Time::Year, 2) <<
                TimeCycleDivision(Time::Day, 2) <<
                0.0 <<
                366.0;
        QTest::newRow("2Year-Hour") <<
                TimeCycleInterval(Time::Year, 2) <<
                TimeCycleDivision(Time::Hour, 1) <<
                0.0 <<
                17568.0;
        QTest::newRow("2Year-2Hour") <<
                TimeCycleInterval(Time::Year, 2) <<
                TimeCycleDivision(Time::Hour, 2) <<
                0.0 <<
                8784.0;
        QTest::newRow("2Year-Minute") <<
                TimeCycleInterval(Time::Year, 2) <<
                TimeCycleDivision(Time::Minute, 1) <<
                0.0 <<
                1054080.0;
        QTest::newRow("2Year-2Minute") <<
                TimeCycleInterval(Time::Year, 2) <<
                TimeCycleDivision(Time::Minute, 2) <<
                0.0 <<
                527040.0;
        QTest::newRow("2Year-Second") <<
                TimeCycleInterval(Time::Year, 2) <<
                TimeCycleDivision(Time::Second, 1) <<
                0.0 <<
                63244800.0;
        QTest::newRow("2Year-2Second") <<
                TimeCycleInterval(Time::Year, 2) <<
                TimeCycleDivision(Time::Second, 2) <<
                0.0 <<
                31622400.0;
        QTest::newRow("2Year-Millisecond") <<
                TimeCycleInterval(Time::Year, 2) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                63244800000.0;
        QTest::newRow("2Year-2Millisecond") <<
                TimeCycleInterval(Time::Year, 2) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                31622400000.0;

        QTest::newRow("Quarter-Quarter") <<
                TimeCycleInterval(Time::Quarter, 1) <<
                TimeCycleDivision(Time::Quarter, 1) <<
                0.0 <<
                1.0;
        QTest::newRow("Quarter-Month") <<
                TimeCycleInterval(Time::Quarter, 1) <<
                TimeCycleDivision(Time::Month, 1) <<
                0.0 <<
                3.0;
        QTest::newRow("Quarter-2Month") <<
                TimeCycleInterval(Time::Quarter, 1) <<
                TimeCycleDivision(Time::Month, 2) <<
                0.0 <<
                1.5;
        QTest::newRow("Quarter-Week") <<
                TimeCycleInterval(Time::Quarter, 1) <<
                TimeCycleDivision(Time::Week, 1) <<
                0.0 <<
                14.0;
        QTest::newRow("Quarter-2Week") <<
                TimeCycleInterval(Time::Quarter, 1) <<
                TimeCycleDivision(Time::Week, 2) <<
                0.0 <<
                7.0;
        QTest::newRow("Quarter-Day") <<
                TimeCycleInterval(Time::Quarter, 1) <<
                TimeCycleDivision(Time::Day, 1) <<
                0.0 <<
                92.0;
        QTest::newRow("Quarter-2Day") <<
                TimeCycleInterval(Time::Quarter, 1) <<
                TimeCycleDivision(Time::Day, 2) <<
                0.0 <<
                46.0;
        QTest::newRow("Quarter-Hour") <<
                TimeCycleInterval(Time::Quarter, 1) <<
                TimeCycleDivision(Time::Hour, 1) <<
                0.0 <<
                2208.0;
        QTest::newRow("Quarter-2Hour") <<
                TimeCycleInterval(Time::Quarter, 1) <<
                TimeCycleDivision(Time::Hour, 2) <<
                0.0 <<
                1104.0;
        QTest::newRow("Quarter-Minute") <<
                TimeCycleInterval(Time::Quarter, 1) <<
                TimeCycleDivision(Time::Minute, 1) <<
                0.0 <<
                132480.0;
        QTest::newRow("Quarter-2Minute") <<
                TimeCycleInterval(Time::Quarter, 1) <<
                TimeCycleDivision(Time::Minute, 2) <<
                0.0 <<
                66240.0;
        QTest::newRow("Quarter-Second") <<
                TimeCycleInterval(Time::Quarter, 1) <<
                TimeCycleDivision(Time::Second, 1) <<
                0.0 <<
                7948800.0;
        QTest::newRow("Quarter-2Second") <<
                TimeCycleInterval(Time::Quarter, 1) <<
                TimeCycleDivision(Time::Second, 2) <<
                0.0 <<
                3974400.0;
        QTest::newRow("Quarter-Millisecond") <<
                TimeCycleInterval(Time::Quarter, 1) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                7948800000.0;
        QTest::newRow("Quarter-2Millisecond") <<
                TimeCycleInterval(Time::Quarter, 1) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                3974400000.0;

        QTest::newRow("2Quarter-2Quarter") <<
                TimeCycleInterval(Time::Quarter, 2) <<
                TimeCycleDivision(Time::Quarter, 2) <<
                0.0 <<
                1.0;
        QTest::newRow("4Quarter-2Quarter") <<
                TimeCycleInterval(Time::Quarter, 4) <<
                TimeCycleDivision(Time::Quarter, 2) <<
                0.0 <<
                2.0;
        QTest::newRow("2Quarter-Month") <<
                TimeCycleInterval(Time::Quarter, 2) <<
                TimeCycleDivision(Time::Month, 1) <<
                0.0 <<
                6.0;
        QTest::newRow("2Quarter-2Month") <<
                TimeCycleInterval(Time::Quarter, 2) <<
                TimeCycleDivision(Time::Month, 2) <<
                0.0 <<
                3.0;
        QTest::newRow("2Quarter-Week") <<
                TimeCycleInterval(Time::Quarter, 2) <<
                TimeCycleDivision(Time::Week, 1) <<
                0.0 <<
                28.0;
        QTest::newRow("2Quarter-2Week") <<
                TimeCycleInterval(Time::Quarter, 2) <<
                TimeCycleDivision(Time::Week, 2) <<
                0.0 <<
                14.0;
        QTest::newRow("2Quarter-Day") <<
                TimeCycleInterval(Time::Quarter, 2) <<
                TimeCycleDivision(Time::Day, 1) <<
                0.0 <<
                184.0;
        QTest::newRow("2Quarter-2Day") <<
                TimeCycleInterval(Time::Quarter, 2) <<
                TimeCycleDivision(Time::Day, 2) <<
                0.0 <<
                92.0;
        QTest::newRow("2Quarter-Hour") <<
                TimeCycleInterval(Time::Quarter, 2) <<
                TimeCycleDivision(Time::Hour, 1) <<
                0.0 <<
                4416.0;
        QTest::newRow("2Quarter-2Hour") <<
                TimeCycleInterval(Time::Quarter, 2) <<
                TimeCycleDivision(Time::Hour, 2) <<
                0.0 <<
                2208.0;
        QTest::newRow("2Quarter-Minute") <<
                TimeCycleInterval(Time::Quarter, 2) <<
                TimeCycleDivision(Time::Minute, 1) <<
                0.0 <<
                264960.0;
        QTest::newRow("2Quarter-2Minute") <<
                TimeCycleInterval(Time::Quarter, 2) <<
                TimeCycleDivision(Time::Minute, 2) <<
                0.0 <<
                132480.0;
        QTest::newRow("2Quarter-Second") <<
                TimeCycleInterval(Time::Quarter, 2) <<
                TimeCycleDivision(Time::Second, 1) <<
                0.0 <<
                15897600.0;
        QTest::newRow("2Quarter-2Second") <<
                TimeCycleInterval(Time::Quarter, 2) <<
                TimeCycleDivision(Time::Second, 2) <<
                0.0 <<
                7948800.0;
        QTest::newRow("2Quarter-Millisecond") <<
                TimeCycleInterval(Time::Quarter, 2) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                15897600000.0;
        QTest::newRow("2Quarter-2Millisecond") <<
                TimeCycleInterval(Time::Quarter, 2) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                7948800000.0;

        QTest::newRow("Month-Month") <<
                TimeCycleInterval(Time::Month, 1) <<
                TimeCycleDivision(Time::Month, 1) <<
                0.0 <<
                1.0;
        QTest::newRow("Month-Week") <<
                TimeCycleInterval(Time::Month, 1) <<
                TimeCycleDivision(Time::Week, 1) <<
                0.0 <<
                4.43;
        QTest::newRow("Month-2Week") <<
                TimeCycleInterval(Time::Month, 1) <<
                TimeCycleDivision(Time::Week, 2) <<
                0.0 <<
                2.215;
        QTest::newRow("Month-Day") <<
                TimeCycleInterval(Time::Month, 1) <<
                TimeCycleDivision(Time::Day, 1) <<
                1.0 <<
                32.0;
        QTest::newRow("Month-2Day") <<
                TimeCycleInterval(Time::Month, 1) <<
                TimeCycleDivision(Time::Day, 2) <<
                0.0 <<
                15.5;
        QTest::newRow("Month-Hour") <<
                TimeCycleInterval(Time::Month, 1) <<
                TimeCycleDivision(Time::Hour, 1) <<
                0.0 <<
                744.0;
        QTest::newRow("Month-2Hour") <<
                TimeCycleInterval(Time::Month, 1) <<
                TimeCycleDivision(Time::Hour, 2) <<
                0.0 <<
                372.0;
        QTest::newRow("Month-Minute") <<
                TimeCycleInterval(Time::Month, 1) <<
                TimeCycleDivision(Time::Minute, 1) <<
                0.0 <<
                44640.0;
        QTest::newRow("Month-2Minute") <<
                TimeCycleInterval(Time::Month, 1) <<
                TimeCycleDivision(Time::Minute, 2) <<
                0.0 <<
                22320.0;
        QTest::newRow("Month-Second") <<
                TimeCycleInterval(Time::Month, 1) <<
                TimeCycleDivision(Time::Second, 1) <<
                0.0 <<
                2678400.0;
        QTest::newRow("Month-2Second") <<
                TimeCycleInterval(Time::Month, 1) <<
                TimeCycleDivision(Time::Second, 2) <<
                0.0 <<
                1339200.0;
        QTest::newRow("Month-Millisecond") <<
                TimeCycleInterval(Time::Month, 1) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                2678400000.0;
        QTest::newRow("Month-2Millisecond") <<
                TimeCycleInterval(Time::Month, 1) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                1339200000.0;

        QTest::newRow("2Month-2Month") <<
                TimeCycleInterval(Time::Month, 2) <<
                TimeCycleDivision(Time::Month, 2) <<
                0.0 <<
                1.0;
        QTest::newRow("4Month-2Month") <<
                TimeCycleInterval(Time::Month, 4) <<
                TimeCycleDivision(Time::Month, 2) <<
                0.0 <<
                2.0;
        QTest::newRow("2Month-Week") <<
                TimeCycleInterval(Time::Month, 2) <<
                TimeCycleDivision(Time::Week, 1) <<
                0.0 <<
                8.86;
        QTest::newRow("2Month-2Week") <<
                TimeCycleInterval(Time::Month, 2) <<
                TimeCycleDivision(Time::Week, 2) <<
                0.0 <<
                4.43;
        QTest::newRow("2Month-Day") <<
                TimeCycleInterval(Time::Month, 2) <<
                TimeCycleDivision(Time::Day, 1) <<
                0.0 <<
                62.0;
        QTest::newRow("2Month-2Day") <<
                TimeCycleInterval(Time::Month, 2) <<
                TimeCycleDivision(Time::Day, 2) <<
                0.0 <<
                31.0;
        QTest::newRow("2Month-Hour") <<
                TimeCycleInterval(Time::Month, 2) <<
                TimeCycleDivision(Time::Hour, 1) <<
                0.0 <<
                1488.0;
        QTest::newRow("2Month-2Hour") <<
                TimeCycleInterval(Time::Month, 2) <<
                TimeCycleDivision(Time::Hour, 2) <<
                0.0 <<
                744.0;
        QTest::newRow("2Month-Minute") <<
                TimeCycleInterval(Time::Month, 2) <<
                TimeCycleDivision(Time::Minute, 1) <<
                0.0 <<
                89280.0;
        QTest::newRow("2Month-2Minute") <<
                TimeCycleInterval(Time::Month, 2) <<
                TimeCycleDivision(Time::Minute, 2) <<
                0.0 <<
                44640.0;
        QTest::newRow("2Month-Second") <<
                TimeCycleInterval(Time::Month, 2) <<
                TimeCycleDivision(Time::Second, 1) <<
                0.0 <<
                5356800.0;
        QTest::newRow("2Month-2Second") <<
                TimeCycleInterval(Time::Month, 2) <<
                TimeCycleDivision(Time::Second, 2) <<
                0.0 <<
                2678400.0;
        QTest::newRow("2Month-Millisecond") <<
                TimeCycleInterval(Time::Month, 2) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                5356800000.0;
        QTest::newRow("2Month-2Millisecond") <<
                TimeCycleInterval(Time::Month, 2) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                2678400000.0;

        QTest::newRow("Week-Week") <<
                TimeCycleInterval(Time::Week, 1) <<
                TimeCycleDivision(Time::Week, 1) <<
                0.0 <<
                1.0;
        QTest::newRow("Week-Day") <<
                TimeCycleInterval(Time::Week, 1) <<
                TimeCycleDivision(Time::Day, 1) <<
                0.0 <<
                7.0;
        QTest::newRow("Week-2Day") <<
                TimeCycleInterval(Time::Week, 1) <<
                TimeCycleDivision(Time::Day, 2) <<
                0.0 <<
                3.5;
        QTest::newRow("Week-Hour") <<
                TimeCycleInterval(Time::Week, 1) <<
                TimeCycleDivision(Time::Hour, 1) <<
                0.0 <<
                168.0;
        QTest::newRow("Week-2Hour") <<
                TimeCycleInterval(Time::Week, 1) <<
                TimeCycleDivision(Time::Hour, 2) <<
                0.0 <<
                84.0;
        QTest::newRow("Week-Minute") <<
                TimeCycleInterval(Time::Week, 1) <<
                TimeCycleDivision(Time::Minute, 1) <<
                0.0 <<
                10080.0;
        QTest::newRow("Week-2Minute") <<
                TimeCycleInterval(Time::Week, 1) <<
                TimeCycleDivision(Time::Minute, 2) <<
                0.0 <<
                5040.0;
        QTest::newRow("Week-Second") <<
                TimeCycleInterval(Time::Week, 1) <<
                TimeCycleDivision(Time::Second, 1) <<
                0.0 <<
                604800.0;
        QTest::newRow("Week-2Second") <<
                TimeCycleInterval(Time::Week, 1) <<
                TimeCycleDivision(Time::Second, 2) <<
                0.0 <<
                302400.0;
        QTest::newRow("Week-Millisecond") <<
                TimeCycleInterval(Time::Week, 1) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                604800000.0;
        QTest::newRow("Week-2Millisecond") <<
                TimeCycleInterval(Time::Week, 1) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                302400000.0;

        QTest::newRow("2Week-2Week") <<
                TimeCycleInterval(Time::Week, 2) <<
                TimeCycleDivision(Time::Week, 2) <<
                0.0 <<
                1.0;
        QTest::newRow("4Week-2Week") <<
                TimeCycleInterval(Time::Week, 4) <<
                TimeCycleDivision(Time::Week, 2) <<
                0.0 <<
                2.0;
        QTest::newRow("2Week-Day") <<
                TimeCycleInterval(Time::Week, 2) <<
                TimeCycleDivision(Time::Day, 1) <<
                0.0 <<
                14.0;
        QTest::newRow("2Week-2Day") <<
                TimeCycleInterval(Time::Week, 2) <<
                TimeCycleDivision(Time::Day, 2) <<
                0.0 <<
                7.0;
        QTest::newRow("2Week-Hour") <<
                TimeCycleInterval(Time::Week, 2) <<
                TimeCycleDivision(Time::Hour, 1) <<
                0.0 <<
                336.0;
        QTest::newRow("2Week-2Hour") <<
                TimeCycleInterval(Time::Week, 2) <<
                TimeCycleDivision(Time::Hour, 2) <<
                0.0 <<
                168.0;
        QTest::newRow("2Week-Minute") <<
                TimeCycleInterval(Time::Week, 2) <<
                TimeCycleDivision(Time::Minute, 1) <<
                0.0 <<
                20160.0;
        QTest::newRow("2Week-2Minute") <<
                TimeCycleInterval(Time::Week, 2) <<
                TimeCycleDivision(Time::Minute, 2) <<
                0.0 <<
                10080.0;
        QTest::newRow("2Week-Second") <<
                TimeCycleInterval(Time::Week, 2) <<
                TimeCycleDivision(Time::Second, 1) <<
                0.0 <<
                1209600.0;
        QTest::newRow("2Week-2Second") <<
                TimeCycleInterval(Time::Week, 2) <<
                TimeCycleDivision(Time::Second, 2) <<
                0.0 <<
                604800.0;
        QTest::newRow("2Week-Millisecond") <<
                TimeCycleInterval(Time::Week, 2) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                1209600000.0;
        QTest::newRow("2Week-2Millisecond") <<
                TimeCycleInterval(Time::Week, 2) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                604800000.0;

        QTest::newRow("Day-Day") <<
                TimeCycleInterval(Time::Day, 1) <<
                TimeCycleDivision(Time::Day, 1) <<
                0.0 <<
                1.0;
        QTest::newRow("Day-Hour") <<
                TimeCycleInterval(Time::Day, 1) <<
                TimeCycleDivision(Time::Hour, 1) <<
                0.0 <<
                24.0;
        QTest::newRow("Day-2Hour") <<
                TimeCycleInterval(Time::Day, 1) <<
                TimeCycleDivision(Time::Hour, 2) <<
                0.0 <<
                12.0;
        QTest::newRow("Day-Minute") <<
                TimeCycleInterval(Time::Day, 1) <<
                TimeCycleDivision(Time::Minute, 1) <<
                0.0 <<
                1440.0;
        QTest::newRow("Day-2Minute") <<
                TimeCycleInterval(Time::Day, 1) <<
                TimeCycleDivision(Time::Minute, 2) <<
                0.0 <<
                720.0;
        QTest::newRow("Day-Second") <<
                TimeCycleInterval(Time::Day, 1) <<
                TimeCycleDivision(Time::Second, 1) <<
                0.0 <<
                86400.0;
        QTest::newRow("Day-2Second") <<
                TimeCycleInterval(Time::Day, 1) <<
                TimeCycleDivision(Time::Second, 2) <<
                0.0 <<
                43200.0;
        QTest::newRow("Day-Millisecond") <<
                TimeCycleInterval(Time::Day, 1) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                86400000.0;
        QTest::newRow("Day-2Millisecond") <<
                TimeCycleInterval(Time::Day, 1) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                43200000.0;

        QTest::newRow("2Day-2Day") <<
                TimeCycleInterval(Time::Day, 2) <<
                TimeCycleDivision(Time::Day, 2) <<
                0.0 <<
                1.0;
        QTest::newRow("4Day-2Day") <<
                TimeCycleInterval(Time::Day, 4) <<
                TimeCycleDivision(Time::Day, 2) <<
                0.0 <<
                2.0;
        QTest::newRow("2Day-Hour") <<
                TimeCycleInterval(Time::Day, 2) <<
                TimeCycleDivision(Time::Hour, 1) <<
                0.0 <<
                48.0;
        QTest::newRow("2Day-2Hour") <<
                TimeCycleInterval(Time::Day, 2) <<
                TimeCycleDivision(Time::Hour, 2) <<
                0.0 <<
                24.0;
        QTest::newRow("2Day-Minute") <<
                TimeCycleInterval(Time::Day, 2) <<
                TimeCycleDivision(Time::Minute, 1) <<
                0.0 <<
                2880.0;
        QTest::newRow("2Day-2Minute") <<
                TimeCycleInterval(Time::Day, 2) <<
                TimeCycleDivision(Time::Minute, 2) <<
                0.0 <<
                1440.0;
        QTest::newRow("2Day-Second") <<
                TimeCycleInterval(Time::Day, 2) <<
                TimeCycleDivision(Time::Second, 1) <<
                0.0 <<
                172800.0;
        QTest::newRow("2Day-2Second") <<
                TimeCycleInterval(Time::Day, 2) <<
                TimeCycleDivision(Time::Second, 2) <<
                0.0 <<
                86400.0;
        QTest::newRow("2Day-Millisecond") <<
                TimeCycleInterval(Time::Day, 2) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                172800000.0;
        QTest::newRow("2Day-2Millisecond") <<
                TimeCycleInterval(Time::Day, 2) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                86400000.0;

        QTest::newRow("Hour-Hour") <<
                TimeCycleInterval(Time::Hour, 1) <<
                TimeCycleDivision(Time::Hour, 1) <<
                0.0 <<
                1.0;
        QTest::newRow("Hour-Minute") <<
                TimeCycleInterval(Time::Hour, 1) <<
                TimeCycleDivision(Time::Minute, 1) <<
                0.0 <<
                60.0;
        QTest::newRow("Hour-2Minute") <<
                TimeCycleInterval(Time::Hour, 1) <<
                TimeCycleDivision(Time::Minute, 2) <<
                0.0 <<
                30.0;
        QTest::newRow("Hour-Second") <<
                TimeCycleInterval(Time::Hour, 1) <<
                TimeCycleDivision(Time::Second, 1) <<
                0.0 <<
                3600.0;
        QTest::newRow("Hour-2Second") <<
                TimeCycleInterval(Time::Hour, 1) <<
                TimeCycleDivision(Time::Second, 2) <<
                0.0 <<
                1800.0;
        QTest::newRow("Hour-Millisecond") <<
                TimeCycleInterval(Time::Hour, 1) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                3600000.0;
        QTest::newRow("Hour-2Millisecond") <<
                TimeCycleInterval(Time::Hour, 1) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                1800000.0;

        QTest::newRow("2Hour-2Hour") <<
                TimeCycleInterval(Time::Hour, 2) <<
                TimeCycleDivision(Time::Hour, 2) <<
                0.0 <<
                1.0;
        QTest::newRow("4Hour-2Hour") <<
                TimeCycleInterval(Time::Hour, 4) <<
                TimeCycleDivision(Time::Hour, 2) <<
                0.0 <<
                2.0;
        QTest::newRow("2Hour-Minute") <<
                TimeCycleInterval(Time::Hour, 2) <<
                TimeCycleDivision(Time::Minute, 1) <<
                0.0 <<
                120.0;
        QTest::newRow("2Hour-2Minute") <<
                TimeCycleInterval(Time::Hour, 2) <<
                TimeCycleDivision(Time::Minute, 2) <<
                0.0 <<
                60.0;
        QTest::newRow("2Hour-Second") <<
                TimeCycleInterval(Time::Hour, 2) <<
                TimeCycleDivision(Time::Second, 1) <<
                0.0 <<
                7200.0;
        QTest::newRow("2Hour-2Second") <<
                TimeCycleInterval(Time::Hour, 2) <<
                TimeCycleDivision(Time::Second, 2) <<
                0.0 <<
                3600.0;
        QTest::newRow("2Hour-Millisecond") <<
                TimeCycleInterval(Time::Hour, 2) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                7200000.0;
        QTest::newRow("2Hour-2Millisecond") <<
                TimeCycleInterval(Time::Hour, 2) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                3600000.0;

        QTest::newRow("Minute-Minute") <<
                TimeCycleInterval(Time::Minute, 1) <<
                TimeCycleDivision(Time::Minute, 1) <<
                0.0 <<
                1.0;
        QTest::newRow("Minute-Second") <<
                TimeCycleInterval(Time::Minute, 1) <<
                TimeCycleDivision(Time::Second, 1) <<
                0.0 <<
                60.0;
        QTest::newRow("Minute-2Second") <<
                TimeCycleInterval(Time::Minute, 1) <<
                TimeCycleDivision(Time::Second, 2) <<
                0.0 <<
                30.0;
        QTest::newRow("Minute-Millisecond") <<
                TimeCycleInterval(Time::Minute, 1) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                60000.0;
        QTest::newRow("Minute-2Millisecond") <<
                TimeCycleInterval(Time::Minute, 1) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                30000.0;

        QTest::newRow("2Minute-2Minute") <<
                TimeCycleInterval(Time::Minute, 2) <<
                TimeCycleDivision(Time::Minute, 2) <<
                0.0 <<
                1.0;
        QTest::newRow("4Minute-2Minute") <<
                TimeCycleInterval(Time::Minute, 4) <<
                TimeCycleDivision(Time::Minute, 2) <<
                0.0 <<
                2.0;
        QTest::newRow("2Minute-Second") <<
                TimeCycleInterval(Time::Minute, 2) <<
                TimeCycleDivision(Time::Second, 1) <<
                0.0 <<
                120.0;
        QTest::newRow("2Minute-2Second") <<
                TimeCycleInterval(Time::Minute, 2) <<
                TimeCycleDivision(Time::Second, 2) <<
                0.0 <<
                60.0;
        QTest::newRow("2Minute-Millisecond") <<
                TimeCycleInterval(Time::Minute, 2) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                120000.0;
        QTest::newRow("2Minute-2Millisecond") <<
                TimeCycleInterval(Time::Minute, 2) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                60000.0;

        QTest::newRow("Second-Second") <<
                TimeCycleInterval(Time::Second, 1) <<
                TimeCycleDivision(Time::Second, 1) <<
                0.0 <<
                1.0;
        QTest::newRow("Second-Millisecond") <<
                TimeCycleInterval(Time::Second, 1) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                1000.0;
        QTest::newRow("Second-2Millisecond") <<
                TimeCycleInterval(Time::Second, 1) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                500.0;

        QTest::newRow("2Second-2Second") <<
                TimeCycleInterval(Time::Second, 2) <<
                TimeCycleDivision(Time::Second, 2) <<
                0.0 <<
                1.0;
        QTest::newRow("4Second-2Second") <<
                TimeCycleInterval(Time::Second, 4) <<
                TimeCycleDivision(Time::Second, 2) <<
                0.0 <<
                2.0;
        QTest::newRow("2Second-Millisecond") <<
                TimeCycleInterval(Time::Second, 2) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                2000.0;
        QTest::newRow("2Second-2Millisecond") <<
                TimeCycleInterval(Time::Second, 2) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                1000.0;

        QTest::newRow("Millisecond-Millisecond") <<
                TimeCycleInterval(Time::Millisecond, 1) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                1.0;
        QTest::newRow("2Millisecond-Millisecond") <<
                TimeCycleInterval(Time::Millisecond, 2) <<
                TimeCycleDivision(Time::Millisecond, 1) <<
                0.0 <<
                2.0;
        QTest::newRow("2Millisecond-2Millisecond") <<
                TimeCycleInterval(Time::Millisecond, 2) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                1.0;
        QTest::newRow("4Millisecond-2Millisecond") <<
                TimeCycleInterval(Time::Millisecond, 4) <<
                TimeCycleDivision(Time::Millisecond, 2) <<
                0.0 <<
                2.0;
    }

    void convertTime()
    {
        QFETCH(TimeCycleInterval, interval);
        QFETCH(Time::LogicalTimeUnit, unit);
        QFETCH(int, count);
        QFETCH(double, base);
        QFETCH(double, timeFor);
        QFETCH(double, expected);

        double result;
        TimeCycleDivision div(unit, count);
        result = const_cast<const TimeCycleDivision *>(&div)->convertTime(base, timeFor, interval);
        QCOMPARE(result, expected);

        result = div.convertTime(base, timeFor, interval);
        QCOMPARE(result, expected);

        result = div.convertTime(base, timeFor, interval);
        QCOMPARE(result, expected);
    }

    void convertTime_data()
    {
        QTest::addColumn<TimeCycleInterval>("interval");
        QTest::addColumn<Time::LogicalTimeUnit>("unit");
        QTest::addColumn<int>("count");
        QTest::addColumn<double>("base");
        QTest::addColumn<double>("timeFor");
        QTest::addColumn<double>("expected");

        TimeCycleInterval interval(Time::Year, 1, true);

        QTest::newRow("Year-Year start") <<
                interval <<
                Time::Year <<
                1 <<
                1293840000.0 <<
                1293840000.0 <<
                0.0;
        QTest::newRow("Year-Year mid") <<
                interval <<
                Time::Year <<
                1 <<
                1293840000.0 <<
                1309608000.0 <<
                0.5;

        QTest::newRow("Year-Month start") <<
                interval <<
                Time::Month <<
                1 <<
                1293840000.0 <<
                1293840000.0 <<
                0.0;
        QTest::newRow("Year-Month mid") <<
                interval <<
                Time::Month <<
                1 <<
                1293840000.0 <<
                1308182400.0 <<
                5.5;

        QTest::newRow("Year-Day start") <<
                interval <<
                Time::Day <<
                1 <<
                1293840000.0 <<
                1293840000.0 <<
                1.0;
        QTest::newRow("Year-Day mid") <<
                interval <<
                Time::Day <<
                1 <<
                1293840000.0 <<
                1309608000.0 <<
                183.5;
    }

    void axisTicks()
    {
        QFETCH(double, min);
        QFETCH(double, max);
        QFETCH(TimeCycleInterval, interval);
        QFETCH(TimeCycleDivision, division);
        QFETCH(AxisTickGenerator::GenerateFlags, flags);
        QFETCH(std::vector<AxisTickGenerator::Tick>, expected);

        AxisTickGeneratorTimeCycle gen(interval, division);
        auto results = gen.generate(min, max, flags);
        QCOMPARE(results.size(), expected.size());
        for (std::size_t i = 0, max = results.size(); i < max; i++) {
            QCOMPARE(results.at(i).point, expected.at(i).point);
            QCOMPARE(results.at(i).label, expected.at(i).label);
            QCOMPARE(results.at(i).secondaryLabel, expected.at(i).secondaryLabel);
            QCOMPARE(results.at(i).blockLabel, expected.at(i).blockLabel);
        }
    }

    void axisTicks_data()
    {
        QTest::addColumn<double>("min");
        QTest::addColumn<double>("max");
        QTest::addColumn<TimeCycleInterval>("interval");
        QTest::addColumn<TimeCycleDivision>("division");
        QTest::addColumn<AxisTickGenerator::GenerateFlags>("flags");
        QTest::addColumn<std::vector<AxisTickGenerator::Tick>>("expected");

        std::vector<AxisTickGenerator::Tick> expected;
        TimeCycleInterval interval(Time::Year, 1, true);
        TimeCycleDivision division(Time::Month, 1);

        QTest::newRow("Empty major") <<
                                     0.0 <<
                                     0.0 <<
                                     interval <<
                                     division << AxisTickGenerator::MajorTick << expected;
        QTest::newRow("Empty minor") << 0.0 << 0.0 << interval << division
                                     << AxisTickGenerator::MinorTick << expected;

        expected.emplace_back(AxisTickGenerator::Tick(0, "JAN", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(2, "MAR", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(4, "MAY", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(6, "JUL", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(8, "SEP", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(10, "NOV", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(12, "", {}, {}));
        QTest::newRow("Month major skip") << 0.0 << 12.0 << interval << division
                                          << AxisTickGenerator::MajorTick << expected;

        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(1, "FEB", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(2, "MAR", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(3, "APR", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(4, "MAY", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(5, "JUN", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(6, "JUL", {}, {}));
        QTest::newRow("Month major section") << 1.1 << 5.9 << interval << division
                                             << AxisTickGenerator::MajorTick << expected;

        division = TimeCycleDivision(Time::Quarter, 1);
        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(0, "Q1", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(1, "Q2", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(2, "Q3", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(3, "Q4", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(4, "", {}, {}));
        QTest::newRow("Quarter major") << 0.0 << 4.0 << interval << division
                                       << AxisTickGenerator::MajorTick << expected;

        interval = TimeCycleInterval(Time::Week, 1, true);
        division = TimeCycleDivision(Time::Day, 1);
        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(0, "MON", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(1, "TUE", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(2, "WED", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(3, "THU", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(4, "FRI", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(5, "SAT", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(6, "SUN", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(7, "", {}, {}));
        QTest::newRow("Week major") << 0.0 << 7.0 << interval << division
                                    << AxisTickGenerator::MajorTick << expected;

        interval = TimeCycleInterval(Time::Day, 1, true);
        division = TimeCycleDivision(Time::Hour, 1);
        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(0, "0", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(3, "3", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(6, "6", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(9, "9", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(12, "12", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(15, "15", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(18, "18", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(21, "21", {}, {}));
        expected.emplace_back(AxisTickGenerator::Tick(24, "24", {}, {}));
        QTest::newRow("Day-hour major") << 0.0 << 24.0 << interval << division
                                        << AxisTickGenerator::MajorTick << expected;
    }
};

QTEST_APPLESS_MAIN(TestCycleCommon)

#include "cyclecommon.moc"
