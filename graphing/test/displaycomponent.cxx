/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>

#include "graphing/displaycomponent.hxx"
#include "graphing/graph2d.hxx"
#include "datacore/sequencematch.hxx"

using namespace CPD3;
using namespace CPD3::Graphing;
using namespace CPD3::Data;

class TestDisplayComponent : public QObject {
Q_OBJECT
private slots:

    void scatter2d()
    {
        ComponentOptions options;
        options = DisplayComponent::options(DisplayComponent::Display_Scatter2D);
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("title")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("x-title")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("y-title")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("legend")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("grid")));
        QVERIFY(qobject_cast<ComponentOptionSequenceMatch *>(options.get("x")));
        QVERIFY(qobject_cast<ComponentOptionSequenceMatch *>(options.get("y")));
        QVERIFY(qobject_cast<ComponentOptionSequenceMatch *>(options.get("z")));

        QVERIFY(!DisplayComponent::examples(DisplayComponent::Display_Scatter2D).isEmpty());

        QVERIFY(qobject_cast<Graph2D *>(
                DisplayComponent::create(DisplayComponent::Display_Scatter2D, options)));
    }
};

QTEST_APPLESS_MAIN(TestDisplayComponent)

#include "displaycomponent.moc"
