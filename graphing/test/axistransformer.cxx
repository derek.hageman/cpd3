/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>

#include "graphing/axistransformer.hxx"
#include "core/number.hxx"
#include "datacore/variant/root.hxx"

using namespace CPD3;
using namespace CPD3::Graphing;
using namespace CPD3::Data;

class TestAxisTransformer : public QObject {
Q_OBJECT
private slots:

    void simple()
    {
        AxisTransformer t;
        t.setMinExtendAbsolute(0);
        t.setMaxExtendAbsolute(0);

        t.setReal(0.0, 10.0);
        t.setScreen(-1.0, 4.0);

        QCOMPARE(t.toScreen(0.0), -1.0);
        QCOMPARE(t.toScreen(10.0), 4.0);
        QCOMPARE(t.toScreen(5.0), 1.5);
        QCOMPARE(t.toReal(-1.0), 0.0);
        QCOMPARE(t.toReal(4.0), 10.0);
        QCOMPARE(t.toReal(1.5), 5.0);

        t = AxisTransformer(Variant::Read::empty());
        t.setMinExtendAbsolute(0);
        t.setMaxExtendAbsolute(0);
        t.setReal(0.0, 10.0);
        t.setScreen(-1.0, 4.0);

        QCOMPARE(t.toScreen(0.0), -1.0);
        QCOMPARE(t.toScreen(10.0), 4.0);
        QCOMPARE(t.toScreen(5.0), 1.5);
        QCOMPARE(t.toReal(-1.0), 0.0);
        QCOMPARE(t.toReal(4.0), 10.0);
        QCOMPARE(t.toReal(1.5), 5.0);

        t.setScreen(4.0, -1.0);
        QCOMPARE(t.toScreen(0.0), 4.0);
        QCOMPARE(t.toScreen(10.0), -1.0);
        QCOMPARE(t.toScreen(5.0), 1.5);
        QCOMPARE(t.toReal(-1.0), 10.0);
        QCOMPARE(t.toReal(4.0), 0.0);
        QCOMPARE(t.toReal(1.5), 5.0);

        double min = 0.0;
        double max = 10.0;
        t.setReal(min, max);
        QCOMPARE(min, 0.0);
        QCOMPARE(max, 10.0);


        min = -20.0;
        max = 20.0;
        t.setReal(min, max, 0.0, 10.0);
        t.setScreen(-1.0, 4.0);
        QCOMPARE(min, 0.0);
        QCOMPARE(max, 10.0);
        QCOMPARE(t.toScreen(0.0), -1.0);
        QCOMPARE(t.toScreen(10.0), 4.0);
        QCOMPARE(t.toScreen(5.0), 1.5);
        QCOMPARE(t.toReal(-1.0), 0.0);
        QCOMPARE(t.toReal(4.0), 10.0);
        QCOMPARE(t.toReal(1.5), 5.0);
    }

    void logarithmic()
    {
        AxisTransformer t;
        t.setLogarithmic(true);
        QVERIFY(t.isLogarithmic());
        t.setMinExtendAbsolute(0);
        t.setMaxExtendAbsolute(0);

        t.setReal(0.1, 10.0);
        t.setScreen(10.0, 20.0);

        QCOMPARE(t.toScreen(0.1), 10.0);
        QCOMPARE(t.toScreen(10.0), 20.0);
        QCOMPARE(t.toScreen(pow(10.0, 0.5)), 17.5);
        QCOMPARE(t.toReal(10.0), 0.1);
        QCOMPARE(t.toReal(20.0), 10.0);
        QCOMPARE(t.toReal(17.5), pow(10.0, 0.5));

        Variant::Root config;
        config["Log"].setBool(true);
        t = AxisTransformer(config);
        QVERIFY(t.isLogarithmic());
        t.setMinExtendAbsolute(0);
        t.setMaxExtendAbsolute(0);

        t.setReal(0.1, 10.0);
        t.setScreen(10.0, 20.0);

        QCOMPARE(t.toScreen(0.1), 10.0);
        QCOMPARE(t.toScreen(10.0), 20.0);
        QCOMPARE(t.toScreen(pow(10.0, 0.5)), 17.5);
        QCOMPARE(t.toReal(10.0), 0.1);
        QCOMPARE(t.toReal(20.0), 10.0);
        QCOMPARE(t.toReal(17.5), pow(10.0, 0.5));
    }

    void extend()
    {
        AxisTransformer t;

        t.setMinFixed(-1.0);
        t.setMaxFixed(11.0);
        double min = 0.0;
        double max = 10.0;
        t.setReal(min, max);
        QCOMPARE(min, -1.0);
        QCOMPARE(max, 11.0);

        Variant::Root config;
        config["Minimum/Type"].setString("Fixed");
        config["Minimum/Value"].setDouble(1.0);
        config["Maximum/Type"].setString("Fixed");
        config["Maximum/Value"].setDouble(9.0);
        t = AxisTransformer(config);
        min = 0.0;
        max = 10.0;
        t.setReal(min, max);
        QCOMPARE(min, 1.0);
        QCOMPARE(max, 9.0);

        t.setMinExtendFraction(0.2);
        t.setMaxExtendFraction(0.2);
        min = 0.0;
        max = 10.0;
        t.setReal(min, max);
        QCOMPARE(min, -2.0);
        QCOMPARE(max, 12.0);
        config.write().setEmpty();
        config["Minimum/Type"].setString("ExtendFraction");
        config["Minimum/Fraction"].setDouble(0.1);
        config["Maximum/Type"].setString("ExtendFraction");
        config["Maximum/Fraction"].setDouble(0.1);
        t = AxisTransformer(config);
        min = 0.0;
        max = 10.0;
        t.setReal(min, max);
        QCOMPARE(min, -1.0);
        QCOMPARE(max, 11.0);

        t.setMinExtendFraction(0.2, 3.0);
        t.setMaxExtendFraction(0.2, 3.0);
        min = 0.0;
        max = 10.0;
        t.setReal(min, max);
        QCOMPARE(min, -3.0);
        QCOMPARE(max, 13.0);

        config.write().setEmpty();
        config["Minimum/Type"].setString("ExtendFraction");
        config["Minimum/Fraction"].setDouble(0.1);
        config["Minimum/AtLeast"].setDouble(1.5);
        config["Maximum/Type"].setString("ExtendFraction");
        config["Maximum/Fraction"].setDouble(0.1);
        config["Maximum/AtLeast"].setDouble(1.5);
        t = AxisTransformer(config);
        min = 0.0;
        max = 10.0;
        t.setReal(min, max);
        QCOMPARE(min, -1.5);
        QCOMPARE(max, 11.5);

        t.setMinExtendFraction(0.2, 0.25, 1.0);
        t.setMaxExtendFraction(0.2, 0.25, 1.0);
        min = 0.0;
        max = 10.0;
        t.setReal(min, max);
        QCOMPARE(min, -1.0);
        QCOMPARE(max, 11.0);

        config.write().setEmpty();
        config["Minimum/Type"].setString("ExtendFraction");
        config["Minimum/Fraction"].setDouble(0.1);
        config["Minimum/AtMost"].setDouble(0.5);
        config["Maximum/Type"].setString("ExtendFraction");
        config["Maximum/Fraction"].setDouble(0.1);
        config["Maximum/AtMost"].setDouble(0.5);
        t = AxisTransformer(config);
        min = 0.0;
        max = 10.0;
        t.setReal(min, max);
        QCOMPARE(min, -0.5);
        QCOMPARE(max, 10.5);

        t.setMinExtendAbsolute(2.0);
        t.setMaxExtendAbsolute(2.0);
        min = 0.0;
        max = 10.0;
        t.setReal(min, max);
        QCOMPARE(min, -2.0);
        QCOMPARE(max, 12.0);

        config.write().setEmpty();
        config["Minimum/Type"].setString("ExtendAbsolute");
        config["Minimum/Amount"].setDouble(1.0);
        config["Maximum/Type"].setString("ExtendAbsolute");
        config["Maximum/Amount"].setDouble(1.0);
        t = AxisTransformer(config);
        min = 0.0;
        max = 10.0;
        t.setReal(min, max);
        QCOMPARE(min, -1.0);
        QCOMPARE(max, 11.0);

        t.setMinExtendPowerRound(10.0);
        t.setMaxExtendPowerRound(10.0);
        min = 0.15;
        max = 9.0;
        t.setReal(min, max);
        QCOMPARE(min, 0.1);
        QCOMPARE(max, 10.0);

        config.write().setEmpty();
        config["Minimum/Type"].setString("PowerRound");
        config["Minimum/Base"].setDouble(2.0);
        config["Maximum/Type"].setString("PowerRound");
        config["Maximum/Base"].setDouble(2.0);
        t = AxisTransformer(config);
        min = 0.75;
        max = 7.0;
        t.setReal(min, max);
        QCOMPARE(min, 0.5);
        QCOMPARE(max, 8.0);

        t.setAbsoluteMin(3.0);
        t.setAbsoluteMax(7.0);
        t.setReal(min, max);
        QCOMPARE(min, 3.0);
        QCOMPARE(max, 7.0);

        config["Minimum/Absolute"].setDouble(4.0);
        config["Maximum/Absolute"].setDouble(6.0);
        t = AxisTransformer(config);
        t.setReal(min, max);
        QCOMPARE(min, 4.0);
        QCOMPARE(max, 6.0);

        t.setReal(min, max, 0.0, 10.0);
        QCOMPARE(min, 0.0);
        QCOMPARE(max, 10.0);

        config.write().setEmpty();
        config["Minimum/Type"].setString("ExtendAbsolute");
        config["Minimum/Amount"].setDouble(1.0);
        config["Maximum/Type"].setString("ExtendAbsolute");
        config["Maximum/Amount"].setDouble(1.0);
        t = AxisTransformer(config);
        t.setRequiredMax(10.0);
        t.setRequiredMin(1.0);
        min = 3.0;
        max = 4.0;
        t.setReal(min, max);
        QCOMPARE(min, 1.0);
        QCOMPARE(max, 10.0);
        config["Minimum/Required"].setDouble(0.0);
        config["Maximum/Required"].setDouble(11.0);
        t = AxisTransformer(config);
        min = 5.0;
        max = 7.0;
        t.setReal(min, max);
        QCOMPARE(min, 0.0);
        QCOMPARE(max, 11.0);
    }
};

QTEST_MAIN(TestAxisTransformer)

#include "axistransformer.moc"
