/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtAlgorithms>
#include <QObject>
#include <QString>
#include <QTest>

#include "graphing/binset.hxx"
#include "datacore/stream.hxx"
#include "datacore/processingtapchain.hxx"
#include "datacore/segment.hxx"
#include "datacore/variant/root.hxx"
#include "core/number.hxx"
#include "algorithms/statistics.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Graphing;
using namespace CPD3::Algorithms;

class DispatchBackend : public ProcessingTapChain::ArchiveBackend {
public:
    SequenceValue::Transfer values;

    DispatchBackend(const SequenceValue::Transfer &sv) : values(sv)
    { }

    virtual ~DispatchBackend()
    { }

    void issueArchiveRead(const Archive::Selection::List &, StreamSink *target) override
    {
        if (!values.empty())
            target->incomingData(values);
        target->endData();
    }
};

typedef BinPoint<1, 1> BinPoint11;

Q_DECLARE_METATYPE(BinPoint11);

Q_DECLARE_METATYPE(std::vector<BinPoint11>);
typedef BinPoint<2, 2> BinPoint22;

Q_DECLARE_METATYPE(BinPoint22);

Q_DECLARE_METATYPE(TraceLimits<2>);

Q_DECLARE_METATYPE(std::vector<BinPoint22>);

Q_DECLARE_METATYPE(TraceLimits<4>);

template<std::size_t I, std::size_t D>
static void setQuantiles(const std::vector<double> &sorted,
                         double qWhisker,
                         double qBox,
                         int dim,
                         BinPoint<I, D> &out)
{
    out.lowest[dim] = Statistics::quantile(sorted, qWhisker);
    out.uppermost[dim] = Statistics::quantile(sorted, 1.0 - qWhisker);
    out.lower[dim] = Statistics::quantile(sorted, qBox);
    out.upper[dim] = Statistics::quantile(sorted, 1.0 - qBox);
    out.middle[dim] = Statistics::quantile(sorted, 0.5);
    out.total[dim] = sorted.size();
}

template<int I, int D>
static bool sortBinPoints(const BinPoint<I, D> &a, const BinPoint<I, D> &b)
{
    for (int i = 0; i < I; i++) {
        if (!FP::defined(a.center[i])) {
            if (FP::defined(b.center[i]))
                return true;
        } else if (!FP::defined(b.center[i])) {
            return false;
        } else if (a.center[i] != b.center[i]) {
            return a.center[i] < b.center[i];
        }
    }
    return false;
}

class TestBinSet : public QObject {
Q_OBJECT
private slots:

    void one()
    {
        QFETCH(Variant::Root, configuration);
        QFETCH(SequenceValue::Transfer, values);
        QFETCH(std::vector<BinPoint11>, points);
        QFETCH(TraceLimits<2>, limits);

        BinDispatchSet<1, 1> set;
        set.initialize(ValueSegment::Transfer{
                ValueSegment(FP::undefined(), FP::undefined(), configuration)});

        DispatchBackend backend(values);
        ProcessingTapChain chain;
        chain.setBackend(&backend);
        set.registerChain(&chain);
        chain.start();
        chain.wait();
        set.process();

        auto handlers = set.getHandlers();
        QCOMPARE((int) handlers.size(), 1);

        BinValueHandler<1, 1> *handler = static_cast<BinValueHandler<1, 1> *>(
                handlers[0]);
        QCOMPARE(handler->getPoints(), points);
        QCOMPARE(handler->getLimits(), limits);
    }

    void one_data()
    {
        QTest::addColumn<Variant::Root>("configuration");
        QTest::addColumn<SequenceValue::Transfer>("values");
        QTest::addColumn<std::vector<BinPoint11> >("points");
        QTest::addColumn<TraceLimits<2> >("limits");

        Variant::Root config;
        config["All/Data/Dimensions/#0/Input"].setString("Ind");
        config["All/Data/Dimensions/#1/Input"].setString("Dep");
        config["All/Settings/Binning/Type"].setString("Uniform");
        config["All/Settings/Binning/Count"].setInt64(3);

        SequenceValue::Transfer values
                {SequenceValue(SequenceName("brw", "raw", "Ind"), Variant::Root(0.8), 100.0, 101.0),
                 SequenceValue(SequenceName("brw", "raw", "Dep"), Variant::Root(1.0), 100.0, 101.0),
                 SequenceValue(SequenceName("brw", "raw", "Ind"), Variant::Root(0.9), 101.0, 102.0),
                 SequenceValue(SequenceName("brw", "raw", "Dep"), Variant::Root(2.0), 101.0, 102.0),
                 SequenceValue(SequenceName("brw", "raw", "Ind"), Variant::Root(0.0), 102.0, 103.0),
                 SequenceValue(SequenceName("brw", "raw", "Dep"), Variant::Root(3.0), 102.0, 103.0),
                 SequenceValue(SequenceName("brw", "raw", "Ind"), Variant::Root(1.5), 200.0, 201.0),
                 SequenceValue(SequenceName("brw", "raw", "Dep"), Variant::Root(10.0), 200.0,
                               201.0),
                 SequenceValue(SequenceName("brw", "raw", "Ind"), Variant::Root(1.6), 201.0, 202.0),
                 SequenceValue(SequenceName("brw", "raw", "Dep"), Variant::Root(20.0), 201.0,
                               202.0),
                 SequenceValue(SequenceName("brw", "raw", "Ind"), Variant::Root(1.4), 202.0, 203.0),
                 SequenceValue(SequenceName("brw", "raw", "Dep"), Variant::Root(30.0), 202.0,
                               203.0),
                 SequenceValue(SequenceName("brw", "raw", "Ind"), Variant::Root(3.0), 300.0, 301.0),
                 SequenceValue(SequenceName("brw", "raw", "Dep"), Variant::Root(0.1), 300.0, 301.0),
                 SequenceValue(SequenceName("brw", "raw", "Ind"), Variant::Root(2.9), 301.0, 302.0),
                 SequenceValue(SequenceName("brw", "raw", "Dep"), Variant::Root(0.2), 301.0, 302.0),
                 SequenceValue(SequenceName("brw", "raw", "Ind"), Variant::Root(2.8), 302.0, 303.0),
                 SequenceValue(SequenceName("brw", "raw", "Dep"), Variant::Root(0.3), 302.0,
                               303.0)};

        TraceLimits<2> limits;
        limits.min[0] = 0.0;
        limits.max[0] = 3.0;

        std::vector<BinPoint11> points;
        BinPoint11 point;
        point.center[0] = 0.5;
        point.width[0] = 1.0;
        setQuantiles(std::vector<double>{1.0, 2.0, 3.0}, 0.05, 0.25, 0, point);
        points.emplace_back(point);
        point.center[0] = 1.5;
        setQuantiles(std::vector<double>{10.0, 20.0, 30.0}, 0.05, 0.25, 0, point);
        limits.max[1] = point.uppermost[0];
        points.emplace_back(point);
        point.center[0] = 2.5;
        setQuantiles(std::vector<double>{0.1, 0.2, 0.3}, 0.05, 0.25, 0, point);
        limits.min[1] = point.lowest[0];
        points.emplace_back(point);

        QTest::newRow("Basic fixed") << config << values << points << limits;

        config["All/Settings/Binning/#0/Type"].setString("Uniform");
        config["All/Settings/Binning/#0/Count"].setInt64(3);
        QTest::newRow("Basic fixed array") << config << values << points << limits;

        config["All/Settings/Binning/Type"].setString("Size");
        config["All/Settings/Binning/Width"].setDouble(1.0);
        QTest::newRow("Basic width") << config << values << points << limits;

        config["All/Settings/Binning/Type"].setString("Static");
        config["All/Settings/Binning/Bins/#0/Start"].setDouble(0.0);
        config["All/Settings/Binning/Bins/#0/End"].setDouble(1.0);
        config["All/Settings/Binning/Bins/#1/Start"].setDouble(1.0);
        config["All/Settings/Binning/Bins/#1/End"].setDouble(2.0);
        config["All/Settings/Binning/Bins/#1/Center"].setDouble(1.5);
        config["All/Settings/Binning/Bins/#2/Start"].setDouble(2.0);
        config["All/Settings/Binning/Bins/#2/End"].setDouble(3.0);
        QTest::newRow("Basic static") << config << values << points << limits;


        config.write().setEmpty();
        config["All/Data/Dimensions/#0/Input"].setString("Ind");
        config["All/Data/Dimensions/#1/Input"].setString("Dep");
        config["All/Settings/Binning/Type"].setString("Uniform");
        config["All/Settings/Binning/Count"].setInt64(6);
        config["All/Settings/Whisker/Quantile"].setDouble(0.1);
        config["All/Settings/Box/Quantile"].setDouble(0.3);

        points.clear();
        point.center[0] = 0.25;
        point.width[0] = 0.5;
        setQuantiles(std::vector<double>{3.0}, 0.10, 0.3, 0, point);
        points.emplace_back(point);
        point.center[0] = 0.75;
        setQuantiles(std::vector<double>{1.0, 2.0}, 0.10, 0.3, 0, point);
        points.emplace_back(point);
        point.center[0] = 1.25;
        setQuantiles(std::vector<double>{30.0}, 0.10, 0.3, 0, point);
        limits.max[1] = point.uppermost[0];
        points.emplace_back(point);
        point.center[0] = 1.75;
        setQuantiles(std::vector<double>{10.0, 20.0}, 0.10, 0.3, 0, point);
        points.emplace_back(point);
        point.center[0] = 2.75;
        setQuantiles(std::vector<double>{0.1, 0.2, 0.3}, 0.10, 0.3, 0, point);
        limits.min[1] = point.lowest[0];
        points.emplace_back(point);

        QTest::newRow("Advanced uniform") << config << values << points << limits;

        config["All/Settings/Binning/Type"].setString("Size");
        config["All/Settings/Binning/Width"].setDouble(0.5);
        QTest::newRow("Advanced width") << config << values << points << limits;

        config["All/Settings/Binning/Type"].setString("Static");
        config["All/Settings/Binning/Bins/#0/Start"].setDouble(0.0);
        config["All/Settings/Binning/Bins/#0/End"].setDouble(0.5);
        config["All/Settings/Binning/Bins/#1/Start"].setDouble(0.5);
        config["All/Settings/Binning/Bins/#1/End"].setDouble(1.0);
        config["All/Settings/Binning/Bins/#2/Start"].setDouble(1.0);
        config["All/Settings/Binning/Bins/#2/End"].setDouble(1.5);
        config["All/Settings/Binning/Bins/#3/Start"].setDouble(1.5);
        config["All/Settings/Binning/Bins/#3/End"].setDouble(2.0);
        config["All/Settings/Binning/Bins/#4/Start"].setDouble(2.5);
        config["All/Settings/Binning/Bins/#4/End"].setDouble(3.0);
        QTest::newRow("Advanced static") << config << values << points << limits;

        config["All/Settings/Binning/Type"].setString("Static");
        config["All/Settings/Binning/Bins/#0/Start"].setDouble(0.0);
        config["All/Settings/Binning/Bins/#0/End"].setDouble(0.5);
        config["All/Settings/Binning/Bins/#1/Start"].setDouble(0.5);
        config["All/Settings/Binning/Bins/#1/End"].setDouble(1.0);
        config["All/Settings/Binning/Bins/#2/Start"].setDouble(1.0);
        config["All/Settings/Binning/Bins/#2/End"].setDouble(1.5);
        config["All/Settings/Binning/Bins/#3/Start"].setDouble(1.5);
        config["All/Settings/Binning/Bins/#3/End"].setDouble(2.0);
        config["All/Settings/Binning/Bins/#4/Start"].setDouble(2.0);
        config["All/Settings/Binning/Bins/#4/End"].setDouble(2.5);
        config["All/Settings/Binning/Bins/#5/Start"].setDouble(2.5);
        config["All/Settings/Binning/Bins/#5/End"].setDouble(3.0);
        QTest::newRow("Advanced static empty") << config << values << points << limits;

        config["All/Settings/Binning/Bins/#6/Start"].setDouble(FP::undefined());
        config["All/Settings/Binning/Bins/#6/End"].setDouble(FP::undefined());
        config["All/Settings/Binning/Bins/#6/Center"].setDouble(2.0);
        point.center[0] = 2.0;
        point.width[0] = FP::undefined();
        setQuantiles(std::vector<double>{0.1, 0.2, 0.3, 1.0, 2.0, 3.0, 10.0, 20.0, 30.0}, 0.10, 0.3,
                     0, point);
        points.emplace_back(point);
        QTest::newRow("Advanced static all") << config << values << points << limits;
    }

    void two()
    {
        QFETCH(Variant::Root, configuration);
        QFETCH(SequenceValue::Transfer, values);
        QFETCH(std::vector<BinPoint22>, points);
        QFETCH(TraceLimits<4>, limits);

        BinDispatchSet<2, 2> set;
        set.initialize(ValueSegment::Transfer{
                ValueSegment(FP::undefined(), FP::undefined(), configuration)});

        DispatchBackend backend(values);
        ProcessingTapChain chain;
        chain.setBackend(&backend);
        set.registerChain(&chain);
        chain.start();
        chain.wait();
        set.process();

        std::vector<TraceValueHandler<4> *> handlers(set.getHandlers());
        QCOMPARE((int) handlers.size(), 1);

        BinValueHandler<2, 2> *handler = static_cast<BinValueHandler<2, 2> *>(
                handlers[0]);
        std::vector<BinPoint<2, 2> > sorted(handler->getPoints());
        std::sort(sorted.begin(), sorted.end(), sortBinPoints<2, 2>);
        std::sort(points.begin(), points.end(), sortBinPoints<2, 2>);
        QCOMPARE(sorted, points);
        QCOMPARE(handler->getLimits(), limits);
    }

    void two_data()
    {
        QTest::addColumn<Variant::Root>("configuration");
        QTest::addColumn<SequenceValue::Transfer>("values");
        QTest::addColumn<std::vector<BinPoint22> >("points");
        QTest::addColumn<TraceLimits<4> >("limits");

        Variant::Root config;
        config["All/Data/Dimensions/#0/Input"].setString("I1");
        config["All/Data/Dimensions/#1/Input"].setString("I2");
        config["All/Data/Dimensions/#2/Input"].setString("D1");
        config["All/Data/Dimensions/#3/Input"].setString("D2");
        config["All/Settings/Binning/#0/Type"].setString("Uniform");
        config["All/Settings/Binning/#0/Count"].setInt64(3);
        config["All/Settings/Binning/#1/Type"].setString("Uniform");
        config["All/Settings/Binning/#1/Count"].setInt64(3);

        SequenceValue::Transfer values
                {SequenceValue(SequenceName("brw", "raw", "I1"), Variant::Root(0.8), 100.0, 101.0),
                 SequenceValue(SequenceName("brw", "raw", "I2"), Variant::Root(-0.8), 100.0, 101.0),
                 SequenceValue(SequenceName("brw", "raw", "D1"), Variant::Root(1.0), 100.0, 101.0),
                 SequenceValue(SequenceName("brw", "raw", "D2"), Variant::Root(-1.0), 100.0, 101.0),
                 SequenceValue(SequenceName("brw", "raw", "I1"), Variant::Root(0.9), 101.0, 102.0),
                 SequenceValue(SequenceName("brw", "raw", "I2"), Variant::Root(-0.9), 101.0, 102.0),
                 SequenceValue(SequenceName("brw", "raw", "D1"), Variant::Root(2.0), 101.0, 102.0),
                 SequenceValue(SequenceName("brw", "raw", "D2"), Variant::Root(-2.0), 101.0, 102.0),
                 SequenceValue(SequenceName("brw", "raw", "I1"), Variant::Root(0.0), 102.0, 103.0),
                 SequenceValue(SequenceName("brw", "raw", "I2"), Variant::Root(0.0), 102.0, 103.0),
                 SequenceValue(SequenceName("brw", "raw", "D1"), Variant::Root(3.0), 102.0, 103.0),
                 SequenceValue(SequenceName("brw", "raw", "D2"), Variant::Root(-3.0), 102.0, 103.0),
                 SequenceValue(SequenceName("brw", "raw", "I1"), Variant::Root(1.5), 200.0, 201.0),
                 SequenceValue(SequenceName("brw", "raw", "I2"), Variant::Root(-1.5), 200.0, 201.0),
                 SequenceValue(SequenceName("brw", "raw", "D1"), Variant::Root(10.0), 200.0, 201.0),
                 SequenceValue(SequenceName("brw", "raw", "D2"), Variant::Root(-10.0), 200.0,
                               201.0),
                 SequenceValue(SequenceName("brw", "raw", "I1"), Variant::Root(1.6), 201.0, 202.0),
                 SequenceValue(SequenceName("brw", "raw", "I2"), Variant::Root(-1.6), 201.0, 202.0),
                 SequenceValue(SequenceName("brw", "raw", "D1"), Variant::Root(20.0), 201.0, 202.0),
                 SequenceValue(SequenceName("brw", "raw", "D2"), Variant::Root(-20.0), 201.0,
                               202.0),
                 SequenceValue(SequenceName("brw", "raw", "I1"), Variant::Root(1.4), 202.0, 203.0),
                 SequenceValue(SequenceName("brw", "raw", "I2"), Variant::Root(-1.4), 202.0, 203.0),
                 SequenceValue(SequenceName("brw", "raw", "D1"), Variant::Root(30.0), 202.0, 203.0),
                 SequenceValue(SequenceName("brw", "raw", "D2"), Variant::Root(-30.0), 202.0,
                               203.0),
                 SequenceValue(SequenceName("brw", "raw", "I1"), Variant::Root(3.0), 300.0, 301.0),
                 SequenceValue(SequenceName("brw", "raw", "I2"), Variant::Root(-3.0), 300.0, 301.0),
                 SequenceValue(SequenceName("brw", "raw", "D1"), Variant::Root(0.1), 300.0, 301.0),
                 SequenceValue(SequenceName("brw", "raw", "D2"), Variant::Root(-0.1), 300.0, 301.0),
                 SequenceValue(SequenceName("brw", "raw", "I1"), Variant::Root(2.9), 301.0, 302.0),
                 SequenceValue(SequenceName("brw", "raw", "I2"), Variant::Root(-2.9), 301.0, 302.0),
                 SequenceValue(SequenceName("brw", "raw", "D1"), Variant::Root(0.2), 301.0, 302.0),
                 SequenceValue(SequenceName("brw", "raw", "D2"), Variant::Root(-0.2), 301.0, 302.0),
                 SequenceValue(SequenceName("brw", "raw", "I1"), Variant::Root(2.8), 302.0, 303.0),
                 SequenceValue(SequenceName("brw", "raw", "I2"), Variant::Root(-2.8), 302.0, 303.0),
                 SequenceValue(SequenceName("brw", "raw", "D1"), Variant::Root(0.3), 302.0, 303.0),
                 SequenceValue(SequenceName("brw", "raw", "D2"), Variant::Root(-0.3), 302.0,
                               303.0)};

        TraceLimits<4> limits;
        limits.min[0] = 0.0;
        limits.max[0] = 3.0;
        limits.min[1] = -3.0;
        limits.max[1] = 0.0;

        std::vector<BinPoint22> points;
        BinPoint22 point;
        point.center[0] = 0.5;
        point.width[0] = 1.0;
        point.center[1] = -0.5;
        point.width[1] = 1.0;
        setQuantiles(std::vector<double>{1.0, 2.0, 3.0}, 0.05, 0.25, 0, point);
        setQuantiles(std::vector<double>{-3.0, -2.0, -1.0}, 0.05, 0.25, 1, point);
        points.emplace_back(point);
        point.center[0] = 1.5;
        point.center[1] = -1.5;
        setQuantiles(std::vector<double>{10.0, 20.0, 30.0}, 0.05, 0.25, 0, point);
        setQuantiles(std::vector<double>{-30.0, -20.0, -10.0}, 0.05, 0.25, 1, point);
        limits.max[2] = point.uppermost[0];
        limits.min[3] = point.lowest[1];
        points.emplace_back(point);
        point.center[0] = 2.5;
        point.center[1] = -2.5;
        setQuantiles(std::vector<double>{0.1, 0.2, 0.3}, 0.05, 0.25, 0, point);
        setQuantiles(std::vector<double>{-0.3, -0.2, -0.1}, 0.05, 0.25, 1, point);
        limits.min[2] = point.lowest[0];
        limits.max[3] = point.uppermost[1];
        points.emplace_back(point);

        QTest::newRow("Basic fixed") << config << values << points << limits;

        config["All/Settings/Binning/#0/Type"].setString("Size");
        config["All/Settings/Binning/#0/Width"].setDouble(1.0);
        config["All/Settings/Binning/#1/Type"].setString("Size");
        config["All/Settings/Binning/#1/Width"].setDouble(1.0);
        QTest::newRow("Basic width") << config << values << points << limits;

        config["All/Settings/Binning/#0/Type"].setString("Static");
        config["All/Settings/Binning/#0/Bins/#0/Start"].setDouble(0.0);
        config["All/Settings/Binning/#0/Bins/#0/End"].setDouble(1.0);
        config["All/Settings/Binning/#0/Bins/#1/Start"].setDouble(1.0);
        config["All/Settings/Binning/#0/Bins/#1/End"].setDouble(2.0);
        config["All/Settings/Binning/#0/Bins/#1/Center"].setDouble(1.5);
        config["All/Settings/Binning/#0/Bins/#2/Start"].setDouble(2.0);
        config["All/Settings/Binning/#0/Bins/#2/End"].setDouble(3.0);
        config["All/Settings/Binning/#1/Type"].setString("Static");
        config["All/Settings/Binning/#1/Bins/#0/End"].setDouble(0.0);
        config["All/Settings/Binning/#1/Bins/#0/Start"].setDouble(-1.0);
        config["All/Settings/Binning/#1/Bins/#1/Start"].setDouble(-1.0);
        config["All/Settings/Binning/#1/Bins/#1/End"].setDouble(-2.0);
        config["All/Settings/Binning/#1/Bins/#1/Center"].setDouble(-1.5);
        config["All/Settings/Binning/#1/Bins/#2/Start"].setDouble(-2.0);
        config["All/Settings/Binning/#1/Bins/#2/End"].setDouble(-3.0);
        QTest::newRow("Basic static") << config << values << points << limits;
    }
};

QTEST_APPLESS_MAIN(TestBinSet)

#include "binset.moc"
