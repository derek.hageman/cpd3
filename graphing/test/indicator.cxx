/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtAlgorithms>
#include <QObject>
#include <QString>
#include <QTest>

#include "graphing/indicator.hxx"
#include "datacore/stream.hxx"
#include "datacore/processingtapchain.hxx"
#include "datacore/segment.hxx"
#include "datacore/variant/root.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Graphing;

class DispatchBackend : public ProcessingTapChain::ArchiveBackend {
public:
    SequenceValue::Transfer values;

    DispatchBackend(const SequenceValue::Transfer &sv) : values(sv)
    { }

    virtual ~DispatchBackend()
    { }

    void issueArchiveRead(const Archive::Selection::List &, StreamSink *target) override
    {
        if (!values.empty())
            target->incomingData(values);
        target->endData();
    }
};

Q_DECLARE_METATYPE(IndicatorPoint<1>);

Q_DECLARE_METATYPE(std::vector<IndicatorPoint<1>>);

class TestIndicatorSet : public QObject {
Q_OBJECT
private slots:

    void trigger()
    {
        QFETCH(Variant::Root, configuration);
        QFETCH(SequenceValue::Transfer, values);
        QFETCH(std::vector<IndicatorPoint<1>>, points);

        IndicatorDispatchSet<1> set;
        set.initialize(ValueSegment::Transfer{
                ValueSegment(FP::undefined(), FP::undefined(), configuration)});

        DispatchBackend backend(values);
        ProcessingTapChain chain;
        chain.setBackend(&backend);
        set.registerChain(&chain);
        chain.start();
        chain.wait();
        set.process();

        auto handlers = set.getHandlers();
        QCOMPARE((int) handlers.size(), 1);

        TraceLimits<1> limits;
        limits.reset();
        for (const auto &p : points) {
            limits.add(p);
        }

        QCOMPARE(handlers[0]->getPoints(), points);
        QCOMPARE(handlers[0]->getLimits(), limits);
    }

    void trigger_data()
    {
        QTest::addColumn<Variant::Root>("configuration");
        QTest::addColumn<SequenceValue::Transfer>("values");
        QTest::addColumn<std::vector<IndicatorPoint<1>>>("points");

        Variant::Root config;
        config["All/Data/Dimensions/#0/Input"].setString("VarTrigger");
        config["All/Data/Dimensions/#1/Input"].setString("VarValue");
        config["All/Settings/Trigger/Type"].setString("Delta");
        config["All/Settings/Trigger/Threshold"].setDouble(1.0);
        config["All/Settings/Invert"].setBool(false);

        SequenceValue::Transfer values
                {SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(1.0), 100.0,
                               125.0),
                 SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.0), 100.0,
                               125.0),
                 SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(2.1), 125.0,
                               150.0),
                 SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.1), 125.0,
                               150.0),
                 SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(3.1), 150.0,
                               175.0),
                 SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.2), 150.0,
                               175.0),
                 SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(), 175.0,
                               200.0),
                 SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.3), 175.0,
                               200.0)};

        std::vector<IndicatorPoint<1>> points;
        IndicatorPoint<1> point;
        point.start = 125.0;
        point.end = 150.0;
        point.d[0] = 10.1;
        points.emplace_back(point);
        QTest::newRow("Delta") << config << values << points;

        config["All/Settings/Trigger/Type"].setString("DeltaEqual");
        config["All/Settings/Trigger/Threshold"].setDouble(1.0);
        config["All/Settings/Invert"].setBool(false);
        point.start = 150.0;
        point.end = 175.0;
        point.d[0] = 10.2;
        points.emplace_back(point);
        QTest::newRow("DeltaEqual") << config << values << points;

        config["All/Settings/Trigger/Type"].setString("GreaterEqual");
        config["All/Settings/Trigger/Threshold"].setDouble(2.1);
        config["All/Settings/Invert"].setBool(false);
        QTest::newRow("GreaterEqual") << config << values << points;

        config["All/Settings/Trigger/Type"].setString("Less");
        config["All/Settings/Trigger/Threshold"].setDouble(2.1);
        config["All/Settings/Invert"].setBool(true);
        QTest::newRow("Less Invert") << config << values << points;

        config["All/Settings/Trigger/Type"].setString("Exactly");
        config["All/Settings/Trigger/Value"].setDouble(1.0);
        config["All/Settings/Invert"].setBool(true);
        QTest::newRow("Exactly Invert") << config << values << points;

        config["All/Settings/Trigger/Type"].setString("Greater");
        config["All/Settings/Trigger/Threshold"].setDouble(2.1);
        config["All/Settings/Invert"].setBool(false);
        points.clear();
        point.start = 150.0;
        point.end = 175.0;
        point.d[0] = 10.2;
        points.emplace_back(point);
        QTest::newRow("Greater") << config << values << points;

        config["All/Settings/Trigger/Type"].setString("LessEqual");
        config["All/Settings/Trigger/Threshold"].setDouble(2.1);
        config["All/Settings/Invert"].setBool(true);
        QTest::newRow("LessEqual invert") << config << values << points;

        points.clear();
        point.start = 100.0;
        point.end = 125.0;
        point.d[0] = 10.0;
        points.emplace_back(point);
        config["All/Settings/Trigger/Type"].setString("Less");
        config["All/Settings/Trigger/Threshold"].setDouble(2.1);
        config["All/Settings/Invert"].setBool(false);
        QTest::newRow("Less") << config << values << points;

        config["All/Settings/Trigger/Type"].setString("Exactly");
        config["All/Settings/Trigger/Value"].setDouble(1.0);
        config["All/Settings/Invert"].setBool(false);
        QTest::newRow("Exactly") << config << values << points;

        config["All/Settings/Trigger/Type"].setString("GreaterEqual");
        config["All/Settings/Trigger/Threshold"].setDouble(2.1);
        config["All/Settings/Invert"].setBool(true);
        QTest::newRow("GreaterEqual Invert") << config << values << points;

        point.start = 125.0;
        point.end = 150.0;
        point.d[0] = 10.1;
        points.emplace_back(point);
        config["All/Settings/Trigger/Type"].setString("LessEqual");
        config["All/Settings/Trigger/Threshold"].setDouble(2.1);
        config["All/Settings/Invert"].setBool(false);
        QTest::newRow("LessEqual") << config << values << points;

        config["All/Settings/Trigger/Type"].setString("Greater");
        config["All/Settings/Trigger/Threshold"].setDouble(2.1);
        config["All/Settings/Invert"].setBool(true);
        QTest::newRow("Greater Invert") << config << values << points;

        points.clear();
        point.start = 175.0;
        point.end = 200.0;
        point.d[0] = 10.3;
        points.emplace_back(point);
        config["All/Settings/Trigger/Type"].setString("Invalid");
        config["All/Settings/Invert"].setBool(false);
        QTest::newRow("Invalid") << config << values << points;

        points.clear();
        point.start = 100.0;
        point.end = 125.0;
        point.d[0] = 10.0;
        points.emplace_back(point);
        point.start = 125.0;
        point.end = 150.0;
        point.d[0] = 10.1;
        points.emplace_back(point);
        point.start = 150.0;
        point.end = 175.0;
        point.d[0] = 10.2;
        points.emplace_back(point);
        config["All/Settings/Trigger/Type"].setString("Invalid");
        config["All/Settings/Invert"].setBool(true);
        QTest::newRow("Invalid Invert") << config << values << points;

        values = SequenceValue::Transfer{
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(0x01), 100.0,
                              125.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.0), 100.0,
                              125.0),
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(0x02), 125.0,
                              150.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.1), 125.0,
                              150.0),
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(0x00), 150.0,
                              175.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.2), 150.0,
                              175.0),
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(), 175.0,
                              200.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.3), 175.0,
                              200.0)};

        points.clear();
        point.start = 100.0;
        point.end = 125.0;
        point.d[0] = 10.0;
        points.emplace_back(point);
        config["All/Settings/Trigger/Type"].setString("AND");
        config["All/Settings/Trigger/Value"].setInt64(0x01);
        config["All/Settings/Invert"].setBool(false);
        QTest::newRow("AND") << config << values << points;

        config["All/Settings/Trigger/Type"].setString("XOR");
        config["All/Settings/Trigger/Value"].setInt64(0x01);
        config["All/Settings/Invert"].setBool(true);
        QTest::newRow("XOR Invert") << config << values << points;

        points.clear();
        point.start = 125.0;
        point.end = 150.0;
        point.d[0] = 10.1;
        points.emplace_back(point);
        point.start = 150.0;
        point.end = 175.0;
        point.d[0] = 10.2;
        points.emplace_back(point);
        config["All/Settings/Trigger/Type"].setString("AND");
        config["All/Settings/Trigger/Value"].setInt64(0x01);
        config["All/Settings/Invert"].setBool(true);
        QTest::newRow("AND Invert") << config << values << points;

        config["All/Settings/Trigger/Type"].setString("XOR");
        config["All/Settings/Trigger/Value"].setInt64(0x01);
        config["All/Settings/Invert"].setBool(false);
        QTest::newRow("XOR") << config << values << points;

        Variant::Root fv;
        fv.write().applyFlag("SomeFlag");
        values = SequenceValue::Transfer{
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), fv, 100.0, 125.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.0), 100.0,
                              125.0),
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), fv, 125.0, 150.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.1), 125.0,
                              150.0)};
        fv.write().clearFlag("SomeFlag");
        values.emplace_back(SequenceName("brw", "raw", "VarTrigger"), fv, 150.0, 175.0);
        values.emplace_back(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.2), 150.0,
                            175.0);
        values.emplace_back(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(), 175.0,
                            200.0);
        values.emplace_back(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.3), 175.0,
                            200.0);

        points.clear();
        point.start = 100.0;
        point.end = 125.0;
        point.d[0] = 10.0;
        points.emplace_back(point);
        point.start = 125.0;
        point.end = 150.0;
        point.d[0] = 10.1;
        points.emplace_back(point);
        config["All/Settings/Trigger/Type"].setString("Flag");
        config["All/Settings/Trigger/Flags"].setFlags({"SomeFlag"});
        config["All/Settings/Invert"].setBool(false);
        QTest::newRow("Flags") << config << values << points;

        config["All/Settings/Trigger/Type"].setString("NOFlag");
        config["All/Settings/Invert"].setBool(true);
        QTest::newRow("NoFlags Invert") << config << values << points;


        points.clear();
        point.start = 150.0;
        point.end = 175.0;
        point.d[0] = 10.2;
        points.emplace_back(point);
        config["All/Settings/Trigger/Type"].setString("Flag");
        config["All/Settings/Invert"].setBool(true);
        QTest::newRow("Flags Invert") << config << values << points;

        config["All/Settings/Trigger/Type"].setString("NoFlag");
        config["All/Settings/Invert"].setBool(false);
        QTest::newRow("NoFlags") << config << values << points;


        values = SequenceValue::Transfer{
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(0.0), 100.0,
                              125.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.0), 100.0,
                              125.0),
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(0.0), 125.0,
                              150.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.1), 125.0,
                              150.0),
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(1.0), 150.0,
                              175.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.2), 150.0,
                              175.0),
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(1.0), 180.0,
                              200.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.3), 180.0,
                              200.0),
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(2.0), 205.0,
                              225.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.4), 205.0,
                              225.0),
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(), 225.0,
                              250.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.5), 225.0,
                              250.0),
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(2.0), 250.0,
                              275.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.6), 250.0,
                              275.0),
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(), 275.0,
                              300.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.7), 275.0,
                              300.0),
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(3.0), 300.0,
                              325.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.8), 300.0,
                              325.0)};

        points.clear();
        point.start = 150.0;
        point.end = 175.0;
        point.d[0] = 10.2;
        points.emplace_back(point);
        config["All/Settings/Trigger/Type"].setString("Changed");
        config["All/Settings/Invert"].setBool(false);
        QTest::newRow("Changed") << config << values << points;

        config["All/Settings/Trigger/Type"].setString("Delta");
        config["All/Settings/Trigger/Threshold"].setDouble(0.5);
        config["All/Settings/Invert"].setBool(false);
        QTest::newRow("Delta Gap") << config << values << points;

        config["All/Settings/Trigger/Type"].setString("DeltaEqual");
        config["All/Settings/Trigger/Threshold"].setDouble(0.5);
        config["All/Settings/Invert"].setBool(false);
        QTest::newRow("DeltaEqual Gap") << config << values << points;

        config["All/Settings/Trigger/Type"].setString("NotChanged");
        config["All/Settings/Invert"].setBool(true);
        QTest::newRow("NotChanged Invert") << config << values << points;

        points.clear();
        point.start = 125.0;
        point.end = 150.0;
        point.d[0] = 10.1;
        points.emplace_back(point);
        config["All/Settings/Trigger/Type"].setString("NotChanged");
        config["All/Settings/Invert"].setBool(false);
        QTest::newRow("NotChanged") << config << values << points;

        config["All/Settings/Trigger/Type"].setString("Changed");
        config["All/Settings/Invert"].setBool(true);
        QTest::newRow("Changed Invert") << config << values << points;


        points.clear();
        point.start = 150.0;
        point.end = 175.0;
        point.d[0] = 10.2;
        points.emplace_back(point);
        point.start = 205.0;
        point.end = 225.0;
        point.d[0] = 10.4;
        points.emplace_back(point);
        config["All/Settings/Continuity"].setDouble(10.0);
        config["All/Settings/Trigger/Type"].setString("Changed");
        config["All/Settings/Invert"].setBool(false);
        QTest::newRow("Changed Continuity") << config << values << points;

        config["All/Settings/Continuity"].setDouble(10.0);
        config["All/Settings/Trigger/Type"].setString("NotChanged");
        config["All/Settings/Invert"].setBool(true);
        QTest::newRow("NotChanged Invert Continuity") << config << values << points;

        config["All/Settings/Continuity"].setDouble(10.0);
        config["All/Settings/Trigger/Type"].setString("Delta");
        config["All/Settings/Trigger/Threshold"].setDouble(0.5);
        config["All/Settings/Invert"].setBool(false);
        QTest::newRow("Delta Gap Continuity") << config << values << points;

        config["All/Settings/Continuity"].setDouble(10.0);
        config["All/Settings/Trigger/Type"].setString("DeltaEqual");
        config["All/Settings/Trigger/Threshold"].setDouble(0.5);
        config["All/Settings/Invert"].setBool(false);
        QTest::newRow("DeltaEqual Gap Continuity") << config << values << points;


        values = SequenceValue::Transfer{
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root("abc"), 100.0,
                              125.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.0), 100.0,
                              125.0),
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(1.0), 125.0,
                              150.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.1), 125.0,
                              150.0),
                SequenceValue(SequenceName("brw", "raw", "VarTrigger"), Variant::Root(), 150.0,
                              175.0),
                SequenceValue(SequenceName("brw", "raw", "VarValue"), Variant::Root(10.2), 150.0,
                              175.0)};

        points.clear();
        point.start = 100.0;
        point.end = 125.0;
        point.d[0] = 10.0;
        points.emplace_back(point);
        point.start = 150.0;
        point.end = 175.0;
        point.d[0] = 10.2;
        points.emplace_back(point);
        config["All/Settings/Trigger/Type"].setString("Undefined");
        config["All/Settings/Invert"].setBool(false);
        QTest::newRow("Undefined") << config << values << points;

        points.erase(points.begin());
        config["All/Settings/LatestOnly"].setBool(true);
        QTest::newRow("Undefined latest") << config << values << points;

        points.clear();
        point.start = 125.0;
        point.end = 150.0;
        point.d[0] = 10.1;
        points.emplace_back(point);
        config["All/Settings/Trigger/Type"].setString("Undefined");
        config["All/Settings/Invert"].setBool(true);
        config["All/Settings/LatestOnly"].setBool(false);
        QTest::newRow("Undefined Invert") << config << values << points;

        config["All/Settings/LatestOnly"].setBool(true);
        QTest::newRow("Undefined invert latest") << config << values << points;
    }
};

QTEST_APPLESS_MAIN(TestIndicatorSet)

#include "indicator.moc"
