/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>
#include <QtAlgorithms>
#include <QObject>
#include <QString>
#include <QTest>

#include "graphing/tracedispatch.hxx"
#include "datacore/stream.hxx"
#include "datacore/processingtapchain.hxx"
#include "datacore/segment.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Graphing;

class DispatchBackend : public ProcessingTapChain::ArchiveBackend {
public:
    SequenceValue::Transfer values;

    DispatchBackend(const SequenceValue::Transfer &sv) : values(sv)
    { }

    virtual ~DispatchBackend()
    { }

    void issueArchiveRead(const Archive::Selection::List &, StreamSink *target) override
    {
        if (!values.empty())
            target->incomingData(values);
        target->endData();
    }
};

class DispatchBackendExternal : public ProcessingTapChain::ArchiveBackend {
public:
    StreamSink *ingress;
    std::mutex mutex;
    std::condition_variable ready;

    DispatchBackendExternal() : ingress(nullptr)
    { }

    virtual ~DispatchBackendExternal()
    { }

    void issueArchiveRead(const Archive::Selection::List &, StreamSink *target) override
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            ingress = target;
        }
        ready.notify_all();
    }
};

Q_DECLARE_METATYPE(TraceDispatch::OutputValue);

Q_DECLARE_METATYPE(std::vector<TraceDispatch::OutputValue>);

Q_DECLARE_METATYPE(std::vector<std::vector<TraceDispatch::OutputValue> >);

namespace QTest {
template<>
char *toString(const std::vector<std::vector<TraceDispatch::OutputValue> > &output)
{
    QByteArray ba;
    for (std::size_t trace = 0, nTraces = output.size(); trace < nTraces; trace++) {
        if (trace != 0) ba += ", ";
        const auto &values = output[trace];
        ba += "Trace_";
        ba += QByteArray::number(static_cast<int>(trace));
        ba += "=(";
        for (std::size_t i = 0, max = values.size(); i < max; i++) {
            const auto &value = values[i];
            if (i != 0) ba += ",";
            ba += "{start=";
            if (FP::defined(value.getStart()))
                ba += QByteArray::number(value.getStart());
            else
                ba += "-INFINITY";
            ba += ",end=";
            if (FP::defined(value.getEnd()))
                ba += QByteArray::number(value.getEnd());
            else
                ba += "INFINITY";
            if (value.isMetadata()) {
                ba += ",META";
            }
            ba += ",values=[";
            for (std::size_t j = 0, nDims = value.getDimensions(); j < nDims; j++) {
                if (j != 0) ba += ",";
                if (value.get(j).getType() == Variant::Type::MetadataReal) {
                    ba += QByteArray::number(value.get(j).metadata("Foo").toDouble());
                } else if (value.get(j).exists()) {
                    ba += QByteArray::number(value.get(j).toDouble());
                } else {
                    ba += "INVALID";
                }
            }
            ba += "]}";
        }
        ba += ")";
    }
    return qstrdup(ba.data());
}
}

static bool outputSort(const std::vector<TraceDispatch::OutputValue> &a,
                       const std::vector<TraceDispatch::OutputValue> &b)
{
    if (a.size() < b.size()) return true;
    if (a.size() > b.size()) return false;
    for (std::size_t i = 0, max = a.size(); i < max; i++) {
        if (a.at(i).isMetadata()) {
            if (!b.at(i).isMetadata())
                return true;
        } else if (b.at(i).isMetadata()) {
            return false;
        }
        if (a.at(i).getDimensions() < b.at(i).getDimensions()) return true;
        if (a.at(i).getDimensions() > b.at(i).getDimensions()) return false;
        for (int j = 0, nDims = a.at(i).getDimensions(); j < nDims; j++) {
            if (a.at(i).get(j).toDouble() < b.at(i).get(j).toDouble())
                return true;
            if (a.at(i).get(j).toDouble() > b.at(i).get(j).toDouble())
                return false;
        }
    }
    return false;
}

class TestTraceDispatch : public QObject {
Q_OBJECT
private slots:

    void mergable()
    {
        Variant::Root a;
        Variant::Root b;

        a["Dimensions/#0/Input"].setString("");
        b["Dimensions/#0/Input"].setString("");
        QVERIFY(TraceDispatch::mergable(a, b));
        a["Dimensions/#1/Input"].setString("BsG_S11");
        QVERIFY(TraceDispatch::mergable(a, b));
        b["Dimensions/#1/Input"].setString("BsG_S11");
        QVERIFY(TraceDispatch::mergable(a, b));
        b["Dimensions/#1/Input"].setString("BsB_S11");
        QVERIFY(TraceDispatch::mergable(a, b));
        a["Dimensions/#1/Input"].setString("BsB_S11");
        QVERIFY(TraceDispatch::mergable(a, b));
        a["Dimensions/#0/Fanout"].setBool(true);
        QVERIFY(!TraceDispatch::mergable(a, b));
        b["Dimensions/#0/Fanout"].setBool(true);
        QVERIFY(TraceDispatch::mergable(a, b));
        a["Dimensions/#0/FlattenFlavors"].setBool(false);
        QVERIFY(TraceDispatch::mergable(a, b));
        b["Dimensions/#0/FlattenFlavors"].setBool(false);
        QVERIFY(TraceDispatch::mergable(a, b));
        a["Dimensions/#0/Processing/Input"].setString("archive");
        QVERIFY(TraceDispatch::mergable(a, b));
        b["Dimensions/#0/Processing/Input"].setString("archive");
        QVERIFY(TraceDispatch::mergable(a, b));
    }

    void basic()
    {
        QFETCH(ValueSegment::Transfer, config);
        QFETCH(SequenceValue::Transfer, input);
        QFETCH(std::vector<std::vector<TraceDispatch::OutputValue> >, expected);

        std::sort(expected.begin(), expected.end(), outputSort);

        TraceDispatch dispatch(config);
        DispatchBackend backend(input);
        std::vector<std::vector<TraceDispatch::OutputValue> > result;

        ProcessingTapChain chain;
        chain.setBackend(&backend);
        dispatch.registerChain(&chain);
        chain.start();
        chain.wait();
        result = dispatch.getPending();
        std::sort(result.begin(), result.end(), outputSort);
        QCOMPARE(result, expected);

        ProcessingTapChain chain2;
        chain2.setBackend(&backend);
        dispatch.registerChain(&chain2);
        chain2.start();
        chain2.wait();
        result = dispatch.getPending();
        std::sort(result.begin(), result.end(), outputSort);
        QCOMPARE(result, expected);

        if (expected.empty())
            return;

        ProcessingTapChain chain3;
        DispatchBackendExternal delayBackend;
        chain3.setBackend(&delayBackend);
        dispatch.registerChain(&chain3);
        chain3.start();
        result.clear();
        {
            std::unique_lock<std::mutex> lock(delayBackend.mutex);
            delayBackend.ready.wait(lock, [&] { return delayBackend.ingress != nullptr; });
        }
        QTest::qSleep(50);
        while (!input.empty()) {
            delayBackend.ingress->incomingData(std::move(input.front()));
            input.erase(input.begin());
            QTest::qSleep(50);
            auto merge = dispatch.getPending();
            for (std::size_t i = 0, max = merge.size(); i < max; i++) {
                if (i >= result.size())
                    result.emplace_back();
                Util::append(std::move(merge[i]), result[i]);
            }
        }
        delayBackend.ingress->endData();
        chain3.wait();
        auto merge = dispatch.getPending();
        for (std::size_t i = 0, max = merge.size(); i < max; i++) {
            if (i >= result.size())
                result.emplace_back();
            Util::append(std::move(merge[i]), result[i]);
        }
        std::sort(result.begin(), result.end(), outputSort);
        QCOMPARE(result, expected);
    }

    void basic_data()
    {
        QTest::addColumn<ValueSegment::Transfer>("config");
        QTest::addColumn<SequenceValue::Transfer>("input");
        QTest::addColumn<std::vector<std::vector<TraceDispatch::OutputValue>>>("expected");

        ValueSegment::Transfer config;
        SequenceValue::Transfer values;
        std::vector<std::vector<TraceDispatch::OutputValue>> result;

        QTest::newRow("Empty") << config << values << result;

        Variant::Root metaBsG;
        metaBsG.write().metadataReal("Foo").setReal(1.0);
        values.emplace_back(SequenceName("brw", "raw_meta", "BsG_S11"), metaBsG, 100.0, 200.0);
        Variant::Root metaBsB;
        metaBsB.write().metadataReal("Foo").setReal(2.0);
        values.emplace_back(SequenceName("brw", "raw_meta", "BsB_S11"), metaBsB, 100.0, 200.0);
        values.emplace_back(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(1.0), 100.0,
                            125.0);
        values.emplace_back(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(1.1), 100.0,
                            125.0);
        values.emplace_back(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(2.0), 125.0,
                            150.0);
        values.emplace_back(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(2.1), 125.0,
                            150.0);
        values.emplace_back(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(3.0), 150.0,
                            175.0);
        values.emplace_back(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(3.1), 150.0,
                            175.0);
        values.emplace_back(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(4.0), 175.0,
                            200.0);
        values.emplace_back(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(4.1), 175.0,
                            200.0);
        Variant::Root v;
        v["Dimensions/#0/Input"].setString("");
        config.emplace_back(FP::undefined(), FP::undefined(), v);
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(100, 200, {metaBsG}, true),
                TraceDispatch::OutputValue(100, 125, {Variant::Root(1.0)}),
                TraceDispatch::OutputValue(125, 150, {Variant::Root(2.0)}),
                TraceDispatch::OutputValue(150, 175, {Variant::Root(3.0)}),
                TraceDispatch::OutputValue(175, 200, {Variant::Root(4.0)})});
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(100, 200, {metaBsB}, true),
                TraceDispatch::OutputValue(100, 125, {Variant::Root(1.1)}),
                TraceDispatch::OutputValue(125, 150, {Variant::Root(2.1)}),
                TraceDispatch::OutputValue(150, 175, {Variant::Root(3.1)}),
                TraceDispatch::OutputValue(175, 200, {Variant::Root(4.1)})});
        QTest::newRow("One dimension fanout") << config << values << result;

        config.clear();
        v.write().setEmpty();
        v["Dimensions/#0/Input"].setString("Bs[BR]_S11");
        v["Dimensions/#1/Input"].setString("Bs[GZ]_S11");
        config.emplace_back(FP::undefined(), FP::undefined(), v);
        result.clear();
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(100, 200, {metaBsB, metaBsG}, true),
                TraceDispatch::OutputValue(100, 125, {Variant::Root(1.1), Variant::Root(1.0)}),
                TraceDispatch::OutputValue(125, 150, {Variant::Root(2.1), Variant::Root(2.0)}),
                TraceDispatch::OutputValue(150, 175, {Variant::Root(3.1), Variant::Root(3.0)}),
                TraceDispatch::OutputValue(175, 200, {Variant::Root(4.1), Variant::Root(4.0)})});
        QTest::newRow("Two dimension simple") << config << values << result;

        values.emplace_back(SequenceName("brw", "raw", "BsR_S11"), Variant::Root(4.2), 175.0,
                            200.0);
        values.emplace_back(SequenceName("brw", "raw", "BsZ_S11"), Variant::Root(4.3), 175.0,
                            200.0);
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(175, 200, {Variant::Read::empty(), metaBsG}, true),
                TraceDispatch::OutputValue(175, 200, {Variant::Root(4.2), Variant::Root(4.0)})});
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(175, 200, {metaBsB, Variant::Root()}, true),
                TraceDispatch::OutputValue(175, 200, {Variant::Root(4.1), Variant::Root(4.3)})});
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(175, 200,
                                           {Variant::Read::empty(), Variant::Read::empty()}, true),
                TraceDispatch::OutputValue(175, 200, {Variant::Root(4.2), Variant::Root(4.3)})});
        QTest::newRow("Two dimension fanout") << config << values << result;

        config.clear();
        v.write().setEmpty();
        v["Dimensions/#0/Input"].setString("BsB_S11");
        v["Dimensions/#1/Input"].setString("Bs[GR]_S11");
        config.emplace_back(FP::undefined(), FP::undefined(), v);
        result.clear();
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(100, 200, {metaBsB, metaBsG}, true),
                TraceDispatch::OutputValue(100, 125, {Variant::Root(1.1), Variant::Root(1.0)}),
                TraceDispatch::OutputValue(125, 150, {Variant::Root(2.1), Variant::Root(2.0)}),
                TraceDispatch::OutputValue(150, 175, {Variant::Root(3.1), Variant::Root(3.0)}),
                TraceDispatch::OutputValue(175, 200, {Variant::Root(4.1), Variant::Root(4.0)})});
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(175, 200, {metaBsB, Variant::Read::empty()}, true),
                TraceDispatch::OutputValue(175, 200, {Variant::Root(4.1), Variant::Root(4.2)})});
        QTest::newRow("Two dimension fanout half") << config << values << result;


        config.clear();
        v.write().setEmpty();
        v["Dimensions/#0/Input"].setString("Bs[BG]_S11");
        v["Dimensions/#1/Input"].setString("Bs[RZ]_S11");
        config.emplace_back(FP::undefined(), FP::undefined(), v);
        result.clear();
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(175, 200, {metaBsG, Variant::Read::empty()}, true),
                TraceDispatch::OutputValue(175, 200, {Variant::Root(4.0), Variant::Root(4.3)})});
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(175, 200, {metaBsB, Variant::Read::empty()}, true),
                TraceDispatch::OutputValue(175, 200, {Variant::Root(4.1), Variant::Root(4.3)})});
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(100, 200, {metaBsG}, true),
                TraceDispatch::OutputValue(100, 125, {Variant::Root(1.0)}),
                TraceDispatch::OutputValue(125, 150, {Variant::Root(2.0)}),
                TraceDispatch::OutputValue(150, 175, {Variant::Root(3.0)}),
                TraceDispatch::OutputValue(175, 200, {Variant::Root(4.0), Variant::Root(4.2)})});
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(100, 200, {metaBsB}, true),
                TraceDispatch::OutputValue(100, 125, {Variant::Root(1.1)}),
                TraceDispatch::OutputValue(125, 150, {Variant::Root(2.1)}),
                TraceDispatch::OutputValue(150, 175, {Variant::Root(3.1)}),
                TraceDispatch::OutputValue(175, 200, {Variant::Root(4.1), Variant::Root(4.2)})});
        QTest::newRow("Two dimension fanout overlap") << config << values << result;


        config.clear();
        v.write().setEmpty();
        v["Dimensions/#0/Input"].setString("BsB_S11");
        v["Dimensions/#0/Fanout"].setBool(false);
        v["Dimensions/#1/Input"].setString("BsG_S11");
        v["Dimensions/#1/Fanout"].setBool(false);
        //config.emplace_back(FP::undefined(), 135, v);
        v["Dimensions/#0/Input"].setString("BsG_S11");
        v["Dimensions/#1/Input"].setString("BsB_S11");
        config.emplace_back(135, 165, v);
        v["Dimensions/#0/Input"].setString("BsR_S11");
        v["Dimensions/#1/Input"].setString("BsZ_S11");
        //config.emplace_back(180, FP::undefined(), v);
        result.clear();
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                /*TraceDispatch::OutputValue(100, 135,
                    {metaBsB, metaBsG}, true),
                TraceDispatch::OutputValue(100, 125,
                    {Variant::Root(1.1), Variant::Root(1.0)}),
                TraceDispatch::OutputValue(125, 135,
                    {Variant::Root(2.1), Variant::Root(2.0)}), */
                TraceDispatch::OutputValue(135, 165, {metaBsG, metaBsB}, true),
                TraceDispatch::OutputValue(135, 150, {Variant::Root(2.0), Variant::Root(2.1)}),
                TraceDispatch::OutputValue(150, 165, {Variant::Root(3.0), Variant::Root(3.1)})/* ,
            TraceDispatch::OutputValue(180, 200, 
                {Variant::Root(4.2), Variant::Root(4.3)})*/});
        QTest::newRow("Time fragment") << config << values << result;

        values.clear();
        values.emplace_back(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(1.0), 100.0,
                            125.0);
        values.emplace_back(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(1.1), 100.0,
                            125.0);
        values.emplace_back(SequenceName("brw", "raw", "BsR_S11"), Variant::Root(1.2), 100.0,
                            125.0);
        values.emplace_back(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(2.0), 125.0,
                            150.0);
        values.emplace_back(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(2.1), 125.0,
                            150.0);
        values.emplace_back(SequenceName("brw", "raw", "BsR_S11"), Variant::Root(2.2), 125.0,
                            150.0);
        values.emplace_back(SequenceName("brw", "raw", "BsZ_S11"), Variant::Root(2.3), 125.0,
                            150.0);

        config.clear();
        v.write().setEmpty();
        v["Dimensions/#0/Input"].setString("Bs[BR]_S11");
        v["Dimensions/#1/Input"].setString("Bs[GRZ]_S11");
        config.emplace_back(FP::undefined(), FP::undefined(), v);
        result.clear();
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(100, 125, {Variant::Root(1.0), Variant::Root(1.1)}),
                TraceDispatch::OutputValue(125, 150, {Variant::Root(2.0), Variant::Root(2.1)})});
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(100, FP::undefined(),
                                           {Variant::Read::empty(), Variant::Read::empty()}, true),
                TraceDispatch::OutputValue(100, 125, {Variant::Root(1.0), Variant::Root(1.2)}),
                TraceDispatch::OutputValue(125, 150, {Variant::Root(2.0), Variant::Root(2.2)})});
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(100, FP::undefined(),
                                           {Variant::Read::empty(), Variant::Read::empty()}, true),
                TraceDispatch::OutputValue(100, 125, {Variant::Root(1.2), Variant::Root(1.1)}),
                TraceDispatch::OutputValue(125, 150, {Variant::Root(2.2), Variant::Root(2.1)})});
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(100, FP::undefined(),
                                           {Variant::Read::empty(), Variant::Read::empty()}, true),
                TraceDispatch::OutputValue(100, 125, {Variant::Root(1.2), Variant::Root(1.2)}),
                TraceDispatch::OutputValue(125, 150, {Variant::Root(2.2), Variant::Root(2.2)})});
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(125, FP::undefined(),
                                           {Variant::Read::empty(), Variant::Read::empty()}, true),
                TraceDispatch::OutputValue(125, 150, {Variant::Root(2.0), Variant::Root(2.3)})});
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(125, FP::undefined(),
                                           {Variant::Read::empty(), Variant::Read::empty()}, true),
                TraceDispatch::OutputValue(125, 150, {Variant::Root(2.2), Variant::Root(2.3)})});
        QTest::newRow("Fanout duplicate") << config << values << result;


        values.clear();
        values.emplace_back(SequenceName("_", "raw_meta", "BsG_S11"), metaBsG, FP::undefined(),
                            FP::undefined());
        values.emplace_back(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(1.1), 100.0,
                            125.0);
        values.emplace_back(SequenceName("brw", "raw_meta", "BsG_S11"), metaBsB, 125.0, 150.0);
        values.emplace_back(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(2.1), 125.0,
                            150.0);

        config.clear();
        v.write().setEmpty();
        v["Dimensions/#0/Input"].setString("BsG_S11");
        config.emplace_back(FP::undefined(), FP::undefined(), v);
        result.clear();
        result.emplace_back(std::vector<TraceDispatch::OutputValue>{
                TraceDispatch::OutputValue(100, FP::undefined(), {metaBsG}, true),
                TraceDispatch::OutputValue(100, 125, {Variant::Root(1.1)}),
                TraceDispatch::OutputValue(125, 150, {metaBsB}, true),
                TraceDispatch::OutputValue(125, 150, {Variant::Root(2.1)}),
                TraceDispatch::OutputValue(150, FP::undefined(), {metaBsG}, true)});
        QTest::newRow("Default underlay") << config << values << result;
    }
};

QTEST_APPLESS_MAIN(TestTraceDispatch)

#include "tracedispatch.moc"
