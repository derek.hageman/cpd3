/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>

#include "graphing/legend.hxx"

using namespace CPD3::Graphing;

class TestAxisLabelEngine2D : public QObject {
Q_OBJECT
private slots:

    void basic()
    {
        Legend l;
        l.layout();
        QCOMPARE(l.getSize(), QSizeF(0, 0));

        std::shared_ptr<LegendItem> li1(new LegendItem("Text", QColor()));
        l.append(li1);
        l.layout();
        QVERIFY(l.getSize().width() > 0);
        QVERIFY(l.getSize().height() > 0);
        QCOMPARE(l.itemAt(1, 1), li1);
        QVERIFY(!l.itemAt(-1, -1));

        double baseHeight = l.getSize().height();
        double baseWidth = l.getSize().width();
        QCOMPARE(l.itemAt(1, baseHeight - 2), li1);
        QCOMPARE(l.itemAt(baseWidth - 2, 1), li1);
        QCOMPARE(l.itemAt(baseWidth - 2, baseHeight - 2), li1);
        QVERIFY(!l.itemAt(baseWidth + 2, baseHeight - 2));
        QVERIFY(!l.itemAt(baseWidth - 2, baseHeight + 2));
        QVERIFY(!l.itemAt(baseWidth + 2, baseHeight + 2));

        std::shared_ptr<LegendItem> li2(new LegendItem("Text", QColor()));

        l.append(li2);
        l.layout();
        QVERIFY(l.getSize().width() > 0);
        QVERIFY(l.getSize().height() > baseHeight);
        QCOMPARE(l.itemAt(1, 1), li1);
        QCOMPARE(l.itemAt(1, baseHeight - 2), li1);
        QCOMPARE(l.itemAt(baseWidth - 2, 1), li1);
        QCOMPARE(l.itemAt(baseWidth - 2, baseHeight - 2), li1);

        double twoHeight = l.getSize().height();
        double twoWidth = l.getSize().width();
        QCOMPARE(l.itemAt(1, baseHeight * 1.5), li2);
        QCOMPARE(l.itemAt(1, twoHeight - 2), li2);
        QCOMPARE(l.itemAt(baseWidth - 2, baseHeight * 1.5), li2);
        QCOMPARE(l.itemAt(baseWidth - 2, twoHeight - 2), li2);

        l.layout(baseWidth * 1.5, -1);
        QVERIFY(l.getSize().width() > 0);
        QVERIFY(l.getSize().height() > baseHeight);
        QCOMPARE(l.itemAt(1, 1), li1);
        QCOMPARE(l.itemAt(1, baseHeight - 2), li1);
        QCOMPARE(l.itemAt(baseWidth - 2, 1), li1);
        QCOMPARE(l.itemAt(baseWidth - 2, baseHeight - 2), li1);
        QCOMPARE(l.itemAt(1, baseHeight * 1.5), li2);
        QCOMPARE(l.itemAt(1, twoHeight - 2), li2);
        QCOMPARE(l.itemAt(baseWidth - 2, baseHeight * 1.5), li2);
        QCOMPARE(l.itemAt(baseWidth - 2, twoHeight - 2), li2);

        l.layout(-1, baseHeight * 1.5);
        QVERIFY(l.getSize().width() > twoWidth);
        QVERIFY(l.getSize().height() > 0);
        QVERIFY(l.getSize().height() < twoHeight);
        QCOMPARE(l.itemAt(1, 1), li1);
        QCOMPARE(l.itemAt(1, baseHeight - 2), li1);
        QCOMPARE(l.itemAt(baseWidth - 2, 1), li1);
        QCOMPARE(l.itemAt(baseWidth - 2, baseHeight - 2), li1);
        QCOMPARE(l.itemAt(baseWidth * 1.75, 1), li2);
        QCOMPARE(l.itemAt(baseWidth * 1.75, baseHeight - 2), li2);
        QCOMPARE(l.itemAt(l.getSize().width() - 2, baseHeight - 2), li2);
        QCOMPARE(l.itemAt(l.getSize().width() - 2, 1), li2);
    }
};

QTEST_MAIN(TestAxisLabelEngine2D)

#include "legend.moc"
