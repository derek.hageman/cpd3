/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QString>
#include <QTest>

#include "graphing/axistickgenerator.hxx"
#include "guicore/guicore.hxx"
#include "core/number.hxx"
#include "algorithms/statistics.hxx"

using namespace CPD3;
using namespace CPD3::Graphing;
using namespace CPD3::Algorithms;

#define TEST_SETTINGS_APP   CPD3GUI_APPLICATION "Testing"

class TimeCompareTick {
public:
    double point;
    QString labelDOY;
    QString secondaryLabelDOY;
    QString blockLabelDOY;
    QString labelDateTime;
    QString secondaryLabelDateTime;
    QString blockLabelDateTime;

    TimeCompareTick() : point(FP::undefined())
    { }

    TimeCompareTick(double setPoint,
                    const QString &setLabelDOY,
                    const QString &setSecondaryLabelDOY,
                    const QString &setBlockLabelDOY,
                    const QString &setLabelDateTime,
                    const QString &setSecondaryLabelDateTime,
                    const QString &setBlockLabelDateTime) : point(setPoint),
                                                            labelDOY(setLabelDOY),
                                                            secondaryLabelDOY(setSecondaryLabelDOY),
                                                            blockLabelDOY(setBlockLabelDOY),
                                                            labelDateTime(setLabelDateTime),
                                                            secondaryLabelDateTime(
                                                                    setSecondaryLabelDateTime),
                                                            blockLabelDateTime(
                                                                    setBlockLabelDateTime)
    { }
};

Q_DECLARE_METATYPE(TimeCompareTick);

Q_DECLARE_METATYPE(QList<TimeCompareTick>);

Q_DECLARE_METATYPE(AxisTickGenerator::GenerateFlags);

Q_DECLARE_METATYPE(std::vector<AxisTickGenerator::Tick>);

class TestAxisTickGenerator : public QObject {
Q_OBJECT
private slots:

    void initTestCase()
    {
        QSettings settings(CPD3GUI_ORGANIZATION, TEST_SETTINGS_APP);
        settings.clear();
    }

    void cleanupTestCase()
    {
        QSettings settings(CPD3GUI_ORGANIZATION, TEST_SETTINGS_APP);
        settings.clear();
    }


    void generatorDecimal()
    {
        QFETCH(double, min);
        QFETCH(double, max);
        QFETCH(AxisTickGenerator::GenerateFlags, flags);
        QFETCH(std::vector<AxisTickGenerator::Tick>, expected);

        auto results = AxisTickGeneratorDecimal().generate(min, max, flags);
        QCOMPARE(results.size(), expected.size());
        for (std::size_t i = 0, nResults = results.size(); i < nResults; i++) {
            QCOMPARE(results.at(i).point, expected.at(i).point);
            QCOMPARE(results.at(i).label, expected.at(i).label);
            QCOMPARE(results.at(i).secondaryLabel, expected.at(i).secondaryLabel);
            QCOMPARE(results.at(i).blockLabel, expected.at(i).blockLabel);
        }
    }

    void generatorDecimal_data()
    {
        QTest::addColumn<double>("min");
        QTest::addColumn<double>("max");
        QTest::addColumn<AxisTickGenerator::GenerateFlags>("flags");
        QTest::addColumn<std::vector<AxisTickGenerator::Tick> >("expected");

        std::vector<AxisTickGenerator::Tick> expected;

        QTest::newRow("Empty major") << 0.0 << 0.0 << AxisTickGenerator::MajorTick << expected;
        QTest::newRow("Empty minor") << 0.0 << 0.0 << AxisTickGenerator::MinorTick << expected;

        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(0.25, "0.25", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(0.5, "0.50", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(0.75, "0.75", QString(), QString()));
        QTest::newRow("Minor five") << 0.0 << 1.0 << AxisTickGenerator::MinorTick << expected;

        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(1.0, "1", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(2.0, "2", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(3.0, "3", QString(), QString()));
        QTest::newRow("Minor four") << 0.0 << 4.0 << AxisTickGenerator::MinorTick << expected;


        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(0.0, "0.0", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(0.5, "0.5", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(1.0, "1.0", QString(), QString()));
        QTest::newRow("0-1") << 0.0 << 1.0 << AxisTickGenerator::MajorTick << expected;
        expected.insert(expected.begin(),
                        AxisTickGenerator::Tick(-0.5, "-0.5", QString(), QString()));
        expected.push_back(AxisTickGenerator::Tick(1.5, "1.5", QString(), QString()));
        QTest::newRow("0-1 extended") << -0.1 << 1.1 << AxisTickGenerator::MajorTick << expected;

        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(1.0E10, "1E10", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(2.0E10, "2E10", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(3.0E10, "3E10", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(4.0E10, "4E10", QString(), QString()));
        QTest::newRow("Large") << 1E10 << 4E10 << AxisTickGenerator::MajorTick << expected;
    }


    void generatorTime()
    {
        QFETCH(double, min);
        QFETCH(double, max);
        QFETCH(AxisTickGenerator::GenerateFlags, flags);
        QFETCH(QList<TimeCompareTick>, expected);

        QSettings settings(CPD3GUI_ORGANIZATION, TEST_SETTINGS_APP);

        settings.setValue("time/displayformat", "datetime");
        std::vector<AxisTickGenerator::Tick>
        results(AxisTickGeneratorTime(QString(), &settings).generate(min, max, flags));
        QCOMPARE((int) results.size(), expected.size());
        for (int i = 0, max = results.size(); i < max; i++) {
            QCOMPARE(results.at(i).point, expected.at(i).point);
            QCOMPARE(results.at(i).label, expected.at(i).labelDateTime);
            QCOMPARE(results.at(i).secondaryLabel, expected.at(i).secondaryLabelDateTime);
            QCOMPARE(results.at(i).blockLabel, expected.at(i).blockLabelDateTime);
        }

        results = AxisTickGeneratorTime("datetime").generate(min, max, flags);
        QCOMPARE((int) results.size(), expected.size());
        for (int i = 0, max = results.size(); i < max; i++) {
            QCOMPARE(results.at(i).point, expected.at(i).point);
            QCOMPARE(results.at(i).label, expected.at(i).labelDateTime);
            QCOMPARE(results.at(i).secondaryLabel, expected.at(i).secondaryLabelDateTime);
            QCOMPARE(results.at(i).blockLabel, expected.at(i).blockLabelDateTime);
        }

        settings.setValue("time/displayformat", "yeardoy");

        results = AxisTickGeneratorTime(QString(), &settings).generate(min, max, flags);
        QCOMPARE((int) results.size(), expected.size());
        for (int i = 0, max = results.size(); i < max; i++) {
            QCOMPARE(results.at(i).point, expected.at(i).point);
            QCOMPARE(results.at(i).label, expected.at(i).labelDOY);
            QCOMPARE(results.at(i).secondaryLabel, expected.at(i).secondaryLabelDOY);
            QCOMPARE(results.at(i).blockLabel, expected.at(i).blockLabelDOY);
        }

        results = AxisTickGeneratorTime("yeardoy").generate(min, max, flags);
        QCOMPARE((int) results.size(), expected.size());
        for (int i = 0, max = results.size(); i < max; i++) {
            QCOMPARE(results.at(i).point, expected.at(i).point);
            QCOMPARE(results.at(i).label, expected.at(i).labelDOY);
            QCOMPARE(results.at(i).secondaryLabel, expected.at(i).secondaryLabelDOY);
            QCOMPARE(results.at(i).blockLabel, expected.at(i).blockLabelDOY);
        }
    }

    void generatorTime_data()
    {
        QTest::addColumn<double>("min");
        QTest::addColumn<double>("max");
        QTest::addColumn<AxisTickGenerator::GenerateFlags>("flags");
        QTest::addColumn<QList<TimeCompareTick> >("expected");

        QList<TimeCompareTick> expected;

        QTest::newRow("Empty") << 0.0 << 0.0 << AxisTickGenerator::DefaultFlags << expected;

        expected <<
                TimeCompareTick(315532800, "1980", QString(), QString(), "1980", QString(),
                                QString());
        expected <<
                TimeCompareTick(631152000, "1990", QString(), QString(), "1990", QString(),
                                QString());
        expected <<
                TimeCompareTick(946684800, "2000", QString(), QString(), "2000", QString(),
                                QString());
        expected <<
                TimeCompareTick(1262304000, "2010", QString(), QString(), "2010", QString(),
                                QString());
        QTest::newRow("Decade exact") <<
                315532800.0 <<
                1262304000.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Decade extend") <<
                315532801.0 <<
                1262303999.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        expected.removeFirst();
        expected.removeLast();
        QTest::newRow("Decade minor") <<
                315532800.0 <<
                1262304000.0 <<
                AxisTickGenerator::MinorTick <<
                expected;
        QTest::newRow("Decade minor interior") <<
                315532801.0 <<
                1262303999.0 <<
                AxisTickGenerator::MinorTick <<
                expected;

        expected.clear();
        expected <<
                TimeCompareTick(631152000, "1990", QString(), QString(), "1990", QString(),
                                QString());
        expected <<
                TimeCompareTick(694224000, "1992", QString(), QString(), "1992", QString(),
                                QString());
        expected <<
                TimeCompareTick(757382400, "1994", QString(), QString(), "1994", QString(),
                                QString());
        expected <<
                TimeCompareTick(820454400, "1996", QString(), QString(), "1996", QString(),
                                QString());
        expected <<
                TimeCompareTick(883612800, "1998", QString(), QString(), "1998", QString(),
                                QString());
        expected <<
                TimeCompareTick(946684800, "2000", QString(), QString(), "2000", QString(),
                                QString());
        QTest::newRow("Two year exact") <<
                631152000.0 <<
                946684800.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Two year extend") <<
                631152001.0 <<
                946684799.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        expected.removeFirst();
        expected.removeLast();
        QTest::newRow("Two year minor") <<
                631152000.0 <<
                946684800.0 <<
                AxisTickGenerator::MinorTick <<
                expected;
        QTest::newRow("Two year minor interior") <<
                631152001.0 <<
                946684799.0 <<
                AxisTickGenerator::MinorTick <<
                expected;

        expected.clear();
        expected <<
                TimeCompareTick(631152000, "1990", QString(), QString(), "1990", QString(),
                                QString());
        expected <<
                TimeCompareTick(662688000, "1991", QString(), QString(), "1991", QString(),
                                QString());
        expected <<
                TimeCompareTick(694224000, "1992", QString(), QString(), "1992", QString(),
                                QString());
        expected <<
                TimeCompareTick(725846400, "1993", QString(), QString(), "1993", QString(),
                                QString());
        QTest::newRow("Year exact") <<
                631152000.0 <<
                725846400.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Year extend") <<
                631152001.0 <<
                725846399.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        expected.removeFirst();
        expected.removeLast();
        QTest::newRow("Year minor") <<
                631152000.0 <<
                725846400.0 <<
                AxisTickGenerator::MinorTick <<
                expected;
        QTest::newRow("Year minor interior") <<
                631152001.0 <<
                725846399.0 <<
                AxisTickGenerator::MinorTick <<
                expected;

        expected.clear();
        expected <<
                TimeCompareTick(631152000, "1990", QString(), QString(), "1990", QString(),
                                QString());
        expected <<
                TimeCompareTick(662688000, "1991", QString(), QString(), "1991", QString(),
                                QString());
        expected <<
                TimeCompareTick(694224000, "1992", QString(), QString(), "1992", QString(),
                                QString());
        expected <<
                TimeCompareTick(725846400, "1993", QString(), QString(), "1993", QString(),
                                QString());
        QTest::newRow("Year exact") <<
                631152000.0 <<
                725846400.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Year extend") <<
                631152001.0 <<
                725846399.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        expected.removeFirst();
        expected.removeLast();
        QTest::newRow("Year minor") <<
                631152000.0 <<
                725846400.0 <<
                AxisTickGenerator::MinorTick <<
                expected;
        QTest::newRow("Year minor interior") <<
                631152001.0 <<
                725846399.0 <<
                AxisTickGenerator::MinorTick <<
                expected;

        expected.clear();
        expected << TimeCompareTick(631152000, "1", "Q1", "1990", "1", QString(), "1990");
        expected << TimeCompareTick(638928000, "91", "Q2", "1990", "4", QString(), "1990");
        expected << TimeCompareTick(646790400, "182", "Q3", "1990", "7", QString(), "1990");
        expected << TimeCompareTick(654739200, "274", "Q4", "1990", "10", QString(), "1990");
        expected << TimeCompareTick(662688000, "1", "Q1", "1991", "1", QString(), "1991");
        QTest::newRow("Quarter exact") <<
                631152000.0 <<
                662688000.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Quarter extend") <<
                631152001.0 <<
                662687999.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        expected.removeFirst();
        expected.removeLast();
        QTest::newRow("Quarter minor") <<
                631152000.0 <<
                662688000.0 <<
                AxisTickGenerator::MinorTick <<
                expected;
        QTest::newRow("Quarter minor interior") <<
                631152001.0 <<
                662687999.0 <<
                AxisTickGenerator::MinorTick <<
                expected;

        expected.clear();
        expected << TimeCompareTick(315532800, "1", "Q1", "1980", "1", QString(), "1980");
        expected << TimeCompareTick(323308800, "91", "Q2", "1980", "03-31", QString(), "1980");
        expected << TimeCompareTick(331171200, "182", "Q3", "1980", "06-30", QString(), "1980");
        expected << TimeCompareTick(339120000, "274", "Q4", "1980", "09-30", QString(), "1980");
        expected << TimeCompareTick(347155200, "1", "Q1", "1981", "1", QString(), "1981");
        QTest::newRow("Quarter leap year") <<
                315532800.0 <<
                347155200.0 <<
                AxisTickGenerator::MajorTick <<
                expected;

        expected.clear();
        expected <<
                TimeCompareTick(631152000, "1", QDate::shortMonthName(1), "1990", "1", QString(),
                                "1990");
        expected <<
                TimeCompareTick(633830400, "32", QDate::shortMonthName(2), "1990", "2", QString(),
                                "1990");
        expected <<
                TimeCompareTick(636249600, "60", QDate::shortMonthName(3), "1990", "3", QString(),
                                "1990");
        expected <<
                TimeCompareTick(638928000, "91", QDate::shortMonthName(4), "1990", "4", QString(),
                                "1990");
        expected <<
                TimeCompareTick(641520000, "121", QDate::shortMonthName(5), "1990", "5", QString(),
                                "1990");
        QTest::newRow("Month exact") <<
                631152000.0 <<
                641520000.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Month extend") <<
                631152001.0 <<
                641519999.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        expected.removeFirst();
        expected.removeLast();
        QTest::newRow("Month minor") <<
                631152000.0 <<
                641520000.0 <<
                AxisTickGenerator::MinorTick <<
                expected;
        QTest::newRow("Month minor interior") <<
                631152001.0 <<
                641519999.0 <<
                AxisTickGenerator::MinorTick <<
                expected;

        expected.clear();
        expected <<
                TimeCompareTick(318211200, "32", QDate::shortMonthName(2), "1980", "2", QString(),
                                "1980");
        expected <<
                TimeCompareTick(320716800, "61", QDate::shortMonthName(3), "1980", "3", QString(),
                                "1980");
        QTest::newRow("Month minor leap year") <<
                315532800.0 <<
                323308800.0 <<
                AxisTickGenerator::MinorTick <<
                expected;

        expected.clear();
        expected << TimeCompareTick(1262563200, "4", "W1", "2010", "01-04", QString(), "2010");
        expected << TimeCompareTick(1263168000, "11", "W2", "2010", "01-11", QString(), "2010");
        expected << TimeCompareTick(1263772800, "18", "W3", "2010", "01-18", QString(), "2010");
        expected << TimeCompareTick(1264377600, "25", "W4", "2010", "01-25", QString(), "2010");
        QTest::newRow("Week exact") <<
                1262563200.0 <<
                1264377600.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Week extend") <<
                1262563201.0 <<
                1264377599.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        expected.removeFirst();
        expected.removeLast();
        QTest::newRow("Week minor") <<
                1262563200.0 <<
                1264377600.0 <<
                AxisTickGenerator::MinorTick <<
                expected;
        QTest::newRow("Week minor interior") <<
                1262563201.0 <<
                1264377599.0 <<
                AxisTickGenerator::MinorTick <<
                expected;

        expected.clear();
        expected <<
                TimeCompareTick(1264636800, "28", QString(), "2010", "28", QString(), "2010-01");
        expected <<
                TimeCompareTick(1264723200, "29", QString(), "2010", "29", QString(), "2010-01");
        expected <<
                TimeCompareTick(1264809600, "30", QString(), "2010", "30", QString(), "2010-01");
        expected <<
                TimeCompareTick(1264896000, "31", QString(), "2010", "31", QString(), "2010-01");
        expected << TimeCompareTick(1264982400, "32", QString(), "2010", "1", QString(), "2010-02");
        QTest::newRow("Day exact") <<
                1264636800.0 <<
                1264982400.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Day extend") <<
                1264636800.0 <<
                1264982399.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        expected.removeFirst();
        expected.removeLast();
        QTest::newRow("Day minor") <<
                1264636800.0 <<
                1264982400.0 <<
                AxisTickGenerator::MinorTick <<
                expected;
        QTest::newRow("Day minor interior") <<
                1264636800.0 <<
                1264982399.0 <<
                AxisTickGenerator::MinorTick <<
                expected;

        expected.clear();
        expected <<
                TimeCompareTick(1262304000, "1.00", "00:00", "2010", "0", QString(), "2010-01-01");
        expected <<
                TimeCompareTick(1262325600, "1.25", "06:00", "2010", "6", QString(), "2010-01-01");
        expected <<
                TimeCompareTick(1262347200, "1.50", "12:00", "2010", "12", QString(), "2010-01-01");
        expected <<
                TimeCompareTick(1262368800, "1.75", "18:00", "2010", "18", QString(), "2010-01-01");
        expected <<
                TimeCompareTick(1262390400, "2.00", "00:00", "2010", "0", QString(), "2010-01-02");
        QTest::newRow("Six hour exact") <<
                1262304000.0 <<
                1262390400.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Six hour extend") <<
                1262304001.0 <<
                1262390399.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        expected.removeFirst();
        expected.removeLast();
        QTest::newRow("Six hour minor") <<
                1262304000.0 <<
                1262390400.0 <<
                AxisTickGenerator::MinorTick <<
                expected;
        QTest::newRow("Six hour minor interior") <<
                1262304001.0 <<
                1262390399.0 <<
                AxisTickGenerator::MinorTick <<
                expected;


        expected.clear();
        expected <<
                TimeCompareTick(1262304000, "1.00", "00:00", "2010", "0", QString(), "2010-01-01");
        expected <<
                TimeCompareTick(1262307600, "1.04", "01:00", "2010", "1", QString(), "2010-01-01");
        expected <<
                TimeCompareTick(1262311200, "1.08", "02:00", "2010", "2", QString(), "2010-01-01");
        expected << TimeCompareTick(1262314800, "1.13", "03:00", "2010", "3", QString(),
                                    "2010-01-01");
        expected <<
                TimeCompareTick(1262318400, "1.17", "04:00", "2010", "4", QString(), "2010-01-01");
        expected <<
                TimeCompareTick(1262322000, "1.21", "05:00", "2010", "5", QString(), "2010-01-01");
        expected <<
                TimeCompareTick(1262325600, "1.25", "06:00", "2010", "6", QString(), "2010-01-01");
        QTest::newRow("Hour exact") <<
                1262304000.0 <<
                1262325600.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Hour extend") <<
                1262304001.0 <<
                1262325599.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        expected.removeFirst();
        expected.removeLast();
        QTest::newRow("Hour minor") <<
                1262304000.0 <<
                1262325600.0 <<
                AxisTickGenerator::MinorTick <<
                expected;
        QTest::newRow("Hour minor interior") <<
                1262304001.0 <<
                1262325599.0 <<
                AxisTickGenerator::MinorTick <<
                expected;

        expected.clear();
        expected <<
                TimeCompareTick(1262304000, "1.00000", "00:00", "2010", "0", QString(),
                                "2010-01-01T00");
        expected <<
                TimeCompareTick(1262304900, "1.01042", "00:15", "2010", "15", QString(),
                                "2010-01-01T00");
        expected <<
                TimeCompareTick(1262305800, "1.02083", "00:30", "2010", "30", QString(),
                                "2010-01-01T00");
        expected <<
                TimeCompareTick(1262306700, "1.03125", "00:45", "2010", "45", QString(),
                                "2010-01-01T00");
        expected <<
                TimeCompareTick(1262307600, "1.04167", "01:00", "2010", "0", QString(),
                                "2010-01-01T01");
        QTest::newRow("15 minute exact") <<
                1262304000.0 <<
                1262307600.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("15 minute extend") <<
                1262304001.0 <<
                1262307599.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        expected.removeFirst();
        expected.removeLast();
        QTest::newRow("15 minute minor") <<
                1262304000.0 <<
                1262307600.0 <<
                AxisTickGenerator::MinorTick <<
                expected;
        QTest::newRow("15 minute minor interior") <<
                1262304001.0 <<
                1262307599.0 <<
                AxisTickGenerator::MinorTick <<
                expected;

        expected.clear();
        expected <<
                TimeCompareTick(1262304900, "1.01042", "00:15", "2010", "15", QString(),
                                "2010-01-01T00");
        expected <<
                TimeCompareTick(1262305200, "1.01389", "00:20", "2010", "20", QString(),
                                "2010-01-01T00");
        expected <<
                TimeCompareTick(1262305500, "1.01736", "00:25", "2010", "25", QString(),
                                "2010-01-01T00");
        expected <<
                TimeCompareTick(1262305800, "1.02083", "00:30", "2010", "30", QString(),
                                "2010-01-01T00");
        expected <<
                TimeCompareTick(1262306100, "1.02431", "00:35", "2010", "35", QString(),
                                "2010-01-01T00");
        QTest::newRow("5 minute exact") <<
                1262304900.0 <<
                1262306100.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("5 minute extend") <<
                1262304901.0 <<
                1262306099.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        expected.removeFirst();
        expected.removeLast();
        QTest::newRow("5 minute minor") <<
                1262304900.0 <<
                1262306100.0 <<
                AxisTickGenerator::MinorTick <<
                expected;
        QTest::newRow("5 minute minor interior") <<
                1262304901.0 <<
                1262306099.0 <<
                AxisTickGenerator::MinorTick <<
                expected;

        expected.clear();
        expected <<
                TimeCompareTick(1262304900, "1.01042", "00:15", "2010", "15", QString(),
                                "2010-01-01T00");
        expected <<
                TimeCompareTick(1262304960, "1.01111", "00:16", "2010", "16", QString(),
                                "2010-01-01T00");
        expected <<
                TimeCompareTick(1262305020, "1.01181", "00:17", "2010", "17", QString(),
                                "2010-01-01T00");
        expected <<
                TimeCompareTick(1262305080, "1.01250", "00:18", "2010", "18", QString(),
                                "2010-01-01T00");
        expected <<
                TimeCompareTick(1262305140, "1.01319", "00:19", "2010", "19", QString(),
                                "2010-01-01T00");
        QTest::newRow("Minute exact") <<
                1262304900.0 <<
                1262305140.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Minute extend") <<
                1262304901.0 <<
                1262305139.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        expected.removeFirst();
        expected.removeLast();
        QTest::newRow("Minute minor") <<
                1262304900.0 <<
                1262305140.0 <<
                AxisTickGenerator::MinorTick <<
                expected;
        QTest::newRow("Minute minor interior") <<
                1262304901.0 <<
                1262305139.0 <<
                AxisTickGenerator::MinorTick <<
                expected;


        expected.clear();
        expected <<
                TimeCompareTick(1262304900, "1.01042", "00:15:00", "2010", "0", QString(),
                                "2010-01-01T00:15");
        expected <<
                TimeCompareTick(1262304915, "1.01059", "00:15:15", "2010", "15", QString(),
                                "2010-01-01T00:15");
        expected <<
                TimeCompareTick(1262304930, "1.01076", "00:15:30", "2010", "30", QString(),
                                "2010-01-01T00:15");
        expected <<
                TimeCompareTick(1262304945, "1.01094", "00:15:45", "2010", "45", QString(),
                                "2010-01-01T00:15");
        expected <<
                TimeCompareTick(1262304960, "1.01111", "00:16:00", "2010", "0", QString(),
                                "2010-01-01T00:16");
        QTest::newRow("15 second exact") <<
                1262304900.0 <<
                1262304960.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("15 second extend") <<
                1262304901.0 <<
                1262304959.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        expected.removeFirst();
        expected.removeLast();
        QTest::newRow("15 second minor") <<
                1262304900.0 <<
                1262304960.0 <<
                AxisTickGenerator::MinorTick <<
                expected;
        QTest::newRow("15 second minor interior") <<
                1262304901.0 <<
                1262304959.0 <<
                AxisTickGenerator::MinorTick <<
                expected;

        expected.clear();
        expected <<
                TimeCompareTick(1262304900, "1.01042", "00:15:00", "2010", "0", QString(),
                                "2010-01-01T00:15");
        expected <<
                TimeCompareTick(1262304905, "1.01047", "00:15:05", "2010", "5", QString(),
                                "2010-01-01T00:15");
        expected <<
                TimeCompareTick(1262304910, "1.01053", "00:15:10", "2010", "10", QString(),
                                "2010-01-01T00:15");
        expected <<
                TimeCompareTick(1262304915, "1.01059", "00:15:15", "2010", "15", QString(),
                                "2010-01-01T00:15");
        expected <<
                TimeCompareTick(1262304920, "1.01065", "00:15:20", "2010", "20", QString(),
                                "2010-01-01T00:15");
        QTest::newRow("5 second exact") <<
                1262304900.0 <<
                1262304920.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("5 second extend") <<
                1262304901.0 <<
                1262304919.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        expected.removeFirst();
        expected.removeLast();
        QTest::newRow("5 second minor") <<
                1262304900.0 <<
                1262304920.0 <<
                AxisTickGenerator::MinorTick <<
                expected;
        QTest::newRow("5 second minor interior") <<
                1262304901.0 <<
                1262304919.0 <<
                AxisTickGenerator::MinorTick <<
                expected;

        expected.clear();
        expected <<
                TimeCompareTick(1262304900, "1.01042", "00:15:00", "2010", "0", QString(),
                                "2010-01-01T00:15");
        expected <<
                TimeCompareTick(1262304901, "1.01043", "00:15:01", "2010", "1", QString(),
                                "2010-01-01T00:15");
        expected <<
                TimeCompareTick(1262304902, "1.01044", "00:15:02", "2010", "2", QString(),
                                "2010-01-01T00:15");
        expected <<
                TimeCompareTick(1262304903, "1.01045", "00:15:03", "2010", "3", QString(),
                                "2010-01-01T00:15");
        expected <<
                TimeCompareTick(1262304904, "1.01046", "00:15:04", "2010", "4", QString(),
                                "2010-01-01T00:15");
        expected <<
                TimeCompareTick(1262304905, "1.01047", "00:15:05", "2010", "5", QString(),
                                "2010-01-01T00:15");
        QTest::newRow("Second exact") <<
                1262304900.0 <<
                1262304905.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Second extend") <<
                1262304900.1 <<
                1262304904.9 <<
                AxisTickGenerator::MajorTick <<
                expected;
        expected.removeFirst();
        expected.removeLast();
        QTest::newRow("Second minor") <<
                1262304900.0 <<
                1262304905.0 <<
                AxisTickGenerator::MinorTick <<
                expected;
        QTest::newRow("Second minor interior") <<
                1262304900.1 <<
                1262304904.9 <<
                AxisTickGenerator::MinorTick <<
                expected;

        expected.clear();
        expected <<
                TimeCompareTick(1294073217.5, "-500", QString(), "Milliseconds from 2011 3.69928",
                                "-500", QString(), "Milliseconds from 2011-01-03T16:46:58Z");
        expected <<
                TimeCompareTick(1294073218, "0", QString(), "Milliseconds from 2011 3.69928", "0",
                                QString(), "Milliseconds from 2011-01-03T16:46:58Z");
        expected <<
                TimeCompareTick(1294073218.5, "500", QString(), "Milliseconds from 2011 3.69928",
                                "500", QString(), "Milliseconds from 2011-01-03T16:46:58Z");
        QTest::newRow("Sub-second exact") <<
                1294073217.5 <<
                1294073218.5 <<
                AxisTickGenerator::MajorTick <<
                expected;

        expected.clear();
        expected <<
                TimeCompareTick(1294073217.75, "-250", QString(), "Milliseconds from 2011 3.69928",
                                "-250", QString(), "Milliseconds from 2011-01-03T16:46:58Z");
        expected <<
                TimeCompareTick(1294073218, "0", QString(), "Milliseconds from 2011 3.69928", "0",
                                QString(), "Milliseconds from 2011-01-03T16:46:58Z");
        expected <<
                TimeCompareTick(1294073218.25, "250", QString(), "Milliseconds from 2011 3.69928",
                                "250", QString(), "Milliseconds from 2011-01-03T16:46:58Z");
        QTest::newRow("Sub-second minor") <<
                1294073217.5 <<
                1294073218.5 <<
                AxisTickGenerator::MinorTick <<
                expected;
    }


    void generatorLog()
    {
        QFETCH(double, min);
        QFETCH(double, max);
        QFETCH(AxisTickGenerator::GenerateFlags, flags);
        QFETCH(std::vector<AxisTickGenerator::Tick>, expected);

        std::vector<AxisTickGenerator::Tick>
                results(AxisTickGeneratorLog().generate(min, max, flags));
        QCOMPARE(results.size(), expected.size());
        for (int i = 0, max = results.size(); i < max; i++) {
            QCOMPARE(results.at(i).point, expected.at(i).point);
            QCOMPARE(results.at(i).label, expected.at(i).label);
            QCOMPARE(results.at(i).secondaryLabel, expected.at(i).secondaryLabel);
            QCOMPARE(results.at(i).blockLabel, expected.at(i).blockLabel);
        }
    }

    void generatorLog_data()
    {
        QTest::addColumn<double>("min");
        QTest::addColumn<double>("max");
        QTest::addColumn<AxisTickGenerator::GenerateFlags>("flags");
        QTest::addColumn<std::vector<AxisTickGenerator::Tick> >("expected");

        std::vector<AxisTickGenerator::Tick> expected;

        QTest::newRow("Empty major") << 1.0 << 1.0 << AxisTickGenerator::MajorTick << expected;
        QTest::newRow("Empty minor") << 1.0 << 1.0 << AxisTickGenerator::MinorTick << expected;
        QTest::newRow("Invalid major") << -10.0 << -1.0 << AxisTickGenerator::MajorTick << expected;

        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(2.0, "2", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(3.0, "3", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(4.0, "4", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(5.0, "5", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(6.0, "6", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(7.0, "7", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(8.0, "8", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(9.0, "9", QString(), QString()));
        QTest::newRow("Minor tens") << 1.0 << 10.0 << AxisTickGenerator::MinorTick << expected;

        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(20.0, "20", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(30.0, "30", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(40.0, "40", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(50.0, "50", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(60.0, "60", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(70.0, "70", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(80.0, "80", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(90.0, "90", QString(), QString()));
        QTest::newRow("Minor hundreds") << 10.0 << 100.0 << AxisTickGenerator::MinorTick
                                        << expected;


        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(0.1, "0.1", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(1.0, "1", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(10.0, "10", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(100.0, "100", QString(), QString()));
        QTest::newRow("Basic scale") << 0.1 << 100.0 << AxisTickGenerator::MajorTick << expected;
        expected.insert(expected.begin(),
                        AxisTickGenerator::Tick(0.01, "0.01", QString(), QString()));
        expected.push_back(AxisTickGenerator::Tick(1000.0, "1000", QString(), QString()));
        QTest::newRow("Basic scale extended") << 0.05 << 150.0 << AxisTickGenerator::MajorTick
                                              << expected;

        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(1E1, "1E1", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(1E2, "1E2", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(1E3, "1E3", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(1E4, "1E4", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(1E5, "1E5", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(1E6, "1E6", QString(), QString()));
        QTest::newRow("Exponent scale") << 1E1 << 2E5 << AxisTickGenerator::MajorTick << expected;

        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(1E-1, "1E-1", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(1, "1E+0", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(1E1, "1E+1", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(1E2, "1E+2", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(1E3, "1E+3", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(1E4, "1E+4", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(1E5, "1E+5", QString(), QString()));
        QTest::newRow("Exponent scale negative") << 1E-1 << 1E5 << AxisTickGenerator::MajorTick
                                                 << expected;

        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(20.0, "20", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(30.0, "30", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(40.0, "40", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(50.0, "50", QString(), QString()));
        QTest::newRow("Small") << 25.0 << 45.0 << AxisTickGenerator::MajorTick << expected;
        QTest::newRow("Small inclusive") << 20.0 << 50.0 << AxisTickGenerator::MajorTick
                                         << expected;

        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(2E5, "2E5", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(3E5, "3E5", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(4E5, "4E5", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(5E5, "5E5", QString(), QString()));
        QTest::newRow("Small exponent") << 2.5E5 << 4.5E5 << AxisTickGenerator::MajorTick
                                        << expected;
        QTest::newRow("Small exponent inclusive") << 2E5 << 5E5 << AxisTickGenerator::MajorTick
                                                  << expected;

        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(71.0, "71", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(72.0, "72", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(73.0, "73", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(74.0, "74", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(75.0, "75", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(76.0, "76", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(77.0, "77", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(78.0, "78", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(79.0, "79", QString(), QString()));
        QTest::newRow("Small minor") << 70.0 << 80.0 << AxisTickGenerator::MinorTick << expected;
    }


    void generatorConstant()
    {
        QFETCH(double, min);
        QFETCH(double, max);
        QFETCH(double, interval);
        QFETCH(double, origin);
        QFETCH(AxisTickGenerator::GenerateFlags, flags);
        QFETCH(std::vector<AxisTickGenerator::Tick>, expected);

        std::vector<AxisTickGenerator::Tick>
                results(AxisTickGeneratorConstant(interval, origin).generate(min, max, flags));
        QCOMPARE(results.size(), expected.size());
        for (int i = 0, max = results.size(); i < max; i++) {
            QCOMPARE(results.at(i).point, expected.at(i).point);
            QCOMPARE(results.at(i).label, expected.at(i).label);
            QCOMPARE(results.at(i).secondaryLabel, expected.at(i).secondaryLabel);
            QCOMPARE(results.at(i).blockLabel, expected.at(i).blockLabel);
        }
    }

    void generatorConstant_data()
    {
        QTest::addColumn<double>("min");
        QTest::addColumn<double>("max");
        QTest::addColumn<double>("interval");
        QTest::addColumn<double>("origin");
        QTest::addColumn<AxisTickGenerator::GenerateFlags>("flags");
        QTest::addColumn<std::vector<AxisTickGenerator::Tick> >("expected");

        std::vector<AxisTickGenerator::Tick> expected;

        QTest::newRow("Empty major") <<
                                     1.0 <<
                                     1.0 <<
                                     1.0 <<
                                     FP::undefined() <<
                                     AxisTickGenerator::MajorTick << expected;
        QTest::newRow("Empty minor") << 1.0 << 1.0 << 1.0 << FP::undefined()
                                     << AxisTickGenerator::MinorTick << expected;

        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(1.0, "1", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(2.0, "2", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(3.0, "3", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(4.0, "4", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(5.0, "5", QString(), QString()));
        QTest::newRow("Major one") << 1.0 << 5.0 << 1.0 << FP::undefined()
                                   << AxisTickGenerator::MajorTick << expected;
        QTest::newRow("Major one origin") << 1.0 << 5.0 <<
                1.0 <<
                1.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Major one origin outside above") <<
                1.0 <<
                5.0 <<
                1.0 <<
                20.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Major one origin outside below") <<
                1.0 <<
                5.0 <<
                1.0 <<
                -15.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Major one extend origin") <<
                1.25 <<
                4.9 <<
                1.0 <<
                1.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Major one extend origin outside above") <<
                1.25 <<
                4.9 <<
                1.0 <<
                20.0 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Major one extend origin outside below") << 1.25 << 4.9 << 1.0 << -15.0
                                                               << AxisTickGenerator::MajorTick
                                                               << expected;


        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(1.5, "1.5", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(3.5, "3.5", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(5.5, "5.5", QString(), QString()));
        QTest::newRow("Major two") << 1.5 << 5.5 << 2.0 << FP::undefined()
                                   << AxisTickGenerator::MajorTick << expected;
        QTest::newRow("Major two origin") << 1.5 << 5.5 <<
                2.0 <<
                1.5 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Major two origin outside above") <<
                1.5 <<
                5.5 <<
                2.0 <<
                7.5 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Major two origin outside below") <<
                1.5 <<
                5.5 <<
                2.0 <<
                -0.5 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Major two extend origin") <<
                1.6 <<
                5.4 <<
                2.0 <<
                1.5 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Major two extend origin outside above") <<
                1.6 <<
                5.4 <<
                2.0 <<
                7.5 <<
                AxisTickGenerator::MajorTick <<
                expected;
        QTest::newRow("Major two extend origin outside below") << 1.6 << 5.4 << 2.0 << -0.5
                                                               << AxisTickGenerator::MajorTick
                                                               << expected;


        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(0.25, "0.25", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(0.50, "0.50", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(0.75, "0.75", QString(), QString()));
        QTest::newRow("Minor five") << 0.0 << 1.0 << 10.0 << FP::undefined()
                                    << AxisTickGenerator::MinorTick << expected;

        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(1.0, "1", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(2.0, "2", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(3.0, "3", QString(), QString()));
        QTest::newRow("Minor four") << 0.0 << 4.0 << 10.0 << FP::undefined()
                                    << AxisTickGenerator::MinorTick << expected;
    }

    void generatorNormalQuantiles()
    {
        QFETCH(double, min);
        QFETCH(double, max);
        QFETCH(AxisTickGenerator::GenerateFlags, flags);
        QFETCH(std::vector<AxisTickGenerator::Tick>, expected);

        std::vector<AxisTickGenerator::Tick>
                results(AxisTickGeneratorNormalQuantiles().generate(min, max, flags));
        QCOMPARE(results.size(), expected.size());
        for (int i = 0, max = results.size(); i < max; i++) {
            QVERIFY(fabs(results.at(i).point - expected.at(i).point) < 0.01);
            QCOMPARE(results.at(i).label, expected.at(i).label);
            QCOMPARE(results.at(i).secondaryLabel, expected.at(i).secondaryLabel);
            QCOMPARE(results.at(i).blockLabel, expected.at(i).blockLabel);
        }
    }

    void generatorNormalQuantiles_data()
    {
        QTest::addColumn<double>("min");
        QTest::addColumn<double>("max");
        QTest::addColumn<AxisTickGenerator::GenerateFlags>("flags");
        QTest::addColumn<std::vector<AxisTickGenerator::Tick> >("expected");

        std::vector<AxisTickGenerator::Tick> expected;

        QTest::newRow("Empty major") << 1.0 << 1.0 << AxisTickGenerator::MajorTick << expected;
        QTest::newRow("Empty minor") << 1.0 << 1.0 << AxisTickGenerator::MinorTick << expected;

        expected.clear();
        expected.emplace_back(
                AxisTickGenerator::Tick(-1.281552, "10", QString::fromUtf8("-1.28\xCF\x83"),
                                        QString()));
        expected.emplace_back(
                AxisTickGenerator::Tick(-0.6744898, "25", QString::fromUtf8("-0.67\xCF\x83"),
                                        QString()));
        expected.emplace_back(
                AxisTickGenerator::Tick(0.0, "50", QString::fromUtf8("0.00\xCF\x83"), QString()));
        expected.emplace_back(
                AxisTickGenerator::Tick(0.6744898, "75", QString::fromUtf8("0.67\xCF\x83"),
                                        QString()));
        expected.emplace_back(
                AxisTickGenerator::Tick(1.281552, "90", QString::fromUtf8("1.28\xCF\x83"),
                                        QString()));
        QTest::newRow("Major basic") << Statistics::normalQuantile(0.1)
                                     << Statistics::normalQuantile(0.9)
                                     << AxisTickGenerator::MajorTick << expected;
    }


    void generatorTimeInterval()
    {
        QFETCH(double, min);
        QFETCH(double, max);
        QFETCH(AxisTickGenerator::GenerateFlags, flags);
        QFETCH(std::vector<AxisTickGenerator::Tick>, expected);

        std::vector<AxisTickGenerator::Tick>
                results(AxisTickGeneratorTimeInterval().generate(min, max, flags));
        QCOMPARE(results.size(), expected.size());
        for (std::size_t i = 0, nResults = results.size(); i < nResults; i++) {
            QCOMPARE(results.at(i).point, expected.at(i).point);
            QCOMPARE(results.at(i).label, expected.at(i).label);
            QCOMPARE(results.at(i).secondaryLabel, expected.at(i).secondaryLabel);
            QCOMPARE(results.at(i).blockLabel, expected.at(i).blockLabel);
        }
    }

    void generatorTimeInterval_data()
    {
        QTest::addColumn<double>("min");
        QTest::addColumn<double>("max");
        QTest::addColumn<AxisTickGenerator::GenerateFlags>("flags");
        QTest::addColumn<std::vector<AxisTickGenerator::Tick> >("expected");

        std::vector<AxisTickGenerator::Tick> expected;

        QTest::newRow("Empty major") << 1.0 << 1.0 << AxisTickGenerator::MajorTick << expected;
        QTest::newRow("Empty minor") << 1.0 << 1.0 << AxisTickGenerator::MinorTick << expected;

        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(1.0, "1S", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(60.0, "1M", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(3600.0, "1H", QString(), QString()));
        QTest::newRow("Major single") << 30.0 << 1800.0 << AxisTickGenerator::MajorTick << expected;
        expected.emplace_back(AxisTickGenerator::Tick(86400.0, "1D", QString(), QString()));
        QTest::newRow("Major basic") << 1.0 << 86400.0 << AxisTickGenerator::MajorTick << expected;
        QTest::newRow("Major extend") << 1.1 << 86399.0 << AxisTickGenerator::MajorTick << expected;

        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(15.0, "15S", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(30.0, "30S", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(45.0, "45S", QString(), QString()));
        QTest::newRow("Major restricted") << 15.0 << 45.0 << AxisTickGenerator::MajorTick
                                          << expected;
        QTest::newRow("Major restricted extend") << 16.0 << 44.0 << AxisTickGenerator::MajorTick
                                                 << expected;
        QTest::newRow("Minor basic") << 1.0 << 60.0 << AxisTickGenerator::MinorTick << expected;

        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(45.0, "45S", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(60.0, "60S", QString(), QString()));
        QTest::newRow("Major small restricted") << 46.0 << 59.0 << AxisTickGenerator::MajorTick
                                                << expected;
        expected.clear();
        QTest::newRow("Minor small restricted") << 45.0 << 60.0 << AxisTickGenerator::MinorTick
                                                << expected;

        expected.clear();
        expected.emplace_back(AxisTickGenerator::Tick(0.0, "0M", QString(), QString()));
        expected.emplace_back(AxisTickGenerator::Tick(900.0, "15M", QString(), QString()));
        QTest::newRow("Major degenerate") << 125.0 << 310.0 << AxisTickGenerator::MajorTick
                                          << expected;

        expected.clear();
        QTest::newRow("Minor degenerate") << 0.0 << 900.0 <<
                AxisTickGenerator::MinorTick <<
                expected;
    }

};

QTEST_APPLESS_MAIN(TestAxisTickGenerator)

#include "axistickgenerator.moc"
