/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <time.h>
#include <math.h>
#include <QtGlobal>
#include <QTest>

#include "graphing/axistickgenerator.hxx"
#include "graphing/axislabelengine2d.hxx"

using namespace CPD3::Graphing;

class TestAxisLabelEngine2D : public QObject {
Q_OBJECT
private slots:

    void sizing()
    {
        QFETCH(Axis2DSide, position);

        QCOMPARE(AxisLabelEngine2D(position).predictSize(
                         std::vector<AxisTickGenerator::Tick>()), 0.0);

        std::vector<AxisTickGenerator::Tick> labelsOnly
                {AxisTickGenerator::Tick(0, "L1", "", ""), AxisTickGenerator::Tick(1, "L2", "", ""),
                 AxisTickGenerator::Tick(2, "L3", "", ""),
                 AxisTickGenerator::Tick(3, "L4", "", "")};
        std::vector<AxisTickGenerator::Tick> secondaryLabelsOnly
                {AxisTickGenerator::Tick(0, "", "SL1", ""),
                 AxisTickGenerator::Tick(1, "", "SL2", ""),
                 AxisTickGenerator::Tick(2, "", "SL3", ""),
                 AxisTickGenerator::Tick(3, "", "SL4", "")};
        std::vector<AxisTickGenerator::Tick> blockLabelsOnly
                {AxisTickGenerator::Tick(0, "", "", "BL1"),
                 AxisTickGenerator::Tick(1, "", "", "BL1"),
                 AxisTickGenerator::Tick(2, "", "", "BL1"),
                 AxisTickGenerator::Tick(3, "", "", "BL2")};
        std::vector<AxisTickGenerator::Tick> labelsNoBlock
                {AxisTickGenerator::Tick(0, "L1", "SL1", ""),
                 AxisTickGenerator::Tick(1, "L2", "SL2", ""),
                 AxisTickGenerator::Tick(2, "L3", "SL3", ""),
                 AxisTickGenerator::Tick(3, "L4", "SL4", "")};
        std::vector<AxisTickGenerator::Tick> noSecondaryLabels
                {AxisTickGenerator::Tick(0, "L1", "", "BL1"),
                 AxisTickGenerator::Tick(1, "L2", "", "BL1"),
                 AxisTickGenerator::Tick(2, "L3", "", "BL1"),
                 AxisTickGenerator::Tick(3, "L4", "", "BL2")};
        std::vector<AxisTickGenerator::Tick> noLabels{AxisTickGenerator::Tick(0, "", "SL1", "BL1"),
                                                      AxisTickGenerator::Tick(1, "", "SL2", "BL1"),
                                                      AxisTickGenerator::Tick(2, "", "SL3", "BL1"),
                                                      AxisTickGenerator::Tick(3, "", "SL4", "BL2")};
        std::vector<AxisTickGenerator::Tick> all{AxisTickGenerator::Tick(0, "L1", "SL1", "BL1"),
                                                 AxisTickGenerator::Tick(1, "L2", "SL2", "BL1"),
                                                 AxisTickGenerator::Tick(2, "L3", "SL3", "BL1"),
                                                 AxisTickGenerator::Tick(3, "L4", "SL4", "BL2")};

        double sLabelsOnly = AxisLabelEngine2D(position).predictSize(labelsOnly);
        double sSecondaryLabelsOnly = AxisLabelEngine2D(position).predictSize(secondaryLabelsOnly);
        double sBlockLabelsOnly = AxisLabelEngine2D(position).predictSize(blockLabelsOnly);
        double sLabelsNoBlock = AxisLabelEngine2D(position).predictSize(labelsNoBlock);
        double sNoSecondaryLabels = AxisLabelEngine2D(position).predictSize(noSecondaryLabels);
        double sNoLabels = AxisLabelEngine2D(position).predictSize(noLabels);
        double sAll = AxisLabelEngine2D(position).predictSize(all);

        QVERIFY(sLabelsOnly > 0.0);
        QVERIFY(sLabelsOnly < sLabelsNoBlock);
        QVERIFY(sLabelsOnly < sAll);
        QVERIFY(sSecondaryLabelsOnly > 0.0);
        QVERIFY(sSecondaryLabelsOnly < sLabelsNoBlock);
        QVERIFY(sSecondaryLabelsOnly < sAll);
        QVERIFY(sBlockLabelsOnly > 0.0);
        QVERIFY(sBlockLabelsOnly < sNoSecondaryLabels);
        QVERIFY(sBlockLabelsOnly < sAll);
        QVERIFY(sLabelsNoBlock < sAll);
        QVERIFY(sNoSecondaryLabels < sAll);
        QVERIFY(sNoLabels < sAll);

        QCOMPARE(AxisLabelEngine2D(position, AxisLabelEngine2D::Labels).predictSize(all),
                 sLabelsOnly);
        QCOMPARE(AxisLabelEngine2D(position, AxisLabelEngine2D::SecondaryLabels).predictSize(all),
                 sSecondaryLabelsOnly);
        QCOMPARE(AxisLabelEngine2D(position, AxisLabelEngine2D::BlockLabels).predictSize(all),
                 sBlockLabelsOnly);
        QCOMPARE(AxisLabelEngine2D(position, AxisLabelEngine2D::Labels |
                AxisLabelEngine2D::SecondaryLabels).predictSize(all), sLabelsNoBlock);
        QCOMPARE(AxisLabelEngine2D(position, AxisLabelEngine2D::Labels |
                AxisLabelEngine2D::BlockLabels).predictSize(all), sNoSecondaryLabels);
        QCOMPARE(AxisLabelEngine2D(position, AxisLabelEngine2D::SecondaryLabels |
                AxisLabelEngine2D::BlockLabels).predictSize(all), sNoLabels);
    }

    void sizing_data()
    {
        QTest::addColumn<Axis2DSide>("position");

        QTest::newRow("Top") << Axis2DTop;
        QTest::newRow("Bottom") << Axis2DBottom;
        QTest::newRow("Left") << Axis2DLeft;
        QTest::newRow("Right") << Axis2DRight;
    }
};

QTEST_MAIN(TestAxisLabelEngine2D)

#include "axislabelengine2d.moc"
