/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtAlgorithms>
#include <QObject>
#include <QString>
#include <QTest>
#include <QWaitCondition>

#include "graphing/traceset.hxx"
#include "datacore/stream.hxx"
#include "datacore/processingtapchain.hxx"
#include "datacore/segment.hxx"
#include "core/number.hxx"
#include "core/timeutils.hxx"
#include "algorithms/model.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Graphing;
using namespace CPD3::Algorithms;

class DispatchBackend : public ProcessingTapChain::ArchiveBackend {
public:
    SequenceValue::Transfer values;

    DispatchBackend(const SequenceValue::Transfer &sv) : values(sv)
    { }

    virtual ~DispatchBackend()
    { }

    void issueArchiveRead(const Archive::Selection::List &, StreamSink *target) override
    {
        if (!values.empty())
            target->incomingData(values);
        target->endData();
    }
};

class TestTraceSet : public QObject {
Q_OBJECT
private slots:

    void basic()
    {
        TraceDispatchSet<2> set;
        Variant::Root v;
        v["All/Data/Dimensions/#0/Input"].setString("Bs[BR]_S11");
        v["All/Data/Dimensions/#1/Input"].setString("BsG_S11");
        v["All/Override/0/Match/#0"].setString("BsR_S11");
        v["All/Override/0/Settings/Transformer/Type"].setString("Constant");
        v["All/Override/0/Settings/Transformer/Value"].setDouble(42.0);
        set.initialize(ValueSegment::Transfer{ValueSegment(FP::undefined(), FP::undefined(), v)});

        DispatchBackend backend(SequenceValue::Transfer{
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(1.0), 100.0,
                              125.0),
                SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(2.0), 100.0,
                              125.0),
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(3.0), 125.0,
                              150.0),
                SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(4.0), 125.0,
                              150.0),
                SequenceValue(SequenceName("brw", "raw", "BsR_S11"), Variant::Root(5.0), 125.0,
                              150.0)});
        ProcessingTapChain chain;
        chain.setBackend(&backend);
        set.registerChain(&chain);
        chain.start();
        chain.wait();
        set.process();

        auto handlers = set.getHandlers();
        QCOMPARE((int) handlers.size(), 2);

        TraceLimits<2> limits(handlers[0]->getLimits());
        QCOMPARE(limits.min[0], 2.0);
        QCOMPARE(limits.max[0], 4.0);
        QCOMPARE(limits.min[1], 1.0);
        QCOMPARE(limits.max[1], 3.0);
        auto points = handlers[0]->getPoints();
        QCOMPARE((int) points.size(), 2);
        QCOMPARE(points[0].start, 100.0);
        QCOMPARE(points[0].end, 125.0);
        QCOMPARE(points[0].d[0], 2.0);
        QCOMPARE(points[0].d[1], 1.0);
        QCOMPARE(points[1].start, 125.0);
        QCOMPARE(points[1].end, 150.0);
        QCOMPARE(points[1].d[0], 4.0);
        QCOMPARE(points[1].d[1], 3.0);

        limits = handlers[1]->getLimits();
        QCOMPARE(limits.min[0], 42.0);
        QCOMPARE(limits.max[0], 42.0);
        QCOMPARE(limits.min[1], 42.0);
        QCOMPARE(limits.max[1], 42.0);
        points = handlers[1]->getPoints();
        QCOMPARE((int) points.size(), 1);
        QCOMPARE(points[0].start, 125.0);
        QCOMPARE(points[0].end, 150.0);
        QCOMPARE(points[0].d[0], 42.0);
        QCOMPARE(points[0].d[1], 42.0);


        set.setVisibleTimeRange(50, 110);
        set.process();
        handlers = set.getHandlers();

        limits = handlers[0]->getLimits();
        QCOMPARE(limits.min[0], 2.0);
        QCOMPARE(limits.max[0], 2.0);
        QCOMPARE(limits.min[1], 1.0);
        QCOMPARE(limits.max[1], 1.0);
        points = handlers[0]->getPoints();
        QCOMPARE((int) points.size(), 1);
        QCOMPARE(points[0].start, 100.0);
        QCOMPARE(points[0].end, 125.0);
        QCOMPARE(points[0].d[0], 2.0);
        QCOMPARE(points[0].d[1], 1.0);

        limits = handlers[1]->getLimits();
        QVERIFY(!FP::defined(limits.min[0]));
        QVERIFY(!FP::defined(limits.max[0]));
        QVERIFY(!FP::defined(limits.min[1]));
        QVERIFY(!FP::defined(limits.max[1]));
        QVERIFY(handlers[1]->getPoints().empty());


        set.setVisibleTimeRange(126, FP::undefined());

        /* Probably not the best way to check the deferred update status. */
        double timeout = Time::time() + 10.0;
        do {
            if (set.process(false, true))
                break;
        } while (Time::time() < timeout);
        if (Time::time() >= timeout)
            QFAIL("Timeout waiting for process update.");

        handlers = set.getHandlers();

        limits = handlers[0]->getLimits();
        QCOMPARE(limits.min[0], 4.0);
        QCOMPARE(limits.max[0], 4.0);
        QCOMPARE(limits.min[1], 3.0);
        QCOMPARE(limits.max[1], 3.0);
        points = handlers[0]->getPoints();
        QCOMPARE((int) points.size(), 1);
        QCOMPARE(points[0].start, 125.0);
        QCOMPARE(points[0].end, 150.0);
        QCOMPARE(points[0].d[0], 4.0);
        QCOMPARE(points[0].d[1], 3.0);

        limits = handlers[1]->getLimits();
        QCOMPARE(limits.min[0], 42.0);
        QCOMPARE(limits.max[0], 42.0);
        QCOMPARE(limits.min[1], 42.0);
        QCOMPARE(limits.max[1], 42.0);
        points = handlers[1]->getPoints();
        QCOMPARE((int) points.size(), 1);
        QCOMPARE(points[0].start, 125.0);
        QCOMPARE(points[0].end, 150.0);
        QCOMPARE(points[0].d[0], 42.0);
        QCOMPARE(points[0].d[1], 42.0);
    }

    void arrayFanout()
    {
        TraceDispatchSet<2> set;
        Variant::Root v;
        v["All/Data/Dimensions/#0/Input"].setString("BsB_S11");
        v["All/Data/Dimensions/#1/Input"].setString("BsG_S11");
        v["All/Override/0/Match/#0"].setString("BsR_S11");
        v["All/Override/0/Settings/Transformer/Type"].setString("Constant");
        v["All/Override/0/Settings/Transformer/Value"].setDouble(42.0);
        set.initialize(ValueSegment::Transfer{ValueSegment(FP::undefined(), FP::undefined(), v)});

        Variant::Root a1;
        Variant::Root a2;

        a1.write().array(0).setDouble(1.0);
        a1.write().array(1).setDouble(2.0);
        a1.write().array(2).setDouble(3.0);
        a2.write().array(0).setDouble(4.0);
        a2.write().array(1).setDouble(5.0);
        a2.write().array(2).setDouble(6.0);

        DispatchBackend backend(SequenceValue::Transfer{
                SequenceValue(SequenceName("brw", "raw", "BsB_S11"), a1, 100.0, 125.0),
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), a2, 100.0, 125.0),
                SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(1.5), 125.0,
                              150.0),
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), a2, 125.0, 150.0),
                SequenceValue(SequenceName("brw", "raw", "BsB_S11"), a1, 150.0, 175.0),
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(4.5), 150.0,
                              175.0)});
        ProcessingTapChain chain;
        chain.setBackend(&backend);
        set.registerChain(&chain);
        chain.start();
        chain.wait();
        set.process();

        auto handlers = set.getHandlers();
        QCOMPARE((int) handlers.size(), 1);

        TraceLimits<2> limits(handlers[0]->getLimits());
        QCOMPARE(limits.min[0], 1.0);
        QCOMPARE(limits.max[0], 3.0);
        QCOMPARE(limits.min[1], 4.0);
        QCOMPARE(limits.max[1], 6.0);
        auto points = handlers[0]->getPoints();
        QCOMPARE((int) points.size(), 9);

        QCOMPARE(points[0].start, 100.0);
        QCOMPARE(points[0].end, 125.0);
        QCOMPARE(points[0].d[0], 1.0);
        QCOMPARE(points[0].d[1], 4.0);
        QCOMPARE(points[1].start, 100.0);
        QCOMPARE(points[1].end, 125.0);
        QCOMPARE(points[1].d[0], 2.0);
        QCOMPARE(points[1].d[1], 5.0);
        QCOMPARE(points[2].start, 100.0);
        QCOMPARE(points[2].end, 125.0);
        QCOMPARE(points[2].d[0], 3.0);
        QCOMPARE(points[2].d[1], 6.0);

        QCOMPARE(points[3].start, 125.0);
        QCOMPARE(points[3].end, 150.0);
        QCOMPARE(points[3].d[0], 1.5);
        QCOMPARE(points[3].d[1], 4.0);
        QCOMPARE(points[4].start, 125.0);
        QCOMPARE(points[4].end, 150.0);
        QCOMPARE(points[4].d[0], 1.5);
        QCOMPARE(points[4].d[1], 5.0);
        QCOMPARE(points[5].start, 125.0);
        QCOMPARE(points[5].end, 150.0);
        QCOMPARE(points[5].d[0], 1.5);
        QCOMPARE(points[5].d[1], 6.0);

        QCOMPARE(points[6].start, 150.0);
        QCOMPARE(points[6].end, 175.0);
        QCOMPARE(points[6].d[0], 1.0);
        QCOMPARE(points[6].d[1], 4.5);
        QCOMPARE(points[7].start, 150.0);
        QCOMPARE(points[7].end, 175.0);
        QCOMPARE(points[7].d[0], 2.0);
        QCOMPARE(points[7].d[1], 4.5);
        QCOMPARE(points[8].start, 150.0);
        QCOMPARE(points[8].end, 175.0);
        QCOMPARE(points[8].d[0], 3.0);
        QCOMPARE(points[8].d[1], 4.5);
    }
};

QTEST_APPLESS_MAIN(TestTraceSet)

#include "traceset.moc"
