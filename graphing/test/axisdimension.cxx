/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtAlgorithms>
#include <QObject>
#include <QString>
#include <QTest>
#include <QWaitCondition>

#include "graphing/axisdimension.hxx"
#include "datacore/stream.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Graphing;


class TestAxisDimension : public QObject {
Q_OBJECT
private slots:

    void basic()
    {
        AxisDimensionSet set;
        Variant::Root v;
        v["Static/Axis1/AlwaysShow"].setBool(true);
        v["Override/0/Match"].setString("Axis2");
        v["Override/0/Settings/Range/Log"].setBool(true);
        v["OverrideUnits/0/Match/#0"].setString(QRegExp::escape("%"));
        v["OverrideUnits/0/Settings/Ticks/TotalLevels"].setInt64(1);
        set.initialize(ValueSegment::Transfer{ValueSegment(FP::undefined(), FP::undefined(), v)});

        set.beginUpdate();
        set.finishUpdate();

        auto disp = set.getDisplayed();
        QCOMPARE((int) disp.size(), 1);
        QVERIFY(!FP::defined(disp.at(0)->getTransformer().getRealMin()));
        auto couple = disp[0]->createRangeCoupling();
        QCOMPARE((int) couple.size(), 0);


        set.beginUpdate();
        set.get(std::unordered_set<QString>{"hPa"}, {}, "Axis1")->registerDataLimits(0.5, 0.6);
        set.finishUpdate();

        disp = set.getDisplayed();
        QCOMPARE((int) disp.size(), 1);
        QCOMPARE((int) disp.at(0)->getTicks().size(), 2);
        QCOMPARE(disp.at(0)->getTransformer().getRealMin(), 0.49);
        QCOMPARE(disp.at(0)->getTransformer().getRealMax(), 0.61);
        QCOMPARE(disp.at(0)->getMin(), 0.49);
        QCOMPARE(disp.at(0)->getMax(), 0.61);
        couple = disp[0]->createRangeCoupling();
        QCOMPARE((int) couple.size(), 1);
        couple[0]->applyCoupling(QList<QVariant>() << QVariant(1.0) << QVariant(2.0));
        QCOMPARE(disp.at(0)->getTransformer().getRealMin(), 0.9);
        QCOMPARE(disp.at(0)->getTransformer().getRealMax(), 2.1);

        auto zoom = set.getZoom();
        QCOMPARE((int) zoom.size(), 1);
        QCOMPARE(zoom.at(0)->getMin(), 0.9);
        QCOMPARE(zoom.at(0)->getMax(), 2.1);
        zoom[0]->setZoom(1.0, 2.0);

        set.beginUpdate();
        set.get(std::unordered_set<QString>{"hPa"}, {}, "Axis1")->registerDataLimits(0.5, 0.6);
        set.finishUpdate();
        disp = set.getDisplayed();
        QCOMPARE((int) disp.size(), 1);
        QCOMPARE(disp.at(0)->getTransformer().getRealMin(), 1.0);
        QCOMPARE(disp.at(0)->getTransformer().getRealMax(), 2.0);
        QCOMPARE(disp.at(0)->getMin(), 1.0);
        QCOMPARE(disp.at(0)->getMax(), 2.0);

        set.beginUpdate();
        set.get(std::unordered_set<QString>{"Mm-1"}, {}, "Axis2")->registerDataLimits(0.9, 1.1);
        set.get(std::unordered_set<QString>{"%"}, {})->registerDataLimits(35.0, 40.0);
        set.finishUpdate();

        disp = set.getDisplayed();
        QCOMPARE((int) disp.size(), 3);
        QVERIFY(disp.at(1)->getTransformer().isLogarithmic());
        QCOMPARE((int) disp.at(2)->getTicks().size(), 1);
    }
};

QTEST_APPLESS_MAIN(TestAxisDimension)

#include "axisdimension.moc"
