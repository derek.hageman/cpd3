/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>
#include <QFont>
#include <QFontMetrics>
#include <QPaintDevice>
#include <QPainter>

#include "core/timeutils.hxx"
#include "core/number.hxx"
#include "graphing/axislabelengine2d.hxx"
#include "guicore/guiformat.hxx"

using namespace CPD3::GUI;

namespace CPD3 {
namespace Graphing {

/** @file graphing/guiformat.hxx
 * Provides utilities to deal with GUI specific formatting.
 */

void AxisLabelEngine2D::setApplicationDefaults()
{
    labelFont = QApplication::font();
    labelFont.setFamily("Courier");
    labelFont.setStyleHint(QFont::TypeWriter);

    secondaryLabelFont = labelFont;
    secondaryLabelFont.setPointSizeF(labelFont.pointSizeF() * 0.75);
    secondaryLabelFont.setWeight(QFont::Light);

    blockLabelFont = labelFont;
    blockLabelFont.setBold(true);
    blockLabelFont.setPointSizeF(labelFont.pointSizeF() * 1.05);

    labelEdgeInset = 0.2;
    secondaryLabelInset = 0.1;
    blockLabelInset = 0.15;
}

/**
 * Construct a label engine using the application font as a base.
 * 
 * @param pos       the position of this axis
 * @param cmps      the components this axis contains
 * @param parent    the object parent
 */
AxisLabelEngine2D::AxisLabelEngine2D(Axis2DSide pos, int cmps, QObject *parent) : QObject(parent),
                                                                                  position(pos),
                                                                                  components(cmps)
{
    setApplicationDefaults();
}

/**
 * Construct a label engine using the application font as a base.
 * 
 * @param pos       the position of this axis
 * @param parent    the object parent
 */
AxisLabelEngine2D::AxisLabelEngine2D(Axis2DSide pos, QObject *parent) : QObject(parent),
                                                                        position(pos),
                                                                        components(
                                                                                DefaultComponents)
{
    setApplicationDefaults();
}

/**
 * Construct a label engine using the given font as the a base.
 * 
 * @param pos       the position of this axis
 * @param baseFont  the base font
 * @param cmps      the components this axis contains
 * @param parent    the object parent
 */
AxisLabelEngine2D::AxisLabelEngine2D(Axis2DSide pos,
                                     const QFont &baseFont,
                                     int cmps,
                                     QObject *parent) : QObject(parent),
                                                        position(pos),
                                                        components(cmps)
{
    labelFont = baseFont;

    secondaryLabelFont = labelFont;
    secondaryLabelFont.setPointSizeF(labelFont.pointSizeF() * 0.75);
    secondaryLabelFont.setWeight(QFont::Light);

    blockLabelFont = labelFont;
    blockLabelFont.setBold(true);
    blockLabelFont.setPointSizeF(labelFont.pointSizeF() * 1.05);

    labelEdgeInset = 0.2;
    secondaryLabelInset = 0.1;
    blockLabelInset = 0.15;
}

/**
 * Construct a label engine using the application font as a base.
 * 
 * @param pos       the position of this axis
 * @param baseFont  the base font
 * @param parent    the object parent
 */
AxisLabelEngine2D::AxisLabelEngine2D(Axis2DSide pos, const QFont &baseFont, QObject *parent)
        : QObject(parent), position(pos), components(DefaultComponents)
{
    labelFont = baseFont;

    secondaryLabelFont = labelFont;
    secondaryLabelFont.setPointSizeF(labelFont.pointSizeF() * 0.75);
    secondaryLabelFont.setWeight(QFont::Light);

    blockLabelFont = labelFont;
    blockLabelFont.setBold(true);
    blockLabelFont.setPointSizeF(labelFont.pointSizeF() * 1.05);

    labelEdgeInset = 0.2;
    secondaryLabelInset = 0.1;
    blockLabelInset = 0.15;
}

AxisLabelEngine2D::AxisLabelEngine2D(const AxisLabelEngine2D &other, QObject *parent) : QObject(
        parent),
                                                                                        position(
                                                                                                other.position),
                                                                                        components(
                                                                                                other.components),
                                                                                        labelFont(
                                                                                                other.labelFont),
                                                                                        secondaryLabelFont(
                                                                                                other.secondaryLabelFont),
                                                                                        blockLabelFont(
                                                                                                other.blockLabelFont),
                                                                                        labelEdgeInset(
                                                                                                other.labelEdgeInset),
                                                                                        secondaryLabelInset(
                                                                                                other.secondaryLabelInset),
                                                                                        blockLabelInset(
                                                                                                other.blockLabelInset)
{ }

AxisLabelEngine2D::~AxisLabelEngine2D()
{
}

/** 
 * Predict the size of the axis.  This only predicts the depth of the axis
 * away from the baseline.  That is, this will return the height for
 * the horizontal axes (top and bottom) and the width for the vertical (left
 * and right) ones.  The ticks do not need to be transformed into real
 * coordinate space.
 * 
 * @param ticks         the ticks that will be displayed
 * @param paintdevice   the paint device that will be used, or NULL for
 *                      screen space
 * @return              the axis depth
 */
qreal AxisLabelEngine2D::predictSize(const std::vector<AxisTickGenerator::Tick> &ticks,
                                     QPaintDevice *paintdevice) const
{
    QFontMetrics fmLabel(labelFont);
    QFontMetrics fmSecondaryLabel(secondaryLabelFont);
    QFontMetrics fmBlockLabel(blockLabelFont);
    if (paintdevice != NULL) {
        fmLabel = QFontMetrics(labelFont, paintdevice);
        fmSecondaryLabel = QFontMetrics(secondaryLabelFont, paintdevice);
        fmBlockLabel = QFontMetrics(blockLabelFont, paintdevice);
    }

    if (position == Axis2DTop || position == Axis2DBottom) {
        qreal maximumLabelHeight = 0.0;
        qreal maximumSecondaryLabelHeight = 0.0;
        qreal maximumBlockLabelHeight = 0.0;
        for (const auto &tick : ticks) {
            if ((components & Labels) && !tick.label.isEmpty()) {
                qreal height =
                        GUIStringLayout::maximumCharacterDimensions(tick.label, fmLabel).height();
                if (height > maximumLabelHeight)
                    maximumLabelHeight = height;
            }
            if ((components & SecondaryLabels) && !tick.secondaryLabel.isEmpty()) {
                qreal height = GUIStringLayout::maximumCharacterDimensions(tick.secondaryLabel,
                                                                           fmSecondaryLabel).height();
                if (height > maximumSecondaryLabelHeight)
                    maximumSecondaryLabelHeight = height;
            }
            if ((components & BlockLabels) && !tick.blockLabel.isEmpty()) {
                qreal height = GUIStringLayout::maximumCharacterDimensions(tick.blockLabel,
                                                                           fmBlockLabel).height();
                if (height > maximumBlockLabelHeight)
                    maximumBlockLabelHeight = height;
            }
        }
        qreal totalHeight = 0.0;
        if (maximumLabelHeight > 0.0) {
            totalHeight += maximumLabelHeight;
            totalHeight += labelEdgeInset * fmLabel.height();
        }
        if (maximumSecondaryLabelHeight > 0.0) {
            totalHeight += maximumSecondaryLabelHeight;
            totalHeight += secondaryLabelInset * fmSecondaryLabel.height();
        }
        if (maximumBlockLabelHeight > 0.0) {
            totalHeight += maximumBlockLabelHeight;
            totalHeight += blockLabelInset * fmBlockLabel.height();
        }
        return totalHeight;
    } else {
        qreal maximumLabelWidth = 0.0;
        qreal maximumSecondaryLabelWidth = 0.0;
        qreal maximumBlockLabelHeight = 0.0;
        for (const auto &tick : ticks) {
            if ((components & Labels) && !tick.label.isEmpty()) {
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
                qreal width = fmLabel.width(tick.label);
#else
                qreal width = fmLabel.horizontalAdvance(tick.label);
#endif
                if (width > maximumLabelWidth)
                    maximumLabelWidth = width;
            }
            if ((components & SecondaryLabels) && !tick.secondaryLabel.isEmpty()) {
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
                qreal width = fmSecondaryLabel.width(tick.secondaryLabel);
#else
                qreal width = fmSecondaryLabel.horizontalAdvance(tick.secondaryLabel);
#endif
                if (width > maximumSecondaryLabelWidth)
                    maximumSecondaryLabelWidth = width;
            }
            if ((components & BlockLabels) && !tick.blockLabel.isEmpty()) {
                qreal height = GUIStringLayout::maximumCharacterDimensions(tick.blockLabel,
                                                                           fmBlockLabel).height();
                if (height > maximumBlockLabelHeight)
                    maximumBlockLabelHeight = height;
            }
        }

        qreal totalWidth = 0.0;

        qreal useLabelInset = 0.0;
        if (maximumLabelWidth > 0.0) {
            useLabelInset = fmLabel.averageCharWidth() * labelEdgeInset;
            maximumLabelWidth += useLabelInset;
            if (maximumLabelWidth > totalWidth)
                totalWidth = maximumLabelWidth;
        }

        if (maximumSecondaryLabelWidth > 0.0) {
            maximumSecondaryLabelWidth +=
                    fmSecondaryLabel.averageCharWidth() * secondaryLabelInset + useLabelInset;
            if (maximumSecondaryLabelWidth > totalWidth)
                totalWidth = maximumSecondaryLabelWidth;
        }

        if (maximumBlockLabelHeight > 0.0) {
            totalWidth += maximumBlockLabelHeight;
            totalWidth += fmBlockLabel.height() * blockLabelInset;
        }
        return totalWidth;
    }
}

/**
 * Paint the axis.  The position of the ticks must be transformed into
 * valid coordinate space for the painter.  The baseline is the outer edge
 * of the axis (e.x. the top of the area for top axes).  Returns the total
 * depth like predictSize(const std::vector<AxisTickGenerator::Tick> &,
 * QPaintDevice *).
 * 
 * @param ticks     the ticks to paint
 * @param painter   the painter to draw with
 * @param baseline  the axis baseline coordinate
 * @param min       the minimum real coordinate to draw on
 * @param max       the maximum real coordinate to draw on
 * @return          the axis depth
 */
double AxisLabelEngine2D::paint(const std::vector<AxisTickGenerator::Tick> &ticks,
                                QPainter *painter,
                                qreal baseline,
                                double min,
                                double max) const
{
    painter->save();

    qreal result = 0.0;

    QFontMetrics fmLabel(labelFont, painter->device());
    QFontMetrics fmSecondaryLabel(secondaryLabelFont, painter->device());
    QFontMetrics fmBlockLabel(blockLabelFont, painter->device());

    if (position == Axis2DTop || position == Axis2DBottom) {
        qreal maximumLabelHeight = 0.0;
        qreal maximumSecondaryLabelHeight = 0.0;
        qreal maximumBlockLabelHeight = 0.0;
        for (const auto &tick : ticks) {
            if ((components & Labels) && !tick.label.isEmpty()) {
                qreal height =
                        GUIStringLayout::maximumCharacterDimensions(tick.label, fmLabel).height();
                if (height > maximumLabelHeight)
                    maximumLabelHeight = height;
            }
            if ((components & SecondaryLabels) && !tick.secondaryLabel.isEmpty()) {
                qreal height = GUIStringLayout::maximumCharacterDimensions(tick.secondaryLabel,
                                                                           fmSecondaryLabel).height();
                if (height > maximumSecondaryLabelHeight)
                    maximumSecondaryLabelHeight = height;
            }
            if ((components & BlockLabels) && !tick.blockLabel.isEmpty()) {
                qreal height = GUIStringLayout::maximumCharacterDimensions(tick.blockLabel,
                                                                           fmBlockLabel).height();
                if (height > maximumBlockLabelHeight)
                    maximumBlockLabelHeight = height;
            }
        }

        qreal startLabel = 0.0;
        if (maximumLabelHeight > 0.0) {
            result += labelEdgeInset * fmLabel.height();
            startLabel = result;
            result += maximumLabelHeight;
        }

        qreal startSecondaryLabel = startLabel;
        if (maximumSecondaryLabelHeight > 0.0) {
            result += secondaryLabelInset * fmSecondaryLabel.height();
            startSecondaryLabel = result;
            result += maximumSecondaryLabelHeight;
        }

        qreal startBlockLabel = startSecondaryLabel;
        if (maximumBlockLabelHeight > 0.0) {
            result += blockLabelInset * fmBlockLabel.height();
            startBlockLabel = result;
            result += maximumBlockLabelHeight;
        }

        QString currentBlockLabel;
        double blockLabelStart = 0.0;
        double blockLabelEnd = 0.0;
        bool blockLabelHadStart = false;
        bool anyBlockInRange = false;

        for (const auto &tick : ticks) {
            bool pointInRange = (!FP::defined(min) || tick.point >= min) &&
                    (!FP::defined(max) || tick.point <= max);

            if (blockLabelHadStart && tick.blockLabel != currentBlockLabel) {
                if (anyBlockInRange || pointInRange) {
                    painter->setFont(blockLabelFont);
                    QRectF textPos;
                    auto center = (blockLabelStart + tick.point) / 2.0;
                    if (position == Axis2DTop) {
                        textPos = GUIStringLayout::alignTextBottom(painter, {center, baseline -
                                                                           startBlockLabel, 0, 0}, Qt::TextDontClip | Qt::AlignHCenter,
                                                                   currentBlockLabel);
                    } else {
                        textPos = GUIStringLayout::alignTextTop(painter,
                                                                {center, baseline + startBlockLabel,
                                                                 0, 0},
                                                                Qt::TextDontClip | Qt::AlignHCenter,
                                                                currentBlockLabel);
                    }

                    if (FP::defined(max) && textPos.right() > max)
                        textPos.moveRight(max);
                    if (FP::defined(min) && textPos.left() < min)
                        textPos.moveLeft(min);
                    painter->drawText(textPos, Qt::TextDontClip, currentBlockLabel);
                }

                blockLabelHadStart = false;
                anyBlockInRange = false;
            }

            if ((components & BlockLabels) && !tick.blockLabel.isEmpty()) {
                if (!blockLabelHadStart) {
                    blockLabelHadStart = true;
                    blockLabelStart = tick.point;
                    currentBlockLabel = tick.blockLabel;
                }
                blockLabelEnd = tick.point;
                if (pointInRange)
                    anyBlockInRange = true;
            }

            if (!pointInRange)
                continue;

            if ((components & Labels) && !tick.label.isEmpty()) {
                painter->setFont(labelFont);
                QRectF textPos;
                if (position == Axis2DTop) {
                    textPos = GUIStringLayout::alignTextBottom(painter,
                                                               {tick.point, baseline - startLabel,
                                                                0, 0},
                                                               Qt::TextDontClip | Qt::AlignHCenter,
                                                               tick.label);
                } else {
                    textPos = GUIStringLayout::alignTextTop(painter,
                                                            {tick.point, baseline + startLabel, 0,
                                                             0},
                                                            Qt::TextDontClip | Qt::AlignHCenter,
                                                            tick.label);
                }

                if (FP::defined(max) && textPos.right() > max)
                    textPos.moveRight(max);
                if (FP::defined(min) && textPos.left() < min)
                    textPos.moveLeft(min);
                painter->drawText(textPos, Qt::TextDontClip, tick.label);
            }

            if ((components & SecondaryLabels) && !tick.secondaryLabel.isEmpty()) {
                painter->setFont(secondaryLabelFont);
                QRectF textPos;
                if (position == Axis2DTop) {
                    textPos = GUIStringLayout::alignTextBottom(painter, {tick.point, baseline -
                                                                       startSecondaryLabel, 0, 0}, Qt::TextDontClip | Qt::AlignHCenter,
                                                               tick.secondaryLabel);
                } else {
                    textPos = GUIStringLayout::alignTextTop(painter, {tick.point, baseline +
                                                                    startSecondaryLabel, 0, 0}, Qt::TextDontClip | Qt::AlignHCenter,
                                                            tick.secondaryLabel);
                }

                if (FP::defined(max) && textPos.right() > max)
                    textPos.moveRight(max);
                if (FP::defined(min) && textPos.left() < min)
                    textPos.moveLeft(min);
                painter->drawText(textPos, Qt::TextDontClip, tick.secondaryLabel);
            }
        }

        if (blockLabelHadStart && anyBlockInRange) {
            painter->setFont(blockLabelFont);
            QRectF textPos;
            auto center = (blockLabelStart + blockLabelEnd) / 2.0;
            if (position == Axis2DTop) {
                textPos = GUIStringLayout::alignTextBottom(painter,
                                                           {center, baseline - startBlockLabel, 0,
                                                            0}, Qt::TextDontClip | Qt::AlignHCenter,
                                                           currentBlockLabel);
            } else {
                textPos = GUIStringLayout::alignTextTop(painter,
                                                        {center, baseline + startBlockLabel, 0, 0},
                                                        Qt::TextDontClip | Qt::AlignHCenter,
                                                        currentBlockLabel);
            }

            if (FP::defined(max) && textPos.right() > max)
                textPos.moveRight(max);
            if (FP::defined(min) && textPos.left() < min)
                textPos.moveLeft(min);
            painter->drawText(textPos, Qt::TextDontClip, currentBlockLabel);
        }
    } else {
        qreal maximumLabelWidth = 0.0;
        qreal maximumSecondaryLabelWidth = 0.0;
        qreal maximumBlockLabelHeight = 0.0;
        for (const auto &tick : ticks) {
            if ((components & Labels) && !tick.label.isEmpty()) {
                painter->setFont(labelFont);
                qreal width = painter->boundingRect(0, 0, 0, 0, 0, tick.label).width();
                if (width > maximumLabelWidth)
                    maximumLabelWidth = width;
            }
            if ((components & SecondaryLabels) && !tick.secondaryLabel.isEmpty()) {
                painter->setFont(secondaryLabelFont);
                qreal width = painter->boundingRect(0, 0, 0, 0, 0, tick.secondaryLabel).width();
                if (width > maximumSecondaryLabelWidth)
                    maximumSecondaryLabelWidth = width;
            }
            if ((components & BlockLabels) && !tick.blockLabel.isEmpty()) {
                painter->setFont(blockLabelFont);
                qreal height = GUIStringLayout::maximumCharacterDimensions(tick.blockLabel,
                                                                           fmBlockLabel).height();
                if (height > maximumBlockLabelHeight)
                    maximumBlockLabelHeight = height;
            }
        }

        qreal useLabelInset = 0.0;
        if (maximumLabelWidth > 0.0) {
            useLabelInset = fmLabel.averageCharWidth() * labelEdgeInset;
            maximumLabelWidth += useLabelInset;
            if (maximumLabelWidth > result)
                result = maximumLabelWidth;
        }

        qreal useSecondaryLabelInset =
                fmSecondaryLabel.averageCharWidth() * secondaryLabelInset + useLabelInset;
        if (maximumSecondaryLabelWidth > 0.0) {
            maximumSecondaryLabelWidth += useSecondaryLabelInset;
            if (maximumSecondaryLabelWidth > result)
                result = maximumSecondaryLabelWidth;
        }

        double startBlockLabel = 0.0;
        if (maximumBlockLabelHeight > 0.0) {
            result += blockLabelInset * fmBlockLabel.height();
            startBlockLabel = result;
            result += maximumBlockLabelHeight;
        }

        QString currentBlockLabel;
        double blockLabelStart = 0.0;
        double blockLabelEnd = 0.0;
        bool blockLabelHadStart = false;
        bool anyBlockInRange = false;

        for (const auto &tick : ticks) {
            bool pointInRange = (!FP::defined(min) || tick.point >= min) &&
                    (!FP::defined(max) || tick.point <= max);
            if (blockLabelHadStart && tick.blockLabel != currentBlockLabel) {
                if (anyBlockInRange || pointInRange) {
                    painter->save();
                    painter->rotate(-90.0);

                    painter->setFont(blockLabelFont);
                    QRectF textPos;
                    auto center = -(blockLabelStart + tick.point) / 2.0;
                    if (position == Axis2DLeft) {
                        textPos = GUIStringLayout::alignTextBottom(painter, {center, baseline -
                                                                           startBlockLabel, 0, 0}, Qt::TextDontClip | Qt::AlignHCenter,
                                                                   currentBlockLabel);
                    } else {
                        textPos = GUIStringLayout::alignTextTop(painter,
                                                                {center, baseline + startBlockLabel,
                                                                 0, 0},
                                                                Qt::TextDontClip | Qt::AlignHCenter,
                                                                currentBlockLabel);
                    }

                    if (FP::defined(min) && textPos.right() > -min)
                        textPos.moveRight(-min);
                    if (FP::defined(max) && textPos.left() < -max)
                        textPos.moveLeft(-max);
                    painter->drawText(textPos, Qt::TextDontClip, currentBlockLabel);
                    painter->restore();
                }

                blockLabelHadStart = false;
                anyBlockInRange = false;
            }

            if ((components & BlockLabels) && !tick.blockLabel.isEmpty()) {
                if (!blockLabelHadStart) {
                    blockLabelHadStart = true;
                    blockLabelStart = tick.point;
                    currentBlockLabel = tick.blockLabel;
                }
                blockLabelEnd = tick.point;
                if (pointInRange)
                    anyBlockInRange = true;
            }

            if (!pointInRange)
                continue;

            if (((components & (Labels | SecondaryLabels)) == (Labels | SecondaryLabels)) &&
                    !tick.label.isEmpty() &&
                    !tick.secondaryLabel.isEmpty()) {
                QRect cDimsL(GUIStringLayout::maximumCharacterDimensions(tick.label, fmLabel));
                QRect cDimsS(GUIStringLayout::maximumCharacterDimensions(tick.secondaryLabel,
                                                                         fmSecondaryLabel));

                QRectF textPosL;
                painter->setFont(labelFont);
                if (position == Axis2DLeft) {
                    textPosL =
                            QRectF(painter->boundingRect(QRectF(baseline - useLabelInset, 0, 0, 0),
                                                         Qt::TextDontClip | Qt::AlignRight,
                                                         tick.label));
                } else {
                    textPosL =
                            QRectF(painter->boundingRect(QRectF(baseline + useLabelInset, 0, 0, 0),
                                                         Qt::TextDontClip | Qt::AlignLeft,
                                                         tick.label));
                }

                QRectF textPosS;
                painter->setFont(secondaryLabelFont);
                if (position == Axis2DLeft) {
                    textPosS = QRectF(painter->boundingRect(
                            QRectF(baseline - useSecondaryLabelInset, 0, 0, 0),
                            Qt::TextDontClip | Qt::AlignRight, tick.secondaryLabel));
                } else {
                    textPosS = QRectF(painter->boundingRect(
                            QRectF(baseline + useSecondaryLabelInset, 0, 0, 0),
                            Qt::TextDontClip | Qt::AlignLeft, tick.secondaryLabel));
                }

                double totalHeight = cDimsS.height() + cDimsL.height() + 1 + fmLabel.leading();

                double effectiveTop = tick.point - totalHeight / 2.0;
                if (FP::defined(max) && effectiveTop + totalHeight > max)
                    effectiveTop = max - totalHeight;
                if (FP::defined(min) && effectiveTop < min)
                    effectiveTop = min;

                textPosL.moveTop(effectiveTop - (fmLabel.ascent() + cDimsL.y()));
                painter->setFont(labelFont);
                painter->drawText(textPosL, Qt::TextDontClip, tick.label);

                textPosS.moveTop(effectiveTop +
                                         totalHeight -
                                         (fmSecondaryLabel.ascent() + cDimsS.y()) -
                                         cDimsS.height());
                painter->setFont(secondaryLabelFont);
                painter->drawText(textPosS, Qt::TextDontClip, tick.secondaryLabel);
            } else if ((components & Labels) && !tick.label.isEmpty()) {
                painter->setFont(labelFont);
                QRect cDims(GUIStringLayout::maximumCharacterDimensions(tick.label, fmLabel));
                QRectF textPos;
                if (position == Axis2DLeft) {
                    textPos =
                            QRectF(painter->boundingRect(QRectF(baseline - useLabelInset, 0, 0, 0),
                                                         Qt::TextDontClip | Qt::AlignRight,
                                                         tick.label));
                } else {
                    textPos =
                            QRectF(painter->boundingRect(QRectF(baseline + useLabelInset, 0, 0, 0),
                                                         Qt::TextDontClip | Qt::AlignLeft,
                                                         tick.label));
                }

                double effectiveTop = tick.point - cDims.height() / 2.0;
                if (FP::defined(max) && effectiveTop + cDims.height() > max)
                    effectiveTop = max - cDims.height();
                if (FP::defined(min) && effectiveTop < min)
                    effectiveTop = min;

                textPos.moveTop(effectiveTop - (fmLabel.ascent() + cDims.y()));
                painter->drawText(textPos, Qt::TextDontClip, tick.label);
            } else if ((components & SecondaryLabels) && !tick.secondaryLabel.isEmpty()) {
                painter->setFont(secondaryLabelFont);
                QRect cDims(GUIStringLayout::maximumCharacterDimensions(tick.secondaryLabel,
                                                                        fmSecondaryLabel));
                QRectF textPos;
                if (position == Axis2DLeft) {
                    textPos = QRectF(painter->boundingRect(
                            QRectF(baseline - useSecondaryLabelInset, 0, 0, 0),
                            Qt::TextDontClip | Qt::AlignRight, tick.secondaryLabel));
                } else {
                    textPos = QRectF(painter->boundingRect(
                            QRectF(baseline + useSecondaryLabelInset, 0, 0, 0),
                            Qt::TextDontClip | Qt::AlignLeft, tick.secondaryLabel));
                }

                double effectiveTop = tick.point - cDims.height() / 2.0;
                if (FP::defined(max) && effectiveTop + cDims.height() > max)
                    effectiveTop = max - cDims.height();
                if (FP::defined(min) && effectiveTop < min)
                    effectiveTop = min;

                textPos.moveTop(effectiveTop - (fmSecondaryLabel.ascent() + cDims.y()));
                painter->drawText(textPos, Qt::TextDontClip, tick.secondaryLabel);
            }
        }

        if (blockLabelHadStart && anyBlockInRange) {
            painter->save();
            painter->rotate(-90.0);

            painter->setFont(blockLabelFont);
            QRectF textPos;
            auto center = -(blockLabelStart + blockLabelEnd) / 2.0;
            if (position == Axis2DLeft) {
                textPos = GUIStringLayout::alignTextBottom(painter,
                                                           {center, baseline - startBlockLabel, 0,
                                                            0}, Qt::TextDontClip | Qt::AlignHCenter,
                                                           currentBlockLabel);
            } else {
                textPos = GUIStringLayout::alignTextTop(painter,
                                                        {center, baseline + startBlockLabel, 0, 0},
                                                        Qt::TextDontClip | Qt::AlignHCenter,
                                                        currentBlockLabel);
            }

            if (FP::defined(min) && textPos.right() > -min)
                textPos.moveRight(-min);
            if (FP::defined(max) && textPos.left() < -max)
                textPos.moveLeft(-max);
            painter->drawText(textPos, Qt::TextDontClip, currentBlockLabel);

            painter->restore();
            blockLabelHadStart = false;
        }
    }

    painter->restore();
    return result;
}

/**
 * Get the font used for labels.
 *
 * @return  the label font
 */
QFont AxisLabelEngine2D::getLabelFont() const
{ return labelFont; }

}
}
