/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>

#include "graphing/indicator.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Graphing {

/** @file graphing/indicators.hxx
 * Provides a class to handle boolean indicators.
 */

IndicatorParameters::IndicatorParameters()
        : TraceParameters(),
          components(0),
          trigger(Trigger_Flag),
          triggerStrings(),
          triggerDouble(FP::undefined()),
          triggerInteger(INTEGER::undefined()),
          invert(false),
          continuity(1.0),
          latestOnly(false)
{ }

IndicatorParameters::~IndicatorParameters() = default;

IndicatorParameters::IndicatorParameters(const IndicatorParameters &other) = default;

IndicatorParameters &IndicatorParameters::operator=(const IndicatorParameters &other) = default;

IndicatorParameters::IndicatorParameters(IndicatorParameters &&other) = default;

IndicatorParameters &IndicatorParameters::operator=(IndicatorParameters &&other) = default;

void IndicatorParameters::copy(const TraceParameters *from)
{ *this = *(static_cast<const IndicatorParameters *>(from)); }

void IndicatorParameters::clear()
{
    TraceParameters::clear();
    components = 0;
}

/**
 * Create indicator parameters from the given configuration.
 * 
 * @param value     the configuration
 */
IndicatorParameters::IndicatorParameters(const Variant::Read &value) : TraceParameters(value),
                                                                       components(0),
                                                                       trigger(Trigger_Flag),
                                                                       triggerStrings(),
                                                                       triggerDouble(
                                                                               FP::undefined()),
                                                                       triggerInteger(
                                                                               INTEGER::undefined()),
                                                                       invert(false),
                                                                       continuity(1.0),
                                                                       latestOnly(false)
{
    if (value["Trigger"].exists()) {
        const auto &type = value["Trigger/Type"].toString();
        if (Util::equal_insensitive(type, "delta", "difference")) {
            triggerDouble = Data::Variant::Composite::toNumber(value["Trigger/Threshold"]);
            if (FP::defined(triggerDouble)) {
                trigger = Trigger_Delta;
                components |= Component_Trigger;
            }
        } else if (Util::equal_insensitive(type, "deltaequal", "differenceequal")) {
            triggerDouble = Data::Variant::Composite::toNumber(value["Trigger/Threshold"]);
            if (FP::defined(triggerDouble)) {
                trigger = Trigger_DeltaEqual;
                components |= Component_Trigger;
            }
        } else if (Util::equal_insensitive(type, "changed", "notequal")) {
            trigger = Trigger_Changed;
            components |= Component_Trigger;
        } else if (Util::equal_insensitive(type, "notchanged")) {
            trigger = Trigger_NotChanged;
            components |= Component_Trigger;
        } else if (Util::equal_insensitive(type, "flag", "flags", "hasflag", "hasflags")) {
            triggerStrings = value["Trigger/Flags"].toChildren().keys();
            if (!triggerStrings.empty()) {
                trigger = Trigger_Flag;
                components |= Component_Trigger;
            }
        } else if (Util::equal_insensitive(type, "noflag", "noflags", "lacksflag", "lacksflags")) {
            triggerStrings = value["Trigger/Flags"].toChildren().keys();
            if (!triggerStrings.empty()) {
                trigger = Trigger_NoFlag;
                components |= Component_Trigger;
            }
        } else if (Util::equal_insensitive(type, "allflags", "requireflags")) {
            triggerStrings = value["Trigger/Flags"].toChildren().keys();
            if (!triggerStrings.empty()) {
                trigger = Trigger_HasFlags;
                components |= Component_Trigger;
            }
        } else if (Util::equal_insensitive(type, "missingflags")) {
            triggerStrings = value["Trigger/Flags"].toChildren().keys();
            if (!triggerStrings.empty()) {
                trigger = Trigger_LacksFlags;
                components |= Component_Trigger;
            }
        } else if (Util::equal_insensitive(type, "greater", "greaterthan", "morethan")) {
            triggerDouble = Data::Variant::Composite::toNumber(value["Trigger/Threshold"]);
            if (FP::defined(triggerDouble)) {
                trigger = Trigger_Greater;
                components |= Component_Trigger;
            }
        } else if (Util::equal_insensitive(type, "greaterequal", "greaterthanorequalto",
                                           "atleast")) {
            triggerDouble = Data::Variant::Composite::toNumber(value["Trigger/Threshold"]);
            if (FP::defined(triggerDouble)) {
                trigger = Trigger_GreaterEqual;
                components |= Component_Trigger;
            }
        } else if (Util::equal_insensitive(type, "smaller", "lessthan", "less")) {
            triggerDouble = Data::Variant::Composite::toNumber(value["Trigger/Threshold"]);
            if (FP::defined(triggerDouble)) {
                trigger = Trigger_Less;
                components |= Component_Trigger;
            }
        } else if (Util::equal_insensitive(type, "lessthanoreqaulto", "lessequal")) {
            triggerDouble = Data::Variant::Composite::toNumber(value["Trigger/Threshold"]);
            if (FP::defined(triggerDouble)) {
                trigger = Trigger_LessEqual;
                components |= Component_Trigger;
            }
        } else if (Util::equal_insensitive(type, "exactly", "exact", "equal")) {
            triggerDouble = Data::Variant::Composite::toNumber(value["Trigger/Value"]);
            if (FP::defined(triggerDouble)) {
                trigger = Trigger_Exactly;
                components |= Component_Trigger;
            }
        } else if (Util::equal_insensitive(type, "and", "bitwiseand")) {
            triggerInteger = value["Trigger/Value"].toInt64();
            if (INTEGER::defined(triggerInteger)) {
                trigger = Trigger_AND;
                components |= Component_Trigger;
            }
        } else if (Util::equal_insensitive(type, "xor", "bitwisexor")) {
            triggerInteger = value["Trigger/Value"].toInt64();
            if (INTEGER::defined(triggerInteger)) {
                trigger = Trigger_XOR;
                components |= Component_Trigger;
            }
        } else if (Util::equal_insensitive(type, "invalid", "empty")) {
            trigger = Trigger_Invalid;
            components |= Component_Trigger;
        } else if (Util::equal_insensitive(type, "undefined", "nan", "undef")) {
            trigger = Trigger_Undefined;
            components |= Component_Trigger;
        }
    }

    if (value["Invert"].exists()) {
        components |= Component_Invert;
        invert = value["Invert"].toBool();
    }

    if (value["Continuity"].exists()) {
        double check = value["Continuity"].toDouble();
        if (FP::defined(check) && check >= 0.0) {
            continuity = check;
            components |= Component_Continuity;
        }
    }

    if (value["LatestOnly"].exists()) {
        components |= Component_LatestOnly;
        latestOnly = value["LatestOnly"].toBool();
    }
}

void IndicatorParameters::save(Variant::Write &value) const
{
    TraceParameters::save(value);

    if (components & Component_Trigger) {
        switch (trigger) {
        case Trigger_Delta:
            value["Trigger/Type"].setString("Delta");
            value["Trigger/Threshold"].setDouble(triggerDouble);
            break;
        case Trigger_DeltaEqual:
            value["Trigger/Type"].setString("DeltaEqual");
            value["Trigger/Threshold"].setDouble(triggerDouble);
            break;
        case Trigger_Changed:
            value["Trigger/Type"].setString("Changed");
            break;
        case Trigger_NotChanged:
            value["Trigger/Type"].setString("NotChanged");
            break;
        case Trigger_Flag:
            value["Trigger/Type"].setString("Flag");
            value["Trigger/Flags"].setFlags(triggerStrings);
            break;
        case Trigger_NoFlag:
            value["Trigger/Type"].setString("NoFlag");
            value["Trigger/Flags"].setFlags(triggerStrings);
            break;
        case Trigger_HasFlags:
            value["Trigger/Type"].setString("RequireFlags");
            value["Trigger/Flags"].setFlags(triggerStrings);
            break;
        case Trigger_LacksFlags:
            value["Trigger/Type"].setString("MissingFlags");
            value["Trigger/Flags"].setFlags(triggerStrings);
            break;
        case Trigger_Greater:
            value["Trigger/Type"].setString("Greater");
            value["Trigger/Threshold"].setDouble(triggerDouble);
            break;
        case Trigger_GreaterEqual:
            value["Trigger/Type"].setString("GreaterEqual");
            value["Trigger/Threshold"].setDouble(triggerDouble);
            break;
        case Trigger_Less:
            value["Trigger/Type"].setString("Less");
            value["Trigger/Threshold"].setDouble(triggerDouble);
            break;
        case Trigger_LessEqual:
            value["Trigger/Type"].setString("GreaterEqual");
            value["Trigger/Threshold"].setDouble(triggerDouble);
            break;
        case Trigger_Exactly:
            value["Trigger/Type"].setString("Exactly");
            value["Trigger/Value"].setDouble(triggerDouble);
            break;
        case Trigger_AND:
            value["Trigger/Type"].setString("AND");
            value["Trigger/Value"].setInt64(triggerInteger);
            break;
        case Trigger_XOR:
            value["Trigger/Type"].setString("XOR");
            value["Trigger/Value"].setInt64(triggerInteger);
            break;
        case Trigger_Invalid:
            value["Trigger/Type"].setString("Invalid");
            break;
        case Trigger_Undefined:
            value["Trigger/Type"].setString("Undefined");
            break;
        }
    }

    if (components & Component_Invert) {
        value["Invert"].setBool(invert);
    }

    if (components & Component_Continuity) {
        value["Continuity"].setDouble(continuity);
    }

    if (components & Component_LatestOnly) {
        value["LatestOnly"].setBool(latestOnly);
    }
}

std::unique_ptr<TraceParameters> IndicatorParameters::clone() const
{ return std::unique_ptr<TraceParameters>(new IndicatorParameters(*this)); }

void IndicatorParameters::overlay(const TraceParameters *over)
{
    TraceParameters::overlay(over);

    const IndicatorParameters *p = static_cast<const IndicatorParameters *>(over);

    if (p->components & Component_Trigger) {
        components |= Component_Trigger;
        trigger = p->trigger;
        triggerStrings = p->triggerStrings;
        triggerDouble = p->triggerDouble;
        triggerInteger = p->triggerInteger;
    }

    if (p->components & Component_Invert) {
        components |= Component_Invert;
        invert = p->invert;
    }

    if (p->components & Component_Continuity) {
        components |= Component_Continuity;
        continuity = p->continuity;
    }

    if (p->components & Component_LatestOnly) {
        components |= Component_LatestOnly;
        latestOnly = p->latestOnly;
    }
}

}
}
