/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGTRACECOMMON_H
#define CPD3GRAPHINGTRACECOMMON_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QColor>
#include <QSizeF>
#include <QPainterPath>
#include <QMenu>

#include "graphing/graphing.hxx"
#include "graphing/display.hxx"
#include "datacore/stream.hxx"
#include "algorithms/model.hxx"
#include "graphing/traceset.hxx"

namespace CPD3 {
namespace Graphing {

/**
 * Provides some general purpose trace utility functions.
 */
class CPD3GRAPHING_EXPORT TraceUtilities {
public:
    static std::deque<QColor> defaultTraceColors(std::size_t needColors,
                                                 const std::deque<QColor> &claimed = {});

    struct CPD3GRAPHING_EXPORT ColorRequest {
        Data::SequenceName unit;
        double wavelength;

        inline ColorRequest() : unit(), wavelength(FP::undefined())
        { }
    };

    static std::deque<QColor> defaultTraceColors(const std::vector<ColorRequest> &requested,
                                                 const std::deque<QColor> &claimed = {});
};

/**
 * Defines a gradient used for a trace colorization.
 */
class CPD3GRAPHING_EXPORT TraceGradient {
    Algorithms::Model *model;
    QColor::Spec spec;

    void setBasicRainbowGradient();

public:
    TraceGradient();

    ~TraceGradient();

    TraceGradient(const TraceGradient &other);

    TraceGradient &operator=(const TraceGradient &other);

    TraceGradient(const QColor &baseColor);

    TraceGradient(const Data::Variant::Read &configuration);

    QColor evaluate(double value);

    QColor evaluate(double value) const;
};

class CPD3GRAPHING_EXPORT TraceSymbol {
public:
    /** 
     * The available types for drawing, ordered in the auto-selection
     * priority order.
     */
    enum Type {
        /** A simple circle with a dot in the center. */
        Circle = 0, /** A simple square with a dot in the center. */
        Square, /** A simple diamond with a dot in the center. */
        Diamond, /** A simple triangle pointing upwards with a dot in the center. */
        TriangleUp, /** A simple triangle pointing downwards with a dot in the center. */
        TriangleDown,

        /** A circle with lines crossing to form an "X". */
        CircleCrossX, /** A square with lines crossing to form an "X". */
        SquareCrossX, /** A diamond with lines crossing to form an "+". */
        DiamondCrossPlus, /** A triangle pointing up with lines from each vertex crossing. */
        TriangleUpCross, /** A triangle pointing down with lines from each vertex crossing. */
        TriangleDownCross,

        /** A filled circle. */
        CircleFilled, /** A filled circle. */
        SquareFilled, /** A filled diamond */
        DiamondFilled, /** A triangle pointing up, filled. */
        TriangleUpFilled, /** A triangle pointing down, filled. */
        TriangleDownFilled,

        /** A simple "X" shape with no outline. */
        SimpleX, /** A simple "+" shape with no outline. */
        SimplePlus,

        /** A circle with lines crossing to form a "+". */
        CircleCrossPlus, /** A diamond with lines crossing to form a "X". */
        DiamondCrossX, /** A square with lines crossing to form a "+". */
        SquareCrossPlus,

        /** A simple triangle pointing left with a dot in the center. */
        TriangleLeft, /** A simple triangle pointing right with a dot in the center. */
        TriangleRight, /** A triangle pointing left with lines from each vertex crossing. */
        TriangleLeftCross, /** A triangle pointing right with lines from each vertex crossing. */
        TriangleRightCross, /** A triangle pointing left, filled. */
        TriangleLeftFilled, /** A triangle pointing right, filled. */
        TriangleRightFilled,

        /** Type currently unselected, will not draw anything. */
        Unselected,
    };

    /* C++ < 14 does not provide specialization for enums */
    struct TypeHash {
        inline std::size_t operator()(Type t) const
        { return std::hash<std::size_t>()(static_cast<std::size_t>(t)); }
    };

    TraceSymbol();

    TraceSymbol(const TraceSymbol &other);

    TraceSymbol &operator=(const TraceSymbol &other);

    TraceSymbol(TraceSymbol &&other);

    TraceSymbol &operator=(TraceSymbol &&other);

    TraceSymbol(const Data::Variant::Read &configuration);

    Data::Variant::Root toConfiguration() const;

    TraceSymbol(Type t, double size = 1.0);

    Type getType() const;

    void setType(Type t);

    void selectTypeIfNeeded(std::unordered_set<Type, TypeHash> &usedTypes);

    QSizeF predictSize(QPaintDevice *paintdevice = NULL) const;

    void paint(QPainter *painter);

    void paint(QPainter *painter, qreal x, qreal y);

    void paint(QPainter *painter) const;

    void paint(QPainter *painter, qreal x, qreal y) const;

    QMenu *customizeMenu(const std::function<void(const TraceSymbol &)> &update,
                         QWidget *parent) const;

private:
    Type type;
    qreal size;
    qreal lastDimension;
    QPainterPath path;

    void paint(QPainter *painter, const QPainterPath &paintPath) const;

    QPainterPath buildPath(qreal dimension) const;
};

}
}

#endif
