/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>
#include <QMenu>
#include <QColorDialog>
#include <QFontDialog>
#include <QInputDialog>

#include "graphing/axisdimension.hxx"
#include "graphing/axis2d.hxx"
#include "guicore/guiformat.hxx"

using namespace CPD3::Data;
using namespace CPD3::GUI;

namespace CPD3 {
namespace Graphing {

/** @file graphing/axis2d.hxx
 * The implementation of the axes for a two dimensional graph.
 */

static const qreal defaultMajorTickSize = 0.3;
static const qreal defaultMinorTickSize = 0.2;
static const qreal tickToLabelSpace = 0.1;
static const qreal titleBeforeSpace = 0.5;
static const qreal sideAxisBetweenSpace = 1.0;
static const qreal colorAxisFirstSpace = 1.0;
static const qreal colorAxisBetweenSpace = 1.0;
static const qreal colorAxisBarSize = 2.0;

static const int defaultMajorGridAlpha = 127;
static const int defaultMinorGridAlpha = 80;

AxisParameters2D::TickLevel::TickLevel() : components(0),
                                           lineEnable(true),
                                           lineSize(defaultMajorTickSize),
                                           lineWidth(1.0),
                                           lineStyle(Qt::SolidLine),
                                           lineColor(0, 0, 0),
                                           lineDrawPriority(Graph2DDrawPrimitive::Priority_Axes),
                                           labelEnable(true),
                                           labelFont(),
                                           labelColor(0, 0, 0),
                                           labelDrawPriority(Graph2DDrawPrimitive::Priority_Axes)
{
    labelFont.setFamily("Courier");
    labelFont.setStyleHint(QFont::TypeWriter);
}

AxisParameters2D::TickLevel::TickLevel(const TickLevel &other) = default;

AxisParameters2D::TickLevel &AxisParameters2D::TickLevel::operator=(const TickLevel &other) = default;


AxisParameters2D::TickLevel::TickLevel(TickLevel &&other) = default;

AxisParameters2D::TickLevel &AxisParameters2D::TickLevel::operator=(TickLevel &&other) = default;

AxisParameters2D::TickLevel::TickLevel(const AxisParameters2D &parent, std::size_t level)
        : components(0),
          lineEnable(true),
          lineSize(level == 0 ? defaultMajorTickSize : defaultMinorTickSize),
          lineWidth(parent.getBaselineWidth()),
          lineStyle(parent.getBaselineStyle()),
          lineColor(parent.getBaselineColor()),
          lineDrawPriority(parent.getDrawPriority()),
          labelEnable(level == 0),
          labelFont(parent.getTitleFont()),
          labelColor(parent.getTitleColor()),
          labelDrawPriority(parent.getDrawPriority())
{
    labelFont.setItalic(false);
    labelFont.setFamily("Courier");
    labelFont.setStyleHint(QFont::TypeWriter);
}

AxisParameters2D::TickLevel::TickLevel(const AxisParameters2D &parent,
                                       const Variant::Read &value,
                                       std::size_t level) : components(0),
                                                            lineEnable(true),
                                                            lineSize(level == 0
                                                                     ? defaultMajorTickSize
                                                                     : defaultMinorTickSize),
                                                            lineWidth(parent.getBaselineWidth()),
                                                            lineStyle(parent.getBaselineStyle()),
                                                            lineColor(parent.getBaselineColor()),
                                                            lineDrawPriority(
                                                                    parent.getDrawPriority()),
                                                            labelEnable(level == 0),
                                                            labelFont(parent.getTitleFont()),
                                                            labelColor(parent.getTitleColor()),
                                                            labelDrawPriority(
                                                                    parent.getDrawPriority())
{
    labelFont.setItalic(false);
    labelFont.setFamily("Courier");
    labelFont.setStyleHint(QFont::TypeWriter);

    if (value["Mark/Enable"].exists()) {
        components |= Component_LineEnable;
        lineEnable = value["Mark/Enable"].toBool();
    }

    if (FP::defined(value["Mark/Size"].toDouble())) {
        components |= Component_LineSize;
        lineSize = value["Mark/Size"].toDouble();
    }

    if (FP::defined(value["Mark/LineWidth"].toDouble())) {
        components |= Component_LineWidth;
        lineSize = value["Mark/LineWidth"].toDouble();
    }

    if (value["Mark/Style"].exists()) {
        lineStyle = Display::parsePenStyle(value["Mark/Style"], lineStyle);
        components |= Component_LineStyle;
    }

    if (value["Mark/Color"].exists()) {
        lineColor = Display::parseColor(value["Mark/Color"], lineColor);
        components |= Component_LineColor;
    }

    if (INTEGER::defined(value["Mark/DrawPriority"].toInt64())) {
        lineDrawPriority = value["Mark/DrawPriority"].toInt64();
        components |= Component_LineDrawPriority;
    }

    if (value["Label/Enable"].exists()) {
        components |= Component_LabelEnable;
        labelEnable = value["Label/Enable"].toBool();
    }

    if (value["Label/Font"].exists()) {
        labelFont = Display::parseFont(value["Label/Font"], labelFont);
        components |= Component_LabelFont;
    }

    if (value["Label/Color"].exists()) {
        labelColor = Display::parseColor(value["Label/Color"], labelColor);
        components |= Component_LabelColor;
    }

    if (INTEGER::defined(value["Label/DrawPriority"].toInt64())) {
        labelDrawPriority = value["Label/DrawPriority"].toInt64();
        components |= Component_LabelDrawPriority;
    }
}

void AxisParameters2D::TickLevel::save(Variant::Write &value) const
{
    if (components & Component_LineEnable) {
        value["Mark/Enable"].setBool(lineEnable);
    }

    if (components & Component_LineSize) {
        value["Mark/Size"].setDouble(lineSize);
    }

    if (components & Component_LineWidth) {
        value["Mark/LineWidth"].setDouble(lineWidth);
    }

    if (components & Component_LineStyle) {
        value["Mark/Style"].set(Display::formatPenStyle(lineStyle));
    }

    if (components & Component_LineColor) {
        value["Mark/Color"].set(Display::formatColor(lineColor));
    }

    if (components & Component_LineDrawPriority) {
        value["Mark/DrawPriority"].setInt64(lineDrawPriority);
    }

    if (components & Component_LabelEnable) {
        value["Label/Enable"].setBool(labelEnable);
    }

    if (components & Component_LabelFont) {
        value["Label/Font"].set(Display::formatFont(labelFont));
    }

    if (components & Component_LabelColor) {
        value["Label/Color"].set(Display::formatColor(labelColor));
    }

    if (components & Component_LabelDrawPriority) {
        value["Label/DrawPriority"].setInt64(labelDrawPriority);
    }
}

void AxisParameters2D::TickLevel::clear()
{ components = 0; }

void AxisParameters2D::TickLevel::overlay(const TickLevel &over)
{
    if (over.components & Component_LineEnable) {
        components |= Component_LineEnable;
        lineEnable = over.lineEnable;
    }

    if (over.components & Component_LineSize) {
        components |= Component_LineSize;
        lineSize = over.lineSize;
    }

    if (over.components & Component_LineWidth) {
        components |= Component_LineWidth;
        lineWidth = over.lineWidth;
    }

    if (over.components & Component_LineStyle) {
        components |= Component_LineStyle;
        lineStyle = over.lineStyle;
    }

    if (over.components & Component_LineColor) {
        components |= Component_LineColor;
        lineColor = over.lineColor;
    }

    if (over.components & Component_LineDrawPriority) {
        components |= Component_LineDrawPriority;
        lineDrawPriority = over.lineDrawPriority;
    }

    if (over.components & Component_LabelEnable) {
        components |= Component_LabelEnable;
        labelEnable = over.labelEnable;
    }

    if (over.components & Component_LabelFont) {
        components |= Component_LabelFont;
        labelFont = over.labelFont;
    }

    if (over.components & Component_LabelColor) {
        components |= Component_LabelColor;
        labelColor = over.labelColor;
    }

    if (over.components & Component_LabelDrawPriority) {
        components |= Component_LabelDrawPriority;
        labelDrawPriority = over.labelDrawPriority;
    }
}

AxisParameters2D::AxisParameters2D()
        : AxisParameters(),
          components(0),
          titleText(),
          titleFont(),
          titleColor(0, 0, 0),
          baselineWidth(1.0),
          baselineStyle(Qt::SolidLine),
          baselineColor(0, 0, 0),
          drawPriority(Graph2DDrawPrimitive::Priority_Axes),
          drawEnable(true),
          toolTipUnits(),
          ticks()
{
    titleFont.setItalic(true);
    for (std::size_t i = 0; i < getTickLevels(); i++) {
        ticks.emplace_back(*this, i);
    }
}

AxisParameters2D::~AxisParameters2D() = default;

AxisParameters2D::AxisParameters2D(const AxisParameters2D &other) = default;

AxisParameters2D &AxisParameters2D::operator=(const AxisParameters2D &other) = default;

AxisParameters2D::AxisParameters2D(AxisParameters2D &&other) = default;

AxisParameters2D &AxisParameters2D::operator=(AxisParameters2D &&other) = default;

AxisParameters2D::AxisParameters2D(const Variant::Read &value, QSettings *settings)
        : AxisParameters(value, settings),
          components(0),
          titleText(),
          titleFont(),
          titleColor(0, 0, 0),
          baselineWidth(1.0),
          baselineStyle(Qt::SolidLine),
          baselineColor(0, 0, 0),
          drawPriority(Graph2DDrawPrimitive::Priority_Axes),
          drawEnable(true),
          toolTipUnits(),
          ticks()
{
    titleFont.setItalic(true);

    if (value["Title/Text"].exists()) {
        titleText = value["Title/Text"].toDisplayString();
        components |= Component_TitleText;
    }

    if (value["Title/Font"].exists()) {
        titleFont = Display::parseFont(value["Title/Font"], titleFont);
        components |= Component_TitleFont;
    }

    if (value["Title/Color"].exists()) {
        titleColor = Display::parseColor(value["Title/Color"], titleColor);
        components |= Component_TitleColor;
    }

    if (FP::defined(value["Baseline/Width"].toDouble())) {
        baselineWidth = value["Baseline/Width"].toDouble();
        components |= Component_BaselineWidth;
    }

    if (value["Baseline/Style"].exists()) {
        baselineStyle = Display::parsePenStyle(value["Baseline/Style"], baselineStyle);
        components |= Component_BaselineStyle;
    }

    if (value["Baseline/Color"].exists()) {
        baselineColor = Display::parseColor(value["Baseline/Color"], baselineColor);
        components |= Component_BaselineColor;
    }

    if (INTEGER::defined(value["DrawPriority"].toInt64())) {
        drawPriority = value["DrawPriority"].toInt64();
        components |= Component_DrawPriority;
    }

    if (value["Draw"].exists()) {
        drawEnable = value["Draw"].toBool();
        components |= Component_DrawEnable;
    }

    if (value["ToolTipUnits"].exists()) {
        toolTipUnits = value["ToolTipUnits"].toDisplayString();
        components |= Component_ToolTipUnits;
    }

    for (std::size_t i = 0; i < getTickLevels(); i++) {
        ticks.emplace_back(*this, value["Ticks/Levels"].array(i), i);
    }
}

void AxisParameters2D::save(Variant::Write &value) const
{
    AxisParameters::save(value);

    if (components & Component_TitleText) {
        value["Title/Text"].setString(titleText);
    }

    if (components & Component_TitleFont) {
        value["Title/Font"].set(Display::formatFont(titleFont));
    }

    if (components & Component_TitleColor) {
        value["Title/Color"].set(Display::formatColor(titleColor));
    }

    if (components & Component_BaselineWidth) {
        value["Baseline/Width"].setDouble(baselineWidth);
    }

    if (components & Component_BaselineStyle) {
        value["Baseline/Style"].set(Display::formatPenStyle(baselineStyle));
    }

    if (components & Component_BaselineColor) {
        value["Baseline/Color"].set(Display::formatColor(baselineColor));
    }

    if (components & Component_DrawPriority) {
        value["DrawPriority"].setInt64(drawPriority);
    }

    if (components & Component_DrawEnable) {
        value["Draw"].setBool(drawEnable);
    }

    if (components & Component_ToolTipUnits) {
        value["ToolTipUnits"].setString(toolTipUnits);
    }


    for (std::size_t i = 0, max = ticks.size(); i < max; i++) {
        auto target = value["Ticks/Levels"].array(i);
        ticks[i].save(target);
    }

}

void AxisParameters2D::copy(const AxisParameters *from)
{ *this = *(static_cast<const AxisParameters2D *>(from)); }

void AxisParameters2D::clear()
{
    AxisParameters::clear();
    components = 0;
    for (auto &t : ticks) {
        t.clear();
    }
}

std::unique_ptr<AxisParameters> AxisParameters2D::clone() const
{ return std::unique_ptr<AxisParameters>(new AxisParameters2D(*this)); }

void AxisParameters2D::overlay(const AxisParameters *over)
{
    AxisParameters::overlay(over);

    auto top = static_cast<const AxisParameters2D *>(over);

    if (top->components & Component_TitleText) {
        components |= Component_TitleText;
        titleText = top->titleText;
    }

    if (top->components & Component_TitleFont) {
        components |= Component_TitleFont;
        titleFont = top->titleFont;
    }

    if (top->components & Component_TitleColor) {
        components |= Component_TitleColor;
        titleColor = top->titleColor;
    }

    if (top->components & Component_BaselineWidth) {
        components |= Component_BaselineWidth;
        baselineWidth = top->baselineWidth;
    }

    if (top->components & Component_BaselineStyle) {
        components |= Component_BaselineStyle;
        baselineStyle = top->baselineStyle;
    }

    if (top->components & Component_BaselineColor) {
        components |= Component_BaselineColor;
        baselineColor = top->baselineColor;
    }

    if (top->components & Component_DrawPriority) {
        components |= Component_DrawPriority;
        drawPriority = top->drawPriority;
    }

    if (top->components & Component_DrawEnable) {
        components |= Component_DrawEnable;
        drawEnable = top->drawEnable;
    }

    if (top->components & Component_ToolTipUnits) {
        components |= Component_ToolTipUnits;
        toolTipUnits = top->toolTipUnits;
    }

    auto original = std::move(ticks);
    ticks.clear();
    for (std::size_t i = 0; i < getTickLevels(); i++) {
        ticks.emplace_back(*this, i);
        if (i < original.size())
            ticks.back().overlay(original[i]);
        if (i < top->ticks.size())
            ticks.back().overlay(top->ticks[i]);
    }
}


AxisParameters2DSide::FixedLine::FixedLine() : position(0.0),
                                               lineWidth(1.0),
                                               lineStyle(Qt::SolidLine),
                                               color(0, 0, 0),
                                               drawPriority(
                                                       Graph2DDrawPrimitive::Priority_AxisFixedLine)
{ }

AxisParameters2DSide::FixedLine::FixedLine(const FixedLine &other) = default;

AxisParameters2DSide::FixedLine &AxisParameters2DSide::FixedLine::operator=(const FixedLine &other) = default;

AxisParameters2DSide::FixedLine::FixedLine(FixedLine &&other) = default;

AxisParameters2DSide::FixedLine &AxisParameters2DSide::FixedLine::operator=(FixedLine &&other) = default;

AxisParameters2DSide::FixedLine::FixedLine(const Variant::Read &value) : position(0.0),
                                                                         lineWidth(1.0),
                                                                         lineStyle(Qt::SolidLine),
                                                                         color(0, 0, 0),
                                                                         drawPriority(
                                                                                 Graph2DDrawPrimitive::Priority_AxisFixedLine)
{
    if (FP::defined(value["Position"].toDouble())) {
        position = value["Position"].toDouble();
    }
    if (FP::defined(value["Width"].toDouble())) {
        lineWidth = value["Width"].toDouble();
    }
    if (value["Style"].exists()) {
        lineStyle = Display::parsePenStyle(value["Style"], lineStyle);
    }
    if (value["Color"].exists()) {
        color = Display::parseColor(value["Color"], color);
    }
    if (INTEGER::defined(value["DrawPriority"].toInt64())) {
        drawPriority = value["DrawPriority"].toInt64();
    }
}

void AxisParameters2DSide::FixedLine::save(Variant::Write &value) const
{
    value["Position"].setDouble(position);
    value["Width"].setDouble(lineWidth);
    value["Style"].set(Display::formatPenStyle(lineStyle));
    value["Color"].set(Display::formatColor(color));
    value["DrawPriority"].setInt64(drawPriority);
}

AxisParameters2DSide::GridLevel::GridLevel() : components(0),
                                               enable(true),
                                               lineWidth(1.0),
                                               lineStyle(Qt::SolidLine),
                                               color(0, 0, 0, defaultMajorGridAlpha),
                                               drawPriority(Graph2DDrawPrimitive::Priority_Grid)
{ }

AxisParameters2DSide::GridLevel::GridLevel(const GridLevel &other) = default;

AxisParameters2DSide::GridLevel &AxisParameters2DSide::GridLevel::operator=(const GridLevel &other) = default;

AxisParameters2DSide::GridLevel::GridLevel(GridLevel &&other) = default;

AxisParameters2DSide::GridLevel &AxisParameters2DSide::GridLevel::operator=(GridLevel &&other) = default;

AxisParameters2DSide::GridLevel::GridLevel(const AxisParameters2DSide &, std::size_t level)
        : components(0),
          enable(true),
          lineWidth(1.0),
          lineStyle(level == 0 ? Qt::SolidLine : Qt::DotLine),
          color(0, 0, 0, level == 0 ? defaultMajorGridAlpha : defaultMinorGridAlpha),
          drawPriority(Graph2DDrawPrimitive::Priority_Grid)
{ }

AxisParameters2DSide::GridLevel::GridLevel(const AxisParameters2DSide &,
                                           const Variant::Read &value,
                                           std::size_t level) : components(0),
                                                                enable(true),
                                                                lineWidth(1.0),
                                                                lineStyle(level == 0 ? Qt::SolidLine
                                                                                     : Qt::DotLine),
                                                                color(0, 0, 0, level == 0
                                                                               ? defaultMajorGridAlpha
                                                                               : defaultMinorGridAlpha),
                                                                drawPriority(
                                                                        Graph2DDrawPrimitive::Priority_Grid)
{
    if (value["Enable"].exists()) {
        enable = value["Enable"].toBool();
        components |= Component_Enable;
    }
    if (FP::defined(value["Width"].toDouble())) {
        lineWidth = value["Width"].toDouble();
        components |= Component_LineWidth;
    }
    if (value["Style"].exists()) {
        lineStyle = Display::parsePenStyle(value["Style"], lineStyle);
        components |= Component_LineStyle;
    }
    if (value["Color"].exists()) {
        color = Display::parseColor(value["Color"], color);
        components |= Component_Color;
    }
    if (INTEGER::defined(value["DrawPriority"].toInt64())) {
        drawPriority = value["DrawPriority"].toInt64();
        components |= Component_DrawPriority;
    }
}

void AxisParameters2DSide::GridLevel::save(Variant::Write &value) const
{
    if (components & Component_Enable) {
        value["Enable"].setBool(enable);
    }

    if (components & Component_LineWidth) {
        value["Width"].setDouble(lineWidth);
    }

    if (components & Component_LineStyle) {
        value["Style"].set(Display::formatPenStyle(lineStyle));
    }

    if (components & Component_Color) {
        value["Color"].set(Display::formatColor(color));
    }

    if (components & Component_DrawPriority) {
        value["DrawPriority"].setInt64(drawPriority);
    }
}

void AxisParameters2DSide::GridLevel::clear()
{ components = 0; }

void AxisParameters2DSide::GridLevel::overlay(const GridLevel &over)
{
    if (over.components & Component_Enable) {
        components |= Component_Enable;
        enable = over.enable;
    }

    if (over.components & Component_LineWidth) {
        components |= Component_LineWidth;
        lineWidth = over.lineWidth;
    }

    if (over.components & Component_LineStyle) {
        components |= Component_LineStyle;
        lineStyle = over.lineStyle;
    }

    if (over.components & Component_Color) {
        components |= Component_Color;
        color = over.color;
    }

    if (over.components & Component_DrawPriority) {
        components |= Component_DrawPriority;
        drawPriority = over.drawPriority;
    }
}

AxisParameters2DSide::AxisParameters2DSide()
        : AxisParameters2D(),
          components(0),
          gridEnable(false),
          sideBinding(Side_Auto),
          grid(),
          fixedLines()
{
    for (std::size_t i = 0; i < getTickLevels(); i++) {
        grid.emplace_back(*this, i);
    }
    if (!getTransformer().isLogarithmic())
        fixedLines.emplace_back();
}

AxisParameters2DSide::~AxisParameters2DSide()
{ }

AxisParameters2DSide::AxisParameters2DSide(const AxisParameters2DSide &other) = default;

AxisParameters2DSide &AxisParameters2DSide::operator=(const AxisParameters2DSide &other) = default;

AxisParameters2DSide::AxisParameters2DSide(AxisParameters2DSide &&other) = default;

AxisParameters2DSide &AxisParameters2DSide::operator=(AxisParameters2DSide &&other) = default;

AxisParameters2DSide::AxisParameters2DSide(const Variant::Read &value, QSettings *settings)
        : AxisParameters2D(value, settings),
          components(0),
          gridEnable(false),
          sideBinding(Side_Auto),
          grid(),
          fixedLines()
{
    if (value["GridEnable"].exists()) {
        gridEnable = value["GridEnable"].toBool();
        components |= Component_GridEnable;
    }

    if (value["Side"].exists()) {
        const auto &side = value["Side"].toString();
        if (Util::equal_insensitive(side, "auto", "automatic")) {
            sideBinding = Side_Auto;
            components |= Component_SideBinding;
        } else if (Util::equal_insensitive(side, "primary", "left", "bottom")) {
            sideBinding = Side_Primary;
            components |= Component_SideBinding;
        } else if (Util::equal_insensitive(side, "secondary", "right", "top")) {
            sideBinding = Side_Secondary;
            components |= Component_SideBinding;
        }
    }

    for (std::size_t i = 0; i < getTickLevels(); i++) {
        grid.emplace_back(*this, value["Grid"].array(i), i);
    }

    if (value["Fixed"].exists()) {
        components |= Component_FixedLines;
        for (auto add : value["Fixed"].toArray()) {
            fixedLines.emplace_back(add);
        }
    } else {
        if (!getTransformer().isLogarithmic())
            fixedLines.emplace_back();
    }
}

void AxisParameters2DSide::save(Variant::Write &value) const
{
    AxisParameters2D::save(value);

    if (components & Component_GridEnable) {
        value["GridEnable"].setBool(gridEnable);
    }

    if (components & Component_SideBinding) {
        switch (sideBinding) {
        case Side_Auto:
            value["Side"].setString("Automatic");
            break;
        case Side_Primary:
            value["Side"].setString("Primary");
            break;
        case Side_Secondary:
            value["Side"].setString("Secondary");
            break;
        }
    }

    if (components & Component_FixedLines) {
        for (std::size_t i = 0, max = fixedLines.size(); i < max; i++) {
            auto target = value["Fixed"].array(i);
            fixedLines[i].save(target);
        }
    }

    for (std::size_t i = 0, max = grid.size(); i < max; i++) {
        auto target = value["Grid"].array(i);
        grid[i].save(target);
    }
}

void AxisParameters2DSide::copy(const AxisParameters *from)
{ *this = *(static_cast<const AxisParameters2DSide *>(from)); }

void AxisParameters2DSide::clear()
{
    AxisParameters2D::clear();
    components = 0;
    for (auto &g : grid) {
        g.clear();
    }
}

std::unique_ptr<AxisParameters> AxisParameters2DSide::clone() const
{ return std::unique_ptr<AxisParameters>(new AxisParameters2DSide(*this)); }

void AxisParameters2DSide::overlay(const AxisParameters *over)
{
    AxisParameters2D::overlay(over);

    auto top = static_cast<const AxisParameters2DSide *>(over);

    if (top->components & Component_GridEnable) {
        components |= Component_GridEnable;
        gridEnable = top->gridEnable;
    }

    if (top->components & Component_SideBinding) {
        components |= Component_SideBinding;
        sideBinding = top->sideBinding;
    }

    if (top->components & Component_FixedLines) {
        components |= Component_FixedLines;
        fixedLines = top->fixedLines;
    }

    auto original = std::move(grid);
    grid.clear();
    for (std::size_t i = 0; i < getTickLevels(); i++) {
        grid.emplace_back(*this, i);
        if (i < original.size())
            grid.back().overlay(original[i]);
        if (i < top->grid.size())
            grid.back().overlay(top->grid[i]);
    }
}


AxisParameters2DColor::AxisParameters2DColor()
        : AxisParameters2D(),
          components(0),
          sideBinding(Side_Auto),
          gradient(),
          gradientConfiguration(),
          barScale(1.0)
{ }

AxisParameters2DColor::~AxisParameters2DColor() = default;

AxisParameters2DColor::AxisParameters2DColor(const AxisParameters2DColor &other) = default;

AxisParameters2DColor &AxisParameters2DColor::operator=(const AxisParameters2DColor &other) = default;

AxisParameters2DColor::AxisParameters2DColor(AxisParameters2DColor &&other) = default;

AxisParameters2DColor &AxisParameters2DColor::operator=(AxisParameters2DColor &&other) = default;

AxisParameters2DColor::AxisParameters2DColor(const Variant::Read &value, QSettings *settings)
        : AxisParameters2D(value, settings),
          components(0),
          sideBinding(Side_Auto),
          gradient(value["Gradient"]),
          gradientConfiguration(),
          barScale(1.0)
{
    if (!hasTransformer()) {
        AxisTransformer tr;
        tr.setMinExtendAbsolute(0);
        tr.setMaxExtendAbsolute(0);
        forceAxisTransformer(tr);
    }

    if (value["Side"].exists()) {
        const auto &side = value["Side"].toString();
        if (Util::equal_insensitive(side, "auto", "automatic")) {
            sideBinding = Side_Auto;
            components |= Component_SideBinding;
        } else if (Util::equal_insensitive(side, "top")) {
            sideBinding = Side_Top;
            components |= Component_SideBinding;
        } else if (Util::equal_insensitive(side, "bottom")) {
            sideBinding = Side_Bottom;
            components |= Component_SideBinding;
        } else if (Util::equal_insensitive(side, "left")) {
            sideBinding = Side_Left;
            components |= Component_SideBinding;
        } else if (Util::equal_insensitive(side, "right")) {
            sideBinding = Side_Right;
            components |= Component_SideBinding;
        }
    }

    if (value["Gradient"].exists()) {
        components |= Component_Gradient;
        gradientConfiguration = Variant::Root(value["Gradient"]);
    }

    if (FP::defined(value["Scale"].toDouble())) {
        barScale = value["Scale"].toDouble();
    }
}

void AxisParameters2DColor::save(Variant::Write &value) const
{
    AxisParameters2D::save(value);

    if (components & Component_SideBinding) {
        switch (sideBinding) {
        case Side_Top:
            value["Side"].setString("Top");
            break;
        case Side_Bottom:
            value["Side"].setString("Bottom");
            break;
        case Side_Left:
            value["Side"].setString("Left");
            break;
        case Side_Right:
            value["Side"].setString("Right");
            break;
        case Side_Auto:
            value["Side"].setString("Automatic");
            break;
        }
    }

    if (components & Component_Gradient) {
        value["Gradient"].set(gradientConfiguration);
    }

    if (components & Component_BarScale) {
        value["Scale"].setDouble(barScale);
    }
}

void AxisParameters2DColor::copy(const AxisParameters *from)
{ *this = *(static_cast<const AxisParameters2DColor *>(from)); }

void AxisParameters2DColor::clear()
{
    AxisParameters2D::clear();
    components = 0;
}

std::unique_ptr<AxisParameters> AxisParameters2DColor::clone() const
{ return std::unique_ptr<AxisParameters>(new AxisParameters2DColor(*this)); }

void AxisParameters2DColor::overlay(const AxisParameters *over)
{
    AxisParameters2D::overlay(over);

    auto top = static_cast<const AxisParameters2DColor *>(over);

    if (top->components & Component_SideBinding) {
        components |= Component_SideBinding;
        sideBinding = top->sideBinding;
    }

    if (top->components & Component_Gradient) {
        components |= Component_Gradient;
        gradient = top->gradient;
    }

    if (top->components & Component_BarScale) {
        components |= Component_BarScale;
        barScale = top->barScale;
    }
}


AxisDimension2D::AxisDimension2D(std::unique_ptr<AxisParameters2D> &&params) : AxisDimension(
        std::move(params)),
                                                                               parameters(
                                                                                       static_cast<AxisParameters2D *>(getParameters())),
                                                                               labelEngines(),
                                                                               side(Axis2DLeft),
                                                                               tickPrimary(true),
                                                                               ticksInside(true),
                                                                               inset(0),
                                                                               labelOffset(0),
                                                                               titleOffset(0),
                                                                               predictedDepth(0),
                                                                               title(),
                                                                               rawTicks(),
                                                                               tickHeights(),
                                                                               previousDataMin(
                                                                                       FP::undefined()),
                                                                               previousDataMax(
                                                                                       FP::undefined())
{ }

AxisDimension2D::~AxisDimension2D() = default;

AxisDimension2D::AxisDimension2D(const AxisDimension2D &other,
                                 std::unique_ptr<AxisParameters> &&params) : AxisDimension(other,
                                                                                           std::move(
                                                                                                   params)),
                                                                             parameters(
                                                                                     static_cast<AxisParameters2D *>(getParameters())),
                                                                             labelEngines(),
                                                                             side(other.side),
                                                                             tickPrimary(
                                                                                     other.tickPrimary),
                                                                             ticksInside(
                                                                                     other.ticksInside),
                                                                             inset(other.inset),
                                                                             labelOffset(
                                                                                     other.labelOffset),
                                                                             titleOffset(
                                                                                     other.titleOffset),
                                                                             predictedDepth(
                                                                                     other.predictedDepth),
                                                                             title(other.title),
                                                                             rawTicks(
                                                                                     other.rawTicks),
                                                                             tickHeights(
                                                                                     other.tickHeights),
                                                                             previousDataMin(
                                                                                     FP::undefined()),
                                                                             previousDataMax(
                                                                                     FP::undefined())
{
    setBinding(side, tickPrimary);
}

std::unique_ptr<AxisDimension> AxisDimension2D::clone() const
{
    return std::unique_ptr<AxisDimension>(new AxisDimension2D(*this, parameters->clone()));
}

/**
 * Set the side binding.
 * @param side          the side
 * @param tickPrimary   the tick primary state (e.x. the leftmost left axis)
 */
void AxisDimension2D::setBinding(Axis2DSide side, bool tickPrimary)
{
    if (!labelEngines.empty() && this->side == side && this->tickPrimary == tickPrimary)
        return;
    this->side = side;
    this->tickPrimary = tickPrimary;

    labelEngines.clear();
    for (std::size_t i = 0; i < parameters->getTickLevels(); i++) {
        if (!parameters->getTickLabelEnable(i)) {
            labelEngines.emplace_back();
            continue;
        }
        labelEngines.emplace_back(new AxisLabelEngine2D(side, parameters->getTickLabelFont(i),
                                                        tickPrimary
                                                        ? AxisLabelEngine2D::AllComponents
                                                        : AxisLabelEngine2D::Labels));
    }
}

/**
 * Set the tick primary state.
 * @param p     the primary state (e.x. the leftmost left axis)
 */
void AxisDimension2D::setTickPrimary(bool p)
{
    setBinding(side, p);
}

/**
 * Prepare for drawing.  This must be called after all data is registered
 * and all couplings applied.
 * 
 * @param paintdevice   the paint device or NULL for the default
 * @param context       the display context
 * @return              true if an external source caused an update
 */
bool AxisDimension2D::prepareDraw(QPaintDevice *paintdevice, const DisplayDynamicContext &context)
{
    bool changed = false;

    /* Catch data bindings changed. */
    if (!FP::equal(previousDataMin, getMin())) {
        previousDataMin = getMin();
        changed = true;
    }
    if (!FP::equal(previousDataMax, getMax())) {
        previousDataMax = getMax();
        changed = true;
    }

    if (parameters->hasTitleText() || !parameters->getTitleText().isEmpty()) {
        title = parameters->getTitleText();
        context.handleString(title, DisplayDynamicContext::String_GraphAxesTitle, &changed);
    } else {
        QStringList sortedUnits;
        Util::append(getUnits(), sortedUnits);
        std::sort(sortedUnits.begin(), sortedUnits.end());
        title = sortedUnits.join(", ");
    }
    rawTicks = getTicks();

    QFontMetrics fm(QApplication::font());
    if (paintdevice)
        fm = QFontMetrics(QApplication::font(), paintdevice);
    qreal baseSize = fm.height();

    qreal tickDepth = 0;
    qreal labelDepth = 0;
    for (std::size_t level = 0; level < rawTicks.size(); level++) {
        if (rawTicks[level].empty())
            continue;
        qreal tickInwards = parameters->getTickLineSize(level);
        if (!ticksInside)
            tickInwards = -tickInwards;
        if (tickInwards < 0.0) {
            tickInwards *= -baseSize;
            if (tickInwards > tickDepth)
                tickDepth = tickInwards;
        }

        if (level < labelEngines.size() && labelEngines[level]) {
            labelDepth = std::max<qreal>(labelDepth,
                                         labelEngines[level]->predictSize(rawTicks[level],
                                                                          paintdevice));
        }
    }

    qreal titleDepth = 0;
    if (!title.isEmpty()) {
        QFontMetrics fmTitle(parameters->getTitleFont());
        if (paintdevice)
            fmTitle = QFontMetrics(parameters->getTitleFont(), paintdevice);
        titleDepth = GUIStringLayout::maximumCharacterDimensions(title, fmTitle).height();
    }

    qreal totalDepth = tickDepth;
    if (labelDepth > 0.0) {
        totalDepth += tickToLabelSpace * baseSize;
        labelOffset = totalDepth;
        totalDepth += labelDepth;
    } else {
        labelOffset = totalDepth;
    }

    if (titleDepth > 0.0) {
        totalDepth += titleBeforeSpace * baseSize;
        titleOffset = totalDepth;
        totalDepth += titleDepth;
    } else {
        titleOffset = totalDepth;
    }

    predictedDepth = totalDepth;

    return changed;
}


namespace {

class Axis2DPainterTicks : public Graph2DDrawPrimitive {
    std::vector<AxisTickGenerator::Tick> ticks;
    AxisTransformer transformer;
    qreal baseline;
    Axis2DSide side;
    qreal lineSize;
    qreal lineWidth;
    Qt::PenStyle lineStyle;
    QColor lineColor;
    quintptr axisPtr;
public:
    Axis2DPainterTicks(std::vector<AxisTickGenerator::Tick> ticks,
                       AxisTransformer transformer,
                       qreal setBaseline,
                       Axis2DSide setSide,
                       qreal setLineSize,
                       qreal setLineWidth,
                       Qt::PenStyle setLineStyle,
                       const QColor &setLineColor,
                       quintptr setAxisPtr,
                       int priority = Graph2DDrawPrimitive::Priority_Default)
            : Graph2DDrawPrimitive(priority),
              ticks(std::move(ticks)),
              transformer(std::move(transformer)),
              baseline(setBaseline),
              side(setSide),
              lineSize(setLineSize),
              lineWidth(setLineWidth),
              lineStyle(setLineStyle),
              lineColor(setLineColor),
              axisPtr(setAxisPtr)
    { }

    virtual ~Axis2DPainterTicks() = default;

    virtual void paint(QPainter *painter)
    {
        painter->save();

        QFontMetrics fm(QApplication::font(), painter->device());
        qreal tickSize = lineSize * fm.height();

        QBrush brush(lineColor);
        painter->setBrush(brush);
        painter->setPen(QPen(brush, lineWidth, lineStyle));

        double screenMin = transformer.getScreenMin();
        double screenMax = transformer.getScreenMax();
        if (screenMin > screenMax) {
            using std::swap;
            swap(screenMin, screenMax);
        }
        for (const auto &tick : ticks) {
            double pos = transformer.toScreen(tick.point, false);
            if (!FP::defined(pos))
                continue;
            if (pos <= screenMin - 1.0)
                continue;
            if (pos >= screenMax + 1.0)
                continue;
            if (pos <= screenMin + 0.5) {
                if (tickSize <= 0.0)
                    continue;
                pos = screenMin;
            } else if (pos >= screenMax - 0.5) {
                if (tickSize <= 0.0)
                    continue;
                pos = screenMax;
            }
            pos = qRound(pos);

            switch (side) {
            case Axis2DTop:
                painter->drawLine(QPointF(pos, baseline), QPointF(pos, baseline - tickSize));
                break;
            case Axis2DBottom:
                painter->drawLine(QPointF(pos, baseline), QPointF(pos, baseline + tickSize));
                break;
            case Axis2DLeft:
                painter->drawLine(QPointF(baseline, pos), QPointF(baseline - tickSize, pos));
                break;
            case Axis2DRight:
                painter->drawLine(QPointF(baseline, pos), QPointF(baseline + tickSize, pos));
                break;
            }
        }

        painter->restore();
    }

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output)
    {
        double screenMin = transformer.getScreenMin();
        double screenMax = transformer.getScreenMax();
        if (screenMin > screenMax)
            qSwap(screenMin, screenMax);

        QFontMetrics fm(QApplication::font());
        qreal tickSize = lineSize * fm.height();

        switch (side) {
        case Axis2DTop:
            if (point.y() > baseline || point.y() < baseline - tickSize)
                return;
            if (point.x() < screenMin || point.x() > screenMax)
                return;
            break;

        case Axis2DBottom:
            if (point.y() < baseline || point.y() > baseline + tickSize)
                return;
            if (point.x() < screenMin || point.x() > screenMax)
                return;
            break;

        case Axis2DLeft:
            if (point.x() > baseline || point.x() < baseline - tickSize)
                return;
            if (point.y() < screenMin || point.y() > screenMax)
                return;
            break;

        case Axis2DRight:
            if (point.x() < baseline || point.x() > baseline + tickSize)
                return;
            if (point.y() < screenMin || point.y() > screenMax)
                return;
            break;
        }

        output = std::shared_ptr<Graph2DMouseoverComponent>(new Graph2DMouseoverAxis(axisPtr));
    }
};

class Axis2DPainterLabels : public Graph2DDrawPrimitive {
    std::vector<AxisTickGenerator::Tick> ticks;
    AxisTransformer transformer;
    qreal baseline;
    double min;
    double max;
    Axis2DSide side;
    AxisLabelEngine2D *engine;
    QColor labelColor;
    quintptr axisPtr;
    double drawnSize;
public:
    Axis2DPainterLabels(std::vector<AxisTickGenerator::Tick> ticks,
                        AxisTransformer transformer,
                        qreal setBaseline,
                        double setMin,
                        double setMax,
                        Axis2DSide setSide,
                        AxisLabelEngine2D *setEngine,
                        const QColor &setColor,
                        quintptr setAxisPtr,
                        int priority = Graph2DDrawPrimitive::Priority_Default)
            : Graph2DDrawPrimitive(priority),
              ticks(std::move(ticks)),
              transformer(std::move(transformer)),
              baseline(setBaseline),
              min(setMin),
              max(setMax),
              side(setSide),
              engine(setEngine),
              labelColor(setColor),
              axisPtr(setAxisPtr),
              drawnSize(FP::undefined())
    { }

    virtual ~Axis2DPainterLabels()
    { delete engine; }

    virtual void paint(QPainter *painter)
    {
        painter->save();

        QBrush brush(labelColor);
        painter->setBrush(brush);
        painter->setPen(QPen(brush, 0));

        auto transformedTicks = ticks;
        for (auto tick = transformedTicks.begin(); tick != transformedTicks.end();) {
            tick->point = transformer.toScreen(tick->point, false);
            if (!FP::defined(tick->point)) {
                tick = transformedTicks.erase(tick);
                continue;
            }
            ++tick;
        }

        drawnSize = engine->paint(transformedTicks, painter, baseline, min, max);

        painter->restore();
    }

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output)
    {
        if (!FP::defined(drawnSize)) {
            auto transformedTicks = ticks;
            for (auto tick = transformedTicks.begin(); tick != transformedTicks.end();) {
                tick->point = transformer.toScreen(tick->point, false);
                if (!FP::defined(tick->point)) {
                    tick = transformedTicks.erase(tick);
                    continue;
                }
                ++tick;
            }
            drawnSize = engine->predictSize(transformedTicks);
        }

        switch (side) {
        case Axis2DTop:
            if (point.y() > baseline || point.y() < baseline - drawnSize)
                return;
            if (point.x() < min || point.x() > max)
                return;
            break;

        case Axis2DBottom:
            if (point.y() < baseline || point.y() > baseline + drawnSize)
                return;
            if (point.x() < min || point.x() > max)
                return;
            break;

        case Axis2DLeft:
            if (point.x() > baseline || point.x() < baseline - drawnSize)
                return;
            if (point.y() < min || point.y() > max)
                return;
            break;

        case Axis2DRight:
            if (point.x() < baseline || point.x() > baseline + drawnSize)
                return;
            if (point.y() < min || point.y() > max)
                return;
            break;
        }

        output = std::shared_ptr<Graph2DMouseoverComponent>(new Graph2DMouseoverAxis(axisPtr));
    }
};

class Axis2DPainterTitle : public Graph2DDrawPrimitive {
    QString text;
    qreal baseline;
    qreal center;
    Axis2DSide side;
    QFont font;
    QColor color;
    quintptr axisPtr;
    qreal limitMin;
    qreal limitMax;
public:
    Axis2DPainterTitle(const QString &setText,
                       qreal setBaseline,
                       qreal setCenter,
                       const Axis2DSide setSide,
                       const QFont &setFont,
                       const QColor &setColor,
                       quintptr setAxisPtr,
                       qreal setLimitMin,
                       qreal setLimitMax,
                       int priority = Graph2DDrawPrimitive::Priority_Default)
            : Graph2DDrawPrimitive(priority),
              text(setText),
              baseline(setBaseline),
              center(setCenter),
              side(setSide),
              font(setFont),
              color(setColor),
              axisPtr(setAxisPtr),
              limitMin(setLimitMin),
              limitMax(setLimitMax)
    { }

    virtual ~Axis2DPainterTitle() = default;

    virtual void paint(QPainter *painter)
    {
        painter->save();

        painter->setFont(font);
        QBrush brush(color);
        painter->setBrush(brush);
        painter->setPen(QPen(brush, 0));

        if (side == Axis2DLeft || side == Axis2DRight) {
            painter->rotate(-90.0);
            center = -center;
        }

        QRectF textPos;
        switch (side) {
        case Axis2DTop:
        case Axis2DLeft:
            textPos = GUIStringLayout::alignTextBottom(painter, {center, baseline, 0, 0},
                                                       Qt::TextDontClip | Qt::AlignHCenter, text);
            break;

        case Axis2DBottom:
        case Axis2DRight:
            textPos = GUIStringLayout::alignTextTop(painter, {center, baseline, 0, 0},
                                                    Qt::TextDontClip | Qt::AlignHCenter, text);
            break;
        }

        painter->drawText(textPos, Qt::TextDontClip, text);

        painter->restore();
    }

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output)
    {
        QFontMetrics fm(font);
        qreal height = GUIStringLayout::maximumCharacterDimensions(text, fm).height();
        //qreal width = fm.boundingRect(text).width();

        switch (side) {
        case Axis2DTop:
            if (point.y() > baseline || point.y() < baseline - height)
                return;
            if (limitMax > limitMin && (point.x() < limitMin || point.x() > limitMax))
                return;
            break;

        case Axis2DBottom:
            if (point.y() < baseline || point.y() > baseline + height)
                return;
            if (limitMax > limitMin && (point.x() < limitMin || point.x() > limitMax))
                return;
            break;

        case Axis2DLeft:
            if (point.x() > baseline || point.x() < baseline - height)
                return;
            if (limitMax > limitMin && (point.y() < limitMin || point.y() > limitMax))
                return;
            break;

        case Axis2DRight:
            if (point.x() < baseline || point.x() > baseline + height)
                return;
            if (limitMax > limitMin && (point.y() < limitMin || point.y() > limitMax))
                return;
            break;
        }

        output = std::shared_ptr<Graph2DMouseoverComponent>(new Graph2DMouseoverAxis(axisPtr));
    }
};

/* Doesn't actually paint anything, but can catch background clicks */
class Axis2DPainterBackgroundMouse : public Graph2DDrawPrimitive {
    qreal baseline;
    qreal depth;
    qreal min;
    qreal max;
    Axis2DSide side;
    quintptr axisPtr;
public:
    Axis2DPainterBackgroundMouse(qreal setBaseline,
                                 qreal setDepth,
                                 qreal setMin,
                                 qreal setMax,
                                 Axis2DSide setSide,
                                 quintptr setAxisPtr) : baseline(setBaseline),
                                                        depth(setDepth),
                                                        min(setMin),
                                                        max(setMax),
                                                        side(setSide),
                                                        axisPtr(setAxisPtr)
    { }

    virtual ~Axis2DPainterBackgroundMouse() = default;

    virtual bool drawLessThan(const Graph2DDrawPrimitive *other) const
    {
        Q_UNUSED(other);
        return true;
    }

    virtual void paint(QPainter *painter)
    { Q_UNUSED(painter); }

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output)
    {
        switch (side) {
        case Axis2DTop:
            if (point.y() > baseline || point.y() < baseline - depth)
                return;
            if (max <= min || point.x() < min || point.x() > max)
                return;
            break;

        case Axis2DBottom:
            if (point.y() < baseline || point.y() > baseline + depth)
                return;
            if (max <= min || point.x() < min || point.x() > max)
                return;
            break;

        case Axis2DLeft:
            if (point.x() > baseline || point.x() < baseline - depth)
                return;
            if (max <= min || point.y() < min || point.y() > max)
                return;
            break;

        case Axis2DRight:
            if (point.x() < baseline || point.x() > baseline + depth)
                return;
            if (max <= min || point.y() < min || point.y() > max)
                return;
            break;
        }

        output = std::shared_ptr<Graph2DMouseoverComponent>(new Graph2DMouseoverAxis(axisPtr));
    }
};

}


/**
 * Create the draw primitives for the axis.
 * 
 * @param baseline      the baseline of the axis
 * @param limitMin      the minimum coordinate of the baseline
 * @param limitMax      the maximum coordinate of the baseline
 * @param transformer   the transformer to use
 */
std::vector<std::unique_ptr<Graph2DDrawPrimitive>> AxisDimension2D::createDraw(qreal baseline,
                                                                               qreal limitMin,
                                                                               qreal limitMax,
                                                                               const AxisTransformer &transformer) const
{
    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> result;

    qreal furthestBaseline = baseline;

    if (limitMax > limitMin) {
        for (std::size_t level = 0; level < rawTicks.size(); level++) {
            if (rawTicks[level].empty())
                continue;

            if (parameters->getTickLineEnable(level)) {
                result.emplace_back(
                        new Axis2DPainterTicks(rawTicks[level], transformer, baseline, side,
                                               ticksInside ? -parameters->getTickLineSize(level)
                                                           : parameters->getTickLineSize(level),
                                               parameters->getTickLineWidth(level),
                                               parameters->getTickLineStyle(level),
                                               parameters->getTickLineColor(level), (quintptr) this,
                                               parameters->getTickLineDrawPriority(level)));
            }

            if (parameters->getTickLabelEnable(level) &&
                    level < labelEngines.size() &&
                    labelEngines[level]) {
                qreal adjustedBaseline = baseline;
                switch (side) {
                case Axis2DTop:
                case Axis2DLeft:
                    adjustedBaseline -= labelOffset;
                    break;
                case Axis2DBottom:
                case Axis2DRight:
                    adjustedBaseline += labelOffset;
                    break;
                }
                furthestBaseline = adjustedBaseline;

                result.emplace_back(
                        new Axis2DPainterLabels(rawTicks[level], transformer, adjustedBaseline,
                                                limitMin, limitMax, side,
                                                new AxisLabelEngine2D(*labelEngines[level]),
                                                parameters->getTickLabelColor(level),
                                                (quintptr) this,
                                                parameters->getTickLabelDrawPriority(level)));
            }
        }
    }

    if (!title.isEmpty()) {
        qreal adjustedBaseline = baseline;
        switch (side) {
        case Axis2DTop:
        case Axis2DLeft:
            adjustedBaseline -= titleOffset;
            break;
        case Axis2DBottom:
        case Axis2DRight:
            adjustedBaseline += titleOffset;
            break;
        }
        furthestBaseline = adjustedBaseline;

        result.emplace_back(
                new Axis2DPainterTitle(title, adjustedBaseline, (limitMin + limitMax) * 0.5, side,
                                       parameters->getTitleFont(), parameters->getTitleColor(),
                                       (quintptr) this, limitMin, limitMax,
                                       parameters->getDrawPriority()));
    }

    /* Don't have to adjust for the outermost component, since that won't
     * have a gap anyway */
    if (furthestBaseline != baseline) {
        result.emplace_back(
                new Axis2DPainterBackgroundMouse(baseline, qAbs(furthestBaseline - baseline),
                                                 limitMin, limitMax, side, (quintptr) this));
    }

    return std::move(result);
}

/**
 * Create the draw primitives for the axis.
 * 
 * @param baseline      the baseline of the axis
 * @param limitMin      the minimum coordinate of the baseline
 * @param limitMax      the maximum coordinate of the baseline
 */
std::vector<std::unique_ptr<Graph2DDrawPrimitive>> AxisDimension2D::createDraw(qreal baseline,
                                                                               qreal limitMin,
                                                                               qreal limitMax) const
{ return createDraw(baseline, limitMin, limitMax, getTransformer()); }

class AxisDimension2D::ModificationComponent : public DisplayModificationComponent {
    QPointer<AxisDimension2D> axis;
public:
    explicit ModificationComponent(AxisDimension2D *axis) : axis(axis)
    { }

    virtual ~ModificationComponent() = default;

    QString getTitle() const override
    { return tr("Axis"); }

    QList<QAction *> getMenuActions(QWidget *parent) override
    {
        if (axis.isNull())
            return {};

        QList<QAction *> result;
        QAction *act;
        QMenu *menu;

        menu = new QMenu(tr("&Title", "Context|TitleMenu"), this);
        result.append(menu->menuAction());

        act = menu->addAction(tr("&Text", "Context|SetAxisTitle"));
        act->setToolTip(tr("Change the the axis title."));
        act->setStatusTip(tr("Set the axis title"));
        act->setWhatsThis(tr("Change or disable the title displayed on the selected axis."));
        QObject::connect(act, &QAction::triggered, this, [this, parent] {
            if (axis.isNull())
                return;

            bool ok = false;
            auto title =
                    QInputDialog::getText(parent, tr("Set Axis Title", "Context|AxisTitleDialog"),
                                          tr("Title:", "Context|AxisTitleLabel"), QLineEdit::Normal,
                                          axis->title, &ok);
            if (!ok)
                return;
            static_cast<AxisParameters2D *>(axis->overrideParameters())->overrideTitleText(title);
            axis->mergeOverrideParameters();
            emit changed();
        });

        act = menu->addAction(tr("&Color", "Context|SetTitleColor"));
        act->setToolTip(tr("Change the axis title color."));
        act->setStatusTip(tr("Set the axis title color"));
        act->setWhatsThis(tr("Set the color of the title text on the selected axis."));
        QObject::connect(act, &QAction::triggered, this, [this, parent] {
            if (axis.isNull())
                return;

            QColor color = axis->parameters->getTitleColor();
            if (!color.isValid())
                color = QColor(0, 0, 0);
            QColorDialog dialog(color, parent);
            dialog.setWindowModality(Qt::ApplicationModal);
            dialog.setWindowTitle(tr("Set Axis Title Color"));
            dialog.setOption(QColorDialog::ShowAlphaChannel);
            if (dialog.exec() != QDialog::Accepted)
                return;
            static_cast<AxisParameters2D *>(axis->overrideParameters())->overrideTitleColor(
                    dialog.currentColor());
            axis->mergeOverrideParameters();
            emit changed();
        });

        act = menu->addAction(tr("&Font", "Context|SetTitleFont"));
        act->setToolTip(tr("Change the axis title font."));
        act->setStatusTip(tr("Set the axis title font"));
        act->setWhatsThis(tr("Set the font of the title text on the selected axis."));
        QObject::connect(act, &QAction::triggered, this, [this, parent] {
            if (axis.isNull())
                return;

            QFontDialog dialog(axis->parameters->getTitleFont(), parent);
            dialog.setWindowModality(Qt::ApplicationModal);
            dialog.setWindowTitle(tr("Set Axis Title Font"));
            if (dialog.exec() != QDialog::Accepted)
                return;
            static_cast<AxisParameters2D *>(axis->overrideParameters())->overrideTitleFont(
                    dialog.selectedFont());
            axis->mergeOverrideParameters();
            emit changed();
        });


        if (axis->parameters->getTickLevels() > 0) {
            menu = new QMenu(tr("T&icks", "Context|TicksMenu"), this);
            result.append(menu->menuAction());

            act = menu->addAction(tr("&Label", "Context|SetTickEnable"));
            act->setToolTip(tr("Enable tick labels."));
            act->setStatusTip(tr("Show tick labels"));
            act->setWhatsThis(tr("This sets if the major tick labels are displayed."));
            act->setCheckable(true);
            act->setChecked(axis->parameters->getTickLabelEnable(0));
            QObject::connect(act, &QAction::toggled, this, [this](bool enable) {
                if (axis.isNull())
                    return;
                static_cast<AxisParameters2D *>(axis->overrideParameters())->overrideTickLabelEnable(
                        0, enable);
                axis->labelEngines.clear();
                axis->mergeOverrideParameters();
                emit changed();
            });

            act = menu->addAction(tr("&Color", "Context|SetTickColor"));
            act->setToolTip(tr("Change the tick label color"));
            act->setStatusTip(tr("Set the tick label color"));
            act->setWhatsThis(tr("Set the color of the major tick labels on the selected axis."));
            QObject::connect(act, &QAction::triggered, this, [this, parent] {
                if (axis.isNull())
                    return;

                QColor color = axis->parameters->getTitleColor();
                if (axis->parameters->hasTickLabelColor(0))
                    color = axis->parameters->getTickLabelColor(0);
                if (!color.isValid())
                    color = QColor(0, 0, 0);
                QColorDialog dialog(color, parent);
                dialog.setWindowModality(Qt::ApplicationModal);
                dialog.setWindowTitle(tr("Set Axis Tick Label Color"));
                dialog.setOption(QColorDialog::ShowAlphaChannel);
                if (dialog.exec() != QDialog::Accepted)
                    return;
                static_cast<AxisParameters2D *>(axis->overrideParameters())->overrideTickLabelColor(
                        0, dialog.currentColor());
                axis->labelEngines.clear();
                axis->mergeOverrideParameters();
                emit changed();
            });

            act = menu->addAction(tr("&Font", "Context|SetTickFont"));
            act->setToolTip(tr("Change the tick label font."));
            act->setStatusTip(tr("Set the tick label font"));
            act->setWhatsThis(tr("Set the font of the major tick labels on the selected axis."));
            QObject::connect(act, &QAction::triggered, this, [this, parent] {
                if (axis.isNull())
                    return;

                QFontDialog dialog(axis->parameters->getTickLabelFont(0), parent);
                dialog.setWindowModality(Qt::ApplicationModal);
                dialog.setWindowTitle(tr("Set Axis Title Font"));
                if (dialog.exec() != QDialog::Accepted)
                    return;
                static_cast<AxisParameters2D *>(axis->overrideParameters())->overrideTickLabelFont(
                        0, dialog.selectedFont());
                axis->labelEngines.clear();
                axis->mergeOverrideParameters();
                emit changed();
            });
        }


        if (axis->enableBoundsModification()) {
            menu = new QMenu(tr("&Bounds", "Context|Bounds"), this);
            result.append(menu->menuAction());

            act = menu->addAction(tr("&Logarithmic", "Context|SetAxisLogarithmic"));
            act->setToolTip(tr("Set the axis to logarithmic or linear."));
            act->setStatusTip(tr("Logarithmic axis"));
            act->setWhatsThis(
                    tr("This sets if the axis is logarithmic or linear in how it scales values."));
            act->setCheckable(true);
            act->setChecked(axis->parameters->getTransformer().isLogarithmic());
            QObject::connect(act, &QAction::toggled, this, [this](bool enable) {
                if (axis.isNull())
                    return;
                auto tx = axis->parameters->getTransformer();
                tx.setLogarithmic(enable);
                axis->overrideParameters()->overrideTransformer(tx);
                axis->mergeOverrideParameters();
                emit changed();
            });

            act = menu->addAction(tr("&Reverse", "Context|SetAxisReverse"));
            act->setToolTip(tr("Set the axis to revere direction."));
            act->setStatusTip(tr("Reversed axis"));
            act->setWhatsThis(
                    tr("This sets if the axis displays values in a reverse order (e.x. larger values at the bottom)."));
            act->setCheckable(true);
            act->setChecked(axis->parameters->getTransformer().isReversed());
            QObject::connect(act, &QAction::toggled, this, [this](bool enable) {
                if (axis.isNull())
                    return;
                auto tx = axis->parameters->getTransformer();
                tx.setReversed(enable);
                axis->overrideParameters()->overrideTransformer(tx);
                axis->mergeOverrideParameters();
                emit changed();
            });

            act = menu->addAction(tr("Mi&nimum", "Context|SetAxisMinimum"));
            act->setToolTip(tr("Configure the axis minimum bound."));
            act->setStatusTip(tr("Set axis minimum"));
            act->setWhatsThis(tr("Configure how the axis transforms or sets the minimum bound."));
            QObject::connect(act, &QAction::triggered, this, [this, parent] {
                if (axis.isNull())
                    return;

                AxisTransformerBoundDialog dialog(parent);
                dialog.setWindowModality(Qt::ApplicationModal);
                dialog.setWindowTitle(tr("Set Axis Minimum"));
                auto tx = axis->parameters->getTransformer();
                dialog.setFromMinimum(tx);
                if (dialog.exec() != QDialog::Accepted)
                    return;
                dialog.applyToMinimum(tx);
                axis->overrideParameters()->overrideTransformer(tx);
                axis->mergeOverrideParameters();
                emit changed();
            });

            act = menu->addAction(tr("Ma&ximum", "Context|SetAxisMaximum"));
            act->setToolTip(tr("Configure the axis maximum bound."));
            act->setStatusTip(tr("Set axis maximum"));
            act->setWhatsThis(tr("Configure how the axis transforms or sets the maximum bound."));
            QObject::connect(act, &QAction::triggered, this, [this, parent] {
                if (axis.isNull())
                    return;

                AxisTransformerBoundDialog dialog(parent);
                dialog.setWindowModality(Qt::ApplicationModal);
                dialog.setWindowTitle(tr("Set Axis Minimum"));
                auto tx = axis->parameters->getTransformer();
                dialog.setFromMaximum(tx);
                if (dialog.exec() != QDialog::Accepted)
                    return;
                dialog.applyToMaximum(tx);
                axis->overrideParameters()->overrideTransformer(tx);
                axis->mergeOverrideParameters();
                emit changed();
            });
        }

        return result;
    }
};

bool AxisDimension2D::enableBoundsModification() const
{ return true; }

/**
 * Get the modification component for the axis.
 * 
 * @param graph the parent graph
 * @return      the modification component, or NULL for none
 */
DisplayModificationComponent *AxisDimension2D::createModificationComponent(Graph2D *)
{ return new ModificationComponent(this); }

AxisDimension2DSide::AxisDimension2DSide(std::unique_ptr<AxisParameters2DSide> &&params)
        : AxisDimension2D(std::move(params)),
          parameters(static_cast<AxisParameters2DSide *>(getParameters())),
          primary(true)
{ }

AxisDimension2DSide::~AxisDimension2DSide() = default;

AxisDimension2DSide::AxisDimension2DSide(const AxisDimension2DSide &other,
                                         std::unique_ptr<AxisParameters> &&params)
        : AxisDimension2D(other, std::move(params)),
          parameters(static_cast<AxisParameters2DSide *>(getParameters())),
          primary(other.primary)
{ }

std::unique_ptr<AxisDimension> AxisDimension2DSide::clone() const
{
    return std::unique_ptr<AxisDimension>(new AxisDimension2DSide(*this, parameters->clone()));
}

void AxisDimension2DSide::beginUpdate()
{
    AxisDimension2D::beginUpdate();
    if (!parameters->hasTitleColor())
        parameters->setTitleColor(QColor(0, 0, 0));
}

/**
 * Bind a color to the axis when it has a unique color associated with it. 
 * Generally this is when there are multiple traces but only one bound to
 * this axis.
 * 
 * @param color the color to bind
 */
void AxisDimension2DSide::bindUniqueTraceColor(const QColor &color)
{
    if (parameters->hasTitleColor())
        return;
    if (!color.isValid() || color.alpha() == 0)
        return;
    parameters->setTitleColor(color);
}

namespace {

class AxisDimension2DSideCoupling : public Internal::AxisUnitCoupling {
    QPointer<AxisDimension2DSide> parent;
public:
    AxisDimension2DSideCoupling() = default;

    AxisDimension2DSideCoupling(DisplayCoupling::CoupleMode mode, AxisDimension2DSide *setParent)
            : Internal::AxisUnitCoupling(mode, setParent), parent(setParent)
    { }

    virtual ~AxisDimension2DSideCoupling() = default;

    virtual void applyCoupling(const QVariant &data, DisplayCoupling::CouplingPosition position)
    {
        AxisUnitCoupling::applyCoupling(data, position);
        if (parent.isNull())
            return;
        switch (parent->getSide()) {
        case Axis2DLeft:
        case Axis2DTop:
            if (!FP::defined(parent->getZoomMin()) && !FP::defined(parent->getZoomMax())) {
                parent->setTickPrimary(
                        position == DisplayCoupling::First || position == DisplayCoupling::Only);
            } else {
                parent->setTickPrimary(true);
            }
            break;
        case Axis2DRight:
        case Axis2DBottom:
            if (!FP::defined(parent->getZoomMin()) && !FP::defined(parent->getZoomMax())) {
                parent->setTickPrimary(
                        position == DisplayCoupling::Last || position == DisplayCoupling::Only);
            } else {
                parent->setTickPrimary(true);
            }
            break;
        }
    }
};

}

std::vector<std::shared_ptr<DisplayCoupling>> AxisDimension2DSide::createRangeCoupling(
        DisplayCoupling::CoupleMode mode)
{
    std::vector<std::shared_ptr<DisplayCoupling>> result;
    if (getUnits().empty() && getGroupUnits().empty())
        return result;

    result.emplace_back(std::make_shared<AxisDimension2DSideCoupling>(mode, this));
    return result;
}

namespace {

class Axis2DPainterBaseline : public Graph2DDrawPrimitive {
    Axis2DSide side;
    qreal min;
    qreal max;
    qreal baseline;
    qreal priorBaseline;
    qreal baselineWidth;
    Qt::PenStyle baselineStyle;
    QColor baselineColor;
public:
    Axis2DPainterBaseline(Axis2DSide setSide,
                          qreal setMin,
                          qreal setMax,
                          qreal setBaseline,
                          qreal setPriorBaseline,
                          qreal setBaselineWidth,
                          Qt::PenStyle setBaselineStyle,
                          const QColor &setBaselineColor,
                          int priority = Graph2DDrawPrimitive::Priority_Default)
            : Graph2DDrawPrimitive(priority),
              side(setSide),
              min(setMin),
              max(setMax),
              baseline(setBaseline),
              priorBaseline(setPriorBaseline),
              baselineWidth(setBaselineWidth),
              baselineStyle(setBaselineStyle),
              baselineColor(setBaselineColor)
    { }

    virtual ~Axis2DPainterBaseline() = default;

    virtual void paint(QPainter *painter)
    {
        if (baselineWidth < 0.0)
            return;

        QPainterPath path;
        switch (side) {
        case Axis2DTop:
        case Axis2DBottom:
            path.moveTo(min, priorBaseline);
            if (priorBaseline != baseline) {
                path.lineTo(min, baseline);
                path.lineTo(max, baseline);
            }
            path.lineTo(max, priorBaseline);
            break;
        case Axis2DLeft:
        case Axis2DRight:
            path.moveTo(priorBaseline, min);
            if (priorBaseline != baseline) {
                path.lineTo(baseline, min);
                path.lineTo(baseline, max);
            }
            path.lineTo(priorBaseline, max);
            break;
        }

        painter->save();
        QBrush brush(baselineColor);
        painter->setBrush(brush);
        painter->strokePath(path, QPen(brush, baselineWidth, baselineStyle));
        painter->restore();
    }
};

class Axis2DPainterGrid : public Graph2DDrawPrimitive {
    std::vector<AxisTickGenerator::Tick> ticks;
    AxisTransformer transformer;
    qreal baseline;
    qreal farSide;
    Axis2DSide side;
    qreal lineWidth;
    Qt::PenStyle lineStyle;
    QColor lineColor;
public:
    Axis2DPainterGrid(std::vector<AxisTickGenerator::Tick> ticks,
                      AxisTransformer transformer,
                      qreal setBaseline,
                      qreal setFarSide,
                      Axis2DSide setSide,
                      qreal setLineWidth,
                      Qt::PenStyle setLineStyle,
                      const QColor &setLineColor,
                      int priority = Graph2DDrawPrimitive::Priority_Default) : Graph2DDrawPrimitive(
            priority),
                                                                               ticks(std::move(
                                                                                       ticks)),
                                                                               transformer(
                                                                                       std::move(
                                                                                               transformer)),
                                                                               baseline(
                                                                                       setBaseline),
                                                                               farSide(setFarSide),
                                                                               side(setSide),
                                                                               lineWidth(
                                                                                       setLineWidth),
                                                                               lineStyle(
                                                                                       setLineStyle),
                                                                               lineColor(
                                                                                       setLineColor)
    { }

    virtual ~Axis2DPainterGrid() = default;

    virtual void paint(QPainter *painter)
    {
        painter->save();

        QBrush brush(lineColor);
        painter->setBrush(brush);
        painter->setPen(QPen(brush, lineWidth, lineStyle));

        double screenMin = transformer.getScreenMin();
        double screenMax = transformer.getScreenMax();
        if (screenMin > screenMax)
            qSwap(screenMin, screenMax);
        screenMin += 0.5;
        screenMax -= 0.5;
        for (const auto &tick : ticks) {
            double pos = transformer.toScreen(tick.point, false);
            if (!FP::defined(pos))
                continue;
            if (pos <= screenMin)
                continue;
            if (pos >= screenMax)
                continue;

            pos = qRound(pos);

            switch (side) {
            case Axis2DTop:
                painter->drawLine(QPointF(pos, baseline), QPointF(pos, farSide));
                break;
            case Axis2DBottom:
                painter->drawLine(QPointF(pos, baseline), QPointF(pos, farSide));
                break;
            case Axis2DLeft:
                painter->drawLine(QPointF(baseline, pos), QPointF(farSide, pos));
                break;
            case Axis2DRight:
                painter->drawLine(QPointF(baseline, pos), QPointF(farSide, pos));
                break;
            }
        }

        painter->restore();
    }
};

class Axis2DPainterFixed : public Graph2DDrawPrimitive {
    double position;
    AxisTransformer transformer;
    qreal baseline;
    qreal farSide;
    Axis2DSide side;
    qreal lineWidth;
    Qt::PenStyle lineStyle;
    QColor lineColor;
public:
    Axis2DPainterFixed(double setPosition,
                       const AxisTransformer &setTransformer,
                       qreal setBaseline,
                       qreal setFarSide,
                       Axis2DSide setSide,
                       qreal setLineWidth,
                       Qt::PenStyle setLineStyle,
                       const QColor &setLineColor,
                       int priority = Graph2DDrawPrimitive::Priority_Default)
            : Graph2DDrawPrimitive(priority),
              position(setPosition),
              transformer(setTransformer),
              baseline(setBaseline),
              farSide(setFarSide),
              side(setSide),
              lineWidth(setLineWidth),
              lineStyle(setLineStyle),
              lineColor(setLineColor)
    { }

    virtual ~Axis2DPainterFixed() = default;

    virtual void paint(QPainter *painter)
    {
        double screenMin = transformer.getScreenMin();
        double screenMax = transformer.getScreenMax();
        if (screenMin > screenMax)
            qSwap(screenMin, screenMax);
        screenMin += 0.5;
        screenMax -= 0.5;

        double pos = transformer.toScreen(position, false);
        if (!FP::defined(pos))
            return;
        if (pos <= screenMin)
            return;
        if (pos >= screenMax)
            return;
        pos = qRound(pos);

        painter->save();

        QBrush brush(lineColor);
        painter->setBrush(brush);
        painter->setPen(QPen(brush, lineWidth, lineStyle));

        switch (side) {
        case Axis2DTop:
            painter->drawLine(QPointF(pos, baseline), QPointF(pos, farSide));
            break;
        case Axis2DBottom:
            painter->drawLine(QPointF(pos, baseline), QPointF(pos, farSide));
            break;
        case Axis2DLeft:
            painter->drawLine(QPointF(baseline, pos), QPointF(farSide, pos));
            break;
        case Axis2DRight:
            painter->drawLine(QPointF(baseline, pos), QPointF(farSide, pos));
            break;
        }

        painter->restore();
    }
};

}

/**
 * Create the draw primitives for the axis.
 * 
 * @param baseline      the baseline of the axis
 * @param priorBaseline the baseline of the axis "before" this one
 * @param farSide       the far edge of the axes
 * @param limitMin      the minimum coordinate of the baseline
 * @param limitMax      the maximum coordinate of the baseline
 */
std::vector<std::unique_ptr<Graph2DDrawPrimitive>> AxisDimension2DSide::createDraw(qreal baseline,
                                                                                   qreal priorBaseline,
                                                                                   qreal farSide,
                                                                                   qreal limitMin,
                                                                                   qreal limitMax) const
{
    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> result;

    if (limitMax > limitMin) {
        const auto &rawTicks = getRawTicks();
        for (std::size_t level = 0; level < rawTicks.size(); level++) {
            if (rawTicks[level].empty())
                continue;

            if (parameters->getGridLevelEnable(level)) {
                result.emplace_back(
                        new Axis2DPainterGrid(rawTicks[level], getTransformer(), baseline, farSide,
                                              getSide(), parameters->getGridLevelLineWidth(level),
                                              parameters->getGridLevelLineStyle(level),
                                              parameters->getGridLevelColor(level),
                                              parameters->getGridLevelDrawPriority(level)));
            }
        }

        auto fixed = parameters->getFixedLines();
        if (!parameters->hasFixedLines()) {
            if (getGroupUnits().count("Transmittance") || getGroupUnits().count("Albedo")) {
                fixed.clear();
                fixed.emplace_back();
                fixed.back().setPosition(1.0);
            }
        }
        for (const auto &line : fixed) {
            result.emplace_back(
                    new Axis2DPainterFixed(line.getPosition(), getTransformer(), baseline, farSide,
                                           getSide(), line.getLineWidth(), line.getLineStyle(),
                                           line.getColor(), line.getDrawPriority()));
        }
    }
    Util::append(AxisDimension2D::createDraw(baseline, limitMin, limitMax), result);

    result.emplace_back(
            new Axis2DPainterBaseline(getSide(), limitMin, limitMax, baseline, priorBaseline,
                                      parameters->getBaselineWidth(),
                                      parameters->getBaselineStyle(),
                                      parameters->getBaselineColor(),
                                      parameters->getDrawPriority()));

    return std::move(result);
}

class AxisDimension2DSide::ModificationComponent : public AxisDimension2D::ModificationComponent {
    QPointer<AxisDimension2DSide> axis;

    void setPosition(AxisParameters2DSide::SideBindingState side)
    {
        if (axis.isNull())
            return;
        static_cast<AxisParameters2DSide *>(axis->overrideParameters())->overrideSideBinding(side);
        axis->mergeOverrideParameters();
        emit changed();
    }

public:
    explicit ModificationComponent(AxisDimension2DSide *axis)
            : AxisDimension2D::ModificationComponent(axis), axis(axis)
    { }

    virtual ~ModificationComponent() = default;

    QList<QAction *> getMenuActions(QWidget *parent) override
    {
        auto result = AxisDimension2D::ModificationComponent::getMenuActions(parent);
        if (axis.isNull())
            return result;

        QAction *act;
        QActionGroup *group;
        QMenu *menu;

        act = new QAction(tr("Enable &Grid", "Context|AxisEnableGrid"), this);
        act->setToolTip(tr("Enable or disable a grid generated from this axis."));
        act->setStatusTip(tr("Enable grid"));
        act->setWhatsThis(
                tr("This enables or disables grid lines extending from the ticks of this axis."));
        act->setCheckable(true);
        act->setChecked(axis->parameters->getGridEnable());
        QObject::connect(act, &QAction::toggled, this, [this](bool enable) {
            if (axis.isNull())
                return;
            static_cast<AxisParameters2DSide *>(axis->overrideParameters())->overrideGridEnable(
                    enable);
            axis->mergeOverrideParameters();
            emit changed();
        });
        result.append(act);

        menu = new QMenu(tr("&Position", "Context|AxisPosition"), this);
        result.append(menu->menuAction());
        group = new QActionGroup(menu);

        act = menu->addAction(tr("&Automatic", "Context|AxisPositionAuto"));
        act->setToolTip(tr("Automatically select the side the axis is displayed on."));
        act->setStatusTip(tr("Set axis position"));
        act->setWhatsThis(
                tr("Set the axis to automatically select the side of the graph it is displayed on."));
        act->setCheckable(true);
        act->setChecked(axis->parameters->getSideBinding() == AxisParameters2DSide::Side_Auto);
        act->setActionGroup(group);
        QObject::connect(act, &QAction::triggered, this,
                         std::bind(&ModificationComponent::setPosition, this,
                                   AxisParameters2DSide::Side_Auto));

        act = menu->addAction(tr("&Primary", "Context|AxisPositionPrimary"));
        act->setToolTip(tr("Display the axis on the \"primary\" (e.x. left) side of the graph."));
        act->setStatusTip(tr("Set axis position"));
        act->setWhatsThis(
                tr("Set the axis to display on the \"primary\" side of the graph, for example on the left or bottom."));
        act->setCheckable(true);
        act->setChecked(axis->parameters->getSideBinding() == AxisParameters2DSide::Side_Primary);
        act->setActionGroup(group);
        QObject::connect(act, &QAction::triggered, this,
                         std::bind(&ModificationComponent::setPosition, this,
                                   AxisParameters2DSide::Side_Primary));

        act = menu->addAction(tr("&Secondary", "Context|AxisPositionSecondary"));
        act->setToolTip(
                tr("Display the axis on the \"secondary\" (e.x. right) side of the graph."));
        act->setStatusTip(tr("Set axis position"));
        act->setWhatsThis(
                tr("Set the axis to display on the \"secondary\" side of the graph, for example on the right or top."));
        act->setCheckable(true);
        act->setChecked(axis->parameters->getSideBinding() == AxisParameters2DSide::Side_Secondary);
        act->setActionGroup(group);
        QObject::connect(act, &QAction::triggered, this,
                         std::bind(&ModificationComponent::setPosition, this,
                                   AxisParameters2DSide::Side_Secondary));

        return result;
    }
};

DisplayModificationComponent *AxisDimension2DSide::createModificationComponent(Graph2D *)
{ return new ModificationComponent(this); }


AxisDimension2DColor::AxisDimension2DColor(std::unique_ptr<AxisParameters2DColor> &&params)
        : AxisDimension2D(std::move(params)),
          parameters(static_cast<AxisParameters2DColor *>(getParameters())),
          barDepth(0)
{
    setTickPolarity(false);
}

AxisDimension2DColor::~AxisDimension2DColor() = default;

AxisDimension2DColor::AxisDimension2DColor(const AxisDimension2DColor &other,
                                           std::unique_ptr<AxisParameters> &&params)
        : AxisDimension2D(other, std::move(params)),
          parameters(static_cast<AxisParameters2DColor *>(getParameters())),
          barDepth(other.barDepth)
{ }

std::unique_ptr<AxisDimension> AxisDimension2DColor::clone() const
{
    return std::unique_ptr<AxisDimension>(new AxisDimension2DColor(*this, parameters->clone()));
}

qreal AxisDimension2DColor::getPredictedDepth() const
{
    return AxisDimension2D::getPredictedDepth() + barDepth;
}

bool AxisDimension2DColor::prepareDraw(QPaintDevice *paintdevice,
                                       const DisplayDynamicContext &context)
{
    bool changed = AxisDimension2D::prepareDraw(paintdevice, context);

    QFontMetrics fm(QApplication::font());
    if (paintdevice != NULL)
        fm = QFontMetrics(QApplication::font(), paintdevice);
    qreal baseSize = fm.height();

    barDepth = baseSize * colorAxisBarSize * parameters->getBarScale();
    return changed;
}

namespace {

class Axis2DPainterGradientBar : public Graph2DDrawPrimitive {
    TraceGradient gradient;
    Axis2DSide side;
    qreal baselineWidth;
    Qt::PenStyle baselineStyle;
    QColor baselineColor;
    QRectF bar;
    quintptr axisPtr;
public:
    Axis2DPainterGradientBar(const TraceGradient &setGradient,
                             Axis2DSide setSide,
                             qreal setBaselineWidth,
                             Qt::PenStyle setBaselineStyle,
                             const QColor &setBaselineColor,
                             const QRectF &setBar,
                             quintptr setAxisPtr,
                             int priority = Graph2DDrawPrimitive::Priority_Default)
            : Graph2DDrawPrimitive(priority),
              gradient(setGradient),
              side(setSide),
              baselineWidth(setBaselineWidth),
              baselineStyle(setBaselineStyle),
              baselineColor(setBaselineColor),
              bar(setBar),
              axisPtr(setAxisPtr)
    { }

    virtual ~Axis2DPainterGradientBar() = default;

    virtual void paint(QPainter *painter)
    {
        painter->save();

        QRectF paintBar(bar);
        if (baselineWidth >= 0.0 && paintBar.width() >= 2.0 && paintBar.height() >= 2.0) {
            paintBar.setTop(paintBar.top() + 1.0);
            paintBar.setBottom(paintBar.bottom() - 1.0);
            paintBar.setLeft(paintBar.left() + 1.0);
            paintBar.setRight(paintBar.right() - 1.0);
        }

        QLinearGradient paintGradient;
        switch (side) {
        case Axis2DTop:
        case Axis2DBottom:
            for (int x = (int) floor(bar.left()), max = (int) ceil(bar.right()); x <= max; x++) {
                qreal c = (qreal) x / (qreal) max;
                paintGradient.setColorAt(c, gradient.evaluate(c));
            }
            paintGradient.setStart(bar.left(), bar.top());
            paintGradient.setFinalStop(bar.right(), bar.top());
            break;

        case Axis2DLeft:
        case Axis2DRight:
            for (int y = (int) floor(bar.top()), max = (int) ceil(bar.bottom()); y <= max; y++) {
                qreal c = (qreal) y / (qreal) max;
                paintGradient.setColorAt(c, gradient.evaluate(c));
            }
            paintGradient.setStart(bar.left(), bar.bottom());
            paintGradient.setFinalStop(bar.left(), bar.top());
            break;
        }

        painter->setBrush(QBrush(paintGradient));
        if (baselineWidth >= 0.0) {
            painter->setPen(QPen(QBrush(baselineColor), baselineWidth, baselineStyle));
        } else {
            painter->setPen(Qt::NoPen);
        }
        painter->drawRect(paintBar);

        painter->restore();
    }

    virtual void updateMouseClick(const QPointF &point,
                                  std::shared_ptr<Graph2DMouseoverComponent> &output)
    {
        QRectF paintBar(bar);
        if (baselineWidth >= 0.0 && paintBar.width() >= 2.0 && paintBar.height() >= 2.0) {
            paintBar.setTop(paintBar.top() + 1.0);
            paintBar.setBottom(paintBar.bottom() - 1.0);
            paintBar.setLeft(paintBar.left() + 1.0);
            paintBar.setRight(paintBar.right() - 1.0);
        }

        if (!paintBar.contains(point))
            return;
        output = std::shared_ptr<Graph2DMouseoverComponent>(new Graph2DMouseoverAxis(axisPtr));
    }
};

}

/**
 * Create the draw primitives for the axis.
 * 
 * @param baseline      the baseline of the axis
 * @param limitMin      the minimum coordinate of the baseline
 * @param limitMax      the maximum coordinate of the baseline
 */
std::vector<std::unique_ptr<Graph2DDrawPrimitive>> AxisDimension2DColor::createDraw(qreal baseline,
                                                                                    qreal limitMin,
                                                                                    qreal limitMax) const
{
    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> result;

    switch (getSide()) {
    case Axis2DTop: {
        if (limitMax > limitMin && barDepth > 0.0) {
            result.emplace_back(new Axis2DPainterGradientBar(parameters->getGradient(), getSide(),
                                                             parameters->getBaselineWidth(),
                                                             parameters->getBaselineStyle(),
                                                             parameters->getBaselineColor(),
                                                             QRectF(limitMin, baseline - barDepth,
                                                                    limitMax - limitMin, barDepth),
                                                             (quintptr) static_cast<const AxisDimension2D *>(this),
                                                             parameters->getDrawPriority()));
        }

        AxisTransformer screenTransformer(getTransformer());
        if (parameters->getBaselineWidth() >= 0.0 && limitMax - limitMin > 2.0)
            screenTransformer.setScreen(limitMin + 1.0, limitMax - 1.0);
        else
            screenTransformer.setScreen(limitMin, limitMax);
        Util::append(AxisDimension2D::createDraw(baseline - barDepth, limitMin, limitMax,
                                                 screenTransformer), result);
        break;
    }

    case Axis2DBottom: {
        if (limitMax > limitMin && barDepth > 0.0) {
            result.emplace_back(new Axis2DPainterGradientBar(parameters->getGradient(), getSide(),
                                                             parameters->getBaselineWidth(),
                                                             parameters->getBaselineStyle(),
                                                             parameters->getBaselineColor(),
                                                             QRectF(limitMin, baseline,
                                                                    limitMax - limitMin, barDepth),
                                                             (quintptr) static_cast<const AxisDimension2D *>(this),
                                                             parameters->getDrawPriority()));
        }

        AxisTransformer screenTransformer(getTransformer());
        if (parameters->getBaselineWidth() >= 0.0 && limitMax - limitMin > 2.0)
            screenTransformer.setScreen(limitMin + 1.0, limitMax - 1.0);
        else
            screenTransformer.setScreen(limitMin, limitMax);
        Util::append(AxisDimension2D::createDraw(baseline + barDepth, limitMin, limitMax,
                                                 screenTransformer), result);
        break;
    }

    case Axis2DLeft: {
        if (limitMax > limitMin && barDepth > 0.0) {
            result.emplace_back(new Axis2DPainterGradientBar(parameters->getGradient(), getSide(),
                                                             parameters->getBaselineWidth(),
                                                             parameters->getBaselineStyle(),
                                                             parameters->getBaselineColor(),
                                                             QRectF(baseline - barDepth, limitMin,
                                                                    barDepth, limitMax - limitMin),
                                                             (quintptr) static_cast<const AxisDimension2D *>(this),
                                                             parameters->getDrawPriority()));
        }

        AxisTransformer screenTransformer(getTransformer());
        if (parameters->getBaselineWidth() >= 0.0 && limitMax - limitMin > 2.0)
            screenTransformer.setScreen(limitMax - 1.0, limitMin + 1.0);
        else
            screenTransformer.setScreen(limitMax, limitMin);
        Util::append(AxisDimension2D::createDraw(baseline - barDepth, limitMin, limitMax,
                                                 screenTransformer), result);
        break;
    }

    case Axis2DRight: {
        if (limitMax > limitMin && barDepth > 0.0) {
            result.emplace_back(new Axis2DPainterGradientBar(parameters->getGradient(), getSide(),
                                                             parameters->getBaselineWidth(),
                                                             parameters->getBaselineStyle(),
                                                             parameters->getBaselineColor(),
                                                             QRectF(baseline, limitMin, barDepth,
                                                                    limitMax - limitMin),
                                                             (quintptr) static_cast<const AxisDimension2D *>(this),
                                                             parameters->getDrawPriority()));
        }

        AxisTransformer screenTransformer(getTransformer());
        if (parameters->getBaselineWidth() >= 0.0 && limitMax - limitMin > 2.0)
            screenTransformer.setScreen(limitMax - 1.0, limitMin + 1.0);
        else
            screenTransformer.setScreen(limitMax, limitMin);
        Util::append(AxisDimension2D::createDraw(baseline + barDepth, limitMin, limitMax,
                                                 screenTransformer), result);
        break;
    }
    }

    return std::move(result);
}

class AxisDimension2DColor::ModificationComponent : public AxisDimension2D::ModificationComponent {
    QPointer<AxisDimension2DColor> axis;

    void setPosition(AxisParameters2DColor::SideBindingState side)
    {
        if (axis.isNull())
            return;
        static_cast<AxisParameters2DColor *>(axis->overrideParameters())->overrideSideBinding(side);
        axis->mergeOverrideParameters();
        emit changed();
    }

    void setGradient(const std::string &type)
    {
        if (axis.isNull())
            return;
        Data::Variant::Root config;
        config["Type"] = type;
        static_cast<AxisParameters2DColor *>(axis->overrideParameters())->overrideGradientConfig(
                config.read());
        axis->mergeOverrideParameters();
        emit changed();
    }

public:
    explicit ModificationComponent(AxisDimension2DColor *axis)
            : AxisDimension2D::ModificationComponent(axis), axis(axis)
    { }

    virtual ~ModificationComponent() = default;

    QList<QAction *> getMenuActions(QWidget *parent) override
    {
        auto result = AxisDimension2D::ModificationComponent::getMenuActions(parent);
        if (axis.isNull())
            return result;

        QAction *act;
        QActionGroup *group;
        QMenu *menu;

        menu = new QMenu(tr("&Position", "Context|AxisPosition"), this);
        result.append(menu->menuAction());
        group = new QActionGroup(menu);

        act = menu->addAction(tr("&Automatic", "Context|AxisPositionAuto"));
        act->setToolTip(tr("Automatically select the side the axis is displayed on."));
        act->setStatusTip(tr("Set axis position"));
        act->setWhatsThis(
                tr("Set the axis to automatically select the side of the graph it is displayed on."));
        act->setCheckable(true);
        act->setChecked(axis->parameters->getSideBinding() == AxisParameters2DColor::Side_Auto);
        act->setActionGroup(group);
        QObject::connect(act, &QAction::triggered, this,
                         std::bind(&ModificationComponent::setPosition, this,
                                   AxisParameters2DColor::Side_Auto));

        act = menu->addAction(tr("&Left", "Context|AxisPositionLeft"));
        act->setToolTip(tr("Display the axis on the left side of the graph."));
        act->setStatusTip(tr("Set axis position"));
        act->setWhatsThis(tr("Set the axis to display outside the graph on the left side."));
        act->setCheckable(true);
        act->setChecked(axis->parameters->getSideBinding() == AxisParameters2DColor::Side_Left);
        act->setActionGroup(group);
        QObject::connect(act, &QAction::triggered, this,
                         std::bind(&ModificationComponent::setPosition, this,
                                   AxisParameters2DColor::Side_Left));

        act = menu->addAction(tr("&Right", "Context|AxisPositionRight"));
        act->setToolTip(tr("Display the axis on the right side of the graph."));
        act->setStatusTip(tr("Set axis position"));
        act->setWhatsThis(tr("Set the axis to display outside the graph on the right side."));
        act->setCheckable(true);
        act->setChecked(axis->parameters->getSideBinding() == AxisParameters2DColor::Side_Right);
        act->setActionGroup(group);
        QObject::connect(act, &QAction::triggered, this,
                         std::bind(&ModificationComponent::setPosition, this,
                                   AxisParameters2DColor::Side_Right));

        act = menu->addAction(tr("&Top", "Context|AxisPositionTop"));
        act->setToolTip(tr("Display the axis on the top of the graph."));
        act->setStatusTip(tr("Set axis position"));
        act->setWhatsThis(tr("Set the axis to display outside and above the graph."));
        act->setCheckable(true);
        act->setChecked(axis->parameters->getSideBinding() == AxisParameters2DColor::Side_Top);
        act->setActionGroup(group);
        QObject::connect(act, &QAction::triggered, this,
                         std::bind(&ModificationComponent::setPosition, this,
                                   AxisParameters2DColor::Side_Top));

        act = menu->addAction(tr("&Bottom", "Context|AxisPositionBottom"));
        act->setToolTip(tr("Display the axis on the bottom of the graph."));
        act->setStatusTip(tr("Set axis position"));
        act->setWhatsThis(tr("Set the axis to display outside and below the graph."));
        act->setCheckable(true);
        act->setChecked(axis->parameters->getSideBinding() == AxisParameters2DColor::Side_Top);
        act->setActionGroup(group);
        QObject::connect(act, &QAction::triggered, this,
                         std::bind(&ModificationComponent::setPosition, this,
                                   AxisParameters2DColor::Side_Top));

        menu = new QMenu(tr("&Gradient", "Context|AxisColorGradient"), this);
        result.append(menu->menuAction());
        group = new QActionGroup(menu);

        auto type = axis->parameters->getGradientConfig()["Type"].toString();
        if (type.empty())
            type = axis->parameters->getGradientConfig().toString();

        act = menu->addAction(tr("&Sequential", "Context|AxisGradientSequential"));
        act->setToolTip(tr("A gradient suitable for sequential data."));
        act->setStatusTip(tr("Set axis color gradient"));
        act->setWhatsThis(tr("Set the axis to use a gradient suitable for sequential data."));
        act->setCheckable(true);
        act->setChecked(Util::equal_insensitive(type, "sequential", "default"));
        act->setActionGroup(group);
        QObject::connect(act, &QAction::triggered, this,
                         std::bind(&ModificationComponent::setGradient, this,
                                   std::string("Sequential")));

        act = menu->addAction(tr("&Sequential", "Context|AxisGradientDiverging"));
        act->setToolTip(tr("A gradient suitable for diverging data."));
        act->setStatusTip(tr("Set axis color gradient"));
        act->setWhatsThis(tr("Set the axis to use a gradient suitable for diverging data."));
        act->setCheckable(true);
        act->setChecked(Util::equal_insensitive(type, "diverging", "diverge"));
        act->setActionGroup(group);
        QObject::connect(act, &QAction::triggered, this,
                         std::bind(&ModificationComponent::setGradient, this,
                                   std::string("Diverging")));

        act = menu->addAction(tr("&Rainbow", "Context|AxisGradientRainbow"));
        act->setToolTip(tr("A gradient that spans the color spectrum."));
        act->setStatusTip(tr("Set axis color gradient"));
        act->setWhatsThis(tr("Set the axis to use a rainbow spectrum spanning gradient."));
        act->setCheckable(true);
        act->setChecked(Util::equal_insensitive(type, "rainbow"));
        act->setActionGroup(group);
        QObject::connect(act, &QAction::triggered, this,
                         std::bind(&ModificationComponent::setGradient, this,
                                   std::string("Rainbow")));

        act = menu->addAction(tr("&Rainbow", "Context|AxisGradientAlternateRainbow"));
        act->setToolTip(tr("An alternate gradient that spans the color spectrum."));
        act->setStatusTip(tr("Set axis color gradient"));
        act->setWhatsThis(
                tr("Set the axis to use an alternate rainbow spectrum spanning gradient."));
        act->setCheckable(true);
        act->setChecked(Util::equal_insensitive(type, "alternaterainbow", "altrainbow"));
        act->setActionGroup(group);
        QObject::connect(act, &QAction::triggered, this,
                         std::bind(&ModificationComponent::setGradient, this,
                                   std::string("AlternateRainbow")));

        return result;
    }
};

DisplayModificationComponent *AxisDimension2DColor::createModificationComponent(Graph2D *)
{ return new ModificationComponent(this); }

AxisDimensionSet2D::AxisDimensionSet2D() = default;

AxisDimensionSet2D::~AxisDimensionSet2D() = default;

AxisDimensionSet2D::AxisDimensionSet2D(const AxisDimensionSet2D &other) = default;

std::unique_ptr<AxisDimensionSet> AxisDimensionSet2D::clone() const
{ return std::unique_ptr<AxisDimensionSet>(new AxisDimensionSet2D(*this)); }

bool AxisDimensionSet2D::prepareDraw(QPaintDevice *paintdevice,
                                     const DisplayDynamicContext &context)
{
    Q_UNUSED(paintdevice);
    Q_UNUSED(context);
    return false;
}

void AxisDimensionSet2D::setArea(const QRectF &traceArea, const QRectF &inner)
{
    Q_UNUSED(traceArea);
    Q_UNUSED(inner);
}

std::vector<std::unique_ptr<Graph2DDrawPrimitive>> AxisDimensionSet2D::createDraw() const
{ return {}; }

/**
 * Create a new axis side set.
 * 
 * @param setVertical   if set then these are vertical (on the left or right) axes
 */
AxisDimensionSet2DSide::AxisDimensionSet2DSide(bool setVertical)
        : AxisDimensionSet2D(),
          vertical(setVertical),
          primary(),
          secondary(),
          primaryDepth(0),
          secondaryDepth(0)
{ }

AxisDimensionSet2DSide::~AxisDimensionSet2DSide() = default;

AxisDimensionSet2DSide::AxisDimensionSet2DSide(const AxisDimensionSet2DSide &) = default;

std::unique_ptr<AxisDimensionSet> AxisDimensionSet2DSide::clone() const
{ return std::unique_ptr<AxisDimensionSet>(new AxisDimensionSet2DSide(*this)); }

void AxisDimensionSet2DSide::finishUpdate()
{
    AxisDimensionSet2D::finishUpdate();

    primary.clear();
    secondary.clear();
    for (auto displayed : getDisplayed()) {
        auto dim = static_cast<AxisDimension2DSide *>(displayed);
        switch (dim->getSideBinding()) {
        case AxisParameters2DSide::Side_Auto:
            if (primary.size() <= secondary.size()) {
                primary.emplace_back(dim);
                dim->setPrimary(true);
                dim->setBinding(vertical ? Axis2DLeft : Axis2DBottom);
            } else {
                secondary.emplace_back(dim);
                dim->setPrimary(false);
                dim->setBinding(vertical ? Axis2DRight : Axis2DTop);
            }
            break;
        case AxisParameters2DSide::Side_Primary:
            primary.emplace_back(dim);
            dim->setPrimary(true);
            dim->setBinding(vertical ? Axis2DLeft : Axis2DBottom);
            break;
        case AxisParameters2DSide::Side_Secondary:
            secondary.emplace_back(dim);
            dim->setPrimary(false);
            dim->setBinding(vertical ? Axis2DRight : Axis2DTop);
            break;
        }
    }
}

bool AxisDimensionSet2DSide::prepareDraw(QPaintDevice *paintdevice,
                                         const DisplayDynamicContext &context)
{
    bool changed = false;

    QFontMetrics fm(QApplication::font());
    if (paintdevice)
        fm = QFontMetrics(QApplication::font(), paintdevice);
    qreal baseSize = fm.height();

    primaryDepth = 0;
    for (auto side : primary) {
        side->setTickPolarity(primaryDepth == 0.0);
        bool local = side->prepareDraw(paintdevice, context);
        if (!side->getDrawEnable())
            continue;
        if (local)
            changed = true;

        if (primaryDepth != 0.0)
            primaryDepth += baseSize * sideAxisBetweenSpace;
        side->setInset(primaryDepth);
        primaryDepth += side->getPredictedDepth();
    }
    if (primaryDepth == 0.0) {
        auto params = static_cast<const AxisParameters2DSide *>(getDefaultParameters());
        Q_ASSERT(params);
        primaryDepth = params->getBaselineWidth();
        if (primaryDepth <= 0.0)
            primaryDepth = 1.0;
    }

    secondaryDepth = 0;
    for (auto side : secondary) {
        side->setTickPolarity(secondaryDepth == 0.0);
        bool local = side->prepareDraw(paintdevice, context);
        if (!side->getDrawEnable())
            continue;
        if (local)
            changed = true;

        if (secondaryDepth != 0.0)
            secondaryDepth += baseSize * sideAxisBetweenSpace;
        side->setInset(secondaryDepth);
        secondaryDepth += side->getPredictedDepth();
    }
    if (secondaryDepth == 0.0) {
        auto params = static_cast<const AxisParameters2DSide *>(getDefaultParameters());
        Q_ASSERT(params);
        secondaryDepth = params->getBaselineWidth();
        if (secondaryDepth <= 0.0)
            secondaryDepth = 1.0;
    }

    return changed;
}

/**
 * Get the predicted depth away from the trace area of the axis side.
 * <br>
 * This may only be used after prepareDraw(QPaintDevice *, const DisplayDynamicContext &).
 * 
 * @param primary   if set then return the depth of the "primary" (left or bottom) side
 * @return          the depth of the side
 */
double AxisDimensionSet2DSide::getPredictedDepth(bool primary) const
{
    if (primary)
        return primaryDepth;
    return secondaryDepth;
}

void AxisDimensionSet2DSide::setArea(const QRectF &traceArea, const QRectF &inner)
{
    this->inner = inner;

    for (auto side : primary) {
        if (vertical)
            side->setScreen(traceArea.bottom() - 1, traceArea.top() + 1);
        else
            side->setScreen(traceArea.left() + 1, traceArea.right() - 1);
    }

    for (auto side : secondary) {
        if (vertical)
            side->setScreen(traceArea.bottom() - 1, traceArea.top() + 1);
        else
            side->setScreen(traceArea.left() + 1, traceArea.right() - 1);
    }
}


std::vector<std::unique_ptr<Graph2DDrawPrimitive>> AxisDimensionSet2DSide::createDraw() const
{
    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> result;

    qreal baseline;
    qreal farside;
    qreal min;
    qreal max;
    if (vertical) {
        baseline = inner.left();
        farside = inner.right() - 1;
        min = inner.top();
        max = inner.bottom();
    } else {
        baseline = inner.bottom();
        farside = inner.top() + 1;
        min = inner.left();
        max = inner.right();
    }
    qreal priorBaseline = baseline;
    bool anyDrawn = false;
    for (auto side : primary) {
        if (!side->getDrawEnable())
            continue;
        anyDrawn = true;
        qreal effectiveBaseline = baseline;
        if (vertical) {
            effectiveBaseline -= side->getInset();
        } else {
            effectiveBaseline += side->getInset();
        }

        Util::append(side->createDraw(effectiveBaseline, priorBaseline, farside, min, max), result);
        priorBaseline = effectiveBaseline;
    }
    if (!anyDrawn) {
        auto params = static_cast<const AxisParameters2DSide *>(getDefaultParameters());
        Q_ASSERT(params);
        result.emplace_back(
                new Axis2DPainterBaseline((vertical ? Axis2DLeft : Axis2DBottom), min, max,
                                          baseline, baseline, params->getBaselineWidth(),
                                          params->getBaselineStyle(), params->getBaselineColor(),
                                          params->getDrawPriority()));
    }

    if (vertical) {
        baseline = inner.right();
        farside = inner.left() + 1;
    } else {
        baseline = inner.top();
        farside = inner.bottom() - 1;
    }
    priorBaseline = baseline;
    anyDrawn = false;
    for (auto side : secondary) {
        if (!side->getDrawEnable())
            continue;
        anyDrawn = true;
        qreal effectiveBaseline = baseline;
        if (vertical) {
            effectiveBaseline += side->getInset();
        } else {
            effectiveBaseline -= side->getInset();
        }
        Util::append(side->createDraw(effectiveBaseline, priorBaseline, farside, min, max), result);
        priorBaseline = effectiveBaseline;
    }
    if (!anyDrawn) {
        auto params = static_cast<const AxisParameters2DSide *>(getDefaultParameters());
        Q_ASSERT(params);
        result.emplace_back(
                new Axis2DPainterBaseline((vertical ? Axis2DRight : Axis2DTop), min, max, baseline,
                                          baseline, params->getBaselineWidth(),
                                          params->getBaselineStyle(), params->getBaselineColor(),
                                          params->getDrawPriority()));
    }

    return result;
}

/**
 * Get the baseline of the given axis.
 * 
 * @param axis  the axis to find the baseline of
 * @return      the baseline
 */
double AxisDimensionSet2DSide::getBaseline(AxisDimension2DSide *axis) const
{
    Q_ASSERT(axis);
    if (axis->getPrimary()) {
        if (vertical) {
            return inner.left() - axis->getInset();
        } else {
            return inner.bottom() + axis->getInset();
        }
    }
    if (vertical) {
        return inner.right() + axis->getInset();
    } else {
        return inner.top() - axis->getInset();
    }
}

std::unique_ptr<AxisParameters> AxisDimensionSet2DSide::parseParameters(const Variant::Read &value,
                                                                        QSettings *settings) const
{ return std::unique_ptr<AxisParameters>(new AxisParameters2DSide(value, settings)); }

std::unique_ptr<AxisDimension> AxisDimensionSet2DSide::createDimension(std::unique_ptr<
        AxisParameters> &&parameters) const
{
    return std::unique_ptr<AxisDimension>(new AxisDimension2DSide(
            std::unique_ptr<AxisParameters2DSide>(
                    static_cast<AxisParameters2DSide *>(parameters.release()))));
}


AxisDimensionSet2DColor::AxisDimensionSet2DColor()
        : top(), bottom(), left(), right(), topDepth(0), bottomDepth(0), leftDepth(0), rightDepth(0)
{ }

AxisDimensionSet2DColor::~AxisDimensionSet2DColor() = default;

AxisDimensionSet2DColor::AxisDimensionSet2DColor(const AxisDimensionSet2DColor &other)
        : AxisDimensionSet2D(other),
          top(),
          bottom(),
          left(),
          right(),
          topDepth(other.topDepth),
          bottomDepth(other.bottomDepth),
          leftDepth(other.leftDepth),
          rightDepth(other.rightDepth)
{
    auto displayed = getDisplayed();
    auto otherDisplayed = other.getDisplayed();
    for (auto it : other.top) {
        auto check = static_cast<AxisDimension *>(it);
        auto idx = std::find(otherDisplayed.begin(), otherDisplayed.end(), check) -
                otherDisplayed.begin();
        Q_ASSERT(idx >= 0 && static_cast<std::size_t>(idx) < displayed.size());
        top.emplace_back(static_cast<AxisDimension2DColor *>(displayed[idx]));
    }
    for (auto it : other.bottom) {
        auto check = static_cast<AxisDimension *>(it);
        auto idx = std::find(otherDisplayed.begin(), otherDisplayed.end(), check) -
                otherDisplayed.begin();
        Q_ASSERT(idx >= 0 && static_cast<std::size_t>(idx) < displayed.size());
        bottom.emplace_back(static_cast<AxisDimension2DColor *>(displayed[idx]));
    }
    for (auto it : other.left) {
        auto check = static_cast<AxisDimension *>(it);
        auto idx = std::find(otherDisplayed.begin(), otherDisplayed.end(), check) -
                otherDisplayed.begin();
        Q_ASSERT(idx >= 0 && static_cast<std::size_t>(idx) < displayed.size());
        left.emplace_back(static_cast<AxisDimension2DColor *>(displayed[idx]));
    }
    for (auto it : other.right) {
        auto check = static_cast<AxisDimension *>(it);
        auto idx = std::find(otherDisplayed.begin(), otherDisplayed.end(), check) -
                otherDisplayed.begin();
        Q_ASSERT(idx >= 0 && static_cast<std::size_t>(idx) < displayed.size());
        right.emplace_back(static_cast<AxisDimension2DColor *>(displayed[idx]));
    }
}

std::unique_ptr<AxisDimensionSet> AxisDimensionSet2DColor::clone() const
{ return std::unique_ptr<AxisDimensionSet>(new AxisDimensionSet2DColor(*this)); }

void AxisDimensionSet2DColor::finishUpdate()
{
    AxisDimensionSet2D::finishUpdate();

    top.clear();
    bottom.clear();
    left.clear();
    right.clear();
    for (auto displayed : getDisplayed()) {
        auto dim = static_cast<AxisDimension2DColor *>(displayed);
        dim->setTickPolarity(false);
        switch (dim->getSideBinding()) {
        case AxisParameters2DColor::Side_Auto:
            if (top.size() < bottom.size() &&
                    top.size() < right.size() &&
                    top.size() < left.size()) {
                dim->setBinding(Axis2DTop);
                top.emplace_back(dim);
            } else if (bottom.size() < right.size() && bottom.size() < left.size()) {
                dim->setBinding(Axis2DBottom);
                bottom.emplace_back(dim);
            } else if (left.size() < right.size()) {
                dim->setBinding(Axis2DLeft);
                left.emplace_back(dim);
            } else {
                dim->setBinding(Axis2DRight);
                right.emplace_back(dim);
            }
            break;
        case AxisParameters2DColor::Side_Top:
            dim->setBinding(Axis2DTop);
            top.emplace_back(dim);
            break;
        case AxisParameters2DColor::Side_Bottom:
            dim->setBinding(Axis2DBottom);
            bottom.emplace_back(dim);
            break;
        case AxisParameters2DColor::Side_Left:
            dim->setBinding(Axis2DLeft);
            left.emplace_back(dim);
            break;
        case AxisParameters2DColor::Side_Right:
            dim->setBinding(Axis2DRight);
            right.emplace_back(dim);
            break;
        }
    }
}

bool AxisDimensionSet2DColor::prepareDraw(QPaintDevice *paintdevice,
                                          const DisplayDynamicContext &context)
{
    bool changed = false;

    QFontMetrics fm(QApplication::font());
    if (paintdevice)
        fm = QFontMetrics(QApplication::font(), paintdevice);
    qreal baseSize = fm.height();

    topDepth = 0;
    for (auto side : top) {
        bool local = side->prepareDraw(paintdevice, context);
        if (!side->getDrawEnable())
            continue;
        if (local)
            changed = true;

        if (topDepth != 0.0)
            topDepth += baseSize * colorAxisBetweenSpace;
        else
            topDepth += baseSize * colorAxisFirstSpace;
        side->setInset(topDepth);
        topDepth += side->getPredictedDepth();
    }

    bottomDepth = 0;
    for (auto side : bottom) {
        bool local = side->prepareDraw(paintdevice, context);
        if (!side->getDrawEnable())
            continue;
        if (local)
            changed = true;

        if (bottomDepth != 0.0)
            bottomDepth += baseSize * colorAxisBetweenSpace;
        else
            bottomDepth += baseSize * colorAxisFirstSpace;
        side->setInset(bottomDepth);
        bottomDepth += side->getPredictedDepth();
    }

    leftDepth = 0;
    for (auto side : left) {
        bool local = side->prepareDraw(paintdevice, context);
        if (!side->getDrawEnable())
            continue;
        if (local)
            changed = true;

        if (leftDepth != 0.0)
            leftDepth += baseSize * colorAxisBetweenSpace;
        else
            leftDepth += baseSize * colorAxisFirstSpace;
        side->setInset(leftDepth);
        leftDepth += side->getPredictedDepth();
    }

    rightDepth = 0;
    for (auto side : right) {
        bool local = side->prepareDraw(paintdevice, context);
        if (!side->getDrawEnable())
            continue;
        if (local)
            changed = true;

        if (rightDepth != 0.0)
            rightDepth += baseSize * colorAxisBetweenSpace;
        else
            rightDepth += baseSize * colorAxisFirstSpace;
        side->setInset(rightDepth);
        rightDepth += side->getPredictedDepth();
    }

    return changed;
}

/**
 * Get the predicted depth away from the trace area of the axis side.
 * <br>
 * This may only be used after prepareDraw(QPaintDevice *, const DisplayDynamicContext &).
 * 
 * @param side      the side to get the depth of
 * @return          the depth of the side
 */
double AxisDimensionSet2DColor::getPredictedDepth(Axis2DSide side) const
{
    switch (side) {
    case Axis2DTop:
        return topDepth;
    case Axis2DBottom:
        return bottomDepth;
    case Axis2DLeft:
        return leftDepth;
    case Axis2DRight:
        return rightDepth;
    }
    Q_ASSERT(false);
    return 0;
}

void AxisDimensionSet2DColor::setArea(const QRectF &traceArea, const QRectF &inner)
{
    innerColumn = inner;
    innerColumn.setLeft(traceArea.left());
    innerColumn.setRight(traceArea.right());

    innerRow = inner;
    innerRow.setTop(traceArea.top());
    innerRow.setBottom(traceArea.bottom());

    for (auto side : top) {
        side->setScreen(0.0, 1.0);
    }
    for (auto side : bottom) {
        side->setScreen(0.0, 1.0);
    }
    for (auto side : left) {
        side->setScreen(0.0, 1.0);
    }
    for (auto side : right) {
        side->setScreen(0.0, 1.0);
    }
}

std::vector<std::unique_ptr<Graph2DDrawPrimitive>> AxisDimensionSet2DColor::createDraw() const
{
    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> result;

    for (auto side : top) {
        if (!side->getDrawEnable())
            continue;
        Util::append(side->createDraw(innerColumn.top() - side->getInset(), innerColumn.left(),
                                      innerColumn.right()), result);
    }

    for (auto side : bottom) {
        if (!side->getDrawEnable())
            continue;
        Util::append(side->createDraw(innerColumn.bottom() + side->getInset(), innerColumn.left(),
                                      innerColumn.right()), result);
    }

    for (auto side : left) {
        if (!side->getDrawEnable())
            continue;
        Util::append(side->createDraw(innerRow.left() - side->getInset(), innerRow.top(),
                                      innerRow.bottom()), result);
    }

    for (auto side : right) {
        if (!side->getDrawEnable())
            continue;
        Util::append(side->createDraw(innerRow.right() + side->getInset(), innerRow.top(),
                                      innerRow.bottom()), result);
    }

    return std::move(result);
}

std::unique_ptr<AxisParameters> AxisDimensionSet2DColor::parseParameters(const Variant::Read &value,
                                                                         QSettings *settings) const
{ return std::unique_ptr<AxisParameters>(new AxisParameters2DColor(value, settings)); }

std::unique_ptr<AxisDimension> AxisDimensionSet2DColor::createDimension(std::unique_ptr<
        AxisParameters> &&parameters) const
{
    return std::unique_ptr<AxisDimension>(new AxisDimension2DColor(
            std::unique_ptr<AxisParameters2DColor>(
                    static_cast<AxisParameters2DColor *>(parameters.release()))));
}

}
}
