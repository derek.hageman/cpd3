/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>
#include <QLoggingCategory>
#include <QInputDialog>
#include <QColorDialog>
#include <QFontDialog>

#include "datacore/variant/root.hxx"
#include "algorithms/model.hxx"
#include "graphing/graphcommon2d.hxx"
#include "graphing/bin2d.hxx"
#include "graphing/graphpainters2d.hxx"
#include "algorithms/logwrapper.hxx"


Q_LOGGING_CATEGORY(log_graphing_bin2d, "cpd3.graphing.bin2d", QtWarningMsg)

using namespace CPD3::Data;
using namespace CPD3::Algorithms;
using namespace CPD3::Graphing::Internal;

namespace CPD3 {
namespace Graphing {

/** @file graphing/boxwhisker2d.hxx
 * The basis for two dimensional box whisker plots.
 */

BinParameters2D::~BinParameters2D() = default;

BinParameters2D::BinParameters2D()
        : BinParameters(),
          components(0),
          drawPriority(Graph2DDrawPrimitive::Priority_Bins),
          fillEnable(true),
          fillColor(255, 255, 255, 0),
          outlineEnable(true),
          outlineWidth(1.0),
          outlineStyle(Qt::SolidLine),
          outlineColor(0, 0, 0),
          whiskerEnable(true),
          whiskerLineWidth(1.0),
          whiskerLineStyle(Qt::SolidLine),
          whiskerColor(0, 0, 0),
          whiskerCapWidth(0),
          middleEnable(true),
          middleLineWidth(1.0),
          middleLineStyle(Qt::SolidLine),
          middleColor(0, 0, 0),
          middleExtendEnable(false),
          middleExtendLineWidth(1.0),
          middleExtendLineStyle(Qt::DashLine),
          middleExtendColor(127, 127, 127),
          middleExtendDrawPriority(Graph2DDrawPrimitive::Priority_BinMiddleLines),
          legendEntry(true),
          legendText(),
          legendFont(),
          legendSortPriority(0),
          iBinding(),
          dBinding(),
          drawMode(Draw_WidthConstantScale),
          drawConstant(0.5),
          drawBoxes(true),
          relocationEnable(true),
          fits()
{
    fits.emplace(Fit_Lowest, Fit());
    fits.emplace(Fit_Lower, Fit());
    fits.emplace(Fit_Middle, Fit());
    fits.emplace(Fit_Upper, Fit());
    fits.emplace(Fit_Uppermost, Fit());
}

BinParameters2D::BinParameters2D(const BinParameters2D &other) = default;

BinParameters2D &BinParameters2D::operator=(const BinParameters2D &other) = default;

BinParameters2D::BinParameters2D(BinParameters2D &&other) = default;

BinParameters2D &BinParameters2D::operator=(BinParameters2D &&other) = default;

void BinParameters2D::copy(const TraceParameters *from)
{ *this = *(static_cast<const BinParameters2D *>(from)); }

void BinParameters2D::clear()
{
    BinParameters::clear();
    components = 0;
    for (auto it : fits) {
        it.second.clear();
    }
}

std::unique_ptr<TraceParameters> BinParameters2D::clone() const
{ return std::unique_ptr<TraceParameters>(new BinParameters2D(*this)); }

/**
 * Construct trace parameters from the given configuration value.
 * 
 * @param value the value to configure from
 */
BinParameters2D::BinParameters2D(const Variant::Read &value) : BinParameters(value),
                                                               components(0),
                                                               drawPriority(
                                                                       Graph2DDrawPrimitive::Priority_Bins),
                                                               fillEnable(true),
                                                               fillColor(255, 255, 255, 0),
                                                               outlineEnable(true),
                                                               outlineWidth(1.0),
                                                               outlineStyle(Qt::SolidLine),
                                                               outlineColor(0, 0, 0),
                                                               whiskerEnable(true),
                                                               whiskerLineWidth(1.0),
                                                               whiskerLineStyle(Qt::SolidLine),
                                                               whiskerColor(0, 0, 0),
                                                               whiskerCapWidth(0),
                                                               middleEnable(true),
                                                               middleLineWidth(1.0),
                                                               middleLineStyle(Qt::SolidLine),
                                                               middleColor(0, 0, 0),
                                                               middleExtendEnable(false),
                                                               middleExtendLineWidth(1.0),
                                                               middleExtendLineStyle(Qt::DashLine),
                                                               middleExtendColor(127, 127, 127),
                                                               middleExtendDrawPriority(
                                                                       Graph2DDrawPrimitive::Priority_BinMiddleLines),
                                                               legendEntry(true),
                                                               legendText(),
                                                               legendFont(),
                                                               legendSortPriority(0),
                                                               iBinding(),
                                                               dBinding(),
                                                               drawMode(Draw_WidthConstantScale),
                                                               drawConstant(0.5),
                                                               drawBoxes(true),
                                                               relocationEnable(true),
                                                               fits()
{
    fits.emplace(Fit_Lowest, Fit(value["Fit/Lowest"]));
    fits.emplace(Fit_Lower, Fit(value["Fit/Lower"]));
    fits.emplace(Fit_Middle, Fit(value["Fit/Middle"]));
    fits.emplace(Fit_Upper, Fit(value["Fit/Upper"]));
    fits.emplace(Fit_Uppermost, Fit(value["Fit/Uppermost"]));

    if (INTEGER::defined(value["DrawPriority"].toInt64())) {
        drawPriority = value["DrawPriority"].toInt64();
        components |= Component_DrawPriority;
    }

    if (value["Fill/Enable"].exists()) {
        fillEnable = value["Fill/Enable"].toBool();
        components |= Component_FillEnable;
    }

    if (value["Fill/Color"].exists()) {
        fillColor = Display::parseColor(value["Fill/Color"], fillColor);
        components |= Component_FillColor;
    }

    if (value["Box/Enable"].exists()) {
        outlineEnable = value["Box/Enable"].toBool();
        components |= Component_OutlineEnable;
    }

    if (value["Box/Width"].exists()) {
        double checkLineWidth = value["Box/Width"].toDouble();
        if (!FP::defined(checkLineWidth) || checkLineWidth < 0.0)
            outlineWidth = 0.0;
        else
            outlineWidth = checkLineWidth;
        whiskerLineWidth = outlineWidth;
        middleLineWidth = outlineWidth;
        middleExtendLineWidth = outlineWidth;
        components |= Component_OutlineWidth;
    }

    if (value["Box/Style"].exists()) {
        outlineStyle = Display::parsePenStyle(value["Box/Style"], outlineStyle);
        whiskerLineStyle = outlineStyle;
        middleLineStyle = outlineStyle;
        components |= Component_OutlineStyle;
    }

    if (value["Box/Color"].exists()) {
        outlineColor = Display::parseColor(value["Box/Color"], outlineColor);
        whiskerColor = outlineColor;
        middleColor = outlineColor;
        middleExtendColor = outlineColor.lighter(150);
        components |= Component_OutlineColor;
    }


    if (value["Whisker/Enable"].exists()) {
        whiskerEnable = value["Whisker/Enable"].toBool();
        components |= Component_WhiskerEnable;
    }

    if (value["Whisker/Width"].exists()) {
        double checkLineWidth = value["Whisker/Width"].toDouble();
        if (!FP::defined(checkLineWidth) || checkLineWidth < 0.0)
            whiskerLineWidth = 0.0;
        else
            whiskerLineWidth = checkLineWidth;
        components |= Component_WhiskerLineWidth;
    }

    if (value["Whisker/CapWidth"].exists()) {
        double checkLineWidth = value["Whisker/CapWidth"].toDouble();
        if (!FP::defined(checkLineWidth) || checkLineWidth < 0.0)
            whiskerCapWidth = 0.0;
        else
            whiskerCapWidth = checkLineWidth;
        components |= Component_WhiskerCapWidth;
    }

    if (value["Whisker/Style"].exists()) {
        whiskerLineStyle = Display::parsePenStyle(value["Whisker/Style"], whiskerLineStyle);
        components |= Component_WhiskerLineStyle;
    }

    if (value["Whisker/Color"].exists()) {
        outlineColor = Display::parseColor(value["Whisker/Color"], whiskerColor);
        components |= Component_WhiskerColor;
    }


    if (value["Middle/Enable"].exists()) {
        middleEnable = value["Middle/Enable"].toBool();
        components |= Component_MiddleEnable;
    }

    if (value["Middle/Width"].exists()) {
        double checkLineWidth = value["Middle/Width"].toDouble();
        if (!FP::defined(checkLineWidth) || checkLineWidth < 0.0)
            middleLineWidth = 0.0;
        else
            middleLineWidth = checkLineWidth;
        middleExtendLineWidth = middleLineWidth;
        components |= Component_MiddleLineWidth;
    }

    if (value["Middle/Style"].exists()) {
        middleLineStyle = Display::parsePenStyle(value["Middle/Style"], middleLineStyle);
        components |= Component_MiddleLineStyle;
    }

    if (value["Middle/Color"].exists()) {
        middleColor = Display::parseColor(value["Middle/Color"], middleColor);
        middleExtendColor = middleColor.lighter(150);
        components |= Component_MiddleColor;
    }


    if (value["MiddleExtend/Enable"].exists()) {
        middleExtendEnable = value["Middle/Enable"].toBool();
        components |= Component_MiddleEnable;
    }

    if (value["MiddleExtend/Width"].exists()) {
        double checkLineWidth = value["MiddleExtend/Width"].toDouble();
        if (!FP::defined(checkLineWidth) || checkLineWidth < 0.0)
            middleExtendLineWidth = 0.0;
        else
            middleExtendLineWidth = checkLineWidth;
        components |= Component_MiddleLineWidth;
    }

    if (value["MiddleExtend/Style"].exists()) {
        middleExtendLineStyle =
                Display::parsePenStyle(value["MiddleExtend/Style"], middleExtendLineStyle);
        components |= Component_MiddleLineStyle;
    }

    if (value["MiddleExtend/Color"].exists()) {
        middleExtendColor = Display::parseColor(value["MiddleExtend/Color"], middleExtendColor);
        components |= Component_MiddleColor;
    }

    if (INTEGER::defined(value["MiddleExtend/DrawPriority"].toInt64())) {
        middleExtendDrawPriority = value["MiddleExtend/DrawPriority"].toInt64();
        components |= Component_MiddleExtendDrawPriority;
    }

    if (value["Legend/Enable"].exists()) {
        legendEntry = value["Legend/Enable"].toBool();
        components |= Component_LegendEntry;
    }

    if (value["Legend/Text"].exists()) {
        legendText = value["Legend/Text"].toDisplayString();
        components |= Component_LegendText;
    }

    if (value["Legend/Font"].exists()) {
        legendFont = Display::parseFont(value["Legend/Font"], legendFont);
        components |= Component_LegendFont;

        for (auto &it : fits) {
            if (it.second.components & Fit::Component_LegendFont)
                continue;
            it.second.legendFont = legendFont;
        }
    }

    if (value["Legend/SortPriority"].exists()) {
        legendSortPriority = (int) value["Legend/SortPriority"].toInt64();
        components |= Component_LegendSortPriority;
    }

    if (value["Axes/Independent"].exists()) {
        iBinding = value["Axes/Independent"].toQString();
        components |= Component_IBinding;
    }

    if (value["Axes/Dependent"].exists()) {
        dBinding = value["Axes/Dependent"].toQString();
        components |= Component_DBinding;
    }

    if (value["Width"].exists()) {
        double check = value["Width"].toDouble();
        if (FP::defined(check) && check > 0.0) {
            drawConstant = check;
            components |= Component_DrawMode;
        } else {
            const auto &type = value["Width/Type"].toString();
            if (Util::equal_insensitive(type, "scale")) {
                check = value["Width/Scale"].toDouble();
                if (FP::defined(check) && check > 0.0) {
                    drawMode = Draw_WidthScale;
                    drawConstant = check;
                    components |= Component_DrawMode;
                }
            } else if (Util::equal_insensitive(type, "fixed")) {
                check = value["Width/Size"].toDouble();
                if (FP::defined(check) && check > 0.0) {
                    drawMode = Draw_WidthFixedReal;
                    drawConstant = check;
                    components |= Component_DrawMode;
                }
            } else if (Util::equal_insensitive(type, "fixedexact")) {
                check = value["Width/Size"].toDouble();
                if (FP::defined(check) && check > 0.0) {
                    drawMode = Draw_WidthFixedScreen;
                    drawConstant = check;
                    components |= Component_DrawMode;
                }
            } else if (Util::equal_insensitive(type, "constant")) {
                check = value["Width/Size"].toDouble();
                if (FP::defined(check) && check > 0.0) {
                    drawMode = Draw_WidthConstantScale;
                    drawConstant = check;
                    components |= Component_DrawMode;
                }
            }
        }
    }

    if (value["DrawBoxes"].exists()) {
        drawBoxes = value["DrawBoxes"].toBool();
        components |= Component_DrawBoxes;
    }

    if (value["Relocation"].exists()) {
        relocationEnable = value["Relocation"].toBool();
        components |= Component_RelocationEnable;
    }
}

void BinParameters2D::save(Variant::Write &value) const
{
    BinParameters::save(value);

    if (components & Component_DrawPriority) {
        value["DrawPriority"].setInt64(drawPriority);
    }

    if (components & Component_FillEnable) {
        value["Fill/Enable"].setBool(fillEnable);
    }

    if (components & Component_FillColor) {
        value["Fill/Color"].set(Display::formatColor(fillColor));
    }

    if (components & Component_OutlineEnable) {
        value["Box/Enable"].setBool(outlineEnable);
    }

    if (components & Component_OutlineWidth) {
        value["Box/Width"].setBool(outlineWidth);
    }

    if (components & Component_OutlineStyle) {
        value["Box/Style"].set(Display::formatPenStyle(outlineStyle));
    }

    if (components & Component_OutlineColor) {
        value["Box/Color"].set(Display::formatColor(outlineColor));
    }

    if (components & Component_WhiskerEnable) {
        value["Whisker/Enable"].setBool(whiskerEnable);
    }

    if (components & Component_WhiskerLineWidth) {
        value["Whisker/Width"].setDouble(whiskerLineWidth);
    }

    if (components & Component_WhiskerLineStyle) {
        value["Whisker/Style"].set(Display::formatPenStyle(whiskerLineStyle));
    }

    if (components & Component_WhiskerColor) {
        value["Whisker/Color"].set(Display::formatColor(whiskerColor));
    }

    if (components & Component_WhiskerCapWidth) {
        value["Whisker/CapWidth"].setDouble(whiskerCapWidth);
    }

    if (components & Component_MiddleEnable) {
        value["Middle/Enable"].setBool(middleEnable);
    }

    if (components & Component_MiddleLineWidth) {
        value["Middle/Width"].setDouble(middleLineWidth);
    }

    if (components & Component_MiddleLineStyle) {
        value["Middle/Style"].set(Display::formatPenStyle(middleLineStyle));
    }

    if (components & Component_MiddleColor) {
        value["Middle/Color"].set(Display::formatColor(middleColor));
    }

    if (components & Component_MiddleExtendEnable) {
        value["MiddleExtend/Enable"].setBool(middleExtendEnable);
    }

    if (components & Component_MiddleExtendLineWidth) {
        value["MiddleExtend/Width"].setDouble(middleExtendLineWidth);
    }

    if (components & Component_MiddleExtendLineStyle) {
        value["MiddleExtend/Style"].set(Display::formatPenStyle(middleExtendLineStyle));
    }

    if (components & Component_MiddleExtendColor) {
        value["MiddleExtend/Color"].set(Display::formatColor(middleExtendColor));
    }

    if (components & Component_MiddleExtendDrawPriority) {
        value["MiddleExtend/DrawPriority"].setInt64(middleExtendDrawPriority);
    }

    if (components & Component_LegendEntry) {
        value["Legend/Enable"].setBool(legendEntry);
    }

    if (components & Component_LegendText) {
        value["Legend/Text"].setString(legendText);
    }

    if (components & Component_LegendFont) {
        value["Legend/Font"].set(Display::formatFont(legendFont));
    }

    if (components & Component_LegendSortPriority) {
        value["Legend/SortPriority"].setInt64(legendSortPriority);
    }

    if (components & Component_IBinding) {
        value["Axes/Independent"].setString(iBinding);
    }

    if (components & Component_DBinding) {
        value["Axes/Dependent"].setString(dBinding);
    }

    if (components & Component_DrawMode) {
        switch (drawMode) {
        case Draw_WidthConstantScale:
            value["Width/Type"].setString("Constant");
            value["Width/Size"].setDouble(drawConstant);
            break;
        case Draw_WidthScale:
            value["Width/Type"].setString("Scale");
            value["Width/Scale"].setDouble(drawConstant);
            break;
        case Draw_WidthFixedReal:
            value["Width/Type"].setString("Fixed");
            value["Width/Size"].setDouble(drawConstant);
            break;
        case Draw_WidthFixedScreen:
            value["Width/Type"].setString("FixedExact");
            value["Width/Size"].setDouble(drawConstant);
            break;
        }
    }

    if (components & Component_DrawBoxes) {
        value["DrawBoxes"].setBool(drawBoxes);
    }

    if (components & Component_RelocationEnable) {
        value["Relocation"].setBool(relocationEnable);
    }

    for (const auto &it : fits) {
        Variant::Write target;
        switch (it.first) {
        case Fit_Lowest:
            target = value["Fit/Lowest"];
            break;
        case Fit_Lower:
            target = value["Fit/Lower"];
            break;
        case Fit_Middle:
            target = value["Fit/Middle"];
            break;
        case Fit_Upper:
            target = value["Fit/Upper"];
            break;
        case Fit_Uppermost:
            target = value["Fit/Uppermost"];
            break;
        }
        it.second.save(target);
    }
}

void BinParameters2D::overlay(const TraceParameters *over)
{
    TraceParameters::overlay(over);

    const BinParameters2D *top = static_cast<const BinParameters2D *>(over);

    if (top->components & Component_DrawPriority) {
        drawPriority = top->drawPriority;
        components |= Component_DrawPriority;
    }

    if (top->components & Component_FillEnable) {
        fillEnable = top->fillEnable;
        components |= Component_FillEnable;
    }

    if (top->components & Component_FillColor) {
        fillColor = top->fillColor;
        components |= Component_FillColor;
    }

    if (top->components & Component_OutlineEnable) {
        outlineEnable = top->outlineEnable;
        components |= Component_OutlineEnable;
    }

    if (top->components & Component_OutlineWidth) {
        outlineWidth = top->outlineWidth;
        if (!hasWhiskerLineWidth())
            whiskerLineWidth = outlineWidth;
        if (!hasMiddleLineWidth())
            middleLineWidth = outlineWidth;
        if (!hasMiddleExtendLineWidth())
            middleExtendLineWidth = outlineWidth;
        components |= Component_OutlineWidth;
    }

    if (top->components & Component_OutlineStyle) {
        outlineStyle = top->outlineStyle;
        if (!hasWhiskerLineStyle())
            whiskerLineStyle = outlineStyle;
        if (!hasMiddleLineStyle())
            middleLineStyle = outlineStyle;
        components |= Component_OutlineStyle;
    }

    if (top->components & Component_OutlineColor) {
        outlineColor = top->outlineColor;
        if (!hasWhiskerColor())
            whiskerColor = outlineColor;
        if (!hasMiddleColor())
            middleColor = outlineColor;
        if (!hasMiddleExtendColor())
            middleExtendColor = outlineColor.lighter(150);
        components |= Component_OutlineColor;
    }

    if (top->components & Component_WhiskerEnable) {
        whiskerEnable = top->whiskerEnable;
        components |= Component_WhiskerEnable;
    }

    if (top->components & Component_WhiskerLineWidth) {
        whiskerLineWidth = top->whiskerLineWidth;
        components |= Component_WhiskerLineWidth;
    }

    if (top->components & Component_WhiskerLineStyle) {
        whiskerLineStyle = top->whiskerLineStyle;
        components |= Component_WhiskerLineStyle;
    }

    if (top->components & Component_WhiskerColor) {
        whiskerColor = top->whiskerColor;
        components |= Component_WhiskerColor;
    }

    if (top->components & Component_WhiskerCapWidth) {
        whiskerCapWidth = top->whiskerCapWidth;
        components |= Component_WhiskerCapWidth;
    }

    if (top->components & Component_MiddleEnable) {
        middleEnable = top->middleEnable;
        components |= Component_MiddleEnable;
    }

    if (top->components & Component_MiddleLineWidth) {
        middleLineWidth = top->middleLineWidth;
        if (!hasMiddleExtendLineWidth())
            middleExtendLineWidth = middleLineWidth;
        components |= Component_MiddleLineWidth;
    }

    if (top->components & Component_MiddleLineStyle) {
        middleLineStyle = top->middleLineStyle;
        components |= Component_MiddleLineStyle;
    }

    if (top->components & Component_MiddleColor) {
        middleColor = top->middleColor;
        if (!hasMiddleExtendColor())
            middleExtendColor = middleColor.lighter(150);
        components |= Component_MiddleColor;
    }

    if (top->components & Component_MiddleExtendEnable) {
        middleExtendEnable = top->middleExtendEnable;
        components |= Component_MiddleExtendEnable;
    }

    if (top->components & Component_MiddleExtendLineWidth) {
        middleExtendLineWidth = top->middleExtendLineWidth;
        components |= Component_MiddleExtendLineWidth;
    }

    if (top->components & Component_MiddleExtendLineStyle) {
        middleExtendLineStyle = top->middleExtendLineStyle;
        components |= Component_MiddleExtendLineStyle;
    }

    if (top->components & Component_MiddleExtendColor) {
        middleExtendColor = top->middleExtendColor;
        components |= Component_MiddleExtendColor;
    }

    if (top->components & Component_MiddleExtendDrawPriority) {
        middleExtendDrawPriority = top->middleExtendDrawPriority;
        components |= Component_MiddleExtendDrawPriority;
    }

    if (top->components & Component_LegendEntry) {
        legendEntry = top->legendEntry;
        components |= Component_LegendEntry;
    }

    if (top->components & Component_LegendText) {
        legendText = top->legendText;
        components |= Component_LegendText;
    }

    if (top->components & Component_LegendFont) {
        legendFont = top->legendFont;
        components |= Component_LegendFont;
    }

    if (top->components & Component_LegendSortPriority) {
        legendSortPriority = top->legendSortPriority;
        components |= Component_LegendSortPriority;
    }

    if (top->components & Component_IBinding) {
        iBinding = top->iBinding;
        components |= Component_IBinding;
    }

    if (top->components & Component_DBinding) {
        dBinding = top->dBinding;
        components |= Component_DBinding;
    }

    if (top->components & Component_DrawMode) {
        drawMode = top->drawMode;
        drawConstant = top->drawConstant;
        components |= Component_DrawMode;
    }

    if (top->components & Component_DrawBoxes) {
        drawBoxes = top->drawBoxes;
        components |= Component_DrawBoxes;
    }

    if (top->components & Component_RelocationEnable) {
        relocationEnable = top->relocationEnable;
        components |= Component_RelocationEnable;
    }

    for (auto &it : fits) {
        it.second.overlay(top->fits.at(it.first));
    }
}


BinParameters2D::Fit::Fit() : components(0),
                              model(),
                              logIBase(FP::undefined()),
                              logDBase(FP::undefined()),
                              lineWidth(2.0),
                              lineStyle(Qt::SolidLine),
                              drawPriority(Graph2DDrawPrimitive::Priority_TraceFit),
                              color(0, 0, 0),
                              legendText(),
                              legendFont()
{
}

BinParameters2D::Fit::Fit(const Fit &) = default;

BinParameters2D::Fit &BinParameters2D::Fit::operator=(const Fit &) = default;

BinParameters2D::Fit::Fit(Fit &&) = default;

BinParameters2D::Fit &BinParameters2D::Fit::operator=(Fit &&) = default;

BinParameters2D::Fit::Fit(const Variant::Read &value) : components(0),
                                                        model(value["Model"]),
                                                        logIBase(FP::undefined()),
                                                        logDBase(FP::undefined()),
                                                        lineWidth(2.0),
                                                        lineStyle(Qt::SolidLine),
                                                        drawPriority(
                                                                Graph2DDrawPrimitive::Priority_TraceFit),
                                                        color(0, 0, 0),
                                                        legendText(),
                                                        legendFont()
{

    if (model.read().exists()) {
        components |= Component_Model;
    }

    if (value["Log/I"].exists()) {
        logIBase = value["Log/I"].toDouble();
        components |= Component_LogIBase;
    }

    if (value["Log/D"].exists()) {
        logDBase = value["Log/D"].toDouble();
        components |= Component_LogDBase;
    }

    if (value["Line/Width"].exists()) {
        lineWidth = value["Line/Width"].toDouble();
        if (!FP::defined(lineWidth) || lineWidth < 0.0)
            lineWidth = 2.0;
        components |= Component_LineWidth;
    }

    if (value["Line/Style"].exists()) {
        lineStyle = Display::parsePenStyle(value["Fit/Line/Style"], lineStyle);
        components |= Component_LineStyle;
    }

    if (INTEGER::defined(value["DrawPriority"].toInt64())) {
        drawPriority = value["DrawPriority"].toInt64();
        components |= Component_DrawPriority;
    }

    if (value["Color"].exists()) {
        color = Display::parseColor(value["Color"], color);
        components |= Component_Color;
    }

    if (value["Legend/Text"].exists()) {
        legendText = value["Legend/Text"].toDisplayString();
        components |= Component_LegendText;
    }

    if (value["Legend/Font"].exists()) {
        legendFont = Display::parseFont(value["Legend/Font"], legendFont);
        components |= Component_LegendFont;
    }
}

void BinParameters2D::Fit::save(Variant::Write &value) const
{
    if (components & Component_Model) {
        value["Model"].set(model);
    }

    if (components & Component_LogIBase) {
        value["Log/I"].setDouble(logIBase);
    }

    if (components & Component_LogDBase) {
        value["Log/D"].setDouble(logDBase);
    }

    if (components & Component_LineWidth) {
        value["Line/Width"].setDouble(lineWidth);
    }

    if (components & Component_LineStyle) {
        value["Line/Style"].set(Display::formatPenStyle(lineStyle));
    }

    if (components & Component_DrawPriority) {
        value["DrawPriority"].setInt64(drawPriority);
    }

    if (components & Component_Color) {
        value["Color"].set(Display::formatColor(color));
    }

    if (components & Component_LegendText) {
        value["Legend/Text"].setString(legendText);
    }

    if (components & Component_LegendFont) {
        value["Legend/Font"].set(Display::formatFont(legendFont));
    }
}

void BinParameters2D::Fit::overlay(const Fit &top)
{
    if (top.components & Component_Model) {
        model = top.model;
        components |= Component_Model;
    }

    if (top.components & Component_LogIBase) {
        logIBase = top.logIBase;
        components |= Component_LogIBase;
    }

    if (top.components & Component_LogDBase) {
        logDBase = top.logDBase;
        components |= Component_LogDBase;
    }

    if (top.components & Component_LineWidth) {
        lineWidth = top.lineWidth;
        components |= Component_LineWidth;
    }

    if (top.components & Component_LineStyle) {
        lineStyle = top.lineStyle;
        components |= Component_LineStyle;
    }

    if (top.components & Component_DrawPriority) {
        drawPriority = top.drawPriority;
        components |= Component_DrawPriority;
    }

    if (top.components & Component_Color) {
        color = top.color;
        components |= Component_Color;
    }

    if (top.components & Component_LegendText) {
        legendText = top.legendText;
        components |= Component_LegendText;
    }

    if (top.components & Component_LegendFont) {
        legendFont = top.legendFont;
        components |= Component_LegendFont;
    }
}

void BinParameters2D::Fit::clear()
{ components = 0; }

static const BinParameters2D::FitOrigin fitOriginOrder[] =
        {BinParameters2D::Fit_Uppermost, BinParameters2D::Fit_Upper, BinParameters2D::Fit_Middle,
         BinParameters2D::Fit_Lower, BinParameters2D::Fit_Lowest,};

BinHandler2D::BinHandler2D(std::unique_ptr<BinParameters2D> &&params) : BinValueHandler<1, 1>(
        std::move(params)),
                                                                        parameters(
                                                                                static_cast<BinParameters2D *>(getParameters())),
                                                                        anyValidPoints(false),
                                                                        alwaysHideLegend(false)
{
    for (std::size_t i = 0; i < sizeof(fitOriginOrder) / sizeof(fitOriginOrder[0]); i++) {
        BinParameters2D::FitOrigin origin = fitOriginOrder[i];
        if (!parameters->hasFitModel(origin))
            continue;
        fits.emplace(origin, std::unique_ptr<Fit>(new Fit));
    }
}

BinHandler2D::~BinHandler2D() = default;

BinHandler2D::BinHandler2D(const BinHandler2D &other, std::unique_ptr<TraceParameters> &&params)
        : BinValueHandler<1, 1>(other, std::move(params)),
          parameters(static_cast<BinParameters2D *>(getParameters())),
          anyValidPoints(other.anyValidPoints),
          alwaysHideLegend(other.alwaysHideLegend)
{
    for (const auto &it : other.fits) {
        std::unique_ptr<Fit> add(new Fit);
        if (it.second->running || it.second->fit)
            add->needRerun = true;
        else
            add->needRerun = it.second->needRerun;
        fits.emplace(it.first, std::move(add));
    }
}

BinHandler2D::Cloned BinHandler2D::clone() const
{ return Cloned(new BinHandler2D(*this, parameters->clone())); }

BinHandler2D::Fit::Fit() : running(NULL), fit(), needRerun(false)
{ }

BinHandler2D::Fit::~Fit()
{
    if (running != NULL)
        running->deleteWhenFinished();
}

Model *BinHandler2D::wrapFit(BinParameters2D::FitOrigin origin, Model *input)
{
    if (input == NULL)
        return NULL;
    return LogWrapperMultiple::create(input,
                                      QVector<double>() << parameters->getFitLogIBase(origin),
                                      QVector<double>() << parameters->getFitLogDBase(origin));
}

static double applyInputLog(double in, double logBase)
{
    if (!FP::defined(logBase))
        return in;
    if (!FP::defined(in))
        return in;
    if (in <= 0.0)
        return FP::undefined();
    return log(in) / logBase;
}

bool BinHandler2D::startFit(BinParameters2D::FitOrigin origin, Fit *fit, bool deferrable)
{
    if (deferrable && fit->running != NULL) {
        fit->needRerun = true;
        return false;
    }
    if (!anyValidPoints) {
        if (fit->running != NULL) {
            fit->running->deleteWhenFinished();
            fit->running = NULL;
        }
        fit->fit.reset();
        return false;
    }
    fit->needRerun = false;

    QList<ModelInputConstraints> inputs;
    QList<ModelOutputConstraints> outputs;
    ModelParameters modelParams;

    double lIBase = parameters->getFitLogIBase(origin);
    if (FP::defined(lIBase) && lIBase > 0.0)
        lIBase = log(lIBase);
    else
        lIBase = FP::undefined();
    double lDBase = parameters->getFitLogDBase(origin);
    if (FP::defined(lDBase) && lDBase > 0.0)
        lDBase = log(lDBase);
    else
        lDBase = FP::undefined();

    if (FP::defined(getLimits().min[0])) {
        inputs.append(ModelInputConstraints(applyInputLog(getLimits().min[0], lIBase),
                                            applyInputLog(getLimits().max[0], lIBase)));
    } else {
        inputs.append(ModelInputConstraints());
    }
    outputs.append(ModelOutputConstraints());

    QVector<double> result;
    for (const auto &it : getPoints()) {
        result.push_back(applyInputLog(it.center[0], lIBase));
    }
    modelParams.setDimension(0, result);

    result.clear();
    switch (origin) {
    case BinParameters2D::Fit_Lowest:
        for (const auto &it : getPoints()) {
            result.push_back(applyInputLog(it.lowest[0], lDBase));
        }
        break;
    case BinParameters2D::Fit_Lower:
        for (const auto &it : getPoints()) {
            result.push_back(applyInputLog(it.lower[0], lDBase));
        }
        break;
    case BinParameters2D::Fit_Middle:
        for (const auto &it : getPoints()) {
            result.push_back(applyInputLog(it.middle[0], lDBase));
        }
        break;
    case BinParameters2D::Fit_Upper:
        for (const auto &it : getPoints()) {
            result.push_back(applyInputLog(it.upper[0], lDBase));
        }
        break;
    case BinParameters2D::Fit_Uppermost:
        for (const auto &it : getPoints()) {
            result.push_back(applyInputLog(it.uppermost[0], lDBase));
        }
        break;
    }
    modelParams.setDimension(1, result);

    if (deferrable) {
        fit->running = new DeferredModelWrapper(parameters->getFitModel(origin), inputs, outputs,
                                                modelParams);
        QObject::connect(fit->running, &DeferredModelWrapper::modelReady, this,
                         &TraceValueHandlerBase::deferredUpdate, Qt::QueuedConnection);
        fit->running->waitForModelReady(5);
    } else {
        if (fit->running != NULL) {
            fit->running->deleteWhenFinished();
            fit->running = NULL;
        }
        fit->fit = std::shared_ptr<Model>(wrapFit(origin, Model::fromConfiguration(
                parameters->getFitModel(origin), inputs, outputs, modelParams)));
        return true;
    }

    return false;
}

bool BinHandler2D::process(const std::vector<TraceDispatch::OutputValue> &incoming, bool deferrable)
{
    bool changed = BinValueHandler<1, 1>::process(incoming, deferrable);

    if (changed) {
        bool hadValidPoints = anyValidPoints;
        anyValidPoints = !getPoints().empty();

        if (anyValidPoints || hadValidPoints) {
            for (auto &it : fits) {
                it.second->needRerun = true;
            }
        }
    }

    return changed;
}

/**
 * Inform the trace that the axes have been bound and pass it parameters
 * about them.  This must be called before anything that uses the fits
 * is invoked.
 * 
 * @param x             the X axis or NULL
 * @param y             the Y axis or NULL
 * @param vertical      true if the the bins are vertical
 * @param deferrable    true if the update can be deferred
 * @return              true if the trace has changed
 */
bool BinHandler2D::axesBound(AxisDimension *x, AxisDimension *y, bool vertical, bool deferrable)
{
    bool changed = false;

    AxisDimension *iAxis;
    AxisDimension *dAxis;
    if (vertical) {
        iAxis = x;
        dAxis = y;
    } else {
        iAxis = y;
        dAxis = x;
    }
    for (auto &it : fits) {
        if (!parameters->hasFitLogIBase(it.first) &&
                iAxis != NULL &&
                !FP::equal(iAxis->getTickLogBase(), parameters->getFitLogIBase(it.first))) {
            parameters->setFitLogIBase(it.first, iAxis->getTickLogBase());
            it.second->needRerun = true;
        }
        if (!parameters->hasFitLogDBase(it.first) &&
                dAxis != NULL &&
                !FP::equal(dAxis->getTickLogBase(), parameters->getFitLogDBase(it.first))) {
            parameters->setFitLogDBase(it.first, dAxis->getTickLogBase());
            it.second->needRerun = true;
        }


        if (it.second->needRerun ||
                (!it.second->fit && (!deferrable || it.second->running == NULL))) {
            if (startFit(it.first, it.second.get(), deferrable))
                changed = true;
        }
        if (it.second->running != NULL) {
            Model *check = it.second->running->takeModel();
            if (check != NULL) {
                delete it.second->running;
                it.second->running = NULL;
                it.second->fit = std::shared_ptr<Model>(wrapFit(it.first, check));
                changed = true;
            }
        }
    }

    return changed;
}

void BinHandler2D::convertValue(const TraceDispatch::OutputValue &value,
                                std::vector<TracePoint<2>> &points)
{
    auto priorSize = points.size();
    BinValueHandler<1, 1>::convertValue(value, points);
    if (priorSize == points.size())
        return;
    for (auto it = points.begin() + priorSize; it != points.end();) {
        if (!FP::defined(it->d[1]) || !FP::defined(it->d[0])) {
            it = points.erase(it);
            continue;
        }
        ++it;
    }
}

void BinHandler2D::trimToLast()
{
    BinValueHandler<1, 1>::trimToLast();
    for (auto &it : fits) {
        it.second->needRerun = true;
    }
}

void BinHandler2D::trimData(double start, double end)
{
    BinValueHandler<1, 1>::trimData(start, end);
    for (auto &it : fits) {
        it.second->needRerun = true;
    }
}

void BinHandler2D::setVisibleTimeRange(double start, double end)
{
    BinValueHandler<1, 1>::setVisibleTimeRange(start, end);
    for (auto &it : fits) {
        it.second->needRerun = true;
    }
}

QString BinHandler2D::getLegendText(const std::vector<SequenceName::Set> &inputUnits,
                                    bool showStation,
                                    bool showArchive,
                                    const DisplayDynamicContext &context,
                                    bool *changed) const
{
    if (parameters->hasLegendText()) {
        QString text(parameters->getLegendText());
        context.handleString(text, DisplayDynamicContext::String_GraphLegend, changed);
        return text;
    }
    return TraceValueHandler<2>::getLegendText(inputUnits, showStation, showArchive);
}

/**
 * Get the effective color of the bins.
 * @return the effective color.
 */
QColor BinHandler2D::getColor() const
{
    if (!parameters->getFillColor().isValid() || parameters->getFillColor().alpha() == 0) {
        if (parameters->getOutlineEnable()) {
            return parameters->getOutlineColor();
        } else if (parameters->getWhiskerEnable()) {
            return parameters->getWhiskerColor();
        } else if (parameters->getMiddleEnable()) {
            return parameters->getMiddleColor();
        } else if (parameters->getMiddleExtendEnable()) {
            return parameters->getMiddleExtendColor();
        }
        return QColor(0, 0, 0);
    } else {
        return parameters->getFillColor();
    }
}

/**
 * Get the display title of this trace.
 * 
 * @param inputUnits    the current set of units that went into this trace
 * @param showStation   if set then show the station in the legend
 * @param showArchive   if set then show the archive in the legend
 * @param context       the dynamic context
 * @return              the in order legend items
 */
QString BinHandler2D::getDisplayTitle(const std::vector<SequenceName::Set> &inputUnits,
                                      bool showStation,
                                      bool showArchive,
                                      const DisplayDynamicContext &context) const
{
    QString title(getLegendText(inputUnits, showStation, showArchive, context, NULL));
    if (!title.isEmpty())
        return title;
    if (fits.size() == 1) {
        auto selectedFit = fits.begin();
        if (parameters->hasFitLegendText(selectedFit->first) &&
                !parameters->getFitLegendText(selectedFit->first).isEmpty()) {
            return context.handleString(parameters->getFitLegendText(selectedFit->first),
                                        DisplayDynamicContext::String_GraphLegend, NULL);
        } else {
            if (selectedFit->second->fit) {
                ModelLegendParameters lp;
                lp.setConfidence(0.95);
                ModelLegendEntry legend(selectedFit->second->fit->legend(lp));
                return legend.getLine();
            }
        }
    }

    return TraceValueHandler<2>::getLegendText(inputUnits, showStation, showArchive);
}

namespace {

class GraphLegendItemBins2D : public GraphLegendItem2D {
    QPointer<BinHandler2D> handler;
public:
    GraphLegendItemBins2D(BinHandler2D *setHandler,
                          const QString &setText,
                          const QColor &setColor,
                          const QFont &setFont,
                          qreal setLineWidth,
                          Qt::PenStyle setStyle,
                          const TraceSymbol &setSymbol,
                          bool setDrawSymbol,
                          bool setDrawSwatch = false) : GraphLegendItem2D(setText, setColor,
                                                                          setFont, setLineWidth,
                                                                          setStyle, setSymbol,
                                                                          setDrawSymbol,
                                                                          setDrawSwatch),
                                                        handler(setHandler)
    { }

    virtual DisplayModificationComponent *createModificationComponent(Graph2D *graph)
    {
        return new Graph2DLegendModificationBins(graph, handler, getText());
    }

    virtual QString getLegendTooltip() const
    {
        if (handler.isNull())
            return QString();
        return handler->getLegendTooltip();
    }
};

class GraphLegendItemBinsFit2D : public GraphLegendItem2D {
    QPointer<BinHandler2D> handler;
    BinParameters2D::FitOrigin origin;
public:
    GraphLegendItemBinsFit2D(BinHandler2D *setHandler,
                             BinParameters2D::FitOrigin setOrigin,
                             const QString &setText,
                             const QColor &setColor,
                             const QFont &setFont,
                             qreal setLineWidth,
                             Qt::PenStyle setStyle,
                             const TraceSymbol &setSymbol,
                             bool setDrawSymbol,
                             bool setDrawSwatch = false) : GraphLegendItem2D(setText, setColor,
                                                                             setFont, setLineWidth,
                                                                             setStyle, setSymbol,
                                                                             setDrawSymbol,
                                                                             setDrawSwatch),
                                                           handler(setHandler),
                                                           origin(setOrigin)
    { }

    virtual DisplayModificationComponent *createModificationComponent(Graph2D *graph)
    {
        return new Graph2DLegendModificationBinsFit(graph, handler, origin, getText());
    }

    virtual QString getLegendTooltip() const
    {
        if (handler.isNull())
            return QString();
        return handler->getLegendTooltip();
    }
};

}

/**
 * Get the legend items (in order) associated with this trace.
 * 
 * @param inputUnits    the current set of units that went into this trace
 * @param showStation   if set then show the station in the legend
 * @param showArchive   if set then show the archive in the legend
 * @param context       the dynamic context
 * @param changed       the changed value passed to the dynamic context
 * @return              the in order legend items
 */
std::vector<std::shared_ptr<LegendItem>> BinHandler2D::getLegend(const std::vector<
        SequenceName::Set> &inputUnits,
                                                                 bool showStation,
                                                                 bool showArchive,
                                                                 const DisplayDynamicContext &context,
                                                                 bool *changed)
{
    if (!anyValidPoints || alwaysHideLegend || !parameters->getLegendEntry())
        return {};

    QString legendText(getLegendText(inputUnits, showStation, showArchive, context, changed));

    std::vector<std::shared_ptr<LegendItem>> result;
    GraphLegendItem2D *add = NULL;

    if (parameters->getDrawBoxes() && !legendText.isEmpty()) {
        if (!parameters->getFillColor().isValid() || parameters->getFillColor().alpha() == 0) {
            if (parameters->getOutlineEnable()) {
                result.emplace_back(std::make_shared<GraphLegendItemBins2D>(this, legendText,
                                                                            parameters->getOutlineColor(),
                                                                            parameters->getLegendFont(),
                                                                            parameters->getOutlineWidth(),
                                                                            parameters->getOutlineStyle(),
                                                                            TraceSymbol(), false));
            } else if (parameters->getWhiskerEnable()) {
                result.emplace_back(std::make_shared<GraphLegendItemBins2D>(this, legendText,
                                                                            parameters->getWhiskerColor(),
                                                                            parameters->getLegendFont(),
                                                                            parameters->getWhiskerLineWidth(),
                                                                            parameters->getWhiskerLineStyle(),
                                                                            TraceSymbol(), false));
            } else if (parameters->getMiddleEnable()) {
                result.emplace_back(std::make_shared<GraphLegendItemBins2D>(this, legendText,
                                                                            parameters->getMiddleColor(),
                                                                            parameters->getLegendFont(),
                                                                            parameters->getMiddleLineWidth(),
                                                                            parameters->getMiddleLineStyle(),
                                                                            TraceSymbol(), false));
            } else if (parameters->getMiddleExtendEnable()) {
                result.emplace_back(std::make_shared<GraphLegendItemBins2D>(this, legendText,
                                                                            parameters->getMiddleExtendColor(),
                                                                            parameters->getLegendFont(),
                                                                            parameters->getMiddleExtendLineWidth(),
                                                                            parameters->getMiddleExtendLineStyle(),
                                                                            TraceSymbol(), false));
            }
        } else {
            result.emplace_back(std::make_shared<GraphLegendItemBins2D>(this, legendText,
                                                                        parameters->getFillColor(),
                                                                        parameters->getLegendFont(),
                                                                        0, Qt::NoPen, TraceSymbol(),
                                                                        false, true));
        }
    }

    for (size_t i = 0; i < sizeof(fitOriginOrder) / sizeof(fitOriginOrder[0]); i++) {
        BinParameters2D::FitOrigin origin = fitOriginOrder[i];
        auto fit = fits.find(origin);
        if (fit == fits.end())
            continue;
        Q_ASSERT(fit->second);
        QString text;
        if (parameters->hasFitLegendText(origin)) {
            text = parameters->getFitLegendText(origin);
            context.handleString(text, DisplayDynamicContext::String_GraphLegend, changed);
        } else {
            if (fit->second->fit) {
                ModelLegendParameters lp;
                lp.setConfidence(0.95);
                ModelLegendEntry legend(fit->second->fit->legend(lp));
                if (add != NULL) {
                    text = tr(" %1", "fit sub label format").arg(legend.getLine());
                } else {
                    if (!legendText.isEmpty()) {
                        text = tr("%1: %2", "fit label format").arg(legendText, legend.getLine());
                    } else {
                        text = legend.getLine();
                    }
                }
            } else {
                if (add != NULL) {
                    text = tr(" Fit", "fit not done yet label");
                } else {
                    if (!legendText.isEmpty()) {
                        if (!parameters->getDrawBoxes()) {
                            text = legendText;
                        } else {
                            text = tr("%1: Fit", "fit not done format").arg(legendText);
                        }
                    } else {
                        text = tr("Fit", "fit not done first");
                    }
                }
            }
        }
        if (!text.isEmpty()) {
            result.emplace_back(std::make_shared<GraphLegendItemBinsFit2D>(this, origin, text,
                                                                           parameters->getFitColor(
                                                                                   origin),
                                                                           parameters->getFitLegendFont(
                                                                                   origin),
                                                                           parameters->getFitLineWidth(
                                                                                   origin),
                                                                           parameters->getFitLineStyle(
                                                                                   origin),
                                                                           TraceSymbol(), false));
        }
    }

    return result;
}

BinDrawPrecursor::BinDrawPrecursor(std::vector<Bin2DPaintPoint> setPoints,
                                   const AxisTransformer &setX,
                                   const AxisTransformer &setY,
                                   BinParameters2D *params,
                                   quintptr setTracePtr) : points(std::move(setPoints)),
                                                           tX(setX),
                                                           tY(setY),
                                                           parameters(params),
                                                           tracePtr(setTracePtr)
{ }

BinDrawPrecursor::BinDrawPrecursor() : parameters(nullptr), tracePtr(0)
{ }

BinDrawPrecursor::BinDrawPrecursor(const BinDrawPrecursor &other) = default;

BinDrawPrecursor &BinDrawPrecursor::operator=(const BinDrawPrecursor &other) = default;

BinDrawPrecursor::BinDrawPrecursor(BinDrawPrecursor &&other) = default;

BinDrawPrecursor &BinDrawPrecursor::operator=(BinDrawPrecursor &&other) = default;

static void binToScreen(AxisTransformer &tX,
                        AxisTransformer &tY,
                        const Bin2DPaintPoint &bin,
                        bool vertical,
                        double &start,
                        double &end,
                        double *center = NULL)
{
    start = FP::undefined();
    end = FP::undefined();

    double tmp;
    if (center != NULL) {
        *center = FP::undefined();
    } else {
        center = &tmp;
    }

    if (vertical) {
        *center = tX.toScreen(bin.paintCenter, false);
        if (FP::defined(bin.paintCenter) && FP::defined(bin.paintWidth) && bin.paintWidth >= 0.0) {
            start = tX.toScreen(bin.paintCenter - bin.paintWidth * 0.5, false);
            end = tX.toScreen(bin.paintCenter + bin.paintWidth * 0.5, false);
        }
    } else {
        *center = tY.toScreen(bin.paintCenter, false);
        if (FP::defined(bin.paintCenter) && FP::defined(bin.paintWidth) && bin.paintWidth >= 0.0) {
            start = tY.toScreen(bin.paintCenter - bin.paintWidth * 0.5, false);
            end = tY.toScreen(bin.paintCenter + bin.paintWidth * 0.5, false);
        }
    }
    if (bin.paintWidth < 0.0) {
        if (FP::defined(*center)) {
            start = *center + bin.paintWidth * 0.5;
            end = *center - bin.paintWidth * 0.5;
        }
    }

    if (!FP::defined(start))
        start = *center;
    if (!FP::defined(end))
        end = *center;
}

std::vector<Bin2DPaintPoint> BinHandler2D::getSizedBins(AxisTransformer &x,
                                                        AxisTransformer &y,
                                                        bool vertical) const
{
    using std::swap;

    const auto &points = getPoints();
    std::vector<Bin2DPaintPoint> output;
    output.reserve(points.size());
    switch (parameters->getDrawMode()) {
    case BinParameters2D::Draw_WidthScale:
        if (parameters->getDrawConstant() != 1.0) {
            for (const auto &it : points) {
                output.emplace_back(it);
                if (!FP::defined(output.back().paintWidth))
                    continue;
                output.back().paintWidth *= parameters->getDrawConstant();
            }
        }
        break;
    case BinParameters2D::Draw_WidthConstantScale: {
        std::vector<double> widths;
        widths.reserve(points.size());
        for (const auto &it : points) {
            output.emplace_back(it);

            double start;
            double end;
            binToScreen(x, y, output.back(), vertical, start, end);
            if (!FP::defined(start) || !FP::defined(end))
                continue;
            if (start > end)
                swap(start, end);
            widths.emplace_back(end - start);
        }
        double multiplier = -parameters->getDrawConstant();
        if (widths.empty()) {
            multiplier = -multiplier;
            for (const auto &it : points) {
                if (!FP::defined(it.width[0]))
                    continue;
                widths.emplace_back(it.width[0]);
            }
        }
        if (widths.empty())
            break;
        std::sort(widths.begin(), widths.end());
        double setWidth = Statistics::quantile(widths, 0.5);
        if (!FP::defined(setWidth))
            break;
        setWidth *= multiplier;
        if (setWidth == widths.front() && setWidth == widths[widths.size() - 1])
            break;
        for (auto &it : output) {
            it.paintWidth = setWidth;
        }
        break;
    }
    case BinParameters2D::Draw_WidthFixedReal:
        for (const auto &it : points) {
            output.emplace_back(it);
            output.back().paintWidth = parameters->getDrawConstant();
        }
        break;
    case BinParameters2D::Draw_WidthFixedScreen:
        for (const auto &it : points) {
            output.emplace_back(it);
            output.back().paintWidth = -parameters->getDrawConstant();
        }
        break;
    }
    return output;
}

/**
 * Get the list of points that should have their middles extended.
 * <br>
 * The default implementation returns all points
 * 
 * @param input     the input points
 * @return          the points to be drawn with extended middles
 */
std::vector<BinPoint<1, 1>> BinHandler2D::getMiddleExtendPoints(const std::vector<
        BinPoint<1, 1>> &input) const
{ return input; }

/**
 * Create draw items for the trace.
 * 
 * @param x             the final X transformer
 * @param y             the final Y transformer
 * @param precursors    the output box precursors
 * @param vertical      if true then generate vertical boxes
 * @param xBaseline     the baseline of the X axis
 * @param yBaseline     the baseline of the Y axis
 */
std::vector<std::unique_ptr<Graph2DDrawPrimitive>> BinHandler2D::createDraw(AxisTransformer &x,
                                                                            AxisTransformer &y,
                                                                            std::vector<
                                                                                    BinDrawPrecursor> &precursors,
                                                                            bool vertical,
                                                                            double xBaseline,
                                                                            double yBaseline) const
{
    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> result;

    for (size_t i = 0; i < sizeof(fitOriginOrder) / sizeof(fitOriginOrder[0]); i++) {
        BinParameters2D::FitOrigin origin = fitOriginOrder[i];
        auto fit = fits.find(origin);
        if (fit == fits.end())
            continue;
        Q_ASSERT(fit->second);
        if (!fit->second->fit)
            continue;
        if (vertical) {
            result.emplace_back(new GraphPainter2DFitX(fit->second->fit, x, y,
                                                       parameters->getFitLineWidth(origin),
                                                       parameters->getFitLineStyle(origin),
                                                       parameters->getFitColor(origin), 1.0,
                                                       ((quintptr) this) + origin,
                                                       parameters->getFitDrawPriority(origin)));
        } else {
            result.emplace_back(new GraphPainter2DFitY(fit->second->fit, x, y,
                                                       parameters->getFitLineWidth(origin),
                                                       parameters->getFitLineStyle(origin),
                                                       parameters->getFitColor(origin), 1.0,
                                                       ((quintptr) this) + origin,
                                                       parameters->getFitDrawPriority(origin)));
        }
    }

    if (!FP::defined(getLimits().min[1]) || !FP::defined(getLimits().min[0]))
        return result;

    auto points = getSizedBins(x, y, vertical);
    if (points.empty())
        return result;

    if (parameters->getDrawBoxes() &&
            (parameters->getOutlineEnable() ||
                    parameters->getWhiskerEnable() ||
                    parameters->getMiddleEnable() ||
                    (parameters->getFillEnable() && parameters->getFillColor().alpha() != 0))) {
        if (parameters->getRelocationEnable()) {
            precursors.emplace_back(BinDrawPrecursor(points, x, y, parameters, (quintptr) this));
        } else {
            result.emplace_back(new GraphPainter2DBin(points, x, y, parameters->getFillEnable()
                                                                    ? parameters->getFillColor()
                                                                    : QColor(),
                                                      parameters->getWhiskerLineWidth(),
                                                      parameters->getWhiskerEnable()
                                                      ? parameters->getWhiskerLineStyle()
                                                      : Qt::NoPen, parameters->getWhiskerColor(),
                                                      parameters->getWhiskerCapWidth(),
                                                      parameters->getOutlineWidth(),
                                                      parameters->getOutlineEnable()
                                                      ? parameters->getOutlineStyle() : Qt::NoPen,
                                                      parameters->getOutlineColor(),
                                                      parameters->getMiddleLineWidth(),
                                                      parameters->getMiddleEnable()
                                                      ? parameters->getMiddleLineStyle()
                                                      : Qt::NoPen, parameters->getMiddleColor(),
                                                      vertical, (quintptr) this,
                                                      parameters->getDrawPriority()));
        }
    }

    if (!FP::defined(y.getScreenMin()) ||
            !FP::defined(y.getScreenMax()) ||
            !FP::defined(x.getScreenMin()) ||
            !FP::defined(x.getScreenMax()))
        return result;

    if (parameters->getMiddleExtendEnable()) {
        qreal startI;
        qreal endI;
        if (vertical) {
            if (x.getScreenMin() < x.getScreenMax()) {
                startI = x.getScreenMin();
                endI = x.getScreenMax();
            } else {
                startI = x.getScreenMax();
                endI = x.getScreenMin();
            }
            if (FP::defined(yBaseline)) {
                startI = qMin(startI, yBaseline);
                endI = qMax(endI, yBaseline);
            }
        } else {
            if (y.getScreenMin() < y.getScreenMax()) {
                startI = y.getScreenMin();
                endI = y.getScreenMax();
            } else {
                startI = y.getScreenMax();
                endI = y.getScreenMin();
            }
            if (FP::defined(xBaseline)) {
                startI = qMin(startI, xBaseline);
                endI = qMax(endI, xBaseline);
            }
        }

        startI += 1.0;
        endI -= 1.0;
        if (startI >= endI)
            return result;

        auto middlePoints = getMiddleExtendPoints(getPoints());
        for (const auto &it : middlePoints) {
            if (vertical) {
                double v = y.toScreen(it.middle[0], false);
                if (!FP::defined(v))
                    continue;
                if (y.getScreenMin() < y.getScreenMax()) {
                    if (v <= y.getScreenMin())
                        continue;
                    if (v >= y.getScreenMax())
                        continue;
                } else {
                    if (v <= y.getScreenMax())
                        continue;
                    if (v >= y.getScreenMin())
                        continue;
                }

                result.emplace_back(
                        new GraphPainter2DSimpleLine(QPointF(startI, v), QPointF(endI, v),
                                                     parameters->getMiddleExtendLineWidth(),
                                                     parameters->getMiddleExtendLineStyle(),
                                                     parameters->getMiddleExtendColor(),
                                                     parameters->getMiddleExtendDrawPriority()));
            } else {
                double v = x.toScreen(it.middle[0], false);
                if (!FP::defined(v))
                    continue;
                if (x.getScreenMin() < x.getScreenMax()) {
                    if (v <= x.getScreenMin())
                        continue;
                    if (v >= x.getScreenMax())
                        continue;
                } else {
                    if (v <= x.getScreenMax())
                        continue;
                    if (v >= x.getScreenMin())
                        continue;
                }

                result.emplace_back(
                        new GraphPainter2DSimpleLine(QPointF(v, startI), QPointF(v, endI),
                                                     parameters->getMiddleExtendLineWidth(),
                                                     parameters->getMiddleExtendLineStyle(),
                                                     parameters->getMiddleExtendColor(),
                                                     parameters->getMiddleExtendDrawPriority()));
            }
        }
    }

    return std::move(result);
}

/**
 * Get an colors in use and the units for any that are requested to be
 * assigned.
 * 
 * @param inputUnits        the inputs that went into this trace
 * @param requestColors     the output list of units to request colors for
 * @param claimedColors     the output list of already claimed colors
 */
void BinHandler2D::getColors(const std::vector<SequenceName::Set> &inputUnits,
                             std::vector<TraceUtilities::ColorRequest> &requestColors,
                             std::deque<QColor> &claimedColors) const
{
    std::size_t wantColors = 0;
    if (parameters->getDrawBoxes() &&
            FP::defined(getLimits().min[1]) &&
            FP::defined(getLimits().min[0]) &&
            (parameters->getFillEnable() ||
                    parameters->getOutlineEnable() ||
                    parameters->getMiddleEnable() ||
                    parameters->getWhiskerEnable())) {
        if (parameters->getFillEnable() && parameters->hasFillColor())
            claimedColors.emplace_back(parameters->getFillColor());
        else if (parameters->getOutlineEnable() && parameters->hasOutlineColor())
            claimedColors.emplace_back(parameters->getOutlineColor());
        else if (parameters->getMiddleEnable() && parameters->hasMiddleColor())
            claimedColors.emplace_back(parameters->getMiddleColor());
        else if (parameters->getWhiskerEnable() && parameters->hasWhiskerColor())
            claimedColors.emplace_back(parameters->getWhiskerColor());
        else
            wantColors++;
    }
    for (const auto &it : fits) {
        if (parameters->hasFitColor(it.first))
            claimedColors.emplace_back(parameters->getFitColor(it.first));
        else
            wantColors++;
    }
    if (wantColors == 0)
        return;

    for (std::ptrdiff_t i = inputUnits.size() - 1; i >= 0; i--) {
        if (inputUnits[i].empty())
            continue;
        TraceUtilities::ColorRequest req;
        req.unit = *(inputUnits[i].begin());
        for (int j = 0; j < 2; j++) {
            req.wavelength = getWavelength(j);
            if (FP::defined(req.wavelength))
                break;
        }
        requestColors.emplace_back(std::move(req));
        wantColors--;
        break;
    }
    for (std::size_t i = 0; i < wantColors; i++) {
        requestColors.emplace_back();
    }
}

/**
 * Claim the colors assigned to the units returned from 
 * getColors( const QVector<QSet<SequenceName> > &, QList<TraceUtilities::ColorRequest> &,
 * QList<QColor> & ).  This should remove the colors used from the front of the 
 * list.
 * 
 * @param colors        the input and output list of assigned colors
 */
void BinHandler2D::claimColors(std::deque<QColor> &colors) const
{
    if (parameters->getDrawBoxes() &&
            !colors.empty() &&
            FP::defined(getLimits().min[1]) &&
            FP::defined(getLimits().min[0])) {
        if (parameters->getFillEnable() && !parameters->hasFillColor()) {
            QColor color(colors.front());
            colors.pop_front();
            if (color == QColor(0, 0, 0)) {
                /* Reinterpret solid black fill with a transparent fill. */
                parameters->setFillColor(QColor(255, 255, 255, 0));
            } else {
                parameters->setFillColor(color);
            }
        } else if ((parameters->getOutlineEnable() && !parameters->hasOutlineColor()) ||
                (parameters->getMiddleEnable() && !parameters->hasMiddleColor()) ||
                (parameters->getWhiskerEnable() && !parameters->hasWhiskerColor())) {
            QColor color(colors.front());
            colors.pop_front();
            if (parameters->getOutlineEnable() && !parameters->hasOutlineColor())
                parameters->setOutlineColor(color);
            if (parameters->getMiddleEnable() && !parameters->hasMiddleColor())
                parameters->setMiddleColor(color);
            if (parameters->getWhiskerEnable() && !parameters->hasWhiskerColor())
                parameters->setWhiskerColor(color);
        }
    }
    for (std::size_t i = 0; i < sizeof(fitOriginOrder) / sizeof(fitOriginOrder[0]); i++) {
        if (colors.empty())
            break;
        BinParameters2D::FitOrigin origin = fitOriginOrder[i];
        auto fit = fits.find(origin);
        if (fit == fits.end())
            continue;
        Q_ASSERT(fit->second);
        parameters->setFitColor(origin, colors.front());
        colors.pop_front();
    }
}

BinOutput2D::BinOutput2D(BinHandler2D *handler, const QString &title) : handler(handler),
                                                                        title(title)
{ }

BinOutput2D::~BinOutput2D() = default;

QString BinOutput2D::getTitle() const
{ return title; }

QList<QAction *> BinOutput2D::getMenuActions(QWidget *parent)
{
    if (handler.isNull())
        return {};
    QList<QAction *> result;

    QAction *act;

    if (!handler->parameters->getAsHistogram()) {
        act = new QAction(tr("Enable &Whiskers", "Context|EnableWhiskers"), this);
        act->setToolTip(tr("Enable or disable the whiskers."));
        act->setStatusTip(tr("Enable whiskers"));
        act->setWhatsThis(
                tr("This enables or disables the whiskers to the outer percentiles on the boxes."));
        act->setCheckable(true);
        act->setChecked(handler->parameters->getWhiskerEnable());
        QObject::connect(act, &QAction::toggled, this, [this](bool enable) {
            if (handler.isNull())
                return;
            static_cast<BinParameters2D *>(handler->overrideParameters())->overrideWhiskerEnable(
                    enable);
            handler->mergeOverrideParameters();
            emit changed();
        });
        result.append(act);
    }

    act = new QAction(tr("Enable F&ill", "Context|EnableFill"), this);
    act->setToolTip(tr("Enable or disable the solid background fill."));
    act->setStatusTip(tr("Enable fill"));
    act->setWhatsThis(tr("This enables or disables the solid background fill of the bins."));
    act->setCheckable(true);
    act->setChecked(handler->parameters->getFillEnable());
    QObject::connect(act, &QAction::toggled, this, [this](bool enable) {
        if (handler.isNull())
            return;
        static_cast<BinParameters2D *>(handler->overrideParameters())->overrideFillEnable(enable);
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    act = new QAction(tr("&Color", "Context|SetBinsColor"), this);
    act->setToolTip(tr("Change the color of the bin outline."));
    act->setStatusTip(tr("Set the bin color"));
    act->setWhatsThis(tr("Set the color of the bin outline."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        QColorDialog dialog(handler->parameters->getOutlineColor(), parent);
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(tr("Set Bin Outline Color"));
        dialog.setOption(QColorDialog::ShowAlphaChannel);
        if (dialog.exec() != QDialog::Accepted)
            return;
        static_cast<BinParameters2D *>(handler->overrideParameters())->overrideOutlineColor(
                dialog.currentColor());
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    act = new QAction(tr("&Fill", "Context|SetBinsFillColor"), this);
    act->setToolTip(tr("Change the color of the bin fill."));
    act->setStatusTip(tr("Set the bin color"));
    act->setWhatsThis(tr("Set the color of the bin fill background."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        QColorDialog dialog(handler->parameters->getFillColor(), parent);
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(tr("Set Bin Fill Color"));
        dialog.setOption(QColorDialog::ShowAlphaChannel);
        if (dialog.exec() != QDialog::Accepted)
            return;
        static_cast<BinParameters2D *>(handler->overrideParameters())->overrideFillColor(
                dialog.currentColor());
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    return result;
}

void BinOutput2D::setLegendAlwaysHide(bool enable)
{
    if (handler.isNull())
        return;
    if (!handler->setLegendAlwaysHide(enable))
        return;
    emit changed();
}

/**
 * The output controllers (if any) for the handler.
 * 
 * @return a list of output controllers
 */
std::vector<std::shared_ptr<DisplayOutput>> BinHandler2D::getOutputs(const std::vector<
        SequenceName::Set> &inputUnits,
                                                                     bool showStation,
                                                                     bool showArchive,
                                                                     const DisplayDynamicContext &context)
{
    if (!anyValidPoints)
        return {};
    return std::vector<std::shared_ptr<DisplayOutput>>{std::make_shared<BinOutput2D>(this,
                                                                                     getDisplayTitle(
                                                                                             inputUnits,
                                                                                             showStation,
                                                                                             showArchive,
                                                                                             context))};
}

BinSet2D::BinSet2D() = default;

BinSet2D::~BinSet2D() = default;

BinSet2D::BinSet2D(const BinSet2D &other) = default;

BinSet2D::Cloned BinSet2D::clone() const
{ return Cloned(new BinSet2D(*this)); }

namespace {
struct Bin2DSortItem {
    BinHandler2D *handler;
    std::vector<SequenceName::Set> units;

    bool operator<(const Bin2DSortItem &other) const
    {
        auto max = std::min(units.size(), other.units.size());
        if (max <= 0) return false;
        for (std::ptrdiff_t i = max - 1; i >= 0; i--) {
            const auto &a = units[i];
            const auto &b = other.units[i];
            if (a.size() == 1 && b.size() == 1) {
                const auto &ua = *a.begin();
                const auto &ub = *b.begin();
                if (ua == ub)
                    continue;
                return SequenceName::OrderDisplay()(ua, ub);
            } else if (!a.empty() && !b.empty()) {
                std::vector<SequenceName> sa(a.begin(), a.end());
                std::sort(sa.begin(), sa.end(), SequenceName::OrderLogical());
                std::vector<SequenceName> sb(b.begin(), b.end());
                std::sort(sb.begin(), sb.end(), SequenceName::OrderLogical());
                for (std::size_t j = 0, nUnits = std::min(sa.size(), sb.size()); j < nUnits; ++j) {
                    const auto &ua = sa[j];
                    const auto &ub = sb[j];
                    if (ua == ub)
                        continue;
                    return SequenceName::OrderDisplay()(ua, ub);
                }
            }
        }
        return false;
    }
};
}

/**
 * Get the handlers in a legend suitable sort order.
 * 
 * @return a list of handlers
 */
std::vector<BinHandler2D *> BinSet2D::getSortedHandlers() const
{
    std::vector<Bin2DSortItem> sort;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        auto dispatch = getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            Bin2DSortItem add;
            add.handler = static_cast<BinHandler2D *>(handlers[j].get());
            add.units = dispatch->getUnits(j);
            sort.push_back(std::move(add));
        }
    }
    std::stable_sort(sort.begin(), sort.end());
    std::vector<BinHandler2D *> result;
    result.reserve(sort.size());
    for (const auto &it : sort) {
        result.emplace_back(it.handler);
    }
    return result;
}

/**
 * Add all trace items to the legend.
 * 
 * @param legend    the target legend
 * @param context   the context to use
 * @return          true if the legend has dynamically changed (as reported by the context, not necessarily if other parts have changed)
 */
bool BinSet2D::addLegend(Legend &legend, const DisplayDynamicContext &context) const
{
    std::vector<Bin2DSortItem> sort;
    bool showStation = false;
    bool showArchive = false;
    bool changed = false;
    Data::SequenceName::Component priorStation;
    Data::SequenceName::Component priorArchive;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        auto dispatch = getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            Bin2DSortItem add;
            add.handler = static_cast<BinHandler2D *>(handlers[j].get());
            add.units = dispatch->getUnits(j);

            for (const auto &dim : add.units) {
                for (const auto &unit : dim) {
                    if (!showStation) {
                        if (priorStation.empty()) {
                            priorStation = unit.getStation();
                        } else if (priorStation != unit.getStation()) {
                            showStation = true;
                        }
                    }
                    if (!showArchive) {
                        if (priorArchive.empty()) {
                            priorArchive = unit.getArchive();
                        } else if (priorArchive != unit.getArchive()) {
                            showArchive = true;
                        }
                    }
                }
            }

            sort.emplace_back(std::move(add));
        }
    }
    std::stable_sort(sort.begin(), sort.end());
    for (const auto &it : sort) {
        legend.append(it.handler->getLegend(it.units, showStation, showArchive, context, &changed));
    }
    return changed;
}


/**
 * Get the title of a given handler.  This is used to generate strings
 * to refer to the trace for user interaction.
 * 
 * @param bins      the bins to get the title for
 * @param context   the context to use
 */
QString BinSet2D::handlerTitle(BinHandler2D *bins, const DisplayDynamicContext &context) const
{
    bool showStation = false;
    bool showArchive = false;
    Data::SequenceName::Component priorStation;
    Data::SequenceName::Component priorArchive;
    std::vector<SequenceName::Set> handlerUnits;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        auto dispatch = getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            const auto &units = dispatch->getUnits(j);
            if (static_cast<BinHandler2D *>(handlers[j].get()) == bins)
                handlerUnits = units;

            for (const auto &dim : units) {
                for (const auto &unit : dim) {
                    if (!showStation) {
                        if (priorStation.empty()) {
                            priorStation = unit.getStation();
                        } else if (priorStation != unit.getStation()) {
                            showStation = true;
                        }
                    }
                    if (!showArchive) {
                        if (priorArchive.empty()) {
                            priorArchive = unit.getArchive();
                        } else if (priorArchive != unit.getArchive()) {
                            showArchive = true;
                        }
                    }
                }
            }
        }
    }

    return bins->getDisplayTitle(handlerUnits, showStation, showArchive, context);
}

/**
 * Get the origin for a given handler.  This is used to generate additional
 * disambiguation information.
 * 
 * @param bins      the bins to get the origin for
 * @param context   the context to use
 */
QString BinSet2D::handlerOrigin(BinHandler2D *bins, const DisplayDynamicContext &context) const
{
    Q_UNUSED(context);
    return bins->getOriginText();
}

int BinSet2D::countOverlapping(std::vector<BinDrawPrecursor> &precursors,
                               bool vertical,
                               const std::vector<std::vector<Bin2DPaintPoint> > &points) const
{
    int maxOverlapping = 0;
    for (auto pointsListStart = points.begin(), pointsList = points.begin(),
            pointsListEnd = points.end(); pointsList != pointsListEnd - 1; ++pointsList) {
        auto idx = pointsList - pointsListStart;
        for (auto it = pointsList->begin(), endPoints = pointsList->end(); it != endPoints; ++it) {
            double start;
            double end;
            binToScreen(precursors[idx].tX, precursors[idx].tY, *it, vertical, start, end);
            if (!FP::defined(start) || !FP::defined(end))
                continue;

            int nOverlap = 0;
            for (auto pointsList2 = pointsList; pointsList2 != pointsListEnd; ++pointsList2) {
                auto idx2 = pointsList2 - pointsListStart;
                for (auto it2 = pointsList2->begin(), endPoints2 = pointsList2->end();
                        it2 != endPoints2;
                        ++it2) {
                    if (it2 == it)
                        continue;
                    double start2;
                    double end2;
                    binToScreen(precursors[idx2].tX, precursors[idx2].tY, *it2, vertical, start2,
                                end2);
                    if (!FP::defined(start2) || !FP::defined(end2))
                        continue;
                    if (end - 1.0 <= start2)
                        continue;
                    if (start + 1.0 >= end2)
                        continue;
                    nOverlap++;
                }
            }

            maxOverlapping = std::max(nOverlap, maxOverlapping);
        }
    }

    return maxOverlapping;
}

void BinSet2D::layoutPrecursors(std::vector<BinDrawPrecursor> &precursors,
                                bool vertical,
                                std::vector<std::vector<Bin2DPaintPoint>> &points) const
{
    points.clear();
    if (precursors.empty())
        return;
    if (precursors.size() == 1) {
        points.emplace_back(precursors.front().points);
        return;
    }

    double scale = 1.0;
    int priorMaxOverlapping = 0;
    bool haveRelativeScaled = false;
    points.reserve(precursors.size());
    std::vector<std::vector<std::uint_fast8_t>>
            processed(precursors.size(), std::vector<std::uint_fast8_t>());
    int itter;
    for (itter = 0; itter < 500; itter++) {
        points.clear();

        for (auto it = precursors.begin(); it != precursors.end(); ++it) {
            points.emplace_back(it->points);
            processed[it - precursors.begin()] =
                    std::vector<std::uint_fast8_t>(it->points.size(), 0);

            if (scale != 1.0) {
                for (auto &p : points.back()) {
                    if (FP::defined(p.paintWidth))
                        p.paintWidth *= scale;
                }
            }
        }

        auto maxOverlapping = countOverlapping(precursors, vertical, points);
        if (maxOverlapping <= 0)
            break;

        if (itter == 0) {
            scale = 1.0 / ((double) (maxOverlapping + 1));
            priorMaxOverlapping = maxOverlapping;
            continue;
        }
        if (priorMaxOverlapping != maxOverlapping) {
            if (!haveRelativeScaled && maxOverlapping > priorMaxOverlapping) {
                scale = 1.0 / ((double) (maxOverlapping + 1));
                priorMaxOverlapping = maxOverlapping;
                continue;
            } else {
                haveRelativeScaled = true;
            }
            if (itter > 1) {
                scale *= 0.1;
                priorMaxOverlapping = maxOverlapping;
                continue;
            }
        }

        for (std::size_t idx = 0, nPointsList = points.size(); idx < nPointsList; idx++) {
            for (std::size_t idxPoint = 0, nPoints = points[idx].size();
                    idxPoint < nPoints;
                    idxPoint++) {
                if (processed[idx][idxPoint])
                    continue;
                double start;
                double end;
                binToScreen(precursors[idx].tX, precursors[idx].tY, points[idx][idxPoint], vertical,
                            start, end);
                if (!FP::defined(start) || !FP::defined(end))
                    continue;
                if (start > end)
                    qSwap(start, end);

                std::vector<std::vector<std::size_t>>
                        overlapping(nPointsList, std::vector<std::size_t>());
                std::vector<std::vector<double>> widths(nPointsList, std::vector<double>());
                double totalWidth = 0;
                double sumCenter = 0;
                std::size_t nCenter = 0;
                for (std::size_t idx2 = 0; idx2 < nPointsList; idx2++) {
                    for (std::size_t idxPoint2 = 0, nPoints2 = points[idx2].size();
                            idxPoint2 < nPoints2;
                            idxPoint2++) {
                        if (processed[idx2][idxPoint2])
                            continue;
                        double start2;
                        double end2;
                        double center;
                        binToScreen(precursors[idx2].tX, precursors[idx2].tY,
                                    points[idx2][idxPoint2], vertical, start2, end2, &center);
                        if (!FP::defined(start2) || !FP::defined(end2))
                            continue;
                        if (start2 > end2)
                            qSwap(start2, end2);
                        if (idx2 != idx || idxPoint2 != idxPoint) {
                            if (end <= start2)
                                continue;
                            if (start >= end2)
                                continue;
                        }
                        if (!FP::defined(center))
                            continue;
                        sumCenter += center;
                        nCenter++;
                        overlapping[idx2].emplace_back(idxPoint2);
                        widths[idx2].emplace_back(end2 - start2);
                        processed[idx2][idxPoint2] = true;
                        totalWidth += end2 - start2;
                    }
                }
                if (nCenter <= 1)
                    continue;

                double offset = sumCenter / ((double) nCenter) - (totalWidth * 0.5);
                for (std::size_t idx2 = 0; idx2 < nPointsList; idx2++) {
                    const auto &indices = overlapping[idx2];
                    for (auto sidx = indices.begin(), it = sidx, eidx = indices.end();
                            it != eidx;
                            ++it) {
                        double width = widths[idx2][it - sidx];
                        double center = offset + width * 0.5;
                        if (vertical) {
                            points[idx2][*it].paintCenter =
                                    precursors[idx2].tX.toReal(center, false);
                        } else {
                            points[idx2][*it].paintCenter =
                                    precursors[idx2].tX.toReal(center, false);
                        }
                        offset += width;
                    }
                }
            }
        }

        if (countOverlapping(precursors, vertical, points) <= 0)
            break;
    }
    if (itter >= 500) {
        qCDebug(log_graphing_bin2d) << "Bin overlap avoidance diverged at scale" << scale << "with"
                                    << priorMaxOverlapping << "overlapping bins";
    }
}

/**
 * Combine the precursors generated by the individual draw handlers
 * into actual draw primitives.
 * 
 * @param precursors    the precursors generated by the individual handlers
 * @param vertical      if true then generate vertical bins
 */
std::vector<std::unique_ptr<Graph2DDrawPrimitive>> BinSet2D::combinePrecursors(std::vector<
        BinDrawPrecursor> &precursors, bool vertical) const
{
    std::vector<std::vector<Bin2DPaintPoint>> points;
    layoutPrecursors(precursors, vertical, points);

    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> result;

    for (auto start = precursors.begin(), end = precursors.end(), it = precursors.begin();
            it != end;
            ++it) {
        auto idx = it - start;
        result.emplace_back(new GraphPainter2DBin(points[idx], it->tX, it->tY,
                                                  it->parameters->getFillEnable() ? it->parameters
                                                                                      ->getFillColor()
                                                                                  : QColor(),
                                                  it->parameters->getWhiskerLineWidth(),
                                                  it->parameters->getWhiskerEnable()
                                                  ? it->parameters->getWhiskerLineStyle()
                                                  : Qt::NoPen, it->parameters->getWhiskerColor(),
                                                  it->parameters->getWhiskerCapWidth(),
                                                  it->parameters->getOutlineWidth(),
                                                  it->parameters->getOutlineEnable()
                                                  ? it->parameters->getOutlineStyle() : Qt::NoPen,
                                                  it->parameters->getOutlineColor(),
                                                  it->parameters->getMiddleLineWidth(),
                                                  it->parameters->getMiddleEnable() ? it->parameters
                                                                                        ->getMiddleLineStyle()
                                                                                    : Qt::NoPen,
                                                  it->parameters->getMiddleColor(), vertical,
                                                  (quintptr) it->tracePtr,
                                                  it->parameters->getDrawPriority()));
    }

    return std::move(result);
}

/**
 * Get an colors in use and the units for any that are requested to be
 * assigned.
 * 
 * @param requestColors     the output list of units to request colors for
 * @param claimedColors     the output list of already claimed colors
 */
void BinSet2D::getColors(std::vector<TraceUtilities::ColorRequest> &requestColors,
                         std::deque<QColor> &claimedColors) const
{
    std::vector<Bin2DSortItem> sort;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        const TraceDispatch *dispatch = getGroupDispatch(i);
        for (int j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            Bin2DSortItem add;
            add.handler = static_cast<BinHandler2D *>(handlers[j].get());
            add.units = dispatch->getUnits(j);
            sort.push_back(std::move(add));
        }
    }
    std::stable_sort(sort.begin(), sort.end());
    for (const auto &it : sort) {
        it.handler->getColors(it.units, requestColors, claimedColors);
    }
}

/**
 * Claim the colors assigned to the units returned from 
 * getColors( std::vector<TraceUtilities::ColorRequest> &, std::deque<QColor> & ).  This should
 * remove the colors used from the front of the list.
 * 
 * @param colors        the input and output list of assigned colors
 */
void BinSet2D::claimColors(std::deque<QColor> &colors) const
{
    std::vector<Bin2DSortItem> sort;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        auto dispatch = getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            Bin2DSortItem add;
            add.handler = static_cast<BinHandler2D *>(handlers[j].get());
            add.units = dispatch->getUnits(j);
            sort.emplace_back(std::move(add));
        }
    }
    std::stable_sort(sort.begin(), sort.end());
    for (const auto &it : sort) {
        it.handler->claimColors(colors);
    }
}

/**
 * The output controllers (if any) for the set of bins.
 * 
 * @param context   the context to use
 * @return          the list of output controllers
 */
std::vector<
        std::shared_ptr<DisplayOutput>> BinSet2D::getOutputs(const DisplayDynamicContext &context)
{
    std::vector<Bin2DSortItem> sort;
    bool showStation = false;
    bool showArchive = false;
    Data::SequenceName::Component priorStation;
    Data::SequenceName::Component priorArchive;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        auto dispatch = getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            Bin2DSortItem add;
            add.handler = static_cast<BinHandler2D *>(handlers[j].get());
            add.units = dispatch->getUnits(j);

            for (const auto &dim : add.units) {
                for (const auto &unit : dim) {
                    if (!showStation) {
                        if (priorStation.empty()) {
                            priorStation = unit.getStation();
                        } else if (priorStation != unit.getStation()) {
                            showStation = true;
                        }
                    }
                    if (!showArchive) {
                        if (priorArchive.empty()) {
                            priorArchive = unit.getArchive();
                        } else if (priorArchive != unit.getArchive()) {
                            showArchive = true;
                        }
                    }
                }
            }

            sort.push_back(add);
        }
    }
    std::stable_sort(sort.begin(), sort.end());

    std::vector<std::shared_ptr<DisplayOutput>> result;
    for (const auto &it : sort) {
        Util::append(it.handler->getOutputs(it.units, showStation, showArchive, context), result);
    }
    return result;
}

std::unique_ptr<TraceParameters> BinSet2D::parseParameters(const Variant::Read &value) const
{ return std::unique_ptr<TraceParameters>(new BinParameters2D(value)); }

BinSet2D::CreatedHandler BinSet2D::createHandler(std::unique_ptr<
        TraceParameters> &&parameters) const
{
    return CreatedHandler(new BinHandler2D(std::unique_ptr<BinParameters2D>(
            static_cast<BinParameters2D *>(parameters.release()))));
}

namespace Internal {

Graph2DLegendModificationBins::Graph2DLegendModificationBins(Graph2D *graph,
                                                             const QPointer<
                                                                     BinHandler2D> &setHandler,
                                                             const QString &setLegendText)
        : Graph2DLegendModification(graph), handler(setHandler), legendText(setLegendText)
{ }

Graph2DLegendModificationBins::~Graph2DLegendModificationBins() = default;

QList<QAction *> Graph2DLegendModificationBins::getMenuActions(QWidget *parent)
{
    QList<QAction *> result(Graph2DLegendModification::getMenuActions(parent));
    if (handler.isNull())
        return result;

    QAction *act;

    act = new QAction(tr("Set Bins &Title", "Context|SetBinsTitle"), this);
    act->setToolTip(tr("Change the main bins title."));
    act->setStatusTip(tr("Set the bins title"));
    act->setWhatsThis(tr("Change or disable the main title for the bins in the legend."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        bool ok = false;
        auto title =
                QInputDialog::getText(parent, tr("Set Trace Title", "Context|TraceTitleDialog"),
                                      tr("Title:", "Context|TraceTitleLabel"), QLineEdit::Normal,
                                      legendText, &ok);
        if (!ok)
            return;
        static_cast<BinParameters2D *>(handler->overrideParameters())->overrideLegendText(title);
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    act = new QAction(tr("Set Bins &Color", "Context|SetBinsColor"), this);
    act->setToolTip(tr("Change the color of the bin outline."));
    act->setStatusTip(tr("Set the bin color"));
    act->setWhatsThis(tr("Set the color of the bin outline."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        QColorDialog dialog(handler->parameters->getOutlineColor(), parent);
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(tr("Set Bin Outline Color"));
        dialog.setOption(QColorDialog::ShowAlphaChannel);
        if (dialog.exec() != QDialog::Accepted)
            return;
        static_cast<BinParameters2D *>(handler->overrideParameters())->overrideOutlineColor(
                dialog.currentColor());
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    act = new QAction(tr("Set Bins &Fill", "Context|SetBinsFillColor"), this);
    act->setToolTip(tr("Change the color of the bin fill."));
    act->setStatusTip(tr("Set the bin color"));
    act->setWhatsThis(tr("Set the color of the bin fill background."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        QColorDialog dialog(handler->parameters->getFillColor(), parent);
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(tr("Set Bin Fill Color"));
        dialog.setOption(QColorDialog::ShowAlphaChannel);
        if (dialog.exec() != QDialog::Accepted)
            return;
        static_cast<BinParameters2D *>(handler->overrideParameters())->overrideFillColor(
                dialog.currentColor());
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    act = new QAction(tr("Legend Entry &Font", "Context|SetBinsFont"), this);
    act->setToolTip(tr("Change the bins legend entry font."));
    act->setStatusTip(tr("Set the bins font"));
    act->setWhatsThis(tr("Set the font used for the selected legend entry text."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        QFontDialog dialog(handler->parameters->getLegendFont(), parent);
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(tr("Set Bins Legend Font"));
        if (dialog.exec() != QDialog::Accepted)
            return;
        static_cast<BinParameters2D *>(handler->overrideParameters())->overrideLegendFont(
                dialog.selectedFont());
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    return result;
}

Graph2DLegendModificationBinsFit::Graph2DLegendModificationBinsFit(Graph2D *graph,
                                                                   const QPointer<
                                                                           BinHandler2D> &setHandler,
                                                                   BinParameters2D::FitOrigin setOrigin,
                                                                   const QString &setLegendText)
        : Graph2DLegendModification(graph),
          handler(setHandler),
          origin(setOrigin),
          legendText(setLegendText)
{ }

Graph2DLegendModificationBinsFit::~Graph2DLegendModificationBinsFit() = default;

QList<QAction *> Graph2DLegendModificationBinsFit::getMenuActions(QWidget *parent)
{
    QList<QAction *> result(Graph2DLegendModification::getMenuActions(parent));
    if (handler.isNull())
        return result;

    QAction *act;

    act = new QAction(tr("Set &Fit Title", "Context|SetTraceFitTitle"), this);
    act->setToolTip(tr("Change the trace fit title."));
    act->setStatusTip(tr("Set the trace fit title"));
    act->setWhatsThis(tr("Change or disable the trace fit title in the legend."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        bool ok = false;
        auto title =
                QInputDialog::getText(parent, tr("Set Fit Title", "Context|TraceFitTitleDialog"),
                                      tr("Title:", "Context|TraceFitTitleLabel"), QLineEdit::Normal,
                                      legendText, &ok);
        if (!ok)
            return;
        static_cast<BinParameters2D *>(handler->overrideParameters())->overrideFitLegendText(origin,
                                                                                             title);
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    act = new QAction(tr("Fit Legend Fo&nt", "Context|SetBinFitFont"), this);
    act->setToolTip(tr("Change the fit legend entry font."));
    act->setStatusTip(tr("Set the fit font"));
    act->setWhatsThis(tr("Set the font used for the selected legend entry fit text."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        QFontDialog dialog(handler->parameters->getFitLegendFont(origin), parent);
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(tr("Set Fit Legend Font"));
        if (dialog.exec() != QDialog::Accepted)
            return;
        static_cast<BinParameters2D *>(handler->overrideParameters())->overrideFitLegendFont(origin,
                                                                                             dialog.selectedFont());
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    act = new QAction(tr("Fit C&olor", "Context|SetBinFitColor"), this);
    act->setToolTip(tr("Change the color of the fit."));
    act->setStatusTip(tr("Set the fit color"));
    act->setWhatsThis(tr("Set the color of the fit."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        QColorDialog dialog(handler->parameters->getFitColor(origin), parent);
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(tr("Set Fit Color"));
        dialog.setOption(QColorDialog::ShowAlphaChannel);
        if (dialog.exec() != QDialog::Accepted)
            return;
        static_cast<BinParameters2D *>(handler->overrideParameters())->overrideFitColor(origin,
                                                                                        dialog.currentColor());
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    act = new QAction(tr("Fit Line w&idth", "Context|SetFitWidth"), this);
    act->setToolTip(tr("Change the width of the fit line."));
    act->setStatusTip(tr("Set the fit width"));
    act->setWhatsThis(tr("Set the width of the fit line of the bin anchor."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        bool ok = false;
        auto width = QInputDialog::getDouble(parent, tr("Set Fit Line Width"),
                                             tr("Width (0 = hairline)"),
                                             handler->parameters->getFitLineWidth(origin), 0, 1000,
                                             0, &ok);
        if (!ok || !FP::defined(width))
            return;
        static_cast<BinParameters2D *>(handler->overrideParameters())->overrideFitLineWidth(origin,
                                                                                            width);
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    {
        auto menu = Display::selectPenStyle(handler->parameters->getFitLineStyle(origin),
                                            [this](Qt::PenStyle style) {
                                                if (handler.isNull())
                                                    return;
                                                static_cast<BinParameters2D *>(handler->overrideParameters())
                                                        ->overrideFitLineStyle(origin, style);
                                                handler->mergeOverrideParameters();
                                                emit changed();
                                            }, this);
        menu->setTitle(tr("Fit Sty&le", "Context|SetFitPenStyle"));
        result.append(menu->menuAction());
    }

    return result;
}

}

}
}
