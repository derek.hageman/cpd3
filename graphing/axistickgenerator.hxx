/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGAXISTICKGENERATOR_H
#define CPD3GRAPHINGAXISTICKGENERATOR_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QSettings>

#include "core/timeutils.hxx"
#include "graphing/graphing.hxx"

namespace CPD3 {
namespace Graphing {

/**
 * Provides the general interface for axis tick generation algorithms.
 */
class CPD3GRAPHING_EXPORT AxisTickGenerator : public QObject {
Q_OBJECT

public:
    /** 
     * A division tick.
     */
    class CPD3GRAPHING_EXPORT Tick {
    public:
        double point;
        QString label;
        QString secondaryLabel;
        QString blockLabel;

        Tick();

        Tick(const Tick &other);

        Tick &operator=(const Tick &other);

        Tick(Tick &&other);

        Tick &operator=(Tick &&other);

        Tick(double setPoint,
             const QString &setLabel,
             const QString &setSecondaryLabel = QString(),
             const QString &setBlockLabel = QString());

        inline double getPoint() const
        { return point; }

        inline QString getLabel() const
        { return label; }

        inline QString getSecondaryLabel() const
        { return secondaryLabel; }

        inline QString getBlockLabel() const
        { return blockLabel; }
    };

    /**
     * Represents the flags that can be used in the generation algorithm to
     * inform it more about the specific axis it's generating for.
     */
    enum GenerateFlags {
        /**
         * Include the minimum bound if it appears as one of the ticks.
         */
        IncludeMin = 0x1,
        /**
         * Include the maximum bound if it appears as one of the ticks.
         */
        IncludeMax = 0x2,
        /**
         * Include ticks until the first one before the minimum.  This can
         * cause ticks to be generated outside of the selected range.
         */
        IncludeBeforeMin = 0x4,
        /**
         * Include ticks until the first one after the maximum.  This can
         * cause ticks to be generated outside of the selected range.
         */
        IncludeAfterMax = 0x8,
        /**
         * Indicates that this tick should have special first level handling.
         * This is used mostly for logical numeric ticks, that actually
         * decide the alignment on the fist level, and subsequent levels are
         * always a fixed number of ticks.
         */
        FirstLevelHandling = 0x10,

        MinorTick = 0,
        MajorTick =
        IncludeMin | IncludeMax | IncludeBeforeMin | IncludeAfterMax | FirstLevelHandling,

        DefaultFlags = MajorTick,
    };

    AxisTickGenerator(QObject *parent = 0);

    virtual ~AxisTickGenerator();

    /**
     * Generate the ticks between the given minimum and maximum values.
     * 
     * @param start     the start
     * @param end       the end
     * @param flags     the flags the generate with
     * @return          a list of division ticks
     */
    virtual std::vector<Tick> generate(double min, double max, int flags = DefaultFlags) const = 0;

    /**
     * Generate labels for the given ticks.  This attempts to generate labels
     * of a fixed format that describes the values in the ticks.
     * 
     * @param ticks     the input and output ticks
     */
    static void generateLabels(std::vector<Tick> &ticks);
};

/**
 * Divides the axis on logical time units (e.x. days or hours).  The axis
 * must be in epoch base time units.  The label format is determined by
 * the application settings.
 */
class CPD3GRAPHING_EXPORT AxisTickGeneratorTime : public AxisTickGenerator {
Q_OBJECT

    QSettings *settings;
    QString timeFormat;

    static const double compareEpsilon;

    AxisTickGenerator::Tick makeTick(double t, Time::LogicalTimeUnit unit) const;

    std::vector<AxisTickGenerator::Tick> runLogical(double min,
                                                    double max,
                                                    int flags,
                                                    double start,
                                                    Time::LogicalTimeUnit unit,
                                                    int count) const;

public:
    AxisTickGeneratorTime(const QString &overrideFormat, QObject *parent = 0);

    AxisTickGeneratorTime(const QString &overrideFormat,
                          QSettings *useSettings,
                          QObject *parent = 0);

    AxisTickGeneratorTime(QSettings *useSettings, QObject *parent = 0);

    AxisTickGeneratorTime(QObject *parent = 0);

    virtual ~AxisTickGeneratorTime();

    std::vector<Tick> generate(double min,
                               double max,
                               int flags = AxisTickGenerator::DefaultFlags) const override;
};

/**
 * Divides the axis on logical base 10 boundaries.
 */
class CPD3GRAPHING_EXPORT AxisTickGeneratorDecimal : public AxisTickGenerator {
Q_OBJECT

    static const int minTotalTicks;
    static const int maxTotalTicks;
    static const double stepSizes[3];
    static const double stepSizeWeights[3];
    static const double totalTickWeights[8];
    static const double hitZeroWeight;
    static const double epsilonNormalized;
    static const double minor4Divisible[4];

    int maxSignificantDigits;

    std::vector<AxisTickGenerator::Tick> generateAligned(double min, double max, int flags) const;

    std::vector<AxisTickGenerator::Tick> generateFixed(double min, double max, int) const;

public:
    AxisTickGeneratorDecimal(QObject *parent = 0);

    AxisTickGeneratorDecimal(int setMaxSigDigits, QObject *parent = 0);

    virtual ~AxisTickGeneratorDecimal();

    std::vector<Tick> generate(double min,
                               double max,
                               int flags = AxisTickGenerator::DefaultFlags) const override;

    /**
     * Get the maximum number of significant digits the generator
     * was constructed with.
     * 
     * @return the maximum number of significant digits
     */
    inline int getMaxSignificantDigits() const
    { return maxSignificantDigits; }
};

/**
 * Divides the axis on logarithmic boundaries.
 */
class CPD3GRAPHING_EXPORT AxisTickGeneratorLog : public AxisTickGenerator {
Q_OBJECT

    double base;
    double lbase;

    static const double epsilonNormalized;

    std::vector<AxisTickGenerator::Tick> generateAligned(double min, double max, int flags) const;

    std::vector<AxisTickGenerator::Tick> generateFixed(double min, double max, int flags) const;

public:
    AxisTickGeneratorLog(QObject *parent = 0);

    AxisTickGeneratorLog(double setBase, QObject *parent = 0);

    virtual ~AxisTickGeneratorLog();

    std::vector<Tick> generate(double min,
                               double max,
                               int flags = AxisTickGenerator::DefaultFlags) const override;

    /**
     * Get the logarithm base the generator was constructed with.
     * 
     * @return the logarithm base
     */
    inline double getBase() const
    { return base; }
};


/**
 * Divides the axis on fixed intervals.
 */
class CPD3GRAPHING_EXPORT AxisTickGeneratorConstant : public AxisTickGenerator {
Q_OBJECT

    double interval;
    double origin;

    static const double epsilonNormalized;
    static const int maximumDivisions;
    static const double minor4Divisible[4];

    std::vector<AxisTickGenerator::Tick> generateAligned(double min, double max, int flags) const;

    std::vector<AxisTickGenerator::Tick> generateFixed(double min, double max, int flags) const;

public:
    AxisTickGeneratorConstant(QObject *parent = 0);

    AxisTickGeneratorConstant(double interval, double origin, QObject *parent = 0);

    virtual ~AxisTickGeneratorConstant();

    virtual std::vector<Tick> generate(double min,
                                       double max,
                                       int flags = AxisTickGenerator::DefaultFlags) const;

    /**
     * Get the interval the generator was constructed with.
     * 
     * @return the logarithm base
     */
    inline double getInterval() const
    { return interval; }

    /**
     * Get the origin the generator was constructed with.
     * 
     * @return the origin base
     */
    inline double getOrigin() const
    { return interval; }
};


/**
 * Divides the axis for a Normal CDF scale quantiles.
 */
class CPD3GRAPHING_EXPORT AxisTickGeneratorNormalQuantiles : public AxisTickGenerator {
Q_OBJECT

    static const double epsilonNormalized;
    static const double tryQuantiles[3];
    static const int tryDecimals[3];
public:
    AxisTickGeneratorNormalQuantiles(QObject *parent = 0);

    virtual ~AxisTickGeneratorNormalQuantiles();

    virtual std::vector<Tick> generate(double min,
                                       double max,
                                       int flags = AxisTickGenerator::DefaultFlags) const;
};

/**
 * Divides the axis for logical time intervals.
 */
class CPD3GRAPHING_EXPORT AxisTickGeneratorTimeInterval : public AxisTickGenerator {
Q_OBJECT

    static const double epsilonNormalized;
    static const double breakIntervals[8];

    std::vector<AxisTickGenerator::Tick> generateTicks(double min,
                                                       double max,
                                                       int flags,
                                                       double step,
                                                       double divisor,
                                                       const QString &unit) const;

    void setMajorLabel(AxisTickGenerator::Tick &tick) const;

public:
    AxisTickGeneratorTimeInterval(QObject *parent = 0);

    virtual ~AxisTickGeneratorTimeInterval();

    virtual std::vector<Tick> generate(double min,
                                       double max,
                                       int flags = AxisTickGenerator::DefaultFlags) const;
};


}
}

#endif
