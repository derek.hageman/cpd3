/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>

#include "datacore/variant/root.hxx"
#include "datacore/variant/composite.hxx"
#include "algorithms/model.hxx"
#include "graphing/cycleplot2d.hxx"
#include "guicore/guiformat.hxx"

using namespace CPD3::Data;
using namespace CPD3::Algorithms;
using namespace CPD3::GUI;
using namespace CPD3::Graphing::Internal;

namespace CPD3 {
namespace Graphing {

/** @file graphing/cycleplot2d.hxx
 * A plot of a cycle of data.
 */

static double timeToPosition(double time,
                             double start,
                             double end,
                             TimeCycleInterval &interval,
                             TimeCycleDivision &division)
{
    if (!FP::defined(time)) return time;

    if (FP::defined(start))
        return division.convertTime(start, time, interval);
    return division.convertTime(end, time, interval);
}

CyclePlot2DParameters::CyclePlot2DParameters() : components(0)
{ }

CyclePlot2DParameters::~CyclePlot2DParameters() = default;

CyclePlot2DParameters::CyclePlot2DParameters(const CyclePlot2DParameters &) = default;

CyclePlot2DParameters &CyclePlot2DParameters::operator=(const CyclePlot2DParameters &) = default;

CyclePlot2DParameters::CyclePlot2DParameters(CyclePlot2DParameters &&) = default;

CyclePlot2DParameters &CyclePlot2DParameters::operator=(CyclePlot2DParameters &&) = default;

CyclePlot2DParameters::CyclePlot2DParameters(const Variant::Read &value,
                                             const DisplayDefaults &defaults) : Graph2DParameters(
        value, defaults), components(0), interval(), division()
{
    if (value["Interval"].exists()) {
        interval = TimeCycleInterval(value["Interval"]);
        components |= Component_Interval;
    }

    if (value["Division"].exists()) {
        division = TimeCycleDivision(value["Division"]);
        components |= Component_Division;
    } else {
        switch (interval.getUnits()) {
        case Time::Year:
            division = TimeCycleDivision(Time::Month, 1);
            break;
        case Time::Quarter:
            division = TimeCycleDivision(Time::Week, 1);
            break;
        case Time::Month:
            division = TimeCycleDivision(Time::Week, 1);
            break;
        case Time::Week:
            division = TimeCycleDivision(Time::Day, 1);
            break;
        case Time::Day:
            division = TimeCycleDivision(Time::Hour, 1);
            break;
        case Time::Hour:
            division = TimeCycleDivision(Time::Minute, 6);
            break;
        case Time::Minute:
            division = TimeCycleDivision(Time::Second, 6);
            break;
        case Time::Second:
            division = TimeCycleDivision(Time::Millisecond, 100);
            break;
        case Time::Millisecond:
            division = TimeCycleDivision(Time::Millisecond, 100);
            break;
        }
    }
    division.rectifyWithInterval(interval);
}

void CyclePlot2DParameters::save(Variant::Write &value) const
{
    Graph2DParameters::save(value);

    if (components & Component_Interval) {
        value["Interval"].set(interval.save());
    }

    if (components & Component_Division) {
        value["Division"].set(division.save());
    }
}

std::unique_ptr<Graph2DParameters> CyclePlot2DParameters::clone() const
{ return std::unique_ptr<Graph2DParameters>(new CyclePlot2DParameters(*this)); }

void CyclePlot2DParameters::overlay(const Graph2DParameters *over)
{
    Graph2DParameters::overlay(over);

    const CyclePlot2DParameters *top = static_cast<const CyclePlot2DParameters *>(over);

    if (top->components & Component_Interval) {
        interval = top->interval;
        components |= Component_Interval;
    }

    if (top->components & Component_Division) {
        interval = top->interval;
        components |= Component_Division;
    }

    division.rectifyWithInterval(interval);
}

void CyclePlot2DParameters::initializeOverlay(const Graph2DParameters *over)
{
    CyclePlot2DParameters::operator=(*(static_cast<const CyclePlot2DParameters *>(over)));
}

void CyclePlot2DParameters::copy(const Graph2DParameters *from)
{ *this = *(static_cast<const CyclePlot2DParameters *>(from)); }

void CyclePlot2DParameters::clear()
{
    Graph2DParameters::clear();
    components = 0;
}

CyclePlotTraceParameters2D::CyclePlotTraceParameters2D() : components(0),
                                                           origin(Origin_Start),
                                                           breakCycle(true)
{ }

CyclePlotTraceParameters2D::~CyclePlotTraceParameters2D() = default;

CyclePlotTraceParameters2D::CyclePlotTraceParameters2D(const CyclePlotTraceParameters2D &) = default;

CyclePlotTraceParameters2D &CyclePlotTraceParameters2D::operator=(const CyclePlotTraceParameters2D &) = default;

CyclePlotTraceParameters2D::CyclePlotTraceParameters2D(CyclePlotTraceParameters2D &&) = default;

CyclePlotTraceParameters2D &CyclePlotTraceParameters2D::operator=(CyclePlotTraceParameters2D &&) = default;

CyclePlotTraceParameters2D::CyclePlotTraceParameters2D(const Variant::Read &value)
        : TraceParameters2D(value), components(0), origin(Origin_Start), breakCycle(true)
{
    if (value["Origin"].exists()) {
        const auto &check = value["Origin"].toString();
        if (Util::equal_insensitive(check, "start")) {
            origin = Origin_Start;
            components |= Component_Origin;
        } else if (Util::equal_insensitive(check, "end")) {
            origin = Origin_End;
            components |= Component_Origin;
        } else if (Util::equal_insensitive(check, "center")) {
            origin = Origin_Center;
            components |= Component_Origin;
        }
    }

    if (value["BreakCycle"].exists()) {
        breakCycle = value["BreakCycle"].toBool();
        components |= Component_BreakCycle;
    }
}

void CyclePlotTraceParameters2D::save(Variant::Write &value) const
{
    TraceParameters2D::save(value);

    if (components & Component_Origin) {
        switch (origin) {
        case Origin_Start:
            value["Origin"].setString("Start");
            break;
        case Origin_End:
            value["Origin"].setString("End");
            break;
        case Origin_Center:
            value["Origin"].setString("Center");
            break;
        }
    }

    if (components & Component_BreakCycle) {
        value["BreakCycle"].setBool(breakCycle);
    }
}

std::unique_ptr<TraceParameters> CyclePlotTraceParameters2D::clone() const
{ return std::unique_ptr<TraceParameters>(new CyclePlotTraceParameters2D(*this)); }

void CyclePlotTraceParameters2D::overlay(const TraceParameters *over)
{
    TraceParameters2D::overlay(over);

    const CyclePlotTraceParameters2D *top = static_cast<const CyclePlotTraceParameters2D *>(over);

    if (top->components & Component_Origin) {
        origin = top->origin;
        components |= Component_Origin;
    }

    if (top->components & Component_BreakCycle) {
        breakCycle = top->breakCycle;
        components |= Component_BreakCycle;
    }
}

void CyclePlotTraceParameters2D::copy(const TraceParameters *from)
{ *this = *(static_cast<const CyclePlotTraceParameters2D *>(from)); }

void CyclePlotTraceParameters2D::clear()
{
    TraceParameters2D::clear();
    components = 0;
}

CyclePlotTraceHandler2D::CyclePlotTraceHandler2D(std::unique_ptr<
        CyclePlotTraceParameters2D> &&params) : TraceHandler2D(std::move(params)),
                                                parameters(
                                                        static_cast<CyclePlotTraceParameters2D *>(getParameters()))
{ }

CyclePlotTraceHandler2D::~CyclePlotTraceHandler2D()
{ }

void CyclePlotTraceHandler2D::setPlotParameters(const TimeCycleInterval &interval,
                                                const TimeCycleDivision &division)
{
    this->interval = interval;
    this->division = division;
}

CyclePlotTraceHandler2D::Cloned CyclePlotTraceHandler2D::clone() const
{
    return Cloned(new CyclePlotTraceHandler2D(*this, parameters->clone()));
}

CyclePlotTraceHandler2D::CyclePlotTraceHandler2D(const CyclePlotTraceHandler2D &other,
                                                 std::unique_ptr<TraceParameters> &&params)
        : TraceHandler2D(other, std::move(params)),
          parameters(static_cast<CyclePlotTraceParameters2D *>(getParameters()))
{ }

QString CyclePlotTraceHandler2D::getFitLegend(const std::shared_ptr<Model> &fit) const
{
    Q_ASSERT(fit);
    ModelLegendParameters lp;
    lp.setConfidence(0.95);
    lp.setShowOffset(false);
    ModelLegendEntry legend(fit->legend(lp));

    QString base(legend.getLine());
    if (base.isEmpty())
        return base;

    return tr("%1 (%2)").arg(base, AxisTickGeneratorTimeCycle::getTitle(interval, division,
                                                                        AxisTickGeneratorTimeCycle::Title_Fit)
            .toLower());
}

void CyclePlotTraceHandler2D::convertValue(const TraceDispatch::OutputValue &value,
                                           std::vector<TracePoint<3> > &points)
{
    if (value.isMetadata()) {
        for (std::size_t i = 0; i < 2; i++) {
            updateMetadata(i + 1, value.get(i));
        }
        return;
    }

    TracePoint<3> add;
    add.start = value.getStart();
    add.end = value.getEnd();
    switch (parameters->getOrigin()) {
    case CyclePlotTraceParameters2D::Origin_Start:
        if (FP::defined(add.start))
            add.d[0] = add.start;
        else
            add.d[0] = add.end;
        break;
    case CyclePlotTraceParameters2D::Origin_End:
        if (FP::defined(add.end))
            add.d[0] = add.end;
        else
            add.d[0] = add.start;
        break;
    case CyclePlotTraceParameters2D::Origin_Center:
        if (FP::defined(add.start) && FP::defined(add.end))
            add.d[0] = (add.start + add.end) * 0.5;
        else if (FP::defined(add.start))
            add.d[0] = add.start;
        else
            add.d[0] = add.end;
        break;
    }
    if (!FP::defined(add.d[0]))
        return;
    add.d[0] = timeToPosition(add.d[0], getStart(), getEnd(), interval, division);
    if (parameters->getBreakCycle() && !points.empty() && points.back().d[0] > add.d[0]) {
        TracePoint<3> br(add);
        for (std::size_t i = 1; i < 3; i++) {
            br.d[i] = FP::undefined();
        }
        points.emplace_back(std::move(br));
    }
    for (std::size_t i = 0; i < 2; i++) {
        switch (value.getType(i)) {
        case Variant::Type::Array:
        case Variant::Type::Matrix: {
            int idxStart = points.size();
            for (int j = i + 1; j <= 2; j++) {
                add.d[j] = FP::undefined();
            }
            for (; i < 2; i++) {
                handleValueFanout(points, add, idxStart, value.get(i), i + 1);
            }
            return;
        }
        default:
            add.d[i + 1] = Data::Variant::Composite::toNumber(value.get(i));
            break;
        }
    }
    points.emplace_back(std::move(add));
}

QVector<TransformerPoint *> CyclePlotTraceHandler2D::prepareTransformer(const std::vector<
        TraceDispatch::OutputValue> &points, TransformerParameters &)
{
    QVector<TransformerPoint *> result;
    result.reserve(points.size());
    for (const auto &it : points) {
        if (!Range::intersects(it.getStart(), it.getEnd(), getStart(), getEnd()))
            continue;

        if (it.isMetadata()) {
            for (std::size_t i = 0; i < 2; i++) {
                updateMetadata(i + 1, it.get(i));
            }
            continue;
        }

        handleTransformerPoint<2>(this->parameters->getTransformerInputMode(), result, it);
    }
    return result;
}

std::vector<TracePoint<3>> CyclePlotTraceHandler2D::convertTransformerResult(const QVector<
        TransformerPoint *> &points)
{
    std::vector<TracePoint<3>> result;
    result.reserve(points.size());
    for (auto it : points) {
        const auto &point = *static_cast<const TransformValue<2> *>(it);

        TracePoint<3> add;
        add.start = point.getValue().getStart();
        add.end = point.getValue().getEnd();
        switch (parameters->getOrigin()) {
        case CyclePlotTraceParameters2D::Origin_Start:
            if (FP::defined(add.start))
                add.d[0] = add.start;
            else
                add.d[0] = add.end;
            break;
        case CyclePlotTraceParameters2D::Origin_End:
            if (FP::defined(add.end))
                add.d[0] = add.end;
            else
                add.d[0] = add.start;
            break;
        case CyclePlotTraceParameters2D::Origin_Center:
            if (FP::defined(add.start) && FP::defined(add.end))
                add.d[0] = (add.start + add.end) * 0.5;
            else if (FP::defined(add.start))
                add.d[0] = add.start;
            else
                add.d[0] = add.end;
            break;
        }

        if (!FP::defined(add.d[0])) {
            delete it;
            continue;
        }
        add.d[0] = timeToPosition(add.d[0], getStart(), getEnd(), interval, division);
        if (parameters->getBreakCycle() && !result.empty() && result.back().d[0] > add.d[0]) {
            TracePoint<3> br(add);
            for (std::size_t i = 1; i < 3; i++) {
                br.d[i] = FP::undefined();
            }
            result.emplace_back(std::move(br));
        }

        for (std::size_t i = 0; i < 2; i++) {
            add.d[i + 1] = point.get(i);
        }
        result.emplace_back(std::move(add));
        delete it;
    }
    return result;
}

TransformerAllocator *CyclePlotTraceHandler2D::getTransformerAllocator()
{ return &TransformValue<1>::allocator; }

CyclePlotTraceSet2D::CyclePlotTraceSet2D()
{ }

CyclePlotTraceSet2D::~CyclePlotTraceSet2D()
{ }

CyclePlotTraceSet2D::Cloned CyclePlotTraceSet2D::clone() const
{ return Cloned(new CyclePlotTraceSet2D(*this)); }

CyclePlotTraceSet2D::CyclePlotTraceSet2D(const CyclePlotTraceSet2D &other) = default;

void CyclePlotTraceSet2D::setPlotParameters(const TimeCycleInterval &interval,
                                            const TimeCycleDivision &division)
{
    this->interval = interval;
    this->division = division;
    for (auto it : getHandlers()) {
        static_cast<CyclePlotTraceHandler2D *>(it)->setPlotParameters(interval, division);
    }
}

std::unique_ptr<
        TraceParameters> CyclePlotTraceSet2D::parseParameters(const Variant::Read &value) const
{ return std::unique_ptr<TraceParameters>(new CyclePlotTraceParameters2D(value)); }

CyclePlotTraceSet2D::CreatedHandler CyclePlotTraceSet2D::createHandler(std::unique_ptr<
        TraceParameters> &&parameters) const
{
    std::unique_ptr<CyclePlotTraceHandler2D> handler(new CyclePlotTraceHandler2D(
            std::unique_ptr<CyclePlotTraceParameters2D>(
                    static_cast<CyclePlotTraceParameters2D *>(parameters.release()))));
    handler->setPlotParameters(interval, division);
    return std::move(handler);
}

CyclePlotBinParameters2D::CyclePlotBinParameters2D() : components(0),
                                                       intervalBins(true),
                                                       totalBefore(false),
                                                       totalAfter(false),
                                                       onlyExtendTotals(true)
{ }

CyclePlotBinParameters2D::~CyclePlotBinParameters2D() = default;

CyclePlotBinParameters2D::CyclePlotBinParameters2D(const CyclePlotBinParameters2D &) = default;

CyclePlotBinParameters2D &CyclePlotBinParameters2D::operator=(const CyclePlotBinParameters2D &) = default;

CyclePlotBinParameters2D::CyclePlotBinParameters2D(CyclePlotBinParameters2D &&) = default;

CyclePlotBinParameters2D &CyclePlotBinParameters2D::operator=(CyclePlotBinParameters2D &&) = default;

CyclePlotBinParameters2D::CyclePlotBinParameters2D(const Variant::Read &value) : BinParameters2D(
        value),
                                                                                 components(0),
                                                                                 intervalBins(true),
                                                                                 totalBefore(false),
                                                                                 totalAfter(false),
                                                                                 onlyExtendTotals(
                                                                                         true)
{
    if (value["Binning/Interval"].exists()) {
        intervalBins = value["Binning/Interval"].toBool();
        components |= Component_IntervalBins;
    }

    if (value["Binning/TotalBefore"].exists()) {
        totalBefore = value["Binning/TotalBefore"].toBool();
        components |= Component_TotalBefore;
    }

    if (value["Binning/TotalAfter"].exists()) {
        totalAfter = value["Binning/TotalAfter"].toBool();
        components |= Component_TotalAfter;
    }

    if (value["ExtendAllMiddles"].exists()) {
        onlyExtendTotals = !value["ExtendAllMiddles"].toBool();
        components |= Component_OnlyExtendTotals;
    } else {
        onlyExtendTotals = (totalBefore || totalAfter);
    }
}

void CyclePlotBinParameters2D::save(Variant::Write &value) const
{
    BinParameters2D::save(value);

    if (components & Component_IntervalBins) {
        value["Binning/Interval"].setBool(intervalBins);
    }

    if (components & Component_TotalBefore) {
        value["Binning/TotalBefore"].setBool(totalBefore);
    }

    if (components & Component_TotalAfter) {
        value["Binning/TotalAfter"].setBool(totalAfter);
    }

    if (components & Component_OnlyExtendTotals) {
        value["ExtendAllMiddles"].setBool(!onlyExtendTotals);
    }
}

std::unique_ptr<TraceParameters> CyclePlotBinParameters2D::clone() const
{ return std::unique_ptr<TraceParameters>(new CyclePlotBinParameters2D(*this)); }

void CyclePlotBinParameters2D::overlay(const TraceParameters *over)
{
    BinParameters2D::overlay(over);

    const CyclePlotBinParameters2D *top = static_cast<const CyclePlotBinParameters2D *>(over);

    if (top->components & Component_IntervalBins) {
        intervalBins = top->intervalBins;
        components |= Component_IntervalBins;
    }

    if (top->components & Component_TotalBefore) {
        totalBefore = top->totalBefore;
        components |= Component_TotalBefore;
    }

    if (top->components & Component_TotalAfter) {
        totalAfter = top->totalAfter;
        components |= Component_TotalAfter;
    }

    if (top->components & Component_OnlyExtendTotals) {
        onlyExtendTotals = top->onlyExtendTotals;
        components |= Component_OnlyExtendTotals;
    } else if (!(components & Component_OnlyExtendTotals)) {
        onlyExtendTotals = (totalBefore || totalAfter);
    }
}

void CyclePlotBinParameters2D::copy(const TraceParameters *from)
{ *this = *(static_cast<const CyclePlotBinParameters2D *>(from)); }

void CyclePlotBinParameters2D::clear()
{
    BinParameters2D::clear();
    components = 0;
}

CyclePlotBinHandler2D::CyclePlotBinHandler2D(std::unique_ptr<CyclePlotBinParameters2D> &&params)
        : BinHandler2D(std::move(params)),
          parameters(static_cast<CyclePlotBinParameters2D *>(getParameters()))
{ }

CyclePlotBinHandler2D::~CyclePlotBinHandler2D()
{ }

void CyclePlotBinHandler2D::setPlotParameters(const TimeCycleInterval &interval,
                                              const TimeCycleDivision &division)
{
    this->interval = interval;
    this->division = division;
}

CyclePlotBinHandler2D::Cloned CyclePlotBinHandler2D::clone() const
{ return Cloned(new CyclePlotBinHandler2D(*this, parameters->clone())); }

CyclePlotBinHandler2D::CyclePlotBinHandler2D(const CyclePlotBinHandler2D &other,
                                             std::unique_ptr<TraceParameters> &&params)
        : BinHandler2D(other, std::move(params)),
          parameters(static_cast<CyclePlotBinParameters2D *>(getParameters()))
{ }

void CyclePlotBinHandler2D::convertValue(const TraceDispatch::OutputValue &value,
                                         std::vector<TracePoint<2>> &points)
{
    if (value.isMetadata()) {
        for (std::size_t i = 0; i < 1; i++) {
            updateMetadata(i + 1, value.get(i));
        }
        return;
    }
    TracePoint<2> add;
    add.start = value.getStart();
    add.end = value.getEnd();
    add.d[0] = add.start;
    for (std::size_t i = 0; i < 1; i++) {
        switch (value.getType(i)) {
        case Variant::Type::Array:
        case Variant::Type::Matrix: {
            int idxStart = points.size();
            for (std::size_t j = i + 1; j <= 1; j++) {
                add.d[j] = FP::undefined();
            }
            for (; i < 1; i++) {
                handleValueFanout(points, add, idxStart, value.get(i), i + 1);
            }
            return;
        }
        default:
            add.d[i + 1] = Data::Variant::Composite::toNumber(value.get(i));
            break;
        }
    }
    points.emplace_back(std::move(add));
}

QVector<TransformerPoint *> CyclePlotBinHandler2D::prepareTransformer(const std::vector<
        TraceDispatch::OutputValue> &points, TransformerParameters &)
{

    QVector<TransformerPoint *> result;
    result.reserve(points.size());
    for (const auto &it : points) {
        if (!Range::intersects(it.getStart(), it.getEnd(), getStart(), getEnd()))
            continue;

        if (it.isMetadata()) {
            for (int i = 0; i < 1; i++) {
                updateMetadata(i + 1, it.get(i));
            }
            continue;
        }

        handleTransformerPoint<1>(this->parameters->getTransformerInputMode(), result, it);
    }
    return result;
}

std::vector<TracePoint<2>> CyclePlotBinHandler2D::convertTransformerResult(const QVector<
        TransformerPoint *> &points)
{
    std::vector<TracePoint<2>> result;
    result.reserve(points.size());
    for (auto it : points) {
        const auto &point = *static_cast<const TransformValue<1> *>(it);

        TracePoint<2> add;
        add.start = point.getValue().getStart();
        add.end = point.getValue().getEnd();
        add.d[0] = add.start;

        for (int i = 0; i < 1; i++) {
            add.d[i + 1] = point.get(i);
        }
        result.emplace_back(std::move(add));

        delete it;
    }
    return result;
}

TransformerAllocator *CyclePlotBinHandler2D::getTransformerAllocator()
{ return &TransformValue<1>::allocator; }

void CyclePlotBinHandler2D::buildBins(std::size_t,
                                      std::vector<double> &starts,
                                      std::vector<double> &ends,
                                      std::vector<double> &centers)
{
    double boundLower = 0;
    double boundUpper = -1;
    division.getTotalBounds(boundLower, boundUpper, interval);

    if (parameters->getIntervalBins()) {
        for (int min = (int) floor(boundLower), i = (int) floor(boundLower),
                max = (int) ceil(boundUpper); i < max; i++) {
            if (i == min && i != max - 1) {
                starts.emplace_back(boundLower);
                ends.emplace_back(i + 1);
            } else if (i == max - 1 && i != min) {
                starts.emplace_back(i);
                ends.emplace_back(boundUpper);
            } else {
                starts.emplace_back(i);
                ends.emplace_back(i + 1);
            }
            centers.emplace_back((starts.back() + ends.back()) * 0.5);
        }
    }

    if (parameters->getTotalBefore()) {
        starts.emplace_back(FP::undefined());
        ends.emplace_back(FP::undefined());
        centers.emplace_back(boundLower - 0.5);
    }

    if (parameters->getTotalAfter()) {
        starts.emplace_back(FP::undefined());
        ends.emplace_back(FP::undefined());
        centers.emplace_back(boundUpper + 0.5);
    }
}

std::vector<TracePoint<2>> CyclePlotBinHandler2D::applyFilter(std::vector<TracePoint<2>> input,
                                                              double start,
                                                              double end,
                                                              std::size_t)
{
    std::vector<TracePoint<2> > result;
    double base = getStart();
    if (!FP::defined(base))
        base = getEnd();
    if (!FP::defined(start)) {
        if (!FP::defined(end))
            return input;
        for (const auto &it : input) {
            if (FP::defined(it.end)) {
                double point = division.convertTime(base, it.start, interval);
                if (FP::defined(point) && point >= end)
                    continue;
            }
            result.emplace_back(it);
        }
    } else if (!FP::defined(end)) {
        for (const auto &it : input) {
            if (FP::defined(it.start)) {
                double point = division.convertTime(base, it.end, interval);
                if (FP::defined(point) && point <= start)
                    continue;
            }
            result.emplace_back(it);
        }
    } else {
        for (const auto &it : input) {
            if (FP::defined(it.start) && FP::defined(it.end)) {
                double projectedStart;
                double projectedEnd;
                interval.intervalBounds(base, it.start, projectedStart, projectedEnd);
                /* If the point wraps the whole set of bins, then it goes in
                 * this one regardless. */
                if (projectedEnd >= it.end) {
                    double pointStart = division.convertTime(base, it.start, interval);
                    double pointEnd = division.convertTime(base, it.end, interval);
                    Q_ASSERT(FP::defined(pointStart));
                    Q_ASSERT(FP::defined(pointEnd));
                    /* Wrap around */
                    if (pointStart <= pointEnd) {
                        if (pointStart >= end)
                            continue;
                        if (pointEnd <= start)
                            continue;
                    } else {
                        if (pointStart >= end && pointEnd <= start)
                            continue;
                    }
                }
            }
            result.emplace_back(it);
        }
    }
    return result;
}

void CyclePlotBinHandler2D::convertBin(const std::vector<TracePoint<2> > &points,
                                       BinPoint<1, 1> &output)
{
    BinValueHandler<1, 1>::convertBin(points, output);
    output.width[0] = 1.0;
}

void CyclePlotBinHandler2D::convertHistogram(const std::vector<TracePoint<2> > &points,
                                             const double totalTime[1],
                                             double effectiveStart,
                                             double effectiveEnd,
                                             BinPoint<1, 1> &output)
{
    BinValueHandler<1, 1>::convertHistogram(points, totalTime, effectiveStart, effectiveEnd,
                                            output);
    output.width[0] = 1.0;
}

std::vector<BinPoint<1, 1>> CyclePlotBinHandler2D::getMiddleExtendPoints(const std::vector<
        BinPoint<1, 1>> &input) const
{
    if (!parameters->getOnlyExtendTotals())
        return input;
    double boundLower = 0;
    double boundUpper = -1;
    division.getTotalBounds(boundLower, boundUpper, interval);
    std::vector<BinPoint<1, 1> > result;
    for (const auto &it : input) {
        if (!FP::defined(it.center[0]))
            continue;
        if (it.center[0] > boundLower - 0.005 && it.center[0] < boundUpper + 0.005)
            continue;
        result.emplace_back(it);
    }
    return result;
}


CyclePlotBinSet2D::CyclePlotBinSet2D() = default;

CyclePlotBinSet2D::~CyclePlotBinSet2D() = default;

CyclePlotBinSet2D::Cloned CyclePlotBinSet2D::clone() const
{ return Cloned(new CyclePlotBinSet2D(*this)); }

CyclePlotBinSet2D::CyclePlotBinSet2D(const CyclePlotBinSet2D &other) = default;

void CyclePlotBinSet2D::setPlotParameters(const TimeCycleInterval &interval,
                                          const TimeCycleDivision &division)
{
    this->interval = interval;
    this->division = division;
    for (const auto &it : getHandlers()) {
        static_cast<CyclePlotBinHandler2D *>(it)->setPlotParameters(interval, division);
    }
}

std::unique_ptr<
        TraceParameters> CyclePlotBinSet2D::parseParameters(const Variant::Read &value) const
{ return std::unique_ptr<TraceParameters>(new CyclePlotBinParameters2D(value)); }

CyclePlotBinSet2D::CreatedHandler CyclePlotBinSet2D::createHandler(std::unique_ptr<
        TraceParameters> &&parameters) const
{
    std::unique_ptr<CyclePlotBinHandler2D> handler(new CyclePlotBinHandler2D(
            std::unique_ptr<CyclePlotBinParameters2D>(static_cast<CyclePlotBinParameters2D *>(
                                                              parameters.release()))));
    handler->setPlotParameters(interval, division);
    return std::move(handler);
}

CyclePlotIndicatorParameters2D::CyclePlotIndicatorParameters2D() : components(0),
                                                                   origin(Origin_Start)
{ }

CyclePlotIndicatorParameters2D::~CyclePlotIndicatorParameters2D() = default;

CyclePlotIndicatorParameters2D::CyclePlotIndicatorParameters2D(const CyclePlotIndicatorParameters2D &) = default;

CyclePlotIndicatorParameters2D &CyclePlotIndicatorParameters2D::operator=(const CyclePlotIndicatorParameters2D &) = default;

CyclePlotIndicatorParameters2D::CyclePlotIndicatorParameters2D(CyclePlotIndicatorParameters2D &&) = default;

CyclePlotIndicatorParameters2D &CyclePlotIndicatorParameters2D::operator=(
        CyclePlotIndicatorParameters2D &&) = default;

CyclePlotIndicatorParameters2D::CyclePlotIndicatorParameters2D(const Variant::Read &value)
        : IndicatorParameters2D(value), components(0), origin(Origin_Start)
{
    if (value["Origin"].exists()) {
        const auto &check = value["Origin"].toString();
        if (Util::equal_insensitive(check, "start")) {
            origin = Origin_Start;
            components |= Component_Origin;
        } else if (Util::equal_insensitive(check, "end")) {
            origin = Origin_End;
            components |= Component_Origin;
        } else if (Util::equal_insensitive(check, "center")) {
            origin = Origin_Center;
            components |= Component_Origin;
        }
    }
}

void CyclePlotIndicatorParameters2D::save(Variant::Write &value) const
{
    IndicatorParameters2D::save(value);

    if (components & Component_Origin) {
        switch (origin) {
        case Origin_Start:
            value["Origin"].setString("Start");
            break;
        case Origin_End:
            value["Origin"].setString("End");
            break;
        case Origin_Center:
            value["Origin"].setString("Center");
            break;
        }
    }
}

std::unique_ptr<TraceParameters> CyclePlotIndicatorParameters2D::clone() const
{ return std::unique_ptr<TraceParameters>(new CyclePlotIndicatorParameters2D(*this)); }

void CyclePlotIndicatorParameters2D::overlay(const TraceParameters *over)
{
    IndicatorParameters2D::overlay(over);

    const CyclePlotIndicatorParameters2D
            *top = static_cast<const CyclePlotIndicatorParameters2D *>(over);

    if (top->components & Component_Origin) {
        origin = top->origin;
        components |= Component_Origin;
    }
}

void CyclePlotIndicatorParameters2D::copy(const TraceParameters *from)
{ *this = *(static_cast<const CyclePlotIndicatorParameters2D *>(from)); }

void CyclePlotIndicatorParameters2D::clear()
{
    IndicatorParameters2D::clear();
    components = 0;
}

CyclePlotIndicatorHandler2D::CyclePlotIndicatorHandler2D(std::unique_ptr<
        CyclePlotIndicatorParameters2D> &&params) : IndicatorHandler2D(std::move(params)),
                                                    parameters(
                                                            static_cast<CyclePlotIndicatorParameters2D *>(getParameters()))
{ }

CyclePlotIndicatorHandler2D::~CyclePlotIndicatorHandler2D() = default;

void CyclePlotIndicatorHandler2D::setPlotParameters(const TimeCycleInterval &interval,
                                                    const TimeCycleDivision &division)
{
    this->interval = interval;
    this->division = division;
}

CyclePlotIndicatorHandler2D::Cloned CyclePlotIndicatorHandler2D::clone() const
{ return Cloned(new CyclePlotIndicatorHandler2D(*this, parameters->clone())); }

void CyclePlotIndicatorHandler2D::convertIndicatorValue(const TraceDispatch::OutputValue &value,
                                                        IndicatorPoint<1> &add)
{

    switch (parameters->getOrigin()) {
    case CyclePlotIndicatorParameters2D::Origin_Start:
        if (FP::defined(value.getStart()))
            add.d[0] = value.getStart();
        else
            add.d[0] = value.getEnd();
        break;
    case CyclePlotIndicatorParameters2D::Origin_End:
        if (FP::defined(value.getEnd()))
            add.d[0] = value.getEnd();
        else
            add.d[0] = value.getStart();
        break;
    case CyclePlotIndicatorParameters2D::Origin_Center:
        if (FP::defined(value.getStart()) && FP::defined(value.getEnd()))
            add.d[0] = (value.getStart() + value.getEnd()) * 0.5;
        else if (FP::defined(add.start))
            add.d[0] = value.getStart();
        else
            add.d[0] = value.getEnd();
        break;
    }
    add.d[0] = timeToPosition(add.d[0], getStart(), getEnd(), interval, division);
}

CyclePlotIndicatorHandler2D::CyclePlotIndicatorHandler2D(const CyclePlotIndicatorHandler2D &other,
                                                         std::unique_ptr<TraceParameters> &&params)
        : IndicatorHandler2D(other, std::move(params)),
          parameters(static_cast<CyclePlotIndicatorParameters2D *>(getParameters()))
{ }

CyclePlotIndicatorSet2D::CyclePlotIndicatorSet2D() = default;

CyclePlotIndicatorSet2D::~CyclePlotIndicatorSet2D() = default;

CyclePlotIndicatorSet2D::Cloned CyclePlotIndicatorSet2D::clone() const
{ return Cloned(new CyclePlotIndicatorSet2D(*this)); }

CyclePlotIndicatorSet2D::CyclePlotIndicatorSet2D(const CyclePlotIndicatorSet2D &other) = default;

void CyclePlotIndicatorSet2D::setPlotParameters(const TimeCycleInterval &interval,
                                                const TimeCycleDivision &division)
{
    this->interval = interval;
    this->division = division;
    for (auto it : getHandlers()) {
        static_cast<CyclePlotIndicatorHandler2D *>(it)->setPlotParameters(interval, division);
    }
}

std::unique_ptr<
        TraceParameters> CyclePlotIndicatorSet2D::parseParameters(const Variant::Read &value) const
{ return std::unique_ptr<TraceParameters>(new CyclePlotIndicatorParameters2D(value)); }

CyclePlotIndicatorSet2D::CreatedHandler CyclePlotIndicatorSet2D::createHandler(std::unique_ptr<
        TraceParameters> &&parameters) const
{
    std::unique_ptr<CyclePlotIndicatorHandler2D> handler(new CyclePlotIndicatorHandler2D(
            std::unique_ptr<CyclePlotIndicatorParameters2D>(
                    static_cast<CyclePlotIndicatorParameters2D *>(parameters.release()))));
    handler->setPlotParameters(interval, division);
    return std::move(handler);
}

AxisParametersCyclePlot2D::AxisParametersCyclePlot2D() : components(0), tickOffset(0.5)
{
    setTickLevels(1);
    clearFixedLines();

    AxisTransformer tr;
    tr.setMinExtendAbsolute(0.5);
    tr.setMaxExtendAbsolute(0.5);
    forceAxisTransformer(tr);
}

AxisParametersCyclePlot2D::~AxisParametersCyclePlot2D() = default;

AxisParametersCyclePlot2D::AxisParametersCyclePlot2D(const AxisParametersCyclePlot2D &) = default;

AxisParametersCyclePlot2D &AxisParametersCyclePlot2D::operator=(const AxisParametersCyclePlot2D &) = default;

AxisParametersCyclePlot2D::AxisParametersCyclePlot2D(AxisParametersCyclePlot2D &&) = default;

AxisParametersCyclePlot2D &AxisParametersCyclePlot2D::operator=(AxisParametersCyclePlot2D &&) = default;

AxisParametersCyclePlot2D::AxisParametersCyclePlot2D(const Variant::Read &value,
                                                     QSettings *settings) : AxisParameters2DSide(
        value, settings), components(0), tickOffset(0.5)
{
    if (!hasTickLevels())
        setTickLevels(1);
    if (!hasFixedLines())
        clearFixedLines();

    AxisTransformer tr;
    tr.setMinExtendAbsolute(0.5);
    tr.setMaxExtendAbsolute(0.5);
    forceAxisTransformer(tr);

    if (FP::defined(value["TickOffset"].toDouble())) {
        tickOffset = value["TickOffset"].toDouble();
        components |= Component_TickOffset;
    }
}

void AxisParametersCyclePlot2D::save(Variant::Write &value) const
{
    AxisParameters2DSide::save(value);

    if (components & Component_TickOffset) {
        value["TickOffset"].setDouble(tickOffset);
    }
}

std::unique_ptr<AxisParameters> AxisParametersCyclePlot2D::clone() const
{ return std::unique_ptr<AxisParameters>(new AxisParametersCyclePlot2D(*this)); }

void AxisParametersCyclePlot2D::overlay(const AxisParameters *over)
{
    AxisParameters2DSide::overlay(over);

    const AxisParametersCyclePlot2D *top = static_cast<const AxisParametersCyclePlot2D *>(over);

    if (top->components & Component_TickOffset) {
        tickOffset = top->tickOffset;
        components |= Component_TickOffset;
    }
}

void AxisParametersCyclePlot2D::copy(const AxisParameters *from)
{ *this = *(static_cast<const AxisParametersCyclePlot2D *>(from)); }

void AxisParametersCyclePlot2D::clear()
{
    AxisParameters2DSide::clear();
    components = 0;
}

AxisDimensionCyclePlot2D::AxisDimensionCyclePlot2D(std::unique_ptr<
        AxisParametersCyclePlot2D> &&params) : AxisDimension2DSide(std::move(params)),
                                               parameters(
                                                       static_cast<AxisParametersCyclePlot2D *>(getParameters()))
{ }

AxisDimensionCyclePlot2D::~AxisDimensionCyclePlot2D() = default;

void AxisDimensionCyclePlot2D::updateTransformer(double &min,
                                                 double &max,
                                                 double zoomMin,
                                                 double zoomMax)
{
    double boundLower = 0;
    double boundUpper = -1;
    division.getTotalBounds(boundLower, boundUpper, interval);

    if (FP::defined(min) && FP::defined(max)) {
        if (min < boundLower - 0.1)
            min = boundLower - 1.0;
        else
            min = boundLower;

        if (max > boundUpper + 0.1)
            max = boundUpper + 1.0;
        else
            max = boundUpper;
    } else {
        min = boundLower;
        max = boundUpper;
    }

    getTransformer().setReal(min, max, zoomMin, zoomMax);
}

static bool tickSort(const AxisTickGenerator::Tick &a, const AxisTickGenerator::Tick &b)
{
    if (!FP::defined(a.point))
        return FP::defined(b.point);
    if (!FP::defined(b.point))
        return false;
    return a.point < b.point;
}

std::vector<std::vector<AxisTickGenerator::Tick>> AxisDimensionCyclePlot2D::getTicks() const
{
    auto result = AxisDimension::getTicks();

    double boundLower = 0;
    double boundUpper = -1;
    division.getTotalBounds(boundLower, boundUpper, interval);

    double epsilon = qMax(fabs(boundLower), fabs(boundUpper)) * 1E-6;
    for (auto &level : result) {
        for (auto tick = level.begin(); tick != level.end();) {
            if (!FP::defined(tick->point)) {
                tick = level.erase(tick);
                continue;
            }
            tick->point += parameters->getTickOffset();
            if (tick->point < boundLower - epsilon || tick->point > boundUpper + epsilon) {
                tick = level.erase(tick);
                continue;
            }
            ++tick;
        }
    }

    if (parameters->getTickLevels() < 1)
        return result;

    std::vector<AxisTickGenerator::Tick> add;

    if (FP::defined(getRawMin()) && getRawMin() < boundLower - 0.1) {
        add.emplace_back(AxisTickGeneratorTimeCycle::getTotalTick(interval, division));
        add.back().point = boundLower - 0.5;
    }

    if (FP::defined(getRawMax()) && getRawMax() > boundUpper + 0.1) {
        add.emplace_back(AxisTickGeneratorTimeCycle::getTotalTick(interval, division));
        add.back().point = boundUpper + 0.5;
    }

    if (!add.empty()) {
        if (result.size() < 1) {
            result.emplace_back(std::move(add));
        } else {
            Util::append(std::move(add), result.front());
            std::sort(result[0].begin(), result[0].end(), tickSort);
        }
    }

    return result;
}

bool AxisDimensionCyclePlot2D::enableBoundsModification() const
{ return false; }

void AxisDimensionCyclePlot2D::setPlotParameters(const TimeCycleInterval &interval,
                                                 const TimeCycleDivision &division)
{
    this->interval = interval;
    this->division = division;
    if (!parameters->hasTickGenerator()) {
        parameters->setTickGenerator(std::shared_ptr<AxisTickGenerator>(
                new AxisTickGeneratorTimeCycle(interval, division)));
    }
    if (!parameters->hasTitleText()) {
        parameters->setTitleText(AxisTickGeneratorTimeCycle::getTitle(interval, division,
                                                                      AxisTickGeneratorTimeCycle::Title_Axis));
    }
}

std::unique_ptr<AxisDimension> AxisDimensionCyclePlot2D::clone() const
{ return std::unique_ptr<AxisDimension>(new AxisDimensionCyclePlot2D(*this, parameters->clone())); }

std::vector<std::shared_ptr<DisplayCoupling>> AxisDimensionCyclePlot2D::createRangeCoupling(
        DisplayCoupling::CoupleMode mode)
{ return {std::make_shared<CyclePlotAxisCoupling>(mode, this)}; }

AxisDimensionCyclePlot2D::AxisDimensionCyclePlot2D(const AxisDimensionCyclePlot2D &other,
                                                   std::unique_ptr<AxisParameters> &&params)
        : AxisDimension2DSide(other, std::move(params)),
          parameters(static_cast<AxisParametersCyclePlot2D *>(getParameters()))
{ }

namespace Internal {

CyclePlotAxisCoupling::CyclePlotAxisCoupling()
{ }

CyclePlotAxisCoupling::CyclePlotAxisCoupling(DisplayCoupling::CoupleMode mode,
                                             AxisDimensionCyclePlot2D *setParent)
        : DisplayRangeCoupling(mode), parent(setParent)
{ }

CyclePlotAxisCoupling::~CyclePlotAxisCoupling()
{ }

double CyclePlotAxisCoupling::getMin() const
{
    if (parent.isNull()) return FP::undefined();
    return parent->getRawMin();
}

double CyclePlotAxisCoupling::getMax() const
{
    if (parent.isNull()) return FP::undefined();
    return parent->getRawMax();
}

bool CyclePlotAxisCoupling::canCouple(const std::shared_ptr<DisplayCoupling> &other) const
{
    if (parent.isNull())
        return false;
    auto sa = qobject_cast<CyclePlotAxisCoupling *>(other.get());
    if (!sa)
        return false;
    if (parent->interval.getUnits() != sa->parent->interval.getUnits())
        return false;
    if (parent->interval.getCount() != sa->parent->interval.getCount())
        return false;
    if (parent->interval.getAlign() != sa->parent->interval.getAlign())
        return false;
    if (parent->division.getUnits() != sa->parent->division.getUnits())
        return false;
    if (parent->division.getCount() != sa->parent->division.getCount())
        return false;
    return true;
}

void CyclePlotAxisCoupling::applyCoupling(const QVariant &data,
                                          DisplayCoupling::CouplingPosition position)
{
    if (parent.isNull())
        return;
    double setMin = FP::undefined();
    double setMax = FP::undefined();
    QList<QVariant> list(data.toList());
    Q_ASSERT(list.size() > 1);
    if (!list.at(0).isNull())
        setMin = list.at(0).toDouble();
    if (!list.at(1).isNull())
        setMax = list.at(1).toDouble();
    parent->couplingUpdated(setMin, setMax, position);
}

CyclePlotAxisZoom::CyclePlotAxisZoom()
{ }

CyclePlotAxisZoom::CyclePlotAxisZoom(AxisDimensionCyclePlot2D *setDimension,
                                     AxisDimensionSetCyclePlot2D *setParent) : AxisDimensionZoom(
        setDimension, setParent), dimension(setDimension), parent(setParent)
{ }

QString CyclePlotAxisZoom::getTitle() const
{
    if (dimension.isNull())
        return tr("Cycle");
    return AxisTickGeneratorTimeCycle::getTitle(dimension->interval, dimension->division,
                                                AxisTickGeneratorTimeCycle::Title_Units);
}

bool CyclePlotAxisZoom::sharedZoom(const std::shared_ptr<DisplayZoomAxis> &other) const
{
    if (parent.isNull())
        return false;
    if (dimension.isNull())
        return false;
    auto sa = qobject_cast<CyclePlotAxisZoom *>(other.get());
    if (!sa)
        return false;
    if (sa->parent.isNull())
        return false;
    if (sa->dimension.isNull())
        return false;
    if (dimension->interval.getUnits() != sa->dimension->interval.getUnits())
        return false;
    if (dimension->interval.getCount() != sa->dimension->interval.getCount())
        return false;
    if (dimension->interval.getAlign() != sa->dimension->interval.getAlign())
        return false;
    if (dimension->division.getUnits() != sa->dimension->division.getUnits())
        return false;
    if (dimension->division.getCount() != sa->dimension->division.getCount())
        return false;
    return true;
}

DisplayZoomAxis::DisplayMode CyclePlotAxisZoom::getDisplayMode() const
{ return DisplayZoomAxis::Display_Decimal; }

}

AxisDimensionSetCyclePlot2D::AxisDimensionSetCyclePlot2D() : AxisDimensionSet2DSide(false)
{ }

AxisDimensionSetCyclePlot2D::~AxisDimensionSetCyclePlot2D()
{ }

std::unique_ptr<AxisDimensionSet> AxisDimensionSetCyclePlot2D::clone() const
{ return std::unique_ptr<AxisDimensionSet>(new AxisDimensionSetCyclePlot2D(*this)); }

AxisDimension *AxisDimensionSetCyclePlot2D::get(const std::unordered_set<QString> &,
                                                const std::unordered_set<QString> &)
{ return AxisDimensionSet2DSide::get({}, {}, "Time"); }

AxisDimension *AxisDimensionSetCyclePlot2D::get(const std::unordered_set<QString> &,
                                                const std::unordered_set<QString> &,
                                                const QString &)
{ return AxisDimensionSet2DSide::get({}, {}, "Time"); }

void AxisDimensionSetCyclePlot2D::setPlotParameters(const TimeCycleInterval &interval,
                                                    const TimeCycleDivision &division)
{
    this->interval = interval;
    this->division = division;
    for (auto it : getAllDimensions()) {
        static_cast<AxisDimensionCyclePlot2D *>(it)->setPlotParameters(interval, division);
    }
}

AxisDimensionSetCyclePlot2D::AxisDimensionSetCyclePlot2D(const AxisDimensionSetCyclePlot2D &other) = default;

std::unique_ptr<
        AxisParameters> AxisDimensionSetCyclePlot2D::parseParameters(const Variant::Read &value,
                                                                     QSettings *settings) const
{ return std::unique_ptr<AxisParameters>(new AxisParametersCyclePlot2D(value, settings)); }

std::unique_ptr<AxisDimension> AxisDimensionSetCyclePlot2D::createDimension(std::unique_ptr<
        AxisParameters> &&parameters) const
{
    std::unique_ptr<AxisDimensionCyclePlot2D> dimension(new AxisDimensionCyclePlot2D(
            std::unique_ptr<AxisParametersCyclePlot2D>(
                    static_cast<AxisParametersCyclePlot2D *>(parameters.release()))));
    dimension->setPlotParameters(interval, division);
    return std::move(dimension);
}

std::shared_ptr<AxisDimensionZoom> AxisDimensionSetCyclePlot2D::createZoom(AxisDimension *dimension)
{
    return std::shared_ptr<AxisDimensionZoom>(
            new CyclePlotAxisZoom(static_cast<AxisDimensionCyclePlot2D *>(dimension), this));
}

CyclePlot2D::~CyclePlot2D() = default;

CyclePlot2D::CyclePlot2D(const DisplayDefaults &defaults, QObject *parent) : Graph2D(defaults,
                                                                                     parent),
                                                                             settings(
                                                                                     defaults.getSettings())
{ }

CyclePlot2D::CyclePlot2D(const CyclePlot2D &other) = default;

std::unique_ptr<Display> CyclePlot2D::clone() const
{ return std::unique_ptr<Display>(new CyclePlot2D(*this)); }

std::unique_ptr<TraceSet2D> CyclePlot2D::createTraces()
{
    std::unique_ptr<CyclePlotTraceSet2D> result(new CyclePlotTraceSet2D);
    result->setPlotParameters(static_cast<CyclePlot2DParameters *>(getParameters())->getInterval(),
                              static_cast<CyclePlot2DParameters *>(getParameters())->getDivision());
    return std::move(result);
}

std::unique_ptr<BinSet2D> CyclePlot2D::createXBins()
{
    std::unique_ptr<CyclePlotBinSet2D> result(new CyclePlotBinSet2D);
    result->setPlotParameters(static_cast<CyclePlot2DParameters *>(getParameters())->getInterval(),
                              static_cast<CyclePlot2DParameters *>(getParameters())->getDivision());
    return std::move(result);
}

std::unique_ptr<BinSet2D> CyclePlot2D::createYBins()
{ return std::unique_ptr<BinSet2D>(new BinSet2DNOOP); }

std::unique_ptr<IndicatorSet2D> CyclePlot2D::createXIndicators()
{
    std::unique_ptr<CyclePlotIndicatorSet2D> result(new CyclePlotIndicatorSet2D);
    result->setPlotParameters(static_cast<CyclePlot2DParameters *>(getParameters())->getInterval(),
                              static_cast<CyclePlot2DParameters *>(getParameters())->getDivision());
    return std::move(result);
}

std::unique_ptr<AxisDimensionSet2DSide> CyclePlot2D::createXAxes()
{
    std::unique_ptr<AxisDimensionSetCyclePlot2D> result(new AxisDimensionSetCyclePlot2D);
    result->setPlotParameters(static_cast<CyclePlot2DParameters *>(getParameters())->getInterval(),
                              static_cast<CyclePlot2DParameters *>(getParameters())->getDivision());
    return std::move(result);
}

std::unique_ptr<Graph2DParameters> CyclePlot2D::parseParameters(const Variant::Read &config,
                                                                const DisplayDefaults &defaults)
{ return std::unique_ptr<Graph2DParameters>(new CyclePlot2DParameters(config, defaults)); }

std::vector<GraphPainter2DHighlight::Region> CyclePlot2D::translateHighlightTrace(
        DisplayHighlightController::HighlightType type,
        double start,
        double end,
        double min,
        double max,
        TraceHandler2D *trace,
        AxisDimension2DSide *x,
        AxisDimension2DSide *y,
        AxisDimension2DColor *z,
        const std::unordered_set<std::size_t> &affectedDimensions)
{
    auto dims = affectedDimensions;
    dims.erase(0);
    return Graph2D::translateHighlightTrace(type, start, end, min, max, trace, x, y, z, dims);
}

std::vector<GraphPainter2DHighlight::Region> CyclePlot2D::translateHighlightXBin(
        DisplayHighlightController::HighlightType type,
        double start,
        double end,
        double min,
        double max,
        BinHandler2D *bins,
        AxisDimension2DSide *x,
        AxisDimension2DSide *y,
        const std::unordered_set<std::size_t> &affectedDimensions)
{
    auto dims = affectedDimensions;
    dims.erase(0);
    return Graph2D::translateHighlightXBin(type, start, end, min, max, bins, x, y, dims);
}

QString CyclePlot2D::convertMouseoverToolTipTrace(TraceHandler2D *trace,
                                                  TraceSet2D *traces,
                                                  AxisDimension2DSide *,
                                                  AxisDimension2DSide *y,
                                                  AxisDimension2DColor *z,
                                                  const TracePoint<3> &point,
                                                  const DisplayDynamicContext &context)
{
    QString text;

    if (FP::defined(point.d[0])) {
        text.append(tr("%1 %2", "trace time position").arg(QString::number(point.d[0]),
                                                           AxisTickGeneratorTimeCycle::getTitle(
                                                                   getInterval(), getDivision(),
                                                                   AxisTickGeneratorTimeCycle::Title_Units)));
    }
    for (std::size_t i = 1; i < 3; i++) {
        double value = point.d[i];
        if (!FP::defined(value))
            break;
        NumberFormat format(trace->getFormat(i));

        QString unitSuffix;
        switch (i) {
        case 1:
            if (y)
                unitSuffix = y->getToolTipUnitOverride();
            break;
        case 2:
            if (z)
                unitSuffix = z->getToolTipUnitOverride();
            break;
        default:
            break;
        }
        if (unitSuffix.isEmpty()) {
            auto sortedUnits = Util::to_qstringlist(trace->getUnits(i));
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(value, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(value, QChar()),
                                                                   unitSuffix));
        }
    }

    QString origin(traces->handlerOrigin(trace, context));
    if (!origin.isEmpty()) {
        origin = tr("%1<br>", "origin insert format").arg(origin);
    }

    text.prepend(
            tr("<font color=\"%2\">%1</font><br>%3At %4 to %5<br><br>", "trace name format").arg(
                    traces->handlerTitle(trace, context), trace->getColor().name(), origin,
                    GUITime::formatTime(point.start, settings, false),
                    GUITime::formatTime(point.end, settings, false)));

    return text;
}

QString CyclePlot2D::convertMouseoverToolTipBins(BinHandler2D *bins,
                                                 BinSet2D *binSet,
                                                 bool,
                                                 AxisDimension2DSide *,
                                                 AxisDimension2DSide *y,
                                                 const BinPoint<1, 1> &point,
                                                 Graph2DMouseoverComponentTraceBox::Location location,
                                                 const DisplayDynamicContext &context)
{
    QString origin(binSet->handlerOrigin(bins, context));
    if (!origin.isEmpty()) {
        origin = tr("%1<br>", "origin insert format").arg(origin);
    }

    QString text(tr("<font color=\"%2\">%1</font><br>%3", "bin name format").arg(
            binSet->handlerTitle(bins, context), bins->getColor().name(), origin));

    if (FP::defined(point.center[0]) && FP::defined(point.width[0]) && point.width[0] > 0.0) {
        text.append(tr("At %1 to %2 %3<br>", "center with width").arg(
                QString::number(point.center[0] - point.width[0] * 0.5),
                QString::number(point.center[0] + point.width[0] * 0.5),
                AxisTickGeneratorTimeCycle::getTitle(getInterval(), getDivision(),
                                                     AxisTickGeneratorTimeCycle::Title_Units)));
    } else if (FP::defined(point.center[0])) {
        text.append(tr("At %1 %2", "center with no width").arg(QString::number(point.center[0]),
                                                               AxisTickGeneratorTimeCycle::getTitle(
                                                                       getInterval(), getDivision(),
                                                                       AxisTickGeneratorTimeCycle::Title_Units)));
    }

    if (bins->isHistogram()) {
        text.append(tr("<br>%1", "histogram total").arg(Time::describeDuration(point.total[0])));
        if (!FP::defined(point.upper[0])) {
            text.append(
                    tr(", %1 %", "histogram percentage").arg(FP::decimalFormat(point.upper[0], 1)));
        }
    } else {
        double value = FP::undefined();
        switch (location) {
        case Graph2DMouseoverComponentTraceBox::Lowest:
            value = point.lowest[0];
            break;
        case Graph2DMouseoverComponentTraceBox::Lower:
            value = point.lower[0];
            break;
        case Graph2DMouseoverComponentTraceBox::Middle:
            value = point.middle[0];
            break;
        case Graph2DMouseoverComponentTraceBox::Upper:
            value = point.upper[0];
            break;
        case Graph2DMouseoverComponentTraceBox::Uppermost:
            value = point.uppermost[0];
            break;
        }
        if (FP::defined(value)) {
            NumberFormat format(bins->getFormat(1));
            QString unitSuffix;
            if (y)
                unitSuffix = y->getToolTipUnitOverride();
            if (unitSuffix.isEmpty()) {
                auto sortedUnits = Util::to_qstringlist(bins->getUnits(1));
                std::sort(sortedUnits.begin(), sortedUnits.end());
                unitSuffix = sortedUnits.join(tr(" ", "unit join"));
            }
            text.append(
                    tr("<br>%1%2", "bins combine").arg(format.apply(value, QChar()), unitSuffix));
        }
        if (FP::defined(point.total[0])) {
            text.append(tr("<br>%1 points", "bins combine").arg(
                    QString::number((int) qRound(point.total[0]))));
        }
    }

    return text;
}

QString CyclePlot2D::convertMouseoverToolTipIndicator(IndicatorHandler2D *indicator,
                                                      IndicatorSet2D *indicatorSet,
                                                      AxisDimension2DSide *axis,
                                                      const IndicatorPoint<1> &point,
                                                      Axis2DSide side,
                                                      const DisplayDynamicContext &context)
{
    QString origin(indicatorSet->handlerOrigin(indicator, context));
    if (!origin.isEmpty()) {
        origin = tr("%1<br>", "origin insert format").arg(origin);
    }

    QString text(tr("<font color=\"%2\">%1</font><br>%3At %4 to %5<br><br>",
                    "indicator name format").arg(indicatorSet->handlerTitle(indicator, context),
                                                 indicator->getColor().name(), origin,
                                                 GUITime::formatTime(point.start, settings, false),
                                                 GUITime::formatTime(point.end, settings, false)));

    switch (side) {
    case Axis2DLeft:
    case Axis2DRight: {
        double value = point.d[0];
        if (FP::defined(value)) {
            NumberFormat format(indicator->getFormat(0));
            QString unitSuffix;
            if (axis)
                unitSuffix = axis->getToolTipUnitOverride();
            if (unitSuffix.isEmpty()) {
                auto sortedUnits = Util::to_qstringlist(indicator->getUnits(0));
                std::sort(sortedUnits.begin(), sortedUnits.end());
                unitSuffix = sortedUnits.join(tr(" ", "unit join"));
            }
            if (!unitSuffix.isEmpty())
                unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
            text.append(tr("%1%2<br>", "coordinate combine first").arg(format.apply(value, QChar()),
                                                                       unitSuffix));
        }
        break;
    }
    case Axis2DTop:
    case Axis2DBottom:
        if (FP::defined(point.d[0])) {
            text.append(tr("%1 %2<br>", "indicator time position").arg(QString::number(point.d[0]),
                                                                       AxisTickGeneratorTimeCycle::getTitle(
                                                                               getInterval(),
                                                                               getDivision(),
                                                                               AxisTickGeneratorTimeCycle::Title_Units)));
        }
        break;
    }

    QString add(indicator->triggerToolip(point.trigger));
    if (!add.isEmpty()) {
        text.append(tr("%1", "indicator trigger format").arg(add));
    }

    return text;
}

QString CyclePlot2D::convertMouseoverToolTipFit(TraceHandler2D *trace,
                                                TraceSet2D *traces,
                                                AxisDimension2DSide *,
                                                AxisDimension2DSide *y,
                                                AxisDimension2DColor *z,
                                                double fx,
                                                double fy,
                                                double fz,
                                                double fi,
                                                const DisplayDynamicContext &context)
{
    QString text;

    if (FP::defined(fy)) {
        NumberFormat format(trace->getFormat(1));
        QString unitSuffix;
        if (y)
            unitSuffix = y->getToolTipUnitOverride();
        if (unitSuffix.isEmpty()) {
            auto sortedUnits = Util::to_qstringlist(trace->getUnits(1));
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(fy, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(fy, QChar()),
                                                                   unitSuffix));
        }
    }
    if (FP::defined(fz)) {
        NumberFormat format(trace->getFormat(2));
        QString unitSuffix;
        if (z)
            unitSuffix = z->getToolTipUnitOverride();
        if (unitSuffix.isEmpty()) {
            auto sortedUnits = Util::to_qstringlist(trace->getUnits(2));
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(fz, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(fz, QChar()),
                                                                   unitSuffix));
        }
    }
    if (FP::defined(fi)) {
        if (!text.isEmpty()) {
            text.append(tr(", %1", "fit I combine any").arg(fi));
        } else {
            text.append(tr("%1", "fit I combine first").arg(fi));
        }
    }

    QString title(static_cast<TraceParameters2D *>(
                          trace->getParameters())->getFitLegendText());
    if (title.isEmpty())
        title = traces->handlerTitle(trace, context);

    QString origin(traces->handlerOrigin(trace, context));
    if (!origin.isEmpty()) {
        origin = tr("%1<br>", "origin insert format").arg(origin);
    }

    if (FP::defined(fx)) {
        text.prepend(tr("<font color=\"%2\">%1</font><br>%3At %4 %5<br><br>",
                        "trace name format time").arg(title, trace->getFitColor().name(),
                                                      QString::number(fx), origin,
                                                      AxisTickGeneratorTimeCycle::getTitle(
                                                              getInterval(), getDivision(),
                                                              AxisTickGeneratorTimeCycle::Title_Units)));
    } else {
        text.prepend(tr("<font color=\"%2\">%1</font><br>%3<br>", "trace name format").arg(title,
                                                                                           trace->getFitColor()
                                                                                                .name(),
                                                                                           origin));
    }

    return text;
}

QString CyclePlot2D::convertMouseoverToolTipModelScan(TraceHandler2D *trace,
                                                      TraceSet2D *traces,
                                                      AxisDimension2DSide *,
                                                      AxisDimension2DSide *y,
                                                      AxisDimension2DColor *z,
                                                      double mx,
                                                      double my,
                                                      double mz,
                                                      const DisplayDynamicContext &context)
{
    QString text;

    if (FP::defined(my)) {
        NumberFormat format(trace->getFormat(1));
        QString unitSuffix;
        if (y)
            unitSuffix = y->getToolTipUnitOverride();
        if (unitSuffix.isEmpty()) {
            auto sortedUnits = Util::to_qstringlist(trace->getUnits(1));
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(my, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(my, QChar()),
                                                                   unitSuffix));
        }
    }
    if (FP::defined(mz)) {
        NumberFormat format(trace->getFormat(2));
        QString unitSuffix;
        if (z)
            unitSuffix = z->getToolTipUnitOverride();
        if (unitSuffix.isEmpty()) {
            auto sortedUnits = Util::to_qstringlist(trace->getUnits(2));
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(mz, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(mz, QChar()),
                                                                   unitSuffix));
        }
    }

    QString origin(traces->handlerOrigin(trace, context));
    if (!origin.isEmpty()) {
        origin = tr("%1<br>", "origin insert format").arg(origin);
    }

    if (FP::defined(mx)) {
        text.prepend(tr("%1<br>%2At %3 %4<br><br>", "fill name format time").arg(
                traces->handlerTitle(trace, context), QString::number(mx), origin,
                AxisTickGeneratorTimeCycle::getTitle(getInterval(), getDivision(),
                                                     AxisTickGeneratorTimeCycle::Title_Units)));
    } else {
        text.prepend(
                tr("%1<br>%2<br>", "fill name format").arg(traces->handlerTitle(trace, context),
                                                           origin));
    }

    return text;
}

QString CyclePlot2D::convertMouseoverToolTipFit(BinHandler2D *bins,
                                                BinSet2D *binSet,
                                                bool,
                                                AxisDimension2DSide *,
                                                AxisDimension2DSide *y,
                                                BinParameters2D::FitOrigin origin,
                                                double fx,
                                                double fy,
                                                const DisplayDynamicContext &context)
{
    QString text;

    if (FP::defined(fy)) {
        NumberFormat format(bins->getFormat(0));
        QString unitSuffix;
        if (y)
            unitSuffix = y->getToolTipUnitOverride();
        if (unitSuffix.isEmpty()) {
            auto sortedUnits = Util::to_qstringlist(bins->getUnits(0));
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(fy, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(fy, QChar()),
                                                                   unitSuffix));
        }
    }

    QString title(static_cast<BinParameters2D *>(
                          bins->getParameters())->getFitLegendText(origin));
    if (title.isEmpty())
        title = binSet->handlerTitle(bins, context);

    QString originText(binSet->handlerOrigin(bins, context));
    if (!originText.isEmpty()) {
        originText = tr("%1<br>", "origin insert format").arg(origin);
    }

    if (FP::defined(fx)) {
        text.prepend(
                tr("<font color=\"%2\">%1</font><br>%3At %4 %5<br><br>", "trace name format").arg(
                        title, bins->getFitColor(origin).name(), originText, QString::number(fx),
                        AxisTickGeneratorTimeCycle::getTitle(getInterval(), getDivision(),
                                                             AxisTickGeneratorTimeCycle::Title_Units)));
    } else {
        text.prepend(tr("<font color=\"%2\">%1</font><br>%3<br>", "trace name format").arg(title,
                                                                                           bins->getFitColor(
                                                                                                       origin)
                                                                                               .name(),
                                                                                           originText));
    }

    return text;
}

}
}
