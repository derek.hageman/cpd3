/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGINDICATOR_H
#define CPD3GRAPHINGINDICATOR_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QtAlgorithms>
#include <QObject>
#include <QList>
#include <QVector>
#include <QString>
#include <QSet>
#include <QRegularExpression>
#include <QStringList>
#include <QDebug>
#include <QDebugStateSaver>

#include "graphing/graphing.hxx"
#include "graphing/traceset.hxx"
#include "core/qtcompat.hxx"


namespace CPD3 {
namespace Graphing {

/**
 * A single point on indicator.
 *
 * @param N the number of dimensions
 */
template<std::size_t N>
struct IndicatorPoint : TracePoint<N> {
    /**
     * The triggering value.
     */
    Data::Variant::Read trigger;

    bool operator==(const IndicatorPoint<N> &other) const
    {
        if (!FP::equal(this->start, other.start))
            return false;
        if (!FP::equal(this->end, other.end))
            return false;
        for (std::size_t i = 0; i < N; i++) {
            if (!FP::equal(this->d[i], other.d[i]))
                return false;
        }
        return true;
    }

    bool operator!=(const IndicatorPoint<N> &other) const
    {
        return !(*this == other);
    }
};

template<int N>
QDebug operator<<(QDebug stream, const IndicatorPoint<N> &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "IndicatorPoint<" << N << ">(" << Logging::range(v.start, v.end) << ',' << v.trigger;
    for (int i = 0; i < N; i++) {
        stream << ',' << v.d[i];
    }
    stream << ')';
    return stream;
}

/**
 * The base class for all indicator parameters.
 */
class CPD3GRAPHING_EXPORT IndicatorParameters : public TraceParameters {
public:
    /**
     * The type of trigger used by the indicator.
     */
    enum TriggerType {
        /** Trigger when the value changes by more than a threshold. */
        Trigger_Delta, /** Trigger when the value changes by at least a threshold. */
        Trigger_DeltaEqual, /** Trigger when the value is not equal to the prior one. */
        Trigger_Changed, /** Trigger when the value is equal to the prior one. */
        Trigger_NotChanged, /** Trigger when a flag is present. */
        Trigger_Flag, /** Trigger when a flag is missing. */
        Trigger_NoFlag, /** Trigger when all flags are present. */
        Trigger_HasFlags, /** Trigger when all flags are missing. */
        Trigger_LacksFlags, /** Trigger when the value is greater than a threshold. */
        Trigger_Greater, /** Trigger when the value is greater than or equal to a threshold. */
        Trigger_GreaterEqual, /** Trigger when the value is less than a threshold. */
        Trigger_Less, /** Trigger when the value is less than or equal to a threshold. */
        Trigger_LessEqual, /** Trigger when the value is exactly equal to a specific value. */
        Trigger_Exactly, /** Trigger when the value bitwise AND'ed with a parameter is non-zero. */
        Trigger_AND, /** Trigger when the value bitwise XOR'ed with a parameter is non-zero. */
        Trigger_XOR, /** Trigger on any invalid value. */
        Trigger_Invalid, /** Trigger on any value that isn't a valid number. */
        Trigger_Undefined,
    };

private:
    enum {
        Component_Trigger = 0x0001,
        Component_Invert = 0x0002,
        Component_Continuity = 0x0004,
        Component_LatestOnly = 0x0008,
    };
    int components;

    TriggerType trigger;
    Data::Variant::Flags triggerStrings;
    double triggerDouble;
    qint64 triggerInteger;
    bool invert;
    double continuity;
    bool latestOnly;
public:
    IndicatorParameters();

    virtual ~IndicatorParameters();

    IndicatorParameters(const IndicatorParameters &other);

    IndicatorParameters &operator=(const IndicatorParameters &other);

    IndicatorParameters(IndicatorParameters &&other);

    IndicatorParameters &operator=(IndicatorParameters &&other);

    IndicatorParameters(const Data::Variant::Read &value);

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<TraceParameters> clone() const override;

    void overlay(const TraceParameters *over) override;

    void copy(const TraceParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify a trigger.
     * @return true if the parameters specify a trigger
     */
    inline bool hasTrigger() const
    { return components & Component_Trigger; }

    /**
     * Get the trigger type.
     * @return the trigger type
     */
    inline TriggerType getTriggerType() const
    { return trigger; }

    /**
     * Get the trigger strings parameter.
     * @return the trigger strings parameter
     */
    inline const Data::Variant::Flags &getTriggerStrings() const
    { return triggerStrings; }

    /**
     * Get the trigger double parameter.
     * @return the trigger double parameter
     */
    inline double getTriggerDouble() const
    { return triggerDouble; }

    /**
     * Get the trigger integer parameter.
     * @return the trigger integer parameter
     */
    inline qint64 getTriggerInteger() const
    { return triggerInteger; }


    /**
     * Test if the parameters specify the inversion state.
     * @return true if the parameters specify the inversion state
     */
    inline bool hasInvert() const
    { return components & Component_Invert; }

    /**
     * Get the inversion state.
     * @return true to invert the final output
     */
    inline bool getInvert() const
    { return invert; }

    /**
     * Test if the parameters specify a continuity threshold.
     * @return true if the components specify a continuity threshold.
     */
    inline bool hasContinuity() const
    { return components & Component_Continuity; }

    /**
     * Get the continuity threshold.
     * @return the continuity threshold
     */
    inline double getContinuity() const
    { return continuity; }

    /**
     * Test if the parameters specify the lasest only show state.
     * @return true if the parameters specify the lasest only show state
     */
    inline bool hasLatestOnly() const
    { return components & Component_LatestOnly; }

    /**
     * Get if only the latest true value should be shown.
     * @return true to show only the latest value
     */
    inline bool getLatestOnly() const
    { return latestOnly; }
};

/**
 * A handler for a single final output indicator.  This uses the "first"
 * dimension as the value indicator and the remainder as the output values.
 * 
 * @param N the number of dimensions
 */
template<std::size_t N>
class IndicatorValueHandler : public TraceValueHandlerComplex<N, IndicatorPoint<N> > {
    IndicatorParameters *parameters;

    double priorStartTime;
    double priorEndTime;
    Data::Variant::Read priorValue;

    Data::Variant::Flags parameterMatchedFlags;
    std::unordered_map<Data::Variant::Flag, QString> triggerFlagsDisplayText;
    QString triggerFormat;

    void resetParameterFlags()
    {
        parameterMatchedFlags.clear();
        for (const auto &pattern : parameters->getTriggerStrings()) {
            QString q = QString::fromStdString(pattern);
            if (QRegularExpression::escape(q) != q)
                continue;
            parameterMatchedFlags.insert(pattern);
        }
    }

    QString flagDisplayText(const Data::Variant::Flag &flag) const
    {
        auto check = triggerFlagsDisplayText.find(flag);
        if (check != triggerFlagsDisplayText.end())
            return check->second;
        return QString::fromStdString(flag);
    }

public:
    IndicatorValueHandler(std::unique_ptr<IndicatorParameters> &&params) : TraceValueHandlerComplex<
            N, IndicatorPoint<N>>(std::move(params)),
                                                                           parameters(
                                                                                   static_cast<IndicatorParameters *>(TraceValueHandlerComplex<
                                                                                           N,
                                                                                           IndicatorPoint<
                                                                                                   N>>::getParameters())),
                                                                           priorStartTime(
                                                                                   FP::undefined()),
                                                                           priorEndTime(
                                                                                   FP::undefined()),
                                                                           priorValue(
                                                                                   Data::Variant::Read::empty()),
                                                                           parameterMatchedFlags(),
                                                                           triggerFlagsDisplayText(),
                                                                           triggerFormat()
    {
        resetParameterFlags();
    }

    using Cloned = typename TraceValueHandlerComplex<N, IndicatorPoint<N> >::Cloned;

    virtual ~IndicatorValueHandler() = default;

    Cloned clone() const override
    { return Cloned(new IndicatorValueHandler<N>(*this, parameters->clone())); }

    void clear() override
    {
        TraceValueHandlerComplex<N, IndicatorPoint<N> >::clear();
        priorStartTime = FP::undefined();
        priorEndTime = FP::undefined();
        priorValue = Data::Variant::Read::empty();
        resetParameterFlags();
        triggerFlagsDisplayText.clear();
        triggerFormat = QString();
    }

    /**
     * Get the tooltip description for the triggering condition.
     * 
     * @param value     the value that triggered the indicator
     * @return          a string suitable for a tooltip describing the trigger
     */
    QString triggerToolip(const Data::Variant::Read &value) const
    {
        NumberFormat format(triggerFormat);
        switch (parameters->getTriggerType()) {
        case IndicatorParameters::Trigger_Delta:
            if (!parameters->getInvert()) {
                return TraceValueHandlerBase::tr("Value (%1) changed by more than %2.",
                                                 "trigger delta").arg(
                        format.apply(Data::Variant::Composite::toNumber(value), QChar()),
                        format.apply(parameters->getTriggerDouble(), QChar()));
            } else {
                return TraceValueHandlerBase::tr("Value (%1) did not change by more than %2.",
                                                 "trigger delta inverted").arg(
                        format.apply(Data::Variant::Composite::toNumber(value), QChar()),
                        format.apply(parameters->getTriggerDouble(), QChar()));
            }
        case IndicatorParameters::Trigger_DeltaEqual:
            if (!parameters->getInvert()) {
                return TraceValueHandlerBase::tr("Value (%1) changed by at least %2.",
                                                 "trigger delta equal").arg(
                        format.apply(Data::Variant::Composite::toNumber(value), QChar()),
                        format.apply(parameters->getTriggerDouble(), QChar()));
            } else {
                return TraceValueHandlerBase::tr("Value (%1) did not change by at least %2.",
                                                 "trigger delta equal inverted").arg(
                        format.apply(Data::Variant::Composite::toNumber(value), QChar()),
                        format.apply(parameters->getTriggerDouble(), QChar()));
            }
        case IndicatorParameters::Trigger_Changed:
            if (!parameters->getInvert()) {
                return TraceValueHandlerBase::tr("Value changed.", "trigger changed");
            } else {
                return TraceValueHandlerBase::tr("Value did not change.",
                                                 "trigger changed inverted");
            }
        case IndicatorParameters::Trigger_NotChanged:
            if (!parameters->getInvert()) {
                return TraceValueHandlerBase::tr("Value did not change.", "trigger not changed");
            } else {
                return TraceValueHandlerBase::tr("Value change.", "trigger not changed inverted");
            }

        case IndicatorParameters::Trigger_Flag: {
            if (parameters->getInvert())
                return TraceValueHandlerBase::tr("Missing required flags.",
                                                 "trigger flag inverted");
            QStringList sorted;
            for (const auto &check : value.toFlags()) {
                if (parameterMatchedFlags.count(check)) {
                    sorted.append(flagDisplayText(check));
                }
            }
            std::sort(sorted.begin(), sorted.end());
            for (QStringList::iterator mod = sorted.begin(), endMod = sorted.end();
                    mod != endMod;
                    ++mod) {
                *mod = TraceValueHandlerBase::tr("<li>%1</li>", "flag list item").arg(*mod);
            }

            return TraceValueHandlerBase::tr("Flags present: <ul>%1</ul>", "trigger flags").arg(
                    sorted.join(""));
        }

        case IndicatorParameters::Trigger_NoFlag: {
            if (parameters->getInvert()) {
                return TraceValueHandlerBase::tr("All required flags present.",
                                                 "trigger no flag inverted");
            }
            const auto &flags = value.toFlags();
            QStringList sorted;
            for (const auto &check : parameterMatchedFlags) {
                if (!flags.count(check)) {
                    sorted.append(flagDisplayText(check));
                }
            }
            std::sort(sorted.begin(), sorted.end());
            for (QStringList::iterator mod = sorted.begin(), endMod = sorted.end();
                    mod != endMod;
                    ++mod) {
                *mod = TraceValueHandlerBase::tr("<li>%1</li>", "flag list item").arg(*mod);
            }

            return TraceValueHandlerBase::tr("Flags missing: <ul>%1</ul>", "trigger no flags").arg(
                    sorted.join(""));
        }

        case IndicatorParameters::Trigger_HasFlags: {
            if (parameters->getInvert()) {
                return TraceValueHandlerBase::tr("Missing all required flags.",
                                                 "trigger has flags inverted");
            }
            QStringList sorted;
            for (const auto &check : value.toFlags()) {
                if (parameterMatchedFlags.count(check)) {
                    sorted.append(flagDisplayText(check));
                }
            }
            std::sort(sorted.begin(), sorted.end());
            for (QStringList::iterator mod = sorted.begin(), endMod = sorted.end();
                    mod != endMod;
                    ++mod) {
                *mod = TraceValueHandlerBase::tr("<li>%1</li>", "flag list item").arg(*mod);
            }

            return TraceValueHandlerBase::tr("Flags present: <ul>%1</ul>", "trigger flags").arg(
                    sorted.join(""));
        }

        case IndicatorParameters::Trigger_LacksFlags: {
            if (parameters->getInvert()) {
                return TraceValueHandlerBase::tr("All required flags present.",
                                                 "trigger lacks flag inverted");
            }
            const auto &flags = value.toFlags();
            QStringList sorted;
            for (const auto &check : parameterMatchedFlags) {
                if (!flags.count(check)) {
                    sorted.append(flagDisplayText(check));
                }
            }
            std::sort(sorted.begin(), sorted.end());
            for (QStringList::iterator mod = sorted.begin(), endMod = sorted.end();
                    mod != endMod;
                    ++mod) {
                *mod = TraceValueHandlerBase::tr("<li>%1</li>", "flag list item").arg(*mod);
            }

            return TraceValueHandlerBase::tr("Flags missing: <ul>%1</ul>", "trigger no flags").arg(
                    sorted.join(""));
        }

        case IndicatorParameters::Trigger_Greater:
            if (!parameters->getInvert()) {
                return TraceValueHandlerBase::tr("Value (%1) is greater than %2.",
                                                 "trigger greater").arg(
                        format.apply(Data::Variant::Composite::toNumber(value), QChar()),
                        format.apply(parameters->getTriggerDouble(), QChar()));
            } else {
                return TraceValueHandlerBase::tr("Value (%1) is less than or equal to %2.",
                                                 "trigger greater invert").arg(
                        format.apply(Data::Variant::Composite::toNumber(value), QChar()),
                        format.apply(parameters->getTriggerDouble(), QChar()));
            }

        case IndicatorParameters::Trigger_GreaterEqual:
            if (!parameters->getInvert()) {
                return TraceValueHandlerBase::tr("Value (%1) is greater than or equal to %2.",
                                                 "trigger greater").arg(
                        format.apply(Data::Variant::Composite::toNumber(value), QChar()),
                        format.apply(parameters->getTriggerDouble(), QChar()));
            } else {
                return TraceValueHandlerBase::tr("Value (%1) is less than to %2.",
                                                 "trigger greater invert").arg(
                        format.apply(Data::Variant::Composite::toNumber(value), QChar()),
                        format.apply(parameters->getTriggerDouble(), QChar()));
            }

        case IndicatorParameters::Trigger_Less:
            if (!parameters->getInvert()) {
                return TraceValueHandlerBase::tr("Value (%1) is less than %2.", "trigger less").arg(
                        format.apply(Data::Variant::Composite::toNumber(value), QChar()),
                        format.apply(parameters->getTriggerDouble(), QChar()));
            } else {
                return TraceValueHandlerBase::tr("Value (%1) is greater than or equal to %2.",
                                                 "trigger less invert").arg(
                        format.apply(Data::Variant::Composite::toNumber(value), QChar()),
                        format.apply(parameters->getTriggerDouble(), QChar()));
            }

        case IndicatorParameters::Trigger_LessEqual:
            if (!parameters->getInvert()) {
                return TraceValueHandlerBase::tr("Value (%1) is less than or equal to %2.",
                                                 "trigger less").arg(
                        format.apply(Data::Variant::Composite::toNumber(value), QChar()),
                        format.apply(parameters->getTriggerDouble(), QChar()));
            } else {
                return TraceValueHandlerBase::tr("Value (%1) is greater than to %2.",
                                                 "trigger less invert").arg(
                        format.apply(Data::Variant::Composite::toNumber(value), QChar()),
                        format.apply(parameters->getTriggerDouble(), QChar()));
            }

        case IndicatorParameters::Trigger_Exactly:
            if (!parameters->getInvert()) {
                return TraceValueHandlerBase::tr("Value is equal to %1.", "trigger exactly").arg(
                        format.apply(parameters->getTriggerDouble(), QChar()));
            } else {
                return TraceValueHandlerBase::tr("Value (%1) is not equal to %2.",
                                                 "trigger exactly invert").arg(
                        format.apply(Data::Variant::Composite::toNumber(value), QChar()),
                        format.apply(parameters->getTriggerDouble(), QChar()));
            }

        case IndicatorParameters::Trigger_AND:
            if (!parameters->getInvert()) {
                return TraceValueHandlerBase::tr("Value (%1) contains any of the bits in %2.",
                                                 "trigger AND").arg(format.apply(value.toInt64()),
                                                                    format.apply(
                                                                            parameters->getTriggerInteger()));
            } else {
                return TraceValueHandlerBase::tr("Value (%1) contains any none of the bits in %2.",
                                                 "trigger AND").arg(format.apply(value.toInt64()),
                                                                    format.apply(
                                                                            parameters->getTriggerInteger()));
            }

        case IndicatorParameters::Trigger_XOR:
            if (!parameters->getInvert()) {
                return TraceValueHandlerBase::tr("Value (%1) differs from %2.", "trigger XOR").arg(
                        format.apply(value.toInt64()),
                        format.apply(parameters->getTriggerInteger()));
            } else {
                return TraceValueHandlerBase::tr("Value does not differ from %1.",
                                                 "trigger XOR inverted").arg(
                        format.apply(parameters->getTriggerInteger()));
            }

        case IndicatorParameters::Trigger_Invalid:
            if (!parameters->getInvert()) {
                return TraceValueHandlerBase::tr("Value is invalid.", "trigger invalid");
            } else {
                return TraceValueHandlerBase::tr("Value is valid.", "trigger invalid inverted");
            }

        case IndicatorParameters::Trigger_Undefined:
            if (!parameters->getInvert()) {
                return TraceValueHandlerBase::tr("Value is undefined.", "trigger undefined");
            } else {
                return TraceValueHandlerBase::tr("Value is defined.", "trigger undefined inverted");
            }
        }
        return QString();
    }

    bool process(const std::vector<TraceDispatch::OutputValue> &incoming,
                 bool deferrable = false) override
    {
        if (!TraceValueHandlerComplex<N, IndicatorPoint<N> >::process(incoming, deferrable))
            return false;
        if (parameters->getLatestOnly())
            TraceValueHandlerComplex<N, IndicatorPoint<N> >::trimToLast();
        return true;
    }

protected:
    IndicatorValueHandler(const IndicatorValueHandler &other,
                          std::unique_ptr<TraceParameters> &&params) : TraceValueHandlerComplex<N,
                                                                                                IndicatorPoint<
                                                                                                        N>>(
            other, std::move(params)),
                                                                       parameters(
                                                                               static_cast<IndicatorParameters *>(TraceValueHandlerComplex<
                                                                                       N,
                                                                                       IndicatorPoint<
                                                                                               N>>::getParameters())),
                                                                       priorStartTime(
                                                                               other.priorStartTime),
                                                                       priorEndTime(
                                                                               other.priorEndTime),
                                                                       priorValue(other.priorValue),
                                                                       parameterMatchedFlags(
                                                                               other.parameterMatchedFlags),
                                                                       triggerFlagsDisplayText(
                                                                               other.triggerFlagsDisplayText),
                                                                       triggerFormat(
                                                                               other.triggerFormat)
    { }

    /**
     * Advance the prior value, returning the last one, if valid.
     * 
     * @param start     the new start time
     * @param end       the new end time
     * @param current   the new value
     */
    Data::Variant::Read advancePriorValue(double start,
                                          double end,
                                          const Data::Variant::Read &current)
    {
        if (!priorValue.exists()) {
            priorStartTime = start;
            priorEndTime = end;
            priorValue = current;
            priorValue.detachFromRoot();
            return Data::Variant::Read::empty();
        }
        if (!FP::defined(parameters->getContinuity())) {
            auto old = std::move(priorValue);
            priorStartTime = start;
            priorEndTime = end;
            priorValue = current;
            priorValue.detachFromRoot();
            return old;
        }
        if (!FP::defined(priorStartTime)) {
            priorStartTime = start;
            priorEndTime = end;
            priorValue = current;
            priorValue.detachFromRoot();
            return Data::Variant::Read::empty();
        }
        Q_ASSERT(FP::defined(start));
        Q_ASSERT(FP::defined(priorStartTime));
        Q_ASSERT(start >= priorStartTime);
        if (FP::defined(priorEndTime) && (start - priorEndTime) > parameters->getContinuity()) {
            priorStartTime = start;
            priorEndTime = end;
            priorValue = current;
            priorValue.detachFromRoot();
            return Data::Variant::Read::empty();
        }
        auto old = std::move(priorValue);
        priorStartTime = start;
        priorEndTime = end;
        priorValue = current;
        priorValue.detachFromRoot();
        return old;
    }

    /**
     * Convert a single true indicator value to the final output value.  This
     * only needs to set the dimensions of it, the rest is handled by the
     * caller.  The dimensions of data start at index one (the first is
     * the trigger value).
     * 
     * @param value     the value to convert
     * @param add       the output value
     */
    virtual void convertIndicatorValue(const TraceDispatch::OutputValue &value,
                                       IndicatorPoint<N> &add)
    {
        for (std::size_t i = 0; i < N; i++) {
            add.d[i] = Data::Variant::Composite::toNumber(value.get(i + 1));
        }
    }

    /**
     * Update the metadata for the triggering dimension.
     * 
     * @param value     triggering dimension the metadata
     */
    void updateTriggerMetadata(const Data::Variant::Read &value)
    {
        switch (parameters->getTriggerType()) {
        case IndicatorParameters::Trigger_Flag:
        case IndicatorParameters::Trigger_NoFlag:
        case IndicatorParameters::Trigger_HasFlags:
        case IndicatorParameters::Trigger_LacksFlags: {
            if (value.getType() != Data::Variant::Type::MetadataFlags)
                break;
            std::vector<QRegularExpression> triggers;
            for (const auto &pattern : parameters->getTriggerStrings()) {
                QString q = QString::fromStdString(pattern);
                if (QRegularExpression::escape(q) == q)
                    continue;
                triggers.emplace_back(std::move(q));
            }
            for (auto check : value.toMetadataSingleFlag()) {
                QString desc(check.second.hash("Description").toDisplayString());
                if (!desc.isEmpty())
                    triggerFlagsDisplayText.emplace(check.first, std::move(desc));

                for (const auto &re : triggers) {
                    if (!Util::exact_match(QString::fromStdString(check.first), re))
                        continue;
                    parameterMatchedFlags.insert(check.first);
                    break;
                }
            }

            break;
        }
        default:
            break;
        }

        QString format(value.metadata("Format").toDisplayString());
        if (!format.isEmpty())
            triggerFormat = format;
    }

    void convertValue(const TraceDispatch::OutputValue &value,
                      std::vector<IndicatorPoint<N> > &points) override
    {
        if (value.isMetadata()) {
            updateTriggerMetadata(value.get(0));
            for (std::size_t i = 0; i < N; i++) {
                TraceValueHandlerComplex<N, IndicatorPoint<N> >::updateMetadata(i,
                                                                                value.get(i + 1));
            }
            return;
        }

        switch (parameters->getTriggerType()) {
        case IndicatorParameters::Trigger_Delta:
        case IndicatorParameters::Trigger_DeltaEqual: {
            double prior = Data::Variant::Composite::toNumber(
                    advancePriorValue(value.getStart(), value.getEnd(), value.get(0)));
            double current = Data::Variant::Composite::toNumber(value.get(0));
            if (!FP::defined(prior) || !FP::defined(current))
                return;
            double delta = fabs(current - prior);
            if (delta < parameters->getTriggerDouble()) {
                if (!parameters->getInvert())
                    return;
            } else if (parameters->getTriggerType() == IndicatorParameters::Trigger_Delta &&
                    delta == parameters->getTriggerDouble()) {
                if (!parameters->getInvert())
                    return;
            } else if (parameters->getInvert()) {
                return;
            }
            break;
        }
        case IndicatorParameters::Trigger_Changed: {
            auto prior = advancePriorValue(value.getStart(), value.getEnd(), value.get(0));
            if (!prior.exists() || !value.get(0).exists())
                return;
            if (prior == value.get(0)) {
                if (!parameters->getInvert())
                    return;
            } else if (parameters->getInvert()) {
                return;
            }
            break;
        }
        case IndicatorParameters::Trigger_NotChanged: {
            auto prior = advancePriorValue(value.getStart(), value.getEnd(), value.get(0));
            if (!prior.exists() || !value.get(0).exists())
                return;
            if (prior != value.get(0)) {
                if (!parameters->getInvert())
                    return;
            } else if (parameters->getInvert()) {
                return;
            }
            break;
        }

        case IndicatorParameters::Trigger_Flag: {
            if (value.get(0).getType() != Data::Variant::Type::Flags)
                return;
            bool hit = false;
            for (const auto &check : value.get(0).toFlags()) {
                if (parameterMatchedFlags.count(check)) {
                    hit = true;
                    break;
                }
            }
            if (hit == parameters->getInvert())
                return;
            break;
        }

        case IndicatorParameters::Trigger_NoFlag: {
            if (value.get(0).getType() != Data::Variant::Type::Flags)
                return;
            const auto &flags = value.get(0).toFlags();
            bool missed = false;
            for (const auto &check : parameterMatchedFlags) {
                if (!flags.count(check)) {
                    missed = true;
                    break;
                }
            }
            if (missed == parameters->getInvert())
                return;
            break;
        }

        case IndicatorParameters::Trigger_HasFlags:
            if (value.get(0).getType() != Data::Variant::Type::Flags)
                return;
            if (!value.get(0).testFlags(parameterMatchedFlags)) {
                if (!parameters->getInvert())
                    return;
            } else if (parameters->getInvert()) {
                return;
            }
            break;

        case IndicatorParameters::Trigger_LacksFlags:
            if (value.get(0).getType() != Data::Variant::Type::Flags)
                return;
            if (value.get(0).testFlags(parameterMatchedFlags)) {
                if (!parameters->getInvert())
                    return;
            } else if (parameters->getInvert()) {
                return;
            }
            break;

        case IndicatorParameters::Trigger_Greater: {
            double v = Data::Variant::Composite::toNumber(value.get(0));
            if (!FP::defined(v))
                return;
            if (v <= parameters->getTriggerDouble()) {
                if (!parameters->getInvert())
                    return;
            } else if (parameters->getInvert()) {
                return;
            }
            break;
        }

        case IndicatorParameters::Trigger_GreaterEqual: {
            double v = Data::Variant::Composite::toNumber(value.get(0));
            if (!FP::defined(v))
                return;
            if (v < parameters->getTriggerDouble()) {
                if (!parameters->getInvert())
                    return;
            } else if (parameters->getInvert()) {
                return;
            }
            break;
        }

        case IndicatorParameters::Trigger_Less: {
            double v = Data::Variant::Composite::toNumber(value.get(0));
            if (!FP::defined(v))
                return;
            if (v >= parameters->getTriggerDouble()) {
                if (!parameters->getInvert())
                    return;
            } else if (parameters->getInvert()) {
                return;
            }
            break;
        }

        case IndicatorParameters::Trigger_LessEqual: {
            double v = Data::Variant::Composite::toNumber(value.get(0));
            if (!FP::defined(v))
                return;
            if (v > parameters->getTriggerDouble()) {
                if (!parameters->getInvert())
                    return;
            } else if (parameters->getInvert()) {
                return;
            }
            break;
        }

        case IndicatorParameters::Trigger_Exactly: {
            double v = Data::Variant::Composite::toNumber(value.get(0));
            if (!FP::defined(v))
                return;
            if (v != parameters->getTriggerDouble()) {
                if (!parameters->getInvert())
                    return;
            } else if (parameters->getInvert()) {
                return;
            }
            break;
        }

        case IndicatorParameters::Trigger_AND: {
            qint64 v = value.get(0).toInt64();
            if (!INTEGER::defined(v))
                return;
            if (!(v & parameters->getTriggerInteger())) {
                if (!parameters->getInvert())
                    return;
            } else if (parameters->getInvert()) {
                return;
            }
            break;
        }

        case IndicatorParameters::Trigger_XOR: {
            qint64 v = value.get(0).toInt64();
            if (!INTEGER::defined(v))
                return;
            if (!(v ^ parameters->getTriggerInteger())) {
                if (!parameters->getInvert())
                    return;
            } else if (parameters->getInvert()) {
                return;
            }
            break;
        }

        case IndicatorParameters::Trigger_Invalid:
            if (value.get(0).exists()) {
                if (!parameters->getInvert())
                    return;
            } else if (parameters->getInvert()) {
                return;
            }
            break;

        case IndicatorParameters::Trigger_Undefined:
            if (FP::defined(Data::Variant::Composite::toNumber(value.get(0)))) {
                if (!parameters->getInvert())
                    return;
            } else if (parameters->getInvert()) {
                return;
            }
            break;

        }

        IndicatorPoint<N> add;
        add.start = value.getStart();
        add.end = value.getEnd();
        add.trigger = value.get(0);
        add.trigger.detachFromRoot();

        convertIndicatorValue(value, add);
        points.emplace_back(std::move(add));
    }

    /* These are not as smart as they could be, but I'm not sure they're
     * useful for anything anyway. */
    QVector<Algorithms::TransformerPoint *> prepareTransformer(const std::vector<
            TraceDispatch::OutputValue> &points, Algorithms::TransformerParameters &) override
    {
        QVector<Algorithms::TransformerPoint *> result;
        result.reserve(points.size());
        for (const auto &it : points) {
            if (!Range::intersects(it.getStart(), it.getEnd(),
                                   TraceValueHandlerComplex<N, IndicatorPoint<N> >::getStart(),
                                   TraceValueHandlerComplex<N, IndicatorPoint<N> >::getEnd()))
                continue;

            if (N == 1) {
                result.append(new typename TraceValueHandlerComplex<N, IndicatorPoint<
                        N> >::template TransformValue<N + 1>(it, Data::Variant::Composite::toNumber(
                        it.get(0)), Data::Variant::Composite::toNumber(it.get(1))));
            } else if (N == 2) {
                result.append(new typename TraceValueHandlerComplex<N, IndicatorPoint<
                        N> >::template TransformValue<N + 1>(it, Data::Variant::Composite::toNumber(
                        it.get(0)), Data::Variant::Composite::toNumber(it.get(1)),
                                                             Data::Variant::Composite::toNumber(
                                                                     it.get(2))));
            } else {
                QVector<double> add;
                add.reserve(N + 1);
                for (int i = 0; i < N + 1; i++) {
                    add.append(Data::Variant::Composite::toNumber(it.get(i)));
                }
                result.append(new typename TraceValueHandlerComplex<N, IndicatorPoint<
                        N> >::template TransformValue<N + 1>(it, add));
            }
        }
        return result;
    }

    std::vector<IndicatorPoint<N>> convertTransformerResult(const QVector<
            Algorithms::TransformerPoint *> &points) override
    {
        std::vector<IndicatorPoint<N>> result;
        result.reserve(points.size());
        for (auto it : points) {
            auto point = static_cast<const typename TraceValueHandlerComplex<N, IndicatorPoint<
                    N> >::template TransformValue<N + 1> *>(it);
            convertValue(point->getValue(), result);
        }
        return result;
    }

    Algorithms::TransformerAllocator *getTransformerAllocator() override
    {
        return &TraceValueHandlerComplex<N, IndicatorPoint<N> >::template TransformValue<
                N + 1>::allocator;
    }
};

/**
 * A handler for a set of multiple indicator dispatches.  This produces a 
 * number of indicator value handlers that represent the actual indicator data.
 * 
 * @param N the number of dimensions
 */
template<std::size_t N>
class IndicatorDispatchSet : public TraceDispatchSetComplex<N, IndicatorValueHandler<N> > {
public:
    IndicatorDispatchSet() : TraceDispatchSetComplex<N, IndicatorValueHandler<N> >()
    { }

    virtual ~IndicatorDispatchSet()
    { }

    using Cloned = typename TraceDispatchSetComplex<N, IndicatorValueHandler<N> >::Cloned;

    using CreatedHandler = typename TraceDispatchSetComplex<N, IndicatorValueHandler<
            N> >::CreatedHandler;

    Cloned clone() const override
    { return Cloned(new IndicatorDispatchSet<N>(*this)); }

protected:
    IndicatorDispatchSet(const IndicatorDispatchSet<N> &other) : TraceDispatchSetComplex<N,
                                                                                         IndicatorValueHandler<
                                                                                                 N> >(
            other)
    { }

    std::unique_ptr<
            TraceParameters> parseParameters(const Data::Variant::Read &value) const override
    { return std::unique_ptr<TraceParameters>(new IndicatorParameters(value)); }

    CreatedHandler createHandler(std::unique_ptr<TraceParameters> &&parameters) const override
    {
        return CreatedHandler(new IndicatorValueHandler<N>(std::unique_ptr<IndicatorParameters>(
                static_cast<IndicatorParameters *>(parameters.release()))));
    }
};

}
}

#endif
