/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGGRAPH2D_H
#define CPD3GRAPHINGGRAPH2D_H

#include "core/first.hxx"

#include <vector>
#include <memory>
#include <mutex>
#include <QtGlobal>
#include <QObject>

#include "graphing/display.hxx"
#include "graphing/axis2d.hxx"
#include "graphing/trace2d.hxx"
#include "graphing/bin2d.hxx"
#include "graphing/indicator2d.hxx"
#include "graphing/graphcommon2d.hxx"
#include "graphing/graphpainters2d.hxx"
#include "graphing/tracecommon.hxx"
#include "core/sharedwork.hxx"

namespace CPD3 {
namespace Graphing {

/**
 * The base class for "simple" two dimensional graphs
 */
class CPD3GRAPHING_EXPORT Graph2D : public Display {
Q_OBJECT

    friend class Internal::Graph2DLegendModification;

    class TitleModificationComponent;

    friend class TitleModificationComponent;

    class InsetCoupling;

    friend class InsetCoupling;

    QSettings *settings;
    bool deferredChanged;
    QSizeF priorDimensions;
    QPointF priorOrigin;
    QRectF traceArea;
    bool havePrepared;
    bool haveFinalized;
    bool haveEverPrepared;
    bool haveEverFinalized;
    bool haveEverRendered;

    std::unique_ptr<TraceSet2D> traces;
    std::unique_ptr<BinSet2D> xBins;
    std::unique_ptr<BinSet2D> yBins;
    std::unique_ptr<IndicatorSet2D> xIndicators;
    std::unique_ptr<IndicatorSet2D> yIndicators;
    std::unique_ptr<AxisDimensionSet2DSide> xAxes;
    std::unique_ptr<AxisDimensionSet2DSide> yAxes;
    std::unique_ptr<AxisDimensionSet2DColor> zAxes;
    std::unique_ptr<Graph2DParameters> parameters;
    std::unique_ptr<Graph2DParameters> originalParameters;
    std::unique_ptr<Graph2DParameters> overridenParameters;

    enum {
        Ready_Traces = 0x0001,
        Ready_XBins = 0x0002,
        Ready_YBins = 0x0004,
        Ready_XIndicators = 0x0008,
        Ready_YIndicators = 0x0010,
        Ready_ALL =
        Ready_Traces | Ready_XBins | Ready_YBins | Ready_XIndicators | Ready_YIndicators,
    };
    int ready;

    QString titleText;
    Legend legend;

    struct HighlightRegion {
        DisplayHighlightController::HighlightType type;
        double start;
        double end;
        double min;
        double max;
        Data::SequenceMatch::Composite selection;
    };
    std::vector<HighlightRegion> highlights;

    struct TraceBinding {
        TraceHandler2D *trace;
        AxisDimension2DSide *x;
        AxisDimension2DSide *y;
        AxisDimension2DColor *z;
    };
    std::vector<TraceBinding> traceBindings;

    struct BinBinding {
        BinHandler2D *bins;
        AxisDimension2DSide *x;
        AxisDimension2DSide *y;
    };
    std::vector<BinBinding> xBinBindings;
    std::vector<BinBinding> yBinBindings;

    struct IndicatorBinding {
        IndicatorHandler2D *indicator;
        AxisDimension2DSide *axis;
    };
    std::vector<IndicatorBinding> xIndicatorBindings;
    std::vector<IndicatorBinding> yIndicatorBindings;

    qreal leftTotalInset;
    qreal rightTotalInset;
    qreal topTotalInset;
    qreal bottomTotalInset;

    qreal previousLeftTotalInset;
    qreal previousRightTotalInset;
    qreal previousTopTotalInset;
    qreal previousBottomTotalInset;

    qreal leftAxisSideDepth;
    qreal rightAxisSideDepth;
    qreal topAxisSideDepth;
    qreal bottomAxisSideDepth;

    bool mouseoverValid;
    bool mouseoverChanged;
    bool mouseoverDone;
    QPoint mouseoverPoint;

    enum MouseZoomMode {
        Zoom_None, Zoom_AutoX, Zoom_AutoY, Zoom_AutoBoth, Zoom_ForceX, Zoom_ForceY, Zoom_ForceBoth,
    };
    MouseZoomMode mouseZoomMode;
    QPoint mouseZoomA;
    QPoint mouseZoomB;
    MouseZoomMode pendingMouseZoom;

    class PrimitiveList {
        struct Private {
            std::mutex mutex;
            std::vector<std::unique_ptr<Graph2DDrawPrimitive>> list;
            QSizeF size;
            QPoint mouseover;

            Private(std::vector<std::unique_ptr<Graph2DDrawPrimitive>> &&list,
                    const QSizeF &setSize,
                    const QPoint &setMouseover);

            ~Private();
        };

        std::shared_ptr<Private> p;
    public:
        PrimitiveList();

        PrimitiveList(std::vector<std::unique_ptr<Graph2DDrawPrimitive>> &&list,
                      const QSizeF &size,
                      const QPoint &mouseover);

        PrimitiveList(const PrimitiveList &other);

        PrimitiveList &operator=(const PrimitiveList &other);

        void clear();

        bool isValid() const;

        const std::vector<std::unique_ptr<Graph2DDrawPrimitive>> &acquire() const;

        void release() const;

        QSizeF getSize() const;

        QPoint getMouseover() const;
    };

    friend class PrimitiveList;

    class DeferredPainter {
        QImage image;
    public:
        DeferredPainter();

        DeferredPainter(const DeferredPainter &other);

        DeferredPainter &operator=(const DeferredPainter &other);

        DeferredPainter(DeferredPainter &&other);

        DeferredPainter &operator=(DeferredPainter &&other);

        DeferredPainter(const PrimitiveList &list);

        QImage getImage() const;
    };

    friend class DeferredPainter;

    class MouseoverUpdate {
        std::shared_ptr<Graph2DMouseoverComponent> component;
    public:
        MouseoverUpdate();

        MouseoverUpdate(const MouseoverUpdate &other);

        MouseoverUpdate &operator=(const MouseoverUpdate &other);

        MouseoverUpdate(MouseoverUpdate &&other);

        MouseoverUpdate &operator=(MouseoverUpdate &&other);

        MouseoverUpdate(const PrimitiveList &list);

        std::shared_ptr<Graph2DMouseoverComponent> getComponent() const;
    };

    PrimitiveList deferredPaintList;
    SharedWorkHandler<DeferredPainter, PrimitiveList> deferredPaintHandler;

    PrimitiveList mouseoverUpdateList;
    SharedWorkHandler<MouseoverUpdate, PrimitiveList> mouseoverUpdateHandler;

    void revertOverrideParameters();

    Data::ValueSegment::Transfer getManualOverrides() const;

    void updateMouseZoomMode();

    void applyMouseZoom(const QPointF &origin);

    void connectComponents();

    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> makeDrawList(const QRectF &dimensions,
                                                                    QPaintDevice *paintdevice = nullptr);

    bool finalRender(QPainter *painter,
                     const QRectF &dimensions,
                     const DisplayDynamicContext &context);

    void drawMouseoverCross(QPainter *painter,
                            AxisDimension2DSide *x,
                            AxisDimension2DSide *y,
                            const QPointF &center);

    void renderMouseZoom(QPainter *painter, const QRectF &dimensions);

    void renderMouseover(QPainter *painter,
                         const QRectF &dimensions,
                         const DisplayDynamicContext &context,
                         Graph2DMouseoverComponent *component);

    void updateMouseoverTooltip(const MouseoverUpdate &mouseover,
                                const DisplayDynamicContext &context);

public:
    virtual ~Graph2D();

    Graph2D(const DisplayDefaults &defaults = {}, QObject *parent = nullptr);

    Graph2D(const Graph2D &other);

    std::unique_ptr<Display> clone() const override;

    /**
     * Get the parameters.
     * 
     * @return the parameters
     */
    inline Graph2DParameters *getParameters() const
    { return parameters.get(); }

    virtual void initialize(const Data::ValueSegment::Transfer &config,
                            const DisplayDefaults &defaults = {});

    /**
     * Test if two values representing configurations for the graphs
     * could be merged together into a single display.
     * 
     * @param a the first configuration
     * @param b the second configuration
     */
    static bool mergable(const Data::Variant::Read &a, const Data::Variant::Read &b);

    void registerChain(Data::ProcessingTapChain *chain) override;

    std::vector<std::shared_ptr<
            DisplayOutput>> getOutputs(const DisplayDynamicContext &context = {}) override;

    std::vector<std::shared_ptr<DisplayCoupling>> getDataCoupling() override;

    std::vector<std::shared_ptr<DisplayCoupling>> getLayoutCoupling() override;

    QSizeF getMinimumSize(QPaintDevice *paintdevice = nullptr,
                          const DisplayDynamicContext &context = {}) override;

    void prepareLayout(const QRectF &maximumDimensions,
                       QPaintDevice *paintdevice = nullptr,
                       const DisplayDynamicContext &context = {}) override;

    void predictLayout(const QRectF &maximumDimensions,
                       QPaintDevice *paintdevice = nullptr,
                       const DisplayDynamicContext &context = {}) override;

    void paint(QPainter *painter,
               const QRectF &dimensions,
               const DisplayDynamicContext &context = {}) override;

    bool eventMousePress(QMouseEvent *event) override;

    bool eventMouseRelease(QMouseEvent *event) override;

    bool eventMouseMove(QMouseEvent *event) override;

    bool eventKeyPress(QKeyEvent *event) override;

    bool eventKeyRelease(QKeyEvent *event) override;

    std::shared_ptr<DisplayModificationComponent> getMouseModification(const QPoint &point,
                                                                       const QRectF &dimensions = {},
                                                                       const DisplayDynamicContext &context = {}) override;

    bool acceptsMouseover() const override;

    void setMouseover(const QPoint &point) override;

    std::vector<std::shared_ptr<DisplayZoomAxis>> zoomAxes() override;

    std::shared_ptr<DisplayHighlightController> highlightController() override;

    void saveOverrides(DisplaySaveContext &target) override;

    /**
     * Clear all highlights.
     */
    virtual void clearHighlight();

    /**
     * Add a new highlighted range.
     * 
     * @param type      the type of this lighlight
     * @param start     the start time of the highlight or undefined for -infinity
     * @param end       the end time of the highlight or undefined for +infinity
     * @param min       the minimum value of the highlight or undefined for -infinity
     * @param max       the maximum value time of the highlight or undefined for +infinity
     * @param selection the selection of variables the highlight applies to
     */
    virtual void addHighlight(DisplayHighlightController::HighlightType type,
                              double start,
                              double end,
                              double min = FP::undefined(),
                              double max = FP::undefined(),
                              const Data::SequenceMatch::Composite &selection = Data::SequenceMatch::Composite(
                                      Data::SequenceMatch::Element::SpecialMatch::All));

public slots:

    void resetOverrides() override;

    void trimData(double start, double end) override;

    void setVisibleTimeRange(double start, double end) override;

    void interactionStopped() override;

    void clearMouseover() override;

private slots:

    void tracesReady();

    void xBinsReady();

    void yBinsReady();

    void xIndicatorsReady();

    void yIndicatorsReady();

    void deferredPaintFinished();

    void mouseoverUpdateFinished();

    void zoomUpdated();

    void modificationChanged();

protected:
    virtual void finalizeLayout(const QRectF &maximumDimensions = QRectF(),
                                QPaintDevice *paintdevice = NULL,
                                const DisplayDynamicContext &context = DisplayDynamicContext());

    virtual std::unique_ptr<TraceSet2D> createTraces();

    virtual std::unique_ptr<BinSet2D> createXBins();

    virtual std::unique_ptr<BinSet2D> createYBins();

    virtual std::unique_ptr<IndicatorSet2D> createXIndicators();

    virtual std::unique_ptr<IndicatorSet2D> createYIndicators();

    virtual std::unique_ptr<AxisDimensionSet2DSide> createXAxes();

    virtual std::unique_ptr<AxisDimensionSet2DSide> createYAxes();

    virtual std::unique_ptr<AxisDimensionSet2DColor> createZAxes();

    virtual std::unique_ptr<Graph2DParameters> parseParameters(const Data::Variant::Read &config,
                                                               const DisplayDefaults &defaults = {});

    virtual void checkFinalReady();

    virtual std::vector<GraphPainter2DHighlight::Region> translateHighlightTrace(
            DisplayHighlightController::HighlightType type,
            double start,
            double end,
            double min,
            double max,
            TraceHandler2D *trace,
            AxisDimension2DSide *x,
            AxisDimension2DSide *y,
            AxisDimension2DColor *z,
            const std::unordered_set<std::size_t> &affectedDimensions);

    virtual std::vector<GraphPainter2DHighlight::Region> translateHighlightXBin(
            DisplayHighlightController::HighlightType type,
            double start,
            double end,
            double min,
            double max,
            BinHandler2D *bins,
            AxisDimension2DSide *x,
            AxisDimension2DSide *y,
            const std::unordered_set<std::size_t> &affectedDimensions);

    virtual std::vector<GraphPainter2DHighlight::Region> translateHighlightYBin(
            DisplayHighlightController::HighlightType type,
            double start,
            double end,
            double min,
            double max,
            BinHandler2D *bins,
            AxisDimension2DSide *x,
            AxisDimension2DSide *y,
            const std::unordered_set<std::size_t> &affectedDimensions);

    virtual bool auxiliaryProcess(const DisplayDynamicContext &context);

    virtual bool auxiliaryBind(const DisplayDynamicContext &context,
                               AxisDimensionSet2DSide *xAxes,
                               AxisDimensionSet2DSide *yAxes,
                               AxisDimensionSet2DColor *zAxes,
                               std::vector<TraceUtilities::ColorRequest> &findColors,
                               std::deque<QColor> &claimedColors,
                               TraceSet2D *traces,
                               BinSet2D *xBins,
                               BinSet2D *yBins,
                               IndicatorSet2D *xIndicators,
                               IndicatorSet2D *yIndicators);

    virtual bool auxiliaryClaim(const DisplayDynamicContext &context,
                                Legend &legend,
                                std::deque<QColor> &dynamicColors,
                                TraceSet2D *traces,
                                BinSet2D *xBins,
                                BinSet2D *yBins,
                                IndicatorSet2D *xIndicators,
                                IndicatorSet2D *yIndicators);

    virtual void bindAxisColors(AxisDimensionSet2DSide *xAxes,
                                AxisDimensionSet2DSide *yAxes,
                                const std::unordered_map<AxisDimension2DSide *,
                                                         TraceHandler2D *> &uniqueXTraces,
                                const std::unordered_map<AxisDimension2DSide *,
                                                         TraceHandler2D *> &uniqueYTraces,
                                const std::unordered_map<AxisDimension2DSide *,
                                                         BinHandler2D *> &uniqueXBins,
                                const std::unordered_map<AxisDimension2DSide *,
                                                         BinHandler2D *> &uniqueYBins,
                                const std::unordered_map<AxisDimension2DSide *,
                                                         IndicatorHandler2D *> &uniqueXIndicators,
                                const std::unordered_map<AxisDimension2DSide *,
                                                         IndicatorHandler2D *> &uniqueYIndicators);

    virtual bool getAuxiliaryInsets(const DisplayDynamicContext &context,
                                    QPaintDevice *paintdevice,
                                    qreal &top,
                                    qreal &bottom,
                                    qreal &left,
                                    qreal &right);

    virtual std::vector<
            std::unique_ptr<Graph2DDrawPrimitive>> getAuxiliaryDraw(const QRectF &traceArea,
                                                                    const QRectF &sideAxisOutline,
                                                                    const QSizeF &totalSize);

    virtual QString convertMouseoverToolTipTrace(TraceHandler2D *trace,
                                                 TraceSet2D *traces,
                                                 AxisDimension2DSide *x,
                                                 AxisDimension2DSide *y,
                                                 AxisDimension2DColor *z,
                                                 const TracePoint<3> &point,
                                                 const DisplayDynamicContext &context);

    virtual QString convertMouseoverToolTipBins(BinHandler2D *bins,
                                                BinSet2D *binSet,
                                                bool vertical,
                                                AxisDimension2DSide *x,
                                                AxisDimension2DSide *y,
                                                const BinPoint<1, 1> &point,
                                                Graph2DMouseoverComponentTraceBox::Location location,
                                                const DisplayDynamicContext &context);

    virtual QString convertMouseoverToolTipIndicator(IndicatorHandler2D *indicator,
                                                     IndicatorSet2D *indicatorSet,
                                                     AxisDimension2DSide *axis,
                                                     const IndicatorPoint<1> &point,
                                                     Axis2DSide side,
                                                     const DisplayDynamicContext &context);

    virtual QString convertMouseoverToolTipFit(TraceHandler2D *trace,
                                               TraceSet2D *traces,
                                               AxisDimension2DSide *x,
                                               AxisDimension2DSide *y,
                                               AxisDimension2DColor *z,
                                               double fx,
                                               double fy,
                                               double fz,
                                               double fi,
                                               const DisplayDynamicContext &context);

    virtual QString convertMouseoverToolTipModelScan(TraceHandler2D *trace,
                                                     TraceSet2D *traces,
                                                     AxisDimension2DSide *x,
                                                     AxisDimension2DSide *y,
                                                     AxisDimension2DColor *z,
                                                     double mx,
                                                     double my,
                                                     double mz,
                                                     const DisplayDynamicContext &context);

    virtual QString convertMouseoverToolTipFit(BinHandler2D *bins,
                                               BinSet2D *binSet,
                                               bool vertical,
                                               AxisDimension2DSide *x,
                                               AxisDimension2DSide *y,
                                               BinParameters2D::FitOrigin origin,
                                               double fx,
                                               double fy,
                                               const DisplayDynamicContext &context);

    virtual QString getMouseoverToolTip(Graph2DMouseoverComponent *component,
                                        const DisplayDynamicContext &context);

    virtual DisplayModificationComponent *convertModificationComponent(Graph2DMouseoverComponent *component);

    virtual void zoomMouseX(AxisDimensionSet2DSide *xAxes, qreal minX, qreal maxX);

    virtual void zoomMouseY(AxisDimensionSet2DSide *yAxes, qreal minX, qreal maxX);

    virtual void zoomMouseBoth(AxisDimensionSet2DSide *xAxes,
                               AxisDimensionSet2DSide *yAxes,
                               qreal minX,
                               qreal maxX,
                               qreal minY,
                               qreal maxY);

    Graph2DParameters *overrideParameters();

    void mergeOverrideParameters();
};

/**
 * An implementation of the bin set that does nothing.
 */
class CPD3GRAPHING_EXPORT BinSet2DNOOP : public BinSet2D {
public:
    BinSet2DNOOP();

    virtual ~BinSet2DNOOP();

    using Cloned = typename BinSet2D::Cloned;

    using CreatedHandler = typename BinSet2D::CreatedHandler;

    Cloned clone() const override;

    void initialize(const Data::ValueSegment::Transfer &config,
                    const DisplayDefaults &defaults = {},
                    bool asRealtime = false) override;

    bool process(bool asRealtime = false, bool deferrable = false) override;

    void registerChain(Data::ProcessingTapChain *chain, bool deferClear = true) override;

protected:
    std::unique_ptr<
            TraceParameters> parseParameters(const Data::Variant::Read &value) const override;

    CreatedHandler createHandler(std::unique_ptr<TraceParameters> &&parameters) const override;
};

}
}


#endif
