/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <vector>
#include <algorithm>
#include <QSet>

#include "graphing/tracedispatch.hxx"
#include "core/util.hxx"
#include "datacore/variant/composite.hxx"

using namespace CPD3::Data;

/* This exists to make different implementations of the sort happy, so just ignore
 * when they're not used. */
#pragma GCC diagnostic ignored "-Wunused-function"

namespace CPD3 {
namespace Graphing {

/** @file graphing/tracedispatch.hxx
 * The engine that handles dispatch/dynamic trace creation.
 * <br>
 * The dimension list is set up with the external dimension as the
 * first index and the trace being dispatched to as the second one.  That is
 * the dimension list is first by dimension number then by output trace.
 * 
 * Data flow looks like:
 * 
 * Chain (thread)
 * Incoming Handlers (one for each configuration segment)
 *   Each handler has a single Dimension it's tied to
 *   The handler takes care of restricting times
 * Dimension
 *   Buffers incoming value
 * 
 * (Processing thread)
 * 
 * getPending()
 *   Iteratively selects the dimension with the first start time
 * dispatchValue(Dimension *, const SequenceValue &)
 *   Lookup on dispatch hash
 *   If no hit then creates Trace(s) including fanout
 * Trace::incomingData(int, const SequenceValue&)
 *   Inserts values into segment readers
 *   Handles unit flagging
 * Trace::handleSegment( DataMultiSegment &, bool )
 *   Creates output values
 */

TraceDispatch::OutputValue::OutputValue(double s,
                                        double e,
                                        const std::vector<Variant::Read> &setValues,
                                        bool setMetadata) : start(s),
                                                            end(e),
                                                            values(setValues),
                                                            metadata(setMetadata)
{ }

TraceDispatch::OutputValue::OutputValue(double s,
                                        double e,
                                        std::vector<Variant::Read> &&setValues,
                                        bool setMetadata) : start(s),
                                                            end(e),
                                                            values(std::move(setValues)),
                                                            metadata(setMetadata)
{ }

TraceDispatch::OutputValue::OutputValue() : start(FP::undefined()),
                                            end(FP::undefined()),
                                            values(),
                                            metadata(false)
{ }

TraceDispatch::OutputValue::OutputValue(const OutputValue &) = default;

TraceDispatch::OutputValue &TraceDispatch::OutputValue::operator=(const OutputValue &) = default;

TraceDispatch::OutputValue::OutputValue(OutputValue &&) = default;

TraceDispatch::OutputValue &TraceDispatch::OutputValue::operator=(OutputValue &&) = default;

bool TraceDispatch::OutputValue::operator==(const OutputValue &other) const
{
    if (getDimensions() != other.getDimensions())
        return false;
    if (isMetadata() != other.isMetadata())
        return false;
    for (int i = 0, max = getDimensions(); i < max; i++) {
        if (get(i) != other.get(i))
            return false;
    }
    return FP::equal(getStart(), other.getStart()) && FP::equal(getEnd(), other.getEnd());
}

class TraceDispatch::IncomingBase : public StreamSink {
protected:
    TraceDispatch *dispatch;
    TraceDispatch::Dimension *dimension;
#ifndef NDEBUG
    bool ended;
#endif
public:
    virtual ~IncomingBase() = default;

    IncomingBase(TraceDispatch *sd, TraceDispatch::Dimension *dim) : dispatch(sd), dimension(dim)
#ifndef NDEBUG
            , ended(false)
#endif
    { }

    void incomingData(const SequenceValue::Transfer &values) override
    {
        std::lock_guard<std::mutex> lock(dispatch->mutex);
        dimension->incomingData(values);
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        std::lock_guard<std::mutex> lock(dispatch->mutex);
        dimension->incomingData(std::move(values));
    }

    void incomingData(const SequenceValue &value) override
    {
        std::lock_guard<std::mutex> lock(dispatch->mutex);
        dimension->incomingData(value);
    }

    void incomingData(SequenceValue &&value) override
    {
        std::lock_guard<std::mutex> lock(dispatch->mutex);
        dimension->incomingData(std::move(value));
    }

    void endData() override
    {
#ifndef NDEBUG
        Q_ASSERT(!ended);
        ended = true;
#endif
        std::unique_lock<std::mutex> lock(dispatch->mutex);
        dimension->endData();
        if (++dispatch->nDispatchersEnded < dispatch->totalDispatchers)
            return;
        lock.unlock();
        dispatch->allDispatchersEnded();
    }

    virtual double firstPossibleEffect()
    { return FP::undefined(); }

    virtual void reset()
    {
#ifndef NDEBUG
        ended = false;
#endif
    }

    virtual std::unique_ptr<TraceDispatch::IncomingBase> clone(TraceDispatch *dispatch,
                                                               TraceDispatch::Dimension *dimension) const
    {
        return std::unique_ptr<TraceDispatch::IncomingBase>(
                new TraceDispatch::IncomingBase(dispatch, dimension));
    }
};

class TraceDispatch::IncomingStartEnd final : public TraceDispatch::IncomingBase {
    double start;
    double end;
    bool dimensionEnded;
    bool startPassed;

    void finishReducingList(SequenceValue::Transfer &reduced,
                            SequenceValue::Transfer::iterator check)
    {
        while (check != reduced.end()) {
            double vs = check->getStart();
            if (FP::defined(vs) && vs >= end) {
                if (check != reduced.begin()) {
                    reduced.erase(check, reduced.end());
                    TraceDispatch::IncomingBase::incomingData(std::move(reduced));
                }

                dimension->endData();
                dimensionEnded = true;
                return;
            }

            double ve = check->getEnd();
            if (FP::defined(ve) && ve <= start) {
                check = reduced.erase(check);
                continue;
            }

            if (!startPassed) {
                if (!FP::defined(vs) || vs < start)
                    check->setStart(start);
                else
                    startPassed = true;
            }
            if (!FP::defined(ve) || ve > end)
                check->setEnd(end);

            ++check;
        }
        TraceDispatch::IncomingBase::incomingData(std::move(reduced));
    }

public:
    virtual ~IncomingStartEnd() = default;

    IncomingStartEnd(double s, double e, TraceDispatch *sd, TraceDispatch::Dimension *dim)
            : TraceDispatch::IncomingBase(sd, dim),
              start(s),
              end(e),
              dimensionEnded(false),
              startPassed(false)
    { }

    void incomingData(const SequenceValue::Transfer &values) override
    {
        if (dimensionEnded)
            return;
        for (auto beginValues = values.begin(), check = beginValues, endValues = values.end();
                check != endValues;
                ++check) {
            double vs = check->getStart();
            if (FP::defined(vs) && vs >= end) {
                if (check != beginValues) {
                    SequenceValue::Transfer before;
                    std::copy(beginValues, check, Util::back_emplacer(before));
                    TraceDispatch::IncomingBase::incomingData(std::move(before));
                }
                dimension->endData();
                dimensionEnded = true;
                return;
            }

            double ve = check->getEnd();
            if (FP::defined(ve) && ve <= start) {
                SequenceValue::Transfer reduced = values;
                auto modify = reduced.begin() + (check - beginValues);
                finishReducingList(reduced, reduced.erase(modify));
                return;
            }

            if (!startPassed) {
                if (!FP::defined(vs) || vs < start) {
                    SequenceValue::Transfer reduced = values;
                    auto modify = reduced.begin() + (check - beginValues);
                    modify->setStart(start);
                    if (!FP::defined(ve) || ve > end)
                        modify->setEnd(end);
                    ++modify;
                    finishReducingList(reduced, modify);
                    return;
                } else {
                    startPassed = true;
                }
            }

            if (!FP::defined(ve) || ve > end) {
                SequenceValue::Transfer reduced = values;
                auto modify = reduced.begin() + (check - beginValues);
                modify->setEnd(end);
                ++modify;
                finishReducingList(reduced, modify);
                return;
            }
        }
        TraceDispatch::IncomingBase::incomingData(values);
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        if (dimensionEnded)
            return;
        for (auto beginValues = values.begin(), check = beginValues, endValues = values.end();
                check != endValues;
                ++check) {
            double vs = check->getStart();
            if (FP::defined(vs) && vs >= end) {
                if (check != beginValues) {
                    SequenceValue::Transfer before;
                    std::move(beginValues, check, Util::back_emplacer(before));
                    TraceDispatch::IncomingBase::incomingData(std::move(before));
                }
                dimension->endData();
                dimensionEnded = true;
                return;
            }

            double ve = check->getEnd();
            if (FP::defined(ve) && ve <= start) {
                SequenceValue::Transfer reduced = std::move(values);
                auto modify = reduced.begin() + (check - beginValues);
                finishReducingList(reduced, reduced.erase(modify));
                return;
            }

            if (!startPassed) {
                if (!FP::defined(vs) || vs < start) {
                    SequenceValue::Transfer reduced = std::move(values);
                    auto modify = reduced.begin() + (check - beginValues);
                    modify->setStart(start);
                    if (!FP::defined(ve) || ve > end)
                        modify->setEnd(end);
                    ++modify;
                    finishReducingList(reduced, modify);
                    return;
                } else {
                    startPassed = true;
                }
            }

            if (!FP::defined(ve) || ve > end) {
                SequenceValue::Transfer reduced = std::move(values);
                auto modify = reduced.begin() + (check - beginValues);
                modify->setEnd(end);
                ++modify;
                finishReducingList(reduced, modify);
                return;
            }
        }
        TraceDispatch::IncomingBase::incomingData(std::move(values));
    }

    void incomingData(const SequenceValue &value) override
    {
        if (dimensionEnded)
            return;

        double vs = value.getStart();
        if (FP::defined(vs) && vs >= end) {
            dimension->endData();
            dimensionEnded = true;
            return;
        }

        double ve = value.getEnd();
        if (FP::defined(ve) && ve <= start) {
            return;
        }

        if (!startPassed) {
            if (!FP::defined(vs) || vs < start) {
                SequenceValue modify = value;
                modify.setStart(start);
                if (!FP::defined(ve) || ve > end)
                    modify.setEnd(end);
                TraceDispatch::IncomingBase::incomingData(std::move(modify));
                return;
            } else {
                startPassed = true;
            }
        }

        if (!FP::defined(ve) || ve > end) {
            SequenceValue modify = value;
            modify.setEnd(end);
            TraceDispatch::IncomingBase::incomingData(std::move(modify));
            return;
        }

        TraceDispatch::IncomingBase::incomingData(value);
    }

    void incomingData(SequenceValue &&value) override
    {
        if (dimensionEnded)
            return;

        double vs = value.getStart();
        if (FP::defined(vs) && vs >= end) {
            dimension->endData();
            dimensionEnded = true;
            return;
        }

        double ve = value.getEnd();
        if (FP::defined(ve) && ve <= start) {
            return;
        }

        if (!startPassed) {
            if (!FP::defined(vs) || vs < start) {
                value.setStart(start);
                if (!FP::defined(ve) || ve > end)
                    value.setEnd(end);
                TraceDispatch::IncomingBase::incomingData(std::move(value));
                return;
            } else {
                startPassed = true;
            }
        }

        if (!FP::defined(ve) || ve > end) {
            value.setEnd(end);
            TraceDispatch::IncomingBase::incomingData(std::move(value));
            return;
        }

        TraceDispatch::IncomingBase::incomingData(std::move(value));
    }

    double firstPossibleEffect() override
    { return start; }

    void reset() override
    {
        TraceDispatch::IncomingBase::reset();
        dimensionEnded = false;
        startPassed = false;
    }

    std::unique_ptr<TraceDispatch::IncomingBase> clone(TraceDispatch *dispatch,
                                                       TraceDispatch::Dimension *dimension) const override
    {
        auto c = new TraceDispatch::IncomingStartEnd(start, end, dispatch, dimension);
        c->dimensionEnded = dimensionEnded;
        c->startPassed = startPassed;
        return std::unique_ptr<TraceDispatch::IncomingBase>(c);
    }
};

class TraceDispatch::IncomingStart final : public TraceDispatch::IncomingBase {
    double start;
    bool startPassed;

    void finishReducingList(SequenceValue::Transfer &reduced,
                            SequenceValue::Transfer::iterator check)
    {
        while (check != reduced.end()) {
            double vs = check->getStart();

            if (FP::defined(vs) && vs >= start) {
                startPassed = true;
                TraceDispatch::IncomingBase::incomingData(std::move(reduced));
                return;
            }

            double ve = check->getEnd();
            if (FP::defined(ve) && ve <= start) {
                check = reduced.erase(check);
                continue;
            }

            check->setStart(start);
            ++check;
        }
        TraceDispatch::IncomingBase::incomingData(std::move(reduced));
    }

public:
    virtual ~IncomingStart() = default;

    IncomingStart(double s, TraceDispatch *sd, TraceDispatch::Dimension *dim)
            : TraceDispatch::IncomingBase(sd, dim), start(s), startPassed(false)
    { }

    void incomingData(const SequenceValue::Transfer &values) override
    {
        if (startPassed) {
            TraceDispatch::IncomingBase::incomingData(values);
            return;
        }

        auto beginValues = values.begin();
        auto check = beginValues;
        auto endValues = values.end();
        for (; check != endValues; ++check) {
            double vs = check->getStart();
            if (FP::defined(vs) && vs >= start) {
                startPassed = true;
                break;
            }

            double ve = check->getEnd();
            if (FP::defined(ve) && ve <= start)
                continue;

            SequenceValue::Transfer reduced;
            std::copy(check, endValues, Util::back_emplacer(reduced));
            auto modify = reduced.begin();
            modify->setStart(start);
            ++modify;
            finishReducingList(reduced, modify);
            return;
        }
        if (check == endValues)
            return;
        if (check != beginValues) {
            SequenceValue::Transfer reduced;
            std::copy(check, endValues, Util::back_emplacer(reduced));
            TraceDispatch::IncomingBase::incomingData(std::move(reduced));
            return;
        }
        TraceDispatch::IncomingBase::incomingData(values);
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        if (startPassed) {
            TraceDispatch::IncomingBase::incomingData(std::move(values));
            return;
        }

        auto beginValues = values.begin();
        auto check = beginValues;
        auto endValues = values.end();
        for (; check != endValues; ++check) {
            double vs = check->getStart();
            if (FP::defined(vs) && vs >= start) {
                startPassed = true;
                break;
            }

            double ve = check->getEnd();
            if (FP::defined(ve) && ve <= start)
                continue;

            values.erase(beginValues, check);
            auto modify = values.begin();
            modify->setStart(start);
            ++modify;
            finishReducingList(values, modify);
            return;
        }
        if (check == endValues)
            return;
        if (check != beginValues) {
            values.erase(beginValues, check);
            TraceDispatch::IncomingBase::incomingData(std::move(values));
            return;
        }
        TraceDispatch::IncomingBase::incomingData(std::move(values));
    }

    void incomingData(const SequenceValue &value) override
    {
        if (startPassed) {
            TraceDispatch::IncomingBase::incomingData(value);
            return;
        }

        double vs = value.getStart();
        if (FP::defined(vs) && vs >= start) {
            startPassed = true;
            TraceDispatch::IncomingBase::incomingData(value);
            return;
        }

        double ve = value.getEnd();
        if (FP::defined(ve) && ve <= start) {
            return;
        }

        SequenceValue modify = value;
        modify.setStart(start);
        TraceDispatch::IncomingBase::incomingData(std::move(modify));
    }

    void incomingData(SequenceValue &&value) override
    {
        if (startPassed) {
            TraceDispatch::IncomingBase::incomingData(std::move(value));
            return;
        }

        double vs = value.getStart();
        if (FP::defined(vs) && vs >= start) {
            startPassed = true;
            TraceDispatch::IncomingBase::incomingData(std::move(value));
            return;
        }

        double ve = value.getEnd();
        if (FP::defined(ve) && ve <= start) {
            return;
        }

        value.setStart(start);
        TraceDispatch::IncomingBase::incomingData(std::move(value));
    }

    double firstPossibleEffect() override
    { return start; }

    void reset() override
    {
        TraceDispatch::IncomingBase::reset();
        startPassed = false;
    }

    std::unique_ptr<TraceDispatch::IncomingBase> clone(TraceDispatch *dispatch,
                                                       TraceDispatch::Dimension *dimension) const override
    {
        auto c = new TraceDispatch::IncomingStart(start, dispatch, dimension);
        c->startPassed = startPassed;
        return std::unique_ptr<TraceDispatch::IncomingBase>(c);
    }
};

class TraceDispatch::IncomingEnd final : public TraceDispatch::IncomingBase {
    double end;
    bool dimensionEnded;

    void finishReducingList(SequenceValue::Transfer &reduced,
                            SequenceValue::Transfer::iterator modify)
    {
        auto endReduce = reduced.end();
        while (modify != endReduce) {
            double vs = modify->getStart();
            if (FP::defined(vs) && vs >= end) {
                reduced.erase(reduced.begin(), modify);
                TraceDispatch::IncomingBase::incomingData(std::move(reduced));
                dimension->endData();
                dimensionEnded = true;
                return;
            }

            double ve = modify->getEnd();
            if (!FP::defined(ve) || ve > end)
                modify->setEnd(end);

            ++modify;
        }
        TraceDispatch::IncomingBase::incomingData(std::move(reduced));
    }

public:
    virtual ~IncomingEnd() = default;

    IncomingEnd(double e, TraceDispatch *sd, TraceDispatch::Dimension *dim)
            : TraceDispatch::IncomingBase(sd, dim), end(e), dimensionEnded(false)
    { }

    void incomingData(const SequenceValue::Transfer &values) override
    {
        if (dimensionEnded)
            return;
        for (auto beginValues = values.begin(), check = beginValues, endValues = values.end();
                check != endValues;
                ++check) {
            double vs = check->getStart();
            if (FP::defined(vs) && vs >= end) {
                if (check != beginValues) {
                    SequenceValue::Transfer before;
                    std::move(beginValues, check, Util::back_emplacer(before));
                    TraceDispatch::IncomingBase::incomingData(std::move(before));
                }
                dimension->endData();
                dimensionEnded = true;
                return;
            }

            double ve = check->getEnd();
            if (!FP::defined(ve) || ve > end) {
                SequenceValue::Transfer reduced = values;
                auto modify = reduced.begin() + (check - beginValues);
                modify->setEnd(end);
                ++modify;
                finishReducingList(reduced, modify);
                return;
            }
        }
        TraceDispatch::IncomingBase::incomingData(values);
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        if (dimensionEnded)
            return;
        for (auto beginValues = values.begin(), check = beginValues, endValues = values.end();
                check != endValues;
                ++check) {
            double vs = check->getStart();
            if (FP::defined(vs) && vs >= end) {
                if (check != beginValues) {
                    SequenceValue::Transfer reduced;
                    std::move(beginValues, check, Util::back_emplacer(reduced));
                    TraceDispatch::IncomingBase::incomingData(std::move(reduced));
                }
                dimension->endData();
                dimensionEnded = true;
                return;
            }

            double ve = check->getEnd();
            if (!FP::defined(ve) || ve > end) {
                check->setEnd(end);
                ++check;
                finishReducingList(values, check);
                return;
            }
        }
        TraceDispatch::IncomingBase::incomingData(std::move(values));
    }

    void incomingData(const SequenceValue &value) override
    {
        if (dimensionEnded)
            return;

        double vs = value.getStart();
        if (FP::defined(vs) && vs >= end) {
            dimension->endData();
            dimensionEnded = true;
            return;
        }

        double ve = value.getEnd();
        if (!FP::defined(ve) || ve > end) {
            SequenceValue modify = value;
            modify.setEnd(end);
            TraceDispatch::IncomingBase::incomingData(std::move(modify));
            return;
        }
        TraceDispatch::IncomingBase::incomingData(value);
    }

    void incomingData(SequenceValue &&value) override
    {
        if (dimensionEnded)
            return;

        double vs = value.getStart();
        if (FP::defined(vs) && vs >= end) {
            dimension->endData();
            dimensionEnded = true;
            return;
        }

        double ve = value.getEnd();
        if (!FP::defined(ve) || ve > end) {
            value.setEnd(end);
        }
        TraceDispatch::IncomingBase::incomingData(std::move(value));
    }

    void reset() override
    {
        TraceDispatch::IncomingBase::reset();
        dimensionEnded = false;
    }

    std::unique_ptr<TraceDispatch::IncomingBase> clone(TraceDispatch *dispatch,
                                                       TraceDispatch::Dimension *dimension) const override
    {
        TraceDispatch::IncomingEnd *c = new TraceDispatch::IncomingEnd(end, dispatch, dimension);
        c->dimensionEnded = dimensionEnded;
        return std::unique_ptr<TraceDispatch::IncomingBase>(c);
    }
};


TraceDispatch::~TraceDispatch() = default;

/**
 * Construct a new dispatch object from the given configuration.
 * 
 * @param config        the configuration to use
 * @param defaults      the DisplayDefaults to register inputs with
 * @param asRealtime    if set then use realtime behavior
 */
TraceDispatch::TraceDispatch(const ValueSegment::Transfer &config,
                             const DisplayDefaults &defaults,
                             bool asRealtime,
                             QObject *parent) : QObject(parent)
{
    totalDispatchers = 0;
    for (const auto &seg : config) {
        double start = seg.getStart();
        double end = seg.getEnd();
        auto v = seg.read();

        {
            InputChainRegion region;
            region.start = start;
            region.end = end;
            chainRegions.emplace_back(std::move(region));
        }

        auto dims = v["Dimensions"].toArray();
        for (std::size_t dim = 0, nDims = dims.size(); dim < nDims; dim++) {
            while (dim >= dimensions.size()) {
                dimensions.emplace_back();
            }

            auto add = new Dimension(dim);

            auto dval = dims[dim];
            add->selection = defaults.constructSequenceMatch(dval["Input"], asRealtime);
            add->fanoutUnits = true;
            if (dval["Fanout"].exists())
                add->fanoutUnits = dval["Fanout"].toBool();
            add->flattenFlavors = asRealtime;
            if (dval["FlattenFlavors"].exists())
                add->flattenFlavors = dval["FlattenFlavors"].toBool();
            if (FP::defined(start) && FP::defined(end)) {
                add->dispatcher.reset(new TraceDispatch::IncomingStartEnd(start, end, this, add));
            } else if (FP::defined(start)) {
                add->dispatcher.reset(new TraceDispatch::IncomingStart(start, this, add));
            } else if (FP::defined(end)) {
                add->dispatcher.reset(new TraceDispatch::IncomingEnd(end, this, add));
            } else {
                add->dispatcher.reset(new TraceDispatch::IncomingBase(this, add));
            }
            Q_ASSERT(add->dispatcher);
            add->chainConfig = Variant::Root(dval["Processing"]);

            dimensions[dim].emplace_back(add);
            totalDispatchers++;
        }
    }
    nDispatchersEnded = 0;
    tracesEnded = false;
}

TraceDispatch::TraceDispatch(const TraceDispatch &other)
        : QObject(),
          chainRegions(other.chainRegions),
          traces(),
          dimensions(),
          trackingSegments(),
          tracesEnded(true),
          nDispatchersEnded(other.totalDispatchers),
          totalDispatchers(other.totalDispatchers)
{
    traces.reserve(other.traces.size());
    for (const auto &add : other.traces) {
        traces.emplace_back(new Trace(*add));
    }

    underlays.reserve(other.underlays.size());
    dimensions.reserve(other.dimensions.size());
    for (const auto &dimBase : other.dimensions) {
        dimensions.emplace_back();
        dimensions.back().reserve(dimBase.size());
        for (const auto &otherDim : dimBase) {
            auto dim = new Dimension(*otherDim);
            dimensions.back().emplace_back(dim);
            dim->dispatcher = otherDim->dispatcher->clone(this, dim);

            for (const auto &od : otherDim->defaultDispatch) {
                auto under = new DefaultUnderlay(*od.second);
                dim->defaultDispatch.emplace(od.first, under);
                underlays.emplace_back(under);
            }
            for (const auto &od : otherDim->traceDispatch) {
                auto &nd =
                        dim->traceDispatch.emplace(od.first, std::vector<Trace *>()).first->second;

                nd.reserve(od.second.size());
                for (auto trace : od.second) {
                    std::size_t idx = 0;
                    for (auto endIdx = other.traces.size(); idx < endIdx; ++idx) {
                        if (trace == other.traces[idx].get()) {
                            break;
                        }
                    }
                    Q_ASSERT(idx < other.traces.size());
                    Q_ASSERT(idx < traces.size());
                    nd.emplace_back(traces[idx].get());
                }
            }
        }
    }
}

static const Data::SequenceName::Set emptyUnits;

bool TraceDispatch::mergable(const Variant::Read &a, const Variant::Read &b)
{
    auto av = a["Dimensions"].toArray();
    auto bv = b["Dimensions"].toArray();
    for (std::size_t i = 0, max = std::min(av.size(), bv.size()); i < max; i++) {
        auto ad = av[i];
        auto bd = bv[i];
        if (ad["Fanout"] != bd["Fanout"])
            return false;
    }
    return true;
}

void TraceDispatch::registerChain(ProcessingTapChain *chain)
{
    std::unique_lock<std::mutex> lock(mutex);
    for (const auto &dim : dimensions) {
        for (const auto &dimDis : dim) {
            dimDis->reset();
            chain->add(dimDis->dispatcher.get(), dimDis->chainConfig, dimDis->selection);
        }
    }
    traces.clear();
    nDispatchersEnded = 0;
    tracesEnded = false;

    if (totalDispatchers <= 0) {
        tracesEnded = true;
        for (const auto &trace : traces) {
            trace->endData();
        }

        lock.unlock();
        emit allStreamsEnded();
    }
}

std::vector<std::vector<TraceDispatch::OutputValue> > TraceDispatch::getPending()
{
    std::lock_guard<std::mutex> lock(mutex);

    for (;;) {
        Dimension *selected = nullptr;
        double selectedStart = 0;
        Dimension *outstanding = nullptr;
        double outstandingStart = 0;

        for (const auto &dim : dimensions) {
            for (const auto &dimDis : dim) {
                if (dimDis->values.empty()) {
                    if (dimDis->ended)
                        continue;
                    double start = dimDis->dispatcher->firstPossibleEffect();
                    if (!outstanding || Range::compareStart(start, outstandingStart) <= 0) {
                        outstanding = dimDis.get();
                        outstandingStart = start;
                    }
                    continue;
                }
                double start = dimDis->values.front().getStart();
                if (!selected || Range::compareStart(start, selectedStart) <= 0) {
                    selected = dimDis.get();
                    selectedStart = start;
                }
            }
        }

        if (!selected)
            break;

        /* Can't process more if there's an outstanding one that can affect
         * the selected value */
        if (outstanding) {
            if (!FP::defined(outstandingStart))
                break;
            double end = selected->values.front().getEnd();
            if (!FP::defined(end))
                break;
            if (end > outstandingStart)
                break;
        }

        dispatchValue(selected, std::move(selected->values.front()));
        selected->values.pop_front();
    }

    if (!tracesEnded && nDispatchersEnded >= totalDispatchers) {
        tracesEnded = true;
        for (const auto &trace : traces) {
            trace->endData();
        }
    }

    std::vector<std::vector<TraceDispatch::OutputValue> > result;
    for (const auto &trace : traces) {
        result.emplace_back(std::move(trace->pending));
        trace->pending.clear();
    }
    return result;
}

/* All the handle... functions are safe to keep copies from the segment
 * because the segment value is effectively being "moved". */

void TraceDispatch::Trace::handleData(SequenceSegment &segment)
{
    std::vector<Variant::Read> values;
    bool updateMeta = false;
    for (std::size_t i = 0, max = units.size(); i < max; i++) {
        for (const auto &u : units[i]) {
            auto v = segment.value(u);
            if (!Variant::Composite::isDefined(v))
                continue;
            while (values.size() < i) {
                values.emplace_back(Variant::Read::empty());
            }
            values.emplace_back(std::move(v));
            if (lastSelectedUnits[i] != u) {
                if (lastSelectedUnits[i].isValid())
                    updateMeta = true;
                lastSelectedUnits[i] = u;
            }
            break;
        }
    }
    if ((needMetadataOutput || updateMeta) &&
            Range::compareEnd(lastMetaSegment.getEnd(), segment.getEnd()) >= 0) {
        std::vector<Variant::Read> metadata;
        for (std::size_t i = 0, max = lastSelectedUnits.size(); i < max; i++) {
            if (!lastSelectedUnits[i].isValid())
                continue;
            auto v = lastMetaSegment.value(lastSelectedUnits[i].toMeta());
            while (metadata.size() < i) {
                values.emplace_back(Variant::Read::empty());
            }
            metadata.emplace_back(std::move(v));
        }
        pending.emplace_back(segment.getStart(), lastMetaSegment.getEnd(), std::move(metadata),
                             true);
        needMetadataOutput = false;
    }
    pending.emplace_back(segment.getStart(), segment.getEnd(), std::move(values), false);
}

void TraceDispatch::Trace::handleMeta(SequenceSegment &segment)
{
    std::vector<Variant::Read> values;
    for (std::size_t i = 0, max = lastSelectedUnits.size(); i < max; i++) {
        const auto &cu = lastSelectedUnits[i];
        if (!cu.isValid()) {
            for (const auto &u : units[i]) {
                auto v = segment.value(u.toMeta());
                if (!Variant::Composite::isDefined(v))
                    continue;
                while (values.size() < i) {
                    values.emplace_back(Variant::Read::empty());
                }
                values.emplace_back(std::move(v));
                break;
            }
            continue;
        }
        auto v = segment.value(cu.toMeta());
        while (values.size() < i) {
            values.emplace_back(Variant::Read::empty());
        }
        values.emplace_back(std::move(v));
    }
    pending.emplace_back(segment.getStart(), segment.getEnd(), std::move(values), true);
    needMetadataOutput = false;
}

void TraceDispatch::Trace::handleCombined(SequenceSegment &segment, SequenceSegment &metaSegment)
{
    std::vector<Variant::Read> values;
    for (std::size_t i = 0, max = units.size(); i < max; i++) {
        for (const auto &u : units[i]) {
            auto v = segment.value(u);
            if (!Variant::Composite::isDefined(v)) {
                if (!lastSelectedUnits[i].isValid())
                    lastSelectedUnits[i] = u;
                continue;
            }
            while (values.size() < i) {
                values.emplace_back(Variant::Read::empty());
            }
            values.emplace_back(std::move(v));
            lastSelectedUnits[i] = u;
            break;
        }
    }

    std::vector<Variant::Read> metadata;
    for (std::size_t i = 0, max = lastSelectedUnits.size(); i < max; i++) {
        if (!lastSelectedUnits[i].isValid())
            continue;
        auto v = metaSegment.value(lastSelectedUnits[i].toMeta());
        while (metadata.size() < i) {
            metadata.emplace_back(Variant::Read::empty());
        }
        metadata.emplace_back(std::move(v));
    }

    pending.emplace_back(metaSegment.getStart(), metaSegment.getEnd(), std::move(metadata), true);
    pending.emplace_back(segment.getStart(), segment.getEnd(), std::move(values), false);
    needMetadataOutput = false;
}

void TraceDispatch::Trace::dispatchOutput(SequenceSegment::Transfer &&output,
                                          SequenceSegment::Transfer &&outputMeta)
{
    std::size_t offset = 0;
    std::size_t offsetMeta = 0;
    std::size_t nOutput = output.size();
    std::size_t nMeta = outputMeta.size();
    while (offset < nOutput && offsetMeta < nMeta) {
        int cr = Range::compareStart(output[offset].getStart(), outputMeta[offsetMeta].getStart());
        if (cr < 0) {
            handleData(output[offset++]);
        } else if (cr == 0) {
            lastMetaSegment = std::move(outputMeta[offsetMeta++]);
            handleCombined(output[offset++], lastMetaSegment);
        } else {
            lastMetaSegment = std::move(outputMeta[offsetMeta++]);
            handleMeta(lastMetaSegment);
        }
    }
    while (offset < nOutput) {
        handleData(output[offset++]);
    }
    while (offsetMeta < nMeta) {
        lastMetaSegment = std::move(outputMeta[offsetMeta++]);
        handleMeta(lastMetaSegment);
    }
}

void TraceDispatch::Trace::incomingData(int dimension, const SequenceValue &value)
{
    /* Safe to do in this order (handle first, then insert the unit after) 
     * since the reader will never produce segments at the current time, only
     * once it's advanced */
    const auto &name = value.getName();

    SequenceSegment::Transfer output;
    SequenceSegment::Transfer outputMeta;
    if (name.isMeta()) {
        outputMeta = readerMeta.add(value);
        output = reader.advance(value.getStart());
    } else {
        output = reader.add(value);
        outputMeta = readerMeta.advance(value.getStart());
    }
    dispatchOutput(std::move(output), std::move(outputMeta));

    /* Clear units available between input chain domains, so we can
     * correctly handle switching back and forth.  This should always be
     * correct, since the input chains themselves force fragmentation to
     * the same region boundaries. */
    auto lastSize = remainingRegions.size();
    if (Range::intersectShift(remainingRegions, value)) {
        if (lastSize != remainingRegions.size()) {
            for (auto &c : units) {
                c.clear();
            }
        }
    }
    if (name.isMeta()) {
        units[dimension].insert(name.fromMeta());
    } else {
        units[dimension].insert(name);
    }
}

void TraceDispatch::Trace::incomingData(int dimension, SequenceValue &&value)
{
    auto name = value.getName();
    auto valueStart = value.getStart();
    auto valueEnd = value.getEnd();

    SequenceSegment::Transfer output;
    SequenceSegment::Transfer outputMeta;
    if (name.isMeta()) {
        outputMeta = readerMeta.add(std::move(value));
        output = reader.advance(valueStart);
        name.clearMeta();
    } else {
        output = reader.add(std::move(value));
        outputMeta = readerMeta.advance(valueStart);
    }
    dispatchOutput(std::move(output), std::move(outputMeta));

    auto lastSize = remainingRegions.size();
    if (Range::intersectShift(remainingRegions, valueStart, valueEnd)) {
        if (lastSize != remainingRegions.size()) {
            for (auto &c : units) {
                c.clear();
            }
        }
    }

    units[dimension].insert(name);
}

void TraceDispatch::Trace::endData()
{
    dispatchOutput(reader.finish(), readerMeta.finish());
}

TraceDispatch::Trace::Trace(int dimensions,
                            const std::deque<TraceDispatch::InputChainRegion> &regions)
        : needMetadataOutput(false), remainingRegions(regions)
{
    for (int i = 0; i < dimensions; i++) {
        units.emplace_back();
        lastSelectedUnits.emplace_back();
    }
}

TraceDispatch::Trace::Trace(int dimensions,
                            const std::deque<TraceDispatch::InputChainRegion> &regions,
                            const SequenceSegment::Stream &underReader,
                            const SequenceSegment::Stream &underReaderMeta) : reader(underReader),
                                                                              readerMeta(
                                                                                      underReaderMeta),
                                                                              needMetadataOutput(
                                                                                      false),
                                                                              remainingRegions(
                                                                                      regions)
{
    for (int i = 0; i < dimensions; i++) {
        units.emplace_back();
        lastSelectedUnits.emplace_back();
    }
}

TraceDispatch::Trace::Trace(const Trace &other) : reader(other.reader),
                                                  readerMeta(other.readerMeta),
                                                  pending(other.pending),
                                                  units(other.units),
                                                  lastSelectedUnits(other.lastSelectedUnits),
                                                  lastMetaSegment(other.lastMetaSegment),
                                                  needMetadataOutput(true),
                                                  remainingRegions(other.remainingRegions)
{ }

TraceDispatch::Trace::Trace(const Trace &other,
                            const SequenceSegment::Stream &underReader,
                            const SequenceSegment::Stream &underReaderMeta) : reader(underReader),
                                                                              readerMeta(
                                                                                      underReaderMeta),
                                                                              pending(other.pending),
                                                                              units(other.units),
                                                                              lastSelectedUnits(
                                                                                      other.lastSelectedUnits),
                                                                              lastMetaSegment(
                                                                                      other.lastMetaSegment),
                                                                              needMetadataOutput(
                                                                                      true),
                                                                              remainingRegions(
                                                                                      other.remainingRegions)
{
    reader.overlay(other.reader);
    readerMeta.overlay(other.readerMeta);
}

TraceDispatch::DefaultUnderlay::DefaultUnderlay() : reader(), readerMeta()
{ }

TraceDispatch::DefaultUnderlay::DefaultUnderlay(const DefaultUnderlay &other) = default;

void TraceDispatch::DefaultUnderlay::incomingData(const SequenceValue &value)
{
    if (value.getUnit().isMeta()) {
        readerMeta.add(value);
    } else {
        reader.add(value);
    }
}

void TraceDispatch::DefaultUnderlay::incomingData(SequenceValue &&value)
{
    if (value.getUnit().isMeta()) {
        readerMeta.add(std::move(value));
    } else {
        reader.add(std::move(value));
    }
}

TraceDispatch::Dimension::Dimension(int dimension) : dimensionNumber(dimension),
                                                     fanoutUnits(false),
                                                     flattenFlavors(false),
                                                     ended(false)
{ }

/* Don't copy dispatch */
TraceDispatch::Dimension::Dimension(const Dimension &other) : dimensionNumber(
        other.dimensionNumber),
                                                              selection(other.selection),
                                                              fanoutUnits(other.fanoutUnits),
                                                              flattenFlavors(other.flattenFlavors),
                                                              chainConfig(other.chainConfig),
                                                              values(other.values),
                                                              ended(other.ended)
{ }

TraceDispatch::Dimension::~Dimension() = default;

void TraceDispatch::Dimension::reset()
{
    values.clear();
    traceDispatch.clear();
    dispatcher->reset();
    ended = false;
}

void TraceDispatch::Dimension::incomingData(const SequenceValue::Transfer &v)
{ Util::append(v, values); }

void TraceDispatch::Dimension::incomingData(SequenceValue::Transfer &&v)
{ Util::append(std::move(v), values); }

void TraceDispatch::Dimension::incomingData(const SequenceValue &v)
{ values.emplace_back(v); }

void TraceDispatch::Dimension::incomingData(SequenceValue &&v)
{ values.emplace_back(std::move(v)); }

void TraceDispatch::Dimension::endData()
{
    ended = true;
}

void TraceDispatch::buildFanoutDispatch(Dimension *dimension,
                                        double time,
                                        const SequenceName &incomingUnit,
                                        std::vector<Trace *> &defaultDispatch,
                                        std::vector<Trace *> &targetDispatch)
{
    Q_ASSERT(!traces.empty());

    /* First see if this is already routed to an existing trace, if
     * it is then duplicate that routing.  This catches the case where
     * we have a dimension with the same units created because of multiple
     * configuration segments. */
    for (const auto &target : traces) {
        if (!(target->units[dimension->dimensionNumber].count(incomingUnit)))
            continue;

        defaultDispatch.emplace_back(target.get());
        targetDispatch.emplace_back(target.get());
        return;
    }

    struct UnitVectorHash {
        std::size_t operator()(const std::vector<SequenceName::Set> &input) const
        {
            std::size_t result = 0;
            for (const auto &add : input) {
                std::size_t set = 0;
                for (const auto &u : add) {
                    set ^= std::hash<SequenceName>()(u);
                }
                INTEGER::mix(result, set);
            }
            return result;
        }
    };

    /* Now step through all existing traces and fan out any that would be
     * duplicated from this unit */
    std::unordered_set<std::vector<Data::SequenceName::Set>, UnitVectorHash> duplicatedUnits;
    for (auto target = traces.begin(), endTarget = traces.end(); target != endTarget; ++target) {
        const auto &existingUnits = (*target)->units[dimension->dimensionNumber];

        /* Build the list of units all the other dimensions of
         * this trace occupy and if we've already duplicated it
         * for those, then do nothing */
        auto sourceUnits = (*target)->units;
        if (!existingUnits.empty())
            sourceUnits[dimension->dimensionNumber].clear();
        if (dimension->flattenFlavors) {
            for (auto &dim : sourceUnits) {
                SequenceName::Set output;
                for (const auto &unit : dim) {
                    SequenceName add = unit;
                    add.clearFlavors();
                    output.insert(std::move(add));
                }
                dim = std::move(output);
            }
        }

        /* If there's nothing on this dimension (first occurrence always)
         * or it already contains the unit (multiple inputs) then
         * direct to it, instead of creating a new trace */
        if (existingUnits.empty() || existingUnits.count(incomingUnit)) {
            targetDispatch.emplace_back(target->get());
            defaultDispatch.emplace_back(target->get());
            duplicatedUnits.insert(sourceUnits);
            continue;
        }

        /* Already have added a duplication for this set of units, so
         * don't need to do anything */
        if (duplicatedUnits.count(sourceUnits))
            continue;
        duplicatedUnits.insert(sourceUnits);

        /* Fan out this trace into a new copy with this unit routed
         * to it */

        Trace *original = target->get();
        Trace *duplicate;
        auto def = dimension->defaultDispatch.find(incomingUnit.toDefaultStation());
        if (def != dimension->defaultDispatch.end()) {
            duplicate = new Trace(*original, def->second->reader, def->second->readerMeta);
        } else {
            duplicate = new Trace(*original);
        }
        duplicate->reader.advance(time);
        auto advancedMeta = duplicate->readerMeta.advance(time);
        if (!advancedMeta.empty())
            duplicate->lastMetaSegment = advancedMeta.back();
        duplicate->units[dimension->dimensionNumber].clear();
        duplicate->lastSelectedUnits[dimension->dimensionNumber] = SequenceName();
        duplicate->needMetadataOutput = true;
        duplicate->pending.clear();
        targetDispatch.push_back(duplicate);
        defaultDispatch.push_back(duplicate);

        std::size_t offset = target - traces.begin();
        traces.emplace_back(duplicate);
        target = traces.begin() + offset;
        endTarget = traces.end();

        /* Update the dispatch for all dimension segments to also point
         * to this new duplicated trace */
        for (std::size_t dim = 0, nDim = dimensions.size(); dim < nDim; dim++) {
            if (static_cast<int>(dim) == dimension->dimensionNumber)
                continue;
            for (const auto &dis : dimensions[dim]) {
                for (auto &update : dis->traceDispatch) {
                    if (std::find(update.second.begin(), update.second.end(), original) ==
                            update.second.end())
                        continue;
                    update.second.push_back(duplicate);
                }
            }
        }
    }
}

void TraceDispatch::dispatchValue(Dimension *dimension, SequenceValue &&v)
{
    Util::ReferenceCopy<SequenceName> withoutMeta(v.getUnit());
    if (withoutMeta().isMeta())
        withoutMeta->clearMeta();
    Util::ReferenceCopy<SequenceName> targetName(withoutMeta());
    if (dimension->flattenFlavors && targetName().hasFlavors())
        targetName->clearFlavors();

    if (targetName->isDefaultStation()) {
        auto def = dimension->defaultDispatch.find(targetName());
        if (def == dimension->defaultDispatch.end()) {
            auto u = new DefaultUnderlay;
            underlays.emplace_back(u);
            def = dimension->defaultDispatch.emplace(targetName(), u).first;
        }
        auto d = dimension->traceDispatch.find(targetName());
        if (d == dimension->traceDispatch.end()) {
            d = dimension->traceDispatch.emplace(targetName(), std::vector<Trace *>()).first;
        }

        if (d->second.empty()) {
            def->second->incomingData(std::move(v));
            return;
        }
        def->second->incomingData(v);

        auto target = d->second.begin();
        for (auto endTarget = d->second.end() - 1; target != endTarget; ++target) {
            (*target)->incomingData(dimension->dimensionNumber, v);
        }
        Q_ASSERT(target != d->second.end());
        (*target)->incomingData(dimension->dimensionNumber, std::move(v));
        return;
    }

    auto d = dimension->traceDispatch.find(targetName());
    if (d == dimension->traceDispatch.end()) {
        SequenceName targetDefault(targetName().toDefaultStation());
        auto defD = dimension->traceDispatch.find(targetDefault);
        if (defD == dimension->traceDispatch.end()) {
            defD = dimension->traceDispatch.emplace(targetDefault, std::vector<Trace *>()).first;
        }

        d = dimension->traceDispatch.emplace(*targetName, std::vector<Trace *>()).first;
        if (traces.empty()) {
            Trace *add;;
            auto def = dimension->defaultDispatch.find(targetDefault);
            if (def != dimension->defaultDispatch.end()) {
                add = new Trace(dimensions.size(), chainRegions, def->second->reader,
                                def->second->readerMeta);
                add->reader.advance(v.getStart());
                auto advancedMeta = add->readerMeta.advance(v.getStart());
                if (!advancedMeta.empty()) {
                    add->lastMetaSegment = advancedMeta.back();
                    add->needMetadataOutput = true;
                }
            } else {
                add = new Trace(dimensions.size(), chainRegions);
            }
            traces.emplace_back(add);
            d->second.emplace_back(add);
            defD->second.emplace_back(add);
        } else if (dimension->fanoutUnits) {
            buildFanoutDispatch(dimension, v.getStart(), withoutMeta(), defD->second, d->second);
        } else {
            for (const auto &target : traces) {
                d->second.emplace_back(target.get());
                defD->second.emplace_back(target.get());
            }
        }
    }

    if (d->second.empty())
        return;

    auto target = d->second.begin();
    for (auto endTarget = d->second.end() - 1; target != endTarget; ++target) {
        (*target)->incomingData(dimension->dimensionNumber, v);
    }
    Q_ASSERT(target != d->second.end());
    (*target)->incomingData(dimension->dimensionNumber, std::move(v));
}

void TraceDispatch::allDispatchersEnded()
{ emit allStreamsEnded(); }

bool TraceDispatch::haveAllStreamsEnded()
{
    std::lock_guard<std::mutex> lock(mutex);
    return nDispatchersEnded >= totalDispatchers;
}

const SequenceName::Set &TraceDispatch::getUnits(std::size_t trace, std::size_t dimension) const
{
    if (trace >= traces.size())
        return emptyUnits;
    if (dimension >= traces[trace]->units.size())
        return emptyUnits;
    return traces[trace]->units[dimension];
}


std::vector<SequenceName::Set> TraceDispatch::getUnits(std::size_t trace) const
{
    if (trace >= traces.size())
        return {};
    std::vector<SequenceName::Set> result;
    for (const auto &add : traces[trace]->units) {
        result.emplace_back(add);
    }
    return result;
}

std::vector<SequenceName::Set> TraceDispatch::getLatestUnits(std::size_t trace) const
{
    if (trace >= traces.size())
        return {};
    const auto &tr = traces[trace];
    std::vector<SequenceName::Set> result;
    for (std::size_t i = 0, max = tr->units.size(); i < max; i++) {
        SequenceName::Set set;
        if (tr->lastSelectedUnits[i].isValid()) {
            set.insert(tr->lastSelectedUnits[i]);
        } else {
            Util::merge(tr->units[i], set);
        }
        result.emplace_back(std::move(set));
    }
    return result;
}


}
}
