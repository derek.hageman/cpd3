/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <memory>
#include <QRectF>
#include <QVariant>
#include <QDateTime>
#include <QIconEngine>
#include <QApplication>

#include "graphing/display.hxx"
#include "graphing/tracedispatch.hxx"
#include "guicore/guiformat.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Graphing {

/** @file graphing/display.hxx
 * Provides the the general interface for data related displays.
 */


DisplayCoupling::~DisplayCoupling() = default;

DisplayCoupling::DisplayCoupling() : coupleMode(Always)
{ }

DisplayCoupling::DisplayCoupling(CoupleMode mode) : coupleMode(mode)
{ }

DisplayCoupling::CoupleMode DisplayCoupling::getMode() const
{ return coupleMode; }

DisplayRangeCoupling::~DisplayRangeCoupling() = default;

DisplayRangeCoupling::DisplayRangeCoupling() = default;

DisplayRangeCoupling::DisplayRangeCoupling(DisplayCoupling::CoupleMode mode) : DisplayCoupling(mode)
{ }

QVariant DisplayRangeCoupling::combine(const std::vector<
        std::shared_ptr<DisplayCoupling>> &components) const
{
    if (components.empty())
        return {};

    double min = FP::undefined();
    double max = FP::undefined();

    for (const auto &i : components) {
        auto c = static_cast<DisplayRangeCoupling *>(i.get());
        Q_ASSERT(c);

        double cmin = c->getMin();
        if (FP::defined(cmin)) {
            if (!FP::defined(min) || cmin < min)
                min = cmin;
        }

        double cmax = c->getMax();
        if (FP::defined(cmax)) {
            if (!FP::defined(max) || cmax > max)
                max = cmax;
        }
    }

    QList<QVariant> result;
    if (FP::defined(min))
        result.append(QVariant(min));
    else
        result.append(QVariant());
    if (FP::defined(max))
        result.append(QVariant(max));
    else
        result.append(QVariant());

    return result;
}


DisplayOutput::DisplayOutput() = default;

DisplayOutput::~DisplayOutput() = default;

QList<QAction *> DisplayOutput::getMenuActions(QWidget *)
{ return {}; }


DisplayZoomAxis::~DisplayZoomAxis()
{ }

DisplayZoomAxis::DisplayZoomAxis()
{ }

std::vector<std::shared_ptr<DisplayZoomAxis>> DisplayZoomAxis::getCoupledAxes() const
{ return {}; }

bool DisplayZoomAxis::sharedZoom(const std::shared_ptr<DisplayZoomAxis> &) const
{ return false; }

DisplayZoomAxis::DisplayMode DisplayZoomAxis::getDisplayMode() const
{ return Display_Automatic; }


DisplayZoomCommand::Axis::Axis()
        : axis(),
          beforeMin(FP::undefined()),
          beforeMax(FP::undefined()),
          afterMin(FP::undefined()),
          afterMax(FP::undefined())
{ }

DisplayZoomCommand::Axis::Axis(const Axis &other) = default;

DisplayZoomCommand::Axis &DisplayZoomCommand::Axis::operator=(const Axis &other) = default;

DisplayZoomCommand::Axis::Axis(Axis &&other) = default;

DisplayZoomCommand::Axis &DisplayZoomCommand::Axis::operator=(Axis &&other) = default;

DisplayZoomCommand::Axis::Axis(std::shared_ptr<DisplayZoomAxis> to, double min, double max) : axis(
        std::move(to)),
                                                                                              beforeMin(
                                                                                                      axis->getZoomMin()),
                                                                                              beforeMax(
                                                                                                      axis->getZoomMax()),
                                                                                              afterMin(
                                                                                                      min),
                                                                                              afterMax(
                                                                                                      max)
{ }

DisplayZoomCommand::Axis::Axis(std::shared_ptr<DisplayZoomAxis> to, const Axis &other) : axis(
        std::move(to)),
                                                                                         beforeMin(
                                                                                                 axis->getZoomMin()),
                                                                                         beforeMax(
                                                                                                 axis->getZoomMax()),
                                                                                         afterMin(
                                                                                                 other.afterMin),
                                                                                         afterMax(
                                                                                                 other.afterMax)
{ }

void DisplayZoomCommand::Axis::undo()
{
    Q_ASSERT(axis);
    axis->setZoom(beforeMin, beforeMax);
}

void DisplayZoomCommand::Axis::redo()
{
    Q_ASSERT(axis);
    axis->setZoom(afterMin, afterMax);
}

QString DisplayZoomCommand::Axis::getTitle() const
{
    if (!axis)
        return QString();
    return axis->getTitle();
}

DisplayZoomCommand::DisplayZoomCommand() : QUndoCommand(), axes()
{ }

DisplayZoomCommand::DisplayZoomCommand(const DisplayZoomCommand &other)
        : QUndoCommand(), axes(other.axes)
{
    updateText();
}

DisplayZoomCommand &DisplayZoomCommand::operator=(const DisplayZoomCommand &other)
{
    if (&other == this) return *this;
    axes = other.axes;
    updateText();
    return *this;
}

DisplayZoomCommand::DisplayZoomCommand(DisplayZoomCommand &&other)
        : QUndoCommand(), axes(std::move(other.axes))
{
    updateText();
}

DisplayZoomCommand &DisplayZoomCommand::operator=(DisplayZoomCommand &&other)
{
    if (&other == this) return *this;
    axes = std::move(other.axes);
    updateText();
    return *this;
}

DisplayZoomCommand::DisplayZoomCommand(std::vector<Axis> set) : QUndoCommand(), axes(std::move(set))
{
    updateText();
}

void DisplayZoomCommand::add(const Axis &axis)
{
    axes.emplace_back(axis);
    updateText();
}

void DisplayZoomCommand::add(const std::shared_ptr<DisplayZoomAxis> &axis, double min, double max)
{
    axes.emplace_back(axis, min, max);
    updateText();
}

void DisplayZoomCommand::addCombining(const std::shared_ptr<DisplayZoomAxis> &axis)
{
    for (const auto &it : axes) {
        if (it.sharedZoom(axis)) {
            axes.emplace_back(axis, it);
            updateText();
            return;
        }
    }
}

void DisplayZoomCommand::append(const DisplayZoomCommand &other)
{
    Util::append(other.axes, axes);
    updateText();
}

void DisplayZoomCommand::redo()
{
    for (auto &it : axes) {
        it.redo();
    }
}

void DisplayZoomCommand::undo()
{
    for (auto &it : axes) {
        it.undo();
    }
}

/**
 * Test if the command is valid (it does anything).
 * @return true if the command is valid
 */
bool DisplayZoomCommand::isValid() const
{ return !axes.empty(); }

void DisplayZoomCommand::updateText()
{
    std::unordered_set<QString> all;
    for (const auto &it : axes) {
        QString add(it.getTitle());
        if (add.isEmpty())
            continue;
        all.insert(add);
    }
    if (all.empty()) {
        setText(Display::tr("Zoom"));
        return;
    }
    QStringList sorted;
    Util::append(all, sorted);
    std::sort(sorted.begin(), sorted.end());
    setText(Display::tr("Zoom: %1").arg(sorted.join(", ")));
}

DisplayHighlightController::~DisplayHighlightController() = default;

DisplayHighlightController::DisplayHighlightController() = default;

DisplayModificationComponent::~DisplayModificationComponent() = default;

DisplayModificationComponent::DisplayModificationComponent() = default;

QList<QAction *> DisplayModificationComponent::getMenuActions(QWidget *)
{ return {}; }

DisplayDefaults::DisplayDefaults() : archiveRT("rt_boxcar"), settings(nullptr)
{ }

DisplayDefaults::DisplayDefaults(const DisplayDefaults &other) = default;

DisplayDefaults &DisplayDefaults::operator=(const DisplayDefaults &other) = default;

DisplayDefaults::DisplayDefaults(DisplayDefaults &&other) = default;

DisplayDefaults &DisplayDefaults::operator=(DisplayDefaults &&other) = default;

SequenceMatch::Composite DisplayDefaults::constructSequenceMatch(const Variant::Read &config,
                                                                 bool asRealtime) const
{
    SequenceMatch::Composite::ConfigurationOptions options;
    options.acceptDefaultStation = true;
    options.acceptMetadata = true;
    if (asRealtime) {
        SequenceMatch::Composite sel(config, {QString::fromStdString(registerStation)},
                                     {QString::fromStdString(archiveRT)}, {}, options);
        if (!sel.valid())
            sel.append(SequenceMatch::Element::SpecialMatch::All);
        return sel;
    } else {
        SequenceMatch::Composite sel(config, {QString::fromStdString(registerStation)},
                                     {QString::fromStdString(registerArchive)}, {}, options);
        if (!sel.valid())
            sel.append(SequenceMatch::Element::SpecialMatch::All);
        return sel;
    }
}

QSettings *DisplayDefaults::getSettings() const
{ return settings; }

void DisplayDefaults::setSettings(QSettings *settings)
{ this->settings = settings; }

void DisplayDefaults::setStation(const Data::SequenceName::Component &station)
{ this->registerStation = station; }

void DisplayDefaults::setArchive(const Data::SequenceName::Component &archive)
{ this->registerArchive = archive; }

void DisplayDefaults::setArchiveRealtime(const Data::SequenceName::Component &archive)
{ this->archiveRT = archive; }

DisplayDynamicContext::DisplayDynamicContext() : interactive(false), settings(nullptr)
{ }

DisplayDynamicContext::DisplayDynamicContext(const DisplayDynamicContext &) = default;

DisplayDynamicContext &DisplayDynamicContext::operator=(const DisplayDynamicContext &) = default;

DisplayDynamicContext::DisplayDynamicContext(DisplayDynamicContext &&) = default;

DisplayDynamicContext &DisplayDynamicContext::operator=(DisplayDynamicContext &&) = default;

DisplayDynamicContext::SubstitutionHandler::SubstitutionHandler(const DisplayDynamicContext *ctx,
                                                                StringContext t,
                                                                bool *chg) : context(ctx),
                                                                             type(t),
                                                                             changed(chg)
{ }

DisplayDynamicContext::SubstitutionHandler::~SubstitutionHandler() = default;

QString DisplayDynamicContext::SubstitutionHandler::applyTimeFormat(double time,
                                                                    const QStringList &elements) const
{
    if (!elements.isEmpty() && elements.at(0).toLower() == "user") {
        if (changed != NULL)
            (*changed) = true;
        return GUI::GUITime::formatTime(time, context->getSettings());
    }
    return formatTime(time, elements);
}

QString DisplayDynamicContext::SubstitutionHandler::substitution(const QStringList &elements) const
{
    int index = 0;
    QString key;
    if (index < elements.size())
        key = elements.at(index++).toLower();
    if (key == "now") {
        if (changed != NULL)
            (*changed) = true;
        return applyTimeFormat(Time::time(), elements.mid(index));
    } else if (key == "station") {
        return formatString(context->station, elements.mid(index), !context->station.isEmpty());
    }

    return QString();
}

QString &DisplayDynamicContext::handleString(QString &str,
                                             StringContext context,
                                             bool *changed) const
{
    SubstitutionHandler sub(this, context, changed);
    str = sub.apply(str);
    return str;
}


TraceDispatchOverride::TraceDispatchOverride(const Variant::Read &sm) : smoothing(sm)
{ }

TraceDispatchOverride::TraceDispatchOverride() = default;

TraceDispatchOverride::TraceDispatchOverride(const TraceDispatchOverride &) = default;

TraceDispatchOverride &TraceDispatchOverride::operator=(const TraceDispatchOverride &) = default;

TraceDispatchOverride::TraceDispatchOverride(TraceDispatchOverride &&) = default;

TraceDispatchOverride &TraceDispatchOverride::operator=(TraceDispatchOverride &&) = default;

/**
 * Create a list of segments defining the override as applied to the given
 * dispatch.
 * 
 * @param dispatch  the dispatch that the override applies to
 * @return          a list of configuration segments that would construct the dispatch with the override
 */
ValueSegment::Transfer TraceDispatchOverride::get(const TraceDispatch *dispatch) const
{
    if (!smoothing.read().exists())
        return ValueSegment::Transfer();
    Variant::Root output;
    for (int dim = 0, nDims = dispatch->getDimensions(); dim < nDims; ++dim) {
        output.write().hash("Dimensions").array(dim).hash("Processing").set(smoothing);
    }
    return ValueSegment::Transfer{{FP::undefined(), FP::undefined(), std::move(output)}};
}


DisplaySaveContext::DisplaySaveContext() : target(), tdo()
{ }

DisplaySaveContext::DisplaySaveContext(const DisplaySaveContext &) = default;

DisplaySaveContext &DisplaySaveContext::operator=(const DisplaySaveContext &) = default;

DisplaySaveContext::DisplaySaveContext(DisplaySaveContext &&) = default;

DisplaySaveContext &DisplaySaveContext::operator=(DisplaySaveContext &&) = default;

/**
 * Overlay the given value into the the result.  This value will take
 * effect for all time.
 * 
 * @param value the value to overlay
 */
void DisplaySaveContext::overlay(const Variant::Read &value)
{
    target.emplace_back(FP::undefined(), FP::undefined(), Variant::Root(value));
    target = ValueSegment::merge(target);
}

/**
 * Underlay the given value into the the result.  This value will take
 * effect for all time.
 * 
 * @param value the value to underlay
 */
void DisplaySaveContext::underlay(const Variant::Read &value)
{
    target.insert(target.begin(),
                  ValueSegment(FP::undefined(), FP::undefined(), Variant::Root(value)));
    target = ValueSegment::merge(target);
}


/**
 * Overlay the given value into the the result.  This value will take
 * effect for all time.  The value is placed at the given path in the
 * result.
 * 
 * @param value the value to overlay
 * @param path  the path the value will be placed at
 */
void DisplaySaveContext::overlay(const Variant::Read &value, const QString &path)
{
    if (path.isEmpty()) {
        overlay(value);
        return;
    }
    QList<QString> paths;
    for (std::size_t i = 0, max = target.size(); i < max; i++) {
        paths.append(QString());
    }
    paths.append(path);
    target.emplace_back(FP::undefined(), FP::undefined(), Variant::Root(value));
    target = ValueSegment::merge(target, paths);
}

/**
 * Underlay the given value into the the result.  This value will take
 * effect for all time.  The value is placed at the given path in the
 * result.
 * 
 * @param value the value to underlay
 * @param path  the path the value will be placed at
 */
void DisplaySaveContext::underlay(const Variant::Read &value, const QString &path)
{
    if (path.isEmpty()) {
        overlay(value);
        return;
    }
    QList<QString> paths;
    paths.append(path);
    for (std::size_t i = 0, max = target.size(); i < max; i++) {
        paths.append(QString());
    }
    target.emplace_back(FP::undefined(), FP::undefined(), Variant::Root(value));
    target = ValueSegment::merge(target, paths);
}

/**
 * Overlay the given values into the result.
 * 
 * @param over  the values to overlay
 */
void DisplaySaveContext::overlay(const ValueSegment::Transfer &over)
{
    if (over.empty())
        return;
    Util::append(over, target);
    target = ValueSegment::merge(target);
}

/**
 * Underlay the given values into the result.
 * 
 * @param under the values to underlay
 */
void DisplaySaveContext::underlay(const ValueSegment::Transfer &under)
{
    if (under.empty())
        return;
    ValueSegment::Transfer merge = under;
    Util::append(std::move(target), merge);
    target = ValueSegment::merge(merge);
}

/**
 * Overlay the given values into the result.  The values are placed at the 
 * given path in the result.
 * 
 * @param over  the values to overlay
 * @param path  the path the value will be placed at
 */
void DisplaySaveContext::overlay(const ValueSegment::Transfer &over, const QString &path)
{
    if (over.empty())
        return;
    if (path.isEmpty()) {
        overlay(over);
        return;
    }
    QList<QString> paths;
    for (std::size_t i = 0, max = target.size(); i < max; i++) {
        paths.append(QString());
    }
    for (std::size_t i = 0, max = over.size(); i < max; i++) {
        paths.append(path);
    }
    Util::append(over, target);
    target = ValueSegment::merge(target, paths);
}

/**
 * Underlay the given values into the result.  The values are placed at the 
 * given path in the result.
 * 
 * @param under the values to underlay
 * @param path  the path the value will be placed at
 */
void DisplaySaveContext::underlay(const ValueSegment::Transfer &under, const QString &path)
{
    if (under.empty())
        return;
    if (path.isEmpty()) {
        underlay(under);
        return;
    }
    QList<QString> paths;
    for (std::size_t i = 0, max = under.size(); i < max; i++) {
        paths.append(path);
    }
    for (std::size_t i = 0, max = target.size(); i < max; i++) {
        paths.append(QString());
    }
    ValueSegment::Transfer merge = under;
    Util::append(std::move(target), merge);
    target = ValueSegment::merge(merge, paths);
}

/**
 * Get the trace dispatch override for the given display.
 * 
 * @param display   the display that uses the traces
 * @return          a trace dispatch override description object
 */
TraceDispatchOverride DisplaySaveContext::traceDispatchOverride(const Display *display)
{
    return tdo.value(display);
}

/**
 * Set the trace dispatch override segments for the given display.
 * 
 * @param display   the display to set
 * @param smoothing the trace dispatch smoothing configuration
 */
void DisplaySaveContext::setTraceDispatchSmoothing(const Display *display,
                                                   const Variant::Read &smoothing)
{
    tdo.insert(display, TraceDispatchOverride(smoothing));
}


Display::Display(const DisplayDefaults &, QObject *parent) : QObject(parent)
{ }

Display::Display(const Display &) : QObject()
{ }

Display::~Display() = default;

void Display::registerChain(ProcessingTapChain *)
{ }

std::vector<std::shared_ptr<DisplayOutput>> Display::getOutputs(const DisplayDynamicContext &)
{ return {}; }

std::vector<std::shared_ptr<DisplayCoupling>> Display::getDataCoupling()
{ return {}; }

std::vector<std::shared_ptr<DisplayCoupling>> Display::getLayoutCoupling()
{ return {}; }

QSizeF Display::getMinimumSize(QPaintDevice *, const DisplayDynamicContext &)
{ return QSizeF(0, 0); }

void Display::prepareLayout(const QRectF &, QPaintDevice *, const DisplayDynamicContext &)
{ }

void Display::predictLayout(const QRectF &, QPaintDevice *, const DisplayDynamicContext &)
{ }

void Display::interactionStopped()
{ }

bool Display::eventMousePress(QMouseEvent *)
{ return false; }

bool Display::eventMouseRelease(QMouseEvent *)
{ return false; }

bool Display::eventMouseMove(QMouseEvent *)
{ return false; }

bool Display::eventKeyPress(QKeyEvent *)
{ return false; }

bool Display::eventKeyRelease(QKeyEvent *)
{ return false; }

std::shared_ptr<DisplayModificationComponent> Display::getMouseModification(const QPoint &,
                                                                            const QRectF &,
                                                                            const DisplayDynamicContext &)
{ return {}; }

void Display::setMouseover(const QPoint &)
{ }

std::vector<std::shared_ptr<DisplayZoomAxis>> Display::zoomAxes()
{ return {}; }

std::shared_ptr<DisplayHighlightController> Display::highlightController()
{ return {}; }

void Display::saveOverrides(DisplaySaveContext &)
{ }

bool Display::acceptsKeyboardInput() const
{ return false; }

bool Display::acceptsMouseover() const
{ return false; }

void Display::resetOverrides()
{ }

void Display::trimData(double, double)
{ }

void Display::setVisibleTimeRange(double, double)
{ }

void Display::clearMouseover()
{ }

static double colorBounded(const Variant::Read &value)
{
    double v = value.toReal();
    if (!FP::defined(v))
        return 0.0;
    if (v < 0.0)
        return 0.0;
    if (v > 1.0)
        return 1.0;
    return v;
}

/**
 * Parse a value into a color.
 * 
 * @param value         the value to parse
 * @param defaultColor  the default color to use
 * @return              a color
 */
QColor Display::parseColor(const Variant::Read &value, const QColor &defaultColor)
{
    if (!value.exists())
        return defaultColor;

    if (value.getType() == Variant::Type::String) {
        QString name(value.toQString().toLower());
        if (name == "default")
            return defaultColor;
        QColor result;
        result.setNamedColor(name);
        if (result.isValid())
            return result;
    }

    auto spec = QColor::Rgb;
    const auto &model = value["Model"].toString();
    if (Util::equal_insensitive(model, "RGB")) {
        spec = QColor::Rgb;
    } else if (Util::equal_insensitive(model, "HSV")) {
        spec = QColor::Hsv;
    } else if (Util::equal_insensitive(model, "HSL")) {
        spec = QColor::Hsl;
    } else if (Util::equal_insensitive(model, "CMYK")) {
        spec = QColor::Cmyk;
    } else {
        if (value["H"].exists() && value["S"].exists() && value["V"].exists()) {
            spec = QColor::Hsv;
        } else if (value["H"].exists() && value["S"].exists() && value["L"].exists()) {
            spec = QColor::Hsl;
        } else if (value["C"].exists() &&
                value["M"].exists() &&
                value["Y"].exists() &&
                value["K"].exists()) {
            spec = QColor::Cmyk;
        } else if (!value["R"].exists() &&
                !value["G"].exists() &&
                !value["B"].exists() &&
                !value["A"].exists()) {
            return defaultColor;
        }
    }

    switch (spec) {
    case QColor::Hsv: {
        QColor result;
        if (value["A"].exists()) {
            result.setHsvF(colorBounded(value["H"]), colorBounded(value["S"]),
                           colorBounded(value["V"]), colorBounded(value["A"]));
        } else {
            result.setHsvF(colorBounded(value["H"]), colorBounded(value["S"]),
                           colorBounded(value["V"]));
        }
        return result;
    }
    case QColor::Hsl: {
        QColor result;
        if (value["A"].exists()) {
            result.setHslF(colorBounded(value["H"]), colorBounded(value["S"]),
                           colorBounded(value["L"]), colorBounded(value["A"]));
        } else {
            result.setHslF(colorBounded(value["H"]), colorBounded(value["S"]),
                           colorBounded(value["L"]));
        }
        return result;
    }
    case QColor::Cmyk: {
        QColor result;
        if (value["A"].exists()) {
            result.setCmykF(colorBounded(value["C"]), colorBounded(value["M"]),
                            colorBounded(value["Y"]), colorBounded(value["K"]),
                            colorBounded(value["A"]));
        } else {
            result.setCmykF(colorBounded(value["C"]), colorBounded(value["M"]),
                            colorBounded(value["Y"]), colorBounded(value["K"]));
        }
        return result;
    }
    default:
        break;
    }

    qreal r = 0.0;
    qreal g = 0.0;
    qreal b = 0.0;
    qreal a = 1.0;
    if (defaultColor.isValid())
        defaultColor.getRgbF(&r, &g, &b, &a);
    if (value["R"].exists())
        r = colorBounded(value["R"]);
    if (value["G"].exists())
        g = colorBounded(value["G"]);
    if (value["B"].exists())
        b = colorBounded(value["B"]);
    if (value["A"].exists())
        a = colorBounded(value["A"]);
    QColor result;
    result.setRgbF(r, g, b, a);
    return result;
}

/**
 * Format the given color into a Value for writing.
 * 
 * @param color the input color
 * @return      the formatted color
 */
Variant::Root Display::formatColor(const QColor &color)
{
    Variant::Root value;
    switch (color.spec()) {
    case QColor::Hsv: {
        qreal h, s, v, a;
        color.getHsvF(&h, &s, &v, &a);
        value["Model"].setString("HSV");
        value["H"].setDouble(h);
        value["S"].setDouble(s);
        value["V"].setDouble(v);
        value["A"].setDouble(a);
        break;
    }
    case QColor::Hsl: {
        qreal h, s, l, a;
        color.getHslF(&h, &s, &l, &a);
        value["Model"].setString("HSL");
        value["H"].setDouble(h);
        value["S"].setDouble(s);
        value["L"].setDouble(l);
        value["A"].setDouble(a);
        break;
    }
    case QColor::Cmyk: {
        qreal c, m, y, k, a;
        /* Qt does not define getCmykF as const */
        {
            QColor copy(color);
            copy.getCmykF(&c, &m, &y, &k, &a);
        }
        value["Model"].setString("CMYK");
        value["C"].setDouble(c);
        value["M"].setDouble(m);
        value["Y"].setDouble(y);
        value["K"].setDouble(k);
        value["A"].setDouble(a);
        break;
    }
    default: {
        qreal r, g, b, a;
        color.getRgbF(&r, &g, &b, &a);
        value["Model"].setString("RGB");
        value["R"].setDouble(r);
        value["G"].setDouble(g);
        value["B"].setDouble(b);
        value["A"].setDouble(a);
        break;
    }
    }
    return value;
}

/**
 * Parse a value into a font.
 * 
 * @param value         the value to parse
 * @param defaultFont   the default font to use
 * @return              a font
 */
QFont Display::parseFont(const Variant::Read &value, const QFont &defaultFont)
{
    QFont result(defaultFont);
    if (value["Family"].exists())
        result.setFamily(value["Family"].toQString());

    if (value["Flags"].exists()) {
        const auto &flags = value["Flags"].toFlags();
        result.setBold(flags.count("Bold"));
        result.setFixedPitch(flags.count("FixedPitch"));
        result.setItalic(flags.count("Italic"));
        result.setKerning(!flags.count("NoKerning"));
        result.setOverline(flags.count("Overline"));
        result.setStrikeOut(flags.count("StrikeOut"));
        result.setUnderline(flags.count("Underline"));
    }

    if (value["LetterSpacing"].exists()) {
        QFont::SpacingType type = QFont::PercentageSpacing;
        double spacing = 100.0;
        if (value["LetterSpacing/Type"].exists() &&
                Util::equal_insensitive(value["LetterSpacing/Type"].toString(), "absolute")) {
            type = QFont::AbsoluteSpacing;
            spacing = 0.0;
        }
        if (value["LetterSpacing/Amount"].exists()) {
            spacing = value["LetterSpacing/Amount"].toDouble();
        } else if (FP::defined(value["Spacing"].toDouble())) {
            spacing = value["LetterSpacing"].toDouble();
        }
        if (type == QFont::PercentageSpacing && spacing <= 0.0)
            spacing = 100.0;
        result.setLetterSpacing(type, spacing);
    }

    if (value["Size"].exists()) {
        if (Util::equal_insensitive(value["Size/Type"].toString(), "pixel")) {
            qint64 i = value["Size/Number"].toInt64();
            if (INTEGER::defined(i) && i > 0) {
                result.setPixelSize((int) i);
            }
        } else if (value["Size/Number"].exists()) {
            double f = value["Size/Number"].toDouble();
            if (FP::defined(f) && f > 0.0) {
                result.setPointSizeF(f);
            }
        }
    }

    if (value["Stretch"].exists()) {
        qint64 i = value["Stretch"].toInt64();
        if (INTEGER::defined(i) && i >= 1 && i <= 4000) {
            result.setStretch((int) i);
        }
    }

    if (value["Weight"].exists()) {
        qint64 i = value["Weight"].toInt64();
        if (INTEGER::defined(i) && i >= 0 && i <= 99) {
            result.setWeight((int) i);
        }
    }

    if (value["WordSpacing"].exists()) {
        double f = value["WordSpacing"].toDouble();
        if (FP::defined(f)) {
            result.setWordSpacing(f);
        }
    }

    return result;
}

/**
 * Format the given font into a Value for writing.
 * 
 * @param font  the font color
 * @return      the formatted font
 */
Variant::Root Display::formatFont(const QFont &font)
{
    Variant::Root value;
    value["Family"].setString(font.family());
    value["Stretch"].setInt64(font.stretch());
    value["Weight"].setInt64(font.weight());
    value["WordSpacing"].setDouble(font.wordSpacing());

    Variant::Flags flags;
    if (font.bold()) flags.insert("Bold");
    if (font.fixedPitch()) flags.insert("FixedPitch");
    if (font.italic()) flags.insert("Italic");
    if (!font.kerning()) flags.insert("NoKerning");
    if (font.overline()) flags.insert("Overline");
    if (font.strikeOut()) flags.insert("StrikeOut");
    if (font.underline()) flags.insert("Underline");
    value["Flags"].setFlags(std::move(flags));

    switch (font.letterSpacingType()) {
    case QFont::PercentageSpacing:
        value["LetterSpacing/Type"].setString("Percentage");
        break;
    case QFont::AbsoluteSpacing:
        value["LetterSpacing/Type"].setString("Absolute");
        break;
    }
    value["LetterSpacing/Amount"].setDouble(font.letterSpacing());

    if (font.pixelSize() > 0) {
        value["Size/Type"].setString("Pixel");
        value["Size/Number"].setInt64(font.pixelSize());
    } else {
        Q_ASSERT(font.pointSizeF() >= 0.0);
        value["Size/Type"].setString("Point");
        value["Size/Number"].setDouble(font.pointSizeF());
    }

    return value;
}

/**
 * Parse a value into a pen style.
 * 
 * @param value         the value to parse
 * @param defaultStyle  the default style
 * @return              a pen style
 */
Qt::PenStyle Display::parsePenStyle(const Variant::Read &value, Qt::PenStyle defaultStyle)
{
    const auto &name = value.toString();
    if (Util::equal_insensitive(name, "solid"))
        return Qt::SolidLine;
    if (Util::equal_insensitive(name, "dash"))
        return Qt::DashLine;
    if (Util::equal_insensitive(name, "dot"))
        return Qt::DotLine;
    if (Util::equal_insensitive(name, "dashdot"))
        return Qt::DashDotLine;
    if (Util::equal_insensitive(name, "dashdotdot", "dotdotdash"))
        return Qt::DashDotDotLine;
    return defaultStyle;
}

/**
 * Format the given pen style into a Value for writing.
 * 
 * @param style the input style
 * @return      the formatted style
 */
Variant::Root Display::formatPenStyle(Qt::PenStyle style)
{
    switch (style) {
    case Qt::DashLine:
        return Variant::Root("Dash");
    case Qt::DotLine:
        return Variant::Root("Dot");
    case Qt::DashDotLine:
        return Variant::Root("DashDot");
    case Qt::DashDotDotLine:
        return Variant::Root("DashDotDot");
    default:
        return Variant::Root("Solid");
    }
    Q_ASSERT(false);
    return Variant::Root();
}

/**
 * Create a menu for selecting the pen style.
 *
 * @param parent    the parent context
 * @param style     the current style
 * @param set       the callback to set the style
 * @return          a selection menu
 */
QMenu *Display::selectPenStyle(Qt::PenStyle style,
                               const std::function<void(Qt::PenStyle)> &set,
                               QWidget *parent)
{
    QAction *act;
    QActionGroup *group;
    QMenu *menu;

    menu = new QMenu(tr("Line St&yle", "Context|SetPenStyle"), parent);

    group = new QActionGroup(menu);

    class PenStyleEngine : public QIconEngine {
        Qt::PenStyle style;
    public:
        explicit PenStyleEngine(Qt::PenStyle style) : style(style)
        { }

        virtual ~PenStyleEngine() = default;

        QIconEngine *clone() const override
        { return new PenStyleEngine(style); }

        void paint(QPainter *painter,
                   const QRect &rect,
                   QIcon::Mode mode,
                   QIcon::State state) override
        {
            painter->save();

            auto color = QApplication::palette().text().color();
            painter->setPen(QPen(QBrush(color), 0, style));

            auto midY = rect.top() + rect.height() / 2;
            painter->drawLine(rect.left(), midY, rect.right(), midY);

            painter->restore();
        }

        QPixmap pixmap(const QSize &size, QIcon::Mode mode, QIcon::State state) override
        {
            QImage img(size, QImage::Format_ARGB32);
            img.fill(0x00FFFFFF);
            QPixmap result = QPixmap::fromImage(img, Qt::NoFormatConversion);
            {
                QPainter painter(&result);
                this->paint(&painter, QRect(QPoint(0, 0), size), mode, state);
            }
            return result;
        }
    };

    act = menu->addAction(tr("&Solid", "Context|SetPenStyleSolid"));
    act->setToolTip(tr("A solid line."));
    act->setStatusTip(tr("Set line style"));
    act->setWhatsThis(tr("Draw a solid line between points."));
    act->setCheckable(true);
    act->setChecked(style == Qt::SolidLine);
    act->setActionGroup(group);
    act->setIcon(QIcon(new PenStyleEngine(Qt::SolidLine)));
    QObject::connect(act, &QAction::triggered, menu, std::bind(set, Qt::SolidLine));

    act = menu->addAction(tr("&Dash", "Context|SetPenStyleDash"));
    act->setToolTip(tr("A dashed line."));
    act->setStatusTip(tr("Set line style"));
    act->setWhatsThis(tr("Draw a dashed line between points."));
    act->setCheckable(true);
    act->setChecked(style == Qt::DashLine);
    act->setActionGroup(group);
    act->setIcon(QIcon(new PenStyleEngine(Qt::DashLine)));
    QObject::connect(act, &QAction::triggered, menu, std::bind(set, Qt::DashLine));

    act = menu->addAction(tr("D&ot", "Context|SetPenStyleDot"));
    act->setToolTip(tr("A dotted line."));
    act->setStatusTip(tr("Set line style"));
    act->setWhatsThis(tr("Draw a dotted line between points."));
    act->setCheckable(true);
    act->setChecked(style == Qt::DotLine);
    act->setActionGroup(group);
    act->setIcon(QIcon(new PenStyleEngine(Qt::DotLine)));
    QObject::connect(act, &QAction::triggered, menu, std::bind(set, Qt::DotLine));

    act = menu->addAction(tr("D&ash Dot", "Context|SetPenStyleDashDot"));
    act->setToolTip(tr("A line of alternating dashes and dots."));
    act->setStatusTip(tr("Set line style"));
    act->setWhatsThis(tr("Draw a line that alternates between dashed and dotted segments."));
    act->setCheckable(true);
    act->setChecked(style == Qt::DashDotLine);
    act->setActionGroup(group);
    act->setIcon(QIcon(new PenStyleEngine(Qt::DashDotLine)));
    QObject::connect(act, &QAction::triggered, menu, std::bind(set, Qt::DashDotLine));

    act = menu->addAction(tr("Dash do&t dot", "Context|SetPenStyleDashDotDot"));
    act->setToolTip(tr("A line dashes with two dots between them."));
    act->setStatusTip(tr("Set line style"));
    act->setWhatsThis(tr("Draw a line with two dots between dashed segments."));
    act->setCheckable(true);
    act->setChecked(style == Qt::DashDotDotLine);
    act->setActionGroup(group);
    act->setIcon(QIcon(new PenStyleEngine(Qt::DashDotDotLine)));
    QObject::connect(act, &QAction::triggered, menu, std::bind(set, Qt::DashDotDotLine));

    return menu;
}

}
}
