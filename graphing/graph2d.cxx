/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>
#include <QMenu>
#include <QColorDialog>
#include <QFontDialog>
#include <QInputDialog>

#include "graphing/graph2d.hxx"
#include "guicore/guiformat.hxx"

using namespace CPD3::Data;
using namespace CPD3::GUI;
using namespace CPD3::Graphing::Internal;

namespace CPD3 {
namespace Graphing {

/** @file graphing/graph2d.hxx
 * The basis for simple two dimensional graphs.
 */

static const qreal topTitleBelowSpace = 0.1;
static const qreal legendOutsideSpacing = 1.0;

bool Graph2D::mergable(const Variant::Read &a, const Variant::Read &b)
{
    if (!TraceSet2D::mergable(a["Traces"], b["Traces"]))
        return false;
    if (!BinSet2D::mergable(a["XBins"], b["XBins"]))
        return false;
    if (!BinSet2D::mergable(a["YBins"], b["YBins"]))
        return false;
    if (!IndicatorSet2D::mergable(a["XIndicators"], b["XIndicators"]))
        return false;
    if (!IndicatorSet2D::mergable(a["YIndicators"], b["YIndicators"]))
        return false;
    if (!AxisDimensionSet2DSide::mergable(a["XAxes"], b["XAxes"]))
        return false;
    if (!AxisDimensionSet2DSide::mergable(a["YAxes"], b["YAxes"]))
        return false;
    if (!AxisDimensionSet2DColor::mergable(a["ZAxes"], b["ZAxes"]))
        return false;
    return true;
}

void Graph2D::connectComponents()
{
    if (traces) {
        connect(traces.get(), &TraceDispatchSetBase::deferredUpdate, this,
                std::bind(&Display::interactiveRepaint, this, false));
        connect(traces.get(), &TraceDispatchSetBase::readyForFinalProcessing, this,
                &Graph2D::tracesReady);
        connect(traces.get(), &TraceDispatchSetBase::handlersChanged, this,
                &Display::outputsChanged);
    }

    if (xBins) {
        connect(xBins.get(), &TraceDispatchSetBase::deferredUpdate, this,
                std::bind(&Display::interactiveRepaint, this, false));
        connect(xBins.get(), &TraceDispatchSetBase::readyForFinalProcessing, this,
                &Graph2D::xBinsReady);
        connect(xBins.get(), &TraceDispatchSetBase::handlersChanged, this,
                &Display::outputsChanged);
    }

    if (yBins) {
        connect(yBins.get(), &TraceDispatchSetBase::deferredUpdate, this,
                std::bind(&Display::interactiveRepaint, this, false));
        connect(yBins.get(), &TraceDispatchSetBase::readyForFinalProcessing, this,
                &Graph2D::yBinsReady);
        connect(yBins.get(), &TraceDispatchSetBase::handlersChanged, this,
                &Display::outputsChanged);
    }

    if (xIndicators) {
        connect(xIndicators.get(), &TraceDispatchSetBase::deferredUpdate, this,
                std::bind(&Display::interactiveRepaint, this, false));
        connect(xIndicators.get(), &TraceDispatchSetBase::readyForFinalProcessing, this,
                &Graph2D::xIndicatorsReady);
        connect(xIndicators.get(), &TraceDispatchSetBase::handlersChanged, this,
                &Display::outputsChanged);
    }

    if (yIndicators) {
        connect(yIndicators.get(), &TraceDispatchSetBase::deferredUpdate, this,
                std::bind(&Display::interactiveRepaint, this, false));
        connect(yIndicators.get(), &TraceDispatchSetBase::readyForFinalProcessing, this,
                &Graph2D::yIndicatorsReady);
        connect(yIndicators.get(), &TraceDispatchSetBase::handlersChanged, this,
                &Display::outputsChanged);
    }

    if (xAxes) {
        connect(xAxes.get(), &AxisDimensionSet::zoomUpdated, this, &Graph2D::zoomUpdated);
    }

    if (yAxes) {
        connect(yAxes.get(), &AxisDimensionSet::zoomUpdated, this, &Graph2D::zoomUpdated);
    }

    if (zAxes) {
        connect(zAxes.get(), &AxisDimensionSet::zoomUpdated, this, &Graph2D::zoomUpdated);
    }
}

/**
 * Initialize the graph.
 * <br>
 * Must be called (even by children) before any other functions are called.
 * 
 * @param config    the configuration
 * @param defaults  the graph defaults
 */
void Graph2D::initialize(const ValueSegment::Transfer &config, const DisplayDefaults &defaults)
{
    settings = defaults.getSettings();

    for (const auto &it : config) {
        if (!parameters) {
            parameters = parseParameters(it.read(), defaults);
            Q_ASSERT(parameters.get() != nullptr);
            continue;
        }
        auto over = parseParameters(it.read(), defaults);
        parameters->initializeOverlay(over.get());
    }
    if (!parameters) {
        parameters = parseParameters(Variant::Read::empty(), defaults);
        Q_ASSERT(parameters.get() != nullptr);

        titleText = parameters->getTitleText();
    }

    traces = createTraces();
    Q_ASSERT(traces.get() != nullptr);
    xBins = createXBins();
    Q_ASSERT(xBins.get() != nullptr);
    yBins = createYBins();
    Q_ASSERT(yBins.get() != nullptr);
    xIndicators = createXIndicators();
    Q_ASSERT(xIndicators.get() != nullptr);
    yIndicators = createYIndicators();
    Q_ASSERT(yIndicators.get() != nullptr);
    xAxes = createXAxes();
    Q_ASSERT(xAxes.get() != nullptr);
    yAxes = createYAxes();
    Q_ASSERT(yAxes.get() != nullptr);
    zAxes = createZAxes();
    Q_ASSERT(zAxes.get() != nullptr);

    connectComponents();

    traces->initialize(ValueSegment::withPath(config, "Traces"), defaults);
    xBins->initialize(ValueSegment::withPath(config, "XBins"), defaults);
    yBins->initialize(ValueSegment::withPath(config, "YBins"), defaults);
    xIndicators->initialize(ValueSegment::withPath(config, "XIndicators"), defaults);
    yIndicators->initialize(ValueSegment::withPath(config, "YIndicators"), defaults);
    xAxes->initialize(ValueSegment::withPath(config, "XAxes"), defaults);
    yAxes->initialize(ValueSegment::withPath(config, "YAxes"), defaults);
    zAxes->initialize(ValueSegment::withPath(config, "ZAxes"), defaults);
}

Graph2D::Graph2D(const DisplayDefaults &defaults, QObject *parent) : Display(defaults, parent),
                                                                     settings(
                                                                             defaults.getSettings()),
                                                                     deferredChanged(true),
                                                                     priorDimensions(),
                                                                     priorOrigin(),
                                                                     traceArea(),
                                                                     havePrepared(false),
                                                                     haveFinalized(false),
                                                                     haveEverPrepared(false),
                                                                     haveEverFinalized(false),
                                                                     haveEverRendered(false),
                                                                     ready(0),
                                                                     leftTotalInset(0),
                                                                     rightTotalInset(0),
                                                                     topTotalInset(0),
                                                                     bottomTotalInset(0),
                                                                     previousLeftTotalInset(0),
                                                                     previousRightTotalInset(0),
                                                                     previousTopTotalInset(0),
                                                                     previousBottomTotalInset(0),
                                                                     leftAxisSideDepth(0),
                                                                     rightAxisSideDepth(0),
                                                                     topAxisSideDepth(0),
                                                                     bottomAxisSideDepth(0),
                                                                     mouseoverValid(false),
                                                                     mouseoverChanged(true),
                                                                     mouseoverDone(false),
                                                                     mouseoverPoint(),
                                                                     mouseZoomMode(Zoom_None),
                                                                     pendingMouseZoom(Zoom_None)
{
    deferredPaintHandler.finished.connect(this, "deferredPaintFinished");
    mouseoverUpdateHandler.finished.connect(this, "mouseoverUpdateFinished");
}

Graph2D::~Graph2D() = default;

Graph2D::Graph2D(const Graph2D &other) : Display(other),
                                         settings(other.settings),
                                         deferredChanged(true),
                                         havePrepared(false),
                                         haveFinalized(false),
                                         haveEverPrepared(false),
                                         haveEverFinalized(false),
                                         haveEverRendered(false),
                                         ready(0),
                                         highlights(other.highlights),
                                         leftTotalInset(0),
                                         rightTotalInset(0),
                                         topTotalInset(0),
                                         bottomTotalInset(0),
                                         previousLeftTotalInset(0),
                                         previousRightTotalInset(0),
                                         previousTopTotalInset(0),
                                         previousBottomTotalInset(0),
                                         leftAxisSideDepth(0),
                                         rightAxisSideDepth(0),
                                         topAxisSideDepth(0),
                                         bottomAxisSideDepth(0),
                                         mouseoverValid(false),
                                         mouseoverChanged(true),
                                         mouseoverDone(false),
                                         mouseoverPoint(),
                                         mouseZoomMode(Zoom_None),
                                         mouseZoomA(),
                                         mouseZoomB(),
                                         pendingMouseZoom(Zoom_None)
{
    deferredPaintHandler.finished.connect(this, "deferredPaintFinished");
    mouseoverUpdateHandler.finished.connect(this, "mouseoverUpdateFinished");

    if (other.traces)
        traces.reset(static_cast<TraceSet2D *>(other.traces->clone().release()));
    if (other.xBins)
        xBins.reset(static_cast<BinSet2D *>(other.xBins->clone().release()));
    if (other.yBins)
        yBins.reset(static_cast<BinSet2D *>(other.yBins->clone().release()));
    if (other.xIndicators)
        xIndicators.reset(static_cast<IndicatorSet2D *>(other.xIndicators->clone().release()));
    if (other.yIndicators)
        yIndicators.reset(static_cast<IndicatorSet2D *>(other.yIndicators->clone().release()));
    if (other.xAxes)
        xAxes.reset(static_cast<AxisDimensionSet2DSide *>(other.xAxes->clone().release()));
    if (other.yAxes)
        yAxes.reset(static_cast<AxisDimensionSet2DSide *>(other.yAxes->clone().release()));
    if (other.zAxes)
        zAxes.reset(static_cast<AxisDimensionSet2DColor *>(other.zAxes->clone().release()));
    if (other.parameters)
        parameters = other.parameters->clone();

    connectComponents();
}

std::unique_ptr<Display> Graph2D::clone() const
{ return std::unique_ptr<Display>(new Graph2D(*this)); }

void Graph2D::registerChain(ProcessingTapChain *chain)
{
    ready = 0;
    traces->registerChain(chain);
    xBins->registerChain(chain);
    yBins->registerChain(chain);
    xIndicators->registerChain(chain);
    yIndicators->registerChain(chain);
}

std::vector<
        std::shared_ptr<DisplayOutput>> Graph2D::getOutputs(const DisplayDynamicContext &context)
{
    auto result = traces->getOutputs(context);
    Util::append(xBins->getOutputs(context), result);
    Util::append(yBins->getOutputs(context), result);
    Util::append(xIndicators->getOutputs(context), result);
    Util::append(yIndicators->getOutputs(context), result);

    for (const auto &it : result) {
        connect(it.get(), &DisplayOutput::changed, this, &Graph2D::modificationChanged);
    }
    return result;
}

static const int zoomDirectionDominateThreshold = 5;
static const int zoomDirectionBothThreshold = 15;

void Graph2D::updateMouseZoomMode()
{
    if (mouseZoomMode == Zoom_None)
        return;
    Qt::KeyboardModifiers modifiers = QApplication::keyboardModifiers();
    if ((modifiers & Qt::ControlModifier) && (modifiers & (Qt::AltModifier | Qt::ShiftModifier))) {
        mouseZoomMode = Zoom_ForceBoth;
    } else if (modifiers & Qt::ControlModifier) {
        mouseZoomMode = Zoom_ForceX;
    } else if (modifiers & (Qt::AltModifier | Qt::ShiftModifier)) {
        mouseZoomMode = Zoom_ForceY;
    } else {
        QPoint delta = mouseZoomA - mouseZoomB;
        if (abs(delta.x()) > zoomDirectionBothThreshold &&
                abs(delta.y()) > zoomDirectionBothThreshold) {
            mouseZoomMode = Zoom_AutoBoth;
        } else if (abs(delta.y()) > zoomDirectionDominateThreshold) {
            mouseZoomMode = Zoom_AutoY;
        } else {
            mouseZoomMode = Zoom_AutoX;
        }
    }
}

void Graph2D::applyMouseZoom(const QPointF &origin)
{
    double minX = mouseZoomA.x() - origin.x();
    double maxX = mouseZoomB.x() - origin.x();
    if (minX > maxX)
        qSwap(minX, maxX);

    double minY = mouseZoomA.y() - origin.y();
    double maxY = mouseZoomB.y() - origin.y();
    if (minY < maxY)
        qSwap(minY, maxY);

    switch (pendingMouseZoom) {
    case Zoom_None:
        Q_ASSERT(false);
        break;
    case Zoom_AutoX:
    case Zoom_ForceX:
        zoomMouseX(xAxes.get(), minX, maxX);
        break;

    case Zoom_AutoY:
    case Zoom_ForceY:
        zoomMouseY(yAxes.get(), minY, maxY);
        break;

    case Zoom_AutoBoth:
    case Zoom_ForceBoth:
        zoomMouseBoth(xAxes.get(), yAxes.get(), minX, maxX, minY, maxY);
        break;
    }

    pendingMouseZoom = Zoom_None;
}

void Graph2D::interactionStopped()
{
    if (mouseZoomMode != Zoom_None) {
        mouseZoomMode = Zoom_None;
        emit interactiveRepaint(true);
    }
}

bool Graph2D::eventMousePress(QMouseEvent *event)
{
    if (mouseZoomMode != Zoom_None) {
        if (event->button() & Qt::LeftButton) {
            mouseZoomB = event->pos();
            updateMouseZoomMode();
            pendingMouseZoom = mouseZoomMode;
            deferredChanged = true;
        }
        emit setKeyboardGrab(false);
        mouseZoomMode = Zoom_None;
        emit interactiveRepaint(true);
        return true;
    } else if (event->button() & Qt::LeftButton) {
        if (mouseoverValid) {
            clearMouseover();
            emit setToolTip(QPoint(), QString());
        }

        mouseZoomMode = Zoom_AutoX;
        mouseZoomA = event->pos();
        mouseZoomB = mouseZoomA;
        emit setKeyboardGrab(true);
        emit interactiveRepaint(true);
        return true;
    }
    return false;
}

bool Graph2D::eventKeyPress(QKeyEvent *event)
{
    Q_UNUSED(event);
    if (mouseZoomMode != Zoom_None) {
        emit interactiveRepaint(true);
    }
    return false;
}

bool Graph2D::eventKeyRelease(QKeyEvent *event)
{
    Q_UNUSED(event);
    if (mouseZoomMode != Zoom_None) {
        emit interactiveRepaint(true);
    }
    return false;
}

static const int dragZoomThreshold = 3;

bool Graph2D::eventMouseRelease(QMouseEvent *event)
{
    if (mouseZoomMode != Zoom_None && (event->button() & Qt::LeftButton)) {
        if ((mouseZoomA - event->pos()).manhattanLength() > dragZoomThreshold) {
            mouseZoomB = event->pos();
            updateMouseZoomMode();
            pendingMouseZoom = mouseZoomMode;
            deferredChanged = true;
            mouseZoomMode = Zoom_None;
            emit setKeyboardGrab(false);
            emit interactiveRepaint(true);
            return true;
        }
    }
    return false;
}

bool Graph2D::eventMouseMove(QMouseEvent *event)
{
    if (mouseZoomMode != Zoom_None) {
        mouseZoomB = event->pos();
        emit interactiveRepaint(true);
        return true;
    }
    return false;
}

std::shared_ptr<DisplayModificationComponent> Graph2D::getMouseModification(const QPoint &point,
                                                                            const QRectF &dimensions,
                                                                            const DisplayDynamicContext &context)
{
    if (!haveEverRendered)
        return std::shared_ptr<DisplayModificationComponent>();

    PrimitiveList sharedList;
    if (deferredPaintList.isValid()) {
        sharedList = deferredPaintList;
    } else if (mouseoverUpdateList.isValid()) {
        sharedList = mouseoverUpdateList;
    }
    deferredPaintHandler.waitAvailable();
    mouseoverUpdateHandler.waitAvailable();

    std::shared_ptr<Graph2DMouseoverComponent> component;
    if (!sharedList.isValid()) {
        QRectF renderDimensions(dimensions);
        if (!renderDimensions.isValid()) {
            renderDimensions = QRectF(priorOrigin.x(), priorOrigin.y(), priorDimensions.width(),
                                      priorDimensions.height());
        }
        if (!havePrepared)
            prepareLayout(renderDimensions, nullptr, context);
        if (!haveFinalized)
            finalizeLayout(renderDimensions, nullptr, context);
        for (const auto &it : makeDrawList(renderDimensions, nullptr)) {
            it->updateMouseClick(point, component);
        }
    } else {
        for (const auto &it : sharedList.acquire()) {
            it->updateMouseClick(point, component);
        }
        sharedList.release();
    }

    if (!component)
        return {};

    std::shared_ptr<DisplayModificationComponent>
            result(convertModificationComponent(component.get()));
    if (!result)
        return {};
    connect(result.get(), &DisplayModificationComponent::changed, this,
            &Graph2D::modificationChanged);
    return result;
}

std::vector<std::shared_ptr<DisplayZoomAxis>> Graph2D::zoomAxes()
{
    auto result = yAxes->getZoom();
    Util::append(xAxes->getZoom(), result);
    Util::append(zAxes->getZoom(), result);
    return result;
}

namespace {

class Graph2DHighlightController : public DisplayHighlightController {
    QPointer<Graph2D> parent;
public:
    Graph2DHighlightController(Graph2D *setParent) : parent(setParent)
    { }

    void clear() override
    {
        if (parent.isNull())
            return;
        parent->clearHighlight();
    }

    void add(DisplayHighlightController::HighlightType type,
             double start,
             double end,
             double min = FP::undefined(),
             double max = FP::undefined(),
             const SequenceMatch::Composite &selection = SequenceMatch::Composite(
                     SequenceMatch::Element::SpecialMatch::All)) override
    {
        if (parent.isNull())
            return;
        parent->addHighlight(type, start, end, min, max, selection);
    }
};

}

std::shared_ptr<DisplayHighlightController> Graph2D::highlightController()
{
    return std::shared_ptr<DisplayHighlightController>(new Graph2DHighlightController(this));
}

ValueSegment::Transfer Graph2D::getManualOverrides() const
{
    if (!overridenParameters)
        return ValueSegment::Transfer();
    Variant::Root result;
    {
        auto wr = result.write();
        overridenParameters->save(wr);
    }
    if (!result.read().exists())
        return ValueSegment::Transfer();
    return ValueSegment::Transfer{
            ValueSegment(FP::undefined(), FP::undefined(), std::move(result))};
}

void Graph2D::saveOverrides(DisplaySaveContext &target)
{
    TraceDispatchOverride dispatchOverride(target.traceDispatchOverride(this));

    target.overlay(traces->getManualOverrides(dispatchOverride), "Traces");
    target.overlay(xBins->getManualOverrides(dispatchOverride), "XBins");
    target.overlay(yBins->getManualOverrides(dispatchOverride), "YBins");
    target.overlay(xIndicators->getManualOverrides(dispatchOverride), "XIndicators");
    target.overlay(yIndicators->getManualOverrides(dispatchOverride), "YIndicators");
    target.overlay(xAxes->getManualOverrides(), "XAxes");
    target.overlay(yAxes->getManualOverrides(), "YAxes");
    target.overlay(zAxes->getManualOverrides(), "ZAxes");
    target.overlay(getManualOverrides());
}

void Graph2D::resetOverrides()
{
    traces->resetManualOverrides();
    xBins->resetManualOverrides();
    yBins->resetManualOverrides();
    xIndicators->resetManualOverrides();
    yIndicators->resetManualOverrides();
    xAxes->resetManualOverrides();
    zAxes->resetManualOverrides();
    yAxes->resetManualOverrides();
    revertOverrideParameters();

    deferredChanged = true;
    emit interactiveRepaint(true);
}

void Graph2D::revertOverrideParameters()
{
    if (!originalParameters)
        return;
    parameters->copy(originalParameters.get());
    originalParameters.reset();
    Q_ASSERT(overridenParameters.get() != nullptr);
    overridenParameters.reset();
}

/**
 * Get the override parameters.  This also flags the graph as having
 * had something overriden.
 * 
 * @return the override parameters
 */
Graph2DParameters *Graph2D::overrideParameters()
{
    if (!overridenParameters) {
        overridenParameters = parameters->clone();
        overridenParameters->clear();
        Q_ASSERT(originalParameters.get() == nullptr);
        originalParameters = parameters->clone();
    }
    return overridenParameters.get();
}

/**
 * Merge all overrides into the main parameters.
 */
void Graph2D::mergeOverrideParameters()
{
    Q_ASSERT(overridenParameters.get() != nullptr);
    parameters->overlay(overridenParameters.get());
}

void Graph2D::trimData(double start, double end)
{
    deferredChanged = true;
    traces->trimData(start, end);
    xBins->trimData(start, end);
    yBins->trimData(start, end);
    xIndicators->trimData(start, end);
    yIndicators->trimData(start, end);
}

void Graph2D::setVisibleTimeRange(double start, double end)
{
    deferredChanged = true;
    traces->setVisibleTimeRange(start, end);
    xBins->setVisibleTimeRange(start, end);
    yBins->setVisibleTimeRange(start, end);
    xIndicators->setVisibleTimeRange(start, end);
    yIndicators->setVisibleTimeRange(start, end);
}

std::vector<std::shared_ptr<DisplayCoupling>> Graph2D::getDataCoupling()
{
    auto result = xAxes->createRangeCoupling(DisplayCoupling::SameColumn);
    Util::append(yAxes->createRangeCoupling(DisplayCoupling::SameRow), result);
    Util::append(zAxes->createRangeCoupling(DisplayCoupling::Always), result);
    return result;
}

QSizeF Graph2D::getMinimumSize(QPaintDevice *paintdevice, const DisplayDynamicContext &context)
{
    if (!haveEverPrepared)
        prepareLayout(QRectF(), paintdevice, context);
    if (!haveEverFinalized)
        finalizeLayout(QRectF(), paintdevice, context);

    QFontMetrics fm(QApplication::font());
    if (paintdevice)
        fm = QFontMetrics(QApplication::font(), paintdevice);
    qreal baseSize = fm.height();

    return QSizeF(leftTotalInset + rightTotalInset + baseSize * 2.0,
                  topTotalInset + bottomTotalInset + baseSize * 2.0);
}

template<typename HandlerType>
static AxisDimension *bindAxis(AxisDimensionSet *axes,
                               const HandlerType *handler,
                               int dim,
                               bool hasBinding,
                               const QString &binding)
{
    if (FP::defined(handler->getLimits().min[dim])) {
        AxisDimension *bound;
        if (hasBinding) {
            bound = axes->get(handler->getUnits(dim), handler->getGroupUnits(dim), binding);
        } else {
            bound = axes->get(handler->getUnits(dim), handler->getGroupUnits(dim));
        }
        bound->registerDataLimits(handler->getLimits().min[dim], handler->getLimits().max[dim]);
        return bound;
    } else if (hasBinding) {
        return axes->get(handler->getUnits(dim), handler->getGroupUnits(dim), binding);
    } else {
        return nullptr;
    }
}

static void extractUniqueBindings(std::unordered_map<AxisDimension2DSide *,
                                                     TraceHandler2D *> &traces,
                                  std::unordered_map<AxisDimension2DSide *, BinHandler2D *> &bins,
                                  std::unordered_map<AxisDimension2DSide *,
                                                     IndicatorHandler2D *> &indicators)
{
    for (auto i = traces.begin(); i != traces.end();) {
        if (bins.erase(i->first) != 0 || indicators.erase(i->first) != 0 || !i->second) {
            i = traces.erase(i);
            continue;
        }
        ++i;
    }
    for (auto i = bins.begin(); i != bins.end();) {
        if (traces.erase(i->first) != 0 || indicators.erase(i->first) != 0 || !i->second) {
            i = bins.erase(i);
            continue;
        }
        ++i;
    }
    for (auto i = indicators.begin(); i != indicators.end();) {
        if (bins.erase(i->first) != 0 || traces.erase(i->first) != 0 || !i->second) {
            i = indicators.erase(i);
            continue;
        }
        ++i;
    }
}

void Graph2D::prepareLayout(const QRectF &maximumDimensions,
                            QPaintDevice *paintdevice,
                            const DisplayDynamicContext &context)
{
    Q_UNUSED(maximumDimensions);
    Q_UNUSED(paintdevice);
    havePrepared = true;
    haveEverPrepared = true;

    if (traces->process(false, context.getInteractive()))
        deferredChanged = true;
    if (xBins->process(false, context.getInteractive()))
        deferredChanged = true;
    if (yBins->process(false, context.getInteractive()))
        deferredChanged = true;
    if (xIndicators->process(false, context.getInteractive()))
        deferredChanged = true;
    if (yIndicators->process(false, context.getInteractive()))
        deferredChanged = true;
    if (auxiliaryProcess(context))
        deferredChanged = true;

    xAxes->beginUpdate();
    yAxes->beginUpdate();
    zAxes->beginUpdate();

    std::vector<TraceUtilities::ColorRequest> findColors;
    std::deque<QColor> claimedColors;
    std::unordered_map<AxisDimension2DSide *, TraceHandler2D *> uniqueTraceXBindings;
    std::unordered_map<AxisDimension2DSide *, TraceHandler2D *> uniqueTraceYBindings;
    std::unordered_map<AxisDimension2DSide *, BinHandler2D *> uniqueBinXBindings;
    std::unordered_map<AxisDimension2DSide *, BinHandler2D *> uniqueBinYBindings;
    std::unordered_map<AxisDimension2DSide *, IndicatorHandler2D *> uniqueIndicatorXBindings;
    std::unordered_map<AxisDimension2DSide *, IndicatorHandler2D *> uniqueIndicatorYBindings;

    traceBindings.clear();
    auto sortedTraces = traces->getSortedHandlers();
    std::unordered_set<TraceSymbol::Type, TraceSymbol::TypeHash> traceSymbols;
    for (auto trace : sortedTraces) {
        TraceBinding binding;
        binding.trace = trace;
        binding.x = static_cast<AxisDimension2DSide *>(
                bindAxis(xAxes.get(), trace, 0, trace->hasXBinding(), trace->getXBinding()));
        binding.y = static_cast<AxisDimension2DSide *>(
                bindAxis(yAxes.get(), trace, 1, trace->hasYBinding(), trace->getYBinding()));
        binding.z = static_cast<AxisDimension2DColor *>(
                bindAxis(zAxes.get(), trace, 2, trace->hasZBinding(), trace->getZBinding()));
        traceBindings.emplace_back(binding);

        if (binding.x) {
            auto uniqueCheck = uniqueTraceXBindings.find(binding.x);
            if (uniqueCheck == uniqueTraceXBindings.end())
                uniqueTraceXBindings.emplace(binding.x, trace);
            else
                uniqueCheck->second = nullptr;
        }
        if (binding.y) {
            auto uniqueCheck = uniqueTraceYBindings.find(binding.y);
            if (uniqueCheck == uniqueTraceYBindings.end())
                uniqueTraceYBindings.emplace(binding.y, trace);
            else
                uniqueCheck->second = nullptr;
        }

        if (trace->axesBound(binding.x, binding.y, binding.z, context.getInteractive()))
            deferredChanged = true;
        trace->insertUsedSymbols(traceSymbols);
    }
    traces->getColors(findColors, claimedColors);

    xBinBindings.clear();
    for (auto bins : xBins->getSortedHandlers()) {
        BinBinding binding;
        binding.bins = bins;
        binding.x = static_cast<AxisDimension2DSide *>(
                bindAxis(xAxes.get(), bins, 0, bins->hasIBinding(), bins->getIBinding()));
        binding.y = static_cast<AxisDimension2DSide *>(
                bindAxis(yAxes.get(), bins, 1, bins->hasDBinding(), bins->getDBinding()));
        xBinBindings.emplace_back(binding);

        if (binding.x) {
            auto uniqueCheck = uniqueBinXBindings.find(binding.x);
            if (uniqueCheck == uniqueBinXBindings.end())
                uniqueBinXBindings.emplace(binding.x, bins);
            else
                uniqueCheck->second = nullptr;
        }
        if (binding.y) {
            auto uniqueCheck = uniqueBinYBindings.find(binding.y);
            if (uniqueCheck == uniqueBinYBindings.end())
                uniqueBinYBindings.emplace(binding.y, bins);
            else
                uniqueCheck->second = nullptr;
        }

        if (bins->axesBound(binding.x, binding.y, true, context.getInteractive()))
            deferredChanged = true;
    }
    xBins->getColors(findColors, claimedColors);

    yBinBindings.clear();
    for (auto bins : yBins->getSortedHandlers()) {
        BinBinding binding;
        binding.bins = bins;
        binding.x = static_cast<AxisDimension2DSide *>(
                bindAxis(xAxes.get(), bins, 1, bins->hasDBinding(), bins->getDBinding()));
        binding.y = static_cast<AxisDimension2DSide *>(
                bindAxis(yAxes.get(), bins, 0, bins->hasIBinding(), bins->getIBinding()));
        yBinBindings.emplace_back(binding);

        if (binding.x) {
            auto uniqueCheck = uniqueBinXBindings.find(binding.x);
            if (uniqueCheck == uniqueBinXBindings.end())
                uniqueBinXBindings.emplace(binding.x, bins);
            else
                uniqueCheck->second = nullptr;
        }
        if (binding.y) {
            auto uniqueCheck = uniqueBinYBindings.find(binding.y);
            if (uniqueCheck == uniqueBinYBindings.end())
                uniqueBinYBindings.emplace(binding.y, bins);
            else
                uniqueCheck->second = nullptr;
        }

        if (bins->axesBound(binding.x, binding.y, false, context.getInteractive()))
            deferredChanged = true;
    }
    yBins->getColors(findColors, claimedColors);

    xIndicatorBindings.clear();
    auto sortedXIndicators = xIndicators->getSortedHandlers();
    std::unordered_set<AxisIndicator2D::Type, AxisIndicator2D::TypeHash> indicatorTypes;
    for (auto indicator : sortedXIndicators) {
        IndicatorBinding binding;
        binding.indicator = indicator;
        binding.axis = static_cast<AxisDimension2DSide *>(
                bindAxis(xAxes.get(), indicator, 0, indicator->hasBinding(),
                         indicator->getBinding()));
        xIndicatorBindings.emplace_back(binding);
        indicator->insertUsedTypes(indicatorTypes);

        if (binding.axis) {
            auto uniqueCheck = uniqueIndicatorXBindings.find(binding.axis);
            if (uniqueCheck == uniqueIndicatorXBindings.end())
                uniqueIndicatorXBindings.emplace(binding.axis, indicator);
            else
                uniqueCheck->second = nullptr;
        }
    }

    yIndicatorBindings.clear();
    auto sortedYIndicators = yIndicators->getSortedHandlers();
    for (auto indicator : sortedYIndicators) {
        IndicatorBinding binding;
        binding.indicator = indicator;
        binding.axis = static_cast<AxisDimension2DSide *>(
                bindAxis(yAxes.get(), indicator, 0, indicator->hasBinding(),
                         indicator->getBinding()));
        yIndicatorBindings.emplace_back(binding);
        indicator->insertUsedTypes(indicatorTypes);

        if (binding.axis) {
            auto uniqueCheck = uniqueIndicatorYBindings.find(binding.axis);
            if (uniqueCheck == uniqueIndicatorYBindings.end())
                uniqueIndicatorYBindings.emplace(binding.axis, indicator);
            else
                uniqueCheck->second = nullptr;
        }
    }

    if (auxiliaryBind(context, xAxes.get(), yAxes.get(), zAxes.get(), findColors, claimedColors,
                      traces.get(), xBins.get(), yBins.get(), xIndicators.get(), yIndicators.get()))
        deferredChanged = true;

    xAxes->finishUpdate();
    yAxes->finishUpdate();
    zAxes->finishUpdate();

    for (auto trace : sortedTraces) {
        trace->bindSymbol(traceSymbols);
    }
    for (auto indicator : sortedXIndicators) {
        indicator->bindType(indicatorTypes);
    }
    for (auto indicator : sortedYIndicators) {
        indicator->bindType(indicatorTypes);
    }


    auto dynamicColors = TraceUtilities::defaultTraceColors(findColors, claimedColors);
    traces->claimColors(dynamicColors);
    xBins->claimColors(dynamicColors);
    yBins->claimColors(dynamicColors);

    legend.clear();
    if (traces->addLegend(legend, context))
        deferredChanged = true;
    if (xBins->addLegend(legend, context))
        deferredChanged = true;
    if (yBins->addLegend(legend, context))
        deferredChanged = true;
    if (auxiliaryClaim(context, legend, dynamicColors, traces.get(), xBins.get(), yBins.get(),
                       xIndicators.get(), yIndicators.get()))
        deferredChanged = true;

    extractUniqueBindings(uniqueTraceXBindings, uniqueBinXBindings, uniqueIndicatorXBindings);
    extractUniqueBindings(uniqueTraceYBindings, uniqueBinYBindings, uniqueIndicatorYBindings);
    bindAxisColors(xAxes.get(), yAxes.get(), uniqueTraceXBindings, uniqueTraceYBindings,
                   uniqueBinXBindings, uniqueBinYBindings, uniqueIndicatorXBindings,
                   uniqueIndicatorYBindings);

    /* Push the legend into the main title, if applicable. */
    if (!parameters->hasTitleText()) {
        if (legend.size() == 1 &&
                !legend.at(0)->getText().isEmpty() &&
                (legend.at(0)->getColor() == QColor(0, 0, 0) ||
                        legend.at(0)->getColor() == QColor(255, 255, 255) ||
                        !legend.at(0)->hasAnyDrawComponents())) {
            titleText = legend.at(0)->getText();
            legend.clear();
        } else {
            titleText = QString();
        }
    } else {
        titleText = context.handleString(parameters->getTitleText(),
                                         DisplayDynamicContext::String_GraphTitle,
                                         &deferredChanged);
    }
}

template<typename HandlerType>
static void bindAxisColorFromHandlers(const std::unordered_map<AxisDimension2DSide *,
                                                               HandlerType *> &unique)
{
    for (const auto &binding : unique) {
        binding.first->bindUniqueTraceColor(binding.second->getColor());
    }
}

/**
 * Called after all bindings and color claiming so that axes can get
 * bound to unique colors for their trace.
 * 
 * @param xAxes             the X axis set
 * @param yAxes             the Y axis set
 * @param uniqueXTraces     the set of unique trace to X axis bindings
 * @param uniqueYTraces     the set of unique trace to Y axis bindings
 * @param uniqueXBins       the set of unique bins to X axis bindings
 * @param uniqueYBins       the set of unique bins to Y axis bindings
 * @param uniqueXIndicators the set of unique indicators to X axis bindings
 * @param uniqueYIndicators the set of unique indicators to Y axis bindings
 */
void Graph2D::bindAxisColors(AxisDimensionSet2DSide *xAxes,
                             AxisDimensionSet2DSide *yAxes,
                             const std::unordered_map<AxisDimension2DSide *,
                                                      TraceHandler2D *> &uniqueXTraces,
                             const std::unordered_map<AxisDimension2DSide *,
                                                      TraceHandler2D *> &uniqueYTraces,
                             const std::unordered_map<AxisDimension2DSide *,
                                                      BinHandler2D *> &uniqueXBins,
                             const std::unordered_map<AxisDimension2DSide *,
                                                      BinHandler2D *> &uniqueYBins,
                             const std::unordered_map<AxisDimension2DSide *,
                                                      IndicatorHandler2D *> &uniqueXIndicators,
                             const std::unordered_map<AxisDimension2DSide *,
                                                      IndicatorHandler2D *> &uniqueYIndicators)
{
    if (xAxes->countDisplayed() > 1) {
        bindAxisColorFromHandlers<TraceHandler2D>(uniqueXTraces);
        bindAxisColorFromHandlers<BinHandler2D>(uniqueXBins);
        bindAxisColorFromHandlers<IndicatorHandler2D>(uniqueXIndicators);
    }
    if (yAxes->countDisplayed() > 1) {
        bindAxisColorFromHandlers<TraceHandler2D>(uniqueYTraces);
        bindAxisColorFromHandlers<BinHandler2D>(uniqueYBins);
        bindAxisColorFromHandlers<IndicatorHandler2D>(uniqueYIndicators);
    }
}

/**
 * Finalize the layout.  This is called after prepare and data bindings are
 * established but before the paint, size fetches, or layout bindings.
 * 
 * @param maximumDimensions     the maximum possible dimensions the display might occupy
 * @param paintdevice           the device that drawing will be done it, if available
 * @param context               the dynamic context
 */
void Graph2D::finalizeLayout(const QRectF &maximumDimensions,
                             QPaintDevice *paintdevice,
                             const DisplayDynamicContext &context)
{
    Q_ASSERT(havePrepared);
    haveFinalized = true;
    haveEverFinalized = true;

    if (xAxes->prepareDraw(paintdevice, context))
        deferredChanged = true;
    if (yAxes->prepareDraw(paintdevice, context))
        deferredChanged = true;
    if (zAxes->prepareDraw(paintdevice, context))
        deferredChanged = true;

    bottomAxisSideDepth = xAxes->getPredictedDepth(true);
    topAxisSideDepth = xAxes->getPredictedDepth(false);
    leftAxisSideDepth = yAxes->getPredictedDepth(true);
    rightAxisSideDepth = yAxes->getPredictedDepth(false);

    qreal addTop = 0;
    qreal addBottom = 0;
    qreal addLeft = 0;
    qreal addRight = 0;
    if (getAuxiliaryInsets(context, paintdevice, addTop, addBottom, addLeft, addRight))
        deferredChanged = true;
    topAxisSideDepth += addTop;
    bottomAxisSideDepth += addBottom;
    leftAxisSideDepth += addLeft;
    rightAxisSideDepth += addRight;

    leftTotalInset = leftAxisSideDepth;
    rightTotalInset = rightAxisSideDepth;
    topTotalInset = topAxisSideDepth;
    bottomTotalInset = bottomAxisSideDepth;

    leftTotalInset += zAxes->getPredictedDepth(Axis2DLeft);
    rightTotalInset += zAxes->getPredictedDepth(Axis2DRight);
    topTotalInset += zAxes->getPredictedDepth(Axis2DTop);
    bottomTotalInset += zAxes->getPredictedDepth(Axis2DBottom);

    if (!titleText.isEmpty()) {
        QFontMetrics fmTitle(parameters->getTitleFont());
        if (paintdevice != NULL)
            fmTitle = QFontMetrics(parameters->getTitleFont(), paintdevice);
        topTotalInset += GUIStringLayout::maximumCharacterDimensions(titleText, fmTitle).height();
        topTotalInset += fmTitle.height() * topTitleBelowSpace;
    }

    topTotalInset += parameters->getInsetTop();
    bottomTotalInset += parameters->getInsetBottom();
    leftTotalInset += parameters->getInsetLeft();
    rightTotalInset += parameters->getInsetRight();

    if (parameters->getLegendEnable() && !legend.isEmpty()) {
        switch (parameters->getLegendPosition()) {
        case Graph2DParameters::Inside_TopLeft:
        case Graph2DParameters::Inside_TopRight:
        case Graph2DParameters::Inside_BottomLeft:
        case Graph2DParameters::Inside_BottomRight: {
            QFontMetrics fm(QApplication::font());
            if (paintdevice != NULL)
                fm = QFontMetrics(QApplication::font(), paintdevice);
            qreal baseSize = fm.height();
            qreal addX = parameters->getLegendBoxXInset() * baseSize;
            qreal addY = parameters->getLegendBoxYInset() * baseSize;
            if (parameters->getLegendBoxSize() >= 0.0) {
                addX += baseSize * parameters->getLegendBoxSize() * 2.0;
                addY += baseSize * parameters->getLegendBoxSize() * 2.0;
            }

            if (maximumDimensions.width() > (leftTotalInset + rightTotalInset + addX) &&
                    maximumDimensions.height() > (topTotalInset + bottomTotalInset + addY)) {
                legend.layout(maximumDimensions.width() - leftTotalInset - rightTotalInset - addX,
                              maximumDimensions.height() - topTotalInset - bottomTotalInset - addY,
                              paintdevice);
            } else {
                legend.layout(-1, -1, paintdevice);
            }
            break;
        }
        case Graph2DParameters::Outside_Left:
        case Graph2DParameters::Outside_Right: {
            if (maximumDimensions.height() > (topTotalInset + bottomTotalInset)) {
                legend.layout(-1, maximumDimensions.height() - topTotalInset - bottomTotalInset,
                              paintdevice);
            } else {
                legend.layout(-1, -1, paintdevice);
            }

            QSizeF size(legend.getSize());
            if (size.width() > 0) {
                QFontMetrics fm(QApplication::font());
                if (paintdevice != NULL)
                    fm = QFontMetrics(QApplication::font(), paintdevice);
                qreal baseSize = fm.height();
                if (parameters->getLegendPosition() == Graph2DParameters::Outside_Left) {
                    leftTotalInset += size.width() + baseSize * legendOutsideSpacing;
                } else {
                    rightTotalInset += size.width() + baseSize * legendOutsideSpacing;
                }
            }

            break;
        }
        }
    }
}

void Graph2D::predictLayout(const QRectF &maximumDimensions,
                            QPaintDevice *paintdevice,
                            const DisplayDynamicContext &context)
{
    if (!havePrepared)
        prepareLayout(maximumDimensions, paintdevice, context);
    finalizeLayout(maximumDimensions, paintdevice, context);
}

class Graph2D::InsetCoupling : public DisplayCoupling {
    QPointer<Graph2D> parent;
    Axis2DSide side;
public:
    InsetCoupling() : side(Axis2DLeft)
    { }

    InsetCoupling(Graph2D *parent, Axis2DSide side, DisplayCoupling::CoupleMode mode)
            : DisplayCoupling(mode), parent(parent), side(side)
    { }

    virtual ~InsetCoupling() = default;

    bool canCouple(const std::shared_ptr<DisplayCoupling> &other) const override
    {
        if (parent.isNull())
            return false;
        auto sa = dynamic_cast<InsetCoupling *>(other.get());
        if (!sa)
            return false;
        if (sa->parent.isNull())
            return false;
        return side == sa->side;
    }

    QVariant combine(const std::vector<std::shared_ptr<DisplayCoupling>> &components) const override
    {
        qreal maximum = 0;
        for (const auto &cmp : components) {
            auto sa = dynamic_cast<InsetCoupling *>(cmp.get());
            if (!sa)
                continue;
            if (!sa->parent)
                continue;
            qreal value = 0;
            switch (sa->side) {
            case Axis2DLeft:
                value = sa->parent->leftTotalInset;
                break;
            case Axis2DRight:
                value = sa->parent->rightTotalInset;
                break;
            case Axis2DTop:
                value = sa->parent->topTotalInset;
                break;
            case Axis2DBottom:
                value = sa->parent->bottomTotalInset;
                break;
            }
            maximum = qMax(maximum, value);
        }
        return QVariant(maximum);
    }

    void applyCoupling(const QVariant &data, CouplingPosition = Middle) override
    {
        if (parent.isNull())
            return;
        qreal size = data.toDouble();
        switch (side) {
        case Axis2DLeft:
            parent->leftTotalInset = size;
            break;
        case Axis2DRight:
            parent->rightTotalInset = size;
            break;
        case Axis2DTop:
            parent->topTotalInset = size;
            break;
        case Axis2DBottom:
            parent->bottomTotalInset = size;
            break;
        }
    }
};

std::vector<std::shared_ptr<DisplayCoupling>> Graph2D::getLayoutCoupling()
{
    Q_ASSERT(haveFinalized);

    std::vector<std::shared_ptr<DisplayCoupling>> result;
    result.emplace_back(
            std::make_shared<InsetCoupling>(this, Axis2DLeft, DisplayCoupling::SameColumn));
    result.emplace_back(
            std::make_shared<InsetCoupling>(this, Axis2DRight, DisplayCoupling::SameColumn));
    result.emplace_back(std::make_shared<InsetCoupling>(this, Axis2DTop, DisplayCoupling::SameRow));
    result.emplace_back(
            std::make_shared<InsetCoupling>(this, Axis2DBottom, DisplayCoupling::SameRow));
    return result;
}

Graph2D::PrimitiveList::Private::Private(std::vector<std::unique_ptr<Graph2DDrawPrimitive>> &&list,
                                         const QSizeF &setSize,
                                         const QPoint &setMouseover)
        : mutex(), list(std::move(list)), size(setSize), mouseover(setMouseover)
{ }

Graph2D::PrimitiveList::Private::~Private() = default;

Graph2D::PrimitiveList::PrimitiveList() : p(NULL)
{ }

Graph2D::PrimitiveList::PrimitiveList(std::vector<std::unique_ptr<Graph2DDrawPrimitive>> &&list,
                                      const QSizeF &size,
                                      const QPoint &mouseover)
{
    p = std::make_shared<Private>(std::move(list), size, mouseover);
}

Graph2D::PrimitiveList::PrimitiveList(const PrimitiveList &other) : p(other.p)
{ }

Graph2D::PrimitiveList &Graph2D::PrimitiveList::operator=(const PrimitiveList &other)
{
    if (&other == this) return *this;
    p = other.p;
    return *this;
}

void Graph2D::PrimitiveList::clear()
{ p.reset(); }

bool Graph2D::PrimitiveList::isValid() const
{ return p.get() != nullptr; }

const std::vector<std::unique_ptr<Graph2DDrawPrimitive>> &Graph2D::PrimitiveList::acquire() const
{
    p->mutex.lock();
    return p->list;
}

void Graph2D::PrimitiveList::release() const
{ p->mutex.unlock(); }

QSizeF Graph2D::PrimitiveList::getSize() const
{ return p->size; }

QPoint Graph2D::PrimitiveList::getMouseover() const
{ return p->mouseover; }


Graph2D::DeferredPainter::DeferredPainter() = default;

Graph2D::DeferredPainter::DeferredPainter(const DeferredPainter &other) : image(other.image.copy())
{ }

Graph2D::DeferredPainter &Graph2D::DeferredPainter::operator=(const DeferredPainter &other)
{
    image = other.image.copy();
    return *this;
}

Graph2D::DeferredPainter::DeferredPainter(DeferredPainter &&other) = default;

Graph2D::DeferredPainter &Graph2D::DeferredPainter::operator=(DeferredPainter &&other) = default;

Graph2D::DeferredPainter::DeferredPainter(const Graph2D::PrimitiveList &list) : image(
        (int) ceil(list.getSize().width()), (int) ceil(list.getSize().height()),
        QImage::Format_ARGB32_Premultiplied)
{
    image.fill(0);

    QPainter painter;
    painter.begin(&image);
    for (const auto &it : list.acquire()) {
        it->paint(&painter);
    }
    list.release();
    painter.end();
}

QImage Graph2D::DeferredPainter::getImage() const
{ return image; }

Graph2D::MouseoverUpdate::MouseoverUpdate() = default;

Graph2D::MouseoverUpdate::MouseoverUpdate(const MouseoverUpdate &other) = default;

Graph2D::MouseoverUpdate &Graph2D::MouseoverUpdate::operator=(const MouseoverUpdate &other) = default;

Graph2D::MouseoverUpdate::MouseoverUpdate(MouseoverUpdate &&other) = default;

Graph2D::MouseoverUpdate &Graph2D::MouseoverUpdate::operator=(MouseoverUpdate &&other) = default;

Graph2D::MouseoverUpdate::MouseoverUpdate(const PrimitiveList &list)
{
    component.reset();
    for (const auto &it : list.acquire()) {
        it->updateMouseover(list.getMouseover(), component);
    }
    list.release();
}

std::shared_ptr<Graph2DMouseoverComponent> Graph2D::MouseoverUpdate::getComponent() const
{ return component; }

void Graph2D::drawMouseoverCross(QPainter *painter,
                                 AxisDimension2DSide *x,
                                 AxisDimension2DSide *y,
                                 const QPointF &center)
{
    painter->save();
    painter->setBrush(QBrush(Qt::black));
    painter->setPen(QPen(Qt::black, 1.0));

    if (x) {
        painter->drawLine(QPointF(center.x(), xAxes->getBaseline(x)), center);
    }

    if (y) {
        painter->drawLine(QPointF(yAxes->getBaseline(y), center.y()), center);
    }

    painter->restore();
}

void Graph2D::renderMouseover(QPainter *painter,
                              const QRectF &,
                              const DisplayDynamicContext &,
                              Graph2DMouseoverComponent *component)
{
    if (!component)
        return;

    auto trace = qobject_cast<Graph2DMouseoverComponentTrace *>(component);
    if (trace) {
        auto ptr = trace->getTracePtr();
        for (const auto &binding : traceBindings) {
            if (reinterpret_cast<quintptr>(binding.trace) == ptr) {
                drawMouseoverCross(painter, binding.x, binding.y, trace->getCenter());
                return;
            }
        }

        for (const auto &binding : xBinBindings) {
            auto binPtr = reinterpret_cast<quintptr>(binding.bins);
            if (binPtr == ptr ||
                    binPtr + BinParameters2D::Fit_Lowest == ptr ||
                    binPtr + BinParameters2D::Fit_Lower == ptr ||
                    binPtr + BinParameters2D::Fit_Middle == ptr ||
                    binPtr + BinParameters2D::Fit_Upper == ptr ||
                    binPtr + BinParameters2D::Fit_Uppermost == ptr) {
                drawMouseoverCross(painter, binding.x, binding.y, trace->getCenter());
                return;
            }
        }

        for (const auto &binding : yBinBindings) {
            auto binPtr = reinterpret_cast<quintptr>(binding.bins);
            if (binPtr == ptr ||
                    binPtr + BinParameters2D::Fit_Lowest == ptr ||
                    binPtr + BinParameters2D::Fit_Lower == ptr ||
                    binPtr + BinParameters2D::Fit_Middle == ptr ||
                    binPtr + BinParameters2D::Fit_Upper == ptr ||
                    binPtr + BinParameters2D::Fit_Uppermost == ptr) {
                drawMouseoverCross(painter, binding.x, binding.y, trace->getCenter());
                return;
            }
        }
    }
}

void Graph2D::updateMouseoverTooltip(const MouseoverUpdate &mouseover,
                                     const DisplayDynamicContext &context)
{
    auto component = mouseover.getComponent();
    if (component) {
        QPoint origin(mouseoverPoint);
        if (auto trace = qobject_cast<Graph2DMouseoverComponentTrace *>(component.get())) {
            origin = trace->getCenter().toPoint();
        }

        emit setToolTip(origin, getMouseoverToolTip(component.get(), context));
    } else {
        emit setToolTip(QPoint(), QString());
    }
}

void Graph2D::renderMouseZoom(QPainter *painter, const QRectF &dimensions)
{
    updateMouseZoomMode();

    painter->save();

    switch (mouseZoomMode) {
    case Zoom_None:
        Q_ASSERT(false);
        break;
    case Zoom_AutoX:
    case Zoom_ForceX: {
        QBrush brush(Qt::black);
        painter->setBrush(brush);
        painter->setPen(QPen(brush, 1));
        painter->drawLine(QPointF(mouseZoomA.x(), traceArea.top() + dimensions.top()),
                          QPointF(mouseZoomA.x(), traceArea.bottom() + dimensions.top()));
        if (mouseZoomA.x() != mouseZoomB.x()) {
            painter->drawLine(QPointF(mouseZoomB.x(), traceArea.top() + dimensions.top()),
                              QPointF(mouseZoomB.x(), traceArea.bottom() + dimensions.top()));
        }
        break;
    }

    case Zoom_AutoY:
    case Zoom_ForceY: {
        QBrush brush(Qt::black);
        painter->setBrush(brush);
        painter->setPen(QPen(brush, 1));
        painter->drawLine(QPointF(traceArea.left() + dimensions.left(), mouseZoomA.y()),
                          QPointF(traceArea.right() + dimensions.left(), mouseZoomA.y()));
        if (mouseZoomA.y() != mouseZoomB.y()) {
            painter->drawLine(QPointF(traceArea.left() + dimensions.left(), mouseZoomB.y()),
                              QPointF(traceArea.right() + dimensions.left(), mouseZoomB.y()));
        }
        break;
    }

    case Zoom_AutoBoth:
    case Zoom_ForceBoth: {
        painter->setBrush(QBrush(QColor(0, 0, 0, 90)));
        painter->setPen(QPen(QBrush(Qt::black), 1.0));
        QRectF rect;
        if (mouseZoomA.x() < mouseZoomB.x()) {
            rect.setLeft(mouseZoomA.x() + dimensions.left());
            rect.setRight(mouseZoomB.x() + dimensions.left());
        } else {
            rect.setLeft(mouseZoomB.x() + dimensions.left());
            rect.setRight(mouseZoomA.x() + dimensions.left());
        }
        if (mouseZoomA.y() < mouseZoomB.y()) {
            rect.setTop(mouseZoomA.y() + dimensions.top());
            rect.setBottom(mouseZoomB.y() + dimensions.top());
        } else {
            rect.setTop(mouseZoomB.y() + dimensions.top());
            rect.setBottom(mouseZoomA.y() + dimensions.top());
        }
        painter->drawRect(rect);
        break;
    }
    }
    painter->restore();
}

static bool drawSortCompare(const std::unique_ptr<Graph2DDrawPrimitive> &a,
                            const std::unique_ptr<Graph2DDrawPrimitive> &b)
{ return a->drawLessThan(b.get()); }

std::vector<std::unique_ptr<Graph2DDrawPrimitive>> Graph2D::makeDrawList(const QRectF &area,
                                                                         QPaintDevice *paintdevice)
{
    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> draw;

    traceArea.setLeft(leftTotalInset);
    if (area.width() > leftTotalInset + rightTotalInset) {
        traceArea.setRight(area.width() - rightTotalInset);
    } else {
        traceArea.setRight(leftTotalInset + 1);
    }
    traceArea.setTop(topTotalInset);
    if (area.height() > topTotalInset + bottomTotalInset) {
        traceArea.setBottom(area.height() - bottomTotalInset);
    } else {
        traceArea.setBottom(topTotalInset + 1);
    }

    xAxes->setArea(traceArea, traceArea);
    yAxes->setArea(traceArea, traceArea);

    QRectF sideAxisOutline;
    sideAxisOutline.setLeft(traceArea.left() - leftAxisSideDepth);
    sideAxisOutline.setRight(traceArea.right() + rightAxisSideDepth);
    sideAxisOutline.setTop(traceArea.top() - topAxisSideDepth);
    sideAxisOutline.setBottom(traceArea.bottom() + bottomAxisSideDepth);
    zAxes->setArea(traceArea, sideAxisOutline);

    std::vector<GraphPainter2DHighlight::Region> highlightRegions;
    for (const auto &highlight : highlights) {
        std::vector<std::unordered_set<std::size_t>> dimensions;
        auto affectedTraces = traces->getAffectedHandlers(highlight.selection, &dimensions);
        for (std::size_t i = 0, max = affectedTraces.size(); i < max; i++) {
            auto handler = static_cast<TraceHandler2D *>(affectedTraces[i]);
            std::size_t bIdx;
            for (bIdx = 0; bIdx < traceBindings.size(); bIdx++) {
                if (traceBindings[bIdx].trace == handler)
                    break;
            }
            if (bIdx >= traceBindings.size())
                continue;

            Util::append(translateHighlightTrace(highlight.type, highlight.start, highlight.end,
                                                 highlight.min, highlight.max, handler,
                                                 traceBindings[bIdx].x, traceBindings[bIdx].y,
                                                 traceBindings[bIdx].z, dimensions[i]),
                         highlightRegions);
        }

        dimensions.clear();
        auto affectedBins = xBins->getAffectedHandlers(highlight.selection, &dimensions);
        for (std::size_t i = 0, max = affectedBins.size(); i < max; i++) {
            auto handler = static_cast<BinHandler2D *>(affectedBins[i]);
            std::size_t bIdx;
            for (bIdx = 0; bIdx < xBinBindings.size(); bIdx++) {
                if (xBinBindings[bIdx].bins == handler)
                    break;
            }
            if (bIdx >= xBinBindings.size())
                continue;

            Util::append(translateHighlightXBin(highlight.type, highlight.start, highlight.end,
                                                highlight.min, highlight.max, handler,
                                                xBinBindings[bIdx].x, xBinBindings[bIdx].y,
                                                dimensions[i]), highlightRegions);
        }

        dimensions.clear();
        affectedBins = yBins->getAffectedHandlers(highlight.selection, &dimensions);
        for (std::size_t i = 0, max = affectedBins.size(); i < max; i++) {
            auto handler = static_cast<BinHandler2D *>(affectedBins[i]);
            std::size_t bIdx;
            for (bIdx = 0; bIdx < yBinBindings.size(); bIdx++) {
                if (yBinBindings[bIdx].bins == handler)
                    break;
            }
            if (bIdx >= yBinBindings.size())
                continue;

            Util::append(translateHighlightXBin(highlight.type, highlight.start, highlight.end,
                                                highlight.min, highlight.max, handler,
                                                yBinBindings[bIdx].x, yBinBindings[bIdx].y,
                                                dimensions[i]), highlightRegions);
        }
    }
    if (!highlightRegions.empty()) {
        draw.emplace_back(new GraphPainter2DHighlight(highlightRegions,
                                                      parameters->getHighlightDrawPriority()));
    }

    for (const auto &binding : traceBindings) {
        if (!binding.y || !binding.x)
            continue;
        if (binding.z) {
            Util::append(binding.trace
                                ->createDraw(binding.x->getTransformer(),
                                             binding.y->getTransformer(),
                                             binding.z->getTransformer(), binding.z->getGradient()),
                         draw);
        } else {
            Util::append(binding.trace
                                ->createDraw(binding.x->getTransformer(),
                                             binding.y->getTransformer()), draw);
        }
    }

    std::vector<BinDrawPrecursor> binPrecursors;
    for (const auto &binding : xBinBindings) {
        if (!binding.x || !binding.y)
            continue;
        Util::append(binding.bins
                            ->createDraw(binding.x->getTransformer(), binding.y->getTransformer(),
                                         binPrecursors, true, xAxes->getBaseline(binding.x),
                                         yAxes->getBaseline(binding.y)), draw);
    }
    Util::append(xBins->combinePrecursors(binPrecursors, true), draw);

    binPrecursors.clear();
    for (const auto &binding : yBinBindings) {
        if (!binding.x || !binding.y)
            continue;
        Util::append(binding.bins
                            ->createDraw(binding.x->getTransformer(), binding.y->getTransformer(),
                                         binPrecursors, false, xAxes->getBaseline(binding.x),
                                         yAxes->getBaseline(binding.y)), draw);
    }
    Util::append(yBins->combinePrecursors(binPrecursors, false), draw);

    for (const auto &binding : xIndicatorBindings) {
        if (!binding.axis)
            continue;
        switch (binding.indicator->getSideMode()) {
        case IndicatorParameters2D::Side_Auto:
            Util::append(binding.indicator
                                ->createDraw(binding.axis->getTransformer(), traceArea.bottom(),
                                             traceArea.top(),
                                             binding.axis->getPrimary() ? Axis2DBottom : Axis2DTop),
                         draw);
            break;
        case IndicatorParameters2D::Side_Primary:
            Util::append(binding.indicator
                                ->createDraw(binding.axis->getTransformer(), traceArea.bottom(),
                                             traceArea.top(), Axis2DBottom), draw);
            break;
        case IndicatorParameters2D::Side_Secondary:
            Util::append(binding.indicator
                                ->createDraw(binding.axis->getTransformer(), traceArea.bottom(),
                                             traceArea.top(), Axis2DTop), draw);
            break;
        }
    }

    for (const auto &binding : yIndicatorBindings) {
        if (!binding.axis)
            continue;
        switch (binding.indicator->getSideMode()) {
        case IndicatorParameters2D::Side_Auto:
            Util::append(binding.indicator
                                ->createDraw(binding.axis->getTransformer(), traceArea.left(),
                                             traceArea.right(),
                                             binding.axis->getPrimary() ? Axis2DLeft : Axis2DRight),
                         draw);
            break;
        case IndicatorParameters2D::Side_Primary:
            Util::append(binding.indicator
                                ->createDraw(binding.axis->getTransformer(), traceArea.left(),
                                             traceArea.right(), Axis2DLeft), draw);
            break;
        case IndicatorParameters2D::Side_Secondary:
            Util::append(binding.indicator
                                ->createDraw(binding.axis->getTransformer(), traceArea.left(),
                                             traceArea.right(), Axis2DRight), draw);
            break;
        }
    }

    Util::append(xAxes->createDraw(), draw);
    Util::append(yAxes->createDraw(), draw);
    Util::append(zAxes->createDraw(), draw);

    if (parameters->getLegendEnable() && !legend.isEmpty()) {
        QFontMetrics fm(QApplication::font());
        if (paintdevice)
            fm = QFontMetrics(QApplication::font(), paintdevice);
        qreal baseSize = fm.height();
        QSizeF size(legend.getSize());

        QPointF legendOrigin;
        QRectF legendBackground;

        switch (parameters->getLegendPosition()) {
        case Graph2DParameters::Inside_TopLeft:
            if (parameters->getLegendBoxSize() >= 0.0) {
                qreal boxSpace = baseSize * parameters->getLegendBoxSize() + 1.0;
                legendBackground.setTop(
                        traceArea.top() + parameters->getLegendBoxYInset() * baseSize);
                legendBackground.setLeft(
                        traceArea.left() + parameters->getLegendBoxXInset() * baseSize);
                legendBackground.setRight(legendBackground.left() + size.width() + boxSpace * 2);
                legendBackground.setBottom(legendBackground.top() + size.height() + boxSpace * 2);
                legendOrigin.setX(legendBackground.left() + boxSpace);
                legendOrigin.setY(legendBackground.top() + boxSpace);
            } else {
                legendOrigin.setX(traceArea.left() + parameters->getLegendBoxXInset() * baseSize);
                legendOrigin.setY(traceArea.top() + parameters->getLegendBoxYInset() * baseSize);
            }
            break;

        case Graph2DParameters::Inside_TopRight:
            if (parameters->getLegendBoxSize() >= 0.0) {
                qreal boxSpace = baseSize * parameters->getLegendBoxSize() + 1.0;
                legendBackground.setTop(
                        traceArea.top() + parameters->getLegendBoxYInset() * baseSize);
                legendBackground.setRight(
                        traceArea.right() - parameters->getLegendBoxXInset() * baseSize);
                legendBackground.setLeft(legendBackground.right() - size.width() - boxSpace * 2);
                legendBackground.setBottom(legendBackground.top() + size.height() + boxSpace * 2);
                legendOrigin.setX(legendBackground.left() + boxSpace);
                legendOrigin.setY(legendBackground.top() + boxSpace);
            } else {
                legendOrigin.setX(traceArea.right() -
                                          size.width() -
                                          parameters->getLegendBoxXInset() * baseSize);
                legendOrigin.setY(traceArea.top() + parameters->getLegendBoxYInset() * baseSize);
            }
            break;

        case Graph2DParameters::Inside_BottomLeft:
            if (parameters->getLegendBoxSize() >= 0.0) {
                qreal boxSpace = baseSize * parameters->getLegendBoxSize() + 1.0;
                legendBackground.setBottom(
                        traceArea.bottom() - parameters->getLegendBoxYInset() * baseSize);
                legendBackground.setLeft(
                        traceArea.left() + parameters->getLegendBoxXInset() * baseSize);
                legendBackground.setRight(legendBackground.left() + size.width() + boxSpace * 2);
                legendBackground.setTop(legendBackground.bottom() - size.height() - boxSpace * 2);
                legendOrigin.setX(legendBackground.left() + boxSpace);
                legendOrigin.setY(legendBackground.top() + boxSpace);
            } else {
                legendOrigin.setX(traceArea.left() + parameters->getLegendBoxXInset() * baseSize);
                legendOrigin.setY(traceArea.bottom() -
                                          size.height() -
                                          parameters->getLegendBoxYInset() * baseSize);
            }
            break;

        case Graph2DParameters::Inside_BottomRight:
            if (parameters->getLegendBoxSize() >= 0.0) {
                qreal boxSpace = baseSize * parameters->getLegendBoxSize() + 1.0;
                legendBackground.setBottom(
                        traceArea.bottom() - parameters->getLegendBoxYInset() * baseSize);
                legendBackground.setRight(
                        traceArea.right() - parameters->getLegendBoxXInset() * baseSize);
                legendBackground.setLeft(legendBackground.right() - size.width() - boxSpace * 2);
                legendBackground.setTop(legendBackground.bottom() - size.height() - boxSpace * 2);
                legendOrigin.setX(legendBackground.left() + boxSpace);
                legendOrigin.setY(legendBackground.top() + boxSpace);
            } else {
                legendOrigin.setX(traceArea.right() -
                                          size.width() -
                                          parameters->getLegendBoxXInset() * baseSize);
                legendOrigin.setY(traceArea.bottom() -
                                          size.height() -
                                          parameters->getLegendBoxYInset() * baseSize);
            }
            break;

        case Graph2DParameters::Outside_Left:
            legendOrigin.setY(traceArea.top());
            legendOrigin.setX(parameters->getInsetLeft());
            break;

        case Graph2DParameters::Outside_Right:
            legendOrigin.setY(traceArea.top());
            legendOrigin.setX(area.width() - (size.width() + parameters->getInsetRight()));
            break;
        }

        draw.emplace_back(new GraphPainter2DLegend(legend, legendOrigin, legendBackground,
                                                   parameters->getLegendBoxOutlineWidth(),
                                                   parameters->getLegendBoxOutlineStyle(),
                                                   parameters->getLegendBoxFillColor(),
                                                   parameters->getLegendBoxOutlineColor(),
                                                   parameters->getLegendDrawPriority()));
    }

    if (!titleText.isEmpty()) {
        draw.emplace_back(new GraphPainter2DTitle(titleText, QPointF(area.width() * 0.5,
                                                                     parameters->getInsetTop()),
                                                  parameters->getTitleFont(),
                                                  parameters->getTitleColor()));
    }

    Util::append(getAuxiliaryDraw(traceArea, sideAxisOutline, area.size()), draw);

    std::stable_sort(draw.begin(), draw.end(), drawSortCompare);

    return std::move(draw);
}

bool Graph2D::finalRender(QPainter *painter,
                          const QRectF &dimensions,
                          const DisplayDynamicContext &context)
{
    if (context.getInteractive()) {
        if (!deferredChanged &&
                (previousLeftTotalInset != leftTotalInset ||
                        previousRightTotalInset != rightTotalInset ||
                        previousTopTotalInset != topTotalInset ||
                        previousBottomTotalInset != bottomTotalInset))
            deferredChanged = true;
        previousLeftTotalInset = leftTotalInset;
        previousRightTotalInset = rightTotalInset;
        previousTopTotalInset = topTotalInset;
        previousBottomTotalInset = bottomTotalInset;

        if (!deferredChanged) {
            auto img = deferredPaintHandler.latestResult().getImage();
            painter->drawImage(dimensions, img);
            return false;
        }
        DeferredPainter deferredPainter;
        if (!deferredPaintHandler.available(&deferredPainter) ||
                !mouseoverUpdateHandler.available()) {
            painter->drawImage(dimensions, deferredPainter.getImage());
            return false;
        }
    }

    haveEverRendered = true;

    auto draw = makeDrawList(dimensions, painter->device());

    if (context.getInteractive()) {
        deferredPaintList = PrimitiveList(std::move(draw), dimensions.size(), mouseoverPoint);
        if (mouseoverChanged)
            mouseoverUpdateList = deferredPaintList;
        DeferredPainter deferredPainter(deferredPaintHandler.startAndWait(deferredPaintList, 10));
        painter->drawImage(dimensions, deferredPainter.getImage());

        deferredChanged = false;
        return true;
    } else {
        painter->save();
        painter->translate(dimensions.topLeft());
        painter->setClipRect(QRectF(0, 0, dimensions.width(), dimensions.height()),
                             Qt::IntersectClip);
        for (const auto &d : draw) {
            d->paint(painter);
        }
        painter->restore();
        return false;
    }
}

void Graph2D::paint(QPainter *painter,
                    const QRectF &dimensions,
                    const DisplayDynamicContext &context)
{
    if (!havePrepared)
        prepareLayout(dimensions, painter->device(), context);
    if (!haveFinalized)
        finalizeLayout(dimensions, painter->device(), context);

    if (dimensions.size() != priorDimensions)
        deferredChanged = true;
    if (deferredChanged)
        mouseoverChanged = true;
    priorDimensions = dimensions.size();
    priorOrigin = dimensions.topLeft();

    if (pendingMouseZoom != Zoom_None)
        deferredChanged = true;
    bool newDraw = finalRender(painter, dimensions, context);

    if (pendingMouseZoom != Zoom_None && context.getInteractive()) {
        applyMouseZoom(dimensions.topLeft());
    }

    if (mouseZoomMode != Zoom_None && context.getInteractive()) {
        renderMouseZoom(painter, dimensions);
    } else if (mouseoverValid && context.getInteractive()) {
        MouseoverUpdate mouseover;
        if (mouseoverChanged) {
            mouseoverDone = false;
            if (mouseoverUpdateHandler.available(&mouseover)) {
                if (newDraw || deferredPaintHandler.available()) {
                    if (!mouseoverUpdateList.isValid()) {
                        mouseoverUpdateList =
                                PrimitiveList(makeDrawList(dimensions, painter->device()),
                                              dimensions.size(), mouseoverPoint);
                    }
                    mouseover = mouseoverUpdateHandler.startAndWait(mouseoverUpdateList, 5,
                                                                    &mouseoverDone);
                    mouseoverChanged = false;
                    if (mouseoverDone) {
                        updateMouseoverTooltip(mouseover, context);
                    } else {
                        emit setToolTip(QPoint(), QString());
                    }
                }
            }
        } else if (mouseoverDone) {
            mouseover = mouseoverUpdateHandler.latestResult();
        } else {
            mouseoverDone = mouseoverUpdateHandler.available(&mouseover);
            if (mouseoverDone)
                updateMouseoverTooltip(mouseover, context);
        }

        renderMouseover(painter, dimensions, context, mouseover.getComponent().get());
    }

    havePrepared = false;
    haveFinalized = false;
}

void Graph2D::clearHighlight()
{
    if (!highlights.empty())
        deferredChanged = true;
    highlights.clear();
}

void Graph2D::addHighlight(DisplayHighlightController::HighlightType type,
                           double start,
                           double end,
                           double min,
                           double max,
                           const SequenceMatch::Composite &selection)
{
    HighlightRegion add;
    add.type = type;
    add.start = start;
    add.end = end;
    add.min = min;
    add.max = max;
    add.selection = selection;
    highlights.emplace_back(std::move(add));
    deferredChanged = true;
}

bool Graph2D::acceptsMouseover() const
{ return true; }

void Graph2D::setMouseover(const QPoint &point)
{
    if (mouseZoomMode != Zoom_None)
        return;
    if (!mouseoverValid) {
        mouseoverPoint = point;
        mouseoverChanged = true;
        mouseoverValid = true;
        emit interactiveRepaint(true);
    } else if (mouseoverPoint != point) {
        mouseoverPoint = point;
        mouseoverChanged = true;
        emit interactiveRepaint(true);
    }
}

void Graph2D::clearMouseover()
{
    if (mouseoverValid) {
        mouseoverChanged = true;
        mouseoverValid = false;
        emit interactiveRepaint(true);
    }
}

void Graph2D::tracesReady()
{
    ready |= Ready_Traces;
    checkFinalReady();
}

void Graph2D::xBinsReady()
{
    ready |= Ready_XBins;
    checkFinalReady();
}

void Graph2D::yBinsReady()
{
    ready |= Ready_YBins;
    checkFinalReady();
}

void Graph2D::xIndicatorsReady()
{
    ready |= Ready_XIndicators;
    checkFinalReady();
}

void Graph2D::yIndicatorsReady()
{
    ready |= Ready_YIndicators;
    checkFinalReady();
}

void Graph2D::deferredPaintFinished()
{
    deferredPaintList.clear();
    emit interactiveRepaint();
}

void Graph2D::mouseoverUpdateFinished()
{
    mouseoverUpdateList.clear();
    emit interactiveRepaint(true);
}

void Graph2D::zoomUpdated()
{
    deferredChanged = true;
    emit interactiveRepaint(true);
}

void Graph2D::modificationChanged()
{
    deferredChanged = true;
    emit interactiveRepaint(true);
}

/**
 * Create the object used to handle traces.
 * @return a trace set object
 */
std::unique_ptr<TraceSet2D> Graph2D::createTraces()
{ return std::unique_ptr<TraceSet2D>(new TraceSet2D); }

/**
 * Create the object used to handle X bins.
 * @return a vertical binning object
 */
std::unique_ptr<BinSet2D> Graph2D::createXBins()
{ return std::unique_ptr<BinSet2D>(new BinSet2D); }

/**
 * Create the object used to handle Y bins.
 * @return a horizontal binning object
 */
std::unique_ptr<BinSet2D> Graph2D::createYBins()
{ return std::unique_ptr<BinSet2D>(new BinSet2D); }

/**
 * Create the object used to handle X indicators.
 * @return an indicator set object
 */
std::unique_ptr<IndicatorSet2D> Graph2D::createXIndicators()
{ return std::unique_ptr<IndicatorSet2D>(new IndicatorSet2D); }

/**
 * Create the object used to handle Y indicators.
 * @return an indicator set object
 */
std::unique_ptr<IndicatorSet2D> Graph2D::createYIndicators()
{ return std::unique_ptr<IndicatorSet2D>(new IndicatorSet2D); }

/**
 * Create the X axis allocation engine.
 * @return an axis handler
 */
std::unique_ptr<AxisDimensionSet2DSide> Graph2D::createXAxes()
{ return std::unique_ptr<AxisDimensionSet2DSide>(new AxisDimensionSet2DSide(false)); }

/**
 * Create the Y axis allocation engine.
 * @return an axis handler
 */
std::unique_ptr<AxisDimensionSet2DSide> Graph2D::createYAxes()
{ return std::unique_ptr<AxisDimensionSet2DSide>(new AxisDimensionSet2DSide(true)); }

/**
 * Create the Z (color) axis allocation engine.
 * @return an axis handler
 */
std::unique_ptr<AxisDimensionSet2DColor> Graph2D::createZAxes()
{ return std::unique_ptr<AxisDimensionSet2DColor>(new AxisDimensionSet2DColor); }

/**
 * Parse and create parameters from a given value.
 * @param config    the configuration
 * @param defaults  the display defaults
 */
std::unique_ptr<Graph2DParameters> Graph2D::parseParameters(const Variant::Read &config,
                                                            const DisplayDefaults &defaults)
{ return std::unique_ptr<Graph2DParameters>(new Graph2DParameters(config, defaults)); }

/**
 * Check if all components are ready for the final paint and emit 
 * readyForFinalPaint() if they are.
 */
void Graph2D::checkFinalReady()
{
    if ((ready & Ready_ALL) != Ready_ALL)
        return;
    emit readyForFinalPaint();
}

/**
 * Translate a trace highlight binding into a drawable items.
 * 
 * @param type      the type of highlight
 * @param start     the start time of the highlight
 * @param end       the end time of the highlight
 * @param min       the minimum highlight value
 * @param max       the maximum highlight value
 * @param x         the X axis
 * @param y         the Y axis
 * @param z         the Z axis (or NULL)
 * @param affectedDimensions the set of affected dimension numbers
 * @return          a list of draw regions
 */
std::vector<GraphPainter2DHighlight::Region> Graph2D::translateHighlightTrace(
        DisplayHighlightController::HighlightType type,
        double start,
        double end,
        double min,
        double max,
        TraceHandler2D *trace,
        AxisDimension2DSide *x,
        AxisDimension2DSide *y,
        AxisDimension2DColor *z,
        const std::unordered_set<size_t> &affectedDimensions)
{
    Q_UNUSED(z);

    if (!x || !y)
        return {};
    if (!Range::intersects(start, end, trace->getStart(), trace->getEnd()))
        return {};

    std::vector<GraphPainter2DHighlight::Region> result;
    if (affectedDimensions.count(0) && affectedDimensions.count(1)) {
        result.emplace_back(x->getTransformer(), y->getTransformer(), min, max, min, max,
                            parameters->getHighlightColor(type));
    } else if (affectedDimensions.count(0)) {
        result.emplace_back(x->getTransformer(), y->getTransformer(), min, max, FP::undefined(),
                            FP::undefined(), parameters->getHighlightColor(type));
    } else if (affectedDimensions.count(1)) {
        result.emplace_back(x->getTransformer(), y->getTransformer(), FP::undefined(),
                            FP::undefined(), min, max, parameters->getHighlightColor(type));
    }
    return result;
}

/**
 * Translate a X bin highlight binding into a drawable items.
 * 
 * @param type      the type of highlight
 * @param start     the start time of the highlight
 * @param end       the end time of the highlight
 * @param min       the minimum highlight value
 * @param max       the maximum highlight value
 * @param x         the X axis
 * @param y         the Y axis
 * @param affectedDimensions the set of affected dimension numbers
 * @return          a list of draw regions
 */
std::vector<GraphPainter2DHighlight::Region> Graph2D::translateHighlightXBin(
        DisplayHighlightController::HighlightType type,
        double start,
        double end,
        double min,
        double max,
        BinHandler2D *bins,
        AxisDimension2DSide *x,
        AxisDimension2DSide *y,
        const std::unordered_set<size_t> &affectedDimensions)
{
    if (!x || !y)
        return {};
    if (!Range::intersects(start, end, bins->getStart(), bins->getEnd()))
        return {};

    std::vector<GraphPainter2DHighlight::Region> result;
    if (affectedDimensions.count(0) && affectedDimensions.count(1)) {
        result.emplace_back(x->getTransformer(), y->getTransformer(), min, max, min, max,
                            parameters->getHighlightColor(type));
    } else if (affectedDimensions.count(0)) {
        result.emplace_back(x->getTransformer(), y->getTransformer(), min, max, FP::undefined(),
                            FP::undefined(), parameters->getHighlightColor(type));
    } else if (affectedDimensions.count(1)) {
        result.emplace_back(x->getTransformer(), y->getTransformer(), FP::undefined(),
                            FP::undefined(), min, max, parameters->getHighlightColor(type));
    }
    return result;
}

/**
 * Translate a Y bin highlight binding into a drawable items.
 * 
 * @param type      the type of highlight
 * @param start     the start time of the highlight
 * @param end       the end time of the highlight
 * @param min       the minimum highlight value
 * @param max       the maximum highlight value
 * @param x         the X axis
 * @param y         the Y axis
 * @param affectedDimensions the set of affected dimension numbers
 * @return          a list of draw regions
 */
std::vector<GraphPainter2DHighlight::Region> Graph2D::translateHighlightYBin(
        DisplayHighlightController::HighlightType type,
        double start,
        double end,
        double min,
        double max,
        BinHandler2D *bins,
        AxisDimension2DSide *x,
        AxisDimension2DSide *y,
        const std::unordered_set<size_t> &affectedDimensions)
{
    if (!x || !y)
        return {};
    if (!Range::intersects(start, end, bins->getStart(), bins->getEnd()))
        return {};

    std::vector<GraphPainter2DHighlight::Region> result;
    if (affectedDimensions.count(0) && affectedDimensions.count(1)) {
        result.emplace_back(x->getTransformer(), y->getTransformer(), min, max, min, max,
                            parameters->getHighlightColor(type));
    } else if (affectedDimensions.count(1)) {
        result.emplace_back(x->getTransformer(), y->getTransformer(), min, max, FP::undefined(),
                            FP::undefined(), parameters->getHighlightColor(type));
    } else if (affectedDimensions.count(0)) {
        result.emplace_back(x->getTransformer(), y->getTransformer(), FP::undefined(),
                            FP::undefined(), min, max, parameters->getHighlightColor(type));
    }
    return result;
}

/**
 * Get any additional draw items needed in the render.
 * <br>
 * The default implementation returns nothing.
 * 
 * @param traceArea     the trace drawing area
 * @param sideAxisOutline the outline of the outermost part of the side axes
 * @param totalSize     the total size of the drawing area
 * @return              a list of draw items
 */
std::vector<
        std::unique_ptr<Graph2DDrawPrimitive>> Graph2D::getAuxiliaryDraw(const QRectF &traceArea,
                                                                         const QRectF &sideAxisOutline,
                                                                         const QSizeF &totalSize)
{
    Q_UNUSED(traceArea);
    Q_UNUSED(sideAxisOutline);
    Q_UNUSED(totalSize);
    return {};
}

/**
 * Do any additional processing (before axis bindings are dealt with).
 * <br>
 * The default implementation does nothing and returns false.
 * 
 * @param context   the drawing context
 * @return          true if the deferred cache may be out of date
 */
bool Graph2D::auxiliaryProcess(const DisplayDynamicContext &context)
{
    Q_UNUSED(context);
    return false;
}

/**
 * Establish any additional bindings on the graph axes.
 * <br>
 * The default implementation does nothing and returns false
 * 
 * @param context       the drawing context
 * @param xAxes         the X axis set
 * @param yAxes         the Y axis set
 * @param zAxes         the Z (color) axis set
 * @param findColors    the units to find colors for
 * @param claimedColors the colors that are already claimed
 * @param traces        the traces set
 * @param xBins         the X bin set
 * @param yBins         the Y bin set
 * @param xIndicators   the X indicator set
 * @param yIndicators   the Y indicator set
 * @return              true if the deferred cache may be out of date
 */
bool Graph2D::auxiliaryBind(const DisplayDynamicContext &context,
                            AxisDimensionSet2DSide *xAxes,
                            AxisDimensionSet2DSide *yAxes,
                            AxisDimensionSet2DColor *zAxes,
                            std::vector<TraceUtilities::ColorRequest> &findColors,
                            std::deque<QColor> &claimedColors,
                            TraceSet2D *traces,
                            BinSet2D *xBins,
                            BinSet2D *yBins,
                            IndicatorSet2D *xIndicators,
                            IndicatorSet2D *yIndicators)
{
    Q_UNUSED(xAxes);
    Q_UNUSED(yAxes);
    Q_UNUSED(zAxes);
    Q_UNUSED(findColors);
    Q_UNUSED(claimedColors);
    Q_UNUSED(context);
    Q_UNUSED(traces);
    Q_UNUSED(xBins);
    Q_UNUSED(yBins);
    Q_UNUSED(xIndicators);
    Q_UNUSED(yIndicators);
    return false;
}

/**
 * Preform any additional claim on the outputs of the bindings and set the 
 * legend for them.  The colors are in the order they where requested with
 * auxiliaryBind( const DisplayDynamicContext &,
 * AxisDimensionSet2DSide *, AxisDimensionSet2DSide *, 
 * AxisDimensionSet2DColor *, QList<SequenceName> &, QList<QColor> &,
 * TraceSet2D *, BinSet2D *, BinSet2D *, IndicatorSet2D *, IndicatorSet2D * )
 * <br>
 * The default implementation does nothing and returns false.
 * 
 * @param context       the drawing context
 * @param legend        the legend the output to
 * @param dynamicColors the output of dynamic color assignment
 * @param traces        the traces set
 * @param xBins         the X bin set
 * @param yBins         the Y bin set
 * @param xIndicators   the X indicator set
 * @param yIndicators   the Y indicator set
 * @return              true if the deferred cache might be out of date
 */
bool Graph2D::auxiliaryClaim(const DisplayDynamicContext &context,
                             Legend &legend,
                             std::deque<QColor> &dynamicColors,
                             TraceSet2D *traces,
                             BinSet2D *xBins,
                             BinSet2D *yBins,
                             IndicatorSet2D *xIndicators,
                             IndicatorSet2D *yIndicators)
{
    Q_UNUSED(context);
    Q_UNUSED(legend);
    Q_UNUSED(dynamicColors);
    Q_UNUSED(traces);
    Q_UNUSED(xBins);
    Q_UNUSED(yBins);
    Q_UNUSED(xIndicators);
    Q_UNUSED(yIndicators);
    return false;
}

/**
 * Get any additional insets in the drawing area.  This is called during
 * the layout finalization (after data bindings, but before layout bindings).
 * <br>
 * The default implementation does nothing and returns false.
 * 
 * @param context       the drawing context
 * @param paintdevice   the paint device in use or NULL
 * @param top           the space on the top to request
 * @param bottom        the space on the bottom to request
 * @param left          the space on the left to request
 * @param right         the space on the right to request
 * @return              true if the deferred cache might be out of date
 */
bool Graph2D::getAuxiliaryInsets(const DisplayDynamicContext &context,
                                 QPaintDevice *paintdevice,
                                 qreal &top,
                                 qreal &bottom,
                                 qreal &left,
                                 qreal &right)
{
    Q_UNUSED(context);
    Q_UNUSED(paintdevice);
    Q_UNUSED(top);
    Q_UNUSED(bottom);
    Q_UNUSED(left);
    Q_UNUSED(right);
    return false;
}

/**
 * Convert the given trace into a mouseover tooltip.
 * 
 * @param trace     the trace
 * @param x         the X axis binding
 * @param y         the Y axis binding
 * @param z         the Z axis binding or NULL
 * @param point     the point hit
 * @param context   the display context
 * @return          the tool tip string or empty to hide
 */
QString Graph2D::convertMouseoverToolTipTrace(TraceHandler2D *trace,
                                              TraceSet2D *traces,
                                              AxisDimension2DSide *x,
                                              AxisDimension2DSide *y,
                                              AxisDimension2DColor *z,
                                              const TracePoint<3> &point,
                                              const DisplayDynamicContext &context)
{

    QString origin(traces->handlerOrigin(trace, context));
    if (!origin.isEmpty()) {
        origin = tr("%1<br>", "origin insert format").arg(origin);
    }

    QString text;
    if (FP::defined(point.start) || FP::defined(point.end)) {
        text = tr("<font color=\"%2\">%1</font><br>%3At %4 to %5<br><br>", "trace name format").arg(
                traces->handlerTitle(trace, context), trace->getColor().name(), origin,
                GUITime::formatTime(point.start, settings, false),
                GUITime::formatTime(point.end, settings, false));
    } else {
        text = tr("<font color=\"%2\">%1</font><br>%3", "trace name format no time").arg(
                traces->handlerTitle(trace, context), trace->getColor().name(), origin);
    }

    for (std::size_t i = 0; i < 3; i++) {
        double value = point.d[i];
        if (!FP::defined(value))
            break;
        NumberFormat format(trace->getFormat(i));
        QString unitSuffix;
        switch (i) {
        case 0:
            if (x)
                unitSuffix = x->getToolTipUnitOverride();
            break;
        case 1:
            if (y)
                unitSuffix = y->getToolTipUnitOverride();
            break;
        case 2:
            if (z)
                unitSuffix = z->getToolTipUnitOverride();
            break;
        default:
            break;
        }
        if (unitSuffix.isEmpty()) {
            QStringList sortedUnits;
            Util::append(trace->getUnits(i), sortedUnits);
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (i != 0) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(value, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(value, QChar()),
                                                                   unitSuffix));
        }
    }
    return text;
}

/**
 * Convert the given bin point into a mouseover tooltip.
 * 
 * @param bins      the bin handler
 * @param binSet    the bin set
 * @param x         the X axis binding
 * @param y         the Y axis binding
 * @param point     the point hit
 * @param context   the display context
 * @return          the tool tip string or empty to hide
 */
QString Graph2D::convertMouseoverToolTipBins(BinHandler2D *bins,
                                             BinSet2D *binSet,
                                             bool vertical,
                                             AxisDimension2DSide *x,
                                             AxisDimension2DSide *y,
                                             const BinPoint<1, 1> &point,
                                             Graph2DMouseoverComponentTraceBox::Location location,
                                             const DisplayDynamicContext &context)
{
    QString origin(binSet->handlerOrigin(bins, context));
    if (!origin.isEmpty()) {
        origin = tr("%1<br>", "origin insert format").arg(origin);
    }

    QString text(tr("<font color=\"%2\">%1</font><br>%3", "bin name format").arg(
            binSet->handlerTitle(bins, context), bins->getColor().name(), origin));

    if (FP::defined(point.center[0]) && FP::defined(point.width[0]) && point.width[0] > 0.0) {
        NumberFormat format(bins->getFormat(0));

        QString unitSuffix;
        if (vertical) {
            if (x)
                unitSuffix = x->getToolTipUnitOverride();
        } else {
            if (y)
                unitSuffix = y->getToolTipUnitOverride();
        }
        if (unitSuffix.isEmpty()) {
            QStringList sortedUnits;
            Util::append(bins->getUnits(0), sortedUnits);
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }

        text.append(tr("<br>%1%3 (%2%3 wide)", "center with width").arg(
                format.apply(point.center[0], QChar()), format.apply(point.width[0], QChar()),
                unitSuffix));
    } else if (FP::defined(point.center[0])) {
        NumberFormat format(bins->getFormat(0));

        QString unitSuffix;
        if (vertical) {
            if (x)
                unitSuffix = x->getToolTipUnitOverride();
        } else {
            if (y)
                unitSuffix = y->getToolTipUnitOverride();
        }
        if (unitSuffix.isEmpty()) {
            QStringList sortedUnits;
            Util::append(bins->getUnits(0), sortedUnits);
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }

        text.append(
                tr("<br>%1%2", "center no width width").arg(format.apply(point.center[0], QChar()),
                                                            unitSuffix));
    }

    if (bins->isHistogram()) {
        text.append(tr("<br>%1", "histogram total").arg(Time::describeDuration(point.total[0])));
        if (FP::defined(point.upper[0])) {
            text.append(
                    tr(", %1%", "histogram percentage").arg(FP::decimalFormat(point.upper[0], 1)));
        }
    } else {
        double value = FP::undefined();
        switch (location) {
        case Graph2DMouseoverComponentTraceBox::Lowest:
            value = point.lowest[0];
            break;
        case Graph2DMouseoverComponentTraceBox::Lower:
            value = point.lower[0];
            break;
        case Graph2DMouseoverComponentTraceBox::Middle:
            value = point.middle[0];
            break;
        case Graph2DMouseoverComponentTraceBox::Upper:
            value = point.upper[0];
            break;
        case Graph2DMouseoverComponentTraceBox::Uppermost:
            value = point.uppermost[0];
            break;
        }
        if (FP::defined(value)) {
            QString unitSuffix;
            NumberFormat format(bins->getFormat(1));
            if (vertical) {
                if (y)
                    unitSuffix = y->getToolTipUnitOverride();
            } else {
                if (x)
                    unitSuffix = x->getToolTipUnitOverride();
            }
            if (unitSuffix.isEmpty()) {
                QStringList sortedUnits;
                Util::append(bins->getUnits(1), sortedUnits);
                std::sort(sortedUnits.begin(), sortedUnits.end());
                unitSuffix = sortedUnits.join(tr(" ", "unit join"));
            }
            text.append(
                    tr("<br>%1%2", "bins combine").arg(format.apply(value, QChar()), unitSuffix));
        }
        if (FP::defined(point.total[0])) {
            text.append(tr("<br>%1 points", "bins combine").arg(
                    QString::number((int) qRound(point.total[0]))));
        }
    }

    return text;
}

/**
 * Convert the given indicator point into a mouseover tooltip.
 * 
 * @param indicator     the indicator handler
 * @param indicatorSet  the indicator set
 * @param axis          the axis binding
 * @param point     the point hit
 * @param context   the display context
 * @return          the tool tip string or empty to hide
 */
QString Graph2D::convertMouseoverToolTipIndicator(IndicatorHandler2D *indicator,
                                                  IndicatorSet2D *indicatorSet,
                                                  AxisDimension2DSide *axis,
                                                  const IndicatorPoint<1> &point,
                                                  Axis2DSide side,
                                                  const DisplayDynamicContext &context)
{
    Q_UNUSED(side);

    QString origin(indicatorSet->handlerOrigin(indicator, context));
    if (!origin.isEmpty()) {
        origin = tr("%1<br>", "origin insert format").arg(origin);
    }

    QString text;
    if (FP::defined(point.start) || FP::defined(point.end)) {
        text =
                tr("<font color=\"%2\">%1</font><br>%3At %4 to %5<br><br>", "indicator name format")
                        .arg(indicatorSet->handlerTitle(indicator, context),
                             indicator->getColor().name(), origin,
                             GUITime::formatTime(point.start, settings, false),
                             GUITime::formatTime(point.end, settings, false));
    } else {
        text = tr("<font color=\"%2\">%1</font><br>%3<br>", "indicator name format no time").arg(
                indicatorSet->handlerTitle(indicator, context), indicator->getColor().name(),
                origin);
    }

    double value = point.d[0];
    if (FP::defined(value)) {
        NumberFormat format(indicator->getFormat(0));
        QString unitSuffix;
        if (axis)
            unitSuffix = axis->getToolTipUnitOverride();
        if (unitSuffix.isEmpty()) {
            QStringList sortedUnits;
            Util::append(indicator->getUnits(0), sortedUnits);
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        text.append(tr("%1%2<br>", "coordinate combine first").arg(format.apply(value, QChar()),
                                                                   unitSuffix));
    }

    QString add(indicator->triggerToolip(point.trigger));
    if (!add.isEmpty()) {
        text.append(tr("%1", "indicator trigger format").arg(add));
    }

    return text;
}

/**
 * Convert the given fit into a mouseover tooltip.
 * 
 * @param trace     the trace
 * @param x         the X axis binding
 * @param y         the Y axis binding
 * @param z         the Z axis binding or NULL
 * @param fx        the fit X coordinate
 * @param fy        the fit Y coordinate
 * @param fz        the fit Z coordinate
 * @param fi        the fit I coordinate
 * @param context   the display context
 * @return          the tool tip string or empty to hide
 */
QString Graph2D::convertMouseoverToolTipFit(TraceHandler2D *trace,
                                            TraceSet2D *traces,
                                            AxisDimension2DSide *x,
                                            AxisDimension2DSide *y,
                                            AxisDimension2DColor *z,
                                            double fx,
                                            double fy,
                                            double fz,
                                            double fi,
                                            const DisplayDynamicContext &context)
{
    QString text;

    if (FP::defined(fx)) {
        NumberFormat format(trace->getFormat(0));
        QString unitSuffix;
        if (x)
            unitSuffix = x->getToolTipUnitOverride();
        if (unitSuffix.isEmpty()) {
            QStringList sortedUnits;
            Util::append(trace->getUnits(0), sortedUnits);
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(fx, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(fx, QChar()),
                                                                   unitSuffix));
        }
    }
    if (FP::defined(fy)) {
        NumberFormat format(trace->getFormat(1));
        QString unitSuffix;
        if (y)
            unitSuffix = y->getToolTipUnitOverride();
        if (unitSuffix.isEmpty()) {
            QStringList sortedUnits;
            Util::append(trace->getUnits(1), sortedUnits);
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(fy, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(fy, QChar()),
                                                                   unitSuffix));
        }
    }
    if (FP::defined(fz)) {
        NumberFormat format(trace->getFormat(2));
        QString unitSuffix;
        if (z)
            unitSuffix = z->getToolTipUnitOverride();
        if (unitSuffix.isEmpty()) {
            QStringList sortedUnits;
            Util::append(trace->getUnits(2), sortedUnits);
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(fz, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(fz, QChar()),
                                                                   unitSuffix));
        }
    }
    if (FP::defined(fi)) {
        if (!text.isEmpty()) {
            text.append(tr(", %1", "fit I combine any").arg(fi));
        } else {
            text.append(tr("%1", "fit I combine first").arg(fi));
        }
    }

    QString title(static_cast<TraceParameters2D *>(
                          trace->getParameters())->getFitLegendText());
    if (title.isEmpty())
        title = traces->handlerTitle(trace, context);

    QString origin(traces->handlerOrigin(trace, context));
    if (!origin.isEmpty()) {
        origin = tr("%1<br>", "origin insert format").arg(origin);
    }

    text.prepend(tr("<font color=\"%2\">%1</font><br>%3<br>", "trace name format").arg(title, trace
            ->getFitColor()
            .name(), origin));

    return text;
}

/**
 * Convert the given model scan value into a mouseover tooltip.
 * 
 * @param trace     the trace
 * @param x         the X axis binding
 * @param y         the Y axis binding
 * @param z         the Z axis binding or NULL
 * @param mx        the model X coordinate
 * @param my        the model Y coordinate
 * @param mz        the model Z coordinate
 * @param context   the display context
 * @return          the tool tip string or empty to hide
 */
QString Graph2D::convertMouseoverToolTipModelScan(TraceHandler2D *trace,
                                                  TraceSet2D *traces,
                                                  AxisDimension2DSide *x,
                                                  AxisDimension2DSide *y,
                                                  AxisDimension2DColor *z,
                                                  double mx,
                                                  double my,
                                                  double mz,
                                                  const DisplayDynamicContext &context)
{
    QString text;

    if (FP::defined(mx)) {
        NumberFormat format(trace->getFormat(0));
        QString unitSuffix;
        if (x)
            unitSuffix = x->getToolTipUnitOverride();
        if (unitSuffix.isEmpty()) {
            QStringList sortedUnits;
            Util::append(trace->getUnits(0), sortedUnits);
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(mx, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(mx, QChar()),
                                                                   unitSuffix));
        }
    }
    if (FP::defined(my)) {
        NumberFormat format(trace->getFormat(1));
        QString unitSuffix;
        if (y)
            unitSuffix = y->getToolTipUnitOverride();
        if (unitSuffix.isEmpty()) {
            QStringList sortedUnits;
            Util::append(trace->getUnits(1), sortedUnits);
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(my, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(my, QChar()),
                                                                   unitSuffix));
        }
    }
    if (FP::defined(mz)) {
        NumberFormat format(trace->getFormat(2));
        QString unitSuffix;
        if (z)
            unitSuffix = z->getToolTipUnitOverride();
        if (unitSuffix.isEmpty()) {
            QStringList sortedUnits;
            Util::append(trace->getUnits(2), sortedUnits);
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(mz, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(mz, QChar()),
                                                                   unitSuffix));
        }
    }

    QString origin(traces->handlerOrigin(trace, context));
    if (!origin.isEmpty()) {
        origin = tr("%1<br>", "origin insert format").arg(origin);
    }

    text.prepend(tr("%1<br>%2<br>", "fill name format").arg(traces->handlerTitle(trace, context),
                                                            origin));

    return text;
}

/**
 * Convert the given fit for bins into a mouseover tooltip.
 * 
 * @param bins      the bin handler
 * @param binSet    the originating bin set
 * @param x         the X axis binding
 * @param y         the Y axis binding
 * @param fx        the fit X coordinate
 * @param fy        the fit Y coordinate
 * @param context   the display context
 * @return          the tool tip string or empty to hide
 */
QString Graph2D::convertMouseoverToolTipFit(BinHandler2D *bins,
                                            BinSet2D *binSet,
                                            bool vertical,
                                            AxisDimension2DSide *x,
                                            AxisDimension2DSide *y,
                                            BinParameters2D::FitOrigin origin,
                                            double fx,
                                            double fy,
                                            const DisplayDynamicContext &context)
{
    Q_UNUSED(vertical);
    QString text;

    if (FP::defined(fx)) {
        NumberFormat format(bins->getFormat(0));
        QString unitSuffix;
        if (x)
            unitSuffix = x->getToolTipUnitOverride();
        if (unitSuffix.isEmpty()) {
            QStringList sortedUnits;
            Util::append(bins->getUnits(0), sortedUnits);
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(fx, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(fx, QChar()),
                                                                   unitSuffix));
        }
    }
    if (FP::defined(fy)) {
        NumberFormat format(bins->getFormat(1));
        QString unitSuffix;
        if (y)
            unitSuffix = y->getToolTipUnitOverride();
        if (unitSuffix.isEmpty()) {
            QStringList sortedUnits;
            Util::append(bins->getUnits(1), sortedUnits);
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(fy, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(fy, QChar()),
                                                                   unitSuffix));
        }
    }

    QString title(static_cast<BinParameters2D *>(
                          bins->getParameters())->getFitLegendText(origin));
    if (title.isEmpty())
        title = binSet->handlerTitle(bins, context);

    QString originText(binSet->handlerOrigin(bins, context));
    if (!originText.isEmpty()) {
        originText = tr("%1<br>", "origin insert format").arg(origin);
    }

    text.prepend(tr("<font color=\"%2\">%1</font><br>%3<br>", "trace name format").arg(title,
                                                                                       bins->getFitColor(
                                                                                                   origin)
                                                                                           .name(),
                                                                                       originText));

    return text;
}

/**
 * Convert the given mouseover component into a string for tooltip display.
 * This string is passed to the emit of setToolTip( const QPoint &, 
 * const QString & ).  This uses convertMouseoverToolTip* to do the actual
 * string conversion.
 * 
 * @param component the component that the mouse is over
 * @param context   the display context
 * @return          the tool tip string or empty to hide
 */
QString Graph2D::getMouseoverToolTip(Graph2DMouseoverComponent *component,
                                     const DisplayDynamicContext &context)
{
    if (auto standard = qobject_cast<Graph2DMouseoverComponentTraceStandard *>(component)) {
        for (const auto &binding : traceBindings) {
            if (reinterpret_cast<quintptr>(binding.trace) != standard->getTracePtr())
                continue;

            return convertMouseoverToolTipTrace(binding.trace, traces.get(), binding.x, binding.y,
                                                binding.z, standard->getPoint(), context);
        }
    }

    if (auto box = qobject_cast<Graph2DMouseoverComponentTraceBox *>(component)) {
        for (const auto &binding : xBinBindings) {
            if (reinterpret_cast<quintptr>(binding.bins) != box->getTracePtr())
                continue;
            return convertMouseoverToolTipBins(binding.bins, xBins.get(), true, binding.x,
                                               binding.y, box->getPoint(), box->getLocation(),
                                               context);
        }
        for (const auto &binding : yBinBindings) {
            if (reinterpret_cast<quintptr>(binding.bins) != box->getTracePtr())
                continue;
            return convertMouseoverToolTipBins(binding.bins, yBins.get(), false, binding.x,
                                               binding.y, box->getPoint(), box->getLocation(),
                                               context);
        }
    }

    if (auto indicator = qobject_cast<Graph2DMouseoverComponentIndicator *>(component)) {
        for (const auto &binding : xIndicatorBindings) {
            if (reinterpret_cast<quintptr>(binding.indicator) != indicator->getTracePtr())
                continue;
            return convertMouseoverToolTipIndicator(binding.indicator, xIndicators.get(),
                                                    binding.axis, indicator->getPoint(),
                                                    indicator->getSide(), context);
        }
        for (const auto &binding : yIndicatorBindings) {
            if (reinterpret_cast<quintptr>(binding.indicator) != indicator->getTracePtr())
                continue;
            return convertMouseoverToolTipIndicator(binding.indicator, yIndicators.get(),
                                                    binding.axis, indicator->getPoint(),
                                                    indicator->getSide(), context);
        }
    }

    if (auto fit = qobject_cast<Graph2DMouseoverComponentTraceFit *>(component)) {
        for (const auto &binding : traceBindings) {
            if (reinterpret_cast<quintptr>(binding.trace) != fit->getTracePtr())
                continue;

            return convertMouseoverToolTipFit(binding.trace, traces.get(), binding.x, binding.y,
                                              binding.z, fit->getX(), fit->getY(), fit->getZ(),
                                              fit->getI(), context);
        }
        for (const auto &binding : xBinBindings) {
            if (reinterpret_cast<quintptr>(binding.bins) + BinParameters2D::Fit_Lowest ==
                    fit->getTracePtr()) {
                return convertMouseoverToolTipFit(binding.bins, xBins.get(), true, binding.x,
                                                  binding.y, BinParameters2D::Fit_Lowest,
                                                  fit->getX(), fit->getY(), context);
            }
            if (reinterpret_cast<quintptr>(binding.bins) + BinParameters2D::Fit_Lower ==
                    fit->getTracePtr()) {
                return convertMouseoverToolTipFit(binding.bins, xBins.get(), true, binding.x,
                                                  binding.y, BinParameters2D::Fit_Lower,
                                                  fit->getX(), fit->getY(), context);
            }
            if (reinterpret_cast<quintptr>(binding.bins) + BinParameters2D::Fit_Middle ==
                    fit->getTracePtr()) {
                return convertMouseoverToolTipFit(binding.bins, xBins.get(), true, binding.x,
                                                  binding.y, BinParameters2D::Fit_Middle,
                                                  fit->getX(), fit->getY(), context);
            }
            if (reinterpret_cast<quintptr>(binding.bins) + BinParameters2D::Fit_Upper ==
                    fit->getTracePtr()) {
                return convertMouseoverToolTipFit(binding.bins, xBins.get(), true, binding.x,
                                                  binding.y, BinParameters2D::Fit_Upper,
                                                  fit->getX(), fit->getY(), context);
            }
            if (reinterpret_cast<quintptr>(binding.bins) + BinParameters2D::Fit_Uppermost ==
                    fit->getTracePtr()) {
                return convertMouseoverToolTipFit(binding.bins, xBins.get(), true, binding.x,
                                                  binding.y, BinParameters2D::Fit_Uppermost,
                                                  fit->getX(), fit->getY(), context);
            }
        }
        for (const auto &binding : yBinBindings) {
            if (reinterpret_cast<quintptr>(binding.bins) + BinParameters2D::Fit_Lowest ==
                    fit->getTracePtr()) {
                return convertMouseoverToolTipFit(binding.bins, yBins.get(), false, binding.x,
                                                  binding.y, BinParameters2D::Fit_Lowest,
                                                  fit->getX(), fit->getY(), context);
            }
            if (reinterpret_cast<quintptr>(binding.bins) + BinParameters2D::Fit_Lower ==
                    fit->getTracePtr()) {
                return convertMouseoverToolTipFit(binding.bins, yBins.get(), false, binding.x,
                                                  binding.y, BinParameters2D::Fit_Lower,
                                                  fit->getX(), fit->getY(), context);
            }
            if (reinterpret_cast<quintptr>(binding.bins) + BinParameters2D::Fit_Middle ==
                    fit->getTracePtr()) {
                return convertMouseoverToolTipFit(binding.bins, yBins.get(), false, binding.x,
                                                  binding.y, BinParameters2D::Fit_Middle,
                                                  fit->getX(), fit->getY(), context);
            }
            if (reinterpret_cast<quintptr>(binding.bins) + BinParameters2D::Fit_Upper ==
                    fit->getTracePtr()) {
                return convertMouseoverToolTipFit(binding.bins, yBins.get(), false, binding.x,
                                                  binding.y, BinParameters2D::Fit_Upper,
                                                  fit->getX(), fit->getY(), context);
            }
            if (reinterpret_cast<quintptr>(binding.bins) + BinParameters2D::Fit_Uppermost ==
                    fit->getTracePtr()) {
                return convertMouseoverToolTipFit(binding.bins, yBins.get(), false, binding.x,
                                                  binding.y, BinParameters2D::Fit_Uppermost,
                                                  fit->getX(), fit->getY(), context);
            }
        }
    }

    if (auto modelScan = qobject_cast<Graph2DMouseoverComponentTraceFillModelScan *>(component)) {
        for (const auto &binding : traceBindings) {
            if (reinterpret_cast<quintptr>(binding.trace) != modelScan->getTracePtr())
                continue;

            return convertMouseoverToolTipModelScan(binding.trace, traces.get(), binding.x,
                                                    binding.y, binding.z, modelScan->getX(),
                                                    modelScan->getY(), modelScan->getZ(), context);
        }
    }

    if (auto item = qobject_cast<Graph2DMouseoverLegendItem *>(component)) {
        if (auto legendItem = item->getItem()) {
            return legendItem->getLegendTooltip();
        }
    }

    return {};
}

class Graph2D::TitleModificationComponent : public DisplayModificationComponent {
    QPointer<Graph2D> graph;
public:
    explicit TitleModificationComponent(Graph2D *graph) : graph(graph)
    { }

    virtual ~TitleModificationComponent() = default;

    QString getTitle() const override
    { return tr("Graph Title"); }

    QList<QAction *> getMenuActions(QWidget *parent = nullptr) override
    {
        if (graph.isNull())
            return {};

        QList<QAction *> result;

        QAction *act;

        act = new QAction(tr("Set &Title", "Context|SetTitle"), this);
        act->setToolTip(tr("Change the main graph title."));
        act->setStatusTip(tr("Set the graph title"));
        act->setWhatsThis(tr("Change or disable the main title on the top of the graph."));
        QObject::connect(act, &QAction::triggered, this, [this, parent] {
            if (graph.isNull())
                return;

            bool ok = false;
            auto title =
                    QInputDialog::getText(parent, tr("Set Graph Title", "Context|GraphTitleDialog"),
                                          tr("Title:", "Context|GraphTitleLabel"),
                                          QLineEdit::Normal, graph->parameters->getTitleText(),
                                          &ok);
            if (!ok)
                return;
            graph->overrideParameters()->overrideTitleText(title);
            graph->mergeOverrideParameters();
            emit changed();
        });
        result.append(act);

        act = new QAction(tr("&Color", "Context|SetTitleColor"), this);
        act->setToolTip(tr("Change the main graph title color."));
        act->setStatusTip(tr("Set the graph title color"));
        act->setWhatsThis(tr("Set the color of the main title on the top of the graph."));
        QObject::connect(act, &QAction::triggered, this, [this, parent] {
            if (graph.isNull())
                return;

            QColor color = graph->parameters->getTitleColor();
            if (!color.isValid())
                color = QColor(0, 0, 0);
            QColorDialog dialog(color, parent);
            dialog.setWindowModality(Qt::ApplicationModal);
            dialog.setWindowTitle(tr("Set Axis Title Color"));
            dialog.setOption(QColorDialog::ShowAlphaChannel);
            if (dialog.exec() != QDialog::Accepted)
                return;
            graph->overrideParameters()->overrideTitleColor(dialog.currentColor());
            graph->mergeOverrideParameters();
            emit changed();
        });
        result.append(act);

        act = new QAction(tr("&Font", "Context|SetTitleFont"), this);
        act->setToolTip(tr("Change the axis title font."));
        act->setStatusTip(tr("Set the axis title font"));
        act->setWhatsThis(tr("Set the font of the title text on the selected axis."));
        QObject::connect(act, &QAction::triggered, this, [this, parent] {
            if (graph.isNull())
                return;

            QFontDialog dialog(graph->parameters->getTitleFont(), parent);
            dialog.setWindowModality(Qt::ApplicationModal);
            dialog.setWindowTitle(tr("Set Axis Title Font"));
            if (dialog.exec() != QDialog::Accepted)
                return;
            graph->overrideParameters()->overrideTitleFont(dialog.selectedFont());
            graph->mergeOverrideParameters();
            emit changed();
        });
        result.append(act);

        return result;
    }
};

namespace Internal {

Graph2DLegendModification::Graph2DLegendModification(Graph2D *graph) : graph(graph)
{ }

Graph2DLegendModification::~Graph2DLegendModification() = default;

QString Graph2DLegendModification::getTitle() const
{ return tr("Legend"); }

void Graph2DLegendModification::setPosition(Graph2DParameters::LegendPosition pos)
{
    if (graph.isNull())
        return;
    graph->overrideParameters()->overrideLegendPosition(pos);
    graph->mergeOverrideParameters();
    emit changed();
}

QList<QAction *> Graph2DLegendModification::getMenuActions(QWidget *parent)
{
    if (graph.isNull())
        return {};

    QList<QAction *> result;

    QAction *act;
    QActionGroup *group;
    QMenu *menu;

    menu = new QMenu(tr("&Position", "Context|LegendPosition"), this);
    result.append(menu->menuAction());
    group = new QActionGroup(menu);

    act = menu->addAction(tr("Top &Left", "Context|LegendTopLeft"));
    act->setToolTip(tr("Position the legend on the top left."));
    act->setStatusTip(tr("Set the legend position"));
    act->setWhatsThis(tr("Set the legend position to the top left corner inside the graph."));
    act->setCheckable(true);
    act->setChecked(graph->parameters->getLegendPosition() == Graph2DParameters::Inside_TopLeft);
    act->setActionGroup(group);
    QObject::connect(act, &QAction::triggered, this,
                     std::bind(&Graph2DLegendModification::setPosition, this,
                               Graph2DParameters::Inside_TopLeft));

    act = menu->addAction(tr("Top &Right", "Context|LegendTopRight"));
    act->setToolTip(tr("Position the legend on the top right."));
    act->setStatusTip(tr("Set the legend position"));
    act->setWhatsThis(tr("Set the legend position to the top right corner inside the graph."));
    act->setCheckable(true);
    act->setChecked(graph->parameters->getLegendPosition() == Graph2DParameters::Inside_TopRight);
    act->setActionGroup(group);
    QObject::connect(act, &QAction::triggered, this,
                     std::bind(&Graph2DLegendModification::setPosition, this,
                               Graph2DParameters::Inside_TopRight));

    act = menu->addAction(tr("&Bottom Left", "Context|LegendBottomLeft"));
    act->setToolTip(tr("Position the legend on the bottom left."));
    act->setStatusTip(tr("Set the legend position"));
    act->setWhatsThis(tr("Set the legend position to the bottom left corner inside the graph."));
    act->setCheckable(true);
    act->setChecked(graph->parameters->getLegendPosition() == Graph2DParameters::Inside_BottomLeft);
    act->setActionGroup(group);
    QObject::connect(act, &QAction::triggered, this,
                     std::bind(&Graph2DLegendModification::setPosition, this,
                               Graph2DParameters::Inside_BottomLeft));

    act = menu->addAction(tr("B&ottom Right", "Context|LegendBottomRight"));
    act->setToolTip(tr("Position the legend on the bottom right."));
    act->setStatusTip(tr("Set the legend position"));
    act->setWhatsThis(tr("Set the legend position to the bottom right corner inside the graph."));
    act->setCheckable(true);
    act->setChecked(
            graph->parameters->getLegendPosition() == Graph2DParameters::Inside_BottomRight);
    act->setActionGroup(group);
    QObject::connect(act, &QAction::triggered, this,
                     std::bind(&Graph2DLegendModification::setPosition, this,
                               Graph2DParameters::Inside_BottomRight));

    act = menu->addAction(tr("&Outside Left", "Context|LegendOutsideLeft"));
    act->setToolTip(tr("Position the legend outside the graph on the left."));
    act->setStatusTip(tr("Set the legend position"));
    act->setWhatsThis(tr("Set the legend position to outside the graph on the left."));
    act->setCheckable(true);
    act->setChecked(graph->parameters->getLegendPosition() == Graph2DParameters::Outside_Left);
    act->setActionGroup(group);
    QObject::connect(act, &QAction::triggered, this,
                     std::bind(&Graph2DLegendModification::setPosition, this,
                               Graph2DParameters::Outside_Left));

    act = menu->addAction(tr("O&utside Right", "Context|LegendOutsideRight"));
    act->setToolTip(tr("Position the legend outside the graph on the right."));
    act->setStatusTip(tr("Set the legend position"));
    act->setWhatsThis(tr("Set the legend position to outside the graph on the right."));
    act->setCheckable(true);
    act->setChecked(graph->parameters->getLegendPosition() == Graph2DParameters::Outside_Right);
    act->setActionGroup(group);
    QObject::connect(act, &QAction::triggered, this,
                     std::bind(&Graph2DLegendModification::setPosition, this,
                               Graph2DParameters::Outside_Right));

    return result;
}

}

/**
 * Convert the given mouseover component into a modification component
 * suitable to return to a remote handler.
 * 
 * @param component the mouse over component
 * @return          a new mouse modification component or NULL
 */
DisplayModificationComponent *Graph2D::convertModificationComponent(Graph2DMouseoverComponent *component)
{
    if (auto legendItem = qobject_cast<Graph2DMouseoverLegendItem *>(component)) {
        if (auto item = legendItem->getItem()) {
            if (auto check = item->createModificationComponent(this))
                return check;
        }
    }

    if (qobject_cast<Graph2DMouseoverLegendItem *>(component)) {
        return new Graph2DLegendModification(this);
    }

    if (auto axis = qobject_cast<Graph2DMouseoverAxis *>(component)) {
        for (auto displayAxis : xAxes->getDisplayed()) {
            auto axis2D = static_cast<AxisDimension2D *>(displayAxis);
            if (reinterpret_cast<quintptr>(axis2D) != axis->getAxisPtr())
                continue;
            if (auto check = axis2D->createModificationComponent(this))
                return check;
        }

        for (auto displayAxis : yAxes->getDisplayed()) {
            auto axis2D = static_cast<AxisDimension2D *>(displayAxis);
            if (reinterpret_cast<quintptr>(axis2D) != axis->getAxisPtr())
                continue;
            if (auto check = axis2D->createModificationComponent(this))
                return check;
        }

        for (auto displayAxis : zAxes->getDisplayed()) {
            auto axis2D = static_cast<AxisDimension2D *>(displayAxis);
            if (reinterpret_cast<quintptr>(axis2D) != axis->getAxisPtr())
                continue;
            if (auto check = axis2D->createModificationComponent(this))
                return check;
        }
    }

    if (qobject_cast<Graph2DMouseoverTitle *>(component)) {
        return new TitleModificationComponent(this);
    }

    return nullptr;
}


/**
 * Apply mouse initiated zoom to the X axes only.
 * 
 * @param xAxes the X axis set
 * @param minX  the coordinate of the minimum value
 * @param maxX  the coordinate of the maximum value
 */
void Graph2D::zoomMouseX(AxisDimensionSet2DSide *xAxes, qreal minX, qreal maxX)
{
    emit internalZoom(xAxes->convertMouseZoom(minX, maxX));
}

/**
 * Apply mouse initiated zoom to the Y axes only.
 * 
 * @param yAxes the Y axis set
 * @param minY  the coordinate of the minimum value
 * @param maxY  the coordinate of the maximum value
 */
void Graph2D::zoomMouseY(AxisDimensionSet2DSide *yAxes, qreal minY, qreal maxY)
{
    emit internalZoom(yAxes->convertMouseZoom(minY, maxY));
}

/**
 * Apply mouse initiated zoom to both axes.
 * 
 * @param xAxes the X axis set
 * @param yAxes the Y axis set
 * @param minX  the coordinate of the minimum X value
 * @param maxX  the coordinate of the maximum X value
 * @param minY  the coordinate of the minimum Y value
 * @param maxY  the coordinate of the maximum Y value
 */
void Graph2D::zoomMouseBoth(AxisDimensionSet2DSide *xAxes,
                            AxisDimensionSet2DSide *yAxes,
                            qreal minX,
                            qreal maxX,
                            qreal minY,
                            qreal maxY)
{
    DisplayZoomCommand zoom(xAxes->convertMouseZoom(minX, maxX));
    zoom.append(yAxes->convertMouseZoom(minY, maxY));
    emit internalZoom(zoom);
}


BinSet2DNOOP::BinSet2DNOOP() = default;

BinSet2DNOOP::~BinSet2DNOOP() = default;

BinSet2DNOOP::Cloned BinSet2DNOOP::clone() const
{ return Cloned(new BinSet2DNOOP()); }

void BinSet2DNOOP::initialize(const ValueSegment::Transfer &, const DisplayDefaults &, bool)
{ }

bool BinSet2DNOOP::process(bool, bool)
{ return false; }

void BinSet2DNOOP::registerChain(ProcessingTapChain *, bool)
{ emit readyForFinalProcessing(); }

std::unique_ptr<TraceParameters> BinSet2DNOOP::parseParameters(const Variant::Read &) const
{ return std::unique_ptr<TraceParameters>(new BinParameters2D()); }

BinSet2DNOOP::CreatedHandler BinSet2DNOOP::createHandler(std::unique_ptr<
        TraceParameters> &&parameters) const
{
    return CreatedHandler(new BinHandler2D(std::unique_ptr<BinParameters2D>(
            static_cast<BinParameters2D *>(parameters.release()))));
}

}
}
