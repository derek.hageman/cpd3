/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QColor>
#include <QMap>
#include <QApplication>
#include <QIconEngine>
#include <QImage>
#include <QPixmap>
#include <QDialog>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QVBoxLayout>
#include <QDialogButtonBox>

#include "graphing/tracecommon.hxx"
#include "algorithms/linearint.hxx"
#include "algorithms/statistics.hxx"

using namespace CPD3::Data;
using namespace CPD3::Algorithms;

namespace CPD3 {
namespace Graphing {

/** @file graphing/tracecommon.hxx
 * Provides general utilities related to normal traces.
 */

static const qreal symbolBaseSize = 0.75;

static double colorDistance(const QColor &a, const QColor &b)
{
    qreal aH, aS, aV;
    a.getHsvF(&aH, &aS, &aV);
    qreal bH, bS, bV;
    b.getHsvF(&bH, &bS, &bV);
    aH -= bH;
    aS -= bS;
    aS *= 0.5;
    aV -= bV;
    aV *= 0.75;
    return sqrt(aH * aH + aS * aS + aV * aV);
}

/**
 * Generate a list of colors for a list of traces.  This makes a "best attempt"
 * to make the colors easily distinguished from one another.
 * 
 * @param needColors    the total number of colors required
 * @param claimed       a list of already claimed colors to try to avoid
 * @return              a list of colors
 * @see http://www.sron.nl/~pault/colourschemes.pdf
 */
std::deque<QColor> TraceUtilities::defaultTraceColors(std::size_t needColors,
                                                      const std::deque<QColor> &claimed)
{
    std::deque<QColor> proposed;
    std::size_t nAdd = 0;
    for (;;) {
        proposed.clear();
        for (std::size_t i = 0, max = needColors + nAdd; i < max; i++) {
            QColor c;
            if (max <= 12) {
                switch (max) {
                case 1:
                    c.setRgb(68, 119, 170);
                    break;
                case 2:
                    switch (i) {
                    case 0:
                        c.setRgb(68, 119, 170);
                        break;
                    case 1:
                        c.setRgb(204, 102, 119);
                        break;
                    }
                    break;
                case 3:
                    switch (i) {
                    case 0:
                        c.setRgb(68, 119, 170);
                        break;
                    case 1:
                        c.setRgb(221, 204, 119);
                        break;
                    case 2:
                        c.setRgb(204, 102, 119);
                        break;
                    }
                    break;
                case 4:
                    switch (i) {
                    case 0:
                        c.setRgb(68, 119, 170);
                        break;
                    case 1:
                        c.setRgb(17, 119, 51);
                        break;
                    case 2:
                        c.setRgb(221, 204, 119);
                        break;
                    case 3:
                        c.setRgb(204, 102, 119);
                        break;
                    }
                    break;
                case 5:
                    switch (i) {
                    case 0:
                        c.setRgb(51, 34, 136);
                        break;
                    case 1:
                        c.setRgb(136, 204, 238);
                        break;
                    case 2:
                        c.setRgb(17, 119, 51);
                        break;
                    case 3:
                        c.setRgb(221, 204, 119);
                        break;
                    case 4:
                        c.setRgb(204, 102, 119);
                        break;
                    }
                    break;
                case 6:
                    switch (i) {
                    case 0:
                        c.setRgb(51, 34, 136);
                        break;
                    case 1:
                        c.setRgb(136, 204, 238);
                        break;
                    case 2:
                        c.setRgb(17, 119, 51);
                        break;
                    case 3:
                        c.setRgb(221, 204, 119);
                        break;
                    case 4:
                        c.setRgb(204, 102, 119);
                        break;
                    case 5:
                        c.setRgb(170, 68, 153);
                        break;
                    }
                    break;
                case 7:
                    switch (i) {
                    case 0:
                        c.setRgb(51, 34, 136);
                        break;
                    case 1:
                        c.setRgb(136, 204, 238);
                        break;
                    case 2:
                        c.setRgb(68, 170, 153);
                        break;
                    case 3:
                        c.setRgb(17, 119, 51);
                        break;
                    case 4:
                        c.setRgb(221, 204, 119);
                        break;
                    case 5:
                        c.setRgb(204, 102, 119);
                        break;
                    case 6:
                        c.setRgb(170, 68, 153);
                        break;
                    }
                    break;
                case 8:
                    switch (i) {
                    case 0:
                        c.setRgb(51, 34, 136);
                        break;
                    case 1:
                        c.setRgb(136, 204, 238);
                        break;
                    case 2:
                        c.setRgb(68, 170, 153);
                        break;
                    case 3:
                        c.setRgb(17, 119, 51);
                        break;
                    case 4:
                        c.setRgb(153, 153, 51);
                        break;
                    case 5:
                        c.setRgb(221, 204, 119);
                        break;
                    case 6:
                        c.setRgb(204, 102, 119);
                        break;
                    case 7:
                        c.setRgb(170, 68, 153);
                        break;
                    }
                    break;
                case 9:
                    switch (i) {
                    case 0:
                        c.setRgb(51, 34, 136);
                        break;
                    case 1:
                        c.setRgb(136, 204, 238);
                        break;
                    case 2:
                        c.setRgb(68, 170, 153);
                        break;
                    case 3:
                        c.setRgb(17, 119, 51);
                        break;
                    case 4:
                        c.setRgb(153, 153, 51);
                        break;
                    case 5:
                        c.setRgb(221, 204, 119);
                        break;
                    case 6:
                        c.setRgb(204, 102, 119);
                        break;
                    case 7:
                        c.setRgb(136, 34, 85);
                        break;
                    case 8:
                        c.setRgb(170, 68, 153);
                        break;
                    }
                    break;
                case 10:
                    switch (i) {
                    case 0:
                        c.setRgb(51, 34, 136);
                        break;
                    case 1:
                        c.setRgb(136, 204, 238);
                        break;
                    case 2:
                        c.setRgb(68, 170, 153);
                        break;
                    case 3:
                        c.setRgb(17, 119, 51);
                        break;
                    case 4:
                        c.setRgb(153, 153, 51);
                        break;
                    case 5:
                        c.setRgb(221, 204, 119);
                        break;
                    case 6:
                        c.setRgb(102, 17, 0);
                        break;
                    case 7:
                        c.setRgb(204, 102, 119);
                        break;
                    case 8:
                        c.setRgb(136, 34, 85);
                        break;
                    case 9:
                        c.setRgb(170, 68, 153);
                        break;
                    }
                    break;
                case 11:
                    switch (i) {
                    case 0:
                        c.setRgb(51, 34, 136);
                        break;
                    case 1:
                        c.setRgb(102, 153, 204);
                        break;
                    case 2:
                        c.setRgb(136, 204, 238);
                        break;
                    case 3:
                        c.setRgb(68, 170, 153);
                        break;
                    case 4:
                        c.setRgb(17, 119, 51);
                        break;
                    case 5:
                        c.setRgb(153, 153, 51);
                        break;
                    case 6:
                        c.setRgb(221, 204, 119);
                        break;
                    case 7:
                        c.setRgb(102, 17, 0);
                        break;
                    case 8:
                        c.setRgb(204, 102, 119);
                        break;
                    case 9:
                        c.setRgb(136, 34, 85);
                        break;
                    case 10:
                        c.setRgb(170, 68, 153);
                        break;
                    }
                    break;
                case 12:
                    switch (i) {
                    case 0:
                        c.setRgb(51, 34, 136);
                        break;
                    case 1:
                        c.setRgb(102, 153, 204);
                        break;
                    case 2:
                        c.setRgb(136, 204, 238);
                        break;
                    case 3:
                        c.setRgb(68, 170, 153);
                        break;
                    case 4:
                        c.setRgb(17, 119, 51);
                        break;
                    case 5:
                        c.setRgb(153, 153, 51);
                        break;
                    case 6:
                        c.setRgb(221, 204, 119);
                        break;
                    case 7:
                        c.setRgb(102, 17, 0);
                        break;
                    case 8:
                        c.setRgb(204, 102, 119);
                        break;
                    case 9:
                        c.setRgb(170, 68, 102);
                        break;
                    case 10:
                        c.setRgb(136, 34, 85);
                        break;
                    case 11:
                        c.setRgb(170, 68, 153);
                        break;
                    }
                    break;
                }
            } else {
                auto mx2 = static_cast<std::size_t>(std::ceil(max / 2.0));
                /* Use light and dark variants */
                if (i < mx2) {
                    c.setHsvF((qreal) (mx2 - 1 - i) / (qreal) mx2, (qreal) 1.0, (qreal) 1.0);
                } else {
                    c.setHsvF((qreal) ((max - 1) - i) / (qreal) mx2, (qreal) 1.0, (qreal) 0.5);
                }
                /* Attenuate green, since it's hard to see against white */
                c.setGreenF(c.greenF() * (qreal) 0.75);
            }

            bool valid = true;
            for (std::size_t j = 0, nClaimed = claimed.size(); j < nClaimed; j++) {
                if (colorDistance(claimed[j], c) < (qreal) 0.5 / (qreal) max) {
                    valid = false;
                    break;
                }
            }
            if (!valid && nAdd < std::max<std::size_t>(10, (claimed.size() + needColors) * 5))
                continue;
            proposed.emplace_back(std::move(c));
            if (proposed.size() >= needColors)
                break;
        }
        if (proposed.size() >= needColors)
            break;
        nAdd++;
    }
    return proposed;
}

namespace {
enum WavelengthClaim {
    Blue_Base, Blue_PM1, Green_Base, Green_PM1, Red_Base, Red_PM1,
};

/* C++ < 14 does not provide specialization for enums */
struct WavelengthClaimHash {
    inline std::size_t operator()(WavelengthClaim t) const
    { return std::hash<std::size_t>()(static_cast<std::size_t>(t)); }
};

static QColor wavelengthColor(WavelengthClaim wl)
{
    switch (wl) {
    case Blue_Base:
        return QColor(0, 0, 255);
    case Blue_PM1:
        return QColor(0, 0, 127);
    case Green_Base:
        return QColor(0, 140, 0);
    case Green_PM1:
        return QColor(0, 95, 0);
    case Red_Base:
        return QColor(255, 0, 0);
    case Red_PM1:
        return QColor(127, 0, 0);
    }
    return QColor();
}
};

/**
 * Generate a list of colors for a list of trace units.  If the unit specifies
 * a variable type with a fixed color (e.x. an optical scattering channel) then
 * the color returned is fixed.  For other variables this makes a "best attempt"
 * to make the colors easily distinguished from one another.
 * 
 * @param requested     the requests to colorize
 * @param claimed       a list of already claimed colors to try to avoid
 * @return              a list of colors in the same order as the given units
 */
std::deque<QColor> TraceUtilities::defaultTraceColors(const std::vector<ColorRequest> &requested,
                                                      const std::deque<QColor> &claimed)
{
    std::deque<QColor> result;
    auto totalClaimed = claimed;

    std::deque<std::size_t> unset;
    std::unordered_map<WavelengthClaim, std::deque<std::size_t>, WavelengthClaimHash>
            wavelengthGeneral;

    static thread_local QRegularExpression colorCodeMatcher(".([BGRQZ])_[SAE]\\d*$");
    for (std::size_t i = 0, max = requested.size(); i < max; i++) {
        const auto &unit = requested[i].unit;
        QRegularExpressionMatch colorFound;
        if (unit.isValid() &&
                (colorFound = colorCodeMatcher.match(unit.getVariableQString())).hasMatch()) {
            auto code = colorFound.captured(1);
            QColor selected;
            if (code == "B") {
                if (!unit.hasFlavor("pm1")) {
                    selected = wavelengthColor(Blue_Base);
                    wavelengthGeneral[Blue_Base].emplace_back(i);
                } else {
                    selected = wavelengthColor(Blue_PM1);
                    wavelengthGeneral[Blue_PM1].emplace_back(i);
                }
            } else if (code == "G") {
                if (!unit.hasFlavor("pm1")) {
                    selected = wavelengthColor(Green_Base);
                    wavelengthGeneral[Green_Base].emplace_back(i);
                } else {
                    selected = wavelengthColor(Green_PM1);
                    wavelengthGeneral[Green_PM1].emplace_back(i);
                }
            } else if (code == "R") {
                if (!unit.hasFlavor("pm1")) {
                    selected = wavelengthColor(Red_Base);
                    wavelengthGeneral[Red_Base].emplace_back(i);
                } else {
                    selected = wavelengthColor(Red_PM1);
                    wavelengthGeneral[Red_PM1].emplace_back(i);
                }
            } else {
                selected = QColor(0, 0, 0);
            }

            if (std::find(totalClaimed.begin(), totalClaimed.end(), selected) ==
                    totalClaimed.end()) {
                while (result.size() < i) {
                    result.emplace_back();
                }
                result.emplace_back(selected);
                totalClaimed.emplace_back(selected);
                continue;
            }
        } else if (FP::defined(requested.at(i).wavelength)) {
            double wl = requested.at(i).wavelength;
            if (wl >= 400.0 && wl <= 750.0) {
                if (wl < 500.0) {
                    if (!unit.hasFlavor("pm1"))
                        wavelengthGeneral[Blue_Base].emplace_back(i);
                    else
                        wavelengthGeneral[Blue_PM1].emplace_back(i);
                } else if (wl < 600.0) {
                    if (!unit.hasFlavor("pm1"))
                        wavelengthGeneral[Green_Base].emplace_back(i);
                    else
                        wavelengthGeneral[Green_PM1].emplace_back(i);
                } else {
                    if (!unit.hasFlavor("pm1"))
                        wavelengthGeneral[Red_Base].emplace_back(i);
                    else
                        wavelengthGeneral[Red_PM1].emplace_back(i);
                }
            }
        }
        unset.emplace_back(i);
    }
    if (unset.empty())
        return result;

    bool multipleWavelength = false;
    for (const auto &check : wavelengthGeneral) {
        if (check.second.size() != 1) {
            multipleWavelength = true;
            break;
        }
    }
    if (!multipleWavelength) {
        for (const auto &check : wavelengthGeneral) {
            Q_ASSERT(check.second.size() == 1);

            auto selected = wavelengthColor(check.first);
            auto index = check.second.front();
            if (std::find(totalClaimed.begin(), totalClaimed.end(), selected) ==
                    totalClaimed.end()) {
                while (result.size() <= index) {
                    result.emplace_back();
                }
                result[index] = selected;
                totalClaimed.emplace_back(selected);
                unset.erase(unset.begin() + index);
                continue;
            }
        }
    }

    if (unset.empty())
        return result;
    auto addColors = defaultTraceColors(unset.size(), totalClaimed);
    for (std::size_t i = 0, max = unset.size(); i < max; i++) {
        auto idx = unset[i];
        if (idx < result.size()) {
            result[idx] = std::move(addColors[i]);
        } else {
            result.emplace_back(std::move(addColors[i]));
        }
    }

    return result;
}

namespace {
class BasicGradientModel : public Model {
protected:
    void serialize(QDataStream &) const override
    { Q_ASSERT(false); }

    virtual void evaluate(double x, double &r, double &g, double &b, double &a) const = 0;

public:
    BasicGradientModel()
    { }

    virtual ~BasicGradientModel()
    { }

    QVector<double> applyConst(const QVector<double> &inputs) const override
    {
        if (inputs.size() < 1)
            return QVector<double>();
        QVector<double> outputs(4, 1.0);
        evaluate(inputs.at(0), outputs[0], outputs[1], outputs[2], outputs[3]);
        return outputs;
    }

    QVector<double> apply(const QVector<double> &inputs) override
    {
        if (inputs.size() < 1)
            return QVector<double>();
        QVector<double> outputs(4, 1.0);
        evaluate(inputs.at(0), outputs[0], outputs[1], outputs[2], outputs[3]);
        return outputs;
    }

    void applyIOConst(QVector<double> &values) const override
    {
        if (values.size() < 1)
            return;
        double x = values.at(0);
        values.fill(1.0, 4);
        evaluate(x, values[0], values[1], values[2], values[3]);
    }

    void applyIO(QVector<double> &values) override
    {
        if (values.size() < 1)
            return;
        double x = values.at(0);
        values.fill(1.0, 4);
        evaluate(x, values[0], values[1], values[2], values[3]);
    }
};

class SequentialGradientModel : public BasicGradientModel {
protected:
    void evaluate(double x, double &r, double &g, double &b, double &) const override
    {
        if (!FP::defined(x))
            return;
        if (x < 0.0) x = 0.0; else if (x > 1.0) x = 1.0;
        r = 1.0 - 0.392 * (1.0 + Statistics::erf((x - 0.869) / 0.255));
        g = 1.021 - 0.456 * (1.0 + Statistics::erf((x - 0.527) / 0.376));
        b = 1.0 - 0.493 * (1.0 + Statistics::erf((x - 0.272) / 0.309));
    }

public:
    SequentialGradientModel()
    { }

    virtual ~SequentialGradientModel()
    { }

    Model *clone() const override
    { return new SequentialGradientModel; }
};

class DivergingGradientModel : public BasicGradientModel {
protected:
    void evaluate(double x, double &r, double &g, double &b, double &) const override
    {
        if (!FP::defined(x))
            return;
        if (x < 0.0) x = 0.0; else if (x > 1.0) x = 1.0;
        double x2 = x * x;
        double x3 = x2 * x;
        double x4 = x2 * x2;
        double x5 = x3 * x2;
        r = 0.237 - 2.13 * x + 26.92 * x2 - 65.5 * x3 + 63.5 * x4 - 22.36 * x5;
        g = (1.0 - 0.291 * x + 0.1574 * x2);
        if (g == 0.0) g = 1E-5;
        g = (0.572 + 1.524 * x - 1.811 * x2) / g;
        g = g * g;
        b = 1.579 - 4.03 * x + 12.92 * x2 - 31.4 * x3 + 48.6 * x4 - 23.36 * x5;
        if (b == 0.0) b = 1E-5;
        b = 1.0 / b;
    }

public:
    DivergingGradientModel()
    { }

    virtual ~DivergingGradientModel()
    { }

    Model *clone() const override
    { return new DivergingGradientModel; }
};

class RainbowGradientModel : public BasicGradientModel {
protected:
    void evaluate(double x, double &r, double &g, double &b, double &) const override
    {
        if (!FP::defined(x))
            return;
        if (x < 0.0) x = 0.0; else if (x > 1.0) x = 1.0;
        double x2 = x * x;
        double x3 = x2 * x;
        double x4 = x2 * x2;
        double x5 = x3 * x2;
        double x6 = x3 * x3;
        r = 1.0 + 8.72 * x - 19.17 * x2 + 14.1 * x3;
        if (r == 0.0) r = 1E-5;
        r = (0.472 - 0.567 * x + 4.05 * x2) / r;
        g = 0.108932 -
                1.22635 * x +
                27.284 * x2 -
                98.577 * x3 +
                163.3 * x4 -
                131.395 * x5 +
                40.634 * x6;
        b = 1.97 + 3.54 * x - 68.5 * x2 + 243.0 * x3 - 297.0 * x4 + 125.0 * x5;
        if (b == 0.0) b = 1E-5;
        b = 1.0 / b;
    }

public:
    RainbowGradientModel()
    { }

    virtual ~RainbowGradientModel()
    { }

    Model *clone() const override
    { return new RainbowGradientModel; }
};

};

TraceGradient::TraceGradient()
{
    model = new SequentialGradientModel;
    spec = QColor::Rgb;
}

TraceGradient::~TraceGradient()
{ delete model; }

TraceGradient::TraceGradient(const TraceGradient &other) : model(other.model->clone()),
                                                           spec(other.spec)
{ }

TraceGradient &TraceGradient::operator=(const TraceGradient &other)
{
    if (&other == this) return *this;
    delete model;
    model = other.model->clone();
    spec = other.spec;
    return *this;
}

void TraceGradient::setBasicRainbowGradient()
{
    QList<ModelInputConstraints> inputs;
    inputs.append(ModelInputConstraints());

    QList<ModelOutputConstraints> outputs;
    outputs.append(ModelOutputConstraints(0.0, 1.0));
    outputs.append(ModelOutputConstraints(0.0, 1.0));
    outputs.append(ModelOutputConstraints(0.0, 1.0));
    outputs.append(ModelOutputConstraints(0.0, 1.0));

    ModelParameters parameters;
    parameters.setDimension(0, QVector<double>() << 0.0 << 0.25 << 0.5 << 0.75 << 1.0);
    parameters.setDimension(1, QVector<double>() << 0.3 << 0.0 << 0.0 << 1.0 << 1.0);
    parameters.setDimension(2, QVector<double>() << 0.0 << 0.0 << 1.0 << 0.0 << 1.0);
    parameters.setDimension(3, QVector<double>() << 0.3 << 1.0 << 1.0 << 0.0 << 0.0);
    parameters.setDimension(4, QVector<double>() << 1.0 << 1.0 << 1.0 << 1.0 << 1.0);

    Variant::Root configuration;
    configuration["Type"].setString("linear");
    model = Model::fromConfiguration(configuration, inputs, outputs, parameters);
    spec = QColor::Rgb;
}

/**
 * Construct a gradient using the given configuration.
 * 
 * @param configuration the configuration to use
 */
TraceGradient::TraceGradient(const Variant::Read &configuration)
{
    if (!configuration.exists()) {
        model = new SequentialGradientModel;
        spec = QColor::Rgb;
        return;
    }

    auto type = configuration["Type"].toString();
    if (type.empty())
        type = configuration.toString();
    if (Util::equal_insensitive(type, "sequential", "default")) {
        model = new SequentialGradientModel;
        spec = QColor::Rgb;
        return;
    } else if (Util::equal_insensitive(type, "diverging", "diverge")) {
        model = new DivergingGradientModel;
        spec = QColor::Rgb;
        return;
    } else if (Util::equal_insensitive(type, "rainbow")) {
        model = new RainbowGradientModel;
        spec = QColor::Rgb;
        return;
    } else if (Util::equal_insensitive(type, "alternaterainbow", "altrainbow")) {
        setBasicRainbowGradient();
        return;
    }

    auto colors = configuration["Colors"].toKeyframe();
    if (colors.empty()) {
        model = new SequentialGradientModel;
        spec = QColor::Rgb;
        return;
    }

    QList<ModelOutputConstraints> outputs;
    const auto &mode = configuration["Mode"].toString();
    if (Util::equal_insensitive(mode, "hsv")) {
        spec = QColor::Hsv;
        outputs.append(ModelOutputConstraints(0.0, 1.0, true));
        outputs.append(ModelOutputConstraints(0.0, 1.0));
        outputs.append(ModelOutputConstraints(0.0, 1.0));
        outputs.append(ModelOutputConstraints(0.0, 1.0));
    } else if (Util::equal_insensitive(mode, "hsl")) {
        spec = QColor::Hsl;
        outputs.append(ModelOutputConstraints(0.0, 1.0, true));
        outputs.append(ModelOutputConstraints(0.0, 1.0));
        outputs.append(ModelOutputConstraints(0.0, 1.0));
        outputs.append(ModelOutputConstraints(0.0, 1.0));
    } else if (Util::equal_insensitive(mode, "cmyk")) {
        spec = QColor::Cmyk;
        outputs.append(ModelOutputConstraints(0.0, 1.0));
        outputs.append(ModelOutputConstraints(0.0, 1.0));
        outputs.append(ModelOutputConstraints(0.0, 1.0));
        outputs.append(ModelOutputConstraints(0.0, 1.0));
        outputs.append(ModelOutputConstraints(0.0, 1.0));
    } else {
        spec = QColor::Rgb;
        outputs.append(ModelOutputConstraints(0.0, 1.0));
        outputs.append(ModelOutputConstraints(0.0, 1.0));
        outputs.append(ModelOutputConstraints(0.0, 1.0));
        outputs.append(ModelOutputConstraints(0.0, 1.0));
    }

    QList<ModelInputConstraints> inputs;
    inputs.append(ModelInputConstraints());

    ModelParameters parameters;

    for (auto i : colors) {
        QColor cl(Display::parseColor(i.second, QColor(0, 0, 0)));
        if (!cl.isValid())
            continue;
        parameters.appendDimension(0, i.first);

        switch (spec) {
        case QColor::Hsv: {
            qreal h, s, v, a;
            cl.getHsvF(&h, &s, &v, &a);
            parameters.appendDimension(1, h);
            parameters.appendDimension(2, s);
            parameters.appendDimension(3, v);
            parameters.appendDimension(4, a);
            break;
        }
        case QColor::Cmyk: {
            qreal c, m, y, k, a;
            cl.getCmykF(&c, &m, &y, &k, &a);
            parameters.appendDimension(1, c);
            parameters.appendDimension(2, m);
            parameters.appendDimension(3, y);
            parameters.appendDimension(4, k);
            parameters.appendDimension(5, a);
            break;
        }
        case QColor::Hsl: {
            qreal h, s, l, a;
            cl.getHslF(&h, &s, &l, &a);
            parameters.appendDimension(1, h);
            parameters.appendDimension(2, s);
            parameters.appendDimension(3, l);
            parameters.appendDimension(4, a);
            break;
        }
        default: {
            qreal r, g, b, a;
            cl.getRgbF(&r, &g, &b, &a);
            parameters.appendDimension(1, r);
            parameters.appendDimension(2, g);
            parameters.appendDimension(3, b);
            parameters.appendDimension(4, a);
            break;
        }
        }
    }

    if (parameters.getDimension(0).isEmpty()) {
        model = new SequentialGradientModel;
        spec = QColor::Rgb;
        return;
    }

    if (!configuration["Model"].exists()) {
        Variant::Root modelConfig;
        modelConfig["Type"].setString("linear");
        model = Model::fromConfiguration(modelConfig, inputs, outputs, parameters);
    } else {
        model = Model::fromConfiguration(configuration, inputs, outputs, parameters);
    }
}

/**
 * Create a gradient that fades out the given color (full intensity at
 * 1.0).
 * 
 * @param baseColor the color to fade
 */
TraceGradient::TraceGradient(const QColor &baseColor)
{
    QVector<Model *> models;
    qreal alpha;
    switch (baseColor.spec()) {
    case QColor::Hsv: {
        qreal h, s, v;
        baseColor.getHsvF(&h, &s, &v, &alpha);
        models.append(new ModelConstant(h));
        models.append(new ModelConstant(s));
        models.append(new ModelConstant(v));
        spec = QColor::Hsv;
        break;
    }
    case QColor::Hsl: {
        qreal h, s, l;
        baseColor.getHslF(&h, &s, &l, &alpha);
        models.append(new ModelConstant(h));
        models.append(new ModelConstant(s));
        models.append(new ModelConstant(l));
        spec = QColor::Hsl;
        break;
    }
    case QColor::Cmyk: {
        qreal c, m, y, k;
        QColor tmp(baseColor);
        tmp.getCmykF(&c, &m, &y, &k, &alpha);
        models.append(new ModelConstant(c));
        models.append(new ModelConstant(m));
        models.append(new ModelConstant(y));
        models.append(new ModelConstant(k));
        spec = QColor::Cmyk;
        break;
    }
    default: {
        qreal r, g, b;
        baseColor.getRgbF(&r, &g, &b, &alpha);
        models.append(new ModelConstant(r));
        models.append(new ModelConstant(g));
        models.append(new ModelConstant(b));
        spec = QColor::Rgb;
        break;
    }
    }

    models.append(LinearInterpolator::create(QVector<double>() << 1.0 << 0.0,
                                             QVector<double>() << alpha << 0.0));
    model = new ModelSingleComposite(models);
}

/**
 * Evaluate the gradient at given value (usually [0,1]).
 * 
 * @param value the value to fetch a color for
 * @return      the color of the gradient
 */
QColor TraceGradient::evaluate(double value)
{
    QVector<double> result;
    result.append(value);
    result.append(value);
    result.append(value);
    result.append(value);
    if (spec == QColor::Cmyk)
        result.append(value);
    model->applyIO(result);
    QColor c;
    switch (spec) {
    case QColor::Hsv:
        c.setHsvF((FP::defined(result.at(0)) ? qBound(0.0, result.at(0), 1.0) : 0),
                  (FP::defined(result.at(1)) ? qBound(0.0, result.at(1), 1.0) : 0),
                  (FP::defined(result.at(2)) ? qBound(0.0, result.at(2), 1.0) : 0),
                  (FP::defined(result.at(3)) ? qBound(0.0, result.at(3), 1.0) : 1.0));
        break;
    case QColor::Cmyk:
        c.setCmykF((FP::defined(result.at(0)) ? qBound(0.0, result.at(0), 1.0) : 0),
                   (FP::defined(result.at(1)) ? qBound(0.0, result.at(1), 1.0) : 0),
                   (FP::defined(result.at(2)) ? qBound(0.0, result.at(2), 1.0) : 0),
                   (FP::defined(result.at(3)) ? qBound(0.0, result.at(3), 1.0) : 0),
                   (FP::defined(result.at(4)) ? qBound(0.0, result.at(4), 1.0) : 1.0));
        break;
    case QColor::Hsl:
        c.setHslF((FP::defined(result.at(0)) ? qBound(0.0, result.at(0), 1.0) : 0),
                  (FP::defined(result.at(1)) ? qBound(0.0, result.at(1), 1.0) : 0),
                  (FP::defined(result.at(2)) ? qBound(0.0, result.at(2), 1.0) : 0),
                  (FP::defined(result.at(3)) ? qBound(0.0, result.at(3), 1.0) : 1.0));
        break;
    default:
        c.setRgbF((FP::defined(result.at(0)) ? qBound(0.0, result.at(0), 1.0) : 0),
                  (FP::defined(result.at(1)) ? qBound(0.0, result.at(1), 1.0) : 0),
                  (FP::defined(result.at(2)) ? qBound(0.0, result.at(2), 1.0) : 0),
                  (FP::defined(result.at(3)) ? qBound(0.0, result.at(3), 1.0) : 1.0));
        break;
    }
    return c;
}

/**
 * Evaluate the gradient at given value (usually [0,1]).
 * 
 * @param value the value to fetch a color for
 * @return      the color of the gradient
 */
QColor TraceGradient::evaluate(double value) const
{
    QVector<double> result;
    result.append(value);
    result.append(value);
    result.append(value);
    result.append(value);
    if (spec == QColor::Cmyk)
        result.append(value);
    model->applyIO(result);
    QColor c;
    switch (spec) {
    case QColor::Hsv:
        c.setHsvF((FP::defined(result.at(0)) ? qBound(0.0, result.at(0), 1.0) : 0),
                  (FP::defined(result.at(1)) ? qBound(0.0, result.at(1), 1.0) : 0),
                  (FP::defined(result.at(2)) ? qBound(0.0, result.at(2), 1.0) : 0),
                  (FP::defined(result.at(3)) ? qBound(0.0, result.at(3), 1.0) : 1.0));
        break;
    case QColor::Cmyk:
        c.setCmykF((FP::defined(result.at(0)) ? qBound(0.0, result.at(0), 1.0) : 0),
                   (FP::defined(result.at(1)) ? qBound(0.0, result.at(1), 1.0) : 0),
                   (FP::defined(result.at(2)) ? qBound(0.0, result.at(2), 1.0) : 0),
                   (FP::defined(result.at(3)) ? qBound(0.0, result.at(3), 1.0) : 0),
                   (FP::defined(result.at(4)) ? qBound(0.0, result.at(4), 1.0) : 1.0));
        break;
    case QColor::Hsl:
        c.setHslF((FP::defined(result.at(0)) ? qBound(0.0, result.at(0), 1.0) : 0),
                  (FP::defined(result.at(1)) ? qBound(0.0, result.at(1), 1.0) : 0),
                  (FP::defined(result.at(2)) ? qBound(0.0, result.at(2), 1.0) : 0),
                  (FP::defined(result.at(3)) ? qBound(0.0, result.at(3), 1.0) : 1.0));
        break;
    default:
        c.setRgbF((FP::defined(result.at(0)) ? qBound(0.0, result.at(0), 1.0) : 0),
                  (FP::defined(result.at(1)) ? qBound(0.0, result.at(1), 1.0) : 0),
                  (FP::defined(result.at(2)) ? qBound(0.0, result.at(2), 1.0) : 0),
                  (FP::defined(result.at(3)) ? qBound(0.0, result.at(3), 1.0) : 1.0));
        break;
    }
    return c;
}

TraceSymbol::TraceSymbol() : type(Unselected), size(1.0), lastDimension(-1.0), path()
{ }

TraceSymbol::TraceSymbol(const TraceSymbol &other) = default;

TraceSymbol &TraceSymbol::operator=(const TraceSymbol &other) = default;

TraceSymbol::TraceSymbol(TraceSymbol &&other) = default;

TraceSymbol &TraceSymbol::operator=(TraceSymbol &&other) = default;

/**
 * Create a symbol from the given configuration.
 * 
 * @param configuration the configuration to use
 */
TraceSymbol::TraceSymbol(const Variant::Read &configuration) : type(Unselected), lastDimension(0.0)
{
    double checkSize = configuration["Scale"].toDouble();
    if (FP::defined(checkSize) && checkSize <= 0.0) {
        size = 1;
    } else if (FP::defined(configuration["Size"].toDouble()) &&
            configuration["Size"].toDouble() > 0.0) {
        size = -configuration["Size"].toDouble();
    } else if (FP::defined(checkSize)) {
        size = checkSize;
    } else {
        size = 1;
    }

    if (configuration["Type"].exists()) {
        const auto &t = configuration["Type"].toString();
        if (Util::equal_insensitive(t, "circle"))
            type = Circle;
        else if (Util::equal_insensitive(t, "square"))
            type = Square;
        else if (Util::equal_insensitive(t, "diamond"))
            type = Diamond;
        else if (Util::equal_insensitive(t, "triangleup", "triangle"))
            type = TriangleUp;
        else if (Util::equal_insensitive(t, "triangledown"))
            type = TriangleDown;
        else if (Util::equal_insensitive(t, "circlecrossx", "circlecross", "crosscircle"))
            type = CircleCrossX;
        else if (Util::equal_insensitive(t, "squarecrossx", "squarecross", "crosssquare"))
            type = SquareCrossX;
        else if (Util::equal_insensitive(t, "diamondcrossplus", "diamondcross", "crossdiamond"))
            type = DiamondCrossPlus;
        else if (Util::equal_insensitive(t, "triangleupcross", "trianglecross", "crosstriangleup",
                                         "crosstriangle"))
            type = TriangleUpCross;
        else if (Util::equal_insensitive(t, "triangledowncross", "crosstriangledown"))
            type = TriangleDownCross;
        else if (Util::equal_insensitive(t, "circlefilled", "fillcircle"))
            type = CircleFilled;
        else if (Util::equal_insensitive(t, "squarefilled", "fillsquare"))
            type = SquareFilled;
        else if (Util::equal_insensitive(t, "diamondfilled", "filldiamond"))
            type = DiamondFilled;
        else if (Util::equal_insensitive(t, "triangleupfilled", "filltriangleup", "trianglefilled",
                                         "filltriangle"))
            type = TriangleUpFilled;
        else if (Util::equal_insensitive(t, "triangledownfilled", "filltriangledown"))
            type = TriangleDownFilled;
        else if (Util::equal_insensitive(t, "x", "simplex"))
            type = SimpleX;
        else if (Util::equal_insensitive(t, "plus", "simpleplus", "+"))
            type = SimplePlus;
        else if (Util::equal_insensitive(t, "circlecrossplus", "crosscircleplus"))
            type = CircleCrossPlus;
        else if (Util::equal_insensitive(t, "diamondcrossx", "crossdiamondx"))
            type = DiamondCrossX;
        else if (Util::equal_insensitive(t, "squarecrossplus", "crosssquareplus"))
            type = SquareCrossPlus;
        else if (Util::equal_insensitive(t, "triangleleft"))
            type = TriangleLeft;
        else if (Util::equal_insensitive(t, "triangleright"))
            type = TriangleRight;
        else if (Util::equal_insensitive(t, "triangleleftcross", "crosstriangleleft"))
            type = TriangleLeftCross;
        else if (Util::equal_insensitive(t, "trianglerightcross", "crosstriangleright"))
            type = TriangleRightCross;
        else if (Util::equal_insensitive(t, "triangleleftfilled", "filltriangleleft"))
            type = TriangleLeftFilled;
        else if (Util::equal_insensitive(t, "trianglerightfilled", "filltriangleright"))
            type = TriangleRightFilled;
    }
}

/**
 * Save the symbol configuration to a Value.
 * 
 * @return the configuration
 */
Variant::Root TraceSymbol::toConfiguration() const
{
    Variant::Root configuration;

    if (size < 0.0) {
        configuration["Size"].setDouble(-size);
    } else {
        configuration["Scale"].setDouble(size);
    }

    switch (type) {
    case Circle:
        configuration["Type"].setString("Circle");
        break;
    case Square:
        configuration["Type"].setString("Square");
        break;
    case Diamond:
        configuration["Type"].setString("Square");
        break;
    case TriangleUp:
        configuration["Type"].setString("TriangleUp");
        break;
    case TriangleDown:
        configuration["Type"].setString("TriangleDown");
        break;
    case CircleCrossX:
        configuration["Type"].setString("CircleCrossX");
        break;
    case SquareCrossX:
        configuration["Type"].setString("SquareCrossX");
        break;
    case DiamondCrossPlus:
        configuration["Type"].setString("DiamongCrossPlus");
        break;
    case TriangleUpCross:
        configuration["Type"].setString("TriangleUpCross");
        break;
    case TriangleDownCross:
        configuration["Type"].setString("TriangleDownCross");
        break;
    case CircleFilled:
        configuration["Type"].setString("CircleFilled");
        break;
    case SquareFilled:
        configuration["Type"].setString("SquareFilled");
        break;
    case DiamondFilled:
        configuration["Type"].setString("DiamondFilled");
        break;
    case TriangleUpFilled:
        configuration["Type"].setString("TriangleUpFilled");
        break;
    case TriangleDownFilled:
        configuration["Type"].setString("TriangleDownFilled");
        break;
    case SimpleX:
        configuration["Type"].setString("X");
        break;
    case SimplePlus:
        configuration["Type"].setString("Plus");
        break;
    case CircleCrossPlus:
        configuration["Type"].setString("CircleCrossPlus");
        break;
    case DiamondCrossX:
        configuration["Type"].setString("DiamondCrossX");
        break;
    case SquareCrossPlus:
        configuration["Type"].setString("SquareCrossPlus");
        break;

    case TriangleLeft:
        configuration["Type"].setString("TriangleLeft");
        break;
    case TriangleRight:
        configuration["Type"].setString("TriangleRight");
        break;
    case TriangleLeftCross:
        configuration["Type"].setString("TriangleLeftCross");
        break;
    case TriangleRightCross:
        configuration["Type"].setString("TriangleRightCross");
        break;
    case TriangleLeftFilled:
        configuration["Type"].setString("TriangleLeftFilled");
        break;
    case TriangleRightFilled:
        configuration["Type"].setString("TriangleRightFilled");
        break;
    case Unselected:
        configuration["Type"].setEmpty();
        break;
    }
    return configuration;
}

/**
 * Create a symbol of the given type with the given size.  If the size is
 * specified as a negative value, it is treated as a device dependent absolute
 * size.  Otherwise it is a scalar based off the font size of the device.
 * 
 * @param setType   the symbol type
 * @param setSize   the symbol size
 */
TraceSymbol::TraceSymbol(Type setType, double setSize) : type(setType),
                                                         size(setSize),
                                                         lastDimension(0.0)
{
}

/**
 * Get the symbol type.
 * 
 * @return  the symbol type
 */
TraceSymbol::Type TraceSymbol::getType() const
{ return type; }

/**
 * Set the symbol type.
 * 
 * @param t the symbol type
 */
void TraceSymbol::setType(Type t)
{ type = t; }

/**
 * Select a symbol type, if this symbol is currently unselected.  This also
 * inserts the type into the used list.
 * 
 * @param usedTypes the symbol types that are already in use
 */
void TraceSymbol::selectTypeIfNeeded(std::unordered_set<Type, TypeHash> &usedTypes)
{
    if (type != Unselected)
        return;
    for (std::size_t i = 0; i < Unselected; i++) {
        if (usedTypes.count(static_cast<Type>(i)))
            continue;
        type = static_cast<Type>(i);
        usedTypes.insert(type);
        lastDimension = -1.0;
        return;
    }
    type = Circle;
    usedTypes.insert(Circle);
    lastDimension = -1.0;
}

/** 
 * Predict the size of the symbol.  This only predicts the width and height of
 * it.  Symbols are drawn centered, so the actual affected points are +/-
 * half the width/height.
 * 
 * @param paintdevice   the paint device that will be used, or NULL for
 *                      screen space
 * @return              the size of the symbol
 */
QSizeF TraceSymbol::predictSize(QPaintDevice *paintdevice) const
{
    qreal dimension;
    if (size > 0.0) {
        QFontMetrics fm(QApplication::font());
        if (paintdevice != NULL)
            fm = QFontMetrics(QApplication::font(), paintdevice);
        dimension = fm.xHeight() * symbolBaseSize * size;
    } else {
        dimension = -size;
    }
    return QSizeF(dimension * 2, dimension * 2);
}

void TraceSymbol::paint(QPainter *painter, const QPainterPath &paintPath) const
{
    switch (type) {
    case Circle:
    case Square:
    case Diamond:
    case TriangleUp:
    case TriangleDown:
    case TriangleLeft:
    case TriangleRight:
        painter->drawLine(0, 0, 0, 0);
    case CircleCrossX:
    case SquareCrossX:
    case DiamondCrossPlus:
    case TriangleUpCross:
    case TriangleDownCross:
    case SimpleX:
    case SimplePlus:
    case CircleCrossPlus:
    case DiamondCrossX:
    case SquareCrossPlus:
    case TriangleLeftCross:
    case TriangleRightCross:
        painter->strokePath(paintPath, painter->pen());
        break;

    case CircleFilled:
    case SquareFilled:
    case DiamondFilled:
    case TriangleUpFilled:
    case TriangleDownFilled:
    case TriangleLeftFilled:
    case TriangleRightFilled:
        painter->drawPath(paintPath);
        break;

    case Unselected:
        break;
    }
}

/**
 * Draw the symbol at the current position.  This usually requires translation
 * to the desired location.  This uses the current pen and/or brush to draw
 * the symbol with.
 * 
 * @param painter   the painter to draw with
 */
void TraceSymbol::paint(QPainter *painter)
{
    if (size > 0.0) {
        QFontMetrics fm(QApplication::font(), painter->device());
        qreal dimension = fm.xHeight() * symbolBaseSize * size;
        if (lastDimension != dimension) {
            lastDimension = dimension;
            path = buildPath(dimension);
        }
    } else {
        if (lastDimension != -size) {
            lastDimension = -size;
            path = buildPath(lastDimension);
        }
    }

    paint(painter, path);
}

/**
 * Draw the symbol at the given location.
 * 
 * @param x         the x position
 * @param y         the y position
 * @param painter   the painter to draw with
 * @see paint(QPainter *)
 */
void TraceSymbol::paint(QPainter *painter, qreal x, qreal y)
{
    painter->save();
    painter->translate(x, y);
    paint(painter);
    painter->restore();
}

/** @see paint( QPainter * ) */
void TraceSymbol::paint(QPainter *painter) const
{
    qreal dimension;

    if (size > 0.0) {
        QFontMetrics fm(QApplication::font(), painter->device());
        dimension = fm.xHeight() * symbolBaseSize * size;
    } else {
        dimension = -size;
    }

    if (lastDimension == dimension) {
        paint(painter, path);
        return;
    }

    paint(painter, buildPath(dimension));
}

/** @see paint(QPainter *, qreal, qreal) */
void TraceSymbol::paint(QPainter *painter, qreal x, qreal y) const
{
    painter->save();
    painter->translate(x, y);
    paint(painter);
    painter->restore();
}

static void buildCenteredPathPoly(QPainterPath &path, int vertexCount, qreal rotation, qreal radius)
{
    double rInc = (3.14159265358979323846 * 2.0) / (qreal) vertexCount;
    for (int i = 0; i < vertexCount; i++, rotation += rInc) {
        double xc = cos(rotation) * radius;
        double yc = sin(rotation) * (-radius);
        if (i == 0)
            path.moveTo(xc, yc);
        else
            path.lineTo(xc, yc);
    }
    path.closeSubpath();
}

static void buildCenteredPathVertexCross(QPainterPath &path,
                                         int vertexCount,
                                         qreal rotation,
                                         qreal radius)
{
    qreal rInc = (3.14159265358979323846 * 2.0) / (qreal) vertexCount;
    for (int i = 0; i < vertexCount; i++, rotation += rInc) {
        double xc = cos(rotation) * radius;
        double yc = sin(rotation) * (-radius);
        path.moveTo(xc, yc);
        path.lineTo(0, 0);
    }
}

QPainterPath TraceSymbol::buildPath(qreal dimension) const
{
    QPainterPath result = QPainterPath();

    switch (type) {
    case Circle:
    case CircleFilled:
        result.addEllipse(-dimension + 0.01, -dimension + 0.01, dimension * 2.0, dimension * 2.0);
        break;

    case CircleCrossPlus:
        result.addEllipse(-dimension + 0.01, -dimension + 0.01, dimension * 2.0, dimension * 2.0);
        result.moveTo(-dimension, 0);
        result.lineTo(dimension, 0);
        result.moveTo(0, -dimension);
        result.lineTo(0, dimension);
        break;

    case CircleCrossX:
        result.addEllipse(-dimension + 0.01, -dimension + 0.01, dimension * 2.0, dimension * 2.0);
        result.moveTo(-dimension * 0.707106781186, -dimension * 0.707106781186);
        result.lineTo(dimension * 0.707106781186, dimension * 0.707106781186);
        result.moveTo(dimension * 0.707106781186, -dimension * 0.707106781186);
        result.lineTo(-dimension * 0.707106781186, dimension * 0.707106781186);
        break;

    case Square:
    case SquareFilled:
        result.addRect(-dimension, -dimension, dimension * 2.0, dimension * 2.0);
        break;

    case SquareCrossX:
        result.addRect(-dimension, -dimension, dimension * 2.0, dimension * 2.0);
        result.moveTo(-dimension, -dimension);
        result.lineTo(dimension, dimension);
        result.moveTo(dimension, -dimension);
        result.lineTo(-dimension, dimension);
        break;

    case SquareCrossPlus:
        result.addRect(-dimension, -dimension, dimension * 2.0, dimension * 2.0);
        result.moveTo(-dimension, 0);
        result.lineTo(dimension, 0);
        result.moveTo(0, -dimension);
        result.lineTo(0, dimension);
        break;

    case Diamond:
    case DiamondFilled:
        result.moveTo(-dimension, 0);
        result.lineTo(0, dimension);
        result.lineTo(dimension, 0);
        result.lineTo(0, -dimension);
        result.closeSubpath();
        break;

    case DiamondCrossPlus:
        result.moveTo(-dimension, 0);
        result.lineTo(0, dimension);
        result.lineTo(dimension, 0);
        result.lineTo(0, -dimension);
        result.closeSubpath();
        result.moveTo(-dimension, 0);
        result.lineTo(dimension, 0);
        result.moveTo(0, -dimension);
        result.lineTo(0, dimension);
        break;

    case DiamondCrossX:
        result.moveTo(-dimension, 0);
        result.lineTo(0, dimension);
        result.lineTo(dimension, 0);
        result.lineTo(0, -dimension);
        result.closeSubpath();
        result.moveTo(-dimension * 0.5, -dimension * 0.5);
        result.lineTo(dimension * 0.5, dimension * 0.5);
        result.moveTo(dimension * 0.5, -dimension * 0.5);
        result.lineTo(-dimension * 0.5, dimension * 0.5);
        break;

    case TriangleUp:
    case TriangleUpFilled:
        buildCenteredPathPoly(result, 3, 1.57079632679489661, dimension);
        break;

    case TriangleUpCross:
        buildCenteredPathPoly(result, 3, 1.57079632679489661, dimension);
        buildCenteredPathVertexCross(result, 3, 1.57079632679489661, dimension);
        break;

    case TriangleDown:
    case TriangleDownFilled:
        buildCenteredPathPoly(result, 3, -1.57079632679489661, dimension);
        break;

    case TriangleDownCross:
        buildCenteredPathPoly(result, 3, -1.57079632679489661, dimension);
        buildCenteredPathVertexCross(result, 3, -1.57079632679489661, dimension);
        break;

    case TriangleLeft:
    case TriangleLeftFilled:
        buildCenteredPathPoly(result, 3, 3.14159265358979323, dimension);
        break;

    case TriangleLeftCross:
        buildCenteredPathPoly(result, 3, 3.14159265358979323, dimension);
        buildCenteredPathVertexCross(result, 3, 3.14159265358979323, dimension);
        break;

    case TriangleRight:
    case TriangleRightFilled:
        buildCenteredPathPoly(result, 3, 0.0, dimension);
        break;

    case TriangleRightCross:
        buildCenteredPathPoly(result, 3, 0.0, dimension);
        buildCenteredPathVertexCross(result, 3, 0.0, dimension);
        break;

    case SimpleX:
        result.moveTo(-dimension, -dimension);
        result.lineTo(dimension, dimension);
        result.moveTo(dimension, -dimension);
        result.lineTo(-dimension, dimension);
        break;

    case SimplePlus:
        result.moveTo(-dimension, 0);
        result.lineTo(dimension, 0);
        result.moveTo(0, -dimension);
        result.lineTo(0, dimension);
        break;

    case Unselected:
        break;
    }

    return result;
}

/**
 * Return a menu used to customize the symbol.
 *
 * @param update    the callback when the symbol is updated
 * @param parent    the parent of the menu and dialogs
 * @return          a menu used to customize the symbol
 */
QMenu *TraceSymbol::customizeMenu(const std::function<void(const TraceSymbol &)> &update,
                                  QWidget *parent) const
{
    QAction *act;
    QActionGroup *group;
    QMenu *menu;

    TraceSymbol output(*this);

    menu = new QMenu(QObject::tr("&Symbol", "Context|SetTraceSymbol"), parent);

    act = menu->addAction(QObject::tr("Si&ze", "Context|TraceSymbolSize"));
    act->setToolTip(QObject::tr("Change the size of the symbol."));
    act->setStatusTip(QObject::tr("Set the symbol size"));
    act->setWhatsThis(QObject::tr("Set the size or scale of the symbol."));
    QObject::connect(act, &QAction::triggered, parent, [update, output, parent] {
        QDialog dialog(parent);
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(QObject::tr("Set Axis Size"));

        auto layout = new QVBoxLayout(&dialog);
        dialog.setLayout(layout);

        auto mode = new QComboBox(&dialog);
        layout->addWidget(mode);
        auto scale = new QDoubleSpinBox(&dialog);
        layout->addWidget(scale);

        auto configureScale = [scale, mode]() {
            if (mode->currentIndex() == 1) {
                scale->setDecimals(0);
                scale->setRange(1, 1000.0);
                scale->setSingleStep(2);
            } else {
                scale->setDecimals(2);
                scale->setRange(0.01, 100.0);
                scale->setSingleStep(0.1);
            }
        };

        mode->setToolTip(QObject::tr("Select the symbol scaling mode."));
        mode->setStatusTip(QObject::tr("Scaling mode"));
        mode->setWhatsThis(QObject::tr("Select the scaling mode used for the symbol."));
        mode->addItem(QObject::tr("Scale factor"));
        mode->addItem(QObject::tr("Exact pixel size"));
        mode->setCurrentIndex(output.size < 0 ? 1 : 0);
        QObject::connect(mode,
                         static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
                         scale, configureScale);

        scale->setToolTip(QObject::tr("Scale factor or pixel size."));
        scale->setStatusTip(QObject::tr("Symbol size"));
        scale->setWhatsThis(
                QObject::tr("The scale factor fraction or the absolute size in pixels."));
        configureScale();
        scale->setValue(std::fabs(output.size));

        layout->addStretch(1);

        auto buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                                            Qt::Horizontal, &dialog);
        layout->addWidget(buttons);
        QObject::connect(buttons, &QDialogButtonBox::accepted, &dialog, &QDialog::accept);
        QObject::connect(buttons, &QDialogButtonBox::rejected, &dialog, &QDialog::reject);

        if (dialog.exec() != QDialog::Accepted)
            return;
        auto result = output;
        result.size = scale->value();
        if (mode->currentIndex() == 1)
            result.size = -result.size;
        update(result);
    });

    menu->addSeparator();

    group = new QActionGroup(menu);

    class SymbolEngine : public QIconEngine {
        TraceSymbol symbol;
    public:
        explicit SymbolEngine(TraceSymbol::Type type)
        { symbol.type = type; }

        virtual ~SymbolEngine() = default;

        QIconEngine *clone() const override
        { return new SymbolEngine(symbol.getType()); }

        void paint(QPainter *painter,
                   const QRect &rect,
                   QIcon::Mode mode,
                   QIcon::State state) override
        {
            painter->save();

            painter->translate(
                    QPoint(rect.topLeft()) + QPoint(rect.width() / 2, rect.height() / 2));

            auto size = std::min(rect.width(), rect.height());
            size /= 2;
            --size;
            if (size < 1)
                size = 1;
            symbol.size = -size;

            auto color = QApplication::palette().text().color();
            painter->setBrush(QBrush(color));
            painter->setPen(color);
            symbol.paint(painter);

            painter->restore();
        }

        QPixmap pixmap(const QSize &size, QIcon::Mode mode, QIcon::State state) override
        {
            QImage img(size, QImage::Format_ARGB32);
            img.fill(0x00FFFFFF);
            QPixmap result = QPixmap::fromImage(img, Qt::NoFormatConversion);
            {
                QPainter painter(&result);
                this->paint(&painter, QRect(QPoint(0, 0), size), mode, state);
            }
            return result;
        }
    };

    act = menu->addAction(QObject::QObject::tr("&Automatic", "Context|TraceSymbolAutomatic"));
    act->setToolTip(QObject::tr("Automatically select the symbol to use."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr("Set the trace to automatically select an unused symbol."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::Unselected);
    act->setActionGroup(group);
    output.setType(TraceSymbol::Unselected);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("&Circle", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A simple circle with a dot in the center."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr("Set the symbol to a simple circle with a dot in the center."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::Circle);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::Circle)));
    output.setType(TraceSymbol::Circle);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("&Square", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A simple square with a dot in the center."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr("Set the symbol to a simple square with a dot in the center."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::Square);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::Square)));
    output.setType(TraceSymbol::Square);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("&Diamond", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A simple diamond with a dot in the center."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr("Set the symbol to a simple diamond with a dot in the center."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::Diamond);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::Diamond)));
    output.setType(TraceSymbol::Diamond);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("&Triangle (up)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A simple triangle pointing upwards with a dot in the center."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr(
            "Set the symbol to a simple triangle pointing upwards with a dot in the center."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::TriangleUp);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::TriangleUp)));
    output.setType(TraceSymbol::TriangleUp);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("T&riangle (down)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A simple triangle pointing downwards with a dot in the center."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr(
            "Set the symbol to a simple triangle pointing downwards with a dot in the center."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::TriangleDown);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::TriangleDown)));
    output.setType(TraceSymbol::TriangleDown);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Crossed circle", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A circle with lines crossing to form an \"X\"."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(
            QObject::tr("Set the symbol to a circle with lines crossing to form an \"X\"."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::CircleCrossX);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::CircleCrossX)));
    output.setType(TraceSymbol::CircleCrossX);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Crossed square", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A square with lines crossing to form an \"X\"."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(
            QObject::tr("Set the symbol to a square with lines crossing to form an \"X\"."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::SquareCrossX);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::SquareCrossX)));
    output.setType(TraceSymbol::SquareCrossX);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Crossed diamond", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A diamond with lines crossing to form an \"+\"."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(
            QObject::tr("Set the symbol to a diamond with lines crossing to form an \"+\"."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::DiamondCrossPlus);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::DiamondCrossPlus)));
    output.setType(TraceSymbol::DiamondCrossPlus);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Crossed triangle (up)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A triangle pointing up with lines from each vertex crossing."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr(
            "Set the symbol to a triangle pointing up with lines from each vertex crossing."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::TriangleUpCross);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::TriangleUpCross)));
    output.setType(TraceSymbol::TriangleUpCross);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Crossed triangle (down)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A triangle pointing down with lines from each vertex crossing."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr(
            "Set the symbol to a triangle pointing down with lines from each vertex crossing."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::TriangleDownCross);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::TriangleDownCross)));
    output.setType(TraceSymbol::TriangleDownCross);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Filled circle", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A filled circle."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr("Set the symbol to a filled circle."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::CircleFilled);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::CircleFilled)));
    output.setType(TraceSymbol::CircleFilled);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Filled square", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A filled square."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr("Set the symbol to a filled square."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::SquareFilled);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::SquareFilled)));
    output.setType(TraceSymbol::SquareFilled);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Filled diamond", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A filled diamond."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr("Set the symbol to a filled diamond."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::DiamondFilled);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::DiamondFilled)));
    output.setType(TraceSymbol::DiamondFilled);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Filled triangle (up)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A triangle pointing up, filled."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr("Set the symbol to a triangle pointing up, filled."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::TriangleUpFilled);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::TriangleUpFilled)));
    output.setType(TraceSymbol::TriangleUpFilled);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Filled triangle (down)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A triangle pointing down, filled."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr("Set the symbol to a triangle pointing down, filled."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::TriangleDownFilled);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::TriangleDownFilled)));
    output.setType(TraceSymbol::TriangleDownFilled);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Diagonal cross", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("Two diagonal lines."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr("Set the symbol to two intersecting diagonal lines."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::SimpleX);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::SimpleX)));
    output.setType(TraceSymbol::SimpleX);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Vertical cross", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("Intersecting vertical and horizontal lines."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(
            QObject::tr("Set the symbol to an intersecting horizontal and vertical line."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::SimplePlus);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::SimplePlus)));
    output.setType(TraceSymbol::SimplePlus);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Crossed circle (plus)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A circle crossed vertical and horizontally."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr(
            "Set the symbol to a circle with intersecting horizontal and vertical lines through the center."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::CircleCrossPlus);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::CircleCrossPlus)));
    output.setType(TraceSymbol::CircleCrossPlus);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Crossed diamond (X)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A diamond crossed with diagonal lines."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(
            QObject::tr("Set the symbol to a diamond with diagonal lines through the center."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::DiamondCrossX);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::DiamondCrossX)));
    output.setType(TraceSymbol::DiamondCrossX);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Crossed square (plus)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A square crossed vertical and horizontally."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr(
            "Set the symbol to a square with intersecting horizontal and vertical lines through the center."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::SquareCrossPlus);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::SquareCrossPlus)));
    output.setType(TraceSymbol::SquareCrossPlus);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Triangle (left)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A simple triangle pointing left with a dot in the center."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr(
            "Set the symbol to a simple triangle pointing left with a dot in the center."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::TriangleLeft);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::TriangleLeft)));
    output.setType(TraceSymbol::TriangleLeft);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Triangle (right)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A simple triangle pointing right with a dot in the center."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr(
            "Set the symbol to a simple triangle pointing right with a dot in the center."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::TriangleRight);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::TriangleRight)));
    output.setType(TraceSymbol::TriangleRight);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Crossed triangle (left)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A triangle pointing left with lines from each vertex crossing."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr(
            "Set the symbol to a triangle pointing left with lines from each vertex crossing."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::TriangleLeftCross);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::TriangleLeftCross)));
    output.setType(TraceSymbol::TriangleLeftCross);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Crossed triangle (right)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A triangle pointing right with lines from each vertex crossing."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr(
            "Set the symbol to a triangle pointing right with lines from each vertex crossing."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::TriangleRightCross);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::TriangleRightCross)));
    output.setType(TraceSymbol::TriangleRightCross);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Filled triangle (left)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A triangle pointing left, filled."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr("Set the symbol to a triangle pointing left, filled."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::TriangleLeftFilled);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::TriangleLeftFilled)));
    output.setType(TraceSymbol::TriangleLeftFilled);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    act = menu->addAction(QObject::tr("Filled triangle (right)", "Context|TraceSymbolSelect"));
    act->setToolTip(QObject::tr("A triangle pointing right, filled."));
    act->setStatusTip(QObject::tr("Set trace symbol"));
    act->setWhatsThis(QObject::tr("Set the symbol to a triangle pointing right, filled."));
    act->setCheckable(true);
    act->setChecked(type == TraceSymbol::TriangleRightFilled);
    act->setActionGroup(group);
    act->setIcon(QIcon(new SymbolEngine(TraceSymbol::TriangleRightFilled)));
    output.setType(TraceSymbol::TriangleRightFilled);
    QObject::connect(act, &QAction::triggered, parent, std::bind(update, output));

    return menu;
}

}
}
