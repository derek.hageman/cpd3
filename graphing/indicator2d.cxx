/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>
#include <QColorDialog>

#include "graphing/indicator2d.hxx"
#include "graphing/graphpainters2d.hxx"

using namespace CPD3::Data;
using namespace CPD3::Graphing::Internal;

namespace CPD3 {
namespace Graphing {

/** @file graphing/indicator.hxx
 * The handlers for indicators on a two dimensional graph.
 */


IndicatorParameters2D::IndicatorParameters2D() : components(0),
                                                 side(Side_Auto),
                                                 indicator(),
                                                 enable(true),
                                                 color(0, 0, 0),
                                                 legendText(),
                                                 drawPriority(
                                                         Graph2DDrawPrimitive::Priority_Indicator),
                                                 binding()
{ }

IndicatorParameters2D::~IndicatorParameters2D() = default;

IndicatorParameters2D::IndicatorParameters2D(const IndicatorParameters2D &) = default;

IndicatorParameters2D &IndicatorParameters2D::operator=(const IndicatorParameters2D &) = default;

IndicatorParameters2D::IndicatorParameters2D(IndicatorParameters2D &&) = default;

IndicatorParameters2D &IndicatorParameters2D::operator=(IndicatorParameters2D &&) = default;

void IndicatorParameters2D::copy(const TraceParameters *from)
{ *this = *(static_cast<const IndicatorParameters2D *>(from)); }

void IndicatorParameters2D::clear()
{
    IndicatorParameters::clear();
    components = 0;
}

IndicatorParameters2D::IndicatorParameters2D(const Variant::Read &value) : IndicatorParameters(
        value),
                                                                           components(0),
                                                                           side(Side_Auto),
                                                                           indicator(
                                                                                   value["Indicator"]),
                                                                           enable(true),
                                                                           color(0, 0, 0),
                                                                           legendText(),
                                                                           drawPriority(
                                                                                   Graph2DDrawPrimitive::Priority_Indicator),
                                                                           binding()
{
    if (value["Side"].exists()) {
        const auto &s = value["Side"].toString();
        if (Util::equal_insensitive(s, "auto", "automatic")) {
            components |= Component_Side;
            side = Side_Auto;
        } else if (Util::equal_insensitive(s, "primary", "left", "bottom")) {
            components |= Component_Side;
            side = Side_Primary;
        } else if (Util::equal_insensitive(s, "secondary", "right", "top")) {
            components |= Component_Side;
            side = Side_Secondary;
        }
    }

    if (value["Indicator"].exists()) {
        components |= Component_AxisIndicator;
    }

    if (value["Enable"].exists()) {
        components |= Component_Enable;
        enable = value["Enable"].toBool();
    }

    if (value["Color"].exists()) {
        components |= Component_Color;
        color = Display::parseColor(value["Color"], color);
    }

    if (value["Legend/Text"].exists()) {
        components |= Component_LegendText;
        legendText = value["Legend/Text"].toDisplayString();
    }

    if (INTEGER::defined(value["DrawPriority"].toInt64())) {
        components |= Component_DrawPriority;
        drawPriority = (int) value["DrawPriority"].toInt64();
    }

    if (value["Axis"].exists()) {
        components |= Component_Binding;
        binding = value["Axis"].toQString();
    }
}

void IndicatorParameters2D::save(Variant::Write &value) const
{
    IndicatorParameters::save(value);

    if (components & Component_Side) {
        switch (side) {
        case Side_Auto:
            value["Side"].setString("Automatic");
            break;
        case Side_Primary:
            value["Side"].setString("Primary");
            break;
        case Side_Secondary:
            value["Side"].setString("Secondary");
            break;
        }
    }

    if (components & Component_AxisIndicator) {
        value["Indicator"].set(indicator.toConfiguration());
    }

    if (components & Component_Enable) {
        value["Enable"].setBool(enable);
    }

    if (components & Component_Color) {
        value["Color"].set(Display::formatColor(color));
    }

    if (components & Component_LegendText) {
        value["Legend/Text"].setString(legendText);
    }

    if (components & Component_DrawPriority) {
        value["DrawPriority"].setInt64(drawPriority);
    }

    if (components & Component_Binding) {
        value["Axis"].setString(binding);
    }
}

std::unique_ptr<TraceParameters> IndicatorParameters2D::clone() const
{ return std::unique_ptr<TraceParameters>(new IndicatorParameters2D(*this)); }

void IndicatorParameters2D::overlay(const TraceParameters *over)
{
    IndicatorParameters::overlay(over);

    const IndicatorParameters2D *top = static_cast<const IndicatorParameters2D *>(over);

    if (top->components & Component_Side) {
        components |= Component_Side;
        side = top->side;
    }

    if (top->components & Component_AxisIndicator) {
        components |= Component_AxisIndicator;
        indicator = top->indicator;
    }

    if (top->components & Component_Enable) {
        components |= Component_Enable;
        enable = top->enable;
    }

    if (top->components & Component_Color) {
        components |= Component_Color;
        color = top->color;
    }

    if (top->components & Component_LegendText) {
        components |= Component_LegendText;
        legendText = top->legendText;
    }

    if (top->components & Component_DrawPriority) {
        components |= Component_DrawPriority;
        drawPriority = top->drawPriority;
    }

    if (top->components & Component_Binding) {
        components |= Component_Binding;
        binding = top->binding;
    }
}


IndicatorHandler2D::IndicatorHandler2D(std::unique_ptr<IndicatorParameters2D> &&params)
        : IndicatorValueHandler<1>(std::move(params)),
          parameters(static_cast<IndicatorParameters2D *>(getParameters()))
{ }

IndicatorHandler2D::~IndicatorHandler2D() = default;

IndicatorHandler2D::IndicatorHandler2D(const IndicatorHandler2D &other,
                                       std::unique_ptr<TraceParameters> &&params)
        : IndicatorValueHandler<1>(other, std::move(params)),
          parameters(static_cast<IndicatorParameters2D *>(getParameters()))
{ }

IndicatorHandler2D::Cloned IndicatorHandler2D::clone() const
{ return Cloned(new IndicatorHandler2D(*this, parameters->clone())); }

/**
 * Create the drawers for the indicator.
 * 
 * @param transformer   the transformer to use
 * @param primary       the primary (left/bottom) baseline
 * @param secondary     the secondary (right/top) baseline
 * @param side          the axis side
 */
std::vector<std::unique_ptr<
        Graph2DDrawPrimitive>> IndicatorHandler2D::createDraw(const AxisTransformer &transformer,
                                                              qreal primary,
                                                              qreal secondary,
                                                              Axis2DSide side) const
{
    if (!parameters->getEnable())
        return {};
    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> result;

    qreal totalDepth = primary - secondary;
    if (totalDepth < 0)
        totalDepth = -totalDepth;

    if (side == Axis2DRight || side == Axis2DTop)
        primary = secondary;

    result.emplace_back(new GraphPainter2DIndicator(getPoints(), transformer, primary, totalDepth,
                                                    AxisIndicator2D(parameters->getAxisIndicator(),
                                                                    side), parameters->getColor(),
                                                    parameters->getContinuity(), (quintptr) this,
                                                    parameters->getDrawPriority()));
    return std::move(result);
}

/**
 * Insert all used display types into the given set of types.
 * 
 * @param types the used indicator types
 */
void IndicatorHandler2D::insertUsedTypes(std::unordered_set<AxisIndicator2D::Type,
                                                            AxisIndicator2D::TypeHash> &types) const
{
    if (!parameters->getEnable())
        return;
    if (!FP::defined(getLimits().min[0]))
        return;
    AxisIndicator2D::Type t = parameters->getAxisIndicator().getType();
    if (t == AxisIndicator2D::Unselected)
        return;
    if (!parameters->hasAxisIndicator())
        return;
    types.insert(t);
}

/**
 * Bind a display type for the indicator, if needed.
 * 
 * @param types the types of indicators in use
 */
void IndicatorHandler2D::bindType(std::unordered_set<AxisIndicator2D::Type,
                                                     AxisIndicator2D::TypeHash> &types) const
{
    if (!parameters->getEnable())
        return;
    if (!FP::defined(getLimits().min[0]))
        return;
    AxisIndicator2D::Type t = parameters->getAxisIndicatorReference().getType();
    if (t == AxisIndicator2D::Unselected) {
        parameters->getAxisIndicatorReference().selectTypeIfNeeded(types);
    } else if (!parameters->hasAxisIndicator()) {
        if (!types.count(t)) {
            types.insert(t);
            return;
        }
        parameters->getAxisIndicatorReference().setType(AxisIndicator2D::Unselected);
        parameters->getAxisIndicatorReference().selectTypeIfNeeded(types);
    }
}

namespace Internal {

IndicatorOutput2D::IndicatorOutput2D(IndicatorHandler2D *setHandler, const QString &setTitle)
        : handler(setHandler), title(setTitle)
{ }

IndicatorOutput2D::~IndicatorOutput2D() = default;

QString IndicatorOutput2D::getTitle() const
{ return title; }

QList<QAction *> IndicatorOutput2D::getMenuActions(QWidget *parent)
{
    if (handler.isNull())
        return {};
    QList<QAction *> result;

    QAction *act;

    act = new QAction(tr("&Enable", "Context|Enable"), this);
    act->setToolTip(tr("Enable or disable indicator."));
    act->setStatusTip(tr("Enable the indicator"));
    act->setWhatsThis(
            tr("This enables or disables the the display of the indicator around the edge of the graph."));
    act->setCheckable(true);
    act->setChecked(handler->parameters->getEnable());
    QObject::connect(act, &QAction::toggled, this, [this](bool enable) {
        if (handler.isNull())
            return;
        static_cast<IndicatorParameters2D *>(handler->overrideParameters())->overrideEnable(enable);
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    act = new QAction(tr("&Color", "Context|SetIndicatorColor"), this);
    act->setToolTip(tr("Change the color of the indicator."));
    act->setStatusTip(tr("Set the indicator color"));
    act->setWhatsThis(tr("Set the color of the indicator."));
    QObject::connect(act, &QAction::triggered, this, [this, parent] {
        if (handler.isNull())
            return;

        QColorDialog dialog(handler->parameters->getColor(), parent);
        dialog.setWindowModality(Qt::ApplicationModal);
        dialog.setWindowTitle(tr("Set Indicator Color"));
        dialog.setOption(QColorDialog::ShowAlphaChannel);
        if (dialog.exec() != QDialog::Accepted)
            return;
        static_cast<IndicatorParameters2D *>(handler->overrideParameters())->overrideColor(
                dialog.currentColor());
        handler->mergeOverrideParameters();
        emit changed();
    });
    result.append(act);

    result.append(handler->parameters
                         ->getAxisIndicator()
                         .customizeMenu([this](const AxisIndicator2D &indicator) {
                             if (handler.isNull())
                                 return;
                             static_cast<IndicatorParameters2D *>(handler->overrideParameters())->overrideAxisIndicator(
                                     indicator);
                             handler->mergeOverrideParameters();
                             emit changed();
                         }, this)
                         ->menuAction());

    return result;
}

}

/**
 * Get the display title of this indicator.
 * 
 * @param inputUnits    the current set of units that went into this trace
 * @param showStation   if set then show the station in the legend
 * @param showArchive   if set then show the archive in the legend
 * @param context       the dynamic context
 * @return              the display title
 */
QString IndicatorHandler2D::getDisplayTitle(const std::vector<SequenceName::Set> &inputUnits,
                                            bool showStation,
                                            bool showArchive,
                                            const DisplayDynamicContext &context) const
{
    if (parameters->hasLegendText()) {
        QString text(parameters->getLegendText());
        context.handleString(text, DisplayDynamicContext::String_GraphLegend);
        return text;
    }
    return IndicatorValueHandler<1>::getLegendText(inputUnits, showStation, showArchive);
}

/**
 * The output controllers (if any) for the handler
 * 
 * @param inputUnits    the current set of units that went into this trace
 * @param showStation   if set then show the station in the legend
 * @param showArchive   if set then show the archive in the legend
 * @param context       the dynamic context
 * @return a            list of output controllers
 */
std::vector<std::shared_ptr<DisplayOutput>> IndicatorHandler2D::getOutputs(const std::vector<
        SequenceName::Set> &inputUnits,
                                                                           bool showStation,
                                                                           bool showArchive,
                                                                           const DisplayDynamicContext &context)
{
    if (!FP::defined(getLimits().min[0]))
        return {};

    QString title(getDisplayTitle(inputUnits, showStation, showArchive, context));
    if (title.isEmpty()) {
        title = IndicatorValueHandler<1>::getLegendText(inputUnits, showStation, showArchive);
    }

    return {std::make_shared<IndicatorOutput2D>(this, title)};
}

IndicatorSet2D::IndicatorSet2D() = default;

IndicatorSet2D::~IndicatorSet2D() = default;

IndicatorSet2D::IndicatorSet2D(const IndicatorSet2D &other) = default;

IndicatorSet2D::Cloned IndicatorSet2D::clone() const
{ return Cloned(new IndicatorSet2D(*this)); }

namespace {
struct IndicatorSet2DSortItem {
    IndicatorHandler2D *handler;
    std::vector<SequenceName::Set> units;

    bool operator<(const IndicatorSet2DSortItem &other) const
    {
        auto max = std::min<std::size_t>(units.size(), other.units.size());
        if (max <= 0) return false;
        for (std::ptrdiff_t i = max - 1; i >= 0; i--) {
            const auto &a = units[i];
            const auto &b = other.units[i];
            if (a.size() == 1 && b.size() == 1) {
                const auto &ua = *a.begin();
                const auto &ub = *b.begin();
                if (ua == ub)
                    continue;
                return SequenceName::OrderDisplay()(ua, ub);
            } else if (!a.empty() && !b.empty()) {
                std::vector<SequenceName> sa(a.begin(), a.end());
                std::sort(sa.begin(), sa.end(), SequenceName::OrderLogical());
                std::vector<SequenceName> sb(b.begin(), b.end());
                std::sort(sb.begin(), sb.end(), SequenceName::OrderLogical());
                for (std::size_t j = 0, nUnits = std::min(sa.size(), sb.size()); j < nUnits; ++j) {
                    const auto &ua = sa[j];
                    const auto &ub = sb[j];
                    if (ua == ub)
                        continue;
                    return SequenceName::OrderDisplay()(ua, ub);
                }
            }
        }
        return false;
    }
};
}

/**
 * Get the handlers in a fixed sort order.
 * 
 * @return a list of handlers
 */
std::vector<IndicatorHandler2D *> IndicatorSet2D::getSortedHandlers() const
{
    std::vector<IndicatorSet2DSortItem> sort;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        auto dispatch = getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            IndicatorSet2DSortItem add;
            add.handler = static_cast<IndicatorHandler2D *>(handlers[j].get());
            add.units = dispatch->getUnits(j);
            sort.emplace_back(std::move(add));
        }
    }
    std::stable_sort(sort.begin(), sort.end());
    std::vector<IndicatorHandler2D *> result;
    result.reserve(sort.size());
    for (const auto &it : sort) {
        result.emplace_back(it.handler);
    }
    return result;
}

std::unique_ptr<TraceParameters> IndicatorSet2D::parseParameters(const Variant::Read &value) const
{ return std::unique_ptr<TraceParameters>(new IndicatorParameters2D(value)); }

IndicatorSet2D::CreatedHandler IndicatorSet2D::createHandler(std::unique_ptr<
        TraceParameters> &&parameters) const
{
    return IndicatorSet2D::CreatedHandler(new IndicatorHandler2D(
            std::unique_ptr<IndicatorParameters2D>(static_cast<IndicatorParameters2D *>(
                                                           parameters.release()))));
}


/**
 * The output controllers (if any) for the set of indicators.
 * 
 * @param context   the context to use
 * @return          the list of output handlers in order
 */
std::vector<std::shared_ptr<
        DisplayOutput>> IndicatorSet2D::getOutputs(const DisplayDynamicContext &context)
{
    std::vector<IndicatorSet2DSortItem> sort;
    bool showStation = false;
    bool showArchive = false;
    SequenceName::Component priorStation;
    SequenceName::Component priorArchive;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        auto dispatch = getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            IndicatorSet2DSortItem add;
            add.handler = static_cast<IndicatorHandler2D *>(handlers[j].get());
            add.units = dispatch->getUnits(j);

            for (const auto &dim : add.units) {
                for (const auto &unit : dim) {
                    if (!showStation) {
                        if (priorStation.empty()) {
                            priorStation = unit.getStation();
                        } else if (priorStation != unit.getStation()) {
                            showStation = true;
                        }
                    }
                    if (!showArchive) {
                        if (priorArchive.empty()) {
                            priorArchive = unit.getArchive();
                        } else if (priorArchive != unit.getArchive()) {
                            showArchive = true;
                        }
                    }
                }
            }

            sort.push_back(add);
        }
    }
    std::stable_sort(sort.begin(), sort.end());

    std::vector<std::shared_ptr<DisplayOutput> > result;
    for (const auto &it : sort) {
        Util::append(it.handler->getOutputs(it.units, showStation, showArchive, context), result);
    }
    return result;
}

/**
 * Get the title of a given handler.  This is used to generate strings
 * to refer to the indicator for user interaction.
 * 
 * @param indicator the indicator to get the title for
 * @param context   the context to use
 */
QString IndicatorSet2D::handlerTitle(IndicatorHandler2D *indicator,
                                     const DisplayDynamicContext &context) const
{
    bool showStation = false;
    bool showArchive = false;
    SequenceName::Component priorStation;
    SequenceName::Component priorArchive;
    std::vector<SequenceName::Set> handlerUnits;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        auto dispatch = getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            auto units = dispatch->getUnits(j);
            if (static_cast<IndicatorHandler2D *>(handlers[j].get()) == indicator)
                handlerUnits = units;

            for (const auto &dim : units) {
                for (const auto &unit : dim) {
                    if (!showStation) {
                        if (priorStation.empty()) {
                            priorStation = unit.getStation();
                        } else if (priorStation != unit.getStation()) {
                            showStation = true;
                        }
                    }
                    if (!showArchive) {
                        if (priorArchive.empty()) {
                            priorArchive = unit.getArchive();
                        } else if (priorArchive != unit.getArchive()) {
                            showArchive = true;
                        }
                    }
                }
            }
        }
    }

    return indicator->getDisplayTitle(handlerUnits, showStation, showArchive, context);
}

/**
 * Get the origin for a given handler.  This is used to generate additional
 * disambiguation information.
 * 
 * @param indicator the indicator to get the origin for
 * @param context   the context to use
 */
QString IndicatorSet2D::handlerOrigin(IndicatorHandler2D *indicator,
                                      const DisplayDynamicContext &) const
{ return indicator->getOriginText(); }

}
}
