/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGTRACEDISPATCH_H
#define CPD3GRAPHINGTRACEDISPATCH_H

#include "core/first.hxx"

#include <vector>
#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QHash>
#include <QSet>

#include "graphing/graphing.hxx"
#include "graphing/display.hxx"
#include "datacore/segment.hxx"

namespace CPD3 {
namespace Graphing {

/**
 * A class that provides the dispatch/selection for various traces on graphs.
 * This provides segmented output for each trace including automatic fanout
 * creation.  Note that the segmentation is slightly violated with metadata
 * in that some metadata segments may not have their ends set correctly (will
 * be set after the start of the next one).  Strict start time ascending order
 * is observed, however.  Metadata values always precede values.
 * <br>
 * Thread safe.
 */
class CPD3GRAPHING_EXPORT TraceDispatch : public QObject {
Q_OBJECT
public:
    /**
     * A single value of a trace.
     */
    class CPD3GRAPHING_EXPORT OutputValue {
        double start;
        double end;
        std::vector<Data::Variant::Read> values;
        bool metadata;
    public:
        OutputValue(double s,
                    double e,
                    const std::vector<Data::Variant::Read> &setValues,
                    bool setMetadata = false);

        OutputValue(double s,
                    double e,
                    std::vector<Data::Variant::Read> &&setValues,
                    bool setMetadata = false);

        OutputValue();

        OutputValue(const OutputValue &);

        OutputValue &operator=(const OutputValue &);

        OutputValue(OutputValue &&);

        OutputValue &operator=(OutputValue &&);

        /**
         * Get the start time this value is effective at.
         * 
         * @return the start time
         */
        inline double getStart() const
        { return start; }

        /**
         * Get the end time this value is effective until.
         * 
         * @return the start time
         */
        inline double getEnd() const
        { return end; }

        /**
         * Get the number of dimensions.
         * 
         * @return the number of dimensions
         */
        inline int getDimensions() const
        { return values.size(); }

        /**
         * Get the value of the given dimension.
         * 
         * @param dim   the dimension to get
         * @return      the value
         */
        inline const Data::Variant::Read &get(int dim) const
        {
            if (dim >= static_cast<int>(values.size()))
                return Data::Variant::Read::empty();
            return values[dim];
        }

        /**
         * Get the base type of the given dimension.
         * 
         * @param dim   the dimension to get
         * @return      the value type
         */
        inline Data::Variant::Type getType(int dim) const
        {
            if (dim >= static_cast<int>(values.size()))
                return Data::Variant::Type::Empty;
            return values[dim].getType();
        }

        /**
         * Test if this value represents a metadata update.
         * 
         * @return      true if this value contains metadata
         */
        inline bool isMetadata() const
        { return metadata; }

        /**
         * Clear all values to undefined.
         */
        inline void clear()
        { std::fill(values.begin(), values.end(), Data::Variant::Read::empty()); }

        bool operator==(const TraceDispatch::OutputValue &other) const;
    };

private:
    std::mutex mutex;

    class IncomingBase;

    class IncomingStartEnd;

    class IncomingStart;

    class IncomingEnd;

    struct InputChainRegion {
        double start;
        double end;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }
    };

    std::deque<InputChainRegion> chainRegions;

    struct Trace {
        Trace(int dimensions, const std::deque<InputChainRegion> &regions);

        Trace(int dimensions,
              const std::deque<InputChainRegion> &regions,
              const Data::SequenceSegment::Stream &underReader,
              const Data::SequenceSegment::Stream &underReaderMeta);

        Trace(const Trace &other);

        Trace(const Trace &other,
              const Data::SequenceSegment::Stream &underReader,
              const Data::SequenceSegment::Stream &underReaderMeta);

        Data::SequenceSegment::Stream reader;
        Data::SequenceSegment::Stream readerMeta;
        std::vector<OutputValue> pending;
        std::vector<Data::SequenceName::Set> units;
        std::vector<Data::SequenceName> lastSelectedUnits;
        Data::SequenceSegment lastMetaSegment;
        bool needMetadataOutput;

        std::deque<InputChainRegion> remainingRegions;

        void handleData(Data::SequenceSegment &segment);

        void handleMeta(Data::SequenceSegment &segment);

        void handleCombined(Data::SequenceSegment &segment, Data::SequenceSegment &metasegment);

        void dispatchOutput(Data::SequenceSegment::Transfer &&output,
                            Data::SequenceSegment::Transfer &&outputMeta);

        void incomingData(int dimension, const Data::SequenceValue &value);

        void incomingData(int dimension, Data::SequenceValue &&value);

        void endData();
    };

    std::vector<std::unique_ptr<Trace>> traces;

    struct DefaultUnderlay {
        DefaultUnderlay();

        DefaultUnderlay(const DefaultUnderlay &other);

        void incomingData(const Data::SequenceValue &value);

        void incomingData(Data::SequenceValue &&value);

        Data::SequenceSegment::Stream reader;
        Data::SequenceSegment::Stream readerMeta;
    };

    std::vector<std::unique_ptr<DefaultUnderlay>> underlays;

    struct Dimension {
        explicit Dimension(int dimension);

        Dimension(const Dimension &other);

        ~Dimension();

        int dimensionNumber;

        Data::SequenceName::Map<std::vector<Trace *>> traceDispatch;
        Data::SequenceMatch::Composite selection;
        bool fanoutUnits;
        bool flattenFlavors;
        Data::SequenceName::Map<DefaultUnderlay *> defaultDispatch;

        std::unique_ptr<IncomingBase> dispatcher;
        Data::Variant::Root chainConfig;

        std::deque<Data::SequenceValue> values;

        bool ended;

        void reset();

        void incomingData(const Data::SequenceValue::Transfer &values);

        void incomingData(Data::SequenceValue::Transfer &&values);

        void incomingData(const Data::SequenceValue &value);

        void incomingData(Data::SequenceValue &&value);

        void endData();
    };

    std::vector<std::vector<std::unique_ptr<Dimension>>> dimensions;

    struct TrackingSegment {
        double start;
        double end;
        std::vector<QSet<Data::SequenceName> > units;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }
    };

    std::vector<TrackingSegment *> trackingSegments;

    bool tracesEnded;
    int nDispatchersEnded;
    int totalDispatchers;

    void buildFanoutDispatch(Dimension *dimension,
                             double time,
                             const Data::SequenceName &incomingUnit,
                             std::vector<Trace *> &defaultDispatch,
                             std::vector<Trace *> &targetDispatch);

    void dispatchValue(Dimension *dimension, Data::SequenceValue &&v);

    void allDispatchersEnded();

public:
    virtual ~TraceDispatch();

    TraceDispatch(const Data::ValueSegment::Transfer &config,
                  const DisplayDefaults &defaults = DisplayDefaults(),
                  bool asRealtime = false,
                  QObject *parent = 0);

    TraceDispatch(const TraceDispatch &other);

    /**
     * Test if two values representing configurations for the dispatch
     * could be merged together into a dispatch.
     * 
     * @param a the first configuration
     * @param b the second configuration
     */
    static bool mergable(const Data::Variant::Read &a, const Data::Variant::Read &b);

    /**
     * Register this dispatch with the given filter chain.  This also clears
     * any pending values.
     */
    void registerChain(Data::ProcessingTapChain *chain);

    /**
     * Get all pending values since the last call.
     * 
     * @return a list of lists of values in dimension then time ascending order
     */
    std::vector<std::vector<OutputValue>> getPending();

    /**
     * Return the number of dimensions that all traces have.
     * 
     * @return the number of dimensions
     */
    inline int getDimensions() const
    { return dimensions.size(); }

    /**
     * Get the units that have gone into a given dimension.
     * 
     * @param trace     the trace to query
     * @param dimension the dimension to query
     * @return          the units in the dimension
     */
    const Data::SequenceName::Set &getUnits(std::size_t trace, std::size_t dimension) const;

    /**
     * Get all the units that have gone into a given trace.
     * 
     * @param trace     the trace to query
     * @return          the units in the trace
     */
    std::vector<Data::SequenceName::Set> getUnits(std::size_t trace) const;

    /**
     * Get the preferred units for a trace.  This is the last selected ones
     * if possible, or the full set if not.
     *
     * @param trace     the trace to query
     * @return          the units in the trace
     */
    std::vector<Data::SequenceName::Set> getLatestUnits(std::size_t trace) const;

    /**
     * Test if all input streams from the current chain have received
     * endData().
     * 
     * @return true if all streams have ended
     */
    bool haveAllStreamsEnded();

signals:

    /**
     * Emitted when all input streams from the current chain have recevied
     * endData().
     */
    void allStreamsEnded();
};

}
}

#endif
