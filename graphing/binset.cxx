/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>

#include "graphing/binset.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Graphing {

/** @file graphing/binset.hxx
 * Provides a class to handle sets of binned points.
 */

BinParameters::BinParameters() : components(0),
                                 quantileMode(Standard),
                                 whiskerQuantile(0.05),
                                 boxQuantile(0.25),
                                 asHistogram(false)
{ }

BinParameters::~BinParameters() = default;

BinParameters::BinParameters(const BinParameters &other) = default;

BinParameters &BinParameters::operator=(const BinParameters &other) = default;

BinParameters::BinParameters(BinParameters &&other) = default;

BinParameters &BinParameters::operator=(BinParameters &&other) = default;

void BinParameters::copy(const TraceParameters *from)
{ *this = *(static_cast<const BinParameters *>(from)); }

void BinParameters::clear()
{
    TraceParameters::clear();
    components = 0;
}

/**
 * Create indicator parameters from the given configuration.
 * 
 * @param value     the configuration
 */
BinParameters::BinParameters(const Variant::Read &value) : TraceParameters(value),
                                                           components(0),
                                                           quantileMode(Standard),
                                                           whiskerQuantile(0.05),
                                                           boxQuantile(0.25),
                                                           asHistogram(false)
{
    if (value["Binning"].exists()) {
        Variant::Container::ReadArray list;
        if (value["Binning"].getType() == Variant::Type::Array) {
            list = value["Binning"].toArray();
        } else {
            Variant::Root temp;
            temp.write().toArray().after_back().set(value["Binning"]);
            list = temp.read().toArray();
        }
        for (auto dim : list) {
            BinningDefinition bd;
            bd.binning = Binning_FixedUniform;
            bd.count = 5;
            bd.size = 1.0;
            bd.logarithmic = false;

            const auto &type = dim["Type"].toString();
            if (Util::equal_insensitive(type, "size", "fixedsize")) {
                double check = dim["Width"].toDouble();
                bd.binning = Binning_FixedSize;
                if (!FP::defined(check) || check <= 0.0)
                    check = dim["Size"].toDouble();
                if (FP::defined(check) && check > 0.0)
                    bd.size = check;
                bd.logarithmic = dim["Log"].toBool();
            } else if (Util::equal_insensitive(type, "uniform", "fixedcount")) {
                bd.binning = Binning_FixedUniform;
                if (INTEGER::defined(dim["Count"].toInt64())) {
                    int check = dim["Count"].toInt64();
                    if (check > 0)
                        bd.count = check;
                }
                bd.logarithmic = dim["Log"].toBool();
            } else if (Util::equal_insensitive(type, "cluster", "clustering")) {
                bd.binning = Binning_Clustering;
                bd.logarithmic = dim["Log"].toBool();
                double check = dim["Width"].toDouble();
                if (FP::defined(check) && check >= 0.0) {
                    bd.size = check;
                } else {
                    bd.size = 0.1;
                }
            } else if (Util::equal_insensitive(type, "static")) {
                auto staticBins = dim["Bins"].toArray();
                if (!staticBins.empty()) {
                    bd.binning = Binning_Static;
                    for (auto bcfg : staticBins) {
                        StaticBin sb;
                        sb.start = bcfg["Start"].toDouble();
                        sb.end = bcfg["End"].toDouble();
                        sb.center = bcfg["Center"].toDouble();
                        if (!FP::defined(sb.center)) {
                            if (!FP::defined(sb.start)) {
                                sb.center = sb.end;
                            } else if (!FP::defined(sb.end)) {
                                sb.center = sb.start;
                            } else {
                                sb.center = (sb.start + sb.end) * 0.5;
                            }
                        }
                        if (FP::defined(sb.start) && FP::defined(sb.end) && sb.end < sb.start)
                            qSwap(sb.start, sb.end);
                        bd.bins.emplace_back(std::move(sb));
                    }
                }
            }

            components |= Component_Binning;
            bins.emplace_back(std::move(bd));
        }
    }

    if (value["QuantileMode"].exists()) {
        const auto &mode = value["QuantileMode"].toString();
        if (Util::equal_insensitive(mode, "standard", "quantiles")) {
            quantileMode = Standard;
            components |= Component_QuantileMode;
        } else if (Util::equal_insensitive(mode, "normal", "meansd", "mean")) {
            quantileMode = Normal;
            components |= Component_QuantileMode;
        }
    }

    if (value["Whisker/Quantile"].exists()) {
        double check = value["Whisker/Quantile"].toDouble();
        if (FP::defined(check) && check > 0.5 && check < 1.0)
            whiskerQuantile = 1.0 - check;
        else if (FP::defined(check) && check > 0.0 && check < 0.5)
            whiskerQuantile = check;
        else
            whiskerQuantile = FP::undefined();
        components |= Component_WhiskerQuantile;
    }

    if (value["Box/Quantile"].exists()) {
        double check = value["Box/Quantile"].toDouble();
        if (FP::defined(check) && check > 0.5 && check < 1.0)
            boxQuantile = 1.0 - check;
        else if (FP::defined(check) && check > 0.0 && check < 0.5)
            boxQuantile = check;
        else
            boxQuantile = FP::undefined();
        components |= Component_BoxQuantile;
    }

    if (value["Box/Histogram"].exists()) {
        asHistogram = value["Box/Histogram"].toBool();
        components |= Component_AsHistogram;
    }
}

void BinParameters::save(Variant::Write &value) const
{
    TraceParameters::save(value);

    if (components & Component_Binning) {
        auto target = value["Bins"].toArray();
        target.clear();
        for (const auto &bd : bins) {
            auto dim = target.after_back();
            switch (bd.binning) {
            case Binning_FixedUniform:
                dim["Type"].setString("Uniform");
                dim["Count"].setInt64(bd.count);
                dim["Log"].setBool(bd.logarithmic);
                break;
            case Binning_FixedSize:
                dim["Type"].setString("Size");
                dim["Width"].setDouble(bd.size);
                dim["Log"].setBool(bd.logarithmic);
                break;
            case Binning_Clustering:
                dim["Type"].setString("Clustering");
                dim["Width"].setDouble(bd.size);
                dim["Log"].setBool(bd.logarithmic);
                break;
            case Binning_Static:
                dim["Type"].setString("Static");
                for (const auto &sb : bd.bins) {
                    auto bin = dim["Bins"].toArray().after_back();
                    bin["Start"].setDouble(sb.start);
                    bin["End"].setDouble(sb.end);
                    bin["Center"].setDouble(sb.center);
                }
                break;
            }
        }
    }

    if (components & Component_QuantileMode) {
        switch (quantileMode) {
        case Standard:
            value["QuantileMode"].setString("Standard");
            break;
        case Normal:
            value["QuantileMode"].setString("Normal");
            break;
        }
    }

    if (components & Component_WhiskerQuantile) {
        value["Whisker/Quantile"].setDouble(whiskerQuantile);
    }

    if (components & Component_BoxQuantile) {
        value["Box/Quantile"].setDouble(boxQuantile);
    }

    if (components & Component_AsHistogram) {
        value["Box/Histogram"].setBool(asHistogram);
    }
}

std::unique_ptr<TraceParameters> BinParameters::clone() const
{ return std::unique_ptr<TraceParameters>(new BinParameters(*this)); }

void BinParameters::overlay(const TraceParameters *over)
{
    TraceParameters::overlay(over);

    const BinParameters *p = static_cast<const BinParameters *>(over);

    if (p->components & Component_Binning) {
        components |= Component_Binning;
        bins = p->bins;
    }

    if (p->components & Component_QuantileMode) {
        components |= Component_QuantileMode;
        quantileMode = p->quantileMode;
    }

    if (p->components & Component_WhiskerQuantile) {
        components |= Component_WhiskerQuantile;
        whiskerQuantile = p->whiskerQuantile;
    }

    if (p->components & Component_BoxQuantile) {
        components |= Component_BoxQuantile;
        boxQuantile = p->boxQuantile;
    }

    if (p->components & Component_AsHistogram) {
        components |= Component_AsHistogram;
        asHistogram = p->asHistogram;
    }
}

}
}
