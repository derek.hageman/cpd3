/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGTRACE2D_H
#define CPD3GRAPHINGTRACE2D_H

#include "core/first.hxx"

#include <vector>
#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QPointer>

#include "graphing/graphing.hxx"
#include "algorithms/model.hxx"
#include "graphing/tracedispatch.hxx"
#include "graphing/tracecommon.hxx"
#include "graphing/graphcommon2d.hxx"
#include "graphing/axistransformer.hxx"
#include "graphing/axisdimension.hxx"

namespace CPD3 {
namespace Graphing {

/**
 * The parameters for two dimensional traces.
 */
class CPD3GRAPHING_EXPORT TraceParameters2D : public TraceParameters {
public:
    /** The way fit models are used. */
    enum FitMode {
        /** The X axis is the independent variable. */
        Fit_IndependentX, /** The Y axis is the independent variable. */
        Fit_IndependentY, /** A secondary "I" variable is the independent axis. */
        Fit_IndependentI,
    };
    /** The type of fill done by the trace. */
    enum FillMode {
        /** A model is scanned at all pixels to produce an image. */
        Fill_ModelScan, /** The points are forced to a grid and bilinearly interpolated. */
        Fill_BilinearGrid, /** The trace projects a shaded are to the top of the trace area. */
        Fill_ShadeToTop, /** The trace projects a shaded are to the bottom of the trace area. */
        Fill_ShadeToBottom, /** The trace projects a shaded are to the left of the trace area. */
        Fill_ShadeToLeft, /** The trace projects a shaded are to the right of the trace area. */
        Fill_ShadeToRight, /** The fit projects a shaded are to the top of the trace area. */
        Fill_FitShadeToTop, /** The fit projects a shaded are to the bottom of the trace area. */
        Fill_FitShadeToBottom, /** The fit projects a shaded are to the left of the trace area. */
        Fill_FitShadeToLeft, /** The fit projects a shaded are to the right of the trace area. */
        Fill_FitShadeToRight,
    };
private:
    enum {
        Component_LineEnable = 0,
        Component_LineWidth,
        Component_LineStyle,
        Component_LineDrawPriority,
        Component_SymbolEnable,
        Component_Symbol,
        Component_SymbolDrawPriority,
        Component_Color,
        Component_LegendEntry,
        Component_LegendText,
        Component_LegendFont,
        Component_LegendSortPriority,
        Component_FitModel,
        Component_FitLogXBase,
        Component_FitLogYBase,
        Component_FitLogZBase,
        Component_FitMode,
        Component_FitDisableZ,
        Component_FitLineWidth,
        Component_FitLineStyle,
        Component_FitDrawPriority,
        Component_FitColor,
        Component_FitLegendText,
        Component_FitLegendFont,
        Component_FillModel,
        Component_FillLogXBase,
        Component_FillLogYBase,
        Component_FillLogZBase,
        Component_FillMode,
        Component_FillDrawPriority,
        Component_XBinding,
        Component_YBinding,
        Component_ZBinding,
        Component_Continuity,
        Component_SkipUndefined,

        Component_LAST,
    };
    std::vector<bool> components;

    bool lineEnable;
    qreal lineWidth;
    Qt::PenStyle lineStyle;
    int lineDrawPriority;
    bool symbolEnable;
    TraceSymbol symbol;
    int symbolDrawPriority;
    QColor color;
    bool legendEntry;
    QString legendText;
    QFont legendFont;
    int legendSortPriority;
    Data::Variant::Root fitModel;
    double fitLogXBase;
    double fitLogYBase;
    double fitLogZBase;
    FitMode fitMode;
    double fitMinI;
    double fitMaxI;
    bool fitDisableZ;
    qreal fitLineWidth;
    Qt::PenStyle fitLineStyle;
    int fitDrawPriority;
    QColor fitColor;
    QString fitLegendText;
    QFont fitLegendFont;
    Data::Variant::Root fillModel;
    double fillLogXBase;
    double fillLogYBase;
    double fillLogZBase;
    FillMode fillMode;
    int fillDrawPriority;
    QString xBinding;
    QString yBinding;
    QString zBinding;
    double continuity;
    bool skipUndefined;
public:
    TraceParameters2D();

    virtual ~TraceParameters2D();

    TraceParameters2D(const TraceParameters2D &);

    TraceParameters2D &operator=(const TraceParameters2D &);

    TraceParameters2D(TraceParameters2D &&);

    TraceParameters2D &operator=(TraceParameters2D &&);

    TraceParameters2D(const Data::Variant::Read &value);

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<TraceParameters> clone() const override;

    void overlay(const TraceParameters *over) override;

    void copy(const TraceParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify the line enable state.
     * @return true if the parameters specify the line enable state
     */
    inline bool hasLineEnable() const
    {
        return components[Component_LineEnable];
    }

    /**
     * Get the line enable state.
     * @return true to draw connecting lines
     */
    inline bool getLineEnable() const
    { return lineEnable; }

    /**
     * Set the line enable state.  This does not set the defined flag.
     * @param s the new line enable state
     */
    inline void setLineEnable(bool s)
    { lineEnable = s; }

    /**
     * Clear the line enable flag.
     */
    inline void clearLineEnable()
    { components[Component_LineEnable] = false; }

    /**
     * Override the line enable state to the given value.  This sets the
     * defined flag.
     * @param s the new line enable state
     */
    inline void overrideLineEnable(bool s)
    {
        lineEnable = s;
        components[Component_LineEnable] = true;
    }

    /**
     * Test if the parameters specify a line width.
     * @return true if the parameters specify a line width
     */
    inline bool hasLineWidth() const
    {
        return components[Component_LineWidth];
    }

    /**
     * Get the line width to use.
     * @return the line width
     */
    inline qreal getLineWidth() const
    { return lineWidth; }

    /**
    * Override the trace line width.  This sets the defined flag.
    * @param width the new width
    */
    inline void overrideLineWidth(double width)
    {
        lineWidth = width;
        components[Component_LineWidth] = true;
    }

    /**
     * Test if the parameters specify a line style.
     * @return true if the parameters specify a line style
     */
    inline bool hasLineStyle() const
    {
        return components[Component_LineStyle];
    }

    /**
     * Get the line style to use.
     * @return the line style
     */
    inline Qt::PenStyle getLineStyle() const
    { return lineStyle; }

    /**
    * Override the trace line style.  This sets the defined flag.
    * @param style the new width
    */
    inline void overrideLineStyle(Qt::PenStyle style)
    {
        lineStyle = style;
        components[Component_LineStyle] = true;
    }

    /**
     * Test if the parameters specify a line draw priority.
     * @return true if the parameters specify a line draw priority.
     */
    inline bool hasLineDrawPriority() const
    { return components[Component_LineDrawPriority]; }

    /**
     * Get the line draw priority.
     * @return the line draw priority.
     */
    inline int getLineDrawPriority() const
    { return lineDrawPriority; }

    /**
     * Test if the parameters specify the symbol enable state.
     * @return true if the parameters specify symbol line enable state
     */
    inline bool hasSymbolEnable() const
    {
        return components[Component_SymbolEnable];
    }

    /**
     * Get the symbol enable state.
     * @return true to draw symbols at all points
     */
    inline bool getSymbolEnable() const
    { return symbolEnable; }

    /**
     * Set the symbol enable state.  This does not set the defined flag.
     * @param s the new symbol enable state
     */
    inline void setSymbolEnable(bool s)
    { symbolEnable = s; }

    /**
     * Clear the symbol enable flag.
     */
    inline void clearSymbolEnable()
    { components[Component_SymbolEnable] = false; }

    /**
     * Override the symbol enable state to the given value.  This sets the
     * defined flag.
     * @param s the new symbol enable state
     */
    inline void overrideSymbolEnable(bool s)
    {
        symbolEnable = s;
        components[Component_SymbolEnable] = true;
    }

    /**
     * Test if the parameters specify a symbol.
     * @return true if the parameters specify a symbol
     */
    inline bool hasSymbol() const
    { return components[Component_Symbol]; }

    /**
     * Get the symbol to use.
     * @return the trace symbol
     */
    inline TraceSymbol getSymbol() const
    { return symbol; }

    /**
     * Get the symbol to use as a reference.
     * @return the trace symbol
     */
    inline TraceSymbol &getSymbolReference()
    { return symbol; }

    /**
    * Override the trace symbol.  This sets the defined flag.
    * @param symbol the new symbol
    */
    inline void overrideTraceSymbol(const TraceSymbol &symbol)
    {
        this->symbol = symbol;
        components[Component_Symbol] = true;
    }

    /**
     * Test if the parameters specify a symbol draw priority.
     * @return true if the parameters specify a symbol draw priority.
     */
    inline bool hasSymbolDrawPriority() const
    { return components[Component_SymbolDrawPriority]; }

    /**
     * Get the symbol draw priority.
     * @return the symbol draw priority.
     */
    inline int getSymbolDrawPriority() const
    { return symbolDrawPriority; }

    /**
     * Test if the parameters specify a default color.
     * @return true if the parameters specify a default color
     */
    inline bool hasColor() const
    { return components[Component_Color]; }

    /**
     * Get the default color.
     * @return the default color
     */
    inline QColor getColor() const
    { return color; }

    /**
     * Change the default color.  This does not set the "color defined" flag.
     * @param set   the new color
     */
    inline void setColor(const QColor &set)
    { color = set; }

    /**
     * Override the default color.  This sets the defined flag.
     * @param color the new color
     */
    inline void overrideColor(const QColor &color)
    {
        this->color = color;
        components[Component_Color] = true;
    }

    /**
     * Test if the parameters specify the legend entry show status.
     * @return true if the parameters specify if the legend entry shows
     */
    inline bool hasLegendEntry() const
    { return components[Component_LegendEntry]; }

    /**
     * Get the legend entry status.
     * @return the legend entry status
     */
    inline bool getLegendEntry() const
    { return legendEntry; }

    /**
     * Override the legend entry to the given status.  This sets the
     * defined flag.
     * @param text  the new legend text
     */
    inline void overrideLegendEntry(bool enable)
    {
        legendEntry = enable;
        components[Component_LegendEntry] = true;
    }

    /**
     * Test if the parameters specify the legend text.
     * @return true if the parameters specify the legend text
     */
    inline bool hasLegendText() const
    { return components[Component_LegendText]; }

    /**
     * Get the legend text.
     * @return the legend text
     */
    inline QString getLegendText() const
    { return legendText; }

    /**
     * Override the legend text to the given value.  This sets the
     * defined flag.
     * @param text  the new legend text
     */
    inline void overrideLegendText(const QString &text)
    {
        legendText = text;
        components[Component_LegendText] = true;
    }


    /**
     * Test if the parameters specify a legend font.
     * @return true if the parameters specify a legend font
     */
    inline bool hasLegendFont() const
    { return components[Component_LegendFont]; }

    /**
     * Get the legend font.
     * @return the legend font.
     */
    inline QFont getLegendFont() const
    { return legendFont; }

    /**
     * Override the legend entry font.  This sets the defined flag.
     * @param font the new font
     */
    inline void overrideLegendFont(const QFont &font)
    {
        legendFont = font;
        components[Component_LegendFont] = true;
    }

    /**
     * Test if the parameters specify a legend sort priority.
     * @return true if the parameters specify a legend sort priority
     */
    inline bool hasLegendSortPriority() const
    { return components[Component_LegendSortPriority]; }

    /**
     * Get the legend sort priority.
     * @return the legend sort priority
     */
    inline int getLegendSortPriority() const
    { return legendSortPriority; }

    /**
     * Test if the parameters specify a fit model.
     * @return true if the parameters specify a fit model
     */
    inline bool hasFitModel() const
    { return components[Component_FitModel]; }

    /**
     * Get the fit model parameters.
     * @return the fit model parameters
     */
    inline const Data::Variant::Root &getFitModel() const
    { return fitModel; }

    /**
     * Disable the fit and clear the defined flag.
     */
    inline void disableFit()
    {
        components[Component_FitModel] = false;
        fitModel = Data::Variant::Root();
    }

    /**
     * Test if the parameters specify a fit log X axis base.
     * @return true if the parameters specify fit log X axis base
     */
    inline bool hasFitLogXBase() const
    { return components[Component_FitLogXBase]; };

    /**
     * Get the fit log X axis base
     * @return the base of the X axis
     */
    inline double getFitLogXBase() const
    { return fitLogXBase; }

    /**
     * Set the fit log X axis base.  This does not set the defined flag.
     * @param b the new base
     */
    inline void setFitLogXBase(double b)
    { fitLogXBase = b; }

    /**
     * Test if the parameters specify a fit log Y axis base.
     * @return true if the parameters specify fit log Y axis base
     */
    inline bool hasFitLogYBase() const
    { return components[Component_FitLogYBase]; };

    /**
     * Get the fit log Y axis base
     * @return the base of the Y axis
     */
    inline double getFitLogYBase() const
    { return fitLogYBase; }

    /**
     * Set the fit log Y axis base.  This does not set the defined flag.
     * @param b the new base
     */
    inline void setFitLogYBase(double b)
    { fitLogYBase = b; }

    /**
     * Test if the parameters specify a fit log Z axis base.
     * @return true if the parameters specify fit log Z axis base
     */
    inline bool hasFitLogZBase() const
    { return components[Component_FitLogZBase]; };

    /**
     * Get the fit log Z axis base
     * @return the base of the Z axis
     */
    inline double getFitLogZBase() const
    { return fitLogZBase; }

    /**
     * Set the fit log Z axis base.  This does not set the defined flag.
     * @param b the new base
     */
    inline void setFitLogZBase(double b)
    { fitLogZBase = b; }

    /**
     * Test if the parameters specify a fit mode.
     * @return true if the parameters specify a fit mode
     */
    inline bool hasFitMode() const
    { return components[Component_FitMode]; }

    /**
     * Get the fit mode.
     * @return the fit mode
     */
    inline FitMode getFitMode() const
    { return fitMode; }

    /**
     * Get the fit minimum I value.  Only valid in independent I mode.
     * @return the minimum I value
     */
    inline double getFitMinI() const
    { return fitMinI; }

    /**
     * Get the fit maximum I value.  Only valid in independent I mode.
     * @return the maximum I value
     */
    inline double getFitMaxI() const
    { return fitMaxI; }

    /**
     * Test if the parameters specify the fit Z disable state.
     * @return true if the parameters specify a fit Z disable state
     */
    inline bool hasFitDisableZ() const
    { return components[Component_FitDisableZ]; }

    /**
     * Get the fit Z disable state.
     * @return true if the fit should explicitly not include the Z axis
     */
    inline bool getFitDisableZ() const
    { return fitDisableZ; }

    /**
     * Test if the parameters specify a fit line width.
     * @return true if the parameters specify a fit line width
     */
    inline bool hasFitLineWidth() const
    { return components[Component_FitLineWidth]; }

    /**
     * Get the fit line width.
     * @return the fit line width
     */
    inline qreal getFitLineWidth() const
    { return fitLineWidth; }

    /**
    * Override the fit line width.  This sets the defined flag.
    * @param width the new width
    */
    inline void overrideFitLineWidth(double width)
    {
        fitLineWidth = width;
        components[Component_FitLineWidth] = true;
    }

    /**
     * Test if the parameters specify a fit line style.
     * @return true if the parameters specify a fit line style
     */
    inline bool hasFitLineStyle() const
    { return components[Component_FitLineStyle]; }

    /**
     * Get the fit line style.
     * @return the fit line style
     */
    inline Qt::PenStyle getFitLineStyle() const
    { return fitLineStyle; }

    /**
    * Override the fit line style.  This sets the defined flag.
    * @param style the new width
    */
    inline void overrideFitLineStyle(Qt::PenStyle style)
    {
        fitLineStyle = style;
        components[Component_FitLineStyle] = true;
    }

    /**
     * Test if the parameters specify a fit draw priority.
     * @return true if the parameters specify a fit draw priority.
     */
    inline bool hasFitDrawPriority() const
    { return components[Component_FitDrawPriority]; }

    /**
     * Get the fit draw priority.
     * @return the fit draw priority.
     */
    inline int getFitDrawPriority() const
    { return fitDrawPriority; }

    /**
     * Test if the parameters specify a fit color.
     * @return true if the parameters specify a fit color
     */
    inline bool hasFitColor() const
    { return components[Component_FitColor]; }

    /**
     * Get the fit color.
     * @return the fit color
     */
    inline QColor getFitColor() const
    { return fitColor; }

    /**
     * Change the fit color.  This does not set the "color defined" flag.
     * @param set   the new color
     */
    inline void setFitColor(const QColor &set)
    { fitColor = set; }

    /**
     * Override the fit color.  This sets the defined flag.
     * @param color the new color
     */
    inline void overrideFitColor(const QColor &color)
    {
        this->fitColor = color;
        components[Component_FitColor] = true;
    }

    /**
     * Test if the parameters specify the fit legend text.
     * @return true if the parameters specify the legend text
     */
    inline bool hasFitLegendText() const
    { return components[Component_FitLegendText]; }

    /**
     * Get the fit legend text.
     * @return the legend text
     */
    inline QString getFitLegendText() const
    { return fitLegendText; }

    /**
     * Override the fit legend text to the given value.  This sets the
     * defined flag.
     * @param text  the new legend text
     */
    inline void overrideFitLegendText(const QString &text)
    {
        fitLegendText = text;
        components[Component_FitLegendText] = true;
    }

    /**
     * Test if the parameters specify a fit legend font.
     * @return true if the parameters specify a legend font
     */
    inline bool hasFitLegendFont() const
    { return components[Component_FitLegendFont]; }

    /**
     * Get the fit legend font.
     * @return the legend font.
     */
    inline QFont getFitLegendFont() const
    { return fitLegendFont; }

    /**
     * Override the fit legend entry font.  This sets the defined flag.
     * @param font the new font
     */
    inline void overrideFitLegendFont(const QFont &font)
    {
        fitLegendFont = font;
        components[Component_FitLegendFont] = true;
    }


    /**
     * Test if the parameters specify a fill model.
     * @return true if the parameters specify a fill model
     */
    inline bool hasFillModel() const
    { return components[Component_FillModel]; }

    /**
     * Get the fill model.
     * @return the fill model
     */
    inline const Data::Variant::Root &getFillModel() const
    { return fillModel; }

    /**
     * Disable the fill and clear the defined flag.
     */
    inline void disableFill()
    {
        components[Component_FillModel] = false;
        components[Component_FillMode] = false;
        fillModel = Data::Variant::Root();
        fillMode = Fill_ModelScan;
    }

    /**
     * Test if the parameters specify a fill log X axis base.
     * @return true if the parameters specify fill log X axis base
     */
    inline bool hasFillLogXBase() const
    { return components[Component_FillLogXBase]; };

    /**
     * Get the fill log X axis base
     * @return the base of the X axis
     */
    inline double getFillLogXBase() const
    { return fillLogXBase; }

    /**
     * Set the fill log X axis base.  This does not set the defined flag.
     * @param b the new base
     */
    inline void setFillLogXBase(double b)
    { fillLogXBase = b; }

    /**
     * Test if the parameters specify a fill log Y axis base.
     * @return true if the parameters specify fill log Y axis base
     */
    inline bool hasFillLogYBase() const
    { return components[Component_FillLogYBase]; };

    /**
     * Get the fill log Y axis base
     * @return the base of the Y axis
     */
    inline double getFillLogYBase() const
    { return fillLogYBase; }

    /**
     * Set the fill log Y axis base.  This does not set the defined flag.
     * @param b the new base
     */
    inline void setFillLogYBase(double b)
    { fillLogYBase = b; }

    /**
     * Test if the parameters specify a fill log Z axis base.
     * @return true if the parameters specify fill log Z axis base
     */
    inline bool hasFillLogZBase() const
    { return components[Component_FillLogZBase]; };

    /**
     * Get the fill log Z axis base
     * @return the base of the Z axis
     */
    inline double getFillLogZBase() const
    { return fillLogZBase; }

    /**
     * Set the fill log Z axis base.  This does not set the defined flag.
     * @param b the new base
     */
    inline void setFillLogZBase(double b)
    { fillLogZBase = b; }

    /**
     * Test if the parameters specify a fill mode.
     * @return true if the parameters specify a fill mode
     */
    inline bool hasFillMode() const
    { return components[Component_FillMode]; }

    /**
     * Get the fill mode.
     * @return the fill mode
     */
    inline FillMode getFillMode() const
    { return fillMode; }

    /**
     * Test if the parameters specify a fill draw priority.
     * @return true if the parameters specify a fill draw priority.
     */
    inline bool hasFillDrawPriority() const
    { return components[Component_FillDrawPriority]; }

    /**
     * Get the fill draw priority.
     * @return the fill draw priority.
     */
    inline int getFillDrawPriority() const
    { return fillDrawPriority; }


    /**
     * Test if the parameters specify the X axis binding.
     * @return true if the parameters specify the X axis binding
     */
    inline bool hasXBinding() const
    { return components[Component_XBinding]; }

    /**
     * Get the X axis binding.
     * @return the X axis binding
     */
    inline QString getXBinding() const
    { return xBinding; }

    /**
     * Test if the parameters specify the Y axis binding.
     * @return true if the parameters specify the Y axis binding
     */
    inline bool hasYBinding() const
    { return components[Component_YBinding]; }

    /**
     * Get the Y axis binding.
     * @return the Y axis binding
     */
    inline QString getYBinding() const
    { return yBinding; }

    /**
     * Test if the parameters specify the Z axis binding.
     * @return true if the parameters specify the Z axis binding
     */
    inline bool hasZBinding() const
    { return components[Component_ZBinding]; }

    /**
     * Get the Z axis binding.
     * @return the Z axis binding
     */
    inline QString getZBinding() const
    { return zBinding; }

    /**
     * Test if the parameters specify the maximum continuity limit.
     * @return true if the parameters specify the maximum continuity limit
     */
    inline bool hasContinuity() const
    { return components[Component_Continuity]; }

    /**
     * Get the maximum continuity limit.
     * @return the maximum continuity limit
     */
    inline double getContinuity() const
    { return continuity; }

    /**
     * Test if the parameters specify if undefined points should be skipped.
     * @return true if the parameters specify the undefined skip state
     */
    inline bool hasSkipUndefined() const
    { return components[Component_SkipUndefined]; }

    /**
     * Get if undefined points should be skipped
     * @return true if undefined points should be skipped
     */
    inline bool getSkipUndefined() const
    { return skipUndefined; }
};

class TraceOutput2D;

namespace Internal {
class Graph2DLegendModificationTrace;

class Graph2DLegendModificationTraceFit;
}

/**
 * The base class for two dimensional traces.
 */
class CPD3GRAPHING_EXPORT TraceHandler2D : public TraceValueHandler<3> {
    TraceParameters2D *parameters;
    bool anyValidPoints;
    bool anyIsolatedPoints;

    Algorithms::DeferredModelWrapper *runningFit;
    std::shared_ptr<Algorithms::Model> fit;
    bool needFitRerun;
    Algorithms::DeferredModelWrapper *runningFill;
    std::shared_ptr<Algorithms::Model> fill;
    bool needFillRerun;

    bool alwaysHideLegend;

    bool startFit(bool deferrable);

    bool startFill(bool deferrable);

    Algorithms::Model *wrapFit(Algorithms::Model *input);

    Algorithms::Model *wrapFill(Algorithms::Model *input);

    QString getLegendText(const std::vector<Data::SequenceName::Set> &inputUnits,
                          bool showStation,
                          bool showArchive,
                          const DisplayDynamicContext &context,
                          bool *changed) const;

    friend class TraceOutput2D;

    friend class Internal::Graph2DLegendModificationTrace;

    friend class Internal::Graph2DLegendModificationTraceFit;

public:
    TraceHandler2D(std::unique_ptr<TraceParameters2D> &&params);

    virtual ~TraceHandler2D();

    using Cloned = typename TraceValueHandler<3>::Cloned;

    Cloned clone() const override;

    /**
     * Get the legend sort priority of this item.
     * @return the legend sort priority
     */
    inline int getLegendSortPriority() const
    { return parameters->getLegendSortPriority(); }

    /**
     * Test if this item has a fixed X binding.
     * @return true if this item has a fixed X binding
     */
    inline bool hasXBinding() const
    { return parameters->hasXBinding(); }

    /**
     * Get the fixed X binding of this item.
     * @return the X binding
     */
    inline QString getXBinding() const
    { return parameters->getXBinding(); }

    /**
     * Test if this item has a fixed Y binding.
     * @return true if this item has a fixed Y binding
     */
    inline bool hasYBinding() const
    { return parameters->hasYBinding(); }

    /**
     * Get the fixed Y binding of this item.
     * @return the Y binding
     */
    inline QString getYBinding() const
    { return parameters->getYBinding(); }

    /**
     * Test if this item has a fixed Z binding.
     * @return true if this item has a fixed Z binding
     */
    inline bool hasZBinding() const
    { return parameters->hasZBinding(); }

    /**
     * Get the fixed Z binding of this item.
     * @return the Z binding
     */
    inline QString getZBinding() const
    { return parameters->getZBinding(); }

    /**
     * Get the color of the trace.
     * @return the trace color
     */
    inline QColor getColor() const
    { return parameters->getColor(); }

    /**
     * Get the fit color of the trace.
     * @return the fit trace color
     */
    inline QColor getFitColor() const
    { return parameters->getFitColor(); }

    /**
     * Test if the trace contains any valid points.
     * 
     * @return true if the trace has valid points
     */
    inline bool getAnyValidPoints() const
    { return anyValidPoints; }

    /**
     * Set the override status for the legend hide.  If enabled then the
     * handler will not produce legend entries.
     * 
     * @param en        the enable state
     * @return          true if the state changed
     */
    inline bool setLegendAlwaysHide(bool en)
    {
        if (en == alwaysHideLegend)
            return false;
        alwaysHideLegend = en;
        return true;
    }

    bool axesBound(AxisDimension *x, AxisDimension *y, AxisDimension *z, bool deferrable = false);

    bool process(const std::vector<TraceDispatch::OutputValue> &incoming,
                 bool deferrable = false) override;

    void trimToLast() override;

    void trimData(double start, double end) override;

    void setVisibleTimeRange(double start, double end) override;

    std::vector<std::shared_ptr<LegendItem> > getLegend(const std::vector<
            Data::SequenceName::Set> &inputUnits = {},
                                                        bool showStation = false,
                                                        bool showArchive = false,
                                                        const DisplayDynamicContext &context = {},
                                                        bool *changed = nullptr);

    QString getDisplayTitle(const std::vector<Data::SequenceName::Set> &inputUnits = {},
                            bool showStation = false,
                            bool showArchive = false,
                            const DisplayDynamicContext &context = {}) const;

    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> createDraw(const AxisTransformer &x,
                                                                  const AxisTransformer &y,
                                                                  const AxisTransformer &z = {},
                                                                  const TraceGradient &gradient = {}) const;

    void getColors(const std::vector<Data::SequenceName::Set> &inputUnits,
                   std::vector<TraceUtilities::ColorRequest> &requestColors,
                   std::deque<QColor> &claimedColors) const;

    void claimColors(std::deque<QColor> &colors) const;

    void insertUsedSymbols(std::unordered_set<TraceSymbol::Type,
                                              TraceSymbol::TypeHash> &types) const;

    void bindSymbol(std::unordered_set<TraceSymbol::Type, TraceSymbol::TypeHash> &types);

    std::vector<std::shared_ptr<DisplayOutput> > getOutputs(const std::vector<
            Data::SequenceName::Set> &inputUnits = {},
                                                            bool showStation = false,
                                                            bool showArchive = false,
                                                            const DisplayDynamicContext &context = {});

protected:
    TraceHandler2D(const TraceHandler2D &other, std::unique_ptr<TraceParameters> &&params);

    virtual double getFitScale() const;

    virtual QString getFitLegend(const std::shared_ptr<Algorithms::Model> &fit) const;

    void convertValue(const TraceDispatch::OutputValue &value,
                      std::vector<TracePoint<3>> &points) override;
};


/**
 * The base class for two dimensional trace sets.
 */
class CPD3GRAPHING_EXPORT TraceSet2D : public TraceDispatchSet<3> {
public:
    TraceSet2D();

    virtual ~TraceSet2D();

    using Cloned = typename TraceDispatchSet<3>::Cloned;

    Cloned clone() const override;

    std::vector<TraceHandler2D *> getSortedHandlers() const;

    bool addLegend(Legend &legend, const DisplayDynamicContext &context = {}) const;

    QString handlerTitle(TraceHandler2D *trace, const DisplayDynamicContext &context) const;

    QString handlerOrigin(TraceHandler2D *trace, const DisplayDynamicContext &context) const;

    void getColors(std::vector<TraceUtilities::ColorRequest> &requestColors,
                   std::deque<QColor> &claimedColors) const;

    void claimColors(std::deque<QColor> &colors) const;

    std::vector<
            std::shared_ptr<DisplayOutput>> getOutputs(const DisplayDynamicContext &context = {});

protected:
    TraceSet2D(const TraceSet2D &other);

    std::unique_ptr<
            TraceParameters> parseParameters(const Data::Variant::Read &value) const override;

    using CreatedHandler = typename TraceDispatchSet<3>::CreatedHandler;

    CreatedHandler createHandler(std::unique_ptr<TraceParameters> &&parameters) const override;
};

/**
 * The output type for 2-D traces.
 */
class CPD3GRAPHING_EXPORT TraceOutput2D : public DisplayOutput {
Q_OBJECT
    QPointer<TraceHandler2D> handler;
    QString title;
public:
    TraceOutput2D(TraceHandler2D *setHandler, const QString &setTitle);

    virtual ~TraceOutput2D();

    QString getTitle() const override;

    QList<QAction *> getMenuActions(QWidget *parent = nullptr) override;

public slots:

    /**
     * Set if the trace should hide legend entries.
     * 
     * @param enable    true to disable the legend entry
     */
    void setLegendAlwaysHide(bool enable);
};

namespace Internal {

class Graph2DLegendModificationTrace : public Graph2DLegendModification {
Q_OBJECT

    QPointer<TraceHandler2D> handler;
    QString legendText;
public:
    Graph2DLegendModificationTrace(Graph2D *graph,
                                   const QPointer<TraceHandler2D> &setHandler,
                                   const QString &setLegendText);

    virtual ~Graph2DLegendModificationTrace();

    QList<QAction *> getMenuActions(QWidget *parent = nullptr) override;
};

class Graph2DLegendModificationTraceFit : public Graph2DLegendModification {
Q_OBJECT

    QPointer<TraceHandler2D> handler;
    QString legendText;
public:
    Graph2DLegendModificationTraceFit(Graph2D *graph,
                                      const QPointer<TraceHandler2D> &setHandler,
                                      const QString &setLegendText);

    virtual ~Graph2DLegendModificationTraceFit();

    QList<QAction *> getMenuActions(QWidget *parent = nullptr) override;
};

}

}
}


#endif
