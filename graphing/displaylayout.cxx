/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>

#include "graphing/displaylayout.hxx"
#include "graphing/graph2d.hxx"
#include "graphing/timeseries2d.hxx"
#include "graphing/cycleplot2d.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Graphing {

/** @file graphing/displaylayout.hxx
 * A layout consisting of nested display components.
 */

DisplayLayout::DisplayLayout(const DisplayDefaults &defaults, QObject *parent) : Display(defaults,
                                                                                         parent),
                                                                                 havePrepared(
                                                                                         false),
                                                                                 havePredicted(
                                                                                         false),
                                                                                 haveEverPrepared(
                                                                                         false),
                                                                                 haveEverPredicted(
                                                                                         false),
                                                                                 priorMouseover(
                                                                                         nullptr),
                                                                                 elementsReady(0)
{ }

DisplayLayout::~DisplayLayout() = default;

DisplayLayout::DisplayLayout(const DisplayLayout &other) : Display(other),
                                                           rowStretch(other.rowStretch),
                                                           columnStretch(other.columnStretch),
                                                           havePrepared(false),
                                                           havePredicted(false),
                                                           haveEverPrepared(false),
                                                           haveEverPredicted(false),
                                                           priorMouseover(nullptr),
                                                           elementsReady(0)
{
    for (const auto &it : other.elements) {
        elements.emplace_back(new Element(*it));
        elements.back()->display = it->display->clone();
    }
    setBreakdowns();
}

std::unique_ptr<Display> DisplayLayout::clone() const
{ return std::unique_ptr<Display>(new DisplayLayout(*this)); }

DisplayLayout::Element::Element() : x(0),
                                    y(0),
                                    width(1),
                                    height(1),
                                    minimumSize(0, 0),
                                    maximumSize(0, 0)
{ }

DisplayLayout::Element::~Element() = default;

DisplayLayout::Element::Element(const Element &other) : x(other.x),
                                                        y(other.y),
                                                        width(other.width),
                                                        height(other.height),
                                                        minimumSize(other.minimumSize),
                                                        maximumSize(other.maximumSize)
{ }

bool displayElementRowSort(const DisplayLayout::Element *a, const DisplayLayout::Element *b)
{ return a->y < b->y; }

bool displayElementColumnSort(const DisplayLayout::Element *a, const DisplayLayout::Element *b)
{ return a->x < b->x; }

void DisplayLayout::setBreakdowns()
{
    rows.clear();
    columns.clear();

    for (const auto &it : elements) {
        for (std::size_t row = it->y, max = it->y + it->height; row < max; ++row) {
            while (row >= rows.size()) {
                rows.emplace_back();
            }
            rows[row].emplace_back(it.get());
        }
        for (std::size_t col = it->x, max = it->x + it->width; col < max; ++col) {
            while (col >= columns.size()) {
                columns.emplace_back();
            }
            columns[col].emplace_back(it.get());
        }

        connect(it->display.get(), &Display::interactiveRepaint, this,
                &DisplayLayout::interactiveRepaint);
        connect(it->display.get(), &Display::outputsChanged, this, &DisplayLayout::outputsChanged);
        connect(it->display.get(), &Display::timeRangeSelected, this,
                &DisplayLayout::timeRangeSelected);
        connect(it->display.get(), &Display::internalZoom, this, &DisplayLayout::internalZoom);
        connect(it->display.get(), &Display::setMouseTracking, this,
                &DisplayLayout::setMouseTracking);
        connect(it->display.get(), &Display::setMouseGrab, this, &DisplayLayout::setMouseGrab);
        connect(it->display.get(), &Display::setKeyboardGrab, this,
                &DisplayLayout::setKeyboardGrab);
        connect(it->display.get(), &Display::setToolTip, this, &DisplayLayout::setToolTip);
        connect(it->display.get(), &Display::readyForFinalPaint, this,
                &DisplayLayout::elementReadyForFinalPaint);
    }

    for (auto &it : rows) {
        std::stable_sort(it.begin(), it.end(), displayElementRowSort);
    }
    for (auto &it : columns) {
        std::stable_sort(it.begin(), it.end(), displayElementColumnSort);
    }
}

void DisplayLayout::elementReadyForFinalPaint()
{
    elementsReady++;
    if (elementsReady >= elements.size()) {
        emit readyForFinalPaint();
    }
}

bool DisplayLayout::mergable(const Variant::Read &a, const Variant::Read &b)
{
    std::unordered_map<Variant::PathElement, Variant::Read> childrenA;
    {
        auto children = a.toChildren();
        for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
            childrenA.emplace(add.key(), add.value());
        }
        if (childrenA.empty()) {
            childrenA.emplace(Variant::PathElement(), a);
        }
    }
    std::unordered_map<Variant::PathElement, Variant::Read> childrenB;
    {
        auto children = b.toChildren();
        for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
            childrenB.emplace(add.key(), add.value());
        }
        if (childrenB.empty()) {
            childrenB.emplace(Variant::PathElement(), a);
        }
    }

    /* This is stricter than it really needs to be, but it's the simplest
     * way to catch things like Cycle -> None -> Timeseries */

    if (childrenA.size() != childrenB.size())
        return false;

    for (const auto &childA : childrenA) {
        auto childB = childrenB.find(childA.first);
        if (childB == childrenB.end())
            return false;

        /* Maybe we should check the layout? */

        const auto &typeA = childA.second.hash("Type").toString();
        const auto &typeB = childB->second.hash("Type").toString();

        if (Util::equal_insensitive(typeA, "layout")) {
            if (!Util::equal_insensitive(typeB, "layout"))
                return false;
            if (!mergable(childA.second, childB->second))
                return false;
        } else if (Util::equal_insensitive(typeA, "timeseries", "timeseries2d")) {
            if (!Util::equal_insensitive(typeB, "timeseries", "timeseries2d"))
                return false;
            if (!TimeSeries2D::mergable(childA.second, childB->second))
                return false;
        } else if (Util::equal_insensitive(typeA, "cycle", "cycle2d")) {
            if (!Util::equal_insensitive(typeB, "cycle", "cycle2d"))
                return false;
            if (!CyclePlot2D::mergable(childA.second, childB->second))
                return false;
        } else {
            if (Util::equal_insensitive(typeB, "layout", "timeseries", "timeseries2d", "cycle",
                                        "cycle2d"))
                return false;
            if (!Graph2D::mergable(childA.second, childB->second))
                return false;
        }
    }

    return true;
}

/**
 * Initialize the layout.
 * <br>
 * Must be called (even by children) before any other functions are called.
 * 
 * @param config    the configuration
 * @param defaults  the graph defaults
 */
void DisplayLayout::initialize(const ValueSegment::Transfer &config,
                               const DisplayDefaults &defaults)
{
    std::map<Variant::PathElement, ValueSegment::Transfer, Variant::PathElement::OrderLogical>
            childConfiguration;
    for (const auto &add : config) {
        bool added = false;
        if (add["Type"].toString().empty()) {
            auto children = add.read().toChildren();
            for (auto c = children.begin(), endC = children.end(); c != endC; ++c) {
                childConfiguration[c.key()].emplace_back(
                        ValueSegment(add.getStart(), add.getEnd(), Variant::Root(c.value())));
                added = true;
            }
        }
        if (!added) {
            childConfiguration[Variant::PathElement()].emplace_back(add);
        }
    }

    for (const auto &child : childConfiguration) {
        Variant::Root merged;
        {
            std::vector<Variant::Root> toMerge;
            for (auto &add : child.second) {
                toMerge.emplace_back(std::move(add.root()));
            }
            merged = Variant::Root::overlay(toMerge.begin(), toMerge.end());
        }
        if (!merged.read().exists())
            continue;

        std::unique_ptr<Element> element(new Element);
        element->name = QString::fromStdString(child.first.toString());

        if (INTEGER::defined(merged["Layout/X"].toInt64())) {
            element->x = (int) merged["Layout/X"].toInt64();
            if (element->x < 0)
                element->x = columnStretch.size();
        }
        if (INTEGER::defined(merged["Layout/Y"].toInt64())) {
            element->y = (int) merged["Layout/Y"].toInt64();
            if (element->y < 0)
                element->y = rowStretch.size();
        } else {
            element->y = rowStretch.size();
        }
        if (INTEGER::defined(merged["Layout/Width"].toInt64())) {
            element->width = (int) merged["Layout/Width"].toInt64();
            if (element->width < 1)
                element->width = 1;
        }
        if (INTEGER::defined(merged["Layout/Height"].toInt64())) {
            element->height = (int) merged["Layout/Height"].toInt64();
            if (element->height < 1)
                element->height = 1;
        }
        while (static_cast<int>(rowStretch.size()) < element->y + element->height) {
            rowStretch.emplace_back(1);
        }
        while (static_cast<int>(columnStretch.size()) < element->x + element->width) {
            columnStretch.emplace_back(1);
        }

        if (INTEGER::defined(merged["Layout/MinimumWidth"].toInt64())) {
            element->minimumSize.rwidth() = (int) merged["Layout/MinimumWidth"].toInt64();
            if (element->minimumSize.rwidth() < 0)
                element->minimumSize.rwidth() = 1;
        }
        if (INTEGER::defined(merged["Layout/MinimumHeight"].toInt64())) {
            element->minimumSize.rheight() = (int) merged["Layout/MinimumHeight"].toInt64();
            if (element->minimumSize.rheight() < 0)
                element->minimumSize.rheight() = 1;
        }
        if (INTEGER::defined(merged["Layout/MaximumWidth"].toInt64())) {
            element->maximumSize.rwidth() = (int) merged["Layout/MaximumWidth"].toInt64();
            if (element->maximumSize.rwidth() < 0)
                element->maximumSize.rwidth() = 1;
        }
        if (INTEGER::defined(merged["Layout/MaximumHeight"].toInt64())) {
            element->maximumSize.rheight() = (int) merged["Layout/MaximumHeight"].toInt64();
            if (element->maximumSize.rheight() < 0)
                element->maximumSize.rheight() = 1;
        }

        if (merged["Layout/Stretch/X"].exists()) {
            if (merged["Layout/Stretch/X"].getType() == Variant::Type::Array) {
                auto stretches = merged["Layout/Stretch/X"].toArray();
                for (std::size_t i = 0,
                        max = std::min<std::size_t>(stretches.size(), element->width);
                        i < max;
                        i++) {
                    int stretch = (int) stretches[i].toInt64();
                    if (stretch > 0) {
                        columnStretch[element->x + i] =
                                qMax(stretch, columnStretch[element->x + i]);
                        while (i > element->columnStretch.size()) {
                            element->columnStretch.emplace_back(1);
                        }
                        element->columnStretch[i] = stretch;
                    }
                }
            } else if (INTEGER::defined(merged["Layout/Stretch/X"].toInt64())) {
                int stretch = (int) merged["Layout/Stretch/X"].toInt64();
                if (stretch > 0) {
                    columnStretch[element->x] = qMax(columnStretch[element->x], stretch);
                    if (element->columnStretch.empty())
                        element->columnStretch.emplace_back(1);
                    element->columnStretch[0] = stretch;
                }
            }
        }
        if (merged["Layout/Stretch/Y"].exists()) {
            if (merged["Layout/Stretch/Y"].getType() == Variant::Type::Array) {
                auto stretches = merged["Layout/Stretch/Y"].toArray();
                for (std::size_t i = 0,
                        max = std::min<std::size_t>(stretches.size(), element->height);
                        i < max;
                        i++) {
                    int stretch = (int) stretches[i].toInt64();
                    if (stretch > 0) {
                        rowStretch[element->y + i] = qMax(stretch, rowStretch[element->y + i]);
                        while (i > element->rowStretch.size()) {
                            element->rowStretch.emplace_back(1);
                        }
                        element->rowStretch[i] = stretch;
                    }
                }
            } else if (INTEGER::defined(merged["Layout/Stretch/Y"].toInt64())) {
                int stretch = (int) merged["Layout/Stretch/Y"].toInt64();
                if (stretch > 0) {
                    rowStretch[element->y] = qMax(rowStretch[element->y], stretch);
                    if (element->rowStretch.empty())
                        element->rowStretch.emplace_back(1);
                    element->rowStretch[0] = stretch;
                }
            }
        }

        const auto &type = merged["Type"].toString();
        if (Util::equal_insensitive(type, "layout")) {
            std::unique_ptr<DisplayLayout> add(new DisplayLayout(defaults, this));
            add->initialize(child.second, defaults);
            element->display = std::move(add);
        } else if (Util::equal_insensitive(type, "timeseries", "timeseries2d")) {
            std::unique_ptr<TimeSeries2D> add(new TimeSeries2D(defaults, this));
            add->initialize(child.second, defaults);
            element->display = std::move(add);
        } else if (Util::equal_insensitive(type, "cycle", "cycle2d")) {
            std::unique_ptr<CyclePlot2D> add(new CyclePlot2D(defaults, this));
            add->initialize(child.second, defaults);
            element->display = std::move(add);
        } else {
            std::unique_ptr<Graph2D> add(new Graph2D(defaults, this));
            add->initialize(child.second, defaults);
            element->display = std::move(add);
        }

        elements.emplace_back(std::move(element));
    }

    setBreakdowns();
}


void DisplayLayout::registerChain(ProcessingTapChain *chain)
{
    elementsReady = 0;
    if (elements.empty()) {
        emit readyForFinalPaint();
        return;
    }
    for (const auto &it : elements) {
        it->display->registerChain(chain);
    }
}

std::vector<std::shared_ptr<
        DisplayOutput>> DisplayLayout::getOutputs(const DisplayDynamicContext &context)
{
    std::vector<std::shared_ptr<DisplayOutput>> result;
    for (const auto &element : elements) {
        Util::append(element->display->getOutputs(context), result);
    }
    return result;
}

QSizeF DisplayLayout::getMinimumSize(QPaintDevice *paintdevice,
                                     const DisplayDynamicContext &context)
{
    if (!haveEverPrepared)
        prepareLayout(QRectF(), paintdevice, context);
    if (!haveEverPredicted)
        predictLayout(QRectF(), paintdevice, context);

    std::vector<qreal> columnWidths;
    std::vector<qreal> rowHeights;

    for (const auto &element : elements) {
        QSizeF size(element->minimumSize);
        size = size.expandedTo(element->display->getMinimumSize(paintdevice, context));

        if (size.width() > 0) {
            int localWeight = 0;
            for (int i = element->x, max = element->x + element->width; i < max; i++) {
                localWeight += columnStretch.at(i);
            }
            if (localWeight > 0) {
                for (std::size_t i = element->x, max = element->x + element->width; i < max; i++) {
                    while (i >= columnWidths.size()) {
                        columnWidths.emplace_back(0);
                    }
                    columnWidths[i] = std::max<qreal>(columnWidths[i], size.width() *
                            ((qreal) columnStretch[i] / (qreal) localWeight));
                }
            }
        }

        if (size.height() > 0) {
            int localWeight = 0;
            for (std::size_t i = element->y, max = element->y + element->height; i < max; i++) {
                localWeight += rowStretch[i];
            }
            if (localWeight > 0) {
                for (std::size_t i = element->y, max = element->y + element->height; i < max; i++) {
                    while (i >= rowHeights.size()) {
                        rowHeights.emplace_back(0);
                    }
                    rowHeights[i] = std::max<qreal>(rowHeights[i], size.height() *
                            ((qreal) rowStretch.at(i) / (qreal) localWeight));
                }
            }
        }
    }

    qreal width = 0;
    qreal height = 0;
    for (auto add : columnWidths) {
        width += add;
    }
    for (auto add : rowHeights) {
        height += add;
    }
    return QSizeF(width, height);
}

static void applyCoupling(std::deque<std::shared_ptr<DisplayCoupling>> &coupling)
{
    while (!coupling.empty()) {
        std::shared_ptr<DisplayCoupling> base(coupling.front());
        coupling.pop_front();
        std::vector<std::shared_ptr<DisplayCoupling>> components;
        components.emplace_back(base);

        for (auto it = coupling.begin(); it != coupling.end();) {
            if ((*it)->canCouple(base)) {
                components.emplace_back(*it);
                it = coupling.erase(it);
                continue;
            }
            ++it;
        }

        QVariant data(base->combine(components));
        for (std::size_t i = 0, max = components.size() - 1; i <= max; i++) {
            if (i == 0) {
                if (i == max)
                    components[i]->applyCoupling(data, DisplayCoupling::Only);
                else
                    components[i]->applyCoupling(data, DisplayCoupling::First);
            } else if (i == max) {
                components[i]->applyCoupling(data, DisplayCoupling::Last);
            } else {
                components[i]->applyCoupling(data, DisplayCoupling::Middle);
            }
        }
    }
}

void DisplayLayout::prepareLayout(const QRectF &maximumDimensions,
                                  QPaintDevice *paintdevice,
                                  const DisplayDynamicContext &context)
{
    havePrepared = true;
    haveEverPrepared = true;

    for (const auto &it : elements) {
        it->display->prepareLayout(maximumDimensions, paintdevice, context);
        it->dataCoupling = it->display->getDataCoupling();
    }

    for (const auto &row : rows) {
        std::deque<std::shared_ptr<DisplayCoupling>> coupling;
        for (auto it : row) {
            for (const auto &add : it->dataCoupling) {
                if (add->getMode() != DisplayCoupling::SameRow)
                    continue;
                coupling.emplace_back(add);
            }
        }
        applyCoupling(coupling);
    }

    for (const auto &column : columns) {
        std::deque<std::shared_ptr<DisplayCoupling>> coupling;
        for (auto it : column) {
            for (const auto &add : it->dataCoupling) {
                if (add->getMode() != DisplayCoupling::SameColumn)
                    continue;
                coupling.emplace_back(add);
            }
        }
        applyCoupling(coupling);
    }

    std::deque<std::shared_ptr<DisplayCoupling>> coupling;
    for (const auto &element : elements) {
        for (const auto &add : element->dataCoupling) {
            if (add->getMode() != DisplayCoupling::Always)
                continue;
            coupling.emplace_back(add);
        }
    }
    applyCoupling(coupling);
}

void DisplayLayout::predictLayout(const QRectF &maximumDimensions,
                                  QPaintDevice *paintdevice,
                                  const DisplayDynamicContext &context)
{
    if (!havePrepared)
        prepareLayout(maximumDimensions, paintdevice, context);

    havePredicted = true;
    haveEverPredicted = true;

    for (const auto &it : elements) {
        it->display->predictLayout(maximumDimensions, paintdevice, context);
        it->layoutCoupling = it->display->getLayoutCoupling();
    }

    for (const auto &row : rows) {
        std::deque<std::shared_ptr<DisplayCoupling>> coupling;
        for (auto it : row) {
            for (const auto &add : it->layoutCoupling) {
                if (add->getMode() != DisplayCoupling::SameRow)
                    continue;
                coupling.emplace_back(add);
            }
        }
        applyCoupling(coupling);
    }

    for (const auto &column : columns) {
        std::deque<std::shared_ptr<DisplayCoupling>> coupling;
        for (auto it : column) {
            for (const auto &add : it->layoutCoupling) {
                if (add->getMode() != DisplayCoupling::SameColumn)
                    continue;
                coupling.emplace_back(add);
            }
        }
        applyCoupling(coupling);
    }

    std::deque<std::shared_ptr<DisplayCoupling>> coupling;
    for (const auto &element : elements) {
        for (const auto &add : element->layoutCoupling) {
            if (add->getMode() != DisplayCoupling::Always)
                continue;
            coupling.emplace_back(add);
        }
    }
    applyCoupling(coupling);
}

void DisplayLayout::setPositions(const QRectF &dimensions)
{
    std::vector<qreal> columnWidths;
    std::vector<qreal> rowHeights;

    /* Set simple fixed scaling. */
    int totalRow = 0;
    int totalColumn = 0;
    for (auto row : rowStretch) {
        totalRow += row;
        rowHeights.emplace_back(0);
    }
    for (auto col : columnStretch) {
        totalColumn += col;
        columnWidths.emplace_back(0);
    }
    if (totalRow > 0) {
        for (std::size_t i = 0, max = rowHeights.size(); i < max; i++) {
            rowHeights[i] = dimensions.height() * ((qreal) rowStretch.at(i) / (qreal) totalRow);
        }
    }
    if (totalColumn > 0) {
        for (std::size_t i = 0, max = columnWidths.size(); i < max; i++) {
            columnWidths[i] =
                    dimensions.width() * ((qreal) columnStretch.at(i) / (qreal) totalColumn);
        }
    }

    /* Apply minimum and maximum sizes. */
    qreal freeWidth = 0;
    qreal freeHeight = 0;
    std::vector<bool> boundRows(rowHeights.size(), false);
    std::vector<bool> boundColumns(columnWidths.size(), false);
    for (const auto &element : elements) {
        if (element->minimumSize.width() > 0 || element->maximumSize.width() > 0) {
            Q_ASSERT(element->x + element->width <= static_cast<int>(columnWidths.size()));

            qreal currentWidth = 0;
            for (int i = element->x, max = element->x + element->width; i < max; i++) {
                currentWidth += columnWidths.at(i);
            }

            qreal deltaWidth = 0;
            if (element->maximumSize.width() > 0 && currentWidth > element->maximumSize.width()) {
                deltaWidth = element->maximumSize.width() - currentWidth;
                currentWidth = element->maximumSize.width();
            }
            if (element->minimumSize.width() > 0 && currentWidth < element->minimumSize.width()) {
                deltaWidth = element->minimumSize.width() - currentWidth;
                currentWidth = element->minimumSize.width();
            }

            if (deltaWidth != 0.0) {
                int localWeight = 0;
                for (int i = element->x, max = element->x + element->width; i < max; i++) {
                    localWeight += columnStretch[i];
                    boundColumns[i] = true;
                }
                freeWidth += deltaWidth;

                if (localWeight > 0) {
                    for (int i = element->x, max = element->x + element->width; i < max; i++) {
                        columnWidths[i] +=
                                deltaWidth * ((qreal) columnStretch[i] / (qreal) localWeight);
                    }
                }
            }
        }

        if (element->minimumSize.height() > 0 || element->maximumSize.height() > 0) {
            Q_ASSERT(element->y + element->height <= static_cast<int>(rowHeights.size()));

            qreal currentHeight = 0;
            for (int i = element->y, max = element->y + element->height; i < max; i++) {
                currentHeight += rowHeights[i];
            }

            qreal deltaHeight = 0;
            if (element->maximumSize.height() > 0 &&
                    currentHeight > element->maximumSize.height()) {
                deltaHeight = element->maximumSize.height() - currentHeight;
                currentHeight = element->maximumSize.height();
            }
            if (element->minimumSize.height() > 0 &&
                    currentHeight < element->minimumSize.height()) {
                deltaHeight = element->minimumSize.height() - currentHeight;
                currentHeight = element->minimumSize.height();
            }

            if (deltaHeight != 0.0) {
                int localWeight = 0;
                for (int i = element->y, max = element->y + element->height; i < max; i++) {
                    localWeight += rowStretch[i];
                    boundRows[i] = true;
                }
                freeHeight += deltaHeight;

                if (localWeight > 0) {
                    for (int i = element->y, max = element->y + element->height; i < max; i++) {
                        rowHeights[i] +=
                                deltaHeight * ((qreal) rowStretch[i] / (qreal) localWeight);
                    }
                }
            }
        }
    }

    /* Allocate or acquire the space deficit. */
    if (freeWidth != 0.0) {
        int unboundWeight = 0;
        for (std::size_t i = 0, max = boundColumns.size(); i < max; i++) {
            if (boundColumns[i])
                continue;
            unboundWeight += columnStretch[i];
        }

        if (unboundWeight > 0) {
            for (std::size_t i = 0, max = boundColumns.size(); i < max; i++) {
                if (boundColumns[i])
                    continue;
                qreal altered = columnWidths[i] +
                        freeWidth * ((qreal) columnStretch[i] / (qreal) totalColumn);
                if (altered < 0.0) altered = 0;
                columnWidths[i] = altered;
            }
        }
    }
    if (freeHeight != 0.0) {
        int unboundWeight = 0;
        for (std::size_t i = 0, max = boundRows.size(); i < max; i++) {
            if (boundRows[i])
                continue;
            unboundWeight += rowStretch[i];
        }

        if (unboundWeight > 0) {
            for (std::size_t i = 0, max = boundRows.size(); i < max; i++) {
                if (boundRows[i])
                    continue;
                qreal altered =
                        rowHeights[i] + freeHeight * ((qreal) rowStretch[i] / (qreal) totalColumn);
                if (altered < 0.0) altered = 0;
                rowHeights[i] = altered;
            }
        }
    }

    /* Set sizes */
    for (const auto &element : elements) {
        qreal x = dimensions.x();
        qreal y = dimensions.y();

        for (int i = 0; i < element->x; i++) {
            x += columnWidths.at(i);
        }
        for (int i = 0; i < element->y; i++) {
            y += rowHeights.at(i);
        }

        qreal width = columnWidths[element->x];
        qreal height = rowHeights[element->y];
        for (int i = element->x + 1, max = element->x + element->width; i < max; i++) {
            width += columnWidths[i];
        }
        for (int i = element->y + 1, max = element->y + element->height; i < max; i++) {
            height += rowHeights[i];
        }
        if (width < 1) width = 1;
        if (height < 1) height = 1;

        element->position = QRectF(x, y, width, height);
    }
}

void DisplayLayout::paint(QPainter *painter,
                          const QRectF &dimensions,
                          const DisplayDynamicContext &context)
{
    if (!havePrepared)
        prepareLayout(dimensions, painter->device(), context);
    if (!havePredicted)
        predictLayout(dimensions, painter->device(), context);

    havePrepared = false;
    havePredicted = false;

    setPositions(dimensions);

    for (const auto &element : elements) {
        element->display->paint(painter, element->position, context);
    }
}

bool DisplayLayout::eventMousePress(QMouseEvent *event)
{
    for (const auto &element : elements) {
        if (element->display->eventMousePress(event))
            return true;
    }
    return false;
}

bool DisplayLayout::eventMouseRelease(QMouseEvent *event)
{
    for (const auto &element : elements) {
        if (element->display->eventMouseRelease(event))
            return true;
    }
    return false;
}

bool DisplayLayout::eventMouseMove(QMouseEvent *event)
{
    for (const auto &element : elements) {
        if (element->display->eventMouseMove(event))
            return true;
    }
    return false;
}

bool DisplayLayout::eventKeyPress(QKeyEvent *event)
{
    for (const auto &element : elements) {
        if (element->display->eventKeyPress(event))
            return true;
    }
    return false;
}

bool DisplayLayout::eventKeyRelease(QKeyEvent *event)
{
    for (const auto &element : elements) {
        if (element->display->eventKeyRelease(event))
            return true;
    }
    return false;
}

std::shared_ptr<
        DisplayModificationComponent> DisplayLayout::getMouseModification(const QPoint &point,
                                                                          const QRectF &dimensions,
                                                                          const DisplayDynamicContext &context)
{
    if (!havePrepared)
        prepareLayout(dimensions, nullptr, context);
    if (!havePredicted)
        predictLayout(dimensions, nullptr, context);

    setPositions(dimensions);

    for (const auto &element : elements) {
        if (!element->position.contains(point))
            continue;
        auto result = element->display->getMouseModification(point, dimensions, context);
        if (result)
            return result;
    }

    return std::shared_ptr<DisplayModificationComponent>();
}

void DisplayLayout::setMouseover(const QPoint &point)
{
    for (const auto &element : elements) {
        if (!element->position.contains(point))
            continue;
        if (priorMouseover && element.get() != priorMouseover) {
            priorMouseover->display->clearMouseover();
            priorMouseover = nullptr;
        }
        if (!priorMouseover) {
            priorMouseover = element.get();
            priorMouseover->display->setMouseover(point);
            return;
        }
    }
    if (priorMouseover) {
        priorMouseover->display->clearMouseover();
        priorMouseover = nullptr;
    }
}

std::vector<std::shared_ptr<DisplayZoomAxis>> DisplayLayout::zoomAxes()
{
    std::vector<std::shared_ptr<DisplayZoomAxis> > result;
    for (const auto &element : elements) {
        Util::append(element->display->zoomAxes(), result);
    }
    return result;
}

namespace {

class DisplayLayoutHighlightController : public DisplayHighlightController {
    std::vector<std::shared_ptr<DisplayHighlightController> > children;
public:
    DisplayLayoutHighlightController(std::vector<std::shared_ptr<DisplayHighlightController> > set)
            : children(std::move(set))
    { }

    virtual ~DisplayLayoutHighlightController()
    { }

    void clear() override
    {
        for (const auto &it : children) {
            it->clear();
        }
    }

    void add(HighlightType type,
             double start,
             double end,
             double min = FP::undefined(),
             double max = FP::undefined(),
             const SequenceMatch::Composite &selection = SequenceMatch::Composite(
                     SequenceMatch::Element::SpecialMatch::All)) override
    {
        for (const auto &it : children) {
            it->add(type, start, end, min, max, selection);
        }
    }
};

}

std::shared_ptr<DisplayHighlightController> DisplayLayout::highlightController()
{
    std::vector<std::shared_ptr<DisplayHighlightController>> set;
    for (const auto &element : elements) {
        auto add = element->display->highlightController();
        if (!add)
            continue;
        set.emplace_back(std::move(add));
    }
    return std::shared_ptr<DisplayHighlightController>(
            new DisplayLayoutHighlightController(std::move(set)));
}

void DisplayLayout::saveOverrides(DisplaySaveContext &target)
{
    for (const auto &element : elements) {
        DisplaySaveContext childTarget;
        element->display->saveOverrides(childTarget);
        target.overlay(childTarget.result(), element->name);
    }
}

bool DisplayLayout::acceptsKeyboardInput() const
{
    for (const auto &element : elements) {
        if (element->display->acceptsKeyboardInput())
            return true;
    }
    return false;
}

bool DisplayLayout::acceptsMouseover() const
{
    for (const auto &element : elements) {
        if (element->display->acceptsMouseover())
            return true;
    }
    return false;
}

void DisplayLayout::resetOverrides()
{
    for (const auto &element : elements) {
        element->display->resetOverrides();
    }
}

void DisplayLayout::trimData(double start, double end)
{
    for (const auto &element : elements) {
        element->display->trimData(start, end);
    }
}

void DisplayLayout::setVisibleTimeRange(double start, double end)
{
    for (const auto &element : elements) {
        element->display->setVisibleTimeRange(start, end);
    }
}

void DisplayLayout::interactionStopped()
{
    for (const auto &element : elements) {
        element->display->interactionStopped();
    }
}

void DisplayLayout::clearMouseover()
{
    for (const auto &element : elements) {
        element->display->clearMouseover();
    }
    priorMouseover = nullptr;
}

}
}
