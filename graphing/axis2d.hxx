/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGAXIS2D_H
#define CPD3GRAPHINGAXIS2D_H

#include "core/first.hxx"

#include <memory>
#include <QtGlobal>
#include <QObject>

#include "graphing/graphing.hxx"
#include "graphing/axisdimension.hxx"
#include "graphing/graphcommon2d.hxx"
#include "graphing/tracecommon.hxx"
#include "graphing/display.hxx"
#include "graphing/axislabelengine2d.hxx"

namespace CPD3 {
namespace Graphing {

class AxisParameters2D;

class Graph2D;

/**
 * The common parameters for an axis on a two dimensional graph.
 */
class CPD3GRAPHING_EXPORT AxisParameters2D : public AxisParameters {
    enum {
        Component_TitleText = 0x00000001,
        Component_TitleFont = 0x00000002,
        Component_TitleColor = 0x00000004,
        Component_BaselineWidth = 0x00000008,
        Component_BaselineStyle = 0x00000010,
        Component_BaselineColor = 0x00000020,
        Component_DrawPriority = 0x00000040,
        Component_DrawEnable = 0x00000080,
        Component_ToolTipUnits = 0x00000100,
    };
    int components;
    QString titleText;
    QFont titleFont;
    QColor titleColor;
    qreal baselineWidth;
    Qt::PenStyle baselineStyle;
    QColor baselineColor;
    int drawPriority;
    bool drawEnable;
    QString toolTipUnits;

    struct TickLevel {
        TickLevel();

        TickLevel(const TickLevel &other);

        TickLevel &operator=(const TickLevel &other);

        TickLevel(TickLevel &&other);

        TickLevel &operator=(TickLevel &&other);

        TickLevel(const AxisParameters2D &parent, std::size_t level);

        TickLevel(const AxisParameters2D &parent,
                  const Data::Variant::Read &value,
                  std::size_t level);

        enum {
            Component_LineEnable = 0x0001,
            Component_LineSize = 0x0002,
            Component_LineWidth = 0x0004,
            Component_LineStyle = 0x0008,
            Component_LineColor = 0x0010,
            Component_LineDrawPriority = 0x0020,
            Component_LabelEnable = 0x0040,
            Component_LabelFont = 0x0080,
            Component_LabelColor = 0x0100,
            Component_LabelDrawPriority = 0x0200,
        };
        int components;
        bool lineEnable;
        qreal lineSize;
        qreal lineWidth;
        Qt::PenStyle lineStyle;
        QColor lineColor;
        int lineDrawPriority;
        bool labelEnable;
        QFont labelFont;
        QColor labelColor;
        int labelDrawPriority;

        void save(Data::Variant::Write &value) const;

        void clear();

        void overlay(const TickLevel &over);
    };

    std::vector<TickLevel> ticks;
public:
    AxisParameters2D();

    virtual ~AxisParameters2D();

    AxisParameters2D(const AxisParameters2D &other);

    AxisParameters2D &operator=(const AxisParameters2D &other);

    AxisParameters2D(AxisParameters2D &&other);

    AxisParameters2D &operator=(AxisParameters2D &&other);

    AxisParameters2D(const Data::Variant::Read &value, QSettings *settings = 0);

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<AxisParameters> clone() const override;

    void overlay(const AxisParameters *over) override;

    void copy(const AxisParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify a title.
     * @return true if the parameters specify a title
     */
    inline bool hasTitleText() const
    { return components & Component_TitleText; }

    /**
     * Get the axis title.
     * @return the axis title
     */
    inline QString getTitleText() const
    { return titleText; }

    /**
     * Set the axis title text.  This does not set the defined flag.
     * @param text  the new title text
     */
    inline void setTitleText(const QString &text)
    { titleText = text; }

    /**
     * Override the axis title.  This sets the defined flag.
     * @param text  the new title text
     */
    inline void overrideTitleText(const QString &text)
    {
        titleText = text;
        components |= Component_TitleText;
    }

    /**
     * Test if the parameters specify a title font.
     * @return true if the parameters specify a title font
     */
    inline bool hasTitleFont() const
    { return components & Component_TitleFont; }

    /**
     * Get the axis title font.
     * @return the axis title font
     */
    inline QFont getTitleFont() const
    { return titleFont; }

    /**
     * Override the axis title font.  This sets the defined flag.
     * @param font  the new title font
     */
    inline void overrideTitleFont(const QFont &font)
    {
        titleFont = font;
        components |= Component_TitleFont;
    }

    /**
     * Test if the parameters specify a title color.
     * @return true if the parameters specify a title color
     */
    inline bool hasTitleColor() const
    { return components & Component_TitleColor; }

    /**
     * Get the axis title color.
     * @return the axis title color
     */
    inline QColor getTitleColor() const
    { return titleColor; }

    /**
     * Set the axis title color.  This does not set the defined flag.
     * @param color  the new title color
     */
    inline void setTitleColor(const QColor &color)
    { titleColor = color; }

    /**
     * Override the axis title color.  This sets the defined flag.
     * @param color  the new title color
     */
    inline void overrideTitleColor(const QColor &color)
    {
        titleColor = color;
        components |= Component_TitleColor;
    }

    /**
     * Test if the parameters specify a baseline width.
     * @return true if the parameters specify a baseline width
     */
    inline bool hasBaselineWidth() const
    { return components & Component_BaselineWidth; }

    /**
     * Get the baseline width.
     * @return the baseline width
     */
    inline qreal getBaselineWidth() const
    { return baselineWidth; }

    /**
     * Test if the parameters specify a baseline width.
     * @return true if the parameters specify a baseline width
     */
    inline bool hasBaselineStyle() const
    { return components & Component_BaselineStyle; }

    /**
     * Get the baseline width.
     * @return the baseline width
     */
    inline Qt::PenStyle getBaselineStyle() const
    { return baselineStyle; }

    /**
     * Test if the parameters specify a baseline color.
     * @return true if the parameters specify a baseline color
     */
    inline bool hasBaselineColor() const
    { return components & Component_BaselineColor; }

    /**
     * Get the baseline color.
     * @return the baseline color
     */
    inline QColor getBaselineColor() const
    { return baselineColor; }

    /**
     * Test if the parameters specify a draw sort priority.
     * @return true if the parameters specify a draw priority
     */
    inline bool hasDrawPriority() const
    { return components & Component_DrawPriority; }

    /**
     * Get the draw sorting priority.
     * @return the draw priority
     */
    inline int getDrawPriority() const
    { return drawPriority; }

    /**
     * Test if the parameters specify a draw enable state.
     * @return true if the parameters specify a draw enable state
     */
    inline bool hasDrawEnable() const
    { return components & Component_DrawEnable; }

    /**
     * Test if the axis should be drawn at all
     * @return true if the axis should be drawn
     */
    inline int getDrawEnable() const
    { return drawEnable; }

    /**
     * Test if the parameters specify a tooltip unit override.
     * @return true if the parameters specify a tooltip unit override
     */
    inline bool hasToolTipUnits() const
    { return components & Component_ToolTipUnits; }

    /**
     * Get the tooltip unit override.
     * @return the tooltip unit override string
     */
    inline QString getToolTipUnits() const
    { return toolTipUnits; }

    /**
     * Test if the parameters specify a tick line draw enable state.
     * @param level the level
     * @return true if the parameters specify a tick line draw enable state
     */
    inline bool hasTickLineEnable(std::size_t level) const
    {
        if (level >= ticks.size()) return false;
        return ticks.at(level).components & TickLevel::Component_LineEnable;
    }

    /**
     * Test if the tick lines should be drawn.
     * @param level the level
     * @return true if the tick lines should be drawn
     */
    inline bool getTickLineEnable(std::size_t level) const
    {
        if (ticks.empty()) return false;
        if (level >= ticks.size()) level = ticks.size() - 1;
        return ticks.at(level).lineEnable;
    }

    /**
     * Test if the parameters specify a tick size.
     * @param level the level
     * @return true if the parameters specify a tick size
     */
    inline bool hasTickLineSize(std::size_t level) const
    {
        if (level >= ticks.size()) return false;
        return ticks.at(level).components & TickLevel::Component_LineSize;
    }

    /**
     * Get the tick size.
     * @param level the level
     * @return the tick size
     */
    inline qreal getTickLineSize(std::size_t level) const
    {
        if (ticks.empty()) return 0;
        if (level >= ticks.size()) level = ticks.size() - 1;
        return ticks.at(level).lineSize;
    }

    /**
     * Test if the parameters specify a tick line width.
     * @param level the level
     * @return true if the parameters specify a tick line width
     */
    inline bool hasTickLineWidth(std::size_t level) const
    {
        if (level >= ticks.size()) return false;
        return ticks.at(level).components & TickLevel::Component_LineWidth;
    }

    /**
     * Get the tick line width.
     * @param level the level
     * @return the tick line width
     */
    inline qreal getTickLineWidth(std::size_t level) const
    {
        if (ticks.empty()) return 0;
        if (level >= ticks.size()) level = ticks.size() - 1;
        return ticks.at(level).lineWidth;
    }

    /**
     * Test if the parameters specify a tick line style.
     * @param level the level
     * @return true if the parameters specify a tick line style
     */
    inline bool hasTickLineStyle(std::size_t level) const
    {
        if (level >= ticks.size()) return false;
        return ticks.at(level).components & TickLevel::Component_LineWidth;
    }

    /**
     * Get the tick line style.
     * @param level the level
     * @return the tick line style
     */
    inline Qt::PenStyle getTickLineStyle(std::size_t level) const
    {
        if (ticks.empty()) return Qt::SolidLine;
        if (level >= ticks.size()) level = ticks.size() - 1;
        return ticks.at(level).lineStyle;
    }

    /**
     * Test if the parameters specify a tick line color.
     * @param level the level
     * @return true if the parameters specify a tick line color
     */
    inline bool hasTickLineColor(std::size_t level) const
    {
        if (level >= ticks.size()) return false;
        return ticks.at(level).components & TickLevel::Component_LineColor;
    }

    /**
     * Get the tick line color.
     * @param level the level
     * @return the tick line color
     */
    inline QColor getTickLineColor(std::size_t level) const
    {
        if (ticks.empty()) return QColor(0, 0, 0);
        if (level >= ticks.size()) level = ticks.size() - 1;
        return ticks.at(level).lineColor;
    }

    /**
     * Test if the parameters specify a tick line draw priority.
     * @param level the level
     * @return true if the parameters specify a tick line draw priority
     */
    inline bool hasTickLineDrawPriority(std::size_t level) const
    {
        if (level >= ticks.size()) return false;
        return ticks.at(level).components & TickLevel::Component_LineColor;
    }

    /**
     * Get the tick line draw priority.
     * @param level the level
     * @return the tick line draw priority
     */
    inline int getTickLineDrawPriority(std::size_t level) const
    {
        if (ticks.empty()) return Graph2DDrawPrimitive::Priority_Axes;
        if (level >= ticks.size()) level = ticks.size() - 1;
        return ticks.at(level).lineDrawPriority;
    }


    /**
     * Test if the parameters specify a tick label draw enable state.
     * @param level the level
     * @return true if the parameters specify a tick label draw enable state
     */
    inline bool hasTickLabelEnable(std::size_t level) const
    {
        if (level >= ticks.size()) return false;
        return ticks.at(level).components & TickLevel::Component_LabelEnable;
    }

    /**
     * Test if the tick labels should be drawn.
     * @param level the level
     * @return true if the tick labels should be drawn
     */
    inline bool getTickLabelEnable(std::size_t level) const
    {
        if (ticks.empty()) return false;
        if (level >= ticks.size()) level = ticks.size() - 1;
        return ticks.at(level).labelEnable;
    }

    /**
     * Override the the tick label enable state.  This sets the defined flag.
     * @param level     the level
     * @param enable    true to enable ticks
     */
    inline void overrideTickLabelEnable(std::size_t level, bool enable)
    {
        if (level >= ticks.size())
            return;
        auto &target = ticks[level];
        target.labelEnable = enable;
        target.components |= TickLevel::Component_LabelEnable;
    }

    /**
     * Test if the parameters specify a tick label font.
     * @param level the level
     * @return true if the parameters specify a tick label font
     */
    inline bool hasTickLabelFont(std::size_t level) const
    {
        if (level >= ticks.size()) return false;
        return ticks.at(level).components & TickLevel::Component_LabelFont;
    }

    /**
     * Get the tick label font.
     * @param level the level
     * @return the tick label font
     */
    inline QFont getTickLabelFont(std::size_t level) const
    {
        if (ticks.empty()) return QFont();
        if (level >= ticks.size()) level = ticks.size() - 1;
        return ticks.at(level).labelFont;
    }

    /**
     * Override the the tick label font.  This sets the defined flag.
     * @param level     the level
     * @param font      the tick font
     */
    inline void overrideTickLabelFont(std::size_t level, const QFont &font)
    {
        if (level >= ticks.size())
            return;
        auto &target = ticks[level];
        target.labelFont = font;
        target.components |= TickLevel::Component_LabelFont;
    }

    /**
     * Test if the parameters specify a tick label color.
     * @param level the level
     * @return true if the parameters specify a tick label color
     */
    inline bool hasTickLabelColor(std::size_t level) const
    {
        if (level >= ticks.size()) return false;
        return ticks.at(level).components & TickLevel::Component_LabelColor;
    }

    /**
     * Get the tick label color.
     * @param level the level
     * @return the tick label color
     */
    inline QColor getTickLabelColor(std::size_t level) const
    {
        if (ticks.empty()) return QColor(0, 0, 0);
        if (level >= ticks.size()) level = ticks.size() - 1;
        return ticks.at(level).labelColor;
    }

    /**
     * Override the the tick label color.  This sets the defined flag.
     * @param level     the level
     * @param color     the tick color
     */
    inline void overrideTickLabelColor(std::size_t level, const QColor &color)
    {
        if (level >= ticks.size())
            return;
        auto &target = ticks[level];
        target.labelColor = color;
        target.components |= TickLevel::Component_LabelColor;
    }

    /**
     * Test if the parameters specify a tick line draw priority.
     * @param level the level
     * @return true if the parameters specify a tick line draw priority
     */
    inline bool hasTickLabelDrawPriority(std::size_t level) const
    {
        if (level >= ticks.size()) return false;
        return ticks.at(level).components & TickLevel::Component_LabelDrawPriority;
    }

    /**
     * Get the tick line draw priority.
     * @param level the level
     * @return the tick line draw priority
     */
    inline int getTickLabelDrawPriority(std::size_t level) const
    {
        if (ticks.empty()) return Graph2DDrawPrimitive::Priority_Axes;
        if (level >= ticks.size()) level = ticks.size() - 1;
        return ticks.at(level).labelDrawPriority;
    }
};

class AxisParameters2DSide;

/**
 * The common parameters for a side (top, bottom, left, right) axis on a
 * two dimensional graph,
 */
class CPD3GRAPHING_EXPORT AxisParameters2DSide : public AxisParameters2D {
public:
    /** The side binding of the axis. */
    enum SideBindingState {
        /** Autoselect based on which side has fewer axes. */
        Side_Auto,

        /** Bind to the "primary" (left/bottom) side. */
        Side_Primary,

        /** Bind to the "secondary" (top/right) side. */
        Side_Secondary,
    };

    class FixedLine {
        double position;
        qreal lineWidth;
        Qt::PenStyle lineStyle;
        QColor color;
        int drawPriority;

        friend class AxisParameters2DSide;

    public:
        FixedLine();

        FixedLine(const FixedLine &other);

        FixedLine &operator=(const FixedLine &other);

        FixedLine(FixedLine &&other);

        FixedLine &operator=(FixedLine &&other);

        FixedLine(const Data::Variant::Read &value);

        void save(Data::Variant::Write &value) const;

        /**
         * Get the line position, in real space.
         * @return the line position
         */
        inline double getPosition() const
        { return position; }

        /**
         * Set the line position, in real space.
         * @param p the line position
         */
        inline void setPosition(double p)
        { position = p; }


        /**
         * Get the line width.
         * @return the line width
         */
        inline qreal getLineWidth() const
        { return lineWidth; }

        /**
         * Get the line style.
         * @return the line style
         */
        inline Qt::PenStyle getLineStyle() const
        { return lineStyle; }

        /**
         * Get the color.
         * @return the color
         */
        inline QColor getColor() const
        { return color; }

        /**
         * Get the drawPriority.
         * @return the drawPriority
         */
        inline int getDrawPriority() const
        { return drawPriority; }
    };

private:
    enum {
        Component_GridEnable = 0x00000001,
        Component_SideBinding = 0x00000002,
        Component_FixedLines = 0x00000004,
    };
    int components;
    bool gridEnable;
    SideBindingState sideBinding;

    struct GridLevel {
        GridLevel();

        GridLevel(const GridLevel &other);

        GridLevel &operator=(const GridLevel &other);

        GridLevel(GridLevel &&other);

        GridLevel &operator=(GridLevel &&other);

        GridLevel(const AxisParameters2DSide &parent, std::size_t level);

        GridLevel(const AxisParameters2DSide &parent,
                  const Data::Variant::Read &value,
                  std::size_t level);

        enum {
            Component_Enable = 0x0001,
            Component_LineWidth = 0x0002,
            Component_LineStyle = 0x0004,
            Component_Color = 0x0008,
            Component_DrawPriority = 0x0010,
        };
        int components;
        bool enable;
        qreal lineWidth;
        Qt::PenStyle lineStyle;
        QColor color;
        int drawPriority;

        void save(Data::Variant::Write &value) const;

        void clear();

        void overlay(const GridLevel &over);
    };

    std::vector<GridLevel> grid;
    std::vector<FixedLine> fixedLines;
public:
    AxisParameters2DSide();

    virtual ~AxisParameters2DSide();

    AxisParameters2DSide(const AxisParameters2DSide &other);

    AxisParameters2DSide &operator=(const AxisParameters2DSide &other);

    AxisParameters2DSide(AxisParameters2DSide &&other);

    AxisParameters2DSide &operator=(AxisParameters2DSide &&other);

    AxisParameters2DSide(const Data::Variant::Read &value, QSettings *settings = 0);

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<AxisParameters> clone() const override;

    void overlay(const AxisParameters *over) override;

    void copy(const AxisParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify a grid enable state.
     * @return true if the parameters specify a grid enable state
     */
    inline bool hasGridEnable() const
    { return components & Component_GridEnable; }

    /**
     * Test if the grid should be drawn.
     * @return true if the grid should be drawn
     */
    inline bool getGridEnable() const
    { return gridEnable; }

    /**
     * Override the grid enable state.  This sets the defined flag.
     * @param s     the new grid enable state
     */
    inline void overrideGridEnable(bool s)
    {
        gridEnable = s;
        components |= Component_GridEnable;
    }

    /**
     * Test if the parameters specify a side binding.
     * @return true if the parameters specify a side binding
     */
    inline bool hasSideBinding() const
    { return components & Component_SideBinding; }

    /**
     * Get the axis side binding.
     * @return the axis side binding
     */
    inline SideBindingState getSideBinding() const
    { return sideBinding; }

    /**
     * Override the side binding target.  This sets the defined flag.
     * @param binding   the new binding
     */
    inline void overrideSideBinding(SideBindingState binding)
    {
        sideBinding = binding;
        components |= Component_SideBinding;
    }

    /**
     * Test if the parameters specify a grid level draw enable state.
     * @param level the level
     * @return true if the parameters specify a grid level draw enable state
     */
    inline bool hasGridLevelEnable(std::size_t level) const
    {
        if (level >= grid.size()) return false;
        return grid.at(level).components & GridLevel::Component_Enable;
    }

    /**
     * Test if a level of the grid should be drawn.
     * @param level the level
     * @return true if the grid level should be drawn
     */
    inline bool getGridLevelEnable(std::size_t level) const
    {
        if (!gridEnable) return false;
        if (grid.empty()) return false;
        if (level >= grid.size()) level = grid.size() - 1;
        return grid.at(level).enable;
    }

    /**
     * Test if the parameters specify a grid level line width.
     * @param level the level
     * @return true if the parameters specify a grid level line width
     */
    inline bool hasGridLevelLineWidth(std::size_t level) const
    {
        if (level >= grid.size()) return false;
        return grid.at(level).components & GridLevel::Component_LineWidth;
    }

    /**
     * Get the line width of a grid level.
     * @param level the level
     * @return the line width of the grid level
     */
    inline qreal getGridLevelLineWidth(std::size_t level) const
    {
        if (grid.empty()) return 0;
        if (level >= grid.size()) level = grid.size() - 1;
        return grid.at(level).lineWidth;
    }

    /**
     * Test if the parameters specify a grid level line style.
     * @param level the level
     * @return true if the parameters specify a grid level line style
     */
    inline bool hasGridLevelLineStyle(std::size_t level) const
    {
        if (level >= grid.size()) return false;
        return grid.at(level).components & GridLevel::Component_LineStyle;
    }

    /**
     * Get the line style of a grid level.
     * @param level the level
     * @return the line style of the grid level
     */
    inline Qt::PenStyle getGridLevelLineStyle(std::size_t level) const
    {
        if (grid.empty()) return Qt::SolidLine;
        if (level >= grid.size()) level = grid.size() - 1;
        return grid.at(level).lineStyle;
    }

    /**
     * Test if the parameters specify a grid level color.
     * @param level the level
     * @return true if the parameters specify a grid level color
     */
    inline bool hasGridLevelColor(std::size_t level) const
    {
        if (level >= grid.size()) return false;
        return grid.at(level).components & GridLevel::Component_Color;
    }

    /**
     * Get the color of a grid level.
     * @param level the level
     * @return the color of the grid level
     */
    inline QColor getGridLevelColor(std::size_t level) const
    {
        if (grid.empty()) return QColor(0, 0, 0, 127);
        if (level >= grid.size()) level = grid.size() - 1;
        return grid.at(level).color;
    }

    /**
     * Test if the parameters specify a grid level draw priority.
     * @param level the level
     * @return true if the parameters specify a grid level draw priority
     */
    inline bool hasGridLevelDrawPriority(std::size_t level) const
    {
        if (level >= grid.size()) return false;
        return grid.at(level).components & GridLevel::Component_Color;
    }

    /**
     * Get the draw priority of a grid level.
     * @param level the level
     * @return the draw priority of the grid level
     */
    inline int getGridLevelDrawPriority(std::size_t level) const
    {
        if (grid.empty()) return Graph2DDrawPrimitive::Priority_Grid;
        if (level >= grid.size()) level = grid.size() - 1;
        return grid.at(level).drawPriority;
    }

    /**
     * Test if the parameters specify fixed lines.
     * @return true if the parameters specify fixed lines
     */
    inline bool hasFixedLines() const
    { return components & Component_FixedLines; }

    /**
     * Get the fixed lines of the axis.
     * @return the list of fixed lines
     */
    inline const std::vector<FixedLine> &getFixedLines() const
    { return fixedLines; }

    /**
     * Remove all fixed lines.  This does not set the defined flag
     */
    inline void clearFixedLines()
    { fixedLines.clear(); }
};

/**
 * The common parameters for a color ("Z") axis on a two dimensional graph.
 */
class CPD3GRAPHING_EXPORT AxisParameters2DColor : public AxisParameters2D {
public:
    /** The side binding of the axis. */
    enum SideBindingState {
        /** Bind to the top. */
        Side_Top,

        /** Bind to the bottom. */
        Side_Bottom,

        /** Bind to the left. */
        Side_Left,

        /** Bind to the right. */
        Side_Right,

        /** Autoselect based on which side has fewer axes. */
        Side_Auto,
    };
private:
    enum {
        Component_SideBinding = 0x00000001,
        Component_Gradient = 0x00000002,
        Component_BarScale = 0x00000004,
    };
    int components;
    SideBindingState sideBinding;
    TraceGradient gradient;
    Data::Variant::Root gradientConfiguration;
    qreal barScale;
public:
    AxisParameters2DColor();

    virtual ~AxisParameters2DColor();

    AxisParameters2DColor(const AxisParameters2DColor &other);

    AxisParameters2DColor &operator=(const AxisParameters2DColor &other);

    AxisParameters2DColor(AxisParameters2DColor &&other);

    AxisParameters2DColor &operator=(AxisParameters2DColor &&other);

    AxisParameters2DColor(const Data::Variant::Read &value, QSettings *settings = 0);

    void save(Data::Variant::Write &value) const override;

    std::unique_ptr<AxisParameters> clone() const override;

    void overlay(const AxisParameters *over) override;

    void copy(const AxisParameters *from) override;

    void clear() override;

    /**
     * Test if the parameters specify a the side to draw the axis on
     * @return true if the parameters specify a side for the axis
     */
    inline bool hasSideBinding() const
    { return components & Component_SideBinding; }

    /**
     * Get the axis side binding state
     * @return the side binding state
     */
    inline SideBindingState getSideBinding() const
    { return sideBinding; }

    /**
     * Override the side binding target.  This sets the defined flag.
     * @param binding   the new binding
     */
    inline void overrideSideBinding(SideBindingState binding)
    {
        sideBinding = binding;
        components |= Component_SideBinding;
    }

    /**
     * Test if the parameters specify a gradient.
     * @return true if the parameters specify a gradient
     */
    inline bool hasGradient() const
    { return components & Component_Gradient; }

    /**
     * Get the color gradient.
     * @return the color gradient
     */
    inline TraceGradient getGradient() const
    { return gradient; }

    /**
     * Get the color gradient configuration
     * @return the color gradient configuration
     */
    Data::Variant::Read getGradientConfig() const
    { return gradientConfiguration.read(); }

    /**
     * Override the gradient configuration.  This sets the defined flag.
     * @param config    the new configuration
     */
    inline void overrideGradientConfig(const Data::Variant::Read &config)
    {
        gradientConfiguration.write().set(config);
        gradient = TraceGradient(gradientConfiguration.read());
        components |= Component_Gradient;
    }

    /**
     * Test if the parameters specify bar scale factor.
     * @return true if the parameters specify a bar scale factor
     */
    inline bool hasBarScale() const
    { return components & Component_BarScale; }

    /**
     * Get the bar scale factor, if negative then it is in exact pixels.
     * @return the bar scale factor
     */
    inline qreal getBarScale() const
    { return barScale; }
};

/**
 * The base class for an axis of a two dimensional graph.
 */
class CPD3GRAPHING_EXPORT AxisDimension2D : public AxisDimension {
Q_OBJECT

    AxisParameters2D *parameters;
    AxisTransformer labelTransformer;
    std::vector<std::unique_ptr<AxisLabelEngine2D>> labelEngines;
    Axis2DSide side;
    bool tickPrimary;
    bool ticksInside;

    qreal inset;
    qreal labelOffset;
    qreal titleOffset;
    qreal predictedDepth;

    QString title;
    std::vector<std::vector<AxisTickGenerator::Tick>> rawTicks;
    std::vector<qreal> tickHeights;

    double previousDataMin;
    double previousDataMax;

protected:

    class ModificationComponent;

    friend class ModificationComponent;

public:
    AxisDimension2D(std::unique_ptr<AxisParameters2D> &&params);

    virtual ~AxisDimension2D();

    std::unique_ptr<AxisDimension> clone() const override;

    /**
     * Get the last bound side.
     * @return the last bound side
     */
    inline Axis2DSide getSide() const
    { return side; }

    /**
     * Get the axis inset.
     * @return the axis inset
     */
    inline qreal getInset() const
    { return inset; }

    /**
     * Set the axis inset.
     * @param inset the new inset
     */
    inline void setInset(qreal inset)
    { this->inset = inset; }

    /**
     * Set the tick polarity.
     * @param inside if true then ticks protrude "inside" the area
     */
    inline void setTickPolarity(bool inside)
    { ticksInside = inside; }

    /**
     * Get the predicted depth of the basic components of the axis.
     * 
     * @return the predicted depth
     */
    inline qreal getPredictedDepth() const
    { return predictedDepth; }

    /**
     * Get the last raw untransformed ticks.
     * 
     * @return the raw ticks
     */
    inline const std::vector<std::vector<AxisTickGenerator::Tick> > &getRawTicks() const
    { return rawTicks; }

    /**
     * Get the draw enable state.
     * @return true if the axis should be drawn
     */
    inline bool getDrawEnable() const
    { return parameters->getDrawEnable(); }

    /**
     * Get the units to display in tooltips instead of what the originating
     * trace specified.
     * @return the unit string to display
     */
    inline QString getToolTipUnitOverride() const
    { return parameters->getToolTipUnits(); }

    virtual bool prepareDraw(QPaintDevice *paintdevice = nullptr,
                             const DisplayDynamicContext &context = {});

    void setTickPrimary(bool p);

    void setBinding(Axis2DSide side, bool tickPrimary = true);

    virtual std::vector<std::unique_ptr<Graph2DDrawPrimitive>> createDraw(qreal baseline,
                                                                          qreal limitMin,
                                                                          qreal limitMax,
                                                                          const AxisTransformer &transformer) const;

    virtual std::vector<std::unique_ptr<Graph2DDrawPrimitive>> createDraw(qreal baseline,
                                                                          qreal limitMin,
                                                                          qreal limitMax) const;

    /**
     * Enable bounds modification from the override menu.
     *
     * @return true if the bounds can be modified
     */
    virtual bool enableBoundsModification() const;

    virtual DisplayModificationComponent *createModificationComponent(Graph2D *graph);

protected:
    AxisDimension2D(const AxisDimension2D &other, std::unique_ptr<AxisParameters> &&params);
};

/**
 * A side (top, bottom, left, right) axis of a two dimensional graph.
 */
class CPD3GRAPHING_EXPORT AxisDimension2DSide : public AxisDimension2D {
Q_OBJECT

    AxisParameters2DSide *parameters;
    bool primary;

protected:

    class ModificationComponent;

    friend class ModificationComponent;

public:
    AxisDimension2DSide(std::unique_ptr<AxisParameters2DSide> &&params);

    virtual ~AxisDimension2DSide();

    std::unique_ptr<AxisDimension> clone() const override;

    virtual std::vector<std::shared_ptr<
            DisplayCoupling>> createRangeCoupling(DisplayCoupling::CoupleMode mode = DisplayCoupling::Always);

    /**
     * Get the preferred side binding.
     * @return the side binding
     */
    inline AxisParameters2DSide::SideBindingState getSideBinding() const
    { return parameters->getSideBinding(); }

    /**
     * Get the last set primary state of the axis.
     * @return true if the axis is primary (left/bottom)
     */
    inline bool getPrimary() const
    { return primary; }

    /**
     * Set the primary state of the axis.
     * @param p true to set the axis to primary (left/bottom)
     */
    inline void setPrimary(bool p)
    { primary = p; }

    /**
     * Get the line width of the mouseover highlight line.
     * @return the line width
     */
    inline qreal getMouseoverLineWidth() const
    { return parameters->getBaselineWidth(); }

    /**
     * Get the line style of the mouseover highlight line.
     * @return the line style
     */
    inline Qt::PenStyle getMouseoverLineStyle() const
    { return parameters->getBaselineStyle(); }

    /**
     * Get the line style of the mouseover highlight line.
     * @return the line color
     */
    inline QColor getMouseoverLineColor() const
    { return parameters->getBaselineColor(); }

    void beginUpdate() override;

    void bindUniqueTraceColor(const QColor &color);

    virtual std::vector<std::unique_ptr<Graph2DDrawPrimitive>> createDraw(qreal baseline,
                                                                          qreal priorBaseline,
                                                                          qreal farSide,
                                                                          qreal limitMin,
                                                                          qreal limitMax) const;

    DisplayModificationComponent *createModificationComponent(Graph2D *graph) override;

protected:
    AxisDimension2DSide(const AxisDimension2DSide &other, std::unique_ptr<AxisParameters> &&params);
};

/**
 * A color ("Z") axis of a two dimensional graph.
 */
class CPD3GRAPHING_EXPORT AxisDimension2DColor : public AxisDimension2D {
Q_OBJECT

    AxisParameters2DColor *parameters;
    qreal barDepth;

protected:

    class ModificationComponent;

    friend class ModificationComponent;

public:
    AxisDimension2DColor(std::unique_ptr<AxisParameters2DColor> &&params);

    virtual ~AxisDimension2DColor();

    std::unique_ptr<AxisDimension> clone() const override;

    /**
     * Get the gradient used for this axis.
     * 
     * @return a trace gradient
     */
    inline TraceGradient getGradient() const
    { return parameters->getGradient(); }

    /**
     * Get the preferred side binding.
     * @return the side binding
     */
    inline AxisParameters2DColor::SideBindingState getSideBinding() const
    { return parameters->getSideBinding(); }

    qreal getPredictedDepth() const;

    bool prepareDraw(QPaintDevice *paintdevice = nullptr,
                     const DisplayDynamicContext &context = {}) override;

    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> createDraw(qreal baseline,
                                                                  qreal limitMin,
                                                                  qreal limitMax) const override;

    DisplayModificationComponent *createModificationComponent(Graph2D *graph) override;

protected:
    AxisDimension2DColor(const AxisDimension2DColor &other,
                         std::unique_ptr<AxisParameters> &&params);
};

/**
 * The base class for dimension axes of a two dimensional graph.
 */
class CPD3GRAPHING_EXPORT AxisDimensionSet2D : public AxisDimensionSet {
Q_OBJECT
public:
    AxisDimensionSet2D();

    virtual ~AxisDimensionSet2D();

    std::unique_ptr<AxisDimensionSet> clone() const override;

    /**
     * Prepare for drawing.  This must be called after all data is registered
     * and all couplings applied.
     * 
     * @param paintdevice   the paint device or NULL for the default
     * @param context       the display context
     * @return              true if an external source caused an update
     */
    virtual bool prepareDraw(QPaintDevice *paintdevice = nullptr,
                             const DisplayDynamicContext &context = {});

    /**
     * Set the area traces are drawn in.
     * 
     * @param traceArea     the trace area (innermost display area)
     * @param inner         the inner edge of the area for axes
     */
    virtual void setArea(const QRectF &traceArea, const QRectF &inner);

    /**
     * Create all drawing primitives.  This must be called after the inner
     * area is set.
     * 
     * @return a list of drawing primitives
     */
    virtual std::vector<std::unique_ptr<Graph2DDrawPrimitive>> createDraw() const;

protected:
    AxisDimensionSet2D(const AxisDimensionSet2D &);
};

/**
 * A set of side (top and bottom or left and right) axes on a two dimensional
 * graph.
 */
class CPD3GRAPHING_EXPORT AxisDimensionSet2DSide : public AxisDimensionSet2D {
Q_OBJECT

    bool vertical;
    std::vector<AxisDimension2DSide *> primary;
    std::vector<AxisDimension2DSide *> secondary;
    qreal primaryDepth;
    qreal secondaryDepth;

    QRectF inner;
public:
    AxisDimensionSet2DSide(bool setVertical);

    virtual ~AxisDimensionSet2DSide();

    std::unique_ptr<AxisDimensionSet> clone() const override;

    void finishUpdate() override;

    bool prepareDraw(QPaintDevice *paintdevice = nullptr,
                     const DisplayDynamicContext &context = {}) override;

    double getPredictedDepth(bool primary) const;

    void setArea(const QRectF &traceArea, const QRectF &inner) override;

    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> createDraw() const override;

    double getBaseline(AxisDimension2DSide *axis) const;

protected:
    AxisDimensionSet2DSide(const AxisDimensionSet2DSide &other);

    std::unique_ptr<AxisParameters> parseParameters(const Data::Variant::Read &value,
                                                    QSettings *settings = 0) const override;

    std::unique_ptr<AxisDimension> createDimension(std::unique_ptr<
            AxisParameters> &&parameters) const override;
};

/**
 * A set of color ("Z") axes on a two dimensional graph.
 */
class CPD3GRAPHING_EXPORT AxisDimensionSet2DColor : public AxisDimensionSet2D {
Q_OBJECT

    std::vector<AxisDimension2DColor *> top;
    std::vector<AxisDimension2DColor *> bottom;
    std::vector<AxisDimension2DColor *> left;
    std::vector<AxisDimension2DColor *> right;
    qreal topDepth;
    qreal bottomDepth;
    qreal leftDepth;
    qreal rightDepth;

    QRectF innerColumn;
    QRectF innerRow;
public:
    AxisDimensionSet2DColor();

    virtual ~AxisDimensionSet2DColor();

    std::unique_ptr<AxisDimensionSet> clone() const override;

    void finishUpdate() override;

    bool prepareDraw(QPaintDevice *paintdevice = nullptr,
                     const DisplayDynamicContext &context = {}) override;

    double getPredictedDepth(Axis2DSide side) const;

    void setArea(const QRectF &traceArea, const QRectF &inner) override;

    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> createDraw() const override;

protected:
    AxisDimensionSet2DColor(const AxisDimensionSet2DColor &other);

    std::unique_ptr<AxisParameters> parseParameters(const Data::Variant::Read &value,
                                                    QSettings *settings = 0) const override;

    std::unique_ptr<AxisDimension> createDimension(std::unique_ptr<
            AxisParameters> &&parameters) const override;
};

}
}

#endif
