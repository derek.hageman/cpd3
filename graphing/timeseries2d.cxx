/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>

#include "datacore/variant/root.hxx"
#include "datacore/variant/composite.hxx"
#include "algorithms/model.hxx"
#include "graphing/timeseries2d.hxx"
#include "guicore/guiformat.hxx"

using namespace CPD3::Data;
using namespace CPD3::Algorithms;
using namespace CPD3::GUI;
using namespace CPD3::Graphing::Internal;

namespace CPD3 {
namespace Graphing {

static const qreal needleHeightPadding = 0.4;
static const qreal needleWidthPadding = 0.5;
static const qreal needlePointSize = 0.25;
static const qreal needleLineStartLength = 0.1;
static const qreal needleLineTransitionLength = 0.25;
static const qreal needleLineEndLength = 0.1;
static const qreal needleIntersectionSpace = 3.0;

/** @file graphing/timeseries2d.hxx
 * A time series/strip chart style two dimensional graph.
 */

TimeSeries2DParameters::TimeSeries2DParameters() : components(0),
                                                   needleDrawPriority(
                                                           Graph2DDrawPrimitive::Priority_Axes - 1)
{ }

TimeSeries2DParameters::~TimeSeries2DParameters() = default;

TimeSeries2DParameters::TimeSeries2DParameters(const TimeSeries2DParameters &) = default;

TimeSeries2DParameters &TimeSeries2DParameters::operator=(const TimeSeries2DParameters &) = default;

TimeSeries2DParameters::TimeSeries2DParameters(const Variant::Read &value,
                                               const DisplayDefaults &defaults) : Graph2DParameters(
        value, defaults), components(0), needleDrawPriority(Graph2DDrawPrimitive::Priority_Axes - 1)
{
    if (INTEGER::defined(value["NeedleDrawPriority"].toInt64())) {
        needleDrawPriority = (int) value["NeedleDrawPriority"].toInt64();
        components |= Component_NeedleDrawPriority;
    }
}

void TimeSeries2DParameters::save(Variant::Write &value) const
{
    Graph2DParameters::save(value);

    if (components & Component_NeedleDrawPriority) {
        value["NeedleDrawPriority"].setInt64(needleDrawPriority);
    }
}

std::unique_ptr<Graph2DParameters> TimeSeries2DParameters::clone() const
{ return std::unique_ptr<Graph2DParameters>(new TimeSeries2DParameters(*this)); }

void TimeSeries2DParameters::overlay(const Graph2DParameters *over)
{
    Graph2DParameters::overlay(over);

    const TimeSeries2DParameters *top = static_cast<const TimeSeries2DParameters *>(over);

    if (top->components & Component_NeedleDrawPriority) {
        needleDrawPriority = top->needleDrawPriority;
        components |= Component_NeedleDrawPriority;
    }
}

void TimeSeries2DParameters::initializeOverlay(const Graph2DParameters *over)
{
    TimeSeries2DParameters::operator=(*(static_cast<const TimeSeries2DParameters *>(over)));
}

void TimeSeries2DParameters::copy(const Graph2DParameters *from)
{ *this = *(static_cast<const TimeSeries2DParameters *>(from)); }

void TimeSeries2DParameters::clear()
{
    Graph2DParameters::clear();
    components = 0;
}

TimeSeriesTraceParameters2D::TimeSeriesTraceParameters2D() : components(0), origin(Origin_Start)
{
    setLineEnable(true);
    setSymbolEnable(false);
}

TimeSeriesTraceParameters2D::~TimeSeriesTraceParameters2D()
{ }

TimeSeriesTraceParameters2D::TimeSeriesTraceParameters2D(const TimeSeriesTraceParameters2D &other)
        : TraceParameters2D(other), components(other.components), origin(other.origin)
{ }

TimeSeriesTraceParameters2D &TimeSeriesTraceParameters2D::operator=(const TimeSeriesTraceParameters2D &other)
{
    if (&other == this) return *this;
    TraceParameters2D::operator=(other);
    components = other.components;
    origin = other.origin;
    return *this;
}

TimeSeriesTraceParameters2D::TimeSeriesTraceParameters2D(const Variant::Read &value)
        : TraceParameters2D(value), components(0), origin(Origin_Start)
{
    if (!hasLineEnable())
        setLineEnable(true);
    if (!hasSymbolEnable())
        setSymbolEnable(false);

    if (value["Origin"].exists()) {
        const auto &check = value["Origin"].toString();
        if (Util::equal_insensitive(check, "start")) {
            origin = Origin_Start;
            components |= Component_Origin;
        } else if (Util::equal_insensitive(check, "end")) {
            origin = Origin_End;
            components |= Component_Origin;
        } else if (Util::equal_insensitive(check, "center")) {
            origin = Origin_Center;
            components |= Component_Origin;
        } else if (Util::equal_insensitive(check, "both")) {
            origin = Origin_Both;
            components |= Component_Origin;
        }
    }
}

void TimeSeriesTraceParameters2D::save(Variant::Write &value) const
{
    TraceParameters2D::save(value);

    if (components & Component_Origin) {
        switch (origin) {
        case Origin_Start:
            value["Origin"].setString("Start");
            break;
        case Origin_End:
            value["Origin"].setString("End");
            break;
        case Origin_Center:
            value["Origin"].setString("Center");
            break;
        case Origin_Both:
            value["Origin"].setString("Both");
            break;
        }
    }
}

std::unique_ptr<TraceParameters> TimeSeriesTraceParameters2D::clone() const
{ return std::unique_ptr<TraceParameters>(new TimeSeriesTraceParameters2D(*this)); }

void TimeSeriesTraceParameters2D::overlay(const TraceParameters *over)
{
    TraceParameters2D::overlay(over);

    const TimeSeriesTraceParameters2D *top = static_cast<const TimeSeriesTraceParameters2D *>(over);

    if (top->components & Component_Origin) {
        origin = top->origin;
        components |= Component_Origin;
    }
}

void TimeSeriesTraceParameters2D::copy(const TraceParameters *from)
{ *this = *(static_cast<const TimeSeriesTraceParameters2D *>(from)); }

void TimeSeriesTraceParameters2D::clear()
{
    TraceParameters2D::clear();
    components = 0;
}

TimeSeriesTraceHandler2D::TimeSeriesTraceHandler2D(std::unique_ptr<
        TimeSeriesTraceParameters2D> &&params) : TraceHandler2D(std::move(params)),
                                                 parameters(
                                                         static_cast<TimeSeriesTraceParameters2D *>(getParameters()))
{ }

TimeSeriesTraceHandler2D::~TimeSeriesTraceHandler2D() = default;

TimeSeriesTraceHandler2D::Cloned TimeSeriesTraceHandler2D::clone() const
{ return Cloned(new TimeSeriesTraceHandler2D(*this, parameters->clone())); }

TimeSeriesTraceHandler2D::TimeSeriesTraceHandler2D(const TimeSeriesTraceHandler2D &other,
                                                   std::unique_ptr<TraceParameters> &&params)
        : TraceHandler2D(other, std::move(params)),
          parameters(static_cast<TimeSeriesTraceParameters2D *>(getParameters()))
{ }

double TimeSeriesTraceHandler2D::getFitInterval() const
{
    if (FP::defined(getStart()) && FP::defined(getEnd()))
        return getEnd() - getStart();
    if (FP::defined(getLimits().min[0]))
        return getLimits().max[0] - getLimits().min[0];
    return 0;
}

double TimeSeriesTraceHandler2D::getFitScale() const
{
    double range = getFitInterval();
    if (range >= 63072000.0)
        return 1.0 / 31536000.0;
    if (range >= 1209600.0)
        return 1.0 / 604800.0;
    if (range >= 172800.0)
        return 1.0 / 86400.0;
    if (range >= 7200.0)
        return 1.0 / 3600.0;
    if (range >= 120.0)
        return 1.0 / 60.0;
    if (range >= 2.0)
        return 1.0 / 2.0;
    return 1000.0;
}

QString TimeSeriesTraceHandler2D::getFitLegend(const std::shared_ptr<Model> &fit) const
{
    Q_ASSERT(fit);
    ModelLegendParameters lp;
    lp.setConfidence(0.95);
    lp.setShowOffset(false);
    ModelLegendEntry legend(fit->legend(lp));

    QString base(legend.getLine());
    if (base.isEmpty())
        return base;

    double range = getFitInterval();
    if (range >= 63072000.0)
        return tr("%1 (year)", "legend fit text").arg(base);
    if (range >= 1209600.0)
        return tr("%1 (week)", "legend fit text").arg(base);
    if (range >= 172800.0)
        return tr("%1 (day)", "legend fit text").arg(base);
    if (range >= 7200.0)
        return tr("%1 (hour)", "legend fit text").arg(base);
    if (range >= 120.0)
        return tr("%1 (minute)", "legend fit text").arg(base);
    if (range >= 2.0)
        return tr("%1 (second)", "legend fit text").arg(base);
    return tr("%1 (millisecond)", "legend fit text").arg(base);
}

void TimeSeriesTraceHandler2D::convertValue(const TraceDispatch::OutputValue &value,
                                            std::vector<TracePoint<3>> &points)
{
    if (value.isMetadata()) {
        for (int i = 0; i < 2; i++) {
            updateMetadata(i + 1, value.get(i));
        }
        return;
    }
    TracePoint<3> add;
    add.start = value.getStart();
    add.end = value.getEnd();
    switch (parameters->getOrigin()) {
    case TimeSeriesTraceParameters2D::Origin_Start:
        if (FP::defined(add.start))
            add.d[0] = add.start;
        else
            add.d[0] = add.end;
        break;
    case TimeSeriesTraceParameters2D::Origin_End:
        if (FP::defined(add.end))
            add.d[0] = add.end;
        else
            add.d[0] = add.start;
        break;
    case TimeSeriesTraceParameters2D::Origin_Center:
        if (FP::defined(add.start) && FP::defined(add.end))
            add.d[0] = (add.start + add.end) * 0.5;
        else if (FP::defined(add.start))
            add.d[0] = add.start;
        else
            add.d[0] = add.end;
        break;
    case TimeSeriesTraceParameters2D::Origin_Both:
        if (FP::defined(add.start)) {
            add.d[0] = add.start;

            int i;
            for (i = 0; i < 2; i++) {
                switch (value.getType(i)) {
                case Variant::Type::Array:
                case Variant::Type::Matrix: {
                    int idxStart = points.size();
                    for (int j = i + 1; j <= 2; j++) {
                        add.d[j] = FP::undefined();
                    }
                    for (; i < 2; i++) {
                        handleValueFanout(points, add, idxStart, value.get(i), i + 1);
                    }
                    i = 4;
                    break;
                }
                default:
                    add.d[i + 1] = Data::Variant::Composite::toNumber(value.get(i));
                    break;
                }
            }
            if (i <= 3)
                points.emplace_back(add);
            if (!FP::defined(add.end))
                return;
        }

        add.d[0] = add.end;
        break;
    }
    for (int i = 0; i < 2; i++) {
        switch (value.getType(i)) {
        case Variant::Type::Array:
        case Variant::Type::Matrix: {
            int idxStart = points.size();
            for (int j = i + 1; j <= 2; j++) {
                add.d[j] = FP::undefined();
            }
            for (; i < 2; i++) {
                handleValueFanout(points, add, idxStart, value.get(i), i + 1);
            }
            return;
        }
        default:
            add.d[i + 1] = Data::Variant::Composite::toNumber(value.get(i));
            break;
        }
    }
    points.emplace_back(std::move(add));
}

QVector<TransformerPoint *> TimeSeriesTraceHandler2D::prepareTransformer(const std::vector<
        TraceDispatch::OutputValue> &points, TransformerParameters &)
{
    QVector<TransformerPoint *> result;
    result.reserve(points.size());
    for (const auto &it : points) {
        if (!Range::intersects(it.getStart(), it.getEnd(), getStart(), getEnd()))
            continue;

        if (it.isMetadata()) {
            for (int i = 0; i < 2; i++) {
                updateMetadata(i + 1, it.get(i));
            }
            continue;
        }

        handleTransformerPoint<2>(this->parameters->getTransformerInputMode(), result, it);
    }
    return result;
}

std::vector<TracePoint<3>> TimeSeriesTraceHandler2D::convertTransformerResult(const QVector<
        TransformerPoint *> &points)
{
    std::vector<TracePoint<3>> result;
    result.reserve(points.size());
    for (auto it : points) {
        auto point = static_cast<const TransformValue<2> *>(it);

        TracePoint<3> add;
        add.start = point->getValue().getStart();
        add.end = point->getValue().getEnd();
        switch (parameters->getOrigin()) {
        case TimeSeriesTraceParameters2D::Origin_Start:
            if (FP::defined(add.start))
                add.d[0] = add.start;
            else
                add.d[0] = add.end;
            break;
        case TimeSeriesTraceParameters2D::Origin_End:
            if (FP::defined(add.end))
                add.d[0] = add.end;
            else
                add.d[0] = add.start;
            break;
        case TimeSeriesTraceParameters2D::Origin_Center:
            if (FP::defined(add.start) && FP::defined(add.end))
                add.d[0] = (add.start + add.end) * 0.5;
            else if (FP::defined(add.start))
                add.d[0] = add.start;
            else
                add.d[0] = add.end;
            break;
        case TimeSeriesTraceParameters2D::Origin_Both:
            if (FP::defined(add.start)) {
                add.d[0] = add.start;
                if (!FP::defined(add.end))
                    break;

                for (int i = 0; i < 2; i++) {
                    add.d[i + 1] = point->get(i);
                }
                result.emplace_back(add);
            }
            add.d[0] = add.end;
            break;
        }

        for (int i = 0; i < 2; i++) {
            add.d[i + 1] = point->get(i);
        }
        result.emplace_back(add);

        delete it;
    }
    return result;
}

TransformerAllocator *TimeSeriesTraceHandler2D::getTransformerAllocator()
{ return &TransformValue<2>::allocator; }

TimeSeriesTraceSet2D::TimeSeriesTraceSet2D() = default;

TimeSeriesTraceSet2D::~TimeSeriesTraceSet2D() = default;

TimeSeriesTraceSet2D::Cloned TimeSeriesTraceSet2D::clone() const
{ return Cloned(new TimeSeriesTraceSet2D(*this)); }

TimeSeriesTraceSet2D::TimeSeriesTraceSet2D(const TimeSeriesTraceSet2D &other) = default;

std::unique_ptr<
        TraceParameters> TimeSeriesTraceSet2D::parseParameters(const Variant::Read &value) const
{ return std::unique_ptr<TraceParameters>(new TimeSeriesTraceParameters2D(value)); }

TimeSeriesTraceSet2D::CreatedHandler TimeSeriesTraceSet2D::createHandler(std::unique_ptr<
        TraceParameters> &&parameters) const
{
    return CreatedHandler(new TimeSeriesTraceHandler2D(std::unique_ptr<TimeSeriesTraceParameters2D>(
            static_cast<TimeSeriesTraceParameters2D *>(parameters.release()))));
}

TimeSeriesNeedleParameters2D::TimeSeriesNeedleParameters2D() : components(0),
                                                               needleEnable(true),
                                                               needleFont(),
                                                               needleLabelColor(255, 255, 255),
                                                               needleLabelFormat()
{
    disableFit();
    disableFill();
    setLineEnable(true);
    setSymbolEnable(false);

    needleFont.setPointSizeF(needleFont.pointSizeF() * 1.1);
    needleFont.setBold(true);
}

TimeSeriesNeedleParameters2D::~TimeSeriesNeedleParameters2D() = default;

TimeSeriesNeedleParameters2D::TimeSeriesNeedleParameters2D(const TimeSeriesNeedleParameters2D &other) = default;

TimeSeriesNeedleParameters2D &TimeSeriesNeedleParameters2D::operator=(const TimeSeriesNeedleParameters2D &other) = default;

TimeSeriesNeedleParameters2D::TimeSeriesNeedleParameters2D(const Variant::Read &value)
        : TimeSeriesTraceParameters2D(value),
          components(0),
          needleEnable(true),
          needleFont(),
          needleLabelColor(255, 255, 255),
          needleLabelFormat()
{
    clearLineEnable();
    clearSymbolEnable();
    disableFit();
    disableFill();
    setLineEnable(true);
    setSymbolEnable(false);

    needleFont.setFamily("Monospace");
    needleFont.setPointSizeF(needleFont.pointSizeF() * 1.1);
    needleFont.setBold(true);

    if (value["Needle/Enable"].exists()) {
        needleEnable = value["Needle/Enable"].toBool();
        components |= Component_NeedleEnable;
    }

    if (value["Needle/Text/Font"].exists()) {
        needleFont = Display::parseFont(value["Needle/Text/Font"], needleFont);
        components |= Component_NeedleFont;
    }

    if (value["Needle/Text/Color"].exists()) {
        needleLabelColor = Display::parseColor(value["Needle/Text/Color"], needleLabelColor);
        components |= Component_NeedleLabelColor;
    }

    if (value["Needle/Text/Format"].exists()) {
        needleLabelFormat = value["Needle/Text/Color"].toDisplayString();
        if (!needleLabelFormat.isEmpty())
            components |= Component_NeedleLabelFormat;
    }
}

void TimeSeriesNeedleParameters2D::save(Variant::Write &value) const
{
    TimeSeriesTraceParameters2D::save(value);

    if (components & Component_NeedleEnable) {
        value["Needle/Enable"].setBool(needleEnable);
    }

    if (components & Component_NeedleFont) {
        value["Needle/Text/Font"].set(Display::formatFont(needleFont));
    }

    if (components & Component_NeedleLabelColor) {
        value["Needle/Text/Color"].set(Display::formatColor(needleLabelColor));
    }

    if (components & Component_NeedleLabelFormat) {
        value["Needle/Text/Format"].setString(needleLabelFormat);
    }
}

std::unique_ptr<TraceParameters> TimeSeriesNeedleParameters2D::clone() const
{ return std::unique_ptr<TraceParameters>(new TimeSeriesNeedleParameters2D(*this)); }

void TimeSeriesNeedleParameters2D::overlay(const TraceParameters *over)
{
    TimeSeriesTraceParameters2D::overlay(over);

    const TimeSeriesNeedleParameters2D
            *top = static_cast<const TimeSeriesNeedleParameters2D *>(over);

    if (top->components & Component_NeedleEnable) {
        needleEnable = top->needleEnable;
        components |= Component_NeedleEnable;
    }

    if (top->components & Component_NeedleFont) {
        needleFont = top->needleFont;
        components |= Component_NeedleFont;
    }

    if (top->components & Component_NeedleLabelColor) {
        needleLabelColor = top->needleLabelColor;
        components |= Component_NeedleLabelColor;
    }

    if (top->components & Component_NeedleLabelFormat) {
        needleLabelFormat = top->needleLabelFormat;
        components |= Component_NeedleLabelFormat;
    }
}

void TimeSeriesNeedleParameters2D::copy(const TraceParameters *from)
{ *this = *(static_cast<const TimeSeriesNeedleParameters2D *>(from)); }

void TimeSeriesNeedleParameters2D::clear()
{
    TimeSeriesTraceParameters2D::clear();
    components = 0;
}

TimeSeriesNeedleHandler2D::TimeSeriesNeedleHandler2D(std::unique_ptr<
        TimeSeriesNeedleParameters2D> &&params) : TimeSeriesTraceHandler2D(std::move(params)),
                                                  parameters(
                                                          static_cast<TimeSeriesNeedleParameters2D *>(getParameters()))
{ }

TimeSeriesNeedleHandler2D::~TimeSeriesNeedleHandler2D() = default;

TimeSeriesNeedleHandler2D::Cloned TimeSeriesNeedleHandler2D::clone() const
{
    return Cloned(new TimeSeriesNeedleHandler2D(*this, parameters->clone()));
}

TimeSeriesNeedleHandler2D::TimeSeriesNeedleHandler2D(const TimeSeriesNeedleHandler2D &other,
                                                     std::unique_ptr<TraceParameters> &&params)
        : TimeSeriesTraceHandler2D(other, std::move(params)),
          parameters(static_cast<TimeSeriesNeedleParameters2D *>(getParameters()))
{ }

static bool traceUnitsMatch(const std::vector<SequenceName::Set> &a,
                            const std::vector<SequenceName::Set> &b)
{
    if (a.size() != b.size())
        return false;
    auto setB = b.begin();
    for (const auto &setA : a) {
        SequenceName::Set cmpA;
        for (const auto &add : setA) {
            cmpA.insert(SequenceName(add.getStation(), {}, add.getVariable(), add.getFlavors()));
        }

        SequenceName::Set cmpB;
        for (const auto &add : *setB) {
            cmpB.insert(SequenceName(add.getStation(), {}, add.getVariable(), add.getFlavors()));
        }

        if (cmpA != cmpB)
            return false;

        ++setB;
    }
    return true;
}

/**
 * Like TraceHandler2D::getColors( const QVector<QSet<SequenceName> > &,
 * QList<TraceUtilities::ColorRequest> &, QList<QColor> & ), except that colors 
 * are copied from corresponding traces first, if they are set or will 
 * attempt to find their own color.
 * 
 * @param latestUnits        the inputs that went into this trace
 * @param requestColors     the output list of units to request colors for
 * @param claimedColors     the output list of already claimed colors
 * @param traces            the traces to copy from
 * @see TraceHandler2D::getColors( const QVector<QSet<SequenceName> > &,
 * QList<TraceUtilities::ColorRequest> &, QList<QColor> & )
 */
void TimeSeriesNeedleHandler2D::getColors(const std::vector<SequenceName::Set> &latestUnits,
                                          const std::vector<SequenceName::Set> &inputUnits,
                                          std::vector<TraceUtilities::ColorRequest> &requestColors,
                                          std::deque<QColor> &claimedColors,
                                          TraceSet2D *traces) const
{
    if (!parameters->getNeedleEnable())
        return;
    const auto &points = getPoints();
    if (points.empty() || !FP::defined(points.front().d[1]) || FP::defined(points.front().d[2]))
        return;

    if (parameters->hasColor()) {
        claimedColors.emplace_back(parameters->getColor());
        return;
    }
    for (std::size_t i = 0, max = traces->getTotalGroups(); i < max; i++) {
        const auto &handlers = traces->getGroupHandlers(i);
        if (handlers.empty())
            continue;
        const TraceDispatch *dispatch = traces->getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            auto units = dispatch->getUnits(j);
            if (!traceUnitsMatch(units, latestUnits) && !traceUnitsMatch(units, inputUnits))
                continue;
            auto handler = static_cast<const TraceHandler2D *>(handlers[j].get());
            if (!handler->getAnyValidPoints())
                continue;
            if (FP::defined(handler->getLimits().min[2]))
                continue;
            TraceParameters2D
                    *traceParameters = static_cast<TraceParameters2D *>(handler->getParameters());
            if (!traceParameters->getLineEnable() && !traceParameters->getSymbolEnable())
                continue;
            /* This trace will claim a color, so use that. */
            return;
        }
    }
    TraceHandler2D::getColors(latestUnits, requestColors, claimedColors);
}

/**
 * Like TraceHandler2D::claimColors( QList<QColor> & ), except that colors are 
 * copied from corresponding traces first.
 * 
 * @param colors        the input and output list of assigned colors
 * @param latestUnits        the inputs that went into this trace
 * @param traces        the traces to copy from
 */
void TimeSeriesNeedleHandler2D::claimColors(std::deque<QColor> &colors,
                                            const std::vector<SequenceName::Set> &latestUnits,
                                            const std::vector<SequenceName::Set> &inputUnits,
                                            TraceSet2D *traces) const
{
    if (!parameters->getNeedleEnable())
        return;
    if (parameters->hasColor())
        return;
    const auto &points = getPoints();
    if (points.empty() || !FP::defined(points.front().d[1]) || FP::defined(points.front().d[2]))
        return;

    bool hadAnyClaim = false;
    for (std::size_t i = 0, max = traces->getTotalGroups(); i < max; i++) {
        const auto &handlers = traces->getGroupHandlers(i);
        if (handlers.empty())
            continue;
        const TraceDispatch *dispatch = traces->getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            auto units = dispatch->getUnits(j);
            if (!traceUnitsMatch(units, latestUnits) && !traceUnitsMatch(units, inputUnits))
                continue;
            TraceHandler2D *handler = static_cast<TraceHandler2D *>(handlers[j].get());
            if (!handler->getAnyValidPoints())
                continue;
            if (FP::defined(handler->getLimits().min[2]))
                continue;
            TraceParameters2D
                    *traceParameters = static_cast<TraceParameters2D *>(handler->getParameters());
            if (!traceParameters->getLineEnable() && !traceParameters->getSymbolEnable())
                continue;
            /* Copy this trace's color */
            parameters->setColor(traceParameters->getColor());

            /* Done if we find one that's got a valid end (currently active) */
            const auto &check = handler->getPoints();
            if (!check.empty() && FP::defined(check.back().d[1]))
                return;

            hadAnyClaim = true;
        }
    }
    if (hadAnyClaim)
        return;
    TraceHandler2D::claimColors(colors);
}

TimeSeriesNeedleSet2D::TimeSeriesNeedleSet2D() = default;

TimeSeriesNeedleSet2D::~TimeSeriesNeedleSet2D() = default;

TimeSeriesNeedleSet2D::Cloned TimeSeriesNeedleSet2D::clone() const
{ return Cloned(new TimeSeriesNeedleSet2D(*this)); }

TimeSeriesNeedleSet2D::TimeSeriesNeedleSet2D(const TimeSeriesNeedleSet2D &other) = default;

std::unique_ptr<
        TraceParameters> TimeSeriesNeedleSet2D::parseParameters(const Variant::Read &value) const
{ return std::unique_ptr<TraceParameters>(new TimeSeriesNeedleParameters2D(value)); }

TimeSeriesNeedleSet2D::CreatedHandler TimeSeriesNeedleSet2D::createHandler(std::unique_ptr<
        TraceParameters> &&parameters) const
{
    return CreatedHandler(new TimeSeriesNeedleHandler2D(
            std::unique_ptr<TimeSeriesNeedleParameters2D>(
                    static_cast<TimeSeriesNeedleParameters2D *>(parameters.release()))));
}

namespace {
struct NeedleSet2DSortItem {
    TimeSeriesNeedleHandler2D *handler;
    std::vector<SequenceName::Set> units;
    std::vector<SequenceName::Set> latestUnits;

    bool operator<(const NeedleSet2DSortItem &other) const
    {
        if (handler->getLegendSortPriority() != other.handler->getLegendSortPriority()) {
            return handler->getLegendSortPriority() < other.handler->getLegendSortPriority();
        }

        std::size_t max = std::min(units.size(), other.units.size());
        if (max <= 0) return false;
        for (std::size_t i = max - 1; i >= 0; i--) {
            const auto &a = units[i];
            const auto &b = other.units[i];
            if (a.size() == 1 && b.size() == 1) {
                const auto &ua = *a.begin();
                const auto &ub = *b.begin();
                if (ua == ub)
                    continue;
                return SequenceName::OrderDisplay()(ua, ub);
            } else if (!a.empty() && !b.empty()) {
                std::vector<SequenceName> sa(a.begin(), a.end());
                std::sort(sa.begin(), sa.end(), SequenceName::OrderLogical());
                std::vector<SequenceName> sb(b.begin(), b.end());
                std::sort(sb.begin(), sb.end(), SequenceName::OrderLogical());
                for (std::size_t j = 0, nUnits = std::min(sa.size(), sb.size()); j < nUnits; ++j) {
                    const auto &ua = sa[j];
                    const auto &ub = sb[j];
                    if (ua == ub)
                        continue;
                    return SequenceName::OrderDisplay()(ua, ub);
                }
            }
        }
        return false;
    }
};
}

/**
 * Like TraceSet2D::getColors( QList<TraceUtilities::ColorRequest> &, 
 * QList<QColor> & ) const except that colors are copied from the corresponding
 * existing other traces.
 * 
 * @param requestColors     the output list of units to request colors for
 * @param claimedColors     the output list of already claimed colors
 * @param traces            the other traces to copy from
 * @see  TraceSet2D::getColors( QList<TraceUtilities::ColorRequest> &, QList<QColor> & )
 */
void TimeSeriesNeedleSet2D::getColors(std::vector<TraceUtilities::ColorRequest> &requestColors,
                                      std::deque<QColor> &claimedColors,
                                      TraceSet2D *traces) const
{
    std::vector<NeedleSet2DSortItem> sort;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        const TraceDispatch *dispatch = getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            NeedleSet2DSortItem add;
            add.handler = static_cast<TimeSeriesNeedleHandler2D *>(handlers[j].get());
            add.units = dispatch->getUnits(j);
            add.latestUnits = dispatch->getLatestUnits(j);
            sort.emplace_back(std::move(add));
        }
    }
    std::sort(sort.begin(), sort.end());
    for (const auto &add : sort) {
        add.handler->getColors(add.latestUnits, add.units, requestColors, claimedColors, traces);
    }
}

/**
 * Like TraceSet2D::claimColors( QList<QColor> & ) const
 * except that colors are copied from the corresponding existing other traces.
 * 
 * @param colors        the input and output list of assigned colors
 * @param traces        the other traces to copy from
 * @see  TraceSet2D::getColors( QList<TraceUtilities::ColorRequest> &, QList<QColor> & )
 */
void TimeSeriesNeedleSet2D::claimColors(std::deque<QColor> &colors, TraceSet2D *traces) const
{
    std::vector<NeedleSet2DSortItem> sort;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        const TraceDispatch *dispatch = getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            NeedleSet2DSortItem add;
            add.handler = static_cast<TimeSeriesNeedleHandler2D *>(handlers[j].get());
            add.units = dispatch->getUnits(j);
            add.latestUnits = dispatch->getLatestUnits(j);
            sort.emplace_back(std::move(add));
        }
    }
    std::sort(sort.begin(), sort.end());
    for (const auto &add : sort) {
        add.handler->claimColors(colors, add.latestUnits, add.units, traces);
    }

}

/**
 * Add all trace items to the legend that do not correspond to an entry
 * in the existing trace set.
 * 
 * @param legend    the target legend
 * @param context   the context to use
 * @param traces    the existing traces
 * @return          true if the legend has dynamically changed (as reported by the context, not necessarily if other parts have changed)
 */
bool TimeSeriesNeedleSet2D::addLegend(Legend &legend,
                                      const DisplayDynamicContext &context,
                                      TraceSet2D *traces) const
{
    std::vector<NeedleSet2DSortItem> sort;
    bool showStation = false;
    bool changed = false;
    SequenceName::Component priorStation;
    for (std::size_t i = 0, max = getTotalGroups(); i < max; i++) {
        const auto &handlers = getGroupHandlers(i);
        if (handlers.empty())
            continue;
        const TraceDispatch *dispatch = getGroupDispatch(i);
        for (std::size_t j = 0, nHandlers = handlers.size(); j < nHandlers; j++) {
            NeedleSet2DSortItem add;
            add.handler = static_cast<TimeSeriesNeedleHandler2D *>(handlers[j].get());
            if (!static_cast<TimeSeriesNeedleParameters2D *>(
                    add.handler->getParameters())->getNeedleEnable())
                continue;
            add.units = dispatch->getUnits(j);
            add.latestUnits = dispatch->getLatestUnits(j);

            bool hit = false;
            for (std::size_t traceIdx = 0, maxTrace = traces->getTotalGroups();
                    traceIdx < maxTrace;
                    traceIdx++) {
                const auto &traceHandlers = traces->getGroupHandlers(traceIdx);
                if (traceHandlers.empty())
                    continue;
                auto traceDispatch = traces->getGroupDispatch(i);
                for (std::size_t idxHandler = 0, nTraceHandlers = traceHandlers.size();
                        idxHandler < nTraceHandlers;
                        idxHandler++) {
                    auto traceUnits = traceDispatch->getUnits(idxHandler);
                    if (traceUnitsMatch(add.latestUnits, traceUnits) ||
                            traceUnitsMatch(add.units, traceUnits)) {
                        hit = true;
                        break;
                    }
                }
                if (hit)
                    break;
            }
            if (hit)
                continue;

            for (const auto &dim : add.units) {
                for (const auto &unit : dim) {
                    if (!showStation) {
                        if (priorStation.empty()) {
                            priorStation = unit.getStation();
                        } else if (priorStation != unit.getStation()) {
                            showStation = true;
                        }
                    }
                }
            }

            sort.emplace_back(std::move(add));
        }
    }
    std::sort(sort.begin(), sort.end());
    for (const auto &add: sort) {
        legend.append(
                add.handler->getLegend(add.latestUnits, showStation, false, context, &changed));
    }
    return changed;
}


TimeSeriesBinParameters2D::TimeSeriesBinParameters2D() : components(0),
                                                         binningType(Binning_FixedUniform),
                                                         binningCount(5),
                                                         binningUnits(Time::Hour),
                                                         binningAlign(true)
{ }

TimeSeriesBinParameters2D::~TimeSeriesBinParameters2D() = default;

TimeSeriesBinParameters2D::TimeSeriesBinParameters2D(const TimeSeriesBinParameters2D &other) = default;

TimeSeriesBinParameters2D &TimeSeriesBinParameters2D::operator=(const TimeSeriesBinParameters2D &other) = default;

TimeSeriesBinParameters2D::TimeSeriesBinParameters2D(const Variant::Read &value) : BinParameters2D(
        value),
                                                                                   components(0),
                                                                                   binningType(
                                                                                           Binning_FixedUniform),
                                                                                   binningCount(5),
                                                                                   binningUnits(
                                                                                           Time::Hour),
                                                                                   binningAlign(
                                                                                           true)
{
    if (value["Binning"].exists()) {
        Variant::Read binning;
        if (value["Binning"].getType() == Variant::Type::Array)
            binning = value["Binning"].array(0);
        else
            binning = value["Binning"];
        const auto &type = binning["Type"].toString();
        if (Util::equal_insensitive(type, "size", "fixedsize", "interval")) {
            components |= Component_Binning;
            binningType = Binning_FixedSize;
            binningUnits =
                    Variant::Composite::toTimeInterval(binning, &binningCount, &binningAlign);
            if (binningCount < 1)
                binningCount = 1;
        } else if (Util::equal_insensitive(type, "uniform", "fixedcount")) {
            if (INTEGER::defined(binning["Count"].toInt64())) {
                binningType = Binning_FixedUniform;
                int check = binning["Count"].toInt64();
                if (check > 0) {
                    binningCount = check;
                    components |= Component_Binning;
                }
            }
        }
    }
}

void TimeSeriesBinParameters2D::save(Variant::Write &value) const
{
    BinParameters2D::save(value);

    if (components & Component_Binning) {
        switch (binningType) {
        case Binning_FixedUniform:
            value["Binning/Type"].setString("Uniform");
            value["Binning/Count"].setInt64(binningCount);
            break;
        case Binning_FixedSize:
            value["Binning/Type"].setString("Interval");
            value["Binning/Count"].setInt64(binningCount);
            value["Binning/Align"].setBool(binningAlign);
            switch (binningUnits) {
            case Time::Millisecond:
                value["Binning/Units"].setString("Millisecond");
                break;
            case Time::Second:
                value["Binning/Units"].setString("Second");
                break;
            case Time::Minute:
                value["Binning/Units"].setString("Minute");
                break;
            case Time::Hour:
                value["Binning/Units"].setString("Hour");
                break;
            case Time::Day:
                value["Binning/Units"].setString("Day");
                break;
            case Time::Week:
                value["Binning/Units"].setString("Week");
                break;
            case Time::Month:
                value["Binning/Units"].setString("Month");
                break;
            case Time::Quarter:
                value["Binning/Units"].setString("Quarter");
                break;
            case Time::Year:
                value["Binning/Units"].setString("Year");
                break;
            }
            break;
        }
    }
}

std::unique_ptr<TraceParameters> TimeSeriesBinParameters2D::clone() const
{ return std::unique_ptr<TraceParameters>(new TimeSeriesBinParameters2D(*this)); }

void TimeSeriesBinParameters2D::overlay(const TraceParameters *over)
{
    BinParameters2D::overlay(over);

    const TimeSeriesBinParameters2D *top = static_cast<const TimeSeriesBinParameters2D *>(over);

    if (top->components & Component_Binning) {
        binningType = top->binningType;
        binningCount = top->binningCount;
        binningUnits = top->binningUnits;
        binningAlign = top->binningAlign;
        components |= Component_Binning;
    }
}

void TimeSeriesBinParameters2D::copy(const TraceParameters *from)
{ *this = *(static_cast<const TimeSeriesBinParameters2D *>(from)); }

void TimeSeriesBinParameters2D::clear()
{
    BinParameters2D::clear();
    components = 0;
}

TimeSeriesBinHandler2D::TimeSeriesBinHandler2D(std::unique_ptr<TimeSeriesBinParameters2D> &&params)
        : BinHandler2D(std::move(params)),
          parameters(static_cast<TimeSeriesBinParameters2D *>(getParameters()))
{ }

TimeSeriesBinHandler2D::~TimeSeriesBinHandler2D() = default;

TimeSeriesBinHandler2D::Cloned TimeSeriesBinHandler2D::clone() const
{ return Cloned(new TimeSeriesBinHandler2D(*this, parameters->clone())); }

TimeSeriesBinHandler2D::TimeSeriesBinHandler2D(const TimeSeriesBinHandler2D &other,
                                               std::unique_ptr<TraceParameters> &&params)
        : BinHandler2D(other, std::move(params)),
          parameters(static_cast<TimeSeriesBinParameters2D *>(getParameters()))
{ }

void TimeSeriesBinHandler2D::convertValue(const TraceDispatch::OutputValue &value,
                                          std::vector<TracePoint<2>> &points)
{
    if (value.isMetadata()) {
        for (std::size_t i = 0; i < 1; i++) {
            updateMetadata(i + 1, value.get(i));
        }
        return;
    }
    TracePoint<2> add;
    add.start = value.getStart();
    add.end = value.getEnd();
    add.d[0] = add.start;
    for (std::size_t i = 0; i < 1; i++) {
        switch (value.getType(i)) {
        case Variant::Type::Array:
        case Variant::Type::Matrix: {
            int idxStart = points.size();
            for (std::size_t j = i + 1; j <= 1; j++) {
                add.d[j] = FP::undefined();
            }
            for (; i < 1; i++) {
                handleValueFanout(points, add, idxStart, value.get(i), i + 1);
            }
            return;
        }
        default:
            add.d[i + 1] = Data::Variant::Composite::toNumber(value.get(i));
            break;
        }
    }
    points.emplace_back(std::move(add));
}

QVector<TransformerPoint *> TimeSeriesBinHandler2D::prepareTransformer(const std::vector<
        TraceDispatch::OutputValue> &points, TransformerParameters &)
{
    QVector<TransformerPoint *> result;
    result.reserve(points.size());
    for (const auto &it : points) {
        if (!Range::intersects(it.getStart(), it.getEnd(), getStart(), getEnd()))
            continue;

        if (it.isMetadata()) {
            for (int i = 0; i < 1; i++) {
                updateMetadata(i + 1, it.get(i));
            }
            continue;
        }

        handleTransformerPoint<1>(this->parameters->getTransformerInputMode(), result, it);
    }
    return result;
}

std::vector<TracePoint<2>> TimeSeriesBinHandler2D::convertTransformerResult(const QVector<
        TransformerPoint *> &points)
{
    std::vector<TracePoint<2>> result;
    result.reserve(points.size());
    for (auto it : points) {

        auto point = static_cast<const TransformValue<1> *>(it);
        TracePoint<2> add;
        add.start = point->getValue().getStart();
        add.end = point->getValue().getEnd();
        for (std::size_t i = 0; i < 1; i++) {
            add.d[i + 1] = point->get(i);
        }
        add.d[0] = add.start;
        result.emplace_back(std::move(add));

        delete it;
    }
    return result;
}

TransformerAllocator *TimeSeriesBinHandler2D::getTransformerAllocator()
{ return &TransformValue<1>::allocator; }

void TimeSeriesBinHandler2D::buildBins(std::size_t,
                                       std::vector<double> &starts,
                                       std::vector<double> &ends,
                                       std::vector<double> &centers)
{
    double min = getStart();
    double max = getEnd();
    if (!FP::defined(min))
        min = getLimits().min[0];
    if (!FP::defined(max) && FP::defined(getLimits().min[0]))
        max = getLimits().max[0];
    if (!FP::defined(min) || !FP::defined(max)) {
        starts.emplace_back(FP::undefined());
        ends.emplace_back(FP::undefined());
        centers.emplace_back(FP::undefined());
        return;
    }

    switch (parameters->getBinningType()) {
    case TimeSeriesBinParameters2D::Binning_FixedUniform: {
        double totalWidth = max - min;
        int nBins = parameters->getBinningCount();
        if (totalWidth == 0.0 || nBins <= 0) {
            starts.emplace_back(min);
            ends.emplace_back(min);
            centers.emplace_back(min);
        } else {
            double width = totalWidth / (double) nBins;
            for (int bin = 0; bin < nBins; bin++) {
                double start = min + width * (double) bin;
                double end = min + width * (double) (bin + 1);
                starts.emplace_back(start);
                ends.emplace_back(end);
                centers.emplace_back((start + end) * 0.5);
            }
        }
        break;
    }
    case TimeSeriesBinParameters2D::Binning_FixedSize: {
        double startTime;
        if (parameters->getBinningAlign()) {
            startTime =
                    Time::floor(min, parameters->getBinningUnits(), parameters->getBinningCount());
        } else {
            startTime = min;
        }
        for (int n = 0; n < FIXEDBINSIZE_NUMBER_LIMIT; n++) {
            double endTime = Time::logical(startTime, parameters->getBinningUnits(),
                                           parameters->getBinningCount(), true);
            if (n > 0 && startTime >= max)
                break;
            starts.emplace_back(startTime);
            ends.emplace_back(endTime);
            centers.emplace_back((startTime + endTime) * 0.5);
            startTime = endTime;
        }
        break;
    }
    }
}

std::vector<TracePoint<2>> TimeSeriesBinHandler2D::applyFilter(std::vector<TracePoint<2>> input,
                                                               double start,
                                                               double end,
                                                               std::size_t)
{
    std::vector<TracePoint<2> > result;
    if (!FP::defined(start)) {
        if (!FP::defined(end))
            return input;
        for (auto &it : input) {
            if (FP::defined(it.start) && it.start >= end)
                continue;
            result.emplace_back(std::move(it));
        }
    } else if (!FP::defined(end)) {
        for (auto &it : input) {
            if (FP::defined(it.end) && it.end <= start)
                continue;
            result.emplace_back(std::move(it));
        }
    } else {
        for (auto &it : input) {
            if (FP::defined(it.start) && it.start >= end)
                continue;
            if (FP::defined(it.end) && it.end <= start)
                continue;
            result.emplace_back(std::move(it));
        }
    }
    /* Fixup time ranges so the total number of seconds in the histogram is
     * correct */
    if (parameters->getAsHistogram()) {
        for (auto &it : result) {
            if (!FP::defined(it.start) || it.start < start)
                it.start = start;
            if (!FP::defined(it.end) || it.end > end)
                it.end = end;
        }
    }
    return result;
}

TimeSeriesBinSet2D::TimeSeriesBinSet2D() = default;

TimeSeriesBinSet2D::~TimeSeriesBinSet2D() = default;

TimeSeriesBinSet2D::Cloned TimeSeriesBinSet2D::clone() const
{ return Cloned(new TimeSeriesBinSet2D(*this)); }

TimeSeriesBinSet2D::TimeSeriesBinSet2D(const TimeSeriesBinSet2D &other) = default;

std::unique_ptr<
        TraceParameters> TimeSeriesBinSet2D::parseParameters(const Variant::Read &value) const
{ return std::unique_ptr<TraceParameters>(new TimeSeriesBinParameters2D(value)); }

TimeSeriesBinSet2D::CreatedHandler TimeSeriesBinSet2D::createHandler(std::unique_ptr<
        TraceParameters> &&parameters) const
{
    return CreatedHandler(new TimeSeriesBinHandler2D(
            std::unique_ptr<TimeSeriesBinParameters2D>(static_cast<TimeSeriesBinParameters2D *>(
                                                               parameters.release()))));
}

TimeSeriesIndicatorParameters2D::TimeSeriesIndicatorParameters2D() : components(0),
                                                                     origin(Origin_Start)
{ }

TimeSeriesIndicatorParameters2D::~TimeSeriesIndicatorParameters2D() = default;

TimeSeriesIndicatorParameters2D::TimeSeriesIndicatorParameters2D(const TimeSeriesIndicatorParameters2D &other) = default;

TimeSeriesIndicatorParameters2D &TimeSeriesIndicatorParameters2D::operator=(const TimeSeriesIndicatorParameters2D &other) = default;

TimeSeriesIndicatorParameters2D::TimeSeriesIndicatorParameters2D(const Variant::Read &value)
        : IndicatorParameters2D(value), components(0), origin(Origin_Start)
{
    if (value["Origin"].exists()) {
        const auto &check = value["Origin"].toString();
        if (Util::equal_insensitive(check, "start")) {
            origin = Origin_Start;
            components |= Component_Origin;
        } else if (Util::equal_insensitive(check, "end")) {
            origin = Origin_End;
            components |= Component_Origin;
        } else if (Util::equal_insensitive(check, "center")) {
            origin = Origin_Center;
            components |= Component_Origin;
        }
    }
}

void TimeSeriesIndicatorParameters2D::save(Variant::Write &value) const
{
    IndicatorParameters2D::save(value);

    if (components & Component_Origin) {
        switch (origin) {
        case Origin_Start:
            value["Origin"].setString("Start");
            break;
        case Origin_End:
            value["Origin"].setString("End");
            break;
        case Origin_Center:
            value["Origin"].setString("Center");
            break;
        }
    }
}

std::unique_ptr<TraceParameters> TimeSeriesIndicatorParameters2D::clone() const
{ return std::unique_ptr<TraceParameters>(new TimeSeriesIndicatorParameters2D(*this)); }

void TimeSeriesIndicatorParameters2D::overlay(const TraceParameters *over)
{
    IndicatorParameters2D::overlay(over);

    const TimeSeriesIndicatorParameters2D
            *top = static_cast<const TimeSeriesIndicatorParameters2D *>(over);

    if (top->components & Component_Origin) {
        origin = top->origin;
        components |= Component_Origin;
    }
}

void TimeSeriesIndicatorParameters2D::copy(const TraceParameters *from)
{ *this = *(static_cast<const TimeSeriesIndicatorParameters2D *>(from)); }

void TimeSeriesIndicatorParameters2D::clear()
{
    IndicatorParameters2D::clear();
    components = 0;
}

TimeSeriesIndicatorHandler2D::TimeSeriesIndicatorHandler2D(std::unique_ptr<
        TimeSeriesIndicatorParameters2D> &&params) : IndicatorHandler2D(std::move(params)),
                                                     parameters(
                                                             static_cast<TimeSeriesIndicatorParameters2D *>(getParameters()))
{ }

TimeSeriesIndicatorHandler2D::~TimeSeriesIndicatorHandler2D() = default;

TimeSeriesIndicatorHandler2D::Cloned TimeSeriesIndicatorHandler2D::clone() const
{ return Cloned(new TimeSeriesIndicatorHandler2D(*this, parameters->clone())); }

void TimeSeriesIndicatorHandler2D::convertIndicatorValue(const TraceDispatch::OutputValue &value,
                                                         IndicatorPoint<1> &add)
{

    switch (parameters->getOrigin()) {
    case TimeSeriesIndicatorParameters2D::Origin_Start:
        if (FP::defined(value.getStart()))
            add.d[0] = value.getStart();
        else
            add.d[0] = value.getEnd();
        break;
    case TimeSeriesIndicatorParameters2D::Origin_End:
        if (FP::defined(value.getEnd()))
            add.d[0] = value.getEnd();
        else
            add.d[0] = value.getStart();
        break;
    case TimeSeriesIndicatorParameters2D::Origin_Center:
        if (FP::defined(value.getStart()) && FP::defined(value.getEnd()))
            add.d[0] = (value.getStart() + value.getEnd()) * 0.5;
        else if (FP::defined(add.start))
            add.d[0] = value.getStart();
        else
            add.d[0] = value.getEnd();
        break;
    }
}

TimeSeriesIndicatorHandler2D::TimeSeriesIndicatorHandler2D(const TimeSeriesIndicatorHandler2D &other,
                                                           std::unique_ptr<
                                                                   TraceParameters> &&params)
        : IndicatorHandler2D(other, std::move(params)),
          parameters(static_cast<TimeSeriesIndicatorParameters2D *>(getParameters()))
{ }

TimeSeriesIndicatorSet2D::TimeSeriesIndicatorSet2D() = default;

TimeSeriesIndicatorSet2D::~TimeSeriesIndicatorSet2D() = default;

TimeSeriesIndicatorSet2D::Cloned TimeSeriesIndicatorSet2D::clone() const
{ return Cloned(new TimeSeriesIndicatorSet2D(*this)); }

TimeSeriesIndicatorSet2D::TimeSeriesIndicatorSet2D(const TimeSeriesIndicatorSet2D &other) = default;

std::unique_ptr<
        TraceParameters> TimeSeriesIndicatorSet2D::parseParameters(const Variant::Read &value) const
{ return std::unique_ptr<TraceParameters>(new TimeSeriesIndicatorParameters2D(value)); }

TimeSeriesIndicatorSet2D::CreatedHandler TimeSeriesIndicatorSet2D::createHandler(std::unique_ptr<
        TraceParameters> &&parameters) const
{
    return CreatedHandler(new TimeSeriesIndicatorHandler2D(
            std::unique_ptr<TimeSeriesIndicatorParameters2D>(
                    static_cast<TimeSeriesIndicatorParameters2D *>(parameters.release()))));
}

AxisParametersTimeSeries2D::AxisParametersTimeSeries2D() : components(0)
{
    AxisTransformer tr;
    tr.setMinExtendAbsolute(0);
    tr.setMaxExtendAbsolute(0);
    forceAxisTransformer(tr);

    forceTickGenerator(std::shared_ptr<AxisTickGenerator>(new AxisTickGeneratorTime));
}

AxisParametersTimeSeries2D::~AxisParametersTimeSeries2D() = default;

AxisParametersTimeSeries2D::AxisParametersTimeSeries2D(const AxisParametersTimeSeries2D &other) = default;

AxisParametersTimeSeries2D &AxisParametersTimeSeries2D::operator=(const AxisParametersTimeSeries2D &other) = default;

AxisParametersTimeSeries2D::AxisParametersTimeSeries2D(const Variant::Read &value,
                                                       QSettings *settings) : AxisParameters2DSide(
        value, settings), components(0), timeFormat()
{
    AxisTransformer tr;
    tr.setMinExtendAbsolute(0);
    tr.setMaxExtendAbsolute(0);
    forceAxisTransformer(tr);

    if (value["TimeFormat"].exists()) {
        timeFormat = value["TimeFormat"].toDisplayString();
        components |= Component_TimeFormat;
    }

    forceTickGenerator(std::shared_ptr<AxisTickGenerator>(new AxisTickGeneratorTime(timeFormat)));
}

void AxisParametersTimeSeries2D::save(Variant::Write &value) const
{
    AxisParameters2DSide::save(value);

    if (components & Component_TimeFormat) {
        value["TimeFormat"].setString(timeFormat);
    }
}

std::unique_ptr<AxisParameters> AxisParametersTimeSeries2D::clone() const
{ return std::unique_ptr<AxisParameters>(new AxisParametersTimeSeries2D(*this)); }

void AxisParametersTimeSeries2D::overlay(const AxisParameters *over)
{
    AxisParameters2DSide::overlay(over);

    const AxisParametersTimeSeries2D *top = static_cast<const AxisParametersTimeSeries2D *>(over);

    if (top->components & Component_TimeFormat) {
        components |= Component_TimeFormat;
        timeFormat = top->timeFormat;

        forceTickGenerator(
                std::shared_ptr<AxisTickGenerator>(new AxisTickGeneratorTime(timeFormat)));
    }
}

void AxisParametersTimeSeries2D::copy(const AxisParameters *from)
{ *this = *(static_cast<const AxisParametersTimeSeries2D *>(from)); }

void AxisParametersTimeSeries2D::clear()
{
    AxisParameters2DSide::clear();
    components = 0;
    timeFormat = QString();

    forceTickGenerator(std::shared_ptr<AxisTickGenerator>(new AxisTickGeneratorTime(timeFormat)));
}

AxisDimensionTimeSeries2D::AxisDimensionTimeSeries2D(std::unique_ptr<
        AxisParametersTimeSeries2D> &&params) : AxisDimension2DSide(std::move(params)),
                                                parameters(
                                                        static_cast<AxisParametersTimeSeries2D *>(getParameters()))
{ }

AxisDimensionTimeSeries2D::~AxisDimensionTimeSeries2D() = default;

std::unique_ptr<AxisDimension> AxisDimensionTimeSeries2D::clone() const
{
    return std::unique_ptr<AxisDimension>(
            new AxisDimensionTimeSeries2D(*this, parameters->clone()));
}

std::vector<std::shared_ptr<DisplayCoupling>> AxisDimensionTimeSeries2D::createRangeCoupling(
        DisplayCoupling::CoupleMode mode)
{ return {std::make_shared<TimeSeriesAxisCoupling>(mode, this)}; }

bool AxisDimensionTimeSeries2D::enableBoundsModification() const
{ return false; }

AxisDimensionTimeSeries2D::AxisDimensionTimeSeries2D(const AxisDimensionTimeSeries2D &other,
                                                     std::unique_ptr<AxisParameters> &&params)
        : AxisDimension2DSide(other, std::move(params)),
          parameters(static_cast<AxisParametersTimeSeries2D *>(getParameters()))
{ }

namespace Internal {

TimeSeriesAxisCoupling::TimeSeriesAxisCoupling() = default;

TimeSeriesAxisCoupling::TimeSeriesAxisCoupling(DisplayCoupling::CoupleMode mode,
                                               AxisDimensionTimeSeries2D *setParent)
        : DisplayRangeCoupling(mode), parent(setParent)
{ }

TimeSeriesAxisCoupling::~TimeSeriesAxisCoupling()
{ }

double TimeSeriesAxisCoupling::getMin() const
{
    if (parent.isNull()) return FP::undefined();
    return parent->getRawMin();
}

double TimeSeriesAxisCoupling::getMax() const
{
    if (parent.isNull()) return FP::undefined();
    return parent->getRawMax();
}

bool TimeSeriesAxisCoupling::canCouple(const std::shared_ptr<DisplayCoupling> &other) const
{
    if (parent.isNull())
        return false;
    auto sa = qobject_cast<TimeSeriesAxisCoupling *>(other.get());
    if (!sa)
        return false;
    return true;
}

void TimeSeriesAxisCoupling::applyCoupling(const QVariant &data,
                                           DisplayCoupling::CouplingPosition position)
{
    if (parent.isNull())
        return;
    double setMin = FP::undefined();
    double setMax = FP::undefined();
    QList<QVariant> list(data.toList());
    Q_ASSERT(list.size() > 1);
    if (!list.front().isNull())
        setMin = list.front().toDouble();
    if (!list[1].isNull())
        setMax = list[1].toDouble();
    parent->couplingUpdated(setMin, setMax, position);

    switch (parent->getSide()) {
    case Axis2DLeft:
    case Axis2DTop:
        if (!FP::defined(parent->getZoomMin()) && !FP::defined(parent->getZoomMax())) {
            parent->setTickPrimary(
                    position == DisplayCoupling::First || position == DisplayCoupling::Only);
        } else {
            parent->setTickPrimary(true);
        }
        break;
    case Axis2DRight:
    case Axis2DBottom:
        if (!FP::defined(parent->getZoomMin()) && !FP::defined(parent->getZoomMax())) {
            parent->setTickPrimary(
                    position == DisplayCoupling::Last || position == DisplayCoupling::Only);
        } else {
            parent->setTickPrimary(true);
        }
        break;
    }
}

TimeSeriesAxisZoom::TimeSeriesAxisZoom() = default;

TimeSeriesAxisZoom::TimeSeriesAxisZoom(AxisDimensionTimeSeries2D *setDimension,
                                       AxisDimensionSetTimeSeries2D *setParent) : AxisDimensionZoom(
        setDimension, setParent), dimension(setDimension), parent(setParent)
{ }

QString TimeSeriesAxisZoom::getTitle() const
{ return tr("Time"); }

bool TimeSeriesAxisZoom::sharedZoom(const std::shared_ptr<DisplayZoomAxis> &other) const
{
    if (parent.isNull())
        return false;
    if (dimension.isNull())
        return false;
    auto sa = qobject_cast<TimeSeriesAxisZoom *>(other.get());
    if (!sa)
        return false;
    if (sa->parent.isNull())
        return false;
    if (sa->dimension.isNull())
        return false;
    return true;
}

DisplayZoomAxis::DisplayMode TimeSeriesAxisZoom::getDisplayMode() const
{ return DisplayZoomAxis::Display_Time; }

}

AxisDimensionSetTimeSeries2D::AxisDimensionSetTimeSeries2D() : AxisDimensionSet2DSide(false)
{ }

AxisDimensionSetTimeSeries2D::~AxisDimensionSetTimeSeries2D() = default;

std::unique_ptr<AxisDimensionSet> AxisDimensionSetTimeSeries2D::clone() const
{ return std::unique_ptr<AxisDimensionSet>(new AxisDimensionSetTimeSeries2D(*this)); }

AxisDimension *AxisDimensionSetTimeSeries2D::get(const std::unordered_set<QString> &,
                                                 const std::unordered_set<QString> &)
{ return AxisDimensionSet2DSide::get({}, {}, "Time"); }

AxisDimension *AxisDimensionSetTimeSeries2D::get(const std::unordered_set<QString> &,
                                                 const std::unordered_set<QString> &,
                                                 const QString &)
{ return AxisDimensionSet2DSide::get({}, {}, "Time"); }

AxisDimensionSetTimeSeries2D::AxisDimensionSetTimeSeries2D(const AxisDimensionSetTimeSeries2D &other) = default;

std::unique_ptr<
        AxisParameters> AxisDimensionSetTimeSeries2D::parseParameters(const Variant::Read &value,
                                                                      QSettings *settings) const
{ return std::unique_ptr<AxisParameters>(new AxisParametersTimeSeries2D(value, settings)); }

std::unique_ptr<AxisDimension> AxisDimensionSetTimeSeries2D::createDimension(std::unique_ptr<
        AxisParameters> &&parameters) const
{
    return std::unique_ptr<AxisDimension>(new AxisDimensionTimeSeries2D(
            std::unique_ptr<AxisParametersTimeSeries2D>(
                    static_cast<AxisParametersTimeSeries2D *>(parameters.release()))));
}

std::shared_ptr<
        AxisDimensionZoom> AxisDimensionSetTimeSeries2D::createZoom(AxisDimension *dimension)
{
    return std::make_shared<TimeSeriesAxisZoom>(static_cast<AxisDimensionTimeSeries2D *>(dimension),
                                                this);
}

TimeSeries2D::~TimeSeries2D() = default;

TimeSeries2D::TimeSeries2D(const DisplayDefaults &defaults, QObject *parent) : Graph2D(defaults,
                                                                                       parent),
                                                                               settings(
                                                                                       defaults.getSettings()),
                                                                               start(FP::undefined()),
                                                                               end(FP::undefined()),
                                                                               timeBindingUpdated(
                                                                                       false),
                                                                               ready(0)
{ }

TimeSeries2D::TimeSeries2D(const TimeSeries2D &other) : Graph2D(other),
                                                        settings(other.settings),
                                                        needles(),
                                                        start(other.start),
                                                        end(other.end),
                                                        timeBindingUpdated(true),
                                                        ready(other.ready)
{
    if (other.needles) {
        needles.reset(static_cast<TimeSeriesNeedleSet2D *>(other.needles->clone().release()));

        connect(needles.get(), &TimeSeriesNeedleSet2D::deferredUpdate, this,
                std::bind(&TimeSeries2D::interactiveRepaint, this, false));
        connect(needles.get(), &TimeSeriesNeedleSet2D::readyForFinalProcessing, this,
                &TimeSeries2D::needlesReady);
        connect(needles.get(), &TimeSeriesNeedleSet2D::handlersChanged, this,
                &TimeSeries2D::outputsChanged);
    }
}

std::unique_ptr<Display> TimeSeries2D::clone() const
{ return std::unique_ptr<Display>(new TimeSeries2D(*this)); }

static ValueSegment::Transfer createNeedlesConfig(const ValueSegment::Transfer &config)
{
    ValueSegment::Transfer result;
    for (const auto &it : config) {
        auto check = it.getValue()["Needles"];
        if (!check.exists())
            check = it.getValue()["Traces"];
        result.emplace_back(it.getStart(), it.getEnd(), Variant::Root(check));
    }
    return result;
}

void TimeSeries2D::initialize(const ValueSegment::Transfer &config, const DisplayDefaults &defaults)
{
    Graph2D::initialize(config, defaults);
    settings = defaults.getSettings();

    needles.reset(new TimeSeriesNeedleSet2D());
    needles->initialize(createNeedlesConfig(config), defaults, true);

    connect(needles.get(), &TimeSeriesNeedleSet2D::deferredUpdate, this,
            std::bind(&TimeSeries2D::interactiveRepaint, this, false));
    connect(needles.get(), &TimeSeriesNeedleSet2D::readyForFinalProcessing, this,
            &TimeSeries2D::needlesReady);
    connect(needles.get(), &TimeSeriesNeedleSet2D::handlersChanged, this,
            &TimeSeries2D::outputsChanged);
}

void TimeSeries2D::registerChain(ProcessingTapChain *chain)
{
    Graph2D::registerChain(chain);
    ready = 0;
    needles->registerChain(chain, false);
}

void TimeSeries2D::setVisibleTimeRange(double start, double end)
{
    Graph2D::setVisibleTimeRange(start, end);

    if (!FP::equal(start, this->start) || !FP::equal(end, this->end)) {
        this->start = start;
        this->end = end;
        timeBindingUpdated = true;
    }
}

std::unique_ptr<TraceSet2D> TimeSeries2D::createTraces()
{ return std::unique_ptr<TraceSet2D>(new TimeSeriesTraceSet2D); }

std::unique_ptr<BinSet2D> TimeSeries2D::createXBins()
{ return std::unique_ptr<BinSet2D>(new TimeSeriesBinSet2D); }

std::unique_ptr<BinSet2D> TimeSeries2D::createYBins()
{ return std::unique_ptr<BinSet2D>(new BinSet2DNOOP); }

std::unique_ptr<IndicatorSet2D> TimeSeries2D::createXIndicators()
{ return std::unique_ptr<IndicatorSet2D>(new TimeSeriesIndicatorSet2D); }

std::unique_ptr<AxisDimensionSet2DSide> TimeSeries2D::createXAxes()
{ return std::unique_ptr<AxisDimensionSet2DSide>(new AxisDimensionSetTimeSeries2D); }

std::unique_ptr<Graph2DParameters> TimeSeries2D::parseParameters(const Variant::Read &config,
                                                                 const DisplayDefaults &defaults)
{ return std::unique_ptr<Graph2DParameters>(new TimeSeries2DParameters(config, defaults)); }

void TimeSeries2D::checkFinalReady()
{
    if ((ready & Ready_ALL) != Ready_ALL)
        return;
    Graph2D::checkFinalReady();
}

void TimeSeries2D::needlesReady()
{
    ready |= Ready_Needles;
    checkFinalReady();
}

std::vector<GraphPainter2DHighlight::Region> TimeSeries2D::translateHighlightTrace(
        DisplayHighlightController::HighlightType type,
        double start,
        double end,
        double min,
        double max,
        TraceHandler2D *,
        AxisDimension2DSide *x,
        AxisDimension2DSide *y,
        AxisDimension2DColor *,
        const std::unordered_set<std::size_t> &affectedDimensions)
{
    if (!x || !y)
        return {};
    if (!Range::intersects(start, end, this->start, this->end))
        return {};

    std::vector<GraphPainter2DHighlight::Region> result;
    if (affectedDimensions.count(0)) {
        result.emplace_back(x->getTransformer(), y->getTransformer(), start, end, min, max,
                            getParameters()->getHighlightColor(type));
    }
    return result;
}

std::vector<GraphPainter2DHighlight::Region> TimeSeries2D::translateHighlightXBin(
        DisplayHighlightController::HighlightType type,
        double start,
        double end,
        double min,
        double max,
        BinHandler2D *,
        AxisDimension2DSide *x,
        AxisDimension2DSide *y,
        const std::unordered_set<size_t> &affectedDimensions)
{
    if (!x || !y)
        return {};
    if (!Range::intersects(start, end, this->start, this->end))
        return {};

    std::vector<GraphPainter2DHighlight::Region> result;
    if (affectedDimensions.count(0)) {
        result.emplace_back(x->getTransformer(), y->getTransformer(), start, end, min, max,
                            getParameters()->getHighlightColor(type));
    }
    return result;
}

template<typename HandlerType>
static AxisDimension *bindAxis(AxisDimensionSet *axes,
                               const HandlerType *handler,
                               int dim,
                               bool hasBinding,
                               const QString &binding)
{
    if (FP::defined(handler->getLimits().min[dim])) {
        AxisDimension *bound;
        if (hasBinding) {
            bound = axes->get(handler->getUnits(dim), handler->getGroupUnits(dim), binding);
        } else {
            bound = axes->get(handler->getUnits(dim), handler->getGroupUnits(dim));
        }
        bound->registerDataLimits(handler->getLimits().min[dim], handler->getLimits().max[dim]);
        return bound;
    } else if (hasBinding) {
        return axes->get(handler->getUnits(dim), handler->getGroupUnits(dim), binding);
    } else {
        return NULL;
    }
}

bool TimeSeries2D::auxiliaryProcess(const DisplayDynamicContext &context)
{
    bool changed = false;

    if (needles->process(true, context.getInteractive()))
        changed = true;

    return changed;
}

bool TimeSeries2D::auxiliaryBind(const DisplayDynamicContext &context,
                                 AxisDimensionSet2DSide *xAxes,
                                 AxisDimensionSet2DSide *yAxes,
                                 AxisDimensionSet2DColor *zAxes,
                                 std::vector<TraceUtilities::ColorRequest> &findColors,
                                 std::deque<QColor> &claimedColors,
                                 TraceSet2D *traces,
                                 BinSet2D *,
                                 BinSet2D *,
                                 IndicatorSet2D *,
                                 IndicatorSet2D *)
{
    bool changed = false;

    needleBindings.clear();
    auto sortedTraces = needles->getSortedHandlers();
    for (auto trace : sortedTraces) {
        NeedleBinding binding;
        binding.needle = static_cast<TimeSeriesNeedleHandler2D *>(trace);
        TimeSeriesNeedleParameters2D
                *needleParameters = static_cast<TimeSeriesNeedleParameters2D *>(
                binding.needle->getParameters());
        if (!needleParameters->getNeedleEnable())
            continue;

        const auto &points = binding.needle->getPoints();
        if (points.empty() || !FP::defined(points.front().d[1]))
            continue;

        QString format;
        if (needleParameters->hasNeedleLabelFormat())
            format = needleParameters->getNeedleLabelFormat();
        if (format.isEmpty())
            format = binding.needle->getFormat(1);
        if (format.isEmpty())
            continue;
        binding.text = NumberFormat(format).apply(points.front().d[1], QChar());
        if (binding.text.isEmpty())
            continue;

        binding.y = static_cast<AxisDimension2DSide *>(
                bindAxis(yAxes, trace, 1, trace->hasYBinding(), trace->getYBinding()));
        binding.z = static_cast<AxisDimension2DColor *>(
                bindAxis(zAxes, trace, 2, trace->hasZBinding(), trace->getZBinding()));
        needleBindings.append(binding);

        if (trace->axesBound(nullptr, binding.y, binding.z, context.getInteractive()))
            changed = true;
    }

    needles->getColors(findColors, claimedColors, traces);

    auto axes = xAxes->getAllDimensions();
    for (auto it : axes) {
        if (FP::defined(start))
            it->setRawMin(start);
        if (FP::defined(end))
            it->setRawMax(end);
    }
    if (timeBindingUpdated) {
        changed = true;
        timeBindingUpdated = false;
    }

    return changed;
}

bool TimeSeries2D::auxiliaryClaim(const DisplayDynamicContext &context,
                                  Legend &legend,
                                  std::deque<QColor> &dynamicColors,
                                  TraceSet2D *traces,
                                  BinSet2D *,
                                  BinSet2D *,
                                  IndicatorSet2D *,
                                  IndicatorSet2D *)
{
    bool changed = false;

    needles->claimColors(dynamicColors, traces);
    if (needles->addLegend(legend, context, traces))
        changed = true;

    return changed;
}

namespace {
class TimeSeriesPainterNeedles : public Graph2DDrawPrimitive {
    struct Bundle {
        qreal min;
        qreal max;
        int count;
    };
public:
    class Needle {
    public:
        AxisTransformer transformer;
        double position;
        QColor color;
        qreal lineWidth;
        Qt::PenStyle style;
        QFont font;
        QColor textColor;
        QString text;
        quintptr tracePtr;

        qreal originalPosition;
        qreal transformedPosition;
        qreal effectiveHeight;

        std::shared_ptr<Bundle> bundle;

        Needle(const AxisTransformer &setTransformer,
               double setPosition,
               const QColor &setColor,
               qreal setLineWidth,
               Qt::PenStyle setStyle,
               const QFont &setFont,
               const QColor &setTextColor,
               const QString &setText,
               quintptr setTracePtr) : transformer(setTransformer),
                                       position(setPosition),
                                       color(setColor),
                                       lineWidth(setLineWidth),
                                       style(setStyle),
                                       font(setFont),
                                       textColor(setTextColor),
                                       text(setText),
                                       tracePtr(setTracePtr),
                                       transformedPosition(0),
                                       effectiveHeight(0),
                                       bundle()
        { }

        bool operator<(const Needle &other) const
        {
            return originalPosition < other.originalPosition;
        }
    };

private:
    std::vector<Needle> needles;
    qreal traceInnerEdge;
    qreal traceOuterEdge;

    bool adjustPositions()
    {
        bool changed = false;
        for (auto first = needles.begin(), it = needles.begin(), end = needles.end();
                it != end;
                ++it) {
            qreal min = it->transformedPosition - it->effectiveHeight * 0.5;
            qreal max = it->transformedPosition + it->effectiveHeight * 0.5;
            double screenMin = it->transformer.getScreenMin();
            double screenMax = it->transformer.getScreenMax();
            if (screenMin > screenMax)
                qSwap(screenMin, screenMax);

            if (min <= screenMin) {
                changed = true;
                min = screenMin + 1;
                max = min + it->effectiveHeight;
                it->transformedPosition = (min + max) * 0.5;
            } else if (max >= screenMax) {
                changed = true;
                max = screenMax - 1;
                min = min - it->effectiveHeight;
                it->transformedPosition = (min + max) * 0.5;
            }

            std::shared_ptr<Bundle> hit(it->bundle);
            bool combine = false;

            qreal checkMin = min;
            qreal checkMax = max;
            if (it->bundle) {
                checkMin = it->bundle->min;
                checkMax = it->bundle->max;
            }
            checkMin -= needleIntersectionSpace;
            checkMax += needleIntersectionSpace;

            for (auto check = first; check != it; ++check) {
                Q_ASSERT(check->bundle);
                if (it->bundle == check->bundle)
                    continue;
                if (checkMax < check->bundle->min)
                    continue;
                if (checkMin > check->bundle->max)
                    continue;

                hit = check->bundle;
                combine = true;
                break;
            }

            if (!hit) {
                Bundle *add = new Bundle;
                add->min = min;
                add->max = max;
                add->count = 1;
                it->bundle = std::shared_ptr<Bundle>(add);
                continue;
            }

            if (!combine)
                continue;

            changed = true;

            if (!it->bundle || it->bundle->count == 1) {
                it->bundle = hit;
                hit->count++;
                hit->min = qMin(hit->min, min);
                hit->max = qMax(hit->max, max);
                continue;
            }

            hit->min = qMin(hit->min, it->bundle->min);
            hit->max = qMin(hit->max, it->bundle->max);
            hit->count += it->bundle->count;

            std::shared_ptr<Bundle> old(it->bundle);
            for (auto check = first; check != end; ++check) {
                if (check->bundle == old) {
                    check->bundle = hit;
                }
            }
        }

        if (!changed)
            return false;

        std::unordered_set<Bundle *> hit;
        for (auto it = needles.begin(), end = needles.end(); it != end; ++it) {
            if (!it->bundle || it->bundle->count < 2)
                continue;
            if (hit.count(it->bundle.get()))
                continue;

            hit.insert(it->bundle.get());

            qreal meanPosition = 0;
            qreal totalHeight = 0;
            int count = 0;
            int anchor = 0;
            double screenBestMin = FP::undefined();
            double screenBestMax = FP::undefined();
            for (auto n = it; n != end; ++n) {
                if (n->bundle != it->bundle)
                    continue;

                meanPosition += n->transformedPosition;
                totalHeight += n->effectiveHeight;
                ++count;

                double screenMin = n->transformer.getScreenMin();
                double screenMax = n->transformer.getScreenMax();
                if (screenMin > screenMax)
                    qSwap(screenMin, screenMax);
                if (n->transformedPosition + n->effectiveHeight >= screenMax)
                    anchor = 1;
                else if (n->transformedPosition - n->effectiveHeight <= screenMin)
                    anchor = -1;

                if (!FP::defined(screenBestMin) || screenMin < screenBestMin)
                    screenBestMin = screenMin;
                if (!FP::defined(screenBestMax) || screenMax > screenBestMax)
                    screenBestMax = screenMax;
            }
            meanPosition /= (qreal) count;

            qreal prior = meanPosition -
                    (totalHeight + (qreal) (count - 1) * (needleIntersectionSpace + 1)) * 0.5;
            qreal predictedMax = prior + totalHeight;

            if (prior <= screenBestMin)
                anchor = -1;
            if (predictedMax >= screenBestMax)
                anchor = 1;

            if (anchor != 1) {
                if (anchor == -1)
                    prior = screenBestMin;
                it->bundle->min = prior;
                it->bundle->max =
                        prior + totalHeight + (qreal) (count - 1) * (needleIntersectionSpace + 1);

                prior -= needleIntersectionSpace + 1;
                for (auto n = it; n != end; ++n) {
                    if (n->bundle != it->bundle)
                        continue;
                    n->transformedPosition =
                            prior + n->effectiveHeight * 0.5 + needleIntersectionSpace + 1;
                    prior = n->transformedPosition + n->effectiveHeight * 0.5;
                }
            } else {
                prior = screenBestMax + needleIntersectionSpace + 1;
                it->bundle->max = screenBestMax;
                it->bundle->max = screenBestMax -
                        totalHeight -
                        (qreal) (count - 1) * (needleIntersectionSpace + 1);

                for (std::ptrdiff_t i = needles.size() - 1; i >= 0; --i) {
                    if (needles[i].bundle != it->bundle)
                        continue;
                    needles[i].transformedPosition =
                            prior - needles[i].effectiveHeight * 0.5 - needleIntersectionSpace - 1;
                    prior = needles[i].transformedPosition - needles[i].effectiveHeight * 0.5;
                }
            }
        }

        return true;
    }

public:
    TimeSeriesPainterNeedles(std::vector<Needle> setNeedles,
                             qreal setTraceInnerEdge,
                             qreal setTraceOuterEdge,
                             int priority = Graph2DDrawPrimitive::Priority_Default)
            : Graph2DDrawPrimitive(priority),
              needles(std::move(setNeedles)),
              traceInnerEdge(setTraceInnerEdge),
              traceOuterEdge(setTraceOuterEdge)
    { }

    virtual ~TimeSeriesPainterNeedles() = default;

    virtual void paint(QPainter *painter)
    {
        qreal maximumWidth = 0;
        for (auto it = needles.begin(); it != needles.end();) {
            double tr = it->transformer.toScreen(it->position);
            if (!FP::defined(tr)) {
                it = needles.erase(it);
                continue;
            }
            it->transformedPosition = (qreal) tr;
            it->originalPosition = it->transformedPosition;
            it->bundle.reset();

            QFontMetrics fm(it->font, painter->device());
            it->effectiveHeight =
                    GUIStringLayout::maximumCharacterDimensions(it->text, fm).height() *
                            (1.0 + needleHeightPadding);

#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
            qreal width = fm.width(it->text);
#else
            qreal width = fm.horizontalAdvance(it->text);
#endif
            width += fm.height() * (needleWidthPadding + needlePointSize);
            maximumWidth = qMax(maximumWidth, width);

            ++it;
        }
        if (needles.empty())
            return;

        if (needles.size() > 1) {
            std::stable_sort(needles.begin(), needles.end());
            for (std::size_t itter = 0,
                    maxItter = std::max<std::size_t>(20, needles.size() * 2 + 1);
                    itter < maxItter;
                    itter++) {
                if (!adjustPositions())
                    break;
            }
        }

        painter->save();

        QFontMetrics fm(QApplication::font(), painter->device());
        qreal startLinePosition = traceInnerEdge + needleLineStartLength * fm.height();
        qreal pointerTipPosition = traceOuterEdge - maximumWidth;
        qreal transitionEndPosition = pointerTipPosition - needleLineEndLength * fm.height();

        for (const auto &it : needles) {
            qreal pointerCenter = it.transformedPosition;
            qreal startPosition = it.originalPosition;

            QPainterPath path;
            path.moveTo(traceInnerEdge, startPosition);
            path.lineTo(startLinePosition, startPosition);
            path.lineTo(transitionEndPosition, pointerCenter);
            path.lineTo(pointerTipPosition, pointerCenter);

            QBrush brush(it.color);
            painter->setBrush(brush);
            painter->strokePath(path, QPen(brush, it.lineWidth, it.style));


            QFontMetrics fm(it.font, painter->device());
            qreal pointDepth = fm.height() * needlePointSize;
            qreal boxMin = pointerCenter - it.effectiveHeight * 0.5;
            qreal boxMax = pointerCenter + it.effectiveHeight * 0.5;

            path = QPainterPath();
            path.moveTo(pointerTipPosition, pointerCenter);
            path.lineTo(pointerTipPosition + pointDepth, boxMin);
            path.lineTo(pointerTipPosition + pointDepth + maximumWidth, boxMin);
            path.lineTo(pointerTipPosition + pointDepth + maximumWidth, boxMax);
            path.lineTo(pointerTipPosition + pointDepth, boxMax);
            path.closeSubpath();
            painter->setPen(QPen(brush, 0));
            painter->drawPath(path);

            qreal widthPadding = fm.height() * needleWidthPadding;

            brush = QBrush(it.textColor);
            painter->setFont(it.font);
            painter->setPen(QPen(brush, 0));
            painter->drawText(
                    QRectF(pointerTipPosition + pointDepth + widthPadding * 0.5, pointerCenter,
                           maximumWidth - widthPadding, 0),
                    Qt::TextDontClip | Qt::AlignVCenter | Qt::AlignRight, it.text);
        }

        painter->restore();
    }
};
}

bool TimeSeries2D::getAuxiliaryInsets(const DisplayDynamicContext &,
                                      QPaintDevice *paintdevice,
                                      qreal &,
                                      qreal &,
                                      qreal &,
                                      qreal &right)
{
    QFontMetrics fm(QApplication::font());
    if (paintdevice)
        fm = QFontMetrics(QApplication::font(), paintdevice);
    qreal lineSpace = fm.height() *
            (needleLineStartLength + needleLineTransitionLength + needleLineEndLength);

    qreal maximumWidth = 0;
    for (const auto &binding : needleBindings) {
        if (!binding.y)
            continue;

        const TimeSeriesNeedleParameters2D
                *needleParameters = static_cast<const TimeSeriesNeedleParameters2D *>(
                binding.needle->getParameters());

        if (paintdevice)
            fm = QFontMetrics(needleParameters->getNeedleFont(), paintdevice);
        else
            fm = QFontMetrics(needleParameters->getNeedleFont());
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
        qreal width = fm.width(binding.text);
#else
        qreal width = fm.horizontalAdvance(binding.text);
#endif
        width += fm.height() * (needleWidthPadding + needlePointSize);

        maximumWidth = qMax(maximumWidth, width + lineSpace);
    }
    right += maximumWidth;

    return false;
}

std::vector<std::unique_ptr<
        Graph2DDrawPrimitive>> TimeSeries2D::getAuxiliaryDraw(const QRectF &traceArea,
                                                              const QRectF &sideAxisOutline,
                                                              const QSizeF &)
{
    std::vector<std::unique_ptr<Graph2DDrawPrimitive>> result;

    std::vector<TimeSeriesPainterNeedles::Needle> needles;
    for (const auto &binding : needleBindings) {
        if (!binding.y)
            continue;
        auto points = binding.needle->getPoints();
        if (points.empty() || !FP::defined(points.front().d[1]))
            continue;

        const TimeSeriesNeedleParameters2D
                *needleParameters = static_cast<const TimeSeriesNeedleParameters2D *>(
                binding.needle->getParameters());

        needles.emplace_back(binding.y->getTransformer(), points.front().d[1],
                             needleParameters->getColor(), needleParameters->getLineWidth(),
                             needleParameters->getLineStyle(), needleParameters->getNeedleFont(),
                             needleParameters->getNeedleLabelColor(), binding.text,
                             (quintptr) binding.needle);
    }
    if (!needles.empty()) {
        result.emplace_back(new TimeSeriesPainterNeedles(std::move(needles), traceArea.right(),
                                                         sideAxisOutline.right(),
                                                         static_cast<const TimeSeries2DParameters *>(getParameters())
                                                                 ->getNeedleDrawPriority()));
    }

    return std::move(result);
}

QString TimeSeries2D::convertMouseoverToolTipTrace(TraceHandler2D *trace,
                                                   TraceSet2D *traces,
                                                   AxisDimension2DSide *,
                                                   AxisDimension2DSide *y,
                                                   AxisDimension2DColor *z,
                                                   const TracePoint<3> &point,
                                                   const DisplayDynamicContext &context)
{
    QString origin(traces->handlerOrigin(trace, context));
    if (!origin.isEmpty()) {
        origin = tr("%1<br>", "origin insert format").arg(origin);
    }

    QString text
            (tr("<font color=\"%2\">%1</font><br>%3At %4 to %5<br><br>", "trace name format").arg(
                    traces->handlerTitle(trace, context), trace->getColor().name(), origin,
                    GUITime::formatTime(point.start, settings, false),
                    GUITime::formatTime(point.end, settings, false)));

    for (int i = 1; i < 3; i++) {
        double value = point.d[i];
        if (!FP::defined(value))
            break;
        NumberFormat format(trace->getFormat(i));
        QString unitSuffix;
        switch (i) {
        case 1:
            if (y)
                unitSuffix = y->getToolTipUnitOverride();
            break;
        case 2:
            if (z)
                unitSuffix = z->getToolTipUnitOverride();
            break;
        default:
            break;
        }
        if (unitSuffix.isEmpty()) {
            auto sortedUnits = Util::to_qstringlist(trace->getUnits(i));
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (i > 1) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(value, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(value, QChar()),
                                                                   unitSuffix));
        }
    }
    return text;
}

QString TimeSeries2D::convertMouseoverToolTipBins(BinHandler2D *bins,
                                                  BinSet2D *binSet,
                                                  bool,
                                                  AxisDimension2DSide *,
                                                  AxisDimension2DSide *y,
                                                  const BinPoint<1, 1> &point,
                                                  Graph2DMouseoverComponentTraceBox::Location location,
                                                  const DisplayDynamicContext &context)
{
    QString origin(binSet->handlerOrigin(bins, context));
    if (!origin.isEmpty()) {
        origin = tr("%1<br>", "origin insert format").arg(origin);
    }

    QString text(tr("<font color=\"%2\">%1</font><br>%3", "bin name format").arg(
            binSet->handlerTitle(bins, context), bins->getColor().name(), origin));

    if (FP::defined(point.center[0]) && FP::defined(point.width[0]) && point.width[0] > 0.0) {
        text.append(tr("At %1 to %2<br>", "center with width").arg(
                GUITime::formatTime(point.center[0] - point.width[0] * 0.5, settings, false),
                GUITime::formatTime(point.center[0] + point.width[0] * 0.5, settings, false)));
    } else if (FP::defined(point.center[0])) {
        text.append(tr("At %1", "center with no width").arg(
                GUITime::formatTime(point.center[0], settings, false)));
    }

    if (bins->isHistogram()) {
        text.append(tr("<br>%1", "histogram total").arg(Time::describeDuration(point.total[0])));
        if (!FP::defined(point.upper[0])) {
            text.append(
                    tr(", %1 %", "histogram percentage").arg(FP::decimalFormat(point.upper[0], 1)));
        }
    } else {
        double value = FP::undefined();
        switch (location) {
        case Graph2DMouseoverComponentTraceBox::Lowest:
            value = point.lowest[0];
            break;
        case Graph2DMouseoverComponentTraceBox::Lower:
            value = point.lower[0];
            break;
        case Graph2DMouseoverComponentTraceBox::Middle:
            value = point.middle[0];
            break;
        case Graph2DMouseoverComponentTraceBox::Upper:
            value = point.upper[0];
            break;
        case Graph2DMouseoverComponentTraceBox::Uppermost:
            value = point.uppermost[0];
            break;
        }
        if (FP::defined(value)) {
            NumberFormat format(bins->getFormat(1));
            QString unitSuffix;
            if (y)
                unitSuffix = y->getToolTipUnitOverride();
            if (unitSuffix.isEmpty()) {
                auto sortedUnits = Util::to_qstringlist(bins->getUnits(1));
                std::sort(sortedUnits.begin(), sortedUnits.end());
                unitSuffix = sortedUnits.join(tr(" ", "unit join"));
            }
            text.append(
                    tr("<br>%1%2", "bins combine").arg(format.apply(value, QChar()), unitSuffix));
        }
        if (FP::defined(point.total[0])) {
            text.append(tr("<br>%1 points", "bins combine").arg(
                    QString::number((int) qRound(point.total[0]))));
        }
    }

    return text;
}

QString TimeSeries2D::convertMouseoverToolTipIndicator(IndicatorHandler2D *indicator,
                                                       IndicatorSet2D *indicatorSet,
                                                       AxisDimension2DSide *axis,
                                                       const IndicatorPoint<1> &point,
                                                       Axis2DSide side,
                                                       const DisplayDynamicContext &context)
{
    QString origin(indicatorSet->handlerOrigin(indicator, context));
    if (!origin.isEmpty()) {
        origin = tr("%1<br>", "origin insert format").arg(origin);
    }

    QString text(tr("<font color=\"%2\">%1</font><br>%3At %4 to %5<br><br>",
                    "indicator name format").arg(indicatorSet->handlerTitle(indicator, context),
                                                 indicator->getColor().name(), origin,
                                                 GUITime::formatTime(point.start, settings, false),
                                                 GUITime::formatTime(point.end, settings, false)));

    switch (side) {
    case Axis2DLeft:
    case Axis2DRight: {
        double value = point.d[0];
        if (FP::defined(value)) {
            NumberFormat format(indicator->getFormat(0));
            QString unitSuffix;
            if (axis)
                unitSuffix = axis->getToolTipUnitOverride();
            if (unitSuffix.isEmpty()) {
                auto sortedUnits = Util::to_qstringlist(indicator->getUnits(0));
                std::sort(sortedUnits.begin(), sortedUnits.end());
                unitSuffix = sortedUnits.join(tr(" ", "unit join"));
            }
            if (!unitSuffix.isEmpty())
                unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
            text.append(tr("%1%2<br>", "coordinate combine first").arg(format.apply(value, QChar()),
                                                                       unitSuffix));
        }
        break;
    }
    case Axis2DTop:
    case Axis2DBottom:
        break;
    }

    QString add(indicator->triggerToolip(point.trigger));
    if (!add.isEmpty()) {
        text.append(tr("%1", "indicator trigger format").arg(add));
    }

    return text;
}

QString TimeSeries2D::convertMouseoverToolTipFit(TraceHandler2D *trace,
                                                 TraceSet2D *traces,
                                                 AxisDimension2DSide *,
                                                 AxisDimension2DSide *y,
                                                 AxisDimension2DColor *z,
                                                 double fx,
                                                 double fy,
                                                 double fz,
                                                 double fi,
                                                 const DisplayDynamicContext &context)
{
    QString text;

    if (FP::defined(fy)) {
        QString unitSuffix;
        if (y)
            unitSuffix = y->getToolTipUnitOverride();
        NumberFormat format(trace->getFormat(1));
        if (unitSuffix.isEmpty()) {
            auto sortedUnits = Util::to_qstringlist(trace->getUnits(1));
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(fy, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(fy, QChar()),
                                                                   unitSuffix));
        }
    }
    if (FP::defined(fz)) {
        QString unitSuffix;
        if (z)
            unitSuffix = z->getToolTipUnitOverride();
        NumberFormat format(trace->getFormat(2));
        if (unitSuffix.isEmpty()) {
            auto sortedUnits = Util::to_qstringlist(trace->getUnits(2));
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(fz, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(fz, QChar()),
                                                                   unitSuffix));
        }
    }
    if (FP::defined(fi)) {
        if (!text.isEmpty()) {
            text.append(tr(", %1", "fit I combine any").arg(fi));
        } else {
            text.append(tr("%1", "fit I combine first").arg(fi));
        }
    }

    QString title(static_cast<TraceParameters2D *>(
                          trace->getParameters())->getFitLegendText());
    if (title.isEmpty())
        title = traces->handlerTitle(trace, context);

    QString origin(traces->handlerOrigin(trace, context));
    if (!origin.isEmpty()) {
        origin = tr("%1<br>", "origin insert format").arg(origin);
    }

    if (FP::defined(fx)) {
        text.prepend(
                tr("<font color=\"%2\">%1</font><br>%3At %4<br><br>", "trace name format time").arg(
                        title, trace->getFitColor().name(), origin,
                        GUITime::formatTime(fx, settings, false)));
    } else {
        text.prepend(tr("<font color=\"%2\">%1</font><br>%3<br>", "trace name format").arg(title,
                                                                                           trace->getFitColor()
                                                                                                .name(),
                                                                                           origin));
    }

    return text;
}

QString TimeSeries2D::convertMouseoverToolTipModelScan(TraceHandler2D *trace,
                                                       TraceSet2D *traces,
                                                       AxisDimension2DSide *,
                                                       AxisDimension2DSide *y,
                                                       AxisDimension2DColor *z,
                                                       double mx,
                                                       double my,
                                                       double mz,
                                                       const DisplayDynamicContext &context)
{
    QString text;

    if (FP::defined(my)) {
        QString unitSuffix;
        if (y)
            unitSuffix = y->getToolTipUnitOverride();
        NumberFormat format(trace->getFormat(1));
        if (unitSuffix.isEmpty()) {
            auto sortedUnits = Util::to_qstringlist(trace->getUnits(1));
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(my, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(my, QChar()),
                                                                   unitSuffix));
        }
    }
    if (FP::defined(mz)) {
        QString unitSuffix;
        if (z)
            unitSuffix = z->getToolTipUnitOverride();
        NumberFormat format(trace->getFormat(2));
        if (unitSuffix.isEmpty()) {
            auto sortedUnits = Util::to_qstringlist(trace->getUnits(2));
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(mz, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(mz, QChar()),
                                                                   unitSuffix));
        }
    }

    QString origin(traces->handlerOrigin(trace, context));
    if (!origin.isEmpty()) {
        origin = tr("%1<br>", "origin insert format").arg(origin);
    }

    if (FP::defined(mx)) {
        text.prepend(tr("%1<br>%2At %2<br><br>", "fill name format time").arg(
                traces->handlerTitle(trace, context), origin,
                GUITime::formatTime(mx, settings, false)));
    } else {
        text.prepend(
                tr("%1<br>%2<br>", "fill name format").arg(traces->handlerTitle(trace, context),
                                                           origin));
    }

    return text;
}

QString TimeSeries2D::convertMouseoverToolTipFit(BinHandler2D *bins,
                                                 BinSet2D *binSet,
                                                 bool,
                                                 AxisDimension2DSide *,
                                                 AxisDimension2DSide *y,
                                                 BinParameters2D::FitOrigin origin,
                                                 double fx,
                                                 double fy,
                                                 const DisplayDynamicContext &context)
{
    QString text;

    if (FP::defined(fy)) {
        QString unitSuffix;
        if (y)
            unitSuffix = y->getToolTipUnitOverride();
        NumberFormat format(bins->getFormat(0));
        if (unitSuffix.isEmpty()) {
            auto sortedUnits = Util::to_qstringlist(bins->getUnits(0));
            std::sort(sortedUnits.begin(), sortedUnits.end());
            unitSuffix = sortedUnits.join(tr(" ", "unit join"));
        }
        if (!unitSuffix.isEmpty())
            unitSuffix.prepend(tr(" ", "non empty unit suffix prefix"));
        if (!text.isEmpty()) {
            text.append(tr(", %1%2", "coordinate combine any").arg(format.apply(fy, QChar()),
                                                                   unitSuffix));
        } else {
            text.append(tr("%1%2", "coordinate combine first").arg(format.apply(fy, QChar()),
                                                                   unitSuffix));
        }
    }

    QString title(static_cast<BinParameters2D *>(
                          bins->getParameters())->getFitLegendText(origin));
    if (title.isEmpty())
        title = binSet->handlerTitle(bins, context);

    QString originText(binSet->handlerOrigin(bins, context));
    if (!originText.isEmpty()) {
        originText = tr("%1<br>", "origin insert format").arg(origin);
    }

    if (FP::defined(fx)) {
        text.prepend(tr("<font color=\"%2\">%1</font><br>%3At %4<br><br>", "trace name format").arg(
                title, bins->getFitColor(origin).name(), originText,
                GUITime::formatTime(fx, settings, false)));
    } else {
        text.prepend(tr("<font color=\"%2\">%1</font><br>%3<br>", "trace name format").arg(title,
                                                                                           bins->getFitColor(
                                                                                                       origin)
                                                                                               .name(),
                                                                                           originText));
    }

    return text;
}

void TimeSeries2D::zoomMouseX(AxisDimensionSet2DSide *xAxes, qreal minX, qreal maxX)
{
    double convertedStart = FP::undefined();
    double convertedEnd = FP::undefined();
    bool zoomed = false;
    for (auto it : xAxes->getAllDimensions()) {
        if (!FP::defined(convertedStart) || !FP::defined(convertedEnd)) {
            convertedStart = it->getTransformer().toReal(minX);
            convertedEnd = it->getTransformer().toReal(maxX);
        }
        if (FP::defined(it->getZoomMin()) || FP::defined(it->getZoomMax())) {
            zoomed = true;
            break;
        }
    }
    if (zoomed) {
        Graph2D::zoomMouseX(xAxes, minX, maxX);
        return;
    }
    if (FP::defined(convertedStart) && FP::defined(convertedEnd)) {
        emit timeRangeSelected(convertedStart, convertedEnd);
    }
}

}
}
