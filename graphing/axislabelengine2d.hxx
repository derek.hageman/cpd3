/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GRAPHINGAXISLABELENGINE2D_H
#define CPD3GRAPHINGAXISLABELENGINE2D_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QPaintDevice>
#include <QPainter>
#include <QFont>

#include "graphing/graphing.hxx"
#include "graphing/axistickgenerator.hxx"
#include "graphing/axiscommon.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace Graphing {

/**
 * A mechanism to layout the labels along flat 2-dimensional axes.
 */
class CPD3GRAPHING_EXPORT AxisLabelEngine2D : public QObject {
Q_OBJECT
public:
    /**
     * The components of the axis to display.
     */
    enum ShowComponents {
        /** Tick labels. */
        Labels = 0x1,
        /** Tick secondary labels. */
        SecondaryLabels = 0x2,
        /** Tick block labels. */
        BlockLabels = 0x4,

        /** Show all components. */
        AllComponents = Labels | SecondaryLabels | BlockLabels,
        /** The default components to show */
        DefaultComponents = AllComponents
    };

    AxisLabelEngine2D(Axis2DSide pos, int cmps, QObject *parent = 0);

    AxisLabelEngine2D(Axis2DSide pos, QObject *parent = 0);

    AxisLabelEngine2D(Axis2DSide pos, const QFont &baseFont, int cmps, QObject *parent = 0);

    AxisLabelEngine2D(Axis2DSide pos, const QFont &baseFont, QObject *parent = 0);

    AxisLabelEngine2D(const AxisLabelEngine2D &other, QObject *parent = 0);

    ~AxisLabelEngine2D();

    double predictSize(const std::vector<AxisTickGenerator::Tick> &ticks,
                       QPaintDevice *paintdevice = NULL) const;

    double paint(const std::vector<AxisTickGenerator::Tick> &ticks,
                 QPainter *painter,
                 qreal baseline,
                 double min = FP::undefined(),
                 double max = FP::undefined()) const;

    QFont getLabelFont() const;

private:
    void setApplicationDefaults();

    Axis2DSide position;
    int components;

    QFont labelFont;
    QFont secondaryLabelFont;
    QFont blockLabelFont;

    /* These are in fractions of the font height or the average
     * character width of their resective font */
    qreal labelEdgeInset;
    qreal secondaryLabelInset;
    qreal blockLabelInset;
};

}
}

#endif
